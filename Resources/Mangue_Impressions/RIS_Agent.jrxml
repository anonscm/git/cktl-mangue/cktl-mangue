<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="RIS_Agent"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="535"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="28"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="DIRECTORY_JASPER" isForPrompting="false" class="java.lang.String">
		<parameterDescription><![CDATA[BASE DIR pour subreports]]></parameterDescription>
		<defaultValueExpression ><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="NUMERO_FICHIER" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="ETABLISSEMENT" isForPrompting="false" class="java.lang.String"/>
	<parameter name="SERVICE" isForPrompting="false" class="java.lang.String"/>
	<parameter name="VILLE" isForPrompting="false" class="java.lang.String"/>
	<parameter name="NO_INDIVIDU" isForPrompting="true" class="java.lang.Integer">
		<parameterDescription><![CDATA[Numéro de l'individu]]></parameterDescription>
		<defaultValueExpression ><![CDATA[new Integer("1610")]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[SELECT 	I.NOM_USUEL,I.PRENOM,I.C_CIVILITE,I.NO_INDIVIDU,I.IND_NO_INSEE,TO_CHAR(I.IND_CLE_INSEE,'00') IND_CLE_INSEE,I.D_NAISSANCE,
I.VILLE_DE_NAISSANCE,I.NOM_PATRONYMIQUE,S.L_SITUATION_FAMILLE,  
PE.NB_ENFANTS,PE.NUMEN,PE.NO_INDIVIDU_URGENCE, PA.L_NATIONALITE
FROM   	GRHUM.INDIVIDU_ULR I, GRHUM.SITUATION_FAMILIALE S, GRHUM.PERSONNEL_ULR PE, 
GRHUM.PAYS PA
WHERE  	I.NO_INDIVIDU=$P{NO_INDIVIDU} AND S.C_SITUATION_FAMILLE=I.IND_C_SITUATION_FAMILLE AND 
PE.NO_DOSSIER_PERS=I.NO_INDIVIDU AND PA.C_PAYS=I.C_PAYS_NATIONALITE]]></queryString>

	<field name="NOM_USUEL" class="java.lang.String"/>
	<field name="PRENOM" class="java.lang.String"/>
	<field name="C_CIVILITE" class="java.lang.String"/>
	<field name="NO_INDIVIDU" class="java.lang.Integer"/>
	<field name="IND_NO_INSEE" class="java.lang.String"/>
	<field name="IND_CLE_INSEE" class="java.lang.String"/>
	<field name="D_NAISSANCE" class="java.sql.Timestamp"/>
	<field name="VILLE_DE_NAISSANCE" class="java.lang.String"/>
	<field name="NOM_PATRONYMIQUE" class="java.lang.String"/>
	<field name="L_SITUATION_FAMILLE" class="java.lang.String"/>
	<field name="NB_ENFANTS" class="java.lang.Integer"/>
	<field name="NUMEN" class="java.lang.String"/>
	<field name="NO_INDIVIDU_URGENCE" class="java.lang.Integer"/>
	<field name="L_NATIONALITE" class="java.lang.String"/>

	<variable name="Ne" class="java.lang.String" resetType="Page" calculation="Nothing">
		<variableExpression><![CDATA[(($F{C_CIVILITE}.equals("M."))? "Né":"Née "+ $F{NOM_PATRONYMIQUE})]]></variableExpression>
	</variable>
	<variable name="Enfant" class="java.lang.String" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[((($F{NB_ENFANTS}.intValue() == 0) || ($F{NB_ENFANTS}.intValue() == 1)) ? "enfant":"enfants")]]></variableExpression>
	</variable>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="181"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="0"
						width="535"
						height="25"
						forecolor="#000000"
						backcolor="#CCCCCC"
						key="Titre"/>
					<box topBorder="1Point" topBorderColor="#000000" leftBorder="1Point" leftBorderColor="#000000" rightBorder="1Point" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="15" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Déclaration préalable à la remise d'un Relevé Individuel de Situation]]></text>
				</staticText>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="399"  isSplitAllowed="true" >
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="9"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="Distinctions"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Distinctions.jasper"]]></subreportExpression>
				</subreport>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="58"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="Enfants-1"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Enfants.jasper"]]></subreportExpression>
				</subreport>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="83"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="Adresses-1"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Adresses.jasper"]]></subreportExpression>
				</subreport>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="106"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="Telephones-1"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Telephones.jasper"]]></subreportExpression>
				</subreport>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="133"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="Situation"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Situation.jasper"]]></subreportExpression>
				</subreport>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="157"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="Affectations-1"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Affectations.jasper"]]></subreportExpression>
				</subreport>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="180"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="Carriere"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<subreportParameter  name="DIRECTORY_JASPER">
						<subreportParameterExpression><![CDATA[$P{DIRECTORY_JASPER}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Carrieres.jasper"]]></subreportExpression>
				</subreport>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="203"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="Contrat"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Contrats.jasper"]]></subreportExpression>
				</subreport>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="34"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="Diplomes-1"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Diplomes.jasper"]]></subreportExpression>
				</subreport>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="0"
						y="226"
						width="535"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="PasseHorsEtab"
						positionType="Float"
						isRemoveLineWhenBlank="true"/>
					<subreportParameter  name="NO_INDIVIDU">
						<subreportParameterExpression><![CDATA[$F{NO_INDIVIDU}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{DIRECTORY_JASPER}+"Passe.jasper"]]></subreportExpression>
				</subreport>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.server;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion.CktlVersionRequirements;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava;



public class Version extends A_CktlVersion {
	public static final String APPLICATIONFINALNAME = "Mangue";
	public static final String APPLICATIONINTERNALNAME = "Mangue";

	// Version minimum de la base de donnees (user mjefy_budget) necessaire pour fonctionner avec cette version
	public static final String MINDBVERSION = "1.3.2.0";

	public static final String HTMLVERSION = "<font color=\"#FF0000\"><b>Version " + VersionMe.appliVersion() + " du " + VersionMe.VERSIONDATE + "</b></font>";
	public static final String TXTVERSION = "Version " + VersionMe.appliVersion() + " du " + VersionMe.VERSIONDATE;
	public static final String COPYRIGHT = "(c) " + VersionMe.VERSIONDATE.substring(VersionMe.VERSIONDATE.length() - 4);

	/** Version du JRE */
	private static final String JRE_VERSION_MIN = "1.5.0.0";
	private static final String JRE_VERSION_MAX = null;

	/** Version de la base de données requise */
	private static final String BD_VERSION_MIN = "1.3.2.0";
	private static final String BD_VERSION_MAX = null;
	public String name() {
		return VersionMe.APPLICATIONFINALNAME;
	}
	public int versionNumMaj() {
		return VersionMe.VERSIONNUMMAJ;
	}
	public int versionNumMin() {
		return VersionMe.VERSIONNUMMIN;
	}
	public int versionNumPatch() {
		return VersionMe.VERSIONNUMPATCH;
	}
	public int versionNumBuild() {
		return VersionMe.VERSIONNUMBUILD;
	}
	public String date() {
		return VersionMe.VERSIONDATE;
	}

	// liste des dependances
	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[] {
				new CktlVersionRequirements(new CktlVersionJava(), JRE_VERSION_MIN, JRE_VERSION_MAX, true),
				new CktlVersionRequirements(new VersionOracleUser(), BD_VERSION_MIN, BD_VERSION_MAX, true),
		};
	}
}

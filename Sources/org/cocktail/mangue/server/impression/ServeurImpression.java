/*
 * Created on 16 mai 2006
 *
 * Classe d'utilitaires pour l'impression de fichiers XML avec Jasper
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.server.impression;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;

import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOPhotosStructuresGrhum;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.server.Application;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
/**
 *
 * Classe d'utilitaires pour l'impression de fichiers XML avec Jasper
 */

public class ServeurImpression {

	private static String numeroFichier = null;	// utilisé pour éviter que les utilisateurs se détruisent mutuellement les fichiers

	public static final String KEY_EDITION_ORDO_UNIV = "ORDONNATEUR_UNIV";
	public static final String KEY_EDITION_SIGNATURE_PRES = "SIGNATURE_PRESIDENT";

	// accesseurs
	public static String numeroFichier() {
		return numeroFichier;
	}

	public static NSDictionary imprimerFichier(String nomFichierXML,String nomFichierJasper,String cheminAccesRecord) {
		return imprimerFichier(nomFichierXML, nomFichierJasper, cheminAccesRecord,null);
	}
	
	/** utilise Jasper Reports pour imprimer un fichier XML<BR>
	 * Le directory contenant les fichiers Jasper (DIRECTORY_JASPER) est une propriete de l'application<BR>
	 *  Le directory dans lequel sont stockes tous les fichiers d'impression (XML et PDF) est une propriete de l'application<BR>
	 * Jasper travaille dans le directory utilisateur<BR>
	 *  @param nomFichierXML (sans extension)
	 * @param nomFichierJasper
	 * @param cheminAccesRecord	chemin d'acces des donnees dans le fichier (/ est le delimiteur) par exemple /IndividuUlr/ si
	 * @param autresParametres NSDictionary contenant des parametres qui doivent etre transmis a Jasper. 
	 * La cle est une String indiquant le nom du parametre, la valeur sa valeur
	 * <IndividuUlr> est la balise de debut du fichier
	 * @return un NSDictionary avec deux cles : data (null si l'impression a echoue),
	 * 												message (indique si l'impression a fonctionne ou le type d'erreur)
	 */
	public static NSDictionary imprimerFichier(String nomFichierXML,String nomFichierJasper,String cheminAccesRecord,NSDictionary autresParametres) {

		NSMutableDictionary dicoRetour = new NSMutableDictionary();
		// Changer de directory de travail
		String oldCurrentDir = System.getProperty("user.dir");

		try {

			String pathFichierJasper = preparerPathJasper(nomFichierJasper);

			if (!pathExiste(pathFichierJasper)) {
				return new NSDictionary("Fichier Jasper introuvable => " + pathFichierJasper + ")","message");
			}

			String directoryJasper = (String)((NSArray.componentsSeparatedByString(pathFichierJasper, (nomFichierJasper+CocktailConstantes.EXTENSION_JASPER))).get(0));
			HashMap parametres = preparerParametres(directoryJasper, cheminDirectoryImpression("XML"), autresParametres);

			String nomFichier = nomFichierXML + numeroFichier();
			
			// Jasper attend une Url
			String directoryImpression = (String)WOApplication.application().valueForKey("directoryImpression");
			if (directoryImpression == null)
				directoryImpression = System.getProperty("DIRECTORY_IMPRESSION");

			String pathFichierXML = "file://" + directoryCommeURL(directoryImpression) + "/XML/" + nomFichier + CocktailConstantes.EXTENSION_XML;
						
			System.out.println("ServeurImpression.imprimerFichier() " + pathFichierXML);
			
			JRXmlDataSource jrxmlds = new JRXmlDataSource(pathFichierXML,cheminAccesRecord);
			JasperPrint print = JasperFillManager.fillReport(pathFichierJasper,parametres,jrxmlds);

			JRPdfExporter exporter = new JRPdfExporter();
			String fichierImpression = cheminDirectoryImpression("PDF") + nomFichier + ".PDF";
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,fichierImpression);
			exporter.setParameter(JRExporterParameter.JASPER_PRINT,print);
			exporter.exportReport();

			// récupérer les données
			File pdfFile = new File(fichierImpression);

			int lg = (int)pdfFile.length();
			byte[] b = new byte[lg];
			FileInputStream stream = new FileInputStream(pdfFile);
			stream.read(b);
			dicoRetour.setObjectForKey("Impression OK","message");
			dicoRetour.setObjectForKey(new NSData(b),"data");

			LogManager.logDetail("ServeurImpression.imprimerFichier() DICO RETOUR : " + dicoRetour);

		} catch (Exception e) {

			e.printStackTrace();
			LogManager.logException(e);
			dicoRetour.setObjectForKey("Impression echouee :\n" + e.getMessage(),"message");
		} finally {
			System.setProperty("user.dir",oldCurrentDir);
		}
		return dicoRetour;
	}
	
	
	public static void preparerPourImpression() {
		// préparer le numéro de fichier
		if (numeroFichier == null) {
			numeroFichier = DateCtrl.dateToString(new NSTimestamp());
			numeroFichier = numeroFichier.replaceAll("/","") + "_1";
		} else {
			String num = numeroFichier.substring(numeroFichier.lastIndexOf("_") + 1);
			numeroFichier = numeroFichier.substring(0,numeroFichier.lastIndexOf("_"));
			int numero = new Integer(num).intValue() + 1;
			numeroFichier = numeroFichier + "_" + numero;
		}
	}

	/**
	 * @param nomDirectory
	 * @return chemin du directory d'impression et cree les directories manquants si necessaire
	 */
	public static String cheminDirectoryImpression(String nomDirectory) {

		String path = ((Application)Application.application()).directoryImpression();
		if (path == null) {
			path = System.getProperty("DIRECTORY_IMPRESSION");
		}				
		verifierPathEtCreer(path);

		path = path + File.separator + nomDirectory + File.separator;
		verifierPathEtCreer(path);
		return path;
	}

	// méthodes privées
	private static void verifierPathEtCreer(String unPath) {
		//System.out.println("ServeurImpression.verifierPathEtCreer() " + unPath);
		File file = new File(unPath);
		// créer le directory si ils n'existe pas
		if (file.exists() == false) {
			file.mkdir();
		}
	}

	private static boolean pathExiste(String unPath) {
		if (unPath == null || unPath.length() == 0) {
			return false;
		}
		File file = new File(unPath);
		return file.exists();
	}
	private static void preparerLogo(EOEditingContext editingContext,EOStructure etab) {
		if (EOGrhumParametres.findParametre(editingContext,"GRHUM_PHOTO").isParametreVrai()) {
			String cheminDir = cheminDirectoryImpression("XML");
			String filePath = cheminDir + "Logo.jpg";
			if (pathExiste(filePath) == false) {
				EOPhotosStructuresGrhum logo = EOPhotosStructuresGrhum.findForStructure(editingContext, etab);
				if (logo != null) {
					NSData image = logo.datasPhoto();
					if (image  != null) {
						ByteArrayInputStream stream = new ByteArrayInputStream(image.bytes());
						try {
							StreamCtrl.saveContentToFile (stream, filePath);
						} catch (Exception e) {}
					} else {
						try {
							FileCtrl.deleteFile(filePath);
						} catch (Exception e) {}
					}
				}
			}
		}
	}
	private static String directoryCommeURL(String unDirectory) {
		if (unDirectory.indexOf("\\") < 0) {
			return unDirectory;
		} else {
			int lg = unDirectory.indexOf(":");
			if (lg >= 0) {	// Windows nom du disque
				unDirectory = unDirectory.substring(lg + 1);
			}
			unDirectory = unDirectory.replaceAll("\\\\", "/");
			return unDirectory;
		}
	}

	public static NSDictionary imprimerPdfAvecModule(Connection sqlConnection,String nomFichierJasper,NSDictionary valeursParametres) {
		return imprimerAvecModule(sqlConnection, nomFichierJasper, valeursParametres, ManGUEConstantes.IMPRESSION_PDF);
	}
	public static NSDictionary imprimerFichierExcelAvecModule(Connection sqlConnection,String nomFichierJasper,NSDictionary valeursParametres) {
		return imprimerAvecModule(sqlConnection, nomFichierJasper, valeursParametres, ManGUEConstantes.IMPRESSION_EXCEL);
	}

	/** utilise Jasper Reports pour imprimer un fichier. Le module Jasper comporte des requetes SQL<BR>
	 * Le directory contenant les modules Jasper (DIRECTORY_MODULE_IMPRESSION) est une propriete de l'application<BR>
	 *  Le directory dans lequel sont stockes tous les fichiers d'impression (XML et PDF) est une propriete de l'application<BR>
	 * Jasper travaille dans le directory utilisateur<BR>
	 * @param nomFichierJasper
	 * @param parametres	dictionnaire contenant les parametres (cle et valeur) a passer au module Jasper
	 * @return un NSDictionary avec deux cles : data (null si l'impression a echoue),
	 * 												message (indique si l'impression a fonctionne ou le type d'erreur)
	 */
	private static NSDictionary imprimerAvecModule(Connection sqlConnection,String nomFichierJasper,NSDictionary valeursParametres,int typeFichier) {
		NSMutableDictionary dicoRetour = new NSMutableDictionary();

		String defaultDirectoryJasper = ((Application)Application.application()).defaultModuleImpression();
		String directoryJasper = ((Application)Application.application()).directoryModulesImpressionLocal();

		if (directoryJasper == null)
			directoryJasper = defaultDirectoryJasper;
		else
			if (!pathExiste(directoryJasper))
				System.err.println("ATTENTION !! Chemin non existant : " + directoryJasper);
		
		String pathFichierJasper = directoryJasper + nomFichierJasper + CocktailConstantes.EXTENSION_JASPER;
		if (!pathExiste(pathFichierJasper)) {
			directoryJasper = defaultDirectoryJasper;
			pathFichierJasper = defaultDirectoryJasper + nomFichierJasper + CocktailConstantes.EXTENSION_JASPER;
		}
		
		HashMap parametres = preparerParametres(directoryJasper, cheminDirectoryImpression("XML"), valeursParametres);

		String oldCurrentDir = System.getProperty("user.dir");
		System.setProperty("user.dir",cheminDirectoryImpression("XML"));
		try {
			String nomFichier = "EditionDivers" + numeroFichier();
			JasperPrint print = JasperFillManager.fillReport(pathFichierJasper,parametres,sqlConnection);
			JRAbstractExporter exporter = null;
			String fichierImpression = null;
			switch (typeFichier) {
			case ManGUEConstantes.IMPRESSION_PDF :
				exporter = new JRPdfExporter();
				fichierImpression = cheminDirectoryImpression("PDF") + nomFichier + CocktailConstantes.EXTENSION_PDF;
				break;
			case ManGUEConstantes.IMPRESSION_EXCEL :
				exporter = new JRXlsExporter();
				fichierImpression = cheminDirectoryImpression("EXCEL") + nomFichier + CocktailConstantes.EXTENSION_EXCEL;
				break;
			}
			exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,fichierImpression);
			exporter.setParameter(JRExporterParameter.JASPER_PRINT,print);
			exporter.exportReport();
			// récupérer les données
			File dataFile = new File(fichierImpression);
			int lg = (int)dataFile.length();
			byte[] b = new byte[lg];
			FileInputStream stream = new FileInputStream(dataFile);
			stream.read(b);
			dicoRetour.setObjectForKey("Impression OK","message");
			dicoRetour.setObjectForKey(new NSData(b),"data");
		} catch (Exception exc) {
			LogManager.logException(exc);
			dicoRetour.setObjectForKey("Impression echouee\n" +  exc.getMessage(),"message");
		} finally {
			System.setProperty("user.dir",oldCurrentDir);
		}
		return dicoRetour;
	}

	/**
	 * 
	 * @param directoryJasper
	 * @param directoryPourXML
	 * @param autresParametres
	 * @return
	 */
	private static HashMap preparerParametres(String directoryJasper, String directoryPourXML, NSDictionary autresParametres) {
		HashMap parametres = new HashMap();
		parametres.put("DIRECTORY_JASPER",directoryJasper);
		if (directoryPourXML != null) {
			parametres.put("DIRECTORY_IMPRESSION",directoryPourXML);
		}
		parametres.put("NUMERO_FICHIER",numeroFichier());
		EOEditingContext editingContext = new EOEditingContext();
		String ville = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_VILLE);
		if (ville != null) {
			parametres.put("VILLE",ville);
		}
		String rne = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE);
		if (rne != null) {
			EOStructure etablissement = (EOStructure)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "StructureUlr", "cRne", rne);
			if (etablissement != null) {
				parametres.put("ETABLISSEMENT",etablissement.llStructure());
				parametres.put("RNE",etablissement.rne().code());
				if (etablissement.academie() != null) {
					parametres.put("ACADEMIE","Académie de " + etablissement.academie().valueForKey("lAcademie"));
				} else {
					parametres.put("ACADEMIE",new String(""));
				}
				EOAdresse adresse = etablissement.adresseProfessionnelle();
				if (adresse != null) {
					String adrString = "" + adresse.adrAdresse2() + " " + adresse.codePostal() + " " + adresse.ville();
					parametres.put("ADRESSE", adrString);
				}
				// 18/01/2011 - Pour éviter le null pointeur si pas de structure
				preparerLogo(editingContext,etablissement);
			}
		}
		String etablissement = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_ETAB);
		if (etablissement != null) {
			parametres.put(ManGUEConstantes.GRHUM_PARAM_KEY_ETAB, etablissement);
		}
		String president = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_PRESIDENT);
		if (president != null) {
			parametres.put(ManGUEConstantes.GRHUM_PARAM_KEY_PRESIDENT, president);
		}
		
		String ordonnateurUniv = EOGrhumParametres.getOrdonnateurUniv();
		if (ordonnateurUniv != null) {
			parametres.put(KEY_EDITION_ORDO_UNIV, ordonnateurUniv);
		}
		
		String signaturePresident = EOGrhumParametres.getSignaturePresident();
		if (signaturePresident != null) {
			parametres.put(KEY_EDITION_SIGNATURE_PRES, signaturePresident);
		}

		CktlConfig cfg = ((CktlWebApplication)WOApplication.application()).config();
		if (cfg != null) {
			String service = cfg.stringForKey("SERVICE");
			if (service != null) {
				parametres.put("SERVICE",service);
			}
		}
		if (autresParametres != null) {
			for (java.util.Enumeration<String> e = autresParametres.keyEnumerator();e.hasMoreElements();) {
				String key = (String)e.nextElement();
				parametres.put(key,autresParametres.valueForKey(key));
			}
		}
		return parametres;
	}

	/**
	 * 
	 * Recuperation de l'edition dans le module local si elle existe sinon dans la repertoire par defaut
	 * 
	 * @param nomFichierJasper
	 * @return
	 */
	private static String preparerPathJasper(String nomFichierJasper) {

		String defaultDirectoryJasper = ((Application)Application.application()).defaultDirectoryJasper();
		String directoryJasper = ((Application)Application.application()).directoryJasperLocal();

		String path = defaultDirectoryJasper + nomFichierJasper + CocktailConstantes.EXTENSION_JASPER;

		if (directoryJasper == null)
			return path;
		else {
			String pathLocal = directoryJasper + nomFichierJasper + CocktailConstantes.EXTENSION_JASPER;
			if (!pathExiste(pathLocal))
				return path;
			return pathLocal;
		}
	}

}
/*
 * Created on 9 mai 2006
 *
 * Classe de g&eacute;n&eacute;ration de XML de base
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.server.impression;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.xerces.jaxp.SAXParserFactoryImpl;
import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.ParserAdapter;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author christine
 *
 * Classe de g&eacute;n&eacute;ration de XML de base
 */
public class GenerateurXML {
	private static NSMutableDictionary dictionnaireDescriptionEntite;
	private static NSMutableArray objetsTraites;

	/** genere le XML pour un objet
	 * @param nomFichierXML	nom du fichier XML genere (sans extension)
	 * @param record			objet a imprimer supportant l'interface NSKeyValueCoding  
	 * @param imprimerRelations	true si il faut gen&erer le XML pour les relations
	 * @return true si pas de pb pour creer le XML
	 */
	public static boolean genererXMLPourRecord(String nomFichierXML,NSKeyValueCoding record,boolean imprimerRelations) {
		return genererXMLPourRecords(nomFichierXML,null,new NSArray(record),null,imprimerRelations,false);
	}
	/** genere le XML pour un objet
	 * @param nomFichierXML	nom du fichier XML genere (sans extension)
	 * @param record			objet &grave; imprimer supportant l'interface NSKeyValueCoding  
	 * @param imprimerRelations	true si il faut generer le XML pour les relations
	 * @param dictionnaireAutresValeurs	dictionnaire contenant des elements supplementaires qui seront stockes dans le noeud racine.
	 * Les cles du dictionnaire sont les strings qui seront les tags XML pour les valeurs (qui doivent repondre a la methode toString()
	 * @return true si pas de pb pour creer le XML
	 */
	public static boolean genererXMLPourRecord(String nomFichierXML,NSKeyValueCoding record,NSDictionary dict,boolean imprimerRelations) {
		return genererXMLPourRecords(nomFichierXML,null,new NSArray(record),dict,imprimerRelations,false);
	}
	/** genere le XML pour un tableau d'objets de meme type
	 * @param nomFichierXML	nom du fichier XML genere (sans extension)
	 * @param nomEntite		nom d'entite des records  a imprimer (pour le cas ou le tableau est vide)
	 * @param records		tableau de EOGenericRecords a imprimer
	 * @param imprimerRelations	true si il faut generer le XML pour les relations
	 * @return true si pas de pb pour creer le XML
	 */
	public static boolean genererXMLPourRecords(String nomFichierXML,String nomEntite,NSArray records,boolean imprimerRelations) {
		return genererXMLPourRecords(nomFichierXML,nomEntite,records,null,imprimerRelations,true);
	}
	/** genere le XML pour un tableau d'objets de meme type
	 * @param nomFichierXML	nom du fichier XML genere (sans extension)
	 * @param nomEntite		nom d'entite des records  a imprimer (pour le cas ou le tableau est vide)
	 * @param records		tableau de EOGenericRecords a imprimer
	 * @param dictionnaireAutresValeurs	dictionnaire contenant des elements supplementaires qui seront stockes dans le noeud racine.
	 * Les cles du dictionnaire sont les strings qui seront les tags XML pour les valeurs (qui doivent repondre a la methode toString()
	 * @param imprimerRelations	true si il faut generer le XML pour les relations
	 * @return true si pas de pb pour creer le XML
	 */
	public static boolean genererXMLPourRecords(String nomFichierXML,String nomEntite,NSArray records,NSDictionary dictionnaireAutresValeurs,boolean imprimerRelations) {
		return genererXMLPourRecords(nomFichierXML,nomEntite,records,dictionnaireAutresValeurs,imprimerRelations,true);
	}

	// méthodes privées
	private static boolean genererXMLPourRecords(String nomFichierXML,String nomEntite,NSArray records,NSDictionary dictionnaireAutresValeurs,boolean imprimerRelations,boolean shouldAddElement) {

		// charger le fichier de description des entités/attributs à imprimer
		if (dictionnaireDescriptionEntite == null) {
			chargerDescriptions();
		}
		
		try {
			String pathDirectory = ServeurImpression.cheminDirectoryImpression("XML");
			nomFichierXML = nomFichierXML + ServeurImpression.numeroFichier() + CocktailConstantes.EXTENSION_XML;
			String xmlFileName = pathDirectory + File.separator + nomFichierXML;
			CktlXMLWriter w = new CktlXMLWriter(xmlFileName);
			w.startDocument();
			NSKeyValueCoding record = null;
			if (records != null && records.count() > 0) {
				record = (NSKeyValueCoding)records.objectAtIndex(0);
			}
			if (record != null && nomEntite == null) {
				nomEntite = nomEntite(record);
			}
			if (shouldAddElement) {

				w.startElement(nomEntite + "s");	// nom entité au pluriel
				if (dictionnaireAutresValeurs != null) {
					for (java.util.Enumeration<String> e = dictionnaireAutresValeurs.allKeys().objectEnumerator();e.hasMoreElements();) {
						String cle = e.nextElement();
						w.writeElement(cle,dictionnaireAutresValeurs.objectForKey(cle).toString());
					}
				}
			}

			objetsTraites = new NSMutableArray();
			for (java.util.Enumeration<NSKeyValueCoding> e = records.objectEnumerator();e.hasMoreElements();) {
				if (!shouldAddElement && dictionnaireAutresValeurs != null) {
					preparerRecord(w,e.nextElement(),dictionnaireAutresValeurs,imprimerRelations,true,true);
				} else {
					preparerRecord(w,e.nextElement(),imprimerRelations,true,true);
				}
			}
			if (shouldAddElement) {
				w.endElement(); // fin nom entité au pluriel
			}
			w.endDocument();
			w.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return false;
		}
	}
	private static void preparerRecord(CktlXMLWriter w,NSKeyValueCoding record,boolean imprimerRelations,boolean ajouterStartElement,boolean estRacine) throws Exception {
		preparerRecord(w,record,null,imprimerRelations,ajouterStartElement,estRacine);
	}
	// Pour les records de début ou pour les relations to-many, on ajoute un start element
	// Pour les éléments racine, on crée tous les attributs de l'entité d'impression associée (pour les relations, on les imprime uniquement si imprimerRelations est vrai
	// Pour les autres on n'imprime que les attributs/relations obligatoires
	private static void preparerRecord(CktlXMLWriter w,NSKeyValueCoding record,NSDictionary dictionnaireAutresValeurs,boolean imprimerRelations,boolean ajouterStartElement,boolean estRacine) throws Exception {
		String nomEntite = nomEntite(record);

		try {
			if (ajouterStartElement) {	
				w.startElement(nomEntite);	// nom Entité
			}
			if (dictionnaireAutresValeurs != null) {
				for (java.util.Enumeration<String> e = dictionnaireAutresValeurs.allKeys().objectEnumerator();e.hasMoreElements();) {
					String cle = e.nextElement();
					w.writeElement(cle,dictionnaireAutresValeurs.objectForKey(cle).toString());
				}
			}
			// récupérer la liste des attributs à imprimer pour cette entité
			// ajouter les attributs
			EntitePourImpression entite = (EntitePourImpression)dictionnaireDescriptionEntite.objectForKey(nomEntite);
			for (java.util.Enumeration<AttributPourImpression> e = entite.attributsPourImpression().objectEnumerator();e.hasMoreElements();) {
				AttributPourImpression attribut = e.nextElement();
				
				if (estRacine || (!estRacine && attribut.estObligatoire())) {

					Object  value = record.valueForKey(attribut.nom());
					if (value == null) {
						if (attribut.estValeurSimple()) {
							w.writeElement(attribut.nom()," ");
						}
					} else {
						if (value instanceof NSArray) {	// toMany Relation
							if (imprimerRelations) {		//  || attribut.estObligatoire()
								NSArray records = (NSArray)value;
								if (records != null && records.count() > 0) {

									w.startElement(attribut.nom());
									for (java.util.Enumeration<NSKeyValueCoding> e1 = records.objectEnumerator();e1.hasMoreElements();) {
										preparerRecord(w, e1.nextElement(),imprimerRelations,true,false);
									}
									w.endElement();
								}
							}
						} else if (value instanceof EOGenericRecord) {		// toOne relation
							if (imprimerRelations && objetsTraites.containsObject(value) == false || attribut.estObligatoire()) {		//  || attribut.estObligatoire()
								w.startElement(attribut.nom());		// mettre le nom de la relation

								EOGenericRecord recordAImprimer = (EOGenericRecord)value;
								preparerRecord(w,recordAImprimer,imprimerRelations,false,false);
								//objetsTraites.addObject(recordAImprimer);	// pour éviter les boucles  //FIXME : problème si liste historique : objet n'est pas répété
								w.endElement();	// fin relation
							}
						} else if (value instanceof NSTimestamp) {
							w.writeElement(attribut.nom(),DateCtrl.dateToString((NSTimestamp)value));
						} else if (value instanceof String) {
							w.writeElement(attribut.nom(),supprimerCaracteresInvalides((String)value));
						} else {
							w.writeElement(attribut.nom(),value.toString());
						}
					}
				}
			}
			if (ajouterStartElement) {
				w.endElement();	// fin nom entité
			}
		} catch (Exception e) {
			throw e;
		}
	}
	// méthodes privées
	private static void chargerDescriptions() {
		URL urlDescription;
		try {
			urlDescription = WOApplication.application().resourceManager().pathURLForResourceNamed("DescriptionImpression.XML",null,null);
		} catch (Exception e) {
			// on est en version 5.2.1
			String path = WOApplication.application().path() + "/Contents/Resources/" + "DescriptionImpression.XML";
			try {
				urlDescription = new URL(path);
			} catch (Exception e1) {
				// URL malformée
				LogManager.logInformation("URL invalide pour " + path);
				LogManager.logException(e1);
				return;
			}
		}
		dictionnaireDescriptionEntite = new NSMutableDictionary();
		AnalyseurXML analyseur = new AnalyseurXML(dictionnaireDescriptionEntite);
		try {
			SAXParserFactoryImpl parserFactory = new SAXParserFactoryImpl();
			ParserAdapter parseur = null;
			try {
				parseur = new ParserAdapter(parserFactory.newSAXParser().getParser());
			} catch (ParserConfigurationException e2) {
				LogManager.logException(e2);
			}
			parseur.setContentHandler(analyseur);
			try {
				parseur.parse(urlDescription.toString());
			} catch (IOException e1) {
				LogManager.logException(e1);
			}
		} catch (SAXException e) {
			LogManager.logException(e);
		}	
	}
	private static String supprimerCaracteresInvalides(String aStr) {
		return aStr.replaceAll("&"," ");
	}
	private static String nomEntite(NSKeyValueCoding objet) {
		if (objet instanceof EOGenericRecord) {
			return ((EOGenericRecord)objet).classDescription().entityName();
		} else {
			String nom = objet.getClass().getName();
			int index = nom.lastIndexOf(".");
			return nom.substring(index + 1);
		}
	}
}

/*
 * Created on 23 sept. 2004
 *
 *	Ex&eacute;cute le parsing XML
*/
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.server.impression;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author christine
 *
 * AnalyseurXML
 * Classe dont les m&eacute;thodes sont invoqu&eacute;es comme callbacks lors du parsing du fichier XML contenant les sp&eacute;cifications
 * d'impression
 */
public class AnalyseurXML extends DefaultHandler {
	private EntitePourImpression entiteCourante;
	private AttributPourImpression attributCourant;
	private String nom;
	private String attributLu;
	private String attributEnCours;
	private boolean estObligatoire,estValeurSimple;
	private boolean litElement;
	private NSMutableDictionary entitesImpression;
	
	/** Constructeur
	 * 
	 * @param dictionnaire : contiendra les entit&eacute;s lues, le nom d'entit&eacute; servant de cl&eacute;
	 */
	public AnalyseurXML(NSMutableDictionary dictionnaire) {
		super();
		entitesImpression = dictionnaire;
		litElement = false;
	}
	
	public void startElement(String namespaceURI, String localName, String qName,Attributes atts) {
		attributLu = null;
		litElement = true;
		attributEnCours = "";
		if (localName.equals("entite")) {
			attributCourant = null;
			nom = atts.getValue("nom");
			entiteCourante = new EntitePourImpression(nom);
			entitesImpression.setObjectForKey(entiteCourante,nom);
		} else if (localName.equals("attribut")) {
			nom = atts.getValue("nom");
			estObligatoire = (atts.getValue("obligatoire") != null && atts.getValue("obligatoire").equals("oui"));
			estValeurSimple = (atts.getValue("valeur_simple") != null && atts.getValue("valeur_simple").equals("oui"));
			attributCourant = new AttributPourImpression(nom, estObligatoire,estValeurSimple);
			entiteCourante.ajouterAttribut(attributCourant);
		} else if (localName.equals("Description") == false) {
			attributLu = localName;
		}
	}
	
	public void characters(char[] ch,int start,int length) throws SAXException {
		if (litElement && attributLu != null) {		
 			attributEnCours = attributEnCours.concat(new String(ch,start,length));
 		}
	}
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		litElement = false;
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.server.impression;

import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

// 25/02/2011 - modification de l'impression pour les élections de type JDNPHU
public class BureauVotePourImpression extends Object implements NSKeyValueCoding {
	private String nom;
	private NSMutableArray colleges;
	
	public BureauVotePourImpression(String nom) {
		this.nom = nom;
		colleges = new NSMutableArray();
	}
	// Accesseurs
	public String nom() {
		return nom;
	}
	public NSArray colleges() {
		return colleges;
	}
	// autres
	public void ajouterCollege(CollegePourImpression college) {
		colleges.addObject(college);
	}
	public CollegePourImpression collegePourNom(String nom) {
		java.util.Enumeration e = colleges.objectEnumerator();
		while (e.hasMoreElements()) {
			CollegePourImpression college = (CollegePourImpression)e.nextElement();
			if (college.nom().equals(nom)) {
				return college;
			}
		}
		return null;
	}
	public String toString() {
		return "nom : " + nom + ", colleges : " + colleges;
	}
	 // interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// MÆthodes statiques
	/** Pr&eacute;pare une liste des &eacute;lecteurs organis&eacute;s par bureau et coll&egrave;ge &agrave; 
	 * partir d'une liste d'&eacute;lecteurs (EOListeElecteurs)
	 */
	public static NSArray preparerBureauxPourElecteurs(NSArray electeurs, boolean estJDNPHU) {
		NSMutableArray bureaux = new NSMutableArray();
		// Trier les électeurs par bureau de vote, college, section élective et cnu pour le JDNPHU, nom, prénom
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("bureauVote", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("college", EOSortOrdering.CompareAscending));
		// 25/02/2011
		if (estJDNPHU) {
			sorts.addObject(EOSortOrdering.sortOrderingWithKey("cnu", EOSortOrdering.CompareAscending));
		}
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.prenom", EOSortOrdering.CompareAscending));
		electeurs = EOSortOrdering.sortedArrayUsingKeyOrderArray(electeurs, sorts);
		BureauVotePourImpression bureauCourant = null;
		CollegePourImpression collegeCourant = null;
		CnuPourImpression cnuCourant = null;
		java.util.Enumeration e = electeurs.objectEnumerator();
		while (e.hasMoreElements()) {
			EOListeElecteur electeur = (EOListeElecteur)e.nextElement();
			String nomBureau = "Sans Bureau";
			if (electeur.bureauVote() != null) {
				nomBureau = electeur.bureauVote().llBureauVote();
			}
			if (bureauCourant == null || bureauCourant.nom().equals(nomBureau) == false) {
				// Nouveau bureau
				bureauCourant = new BureauVotePourImpression(nomBureau);
				bureaux.addObject(bureauCourant);
				collegeCourant = null;
				cnuCourant = null;
			}
			if (collegeCourant == null || electeur.college().llCollege().equals(collegeCourant.nom()) == false) {
				// Nouveau college
				collegeCourant = new CollegePourImpression(electeur.college().llCollege());
				bureauCourant.ajouterCollege(collegeCourant);
				cnuCourant = null;
			}
			if (estJDNPHU && (cnuCourant == null || electeur.cnu() != cnuCourant.cnu())) {
				// Nouvelle cnu
				cnuCourant = new CnuPourImpression(electeur.cnu());
				collegeCourant.ajouterCnu(cnuCourant);
			}
			if (estJDNPHU == false) {
				collegeCourant.ajouterElecteur(electeur);
			} else {
				cnuCourant.ajouterElecteur(electeur);
			}
		}
		return bureaux;
	}
	
}

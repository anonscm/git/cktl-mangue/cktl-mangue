/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.server.impression;

import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

public class CnuPourImpression implements NSKeyValueCoding {
	private EOCnu cnu;
	private NSMutableArray electeurs;
	
	public CnuPourImpression(EOCnu cnu) {
		this.cnu = cnu;
		electeurs = new NSMutableArray();
	}
	// Accesseurs
	public EOCnu cnu() {
		return cnu;
	}
	public String section() {
		if (cnu == null) {
			return "Non renseignée";
		} else {
			return cnu.code();
		}
	}
	public String sousSection() {
		if (cnu == null) {
			return "";
		} else if (cnu.codeSousSection() == null) {
			return "";
		} else {
			return cnu.codeSousSection();
		}
	}
	public String libelle() {
		if (cnu == null) {
			return "";
		} else {
			return cnu.libelleLong();
		}
	}
	public NSArray electeurs() {
		return electeurs;
	}
	// autres
	public void ajouterElecteur(EOListeElecteur electeur) {
		electeurs.addObject(new IndividuPourImpressionElection(electeur));
	}
	public String toString() {
		return "nom : " + section() + " - " + libelle() + ", electeurs : " + electeurs;
	}
	 // interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}

}

/*
 * Created on 18 mai 2006
 *
 * Contient les méthodes d'impression
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.server.impression;

import java.io.ByteArrayInputStream;
import java.sql.Connection;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiCategorieFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiLocalisationFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiNatureBudgetFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiSpecialisationFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.OccupationFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.Outils;
import org.cocktail.mangue.common.utilities.StreamCtrl;
import org.cocktail.mangue.modele.AffectationServicePublicIndividu;
import org.cocktail.mangue.modele.DetailNbi;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.DureeAvecArrete;
import org.cocktail.mangue.modele.FicheAnciennete;
import org.cocktail.mangue.modele.FicheSynthese;
import org.cocktail.mangue.modele.IndividuPromouvable;
import org.cocktail.mangue.modele.IndividuPromouvableEc;
import org.cocktail.mangue.modele.InfoPourEditionMedicale;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.ParametrageEdition;
import org.cocktail.mangue.modele.StructureAvecFils;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOStatutIndividu;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.elections.EOTypeInstance;
import org.cocktail.mangue.modele.grhum.referentiel.EOBlocNotes;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.cir.EOCirIdentite;
import org.cocktail.mangue.modele.mangue.conges.CongeDetaillePourHistorique;
import org.cocktail.mangue.modele.mangue.conges.EOCld;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.conges.EOCongeFormation;
import org.cocktail.mangue.modele.mangue.conges.EOCongeGardeEnfant;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaternite;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;
import org.cocktail.mangue.modele.mangue.impression.EOContratImpression;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOCgFinActivite;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOHistoPromotion;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.EOStage;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudget;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention;
import org.cocktail.mangue.modele.mangue.individu.medical.EOExamenMedical;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVaccin;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVisiteMedicale;
import org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste;
import org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf;
import org.cocktail.mangue.modele.mangue.lolf.EOPoste;
import org.cocktail.mangue.modele.mangue.modalites.EOCessProgActivite;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.modele.mangue.modalites.EOMiTpsTherap;
import org.cocktail.mangue.modele.mangue.modalites.EORepriseTempsPlein;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;
import org.cocktail.mangue.modele.mangue.nbi.EONbiImpressions;
import org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation;
import org.cocktail.mangue.modele.mangue.prolongations.EOEmeritat;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;
import org.cocktail.mangue.server.ServerThreadManager;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Methodes d'impression
 */

public class Imprimeur {

	private static final String FICHIER_XML_DESTINATAIRES = "Destinataires";
	private static final String FICHIER_XML_PARAM_EDITION = "ParametrageEdition";
	private static final String FICHIER_XML_VISAS = "Visas";
	
	private static Imprimeur sharedInstance = null;

	/**
	 * 
	 * @return
	 */
	public static Imprimeur sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new Imprimeur();
		}
		return sharedInstance;
	}
	/** Imprime le bloc-notes d'un individu pour la periode fournie */
	public NSDictionary imprimerBlocNotes(InfoPourEditionRequete infoEdition,NSArray notes) {
		try {
			ServeurImpression.preparerPourImpression();
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			GenerateurXML.genererXMLPourRecords("BlocNotes",EOBlocNotes.ENTITY_NAME,notes,false);

			return ServeurImpression.imprimerFichier("InfoEdition",infoEdition.nomFichierJasper(),"/InfoPourEditionRequete");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/**
	 * 
	 * @param individu
	 * @return
	 */
	public NSDictionary imprimerSyntheseCarriere(EOIndividu individu) {
		ServeurImpression.preparerPourImpression();
		return imprimerSyntheseCarriere(individu,null);
	}

	/**
	 * 
	 * @param individu
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerSyntheseCarriere(EOIndividu individu,ServerThreadManager manager) {
		ServeurImpression.preparerPourImpression();
		EOEditingContext editingContext = individu.editingContext();
		NSMutableArray arraySynthese = new NSMutableArray();
		// pour ramener à une date sans prendre en compte les heures
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		signaler(manager,"Préparation des éléments de synthèse");
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayCarrieres(editingContext,individu,today));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayContrats(editingContext,individu,today));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext,individu,today, EOStage.ENTITY_NAME, "STA"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext,individu,today, EOCessProgActivite.ENTITY_NAME, "CPA"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext,individu,today, EOCgFinActivite.ENTITY_NAME, EOTypeAbsence.TYPE_CFA));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext,individu,today, EOCld.ENTITY_NAME,"CLD"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext,individu,today, EOClm.ENTITY_NAME,"CLM"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext,individu,today, EOCongeFormation.ENTITY_NAME, EOTypeAbsence.TYPE_CFP));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext,individu,today,"Mad","MAD"));

		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayPasseSyntheseCarriere(editingContext,individu,today));

		EOSortOrdering.sortArrayUsingKeyOrderArray(arraySynthese,new NSArray(EOSortOrdering.sortOrderingWithKey("periodeDeb",EOSortOrdering.CompareDescending)));

		try {
			signaler(manager,"Préparation des données");
			NSMutableDictionary dict = new NSMutableDictionary();
			dict.setObjectForKey(individu.identitePrenomFirst(),"agent");
			String nomEntite = "FicheSynthese";
			GenerateurXML.genererXMLPourRecords("Synthese",nomEntite,arraySynthese,dict,false);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("Synthese","SyntheseCarriere","/" + nomEntite + "s");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/**
	 * 
	 * @param individu
	 * @param dateReference
	 * @param estAncienneteComplete
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerAnciennete(EOIndividu individu,NSTimestamp dateReference,Boolean estAncienneteComplete,ServerThreadManager manager) {
		ServeurImpression.preparerPourImpression();
		
		EOEditingContext editingContext = individu.editingContext();
		NSArray<FicheAnciennete> fichesAnciennete = FicheAnciennete.calculerAnciennete(editingContext,individu,dateReference, estAncienneteComplete.booleanValue());
		
		// ajouter dans un dictionnaire les sommes totales
		NSMutableDictionary dict = new NSMutableDictionary();
		dict.setObjectForKey(individu.identitePrenomFirst(),"agent");
		dict.setObjectForKey(FicheAnciennete.calculerTotal(fichesAnciennete,FicheAnciennete.ANCIENNETE_AUXILIAIRE),"ancienneteAuxiliaireService");
		dict.setObjectForKey(FicheAnciennete.calculerTotal(fichesAnciennete,FicheAnciennete.ANCIENNETE_VALIDES),"ancienneteAuxiliaireValide");
		dict.setObjectForKey(FicheAnciennete.calculerTotal(fichesAnciennete,FicheAnciennete.ANCIENNETE_TITULAIRE),"ancienneteTitulaireService");
		dict.setObjectForKey(FicheAnciennete.calculerTotal(fichesAnciennete,FicheAnciennete.TOUTE_ANCIENNETE),"ancienneteService");
		dict.setObjectForKey(FicheAnciennete.ancienneteGenerale(fichesAnciennete),"ancienneteGenerale");
		dict.setObjectForKey(DateCtrl.dateToString(dateReference),"dateReference");
		try {
			String nomEntite = "FicheAnciennete";
			GenerateurXML.genererXMLPourRecords("Anciennete",nomEntite,fichesAnciennete,dict,false);
			return ServeurImpression.imprimerFichier("Anciennete","FicheAnciennete","/" + nomEntite + "s");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/**
	 * 
	 * @param structure
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerServices(EOStructure structure,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			StructureAvecFils structureImprimable = new StructureAvecFils(structure);
			GenerateurXML.genererXMLPourRecord("Structures",structureImprimable,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("Structures","OrganigrammeService","/StructureAvecFils");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerArreteNbi(EONbiOccupation occupationNbi,NSArray destinataires,NSDictionary dict,ServerThreadManager manager) {
		ServeurImpression.preparerPourImpression();
		preparerDates(occupationNbi.toIndividu(),occupationNbi.dDebNbiOcc());
		NSMutableDictionary dictAutresParametres = new NSMutableDictionary(dict);
		Integer annee = new Integer(DateCtrl.getYear(new NSTimestamp()));
		dictAutresParametres.setObjectForKey(annee,"anneeArrete");
		// rechercher le numéro d'impression
		Number numero  = EONbiImpressions.nouveauNumeroImpressionPourAnnee(occupationNbi.editingContext(),annee);
		dictAutresParametres.setObjectForKey(numero,"noArrete");
		boolean infoReady = preparerInfoImpression(occupationNbi,dictAutresParametres);
		if (infoReady) {
			String typeArrete = ((String)dictAutresParametres.objectForKey("typeArrete"));
			String nomFichierJasper = "";
			if (typeArrete.equals(ManGUEConstantes.TYPE_ARRETE_NBI_PRISE_DE_FONCTION)) {
				nomFichierJasper = "ArreteNbiPriseFonction";
			} else if (typeArrete.equals(ManGUEConstantes.TYPE_ARRETE_NBI_CESSATION)) {
				nomFichierJasper = "ArreteNbiCessation";
			} else if (typeArrete.equals(ManGUEConstantes.TYPE_ARRETE_NBI_REGULARISATION)) {
				nomFichierJasper = "ArreteNbiRegularisation";
			}
			signaler(manager,"Préparation de l'arrêté de Nbi");
			try {
				String nomEntite = "NbiOccupation";
				GenerateurXML.genererXMLPourRecords("ArreteNbi",nomEntite,new NSArray(occupationNbi),dictAutresParametres,true);
				// trier les destinataires par ordre
				destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
				GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,"Destinataire",destinataires,true);
				signaler(manager,"Génération du fichier pdf");
				NSDictionary result = ServeurImpression.imprimerFichier("ArreteNbi",nomFichierJasper,"/" + nomEntite + "s");
				if (result.objectForKey("message").equals("Impression OK")) {
					// sauvegarder l'editing context pour sauvegarder le record nbiImpression
					occupationNbi.editingContext().saveChanges();
				}
				return result;
			} catch (Exception e) {
				LogManager.logException(e);
				return new NSDictionary(e.getMessage(),"message");
			}
		} else {
			return new NSDictionary("Impression échouée","message");
		}
	}

	/**
	 * 
	 * @param dateReference
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerBeneficiairesNbi(NSTimestamp dateReference,ServerThreadManager manager) {
		ServeurImpression.preparerPourImpression();
		EOEditingContext editingContext = new EOEditingContext();
		signaler(manager,"Préparation des données de Nbi");
		NSArray details = DetailNbi.preparerDetailsNbi(editingContext, dateReference);
		// ajouter dans un dictionnaire des infos complémentaires
		NSMutableDictionary dict = new NSMutableDictionary();
		NSTimestamp debutAnnee = CocktailUtilities.debutAnneeUniversitaire(dateReference), finAnnee = CocktailUtilities.finAnneeUniversitaire(dateReference);
		int anneeDeb = DateCtrl.getYear(debutAnnee);
		int anneeFin = DateCtrl.getYear(finAnnee);
		String anneeUniversitaire = "" + anneeDeb;
		if (anneeDeb != anneeFin) {
			anneeUniversitaire = anneeUniversitaire + " - " + anneeFin;
		} 
		dict.setObjectForKey(anneeUniversitaire,"annee");
		try {
			signaler(manager,"Préparation des données");
			String nomEntite = "DetailNbi";
			GenerateurXML.genererXMLPourRecords("BeneficiairesNbi",nomEntite,details,dict,false);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("BeneficiairesNbi","ListeBeneficiairesNbi","/" + nomEntite + "s");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerHistoriqueNbi(NSTimestamp dateDebut,NSTimestamp dateFin,ServerThreadManager manager) {
		ServeurImpression.preparerPourImpression();
		EOEditingContext editingContext = new EOEditingContext();
		signaler(manager,"Préparation des données d'historique de Nbi");
		NSArray details = DetailNbi.preparerDetailsNbiHistorique(editingContext,dateDebut,dateFin);
		// ajouter dans un dictionnaire des infos complémentaires
		NSMutableDictionary dict = new NSMutableDictionary();
		dict.setObjectForKey(DateCtrl.dateToString(dateDebut),"dateDebut");
		dict.setObjectForKey(DateCtrl.dateToString(dateFin),"dateFin");
		try {
			signaler(manager,"Préparation des données");
			String nomEntite = "DetailNbi";
			GenerateurXML.genererXMLPourRecords("HistoriqueNbi",nomEntite,details,dict,false);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("HistoriqueNbi","HistoriqueNbi","/" + nomEntite + "s");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}

	}
	public NSDictionary imprimerFonctionsNbi(NSTimestamp dateReference,ServerThreadManager manager) {
		ServeurImpression.preparerPourImpression();
		EOEditingContext editingContext = new EOEditingContext();
		signaler(manager,"Préparation des données de Nbi");
		NSArray details = DetailNbi.preparerDetailsNbi(editingContext,dateReference);
		// ajouter dans un dictionnaire des infos complémentaires
		NSMutableDictionary dict = new NSMutableDictionary();
		NSTimestamp debutAnnee = CocktailUtilities.debutAnneeUniversitaire(dateReference), finAnnee = CocktailUtilities.finAnneeUniversitaire(dateReference);
		int anneeDeb = DateCtrl.getYear(debutAnnee);
		int anneeFin = DateCtrl.getYear(finAnnee);
		String anneeUniversitaire = "" + anneeDeb;
		if (anneeDeb != anneeFin) {
			anneeUniversitaire = anneeUniversitaire + " - " + anneeFin;
		} 
		dict.setObjectForKey(anneeUniversitaire,"annee");
		try {
			signaler(manager,"Préparation des données");
			String nomEntite = "DetailNbi";
			GenerateurXML.genererXMLPourRecords("FonctionsNbi",nomEntite,details,dict,false);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("FonctionsNbi","ListeFonctionsNbi","/" + nomEntite + "s");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	public NSDictionary imprimerAbsencesPourIndividu(InfoPourEditionRequete infoEdition,EOIndividu individu,NSArray absences,Boolean imprimerDetails,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			GenerateurXML.genererXMLPourRecord("Individu",individu,true);
			// 25/02/2011
			if (imprimerDetails.booleanValue() == false) {
				GenerateurXML.genererXMLPourRecords("Absences", EOAbsences.ENTITY_NAME,absences,false);
				signaler(manager,"Génération du fichier pdf");
				return ServeurImpression.imprimerFichier("InfoEdition","Absences","/InfoPourEditionRequete");
			} else {
				NSArray congesDetailles = CongeDetaillePourHistorique.preparerCongesDetaillesPourAbsences(absences);
				GenerateurXML.genererXMLPourRecords("AbsencesAvecDetails","CongeDetaillePourHistorique",congesDetailles,true);
				signaler(manager,"Génération du fichier pdf");
				return ServeurImpression.imprimerFichier("InfoEdition","AbsencesAvecDetails","/InfoPourEditionRequete");
			}
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}


	public NSDictionary imprimerCirsIdentite(InfoPourEditionRequete infoEdition, NSArray cirs,Boolean imprimerDetails,ServerThreadManager manager) {

		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);

			GenerateurXML.genererXMLPourRecords("CirIde",EOCirIdentite.ENTITY_NAME, cirs, false);

			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("InfoEdition","CirIde","/InfoPourEditionRequete");
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}

	}


	/**
	 * 
	 * @param duree
	 * @param destinataires
	 * @param estArreteAnnulation
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerArreteConge(Duree duree,NSArray destinataires,Boolean estArreteAnnulation,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			String nomEntite = duree.classDescription().entityName();
			preparerDates(duree.individu(),duree.dateDebut());
			LogManager.logDetail("Impression arrete conge de type " + nomEntite);
			LogManager.logInformation("Impression arrete conge de type " + duree);
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS, EOVisa.ENTITY_NAME,duree.visas(),true);
			GenerateurXML.genererXMLPourRecord(nomEntite + "",duree,true);
			GenerateurXML.genererXMLPourRecord(FICHIER_XML_PARAM_EDITION,new ParametrageEdition(duree.editingContext()),true);
			// trier les destinataires par ordre
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			String nomFichierJasper = "Arrete";
			if (estArreteAnnulation.booleanValue()) {
				nomFichierJasper = nomFichierJasper + "Annulation";
			}
			nomFichierJasper = nomFichierJasper + nomEntite;

			return ServeurImpression.imprimerFichier(nomEntite,nomFichierJasper,"/" + nomEntite);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/**
	 * 
	 * @param conge
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerCongeMaternite(EOCongeMaternite conge,NSArray<EODestinataire> destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			preparerDates(conge.individu(),conge.dateDebut());
			String nomEntite = conge.classDescription().entityName();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord(nomEntite + "",conge,true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier(nomEntite,"CongeMaternite","/" + nomEntite);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/**
	 * 
	 * @param conge
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerCongeGardeEnfant(EOCongeGardeEnfant conge,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			preparerDates(conge.individu(),conge.dateDebut());
			String nomEntite = conge.classDescription().entityName();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord(nomEntite + "",conge,true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier(nomEntite,"CongeGardeEnfant","/" + nomEntite);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/**
	 * 
	 * @param avenant
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerArretePourAvenant(EOContratAvenant avenant,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			// Pour afficher la situation à la date de l'avenant
			preparerDates(avenant.individu(),avenant.dateDebut());
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS,"Visa",avenant.visas(),true);
			// trier les destinataires par ordre
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,"Destinataire",destinataires,true);
			GenerateurXML.genererXMLPourRecord("ContratAvenant",avenant,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("ContratAvenant","ArreteAvenant","/ContratAvenant");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/**
	 * 
	 * @param sqlConnection
	 * @param avenant
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerContratTravail(Connection sqlConnection,EOContratAvenant avenant,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			avenant.preparerEmplois();
			avenant.preparerAffectations();
			avenant.preparerActivites();
			// 04/03/2011 - vérifier si il existe des modèles pour le type de contrat
			EOContratImpression contratImpression = EOContratImpression.rechercherContratImpressionPourTypeContrat(avenant.editingContext(), avenant.contrat().toTypeContratTravail(), true);
			if (contratImpression != null) {
				NSMutableDictionary valeursParametres = new NSMutableDictionary();
				valeursParametres.setObjectForKey(avenant.individu().noIndividu(), "NO_INDIVIDU");
				NSDictionary dict = EOUtilities.primaryKeyForObject(avenant.editingContext(),avenant);
				valeursParametres.setObjectForKey(dict.objectForKey("avenantOrdre"), "CTRA_ORDRE");
				String noArrete = avenant.noArretePourEdition();
				if (noArrete != null) {
					valeursParametres.setObjectForKey(noArrete,"NO_ARRETE");
				}
				String indiceBrut = avenant.indiceBrut();
				if (indiceBrut != null) {
					valeursParametres.setObjectForKey(indiceBrut,"INDICE_BRUT");
				}
				String ligneBudgetaire = avenant.contrat().ligneBudgetaire();
				if (ligneBudgetaire != null) {
					valeursParametres.setObjectForKey(ligneBudgetaire,"LIGNE_BUD");
				}
				String programme = avenant.programmeEmploi();
				if (programme != null) {
					valeursParametres.setObjectForKey(programme,"PROGRAMME_EMPLOI");
				}

				String numeroEmploi = avenant.numeroEmploi();
				if (numeroEmploi != null) {
					valeursParametres.setObjectForKey(numeroEmploi,"NUMERO_EMPLOI");
				}

				String lieuAffectation = avenant.lieuAffectation();
				if (lieuAffectation != null) {
					valeursParametres.setObjectForKey(lieuAffectation, "LIEU_AFFECTATION");
				}

				String activites = avenant.activitesPourAvenant();
				if (activites != null) {
					valeursParametres.setObjectForKey(activites, "ACTIVITES");
				}

				if (destinataires.count() > 0) {
					GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
				}
				return ServeurImpression.imprimerPdfAvecModule(sqlConnection, contratImpression.cimpFichier(), valeursParametres);
			} else {
				String nomFichierJasper = "";
				if (avenant.estTitulaireEmploi()) {
					nomFichierJasper = "ContratPourVacant";
				} else if (avenant.quotitesFractionnement() != null && avenant.quotitesFractionnement().length() > 0) {
					nomFichierJasper = "ContratPourRompu";
				}
				if (nomFichierJasper.length() > 0) {
					// Il y a bien un emploi associé, on peut imprimer le contrat de travail
					preparerDates(avenant.contrat().individu(),avenant.dateDebut());
					// trier les destinataires par ordre
					destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
					GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
					GenerateurXML.genererXMLPourRecord("ContratAvenant",avenant,true);
					signaler(manager,"Génération du fichier pdf");
					return ServeurImpression.imprimerFichier("ContratAvenant",nomFichierJasper,"/ContratAvenant");
				} else {
					return new NSDictionary("Pas d'emploi associé, pas de contrat de travail","message");
				}
			}
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/**
	 * 
	 * @param sqlConnection
	 * @param vacation
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerVacation(Connection sqlConnection,EOVacataires vacation,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");

			NSMutableDictionary valeursParametres = new NSMutableDictionary();
			valeursParametres.setObjectForKey(vacation.toIndividu().noIndividu(), "NO_INDIVIDU");
			NSDictionary dict = EOUtilities.primaryKeyForObject(vacation.editingContext(),vacation);
			valeursParametres.setObjectForKey( dict.objectForKey(EOVacataires.VAC_ID_KEY), "VAC_ID");

			String noArrete = vacation.noArretePourEdition();

			if (noArrete != null) {
				valeursParametres.setObjectForKey(noArrete, "NO_ARRETE");
			}				
			if (destinataires.count() > 0) {
				GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			}
			if (vacation.estTitulaire())
				return ServeurImpression.imprimerPdfAvecModule(sqlConnection, "Contrat_Vacation_Fonctionnaire", valeursParametres);
			else
				return ServeurImpression.imprimerPdfAvecModule(sqlConnection, "Contrat_Vacation_Non_Fonctionnaire", valeursParametres);

		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}


	/**
	 * 
	 * @param sqlConnection
	 * @param avenant
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerContratVacation(Connection sqlConnection,EOVacataires vacation, NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");

			vacation.preparerEmplois();
			vacation.preparerAffectations();

			EOContratImpression contratImpression = EOContratImpression.rechercherContratImpressionPourTypeContrat(vacation.editingContext(), vacation.toTypeContratTravail(), true);

			if (contratImpression != null) {
				
				NSMutableDictionary valeursParametres = new NSMutableDictionary();
				valeursParametres.setObjectForKey(vacation.toIndividu().noIndividu(), "NO_INDIVIDU");
				NSDictionary dict = EOUtilities.primaryKeyForObject(vacation.editingContext(), vacation);
				valeursParametres.setObjectForKey(dict.objectForKey("vacId"), "VAC_ID");
				String noArrete = vacation.noArretePourEdition();
				if (noArrete != null) {
					valeursParametres.setObjectForKey(noArrete,"NO_ARRETE");
				}
				
				String programme = vacation.programmeEmploi();
				if (programme != null) {
					valeursParametres.setObjectForKey(programme,"PROGRAMME_EMPLOI");
				}

				String numeroEmploi = vacation.numeroEmploi();
				if (numeroEmploi != null) {
					valeursParametres.setObjectForKey(numeroEmploi,"NUMERO_EMPLOI");
				}

				String lieuAffectation = vacation.lieuAffectation();
				if (lieuAffectation != null) {
					valeursParametres.setObjectForKey(lieuAffectation, "LIEU_AFFECTATION");
				}
				
				if (destinataires.count() > 0) {
					GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
				}
				return ServeurImpression.imprimerPdfAvecModule(sqlConnection, contratImpression.cimpFichier(), valeursParametres);
			} 

			return null;

		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}



	/**
	 * 
	 * @param element
	 * @param destinataires
	 * @param estArreteAnnulation
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerArretePourCarriere(EOElementCarriere element,NSArray destinataires,Boolean estArreteAnnulation,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			preparerDates(element.individu(),element.dateDebut());
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS,"Visa",element.visas(),true);
			// trier les destinataires par ordre
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,"Destinataire",destinataires,true);
			GenerateurXML.genererXMLPourRecord("ElementCarriere" + "",element,true);
			signaler(manager,"Génération du fichier pdf");
			String nomFichierJasper = "ArreteCarriere";
			if (estArreteAnnulation.booleanValue()) {
				nomFichierJasper = nomFichierJasper + "Annulation";
			}
			return ServeurImpression.imprimerFichier("ElementCarriere",nomFichierJasper,"/ElementCarriere");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** imprime un arrete pour un evenement de type Mi-temps therapeutique */
	public NSDictionary imprimerArretePourEvenement(DureeAvecArrete duree,NSArray destinataires,Boolean estArreteAnnulation,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			preparerDates(duree.individu(),duree.dateDebut());
			signaler(manager,"Préparation des données");
			String nomEntite = duree.classDescription().entityName();
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS,EOVisa.ENTITY_NAME,duree.visas(),true);
			// trier les destinataires par ordre
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			GenerateurXML.genererXMLPourRecord(nomEntite + "",duree,true);
			signaler(manager,"Génération du fichier pdf");
			String nomFichierJasper = "Arrete" + nomEntite;
			if (estArreteAnnulation.booleanValue()) {
				nomFichierJasper = nomFichierJasper + "Annulation";
			}
			return ServeurImpression.imprimerFichier(nomEntite,nomFichierJasper,"/" + nomEntite);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/** 
	 * Imprimer la fiche detaillée d'un emploi
	 * 
	 * @param emploi : l'emploi à imprimer
	 * @param manager : manager
	 * @return le fichier
	 */
	public NSDictionary imprimerEmploi(IEmploi emploi, ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager, "Préparation des données");
			EOEmploi unEmploi = (EOEmploi) emploi;
			
			GenerateurXML.genererXMLPourRecord("FicheEmploi", unEmploi, true);
			GenerateurXML.genererXMLPourRecord("DetailEmploi", unEmploi, true);
			
			NSMutableArray liste = new NSMutableArray();
			if (EmploiNatureBudgetFinder.sharedInstance().findEmploiNatureBudgetCourant(unEmploi.editingContext(), emploi) != null) {
				liste.add(EmploiNatureBudgetFinder.sharedInstance().findEmploiNatureBudgetCourant(unEmploi.editingContext(), emploi));
			}
			GenerateurXML.genererXMLPourRecords("DetailEmploiNatureBudget", EOEmploiNatureBudget.ENTITY_NAME, liste, true);
			
			liste.clear();
			if (EmploiCategorieFinder.sharedInstance().findEmploiCategorieCourant(unEmploi.editingContext(), emploi) != null) {
				liste.add(EmploiCategorieFinder.sharedInstance().findEmploiCategorieCourant(unEmploi.editingContext(), emploi));
			}
			GenerateurXML.genererXMLPourRecords("DetailEmploiCategorie", EOEmploiCategorie.ENTITY_NAME, liste, true);
			
			liste.clear();
			if (EmploiSpecialisationFinder.sharedInstance().findEmploiSpecialisationCourante(unEmploi.editingContext(), emploi) != null) {
				liste.add(EmploiSpecialisationFinder.sharedInstance().findEmploiSpecialisationCourante(unEmploi.editingContext(), emploi));
			}
			GenerateurXML.genererXMLPourRecords("DetailEmploiSpecialisation", EOEmploiSpecialisation.ENTITY_NAME, liste, true);
			
			GenerateurXML.genererXMLPourRecords("DetailEmploiLocalisations", EOEmploiLocalisation.ENTITY_NAME, EmploiLocalisationFinder.sharedInstance()
						.findListeAffectationsCourantes(unEmploi.editingContext(), emploi), true);
			
			GenerateurXML.genererXMLPourRecords("DetailEmploiOccupations", EOOccupation.ENTITY_NAME, OccupationFinder.sharedInstance()
						.findListeOccupationsCourantes(unEmploi.editingContext(), emploi), true);

			GenerateurXML.genererXMLPourRecords("DetailEmploiBudgetLolfs", EOPersBudgetAction.ENTITY_NAME, 
					EOPersBudgetAction.listeActionsCourantesParEmploi(unEmploi.editingContext(), emploi), true);

			GenerateurXML.genererXMLPourRecords("DetailEmploiBudgetConventions", EOPersBudgetConvention.ENTITY_NAME, 
					EOPersBudgetConvention.listeConventionsCourantesParEmploi(unEmploi.editingContext(), emploi), true);

			GenerateurXML.genererXMLPourRecords("DetailEmploiBudgetAnalytiques", EOPersBudgetAnalytique.ENTITY_NAME, 
					EOPersBudgetAnalytique.listeAnalytiquesCourantesParEmploi(unEmploi.editingContext(), emploi), true);
			
			signaler(manager, "Génération du fichier pdf");

			return ServeurImpression.imprimerFichier("FicheEmploi", "FicheEmploi", "/Emploi");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(), "message");
		}
	}
	
	/** 
	 * Imprimer la fiche detaillée de l'historique d'un emploi
	 * 
	 * @param emploi : l'emploi à imprimer
	 * @param manager : manager
	 * @return le fichier
	 */
	public NSDictionary imprimerEmploiHistorique(IEmploi emploi, ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager, "Préparation des données");
			EOEmploi unEmploi = (EOEmploi) emploi;
			
			GenerateurXML.genererXMLPourRecord("FicheEmploiHistorique", unEmploi, true);
			GenerateurXML.genererXMLPourRecord("DetailEmploi", unEmploi, true);

			GenerateurXML.genererXMLPourRecords("DetailEmploiNatureBudget", EOEmploiNatureBudget.ENTITY_NAME, EmploiNatureBudgetFinder.sharedInstance()
					.findForEmploi(unEmploi.editingContext(), emploi), true);
			
			GenerateurXML.genererXMLPourRecords("DetailEmploiCategorie", EOEmploiCategorie.ENTITY_NAME, EmploiCategorieFinder.sharedInstance()
					.findForEmploi(unEmploi.editingContext(), emploi), true);
			
			GenerateurXML.genererXMLPourRecords("DetailEmploiSpecialisation", EOEmploiSpecialisation.ENTITY_NAME, EmploiSpecialisationFinder.sharedInstance()
					.findForEmploi(unEmploi.editingContext(), emploi), true);
			
			GenerateurXML.genererXMLPourRecords("DetailEmploiLocalisations", EOEmploiLocalisation.ENTITY_NAME, EmploiLocalisationFinder.sharedInstance()
					.findForEmploi(unEmploi.editingContext(), emploi), true);
			
			GenerateurXML.genererXMLPourRecords("DetailEmploiOccupations", EOOccupation.ENTITY_NAME, OccupationFinder.sharedInstance()
					.findForEmploi(unEmploi.editingContext(), emploi), true);

			GenerateurXML.genererXMLPourRecords("DetailEmploiBudgetLolfs", EOPersBudgetAction.ENTITY_NAME, 
					EOPersBudgetAction.listeActionsParEmploi(unEmploi.editingContext(), emploi), true);

			GenerateurXML.genererXMLPourRecords("DetailEmploiBudgetConventions", EOPersBudgetConvention.ENTITY_NAME, 
					EOPersBudgetConvention.listeConventionsParEmploi(unEmploi.editingContext(), emploi), true);

			GenerateurXML.genererXMLPourRecords("DetailEmploiBudgetAnalytiques", EOPersBudgetAnalytique.ENTITY_NAME, 
					EOPersBudgetAnalytique.listeAnalytiquesParEmploi(unEmploi.editingContext(), emploi), true);
			
			signaler(manager, "Génération du fichier pdf");

			return ServeurImpression.imprimerFichier("FicheEmploiHistorique", "FicheEmploiHistorique", "/Emploi");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(), "message");
		}
	}
	
	
	public NSDictionary imprimerEmplois(InfoPourEditionRequete infoEdition,NSArray emplois,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			String nomEntite = "Emploi";
			GenerateurXML.genererXMLPourRecords("Emplois",nomEntite,emplois,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("InfoEdition","Emplois","/InfoPourEditionRequete");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerAttributionsPrimes(InfoPourEditionRequete infoEdition,NSArray attributionsPrime,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			GenerateurXML.genererXMLPourRecords("Primes","PrimeAttribution",attributionsPrime,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("InfoEdition","Primes","/InfoPourEditionRequete");

		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerPostes(NSArray postes,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");

			String nomEntite = "Poste";
			GenerateurXML.genererXMLPourRecords("Postes",nomEntite,postes,true);
			signaler(manager,"Génération du fichier pdf");

			return ServeurImpression.imprimerFichier("Postes","Postes","/" + nomEntite + "s");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** imprimer la fiche de poste courante ou la fiche Lolf courante d'un poste selon la valeur du booleen */
	public NSDictionary imprimerPoste(EOPoste poste,Boolean estFicheDePoste,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");

			GenerateurXML.genererXMLPourRecord("Individu",poste.occupantActuel(),true);
			GenerateurXML.genererXMLPourRecord("FichePoste",poste,true);
			signaler(manager,"Génération du fichier pdf");
			String nomFichierJasper = "FicheLolfCourante";
			if (estFicheDePoste.booleanValue()) {
				nomFichierJasper = "FicheDePosteCourante";
			}
			return ServeurImpression.imprimerFichier("Individu",nomFichierJasper,"/IndividuUlr");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** imprimer la fiche de poste */
	public NSDictionary imprimerFicheDePoste(EOFicheDePoste fiche,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");

			// on imprime sous la forme d'un record multiple pour passer dans un dictionary des infos sur le poste
			NSDictionary dict = new NSDictionary(fiche.toPoste().libelle(),"intitulePoste");
			String nomEntite = "FicheDePoste";
			GenerateurXML.genererXMLPourRecords("FicheDePoste",EOFicheDePoste.ENTITY_NAME,new NSArray(fiche),dict,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("FicheDePoste","FicheDePoste","/" + nomEntite + "s");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** imprimer la fiche de poste */
	public NSDictionary imprimerFicheLolf(EOFicheLolf fiche,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");

			// on imprime sous la forme d'un record multiple pour passer dans un dictionary des infos sur le poste
			String nomEntite = "FicheLolf";
			NSDictionary dict = new NSDictionary(fiche.toPoste().libelle(),"intitulePoste");
			GenerateurXML.genererXMLPourRecords("FicheLolf",nomEntite,new NSArray(fiche),dict,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("FicheLolf","FicheLolf","/" + nomEntite + "s");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** Impression du resultat des requetes sur les individus */
	public NSDictionary imprimerResultatRequeteIndividu(NSArray elementsAImprimer,InfoPourEditionRequete infoEdition,ServerThreadManager manager) {
		return imprimerResultatRequete(elementsAImprimer,infoEdition,"IndividuPourEdition",manager);
	}
	/** Impression du resultat des requetes sur les conges */
	public NSDictionary imprimerResultatRequeteEvenement(NSArray elementsAImprimer,InfoPourEditionRequete infoEdition,ServerThreadManager manager) {
		return imprimerResultatRequete(elementsAImprimer,infoEdition,"EvenementPourEdition",manager);
	}
	/** Impression du resultat des requetes sur les emplois */
	public NSDictionary imprimerResultatRequeteEmploi(NSArray elementsAImprimer,InfoPourEditionRequete infoEdition,ServerThreadManager manager) {
		return imprimerResultatRequete(elementsAImprimer,infoEdition,"EmploiPourEdition",manager);
	}
	/** Impression de l'arrete de promouvabilite
	 * @param individuPromouvable IndividuPromouvable
	 */
	public NSDictionary imprimerArretePromouvabilite(IndividuPromouvable individuPromouvable,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS,EOVisa.ENTITY_NAME,individuPromouvable.visas(),true);
			GenerateurXML.genererXMLPourRecord("IndividuPromouvable",individuPromouvable,true);
			GenerateurXML.genererXMLPourRecord(FICHIER_XML_PARAM_EDITION,new ParametrageEdition(individuPromouvable.editingContext()),true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("IndividuPromouvable","ArretePromouvabilite","/IndividuPromouvable");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	public NSDictionary imprimerArretePromouvabiliteChevron(IndividuPromouvable individuPromouvable,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager, "Préparation des données");
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS, EOVisa.ENTITY_NAME, individuPromouvable.visas(),true);
			GenerateurXML.genererXMLPourRecord("IndividuPromouvableChevron", individuPromouvable, true);
			GenerateurXML.genererXMLPourRecord(FICHIER_XML_PARAM_EDITION, new ParametrageEdition(individuPromouvable.editingContext()),true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES, EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("IndividuPromouvableChevron", "ArretePromouvabiliteChevron", "/IndividuPromouvable");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/**
	 * 
	 * @param crct
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerArreteCrct(EOCrct crct, NSArray destinataires,ServerThreadManager manager) {

		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS, EOVisa.ENTITY_NAME, crct.visas(),true);
			GenerateurXML.genererXMLPourRecord("Crct", crct,true);
			GenerateurXML.genererXMLPourRecord(FICHIER_XML_PARAM_EDITION, new ParametrageEdition(crct.editingContext()),true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, EODestinataire.SORT_LIBELLE_ASC);
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES, EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("Crct","ArreteCrct","Crct");
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new NSDictionary();
		}
	}	

	/**
	 * 
	 * @param tempsPartiel
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerArreteTempsPartiel(EOTempsPartiel tempsPartiel,NSArray destinataires,ServerThreadManager manager) {

		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS, EOVisa.ENTITY_NAME, tempsPartiel.visas(),true);
			GenerateurXML.genererXMLPourRecord("TempsPartiel", tempsPartiel,true);
			GenerateurXML.genererXMLPourRecord(FICHIER_XML_PARAM_EDITION, new ParametrageEdition(tempsPartiel.editingContext()),true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, EODestinataire.SORT_LIBELLE_ASC);
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES, EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("TempsPartiel","ArreteTempsPartiel","TempsPartiel");
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new NSDictionary();
		}
	}
	
	/**
	 * 
	 * @param reprise
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerArreteRepriseTempsPlein(EORepriseTempsPlein reprise,NSArray destinataires,ServerThreadManager manager) {

		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS, EOVisa.ENTITY_NAME,reprise.visas(),true);
			GenerateurXML.genererXMLPourRecord("RepriseTempsPlein", reprise,true);

			GenerateurXML.genererXMLPourRecord(FICHIER_XML_PARAM_EDITION,new ParametrageEdition(reprise.editingContext()),true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, EODestinataire.SORT_LIBELLE_ASC);
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("RepriseTempsPlein","ArreteRepriseTempsPlein","RepriseTempsPlein");
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}


	/**
	 * 
	 * @param mtt
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerArreteEmeritat(EOEmeritat mtt,NSArray destinataires,ServerThreadManager manager) {

		try {

			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS, EOVisa.ENTITY_NAME, mtt.visas(),true);
			GenerateurXML.genererXMLPourRecord("Emeritat", mtt,true);
			GenerateurXML.genererXMLPourRecord(FICHIER_XML_PARAM_EDITION,new ParametrageEdition(mtt.editingContext()),true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, EODestinataire.SORT_LIBELLE_ASC);
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("Emeritat","ArreteEmeritat","Emeritat");
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}


	/**
	 * 
	 * @param mtt
	 * @param destinataires
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerArreteMiTempsTherapeutique(EOMiTpsTherap mtt,NSArray destinataires,ServerThreadManager manager) {

		try {

			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS, EOVisa.ENTITY_NAME, mtt.visas(),true);
			GenerateurXML.genererXMLPourRecord("MiTempsTherap", mtt,true);
			GenerateurXML.genererXMLPourRecord(FICHIER_XML_PARAM_EDITION,new ParametrageEdition(mtt.editingContext()),true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, EODestinataire.SORT_LIBELLE_ASC);
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("MiTempsTherap","ArreteMiTpsTherap","MiTpsTherap");
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}



	/** imprime l'arrete de promotion des enseignants-chercheurs */
	public NSDictionary imprimerArretePromotionEC(IndividuPromouvableEc individuPromouvable,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			String nomEntite = "IndividuPromouvableEc";
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_VISAS, EOVisa.ENTITY_NAME,individuPromouvable.visas(),true);
			GenerateurXML.genererXMLPourRecord(FICHIER_XML_PARAM_EDITION,new ParametrageEdition(individuPromouvable.editingContext()),true);
			// trier les destinataires par ordre
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,"Destinataire",destinataires,true);
			GenerateurXML.genererXMLPourRecord(nomEntite,individuPromouvable,true);
			signaler(manager,"Génération du fichier pdf");
			String nomFichierJasper = "ArretePromotionEC";
			return ServeurImpression.imprimerFichier(nomEntite,nomFichierJasper,"/" + nomEntite);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerEtatPersonnel(InfoPourEditionRequete infoEdition,NSArray etatsPersonnel,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			GenerateurXML.genererXMLPourRecords("EtatsPersonnels",EOStatutIndividu.ENTITY_NAME,etatsPersonnel,false);
			signaler(manager,"Génération du fichier pdf");

			return ServeurImpression.imprimerFichier("InfoEdition","ListeEtatPersonnel","/InfoPourEditionRequete");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerDifs(InfoPourEditionRequete infoEdition,NSArray difs,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			GenerateurXML.genererXMLPourRecords("Difs","Dif",difs,true);
			signaler(manager,"Génération du fichier pdf");

			return ServeurImpression.imprimerFichier("InfoEdition","ListeDifs","/InfoPourEditionRequete");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerConvocationExamen(EOExamenMedical examen,InfoPourEditionMedicale infoImpression,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			preparerDates(examen.individu(),examen.date());
			signaler(manager,"Préparation des données");
			NSDictionary dictInfoComplementaire = preparerInfoMedicaleComplementaire();
			GenerateurXML.genererXMLPourRecord(infoImpression.nomFichierImpression(),examen,true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");

			return ServeurImpression.imprimerFichier(infoImpression.nomFichierImpression(),infoImpression.nomFichierJasper(),"/ExamenMedical",dictInfoComplementaire);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerConvocationsExamen(NSArray examens,InfoPourEditionMedicale infoImpression,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			NSDictionary dictInfoComplementaire = preparerInfoMedicaleComplementaire();
			GenerateurXML.genererXMLPourRecords(infoImpression.nomFichierImpression(),"ExamenMedical",examens,true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,EODestinataire.ENTITY_NAME,destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier(infoImpression.nomFichierImpression(),infoImpression.nomFichierJasper(),"/ExamenMedicals",dictInfoComplementaire);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerListeConvocationsExamen(NSArray examens,InfoPourEditionMedicale infoImpression,String titre,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(infoImpression.nomFichierImpression(),"ExamenMedical",examens,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier(infoImpression.nomFichierImpression(),infoImpression.nomFichierJasper(),"/ExamenMedicals",new NSDictionary(titre,"TITRE_EDITION"));
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerConvocationVaccin(EOVaccin vaccin,InfoPourEditionMedicale infoImpression,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			preparerDates(vaccin.individu(),vaccin.date());
			signaler(manager,"Préparation des données");
			NSDictionary dictInfoComplementaire = preparerInfoMedicaleComplementaire();
			GenerateurXML.genererXMLPourRecord(infoImpression.nomFichierImpression(),vaccin,true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,"Destinataire",destinataires,true);
			signaler(manager,"Génération du fichier pdf");

			return ServeurImpression.imprimerFichier(infoImpression.nomFichierImpression(),infoImpression.nomFichierJasper(),"/Vaccin",dictInfoComplementaire);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerConvocationsVaccin(NSArray vaccins,InfoPourEditionMedicale infoImpression,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			NSDictionary dictInfoComplementaire = preparerInfoMedicaleComplementaire();
			GenerateurXML.genererXMLPourRecords(infoImpression.nomFichierImpression(),"Vaccin",vaccins,true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,"Destinataire",destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier(infoImpression.nomFichierImpression(),infoImpression.nomFichierJasper(),"/Vaccins",dictInfoComplementaire);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerListeConvocationsVaccin(NSArray vaccins,InfoPourEditionMedicale infoImpression,String titre,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(infoImpression.nomFichierImpression(),"Vaccin",vaccins,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier(infoImpression.nomFichierImpression(),infoImpression.nomFichierJasper(),"/Vaccins",new NSDictionary(titre,"TITRE_EDITION"));
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerConvocationVisite(EOVisiteMedicale visite,InfoPourEditionMedicale infoImpression,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			preparerDates(visite.individu(),visite.date());
			signaler(manager,"Préparation des données");
			NSDictionary dictInfoComplementaire = preparerInfoMedicaleComplementaire();
			GenerateurXML.genererXMLPourRecord(infoImpression.nomFichierImpression(),visite,true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,"Destinataire",destinataires,true);
			signaler(manager,"Génération du fichier pdf");

			return ServeurImpression.imprimerFichier(infoImpression.nomFichierImpression(),infoImpression.nomFichierJasper(),"/VisiteMedicale",dictInfoComplementaire);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerConvocationsVisite(NSArray visites,InfoPourEditionMedicale infoImpression,NSArray destinataires,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			NSDictionary dictInfoComplementaire = preparerInfoMedicaleComplementaire();
			GenerateurXML.genererXMLPourRecords(infoImpression.nomFichierImpression(),"VisiteMedicale",visites,true);
			destinataires = EOSortOrdering.sortedArrayUsingKeyOrderArray(destinataires, new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
			GenerateurXML.genererXMLPourRecords(FICHIER_XML_DESTINATAIRES,"Destinataire",destinataires,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier(infoImpression.nomFichierImpression(),infoImpression.nomFichierJasper(),"/VisiteMedicales",dictInfoComplementaire);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerListeConvocationsVisite(NSArray visites,InfoPourEditionMedicale infoImpression,String titre,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords(infoImpression.nomFichierImpression(),"VisiteMedicale",visites,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier(infoImpression.nomFichierImpression(),infoImpression.nomFichierJasper(),"/VisiteMedicales",new NSDictionary(titre,"TITRE_EDITION"));
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerListeAcmos(NSArray listeAcmos,String titre,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords("ListeAcmos","IndividuAcmoPourEdition",listeAcmos,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("ListeAcmos","ListeAcmos","/IndividuAcmoPourEditions",new NSDictionary(titre,"TITRE_EDITION"));
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** Impression des promotions d'echelons	*/
	public NSDictionary imprimerPromouvables(NSTimestamp debutPeriode,NSTimestamp finPeriode,NSArray promouvables,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			InfoPourEditionRequete infoEdition = new InfoPourEditionRequete("IndividusPromouvables","Liste des Promouvabilités",debutPeriode,finPeriode);
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			String nomEntite = "IndividuPromouvable";
			GenerateurXML.genererXMLPourRecords("IndividusPromouvables",nomEntite,promouvables,true);

			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("InfoEdition","ListePromouvables","/InfoPourEditionRequete");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}




	/** Impression des promotions des individus pour LA-TA	*/
	public NSDictionary imprimerPromotions(NSTimestamp debutPeriode, NSTimestamp finPeriode, NSArray promouvables,String titre,String sousTitre,ServerThreadManager manager) {
		try {

			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			InfoPourEditionRequete infoEdition = new InfoPourEditionRequete("HistoPromotions", titre, sousTitre,debutPeriode,finPeriode);

			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			GenerateurXML.genererXMLPourRecords("HistoPromotions",EOHistoPromotion.ENTITY_NAME,promouvables,true);

			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("InfoEdition", "ListePromouvablesLATA", "/InfoPourEditionRequete");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	
	/**
	 * 
	 * @param promotion
	 * @param dateReference
	 * @param manager
	 * @return
	 */
	public NSDictionary imprimerFicheLATA(EOHistoPromotion promotion,NSTimestamp dateReference,ServerThreadManager manager) {
		ServeurImpression.preparerPourImpression();
		EOEditingContext editingContext = promotion.individu().editingContext();
		try {
			signaler(manager,"Préparation des données");

			signaler(manager,"Préparation des informations sur les diplômes");
			GenerateurXML.genererXMLPourRecords("Diplomes", EODiplomes.ENTITY_NAME, EOIndividuDiplomes.findForIndividu(editingContext,promotion.individu()), true);
			signaler(manager,"Calcul des etats de services");

			NSArray affectationsServicePublic = AffectationServicePublicIndividu.preparerAffectationsServicePublicPourIndividuADate(promotion.individu(), dateReference);

			GenerateurXML.genererXMLPourRecords("AffectationsServicePublic", "AffectationServicePublicIndividu", affectationsServicePublic, false);
			
			signaler(manager,"Calcul de l'ancienneté");
			NSArray fichesAnciennete = FicheAnciennete.calculerAncienneteAvecSyntheseMaximum(editingContext, promotion.individu(), dateReference);
			String nomEntite = "FicheAnciennete";
			// Trier les fiches d'ancienneté par ordre croissant
			fichesAnciennete = EOSortOrdering.sortedArrayUsingKeyOrderArray(fichesAnciennete, new NSArray(EOSortOrdering.sortOrderingWithKey("periodeDeb", EOSortOrdering.CompareAscending)));
			// Les restreindre selon le paramètre de promotion
			fichesAnciennete = promotion.restreindreAnciennete(fichesAnciennete);
			GenerateurXML.genererXMLPourRecords("Anciennete", nomEntite, fichesAnciennete,false);
			// ajouter dans un dictionnaire les autres infos
			NSMutableDictionary dict = new NSMutableDictionary();
			dict.setObjectForKey(FicheAnciennete.calculerTotal(fichesAnciennete, FicheAnciennete.TOUTE_ANCIENNETE), "ancienneteService");
			// Rechercher si il existe des événements comme MTT, CLM, CLD ou CPA
			boolean shouldLookPosition = true;
			NSArray<EOAbsences> evenements = EOAbsences.rechercherEvenementsPourIndividuEtPeriode(promotion.editingContext(), promotion.individu(), dateReference, dateReference);
			if (evenements.count() > 0) {
				EOAbsences evenement = (EOAbsences)evenements.get(0);
				String typeEvenement = evenement.toTypeAbsence().code();
				if (typeEvenement.equals(EOTypeAbsence.TYPE_CLD) || typeEvenement.equals(EOTypeAbsence.TYPE_CLM) ||
						typeEvenement.equals(EOTypeAbsence.TYPE_CPA) || typeEvenement.equals(EOTypeAbsence.TYPE_TEMPS_PARTIEL_THERAP)) {
					dict.setObjectForKey(typeEvenement, "position");
					shouldLookPosition = false;
				}
			}
			if (shouldLookPosition) {
				NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(promotion.editingContext(), promotion.individu(), dateReference, dateReference);
				if (changements.size() > 0) {
					dict.setObjectForKey((changements.get(0)).toPosition().code(), "position");
				}
			}
			NSArray<EOElementCarriere> elements = EOElementCarriere.findForPeriode(promotion.editingContext(), promotion.individu(), dateReference, dateReference);
			if (elements.count() > 0) {
				EOElementCarriere element = elements.get(0);
				if (element.toCarriere().toReferensEmploi() != null) {
					dict.setObjectForKey(element.toCarriere().toReferensEmploi().libelle(), "bap");
				} else if (element.toCarriere().toSpecialiteAtos() != null) {
					dict.setObjectForKey(element.toCarriere().toSpecialiteAtos().libelleLong(), "bap");
				}  else if (element.toCarriere().toDiscSecondDegre() != null) {
					dict.setObjectForKey(element.toCarriere().toDiscSecondDegre().libelleLong(), "bap");
				}
				dict.setObjectForKey(element.toCorps().cCategorie(),"categorie");
				dict.setObjectForKey(element.toCorps().lcCorps(),"corps");
				dict.setObjectForKey(element.toGrade().lcGrade(),"grade");
				if (element.cEchelon() != null) {
					dict.setObjectForKey(element.cEchelon(),"echelon");
				}

			}
			dict.setObjectForKey(DateCtrl.dateToString(dateReference),"dateReference");
			signaler(manager,"Génération des données de promotion");
			GenerateurXML.genererXMLPourRecord("HistoPromotion",promotion,dict,false);
			signaler(manager,"Génération du fichier pdf");
			String nomFichier = "FicheIndividuelleTA";
			if (promotion.paramPromotion().estListeAptitude()) {
				nomFichier = "FicheIndividuelleLA";
			}
			return ServeurImpression.imprimerFichier("Anciennete",nomFichier,"/" + nomEntite + "s");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** Impression des nomenclatures : type d'instance
	 * @param typeInstance type d'&eacute;lection
	 */
	public NSDictionary imprimerTypeInstance(EOTypeInstance typeInstance,Boolean editionDetaillee,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord("TypeInstance",typeInstance,true);

			signaler(manager,"Génération du fichier pdf");
			if (editionDetaillee.booleanValue()) {
				return ServeurImpression.imprimerFichier("TypeInstance","TypeInstance_Detaillee","/TypeInstance");
			} else {
				return ServeurImpression.imprimerFichier("TypeInstance","TypeInstance","/TypeInstance");
			}
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** Impression des nomenclatures : coll&eagrave;ge
	 * @param college  (EOCollege)
	 */
	public NSDictionary imprimerCollege(EOCollege college,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord("College",college,true);

			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("College","College","/College");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** Impression des nomenclatures : secteurs
	 * @param secteurs tableau de EOSecteur
	 */
	public NSDictionary imprimerSecteurs(NSArray secteurs,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords("Secteurs","Secteur",secteurs,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("Secteurs","Secteurs","/");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** Impression des nomenclatures : composantes &eacute;lectives
	 * @param composantes tableau de EOComposanteElective
	 */
	public NSDictionary imprimerComposantesElectives(NSArray composantes,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords("ComposantesElectives","ComposantesElectives",composantes,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("ComposantesElectives","ComposantesElectives","/");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** Impression des nomenclatures : sections &eacute;lectives
	 * @param sections tableau de EOSectionElective
	 */
	public NSDictionary imprimerSectionsElectives(NSArray sections,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords("SectionsElectives","SectionsElectives",sections,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("SectionsElectives","SectionsElectives","/");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	public NSDictionary imprimerCollegesSections(NSArray collegesSections,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords("CollegesSectionsElectives","CollegeSectionElective",collegesSections,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("CollegesSectionsElectives","CollegesSectionsElectives","/");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}

	/** Impression des nomenclatures : bureaux de vote
	 * @param sections tableau de EOSectionElective
	 */
	public NSDictionary imprimerBureauxDeVote(NSArray bureaux,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecords("BureauxVote","BureauxVote",bureaux,true);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("BureauxVote","BureauxVote","/");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** Impression du r&eacute;sultat des listes &eacute;lectorales 
	 * @param instance &eacute;lection recherch&eacute;e
	 * @param typeElecteur (retenus, exclus, sans bureau de vote, double affectation, tous) */
	public NSDictionary imprimerListeElectorale(InfoPourEditionRequete infoEdition,EOInstance instance,Integer typeElecteur,ServerThreadManager manager) {
		try {
			int type = typeElecteur.intValue();
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Préparation des données");
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			NSArray electeurs = EOListeElecteur.rechercherElecteursPourInstanceEtType(instance,type , true);
			NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOListeElecteur.INDIVIDU_KEY + ".nomUsuel", EOSortOrdering.CompareAscending));
			sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOListeElecteur.INDIVIDU_KEY + ".prenom", EOSortOrdering.CompareAscending));
			electeurs = EOSortOrdering.sortedArrayUsingKeyOrderArray(electeurs, sorts);
			if (type == ManGUEConstantes.RETENUS) {
				NSArray bureaux = BureauVotePourImpression.preparerBureauxPourElecteurs(electeurs,instance.typeInstance().estTypeJuridDiscHu());
				GenerateurXML.genererXMLPourRecords("ListeElecteurs","BureauVotePourImpression",bureaux,true);
			} else {
				GenerateurXML.genererXMLPourRecords("ListeElecteurs", EOListeElecteur.ENTITY_NAME,electeurs,true);
			}
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("InfoEdition",infoEdition.nomFichierJasper(),"/InfoPourEditionRequete");
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** imprimer des donn&eacute;es via un module externe au format pdf */
	public NSDictionary imprimerPdfAvecModule(java.sql.Connection connection,String nomModule,NSDictionary parametres,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerPdfAvecModule(connection,nomModule,parametres);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	/** imprimer des donn&eacute;es via un module externe au format excel */
	public NSDictionary imprimerFichierExcelAvecModule(java.sql.Connection connection,String nomModule,NSDictionary parametres,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			signaler(manager,"Génération du fichier Excel");
			return ServeurImpression.imprimerFichierExcelAvecModule(connection,nomModule,parametres);
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	// méthodes privées
	private void genererPhoto(EOIndividu individu) {
		String cheminDir = ServeurImpression.cheminDirectoryImpression("XML");
		String filePath = cheminDir + "Photo" + ServeurImpression.numeroFichier() + ".jpg";
		NSData image = individu.image();
		if (image  != null) {
			ByteArrayInputStream stream = new ByteArrayInputStream(image.bytes());
			try {
				StreamCtrl.saveContentToFile (stream, filePath);
			} catch (Exception e) {}
		} else {
			try {
				FileCtrl.deleteFile(filePath);
			} catch (Exception e) {}
		}
	}
	private void signaler(ServerThreadManager manager,String message) {
		if (manager != null) {
			manager.setMessage(message);
		}
	}
	private boolean preparerInfoImpression(EONbiOccupation occupation,NSDictionary dict) {
		try {
			EONbiImpressions info = new EONbiImpressions();
			info.setCNbi(occupation.toNbi().cNbi());
			info.setImpAnnee((Integer)dict.objectForKey("anneeArrete"));
			info.setImpNumero((Integer)dict.objectForKey("noArrete"));
			info.setImpType((String)dict.objectForKey("typeArrete"));
			info.setNoAuteur((Integer)dict.objectForKey("auteur"));
			NSTimestamp today = new NSTimestamp();
			info.setDCreation(today);
			info.setDModification(today);
			info.setNoDossierPers(occupation.toIndividu().noIndividu());
			occupation.editingContext().insertObject(info);
			return true;
		} catch (Exception e) {
			LogManager.logException(e);
			return false;
		}

	}
	private NSDictionary imprimerResultatRequete(NSArray elementsAImprimer,InfoPourEditionRequete infoEdition,String nomEntiteEdition,ServerThreadManager manager) {
		try {
			ServeurImpression.preparerPourImpression();
			GenerateurXML.genererXMLPourRecord("InfoEdition",infoEdition,false);
			GenerateurXML.genererXMLPourRecords(nomEntiteEdition + "s",nomEntiteEdition,elementsAImprimer,false);
			signaler(manager,"Génération du fichier pdf");
			return ServeurImpression.imprimerFichier("InfoEdition",infoEdition.nomFichierJasper(),"/InfoPourEditionRequete");
		} catch (Exception e) {
			LogManager.logException(e);
			signaler(manager,"Erreur " + e.getMessage());
			return new NSDictionary(e.getMessage(),"message");
		}
	}
	private void preparerDates(EOIndividu individu,NSTimestamp date) {
		individu.setDatePourImpression(date);
	}
	
	/**
	 * 
	 * @return
	 */
	private NSDictionary preparerInfoMedicaleComplementaire() {
		String texteComplementaire = EOGrhumParametres.getComplementVisiteMedicale();
		if (texteComplementaire != null) {
			return new NSDictionary(texteComplementaire,"COMPLEMENT_VISITE_MEDICALE");
		} else {
			return null;
		}
	}

}

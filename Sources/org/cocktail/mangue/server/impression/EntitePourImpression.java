/*
 * Created on 17 mai 2006
 *
 * D&eacute;crit une entit&eacute; &agrave; imprimer
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.server.impression;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**Decrit une entite a imprimer. Elle comporte des attributs qui correspondent a des attributs/relations/methodes
 * des records associes &agrave; cette entite. Lors de l'impression d'un objet : si celui-ci est l'objet d'origine a imprimer tous les attributs sont imprimes. Si il
 * est issu d'une relation, seuls attributs obligatoires sont imprimes<BR>
 * 
 * @author christine
 */
public class EntitePourImpression {
	private String nom;
	private NSMutableArray attributsPourImpression;		// tableau d'attributs d'impression
	
	// constructeur
	/** Constructeur
	 * @param nom
	 */
	public EntitePourImpression(String nom) {
		this.nom = nom;
		attributsPourImpression = new NSMutableArray();
	}
	
	/**
	 * @return Retourne le nom de l'entite
	 */
	public String nom() {
		return nom;
	}
	/**
	 * @return Retourne la liste des sous-rubriques the attributsPourImpression.
	 */
	public NSArray attributsPourImpression() {
		return attributsPourImpression;
	}
	/**
	 * Ajoute une attribut a la liste des attributs
	 * @param attribut attribut à ajouter
	 */
	public void ajouterAttribut(AttributPourImpression attribut) {
		attributsPourImpression.addObject(attribut);
	}
	public String toString() {
		String s = nom() + ", attributs :\n(";
		java.util.Enumeration e = attributsPourImpression().objectEnumerator();
		while (e.hasMoreElements()) {
			s = s + e.nextElement().toString() + ",";
		}
		s = s.substring(0,s.length() - 1) + ")";
		return s;
	}
	
}

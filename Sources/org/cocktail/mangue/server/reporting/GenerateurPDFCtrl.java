package org.cocktail.mangue.server.reporting;

import org.cocktail.reporting.server.CktlAbstractReporter;

/**
 * Controleur permettant de générer des fichiers PDF.
 *
 * @author Chama LAATIK
 */
public class GenerateurPDFCtrl extends GenerateurEditionCtrl {

	/**
	 * Constructeur.
	 * @param xml : le XML à générer
	 * @param titreEdition : titre de l'édition
	 * @param cheminJasper : chemin du jasper
	 * @param recordPath : 
	 */
	public GenerateurPDFCtrl(String xml, String titreEdition, String cheminJasper, String recordPath) {
		super(xml);
		super.setTitreEdition(titreEdition);
		super.setPathForJasper(cheminJasper);
		super.setRecordPath(recordPath);
	}

	@Override
	protected int formatExport() {
		return CktlAbstractReporter.EXPORT_FORMAT_PDF;
	}
}

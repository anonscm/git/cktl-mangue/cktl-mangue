package org.cocktail.mangue.server.reporting;

import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;

/**
 * Implémentation de la fenêtre de progression des impressions
 */
public class ReporterAjaxProgress extends CktlAbstractReporterAjaxProgress implements IJrxmlReportListener {

	/**
	 * @param maximum : maximum de la barre de progression
	 */
	public ReporterAjaxProgress(int maximum) {
		super(maximum);
	}

}

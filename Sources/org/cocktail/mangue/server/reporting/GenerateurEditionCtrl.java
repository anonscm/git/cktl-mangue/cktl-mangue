package org.cocktail.mangue.server.reporting;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.appserver.WOActionResults;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;

/**
 * Super controleur permettant de générer les éditions pour les flux XML.
 * @author Chama LAATIK
 *
 */
public abstract class GenerateurEditionCtrl {
	private CktlAbstractReporter reporter;
	private IJrxmlReportListener listener = new ReporterAjaxProgress(100);

	private String xml;
	
	private String titreEdition;
	private String pathForJasper;
	private String recordPath;
	
	protected abstract int formatExport();
	
	/**
	 * Constructeur.
	 * @param xml : le XML à générer
	 */
	public GenerateurEditionCtrl(String xml) {
		//TODO : transformer en singleton?
		this.xml = xml;
	}
	
	/**
	 * Génère l'édition demandée
	 * @return null
	 */
	public WOActionResults genererEdition() {
		reporter = reporterEdition();		
		return null;
	}
	
	private CktlAbstractReporter reporterEdition() {
		JrxmlReporterWithXmlDataSource jr = null;
		
		try {
			InputStream xmlFileStream = new ByteArrayInputStream(xml.getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			
			jr.printWithThread(getTitreEdition(), xmlFileStream, getRecordPath(), pathForJasper(getPathForJasper()),
					parameters, formatExport(), true, getListener());						

		} catch (Throwable e) {
			e.printStackTrace();
		}
		return jr;
	}
	
	/**
	 * @param reportPath : le jasper
	 * @return le chemin du jasper
	 */
	protected String pathForJasper(String reportPath) {
		ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
		URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
		return url.getFile();
	}
	
	public CktlAbstractReporter getReporter() {
		return reporter;
	}

	public IJrxmlReportListener getListener() {
		return listener;
	}

	public String getTitreEdition() {
		return titreEdition;
	}

	public void setTitreEdition(String titreEdition) {
		this.titreEdition = titreEdition;
	}

	public String getPathForJasper() {
		return pathForJasper;
	}

	public void setPathForJasper(String pathForJasper) {
		this.pathForJasper = pathForJasper;
	}

	public String getRecordPath() {
		return recordPath;
	}

	public void setRecordPath(String recordPath) {
		this.recordPath = recordPath;
	}
}

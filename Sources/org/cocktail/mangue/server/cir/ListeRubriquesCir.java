/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.server.cir;

import org.cocktail.common.LogManager;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ListeRubriquesCir {
	private NSMutableDictionary dictionnaireRubriquesIndividu;
	private NSMutableDictionary dictionnaireRubriquesGenerales;
	
	private final static int PRIORITE_ENTETE = 0;
	private final static int PRIORITE_FIN = 99;

	public ListeRubriquesCir() {
		dictionnaireRubriquesIndividu = new NSMutableDictionary();
		dictionnaireRubriquesGenerales = new NSMutableDictionary();
	}
	public void ajouterRubrique(String nom,RubriqueCir rubriqueCir) {
		if (rubriqueCir.priorite() == PRIORITE_ENTETE || rubriqueCir.priorite() == PRIORITE_FIN) {
			dictionnaireRubriquesGenerales.setObjectForKey(rubriqueCir,nom);
		} else {
			dictionnaireRubriquesIndividu.setObjectForKey(rubriqueCir,nom);
		}
	}
	public RubriqueCir rubriqueGeneralePourNom(String nom) {
		return (RubriqueCir)dictionnaireRubriquesGenerales.valueForKey(nom);
	}
	public RubriqueCir rubriqueIndividuPourNom(String nom) {
		return (RubriqueCir)dictionnaireRubriquesIndividu.valueForKey(nom);
	}
	public NSArray rubriquesIndividuTriees() {
		NSArray rubriques = dictionnaireRubriquesIndividu.allValues();
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(rubriques, new NSArray(EOSortOrdering.sortOrderingWithKey("priorite", EOSortOrdering.CompareAscending)));
	}
	public void imprimerResumeRubriques() {
		java.util.Enumeration e = dictionnaireRubriquesGenerales.allValues().objectEnumerator();
		while (e.hasMoreElements()) {
			RubriqueCir rubrique = (RubriqueCir)e.nextElement();
			LogManager.logInformation(rubrique.shortString());
		}
		e = dictionnaireRubriquesIndividu.allValues().objectEnumerator();
		while (e.hasMoreElements()) {
			RubriqueCir rubrique = (RubriqueCir)e.nextElement();
			LogManager.logInformation(rubrique.shortString());
		}
	}
	public void imprimerRubriques() {
		java.util.Enumeration e = dictionnaireRubriquesGenerales.allValues().objectEnumerator();
		while (e.hasMoreElements()) {
			RubriqueCir rubrique = (RubriqueCir)e.nextElement();
			LogManager.logInformation(rubrique.toString());
		}
		e = dictionnaireRubriquesIndividu.allValues().objectEnumerator();
		while (e.hasMoreElements()) {
			RubriqueCir rubrique = (RubriqueCir)e.nextElement();
			LogManager.logInformation(rubrique.toString());
		}
	}
}

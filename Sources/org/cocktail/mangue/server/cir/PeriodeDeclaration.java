// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeCirAvecDates.java

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.foundation.NSTimestamp;

public class PeriodeDeclaration {

    private NSTimestamp dateDebut;
    private NSTimestamp dateFin;
    private EOIndividu individu;

    public PeriodeDeclaration(EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin)  {
    	    	
          this.dateDebut = dateDebut;
          this.dateFin = dateFin;
          this.individu = individu;
        
    }

    /**
     * 
     */
    public NSTimestamp getDateDebut() {    	
    	return dateDebut;
    }
    public NSTimestamp getDateFin() {    	
    	return dateFin;
    }
    public EOIndividu getIndividu() {    	
    	return individu;
    }


}
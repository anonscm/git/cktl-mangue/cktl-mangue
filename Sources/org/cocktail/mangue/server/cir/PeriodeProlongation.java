// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeProlongation.java

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.modele.mangue.prolongations.EOReculAge;
import org.cocktail.mangue.modele.mangue.prolongations.ProlongationActivite;

import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.server.cir:
//            PeriodeCirAvecDates

public class PeriodeProlongation extends PeriodeCirAvecDates
{

    public PeriodeProlongation(ProlongationActivite prolongation, NSTimestamp dateFin)
    {
        super(prolongation.dateDebut(), dateFin, true);
        this.prolongation = prolongation;
    }

    public String motifProlongation()
    {
        return prolongation.motif().codeOnp();
    }

    public String nom1()
    {
        if((prolongation instanceof EOReculAge) && ((EOReculAge)prolongation).enfant() != null)
            return ((EOReculAge)prolongation).enfant().nom();
        else
            return null;
    }

    public String prenom1()
    {
        if((prolongation instanceof EOReculAge) && ((EOReculAge)prolongation).enfant() != null)
            return ((EOReculAge)prolongation).enfant().prenom();
        else
            return null;
    }

    public String nom2()
    {
        if((prolongation instanceof EOReculAge) && ((EOReculAge)prolongation).enfant2() != null)
            return ((EOReculAge)prolongation).enfant2().nom();
        else
            return null;
    }

    public String prenom2()
    {
        if((prolongation instanceof EOReculAge) && ((EOReculAge)prolongation).enfant2() != null)
            return ((EOReculAge)prolongation).enfant2().prenom();
        else
            return null;
    }

    public String nom3()
    {
        if((prolongation instanceof EOReculAge) && ((EOReculAge)prolongation).enfant3() != null)
            return ((EOReculAge)prolongation).enfant3().nom();
        else
            return null;
    }

    public String prenom3()
    {
        if((prolongation instanceof EOReculAge) && ((EOReculAge)prolongation).enfant3() != null)
            return ((EOReculAge)prolongation).enfant3().prenom();
        else
            return null;
    }

    private ProlongationActivite prolongation;
}

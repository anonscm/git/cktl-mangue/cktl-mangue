/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.server.cir;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

// 01/12/2010 - Vérification du path du directory avant la création d'un fichier
public class FichierCir {
	private static BufferedWriter fichierCIR;
		
	/**
	 * 
	 * @param path
	 * @param nomFichier
	 * @param dictionnaire
	 */
	public static void ajouterFichierAuDictionnaire(String path,String nomFichier,NSMutableDictionary dictionnaire) {
		// on récupére des lignes plutôt qu'une string car les délimiteurs de fin de ligne diffèrent d'une plate-forme à l'autre
		// et qu'on ne connaît pas la plateforme d'arrivée
		NSArray lignes = lireLignesFichier(path, nomFichier);
		String dateDuJour = DateCtrl.dateToString(new NSTimestamp(),"%Y%m%d");
		if (lignes != null  && lignes.count() > 0) {
			// rechercher où commence l'extension si il y en a une
			String nouveauNom = "";
			int lg = nomFichier.indexOf(".");
			if (lg > 0) {
				nouveauNom = nomFichier.substring(0,lg) + dateDuJour + nomFichier.substring(lg);
			} else {
				nouveauNom = nomFichier + "_" + dateDuJour;
			}
			
			dictionnaire.setObjectForKey(lignes, nomFichier);
		}
	}

	
	
/**	Retourne le contenu du fichier Cir sous forme de NSData */
	public static NSData lireFichierCir(String pathFichierCir) {
		// récupérer les données
		File file = new File(pathFichierCir);
		int lg = (int)file.length();
		byte[] b = new byte[lg];
		try {
			FileInputStream stream = new FileInputStream(file);
		stream.read(b);
		return new NSData(b);
		} catch (Exception e) {
			return null;
		}
	}
	/** Creation du fichier Cir */
	public static void creerFichier(String path, String nomFichier) {
		try {
			// créer le fichier
			String newPath = new String(path);
			// 01/12/2010 - Vérification du path du directory
			verifierPathEtCreer(newPath);
			newPath = path.concat(System.getProperty("file.separator"));
			newPath =  newPath.concat(nomFichier);
			FileOutputStream stream = new  FileOutputStream(new File(newPath));
			fichierCIR = new BufferedWriter(new OutputStreamWriter(stream,Charset.forName("ISO-8859-1")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** Ecrit dans le fichier Cir */
	public static void ecrire(String texte)  throws Exception {
		try {
			fichierCIR.write(texte);
		} catch (IOException e) {
			throw new Exception("Erreur lors de l'ecriture dans le fichier DADS " + e.getMessage());
		}
	}
	public static void fermerFichier() {
		try {
			fichierCIR.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static RandomAccessFile randomFichierPourPath(String path,String nomFichier) throws Exception {
		// créer le fichier
		String newPath = new String(path);
		newPath = path.concat(System.getProperty("file.separator"));
		newPath =  newPath.concat(nomFichier);
		RandomAccessFile fichier = new  RandomAccessFile(new File(newPath),"rw");
		return fichier;
	}
	public static BufferedReader fichierEnLecturePourPath(String path,String nomFichier) throws Exception {
		String newPath = new String(path);
		newPath = path.concat(System.getProperty("file.separator"));
		newPath =  newPath.concat(nomFichier);
		FileInputStream stream = new  FileInputStream(new File(newPath));
		BufferedReader fichier = new BufferedReader(new InputStreamReader(stream,Charset.forName("ISO-8859-1")));
		return fichier;
	}
	public static NSArray lireLignesFichier(String path,String nomFichier) {
		try {
			// créer le fichier
			String newPath = new String(path);
			newPath = path.concat(System.getProperty("file.separator"));
			newPath =  newPath.concat(nomFichier);
			File file = new File(newPath);
			if (file.exists()) {
				FileInputStream stream = new  FileInputStream(file);
				BufferedReader fichier = new BufferedReader(new InputStreamReader(stream,Charset.forName("ISO-8859-1")));
				NSMutableArray lignes = new NSMutableArray();
				try {
					boolean fini = false;
					while (!fini) {
						String texte = fichier.readLine();
						if (texte == null) {
							fini = true;
						} else {
							lignes.addObject(texte);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					fichier.close();
				}
				return lignes; 
			}  else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	// Méthodes privées
	private static void verifierPathEtCreer(String unPath) {
		// Vérifier récursivement que tous les directories existent
		File file = new File(unPath);
		// créer le directory si ils n'existe pas
		if (file.exists() == false) {
			file.mkdirs();
		}
	}
}

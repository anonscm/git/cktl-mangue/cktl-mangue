/** Gere la preparation des rubriques Cir et leur sauvegarde dans une table. Gere aussi la creation
 * et le remplissage du fichier destine au SPE.<BR/>
 * Le fichier DescriptionCir.XML est utilise pour g&eacute;n&eacute;rer les rubriques qui sont enregistrees dans une table.
 * Seules les rubriques des individus sont enregistrees dans la table CIR_DATA, les donnees pour fabriquer l'entete de fichier sont
 * enregistreees dans la table CIR_FICHIER_IMPORT.
 * La rubrique finale n'est genee qu'au moment ou le fichier est genere.
 * Pour ajouter une rubrique concernant un individu :
 * 	- Modifier le fichier DescriptionCir.XML et definir la rubrique en lui donnant un nom et une priorite
 * 	- Modifier AutomateCir pour ajouter une methode publique qui effectue le traitement : le nom de la methode est generer+nomRubrique (ex genererIdentiteAgent).
 * Elle doit attendre trois parametres : l'editing context, l'individu courant et la rubrique courante. La methode est
 * responsable des traitements i.e, elle a pour responsabilite de generer les records de type EOCirData et de les
 * inserer dans l'editingContext. L'appelant se charge d'enregistrer l'editing context. En cas d'erreur, elle doit lancer
 * une exception. Elle ne retourne pas de resultat.<BR>
 * Gestion des erreurs/warnings dans les controles des regles du Cir : une regle qui doit etre respectee,
 * sous contrainte de rejet par CARMEN du fichier CIR genere un message dans le diagnostic.<BR>
 * Les regles qui ne sont pas respectees mais pour lesquelles CARMEN essaiera de contourner la regle genere un message dans le diagnostic detaille. 
 * En cas de reussite, la declaration sera acceptee, en cas d'echec elle sera rejetee

 * @author christine
 *
 */
// 18/11/2010 - ajout de la création des congés pour les CLD, CLM et CFA
// 23/11/2010 - Modification du calcul de la quotité pour les positions non en activité
// 23/11/2010 - Modification de l'évaluation de la date courante
// 24/11/2010 - Changement de clé du paramètre pour l'UG
// 24/11/2010 - Génération du record de type P1
// 24/11/2010 - Génération des congés de formation professionnelle
// 01/12/2010 - Ajout d'un commentaire sur l'évaluation de la date de fin de gestion
// 01/12/2010 - Modification de la génération du fichier Electra pour ajouter la génération d'un record 03 par année
// 02/12/2010 - Les périodes de campagne ne sont utilisées que pour les dates début et fin
// 02/12/2010 - Les rapports peuvent être détaillés ou non (génération de messages d'erreur ou de warnings)
// 03/12/2010 - Correction d'un bug lorsque les affectations se chevauchent
// 05/12/2010 - les indices doivent être déclarés dans la période de gestion + ajout d'un warning si dates de changement de position et dates de début de carrière ne coïncident pas
// 05/12/2010 - un ensemble d'articles de position statutaire (03PS) ne peut commencer ou finir par une position de service militaire,
// celui-ci doit alors être déclaré dans les articles O4SN
// 07/12/2010 - ajout de warnings quand carrière et changement de position ne coïncident pas
// 07/12/2010 - pas de chevauchement des congés et correction pour traiter tous les congés légaux qui relèvent du Cir
// 08/12/2010 - changement du paramètre pour le département Cir (UNITE_GESTION à la place de CODE_SPE_CIR) suite à la validation Carmen
// 08/12/2010 - généralisation de la règle un ensemble d'articles de position statutaire (03PS) ne peut commencer ou finir par une position de service militaire,
// 08/12/2010 - on recherche les départs commençant à partir du jour de la fin de déclaration
// 08/12/2010 - génération des données dans le fichier Cir par ordre anté-chronologique
// 08/12/2010 - application de la règle RG3704 :Si la date de cessation de service est incluse dans la période de déclaration, 
// elle doit clore une période avec un taux d’activité non nul (article 03-MT)
// 09/12/2010 - modification de la gestion des détachements internes : 2 déclarations doivent être faites

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.cir.EOCirIdentite;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.server.AutomateAvecThread;
import org.cocktail.mangue.server.ServerThreadManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class AutomateCirCertification extends AutomateAvecThread {

	private static AutomateCirCertification sharedInstance;

	public AutomateCirCertification(EOEditingContext edc) {
		super(edc);
		// TODO Auto-generated constructor stub
	}

	public static AutomateCirCertification sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null)
			sharedInstance = new AutomateCirCertification(edc);
		return sharedInstance;
	}

	/** Prepare les comptes individuels retraite a pour l'annee de reference : ne recree pas ceux 
	 * deja existants mais les modifient si ils ne sont pas certifies et les enregistrent dans la base.
	 * On recherche les fonctionnaires en se basant sur les changements de position  valide sur l'annee
	 * 
	 * @return nombre comptes crees
	 */
	public Boolean preparerCertifications(Integer anneeReference, 
			ServerThreadManager thread) throws Exception {
		
		setThreadCourant(thread);
		
		informerThread(">> Préparation des données de CERTIFICATION ...");

		int nbComptesCrees = 0;
		int nbComptesModifies = 0;
		int annee = anneeReference.intValue();

		NSTimestamp debutAnnee = DateCtrl.debutAnnee(annee);
		NSTimestamp finAnnee = DateCtrl.finAnnee(annee);

		informerThread(">> Chargement des agents ...");
		
		NSMutableArray<EOIndividu> individus = new NSMutableArray<EOIndividu>();
		
		NSArray<EOChangementPosition> changementsPosition = EOChangementPosition.findForPeriode(getEdc(), debutAnnee, finAnnee);
		informerThread(changementsPosition.size() + " >> Changements de position trouvés ! ");
		individus.addObjectsFromArray((NSArray<EOIndividu>)changementsPosition.valueForKey(EOChangementPosition.TO_INDIVIDU_KEY));

		NSArray<EOContratAvenant> contrats= EOContratAvenant.findForCir(getEdc(), null, debutAnnee, finAnnee);
		informerThread(contrats.size() + " >> Contrats d'ATER trouvés ! ");

		individus.addObjectsFromArray((NSArray<EOIndividu>)contrats.valueForKey(EOContratAvenant.CONTRAT_KEY+"." + EOContrat.TO_INDIVIDU_KEY));

		try {
			getEdc().lock();
			NSMutableArray<EOIndividu> individusCrees = new NSMutableArray<EOIndividu>();
			// On recherche si l'individu a un compte. Si c'est le cas, si
			// le compte est certifié, on ne le modifie pas. Si il n'est pas certifie, 
			// on le modifie avec les caracteristiques de l'individu. Si il n'existe pas, on le cree.

			int index = 1;
			for (EOIndividu individu : individus) {

					if ( ! individusCrees.containsObject(individu)) {
						
						informerThread("\t" + index + "/"+individus.size()+" > " + individu.identitePrenomFirst());
						
						EOCirIdentite compte = EOCirIdentite.rechercherComptePourIndividu(getEdc(), anneeReference, individu);
						if (compte == null) {
							nbComptesCrees++;
							compte = new EOCirIdentite();
							compte.initAvecIndividu(individu, anneeReference);
							informerThread(">> Ajout d'une demande de certification");
							getEdc().insertObject(compte);
							individusCrees.addObject(individu);
						} else 
							// On ne modifie que les comptes non certifies
							if (!compte.estCertifie()) {
								nbComptesModifies++;
								informerThread(">> Modification des données");
								compte.reinitAvecIndividu(individu, anneeReference);
								individusCrees.addObject(individu);
							}
				}
				index++;
			}
			getEdc().saveChanges();

			informerThread(nbComptesCrees + " Compte créés !");
			informerThread(nbComptesModifies + " Compte modifiés !");
			return new Boolean (true);
		} catch (Exception exc) {
			exc.printStackTrace();
			return new Boolean (false);
		} finally {
			getEdc().unlock();
		}
	}

}

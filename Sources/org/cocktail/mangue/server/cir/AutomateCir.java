/** Gere la preparation des rubriques Cir et leur sauvegarde dans une table. Gere aussi la creation
 * et le remplissage du fichier destine au SPE.<BR/>
 * Le fichier DescriptionCir.XML est utilise pour g&eacute;n&eacute;rer les rubriques qui sont enregistrees dans une table.
 * Seules les rubriques des individus sont enregistrees dans la table CIR_DATA, les donnees pour fabriquer l'entete de fichier sont
 * enregistreees dans la table CIR_FICHIER_IMPORT.
 * La rubrique finale n'est genee qu'au moment ou le fichier est genere.
 * Pour ajouter une rubrique concernant un individu :
 * 	- Modifier le fichier DescriptionCir.XML et definir la rubrique en lui donnant un nom et une priorite
 * 	- Modifier AutomateCir pour ajouter une methode publique qui effectue le traitement : le nom de la methode est generer+nomRubrique (ex genererIdentiteAgent).
 * Elle doit attendre trois parametres : l'editing context, l'individu courant et la rubrique courante. La methode est
 * responsable des traitements i.e, elle a pour responsabilite de generer les records de type EOCirData et de les
 * inserer dans l'editingContext. L'appelant se charge d'enregistrer l'editing context. En cas d'erreur, elle doit lancer
 * une exception. Elle ne retourne pas de resultat.<BR>
 * Gestion des erreurs/warnings dans les controles des regles du Cir : une regle qui doit etre respectee,
 * sous contrainte de rejet par CARMEN du fichier CIR genere un message dans le diagnostic.<BR>
 * Les regles qui ne sont pas respectees mais pour lesquelles CARMEN essaiera de contourner la regle genere un message dans le diagnostic detaille. 
 * En cas de reussite, la declaration sera acceptee, en cas d'echec elle sera rejetee

 * @author christine
 *
 */
// 18/11/2010 - ajout de la création des congés pour les CLD, CLM et CFA
// 23/11/2010 - Modification du calcul de la quotité pour les positions non en activité
// 23/11/2010 - Modification de l'évaluation de la date courante
// 24/11/2010 - Changement de clé du paramètre pour l'UG
// 24/11/2010 - Génération du record de type P1
// 24/11/2010 - Génération des congés de formation professionnelle
// 01/12/2010 - Ajout d'un commentaire sur l'évaluation de la date de fin de gestion
// 01/12/2010 - Modification de la génération du fichier Electra pour ajouter la génération d'un record 03 par année
// 02/12/2010 - Les périodes de campagne ne sont utilisées que pour les dates début et fin
// 02/12/2010 - Les rapports peuvent être détaillés ou non (génération de messages d'erreur ou de warnings)
// 03/12/2010 - Correction d'un bug lorsque les affectations se chevauchent
// 05/12/2010 - les indices doivent être déclarés dans la période de gestion + ajout d'un warning si dates de changement de position et dates de début de carrière ne coïncident pas
// 05/12/2010 - un ensemble d'articles de position statutaire (03PS) ne peut commencer ou finir par une position de service militaire,
// celui-ci doit alors être déclaré dans les articles O4SN
// 07/12/2010 - ajout de warnings quand carrière et changement de position ne coïncident pas
// 07/12/2010 - pas de chevauchement des congés et correction pour traiter tous les congés légaux qui relèvent du Cir
// 08/12/2010 - changement du paramètre pour le département Cir (UNITE_GESTION à la place de CODE_SPE_CIR) suite à la validation Carmen
// 08/12/2010 - généralisation de la règle un ensemble d'articles de position statutaire (03PS) ne peut commencer ou finir par une position de service militaire,
// 08/12/2010 - on recherche les départs commençant à partir du jour de la fin de déclaration
// 08/12/2010 - génération des données dans le fichier Cir par ordre anté-chronologique
// 08/12/2010 - application de la règle RG3704 :Si la date de cessation de service est incluse dans la période de déclaration, 
// elle doit clore une période avec un taux d’activité non nul (article 03-MT)
// 09/12/2010 - modification de la gestion des détachements internes : 2 déclarations doivent être faites

package org.cocktail.mangue.server.cir;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.EOMotifDepart;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DureeCroissanteComparator;
import org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.IDuree;
import org.cocktail.mangue.modele.IDureePourIndividu;
import org.cocktail.mangue.modele.IEvenementAvecEnfant;
import org.cocktail.mangue.modele.PeriodeAvecQuotite;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOLienFiliationEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.cir.EOBeneficeEtudes;
import org.cocktail.mangue.modele.mangue.cir.EOBonifications;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriere;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriereData;
import org.cocktail.mangue.modele.mangue.cir.EOCirFichierImport;
import org.cocktail.mangue.modele.mangue.cir.EOEtudesRachetees;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAdoption;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOCgFinActivite;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodeHandicap;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodesMilitaires;
import org.cocktail.mangue.modele.mangue.individu.EOStage;
import org.cocktail.mangue.modele.mangue.individu.EOValidationServices;
import org.cocktail.mangue.modele.mangue.modalites.EOCessProgActivite;
import org.cocktail.mangue.modele.mangue.modalites.EOCongeSsTraitement;
import org.cocktail.mangue.modele.mangue.modalites.EOMiTpsTherap;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationActivite;
import org.cocktail.mangue.modele.mangue.prolongations.EOReculAge;
import org.cocktail.mangue.modele.mangue.prolongations.ProlongationActivite;
import org.cocktail.mangue.server.Application;
import org.cocktail.mangue.server.ServerThreadManager;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.foundation.ERXProperties;

public class AutomateCir {

	private static final String CODE_SERVICE_NATIONAL_EXEMPTE = "SN000";

	public static String DEFAULT_CS_TYPE = "S";

	private static AutomateCir sharedInstance;
	protected static String nomFichierPourEnvoi;

	private int anneeCir;
	private ServerThreadManager threadCourant;
	private ListeRubriquesCir listeRubriques;
	private EOCirFichierImport fichierCourant;
	private EOCirCarriere currentCirCarriere;
	private NSMutableArray<PeriodeAvecQuotite> periodesAvecQuotite;

	private RecordSpen 			recordSpenPourIndividu;
	private static final String NOM_RUBRIQUE_ENTETE = "Entete";
	private static final String NOM_SOUS_RUBRIQUE_NORME = "VERSION_FIP";
	private int 				numeroClassement;
	private boolean 			estRapportDetaille;

	private boolean rubriqueServiceNationalCalculee, rubriqueEnfantCalculee, rubriqueServiceAuxiliaireCalculee, rubriqueBonificationsCalculee, rubriqueEtudesCalculee;

	private 		EORne 				rneEtablissement;
	protected 		AgentCIR agentCIR;
	protected 		NSMutableArray<AgentCIR> mesAgents = new NSMutableArray<AgentCIR>();

	public AutomateCir() {
	}

	public static AutomateCir sharedInstance() {
		if (sharedInstance == null)
			sharedInstance = new AutomateCir();
		return sharedInstance;
	}

	public AgentCIR getAgentCIR() {
		return agentCIR;
	}
	public EOIndividu getCurrentIndividu() {
		return getAgentCIR().getIndividu();
	}

	public int getAnneeCir() {
		return anneeCir;
	}

	public void setAnneeCir(int anneeCir) {
		this.anneeCir = anneeCir;
	}

	public void setAgentCIR(AgentCIR agentCIR) {
		this.agentCIR = agentCIR;
	}

	public static String getNomFichierPourEnvoi() {
		return nomFichierPourEnvoi;
	}

	public NSMutableArray<AgentCIR> getMesAgents() {
		return mesAgents;
	}

	public EOCirCarriere getCurrentCirCarriere() {
		return currentCirCarriere;
	}

	public void setCurrentCirCarriere(EOCirCarriere currentCirCarriere) {
		this.currentCirCarriere = currentCirCarriere;
	}

	public void setMesAgents(NSMutableArray<AgentCIR> mesAgents) {
		this.mesAgents = mesAgents;
	}

	public void setNomFichierPourEnvoi(String nomFichierPourEnvoi) {
		this.nomFichierPourEnvoi = nomFichierPourEnvoi;
	}

	public NSTimestamp getDebutPeriodeDeclaration() {
		return getAgentCIR().getDebutPeriode();
	}

	public NSTimestamp getDebutPeriodeCampagne() {
		return DateCtrl.debutAnnee(getAnneeCir());
	}

	public NSTimestamp getFinPeriodeDeclaration() {
		return getAgentCIR().getFinPeriode();
	}

	public NSTimestamp getFinPeriodeCampagne() {
		return DateCtrl.finAnnee(getAnneeCir());
	}
	public boolean isRubriqueServiceNationalCalculee() {
		return rubriqueServiceNationalCalculee;
	}

	public void setRubriqueServiceNationalCalculee(
			boolean rubriqueServiceNationalCalculee) {
		this.rubriqueServiceNationalCalculee = rubriqueServiceNationalCalculee;
	}

	public boolean isRubriqueEnfantCalculee() {
		return rubriqueEnfantCalculee;
	}

	public void setRubriqueEnfantCalculee(boolean rubriqueEnfantCalculee) {
		this.rubriqueEnfantCalculee = rubriqueEnfantCalculee;
	}

	public boolean isRubriqueServiceAuxiliaireCalculee() {
		return rubriqueServiceAuxiliaireCalculee;
	}

	public void setRubriqueServiceAuxiliaireCalculee(
			boolean rubriqueServiceAuxiliaireCalculee) {
		this.rubriqueServiceAuxiliaireCalculee = rubriqueServiceAuxiliaireCalculee;
	}

	public boolean isRubriqueBonificationsCalculee() {
		return rubriqueBonificationsCalculee;
	}

	public void setRubriqueBonificationsCalculee(
			boolean rubriqueBonificationsCalculee) {
		this.rubriqueBonificationsCalculee = rubriqueBonificationsCalculee;
	}

	public boolean isRubriqueEtudesCalculee() {
		return rubriqueEtudesCalculee;
	}

	public void setRubriqueEtudesCalculee(boolean rubriqueEtudesCalculee) {
		this.rubriqueEtudesCalculee = rubriqueEtudesCalculee;
	}

	public EORne getRneEtablissement() {
		return rneEtablissement;
	}

	public void setRneEtablissement(EORne rneEtablissement) {
		this.rneEtablissement = rneEtablissement;
	}


	public RecordSpen getRecordSpenPourIndividu() {
		return recordSpenPourIndividu;
	}

	public void setRecordSpenPourIndividu(RecordSpen recordSpenPourIndividu) {
		this.recordSpenPourIndividu = recordSpenPourIndividu;
	}

	public EOCirFichierImport getFichierCourant() {
		return fichierCourant;
	}

	public void setFichierCourant(EOCirFichierImport fichierCourant) {
		this.fichierCourant = fichierCourant;
	}

	/**
	 * 
	 * @param pathFichier
	 * @param fichierCir
	 * @param individus
	 * @param pathXMLPourCir
	 * @param estRapportDetaille
	 * @param threadCourant
	 */
	public void preparerRecords(EOCirFichierImport fichierCir, NSArray<EOIndividu> individus, 
			String pathXMLPourCir, Boolean estRapportDetaille, ServerThreadManager threadCourant) {

		setFichierCourant(fichierCir);

		EOEditingContext editingContext = getFichierCourant().editingContext();

		setRneEtablissement(EOStructure.rechercherEtablissement(editingContext).rne());
		setAnneeCir(getFichierCourant().cfimAnnee().intValue() - 1);

		LogManager.logDetail("CIR - Préparer Records");

		EnvironnementCir.sharedInstance().init();

		this.threadCourant = threadCourant;
		this.estRapportDetaille = estRapportDetaille.booleanValue();

		try {

			// Suppression des anciennes donnees
			String message = supprimerDonneesCir(individus);

			if (message != null) {
				informerThread(message);
				return;
			}
			editingContext.lock();

			listeRubriques = ParserCir.parseXML(pathXMLPourCir);

			message = modifierInformationsFichier(editingContext);
			if (message != null) {
				informerThread(" >> Generation Entete Fichier " + message);
				return;
			}

			// Generation de toutes les donnees CIR
			genererInformationsAgents(editingContext, individus);

			informerThread(" >> TRAITEMENT TERMINE");

		} catch (Exception e1) {
			e1.printStackTrace();
			signalerExceptionThread(e1);
		} finally {
			editingContext.unlock();
		}

	}

	/**
	 * 
	 * @param editingContext
	 * @param individus
	 */
	private void genererInformationsAgents(EOEditingContext edc, NSArray<EOIndividu> individus) {

		try {

			informerThread(" >> Generation données agents ");

			if (EnvironnementCir.sharedInstance().estInitialise()) {

				NSArray rubriquesPourIndividu = listeRubriques.rubriquesIndividuTriees();

				for (EOIndividu myIndividu : individus) {

					informerThread("\t> " + myIndividu.identite());

					setCurrentCirCarriere(EOCirCarriere.creer(myIndividu.editingContext(), fichierCourant, myIndividu));

					numeroClassement = 0;

					setRubriqueEnfantCalculee(false);
					setRubriqueServiceNationalCalculee(false);
					setRubriqueBonificationsCalculee(false);
					setRubriqueEtudesCalculee(false);

					genererInformationsIndividu(edc, myIndividu, rubriquesPourIndividu);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Generation des sequences CIR pour un individu
	 * @param editingContext
	 * @param individu
	 * @param rubriquesPourIndividu
	 */
	private void genererInformationsIndividu(EOEditingContext edc, EOIndividu individu, NSArray<RubriqueCir> rubriquesPourIndividu) {

		// RAZ du tableau des periodes
		setMesAgents(new NSMutableArray<AgentCIR>());

		// Verification des positions
		NSArray<EOCarriere> carrieresAgent = EOCarriere.findForIndividu(edc, individu);
		for (EOCarriere carriere : carrieresAgent) {
			validerChangementsPosition(edc, carriere);
		}

		NSArray<EOContratAvenant> contrats = EOContratAvenant.findForCir(edc, individu, getDebutPeriodeCampagne(), getFinPeriodeCampagne());
		for (EOContratAvenant avenant : contrats) {
			validerAvenant(edc, avenant);			
		}

		// Preparation de la ou des perioeds de declaration
		positionnerPeriodesAgent(edc, individu);

		// Controle de la validite des changements de position pour une periode
		int index = 1;
		for (AgentCIR agent : getMesAgents()) {

			agent.setLastPeriode(false);
			if (index == getMesAgents().size())
				agent.setLastPeriode(true);
			setAgentCIR(agent);

			genererIndividu(edc, rubriquesPourIndividu);
			index ++ ;
		}
	}

	/**
	 * 
	 * @param edc
	 * @param avenant
	 */
	private void validerAvenant(EOEditingContext edc, EOContratAvenant avenant) {

	}


	/**
	 * Controle de la validite des changements de position pour la periode de declaration
	 * Les changements de position doivent couvrir TOUTE la periode et etre CONTIGUS
	 * @param edc
	 * @param individu
	 */
	private void validerChangementsPosition(EOEditingContext edc, EOCarriere carriere) {

		String message = null;

		NSTimestamp debutPeriode = carriere.dateDebut();
		NSTimestamp finPeriode = carriere.dateFin();
		if (finPeriode == null)
			finPeriode = getFinPeriodeCampagne();

		NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsPourCarriere(edc, carriere);
		changements = EOSortOrdering.sortedArrayUsingKeyOrderArray(changements, EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);

		if (changements.count() == 0) {
			message = "Pas de changements de position pour la période du " + DateCtrl.dateToString(debutPeriode) + " au " + DateCtrl.dateToString(finPeriode);
		} else {

			EOChangementPosition premierChangement = changements.get(0);
			EOChangementPosition dernierChangement = (EOChangementPosition) changements.lastObject();

			if (DateCtrl.isAfter(premierChangement.dateDebut(), debutPeriode))
				message = "Les changements de position commencent après la carrière du " + DateCtrl.dateToString(debutPeriode) + " au " + DateCtrl.dateToString(finPeriode);

			if ( dernierChangement.dateFin() != null && DateCtrl.isBefore(dernierChangement.dateFin(), finPeriode))
				message = "Les changements de position ne couvrent pas toute la carrière du " + DateCtrl.dateToString(debutPeriode) + " au " + DateCtrl.dateToString(finPeriode);

			// Tester s'il n y a pas de trous dans les changements de position
			EOChangementPosition positionPrecedente = null;
			for (EOChangementPosition myPosition : changements) {

				if (positionPrecedente != null
						&& positionPrecedente.dateFin() != null
						&& !DateCtrl.isSameDay(myPosition.dateDebut(),
								positionPrecedente.dateDebut())
								&& !DateCtrl.isSameDay(myPosition.dateDebut(),
										DateCtrl.jourSuivant(positionPrecedente
												.dateFin()))) {

					if (message == null)
						message = "Les changements de position ne sont pas contigus ! ( " + positionPrecedente.dateFinFormatee() + " / " + myPosition.dateDebutFormatee() + " )";
					else
						message += "\nLes changements de position ne sont pas contigus ! ( " + positionPrecedente.dateFinFormatee() + " / " + myPosition.dateDebutFormatee() + " )";

				}
				positionPrecedente = myPosition;

			}

		}

		if (message != null) {

			getCurrentCirCarriere().setTemValide(CocktailConstantes.FAUX);
			if (getCurrentCirCarriere().circCommentaires() == null)
				getCurrentCirCarriere().setCircCommentaires(message);
			else
				getCurrentCirCarriere().setCircCommentaires(getCurrentCirCarriere().circCommentaires() + "\n" + message);

			informerThread(message);
			EnvironnementCir.sharedInstance().ajouterErreur("**************************\n" + carriere.toIndividu().identite() + " : " + message);
		}

	}


	/**
	 * Preparation des periodes de declaration d'un agent en fonction de ses segments de carriere
		Gestion des carrieres "a trous", entre 2 segments de carriere ou entre le PASSE et un segment de carriere.
	 * 
	 * @param edc
	 * @param individu
	 */
	private void positionnerPeriodesAgent(EOEditingContext edc, EOIndividu individu) {

		NSArray<EOCarriere> carrieres = EOCarriere.findCarrieresCIRPourIndividu(edc, individu);

		if (carrieres.size() == 0) {
			NSArray<EOContratAvenant> avenants = EOContratAvenant.findForCir(edc, individu, getFinPeriodeCampagne(), getFinPeriodeCampagne());
			if (avenants.size() >  0) {
				AgentCIR myAgent = new AgentCIR(individu, avenants.get(0).dateDebut() , getFinPeriodeCampagne(), true);
				myAgent.setAvenants(avenants);
				myAgent.setChangementsPositions(new NSArray<EOChangementPosition>());
				myAgent.setContractuel(true);
				getMesAgents().addObject(myAgent);
			}
		}
		else {
			try {
				EOChangementPosition positionEnCours = EOChangementPosition.findChangementPourDate(edc, individu, getFinPeriodeCampagne());
				NSMutableArray<EOChangementPosition> mesPositions = new NSMutableArray<EOChangementPosition>();

				// L'agent est il en position de detachement entrant
				// Si OUI, sa periode de declaration correspondra a (ou les) periodes de detachement entrant
				if (positionEnCours != null && positionEnCours.estDetachementEntrant()) {

					NSArray<EOChangementPosition> changementsDetachement = EOChangementPosition.findDetachementsForPeriode(edc, individu, getFinPeriodeCampagne(), getFinPeriodeCampagne());

					NSTimestamp debutPeriode = changementsDetachement.get(0).dateDebut();

					EOChangementPosition positionPrecedente = null;
					for (EOChangementPosition myChangement : changementsDetachement) {

						if (myChangement.estDetachementEntrant()) {

							mesPositions.addObject(myChangement);

							if (positionPrecedente != null && !positionPrecedente.estDetachementEntrant()) {
								debutPeriode=  myChangement.dateDebut();
							}
						}

						positionPrecedente = myChangement;
					}			

					AgentCIR myAgent = new AgentCIR(individu, debutPeriode, getFinPeriodeCampagne(), true);
					myAgent.setChangementsPositions(mesPositions);
					getMesAgents().addObject(myAgent);

				}
				else {

					NSMutableArray<PeriodeDeclaration> periodesDeclaration = new NSMutableArray<PeriodeDeclaration>();
					NSTimestamp debutDeclaration = carrieres.get(0).dateDebut();

					// On verifie s'il y a des periodes de PASSE type EAS, ENGAGE ou ELEVE, auquel cas la
					// date de debut de declaration est modifiee
					NSArray<EOPasse> passes = EOPasse.rechercherPassesPourRemonteeCIR(edc, individu, debutDeclaration);
					if (passes.size() > 0) {

						passes = EOSortOrdering.sortedArrayUsingKeyOrderArray(passes, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
						debutDeclaration = passes.get(0).dateDebut();

						for (EOPasse passe : passes) {
							periodesDeclaration.addObject(new PeriodeDeclaration(passe.individu(), passe.dateDebut(), passe.dateFin()));
						}
					}

					for (EOCarriere carriere : carrieres) {
						periodesDeclaration.addObject(new PeriodeDeclaration(carriere.toIndividu(), carriere.dateDebut(), carriere.dateFin()));
					}

					PeriodeDeclaration dernierePeriode = periodesDeclaration.lastObject();

					PeriodeDeclaration periodePrecedente = null;
					for (PeriodeDeclaration periodeDeclaration : periodesDeclaration) {

						if (periodePrecedente != null && periodePrecedente.getDateFin() != null) {

							// Gerer les interruptions de carriere (==> Carrieres " a trous")
							if (!DateCtrl.isSameDay(periodeDeclaration.getDateDebut(),
									DateCtrl.dateAvecAjoutJours(periodePrecedente.getDateFin(), 1))) {

								AgentCIR myAgent = new AgentCIR(individu, debutDeclaration, periodePrecedente.getDateFin(), false);
								NSMutableArray<EOChangementPosition> changements = new NSMutableArray<EOChangementPosition>();
								changements.addObjectsFromArray(EOChangementPosition.rechercherChangementsCirPourIndividuEtPeriode(edc, individu, debutDeclaration, periodePrecedente.getDateFin()));
								changements.addObjectsFromArray(EOChangementPosition.rechercherChangementsCirPassePourIndividuEtPeriode(edc, individu, debutDeclaration, periodePrecedente.getDateFin()));							
								NSArray<EOChangementPosition> positions = EOSortOrdering.sortedArrayUsingKeyOrderArray( changements, EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);
								myAgent.setChangementsPositions(positions);
								getMesAgents().addObject(myAgent);

								debutDeclaration = periodeDeclaration.getDateDebut();

							}
						}

						periodePrecedente = periodeDeclaration;

					}

					// Ajout de la derniere periode de declaration
					if (periodePrecedente.getDateFin() == null || DateCtrl.isAfter(dernierePeriode.getDateFin(), getFinPeriodeCampagne())) {
						AgentCIR myAgent = new AgentCIR(individu, debutDeclaration, getFinPeriodeCampagne(), false);

						NSMutableArray<EOChangementPosition> changements = new NSMutableArray<EOChangementPosition>();
						changements.addObjectsFromArray(EOChangementPosition.rechercherChangementsCirPourIndividuEtPeriode(edc, individu, debutDeclaration, getFinPeriodeCampagne()));
						changements.addObjectsFromArray(EOChangementPosition.rechercherChangementsCirPassePourIndividuEtPeriode(edc, individu, debutDeclaration, getFinPeriodeCampagne()));
						NSArray<EOChangementPosition> positions = EOSortOrdering.sortedArrayUsingKeyOrderArray( changements, EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);
						myAgent.setChangementsPositions(positions);
						getMesAgents().addObject(myAgent);
					}
					else	{
						AgentCIR myAgent = new AgentCIR(individu, debutDeclaration, dernierePeriode.getDateFin(), false);
						NSMutableArray<EOChangementPosition> changements = new NSMutableArray<EOChangementPosition>();
						changements.addObjectsFromArray(EOChangementPosition.rechercherChangementsCirPourIndividuEtPeriode(edc, individu, debutDeclaration, dernierePeriode.getDateFin()));
						changements.addObjectsFromArray(EOChangementPosition.rechercherChangementsCirPassePourIndividuEtPeriode(edc, individu, debutDeclaration, dernierePeriode.getDateFin()));
						NSArray<EOChangementPosition> positions = EOSortOrdering.sortedArrayUsingKeyOrderArray( changements, EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);
						myAgent.setChangementsPositions(positions);
						getMesAgents().addObject(myAgent);
					}
				}

				// Periodes de declaration : 

				int index = 1;

				for (AgentCIR agent : getMesAgents()) {
					index ++;
				}

			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * 
	 * @param fichierCir
	 * @param individus
	 * @param threadCourant
	 */
	public void supprimerRecords(EOCirFichierImport fichierCir,
			NSArray<EOIndividu> individus, ServerThreadManager threadCourant) {
		EnvironnementCir.sharedInstance().init();
		fichierCourant = fichierCir;
		setAnneeCir(fichierCir.cfimAnnee().intValue());
		this.threadCourant = threadCourant;

		String message = supprimerDonneesCir(individus);
		if (message != null) {
			informerThread(message);
		}
		informerThread("Termine");

	}

	/**
	 * Retourne l'ensemble des fichiers generes pour la DADS sous la forme d'un
	 * dictionnaire.<BR>
	 * La cle est le nom du fichier et la valeur est un tableau contenant une
	 * entrée par ligne de texte
	 * 
	 * @param path
	 *            chemin d'acces des fichiers
	 * @return null si erreur
	 */
	public static NSDictionary recupererFichierCIR(String path) {
		NSMutableDictionary dict = new NSMutableDictionary();
		FichierCir.ajouterFichierAuDictionnaire(path, getNomFichierPourEnvoi(),
				dict);

		return dict;
	}

	/**
	 * genere le fichier Cir pour la liste des individus passee en parametre. On
	 * ecrit dans le fichier pour chaque individu. Cette mzthode ne genere que
	 * les fichiers de type Campagne
	 * 
	 * @param fichierCir
	 *            record contenant les donnees pour generer le fichier Cir
	 * @param individus
	 *            tableau des individus (EOIndividu) pour lesquels generer le
	 *            fichier Cir
	 * @param pathDirectory
	 *            chemin d'acces du directory dans lequel generer le fichier
	 */
	public void preparerFichier(EOCirFichierImport fichierCir, String pathFichier, NSArray individus,
			ServerThreadManager threadCourant) {

		pathFichier = System.getProperty("java.io.tmpdir");

		EOEditingContext editingContext = fichierCir.editingContext();
		this.fichierCourant = fichierCir;
		this.threadCourant = threadCourant;
		boolean fichierOuvert = false;
		int nbDeclarations = 0;

		try {

			editingContext.lock();

			// Modifier la date de génération du fichier
			NSTimestamp today = new NSTimestamp();
			fichierCir.setCfimDate(today);
			setNomFichierPourEnvoi(EOCirFichierImport.nomFichierEnvoi(editingContext, fichierCir));
			FichierCir.creerFichier(pathFichier, getNomFichierPourEnvoi());
			fichierOuvert = true;

			// Générer la rubrique d'entête sous la forme d'une string
			// CodeArticle$UniteGestion$Date$VersionFip
			String uniteGestion = EOGrhumParametres.getUniteGestionCir();

			String texte = ConstantesCir.CODE_ARTICLE_ENTETE
					+ ConstantesCir.DELIMITEUR_CIR
					+ uniteGestion
					+ ConstantesCir.DELIMITEUR_CIR
					+ DateCtrl.dateToString(fichierCir.cfimDate(),
							ConstantesCir.dateFormat)
							+ ConstantesCir.DELIMITEUR_CIR + fichierCir.cfimNorme()
							+ ConstantesCir.DELIMITEUR_LIGNE;

			FichierCir.ecrire(texte);

			informerThread(">> Préparation du fichier CIR ...");

			// Pour chaque agent, rechercher les records du Cir et créer les
			// données dans le fichier. Les records sont triés
			// par ordre de création croissant. On écrit dans le fichier d'un
			// coup toutes les données d'un individu
			java.util.Enumeration<EOIndividu> e = individus.objectEnumerator();
			while (e.hasMoreElements()) {

				EOIndividu individu = e.nextElement();
				informerThread("\t>"+individu.identitePrenomFirst());
				NSArray<EOCirCarriereData> cirsData = EOCirCarriereData.recordsPourFichierEtIndividu(editingContext, fichierCir, individu);

				if (cirsData.count() > 0) {
					texte = "";
					// 08/12/2010 - Générer les données par ordre
					// anté-chronologique : pour cela chaque fois, on parcourt
					// la liste des records
					// par ordre de classement décroissant et quand on identifie
					// un record 01, on génère les données avec tous les records
					// qu'on a lus auparavant
					NSMutableArray<EOCirCarriereData> recordsAGenerer = new NSMutableArray<EOCirCarriereData>();
					for (int i = cirsData.count() - 1; i >= 0; i--) {
						EOCirCarriereData cirData = (EOCirCarriereData) cirsData
								.objectAtIndex(i);
						recordsAGenerer.addObject(cirData);
						if (cirData.circdRubrique().equals(
								EOCirCarriereData.RUBRIQUE_IDENTITE)) {
							nbDeclarations++;
							// les données sont rangées à l'envers
							for (Enumeration<EOCirCarriereData> e1 = recordsAGenerer.reverseObjectEnumerator();e1.hasMoreElements();) {
								EOCirCarriereData data = e1.nextElement();
								texte += genererTexte(data.circdData());
							}
							// On recommence à zéro
							recordsAGenerer = new NSMutableArray();
						}
					}
					FichierCir.ecrire(texte);
				}
			}

			// Generer le record final
			texte = ConstantesCir.CODE_ARTICLE_FINAL
					+ ConstantesCir.DELIMITEUR_CIR + nbDeclarations
					+ ConstantesCir.DELIMITEUR_LIGNE;
			FichierCir.ecrire(texte);
			// Modifier le statut du fichier Cir et enregister le record
			fichierCir.setDModification(new NSTimestamp());
			fichierCir.setEstGenere(true);
			fichierCir.setCfimFichierEnvoi(nomFichierPourEnvoi);
			editingContext.saveChanges();

			informerThread("Termine");

		} catch (Exception exc) {
			exc.printStackTrace();
			editingContext.revert();
			// autres exceptions que celles attendues
			signalerExceptionThread(exc);
		} finally {
			editingContext.unlock();
			if (fichierOuvert) {
				FichierCir.fermerFichier();
			}
		}
	}

	/**
	 * 
	 * @param fichierCir
	 * @param pathFichier
	 * @param estFichierPrincipal
	 * @return
	 */
	public String contenuFichier(EOCirFichierImport fichierCir,
			String pathFichier, boolean estFichierPrincipal) {
		String nomFichier = fichierCir.cfimFichier();
		nomFichier = getNomFichierPourEnvoi();
		InputStream stream = null;

		String fileSeparator = System.getProperty("file.separator");
		if (!pathFichier.substring(pathFichier.length() - 1,
				pathFichier.length()).equals(fileSeparator))
			pathFichier = pathFichier.concat(fileSeparator);
		pathFichier = pathFichier.concat(nomFichier);
		try {
			File fichier = new File(pathFichier.toString());
			stream = new FileInputStream(fichier);
			int lg = stream.available();
			byte b[] = new byte[lg];
			stream.read(b);
			return new String(b, "ISO-8859-1");
		} catch (Exception e) {
			LogManager.logException(e);
			return e.getMessage();
		}
	}

	/**
	 * 
	 * @param nomRubrique
	 * @param pathXMLPourCir
	 * @return
	 */
	public NSArray sousRubriquesPourRubrique(String nomRubrique,
			String pathXMLPourCir) {
		if (listeRubriques == null)
			listeRubriques = ParserCir.parseXML(pathXMLPourCir);
		RubriqueCir rubrique = listeRubriques
				.rubriqueIndividuPourNom(nomRubrique);
		return rubrique.sousRubriques();
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 * @throws Exception
	 */
	public void genererIdentiteAgent(EOEditingContext edc, RubriqueCir rubrique) throws Exception {
		NSMutableArray parametres = new NSMutableArray(getCurrentIndividu());
		parametres.addObject(getCurrentIndividu().personnel());
		String message = getCurrentIndividu().validationsCir();
		if (message != null && message.length() > 0)
			ajouterErreurARubrique(rubrique, message, 0);
		calculerRubrique(rubrique, parametres);
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 * @throws Exception
	 */
	public void genererSpen(EOEditingContext edc, RubriqueCir rubrique) throws Exception {
		try {
			if (getRecordSpenPourIndividu() == null)
				setRecordSpenPourIndividu(new RecordSpen(
						EOStructure.rechercherEtablissement(edc), new NSTimestamp(), fichierCourant.cfimType().equals("C")));
			getRecordSpenPourIndividu().changerIndividu(getCurrentIndividu());
			NSMutableArray parametres = new NSMutableArray(getRecordSpenPourIndividu());
			calculerRubrique(rubrique, parametres);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * Plusieurs cas a analyser :
	 * 
	 * 1 - Carriere sans detachement - On renseigne tout , 1 seule remontee de
	 * carriere 2 - Detachement entrant en cours : On ne declare que cette
	 * période - (L'autre etablissement est en charge de tout déclarer). 3 -
	 * Detachement entrant PUIS intégration dans l'établissement : 2
	 * déclarations une pour le détachement entrant (IND_DETACHEMENT = 1) et une
	 * autre pour le détachement sortant (0) On declare donc dans ce cas
	 * l'ensemble de sa carriere 4 - Interruption de carriere 2 périodes de
	 * déclaration avec un motif de démission pour la première
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 * @throws Exception
	 */
	public void genererCarriere(EOEditingContext edc, RubriqueCir rubrique) throws Exception {

		getAgentCIR().setDateCessationService(null);
		getAgentCIR().setMotifCessationService(null);

		NSMutableArray<PeriodeCarriere> periodesCarriere = new NSMutableArray<PeriodeCarriere>(
				new PeriodeCarriere(getAgentCIR()));

		NSMutableArray parametres;
		for (PeriodeCarriere myPeriode : periodesCarriere) {

			parametres = new NSMutableArray(myPeriode);
			parametres.addObject(myPeriode.getIndividu());
			parametres.addObject(myPeriode.getIndividu().personnel());
			EOAdresse adresse = myPeriode
					.adresseCourante(getFinPeriodeDeclaration());

			if (adresse != null) {
				String message = adresse.validationsCir();
				if (message != null && message.length() > 0)
					ajouterErreurARubrique(rubrique, "Adresse personnelle\n" + message, 0);
				parametres.addObject(adresse);
			} else {
				ajouterErreurARubrique(rubrique, " - Aucune adresse PERSONNELLE n'est renseignée !", 0);
			}

			EOPeriodeHandicap handicap = myPeriode.periodeHandicapCourante();
			if (handicap != null)
				parametres.addObject(handicap);

			// Gestion des departs

			NSArray<EODepart> departs = EODepart.rechercherDepartsValidesEtDefinitifsPourIndividuEtPeriode(
					edc, myPeriode.getIndividu(), getAgentCIR().getDebutPeriode(), DateCtrl.dateAvecAjoutJours(getAgentCIR().getFinPeriode(),1));

			if (departs.size() > 0) {
				departs = EOSortOrdering.sortedArrayUsingKeyOrderArray(departs,	EODepart.SORT_DATE_DEBUT_DESC);
				EODepart depart = departs.get(0);
				String message = null;
				if (depart.individu().dDeces() != null) {
					if (!depart.motifDepart().estDeces())
						message = "L'individu est décédé, le motif de départ doit etre \"pour décès\"";
					else if (depart.dCessationService() == null)
						message = "La date de cessation de service doit etre fournie !";
				} else {
					message = depart.validationsCir();
				}
				if (message != null && message.length() > 0)
					ajouterErreurARubrique(rubrique, "Informations de départ\n" + message, 0);

				getAgentCIR().setMotifCessationService(depart.motifDepart().cMotifDepartOnp());
				getAgentCIR().setDateCessationService(depart.dCessationService());
				getAgentCIR().setDateRadiationCadre(depart.dateRadiationCir());

				parametres.addObject(depart);
			}
			else {
				if (!getAgentCIR().isLastPeriode()) {
					getAgentCIR().setMotifCessationService(EOMotifDepart.TYPE_MOTIF_DEMISSION);
					getAgentCIR().setDateCessationService(getAgentCIR().getFinPeriode());
					getAgentCIR().setDateRadiationCadre(DateCtrl.dateAvecAjoutJours(getAgentCIR().getFinPeriode(), 1));
				}
			}

			calculerRubrique(rubrique, parametres);

		}

	}

	/**
	 * 
	 * @param edc
	 * @param individu
	 * @param rubrique
	 */
	public void genererStatut(EOEditingContext edc, RubriqueCir rubrique) {

		try {
			NSArray<EOStage> stages = EOStage.findForIndividuEtPeriode(edc, 
					getCurrentIndividu(), getDebutPeriodeDeclaration(), getFinPeriodeDeclaration(),
					false);
			stages = EOSortOrdering.sortedArrayUsingKeyOrderArray(stages, EOStage.SORT_ARRAY_DATE_DEBUT_ASC);

			NSTimestamp dateDebut = getDebutPeriodeDeclaration();
			NSTimestamp dateFin = null;
			String statutIndividu = null;
			NSMutableArray<PeriodeStatut> periodes = new NSMutableArray<PeriodeStatut>();

			EOChangementPosition positionPrecedente = null;

			for (EOChangementPosition myPosition : getAgentCIR().getChangementsPositions()) {

				if (DateCtrl.isBefore(myPosition.dateDebut(),getDebutPeriodeDeclaration()))
					continue;

				if (positionPrecedente != null
						&& DateCtrl.isSameDay(myPosition.dateDebut(), positionPrecedente.dateDebut())
						&& DateCtrl.isSameDay(myPosition.dateFin(), positionPrecedente.dateFin())) {
					continue;
				}

				positionPrecedente = myPosition;

				boolean estFinPeriodeDeclaration = false;
				if (DateCtrl.isAfter(myPosition.dateDebut(), dateDebut)) {
					dateDebut = myPosition.dateDebut();
				}
				if (myPosition.toPosition().estServiceNational()) {
					statutIndividu = ConstantesCir.MILITAIRE;
				}
				else 
					if (myPosition.carriere() != null && myPosition.carriere().toTypePopulation().estNormalien()) {
						statutIndividu = ConstantesCir.STAGIAIRE;
					}
					else {
						statutIndividu = ConstantesCir.TITULAIRE;
					}

				if (myPosition.dateFin() != null && DateCtrl.isBefore(myPosition.dateFin(), getFinPeriodeDeclaration()))
					dateFin = myPosition.dateFin();
				else {
					dateFin = getFinPeriodeDeclaration();
					estFinPeriodeDeclaration = true;
				}

				String typeCategorieService = evaluerCategorieServicePourCarriere(myPosition.carriere(), dateFin, statutIndividu);

				for (EOStage myStage : stages) {

					if (myPosition.carriere() != null && myStage.carriere() != myPosition.carriere())
						break;
					if (org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps
							.intersectionPeriodes(dateDebut, dateFin,
									myStage.dateDebut(), myStage.dateFin()) == null)
						continue;
					if (DateCtrl.isAfter(myStage.dateDebut(), dateDebut)) {
						periodes.addObject(new PeriodeStatut(dateDebut, DateCtrl
								.jourPrecedent(myStage.dateDebut()),
								statutIndividu, typeCategorieService, true));
						dateDebut = myStage.dateDebut();
					}
					if (myStage.dateFin() == null
							|| DateCtrl.isAfterEq(myStage.dateFin(), dateFin)) {
						periodes.addObject(new PeriodeStatut(dateDebut, dateFin,
								ConstantesCir.STAGIAIRE, DEFAULT_CS_TYPE, true));
						dateDebut = DateCtrl.jourSuivant(dateFin);
						break;
					}
					periodes.addObject(new PeriodeStatut(dateDebut, myStage
							.dateFin(), ConstantesCir.STAGIAIRE, DEFAULT_CS_TYPE,
							true));
					dateDebut = DateCtrl.jourSuivant(myStage.dateFin());
				}

				if (DateCtrl.isBefore(getFinPeriodeDeclaration(), dateDebut)) {
					break;
				}

				if (DateCtrl.isBeforeEq(dateDebut, dateFin)) {
					periodes.addObject(new PeriodeStatut(dateDebut, dateFin,
							statutIndividu, typeCategorieService, true));

					if (estFinPeriodeDeclaration)
						break;
					dateDebut = DateCtrl.jourSuivant(dateFin);
					continue;
				}
				if (stages.count() != 0)
					continue;

				periodes.addObject(new PeriodeStatut(dateDebut, dateFin,
						statutIndividu, typeCategorieService, true));
				if (estFinPeriodeDeclaration)
					break;
				dateDebut = DateCtrl.jourSuivant(dateFin);
			}

			for (EOContratAvenant myAvenant : getAgentCIR().getAvenants()) {
				statutIndividu = ConstantesCir.TITULAIRE;
				periodes.addObject(new PeriodeStatut(myAvenant.dateDebut(), myAvenant.dateFin(),
						ConstantesCir.TITULAIRE, DEFAULT_CS_TYPE, true));
			}


			if (periodes.count() > 1) {
				periodes = PeriodeCirAvecDates.regrouperPeriodes(periodes);
				EOSortOrdering.sortArrayUsingKeyOrderArray(
						periodes,
						new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebutPourCir", EOSortOrdering.CompareDescending)));
			}

			for (PeriodeStatut periode : periodes) {
				calculerRubrique(rubrique, new NSArray<PeriodeStatut>(periode));
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 */
	public void genererPositionStatutaire(EOEditingContext edc, RubriqueCir rubrique) {

		periodesAvecQuotite = new NSMutableArray();

		NSMutableArray<PeriodePositionStatutaire> periodesStatutaires = preparerPeriodesStatutairesPourIndividuEtRubrique(edc, rubrique);
		if (periodesStatutaires.count() > 1) {
			periodesStatutaires = PeriodeCirAvecDates.regrouperPeriodes(periodesStatutaires);
			EOSortOrdering.sortArrayUsingKeyOrderArray(	periodesStatutaires, 
					new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebutPourCir", EOSortOrdering.CompareDescending)));
		}

		NSMutableArray parametres;
		for (PeriodePositionStatutaire periode : periodesStatutaires) {

			if (periode.changementPosition() != null) {
				String message = periode.changementPosition().validationsCir();
				if (message != null && message.length() > 0) {
					int niveauMessage = 0;
					if (message.indexOf("enfant") > 0)
						niveauMessage = 1;
					ajouterErreurARubrique(rubrique, message, niveauMessage);
				}
			}
			parametres = new NSMutableArray(periode);
			if (periode.changementPosition() != null
					&& periode.changementPosition().enfant() != null)
				parametres.addObject(periode.changementPosition().enfant());

			calculerRubrique(rubrique, parametres);

		}

	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 */
	public void genererConge(EOEditingContext editingContext, RubriqueCir rubrique) {

		NSArray absences = EOAbsences.findAbsencesCirPourIndividuEtDates(editingContext, getAgentCIR().getIndividu(), getDebutPeriodeDeclaration(),getFinPeriodeDeclaration());

		absences = EOSortOrdering.sortedArrayUsingKeyOrderArray(absences,
				PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);

		if (dureesSansChevauchement(absences, rubrique.nom())) {
			NSMutableArray<PeriodeConge> periodes = new NSMutableArray<PeriodeConge>();
			for (Enumeration<EOAbsences> e = absences.objectEnumerator(); e
					.hasMoreElements();) {
				EOAbsences absence = e.nextElement();

				String nomEntite = EOEntity.nameForExternalName(absence
						.toTypeAbsence().codeHarpege(), "_", true);
				EOGenericRecord conge = Conge.rechercherCongeAvecAbsence(
						editingContext, nomEntite, absence);
				if (conge == null) {
					ajouterErreurARubrique(
							rubrique,
							"Pas de congé  associé à l'absence du "
									+ DateCtrl.dateToString(absence.dateDebut()),
									0);
				} else if ((conge instanceof IEvenementAvecEnfant)
						&& ((IEvenementAvecEnfant) conge).enfant() == null)
					if (conge instanceof EOCongeAdoption) {
						String message = ((EOCongeAdoption) conge)
								.validationsCir();
						if (message != null && message.length() > 0)
							ajouterErreurARubrique(rubrique, message, 0);
					} else {
						ajouterErreurARubrique(
								rubrique,
								"Le congé de type "
										+ absence.toTypeAbsence().code()
										+ " débutant le "
										+ DateCtrl.dateToString(absence
												.dateDebut())
												+ ",  requiert que l'enfant soit fourni",
												1);
					}
				periodes.addObject(new PeriodeConge(absence, conge));

			}

			if (periodes.count() > 1) {
				periodes = PeriodeCirAvecDates.regrouperPeriodes(periodes);
				EOSortOrdering.sortArrayUsingKeyOrderArray(
						periodes,
						new NSArray(EOSortOrdering.sortOrderingWithKey(
								"dateDebutPourCir",
								EOSortOrdering.CompareDescending)));
			}

			NSMutableArray parametres;
			for (Enumeration<PeriodeConge> e1 = periodes.objectEnumerator(); e1
					.hasMoreElements(); calculerRubrique(rubrique, parametres)) {
				PeriodeConge periode = e1.nextElement();
				parametres = new NSMutableArray(periode);
				EOEnfant enfant = periode.enfant();
				if (enfant != null)
					parametres.addObject(enfant);
			}
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 */
	public void genererAffectation(EOEditingContext edc,RubriqueCir rubrique) {

		NSMutableArray<EOChangementPosition> changementsPositions = new NSMutableArray(EOChangementPosition
				.rechercherChangementsCirPourIndividuEtPeriode(edc, getAgentCIR().getIndividu(), getDebutPeriodeDeclaration(),
						getFinPeriodeDeclaration()));
		changementsPositions.addObjectsFromArray(EOChangementPosition
				.rechercherChangementsCirPassePourIndividuEtPeriode(edc, getAgentCIR().getIndividu(), getDebutPeriodeDeclaration(),
						getFinPeriodeDeclaration()));

		NSArray<EOChangementPosition> positions = EOSortOrdering.sortedArrayUsingKeyOrderArray(changementsPositions, EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);

		NSMutableArray<PeriodeAffectation> periodes = new NSMutableArray<PeriodeAffectation>();
		NSTimestamp dateDebut = new NSTimestamp();
		NSTimestamp dateFin = new NSTimestamp();

		EOChangementPosition positionPrecedente = null;
		// On parcourt toutes les positions et on genere une periode
		// d'affectation pour les positions avec un RNE ou
		for (EOChangementPosition myPosition : positions) {

			dateDebut = dateDebutPourPeriodeDeclaration(myPosition.dateDebut());
			dateFin = dateFinPourPeriodeDeclaration(myPosition.dateFin());

			if (positionPrecedente != null
					&& DateCtrl.isSameDay(myPosition.dateDebut(),
							positionPrecedente.dateDebut())
							&& DateCtrl.isSameDay(myPosition.dateFin(),
									positionPrecedente.dateFin())) {
				continue;
			}

			if (myPosition.toRne() != null)
				ajouterRubriqueAffectationAvecDates(periodes, myPosition.toRne().libelleLong(), dateDebut, dateFin);
			else if (myPosition.lieuPosition() != null)
				ajouterRubriqueAffectationAvecDates(periodes, myPosition.lieuPosition(), dateDebut, dateFin);
			else
				ajouterRubriqueAffectationAvecDates(periodes, myPosition.toPosition().libelleLong(), dateDebut, dateFin);

			positionPrecedente = myPosition;

		}

		if (periodes.count() > 1) {
			periodes = PeriodeCirAvecDates.regrouperPeriodesAffectation(periodes);
			EOSortOrdering.sortArrayUsingKeyOrderArray( periodes,
					new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebutPourCir", EOSortOrdering.CompareDescending)));
		}

		for (PeriodeAffectation periode : periodes) {

			NSMutableArray parametres = new NSMutableArray(periode);
			if (periode.rne() != null)
				parametres.addObject(periode.rne());
			else
				parametres.addObject(getRneEtablissement());

			calculerRubrique(rubrique, parametres);
		}

	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 */
	public void genererTempsTravail(EOEditingContext edc, RubriqueCir rubrique) {

		NSMutableArray<PeriodeTempsTravail> periodes = new NSMutableArray<PeriodeTempsTravail>();

		NSMutableArray durees = new NSMutableArray();
		durees.addObjectsFromArray(EOTempsPartiel
				.rechercherTempsPartielPourIndividuEtPeriode(edc,
						getCurrentIndividu(), getDebutPeriodeDeclaration(),
						getFinPeriodeDeclaration()));
		durees.addObjectsFromArray(EOCessProgActivite
				.rechercherDureesPourIndividuEtPeriode(edc,
						EOCessProgActivite.ENTITY_NAME, getCurrentIndividu(),
						getDebutPeriodeDeclaration(), getFinPeriodeDeclaration()));
		durees.addObjectsFromArray(EOMiTpsTherap
				.rechercherDureesPourIndividuEtPeriode(edc,
						EOMiTpsTherap.ENTITY_NAME, getCurrentIndividu(),
						getDebutPeriodeDeclaration(), getFinPeriodeDeclaration()));
		durees.addObjectsFromArray(EOCongeSsTraitement
				.rechercherCongesSansTraitementPourIndividuEtPeriode(
						edc, getCurrentIndividu(), getDebutPeriodeDeclaration(),
						getFinPeriodeDeclaration()));

		if (durees.count() > 0) {
			EOSortOrdering.sortArrayUsingKeyOrderArray(
					durees,	new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",	EOSortOrdering.CompareAscending)));

			if (dureesSansChevauchement(durees, rubrique.nom())) {

				NSTimestamp dateDebut = getDebutPeriodeDeclaration();

				for (Enumeration<Duree> e = durees.objectEnumerator(); e.hasMoreElements();) {

					Duree duree = e.nextElement();
					NSTimestamp dateFin = duree.dateFin();

					if (!duree.estAnnule()) {

						// si la date de début de cette durée est postérieure à
						// la date de début alors il y a une période de temps
						// plein
						if (DateCtrl.isAfter(duree.dateDebut(), dateDebut)) {
							// System.out.println("	1 - AJOUT PERIODE TEMPS PLEIN : DU "
							// + dateDebut + " AU " +
							// DateCtrl.jourPrecedent(duree.dateDebut()));
							periodes.addObjectsFromArray(periodesTempsPleinPourDates(
									dateDebut,
									DateCtrl.jourPrecedent(duree.dateDebut())));
							dateDebut = duree.dateDebut();
						}

						// TEMPS PARTIEL
						if (duree instanceof EOTempsPartiel) {
							EOTempsPartiel tempsPartiel = (EOTempsPartiel) duree;
							String message = tempsPartiel.validationsCir();
							int niveauMessage = 0;
							if (tempsPartiel.estDeclarationEnfantObligatoire()&& tempsPartiel.enfant() == null)
								niveauMessage = 1;
							if (message != null && message.length() > 0)
								ajouterErreurARubrique(rubrique, message,niveauMessage);
							dateFin = tempsPartiel.dateFinReelle();

							if (dateFin == null || DateCtrl.isAfter(dateFin, getFinPeriodeDeclaration()))
								dateFin = getFinPeriodeDeclaration();
						}

						// CONGE SANS TRAITEMENT
						if (duree instanceof EOCongeSsTraitement) {
							EOCongeSsTraitement conge = (EOCongeSsTraitement) duree;
							String message = conge.validationsCir();
							int niveauMessage = 0;
							if (message != null && message.length() > 0)
								ajouterErreurARubrique(rubrique, message, niveauMessage);
							dateFin = conge.dateFin();
							if (dateFin == null	|| DateCtrl.isAfter(dateFin, getFinPeriodeDeclaration()))
								dateFin = getFinPeriodeDeclaration();
						}

						// CPA
						if (duree instanceof EOCessProgActivite) {
							EOCessProgActivite cpa = (EOCessProgActivite) duree;
							String message = cpa.validationsCir();
							if (message != null && message.length() > 0)
								ajouterErreurARubrique(rubrique, message, 0);
							if (cpa.estQuotiteDegressive())
								periodes.addObjectsFromArray(preparerPeriodesPourCpaAvecQuotiteDegressive((EOCessProgActivite) duree));
							else if (cpa.estCessationTotaleActivite())
								periodes.addObjectsFromArray(preparerPeriodesPourCpaAvecCessationTotale((EOCessProgActivite) duree));
							else
								periodes.addObject(new PeriodeTempsTravail(duree, dateDebut, dateFin, 50));
						} else {
							periodes.addObject(new PeriodeTempsTravail(duree, dateDebut, dateFin, 100));
						}

						if (duree.dateFin() != null)
							dateDebut = DateCtrl.jourSuivant(dateFin);
						else
							dateDebut = null;
					}
				}

				if (dateDebut != null && DateCtrl.isBefore(dateDebut, getFinPeriodeDeclaration())) {
					periodes.addObjectsFromArray(periodesTempsPleinPourDates(dateDebut, getFinPeriodeDeclaration()));
				}
			}
		} else {
			periodes.addObjectsFromArray(periodesTempsPleinPourDates(getDebutPeriodeDeclaration(), getFinPeriodeDeclaration()));
		}

		if (periodes.count() > 1) {
			periodes = PeriodeCirAvecDates.regrouperPeriodes(periodes);
			EOSortOrdering.sortArrayUsingKeyOrderArray(
					periodes,
					new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebutPourCir", EOSortOrdering.CompareDescending)));
		}
		
		if (getAgentCIR().getDateCessationService() != null && getAgentCIR().isLastPeriode()
				&& DateCtrl.isBeforeEq(getAgentCIR().getDateCessationService(), getFinPeriodeDeclaration())) {
			PeriodeTempsTravail periode = (PeriodeTempsTravail) periodes.lastObject();
			if ( periode == null || periode.quotiteActivite() == 0.0D)
				ajouterErreurARubrique(
						rubrique,
						"Si la date de cessation de service de " + getAgentCIR().individu.identitePrenomFirst() + " (" + DateCtrl.dateToString(getAgentCIR().getDateCessationService()) + ") est incluse dans la période de déclaration, elle doit être une période avec un taux d'activité non nul",
						0);
		}

		NSMutableArray parametres;
		for (PeriodeTempsTravail myPeriode : periodes) {
			if (myPeriode.typeTempsTravail().equals(
					ConstantesCir.TEMPS_INCOMPLET)) {
				ajouterErreurARubrique(rubrique, "POSITIONS - Quotité INVALIDE ( <> 100)", 1);
			}

			parametres = new NSMutableArray(myPeriode);
			if (myPeriode.enfant() != null)
				parametres.addObject(myPeriode.enfant());
			calculerRubrique(rubrique, parametres);
		}

	}

	/**
	 * 
	 * Generation des periodes de RECUL D AGE ou de PROLONGATION D ACTIVITE
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 */
	public void genererProlongationActivite(EOEditingContext edc, RubriqueCir rubrique) {

		NSMutableArray<ProlongationActivite> prolongationsActivite = new NSMutableArray<ProlongationActivite>();

		prolongationsActivite.addObjectsFromArray(EOReculAge.findForIndividu(
				edc, getCurrentIndividu(), getDebutPeriodeDeclaration(),
				getFinPeriodeDeclaration()));
		prolongationsActivite.addObjectsFromArray(EOProlongationActivite
				.findForIndividu(edc, getCurrentIndividu(), getDebutPeriodeDeclaration(),
						getFinPeriodeDeclaration()));

		prolongationsActivite = new NSMutableArray(
				EOSortOrdering.sortedArrayUsingKeyOrderArray(
						prolongationsActivite,
						EOProlongationActivite.SORT_ARRAY_DATE_DEBUT_ASC));

		for (ProlongationActivite myProlongation : prolongationsActivite) {

			NSTimestamp dateFin = myProlongation.dateFin();
			if (myProlongation.dFinExecution() != null
					&& DateCtrl.isBefore(myProlongation.dFinExecution(),
							dateFin))
				dateFin = myProlongation.dFinExecution();

			PeriodeProlongation periode = new PeriodeProlongation(
					myProlongation, dateFin);
			if (myProlongation instanceof EOReculAge) {
				String message = ((EOReculAge) myProlongation).validationsCir();
				if (message != null && message.length() > 0)
					ajouterErreurARubrique(rubrique, message, 0);
			}

			calculerRubrique(rubrique, new NSMutableArray<PeriodeProlongation>(periode));

		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param rubrique
	 */
	public void genererGradeIndice(EOEditingContext editingContext, RubriqueCir rubrique) {

		NSMutableArray<PeriodeGradeIndice> periodes = new NSMutableArray<PeriodeGradeIndice>();

		NSArray<EOElementCarriere> elements = EOElementCarriere.rechercherElementsAvecCriteres(
				editingContext, EOQualifier.qualifierWithQualifierFormat(
						EOElementCarriere.TO_INDIVIDU_KEY + " = %@", new NSArray(getCurrentIndividu())), true, false);

		for (EOElementCarriere element : elements) {

			if (IntervalleTemps.intersectionPeriodes(element.dateDebut(),
					element.dateFin(), getDebutPeriodeDeclaration(),
					getFinPeriodeDeclaration()) != null && !element.estAnnule()) {

				NSMutableArray parametres = new NSMutableArray(element);
				parametres.addObject(element.toGrade());

				if (element.toGrade().cGradeNNE() != null && (element.indiceBrut() != null || element.indiceMajore() != null)) {
					periodes.addObject(new PeriodeGradeIndice(element));
				}
			}
		}

		NSArray<EOContratAvenant> avenants = EOContratAvenant.findForIndividu(editingContext, getCurrentIndividu());
		
		for (EOContratAvenant avenant : avenants) {

			if (avenant.contrat().estCIR() && avenant.estAnnule() == false) {
				if (IntervalleTemps.intersectionPeriodes(avenant.dateDebut(),
						avenant.dateFin(), getDebutPeriodeDeclaration(),
						getFinPeriodeDeclaration()) != null ) {

					NSMutableArray parametres = new NSMutableArray(avenant);
					parametres.addObject(avenant.toGrade());

					if (avenant.toGrade().cGradeNNE() != null && (avenant.indiceBrut() != null || avenant.indiceMajore() != null)) {
						periodes.addObject(new PeriodeGradeIndice(avenant));
					}
				}
			}
		}

		NSMutableArray parametres;
		for (PeriodeGradeIndice periode : periodes) {
			parametres = new NSMutableArray(periode);
			if (periode.getElement() != null) {
				parametres.addObject(periode.getElement());
			}
			parametres.addObject(periode.getGrade());
			calculerRubrique(rubrique, parametres);
		}

	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 */
	public void genererServiceAuxiliaire(EOEditingContext edc, RubriqueCir rubrique) {

		if (isRubriqueServiceAuxiliaireCalculee() || !getAgentCIR().isLastPeriode()) {
			return;
		}

		NSMutableArray<PeriodeServiceAuxiliaire> periodes = new NSMutableArray<PeriodeServiceAuxiliaire>();

		if ( ERXProperties.booleanForKeyWithDefault(Application.FEATURE_SERVICES_VALIDES, false)) {

			NSArray<EOValidationServices> validations = EOValidationServices.findForIndividu(edc, getCurrentIndividu());
			validations = EOSortOrdering.sortedArrayUsingKeyOrderArray(validations, EOValidationServices.SORT_ARRAY_DATE_DESC);
			for (EOValidationServices validation : validations) {
				String message = validation.validationsCir();
				if (message != null && message.length() > 0)
					ajouterErreurARubrique(rubrique, message, 0);

				periodes.addObject(new PeriodeServiceAuxiliaire(validation));
			}

		}
		else {

			NSArray<EOPasse> passes = EOPasse.rechercherPassesPourIndividuAvecServicesValides(edc, getCurrentIndividu());
			passes = EOSortOrdering.sortedArrayUsingKeyOrderArray(passes, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
			for (EOPasse passe : passes) {
				String message = passe.validationsCir();
				if (message != null && message.length() > 0)
					ajouterErreurARubrique(rubrique, message, 0);

				periodes.addObject(new PeriodeServiceAuxiliaire(passe));
			}
		}

		if (periodes.size() > 0) {
			EOSortOrdering.sortArrayUsingKeyOrderArray(periodes, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);

			periodes = new NSMutableArray(regrouperPeriodes(periodes));

			for (PeriodeServiceAuxiliaire periode : periodes) {
				calculerRubrique(rubrique, new NSArray<PeriodeServiceAuxiliaire>(periode));
			}

			setRubriqueServiceAuxiliaireCalculee(true);
		}
	}

	/**
	 * 
	 * Rassembler les periodes se chevauchant et cumuler la quotite
	 * FIXME 
	 * On estime que seules 2 periodes peuvent se chevaucher
	 * A ameliorer !!!
	 * 
	 * @param periodes
	 * @return
	 */
	public NSArray<PeriodeServiceAuxiliaire> regrouperPeriodes( NSArray<PeriodeServiceAuxiliaire> periodes) {

		NSMutableArray<PeriodeServiceAuxiliaire> nouvellesPeriodes = new NSMutableArray<PeriodeServiceAuxiliaire>();
		PeriodeServiceAuxiliaire periodePrecedente = null;
		boolean ajouterDernierPeriode = true;

		// On parcourt les periodes et on teste les chevauchements
		for(PeriodeServiceAuxiliaire periode : periodes) {

			ajouterDernierPeriode = true;

			if (periodePrecedente != null &&
					DateCtrl.chevauchementPeriode(periode.getDateDebut(), periode.getDateFin(), periodePrecedente.getDateDebut(), periodePrecedente.getDateFin())) {

				PeriodeServiceAuxiliaire nouvellePeriode = getNouvellePeriode(periodePrecedente, periode);
				nouvellesPeriodes.addObject(nouvellePeriode);
				ajouterDernierPeriode = false;
			}
			else {
				if (periodePrecedente != null) {
					nouvellesPeriodes.add(periodePrecedente);
				}
			}

			periodePrecedente = periode;
		}

		if (ajouterDernierPeriode)
			nouvellesPeriodes.addObject(periodePrecedente);

		return nouvellesPeriodes;

	}

	/**
	 * Creation d'une periode de service auxiliaire a partir de la combinaison de 2 periodes
	 * @param periodePrecedente
	 * @param periode
	 * @return
	 */
	private PeriodeServiceAuxiliaire getNouvellePeriode(PeriodeServiceAuxiliaire periodePrecedente, PeriodeServiceAuxiliaire periode) {

		PeriodeServiceAuxiliaire nouvellePeriode = new PeriodeServiceAuxiliaire();

		if (periode.getPasse() != null)
			nouvellePeriode.setPasse(periode.getPasse());
		else
			nouvellePeriode.setValidationService(periode.getValidationService());

		nouvellePeriode.setDateDebut(periodePrecedente.getDateDebut());
		nouvellePeriode.setDateFin(periode.getDateFin());

		BigDecimal quotite = periode.getQuotiteCotisation().add(periodePrecedente.getQuotiteCotisation());
		nouvellePeriode.setQuotiteCotisation(quotite);

		int annees = periode.getAnnees().intValue() + periodePrecedente.getAnnees().intValue(); 
		int mois = periode.getMois().intValue() + periodePrecedente.getMois().intValue(); 
		int jours = periode.getJours().intValue() + periodePrecedente.getJours().intValue(); 

		nouvellePeriode.setAnnees(annees);
		nouvellePeriode.setMois(mois);
		nouvellePeriode.setJours(jours);

		nouvellePeriode.calculerDureeService();

		if (quotite.doubleValue() == 100)
			nouvellePeriode.setTypeTempsTravail(ConstantesCir.TEMPS_PLEIN);

		return nouvellePeriode;
	}

	/**
	 * 
	 * @param eoeditingcontext
	 * @param individu
	 * @param rubrique
	 */
	public void genererBeneficeEtudes(EOEditingContext edc, RubriqueCir rubrique) {

		if (isRubriqueEtudesCalculee()) {
			return;
		}

		NSArray<EOBeneficeEtudes> periodes = EOBeneficeEtudes.findForIndividu(edc, getCurrentIndividu());
		if (periodes.count() > 0) {
			for (EOBeneficeEtudes periode : periodes) {
				calculerRubrique(rubrique, new NSArray<EOBeneficeEtudes>(periode));
			}
		}

		setRubriqueEtudesCalculee(true);

	}

	/**
	 * 
	 * @param eoeditingcontext
	 * @param individu
	 * @param rubrique
	 */
	public void genererBonifications(EOEditingContext edc, RubriqueCir rubrique) {

		if (isRubriqueBonificationsCalculee()) {
			return;
		}

		NSMutableArray<EOBonifications> periodes = new NSMutableArray<EOBonifications>();
		NSArray<EOBonifications> bonifications = EOBonifications.findForIndividuAnterieuresADate(edc, getCurrentIndividu(), getFinPeriodeDeclaration());

		for (EOBonifications bonification : bonifications) {
			String message = bonification.validationsCir();
			if (message != null && message.length() > 0) {
				ajouterErreurARubrique(rubrique, message, 0);
			}

			periodes.addObject(bonification);

		}

		if (periodes.count() > 0) {
			for (EOBonifications periode : periodes) {
				calculerRubrique(rubrique, new NSArray<EOBonifications>(periode));
			}
		}

		setRubriqueBonificationsCalculee(true);
	}

	/**
	 * 
	 * @param eoeditingcontext
	 * @param individu
	 * @param rubrique
	 */
	public void genererEtudesRachetees(EOEditingContext edc, RubriqueCir rubrique) {
		NSArray<EOEtudesRachetees> periodes = EOEtudesRachetees.findForIndividu(edc, getCurrentIndividu());
		if (periodes.count() > 0) {
			for (EOEtudesRachetees myPeriode : periodes) {
				String message = myPeriode.validationsCir();
				if (message != null && message.length() > 0)
					ajouterErreurARubrique(rubrique, message, 0);
				calculerRubrique(rubrique, new NSArray<EOEtudesRachetees>(myPeriode));
			}
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 */
	public void genererServiceNational(EOEditingContext edc, RubriqueCir rubrique) {

		if (isRubriqueServiceNationalCalculee()  || !getAgentCIR().isLastPeriode())
			return;

		NSArray<EOPeriodesMilitaires> services = EOPeriodesMilitaires.findForIndividuAnterieuresADate(edc, getCurrentIndividu(), getAgentCIR().getDebutPeriode());
		services = EOSortOrdering.sortedArrayUsingKeyOrderArray(services,
				EOPeriodesMilitaires.SORT_DEBUT_DESC);

		NSMutableArray<PeriodeServiceNational> periodes = new NSMutableArray<PeriodeServiceNational>();

		if (evaluerChevauchementsPeriodesMilitaires(services))
			ajouterErreurARubrique(
					rubrique,
					"Veuillez vérifier les périodes militaires car elles se chevauchent.",
					0);

		for (EOPeriodesMilitaires service : services) {
			periodes.addObject(new PeriodeServiceNational(service.dateDebut(), service.dateFin(), service.typePeriodeOnp()));
		}

		if (services.size() == 0) {
			if (getAgentCIR().getIndividu().toSituationMilitaire() != null
					&& (getAgentCIR().getIndividu().toSituationMilitaire().isReforme() || getAgentCIR().getIndividu().toSituationMilitaire().isExempte())) { {
						periodes.addObject(new PeriodeServiceNational(
								DateCtrl.dateAvecAjoutJours(getAgentCIR().getIndividu().dNaissance(), 1), 
								DateCtrl.dateAvecAjoutJours(getAgentCIR().getIndividu().dNaissance(), 1), CODE_SERVICE_NATIONAL_EXEMPTE));
					}
			}
		}

		for (PeriodeServiceNational periode : periodes) {
			calculerRubrique(rubrique, new NSMutableArray(periode));
		}

		setRubriqueServiceNationalCalculee(true);

	}

	/**
	 * 
	 * @param edc
	 * @param individu
	 * @param rubrique
	 */
	public void genererEnfant(EOEditingContext edc, RubriqueCir rubrique) {

		// On enregistre les enfants une seule fois sur la derniere periode de prise en charge
		if (isRubriqueEnfantCalculee() || !getAgentCIR().isLastPeriode() ) {
			return;
		}

		NSArray<EORepartEnfant> enfants = EORepartEnfant.rechercherPourParent(edc, getCurrentIndividu());
		enfants = EOSortOrdering.sortedArrayUsingKeyOrderArray(
				enfants, EORepartEnfant.SORT_ARRAY_NAISSANCE_DESC);

		for (EORepartEnfant repartEnfant : enfants) {

			EOEnfant enfant = repartEnfant.enfant();

			String message = repartEnfant.validationsCir();
			if (message != null && message.length() > 0)
				ajouterErreurARubrique(rubrique, message, 0);

			message = enfant.validationsCir();
			if (message != null && message.length() > 0)
				ajouterErreurARubrique(rubrique, message, 0);

			if (message == null || message.length() == 0) {

				NSMutableArray parametres = new NSMutableArray();

				message = repartEnfant.preparerPourCir(getFinPeriodeDeclaration(), getCurrentIndividu().dDeces());
				if (message != null && message.length() > 0)
					ajouterErreurARubrique(rubrique, message, 1);

				parametres.addObject(enfant);
				parametres.addObject(repartEnfant);
				if (repartEnfant.lienFiliation() != null)
					parametres.addObject(repartEnfant.lienFiliation());
				else
					parametres.addObject(EOLienFiliationEnfant.getDefault(edc));

				// Handicaps
				NSMutableArray qualifiers = new NSMutableArray(
						EOQualifier.qualifierWithQualifierFormat(
								EOPeriodeHandicap.ENFANT_KEY + "=%@",
								new NSArray(enfant)));
				qualifiers.addObject(SuperFinder
						.qualifierPourPeriode(EOPeriodeHandicap.DATE_DEBUT_KEY,
								getDebutPeriodeDeclaration(),
								EOPeriodeHandicap.DATE_FIN_KEY,
								getFinPeriodeDeclaration()));
				NSArray<EOPeriodeHandicap> periodes = EOPeriodeHandicap
						.rechercherDureePourEntiteAvecCriteres(edc,
								EOPeriodeHandicap.ENTITY_NAME,
								new EOAndQualifier(qualifiers), false);
				if (periodes.count() > 0) {
					periodes = EOSortOrdering.sortedArrayUsingKeyOrderArray(
							periodes,
							new NSArray(EOSortOrdering.sortOrderingWithKey(
									"dateDebut",
									EOSortOrdering.CompareDescending)));
					parametres.addObject(periodes.objectAtIndex(0));
				}

				calculerRubrique(rubrique, parametres);
			}
		}
		setRubriqueEnfantCalculee(true);
	}

	/**
	 * 
	 * @param individus
	 * @return
	 */
	private String supprimerDonneesCir(NSArray<EOIndividu> individus) {

		EOEditingContext edc = getFichierCourant().editingContext();

		try {
			informerThread(">> Suppression des donnees des individus ...");
			for (EOIndividu individu : individus) {

				if (individu != null) {
					EOCirCarriere carriere = EOCirCarriere.carrierePourFichierEtIndividu(edc, fichierCourant, individu);
					if (carriere != null) {
						NSArray<EOCirCarriereData> datas = EOCirCarriereData.recordsPourCirCarriere(edc,	carriere);

						for (EOCirCarriereData data : datas)
							edc.deleteObject(data);

						edc.deleteObject(carriere);
					}
				}
			}

			boolean shouldSave = false;
			if (getFichierCourant().cfimFichierEnvoi() != null) {
				getFichierCourant().setCfimFichierEnvoi(null);
				shouldSave = true;
			}
			if (shouldSave)
				edc.saveChanges();

			informerThread(">> Anciennes données supprimées ...");

		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return e.getMessage();
		}
		return null;
	}

	// la generation de l'entete modifie un record de type EOCirFichierImport
	// pour y ajouter la norme
	private String modifierInformationsFichier(EOEditingContext edc) {
		RubriqueCir rubrique = listeRubriques
				.rubriqueGeneralePourNom(NOM_RUBRIQUE_ENTETE);
		// Rechercher les constantes pour générer la rubrique de norme
		SousRubriqueCir sousRubrique = rubrique
				.sousRubriqueAvecCle(NOM_SOUS_RUBRIQUE_NORME);
		try {
			edc.lock();
			fichierCourant.setCfimNorme(sousRubrique.valeur());
			edc.saveChanges();
		} catch (Exception e) {
			edc.revert();
			fichierCourant = null;
			LogManager.logException(e);
			return e.getMessage();
		} finally {
			edc.unlock();
		}
		return null;
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubriquesPourIndividu
	 */
	private void genererIndividu(EOEditingContext edc, NSArray<RubriqueCir> rubriquesPourIndividu) {

		try {

			edc.lock();

			// Les rubriques sont triées par ordre de priorité. On passera à
			// chaque méthode invoquée, l'editing context, l'individu et la
			// rubrique courante
			for (RubriqueCir rubrique : rubriquesPourIndividu) {

				Class[] typeParametres = new Class[] { EOEditingContext.class, RubriqueCir.class };
				try {
					java.lang.reflect.Method methode = this
							.getClass()
							.getMethod(rubrique.nomMethodePourGenerationRubrique(),typeParametres);
					Object[] parametres = new Object[] { edc, rubrique };

					methode.invoke(this, parametres);

				} catch (Exception exc) {
					exc.printStackTrace();
					LogManager.logException(exc);
					signalerExceptionThread(exc);
				}
			}

			edc.saveChanges();

		} catch (Exception exc) {
			LogManager.logException(exc);
			exc.printStackTrace();
			signalerExceptionThread(exc);
		}
		edc.unlock();

		//		// 09/12/2010- Vérifier si l'indicateur de doubleCarrière a été
		//		// positionné pour regénérer une déclaration
		//		// CAS d'un detachement entrant puis activite dans l'etablissement
		//		if (isGereDoubleCarriere()) {
		//			genererIndividu(edc, getAgentCIR(), rubriquesPourIndividu);
		//			setGereDoubleCarriere(false);
		//		}

	}

	/**
	 * 
	 * @param rubrique
	 * @param individu
	 * @param parametres
	 */
	private void calculerRubrique(RubriqueCir rubrique,	NSArray parametres) {

		// LogManager.logDetail((new
		// StringBuilder("Generation de la rubrique ")).append(rubrique.nom()).toString());
		NSMutableArray classeParametres = new NSMutableArray();
		for (Enumeration e = parametres.objectEnumerator(); e.hasMoreElements(); classeParametres
				.addObject(e.nextElement().getClass()))
			;
		try {
			String resultat = rubrique.preparer(parametres.objects(),
					classeParametres.objects());
			if (!rubrique.getExceptions().equals("")) {
				if (getCurrentIndividu() != null) {
					EnvironnementCir.sharedInstance().ajouterErreur(	"*************************\n" + getCurrentIndividu().identite());
					getCurrentCirCarriere().setTemValide("N");
					if (getCurrentCirCarriere().circCommentaires() == null)
						getCurrentCirCarriere().setCircCommentaires(rubrique.getExceptions());
					else {
						if ((getCurrentCirCarriere().circCommentaires() + "\n" + rubrique.getExceptions()).length() < 2000)
							getCurrentCirCarriere().setCircCommentaires(currentCirCarriere.circCommentaires() + "\n"+ rubrique.getExceptions());
					}

				}
				informerThread("Rubrique " + rubrique.nom() + "\n"+ rubrique.getExceptions());
				EnvironnementCir.sharedInstance().ajouterErreur("**************************\n" + "Rubrique " + rubrique.nom() + "\n" + rubrique.getExceptions());
				rubrique.resetExceptions();
			}
			if (resultat.length() > 0) {

				numeroClassement++;
				EOCirCarriereData data = new EOCirCarriereData();
				data.setCirCarriereRelationship(currentCirCarriere);
				data.setCircdRubrique(rubrique.nom());
				data.setCircdData(resultat);
				data.setCircdClassement(new Integer(numeroClassement));
				getCurrentIndividu().editingContext().insertObject(data);

			}
		} catch (Exception e1) {
			EnvironnementCir.sharedInstance().ajouterErreur(e1.getMessage());
			informerThread((new StringBuilder("Une exception s'est produite "))
					.append(e1.getMessage()).toString());
			e1.printStackTrace();
		}
	}

	/**
	 * 
	 * @param carriere
	 * @param date
	 * @param statutIndividu
	 * @return
	 */
	private String evaluerCategorieServicePourCarriere(EOCarriere carriere, NSTimestamp date, String statutIndividu) {

		String typeCategorie = DEFAULT_CS_TYPE;
		if (carriere != null && carriere.temEnseignant().equals(CocktailConstantes.VRAI)) {
			EOGrade grade = EOElementCarriere.rechercherGradePourIndividuADate(
					carriere.editingContext(), carriere.toIndividu(), date);
			// Cas des instituteurs
			if (grade != null && grade.toCorps().cCorps().equals("600"))
				typeCategorie = "A";
		}

		// Pas de categorie de service (Actif ou Sedentaire) pour les
		// militaires.
		if (statutIndividu.equals(ConstantesCir.MILITAIRE))
			typeCategorie = "";

		return typeCategorie;
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param rubrique
	 * @return
	 */
	private NSMutableArray<PeriodePositionStatutaire> preparerPeriodesStatutairesPourIndividuEtRubrique(EOEditingContext edc, RubriqueCir rubrique) {

		periodesAvecQuotite = new NSMutableArray();

		NSMutableArray<PeriodePositionStatutaire> periodesStatutaires = new NSMutableArray<PeriodePositionStatutaire>();

		if (getAgentCIR().getChangementsPositions() != null && getAgentCIR().getChangementsPositions().size() > 0) {
			NSMutableArray<DureePourPositionStatutaire> durees = new NSMutableArray(getAgentCIR().getChangementsPositions());

			durees = preparerDureesPourPositionStatutaire(durees, rubrique);

			DureePourPositionStatutaire dureeInitiale = durees.get(0);
			if (durees.count() == 1) {
				periodesStatutaires.addObject(new PeriodePositionStatutaire( dureeInitiale, dureeInitiale.dateDebut(), dureeInitiale
						.dateFin(), getAgentCIR().isDetachementEntrant()));
				periodesAvecQuotite.addObject(new PeriodeAvecQuotite(dureeInitiale
						.dateDebut(), dureeInitiale.dateFin(), evaluerQuotite(
								dureeInitiale, false)));

			} else {
				NSMutableArray pileDureesACheval = new NSMutableArray();
				NSTimestamp dateDebutComparaison = dureeInitiale.dateDebut();
				NSTimestamp dateFinComparaison = dureeInitiale.dateFin();
				for (int i = 1; i < durees.count(); i++) {
					DureePourPositionStatutaire duree = (DureePourPositionStatutaire) durees
							.objectAtIndex(i);
					org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps intervalle = org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps
							.intersectionPeriodes(dateDebutComparaison,
									dateFinComparaison, duree.dateDebut(),
									duree.dateFin());
					boolean derniereDureeEmpilee = false;
					if (intervalle != null) {
						pileDureesACheval.addObject(dureeInitiale);
						dureeInitiale = duree;
						dateDebutComparaison = duree.dateDebut();
						if (DateCtrl.isAfter(duree.dateFin(), dateFinComparaison))
							dateFinComparaison = duree.dateFin();
						if (i == durees.count() - 1) {
							derniereDureeEmpilee = true;
							pileDureesACheval.addObject(duree);
						}
					}
					if (intervalle == null || i == durees.count() - 1) {
						if (pileDureesACheval.count() > 0) {
							if (intervalle == null)
								pileDureesACheval.addObject(dureeInitiale);
							periodesStatutaires
							.addObjectsFromArray(gererPileDureesACheval(
									pileDureesACheval, dateFinComparaison));
						} else {
							periodesStatutaires
							.addObject(new PeriodePositionStatutaire(
									dureeInitiale, dateDebutComparaison,
									dateFinComparaison,
									getAgentCIR().isDetachementEntrant()));
							periodesAvecQuotite.addObject(new PeriodeAvecQuotite(
									dateDebutComparaison, dateFinComparaison,
									evaluerQuotite(dureeInitiale, false)));
						}
						if (intervalle == null) {
							pileDureesACheval = new NSMutableArray();
							dureeInitiale = duree;
							dateDebutComparaison = duree.dateDebut();
							dateFinComparaison = duree.dateFin();
						}
					}
					if (i == durees.count() - 1 && !derniereDureeEmpilee) {
						periodesStatutaires.addObject(new PeriodePositionStatutaire(
								dureeInitiale, dateDebutComparaison,
								dateFinComparaison,
								getAgentCIR().isDetachementEntrant()));
						periodesAvecQuotite.addObject(new PeriodeAvecQuotite(
								dateDebutComparaison, dateFinComparaison,
								evaluerQuotite(dureeInitiale, false)));
					}
				}

			}
			return periodesStatutaires;
		}
		else {
			// Prise en compte des contrats d ATER titulaires
			for (EOContratAvenant avenant : getAgentCIR().getAvenants()) {
				periodesStatutaires.addObject(new PeriodePositionStatutaire(avenant));
				periodesAvecQuotite.addObject(new PeriodeAvecQuotite(
						avenant.dateDebut(), avenant.dateFin(), avenant.quotite()));
			}			
		}


		return periodesStatutaires;
	}

	/**
	 * 
	 * @param durees
	 * @param rubrique
	 * @return
	 */
	private NSMutableArray preparerDureesPourPositionStatutaire(NSArray durees, RubriqueCir rubrique) {

		NSMutableArray dureesPourPositionStatutaire = new NSMutableArray(durees.size());

		for (Enumeration<IDureePourIndividu> e = durees.objectEnumerator(); e.hasMoreElements(); 
				dureesPourPositionStatutaire.addObject(new DureePourPositionStatutaire(e.nextElement())));

		try {
			dureesPourPositionStatutaire.sortUsingComparator(new DureeCroissanteComparator());
			return dureesPourPositionStatutaire;
		} catch (Exception exc) {
			exc.printStackTrace();
			ajouterErreurARubrique(rubrique, "Erreur lors du tri des durees (" + exc.getMessage()
					+ ", pas de rubrique de position statutaire g\351n\351r\351e",	0);
			return new NSMutableArray();
		}
	}

	/**
	 * 
	 * @param dureesACheval
	 * @param dateFinComparaison
	 * @return
	 */
	private NSArray gererPileDureesACheval(NSArray dureesACheval,
			NSTimestamp dateFinComparaison) {
		NSTimestamp debutPeriode = null;
		NSTimestamp finPeriode = null;
		boolean pileTerminee = false;
		NSMutableArray periodesStatutaires = new NSMutableArray();
		for (int i = 0; i < dureesACheval.count() && !pileTerminee; i++) {
			DureePourPositionStatutaire dureeCourante = (DureePourPositionStatutaire) dureesACheval
					.objectAtIndex(i);
			if (debutPeriode == null
					|| DateCtrl.isBeforeEq(debutPeriode,
							dureeCourante.dateFin())) {
				if (debutPeriode == null
						|| DateCtrl.isBefore(debutPeriode,
								dureeCourante.dateDebut())) {
					debutPeriode = dureeCourante.dateDebut();
					if (finPeriode == null
							|| DateCtrl.isBeforeEq(finPeriode, debutPeriode))
						finPeriode = dureeCourante.dateFin();
				}
				double quotite = evaluerQuotite(dureeCourante, true)
						.doubleValue();
				if (i == dureesACheval.count() - 1) {
					periodesStatutaires
					.addObject(new PeriodePositionStatutaire(
							dureeCourante, debutPeriode, finPeriode,
							getAgentCIR().isDetachementEntrant()));
					if (quotite == 0.0D)
						quotite = 100D;
					periodesAvecQuotite.addObject(new PeriodeAvecQuotite(
							debutPeriode, finPeriode, new Double(quotite)));
				} else {
					for (boolean termine = false; !termine;) {
						DureePourPositionStatutaire dureePrioritaire = dureeCourante;
						for (int j = i + 1; j < dureesACheval.count(); j++) {
							DureePourPositionStatutaire dureeAComparer = (DureePourPositionStatutaire) dureesACheval
									.objectAtIndex(j);
							if (!DateCtrl.isAfterEq(dureeAComparer.dateFin(),
									debutPeriode))
								continue;
							if (DateCtrl.isBeforeEq(dureeAComparer.dateDebut(),
									debutPeriode)) {
								quotite += evaluerQuotite(dureeAComparer, true)
										.doubleValue();
								if (DateCtrl.isBefore(dureeAComparer.dateFin(),
										finPeriode))
									finPeriode = dureeAComparer.dateFin();
								if (!dureeAComparer
										.dureeAvecChangementPosition())
									dureePrioritaire = dureeAComparer;
								if (dureeAComparer != dureesACheval
										.lastObject())
									continue;
								periodesStatutaires
								.addObject(new PeriodePositionStatutaire(
										dureePrioritaire, debutPeriode,
										finPeriode,
										getAgentCIR().isDetachementEntrant()));
								periodesAvecQuotite
								.addObject(new PeriodeAvecQuotite(
										debutPeriode, finPeriode,
										new Double(quotite)));
								debutPeriode = DateCtrl.jourSuivant(finPeriode);
								finPeriode = dateFinComparaison;
								if (DateCtrl.isBefore(debutPeriode,
										dureeCourante.dateFin())) {
									periodesStatutaires
									.addObject(new PeriodePositionStatutaire(
											dureeCourante,
											debutPeriode, finPeriode,
											getAgentCIR().isDetachementEntrant()));
									periodesAvecQuotite
									.addObject(new PeriodeAvecQuotite(
											debutPeriode, finPeriode,
											new Double(quotite)));
									termine = true;
									pileTerminee = true;
								}
							} else {
								NSTimestamp finPossible = DateCtrl
										.jourPrecedent(dureeAComparer
												.dateDebut());
								if (DateCtrl.isBefore(finPossible, finPeriode))
									finPeriode = finPossible;
								periodesStatutaires
								.addObject(new PeriodePositionStatutaire(
										dureePrioritaire, debutPeriode,
										finPeriode,
										getAgentCIR().isDetachementEntrant()));
								periodesAvecQuotite
								.addObject(new PeriodeAvecQuotite(
										debutPeriode, finPeriode,
										new Double(quotite)));
								debutPeriode = DateCtrl.jourSuivant(finPeriode);
								finPeriode = dateFinComparaison;
								quotite = evaluerQuotite(dureeCourante, true)
										.doubleValue();
							}
							break;
						}

						if (DateCtrl.isAfterEq(debutPeriode,
								dureeCourante.dateFin()))
							termine = true;
					}

				}
			}
		}

		return periodesStatutaires;
	}


	/**
	 * 
	 * @param duree
	 * @param estCumulQuotite
	 * @return
	 */
	private Number evaluerQuotite(DureePourPositionStatutaire duree,
			boolean estCumulQuotite) {
		Number quotite;
		if (estCumulQuotite)
			quotite = new Double(0.0D);
		else
			quotite = new Double(100D);
		if (duree != null && duree.changement() != null) {
			// FIXME Quotite automate CIR
			quotite = duree.changement().quotite();
			if (!duree.changement().toPosition().estEnActivite()
					&& !duree.changement().toPosition().estServiceNational()) {
				quotite = new Double(0.0D);
			}
		}
		return quotite;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	private NSTimestamp dateDebutPourPeriodeDeclaration(NSTimestamp date) {
		NSTimestamp debutPeriode = getDebutPeriodeDeclaration();
		if (DateCtrl.isBefore(date, debutPeriode))
			return debutPeriode;
		else
			return date;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	private NSTimestamp dateFinPourPeriodeDeclaration(NSTimestamp date) {
		NSTimestamp finPeriode = getFinPeriodeDeclaration();
		if (date == null || DateCtrl.isAfter(date, finPeriode))
			return finPeriode;
		else
			return date;
	}


	/**
	 * 
	 * @param periodes
	 * @param lieu
	 * @param dateDebut
	 * @param dateFin
	 */
	private void ajouterRubriqueAffectationAvecDates(NSMutableArray<PeriodeAffectation> periodes, String lieu, NSTimestamp dateDebut, NSTimestamp dateFin) {
		PeriodeAffectation periode = new PeriodeAffectation(dateDebut, dateFin,	lieu);
		periodes.addObject(periode);
	}

	/**
	 * 
	 * @param duree
	 * @return
	 */
	private NSTimestamp getDateFinForDuree(IDuree duree) {

		NSTimestamp dateFinCalculee;

		if (duree instanceof EOTempsPartiel) {
			EOTempsPartiel tempsPartiel = (EOTempsPartiel)duree;
			dateFinCalculee = tempsPartiel.dFinExecution();
		}
		else {
			dateFinCalculee = duree.dateFin();
		}

		return dateFinCalculee;
	}

	/**
	 * 
	 * @param durees
	 * @param nomRubrique
	 * @return
	 */
	private boolean dureesSansChevauchement(NSArray<IDuree> durees, String nomRubrique) {

		// informerThread("		AutomateCir.dureesSansChevauchement()");
		NSTimestamp dateFinInitiale, dateFinDuree;

		if (durees != null && durees.count() > 0) {

			IDuree dureeInitiale = durees.get(0);
			dateFinInitiale = getDateFinForDuree(dureeInitiale);

			String message = null;
			String identite = "";
			if (dureeInitiale instanceof Duree) {
				identite = ((Duree) dureeInitiale).individu().identite();
			}
			else if (dureeInitiale instanceof EOAbsences) {
				identite = ((EOAbsences) dureeInitiale).toIndividu().identite();
			}

			for (int i = 1; i < durees.count(); i++) {

				IDuree duree = durees.get(i);
				dateFinDuree = getDateFinForDuree(duree);

				boolean estValide = false;
				String typeDuree = "";
				String typeInitial = "";
				if (duree instanceof Duree) {
					estValide = !((Duree) duree).estAnnule();
					typeDuree = ((Duree) duree).typeEvenement();
					typeInitial = ((Duree) dureeInitiale).typeEvenement();
				} else if (duree instanceof EOAbsences) {
					estValide = ((EOAbsences) duree).estValide();
					typeDuree = ((EOAbsences) duree).toTypeAbsence().code();
					typeInitial = ((EOAbsences) dureeInitiale).toTypeAbsence().code();
				}

				if (!estValide)
					continue;

				if (org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps.intersectionPeriodes(
						dureeInitiale.dateDebut(),
						dateFinInitiale, 
						duree.dateDebut(),
						dateFinDuree) != null) {

					message = "Chevauchement entre " + typeDuree
							+ " débutant le "
							+ DateCtrl.dateToString(duree.dateDebut()) + " et "
							+ typeInitial + " débutant le "
							+ DateCtrl.dateToString(dureeInitiale.dateDebut());
					currentCirCarriere.setTemValide("N");
					if (currentCirCarriere.circCommentaires() != null) {

						if ((currentCirCarriere.circCommentaires() + "\n" + message).length() < 2000)
							currentCirCarriere
							.setCircCommentaires(currentCirCarriere
									.circCommentaires()
									+ "\n"
									+ message);

					} else
						currentCirCarriere.setCircCommentaires(message);
					break;

				}
				dureeInitiale = duree;
				dateFinInitiale = getDateFinForDuree(dureeInitiale);
			}

			if (message != null) {
				message = (new StringBuilder(
						"Pas de generation de la rubrique "))
						.append(nomRubrique).append(" : ").append(message)
						.toString();
				informerThread(message);
				EnvironnementCir.sharedInstance().ajouterErreur(
						(new StringBuilder("**************************\n"
								+ String.valueOf(identite))).append(" : ")
								.append(message).toString());
				return false;
			} else
				return true;
		}

		return true;

	}

	/**
	 * 
	 * @param periodes
	 * @return
	 */
	private boolean evaluerChevauchementsPeriodesMilitaires(NSArray periodes) {
		if (periodes.count() <= 1)
			return false;
		EOPeriodesMilitaires periodeInitiale = (EOPeriodesMilitaires) periodes
				.objectAtIndex(0);
		for (int i = 1; i < periodes.count(); i++) {
			EOPeriodesMilitaires periode = (EOPeriodesMilitaires) periodes
					.objectAtIndex(i);
			if (org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps
					.intersectionPeriodes(periodeInitiale.dateDebut(),
							periodeInitiale.dateFin(), periode.dateDebut(),
							periode.dateFin()) != null)
				return true;
			periodeInitiale = periode;
		}

		return false;
	}


	/**
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	private NSArray periodesTempsPleinPourDates(NSTimestamp dateDebut,
			NSTimestamp dateFin) {
		NSMutableArray periodes = new NSMutableArray();
		for (PeriodeAvecQuotite myPeriode : periodesAvecQuotite) {
			IntervalleTemps intervalle = IntervalleTemps.intersectionPeriodes(
					myPeriode.dateDebut(), myPeriode.dateFin(), dateDebut,
					dateFin);
			if (intervalle != null) {
				PeriodeTempsTravail newPeriode = new PeriodeTempsTravail(
						intervalle.debutPeriode(), intervalle.finPeriode(),
						myPeriode.quotite().doubleValue());
				periodes.addObject(newPeriode);
			}
		}

		return periodes;
	}

	/**
	 * 
	 * @param cpa
	 * @return
	 */
	private NSArray preparerPeriodesPourCpaAvecQuotiteDegressive(EOCessProgActivite cpa) {

		NSTimestamp dateFin80Pourcent = DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutAnnees(cpa.dateDebut(), 2));

		NSMutableArray periodes = new NSMutableArray();

		if (cpa.dateFin() == null || DateCtrl.isAfter(cpa.dateFin(), dateFin80Pourcent)) {
			if (DateCtrl.isBefore(dateFin80Pourcent, getDebutPeriodeDeclaration()))
				periodes.addObject(new PeriodeTempsTravail(cpa, cpa.dateDebut(), cpa.dateFin(), 60D));
			else if (DateCtrl.isBefore(dateFin80Pourcent, getFinPeriodeDeclaration())) {
				periodes.addObject(new PeriodeTempsTravail(cpa, cpa.dateDebut(), dateFin80Pourcent, 80D));
				periodes.addObject(new PeriodeTempsTravail(cpa, DateCtrl.jourSuivant(dateFin80Pourcent), cpa.dateFin(), 60D));
			} else {
				periodes.addObject(new PeriodeTempsTravail(cpa, cpa.dateDebut(), cpa.dateFin(), 80D));
			}
		} else {
			periodes.addObject(new PeriodeTempsTravail(cpa, cpa.dateDebut(), cpa.dateFin(), 80D));
		}
		return periodes;
	}

	/**
	 * 
	 * @param cpa
	 * @return
	 */
	private NSArray preparerPeriodesPourCpaAvecCessationTotale(
			EOCessProgActivite cpa) {
		NSArray cfas = EOCgFinActivite.rechercherDureesPourIndividuEtPeriode(
				cpa.editingContext(), "CgFinActivite", cpa.individu(),
				getDebutPeriodeDeclaration(), getFinPeriodeDeclaration());
		cfas = EOSortOrdering.sortedArrayUsingKeyOrderArray(
				cfas,
				new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",
						EOSortOrdering.CompareAscending)));
		NSTimestamp dateFin = getFinPeriodeDeclaration();
		NSTimestamp dateDebutCfa = null;
		for (Enumeration<EOCgFinActivite> e = cfas.objectEnumerator(); e
				.hasMoreElements();) {
			EOCgFinActivite cfa = e.nextElement();
			if (DateCtrl.isAfter(cfa.dateDebut(), getDebutPeriodeDeclaration())) {
				dateFin = DateCtrl.jourPrecedent(cfa.dateDebut());
				dateDebutCfa = cfa.dateDebut();
				break;
			}
		}

		NSMutableArray periodesCpa = new NSMutableArray();
		NSArray periodes = periodesTempsPleinPourDates(
				getDebutPeriodeDeclaration(), dateFin);
		PeriodeTempsTravail periode;
		for (Enumeration<PeriodeTempsTravail> e1 = periodes.objectEnumerator(); e1
				.hasMoreElements(); periodesCpa
				.addObject(new PeriodeTempsTravail(cpa, periode
						.dateDebutPourCir(), periode.dateFinPourCir(), periode
						.quotiteActivite())))
			periode = e1.nextElement();

		if (dateDebutCfa != null)
			periodesCpa.addObject(new PeriodeTempsTravail(cpa, dateDebutCfa,
					cpa.dateFin(), 0.0D));
		return periodesCpa;
	}

	/**
	 * 
	 * @param rubrique
	 * @param message
	 * @param niveauErreur
	 */
	private void ajouterErreurARubrique(RubriqueCir rubrique, String message, int niveauErreur) {

		getCurrentCirCarriere().setTemValide("N");
		if (getCurrentCirCarriere().circCommentaires() == null)
			getCurrentCirCarriere().setCircCommentaires(message);
		else {
			if ((getCurrentCirCarriere().circCommentaires() + "\n" + message).length() > 2000)
				getCurrentCirCarriere().setCircCommentaires(currentCirCarriere
						.circCommentaires() + "\n" + message);
		}

		if (estRapportDetaille || niveauErreur == 0)
			rubrique.ajouterException(message);
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	private String genererTexte(String data) {

		if (!data.startsWith("03$"))
			return (new StringBuilder(String.valueOf(data))).append("\n")
					.toString();
		String debString = data.substring(0, 6);
		NSTimestamp dateDebut = DateCtrl.stringToDate(data.substring(6, 14), "%Y%m%d");
		NSTimestamp dateFin = DateCtrl.stringToDate(data.substring(15, 23),	"%Y%m%d");
		String finString = data.substring(24);
		int anneeDebut = DateCtrl.getYear(dateDebut);
		int anneeFin = DateCtrl.getYear(dateFin);
		String texte = "";
		for (; anneeFin >= anneeDebut; anneeFin--) {
			NSTimestamp dateDeb = DateCtrl.debutAnnee(anneeFin);
			if (DateCtrl.isBefore(dateDeb, dateDebut))
				dateDeb = dateDebut;
			NSTimestamp dateF = DateCtrl.finAnnee(anneeFin);
			if (DateCtrl.isAfter(dateF, dateFin))
				dateF = dateFin;
			texte = texte + debString + DateCtrl.dateToString(dateDeb, "%Y%m%d") + "$";
			texte = texte + DateCtrl.dateToString(dateF, "%Y%m%d") + "$" + finString + "\n";
		}

		return texte;
	}

	/**
	 * 
	 * @param message
	 */
	private void informerThread(String message) {
		if (message != null) {
			if (threadCourant != null)
				threadCourant.setMessage(message);
			else
				LogManager.logInformation(message);
		}
	}

	/**
	 * 
	 * @param e
	 */
	private void signalerExceptionThread(Exception e) {
		if (threadCourant != null && e.getMessage() != null)
			threadCourant.setException(e.getMessage());
		LogManager.logException(e);
	}

	// Classe pour stocker les durées de position statutaire et permettant de
	// limiter la durée à la période de déclaration
	public class DureePourPositionStatutaire implements NSKeyValueCoding,
	IDuree {
		private NSTimestamp dateDebut;
		private NSTimestamp dateFin;
		private EOChangementPosition changement;
		private Duree duree;

		public DureePourPositionStatutaire(IDureePourIndividu duree) {
			dateDebut = dateDebutPourPeriodeDeclaration(duree.dateDebut());
			dateFin = dateFinPourPeriodeDeclaration(duree.dateFin());
			if (duree instanceof EOChangementPosition) {
				changement = (EOChangementPosition) duree;
			} else if (duree instanceof Duree) {
				this.duree = (Duree) duree;
			}
		}

		// Accesseurs
		public NSTimestamp dateDebut() {
			return dateDebut;
		}

		public NSTimestamp dateFin() {
			return dateFin;
		}

		public EOChangementPosition changement() {
			return changement;
		}

		public Duree duree() {
			return duree;
		}

		public boolean dureeAvecChangementPosition() {
			return changement != null;
		}

		// interface keyValueCoding
		public void takeValueForKey(Object valeur, String cle) {
			NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,
					valeur, cle);
		}

		public Object valueForKey(String cle) {
			return NSKeyValueCoding.DefaultImplementation
					.valueForKey(this, cle);
		}

		public String toString() {
			return "debut : " + DateCtrl.dateToString(dateDebut()) + ", fin :"
					+ DateCtrl.dateToString(dateFin()) + "\n cp : "
					+ changement + "\n duree : " + duree;
		}
	}


}

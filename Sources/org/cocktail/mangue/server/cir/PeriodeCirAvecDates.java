// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeCirAvecDates.java

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.server.cir:
//            AutomateCir

public class PeriodeCirAvecDates implements NSKeyValueCoding {

    private NSTimestamp dateDebut;
    private NSTimestamp dateFin;

    public PeriodeCirAvecDates(NSTimestamp dateDebut, NSTimestamp dateFin, boolean calculerDateDebut)  {
    	    	
        if(calculerDateDebut && DateCtrl.isBefore(dateDebut, AutomateCir.sharedInstance().getDebutPeriodeDeclaration()))
            this.dateDebut = AutomateCir.sharedInstance().getDebutPeriodeDeclaration();
        else
            this.dateDebut = dateDebut;
        if(dateFin == null || DateCtrl.isAfter(dateFin, AutomateCir.sharedInstance().getFinPeriodeDeclaration()))
            this.dateFin = AutomateCir.sharedInstance().getFinPeriodeDeclaration();
        else
            this.dateFin = dateFin;
        
    }

    /**
     * 
     */
    public NSTimestamp dateDebutPourCir() {
    	
    	if (DateCtrl.isBefore(dateFinPourCir(), dateDebut)) {
     		return null;
    	}

    	return dateDebut;
    }

    public void setDateDebutPourCir(NSTimestamp uneDate)
    {
        dateDebut = uneDate;
    }

    public NSTimestamp dateFinPourCir()
    {
        return dateFin;
    }

    public void setDateFinPourCir(NSTimestamp uneDate)
    {
        dateFin = uneDate;
    }

    public boolean estMemeType(PeriodeCirAvecDates periode) {
        return true;
    }

    public String toString()
    {
        return "date debut : " + DateCtrl.dateToString(dateDebutPourCir()) + ", date fin : " + DateCtrl.dateToString(dateFinPourCir());
    }

    public void takeValueForKey(Object valeur, String cle)
    {
        com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
    }

    public Object valueForKey(String cle)
    {
        return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
    }

    /**
     * 
     * @param periodes
     * @return
     */
    public static NSMutableArray regrouperPeriodes(NSMutableArray periodes) {
    	
        NSMutableArray nouvellesPeriodes = new NSMutableArray();
        PeriodeCirAvecDates periodeInitiale = (PeriodeCirAvecDates)periodes.objectAtIndex(0);
        
        for(int i = 1; i < periodes.count(); i++)
        {
            PeriodeCirAvecDates periode = (PeriodeCirAvecDates)periodes.objectAtIndex(i);
            if(periodeContigueAPeriode(periodeInitiale, periode) && periode.estMemeType(periodeInitiale)){
                periodeInitiale.setDateFinPourCir(periode.dateFinPourCir());
            } 
            else {
                nouvellesPeriodes.addObject(periodeInitiale);
                periodeInitiale = periode;
            }
        }

        nouvellesPeriodes.addObject(periodeInitiale);    	
    	return nouvellesPeriodes;
    }

    /**
     * 
     * @param periodeInitiale
     * @param periode
     * @return
     */
    private static boolean periodeContigueAPeriode(PeriodeCirAvecDates periodeInitiale, PeriodeCirAvecDates periode){
        return periodeInitiale.dateFinPourCir() != null && periode.dateDebutPourCir() != null && DateCtrl.dateToString(DateCtrl.jourSuivant(periodeInitiale.dateFinPourCir())).equals(DateCtrl.dateToString(periode.dateDebutPourCir()));
    }
   

    /**
     * 
     * @param periodes
     * @return
     */
    public static NSMutableArray<PeriodeAffectation> regrouperPeriodesAffectation(NSMutableArray<PeriodeAffectation> periodes) {
        NSMutableArray<PeriodeAffectation> nouvellesPeriodes = new NSMutableArray<PeriodeAffectation>();
        PeriodeAffectation periodeInitiale = (PeriodeAffectation)periodes.objectAtIndex(0);

        for(int i = 1; i < periodes.count(); i++) {
        	
        	PeriodeAffectation periode = (PeriodeAffectation)periodes.objectAtIndex(i);

            if(	periodeAffectationContigueAPeriode(periodeInitiale, periode) 
            		&& (periodeInitiale.affLibelle() != null && periodeInitiale.affLibelle() != null)
            		&& periodeInitiale.affLibelle().equals(periode.affLibelle()) ){
                periodeInitiale.setDateFinPourCir(periode.dateFinPourCir());
            } 
            else {
                nouvellesPeriodes.addObject(periodeInitiale);
                periodeInitiale = periode;
            }
        }

        nouvellesPeriodes.addObject(periodeInitiale);
        return nouvellesPeriodes;
    }

    /**
     * 
     * @param periodeInitiale
     * @param periode
     * @return
     */
    private static boolean periodeAffectationContigueAPeriode(PeriodeAffectation periodeInitiale, PeriodeAffectation periode){
        return periodeInitiale.dateFinPourCir() != null 
        		&& periode.dateDebutPourCir() != null 
        		&& DateCtrl.dateToString(DateCtrl.jourSuivant(periodeInitiale.dateFinPourCir())).equals(DateCtrl.dateToString(periode.dateDebutPourCir()));
    }

}
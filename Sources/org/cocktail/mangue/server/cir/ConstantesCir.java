// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ConstantesCir.java

package org.cocktail.mangue.server.cir;

public class ConstantesCir
{

    public ConstantesCir()
    {
    }

    public static final String POSITION_ACTIVITE = "ACI00";
    public static final String POSITION_DETACHEMENT = "DET00";
    public static final String POSITION_DETACHEMENT_ENTRANT = "ACI10";
    public static final String POSITION_MISE_A_DISPOSITION = "MAD10";
    public static final String POSITION_CFP = "CF001";
    public static final String POSITION_CFA = "CFA00";

    public static final String NORME_CIR = "V1.11";
    public static final String SOUS_RUBRIQUE_CONSTANTE = "constante";
    public static final String NOM_DIRECTORY_CIR = "CIR";
    public static final String NOM_DIRECTORY_ENVOI = "CIR_A_Envoyer";
    public static final String CODE_ARTICLE_ENTETE = "00";
    public static final String CODE_ARTICLE_FINAL = "99";
    public static final String DELIMITEUR_CIR = "$";
    public static final String DELIMITEUR_LIGNE = "\n";
    public static final String CODE_ARTICLE_IDENTITE_AGENT = "01";
    public static final String DEPT_ETRANGER = "099";
    public static final String CODE_PAYS_DEBUT = "99";
    public static final String dateFormat = "%Y%m%d";
    public static final String CORPS_INSTITUTEUR = "600";
    public static final String CATEGORIE_ACTIF = "A";
    public static final String CATEGORIE_SEDENTAIRE = "S";
    public static final String CATEGORIE_INSALUBRE = "I";
    public static final String TITULAIRE = "TITU1";
    public static final String STAGIAIRE = "STAGI";
    public static final String ELEVE = "ELEVE";
    public static final String MILITAIRE = "MIL40";
    public static final String CONTRACTUEL = "C0000";
    public static final String MODALITE_NON_PRECISEE = "MS000";
    public static final String TEMPS_PLEIN = "MS100";
    public static final String TEMPS_PARTIEL = "MS200";
    public static final String CESS_PROG_ACTIVITE = "MS300";
    public static final String TEMPS_INCOMPLET = "INCO";
    public static final String MTT_POUR_AT = "MS511";
    public static final String MTT_POUR_CONGE_MALADIE = "MS510";
    public static final int DUREE_CPA_QUOTITE_80 = 2;
    public static final double QUOTITE_CPA_80 = 80D;
    public static final double QUOTITE_CPA_60 = 60D;
    public static final String CONGE_GROSSESSE_PATHOLOGIQUE_ONP = "CG002";
    public static final String CONGE_ACCOUCHEMENT_PATHOLOGIQUE_ONP = "CG003";

}

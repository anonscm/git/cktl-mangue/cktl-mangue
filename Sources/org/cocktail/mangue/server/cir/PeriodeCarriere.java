// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeCarriere.java

package org.cocktail.mangue.server.cir;

import java.util.Enumeration;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOCompte;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonneTelephone;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodeHandicap;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationActivite;
import org.cocktail.mangue.modele.mangue.prolongations.EOReculAge;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class PeriodeCarriere extends PeriodeCirAvecDates {

	private String referenceTexteProlongation;
	private AgentCIR agentCir;
	
	public PeriodeCarriere(AgentCIR agent) {
		super(agent.getDebutPeriode(), agent.getFinPeriode(), false);
		setAgentCir(agent);
		referenceTexteProlongation = null;
	}

	public NSTimestamp dateRadiation() {
		return getAgentCir().getDateRadiationCadre();
	}
	public NSTimestamp dateCessationService() {
		return getAgentCir().getDateCessationService();
	}
	public String motifCessationService() {
		return getAgentCir().getMotifCessationService();
	}
	
    /**
     * 
     * @return
     */
    public Boolean enDetachementAccueil() {
        return new Boolean(getAgentCir().isDetachementEntrant());
    }
	
	public AgentCIR getAgentCir() {
		return agentCir;
	}



	public void setAgentCir(AgentCIR agentCir) {
		this.agentCir = agentCir;
	}



	public EOIndividu getIndividu()
	{
		return getAgentCir().getIndividu();
	}


	public NSTimestamp dateDebutDeclaration()
	{
		return dateDebutPourCir();
	}

	public NSTimestamp dateFinDeclaration()
	{
		return dateFinPourCir();
	}

	public String noFax()
	{
		NSArray faxs = EOPersonneTelephone.rechercherFaxProfessionnels(editingContext(), getIndividu());
		for(Enumeration e = faxs.objectEnumerator(); e.hasMoreElements();)
		{
			EOPersonneTelephone fax = (EOPersonneTelephone)e.nextElement();
			if(fax.estTelPrincipal())
				return fax.noTelephone();
		}

		if(faxs.count() > 0)
			return ((EOPersonneTelephone)faxs.objectAtIndex(0)).noTelephone();
		else
			return null;
	}

	public String noTel()
	{
		String noTel = noTelPourTelephones(EOPersonneTelephone.rechercherTelEtMobileProfessionnels(editingContext(), getIndividu()));
		if(noTel != null)
			return noTel;
		else
			return noTelPourTelephones(EOPersonneTelephone.rechercherTelEtMobilePersonnels(editingContext(), getIndividu()));
	}

	public EOAdresse adresseCourante(NSTimestamp dateValiditeMax)
	{
		NSArray rpas = EORepartPersonneAdresse.adressesPersoValides(editingContext(), getIndividu());
		for(Enumeration e = rpas.objectEnumerator(); e.hasMoreElements();)
		{
			EORepartPersonneAdresse rpa = (EORepartPersonneAdresse)e.nextElement();
			if(rpa.estAdressePrincipale() && (rpa.toAdresse().dDebVal() == null || DateCtrl.isBefore(rpa.toAdresse().dDebVal(), dateValiditeMax)))
				return rpa.toAdresse();
		}

		if(rpas.count() > 0)
		{
			for(Enumeration<EORepartPersonneAdresse> e = rpas.objectEnumerator(); e.hasMoreElements();) 	{
				EOAdresse adresse = e.nextElement().toAdresse();
				if(adresse.ville() != null && adresse.toPays() != null && (adresse.dDebVal() == null || DateCtrl.isBefore(adresse.dDebVal(), dateValiditeMax)))
					return adresse;
			}

			return ((EORepartPersonneAdresse)rpas.objectAtIndex(0)).toAdresse();
		} else
		{
			return null;
		}
	}

	public String adresseMail()
	{

		NSArray<EOCompte> comptes = EOCompte.rechercherPourIndividu(editingContext(), getIndividu());
		for(Enumeration e = comptes.objectEnumerator(); e.hasMoreElements();)
		{
			EOCompte compte = (EOCompte)e.nextElement();
			if(compte.estCompteEtablissement())
				return compte.email();
		}

		if(comptes.count() > 0)
			return ((EOCompte)comptes.objectAtIndex(0)).email();
		else
			return null;
	}

	public String finServicePourMilitaire() {
		return null;
	}


	/**
	 * Age personnel de depart a la retraite
	 * @return ageMiseOfficeRetraite + Reculs Age + Prolongations
	 */
	public String dateLimiteAgePersonnelle() {

		int nbJoursComptables = 0;

		NSArray<EOReculAge> reculsAge = EOReculAge.findForIndividu(editingContext(), getIndividu());
		for (EOReculAge myReculAge : reculsAge) {
			nbJoursComptables += DateCtrl.nbJoursEntre(myReculAge.dateDebut(),myReculAge.dateFin(), true, true);
			referenceTexteProlongation = myReculAge.motif().refMotProlongation();
		}

		NSArray<EOProlongationActivite> prolongations = EOProlongationActivite.findForIndividu(editingContext(), getIndividu(), dateDebutDeclaration(), dateFinDeclaration());
		for (EOProlongationActivite myProlongation : prolongations) {
			nbJoursComptables += DateCtrl.nbJoursEntre(myProlongation.dateDebut(),myProlongation.dateFin(), true, true);
			referenceTexteProlongation = myProlongation.motif().refMotProlongation();
		}

		// S'il n y a pas de prolongations d'activite on retourne NULL
		if (nbJoursComptables > 0) {

			String ageEmp = getIndividu().personnel().ageMiseOfficeRetraite();

			int anneesEmp = new Integer(ageEmp.substring(0,2)).intValue() + ( nbJoursComptables / 360 );
			int nbAnnees = nbJoursComptables / 360;
			nbJoursComptables -= (nbAnnees * 360);

			int moisEmp = new Integer(ageEmp.substring(2,4)).intValue() + ( nbJoursComptables/30 );
			int nbMois = nbJoursComptables/30;
			nbJoursComptables -= (nbMois * 30);

			// Retour sous le format AAMMJJ
			return StringCtrl.stringCompletion(String.valueOf(anneesEmp), 2, "0", "G")
				+ StringCtrl.stringCompletion(String.valueOf(moisEmp), 2, "0", "G")
				+ StringCtrl.stringCompletion(String.valueOf(nbJoursComptables), 2, "0", "G");
		}

		return null;
	}

	public String texteReglementairePourLimiteAge() {
		return referenceTexteProlongation;
	}

	public EOPeriodeHandicap periodeHandicapCourante() {
		NSArray periodes = EOPeriodeHandicap.rechercherDureesPourIndividuEtPeriode(editingContext(), "PeriodeHandicap", getIndividu(), AutomateCir.sharedInstance().getDebutPeriodeDeclaration(), AutomateCir.sharedInstance().getFinPeriodeDeclaration());
		if(periodes.count() > 0)
		{
			periodes = EOSortOrdering.sortedArrayUsingKeyOrderArray(periodes, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
			return (EOPeriodeHandicap)periodes.objectAtIndex(0);
		} else
		{
			return null;
		}
	}

	public NSTimestamp dateDecesIndividu() {
		if(getIndividu().dDeces() != null && DateCtrl.isBefore(getIndividu().dDeces(), AutomateCir.sharedInstance().getFinPeriodeDeclaration()))
			return null;
		else
			return getIndividu().dDeces();
	}

	public String lieuDecesIndividu() 	 {
		if(getIndividu().dDeces() != null && DateCtrl.isAfter(getIndividu().dDeces(), AutomateCir.sharedInstance().getFinPeriodeDeclaration()))
			return null;
		else
			return getIndividu().lieuDeces();
	}

	private EOEditingContext editingContext()	{
		return getIndividu().editingContext();
	}

	private String noTelPourTelephones(NSArray<EOPersonneTelephone> telephones)	{
		for(EOPersonneTelephone myTelephone : telephones) {
			if(myTelephone.estTelPrincipal())
				return myTelephone.noTelephone();
		}

		if(telephones.count() > 0)	{
			String noFixe = null;
			String noMobile = null;
			for(Enumeration e = telephones.objectEnumerator(); e.hasMoreElements();)
			{
				EOPersonneTelephone tel = (EOPersonneTelephone)e.nextElement();
				if(!tel.estSurListeRouge())
					if(tel.toTypeNo().estTelephoneFixe())
						noFixe = tel.noTelephone();
					else
						if(tel.toTypeNo().estTelephoneMobile())
							noMobile = tel.noTelephone();
			}

			if(noFixe != null)
				return noFixe;
			else
				return noMobile;
		} else
		{
			return null;
		}
	}

}

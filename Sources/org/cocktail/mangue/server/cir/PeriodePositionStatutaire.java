// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodePositionStatutaire.java

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

public class PeriodePositionStatutaire extends PeriodeCirAvecDates implements NSKeyValueCoding
{

	private EOChangementPosition changementPosition;
	private boolean estSiretFourni;
	private boolean detachementEntrantEnCours;
	private boolean estDetachementEntrant;
	String positionStatutaire;
	NSTimestamp dateDecision;

	public PeriodePositionStatutaire(AutomateCir.DureePourPositionStatutaire duree, NSTimestamp dateDebut, NSTimestamp dateFin, boolean detachementEntrantEnCours)	{
		super(dateDebut, dateFin, false);
		estSiretFourni = false;
		changementPosition = duree.changement();

		this.detachementEntrantEnCours = detachementEntrantEnCours;
		preparerPositionStatutaire(duree);

		estDetachementEntrant = false;
	}

	public PeriodePositionStatutaire(EOContratAvenant avenant)	{
		super(avenant.dateDebut(), avenant.dateFin(), false);
		estSiretFourni = false;

		this.detachementEntrantEnCours = true;
		positionStatutaire = ConstantesCir.POSITION_DETACHEMENT_ENTRANT;

		estDetachementEntrant = false;
	}

	public void setEstDetachementEntrant(boolean aBool)
	{
		estDetachementEntrant = aBool;
		if(estDetachementEntrant)
			positionStatutaire = ConstantesCir.POSITION_DETACHEMENT_ENTRANT;
	}

	public EOChangementPosition changementPosition()
	{
		return changementPosition;
	}

	public String positionCir() {
		return positionStatutaire;
	}

	public NSTimestamp dateDecision()
	{
		return dateDecision;
	}

	public String sirenEtablissementDetachement() throws Exception {
		if(changementPosition != null && changementPosition.estDetachementSortant()) {
			if(changementPosition.toRne() != null) {
				if(changementPosition.toRne().siret() != null)
					return changementPosition.toRne().siret();
			} else
				return null;
		} else
			return null;

		return "";
	}

	public String lieuDetachement() {
		try {
			if (sirenEtablissementDetachement() == null) {

				if(changementPosition != null && changementPosition.estDetachementSortant() && !estSiretFourni)
					return changementPosition.lieuPosition();
			}

			return null;
		}
		catch (Exception e) {
			return null;
		}

	}


	/**
	 * 
	 * @return
	 */
	public String pensionDetachement() {

		if (changementPosition != null && changementPosition.estDetachementSortant()) {
			return (changementPosition.estPcAcquitee()?"1":"0");
		}

		return "0";

	}

	/**
	 * 
	 */
	public boolean estMemeType(PeriodeCirAvecDates periode)  {

		PeriodePositionStatutaire periodeStatutaire = (PeriodePositionStatutaire)periode;

		if (periodeStatutaire == null || positionCir() == null)
			return false;

		if(!(periode instanceof PeriodePositionStatutaire))
			return false;

		if(periodeStatutaire != null && periodeStatutaire.positionCir() != null
				&& positionCir().equals(periodeStatutaire.positionCir()))
			return changementPosition == null || !changementPosition.estDetachementSortant();
		else
			return false;
	}

	public void takeValueForKey(Object valeur, String cle) {
		com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
	}
	public Object valueForKey(String cle) {
		return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
	}

	/**
	 * 
	 * @param duree
	 */
	private void preparerPositionStatutaire(AutomateCir.DureePourPositionStatutaire duree)    {

			dateDecision = changementPosition().dateArrete();
			if(dateDecision != null && DateCtrl.isAfter(dateDecision, new NSTimestamp()))
				dateDecision = null;

			positionStatutaire = changementPosition.toPosition().codeOnp();

			if(changementPosition().estDetachementEntrant()) {
				if (detachementEntrantEnCours)
					positionStatutaire = ConstantesCir.POSITION_DETACHEMENT_ENTRANT;
				else
					positionStatutaire = ConstantesCir.POSITION_DETACHEMENT;

			}
	}

}

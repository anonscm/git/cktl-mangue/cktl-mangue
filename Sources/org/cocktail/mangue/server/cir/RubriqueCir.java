/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (papaye@cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.server.cir;

import java.util.Enumeration;

import org.cocktail.common.LogManager;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author christine
 *
 * Contient toutes les informations utiles pour la g&eacute;n&eacute;ration des rubriques du Cir
 */
public class RubriqueCir implements NSKeyValueCoding {
	private String nom;
	private int priorite;
	private String exceptions;
	private boolean estObligatoire,aComparer,estAnnuelle;
	private NSMutableArray sousRubriques;		// tableau de sous-rubriques Cir
	
	// constructeur
	/**
	 * @param nom
	 * @param priorite - ordre dans lequel la rubrique est generee
	 * @param estObligatoire true si la rubrique doit toujours etre generee
	 * @parm aComparer true si il faut comparer les donnees de la rubrique avec ce qui est dans la base
	 * @param estAnnuelle true si seules les donnees de l'annee sont transmises
	 */
	public RubriqueCir(String nom, int priorite,boolean estObligatoire,boolean aComparer, boolean estAnnuelle) {
		super();
		this.nom = nom;
		this.priorite = priorite;
		this.estObligatoire = estObligatoire;
		this.aComparer = aComparer;
		this.estAnnuelle = estAnnuelle;
		sousRubriques = new NSMutableArray();
		exceptions = "";
	}
	/**
	 * @return Retourne la priorite de la rubrique
	 */
	public int priorite() {
		return priorite;
	}
	
	/**
	 * @return retourne true si une rubrique est obligatoire
	 */
	public boolean estObligatoire() {
		return estObligatoire;
	}

	/**
	 * @return retourne true si une rubrique est a comparer avec une rubrique generee dans un envoi anterieur
	 */
	public boolean aComparer() {
		return aComparer;
	}
	/**
	 * @return retourne true si on ne declare que les donnees de l'annee
	 */
	public boolean estAnnuelle() {
		return estAnnuelle;
	}
	/**
	 * @return Retourne le nom de la rubrique
	 */
	public String nom() {
		return nom;
	}
	/** Ajoute un message d'erreur a la liste des exceptions */
	public void ajouterException(String message) {
		if (exceptions.length() > 0) {
			exceptions += "\n";
		}
		exceptions += message;
	}
	/** Purge la liste des exceptions */
	public void resetExceptions() {
		exceptions = "";
	}
	/**
	 * @return Retourne le nom de la methode pour g&eacute;n&eacute;rer la rubrique : generer + nomRubrique (ex : genererIdentiteAgent)
	 * La methode doit etre une methode publique de AutomateCir
	 */
	public String nomMethodePourGenerationRubrique() {
		return "generer" + nom();
	}
	/**
	 * @return Retourne la liste des sous-rubriques the sousRubriques.
	 */
	public NSArray sousRubriques() {
		return sousRubriques;
	}
	/**
	 * Ajoute une sous-rubrique a la liste des sous-rubriques
	 * @param sousRubrique sous-rubrique à ajouter
	 */
	public void ajouterSousRubrique(SousRubriqueCir sousRubrique) {
		sousRubriques.addObject(sousRubrique);
	}
	public SousRubriqueCir sousRubriqueAvecCle(String cle) {
		for (java.util.Enumeration<SousRubriqueCir> e = sousRubriques.objectEnumerator();e.hasMoreElements();) {
			SousRubriqueCir sousRubrique = (SousRubriqueCir)e.nextElement();
			if (sousRubrique.cle().equals(cle)) {
				return sousRubrique;
			}
		}
		return null;
	}
	public String toString() {
		return shortString() + "\nSous-rubriques :\n" + sousRubriques();
	}
	/**
	 * Pre une rubrique i.e toutes les sous-rubriques de celle-ci
	 * @param parametres parametres fournis pour l'evaluation de la rubrique
	 * @param classes	classes des parametres
	 * @return	une string avec les valeurs separees par des dollars
	 */
	public String preparer(Object parametres[],Object classes[]) throws Exception {
	
		// on retournera une chaîne de caractères qui est la concaténation de l'évaluation de toutes les rubriques
		String exceptionsExec = "";
		String resultat = "";

		Enumeration<SousRubriqueCir> e = sousRubriques().objectEnumerator();
		while (e.hasMoreElements()) {
			
			SousRubriqueCir sousRubrique = e.nextElement();
						
			try {
				SousRubriqueCirAvecValeur resultatSousRubrique = sousRubrique.preparer(parametres,classes);
								
				if (resultatSousRubrique != null)
					resultat += resultatSousRubrique.valeur();

			} catch (Exception e1) {
				if (e1.getMessage() == null) {	// exception suite à une erreur
					e1.printStackTrace();
					throw e1;
				} else {
					//e1.printStackTrace();
					exceptionsExec = exceptionsExec + "\nPour la sous-rubrique :" + sousRubrique.cle() + " " + e1.getMessage();
				}
			}
			resultat += ConstantesCir.DELIMITEUR_CIR;
		}
	
		exceptions += exceptionsExec;
		// pour l'instant, on se contente d'afficher les exceptions, il faudra ensuite les générer
		if (exceptions.equals("") == false) {
			LogManager.logInformation("liste des exceptions\n" + exceptions);
		}
		// supprimer le dernier paramètre et vérifier que la chaîne n'est pas non vide dans le cas d'une rubrique obligatoire
		if (estObligatoire() && resultat.length() == 0) {
			throw new Exception("Pas de donnees evaluees pour la rubrique " + nom() + " alors qu'elle est obligatoire");
		}
		
		// On supprime le $ en fin de ligne
		resultat = resultat.substring( 0, (resultat.length() -1));

		return resultat;
	}
	/**
	 * @return retourne la liste des exceptions generees pendant la preparation de la rubrique
	 */
	public String getExceptions() {
		return exceptions;
	}
	/**
	 * Retourne une version resumee de la rubrique
	 * @return
	 */
	public String shortString() {
		String texte = "nom : " + nom + " priorite : " + priorite;
		if (estObligatoire) {
			texte = texte + ", obligatoire";
		}
		return texte;
	}
	// NSKeyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ParserCir.java

package org.cocktail.mangue.server.cir;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.xerces.jaxp.SAXParserFactoryImpl;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.ParserAdapter;

// Referenced classes of package org.cocktail.mangue.server.cir:
//            ListeRubriquesCir, AnalyseurXMLPourCir

public class ParserCir
{

    public ParserCir()
    {
    }

    public static ListeRubriquesCir parseXML(String url)
    {
        ListeRubriquesCir listeRubriques = new ListeRubriquesCir();
        AnalyseurXMLPourCir analyseur = new AnalyseurXMLPourCir(listeRubriques);
        try
        {
            SAXParserFactoryImpl parserFactory = new SAXParserFactoryImpl();
            ParserAdapter parseur = null;
            try
            {
                parseur = new ParserAdapter(parserFactory.newSAXParser().getParser());
            }
            catch(ParserConfigurationException e2)
            {
                e2.printStackTrace();
            }
            parseur.setContentHandler(analyseur);
            try
            {
                parseur.parse(url);
            }
            catch(IOException e1)
            {
                e1.printStackTrace();
            }
        }
        catch(SAXException e)
        {
            e.printStackTrace();
        }
        return listeRubriques;
    }
}

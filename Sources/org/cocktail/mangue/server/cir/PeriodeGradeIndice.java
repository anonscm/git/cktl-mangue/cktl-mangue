// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeConge.java

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.server.cir:
//            PeriodeCirAvecDates

public class PeriodeGradeIndice implements NSKeyValueCoding {

	NSTimestamp debutPeriode, finPeriode;
	String indiceBrut, indiceMajore, inmEffectif;
	EOContratAvenant avenant;
	EOElementCarriere element;
	EOGrade grade;
	
    public PeriodeGradeIndice(EOElementCarriere element) {
    	
    	setElement(element);
    	
    	setDebutPeriode(element.dateDebut());
    	setFinPeriode(element.dateFin());
    	setIndiceBrut(element.indiceBrut());
    	
    	if (element.indiceMajore() != null)
    		setIndiceMajore(element.indiceMajore().toString());
    	
    	if (element.inmEffectif() != null) {
    		setInmEffectif(element.inmEffectif().toString());    	
    	}
    	
    	setGrade(element.toGrade());
    }
    public PeriodeGradeIndice(EOContratAvenant avenant) {
    	setDebutPeriode(avenant.dateDebut());
    	setFinPeriode(avenant.dateFin());
    	
    	setIndiceMajore(avenant.indiceContrat());
    	setIndiceBrut(avenant.indiceBrut());

    	setGrade(avenant.toGrade());
    }

	public NSTimestamp debutPeriode() {
		return debutPeriode;
	}
	public void setDebutPeriode(NSTimestamp debutPeriode) {
		this.debutPeriode = debutPeriode;
	}

	public NSTimestamp finPeriode() {
		return finPeriode;
	}

	public void setFinPeriode(NSTimestamp finPeriode) {
		this.finPeriode = finPeriode;
	}    
    public String getIndiceBrut() {
		return indiceBrut;
	}
	public void setIndiceBrut(String indiceBrut) {
		this.indiceBrut = indiceBrut;
	}

	public String getIndiceMajore() {
		return indiceMajore;
	}

	public void setIndiceMajore(String indiceMajore) {
		this.indiceMajore = indiceMajore;
	}

	public NSTimestamp getDebutPeriode() {
		return debutPeriode;
	}

	public NSTimestamp getFinPeriode() {
		return finPeriode;
	}

	public EOGrade getGrade() {
		return grade;
	}
	public void setGrade(EOGrade grade) {
		this.grade = grade;
	}
	public String getInmEffectif() {
		return inmEffectif;
	}
	public void setInmEffectif(String inmEffectif) {
		this.inmEffectif = inmEffectif;
	}

	public EOContratAvenant getAvenant() {
		return avenant;
	}
	public void setAvenant(EOContratAvenant avenant) {
		this.avenant = avenant;
	}
	public EOElementCarriere getElement() {
		return element;
	}
	public void setElement(EOElementCarriere element) {
		this.element = element;
	}
	public void takeValueForKey(Object valeur, String cle)
    {
        com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
    }

    public Object valueForKey(String cle)
    {
        return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
    }

}

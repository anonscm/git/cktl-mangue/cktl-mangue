// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   RecordSpen.java

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgramme;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction;
import org.cocktail.mangue.server.VersionMe;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

public class RecordSpen implements NSKeyValueCoding {

    private static final String REMONTEE_PERIODIQUE = " RP";
    private static final String REMONTEE_INDIVIDUELLE = " RI";
    private boolean estCampagne;
    private NSTimestamp dateCertification;
    private NSTimestamp dateExtraction;
    private NSTimestamp dateCompletude;
    private NSTimestamp dateReference;
    private String uaiEtablissement;
    
    private EOIndividu currentIndividu;
    private IEmploi currentEmploi;

    public RecordSpen(EOStructure etablissement, NSTimestamp dateExtraction, boolean estCampagne) {
    	
        this.dateExtraction = dateExtraction;
        this.estCampagne = estCampagne;
        if(etablissement != null)
            uaiEtablissement = etablissement.rne().code();
    }

    /**
     * 
     * @param individu
     */
    public void changerIndividu(EOIndividu individu) {

    	currentIndividu = individu;
    	currentEmploi = null;

//    	NSArray occupations = EOOccupation.rechercherOccupationsADate(currentIndividu.editingContext(), currentIndividu, new NSTimestamp());
//		if (occupations.count() > 0) {
//			EOOccupation occupation = (EOOccupation)occupations.objectAtIndex(0);    	
//			currentEmploi = occupation.toEmploi();
//		}

    	// L'agent est il en detachement entrant
    	boolean estDetachementEntrant = false;
    	NSArray<EOChangementPosition> positions = EOChangementPosition.rechercherChangementsEnActivitePourIndividuEtPeriode(currentIndividu.editingContext(), currentIndividu, dateExtraction(), dateExtraction());
    	for (EOChangementPosition myPosition : positions) {
    		if (myPosition.estDetachementEntrant())
    			estDetachementEntrant = true;
    	}
    	if (estDetachementEntrant == false)
    		dateCompletude = currentIndividu.personnel().cirDCompletude();
    	else
    		dateCompletude = null;
		
    }

    public NSTimestamp dateExtraction()    {
        return dateExtraction;
    }

    public NSTimestamp dateCompletude(){
        return dateCompletude;
    }
    public NSTimestamp dateReference()
    {
        return dateReference;
    }

    public String discipline()
    {
        return null;
    }

    public String categEmploiMen()
    {
        return null;
    }

    public String nomGestionnaire()
    {
    	return VersionMe.appliVersion();
    }

    public String mailGestionnaire()
    {
        return null;
    }

    public String telGestionnaire()
    {
        return null;
    }

    public String bureauGestionnaire()
    {
        return null;
    }

    public String melBureauGestionnaire()
    {
        return null;
    }

    public String categorieEmploiLolf() {
   		return null;
    }

    public String programmeLolf() {    	
    	if (currentEmploi != null && currentEmploi.getToProgramme() != null) {
    		return currentEmploi.getToProgramme().cProgramme();
    	}
    	
        return EOProgramme.DEFAULT_PROGRAMME;
    }

    public String actionLolf() {
    	
    	NSArray actions = EOPersBudgetAction.listeActionsParIndividu(currentIndividu.editingContext(), currentIndividu);
    	if (actions.count() > 0)
    		return ((EOPersBudgetAction)actions.objectAtIndex(0)).lolfNomenclatureDepense().lolfCode();

    	return null;
    }

    public String uaiEtablissement() {
        return uaiEtablissement;
    }

    public String categorieRecord() {
        if(estCampagne)
            return REMONTEE_PERIODIQUE;
        else
            return REMONTEE_INDIVIDUELLE;
    }

    public void takeValueForKey(Object valeur, String cle) {
        com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
    }

    public Object valueForKey(String cle) {
        return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
    }

}

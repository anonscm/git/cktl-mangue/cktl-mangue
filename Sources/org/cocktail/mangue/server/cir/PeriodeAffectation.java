// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeAffectation.java

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.server.cir:
//            PeriodeCirAvecDates

public class PeriodeAffectation extends PeriodeCirAvecDates
{

	private String affLibelle;
    private String lieu;
    private EORne rne;
    private EOStructure structure;

    public PeriodeAffectation(NSTimestamp dateDebut, NSTimestamp dateFin, String affLibelle)
    {
        super(dateDebut, dateFin, true);
        this.affLibelle = affLibelle;
    }

    public String toString() {
        if(rne() != null)
            return "DEBUT : " + DateCtrl.dateToString(dateDebutPourCir()) + " , FIN : " + DateCtrl.dateToString(dateFinPourCir()) + " RNE : " + rne.libelleCourt();
        else
            return "DEBUT : " + DateCtrl.dateToString(dateDebutPourCir()) + " , FIN : " + DateCtrl.dateToString(dateFinPourCir()) + " RNE NULL ! ";
    }

    public EORne rne() {
        return rne;
    }

    public EOStructure structure() {
        return structure;
    }
    
    public String lieu() {
        return lieu;
    }

    public String affLibelle() {
    	return this.affLibelle;
    }
    
    public String affLibelleCir() {
    	return StringCtrl.stringCompletion(affLibelle(), 25, "", "G");
    }

  }
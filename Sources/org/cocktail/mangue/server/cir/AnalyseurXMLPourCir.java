// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   AnalyseurXMLPourCir.java

package org.cocktail.mangue.server.cir;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

// Referenced classes of package org.cocktail.mangue.server.cir:
//            RubriqueCir, ListeRubriquesCir, SousRubriqueCir

public class AnalyseurXMLPourCir extends DefaultHandler
{

    public AnalyseurXMLPourCir(ListeRubriquesCir listeRubriques)
    {
        this.listeRubriques = listeRubriques;
        litElement = false;
    }

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
    {
        attributLu = null;
        litElement = true;
        attributEnCours = "";
        if(localName.equals("Rubrique"))
        {
            sousRubriqueCourante = null;
            nom = atts.getValue("nom");
            String temp = atts.getValue("priorite");
            priorite = (new Integer(temp)).intValue();
            aComparer = atts.getValue("comparer") != null && atts.getValue("comparer").equals("oui");
            estObligatoire = atts.getValue("obligatoire") != null && atts.getValue("obligatoire").equals("oui");
            estAnnuelle = atts.getValue("annuelle") != null && atts.getValue("annuelle").equals("oui");
            rubriqueCourante = new RubriqueCir(nom, priorite, estObligatoire, aComparer, estAnnuelle);
            listeRubriques.ajouterRubrique(nom, rubriqueCourante);
            numeroSousRubrique = 0;
        } else
        if(localName.equals("SousRubrique"))
        {
            cle = atts.getValue("cle");
            aComparer = false;
            estObligatoire = atts.getValue("obligatoire") != null && atts.getValue("obligatoire").equals("oui");
            aLongueurFixe = atts.getValue("fixe") != null && atts.getValue("fixe").equals("oui");
            longueur = (new Integer(atts.getValue("longueur"))).intValue();
            numeroSousRubrique++;
            sousRubriqueCourante = new SousRubriqueCir(numeroSousRubrique, cle, estObligatoire, aLongueurFixe, longueur);
            rubriqueCourante.ajouterSousRubrique(sousRubriqueCourante);
        } else
        if(!localName.equals("Description"))
            attributLu = localName;
    }

    public void characters(char ch[], int start, int length)
        throws SAXException
    {
        if(litElement && attributLu != null)
            attributEnCours = attributEnCours.concat(new String(ch, start, length));
    }

    public void endElement(String namespaceURI, String localName, String qName)
        throws SAXException
    {
        litElement = false;
        if(attributLu.equals("valeur"))
            sousRubriqueCourante.setValeur(attributEnCours);
        else
        if(attributLu.equals("type"))
            sousRubriqueCourante.setTypeDonnee(attributEnCours);
        else
        if(attributLu.equals("nature"))
            sousRubriqueCourante.setNature(attributEnCours);
        else
        if(attributLu.equals("classe"))
            sousRubriqueCourante.setClasseAssociee(attributEnCours);
        else
        if(attributLu.equals("aide"))
            sousRubriqueCourante.setAide(attributEnCours);
    }

    private RubriqueCir rubriqueCourante;
    private SousRubriqueCir sousRubriqueCourante;
    private String nom;
    private String cle;
    private int priorite;
    private String attributLu;
    private String attributEnCours;
    private int longueur;
    private boolean estObligatoire;
    private boolean aLongueurFixe;
    private boolean aComparer;
    private boolean estAnnuelle;
    private boolean litElement;
    private ListeRubriquesCir listeRubriques;
    private int numeroSousRubrique;
}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeServiceAuxiliaire.java

package org.cocktail.mangue.server.cir;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriereData;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;
import org.cocktail.mangue.modele.mangue.individu.EOValidationServices;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

public class PeriodeServiceAuxiliaire implements NSKeyValueCoding {

	private NSTimestamp dateDebut;
	private NSTimestamp dateFin;
	private NSTimestamp dateValidation;
	private String 		typeTempsTravail;
	private String 		siretEtablissement;
	private String 		libelleEtablissement;
	private String 		dureeService;
	private String 		dureeAssurance;
	private BigDecimal  quotiteService;
	private BigDecimal  quotiteCotisation;
	private boolean 	cotisationsAcquittees;
	private Integer		annees;
	private Integer		mois;
	private Integer		jours;

	private EOPasse 				passe;
	private EOValidationServices 	validationService;

	/**
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @param quotite
	 */
	public PeriodeServiceAuxiliaire() {

	}

	/**
	 * 
	 * @param passe
	 */
	public PeriodeServiceAuxiliaire(EOPasse passe) {
		setPasse(passe);
	}
	/**
	 * 
	 * @param passe
	 */
	public PeriodeServiceAuxiliaire(EOValidationServices validation) {
		setValidationService(validation);
	}
	/**
	 * 
	 * @return
	 */
	public EOValidationServices getValidationService() {
		return validationService;
	}
	/**
	 * 
	 * @return
	 */
	public EOPasse getPasse() {
		return passe;
	}
	public Integer getAnnees() {
		return annees;
	}

	public void setAnnees(Integer annees) {
		this.annees = annees;
		if (annees == null)
			this.annees = new Integer(0);
	}

	public Integer getMois() {
		return mois;
	}

	public void setMois(Integer mois) {
		this.mois = mois;
		if (mois == null)
			this.mois = new Integer(0);
	}

	public Integer getJours() {
		return jours;
	}

	public void setJours(Integer jours) {
		this.jours = jours;
		if (jours == null)
			this.jours = new Integer(0);
	}

	/**
	 * 
	 * @param validation
	 */
	public void setValidationService(EOValidationServices validation) {

		this.validationService = validation;

		if (validation != null) {

			setDateDebut(validation.dateDebut());
			setDateFin(validation.dateFin());
			setDateValidation(validation.dateValidation());
			setCotisationsAcquittees(validation.isPcAcquitees());

			if(validation.estTempsIncomplet())
				setTypeTempsTravail("");
			else
				if(validation.estTempsComplet())
					setTypeTempsTravail(ConstantesCir.TEMPS_PLEIN);
				else
					if(validation.estTempsPartiel())
						setTypeTempsTravail(ConstantesCir.TEMPS_PARTIEL);

			if(validation.valQuotite() != null)
				setQuotiteCotisation(validation.valQuotite());
			else
				setQuotiteCotisation(new BigDecimal("-1"));

			// Duree de service
			setAnnees(validation.valAnnees());
			setMois(validation.valMois());
			setJours(validation.valJours());

			calculerDureeService();

			if (validation.valMinistere() != null)
				setLibelleEtablissement(validation.valMinistere());
			else {
				if (validation.valEtablissement() != null) {
					if (validation.valEtablissement().length() > 25)
						setLibelleEtablissement(validation.valEtablissement().substring(0,25));
					else
						setLibelleEtablissement(validation.valEtablissement());
				}
			}

		}

	}

	/**
	 * 
	 * @param passe
	 */
	public void setPasse(EOPasse passe) {
		this.passe = passe;
		if (passe != null) {

			setDateDebut(passe.dateDebut());
			setDateFin(passe.dateFin());
			setDateValidation(passe.dValidationService());
			setCotisationsAcquittees(passe.estPcAquitees());

			if(passe.estTempsIncomplet())
				setTypeTempsTravail("");
			else
				if(passe.estTempsComplet())
					setTypeTempsTravail(ConstantesCir.TEMPS_PLEIN);
				else
					if(passe.estTempsPartiel())
						setTypeTempsTravail(ConstantesCir.TEMPS_PARTIEL);

			if(passe.pasQuotiteCotisation() != null)
				setQuotiteCotisation(passe.pasQuotiteCotisation());
			else
				setQuotiteCotisation(new BigDecimal("-1"));

			// Duree de service
			setAnnees(passe.dureeValideeAnnees());
			setMois(passe.dureeValideeMois());
			setJours(passe.dureeValideeJours());

			calculerDureeService();

			if (passe.toMinistere() != null) {
				setLibelleEtablissement(passe.toMinistere().libelleCourt());				
			}
			else
				if (passe.pasMinistere() != null)
					setLibelleEtablissement(passe.pasMinistere());
				else {
					if (passe.etablissementPasse() != null) {
						if (passe.etablissementPasse().length() > 25)
							setLibelleEtablissement(passe.etablissementPasse().substring(0,25));
						else
							setLibelleEtablissement(passe.etablissementPasse());
					}
				}
		}
	}

	public NSTimestamp getDateDebut() {
		return dateDebut;
	}


	public void setDateDebut(NSTimestamp dateDebut) {
		this.dateDebut = dateDebut;
	}


	public NSTimestamp getDateFin() {
		return dateFin;
	}


	public void setDateFin(NSTimestamp dateFin) {
		this.dateFin = dateFin;
	}


	public NSTimestamp getDateValidation() {
		return dateValidation;
	}


	public void setDateValidation(NSTimestamp dateValidation) {
		this.dateValidation = dateValidation;
	}


	public String getTypeTempsTravail() {
		return typeTempsTravail;
	}


	public void setTypeTempsTravail(String typeTempsTravail) {
		this.typeTempsTravail = typeTempsTravail;
	}


	public String getSiretEtablissement() {
		return siretEtablissement;
	}


	public void setSiretEtablissement(String siretEtablissement) {
		this.siretEtablissement = siretEtablissement;
	}


	public String getLibelleEtablissement() {
		return libelleEtablissement;
	}

	public void setLibelleEtablissement(String libelleEtablissement) {
		this.libelleEtablissement = libelleEtablissement;
	}

	public String getDureeService() {
		if( getQuotiteService() != null && getQuotiteService().floatValue() != 100.00)
			return null;
		else
			return dureeService;
	}


	/**
	 * 
	 * @param annees
	 * @param mois
	 * @param jours
	 */
	public void calculerDureeService() {

		if (getAnnees() == 0 && getMois() == 0 && getJours() == 0)
			dureeService = null;

		dureeService = EOCirCarriereData.formaterDuree(getAnnees(), getMois(), getJours());

	}

	/**
	 * 
	 * @return
	 */
	public String getDureeAssurance() {
		return dureeAssurance;
	}


	public void setDureeAssurance(String dureeAssurance) {
		this.dureeAssurance = dureeAssurance;
	}

	public BigDecimal getQuotiteService() {
		return quotiteService;
	}

	public void setQuotiteService(BigDecimal quotiteService) {
		this.quotiteService = quotiteService;
	}

	public BigDecimal getQuotiteCotisation() {
		return quotiteCotisation;
	}

	public void setQuotiteCotisation(BigDecimal quotiteCotisation) {
		this.quotiteCotisation = quotiteCotisation;
	}

	/**
	 * 
	 * @return
	 */
	public String getTauxActiviteFormatte() {

		if (quotiteService != null && quotiteService.floatValue()	>= 0.0)
			return EOCirCarriereData.formaterQuotite(quotiteService) + EOCirCarriereData.formaterQuotite(ManGUEConstantes.QUOTITE_100);
		else {
			return getTauxCotisationFormatte();
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getTauxCotisationFormatte() {

		if (quotiteCotisation != null && quotiteCotisation.floatValue() >= 0.0)
			return EOCirCarriereData.formaterQuotite(quotiteCotisation)+EOCirCarriereData.formaterQuotite(ManGUEConstantes.QUOTITE_100);

		return null;
	}



	public boolean isCotisationsAcquittees() {
		return cotisationsAcquittees;
	}

	public void setCotisationsAcquittees(boolean cotisationsAcquittees) {
		this.cotisationsAcquittees = cotisationsAcquittees;
	}

	/**
	 * 
	 * @return
	 */
	public Boolean valideSelonReglementation2004() {
		return (getDateValidation() != null && DateCtrl.isBeforeEq(getDateValidation(), DateCtrl.debutAnnee(new Integer(2004))));		
	}

	public void takeValueForKey(Object valeur, String cle)
	{
		com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
	}

	public Object valueForKey(String cle)
	{
		return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
	}


}

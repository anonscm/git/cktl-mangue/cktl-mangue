// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeTempsTravail.java

package org.cocktail.mangue.server.cir;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriereData;
import org.cocktail.mangue.modele.mangue.conges.EOCld;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAccidentServ;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaladie;
import org.cocktail.mangue.modele.mangue.modalites.EOCessProgActivite;
import org.cocktail.mangue.modele.mangue.modalites.EOCongeSsTraitement;
import org.cocktail.mangue.modele.mangue.modalites.EOMiTpsTherap;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.server.cir:
//            PeriodeCirAvecDates

public class PeriodeTempsTravail extends PeriodeCirAvecDates {

	private final NSTimestamp DATE_DEBUT_VALIDITE_MTT = DateCtrl.stringToDate("07/02/2007");
	private final NSTimestamp DATE_CHANGEMENT_VALIDITE_MTT = DateCtrl.stringToDate("31/03/2010");

	private String typeTempsTravail;
	private double numQuotiteActivite;
	private double denQuotiteActivite;
	private double quotiteCotisation;
	private boolean surcotisePourRetraite;
	private boolean estTempsPartielAvecEnfant;
	private EOEnfant enfant;

	/**
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @param quotite
	 */
	public PeriodeTempsTravail(NSTimestamp dateDebut, NSTimestamp dateFin, double quotite) {
		
		super(dateDebut, dateFin, true);

		numQuotiteActivite = quotite;
		denQuotiteActivite = 100;
		quotiteCotisation = numQuotiteActivite;
		if(quotite == 0)
			typeTempsTravail = ConstantesCir.MODALITE_NON_PRECISEE;
		else
			if( quotite != 100.00 )
				typeTempsTravail = ConstantesCir.TEMPS_INCOMPLET;
			else
				typeTempsTravail = ConstantesCir.TEMPS_PLEIN;

		estTempsPartielAvecEnfant = false;
		enfant = null;
		surcotisePourRetraite = false;
	}

	/**
	 * 
	 * @param duree
	 * @param dateDebutReelle
	 * @param dateFinReelle
	 * @param quotiteActivite
	 */
	public PeriodeTempsTravail(Duree duree, NSTimestamp dateDebutReelle, NSTimestamp dateFinReelle, double quotiteActivite) {
		super(dateDebutReelle, dateFinReelle, true);
		preparerAvecDuree(duree, quotiteActivite);
	}

	public double quotiteActivite() {
		return numQuotiteActivite;
	}

	public String typeTempsTravail() {

		//		if (typeTempsTravail.equals("INCO"))
		//			return null;

		return typeTempsTravail;
	}

	public boolean estTempsPartielAvecEnfant()
	{
		return estTempsPartielAvecEnfant;
	}

	public EOEnfant enfant()
	{
		return enfant;
	}

	public String tauxActivite()
	{
		return EOCirCarriereData.formaterQuotite(new BigDecimal(numQuotiteActivite)) + EOCirCarriereData.formaterQuotite(new BigDecimal(denQuotiteActivite));
	}

	public String tauxCotisation()
	{
		return EOCirCarriereData.formaterQuotite(new BigDecimal(quotiteCotisation)) + EOCirCarriereData.formaterQuotite(ManGUEConstantes.QUOTITE_100);
	}

	public Boolean surcotisePourRetraite()
	{
		return new Boolean(surcotisePourRetraite);
	}

	public boolean estMemeType(PeriodeCirAvecDates periode) {

		PeriodeTempsTravail periodeT = (PeriodeTempsTravail)periode;

		if (typeTempsTravail() == null || periodeT == null)
			return false;

		if(!(periode instanceof PeriodeTempsTravail))
			return false;

		return typeTempsTravail().equals(periodeT.typeTempsTravail()) && quotiteActivite() == periodeT.quotiteActivite() && enfant() == periodeT.enfant() && surcotisePourRetraite().equals(periodeT.surcotisePourRetraite());
	}

	/**
	 * 
	 * @param duree
	 * @param quotiteActivite
	 */
	private void preparerAvecDuree(Duree duree, double quotiteActivite) {

		estTempsPartielAvecEnfant = false;
		enfant = null;
		if(duree instanceof EOTempsPartiel) {
			EOTempsPartiel tp = (EOTempsPartiel)duree;
			numQuotiteActivite = tp.quotite().doubleValue();
			denQuotiteActivite = new Double(100);
			typeTempsTravail = tp.motif().codeOnp();
			surcotisePourRetraite = tp.surcotisePourRetraite();
			if(surcotisePourRetraite)
				quotiteCotisation = 100;
			else
				quotiteCotisation = numQuotiteActivite;
			estTempsPartielAvecEnfant = tp.motif().estPourEleverEnfant() || tp.motif().estPourEleverEnfantAdopte();
			enfant = tp.enfant();
		} else
			if(duree instanceof EOMiTpsTherap) {
				EOMiTpsTherap mtt = (EOMiTpsTherap)duree;

				Duree conge = mtt.congePourMTT(true, false);			
				if(conge instanceof EOCongeAccidentServ) {

					if (DateCtrl.isBeforeEq(duree.dateDebut(), DATE_DEBUT_VALIDITE_MTT))
						typeTempsTravail = "MS206";
					else
						if (DateCtrl.isBeforeEq(duree.dateDebut(), DATE_CHANGEMENT_VALIDITE_MTT))
							typeTempsTravail = "MS211";
						else
							typeTempsTravail = "MS511";
				}
				else
					if((conge instanceof EOCongeMaladie) || (conge instanceof EOCld) || (conge instanceof EOClm)) {
						if (DateCtrl.isBeforeEq(duree.dateDebut(), DATE_DEBUT_VALIDITE_MTT))
							typeTempsTravail = "MS205";
						else
							if (DateCtrl.isBeforeEq(duree.dateDebut(), DATE_CHANGEMENT_VALIDITE_MTT))
								typeTempsTravail = "MS210";
							else
								typeTempsTravail = "MS510";						
					}

				numQuotiteActivite = mtt.quotite().doubleValue();
				denQuotiteActivite = 100;
				quotiteCotisation = 100;
			} else
				if(duree instanceof EOCessProgActivite) {
					EOCessProgActivite cpa = (EOCessProgActivite)duree;
					typeTempsTravail = "MS300";
					numQuotiteActivite = quotiteActivite;
					denQuotiteActivite = 100D;
					surcotisePourRetraite = cpa.surcotiseRetraite();
					if(surcotisePourRetraite)
						quotiteCotisation = 100D;
					else
						quotiteCotisation = numQuotiteActivite;
				}
				else 
					if (duree instanceof EOCongeSsTraitement) {
						EOCongeSsTraitement conge = (EOCongeSsTraitement)duree;
						typeTempsTravail = conge.motif().cCir();
						numQuotiteActivite = new Double(0);
						denQuotiteActivite = 100D;
						
						// Tester la surcotisation Retraite
						quotiteCotisation = 100D;
					}
					else {
						numQuotiteActivite = quotiteActivite;
						denQuotiteActivite = 100;
						quotiteCotisation = 100;
					}

	}
}

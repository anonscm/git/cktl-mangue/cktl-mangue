// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeConge.java

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.modele.IEvenementAvecEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaternite;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSKeyValueCoding;

// Referenced classes of package org.cocktail.mangue.server.cir:
//            PeriodeCirAvecDates

public class PeriodeConge extends PeriodeCirAvecDates
    implements NSKeyValueCoding
{

    public PeriodeConge(EOAbsences absence, EOGenericRecord conge)
    {
        super(absence.dateDebut(), absence.dateFin(), true);
        this.absence = absence;
        congeAssocie = conge;
    }

    public EOGenericRecord congeAssocie()
    {
        return congeAssocie;
    }

    public String typeAbsenceCir()
    {
        String type = absence.toTypeAbsence().codeOnp();
        if(congeAssocie() != null && (congeAssocie() instanceof EOCongeMaternite))
        {
            EOCongeMaternite congeMaternite = (EOCongeMaternite)congeAssocie();
            if(congeMaternite.estTypePathologieGrossesse())
                type = "CG002";
            else
            if(congeMaternite.estTypePathologieAccouchement())
                type = "CG003";
        }
        return type;
    }

    public String dureeAbsenceCir()
    {
        return null;
    }

    public EOEnfant enfant()
    {
        if(congeAssocie() != null && (congeAssocie() instanceof IEvenementAvecEnfant))
            return ((IEvenementAvecEnfant)congeAssocie()).enfant();
        else
            return null;
    }

    public boolean estMemeType(PeriodeCirAvecDates periode)
    {
        if(!(periode instanceof PeriodeConge))
            return false;
        else
            return typeAbsenceCir().equals(((PeriodeConge)periode).typeAbsenceCir());
    }

    public void takeValueForKey(Object valeur, String cle)
    {
        com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
    }

    public Object valueForKey(String cle)
    {
        return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
    }

    private EOAbsences absence;
    private EOGenericRecord congeAssocie;
}

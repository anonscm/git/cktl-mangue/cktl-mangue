package org.cocktail.mangue.server.cir;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

public class PeriodeStatut extends PeriodeCirAvecDates implements NSKeyValueCoding {

    private String typeCategorie;
    private String statut;

    /**
     * 
     * @param dateDebut
     * @param dateFin
     * @param statut
     * @param typeCategorieService
     * @param calculerDateDebut
     */
    public PeriodeStatut(NSTimestamp dateDebut, NSTimestamp dateFin, String statut, String typeCategorieService, boolean calculerDateDebut) {
        
    	super(dateDebut, dateFin, calculerDateDebut);
        this.statut = statut;

        typeCategorie=typeCategorieService;

    }

    public String statut() {
        return statut;
    }

    public String categorieService() {
        return typeCategorie;
    }

    public boolean estMemeType(PeriodeCirAvecDates periode) {
        if(!(periode instanceof PeriodeStatut))
            return false;
        PeriodeStatut periodeStatut = (PeriodeStatut)periode;
        return statut().equals(periodeStatut.statut()) && categorieService().equals(periodeStatut.categorieService());
    }

    public String toString() {
        return (new StringBuilder(String.valueOf(super.toString()))).append(", statut : ").append(statut()).append(", categorie service : ").append(categorieService()).toString();
    }

    public void takeValueForKey(Object valeur, String cle) {
        com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
    }

    public Object valueForKey(String cle) {
        return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
    }

}

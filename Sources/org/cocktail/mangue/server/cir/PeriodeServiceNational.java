// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   PeriodeConge.java

package org.cocktail.mangue.server.cir;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.server.cir:
//            PeriodeCirAvecDates

public class PeriodeServiceNational implements NSKeyValueCoding {

	String typePeriode = "";
	NSTimestamp debutPeriode, finPeriode;
	
    public PeriodeServiceNational(NSTimestamp debut, NSTimestamp fin, String type) {
    	setDebutPeriode(debut);
    	setFinPeriode(fin);
    	setTypePeriode(type);
    }

    public String typePeriode() {
		return typePeriode;
	}
	public void setTypePeriode(String typePeriode) {
		this.typePeriode = typePeriode;
	}
	public NSTimestamp debutPeriode() {
		return debutPeriode;
	}
	public void setDebutPeriode(NSTimestamp debutPeriode) {
		this.debutPeriode = debutPeriode;
	}

	public NSTimestamp finPeriode() {
		return finPeriode;
	}

	public void setFinPeriode(NSTimestamp finPeriode) {
		this.finPeriode = finPeriode;
	}    
	public String dureePeriode() {
		return null;
	}

    public void takeValueForKey(Object valeur, String cle)
    {
        com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
    }

    public Object valueForKey(String cle)
    {
        return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
    }

}

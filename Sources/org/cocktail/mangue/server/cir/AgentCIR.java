// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   EnvironnementCir.java

package org.cocktail.mangue.server.cir;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class AgentCIR
{
	EOIndividu individu;
	
	NSArray<EOChangementPosition> changementsPositions;
	NSArray<EOContratAvenant> avenants;
	
	NSTimestamp debutPeriode;
	NSTimestamp finPeriode;
	
	boolean detachementEntrant;
	boolean lastPeriode;
	boolean contractuel;
	
	private String 			motifCessationService;
	private NSTimestamp 	dateCessationService;
	private NSTimestamp 	dateRadiationCadre;

    public AgentCIR(EOIndividu individu) {
    	setIndividu(individu);
    }
    public AgentCIR(EOIndividu individu, NSTimestamp debut, NSTimestamp fin, boolean detachementEntrant) {
    	setIndividu(individu);
    	setDebutPeriode(debut);
    	setFinPeriode(fin);
    	setDetachementEntrant(detachementEntrant);
    }
	public EOIndividu getIndividu() {
		return individu;
	}

	public boolean isContractuel() {
		return contractuel;
	}
	public void setContractuel(boolean contractuel) {
		this.contractuel = contractuel;
	}
	public boolean isLastPeriode() {
		return lastPeriode;
	}
	public void setLastPeriode(boolean lastPeriode) {
		this.lastPeriode = lastPeriode;
	}
	public void setIndividu(EOIndividu individu) {
		this.individu = individu;
	}

	public NSTimestamp getDebutPeriode() {
		return debutPeriode;
	}
	
	public String getMotifCessationService() {
		return motifCessationService;
	}
	public void setMotifCessationService(String motifCessationService) {
		this.motifCessationService = motifCessationService;
	}
	
	public NSTimestamp getDateCessationService() {
		return dateCessationService;
	}
	public void setDateCessationService(NSTimestamp dateCessationService) {
		this.dateCessationService = dateCessationService;
	}

	
	
	
	public NSTimestamp getDateRadiationCadre() {
		return dateRadiationCadre;
	}
	public void setDateRadiationCadre(NSTimestamp dateRadiationCadre) {
		this.dateRadiationCadre = dateRadiationCadre;
	}
	public String getDebutPeriodeFormatte() {
		return DateCtrl.dateToString(getDebutPeriode());
	}
	public String getFinPeriodeFormatte() {
		if (getFinPeriode() != null)
			return DateCtrl.dateToString(getFinPeriode());
		return "";
	}

	public void setDebutPeriode(NSTimestamp debutPeriode) {
		this.debutPeriode = debutPeriode;
	}

	public NSTimestamp getFinPeriode() {
		return finPeriode;
	}

	public void setFinPeriode(NSTimestamp finPeriode) {
		this.finPeriode = finPeriode;
	}

	public boolean isDetachementEntrant() {
		return detachementEntrant;
	}

	public void setDetachementEntrant(boolean detachementEntrant) {
		this.detachementEntrant = detachementEntrant;
	}
	public NSArray<EOChangementPosition> getChangementsPositions() {
		return changementsPositions;
	}
	public void setChangementsPositions(
			NSArray<EOChangementPosition> changementsPositions) {
		this.changementsPositions = changementsPositions;
	}
	public NSArray<EOContratAvenant> getAvenants() {
		if (avenants == null)
			return new NSArray<EOContratAvenant>();
		return avenants;
	}
	public void setAvenants(NSArray<EOContratAvenant> avenants) {
		this.avenants = avenants;
	}

    

}

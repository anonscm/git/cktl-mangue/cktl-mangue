// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SousRubriqueCirAvecValeur.java

package org.cocktail.mangue.server.cir;

import com.webobjects.foundation.NSKeyValueCoding;

public class SousRubriqueCirAvecValeur
    implements NSKeyValueCoding
{

    public SousRubriqueCirAvecValeur(String cle, String valeur)
    {
        this.cle = cle;
        this.valeur = valeur;
    }

    public String cle()
    {
        return cle;
    }

    public String valeur()
    {
        return valeur;
    }

    public void takeValueForKey(Object valeur, String cle)
    {
        com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
    }

    public Object valueForKey(String cle)
    {
        return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
    }

    private String cle;
    private String valeur;
}

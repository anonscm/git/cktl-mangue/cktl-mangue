// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   EnvironnementCir.java

package org.cocktail.mangue.server.cir;

import java.io.File;

public class EnvironnementCir
{

	private static EnvironnementCir sharedInstance;
    private String listeErreurs;
    private boolean estInitialise;

    public EnvironnementCir()
    {
    }

    public static EnvironnementCir sharedInstance()
    {
        if(sharedInstance == null)
            sharedInstance = new EnvironnementCir();
        return sharedInstance;
    }

    public void init()
    {
        listeErreurs = "";
        estInitialise = true;
    }

    public boolean estInitialise() {
        return estInitialise;
    }

    public String listeErreurs()
    {
        return listeErreurs;
    }

    public void ajouterErreur(String texte)
    {
        listeErreurs = String.valueOf(listeErreurs) + texte + "\n";
    }

    public void ajouterErreurUnique(String texte)
    {
        if(listeErreurs.indexOf(texte) < 0)
            ajouterErreur(texte);
    }

    private void verifierPathEtCreer(String unPath)
    {
        File file = new File(unPath);
        if(!file.exists())
            file.mkdirs();
    }

}

// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SupInfoCtrl.java

package org.cocktail.mangue.server.dif;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EODif;
import org.cocktail.mangue.modele.mangue.individu.EODifDetail;
import org.cocktail.mangue.server.AutomateAvecThread;
import org.cocktail.mangue.server.ServerThreadManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AutomateDif extends AutomateAvecThread {

	private Integer anneeCalcul;

	public AutomateDif(EOEditingContext edc) {
		super(edc);

	}

	private static AutomateDif sharedInstance;

	public static AutomateDif sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null)
			sharedInstance = new AutomateDif(edc);
		return sharedInstance;
	}

	public Integer getAnneeCalcul() {
		return anneeCalcul;
	}

	public void setAnneeCalcul(Integer anneeCalcul) {
		this.anneeCalcul = anneeCalcul;
	}

	/**
	 * 
	 * @param fichier
	 * @param rne
	 * @param thread
	 * @throws Exception
	 */
	public void preparerDifsPourAnnee(
			Integer annee,
			ServerThreadManager thread) throws Exception {

		try {

			setThreadCourant(thread);
			setAnneeCalcul(annee);

			informerThread(">> Préparation des données DIF pour " + getAnneeCalcul() + " ...");

			NSArray<EOIndividu> individus = EODif.rechercherIndividusAptesPourDif(getEdc(), getAnneeCalcul(), 2, 2);

			NSMutableArray<EOIndividu> individusTraites = new NSMutableArray<EOIndividu>();
			for (EOIndividu individu : individus) {
				if (!individusTraites.contains(individu)) {
					individusTraites.addObject(individu);
				}
			}

			int index = 1;
			for (EOIndividu individu : individusTraites) {
				informerThread("\t" + index + "/"+individusTraites.size()+" > " + individu.identitePrenomFirst());
				toutRecalculerPourIndividuEtAnnee(individu, getAnneeCalcul());
				index++;
			}

			// Suppression des DIFS à 0
			informerThread(">> Suppression des DIFs avec un credit annuel de 0");
			NSArray<EODif> difsASupprimer = EODif.findDifsASupprimer(getEdc());
			for (EODif dif : difsASupprimer) {
				NSArray<EODifDetail> details = EODifDetail.findForDif(getEdc(), dif);
				for (EODifDetail detail : details) {
					getEdc().deleteObject(detail);
				}
				getEdc().deleteObject(dif);
			}
			getEdc().saveChanges();
			
			informerThread(">> TERMINE !!!!");

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param fichier
	 * @param rne
	 * @param thread
	 * @throws Exception
	 */
	public void preparerDifsPourAnneeEtIndividus(
			Integer annee,
			NSArray<EOIndividu> individus,
			ServerThreadManager thread) throws Exception {

		try {

			setThreadCourant(thread);
			setAnneeCalcul(annee);

			informerThread(">> Préparation du DIF pour l'année " + getAnneeCalcul() + " ...");

			int index = 1;
			for (EOIndividu individu : individus) {
				informerThread("\t" + index + "/"+individus.size()+" > " + individu.identitePrenomFirst());
				toutRecalculerPourIndividuEtAnnee(individu, getAnneeCalcul());
				index++;
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void toutRecalculerPourIndividuEtAnnee(EOIndividu individu, Integer annee) {

		try {

			NSMutableArray<EODif> difs = new NSMutableArray<EODif>(EODif.findForIndividu(getEdc(), individu));
			EOSortOrdering.sortArrayUsingKeyOrderArray(difs, EODif.SORT_ARRAY_DATE_DEBUT);

			int debits = 0;
			for (EODif dif : difs) {

				NSArray<EODifDetail> details = EODifDetail.findForDif(getEdc(), dif);
				debits += CocktailUtilities.computeSumIntegerForKey(details, EODifDetail.DIF_DETAIL_DEBIT_KEY);

				dif.setDifDebitCumule(debits);

			}

			int premiereAnnee = 2007;
			int derniereAnnee = getAnneeCalcul();

			NSArray<EOCarriere> carrieres = EOCarriere.findForIndividu(getEdc(), individu);
			if (carrieres.size() > 0) {
				EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
				premiereAnnee = DateCtrl.getYear(carrieres.get(0).dateDebut()) + 1;
			}
			else {
				NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividu(getEdc(), individu, false);
				EOSortOrdering.sortedArrayUsingKeyOrderArray(contrats, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
				if (contrats.size() > 0) {
					premiereAnnee = DateCtrl.getYear(contrats.get(0).dateDebut()) + 1;
				}
			}

			if (premiereAnnee > 1900) {

				if (premiereAnnee < ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF)
					premiereAnnee = ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF;

				for (int i=premiereAnnee;i<=derniereAnnee;i++) {
					EODif dif = EODif.findForAnneeEtAgent(getEdc(), individu, new Integer(i));
					if (dif == null) {
						EODif newDif = EODif.creer(getEdc(), individu);
						newDif.setAnneeCalcul(i);
						newDif.setDateDebut(DateCtrl.debutAnnee(i-1));
						newDif.setDateFin(DateCtrl.finAnnee(i-1));
						newDif.calculerCredit();
					}
				}

				getEdc().saveChanges();

				difs = new NSMutableArray<EODif>(EODif.findForIndividu(getEdc(),individu));
				EOSortOrdering.sortArrayUsingKeyOrderArray(difs, EODif.SORT_ARRAY_DATE_DEBUT);
				for (EODif dif : difs) {
					dif.calculerDebit();
					dif.calculerCredit();
				}

				getEdc().saveChanges();
			}

		} catch (ValidationException ex)	{
			informerThread("ERREUR DE CALCUL : " + ex.getMessage());
			return;
		} catch (Exception e)	{
			informerThread("ERREUR DE CALCUL : " + e.getMessage());
			e.printStackTrace();
		}

	}


}
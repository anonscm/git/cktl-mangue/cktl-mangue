// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SupInfoCtrl.java

package org.cocktail.mangue.server.supinfo;

import java.math.BigDecimal;

import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOSupInfoData;
import org.cocktail.mangue.modele.mangue.EOSupInfoFichier;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.server.AutomateAvecThread;
import org.cocktail.mangue.server.ServerThreadManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class AutomateSupinfo extends AutomateAvecThread {

	private EOSupInfoFichier fichierSupInfo;

	public AutomateSupinfo(EOEditingContext edc) {
		super(edc);
		// TODO Auto-generated constructor stub
		setRneEtablissement(EOStructure.rechercherEtablissement(getEdc()).rne());
	}

	private static AutomateSupinfo sharedInstance;
	private EORne rneEtablissement;

	public EORne getRneEtablissement() {
		return rneEtablissement;
	}

	public void setRneEtablissement(EORne rneEtablissement) {
		this.rneEtablissement = rneEtablissement;
	}

	public static AutomateSupinfo sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null)
			sharedInstance = new AutomateSupinfo(edc);
		return sharedInstance;
	}

	public EOSupInfoFichier getFichierSupInfo() {
		return fichierSupInfo;
	}

	public void setFichierSupInfo(EOSupInfoFichier fichierSupInfo) {
		this.fichierSupInfo = fichierSupInfo;
	}

	/**
	 * 
	 * @param fichier
	 * @param rne
	 * @param thread
	 * @throws Exception
	 */
	public void preparerRemonteeSupInfo(
			EOSupInfoFichier fichier,
			ServerThreadManager thread) throws Exception {

		try {
		
		setThreadCourant(thread);
		setFichierSupInfo(fichier);

		informerThread(">> Préparation des données SUPINFO ...");
		informerThread(">> Suppression des anciennes données ...");

		supprimerDonnees(getFichierSupInfo(), null);

		if (getFichierSupInfo().estRemonteeSupinfo()) {
			informerThread(">> Préparation des données SUPINFO TITULAIRES...");
			preparerRemonteeSupInfoTitulaires(getFichierSupInfo());
		}
		else
			if (getFichierSupInfo().estRemonteeCrct()) {
				informerThread(">> Préparation des données SUPINFO CRCT...");
				preparerRemonteeSupInfoCrct(getFichierSupInfo());
			}
			else
				if (getFichierSupInfo().estRemonteeAnt()) {
					informerThread(">> Préparation des données SUPINFO ANT...");
					preparerRemonteeSupInfoAnt(getFichierSupInfo());
				}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Preparation des donnees SUPINFO pour un individu (individu != null) ou tous les agents  (individu = null) (Pour une annee ET une date donnee).
	 * 
	 * @param edc
	 * @param anneeReference
	 * @param dateReference
	 * @param individu
	 * @param rne
	 * @return
	 * @throws Exception
	 */
	private void preparerRemonteeSupInfoTitulaires(EOSupInfoFichier fichier) throws Exception {

		setEdc(fichier.editingContext());
		int nbComptes = 1;

		try {

			// On prend la date de debut de periode de reference au premier janvier de l'annee en cours
			NSTimestamp debutAnnee = DateCtrl.debutAnnee(fichier.supfAnnee());

			NSArray<EOCarriere> carrieres = EOCarriere.findForPeriode(getEdc(), null, fichier.supfDateObs(), fichier.supfDateObs());

			NSArray<EOIndividu> individus = (NSArray<EOIndividu>)carrieres.valueForKey(EOCarriere.TO_INDIVIDU_KEY);
			NSMutableArray<EOIndividu> individusGeres = new NSMutableArray<EOIndividu>();

			
			for(EOIndividu myIndividu : individus) {
								
				if(!individusGeres.contains(myIndividu)) {

					informerThread("\t" + nbComptes + "/"+individus.size()+" > " + myIndividu.identitePrenomFirst());

					NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsActivitePourPeriode(myIndividu.editingContext(), myIndividu, debutAnnee, fichier.supfDateObs());

					if (changements.size() > 0) {
						if(preparerRecordSupInfoPourIndividu(getEdc(), fichier, myIndividu, changements.get(0))) {
							nbComptes++;
							individusGeres.addObject(myIndividu);
						}
					}
				}
			}

			informerThread(nbComptes + " Comptes créés !");
			getEdc().saveChanges();

		}
		catch(Exception ex) {
			getEdc().revert();
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Suppression des donnees supinfo existantes.
	 * Si individu == null, suppression de toutes les donnees de l'annee et date de reference renseignees
	 * Sinon suppression des donnees de l'individu seulement
	 * 
	 * @param ec
	 * @param fichier
	 * @param individu
	 * @throws Exception
	 */
	private void supprimerDonnees(EOSupInfoFichier fichier, EOIndividu individu) throws Exception {

		if (individu == null) {

			NSArray<EOSupInfoData> datas = EOSupInfoData.findForFichier(getEdc(), fichier);
			for(EOSupInfoData myData : datas) {
				getEdc().deleteObject(myData);
			}

		}
		else {
			EOSupInfoData recordSupInfo = EOSupInfoData.findForFichierAndIndividu(getEdc(), fichier, individu);
			if (recordSupInfo != null)
				getEdc().deleteObject(recordSupInfo);
		}

		getEdc().saveChanges();
	}


	/**
	 * 
	 * Preparation d'un record SUP_INFO (ELECTRA_FICHIER) pour un individu donne
	 * 
	 * @param edc
	 * @param record
	 * @param individu
	 * @param sexe
	 * @param changement
	 * @param elementsCarriere
	 * @param anneeReference
	 * @param dateReference
	 * @return
	 */
	private boolean preparerRecordSupInfoPourIndividu(EOEditingContext edc, EOSupInfoFichier fichier, 
			EOIndividu individu, EOChangementPosition changement) {

		NSTimestamp debutAnnee = DateCtrl.debutAnnee(fichier.supfAnnee());

		NSArray<EOElementCarriere> elementsCarriere = EOElementCarriere.findForPeriode(edc, individu, debutAnnee, fichier.supfDateObs());
		NSArray<EOChangementPosition>  changements = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(edc, individu, null, null);
		EOElementCarriere elementConcerne = elementSupInfoValide(elementsCarriere, fichier.supfDateObs());

		// L'agent doit avoir au moins un element de carriere valide
		if (elementConcerne == null)
			return false;

		// Creation et initialisation du record
		EOSupInfoData record = EOSupInfoData.creer(edc, fichier);
		record.initIndividu(individu);
		record.initUAI(getRneEtablissement());
		record.initChangementPosition(changement);
		record.initElement(elementConcerne);
		record.initSpecialisation(elementConcerne.toCarriere());
		record.initConges();
		record.initModalites();
		record.initDiplomes();
		record.initAnciennete(elementConcerne);

		// On recherche la date d'entree dans le corps dans les elements de carriere			
		EOTypeAcces accesCorps = getTypeAccesCorps(edc, elementConcerne.toIndividu(), elementConcerne.toCorps());
		if (accesCorps != null && accesCorps.code().length() <= 2)
			record.setEficCTypeAccesCorps(accesCorps.code());

		// On recherche la date d'entree dans le grade dans les elements de carriere
		EOTypeAcces accesGrade = getTypeAccesGrade(edc, elementConcerne.toIndividu(), elementConcerne.toGrade());
		if (accesGrade != null && accesGrade.code().length() <= 2)
			record.setEficCTypeAccesGrade(accesGrade.code());

		// La date d'arrivee est la premiere date d'affectation dans l'établissement en tant que titulaire / stagiaire
		for (EOChangementPosition myChangement : changements) {

			if (myChangement.toRne() == null && ! myChangement.toPosition().estUnDetachement()) {
				record.setEficDEffetArrivee(myChangement.dateDebut());
			}
			else {
				if (myChangement.toRne() != null && rneEtablissement != null) {
					if (myChangement.toRne().code().equals(rneEtablissement.code())) {
						record.setEficDEffetArrivee(myChangement.dateDebut());
						break;
					}
				}
			}
		}
		if (record.eficDEffetArrivee() == null) {
			NSArray<EOAffectation> affectations = EOAffectation.findForIndividu(edc, individu, null);
			EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
			for (EOAffectation myAffectation : affectations) {
				if ( EOCarriere.rechercherCarrieresADate(edc, individu, myAffectation.dateDebut()).size() > 0 )
					record.setEficDEffetArrivee(myAffectation.dateDebut());
				break;
			}
		}

		record.validerRecord();

		return true;

	}


	/**
	 * 
	 * Preparation d'une demande de CRCT pour un individu donne
	 * 
	 * @param editingContext
	 * @param anneeReference
	 * @param individu
	 * @return
	 * @throws Exception
	 */
	private void preparerRemonteeSupInfoCrct(EOSupInfoFichier fichier) throws Exception {

		try {

			setEdc(fichier.editingContext());

			NSTimestamp debutAnnee = DateCtrl.debutAnnee(fichier.supfAnnee());

			// Recuperation de tous les agentes ENSEIGNANT_CHERCHEUR
			NSArray<EOCarriere> carrieres = EOCarriere.fetchForDateAndType(getEdc(), 
					EOTypePopulation.fetchForCode(getEdc(), EOTypePopulation.ENSEIGNANT_CHERCHEUR), 
					fichier.supfDateObs());

			int nbComptes = 1;
			for (EOCarriere myCarriere : carrieres) {

				EOIndividu myIndividu = myCarriere.toIndividu();

				informerThread("\t" + nbComptes + "/"+carrieres.size()+" > " + myIndividu.identitePrenomFirst());

				NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsActivitePourPeriode(myIndividu.editingContext(), myIndividu, debutAnnee, fichier.supfDateObs());

				if (changements.size() > 0) {
					if(preparerRecordSupInfoPourIndividu(getEdc(), fichier, myIndividu, changements.get(0))) {
						nbComptes++;
					}
					else {
						informerThread("\t\t Non Valide !");						
					}
				}
			}

			informerThread(nbComptes + " Comptes créés !");
			getEdc().saveChanges();

			informerThread(nbComptes + " Mise à jour des TEMOINS CRCT ...");

			NSArray<EOSupInfoData> datas = EOSupInfoData.findForFichier(getEdc(), fichier);
			for (EOSupInfoData data : datas) {
				// L'agent est il deja en CRCT ?
				NSArray<EOCrct> crcts = EOCrct.rechercherPourIndividuEtPeriode(getEdc(), data.toIndividu(), DateCtrl.dateAvecAjoutAnnees(fichier.supfDateObs(), -6), fichier.supfDateObs());
				if (crcts.count() > 0) {
					data.setEficTemCrct(CocktailConstantes.VRAI);
				}
			}
			getEdc().saveChanges();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Preparation des donnees SUPINFO pour un individu (individu != null) ou tous les agents  (individu = null) (Pour une annee ET une date donnee).
	 * 
	 * @param ec
	 * @param anneeReference
	 * @param dateReference
	 * @param individu
	 * @param rne
	 * @return
	 * @throws Exception
	 */
	private void preparerRemonteeSupInfoAnt(EOSupInfoFichier fichier) throws Exception {

		int nbComptes = 0;
		setEdc(fichier.editingContext());

		try {

			// On prend la date de debut de periode de reference au premier janvier de l'annee en cours
			NSTimestamp debutAnnee = DateCtrl.debutAnnee(fichier.supfAnnee());
			NSTimestamp finAnnee = DateCtrl.finAnnee(fichier.supfAnnee());

			// On recherche tous les agents ayant eu un contrat sur l'annee
			NSArray<EOIndividu> individus = (NSArray<EOIndividu>)(EOContrat.findForPeriode(getEdc(), debutAnnee, finAnnee)).valueForKey(EOContrat.TO_INDIVIDU_KEY);

			int index = 0;
			NSMutableArray<EOIndividu> individusGeres = new NSMutableArray<EOIndividu>();
			for(EOIndividu myIndividu : individus) {

				index ++;
				if(!individusGeres.contains(myIndividu)) {

					informerThread("\t" + index + "/"+individus.size()+" > " + myIndividu.identitePrenomFirst());

					NSArray<EOContratAvenant> avenants = EOContratAvenant.rechercherAvenantsRemunerationPrincipalePourIndividuEtDates(getEdc(), myIndividu, debutAnnee, finAnnee);

					if(avenants != null) {
						for (EOContratAvenant myAvenant : avenants) {
							preparerRecordSupInfoAntPourIndividu(getEdc(), fichier, myIndividu, myAvenant, fichier.supfAnnee());
						}
						individusGeres.addObject(myIndividu);
						nbComptes++;
					} else  {
						informerThread("  \t\t > Pas d'avenant");
					}

				}
			}
			getEdc().saveChanges();
			informerThread(nbComptes + " Comptes créés !");
		}
		catch(Exception ex) {
			getEdc().revert();
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 * Preparation d'un record SUP_INFO (ELECTRA_FICHIER) pour un individu donne
	 * 
	 * @param ec
	 * @param record
	 * @param individu
	 * @param sexe
	 * @param changement
	 * @param elementsCarriere
	 * @param anneeReference
	 * @param dateReference
	 * @return
	 */
	private boolean preparerRecordSupInfoAntPourIndividu(EOEditingContext ec, EOSupInfoFichier fichier, 
			EOIndividu individu, EOContratAvenant avenant, Integer anneeReference) {

		EOSupInfoData record = EOSupInfoData.creer(ec, fichier);

		record.initIndividu(individu);
		record.initUAI(getRneEtablissement());
		record.initDiplomes();
		record.initAvenant(avenant);
		record.initSpecialisation(avenant);

		return true;
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param corps
	 * @return
	 */
	private static EOTypeAcces getTypeAccesCorps(EOEditingContext ec, EOIndividu individu, EOCorps corps) {

		try {
			NSArray<EOElementCarriere> elements = EOElementCarriere.findforIndividuAndCorps(ec, individu, corps);
			return elements.get(0).toTypeAcces();
		}
		catch (Exception e) {
			return null;
		}
	}
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param grade
	 * @return
	 */
	private static EOTypeAcces getTypeAccesGrade(EOEditingContext ec, EOIndividu individu, EOGrade grade) {

		try {
			NSArray<EOElementCarriere> elements = EOElementCarriere.rechercherElementsPourIndividuEtGrade(ec, individu, grade);
			return elements.get(0).toTypeAcces();
		}
		catch (Exception e) {
			return null;
		}
	}


	/**
	 * 
	 * @param elementsCarriere
	 * @param dateReference
	 * @return
	 */
	private static EOElementCarriere elementSupInfoValide(NSArray<EOElementCarriere> elementsCarriere, NSTimestamp dateReference) {

		for (EOElementCarriere myElement : elementsCarriere) {
			if(DateCtrl.isBefore(myElement.dateDebut(), dateReference) && (myElement.dateFin() == null 
					|| DateCtrl.isAfterEq(myElement.dateFin(), dateReference)))
				return myElement;
		}

		// Si on a rien trouve, on renvoie le dernier element connu
		return elementsCarriere.lastObject();
	}
}
// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   EvalECCtrl.java

package org.cocktail.mangue.server.supinfo;

import java.util.Enumeration;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOParamPromotion;
import org.cocktail.mangue.modele.mangue.EOSupInfoData;
import org.cocktail.mangue.modele.mangue.EOSupInfoFichier;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationActivite;
import org.cocktail.mangue.server.AutomateAvecThread;
import org.cocktail.mangue.server.ServerThreadManager;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class AutomatePromoEC extends AutomateAvecThread {
	private static final String CODE_CORPS_MCF = "301";
	private static final String CORPS_MA = "302";
	private EOSupInfoFichier fichierSupInfo;
	private EORne rneEtablissement;

	public AutomatePromoEC(EOEditingContext edc) {
		super(edc);
		setRneEtablissement(EOStructure.rechercherEtablissement(getEdc()).rne());
	}
	private static AutomatePromoEC sharedInstance;
	public static AutomatePromoEC sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null)
			sharedInstance = new AutomatePromoEC(edc);
		return sharedInstance;
	}

	public EORne getRneEtablissement() {
		return rneEtablissement;
	}

	public void setRneEtablissement(EORne rneEtablissement) {
		this.rneEtablissement = rneEtablissement;
	}

	public EOSupInfoFichier getFichierSupInfo() {
		return fichierSupInfo;
	}

	public void setFichierSupInfo(EOSupInfoFichier fichierSupInfo) {
		this.fichierSupInfo = fichierSupInfo;
	}

	/**
	 * 
	 * @param annee
	 */
	private void preparerFichier(Integer annee) {
		EOSupInfoFichier fichier = EOSupInfoFichier.fetchForAnneeAndType(getEdc(), annee, EOSupInfoFichier.OBJET_PROM_EC);
		if (fichier == null) {
			fichier = EOSupInfoFichier.creer(getEdc(), EOSupInfoFichier.OBJET_PROM_EC);
		}
		fichier.setSupfAnnee(annee);
		fichier.setSupfDateObs(DateCtrl.finAnnee(annee));
		getEdc().saveChanges();
		setFichierSupInfo(fichier);
	}


	/** Prepare les donnees Electra pour les enseignants-chercheurs : ne recree pas ceux 
	 * deja existants mais les modifient et les enregistrent dans la base.
	 * On recherche les enseignants-chercheurs en se basant sur les derniers changements de position valides
	 * et on applique les criteres lies aux parametres de promotion
	 * Lance une exception si un individu n'a pas un des champs obligatoires fournis
	 * @return nombre de donnees crees
	 */
	public void preparer(Integer annee, NSArray<EOParamPromotion> parametresPromotion, ServerThreadManager thread) throws Exception {

		preparerFichier(annee);
		
		// Rechercher toutes les donnees deja existantes
		NSArray<EOSupInfoData> donneesExistantes = EOSupInfoData.findForFichierAndPromotions(getEdc(), getFichierSupInfo(), parametresPromotion);
		setThreadCourant(thread);

		try {

			NSMutableArray<EOIndividu> individusGeres = new NSMutableArray<EOIndividu>();

			informerThread(">> Suppression des données ...");
			supprimerDonnees(getEdc(), getFichierSupInfo());

			for (EOParamPromotion grade : parametresPromotion) {
				
				NSArray<EOParamPromotion> params = EOParamPromotion.rechercherParametresPourCode(getEdc(), grade.parpCode());

				// On traite les parametres de promotion les uns à la suite des autres en recherchant pour chaque parametre
				// les elements de carriere remplissant les conditions du parametre
				parametresPromotion = EOSortOrdering.sortedArrayUsingKeyOrderArray(parametresPromotion, EOParamPromotion.triParametres(""));

				for (EOParamPromotion parametre : params) {

					informerThread(">> GRADE : " + parametre.gradeDepart().lcGrade() + " , Ech : " + parametre.cEchelon() + " , Che : " + parametre.cChevronDepart());

					NSArray<EOElementCarriere> elementsCarriere = EOElementCarriere.rechercherElementsAvecCriteres(getEdc(), qualifierElementsCarrierePourParametres(getEdc(), new NSArray<EOParamPromotion>(parametre), getFichierSupInfo().supfDateObs()), true, true);

					// les elements de carriere sont ranges par individu et date d'effet decroissants
					EOIndividu individuCourant = null;
					NSMutableArray<EOElementCarriere> elementsPourIndividu = new NSMutableArray<EOElementCarriere>();

					for (EOElementCarriere myElement : elementsCarriere) {

						if (individuCourant == null) {
							individuCourant = myElement.toIndividu();
							elementsPourIndividu.addObject(myElement);
						} else if (myElement.toIndividu() == individuCourant) {
							elementsPourIndividu.addObject(myElement);
						} else { // il s'agit d'un autre individu, on traite l'individu courant

							informerThread("\t" + elementsCarriere.size() + " > " + individuCourant.identitePrenomFirst());

							// On ne retient que les individus en activite à la date de reference
							EOChangementPosition changement = changementEnActivite(individuCourant, getFichierSupInfo().supfDateObs());

							// Verifier s'il n'y a pas de surnombre debutant avant le debut de la campage (01/09/anneeReference)
							NSArray<EOProlongationActivite> surnombres = EOProlongationActivite.rechercherSurnombrePourPeriode(getEdc(), individuCourant, null, datePromotionEcPourAnnee(getFichierSupInfo().supfAnnee()));

							if (individusGeres.containsObject(individuCourant) == false 
									&& changement != null && surnombres.count() == 0) {

								EOSupInfoData record = recordPourIndividu(donneesExistantes, individuCourant);
									
								if (record == null || record.estProvisoire()) {
									if (preparerRecordPourIndividuAvec(getEdc(), getFichierSupInfo(), record, individuCourant, changement, elementsPourIndividu, parametre, getFichierSupInfo().supfAnnee(), false)) {	
										individusGeres.addObject(individuCourant);
									}
								} else {
									informerThread("\t\t Existant");
									individusGeres.addObject(individuCourant);
								}
							}
							else {
								if (changement == null)
									informerThread("\t\t Changement de position non valide !");
							}
							// on repart de l'élement en cours
							individuCourant = myElement.toIndividu();
							elementsPourIndividu = new NSMutableArray(myElement);
						}			
					}
					// Traiter le dernier individu courant
					if (individuCourant != null) {

						EOChangementPosition changement = changementEnActivite(individuCourant, getFichierSupInfo().supfDateObs());
						if (individusGeres.containsObject(individuCourant) == false && changement != null) {
							EOSupInfoData record = recordPourIndividu(donneesExistantes, individuCourant);

							if (record == null || record.estProvisoire()) {

								if (preparerRecordPourIndividuAvec(getEdc(), getFichierSupInfo(), record, individuCourant, changement, elementsPourIndividu, parametre, getFichierSupInfo().supfAnnee(),false)) {	
									individusGeres.addObject(individuCourant);
								}
								else {
									informerThread("\t\t Non valide");
								}
							} else {
								individusGeres.addObject(individuCourant);	// Cet individu n'est plus à traiter
							}
						}
					}
				}
			}

			getEdc().saveChanges();

		} catch (Exception exc) {
			exc.printStackTrace();
			getEdc().revert();
			throw exc;
		} 
	}

	/**
	 * 
	 * Suppression des donnees supinfo existantes.
	 * Si individu == null, suppression de toutes les donnees de l'annee et date de reference renseignees
	 * Sinon suppression des donnees de l'individu seulement
	 * 
	 * @param ec
	 * @param fichier
	 * @param individu
	 * @throws Exception
	 */
	private void supprimerDonnees(EOEditingContext ec, EOSupInfoFichier fichier) throws Exception {

		NSArray<EOSupInfoData> datas = EOSupInfoData.findForFichier(ec, fichier);
		for(EOSupInfoData myData : datas) {
			ec.deleteObject(myData);
		}

		ec.saveChanges();
	}

	/**
	 * 
	 * Preparation d'une promotion d'EC pour un individu donne et des parametres de promotion donnes
	 * 
	 * @param editingContext
	 * @param anneeReference
	 * @param parametresPromotion
	 * @param individu
	 * @return
	 * @throws Exception
	 */
	public int preparerPromECManuelle(EOEditingContext ec,EOSupInfoFichier fichier,NSArray parametresPromotion, EOIndividu individu) throws Exception {

		int nbComptes = 0;
		String observations = "Ajout Manuel\n";
		NSTimestamp dateReference = fichier.supfDateObs();

		try {

			EOParamPromotion parametre = (EOParamPromotion)parametresPromotion.objectAtIndex(0);

			// On verifie qu'on ait bien un ou plusieurs elements correspondant aux criteres demandes pour l'agent.
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_INDIVIDU_KEY + "=%@", new NSArray<EOIndividu>(individu)));  
			qualifiers.addObject(qualifierElementsCarrierePourParametres(ec, new NSArray<EOParamPromotion>(parametresPromotion), dateReference));

			NSArray<EOElementCarriere> elementsCarriere = EOElementCarriere.rechercherElementsAvecCriteres(ec, new EOAndQualifier(qualifiers), true, true);

			if (elementsCarriere.count() > 0) {

				EOElementCarriere lastElement = (EOElementCarriere)elementsCarriere.objectAtIndex(0);

				EOSupInfoData myRecord = EOSupInfoData.rechercherDemande(ec, fichier, individu);
				if (myRecord != null) {
					ec.deleteObject(myRecord);
				}

				myRecord = EOSupInfoData.creer(ec, fichier);
				myRecord.initIndividu(individu);	

				EOChangementPosition changement = changementEnActivite(individu, dateReference);

				myRecord.initElement(lastElement);
				myRecord.initChangementPosition(changement);

				NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsPourIndividuPositionsEtPeriode(ec, individu, null, null, null);
				try {
					myRecord.setEficDDebPostion(((EOChangementPosition)changements.objectAtIndex(0)).dateDebut());
				}
				catch(Exception exception) {
				}

				myRecord.setToParamPromotionRelationship(parametre);
				if(parametre.parpDureeMaxAncienneteCons() != null)
				{
					int nbMois = DateCtrl.calculerDureeEnMois(myRecord.eficDEchelon(), dateReference).intValue();
					int nbMoisMax = parametre.parpDureeMaxAncienneteCons().intValue() * 12;
					if(nbMois < nbMoisMax)
						myRecord.setEficAncConservee(myRecord.preparerConservationAnciennete(dateReference));
				}

				if(parametre.cChevronDepart() != null && myRecord.eficCChevron() == null)	{
					myRecord.setEficCChevron(null);
					myRecord.setEstValide(false, "Problème de chevron");
				}

				myRecord.setEficObservations(observations);

				ec.saveChanges();
				nbComptes = 1;

			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}

		return nbComptes;
	}


	/**
	 * 
	 * @param edc
	 * @param fichier
	 * @param record
	 * @param individu
	 * @param sexe
	 * @param changement
	 * @param elementsCarrierePourIndividu
	 * @param parametre
	 * @param anneeReference
	 * @param estCreationManuelle
	 * @return
	 */
	private boolean preparerRecordPourIndividuAvec(EOEditingContext edc, EOSupInfoFichier fichier, EOSupInfoData record, EOIndividu individu, EOChangementPosition changement, NSArray elementsCarrierePourIndividu, EOParamPromotion parametre, Integer anneeReference, 
			boolean estCreationManuelle) {

		EOElementCarriere elementConcerne = elementValidePourIndividuEtCriteres(elementsCarrierePourIndividu, parametre, fichier.supfDateObs());

		if(elementConcerne != null) {

			boolean estCreation = record == null;
			if(estCreation) {
				record = EOSupInfoData.creer(edc, fichier);
				record.initIndividu(individu);
				edc.insertObject(record);
			} 
			
			record.initUAI(getRneEtablissement());
			record.initElement(elementConcerne);
			record.initChangementPosition(changement);
			record.initSpecialisation(elementConcerne.toCarriere());

			NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsPourIndividuPositionsEtPeriode(edc, individu, null, null, null);
			try {
				record.setEficDDebPostion(changements.get(0).dateDebut());
			}
			catch(Exception exception) {
			}

			record.setToParamPromotionRelationship(parametre);
			if(parametre.parpDureeMaxAncienneteCons() != null)
			{
				int nbMois = DateCtrl.calculerDureeEnMois(record.eficDEchelon(), fichier.supfDateObs()).intValue();
				int nbMoisMax = parametre.parpDureeMaxAncienneteCons().intValue() * 12;
				if(nbMois < nbMoisMax)
					record.setEficAncConservee(record.preparerConservationAnciennete(fichier.supfDateObs()));
			}

			if(parametre.cChevronDepart() != null && record.eficCChevron() == null)	{
				record.setEficCChevron(null);
				record.setEstValide(false, "Problème de chevron");
			}
			return true;
		} else {
			informerThread("\t\t Promotion non applicable");
			return false;
		}
	}


	/**
	 * 
	 * @param elementsCarriere
	 * @param parametre
	 * @param dateReference
	 * @return
	 */
	private EOElementCarriere elementValidePourIndividuEtCriteres(NSArray<EOElementCarriere> elementsCarriere, EOParamPromotion parametre, NSTimestamp dateReference) {

		EOIndividu individu = elementsCarriere.get(0).toIndividu();

		if(!conditionsInitialesRempliesPourIndividuParametresEtAnneeReference(individu, parametre, dateReference)) {
			informerThread("\t\tMCF : Conditions Initiales non remplies");
			return null;
		}

		if(conditionsRemplies(elementsCarriere, parametre, dateReference)) {
			EOElementCarriere element = (EOElementCarriere)elementsCarriere.lastObject();
			if(element.dateFin() == null || DateCtrl.isAfterEq(element.dateFin(), dateReference))
				return element;
		}
		return null;
	}


	/**
	 * 
	 * @param donnees
	 * @param individu
	 * @return
	 */
	private static EOSupInfoData recordPourIndividu(NSArray<EOSupInfoData> donnees, EOIndividu individu) {
		for(EOSupInfoData myData : donnees)	{
			if(myData.toIndividu() == individu)
				return myData;
		}

		return null;
	}

	/**
	 * 
	 * @param editingContext
	 * @param nomRelation
	 * @param parametres
	 * @param dateReference
	 * @return
	 */
	public EOQualifier qualifierElementsCarrierePourParametres(EOEditingContext editingContext, NSArray<EOParamPromotion> parametres, NSTimestamp dateReference)
	{
		if(dateReference == null || parametres == null || parametres.count() == 0)
			return null;
		
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TEM_PROVISOIRE_KEY + "=%@", new NSArray(CocktailConstantes.FAUX)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_CARRIERE_KEY + "."+EOCarriere.TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOElementCarriere.DATE_DEBUT_KEY, dateReference, EOElementCarriere.DATE_FIN_KEY, dateReference));

		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
		for(EOParamPromotion myPromotion : parametres) {

			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
			if(myPromotion.typePopulationDepart() != null)
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_CARRIERE_KEY+"."+EOCarriere.TO_TYPE_POPULATION_KEY+"=%@", new NSArray(myPromotion.typePopulationDepart())));
			if(myPromotion.corpsDepart() != null)
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_CORPS_KEY  + "=%@", new NSArray(myPromotion.corpsDepart())));
			if(myPromotion.gradeDepart() != null)
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_GRADE_KEY  + "=%@", new NSArray(myPromotion.gradeDepart())));
			if(myPromotion.cEchelon() != null)
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.C_ECHELON_KEY + "=%@", new NSArray(myPromotion.cEchelon())));
			if(myPromotion.cChevronDepart() != null)
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.C_CHEVRON_KEY + "=%@", new NSArray(myPromotion.cChevronDepart())));
			if(andQualifiers.count() > 0)
				orQualifiers.addObject(new EOAndQualifier(andQualifiers));
		}

		if(orQualifiers.count() > 0)
			qualifiers.addObject(new EOOrQualifier(orQualifiers));

		return new EOAndQualifier(qualifiers);
	}



	/**
	 * 
	 * CONDITIONS INITIALES A REMPLIR POUR LES MCF
	 * 
	 * @param individu
	 * @param parametre
	 * @param dateReference
	 * @return
	 */
	private boolean conditionsInitialesRempliesPourIndividuParametresEtAnneeReference(EOIndividu individu, EOParamPromotion parametre, NSTimestamp dateReference) {

		if(!parametre.gradeDepart().toCorps().cCorps().equals(CODE_CORPS_MCF))
			return true;

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TEM_PROVISOIRE_KEY + "='N'", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TEM_VALIDE_KEY + "='O'", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_CARRIERE_KEY+"."+EOCarriere.TEM_VALIDE_KEY + "='O'", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.DATE_DEBUT_KEY + " <= %@ ", new NSArray(dateReference)));

		NSMutableArray<EOElementCarriere> elementsCarriere = new NSMutableArray<EOElementCarriere>(EOElementCarriere.rechercherElementsAvecCriteres(individu.editingContext(), new EOAndQualifier(qualifiers), true, false));
		EOSortOrdering.sortArrayUsingKeyOrderArray(elementsCarriere, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);

		NSTimestamp debutPeriode = null;
		NSTimestamp finElementCarriere = null;

		// On recupere l'anciennete dans le corps (sans interruption de segments de carriere)
		for(EOElementCarriere elementCourant : elementsCarriere)	{

			if(elementCourant.toCorps().cCorps().equals(CORPS_MA) || elementCourant.toCorps().cCorps().equals(CODE_CORPS_MCF)) {
				if(debutPeriode == null) {
					debutPeriode = elementCourant.dateDebut();
					finElementCarriere = elementCourant.dateFin();
				} else
					if(finElementCarriere != null)
						if(DateCtrl.isSameDay(DateCtrl.jourSuivant(finElementCarriere), elementCourant.dateDebut()))
							finElementCarriere = elementCourant.dateFin();
						else {
							debutPeriode = elementCourant.dateDebut();
							finElementCarriere = elementCourant.dateFin();
						}
			} else {
				debutPeriode = null;
				finElementCarriere = null;
			}

		}

		if(debutPeriode == null)
			return false;
		if(finElementCarriere == null)
			finElementCarriere = dateReference;

		int nbMois = DateCtrl.calculerDureeEnMois(debutPeriode, finElementCarriere, true).intValue();

		if(nbMois < 60) {
			informerThread("\t\tNombre de mois < 60 ==> " + nbMois + " ( Période du " + DateCtrl.dateToString(debutPeriode) + " au " + DateCtrl.dateToString(finElementCarriere) + ")");
			return false;
		}

		NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsEnActiviteOuDetachementPourIndividuEtPeriode(individu.editingContext(), individu, debutPeriode, finElementCarriere);

		if(changements.count() == 0) {
			LogManager.logDetail("Pas de changement de position en activite sur la periode MCF ou MA");
			return false;
		}
		changements = EOSortOrdering.sortedArrayUsingKeyOrderArray(changements, EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC );
		NSTimestamp debutChangement = ((EOChangementPosition)changements.objectAtIndex(0)).dateDebut();
		NSTimestamp finChangement = null;
		nbMois = 0;

		for (EOChangementPosition changement : changements)  {

			if(finChangement == null)
				finChangement = changement.dateFin();
			else
				if(DateCtrl.isSameDay(DateCtrl.jourSuivant(finChangement), changement.dateDebut()))
					finChangement = changement.dateFin();
				else {
					nbMois += DateCtrl.calculerDureeEnMois(debutChangement, finChangement).intValue();
					debutChangement = changement.dateDebut();
					finChangement = changement.dateFin();
				}
		}
		if(finChangement == null)
			finChangement = dateReference;

		nbMois += DateCtrl.calculerDureeEnMois(debutChangement, finChangement).intValue();

		return nbMois >= 60;
	}


	/**
	 * 
	 * @param elementsCarriere
	 * @param dateReference
	 * @return
	 */
	private static int dureePourElements(NSArray elementsCarriere, NSTimestamp dateReference) 	{
		
		elementsCarriere = EOSortOrdering.sortedArrayUsingKeyOrderArray(elementsCarriere, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		NSTimestamp debutPeriode = null;
		NSTimestamp finElementCarriere = null;
		for(Enumeration<EOElementCarriere> e = elementsCarriere.objectEnumerator(); e.hasMoreElements();){
			EOElementCarriere elementCourant = e.nextElement();
			if(debutPeriode == null)
			{
				debutPeriode = elementCourant.dateDebut();
				finElementCarriere = elementCourant.dateFin();
			} else
				if(finElementCarriere != null)
					if(DateCtrl.isSameDay(DateCtrl.jourSuivant(finElementCarriere), elementCourant.dateDebut()))
						finElementCarriere = elementCourant.dateFin();
					else {
						debutPeriode = elementCourant.dateDebut();
						finElementCarriere = elementCourant.dateFin();
					}
		}

		if(debutPeriode == null)
			return 0;
		if(finElementCarriere == null || DateCtrl.isAfter(finElementCarriere, dateReference))
			finElementCarriere = dateReference;
		return DateCtrl.calculerDureeEnMois(debutPeriode, finElementCarriere, true).intValue();
	}
	/**
	 * 
	 * @param elementsCarriere
	 * @param parametrePromotion
	 * @param dateReference
	 * @return
	 */
	private static boolean conditionsRemplies(NSArray elementsCarriere, EOParamPromotion parametrePromotion, NSTimestamp dateReference) {

		int nbMoisAnciennete = 0;
		EOIndividu individu = ((EOElementCarriere)elementsCarriere.objectAtIndex(0)).toIndividu();

		if(parametrePromotion.cEchelon() != null) {
			if(parametrePromotion.parpDureeEchelon() != null)
				nbMoisAnciennete = parametrePromotion.parpDureeEchelon().intValue() * 12;


			if(nbMoisAnciennete > 0)
			{
				int nbMoisAncienneteEchelon = dureePourElements(elementsCarriere, dateReference);
				if(nbMoisAncienneteEchelon < nbMoisAnciennete) 	{
					return false;
				}
				nbMoisAnciennete = 0;
			}
		}
		if(parametrePromotion.parpDureeGrade() != null)
			nbMoisAnciennete = parametrePromotion.parpDureeGrade().intValue() * 12;
		if(parametrePromotion.parpDureeMoisGrade() != null)
			nbMoisAnciennete += parametrePromotion.parpDureeMoisGrade().intValue();

		if(nbMoisAnciennete > 0) {
			if(parametrePromotion.cEchelon() != null) {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TEM_VALIDE_KEY + " = 'O'", null));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_CARRIERE_KEY+"."+EOCarriere.TEM_VALIDE_KEY + "='O'", null));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TEM_PROVISOIRE_KEY + " = 'N'", null));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.DATE_DEBUT_KEY + " <= %@", new NSArray(dateReference)));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_GRADE_KEY + "=%@", new NSArray(parametrePromotion.gradeDepart())));
				elementsCarriere = EOElementCarriere.rechercherElementsAvecCriteres(parametrePromotion.editingContext(), new EOAndQualifier(qualifiers), true, false);
			}
			int nbMoisAncienneteGrade = dureePourElements(elementsCarriere, dateReference);

			if(nbMoisAncienneteGrade < nbMoisAnciennete) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param anneeReference
	 * @return
	 */
	public static NSTimestamp datePromotionEcPourAnnee(int anneeReference)	{
		return DateCtrl.stringToDate("01/09/" + String.valueOf(anneeReference));
	}


	/**
	 * 
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static EOChangementPosition changementEnActivite(EOIndividu individu, NSTimestamp dateReference)
	{
		NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsEnActiviteOuDetachementPourIndividuEtPeriode(individu.editingContext(), individu, dateReference, dateReference);
		if(changements.size() > 0)
			return changements.lastObject();
		else
			return null;
	}

}


package org.cocktail.mangue.server;

import java.sql.Connection;

import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.DureeAvecArrete;
import org.cocktail.mangue.modele.IndividuPourEdition;
import org.cocktail.mangue.modele.IndividuPromouvable;
import org.cocktail.mangue.modele.IndividuPromouvableEc;
import org.cocktail.mangue.modele.InfoPourEditionMedicale;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageChevron;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.elections.EOTypeInstance;
import org.cocktail.mangue.modele.grhum.referentiel.EOBlocNotes;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.conges.EOCongeGardeEnfant;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOHistoPromotion;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;
import org.cocktail.mangue.modele.mangue.individu.medical.EOExamenMedical;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVaccin;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVisiteMedicale;
import org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste;
import org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf;
import org.cocktail.mangue.modele.mangue.lolf.EOPoste;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.modele.mangue.modalites.EOMiTpsTherap;
import org.cocktail.mangue.modele.mangue.modalites.EORepriseTempsPlein;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;
import org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation;
import org.cocktail.mangue.modele.mangue.prolongations.EOEmeritat;
import org.cocktail.mangue.server.impression.Imprimeur;

import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;

public class SessionPrint
{
	
	private final static String FICHES_IDENTITES = "FichesIdentites";
	private final static String FICHE_IDENTITE_INDIVIDU = "FicheIdentiteIndividu";
	private final static String FICHE_IDENTITE_CIR = "FicheIdentiteCir";
	private final static String FICHE_IDENTITE_ELECTRA = "FicheIdentiteElectra";

	Application	myApp;	
	EOEditingContext myEditingContext;
	
	private ServerThreadManager threader;
	private String loginAgent;

	/**
	 * 
	 *
	 */
	public SessionPrint(Application woApplication, EOEditingContext ec)	{
		
		super();

		myApp = woApplication;
		myEditingContext = ec;
		
	}
	private EOEditingContext getNewEditingContext() {
		return ERXEC.newEditingContext();
	}
	public EOEditingContext defaultEditingContext() {
		return myEditingContext;
	}

	public String getLoginAgent() {
		return loginAgent;
	}
	
	public void setLoginAgent(String value) {
		loginAgent = value;
	}
	
	/** Impression d'un type d'election */
	public Boolean clientSideRequestImprimerTypeInstance(EOGlobalID globalIDInstance,Boolean editionDetaillee) {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		EOTypeInstance type = (EOTypeInstance)SuperFinder.objetForGlobalIDDansEditingContext(globalIDInstance,editingContext);
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerTypeInstance : " + type.llTypeInstance());
		try {
			Class[] classeParametres =  new Class[] {EOTypeInstance.class,Boolean.class};
			Object[] parametres = new Object[]{type,editionDetaillee};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerTypeInstance",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression d'un college */
	public Boolean clientSideRequestImprimerCollege(EOGlobalID globalID) {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		EOCollege college = (EOCollege)SuperFinder.objetForGlobalIDDansEditingContext(globalID,editingContext);
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerCollege : " + college.llCollege());
		try {
			Class[] classeParametres =  new Class[] {EOCollege.class};
			Object[] parametres = new Object[]{college};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerCollege",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des secteurs */
	public Boolean clientSideRequestImprimerSecteurs() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray secteurs = SuperFinder.rechercherEntite(editingContext,"Secteur");
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerSecteurs");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{secteurs};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerSecteurs",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des composantes &eacute;lectives */
	public Boolean clientSideRequestImprimerComposantes() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray composantes = SuperFinder.rechercherEntite(editingContext,"ComposanteElective");
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerComposantes");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{composantes};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerComposantesElectives",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des sections &eacute;lectives */
	public Boolean clientSideRequestImprimerSections() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray sections = SuperFinder.rechercherEntite(editingContext,"SectionElective");
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerSections");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{sections};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerSectionsElectives",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des coll&grave;ges-sections &eacute;lectives */
	public Boolean clientSideRequestImprimerCollegesSections() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray collegesSections = SuperFinder.rechercherEntite(editingContext,"CollegeSectionElective");
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("college.llCollege", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("sectionElective.llSectionElective", EOSortOrdering.CompareAscending));
		collegesSections = EOSortOrdering.sortedArrayUsingKeyOrderArray(collegesSections, sorts);
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerCollegesSections");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{collegesSections};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerCollegesSections",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des bureaux de vote */
	public Boolean clientSideRequestImprimerBureauxDeVote() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray bureaux = SuperFinder.rechercherEntite(editingContext,"BureauVote");
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerBureauxDeVote");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{bureaux};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerBureauxDeVote",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression de la liste &eacute;lectorale pour une &eacute;lection
		Retourne true si impression OK */
	public Boolean clientSideRequestImprimerListeElectorale(InfoPourEditionRequete infoEdition,EOGlobalID globalIDInstance,Integer typeElecteur) {
		EOInstance instance = (EOInstance)recordPourGlobalID(globalIDInstance);
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeElectorale : " + instance.llInstance() + ", date scrutin :" + instance.dateScrutinFormatee());
		try {
			Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,EOInstance.class,Integer.class};
			Object[] parametres = new Object[]{infoEdition,instance,typeElecteur};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeElectorale",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	
	public Boolean clientSideRequestImprimerAbsencesPourIndividu(InfoPourEditionRequete infoEdition,EOGlobalID individuID,NSArray absencesID,Boolean imprimerDetails) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerAbsencesPourIndividu");
		try {
			EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
			NSArray absences = Utilitaires.tableauObjetsMetiers(absencesID,individu.editingContext());
			Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,EOIndividu.class,NSArray.class,Boolean.class};
			Object[] parametres = new Object[]{infoEdition,individu,absences,imprimerDetails};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerAbsencesPourIndividu",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerCirIdentite(InfoPourEditionRequete infoEdition,NSArray cirsID,Boolean imprimerDetails) {

		try {
			NSArray cirs = Utilitaires.tableauObjetsMetiers(cirsID,defaultEditingContext());

			threader = new ServerThreadManager(Imprimeur.sharedInstance(),
					"imprimerCirsIdentite",
					new Class[] {InfoPourEditionRequete.class,NSArray.class,Boolean.class},
					new Object[]{infoEdition,cirs,imprimerDetails});
			threader.start();

			return new Boolean(true);

		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param congeID
	 * @param destinataires
	 * @param estArreteAnnulation
	 * @return
	 */
	public Boolean clientSideRequestImprimerArreteConge(EOGlobalID congeID,NSArray destinataires,Boolean estArreteAnnulation) {
		Duree conge = (Duree)recordPourGlobalID(congeID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,conge.editingContext());
		try {
			Class[] classeParametres =  new Class[] {Duree.class,NSArray.class,Boolean.class};
			Object[] parametres = new Object[]{conge,destins,estArreteAnnulation};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteConge",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param congeID
	 * @param destinataires
	 * @return
	 */
	public Boolean clientSideRequestImprimerCongeGardeEnfant(EOGlobalID congeID,NSArray destinataires) {
		EOCongeGardeEnfant conge = (EOCongeGardeEnfant)recordPourGlobalID(congeID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,conge.editingContext());
		try {
			Class[] classeParametres =  new Class[] {EOCongeGardeEnfant.class,NSArray.class};
			Object[] parametres = new Object[]{conge,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerCongeGardeEnfant",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param noArrete
	 * @param globalID
	 * @param destinataires
	 * @param estVacation
	 * @return
	 */
	public Boolean clientSideRequestImprimerContratTravail(String noArrete,EOGlobalID globalID,NSArray destinataires,Boolean estVacation) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerContratTravail");
		// 04/03/2011 - pour l'utilisation des modules d'impression
		Connection connection = null;
		EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
		if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
			connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
		}
		EOContratAvenant avenant = (EOContratAvenant)recordPourGlobalID(globalID);
		avenant.setNoArretePourEdition(noArrete);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,avenant.editingContext());
		if (connection != null) {
			try {
				Class[] classeParametres =  new Class[] {Connection.class,EOContratAvenant.class,NSArray.class};
				Object[] parametres = new Object[]{connection,avenant,destins};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerContratTravail",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} catch (Exception e) {
				LogManager.logException(e);
				return new Boolean(false);
			}
		} else {
			LogManager.logDetail("Pas de connexion avec la base");
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param noArrete
	 * @param globalID
	 * @param destinataires
	 * @param estVacation
	 * @return
	 */
	public Boolean clientSideRequestImprimerContratVacationLocal(String noArrete,EOGlobalID globalID,NSArray destinataires) {

		Connection connection = null;
		EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
		if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
			connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
		}
		EOVacataires vacation = (EOVacataires)recordPourGlobalID(globalID);
		vacation.setNoArretePourEdition(noArrete);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,vacation.editingContext());
		if (connection != null) {
			try {
				Class[] classeParametres =  new Class[] {Connection.class,EOVacataires.class,NSArray.class};
				Object[] parametres = new Object[]{connection,vacation,destins};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerContratVacation",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} catch (Exception e) {
				LogManager.logException(e);
				return new Boolean(false);
			}
		} else {
			LogManager.logDetail("Pas de connexion avec la base");
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param noArrete
	 * @param globalID
	 * @param destinataires
	 * @return
	 */
	public Boolean clientSideRequestImprimerContratVacation(String noArrete,EOGlobalID globalID,NSArray destinataires) {

		Connection connection = null;
		EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
		if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
			connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
		}
		EOVacataires vacation = (EOVacataires)recordPourGlobalID(globalID);
		vacation.setNoArretePourEdition(noArrete);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,vacation.editingContext());
		if (connection != null) {
			try {
				Class[] classeParametres =  new Class[] {Connection.class,EOVacataires.class,NSArray.class};
				Object[] parametres = new Object[]{connection,vacation,destins};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerVacation",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} catch (Exception e) {
				LogManager.logException(e);
				return new Boolean(false);
			}
		} else {
			LogManager.logDetail("Pas de connexion avec la base");
			return new Boolean(false);
		}

	}


	public Boolean clientSideRequestImprimerArretePourAvenant(EOGlobalID avenantID,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerArretePourAvenant");
		EOContratAvenant avenant = (EOContratAvenant)recordPourGlobalID(avenantID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,avenant.editingContext());
		try {
			Class[] classeParametres =  new Class[] {EOContratAvenant.class,NSArray.class};
			Object[] parametres = new Object[]{avenant,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePourAvenant",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerArretePourCarriere(EOGlobalID elementID,NSArray destinataires,Boolean estArreteAnnulation) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerArretePourCarriere");
		EOElementCarriere element = (EOElementCarriere)recordPourGlobalID(elementID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,element.editingContext());
		try {
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePourCarriere",
					new Class[] {EOElementCarriere.class,NSArray.class,Boolean.class},
					new Object[]{element,destins,estArreteAnnulation});
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerArretePourEvenement(EOGlobalID evenementID,NSArray destinataires,Boolean estArreteAnnulation) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerArretePourEvenement");
		DureeAvecArrete duree = (DureeAvecArrete)recordPourGlobalID(evenementID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,duree.editingContext());
		try {
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePourEvenement",
					new Class[] {DureeAvecArrete.class,NSArray.class,Boolean.class},
					new Object[]{duree,destins,estArreteAnnulation});
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	public Boolean clientSideRequestImprimerArreteEmeritat(EOGlobalID gidEmeritat, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EOEmeritat object = (EOEmeritat)SuperFinder.objetForGlobalIDDansEditingContext(gidEmeritat,editingContext);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,object.editingContext());
		try {
			Class[] classeParametres	=  new Class[] {EOEmeritat.class,NSArray.class};
			Object[] parametres = new Object[]{object,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteEmeritat",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}


	public Boolean clientSideRequestImprimerArretePromotionEC(IndividuPromouvableEc individuPromouvable,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerArretePromotionEC");
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,individuPromouvable.editingContext());
		try {
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePromotionEC",
					new Class[] {IndividuPromouvableEc.class,NSArray.class},
					new Object[]{individuPromouvable,destins});
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * @param emploiID : emploi à imprimer
	 * @return Vrai/Faux
	 */
	public Boolean clientSideRequestImprimerEmploi(EOGlobalID emploiID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerEmploi");
		IEmploi emploi = (IEmploi) recordPourGlobalID(emploiID);
		try {
			Class[] classeParametres =  new Class[] {IEmploi.class};
			Object[] parametres = new Object[]{emploi};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(), "imprimerEmploi", classeParametres, parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * @param emploiID : emploi à imprimer
	 * @return Vrai/Faux
	 */
	public Boolean clientSideRequestImprimerEmploiHistorique(EOGlobalID emploiID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerEmploiHistorique");
		IEmploi emploi = (IEmploi) recordPourGlobalID(emploiID);
		try {
			Class[] classeParametres =  new Class[] {IEmploi.class};
			Object[] parametres = new Object[]{emploi};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(), "imprimerEmploiHistorique", classeParametres, parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	public Boolean clientSideRequestImprimerListeEmplois(InfoPourEditionRequete infoPourEdition,NSArray emploisID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeEmplois");
		NSArray emplois = Utilitaires.tableauObjetsMetiers(emploisID, getNewEditingContext());
		try {
			Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,NSArray.class};
			Object[] parametres = new Object[]{infoPourEdition,emplois};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerEmplois",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerPrimes(InfoPourEditionRequete infoEdition,NSArray attributionsID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerPrimes");
		try {
			EOEditingContext editingContext = getNewEditingContext();
			NSArray attributions = Utilitaires.tableauObjetsMetiers(attributionsID,editingContext);
			Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,NSArray.class};
			Object[] parametres = new Object[]{infoEdition,attributions};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerAttributionsPrimes",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerPoste(EOGlobalID globalID,Boolean estFicheDePoste) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerPoste");
		EOPoste poste = (EOPoste)recordPourGlobalID(globalID);
		try {
			Class[] classeParametres =  new Class[] {EOPoste.class,Boolean.class};
			Object[] parametres = new Object[]{poste,estFicheDePoste};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPoste",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerListePostes(NSArray postesID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListePostes");
		NSArray postes = Utilitaires.tableauObjetsMetiers(postesID, getNewEditingContext());
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{postes};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPostes",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerFicheDePoste(EOGlobalID ficheDePosteID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFicheDePoste");
		EOFicheDePoste fiche = (EOFicheDePoste)recordPourGlobalID(ficheDePosteID);
		try {
			Class[] classeParametres =  new Class[] {EOFicheDePoste.class};
			Object[] parametres = new Object[]{fiche};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFicheDePoste",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerFicheLolf(EOGlobalID ficheID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFicheLolf");
		EOFicheLolf fiche = (EOFicheLolf)recordPourGlobalID(ficheID);
		try {
			Class[] classeParametres =  new Class[] {EOFicheLolf.class};
			Object[] parametres = new Object[]{fiche};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFicheLolf",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	//	Impression du résultat des requêtes
	public Boolean clientSideRequestImprimerResultatRequeteIndividu(NSArray individusEdition,InfoPourEditionRequete infoPourEdition) {
		if (infoPourEdition.nomFichierJasper().equals(FICHE_IDENTITE_INDIVIDU)) {
			LogManager.logDetail(loginAgent + " : clientSideRequestImprimerResultatRequeteIndividu - impression des fiches");
			java.util.Enumeration e = individusEdition.objectEnumerator();
			String texte = "";
			while (e.hasMoreElements()) {
				IndividuPourEdition individu = (IndividuPourEdition)e.nextElement();
				if (texte.length() > 0) {
					texte += ",";
				} 
				texte += individu.noIndividu();
			}
			texte = "where NO_INDIVIDU in (" + texte + ")";
			NSDictionary dict = new NSDictionary(texte,"NO_INDIVIDUS");
			Boolean result = clientSideRequestImprimerAvecModule(FICHES_IDENTITES,dict);
			if (result.booleanValue() == false) {
				return result;
			}
			return new Boolean(true);
		} else {
			LogManager.logDetail(loginAgent + " : clientSideRequestImprimerResultatRequeteIndividu");
			try {
				Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionRequete.class};
				Object[] parametres = new Object[]{individusEdition,infoPourEdition};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerResultatRequeteIndividu",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} catch (Exception e) {
				LogManager.logException(e);
				return new Boolean(false);
			}
		}
	}	
	public Boolean clientSideRequestImprimerResultatRequeteEvenement(NSArray evenementsEdition,InfoPourEditionRequete infoPourEdition) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerResultatRequeteEvenement");
		try {
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionRequete.class};
			Object[] parametres = new Object[]{evenementsEdition,infoPourEdition};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerResultatRequeteEvenement",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerResultatRequeteEmploi(NSArray emploisEdition,InfoPourEditionRequete infoPourEdition) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerResultatRequeteEmploi");
		try {
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionRequete.class};
			Object[] parametres = new Object[]{emploisEdition,infoPourEdition};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerResultatRequeteEmploi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerEtatPersonnel(InfoPourEditionRequete infoEdition,NSArray etatsPersonnels) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerEtatPersonnel");		
		try {                                                                                                                  
			Class[] classeParametres	=  new Class[] {InfoPourEditionRequete.class,NSArray.class};
			Object[] parametres = new Object[]{infoEdition,etatsPersonnels};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerEtatPersonnel",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerDifs(InfoPourEditionRequete infoEdition,NSArray difsGlobalIDs) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerDifs");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray difs = Utilitaires.tableauObjetsMetiers(difsGlobalIDs,editingContext);
			Class[] classeParametres	=  new Class[] {InfoPourEditionRequete.class,NSArray.class};
			Object[] parametres = new Object[]{infoEdition,difs};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerDifs",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationExamen(EOGlobalID examenGlobalID,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationExamen");		
		try {                         
			EOEditingContext editingContext = getNewEditingContext();
			EOExamenMedical examen = (EOExamenMedical)SuperFinder.objetForGlobalIDDansEditingContext(examenGlobalID, editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {EOExamenMedical.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{examen,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationExamen",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationsExamen(NSArray examensGlobalIDs,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationsExamen");		
		try {                         
			EOEditingContext editingContext = getNewEditingContext();
			NSArray examens = Utilitaires.tableauObjetsMetiers(examensGlobalIDs,editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{examens,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationsExamen",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerListeConvocationsExamen(NSArray examensGlobalIDs,InfoPourEditionMedicale infoImpression,String titre) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeConvocationsExamen");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray examens = Utilitaires.tableauObjetsMetiers(examensGlobalIDs,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,String.class};
			Object[] parametres = new Object[]{examens,infoImpression,titre};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeConvocationsExamen",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationVaccin(EOGlobalID vaccinGlobalID,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationVaccin");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			EOVaccin vaccin = (EOVaccin)SuperFinder.objetForGlobalIDDansEditingContext(vaccinGlobalID, editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {EOVaccin.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{vaccin,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationVaccin",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationsVaccin(NSArray vaccinsGlobalIDs,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationsVaccin");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray vaccins = Utilitaires.tableauObjetsMetiers(vaccinsGlobalIDs,editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{vaccins,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationsVaccin",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerListeConvocationsVaccin(NSArray vaccinsGlobalIDs,InfoPourEditionMedicale infoImpression,String titre) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeConvocationsVaccin");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray vaccins = Utilitaires.tableauObjetsMetiers(vaccinsGlobalIDs,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,String.class};
			Object[] parametres = new Object[]{vaccins,infoImpression,titre};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeConvocationsVaccin",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationVisite(EOGlobalID visiteGlobalID,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationVisite");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			EOVisiteMedicale visite = (EOVisiteMedicale)SuperFinder.objetForGlobalIDDansEditingContext(visiteGlobalID, editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {EOVisiteMedicale.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{visite,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationVisite",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationsVisite(NSArray visitesGlobalIDs,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationsVisite");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray visites = Utilitaires.tableauObjetsMetiers(visitesGlobalIDs,editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{visites,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationsVisite",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerListeConvocationsVisite(NSArray visitesGlobalIDs,InfoPourEditionMedicale infoImpression,String titre) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeConvocationsVisite");		
		try {                         
			EOEditingContext editingContext = getNewEditingContext();
			NSArray visites = Utilitaires.tableauObjetsMetiers(visitesGlobalIDs,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,String.class};
			Object[] parametres = new Object[]{visites,infoImpression,titre};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeConvocationsVisite",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerListeAcmos(NSArray individusAcmos,String titreEdition) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeAcmos");		
		try {                         
			Class[] classeParametres	=  new Class[] {NSArray.class,String.class};
			Object[] parametres = new Object[]{individusAcmos,titreEdition};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeAcmos",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	// Promouvabilités
	public Boolean clientSideRequestImprimerPromouvables(NSTimestamp debutPeriode,NSTimestamp finPeriode,NSArray promouvables) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerPromouvables");
		try {                                                                                                                  
			Class[] classeParametres	=  new Class[] {NSTimestamp.class, NSTimestamp.class,NSArray.class};
			Object[] parametres = new Object[]{debutPeriode,finPeriode,promouvables};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPromouvables",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}




	public Boolean clientSideRequestImprimerArretePromouvabilite(IndividuPromouvable individuPromouvable,NSArray destinataires) {

		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,individuPromouvable.editingContext());

		EOPassageEchelon passage = 
				(EOPassageEchelon)EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(defaultEditingContext(), 
						individuPromouvable.grade().cGrade(), 
						individuPromouvable.echelonSuivant(), 
						individuPromouvable.datePromotion(), 
						true).objectAtIndex(0);

		if (passage != null)
			individuPromouvable.setIndiceSuivant(EOIndice.indiceMajorePourIndiceBrutEtDate(individuPromouvable.editingContext(), passage.cIndiceBrut(), individuPromouvable.datePromotion()));

		try {
			Class[] classeParametres	=  new Class[] {IndividuPromouvable.class,NSArray.class};
			Object[] parametres = new Object[]{individuPromouvable,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePromouvabilite",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}


	public Boolean clientSideRequestImprimerArretePromouvabiliteChevron(IndividuPromouvable individuPromouvable,NSArray destinataires) {

		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,individuPromouvable.editingContext());

		EOPassageChevron passage = 
				(EOPassageChevron)EOPassageChevron.chevronSuivant(defaultEditingContext(), 
						individuPromouvable.grade(), 
						individuPromouvable.chevron());

		if (passage != null) {
			individuPromouvable.setChevronSuivant(passage.cChevron());
			individuPromouvable.setIndiceSuivant(EOIndice.indiceMajorePourIndiceBrutEtDate(individuPromouvable.editingContext(), passage.cIndiceBrut(), individuPromouvable.datePromotion()));

		}

		try {
			Class[] classeParametres	=  new Class[] {IndividuPromouvable.class,NSArray.class};
			Object[] parametres = new Object[]{individuPromouvable,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePromouvabiliteChevron",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}


	/**
	 * 
	 * @param gidCrct
	 * @param destinataires
	 * @return
	 */
	public Boolean clientSideRequestImprimerArreteCrct(EOGlobalID gidCrct, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EOCrct crct = (EOCrct)SuperFinder.objetForGlobalIDDansEditingContext(gidCrct,defaultEditingContext());
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
		try {
			Class[] classeParametres	=  new Class[] {EOCrct.class,NSArray.class};
			Object[] parametres = new Object[]{crct,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteCrct",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}

	/**
	 * 
	 * @param gidTempsPartiel
	 * @param destinataires
	 * @return
	 */
	public Boolean clientSideRequestImprimerArreteTempsPartiel(EOGlobalID gidTempsPartiel, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EOTempsPartiel tempsPartiel = (EOTempsPartiel)SuperFinder.objetForGlobalIDDansEditingContext(gidTempsPartiel,defaultEditingContext());
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
		try {
			Class[] classeParametres	=  new Class[] {EOTempsPartiel.class,NSArray.class};
			Object[] parametres = new Object[]{tempsPartiel,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteTempsPartiel",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerArreteRepriseTempsPlein(EOGlobalID gidReprise, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EORepriseTempsPlein reprise = (EORepriseTempsPlein)SuperFinder.objetForGlobalIDDansEditingContext(gidReprise,editingContext);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
		try {
			Class[] classeParametres	=  new Class[] {EORepriseTempsPlein.class,NSArray.class};
			Object[] parametres = new Object[]{reprise,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteRepriseTempsPlein",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}

	public Boolean clientSideRequestImprimerArreteMiTempsTherapeutique(EOGlobalID gidMtt, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EOMiTpsTherap object = (EOMiTpsTherap)SuperFinder.objetForGlobalIDDansEditingContext(gidMtt,editingContext);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires, editingContext);
		try {
			Class[] classeParametres	=  new Class[] {EOMiTpsTherap.class,NSArray.class};
			Object[] parametres = new Object[]{object,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteMiTempsTherapeutique",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}


	// Autres
	/** Impression avec un module Jasper comportant des requetes SQL */
	public Boolean clientSideRequestImprimerAvecModule(String nomModule,NSDictionary valeursParametres) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerAvecModule : " + nomModule);
		try {
			Connection connection = null;
			EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
			if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
				connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
			}
			if (connection != null) {
				Class[] classeParametres =  new Class[] {java.sql.Connection.class,String.class,NSDictionary.class};
				Object[] parametres = new Object[]{connection,nomModule,valeursParametres};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPdfAvecModule",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} else {
				return new Boolean(false);
			}
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression avec un module Jasper comportant des requetes SQL */
	public Boolean clientSideRequestImprimerFichierExcelAvecModule(String nomModule,NSDictionary valeursParametres) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerAvecModule : " + nomModule);
		try {
			Connection connection = null;
			EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
			if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
				connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
			}
			if (connection != null) {
				Class[] classeParametres =  new Class[] {java.sql.Connection.class,String.class,NSDictionary.class};
				Object[] parametres = new Object[]{connection,nomModule,valeursParametres};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFichierExcelAvecModule",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} else {
				return new Boolean(false);
			}
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param individusGid
	 * @return
	 */
	public Boolean clientSideRequestImprimerFichesIndividu(NSArray individusGid) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFichesIndividu - impression des fiches");
		EOEditingContext editingContext = getNewEditingContext();
		NSArray individus = Utilitaires.tableauObjetsMetiers(individusGid, editingContext);
		java.util.Enumeration<EOIndividu> e = individus.objectEnumerator();
		String texte = "";
		while (e.hasMoreElements()) {
			EOIndividu individu = e.nextElement();
			if (texte.length() > 0) {
				texte += ",";
			} 
			texte += individu.noIndividu();
		}
		texte = "where NO_INDIVIDU in (" + texte + ")";
		NSDictionary dict = new NSDictionary(texte,"NO_INDIVIDUS");
		Boolean result = clientSideRequestImprimerAvecModule(FICHES_IDENTITES,dict);
		if (result.booleanValue() == false) {
			return result;
		}
		return new Boolean(true);

	}

	public Boolean clientSideRequestImprimerAnciennete(EOGlobalID individuID,NSTimestamp dateReference, Boolean estAncienneteComplete) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerAnciennete");
		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		try {
			Class[] classeParametres =  new Class[] {EOIndividu.class,NSTimestamp.class,Boolean.class};
			Object[] parametres = new Object[]{individu,dateReference,estAncienneteComplete};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerAnciennete",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerServices(EOGlobalID serviceID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerServices");
		EOEditingContext editingContext = getNewEditingContext();
		EOStructure structure = (EOStructure)SuperFinder.objetForGlobalIDDansEditingContext(serviceID, editingContext);
		try {
			Class[] classeParametres =  new Class[] {EOStructure.class};
			Object[] parametres = new Object[]{structure};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerServices",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerArreteNbi(EOGlobalID occupationID,NSArray destinataires,NSDictionary dict) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerArreteNbi");
		EONbiOccupation occupation = (EONbiOccupation)recordPourGlobalID(occupationID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,occupation.editingContext());
		try {
			Class[] classeParametres =  new Class[] {EONbiOccupation.class,NSArray.class,NSDictionary.class};
			Object[] parametres = new Object[]{occupation,destins,dict};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteNbi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerBeneficiairesNbi(NSTimestamp dateReference) {
		try {
			Class[] classeParametres =  new Class[] {NSTimestamp.class};
			Object[] parametres = new Object[]{dateReference};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerBeneficiairesNbi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerHistoriqueNbi(NSTimestamp dateDebut,NSTimestamp dateFin) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerHistoriqueNbi");
		try {
			Class[] classeParametres =  new Class[] {NSTimestamp.class,NSTimestamp.class};
			Object[] parametres = new Object[]{dateDebut,dateFin};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerHistoriqueNbi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerFonctionsNbi(NSTimestamp dateReference) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFonctionsNbi");
		try {
			Class[] classeParametres =  new Class[] {NSTimestamp.class};
			Object[] parametres = new Object[]{dateReference};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFonctionsNbi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	public NSDictionary clientSideRequestImprimerFicheElectraSansThread(EOGlobalID individuID) {

		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);

		Connection connection = null;
		EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
		if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext)
			connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();

		if (connection != null) {
			NSDictionary dict = new NSDictionary(individu.noIndividu(),"NO_INDIVIDU");

			NSDictionary datasImpression = Imprimeur.sharedInstance().imprimerPdfAvecModule(connection, FICHE_IDENTITE_ELECTRA, dict, null);			
			return datasImpression;

		}

		return null;
	}

	//	Avec Thread
	public Boolean clientSideRequestImprimerFicheCIR(EOGlobalID individuID) {

		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);

		NSDictionary dict = new NSDictionary(individu.noIndividu(), EOIndividu.NO_INDIVIDU_COLKEY);
		Boolean result = clientSideRequestImprimerAvecModule(FICHE_IDENTITE_CIR,dict);
		if (result.booleanValue() == false) {
			return result;
		}
		return new Boolean(true);
	}


	//	Avec Thread
	public Boolean clientSideRequestImprimerFicheIdentite(EOGlobalID individuID,NSArray elementsAImprimer,Boolean peutImprimerInfoPerso) {

		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFicheIdentite - impression fiche identite");
		NSDictionary dict = new NSDictionary(individu.noIndividu(), EOIndividu.NO_INDIVIDU_COLKEY);
		Boolean result = clientSideRequestImprimerAvecModule(FICHE_IDENTITE_INDIVIDU,dict);
		if (result.booleanValue() == false) {
			return result;
		}

		return new Boolean(true);
	}
	public NSDictionary clientSideRequestImprimerBlocNotesSansThread(InfoPourEditionRequete info,EOGlobalID individuID) {
		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		// sans thread
		EOEditingContext editingContext = getNewEditingContext();
		NSArray notes = EOBlocNotes.rechercherNotesPourIndividuEtPeriode(editingContext, individu,info.debutPeriode(),info.finPeriode());
		return Imprimeur.sharedInstance().imprimerBlocNotes(info,notes);
	}
	public NSDictionary clientSideRequestImprimerSyntheseCarriereSansThread(EOGlobalID individuID) {
		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		return Imprimeur.sharedInstance().imprimerSyntheseCarriere(individu);
	}
	public Boolean clientSideRequestImprimerSyntheseCarriere(EOGlobalID individuID) {
		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		try {
			Class[] classeParametres =  new Class[] {EOIndividu.class};
			Object[] parametres = new Object[]{individu};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerSyntheseCarriere",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param anneePromotion
	 * @param promotionsID
	 * @param titre
	 * @param sousTitre
	 * @return
	 */
	public Boolean clientSideRequestImprimerPromotions(Integer anneePromotion,NSArray promotionsID,String titre,String sousTitre) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerPromotions");
		try {                        
			EOEditingContext editingContext = getNewEditingContext();

			NSArray promotions = Utilitaires.tableauObjetsMetiers(promotionsID,editingContext);

			Class[] classeParametres =  new Class[] {NSTimestamp.class,NSTimestamp.class,NSArray.class,String.class,String.class};
			Object[] parametres = new Object[]{
					DateCtrl.debutAnnee(anneePromotion),
					DateCtrl.finAnnee(anneePromotion),
					promotions,titre,sousTitre};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPromotions",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}

	public Boolean clientSideRequestimprimerFicheLATA(EOGlobalID promotionID,NSTimestamp dateReference) {

		try {                                    
			EOEditingContext editingContext = getNewEditingContext();
			EOHistoPromotion promotion = (EOHistoPromotion)SuperFinder.objetForGlobalIDDansEditingContext(promotionID, editingContext);

			Class[] classeParametres	=  new Class[] {EOHistoPromotion.class, NSTimestamp.class};
			Object[] parametres = new Object[]{promotion,dateReference};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFicheLATA",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	
	private EOGenericRecord recordPourGlobalID(EOGlobalID globalID) {
		// ne pas travailler dans l'editing context de la session cela bloque la session (côté serveur et client) dans certains cas
		return SuperFinder.objetForGlobalIDDansEditingContext(globalID,getNewEditingContext());
	}
    
}
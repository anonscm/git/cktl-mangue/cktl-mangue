package org.cocktail.mangue.server;
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
import java.util.Enumeration;
import java.util.TimeZone;

import org.cocktail.application.serveur.CocktailApplication;
import org.cocktail.application.serveur.VersionCocktailApplication;
import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.util.EOModelCtrl;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EODbVersion;

import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.woinject.WOInject;

import er.extensions.appserver.ERXApplication;
import er.extensions.eof.ERXEC;

public class Application extends CocktailApplication {

	public static String[] args;
	
	public static final String FEATURE_SERVICES_VALIDES = "cocktail.feature.servicesValides";
	public static final String FEATURE_ADRESSES_NORMEES = "cocktail.feature.adressesNormees";

	private NSMutableDictionary appParametres;
	private RemoteLogManager logManager;
	private VersionCocktailApplication _appVersionCocktail;
	private VersionOracleUser _dbVersion;
	public static final boolean SHOWSQLLOGS    = false;
	private Version _appVersion;

	private	NSMutableArray utilisateursConnectes = new NSMutableArray();

	public static void main(String argv[]) {
		NSLegacyBundleMonkeyPatch.apply();
		WOInject.init("org.cocktail.mangue.server.Application", argv);
	}
	public Application() {

		super();
				
		String timeZone = defaultTimeZone();
		TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
		NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName(timeZone,false));
		String directoryImpression = "";
		
		System.setProperty("java.awt.headless","true");
		// installer le receveur des invocations de méthodes distantes
		logManager = new RemoteLogManager();
		CktlConfig cfg = ((CktlWebApplication) ERXApplication.application()).config();
		if (cfg != null) {
			String mode = cfg.stringForKey("MODE_CONSOLE");
			//mode = "N";
			String modeDebug = cfg.stringForKey("MODE_DEBUG");
			if (outputPath() != null) {
			}
			LogManager.preparerLog("LogMangueServeur",mode,modeDebug);
			LogManager.logInformation("Bienvenue dans " + this.name() + "!");
			LogManager.logInformation("OS : " + System.getProperty("os.name"));
			directoryImpression = getParam("DIRECTORY_IMPRESSION");
			if (directoryImpression != null) {
				LogManager.logInformation("Directory Impression de Mangue.config : " + directoryImpression);
			} else {
				LogManager.logInformation("Directory Impression de Properties : " + directoryImpression);
			}
			LogManager.logInformation("Mode Console " + mode + ", Mode Debug " + modeDebug);
			LogManager.logDetail("Default Time Zone " + TimeZone.getDefault());
			LogManager.logDetail("Default NSTime Zone " + NSTimeZone.defaultTimeZone());
		}
		// vérifier la version de la base
		EODbVersion dbVersion = EODbVersion.derniereVersionBase(ERXEC.newEditingContext());
		if (dbVersion == null) {
			LogManager.logInformation("La table DB_VERSION n'est pas definie dans le user Mangue");
		} else if (dbVersion.dbVersionLibelle().equals(VersionMe.MINDBVERSION) == false) {
			LogManager.logDetail("Version minimum de la base pour l'application " + VersionMe.MINDBVERSION);
			String texte = "Version de la table DB_VERSION : " + dbVersion.dbVersionLibelle();
			if (dbVersion.dbVersionDate() != null) {
				texte = texte + ", date version " + DateCtrl.dateToString(dbVersion.dbVersionDate());
			}
			if (dbVersion.dbInstallDate() != null) {
				texte = texte + ", date installation " + DateCtrl.dateToString(dbVersion.dbInstallDate());
			}
			System.out.println("Application.Application() Version / Installation : "  + texte);
		}

		if (showSqlLogs()) {
			NSLog.setAllowedDebugLevel(NSLog.DebugLevelDetailed);
			NSLog.allowDebugLoggingForGroups(-1);
		}

		// fix pour que les WebServerResources marchent en javaclient
		if (isDirectConnectEnabled()) {
			registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
		}

		// Programmation de differentes taches
		
		String heureCalcul = "";
		if (config().valueForKey("HEURE_MAJ_SERVICES") != null)
			heureCalcul = (String)config().valueForKey("HEURE_MAJ_SERVICES");

		try {
			
			CalcAffectationsTaskController.sharedInstance(new EOEditingContext()).programmer(heureCalcul);

			CalcCirTaskController.sharedInstance(new EOEditingContext()).programmer();

			if ( (config().valueForKey("MAJ_ABSENCES") != null) && ((String)config().valueForKey("MAJ_ABSENCES")).equals("O") ) {
				System.out.println(" ==> Mise à jour de la table MANGUE.ABSENCES ...");
				EOUtilities.executeStoredProcedureNamed(new EOEditingContext(),"majMangueEvenements",null);
				System.out.println(" Fin de la mise à jour de la table MANGUE.ABSENCES ! <==");
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}

		EOGrhumParametres.initParametres(new EOEditingContext());
		
		System.out.println(">>> Répertoire d'enregitrement des éditions : " + directoryImpression);
		System.out.println("\t\t Default Impressions : " + defaultDirectoryJasper());
		System.out.println("\t\t Impressions locales : " + directoryJasperLocal());
		System.out.println("\t\t Default Modules Impressions : " + defaultModuleImpression());
		System.out.println("\t\t Modules Impressions locaux : " + directoryModulesImpressionLocal());
		
	}

	/**
	 * 
	 * @return
	 */
	protected boolean showSqlLogs() {
		if     (config().valueForKey("SHOWSQLLOGS") == null) {
			return SHOWSQLLOGS ;
		}
		return config().valueForKey("SHOWSQLLOGS").equals("O");
	} 

	/**
	 * 
	 */
	public void rawLogModelInfos() {
		final NSMutableDictionary mdlsDico = EOModelCtrl.getModelsDico();
		final StringBuffer sb = new StringBuffer("Informations modeles\n--------------------\n\n");
		final Enumeration<String> mdls = mdlsDico.keyEnumerator();
		while (mdls.hasMoreElements()) {
			final String mdlName = (String) mdls.nextElement();
			sb.append("  > Modele ").append(mdlName).append(" :\n");
			sb.append("    * Connexion base de donnees : ").append(EOModelCtrl.bdConnexionUrl((EOModel) mdlsDico.objectForKey(mdlName))).append("\n");
			sb.append("    * Instance : ").append(EOModelCtrl.bdConnexionServerId((EOModel) mdlsDico.objectForKey(mdlName))).append("\n");
			sb.append("    * User base de donnees : ").append(EOModelCtrl.bdConnexionUser((EOModel) mdlsDico.objectForKey(mdlName))).append("\n");
			sb.append("    * Chemin : ").append(((EOModel) mdlsDico.objectForKey(mdlName)).pathURL()).append("\n");
			sb.append("\n");                        
		}
		sb.append("\n");
		System.out.println("Application.rawLogModelInfos() " + sb.toString());
	} 


	public VersionCocktailApplication appVersionCocktail() {
		if (_appVersionCocktail == null) {
			_appVersionCocktail = new VersionCocktailApplication();
		}
		return _appVersionCocktail;
	}
	public VersionOracleUser appVersionCocktailDb() {
		if (_dbVersion == null) {
			_dbVersion = new VersionOracleUser();
		}
		return _dbVersion;
	}
	
	public A_CktlVersion appCktlVersion() {
		if (_appVersion == null) {
			_appVersion = new Version();
		}
		return _appVersion;
	}

	/**
	 * Repertoire de depot de toutes les editions generees dans Mangue (PDF et XML)
	 * @return filePath
	 */
	public String directoryImpression() {
		return getParam("DIRECTORY_IMPRESSION");
	}

	
	/**
	 * Repertoire de depot par defaut des maquettes d'edition
	 * @return filePath
	 */
	public String defaultDirectoryJasper() {
		return path().concat("/Contents/Resources/Impressions/") ;
	}
	/**
	 * Repertoire de depot local des maquettes d'edition
	 * @return filePath
	 */
	public String directoryJasperLocal() {
		return getParam("DIRECTORY_JASPER");
	}

	/**
	 * Repertoire de depot par defaut des maquettes de modules d'edition
	 * @return filePath
	 */
	public String defaultModuleImpression() {
		return path().concat("/Contents/Resources/Mangue_Impressions/") ;
	}
	/**
	 * Repertoire de depot local des maquettes de modules d'edition
	 * @return filePath
	 */
	public String directoryModulesImpressionLocal() {
		return getParam("DIRECTORY_MODULE_IMPRESSION");
	}

	
	/**
	 * 
	 * @param paramKey
	 * @return
	 */
	public String getParam(String paramKey) {
		NSArray<String> a = (NSArray<String>) appParametres().valueForKey(paramKey);
		String res = null;
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			res = config().stringForKey(paramKey);
		}
		else {
			res = a.get(0);
		}
		return res;
	}
	/** 
	 * fixe le nom du fichier de config 
	 */
	public String configFileName() {
		return "Mangue.config";
	}
	public String configTableName() {
		return "GrhumParametres";
	}
	public String mainModelName() {return "Mangue";}

	// méthodes privées
	public String parametresTableName() {
		return null;
	}

	/**
	 * 
	 * @return
	 */
	private NSMutableDictionary appParametres() {
		if (appParametres == null) {
			if (EOSharedEditingContext.defaultSharedEditingContext() == null || parametresTableName() == null) {
				return new NSMutableDictionary();
			}
			appParametres = new NSMutableDictionary();
			NSArray vParam = dataBus().fetchArray(parametresTableName(), null, null);
			EOGenericRecord vTmpRec;
			String previousParamKey = null;
			NSMutableArray a = null;
			for (java.util.Enumeration<EOGenericRecord> enumerator = vParam.objectEnumerator();enumerator.hasMoreElements();) {
				vTmpRec = enumerator.nextElement();
				if (vTmpRec.valueForKey("paramKey") == null || ((String) vTmpRec.valueForKey("paramKey")).equals("")
						|| vTmpRec.valueForKey("paramValue") == null) {
					continue;
				}
				if (!((String) vTmpRec.valueForKey("paramKey")).equalsIgnoreCase(previousParamKey)) {
					if (a != null && a.count() > 0) {
						appParametres.setObjectForKey(a, previousParamKey);
					}
					previousParamKey = (String) vTmpRec.valueForKey("paramKey");
					a = new NSMutableArray();
				}
				if (vTmpRec.valueForKey("paramValue") != null) {
					a.addObject((String) vTmpRec.valueForKey("paramValue"));
				}
			}
			if (a != null && a.count() > 0) {
				appParametres.setObjectForKey(a, previousParamKey);
			}
			EOSharedEditingContext.defaultSharedEditingContext().invalidateAllObjects();
		}
		return appParametres;
	}
	public String version() {return VersionMe.appliVersion();}
	public String copyright() {return "@c";}

	/**
	 * 
	 * Recuperation du time zone par defaut.
	 * Recuperation du timezone du config, puis GRHUM_PARAMETRES si inexistant et sinon GMT.s
	 * 
	 * @return
	 */
	private String defaultTimeZone() {

		// config
		String valeur = getParam("DEFAULT_TIME_ZONE");

		if (valeur == null) {
			EOEditingContext editingContext = ERXEC.newEditingContext();
			valeur = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_TIME_ZONE);
			if (valeur == null) {
				valeur = "GMT";
			}
		}

		System.out.println("Application.defaultTimeZone() DEFAULT TIME ZONE : " + valeur);

		return valeur; 
	}
}	
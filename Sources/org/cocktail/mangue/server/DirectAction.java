package org.cocktail.mangue.server;
//
// DirectAction.java
// Project Gepeto
//
// Created by christine on Wed Apr 13 2005
//

import org.cocktail.fwkcktlwebapp.server.CktlWebAction;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WORequest;
import com.webobjects.foundation.NSDictionary;

public class DirectAction extends CktlWebAction {

    public DirectAction(WORequest aRequest) {
        super(aRequest);
    }

    public WOActionResults defaultAction() {
        return pageWithName("Main");
    }

	@Override
	public WOActionResults loginCasFailurePage(String arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WOActionResults loginCasSuccessPage(String arg0, NSDictionary arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WOActionResults loginNoCasPage(NSDictionary arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}

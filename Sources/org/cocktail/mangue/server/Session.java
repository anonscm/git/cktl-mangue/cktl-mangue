package org.cocktail.mangue.server;

//Session.java
//Project Gepeto

//Created by christine on Wed Apr 13 2005


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;

import org.cocktail.application.serveur.CocktailSession;
import org.cocktail.common.LogManager;
import org.cocktail.crypto.jcrypt.Jcrypt;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.CriteresEvaluationPrime;
import org.cocktail.mangue.common.primes.PrimeCalcul;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.DureeAvecArrete;
import org.cocktail.mangue.modele.EtatComparatifCarriere;
import org.cocktail.mangue.modele.IndividuPourEdition;
import org.cocktail.mangue.modele.IndividuPromouvable;
import org.cocktail.mangue.modele.IndividuPromouvableEc;
import org.cocktail.mangue.modele.InfoPourEditionMedicale;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPrimeBudget;
import org.cocktail.mangue.modele.goyave.EOPrimeExercice;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.PrimeResultat;
import org.cocktail.mangue.modele.goyave.primes.PrimeResultatCalcul;
import org.cocktail.mangue.modele.goyave.primes.PrimeResultatEligibilite;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageChevron;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.elections.EOTypeInstance;
import org.cocktail.mangue.modele.grhum.referentiel.EOBlocNotes;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOParamPromotion;
import org.cocktail.mangue.modele.mangue.EOSupInfoFichier;
import org.cocktail.mangue.modele.mangue.cir.EOCirFichierImport;
import org.cocktail.mangue.modele.mangue.conges.EOCongeGardeEnfant;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamRepartElec;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOHistoPromotion;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;
import org.cocktail.mangue.modele.mangue.individu.medical.EOExamenMedical;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVaccin;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVisiteMedicale;
import org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste;
import org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf;
import org.cocktail.mangue.modele.mangue.lolf.EOPoste;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.modele.mangue.modalites.EOMiTpsTherap;
import org.cocktail.mangue.modele.mangue.modalites.EORepriseTempsPlein;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;
import org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation;
import org.cocktail.mangue.modele.mangue.prolongations.EOEmeritat;
import org.cocktail.mangue.server.cir.AutomateCir;
import org.cocktail.mangue.server.cir.AutomateCirCertification;
import org.cocktail.mangue.server.cir.ConstantesCir;
import org.cocktail.mangue.server.cir.SousRubriqueCir;
import org.cocktail.mangue.server.dif.AutomateDif;
import org.cocktail.mangue.server.impression.Imprimeur;
import org.cocktail.mangue.server.supinfo.AutomatePromoEC;
import org.cocktail.mangue.server.supinfo.AutomateSupinfo;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.EODistributionContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXProperties;

public class Session extends CocktailSession {

	private ServerThreadManager threader;
	Application	myApp;
	private String loginAgent;

	private final static String FICHES_IDENTITES = "FichesIdentites";
	private final static String FICHE_IDENTITE_INDIVIDU = "FicheIdentiteIndividu";
	private final static String FICHE_IDENTITE_CIR = "FicheIdentiteCir";
	private final static String FICHE_IDENTITE_ELECTRA = "FicheIdentiteElectra";

	public SessionPrint remoteCallPrint;

	public Session() {

		super();
		myApp = (Application)Application.application();

		defaultEditingContext().setDelegate(this);
		remoteCallPrint = new SessionPrint(myApp, defaultEditingContext());

	}

	public void clientSideRequestMsgToServer(String msg) {
	}
	private EOEditingContext getNewEditingContext() {
		return ERXEC.newEditingContext();
	}

	public String clientSideRequestBdConnexionName() {return woApplication.bdConnexionName();}
	public String clientSideRequestAppVersion() { return VersionMe.txtAppliVersion();}
	public void clientSideRequestRevert() { defaultEditingContext().revert(); }

	public boolean editingContextShouldPresentException(EOEditingContext context,Throwable exception) {
		LogManager.logException(exception);
		return true;
	}

	/**
	 * Pour ajouter automatiquement les dates de création/modification
	 * @param ec
	 * @return
	 */
	public boolean editingContextShouldValidateChanges(EOEditingContext ec) {
		try {
			for (java.util.Enumeration<EOEnterpriseObject> e = ec.insertedObjects().objectEnumerator();e.hasMoreElements();) {
				EOGenericRecord record = (EOGenericRecord)e.nextElement();
				updateDate(record,"dCreation");
				updateDate(record,"dModification");
			}
			//	System.out.println("updated " + ec.updatedObjects());
			for (java.util.Enumeration<EOEnterpriseObject> e1 = ec.updatedObjects().objectEnumerator();e1.hasMoreElements();) {
				updateDate(e1.nextElement(),"dModification");
			}
			//	System.out.println("deletedObjects " + ec.deletedObjects());
			for (java.util.Enumeration<EOEnterpriseObject> e2 = ec.deletedObjects().objectEnumerator();e2.hasMoreElements();) {
				updateDate(e2.nextElement(),"dModification");
			}			
		} catch (Exception e) {
			LogManager.logException(e);
		} finally {
		}
		return true;	// les objets métier sont partagés côté client et serveur, on n'aurait donc pas besoin de refaire les validations
		// mais dans certains rares cas, des modifications sont faites dans validateForSave, il faut donc que du côté serveur, on fasse les validations
	}


	private void updateDate(EOEnterpriseObject record,String cle) {
		if (record.classDescription().classForAttributeKey(cle) != null) {
			record.takeValueForKey(new NSTimestamp(),cle);
		}
	}

	public boolean distributionContextShouldFollowKeyPath(EODistributionContext distributionContext, String path) {
		return (path.startsWith("session"));
	}

	public String clientSideRequestGetCryptPassword(String passwdClair) {
		return Jcrypt.crypt(passwdClair);
	}

	public void clientSideRequestStartProgCir(EOGlobalID idFichier) {
		EOCirFichierImport fichier = (EOCirFichierImport)defaultEditingContext().objectForGlobalID(idFichier);
		CalcCirTaskController.sharedInstance(defaultEditingContext()).startProgrammation(fichier);
	}

	public void clientSideRequestStopProgCir() {
		CalcCirTaskController.sharedInstance(defaultEditingContext()).stopProgrammation();
	}

	/** retourne un parametre
	 * 
	 * @param paramKey
	 * @return
	 */
	public String clientSideRequestGetParam(String paramKey) {
		return ((Application)WOApplication.application()).getParam(paramKey);
	}
	public boolean clientSideRequestGetBooleanParam(String paramKey) {
		return ERXProperties.booleanForKeyWithDefault(paramKey, false);
	}
	/**  
	 * Recuperation d'une cle primaire 
	 *
	 * @param eo Objet pour lequel on veut recuperer la cle primaire
	 *
	 * @return Retourne un dictionnaire contenant les cles primaires et leur valeur
	 */
	public  NSDictionary clientSideRequestPrimaryKeyForObject(EOEnterpriseObject eo) 	{
		NSDictionary myResult = EOUtilities.primaryKeyForObject(defaultEditingContext(),eo);
		return myResult;
	}
	/** Rapatrie les fichiers CIR sous la forme d'un dictionnaire contenant comme cle le nom du fichier et comme valeur un tableau de lignes (String) */
	public NSDictionary clientSideRequestRecupererFichiersCIR() {
		return AutomateCir.recupererFichierCIR(System.getProperty("java.io.tmpdir"));//WOApplication.application().path());		
	}

	/** Retourne le nom de la base utilisee */
	public String clientSideRequestRecupererNomBase() {
		EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		EOModel vModel = vModelGroup.modelNamed("Mangue");
		NSDictionary vDico = vModel.connectionDictionary();
		NSArray url = NSArray.componentsSeparatedByString((String)vDico.valueForKey("URL"),"@");
		if (url.count() == 0)
			return " Connexion ??";
		if (url.count() == 1)
			return (String)url.objectAtIndex(0);
		return (String)(NSArray.componentsSeparatedByString((String)vDico.valueForKey("URL"),"@")).objectAtIndex(1); 
	}

	public String clientSideRequestRecupererNomTable(String nomEntite) {
		EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		EOModel vModel = vModelGroup.modelNamed("Mangue");
		if (vModel != null) {
			return vModel.entityNamed(nomEntite).externalName();
		} else {
			return null;
		}
	}
	/** Change la date de modification d'un individu */
	public Boolean clientSideRequestChangerDateModificationPourIndividu(Number noIndividu) {
		return SQLDirect.changerDateModificationPourIndividu(new EOEditingContext(),noIndividu);
	}
	/** Recupere le login de l'agent en vue de l'utiliser lors des impressions */
	public void clientSideRequestSetAgent(String unNom) {
		LogManager.logInformation(DateCtrl.dateToString(new NSTimestamp()));
		loginAgent = unNom;
		LogManager.logDetail(">>>>>>>>> AGENT CONNECTE " + unNom);
		remoteCallPrint.setLoginAgent(unNom);
	}

	/**
	 * 
	 * @param pageAide
	 * @return
	 */
	public String clientSideRequestHtmlPourPageAide(String pageAide) {
		WOComponent page = WOApplication.application().pageWithName(pageAide,context());
		WOResponse response = page.generateResponse();
		NSData data = response.content();
		return new String(data.bytes(0, data.length()));

	}
	public Boolean clientSideRequestExisteDocumentAide(String documentAide) {
		InputStream stream = null;
		try {
			stream = WOApplication.application().resourceManager().inputStreamForResourceNamed(documentAide + ".pdf",null,null);
			return new Boolean(true);
		}
		catch (Exception e) {
			// on est en version 5.2.1
			String path = WOApplication.application().path() + "/Contents/Resources/" + documentAide + ".pdf";
			File pdfFile = new File(path.toString());
			try {
				stream = new FileInputStream(pdfFile);
				return new Boolean(true);
			}
			catch (Exception e1) {
				return new Boolean(false);
			}
		}
	}
	/** Evaluation du login, password, email, domaine,uid d'un individu en utilisant des proc&eacute;dures
	 * stock&eacute;es de Grhum */
	public NSDictionary clientSideRequestEvaluerInformationsCompte(String nom,String prenom) {
		EOEditingContext editingContext = getNewEditingContext();
		NSMutableDictionary dict = new NSMutableDictionary(), dictInfo = new NSMutableDictionary();
		dict.setObjectForKey(nom, "nom");
		dict.setObjectForKey(prenom, "prenom");
		NSDictionary result = EOUtilities.executeStoredProcedureNamed(editingContext,"consLogin",dict);
		String login = (String)result.objectForKey("login");
		if (login != null) {
			dictInfo.setObjectForKey(login, "login");
		}
		result = EOUtilities.executeStoredProcedureNamed(editingContext,"consPasswd",new NSDictionary());
		String password = (String)result.objectForKey("password");
		if (password != null) {
			dictInfo.setObjectForKey(password, "password");
		}
		result = EOUtilities.executeStoredProcedureNamed(editingContext,"consEmailLong",dict);
		String emailLong = (String)result.objectForKey("emailLong");
		if (emailLong != null) {
			int index = emailLong.indexOf("@");
			if (index > 0) {
				dictInfo.setObjectForKey(emailLong.substring(0,index),"email");
				dictInfo.setObjectForKey(emailLong.substring(index + 1),"domaine");
			}
		}
		result = EOUtilities.executeStoredProcedureNamed(editingContext,"uidLibre",new NSDictionary());
		Number uid = (Number)result.objectForKey("uid");
		if (uid != null) {
			dictInfo.setObjectForKey(uid,"uid");
		}
		return dictInfo;
	}
	// Traitements des difs
	public Boolean clientSideRequestCreerDifs(Integer annee, Integer typePopulation,Integer typePersonnel) {
		LogManager.logDetail("clientSideRequestCreerDifs pour annee " + annee + ", population " + typePopulation + ", personnel " + typePersonnel);
		try {
			Class[] classeParametres =  new Class[] {Integer.class,Integer.class,Integer.class};
			Object[] parametres = new Object[]{annee,typePopulation,typePersonnel};
			threader = new ServerThreadManager(RemoteServer.sharedInstance(),"creerDifsPourAnneeTypePopulationEtTypePersonnel",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}


	/**
	 * 
	 * @param fichierGID
	 * @param parametresID
	 * @param gidIndividu
	 * @return
	 */
	public String clientSideRequestPreparerPromoEcManuelle(EOGlobalID fichierGID, NSArray parametresID, EOGlobalID gidIndividu) {

		EOEditingContext edc = getNewEditingContext();

		EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(gidIndividu,edc);
		NSArray parametresPromotion = Utilitaires.tableauObjetsMetiers(parametresID, edc);

		EOSupInfoFichier fichier = (EOSupInfoFichier)SuperFinder.objetForGlobalIDDansEditingContext(fichierGID, edc);

		try {
			AutomatePromoEC myAutomate = new AutomatePromoEC(edc);
			int nbComptes = myAutomate.preparerPromECManuelle(edc, fichier, parametresPromotion, individu);
			return nbComptes + " créé(s) pour " + individu.identitePrenomFirst();
		} catch (Exception e) {
			LogManager.logException(e);
			e.printStackTrace();
			return "Une exception s'est produite " + e.getMessage();
		} 

	}


	/**
	 * 
	 * @param fichierGID
	 * @param parametresID
	 * @return
	 */
	public Boolean clientSideRequestPreparerRecordsPromEC(Integer annee, NSArray<EOGlobalID> parametresID) {

		EOEditingContext edc = getNewEditingContext();

		try {

			NSArray<EOParamPromotion> parametresPromotion = Utilitaires.tableauObjetsMetiers(parametresID, edc);

			Class[] classeParametres =  new Class[] {Integer.class, NSArray.class};
			Object[] parametres = new Object[]{annee, parametresPromotion};

			threader = new ServerThreadManager(AutomatePromoEC.sharedInstance(edc) ,"preparer",classeParametres, parametres);
			threader.start();

			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * CIR - Certifications d'identite
	 * @param annee
	 * @return
	 */
	public Boolean clientSideRequestPreparerCertifications(Integer annee) {

		try {

			Class[] classeParametres =  new Class[] {Integer.class};
			Object[] parametres = new Object[]{annee};

			threader = new ServerThreadManager(AutomateCirCertification.sharedInstance(defaultEditingContext())
					,"preparerCertifications",classeParametres,parametres);
			threader.start();

			return new Boolean(true);

		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param pathFichier
	 * @param gidFichierImport
	 * @param individusId
	 * @param estRapportDetaille
	 * @return
	 */
	public Boolean clientSideRequestPreparerRecordsCir(EOGlobalID gidFichierImport, NSArray individusId,Boolean estRapportDetaille) {

		EOEditingContext editingContext = getNewEditingContext();

		EOCirFichierImport fichierImport = (EOCirFichierImport)SuperFinder.objetForGlobalIDDansEditingContext(gidFichierImport, editingContext);

		URL path = WOApplication.application().resourceManager().pathURLForResourceNamed("DescriptionCir.XML",null,null);

		try {
			Class[] classeParametres =  new Class[] {EOCirFichierImport.class,NSArray.class,String.class,Boolean.class};
			Object[] parametres = new Object[]{fichierImport,Utilitaires.tableauObjetsMetiers(individusId, editingContext),path.getPath(),estRapportDetaille};
			threader = new ServerThreadManager(AutomateCir.sharedInstance(),"preparerRecords",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	
	/**
	 * Préparation des donnees SUPINFO - Le type de remontee est gere dans AutomateSupinfo
	 * @param fichierGid
	 * @param rneGid
	 * @return
	 */
	public Boolean clientSideRequestPreparerDif(Integer anneeCalcul) {

		EOEditingContext edc = getNewEditingContext();
		try {
			
			AutomateDif automate = new AutomateDif(edc);

			Class[] classeParametres =  new Class[] {Integer.class};
			Object[] parametres = new Object[]{anneeCalcul};

			threader = new ServerThreadManager(automate,"preparerDifsPourAnnee",classeParametres,parametres);
			threader.start();

			return true;
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		} 
	}
	/**
	 * Préparation des donnees SUPINFO - Le type de remontee est gere dans AutomateSupinfo
	 * @param fichierGid
	 * @param rneGid
	 * @return
	 */
	public Boolean clientSideRequestPreparerDifPourIndividus(NSDictionary params) {

		EOEditingContext edc = getNewEditingContext();
		try {
			
			AutomateDif automate = new AutomateDif(edc);

			NSArray individus = (NSArray<EOIndividu>)params.objectForKey("individus"); 

			Class[] classeParametres =  new Class[] {Integer.class, NSArray.class};
			Object[] parametres = new Object[]{(Integer)params.objectForKey("anneeCalcul"), individus};

			threader = new ServerThreadManager(automate,"preparerDifsPourAnneeEtIndividus",classeParametres,parametres);
			threader.start();

			return true;
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		} 
	}


	/**
	 * Préparation des donnees SUPINFO - Le type de remontee est gere dans AutomateSupinfo
	 * @param fichierGid
	 * @param rneGid
	 * @return
	 */
	public Boolean clientSideRequestPreparerSupInfo(NSDictionary params) {

		EOEditingContext edc = getNewEditingContext();
		try {
			
			AutomateSupinfo automate = new AutomateSupinfo(edc);

			EOSupInfoFichier fichier = (EOSupInfoFichier)params.objectForKey(EOSupInfoFichier.ENTITY_TABLE_NAME); 
			Class[] classeParametres =  new Class[] {EOSupInfoFichier.class};
			Object[] parametres = new Object[]{fichier};

			threader = new ServerThreadManager(automate,"preparerRemonteeSupInfo",classeParametres,parametres);
			threader.start();

			return true;
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		} 
	}

	// Preparation des donnees SUP-INFO pour un individu 
	public String clientSideRequestPreparerSupInfoPourIndividu(EOGlobalID fichierGid, EOGlobalID individuGid, EOGlobalID rneGid) {

		EOEditingContext editingContext = getNewEditingContext();

		try {

			EORne rneEtablissement = (EORne)SuperFinder.objetForGlobalIDDansEditingContext(rneGid, editingContext);
			EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuGid, editingContext);
			EOSupInfoFichier fichier = (EOSupInfoFichier)SuperFinder.objetForGlobalIDDansEditingContext(fichierGid, editingContext);

			//SupInfoCtrl.preparerRemonteeSupInfoTitulaires(editingContext, fichier, individu, rneEtablissement);

			return "Données Sup Info recalculées pour " + individu.identitePrenomFirst();

		} catch (Exception e) {
			LogManager.logException(e);
			return "Une exception s'est produite " + e.getMessage();
		} 

	}


	/**
	 * 
	 * @param gidFichierImport
	 * @param individusId
	 * @return
	 */
	public Boolean clientSideRequestSupprimerRecordsCir(EOGlobalID gidFichierImport,NSArray individusId) {
		LogManager.logDetail(loginAgent + " : clientSideRequestSupprimerRecordsCir");
		EOEditingContext editingContext = getNewEditingContext();
		EOCirFichierImport fichierImport = (EOCirFichierImport)SuperFinder.objetForGlobalIDDansEditingContext(gidFichierImport, editingContext);
		try {
			Class[] classeParametres =  new Class[] {EOCirFichierImport.class,NSArray.class};
			Object[] parametres = new Object[]{fichierImport,Utilitaires.tableauObjetsMetiers(individusId, editingContext)};
			threader = new ServerThreadManager(AutomateCir.sharedInstance(),"supprimerRecords",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param gidFichierImport
	 * @param pathFichier
	 * @param gidsIndividus
	 * @return
	 * @throws Exception
	 */
	public Boolean clientSideRequestPreparerFichierCir(EOGlobalID gidFichierImport, String pathFichier, NSArray gidsIndividus) throws Exception {
		LogManager.logDetail(loginAgent + " : clientSideRequestPreparerFichierCir");
		EOEditingContext editingContext = getNewEditingContext();
		EOCirFichierImport fichierImport = (EOCirFichierImport)SuperFinder.objetForGlobalIDDansEditingContext(gidFichierImport, editingContext);
		try {
			Class[] classeParametres =  new Class[] {EOCirFichierImport.class,String.class, NSArray.class};
			Object[] parametres = new Object[]{fichierImport, pathFichier, Utilitaires.tableauObjetsMetiers(gidsIndividus, editingContext)};
			threader = new ServerThreadManager(AutomateCir.sharedInstance(),"preparerFichier",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/**
	 * 
	 * @param gidFichierImport
	 * @param pathFichier
	 * @param estFichierCir
	 * @return
	 */
	public String clientSideRequestRapatrierFichierCir(EOGlobalID gidFichierImport, String pathFichier, Boolean estFichierCir) {
		boolean estFichierPrincipal = estFichierCir.booleanValue();
		LogManager.logDetail(loginAgent + " : clientSideRequestRapatrierFichierCir, fichier Cir : " + estFichierPrincipal);
		EOEditingContext editingContext = getNewEditingContext();
		EOCirFichierImport fichierImport = (EOCirFichierImport)SuperFinder.objetForGlobalIDDansEditingContext(gidFichierImport, editingContext);
		return AutomateCir.sharedInstance().contenuFichier(fichierImport, pathFichier, estFichierPrincipal);
	}


	public NSArray clientSideRequestGetSousRubriques(String nomRubrique, String data) throws Exception {

		try {

			URL path = WOApplication.application().resourceManager().pathURLForResourceNamed("DescriptionCir.XML",null,null);

			NSArray sousRubriques = AutomateCir.sharedInstance().sousRubriquesPourRubrique(nomRubrique, path.getPath());
			sousRubriques = EOSortOrdering.sortedArrayUsingKeyOrderArray(sousRubriques, new NSArray(EOSortOrdering.sortOrderingWithKey("numeroOrdre", EOSortOrdering.CompareAscending)));

			NSMutableArray valeursSousRubriques = new NSMutableArray();

			for (int i = 0; i < sousRubriques.count();i++) {

				int index = data.indexOf(ConstantesCir.DELIMITEUR_CIR);

				if (index >= 0) {
					// On ne prend pas en compte la première sous-rubrique car elle est non modifiable, c'est la clé de la sous-rubrique

					String valeur = data.substring(0,index);
					valeursSousRubriques.addObject(new ValeurSousRubrique((SousRubriqueCir)sousRubriques.objectAtIndex(i), valeur));
					if (index < data.length())
						data = data.substring(index + 1);

				}
				else {
					String valeur = data;
					valeursSousRubriques.addObject(new ValeurSousRubrique((SousRubriqueCir)sousRubriques.objectAtIndex(i), valeur));
				}

			}

			NSMutableArray result = new NSMutableArray();
			for (int i=0;i<valeursSousRubriques.count();i++) {

				ValeurSousRubrique valeur = (ValeurSousRubrique)valeursSousRubriques.objectAtIndex(i);

				NSMutableDictionary parametres = new NSMutableDictionary();
				parametres.setObjectForKey( valeur.cle() , "cle");
				parametres.setObjectForKey( valeur.valeur() , "valeur");
				result.addObject(parametres);
			}


			return result.immutableClone();
		}
		catch (Exception e) {
			throw e;
		}

	}

	public NSArray clientSideRequestSousRubriquesPourRubrique(String nomRubrique) {

		URL path = WOApplication.application().resourceManager().pathURLForResourceNamed("DescriptionCir.XML",null,null);

		return AutomateCir.sharedInstance().sousRubriquesPourRubrique(nomRubrique, path.getPath());

	}

	// Promotion

	/** Evalue tous les individus qui peuvent beneficier d'une promotion pour une annee donnee */
	public String clientSideRequestPreparerPromotions(Integer anneePromotion,NSArray parametresID, Boolean rechercherPromotionsEchelon) {
		LogManager.logDetail(loginAgent + " : clientSideRequestPreparerPromotions");
		EOEditingContext editingContext = getNewEditingContext();
		NSArray parametresPromotion = Utilitaires.tableauObjetsMetiers(parametresID, editingContext);
		return EOHistoPromotion.preparerPromotionsPourParametresEtAnnee(editingContext, parametresPromotion, anneePromotion,rechercherPromotionsEchelon.booleanValue());
	}

	/**
	 * Préparation des états comparatifs en vue d'extractions
	 * @param dateDebut
	 * @param dateFin
	 * @param typePersonnel
	 * @param typeEnseignant
	 * @param estComparaisonDebut
	 * @return
	 */
	public NSArray clientSideRequestPreparerEtatsComparatifs(NSTimestamp dateDebut, NSTimestamp dateFin, Integer typePersonnel,Integer typeEnseignant,Boolean estComparaisonDebut) {
		if (dateDebut == null || dateFin == null || typePersonnel == null) {
			return null;
		}
		return EtatComparatifCarriere.preparerEtatsComparatifs(getNewEditingContext(), dateDebut, dateFin, typePersonnel.intValue(),typeEnseignant.intValue(),estComparaisonDebut.booleanValue());
	}

	/**
	 * Calcule le montant des attributions de prime pour ce budget
	 * @param budgetID
	 * @return
	 * @throws Exception
	 */
	public BigDecimal clientSideRequestCalculerEngagePourBudget(EOGlobalID budgetID) throws Exception {

		EOEditingContext editingContext = getNewEditingContext();
		try {
			editingContext.lock();
			EOPrimeBudget budget = (EOPrimeBudget)SuperFinder.objetForGlobalIDDansEditingContext(budgetID, editingContext);
			NSArray primesExercices = EOPrimeExercice.rechercherPrimesExercicesPourBudget(editingContext, budget);
			BigDecimal result = new BigDecimal(0);
			NSTimestamp debutPeriode = DateCtrl.debutAnnee((Integer)budget.pbudExercice());
			NSTimestamp finPeriode = DateCtrl.finAnnee((Integer)budget.pbudExercice());

			NSArray<EOPrime> primes = (NSArray<EOPrime>)primesExercices.valueForKey("prime");

			for (EOPrime prime : primes) {			
				NSArray<EOPrimeAttribution> attributions = EOPrimeAttribution.rechercherAttributionsValidesPourPrimeIndividuEtPeriode(editingContext, null, prime, debutPeriode, finPeriode);
				for (EOPrimeAttribution attribution : attributions) {			
					result = result.add(attribution.montantSurPeriode(debutPeriode,finPeriode));
				}
			}
			return result;
		} catch (Exception exc) {
			exc.printStackTrace();
			throw exc;
		} finally {
			editingContext.unlock();
		}
	}
	/** Calcule le montant des attributions de prime pour ce budget deja consommes en se basant sur les periodes */
	public BigDecimal clientSideRequestCalculerDebitPourBudget(EOGlobalID budgetID) throws Exception {
		EOEditingContext editingContext = getNewEditingContext();
		try {
			editingContext.lock();
			EOPrimeBudget budget = (EOPrimeBudget)SuperFinder.objetForGlobalIDDansEditingContext(budgetID, editingContext);
			NSArray primesExercices = EOPrimeExercice.rechercherPrimesExercicesPourBudget(editingContext, budget);
			BigDecimal result = new BigDecimal(0);
			NSTimestamp debutPeriode = DateCtrl.stringToDate("01/01/" + budget.pbudExercice());
			NSTimestamp finPeriode = DateCtrl.stringToDate("31/12/" + budget.pbudExercice());
			java.util.Enumeration e = ((NSArray)primesExercices.valueForKey("prime")).objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrime prime = (EOPrime)e.nextElement();
				NSArray attributions = EOPrimeAttribution.rechercherAttributionsValidesPourPrimeIndividuEtPeriode(editingContext, null, prime, debutPeriode, finPeriode);
				java.util.Enumeration e1 = attributions.objectEnumerator();
				while (e1.hasMoreElements()) {
					EOPrimeAttribution attribution = (EOPrimeAttribution)e1.nextElement();
					result = result.add(attribution.montantSurPeriode(debutPeriode,finPeriode));
				}
			}
			return result;
		} catch (Exception exc) {
			LogManager.logException(exc);
			throw exc;
		} finally {
			editingContext.unlock();
		}
	}
	// Primes
	/** Retourne la formule de calcul d'une prime
	 *  primeID EOGlobalID de la prime
	 */
	public String clientSideRequestModeCalculPourPrime(EOGlobalID primeID) throws Exception {
		LogManager.logDetail("clientSideRequestModeCalculPourPrime");
		EOEditingContext editingContext = new EOEditingContext();
		try {
			EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext);
			return PrimeCalcul.sharedInstance().modeCalculPourPrime(prime);
		} catch (Exception exc) {
			LogManager.logException(exc);
			throw exc;
		} 
	}
	/** Calcul des attributions de prime en d&eacute;terminant les individus &eacute;ligibles pour cette prime sans les enregistrer dans la base
	Retourne le tableau de r&eacute;sultats obtenus sous la forme de PrimeResultat */
	public NSArray clientSideRequestCalculerResultatsAttributionsPourPrime(EOGlobalID primeID,NSTimestamp debutPeriode,NSTimestamp finPeriode,CriteresEvaluationPrime criteres) throws Exception {
		LogManager.logDetail("clientSideRequestCalculerResultatsAttributionsPourPrime");
		EOEditingContext editingContext = getNewEditingContext();
		try {
			editingContext.lock();
			EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext);
			String message = ", debut periode : " + DateCtrl.dateToString(debutPeriode);
			if (finPeriode != null) {
				message += ", fin periode : " + DateCtrl.dateToString(finPeriode);
			}
			if (criteres != null) {
				// Il contenait des globalIDs
				criteres.transformerEnObjetsMetier(editingContext);
			}
			LogManager.logDetail("clientSideRequestCalculerResultatsAttributionsPourPrime pour prime : " + prime.primLibLong() + message);
			return PrimeCalcul.sharedInstance().resultatsAttributionsPrimePourPeriodeEtCriteres(prime, debutPeriode, finPeriode, criteres);

		} catch (Exception exc) {
			LogManager.logException(exc);
			throw exc;
		} finally {
			editingContext.unlock();
		}
	}
	/** Calcul des attributions de prime en d&eacute;terminant les individus &eacute;ligibles pour cette prime et les enregistre dans la base. 
	 * Lance une exception si une erreur se produit en cours de route */
	public NSArray clientSideRequestCalculerAttributionsPourPrime(EOGlobalID primeID,NSTimestamp debutPeriode,NSTimestamp finPeriode,CriteresEvaluationPrime criteres,Boolean toutRecalculer) throws Exception {
		LogManager.logDetail("clientSideRequestCalculerAttributionsPourPrime");
		EOEditingContext editingContext = getNewEditingContext();
		try {
			editingContext.lock();
			EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext);
			String message = ", debut periode : " + DateCtrl.dateToString(debutPeriode);
			if (finPeriode != null) {
				message += ", fin periode : " + DateCtrl.dateToString(finPeriode);
			}
			if (criteres != null) {
				// Il contenait des globalIDs
				criteres.transformerEnObjetsMetier(editingContext);
			}
			LogManager.logDetail("clientSideRequestCalculerAttributionsPourPrime pour prime : " + prime.primLibLong() + message);
			NSArray attributions = PrimeCalcul.sharedInstance().attributionsPrimePourPeriodeEtCriteres(prime, debutPeriode, finPeriode, criteres, true, toutRecalculer.booleanValue());
			if (attributions != null && attributions.count() > 0) {
				// les enregistrer dans la base
				editingContext.saveChanges();
				return Utilitaires.tableauDeGlobalIDs(attributions, editingContext);
			} else {
				return null;
			}
		} catch (Exception e) {
			LogManager.logException(e);
			throw e;
		} finally {
			editingContext.unlock();
		}
	}
	/** Retourne le r&eacute;sultat de l'&eacute;ligibilit&eacute; d'un individu pour une prime.
	 * @param individuID globalID de l'individu
	 * @param primeID	globalID de la prime
	 * @param debutPeriode	d&eacute;but de la p&eacute;riode d'attribution
	 * @param finPeriode fin de la p&eacute;riode d'attribution
	 * @return
	 */
	public PrimeResultatEligibilite clientSideRequestResultatEligibilitePourPrime(EOGlobalID individuID,EOGlobalID primeID,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		LogManager.logDetail("clientSideRequestResultatEligibilitePourPrime");
		EOEditingContext editingContext = getNewEditingContext();
		try {
			editingContext.lock();
			EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext);
			EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuID, editingContext);
			String message = ", debut periode : " + DateCtrl.dateToString(debutPeriode);
			if (finPeriode != null) {
				message += ", fin periode : " + DateCtrl.dateToString(finPeriode);
			}
			LogManager.logDetail("clientSideRequestCalculerAttributionsPourPrime pour prime : " + prime.primLibLong() + message);
			PrimeResultatEligibilite resultat = PrimeCalcul.sharedInstance().individuEligiblePourPrimeEtPeriode(individu, prime, debutPeriode, finPeriode);
			if (resultat.diagnosticRefus() != null) {
				LogManager.logDetail("L'individu ne peut beneficier de la prime, diagnosic : " + resultat.diagnosticRefus());
			}
			return resultat;
		} catch (Exception e) {
			LogManager.logException(e);
			throw e;
		} finally {
			editingContext.unlock();
		}
	}
	/** Valide un montant personnel pour un individu */
	public String clientSideRequestVerifierValiditeMontantPourIndividuEtPrime(BigDecimal montant,EOGlobalID individuID,EOGlobalID primeID,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		LogManager.logDetail("clientSideRequestVerifierValiditeMontantPourIndividuEtPrime");
		EOEditingContext editingContext = getNewEditingContext();
		EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext);
		EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuID, editingContext);
		String message = ", debut periode : " + DateCtrl.dateToString(debutPeriode);
		message += ", fin periode : " + DateCtrl.dateToString(finPeriode);
		LogManager.logDetail(individu.identite() + ", prime : " + prime.primLibLong() + message);
		String resultat = PrimeCalcul.sharedInstance().verifierMontant(montant,individu,prime,debutPeriode,finPeriode);
		if (resultat == null) {
			LogManager.logDetail("" + montant + " : montant valide");
		} else {
			LogManager.logDetail(resultat);
		}
		return resultat;
	}
	/** Valide des param&egrave;tres personnels pour un individu */
	public String clientSideRequestValiderParametresPersosPourIndividuPrimeEtPeriode(EOGlobalID individuID,EOGlobalID primeID,NSArray parametresPersonnels,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		LogManager.logDetail("clientSideRequestValiderParametresPersosPourIndividuPrimeEtPeriode");
		EOEditingContext editingContext = getNewEditingContext();
		EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext);
		EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuID, editingContext);
		String message = ", debut periode : " + DateCtrl.dateToString(debutPeriode);
		message += ", fin periode : " + DateCtrl.dateToString(finPeriode);
		LogManager.logDetail(individu.identite() + ", prime : " + prime.primLibLong() + message);
		String resultat = PrimeCalcul.sharedInstance().verifierParametresPersonnels(parametresPersonnels,individu,prime,debutPeriode,finPeriode);
		if (resultat == null) {
			LogManager.logDetail("parametres personnels valides");
		} else {
			LogManager.logDetail(resultat);
		}
		return resultat;

	}
	/** Retourne les r&eacute;sultats de calcul de la prime pour un individu (tableau de PrimeResultat) */
	public PrimeResultatCalcul clientSideRequestCalculerResultatsAttributionPourIndividuEtPrime(EOGlobalID individuID,EOGlobalID primeID,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		LogManager.logDetail("clientSideRequestCalculerResultatsAttributionPourIndividuEtPrime");
		EOEditingContext editingContext = getNewEditingContext();
		EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuID,editingContext);
		EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext);
		String message = ", debut periode : " + DateCtrl.dateToString(debutPeriode);
		if (finPeriode != null) {
			message += ", fin periode : " + DateCtrl.dateToString(finPeriode);
		}
		LogManager.logDetail(individu.identite() + ", prime : " + prime.primLibLong() + message);
		return calculerAttributionsPourIndividuPrimeEtPeriode(individu,prime,debutPeriode,finPeriode,false);
	}
	/** Calcule des attributions de prime pour un individu et les enregistre dans la base */
	public PrimeResultatCalcul clientSideRequestCalculerAttributionsPourIndividuEtPrime(EOGlobalID individuID,EOGlobalID primeID,NSTimestamp debutPeriode,NSTimestamp finPeriode) throws Exception {
		LogManager.logDetail("clientSideRequestCalculerAttributionsPourPrime");
		EOEditingContext editingContext = getNewEditingContext();
		EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuID,editingContext);
		EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext);
		String message = ", debut periode : " + DateCtrl.dateToString(debutPeriode);
		if (finPeriode != null) {
			message += ", fin periode : " + DateCtrl.dateToString(finPeriode);
		}
		LogManager.logDetail(individu.identite() + ", prime : " + prime.primLibLong() + message);
		return calculerAttributionsPourIndividuPrimeEtPeriode(individu,prime,debutPeriode,finPeriode,true);
	}
	/** Recalcule une attribution de prime pour un individu */
	public PrimeResultatCalcul clientSideRequestRecalculerAttribution(EOGlobalID attributionID) throws Exception {
		LogManager.logDetail("clientSideRequestRecalculerAttribution");
		EOEditingContext editingContext = defaultEditingContext();	// On travaille dans l'editing context de la session pour que les données soient synchronisées entre le serveur et le client
		try {
			editingContext.lock();
			EOPrimeAttribution attribution = (EOPrimeAttribution)SuperFinder.objetForGlobalIDDansEditingContext(attributionID,editingContext);
			EOPrime prime = attribution.prime();
			String message = ", debut periode : " + DateCtrl.dateToString(attribution.debutValidite());
			if (attribution.finValidite() != null) {
				message += ", fin periode : " + DateCtrl.dateToString(attribution.finValidite());
			}
			LogManager.logDetail(attribution.individu().identite() + ", prime : " + prime.primLibLong() + message);
			// preparerAttributions = false, on veut récupérer un tableau de PrimeResultat
			PrimeResultatCalcul resultatCalcul = calculerAttributionsPourIndividuPrimeEtPeriode(attribution.individu(),prime,attribution.debutValidite(),attribution.finValidite(),false);
			// On récupère un tableau de PrimeResultat (paramètre false ci-dessus)
			NSMutableArray mutableAttributions = new NSMutableArray(attribution);
			if (resultatCalcul.diagnostic() != null) {
				attribution.setPattMontant(new BigDecimal(0));
			} else {
				mutableAttributions = new NSMutableArray(attribution);
				if (resultatCalcul.resultats().count() == 0) {
					attribution.setPattMontant(new BigDecimal(0));	// Ne devrait pas se produire
				} else {
					PrimeResultat resultat = (PrimeResultat)resultatCalcul.resultats().objectAtIndex(0);
					attribution.setPattMontant(resultat.montant());
					attribution.setPattCalcul(resultat.formuleCalcul());
					if (DateCtrl.dateToString(resultat.debutValidite()).equals(DateCtrl.dateToString(attribution.debutValidite())) == false) {
						attribution.setDebutValidite(resultat.debutValidite());
					}
					if (attribution.finValidite() != null && resultat.finValidite() != null &&
							DateCtrl.dateToString(resultat.finValidite()).equals(DateCtrl.dateToString(attribution.finValidite())) == false) {
						attribution.setFinValidite(resultat.finValidite());
					}
					// Si il y a plus de résultats, les créer et les insérer dans l'editing context
					if (resultatCalcul.resultats().count() > 1) {
						NSMutableArray resultats = new NSMutableArray(resultatCalcul.resultats());
						resultats.removeObjectAtIndex(0);
						mutableAttributions.addObjectsFromArray(creerAttributions(resultats, prime, attribution.individu()));
					}
				}
			}
			// Dans tous les cas, enregistrer l'editing context car on a modifié l'attribution courante
			editingContext.saveChanges();
			NSArray attributions = new NSArray(mutableAttributions);

			return 	new PrimeResultatCalcul(PrimeResultatCalcul.TYPE_ATTRIBUTION,resultatCalcul.diagnostic(),Utilitaires.tableauDeGlobalIDs(attributions, editingContext));
		} catch (Exception e) {
			LogManager.logException(e);
			editingContext.revert();
			throw e;
		} finally {
			editingContext.unlock();
		}
	}
	/** Renouvelle une attribution pour l'ann&eacute;e suivante si l'individu est toujours &eacute;ligible pour cette prime en utilisant
	 * les m&ecirc;mes param&egrave;tres personnels si la prime supporte des param&grave;tres personnels
	 */
	public PrimeResultatCalcul clientSideRequestRenouvelerAttribution(EOGlobalID attributionID) throws Exception {
		LogManager.logDetail("clientSideRequestRenouvelerAttribution");
		EOEditingContext editingContext = defaultEditingContext();	// On travaille dans l'editing context de la session pour que les données soient synchronisées entre le serveur et le client
		try {
			editingContext.lock();
			EOPrimeAttribution attribution = (EOPrimeAttribution)SuperFinder.objetForGlobalIDDansEditingContext(attributionID,editingContext);
			EOPrime prime = attribution.prime();
			int nouvelleAnnee = DateCtrl.getYear(attribution.debutValidite()) + 1;
			NSTimestamp debutCampagne = prime.debutPeriode(nouvelleAnnee), finCampagne = prime.finPeriode(nouvelleAnnee);
			if (prime.finValidite() != null && DateCtrl.isBefore(prime.finValidite(), finCampagne)) {
				return new PrimeResultatCalcul(PrimeResultatCalcul.TYPE_ATTRIBUTION,"La prime n'est pas valide en " + nouvelleAnnee,null);
			}
			// Vérifier si l'attribution pour la nouvelle période existe déjà
			NSArray attributionsPourNouvelleAnnee = EOPrimeAttribution.rechercherAttributionsEnCoursPourPrimeIndividuEtPeriode(editingContext, attribution.individu(), prime, debutCampagne, finCampagne);
			if (attributionsPourNouvelleAnnee.count() > 0) {
				return new PrimeResultatCalcul(PrimeResultatCalcul.TYPE_ATTRIBUTION,"L'individu a déjà une attribution pour cette prime en " + nouvelleAnnee,null);
			}
			String message = ", debut periode : " + DateCtrl.dateToString(debutCampagne) + ", fin periode : " + DateCtrl.dateToString(finCampagne);
			LogManager.logDetail(attribution.individu().identite() + ", prime : " + prime.primLibLong() + message);
			// preparerAttributions = false, on veut récupérer un tableau de PrimeResultat
			return calculerAttributionsPourIndividuPrimeEtPeriode(attribution.individu(),prime,debutCampagne,finCampagne,true,true);

		} catch (Exception e) {
			LogManager.logException(e);
			throw e;
		} finally {
			editingContext.unlock();
		}
	}
	/** Renouvelle toutes les attributions d'une ann&eacute;e */
	public String clientSideRequestRenouvelerAttributions(NSArray attributionsID) {
		LogManager.logDetail("clientSideRequestRenouvelerAttributions");
		EOEditingContext editingContext = defaultEditingContext();	// On travaille dans l'editing context de la session pour que les données soient synchronisées entre le serveur et le client
		try {
			editingContext.lock();
			NSArray attributions = Utilitaires.tableauObjetsMetiers(attributionsID, editingContext);
			if (attributions.count() == 0) {
				return "Erreur - Pas d'attribution pour le renouvellement";
			}
			EOPrimeAttribution attribution = (EOPrimeAttribution)attributions.objectAtIndex(0);
			EOPrime prime = attribution.prime();
			int nouvelleAnnee = DateCtrl.getYear(attribution.debutValidite()) + 1;
			NSTimestamp debutCampagne = prime.debutPeriode(nouvelleAnnee), finCampagne = prime.finPeriode(nouvelleAnnee);
			if (prime.finValidite() != null && DateCtrl.isBefore(prime.finValidite(), finCampagne)) {
				return "La prime n'est pas valide en " + nouvelleAnnee;
			}
			int nbNouvellesAttributions = 0;
			java.util.Enumeration e = attributions.objectEnumerator();
			while (e.hasMoreElements()) {
				attribution = (EOPrimeAttribution)e.nextElement();
				// Vérifier si l'attribution pour la nouvelle période existe déjà
				NSArray attributionsPourNouvelleAnnee = EOPrimeAttribution.rechercherAttributionsEnCoursPourPrimeIndividuEtPeriode(editingContext, attribution.individu(), prime, debutCampagne, finCampagne);
				if (attributionsPourNouvelleAnnee.count() == 0) {
					String message = ", debut periode : " + DateCtrl.dateToString(debutCampagne) + ", fin periode : " + DateCtrl.dateToString(finCampagne);
					LogManager.logDetail(attribution.individu().identite() + ", prime : " + prime.primLibLong() + message);
					// preparerAttributions = false, on veut récupérer un tableau de PrimeResultat
					PrimeResultatCalcul resultat = calculerAttributionsPourIndividuPrimeEtPeriode(attribution.individu(),prime,debutCampagne,finCampagne,true,true);
					if (resultat.diagnostic() == null) {
						nbNouvellesAttributions++;
					}
				}
			}
			return "" + nbNouvellesAttributions + " attributions créées";
		} catch (Exception exc) {
			LogManager.logException(exc);
			return "Erreur - " + exc.getMessage();
		} finally {
			editingContext.unlock();
		}
	}
	/** Cree les personnalisations pour l'annee suivant l'annee passee en parametre
	 * precedente si elles n'existent pas
	 * @param primeID global ID de la prime
	 * @param annee a renouvele
	 * @return
	 */
	public String clientSideRequestRenouvelerPersonalisations(EOGlobalID primeID,String annee) {
		EOEditingContext editingContext = getNewEditingContext();
		EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext);
		int anneePrecedente = new Integer(annee).intValue();
		int anneeARenouveler = anneePrecedente + 1;
		NSTimestamp debutCampagne = DateCtrl.debutAnnee(anneeARenouveler), finCampagne = DateCtrl.finAnnee(anneeARenouveler);
		
		// Vérifier si la prime est toujours valide l'année suivante
		if (prime.finValidite() != null && DateCtrl.isAfter(finCampagne, prime.finValidite())) {
			return "La prime n'est plus valable en " + anneeARenouveler;
		}
		NSArray personnalisations = EOPrimePersonnalisation.rechercherPersonnalisationPourAnneeEtPrime(editingContext, anneePrecedente, prime);
		NSArray individusAvecPersonnalisation = (NSArray)EOPrimePersonnalisation.rechercherPersonnalisationPourAnneeEtPrime(editingContext, anneeARenouveler, prime).valueForKey("individu");
		boolean shouldSave = false;
		try {
			editingContext.lock();
			java.util.Enumeration e = personnalisations.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimePersonnalisation personnalisation = (EOPrimePersonnalisation)e.nextElement();
				if (individusAvecPersonnalisation.containsObject(personnalisation.individu()) == false) {
					EOPrimePersonnalisation nouvellePersonnalisation = 	personnalisation.clonerAvecDates(debutCampagne, finCampagne);
					editingContext.insertObject(nouvellePersonnalisation);
					shouldSave = true;
				}
			}
			if (shouldSave) {
				editingContext.saveChanges();
			}
			return null;
		} catch (Exception exc) {
			return exc.getMessage();
		} finally {
			editingContext.unlock();
		}
	}

	/** Supprime un type d'election et toutes les donne associees. Retourne null
	 * ou un message d'erreur */
	public String clientSideRequestSupprimerTypeInstance(EOGlobalID globalIDTypeInstance) {
		EOEditingContext editingContext = getNewEditingContext();
		try {
			editingContext.lock();
			editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
			EOTypeInstance typeInstance = (EOTypeInstance)SuperFinder.objetForGlobalIDDansEditingContext(globalIDTypeInstance,editingContext);
			typeInstance.supprimerAutresDonneesEtEnregistrer();
			editingContext.deleteObject(typeInstance);
			editingContext.saveChanges();
			return null;
		} catch (Exception e) {
			editingContext.revert();
			LogManager.logException(e);
			return e.getMessage();
		}
	}
	/** Generation du parametrage final d'une election
		Retourne le texte de l'exception rencontree ou null */
	public String clientSideRequestPreparerParametrageFinal(EOInstance instance) {

		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		//EOInstance instance = (EOInstance)SuperFinder.objetForGlobalIDDansEditingContext(globalIDInstance,editingContext);
		LogManager.logDetail(loginAgent + " : clientSideRequestPreparerParametrageFinal : " + instance.llInstance() + ", date scrutin :" + instance.dateScrutinFormatee());
		try {
			EOParamRepartElec.preparerParametragesPourInstance(instance);
			return null;
		} catch (Exception e) {
			LogManager.logException(e);
			if (e.getMessage() == null) {
				return "Erreur nulle. Veuillez voir le log serveur";
			} else {
				return e.getMessage();
			}
		}
	}


	/**
	 * 
	 * Generation d'une liste electorale pour une instance donnee
	 * 
	 * @param instance
	 * @return
	 */
	public String clientSideRequestGenererListeElectorale(EOInstance instance) {
		EOEditingContext edc = getNewEditingContext();
		edc.setDelegate(this);	// pour la gestion des dates de création et modification

		try {
			EOListeElecteur.genererListeElecteursPourInstance(instance);
			return null;
		} catch (Exception e) {
			LogManager.logException(e);
			if (e.getMessage() == null) {
				return "Erreur nulle. Veuillez voir le log serveur";
			} else {
				return e.getMessage();
			}
		}
	}

	//	méthodes utilisées par l'impression pour gérer les threads
	/** Lorsque l'action est terminée, retourne un dictionnaire qui contient :
	<UL>- le résultat de l'action si celle-ci retourne une valeur (clé : resultat). 
	Ce dernier est lui-même un dictionnaire qui contient la classe du résultat (clé : classe) et la valeur du résultat (clé  valeur)</UL>
	<UL>- le diagnostic de l'action (clé : diagnostic)
	Ce dernier est lui-même un dictionnaire qui contient l'exception si elle s'est produite (clé : exception) et le message (clé: message)
	 */
	public NSDictionary clientSideRequestResultatAction() {
		NSMutableDictionary resultatAction = new NSMutableDictionary();
		if (threader != null && threader.status() != null) {
			if (threader.status().equals(ServerThreadManager.TERMINE)) {
				if (threader.resultat() != null) {
					resultatAction.setObjectForKey(threader.resultat(),"resultat");
				}
				if (threader.diagnostic() != null) {
					resultatAction.setObjectForKey(threader.diagnostic(),"diagnostic");
				}
				threader = null;
			} 
		}
		return resultatAction;
	}

	public String clientSideRequestMessage() {
		try {
			if (threader != null && threader.status() != null) {
				if (threader.status().equals(ServerThreadManager.EN_COURS)) {
					return threader.message();
				} else if (threader.status().equals(ServerThreadManager.TERMINE)) {
					return "termine";
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			LogManager.logException(e);
		}
		return null;
	}	

	//	méthodes privées
	// calcule les attributions pour un individidu et une prime.
	private PrimeResultatCalcul calculerAttributionsPourIndividuPrimeEtPeriode(EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean preparerAttributions,boolean estRenouvellement) throws Exception {
		EOEditingContext editingContext = individu.editingContext();
		try {	
			editingContext.lock();
			PrimeResultatEligibilite resultat = PrimeCalcul.sharedInstance().individuEligiblePourPrimeEtPeriode(individu, prime, debutPeriode, finPeriode);
			if (resultat.diagnosticRefus() == null) {
				// L'individu est éligible pour la prime, si il s'agit d'un renouvellement, on crée si nécessaire la personnalisation 
				// et les paramètres personnels l'année en utilisant ceux de l'année précédente
				if (estRenouvellement) {
					LogManager.logDetail("Verification des renouvellements de personnalisation");
					EOPrimePersonnalisation personnalisation = EOPrimePersonnalisation.rechercherPersonnalisationEnCoursPourIndividuPrimeEtPeriode(editingContext, individu, prime,DateCtrl.dateAvecAjoutAnnees(debutPeriode, -1), DateCtrl.dateAvecAjoutAnnees(finPeriode,-1));
					EOPrimePersonnalisation personnalisationRenouvelee = EOPrimePersonnalisation.rechercherPersonnalisationEnCoursPourIndividuPrimeEtPeriode(editingContext, individu, prime,debutPeriode,finPeriode);
					if (personnalisation != null) {
						// L'individu a une personnalisation
						NSArray codesRenouveles = null;
						if (personnalisationRenouvelee == null) {
							LogManager.logDetail("Renouvellement de la personnalisation");
							// La personnalisation n'a pas encore été renouvelée, faire une copie de celle de l'année précédente et
							// modifier pour la nouvelle année
							personnalisationRenouvelee = personnalisation.clonerAvecDates(debutPeriode, finPeriode);
							editingContext.insertObject(personnalisationRenouvelee);
							editingContext.saveChanges();
						} else {
							LogManager.logDetail("La personnalisation existe déjà");
							codesRenouveles = (NSArray)EOPrimeParamPerso.rechercherParametresPersonnelsValidesPourPersonnalisation(editingContext, personnalisationRenouvelee).valueForKey("code");
						}
						// Vérifier si il existe des paramètres personnels pour la personnalisation de l'année précédente
						NSArray paramsPerso = EOPrimeParamPerso.rechercherParametresPersonnelsValidesPourPersonnalisation(editingContext, personnalisation);
						java.util.Enumeration e = paramsPerso.objectEnumerator();
						boolean shouldSave = false;
						while (e.hasMoreElements()) {
							EOPrimeParamPerso param = (EOPrimeParamPerso)e.nextElement();
							if (codesRenouveles == null || codesRenouveles.containsObject(param.code()) == false) {
								EOPrimeParamPerso paramRenouvele = param.clonerAvecDatesEtPersonnalisation(debutPeriode, finPeriode, personnalisation);
								editingContext.insertObject(paramRenouvele);
								shouldSave = true;
							}
						}
						if (shouldSave) {
							LogManager.logDetail("Renouvellement des paramètres personnels");
							editingContext.saveChanges();
						}
					}
				}
				NSArray resultats = PrimeCalcul.sharedInstance().evaluerResultatsPourIndividu(prime, resultat.individuPourPrimeAtIndex(0),debutPeriode, finPeriode);
				// Vérifier si une erreur s'est produite, on ramène la première erreur
				if (resultats != null && resultats.count() > 0) {
					String diagnostic = null;
					LogManager.logDetail("calculerAttributionsPourIndividuPrimeEtPeriode - Vérification diagnostic");
					java.util.Enumeration e = resultats.objectEnumerator();
					while (e.hasMoreElements()) {
						PrimeResultat primeResultat = (PrimeResultat)e.nextElement();
						if (primeResultat.messageErreur() != null) {
							diagnostic = primeResultat.messageErreur();
							break;
						}
					}
					LogManager.logDetail("calculerAttributionsPourIndividuPrimeEtPeriode - Nb resultats " + resultats.count());
					if (!preparerAttributions) {
						return new PrimeResultatCalcul(PrimeResultatCalcul.TYPE_PRIME_RESULTAT,diagnostic,resultats);
					}
					NSArray attributions = creerAttributions(resultats, prime, individu);
					if (attributions != null && attributions.count() > 0) {
						// les enregistrer dans la base
						editingContext.saveChanges();
					}
					return new PrimeResultatCalcul(PrimeResultatCalcul.TYPE_ATTRIBUTION,diagnostic,Utilitaires.tableauDeGlobalIDs(attributions, editingContext));
				} else {
					return new PrimeResultatCalcul(PrimeResultatCalcul.TYPE_ATTRIBUTION,PrimeResultatCalcul.SANS_ATTRIBUTION,null);
				}
			} else {
				return new PrimeResultatCalcul(PrimeResultatCalcul.TYPE_ATTRIBUTION,PrimeResultatCalcul.INDIVIDU_NON_ELIGIBLE + "\n" + resultat.diagnosticRefus(),null);
			}

		} catch (Exception e) {
			LogManager.logException(e);
			throw e;
		} finally {
			editingContext.unlock();
		}
	}
	private PrimeResultatCalcul calculerAttributionsPourIndividuPrimeEtPeriode(EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean preparerAttributions) throws Exception {
		return calculerAttributionsPourIndividuPrimeEtPeriode(individu, prime, debutPeriode, finPeriode, preparerAttributions,false);
	}
	// Crée les attributions et les insère dans l'editing contexte
	private NSArray creerAttributions(NSArray resultats,EOPrime prime,EOIndividu individu) {
		NSMutableArray attributions = new NSMutableArray();
		java.util.Enumeration e = resultats.objectEnumerator();
		// créer les attributions;
		e = resultats.objectEnumerator();
		while (e.hasMoreElements()) {
			PrimeResultat primeResultat = (PrimeResultat)e.nextElement();
			LogManager.logDetail("clientSideRequestRecalculerAttributionsPourPrimeEtAttribution - Ajout d'une nouvelle attribution pour le resultat : " + primeResultat);
			EOPrimeAttribution attribution = PrimeCalcul.sharedInstance().preparerAttributionPourIndividu(prime,individu,primeResultat);
			prime.editingContext().insertObject(attribution);
			attributions.addObject(attribution);
		}
		return attributions;
	}
	private EOGenericRecord recordPourGlobalID(EOGlobalID globalID) {
		// ne pas travailler dans l'editing context de la session cela bloque la session (côté serveur et client) dans certains cas
		return SuperFinder.objetForGlobalIDDansEditingContext(globalID,getNewEditingContext());
	}


	/** Seule la valeur de la sous-rubrique est modifiable */
	public class ValeurSousRubrique implements NSKeyValueCoding {
		private SousRubriqueCir sousRubrique;
		private String valeur;
		private boolean isEdited;
		private String messageErreur;


		public ValeurSousRubrique(SousRubriqueCir sousRubrique,String valeur) {
			this.sousRubrique = sousRubrique;
			this.valeur = valeur;
			this.isEdited = false;
		}
		public int numeroOrdre() {
			return sousRubrique.numeroOrdre();
		}
		public String cle() {
			return sousRubrique.cle();
		}
		public String aide() {
			return sousRubrique.aide();
		}
		public boolean isEdited() {
			return isEdited;
		}
		public boolean estValeurConstante() {
			return sousRubrique.nature().equals(ConstantesCir.SOUS_RUBRIQUE_CONSTANTE);
		}
		public String messageErreur() {
			return messageErreur;
		}
		public void setMessageErreur(String messageErreur) {
			this.messageErreur = messageErreur;
		}
		public String valeur() {
			return valeur;
		}
		public void setValeur(String valeur) {

			if (estValeurConstante()) {
				setMessageErreur("Vous ne pouvez pas modifier cette valeur");
				this.isEdited = false;
				return;
			}
			// On valide les valeurs au fur et à-mesure de leurs saisie
			try {
				this.valeur = sousRubrique.validerValeur(valeur);	// la validation peut changer la valeur
				this.isEdited = true;
			} catch (Exception e) {
				String message = e.getMessage();
				if (message.indexOf("\t") == 0) {
					message = message.substring(1);
				}
				this.isEdited = false;
				setMessageErreur(message);
			}
		}
		// Interface NSKeyValueCoding
		public void takeValueForKey(Object valeur,String cle) {
			NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
		}
		public Object valueForKey(String cle) {
			return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
		}
	}

	/** Impression d'un type d'election */
	public Boolean clientSideRequestImprimerTypeInstance(EOGlobalID globalIDInstance,Boolean editionDetaillee) {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		EOTypeInstance type = (EOTypeInstance)SuperFinder.objetForGlobalIDDansEditingContext(globalIDInstance,editingContext);

		try {
			Class[] classeParametres =  new Class[] {EOTypeInstance.class,Boolean.class};
			Object[] parametres = new Object[]{type,editionDetaillee};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerTypeInstance",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression d'un college */
	public Boolean clientSideRequestImprimerCollege(EOGlobalID globalID) {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		EOCollege college = (EOCollege)SuperFinder.objetForGlobalIDDansEditingContext(globalID,editingContext);
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerCollege : " + college.llCollege());
		try {
			Class[] classeParametres =  new Class[] {EOCollege.class};
			Object[] parametres = new Object[]{college};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerCollege",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des secteurs */
	public Boolean clientSideRequestImprimerSecteurs() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray secteurs = SuperFinder.rechercherEntite(editingContext,"Secteur");
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerSecteurs");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{secteurs};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerSecteurs",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des composantes &eacute;lectives */
	public Boolean clientSideRequestImprimerComposantes() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray composantes = SuperFinder.rechercherEntite(editingContext,"ComposanteElective");
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerComposantes");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{composantes};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerComposantesElectives",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des sections &eacute;lectives */
	public Boolean clientSideRequestImprimerSections() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray sections = SuperFinder.rechercherEntite(editingContext,"SectionElective");
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerSections");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{sections};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerSectionsElectives",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des coll&grave;ges-sections &eacute;lectives */
	public Boolean clientSideRequestImprimerCollegesSections() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray collegesSections = SuperFinder.rechercherEntite(editingContext,"CollegeSectionElective");
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("college.llCollege", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("sectionElective.llSectionElective", EOSortOrdering.CompareAscending));
		collegesSections = EOSortOrdering.sortedArrayUsingKeyOrderArray(collegesSections, sorts);
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerCollegesSections");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{collegesSections};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerCollegesSections",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression des bureaux de vote */
	public Boolean clientSideRequestImprimerBureauxDeVote() {
		EOEditingContext editingContext = getNewEditingContext();
		editingContext.setDelegate(this);	// pour la gestion des dates de création et modification
		NSArray bureaux = SuperFinder.rechercherEntite(editingContext,"BureauVote");
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerBureauxDeVote");
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{bureaux};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerBureauxDeVote",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression de la liste &eacute;lectorale pour une &eacute;lection
		Retourne true si impression OK */
	public Boolean clientSideRequestImprimerListeElectorale(InfoPourEditionRequete infoEdition,EOGlobalID globalIDInstance,Integer typeElecteur) {
		EOInstance instance = (EOInstance)recordPourGlobalID(globalIDInstance);
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeElectorale : " + instance.llInstance() + ", date scrutin :" + instance.dateScrutinFormatee());
		try {
			Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,EOInstance.class,Integer.class};
			Object[] parametres = new Object[]{infoEdition,instance,typeElecteur};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeElectorale",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	
	public Boolean clientSideRequestImprimerAbsencesPourIndividu(InfoPourEditionRequete infoEdition,EOGlobalID individuID,NSArray absencesID,Boolean imprimerDetails) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerAbsencesPourIndividu");
		try {
			EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
			NSArray absences = Utilitaires.tableauObjetsMetiers(absencesID,individu.editingContext());
			Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,EOIndividu.class,NSArray.class,Boolean.class};
			Object[] parametres = new Object[]{infoEdition,individu,absences,imprimerDetails};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerAbsencesPourIndividu",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerCirIdentite(InfoPourEditionRequete infoEdition,NSArray cirsID,Boolean imprimerDetails) {

		try {
			NSArray cirs = Utilitaires.tableauObjetsMetiers(cirsID,defaultEditingContext());

			threader = new ServerThreadManager(Imprimeur.sharedInstance(),
					"imprimerCirsIdentite",
					new Class[] {InfoPourEditionRequete.class,NSArray.class,Boolean.class},
					new Object[]{infoEdition,cirs,imprimerDetails});
			threader.start();

			return new Boolean(true);

		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param congeID
	 * @param destinataires
	 * @param estArreteAnnulation
	 * @return
	 */
	public Boolean clientSideRequestImprimerArreteConge(EOGlobalID congeID,NSArray destinataires,Boolean estArreteAnnulation) {
		Duree conge = (Duree)recordPourGlobalID(congeID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,conge.editingContext());
		try {
			Class[] classeParametres =  new Class[] {Duree.class,NSArray.class,Boolean.class};
			Object[] parametres = new Object[]{conge,destins,estArreteAnnulation};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteConge",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param congeID
	 * @param destinataires
	 * @return
	 */
	public Boolean clientSideRequestImprimerCongeGardeEnfant(EOGlobalID congeID,NSArray destinataires) {
		EOCongeGardeEnfant conge = (EOCongeGardeEnfant)recordPourGlobalID(congeID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,conge.editingContext());
		try {
			Class[] classeParametres =  new Class[] {EOCongeGardeEnfant.class,NSArray.class};
			Object[] parametres = new Object[]{conge,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerCongeGardeEnfant",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param noArrete
	 * @param globalID
	 * @param destinataires
	 * @param estVacation
	 * @return
	 */
	public Boolean clientSideRequestImprimerContratTravail(String noArrete,EOGlobalID globalID,NSArray destinataires,Boolean estVacation) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerContratTravail");
		// 04/03/2011 - pour l'utilisation des modules d'impression
		Connection connection = null;
		EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
		if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
			connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
		}
		EOContratAvenant avenant = (EOContratAvenant)recordPourGlobalID(globalID);
		avenant.setNoArretePourEdition(noArrete);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,avenant.editingContext());
		if (connection != null) {
			try {
				Class[] classeParametres =  new Class[] {Connection.class,EOContratAvenant.class,NSArray.class};
				Object[] parametres = new Object[]{connection,avenant,destins};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerContratTravail",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} catch (Exception e) {
				LogManager.logException(e);
				return new Boolean(false);
			}
		} else {
			LogManager.logDetail("Pas de connexion avec la base");
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param noArrete
	 * @param globalID
	 * @param destinataires
	 * @param estVacation
	 * @return
	 */
	public Boolean clientSideRequestImprimerContratVacationLocal(String noArrete,EOGlobalID globalID,NSArray destinataires) {

		Connection connection = null;
		EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
		if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
			connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
		}
		EOVacataires vacation = (EOVacataires)recordPourGlobalID(globalID);
		vacation.setNoArretePourEdition(noArrete);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,vacation.editingContext());
		if (connection != null) {
			try {
				Class[] classeParametres =  new Class[] {Connection.class,EOVacataires.class,NSArray.class};
				Object[] parametres = new Object[]{connection,vacation,destins};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerContratVacation",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} catch (Exception e) {
				LogManager.logException(e);
				return new Boolean(false);
			}
		} else {
			LogManager.logDetail("Pas de connexion avec la base");
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param noArrete
	 * @param globalID
	 * @param destinataires
	 * @return
	 */
	public Boolean clientSideRequestImprimerContratVacation(String noArrete,EOGlobalID globalID,NSArray destinataires) {

		Connection connection = null;
		EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
		if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
			connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
		}
		EOVacataires vacation = (EOVacataires)recordPourGlobalID(globalID);
		vacation.setNoArretePourEdition(noArrete);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,vacation.editingContext());
		if (connection != null) {
			try {
				Class[] classeParametres =  new Class[] {Connection.class,EOVacataires.class,NSArray.class};
				Object[] parametres = new Object[]{connection,vacation,destins};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerVacation",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} catch (Exception e) {
				LogManager.logException(e);
				return new Boolean(false);
			}
		} else {
			LogManager.logDetail("Pas de connexion avec la base");
			return new Boolean(false);
		}

	}


	public Boolean clientSideRequestImprimerArretePourAvenant(EOGlobalID avenantID,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerArretePourAvenant");
		EOContratAvenant avenant = (EOContratAvenant)recordPourGlobalID(avenantID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,avenant.editingContext());
		try {
			Class[] classeParametres =  new Class[] {EOContratAvenant.class, NSArray.class};
			Object[] parametres = new Object[]{avenant, destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(), "imprimerArretePourAvenant",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	
	/**
	 * 
	 * @param elementID
	 * @param destinataires
	 * @param estArreteAnnulation
	 * @return
	 */
	public Boolean clientSideRequestImprimerArretePourCarriere(EOGlobalID elementID,NSArray destinataires,Boolean estArreteAnnulation) {

		EOElementCarriere element = (EOElementCarriere)recordPourGlobalID(elementID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,element.editingContext());
		try {
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePourCarriere",
					new Class[] {EOElementCarriere.class, NSArray.class, Boolean.class},
					new Object[]{element, destins, estArreteAnnulation});
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	
	/**
	 * 
	 * @param evenementID
	 * @param destinataires
	 * @param estArreteAnnulation
	 * @return
	 */
	public Boolean clientSideRequestImprimerArretePourEvenement(EOGlobalID evenementID,NSArray destinataires,Boolean estArreteAnnulation) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerArretePourEvenement");
		DureeAvecArrete duree = (DureeAvecArrete)recordPourGlobalID(evenementID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,duree.editingContext());
		try {
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePourEvenement",
					new Class[] {DureeAvecArrete.class,NSArray.class,Boolean.class},
					new Object[]{duree,destins,estArreteAnnulation});
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	public Boolean clientSideRequestImprimerArreteEmeritat(EOGlobalID gidEmeritat, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EOEmeritat object = (EOEmeritat)SuperFinder.objetForGlobalIDDansEditingContext(gidEmeritat,editingContext);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,object.editingContext());
		try {
			Class[] classeParametres	=  new Class[] {EOEmeritat.class,NSArray.class};
			Object[] parametres = new Object[]{object,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteEmeritat",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}


	public Boolean clientSideRequestImprimerArretePromotionEC(IndividuPromouvableEc individuPromouvable,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerArretePromotionEC");
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,individuPromouvable.editingContext());
		try {
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePromotionEC",
					new Class[] {IndividuPromouvableEc.class,NSArray.class},
					new Object[]{individuPromouvable,destins});
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * @param emploiID : emploi à imprimer
	 * @return Vrai/Faux
	 */
	public Boolean clientSideRequestImprimerEmploi(EOGlobalID emploiID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerEmploi");
		IEmploi emploi = (IEmploi) recordPourGlobalID(emploiID);
		try {
			Class[] classeParametres =  new Class[] {IEmploi.class};
			Object[] parametres = new Object[]{emploi};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(), "imprimerEmploi", classeParametres, parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * @param emploiID : emploi à imprimer
	 * @return Vrai/Faux
	 */
	public Boolean clientSideRequestImprimerEmploiHistorique(EOGlobalID emploiID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerEmploiHistorique");
		IEmploi emploi = (IEmploi) recordPourGlobalID(emploiID);
		try {
			Class[] classeParametres =  new Class[] {IEmploi.class};
			Object[] parametres = new Object[]{emploi};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(), "imprimerEmploiHistorique", classeParametres, parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	public Boolean clientSideRequestImprimerListeEmplois(InfoPourEditionRequete infoPourEdition,NSArray emploisID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeEmplois");
		NSArray emplois = Utilitaires.tableauObjetsMetiers(emploisID, getNewEditingContext());
		try {
			Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,NSArray.class};
			Object[] parametres = new Object[]{infoPourEdition,emplois};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerEmplois",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerPrimes(InfoPourEditionRequete infoEdition,NSArray attributionsID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerPrimes");
		try {
			EOEditingContext editingContext = getNewEditingContext();
			NSArray attributions = Utilitaires.tableauObjetsMetiers(attributionsID,editingContext);
			Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,NSArray.class};
			Object[] parametres = new Object[]{infoEdition,attributions};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerAttributionsPrimes",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerPoste(EOGlobalID globalID,Boolean estFicheDePoste) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerPoste");
		EOPoste poste = (EOPoste)recordPourGlobalID(globalID);
		try {
			Class[] classeParametres =  new Class[] {EOPoste.class,Boolean.class};
			Object[] parametres = new Object[]{poste,estFicheDePoste};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPoste",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerListePostes(NSArray postesID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListePostes");
		NSArray postes = Utilitaires.tableauObjetsMetiers(postesID, getNewEditingContext());
		try {
			Class[] classeParametres =  new Class[] {NSArray.class};
			Object[] parametres = new Object[]{postes};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPostes",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerFicheDePoste(EOGlobalID ficheDePosteID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFicheDePoste");
		EOFicheDePoste fiche = (EOFicheDePoste)recordPourGlobalID(ficheDePosteID);
		try {
			Class[] classeParametres =  new Class[] {EOFicheDePoste.class};
			Object[] parametres = new Object[]{fiche};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFicheDePoste",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerFicheLolf(EOGlobalID ficheID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFicheLolf");
		EOFicheLolf fiche = (EOFicheLolf)recordPourGlobalID(ficheID);
		try {
			Class[] classeParametres =  new Class[] {EOFicheLolf.class};
			Object[] parametres = new Object[]{fiche};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFicheLolf",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	//	Impression du résultat des requêtes
	public Boolean clientSideRequestImprimerResultatRequeteIndividu(NSArray individusEdition,InfoPourEditionRequete infoPourEdition) {
		if (infoPourEdition.nomFichierJasper().equals(FICHE_IDENTITE_INDIVIDU)) {
			LogManager.logDetail(loginAgent + " : clientSideRequestImprimerResultatRequeteIndividu - impression des fiches");
			java.util.Enumeration e = individusEdition.objectEnumerator();
			String texte = "";
			while (e.hasMoreElements()) {
				IndividuPourEdition individu = (IndividuPourEdition)e.nextElement();
				if (texte.length() > 0) {
					texte += ",";
				} 
				texte += individu.noIndividu();
			}
			texte = "where NO_INDIVIDU in (" + texte + ")";
			NSDictionary dict = new NSDictionary(texte,"NO_INDIVIDUS");
			Boolean result = clientSideRequestImprimerAvecModule(FICHES_IDENTITES,dict);
			if (result.booleanValue() == false) {
				return result;
			}
			return new Boolean(true);
		} else {
			LogManager.logDetail(loginAgent + " : clientSideRequestImprimerResultatRequeteIndividu");
			try {
				Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionRequete.class};
				Object[] parametres = new Object[]{individusEdition,infoPourEdition};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerResultatRequeteIndividu",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} catch (Exception e) {
				LogManager.logException(e);
				return new Boolean(false);
			}
		}
	}	
	public Boolean clientSideRequestImprimerResultatRequeteEvenement(NSArray evenementsEdition,InfoPourEditionRequete infoPourEdition) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerResultatRequeteEvenement");
		try {
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionRequete.class};
			Object[] parametres = new Object[]{evenementsEdition,infoPourEdition};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerResultatRequeteEvenement",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerResultatRequeteEmploi(NSArray emploisEdition,InfoPourEditionRequete infoPourEdition) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerResultatRequeteEmploi");
		try {
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionRequete.class};
			Object[] parametres = new Object[]{emploisEdition,infoPourEdition};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerResultatRequeteEmploi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerEtatPersonnel(InfoPourEditionRequete infoEdition,NSArray etatsPersonnels) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerEtatPersonnel");		
		try {                                                                                                                  
			Class[] classeParametres	=  new Class[] {InfoPourEditionRequete.class,NSArray.class};
			Object[] parametres = new Object[]{infoEdition,etatsPersonnels};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerEtatPersonnel",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerDifs(InfoPourEditionRequete infoEdition,NSArray difsGlobalIDs) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerDifs");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray difs = Utilitaires.tableauObjetsMetiers(difsGlobalIDs,editingContext);
			Class[] classeParametres	=  new Class[] {InfoPourEditionRequete.class,NSArray.class};
			Object[] parametres = new Object[]{infoEdition,difs};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerDifs",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationExamen(EOGlobalID examenGlobalID,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationExamen");		
		try {                         
			EOEditingContext editingContext = getNewEditingContext();
			EOExamenMedical examen = (EOExamenMedical)SuperFinder.objetForGlobalIDDansEditingContext(examenGlobalID, editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {EOExamenMedical.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{examen,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationExamen",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationsExamen(NSArray examensGlobalIDs,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationsExamen");		
		try {                         
			EOEditingContext editingContext = getNewEditingContext();
			NSArray examens = Utilitaires.tableauObjetsMetiers(examensGlobalIDs,editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{examens,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationsExamen",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerListeConvocationsExamen(NSArray examensGlobalIDs,InfoPourEditionMedicale infoImpression,String titre) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeConvocationsExamen");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray examens = Utilitaires.tableauObjetsMetiers(examensGlobalIDs,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,String.class};
			Object[] parametres = new Object[]{examens,infoImpression,titre};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeConvocationsExamen",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationVaccin(EOGlobalID vaccinGlobalID,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationVaccin");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			EOVaccin vaccin = (EOVaccin)SuperFinder.objetForGlobalIDDansEditingContext(vaccinGlobalID, editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {EOVaccin.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{vaccin,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationVaccin",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationsVaccin(NSArray vaccinsGlobalIDs,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationsVaccin");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray vaccins = Utilitaires.tableauObjetsMetiers(vaccinsGlobalIDs,editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{vaccins,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationsVaccin",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerListeConvocationsVaccin(NSArray vaccinsGlobalIDs,InfoPourEditionMedicale infoImpression,String titre) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeConvocationsVaccin");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray vaccins = Utilitaires.tableauObjetsMetiers(vaccinsGlobalIDs,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,String.class};
			Object[] parametres = new Object[]{vaccins,infoImpression,titre};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeConvocationsVaccin",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationVisite(EOGlobalID visiteGlobalID,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationVisite");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			EOVisiteMedicale visite = (EOVisiteMedicale)SuperFinder.objetForGlobalIDDansEditingContext(visiteGlobalID, editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {EOVisiteMedicale.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{visite,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationVisite",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerConvocationsVisite(NSArray visitesGlobalIDs,InfoPourEditionMedicale infoImpression,NSArray destinataires) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerConvocationsVisite");		
		try {                         
			EOEditingContext editingContext = new EOEditingContext();
			NSArray visites = Utilitaires.tableauObjetsMetiers(visitesGlobalIDs,editingContext);
			NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,NSArray.class};
			Object[] parametres = new Object[]{visites,infoImpression,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerConvocationsVisite",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerListeConvocationsVisite(NSArray visitesGlobalIDs,InfoPourEditionMedicale infoImpression,String titre) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeConvocationsVisite");		
		try {                         
			EOEditingContext editingContext = getNewEditingContext();
			NSArray visites = Utilitaires.tableauObjetsMetiers(visitesGlobalIDs,editingContext);
			Class[] classeParametres	=  new Class[] {NSArray.class,InfoPourEditionMedicale.class,String.class};
			Object[] parametres = new Object[]{visites,infoImpression,titre};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeConvocationsVisite",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerListeAcmos(NSArray individusAcmos,String titreEdition) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerListeAcmos");		
		try {                         
			Class[] classeParametres	=  new Class[] {NSArray.class,String.class};
			Object[] parametres = new Object[]{individusAcmos,titreEdition};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerListeAcmos",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	// Promouvabilités
	public Boolean clientSideRequestImprimerPromouvables(NSTimestamp debutPeriode,NSTimestamp finPeriode,NSArray promouvables) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerPromouvables");
		try {                                                                                                                  
			Class[] classeParametres	=  new Class[] {NSTimestamp.class, NSTimestamp.class,NSArray.class};
			Object[] parametres = new Object[]{debutPeriode,finPeriode,promouvables};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPromouvables",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}




	public Boolean clientSideRequestImprimerArretePromouvabilite(IndividuPromouvable individuPromouvable,NSArray destinataires) {

		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,individuPromouvable.editingContext());

		EOPassageEchelon passage = 
				(EOPassageEchelon)EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(defaultEditingContext(), 
						individuPromouvable.grade().cGrade(), 
						individuPromouvable.echelonSuivant(), 
						individuPromouvable.datePromotion(), 
						true).objectAtIndex(0);

		if (passage != null)
			individuPromouvable.setIndiceSuivant(EOIndice.indiceMajorePourIndiceBrutEtDate(individuPromouvable.editingContext(), passage.cIndiceBrut(), individuPromouvable.datePromotion()));

		try {
			Class[] classeParametres	=  new Class[] {IndividuPromouvable.class,NSArray.class};
			Object[] parametres = new Object[]{individuPromouvable,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePromouvabilite",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}


	public Boolean clientSideRequestImprimerArretePromouvabiliteChevron(IndividuPromouvable individuPromouvable,NSArray destinataires) {

		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,individuPromouvable.editingContext());

		EOPassageChevron passage = 
				(EOPassageChevron)EOPassageChevron.chevronSuivant(defaultEditingContext(), 
						individuPromouvable.grade(), 
						individuPromouvable.chevron());

		if (passage != null) {
			individuPromouvable.setChevronSuivant(passage.cChevron());
			individuPromouvable.setIndiceSuivant(EOIndice.indiceMajorePourIndiceBrutEtDate(individuPromouvable.editingContext(), passage.cIndiceBrut(), individuPromouvable.datePromotion()));

		}

		try {
			Class[] classeParametres	=  new Class[] {IndividuPromouvable.class,NSArray.class};
			Object[] parametres = new Object[]{individuPromouvable,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArretePromouvabiliteChevron",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}


	/**
	 * 
	 * @param gidCrct
	 * @param destinataires
	 * @return
	 */
	public Boolean clientSideRequestImprimerArreteCrct(EOGlobalID gidCrct, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EOCrct crct = (EOCrct)SuperFinder.objetForGlobalIDDansEditingContext(gidCrct,defaultEditingContext());
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
		try {
			Class[] classeParametres	=  new Class[] {EOCrct.class,NSArray.class};
			Object[] parametres = new Object[]{crct,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteCrct",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}

	/**
	 * 
	 * @param gidTempsPartiel
	 * @param destinataires
	 * @return
	 */
	public Boolean clientSideRequestImprimerArreteTempsPartiel(EOGlobalID gidTempsPartiel, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EOTempsPartiel tempsPartiel = (EOTempsPartiel)SuperFinder.objetForGlobalIDDansEditingContext(gidTempsPartiel,defaultEditingContext());
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
		try {
			Class[] classeParametres	=  new Class[] {EOTempsPartiel.class,NSArray.class};
			Object[] parametres = new Object[]{tempsPartiel,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteTempsPartiel",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	public Boolean clientSideRequestImprimerArreteRepriseTempsPlein(EOGlobalID gidReprise, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EORepriseTempsPlein reprise = (EORepriseTempsPlein)SuperFinder.objetForGlobalIDDansEditingContext(gidReprise,editingContext);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,editingContext);
		try {
			Class[] classeParametres	=  new Class[] {EORepriseTempsPlein.class,NSArray.class};
			Object[] parametres = new Object[]{reprise,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteRepriseTempsPlein",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}

	public Boolean clientSideRequestImprimerArreteMiTempsTherapeutique(EOGlobalID gidMtt, NSArray destinataires) {
		EOEditingContext editingContext = getNewEditingContext();
		EOMiTpsTherap object = (EOMiTpsTherap)SuperFinder.objetForGlobalIDDansEditingContext(gidMtt,editingContext);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires, editingContext);
		try {
			Class[] classeParametres	=  new Class[] {EOMiTpsTherap.class,NSArray.class};
			Object[] parametres = new Object[]{object,destins};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteMiTempsTherapeutique",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}


	// Autres
	/** Impression avec un module Jasper comportant des requetes SQL */
	public Boolean clientSideRequestImprimerAvecModule(String nomModule,NSDictionary valeursParametres) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerAvecModule : " + nomModule);
		try {
			Connection connection = null;
			EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
			if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
				connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
			}
			if (connection != null) {
				Class[] classeParametres =  new Class[] {java.sql.Connection.class,String.class,NSDictionary.class};
				Object[] parametres = new Object[]{connection,nomModule,valeursParametres};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPdfAvecModule",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} else {
				return new Boolean(false);
			}
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	/** Impression avec un module Jasper comportant des requetes SQL */
	public Boolean clientSideRequestImprimerFichierExcelAvecModule(String nomModule,NSDictionary valeursParametres) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerAvecModule : " + nomModule);
		try {
			Connection connection = null;
			EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
			if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext) {
				connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();
			}
			if (connection != null) {
				Class[] classeParametres =  new Class[] {java.sql.Connection.class,String.class,NSDictionary.class};
				Object[] parametres = new Object[]{connection,nomModule,valeursParametres};
				threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFichierExcelAvecModule",classeParametres,parametres);
				threader.start();
				return new Boolean(true);
			} else {
				return new Boolean(false);
			}
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	/**
	 * 
	 * @param individusGid
	 * @return
	 */
	public Boolean clientSideRequestImprimerFichesIndividu(NSArray individusGid) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFichesIndividu - impression des fiches");
		EOEditingContext editingContext = getNewEditingContext();
		NSArray individus = Utilitaires.tableauObjetsMetiers(individusGid, editingContext);
		java.util.Enumeration<EOIndividu> e = individus.objectEnumerator();
		String texte = "";
		while (e.hasMoreElements()) {
			EOIndividu individu = e.nextElement();
			if (texte.length() > 0) {
				texte += ",";
			} 
			texte += individu.noIndividu();
		}
		texte = "where NO_INDIVIDU in (" + texte + ")";
		NSDictionary dict = new NSDictionary(texte,"NO_INDIVIDUS");
		Boolean result = clientSideRequestImprimerAvecModule(FICHES_IDENTITES,dict);
		if (result.booleanValue() == false) {
			return result;
		}
		return new Boolean(true);

	}

	public Boolean clientSideRequestImprimerAnciennete(EOGlobalID individuID,NSTimestamp dateReference, Boolean estAncienneteComplete) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerAnciennete");
		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		try {
			Class[] classeParametres =  new Class[] {EOIndividu.class,NSTimestamp.class,Boolean.class};
			Object[] parametres = new Object[]{individu,dateReference,estAncienneteComplete};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerAnciennete",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerServices(EOGlobalID serviceID) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerServices");
		EOEditingContext editingContext = getNewEditingContext();
		EOStructure structure = (EOStructure)SuperFinder.objetForGlobalIDDansEditingContext(serviceID, editingContext);
		try {
			Class[] classeParametres =  new Class[] {EOStructure.class};
			Object[] parametres = new Object[]{structure};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerServices",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerArreteNbi(EOGlobalID occupationID,NSArray destinataires,NSDictionary dict) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerArreteNbi");
		EONbiOccupation occupation = (EONbiOccupation)recordPourGlobalID(occupationID);
		NSArray destins = Utilitaires.tableauObjetsMetiers(destinataires,occupation.editingContext());
		try {
			Class[] classeParametres =  new Class[] {EONbiOccupation.class,NSArray.class,NSDictionary.class};
			Object[] parametres = new Object[]{occupation,destins,dict};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerArreteNbi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerBeneficiairesNbi(NSTimestamp dateReference) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerBeneficiairesNbi");
		try {
			Class[] classeParametres =  new Class[] {NSTimestamp.class};
			Object[] parametres = new Object[]{dateReference};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerBeneficiairesNbi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerHistoriqueNbi(NSTimestamp dateDebut,NSTimestamp dateFin) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerHistoriqueNbi");
		try {
			Class[] classeParametres =  new Class[] {NSTimestamp.class,NSTimestamp.class};
			Object[] parametres = new Object[]{dateDebut,dateFin};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerHistoriqueNbi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}
	public Boolean clientSideRequestImprimerFonctionsNbi(NSTimestamp dateReference) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFonctionsNbi");
		try {
			Class[] classeParametres =  new Class[] {NSTimestamp.class};
			Object[] parametres = new Object[]{dateReference};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFonctionsNbi",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	public NSDictionary clientSideRequestImprimerFicheElectraSansThread(EOGlobalID individuID) {

		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);

		Connection connection = null;
		EOAdaptorContext contexte =  CktlDataBus.adaptorContext();
		if (contexte instanceof com.webobjects.jdbcadaptor.JDBCContext)
			connection = ((com.webobjects.jdbcadaptor.JDBCContext)contexte).connection();

		if (connection != null) {
			NSDictionary dict = new NSDictionary(individu.noIndividu(),"NO_INDIVIDU");

			NSDictionary datasImpression = Imprimeur.sharedInstance().imprimerPdfAvecModule(connection, FICHE_IDENTITE_ELECTRA, dict, null);			
			return datasImpression;

		}

		return null;
	}

	//	Avec Thread
	public Boolean clientSideRequestImprimerFicheCIR(EOGlobalID individuID) {

		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);

		NSDictionary dict = new NSDictionary(individu.noIndividu(), EOIndividu.NO_INDIVIDU_COLKEY);
		Boolean result = clientSideRequestImprimerAvecModule(FICHE_IDENTITE_CIR,dict);
		if (result.booleanValue() == false) {
			return result;
		}
		return new Boolean(true);
	}


	//	Avec Thread
	public Boolean clientSideRequestImprimerFicheIdentite(EOGlobalID individuID,NSArray elementsAImprimer,Boolean peutImprimerInfoPerso) {

		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerFicheIdentite - impression fiche identite");
		NSDictionary dict = new NSDictionary(individu.noIndividu(), EOIndividu.NO_INDIVIDU_COLKEY);
		Boolean result = clientSideRequestImprimerAvecModule(FICHE_IDENTITE_INDIVIDU,dict);
		if (result.booleanValue() == false) {
			return result;
		}

		return new Boolean(true);
	}
	public NSDictionary clientSideRequestImprimerBlocNotesSansThread(InfoPourEditionRequete info,EOGlobalID individuID) {
		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		// sans thread
		EOEditingContext editingContext = getNewEditingContext();
		NSArray notes = EOBlocNotes.rechercherNotesPourIndividuEtPeriode(editingContext, individu,info.debutPeriode(),info.finPeriode());
		return Imprimeur.sharedInstance().imprimerBlocNotes(info,notes);
	}
	public NSDictionary clientSideRequestImprimerSyntheseCarriereSansThread(EOGlobalID individuID) {
		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		return Imprimeur.sharedInstance().imprimerSyntheseCarriere(individu);
	}
	public Boolean clientSideRequestImprimerSyntheseCarriere(EOGlobalID individuID) {
		EOIndividu individu = (EOIndividu)recordPourGlobalID(individuID);
		try {
			Class[] classeParametres =  new Class[] {EOIndividu.class};
			Object[] parametres = new Object[]{individu};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerSyntheseCarriere",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}
	}

	public Boolean clientSideRequestImprimerPromotions(Integer anneePromotion,NSArray promotionsID,String titre,String sousTitre) {
		LogManager.logDetail(loginAgent + " : clientSideRequestImprimerPromotions");
		try {                        
			EOEditingContext editingContext = getNewEditingContext();
			NSArray promotions = Utilitaires.tableauObjetsMetiers(promotionsID,editingContext);

			Class[] classeParametres =  new Class[] {NSTimestamp.class,NSTimestamp.class,NSArray.class,String.class,String.class};
			Object[] parametres = new Object[]{
					DateCtrl.debutAnnee(anneePromotion),
					DateCtrl.finAnnee(anneePromotion),
					promotions,
					titre,sousTitre};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerPromotions",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}

	public Boolean clientSideRequestimprimerFicheLATA(EOGlobalID promotionID,NSTimestamp dateReference) {

		try {                                    
			EOEditingContext editingContext = getNewEditingContext();
			EOHistoPromotion promotion = (EOHistoPromotion)SuperFinder.objetForGlobalIDDansEditingContext(promotionID, editingContext);

			Class[] classeParametres	=  new Class[] {EOHistoPromotion.class, NSTimestamp.class};
			Object[] parametres = new Object[]{promotion,dateReference};
			threader = new ServerThreadManager(Imprimeur.sharedInstance(),"imprimerFicheLATA",classeParametres,parametres);
			threader.start();
			return new Boolean(true);
		} catch (Exception e) {
			LogManager.logException(e);
			return new Boolean(false);
		}	
	}
	/**
	 * Permet d'envoyer un mail a partir du client.
	 * 
	 * @param ec
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 */
	public void clientSideRequestSendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) throws Exception {
		try {
			if (!mailBus().sendMail(mailFrom,mailTo,mailCC, mailSubject,mailBody)) {
				throw new Exception ("Erreur lors de l'envoi du mail.");  
			}
		} catch (Throwable e) {
			throw new Exception (e.getMessage());
		}

	}

}
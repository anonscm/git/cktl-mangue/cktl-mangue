package org.cocktail.mangue.server;
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartStructure;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

public class CalcAffectationsTaskController {

	private static CalcAffectationsTaskController sharedInstance;

	private EOEditingContext ec;
	private Timer currentTimer;

	boolean updateServicesPeres = false;

	public CalcAffectationsTaskController(EOEditingContext editingContext) {

		ec = editingContext;

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CalcAffectationsTaskController sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CalcAffectationsTaskController(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 * @return
	 */
	public Timer getTimer() {
		return currentTimer;
	}

	/**
	 * 
	 * @param aTimer
	 */
	public void setTimer(Timer aTimer) {
		currentTimer = aTimer;
	}


	/**
	 * 
	 * @param jours
	 */
	public void programmer(String heure) {

		NSTimestamp dateJour = new NSTimestamp();

		if (System.getProperties().getProperty("WOCronMajAnnuaire") != null && System.getProperties().getProperty("WOCronMajAnnuaire").equals("true")) {

			System.out.println("/**********************************************");
			System.out.println("	Programmation affectations - " + DateCtrl.dateToString(dateJour, "%d/%m/%Y %H:%M:%S"));
			System.out.println("	Toutes les affectations dans les services seront recalculées à " + heure);

			currentTimer = new Timer(true);

			// Recuperation du delai entre l'heure actuelle et l'heure de programmation
			GregorianCalendar calendar1 = new GregorianCalendar();
			calendar1.setTime(dateJour);
			long flag1 = calendar1.getTimeInMillis();

			GregorianCalendar calendar2 = new GregorianCalendar();
			NSTimestamp dateProg = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp(),"%d/%m/%Y")+" "+heure,"%d/%m/%Y %H:%M");		
			calendar2.setTime(dateProg);
			long flag2 = calendar2.getTimeInMillis();

			long delay = flag2 - flag1;

			if (delay < 0) {
				delay = 86400000  + delay;
			}
			System.out.println("	Demarrage dans " + ((delay / 1000) / 60) + " minutes ...");
			System.out.println(" *********************************************/");

			long period = 86400000;	// Toutes les 24 Heures

			currentTimer.scheduleAtFixedRate(new CalcBSTask(), delay, period);

		}

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class CalcBSTask extends TimerTask {

		public CalcBSTask() {
		}

		public void run() {		

			EOEditingContext edc = new EOEditingContext();

			System.out.println("Application.Application() TRAITEMENT DES AFFECTATIONS !!!!!!!!");
			try {

				// Recuperation de toutes les affectations connues depuis le debut de l'annee
				// Suppression dans repart strucure des services pour lesquels l affectation est cloturee
				// Ajout pour la ou les affectations en cours
				System.out.println(">>>>>>>>>>>>>> CalcAffectationsTaskController.CalcBSTask.run() RECUPERATION AFFECTATIONS ");
				EOQualifier qualifier = ERXQ.and(
						ERXQ.equals(EOAffectation.TEM_VALIDE_KEY, "O"),
						//ERXQ.equals(EOAffectation.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, "ALEJO"),
						ERXQ.lessThanOrEqualTo(EOAffectation.DATE_DEBUT_KEY, DateCtrl.today()),
						ERXQ.or(ERXQ.isNull(EOAffectation.DATE_FIN_KEY), ERXQ.greaterThanOrEqualTo(EOAffectation.DATE_FIN_KEY, DateCtrl.debutAnnee(DateCtrl.getYear(DateCtrl.today()))))
						);
				NSArray<EOAffectation> affectationsCourantes = EOAffectation.rechercherAffectationsAvecCriteres(edc, qualifier);
				System.out.println(">>>>>>>>>>>>>> CalcAffectationsTaskController.CalcBSTask.run() AFFECTATIONS A TRAITER : " + affectationsCourantes.count());

				NSMutableArray<EOIndividu> individusTraites = new NSMutableArray<EOIndividu>();

				int indexAff = 1;

				for (EOAffectation myAffectation : affectationsCourantes) {
					
					System.out.println("CalcAffectationsTaskController.CalcBSTask.run() AFFECTATIONS TRAITEES : " + indexAff + " / " + affectationsCourantes.count());

					EOIndividu individu = myAffectation.individu();
					EOStructure structure = myAffectation.toStructureUlr();
					EORepartStructure repart = EORepartStructure.rechercherPourPersIdEtStructure(edc, myAffectation.individu().persId(), structure);

					boolean estPrisEnCharge = true;

					NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(ec, individu, DateCtrl.today(), DateCtrl.today());
					if (carrieres.count() == 0) {
						NSArray<EOContratHeberges> contratsHeberges = EOContratHeberges.rechercherContratsEnCoursPourIndividu(ec, individu);
						if (contratsHeberges.count() == 0) {
							NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(ec, individu, DateCtrl.today(), DateCtrl.today(), false);
							if (contrats.count() == 1) {										
								for (EOContrat myContrat : contrats) {
									if ( myContrat.toTypeContratTravail().estEtudiant()) {
										estPrisEnCharge = false;
									}
								}
							}
							else {
								if (contrats.count() == 0) {
									estPrisEnCharge = false;
								}
							}
						}
					}

					System.out
					.println("\t Individu : " + individu.identitePrenomFirst() + " - Structure : " + structure.llStructure()); 

					try {

						if (!estPrisEnCharge ||
								(myAffectation.dateFin() != null && DateCtrl.isBefore(myAffectation.dateFin(), DateCtrl.today())) ) {
							// Suppression de l association dans le service traite
							if (repart != null) {
								System.out.println("\t\t SUPPRESSION Structure : " + structure.llStructure()); 
								edc.deleteObject(repart);
							}

							if (updateServicesPeres) {
								// Suppression dans les services peres
								EOStructure structurePere = structure.toStructurePere();
								while (structurePere.estEtablissement() == false 
										&& structurePere.estEtablissementPrincipal() == false) {								
									EORepartStructure repartPere = EORepartStructure.rechercherPourPersIdEtStructure(edc, myAffectation.individu().persId(), structurePere);
									System.out.println("\t\t SUPPRESSION Structure pere : " + structurePere.llStructure()); 
									if (repartPere != null) {
										edc.deleteObject(repartPere);
									}
									structurePere = structurePere.toStructurePere();
								}
							}

						}
						else {

							// AJOUT SEULEMENT POUR LES AGENTS AYANT UNE CARRIERE OU UN CONTRAT NON 'VF', 'VN', 'DO', 'EE'
							if (DateCtrl.isBeforeEq(myAffectation.dateDebut(), DateCtrl.today())
									&& (myAffectation.dateFin() == null || DateCtrl.isAfterEq(myAffectation.dateFin(), DateCtrl.today())) ) {

								if (estPrisEnCharge) {

									// Ajout de l association dans le service traite
									System.out.println("\t\t AJOUT Structure : " + structure.llStructure()); 
									if (repart == null) {
										EORepartStructure newRepart = new EORepartStructure();
										newRepart.setPersId(myAffectation.individu().persId());
										newRepart.setStructureRelationship(myAffectation.toStructureUlr());
										newRepart.setCStructure(myAffectation.toStructureUlr().cStructure());

										edc.insertObject(newRepart);
									}
									// Ajout dans les services peres
									if (updateServicesPeres) {
										EOStructure structurePere = structure;
										while (structurePere.estComposante() == false 
												&& structurePere.estEtablissement() == false 
												&& structurePere.estEtablissementPrincipal() == false) {								

											structurePere = structurePere.toStructurePere();

											EORepartStructure repartPere = EORepartStructure.rechercherPourPersIdEtStructure(edc, myAffectation.individu().persId(), structurePere);

											if (repartPere == null) {										
												System.out.println("\t\t AJOUT Structure pere : " + structurePere.llStructure()); 
												EORepartStructure newRepart = new EORepartStructure();
												newRepart.setPersId(myAffectation.individu().persId());
												newRepart.setStructureRelationship(structurePere);
												newRepart.setCStructure(structurePere.cStructure());

												edc.insertObject(newRepart);
											}							
										}
									}
								}

							}
						}

						edc.saveChanges();

						individusTraites.addObject(myAffectation.individu());

						indexAff++;
					}
					catch (Exception e)  {
						e.printStackTrace();
					}

				}


			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
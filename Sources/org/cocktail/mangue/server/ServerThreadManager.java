//
//  ServerThreadManager.java
//  TestThread
//
//  Created by Christine Buttin on Mon Jun 28 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//
/** @author christine
 * G&egrave;re le lancement de threads sur le serveur d'applications :
 * La m&eacute;thode invoqu&eacute; doit admettre comme dernier param&eegrave;tre une r&eacute;f&eacu;terence sur le thread manager
 * pour pouvoir communiquer avec celui-ci
 * ex: public void imprimer(Object parametre1,Object parametre2,org.cocktail.mangue.serveur.ServerThreadManager thread)
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.server;
import org.cocktail.common.LogManager;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;


public class ServerThreadManager extends Thread {
	private NSMutableDictionary resultat;
	private NSMutableDictionary diagnostic;
	private String nomMethodeAInvoquer;
	private Class[] classeParametres;
	private Object[] parametres;
	private Object target;
	private Class classeTarget;

	/** Statuts du serveur */
	public static final String EN_COURS = "En cours";
	public static final String TERMINE = "Termine";
	/** constructeur
	 * @param objetCible objet dans lequel on invoque la methode
	 * @param classeCible classe de cet objet
	 * @param nomMethode methode a invoquer
	 * @param classes classes des parametres de la methode
	 * @param valeurs valeurs des parametres 
	 */
	public ServerThreadManager(Object objetCible,Class classeCible,String nomMethode,Class[] classes,Object[]valeurs) {
		target = objetCible;
		classeTarget = classeCible;
		nomMethodeAInvoquer = nomMethode;
		preparerParametres(classes,valeurs);	// pour ajouter la référence sur le thread manager
		resultat = new NSMutableDictionary(2);
		diagnostic = new NSMutableDictionary(3);
		
	}
	
	/** constructeur
	 * @param objetCible objet dans lequel on invoque la methode
	 * @param nomMethode methode a invoquer
	 * @param classes classes des parametres de la methode
	 * @param valeurs valeurs des parametres 
	 */
	public ServerThreadManager(Object objetCible,String nomMethode,Class[] classes,Object[]valeurs) {
		this(objetCible,objetCible.getClass(),nomMethode,classes,valeurs);
	}
	/** constructeur pour invocation d'une methode de classe
	 * @param classeCible classe
	 * @param nomMethode methode a invoquer
	 * @param classes classes des parametres de la methode
	 * @param valeurs valeurs des parametres 
	 */
	public ServerThreadManager(Class classeCible,String nomMethode,Class[] classes,Object[]valeurs) {
		this(null,classeCible,nomMethode,classes,valeurs);
	}
	
	/** retourne le resultat de la methode invoquee sous la forme d'un dictionnaire 
	 * dont les cles sont la classe du resultat et le resultat lui-meme **/
	public NSDictionary resultat() {
		return resultat;
	}
	/** retourne le diagnostic de la methode invoquee sous la forme d'un dictionnaire **/
	public NSDictionary diagnostic() {
		return diagnostic;
	}
	
	/** retourne la classe du resultat */
	public String classeResultat() {
		return (String)resultat.objectForKey("classe");
	}
	
	/** retourne la valeur du resultat */
	public Object valeurResultat() {
		return resultat.objectForKey("valeur");
	}
	
	/** modifie la classe du resultat */
	public void setClasseResultat(String nomClasse) {
		resultat.setObjectForKey(nomClasse,"classe");
	}
	/** modifie la valeur du resultat */
	public void setValeurResultat(Object valeur) {
		resultat.setObjectForKey(valeur,"valeur");
	}
	/** retourne le status de l'operation en cours */
	public String status() {
		return (String)diagnostic.objectForKey("status");
	}
	/** retourne le message d'information de l'operation en cours. Peut contenir plusieurs messages separes par
	 * "\n" si l'operation a emis plusieurs messages qui n'ont pas ete transmis */
	public String message() {
		String texte = (String)diagnostic.objectForKey("message");
		diagnostic.removeObjectForKey("message");
		return texte;
	}
	/** retourne le message d'exception declenche pendant l'operation */
	public String exception() {
		return (String)diagnostic.objectForKey("exception");
	}
	/** modifie le status de l'operation en cours */
	public void setStatus(String unTexte) {
		diagnostic.setObjectForKey(unTexte,"status");
	}
	/** modifie le message d'information de l'operation en cours */
	public void setMessage(String unTexte) {
		setMessage(unTexte,true);
	}
	/** modifie le message d'information de l'operation en cours. Si append = true ajoute le texte
	 * au message courant */
	public void setMessage(String unTexte,boolean append) {
		// concaténer les messages si ils n'ont pas encore été lus
		if (append && diagnostic.objectForKey("message") != null) {
			diagnostic.setObjectForKey(diagnostic.objectForKey("message") + "\n" + unTexte,"message");
		} else {
			diagnostic.setObjectForKey(unTexte,"message");
		}
	}
	/** modifie le message d'exception de l'operation en cours */
	public void setException(String unTexte) {
		if (unTexte != null) {		
			diagnostic.setObjectForKey(unTexte,"exception");
		}
	}
	/** lance l'execution de la methode : a utiliser pour une application Java Client */
	public void run() {
		lancerOperation();
	}
	/** lance l'execution de la methode : a utiliser pour une application WO HTML */
	public void lancerOperation() {
		try {
			setStatus(EN_COURS);
			java.lang.reflect.Method methode = classeTarget.getMethod(nomMethodeAInvoquer,classeParametres);
			Object object = methode.invoke(target, parametres);
			if (object != null) {
				setClasseResultat(object.getClass().getName());
				setValeurResultat(object);
			}
			setStatus(TERMINE);

		} catch(Exception e) {
			LogManager.logException(e);
			setException(e.getMessage());
			setStatus(TERMINE);

		}
	}
	private void preparerParametres(Class[] classes,Object[] valeurs) {
		classeParametres = new Class[classes.length + 1];
		for (int i = 0; i < classes.length; i++) {
			classeParametres[i] = classes[i];
		}
		try {
			classeParametres[classes.length] = this.getClass();
		} catch (Exception e) {}
		parametres = new Object[valeurs.length + 1];
		for (int i = 0; i < valeurs.length; i++) {
			parametres[i] = valeurs[i];
		}
		parametres[valeurs.length] = this;
	}		
}

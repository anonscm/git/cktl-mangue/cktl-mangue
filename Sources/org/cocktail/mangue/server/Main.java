package org.cocktail.mangue.server;
//
//  Main.java
//  Gepeto
//
//  Created by Christine Buttin on Wed Apr 13 2005.
//  Copyright (c) 2001 __MyCompanyName__. All rights reserved.
//

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eodistribution.WOJavaClientComponent;

public class Main extends WOComponent {

    public Main(WOContext context) {
        super(context);
    }

    public String javaClientLink() {
        return WOJavaClientComponent.webStartActionURL(context(), "JavaClient");
    }    
}

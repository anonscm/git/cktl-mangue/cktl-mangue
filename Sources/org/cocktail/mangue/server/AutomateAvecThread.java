// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SupInfoCtrl.java

package org.cocktail.mangue.server;

import org.cocktail.common.LogManager;

import com.webobjects.eocontrol.EOEditingContext;

public class AutomateAvecThread {

	private ServerThreadManager threadCourant;

	private EOEditingContext edc;

	public AutomateAvecThread(EOEditingContext edc) {
		setEdc(edc);
	}

	protected EOEditingContext getEdc() {
		return edc;
	}

	protected void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	protected ServerThreadManager getThreadCourant() {
		return threadCourant;
	}

	protected void setThreadCourant(ServerThreadManager threadCourant) {
		this.threadCourant = threadCourant;
	}

	/**
	 * 
	 * @param message
	 */
	protected void informerThread(String message) {
		if (message != null) {
			if (getThreadCourant() != null) {
				getThreadCourant().setMessage(message);
			}
			else {
				LogManager.logInformation(message);
			}
		}
	}
}
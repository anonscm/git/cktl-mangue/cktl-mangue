package org.cocktail.mangue.server;
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

import java.net.URL;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.cir.EOCirFichierImport;
import org.cocktail.mangue.modele.mangue.cir.EOCirIdentite;
import org.cocktail.mangue.server.cir.AutomateCir;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class CalcCirTaskController {

	private static CalcCirTaskController sharedInstance;

	private EOEditingContext edc;
	private Timer currentTimer;
	private ServerThreadManager threader;
	private EOCirFichierImport fichierCir;

	public CalcCirTaskController(EOEditingContext edc) {

		this.edc = edc;

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CalcCirTaskController sharedInstance(EOEditingContext edc)	{
		if (sharedInstance == null)	
			sharedInstance = new CalcCirTaskController(edc);
		return sharedInstance;
	}


	public EOCirFichierImport getFichierCir() {
		return fichierCir;
	}

	public void setFichierCir(EOCirFichierImport fichierCir) {
		this.fichierCir = fichierCir;
	}

	// Arrete de la programmation
	public void stopProgrammation() {
		if (getTimer() != null)
			getTimer().cancel();
		System.out.println("CalcCirTaskController.stopProgrammation() ARRET DE LA PROGRAMMATION DU CIR !");
	}
	// Programmation
	public void startProgrammation(EOCirFichierImport fichier) {
		setFichierCir(fichier);
		programmer();
	}

	/**
	 * 
	 * @return
	 */
	public Timer getTimer() {
		return currentTimer;
	}

	/**
	 * 
	 * @param aTimer
	 */
	public void setTimer(Timer aTimer) {
		currentTimer = aTimer;
	}


	/**
	 * 
	 * @param jours
	 */
	public void programmer() {

		if (getFichierCir() == null) {
			setFichierCir(EOCirFichierImport.findFichierProgramme(edc));
			if (getFichierCir() == null)
				return;
		}

		NSTimestamp dateJour = new NSTimestamp();

		System.out.println("/**********************************************");
		System.out.println("	Programmation calcul CIR - " + DateCtrl.dateToString(dateJour, "%d/%m/%Y %H:%M:%S"));
		System.out.println("	Toutes les données CIR seront recalculées à " + getFichierCir().cfimHeureCalcul());

		if (getTimer() == null)
			currentTimer = new Timer(true);

		// Recuperation du delai entre l'heure actuelle et l'heure de programmation
		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.setTime(dateJour);
		long flag1 = calendar1.getTimeInMillis();

		GregorianCalendar calendar2 = new GregorianCalendar();
		NSTimestamp dateProg = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp(),"%d/%m/%Y")+" "+getFichierCir().cfimHeureCalcul(),"%d/%m/%Y %H:%M");		
		calendar2.setTime(dateProg);
		long flag2 = calendar2.getTimeInMillis();

		long delay = flag2 - flag1;

		if (delay < 0) {
			delay = 86400000  + delay;
		}
		System.out.println("	Demarrage dans " + ((delay / 1000) / 60) + " minutes ...");
		System.out.println(" *********************************************/");

		long period = 86400000;	// Toutes les 24 Heures

		currentTimer.scheduleAtFixedRate(new CalcComptesCirTask(), delay, period);

	}


	public String pathFichierCirCAR() {
		return System.getProperty("java.io.tmpdir");
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class CalcComptesCirTask extends TimerTask {

		public CalcComptesCirTask() {
		}

		/**
		 * 
		 */
		public void run() {		

			EOEditingContext edc = getFichierCir().editingContext();
			URL path = WOApplication.application().resourceManager().pathURLForResourceNamed("DescriptionCir.XML",null,null);

			System.out.println(">>>> Application.Application() TRAITEMENT DES COMPTES CIR <<<<");
			
			try {

				// Recuperation des comptes a mettre à jour
				NSMutableArray<EOCirIdentite> identites = new NSMutableArray<EOCirIdentite>(EOCirIdentite.findForAnnee(edc, new Integer (getFichierCir().cfimAnnee().intValue() - 1)));
				EOSortOrdering.sortArrayUsingKeyOrderArray(
						identites, 
						new NSArray(new EOSortOrdering(EOCirIdentite.D_MODIFICATION_KEY, EOSortOrdering.CompareAscending)) );

				System.out
					.println("CalcCirTaskController.CalcComptesCirTask.run() IDENTITE : " + identites.get(0).toIndividu().nomUsuel());
				System.out
					.println("CalcCirTaskController.CalcComptesCirTask.run() MODIF : " + DateCtrl.dateToString(identites.get(0).toIndividu().dModification()));
				
				System.out
				.println("\t Nombre de comptes à recalculer pour l'année " + getFichierCir().cfimAnnee() + " : " + identites.count());

				int indexIndividus = 1;
				for (EOCirIdentite identite : identites) {
						NSMutableArray<EOIndividu> individus = new NSMutableArray<EOIndividu>();
						individus.addObject(identite.toIndividu());

						AutomateCir.sharedInstance().preparerRecords(getFichierCir(), individus,  path.getPath(), false, null);
						if (indexIndividus%30 == 0)
							System.out.println(" Nombre de dossiers traités : " + indexIndividus + " / " + identites.count() + " ... " );
						
						identite.setDModification(new NSTimestamp());
						edc.saveChanges();
						
						indexIndividus ++ ;
					}

			} catch (Exception e) {
				LogManager.logException(e);
				e.printStackTrace();
				System.out
				.println("CalcCirTaskController.CalcComptesCirTask.run() ERREUR DE TRAITEMENT !!");
			}

		}

	}

}
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.medical;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.individu.PeriodesHandicapView;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.mangue.client.select.SituationHandicapSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOSituationHandicap;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeHandicap;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodeHandicap;

import com.webobjects.eointerface.EODisplayGroup;

public class PeriodesHandicapCtrl extends ModelePageGestion {

	private PeriodesHandicapView 		myView;
	private EODisplayGroup 				eod;
	private InfosComplementairesCtrl 	ctrlParent;

	private ListenerPeriode 		listenerPeriode = new ListenerPeriode();
	private EOPeriodeHandicap		currentPeriode;
	private EOTypeHandicap			currentType;
	private EOSituationHandicap		currentSituation;
	private EOIndividu 				currentIndividu;

	public PeriodesHandicapCtrl(InfosComplementairesCtrl ctrlParent) {

		super(ctrlParent.getEdc());
		this.ctrlParent = ctrlParent;
		eod = new EODisplayGroup();
		myView = new PeriodesHandicapView(null, eod, false);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnGetSituation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectSituationHandicap();}
		});
		myView.getBtnDelSituation().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delSituationHandicap();}
		});

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());
		setDateListeners(myView.getTfDateMedecin());

		setSaisieEnabled(false);

		myView.getMyEOTable().addListener(listenerPeriode);		
		CocktailUtilities.initTextField(myView.getTfSituation(), false, false);
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}

	public EOPeriodeHandicap getCurrentPeriode() {
		return currentPeriode;
	}

	public void setCurrentPeriode(EOPeriodeHandicap currentPeriode) {
		this.currentPeriode = currentPeriode;
		updateDatas();
	}

	public EOTypeHandicap getCurrentType() {
		return currentType;
	}

	public void setCurrentType(EOTypeHandicap currentType) {
		this.currentType = currentType;
	}

	public EOSituationHandicap getCurrentSituation() {
		return currentSituation;
	}

	public void setCurrentSituation(EOSituationHandicap currentSituation) {
		this.currentSituation = currentSituation;
		CocktailUtilities.viderTextField(myView.getTfSituation());
		if (currentSituation != null) {
			myView.getTfSituation().setText(getCurrentSituation().libelleLong());
		}
		updateInterface();

	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public void actualiser() {
		eod.setObjectArray(EOPeriodeHandicap.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}
	
    public JPanel getViewHandicap() {
        return myView.getViewHandicap();
    }
        
	private void selectSituationHandicap() {

		EOSituationHandicap situation = SituationHandicapSelectCtrl.sharedInstance(getEdc()).getSituation();

		if (situation != null) {
			setCurrentSituation(situation);
		}
	}

	private void delSituationHandicap() {
		currentSituation = null;
		myView.getTfSituation().setText("");
	}

	private class ListenerPeriode implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentPeriode != null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentPeriode((EOPeriodeHandicap)eod.selectedObject());
		}
	}
	
	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfDateMedecin());
		CocktailUtilities.viderTextField(myView.getTfSituation());
		CocktailUtilities.viderTextField(myView.getTfTaux());
	}
	
	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		
		if (getCurrentPeriode() != null) {

			setCurrentType(getCurrentPeriode().typeHandicap());
			setCurrentSituation(getCurrentPeriode().situationHandicap());
			
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentPeriode().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentPeriode().dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateMedecin(), getCurrentPeriode().dateAvisMedecin());

			CocktailUtilities.setNumberToField(myView.getTfTaux(), getCurrentPeriode().tauxHandicap());
		}
		
		updateInterface();

	}
	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateMedecin(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfTaux(), false, isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null && !isSaisieEnabled());		
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentPeriode() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentPeriode() != null);

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getBtnGetSituation().setEnabled(isSaisieEnabled());
		myView.getBtnDelSituation().setEnabled(isSaisieEnabled() && getCurrentType() != null);

	}
	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentPeriode().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentPeriode().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
		getCurrentPeriode().setDateAvisMedecin(CocktailUtilities.getDateFromField(myView.getTfDateMedecin()));
		getCurrentPeriode().setTauxHandicap(CocktailUtilities.getDoubleFromField(myView.getTfTaux()));		
		getCurrentPeriode().setSituationHandicapRelationship(getCurrentSituation());
		getCurrentPeriode().setTypeHandicapRelationship(getCurrentType());

	}
	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(false);

	}
	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(false);		
		listenerPeriode.onSelectionChanged();
	}
	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentPeriode(EOPeriodeHandicap.creer(getEdc(), getCurrentIndividu()));
		ctrlParent.setIsLocked(true);
	}
	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(true);
		
	}
	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(getCurrentPeriode());

	}
	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}
}

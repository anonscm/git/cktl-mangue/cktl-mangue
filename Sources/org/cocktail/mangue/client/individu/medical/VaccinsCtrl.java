package org.cocktail.mangue.client.individu.medical;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.VaccinsView;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EORappelVaccin;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeVaccin;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.individu.medical.EODonneesMedicales;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVaccin;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class VaccinsCtrl {

	private EOEditingContext ec;

	private VaccinsView myView;
	private DonneesMedicalesCtrl ctrlParent;
	private EOTypeVaccin currentTypeVaccin;
	private EORappelVaccin currentTypeRappel;
	private EOVaccin currentObject;
	private boolean saisieEnabled;

	public VaccinsCtrl(DonneesMedicalesCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;
		
		myView = new VaccinsView();

		myView.getTfDateProchainRappel().addFocusListener(new FocusListenerDateTextField(myView.getTfDateProchainRappel()));
		myView.getTfDateProchainRappel().addActionListener(new ActionListenerDateTextField(myView.getTfDateProchainRappel()));

		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeVaccin(), NomenclatureFinder.findStatic(ec, EOTypeVaccin.ENTITY_NAME), true);
		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeRappel(), NomenclatureFinder.findStatic(ec, EORappelVaccin.ENTITY_NAME), true);
		
		setSaisieEnabled(false);
		
	}

	public JPanel getView() {
		return myView;
	}	

	
	public EOTypeVaccin currentTypeVaccin() {
		return currentTypeVaccin;
	}

	public void setCurrentTypeVaccin(EOTypeVaccin currentTypeVaccin) {
		this.currentTypeVaccin = currentTypeVaccin;
		myView.getPopupTypeVaccin().setSelectedIndex(0);
		if (currentTypeVaccin != null)
			myView.getPopupTypeVaccin().setSelectedItem(currentTypeVaccin);
	}

	public EORappelVaccin currentTypeRappel() {
		return currentTypeRappel;
	}

	public void setCurrentTypeRappel(EORappelVaccin currentResultatVisite) {
		this.currentTypeRappel = currentResultatVisite;
		myView.getPopupTypeRappel().setSelectedIndex(0);
		if (currentResultatVisite != null)
			myView.getPopupTypeRappel().setSelectedItem(currentTypeRappel);
	}

	public EOVaccin currentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOVaccin currentObject) {
		this.currentObject = currentObject;
	}

	public void clearTextFields() {
		myView.getTfDateProchainRappel().setText("");
		setCurrentTypeVaccin(null);
		setCurrentTypeVaccin(null);
	}
	
	public void ajouter(EOVaccin newObject) {		
		setCurrentObject(newObject);
	}
	
	public EOTypeVaccin getTypeVaccin() {
		if (myView.getPopupTypeVaccin().getSelectedIndex() > 0)
			return (EOTypeVaccin) myView.getPopupTypeVaccin().getSelectedItem();
		return null;
	}
	public EORappelVaccin getRappelVaccin() {
		if (myView.getPopupTypeRappel().getSelectedIndex() > 0)
			return (EORappelVaccin) myView.getPopupTypeRappel().getSelectedItem();
		return null;
	}

	
	public void valider() {
		
		currentObject().setMedecinRelationship(ctrlParent.getCurrentMedecin());
		currentObject().setDate(ctrlParent.getDate());
		currentObject().setHeure(ctrlParent.getHeure());
		currentObject.setDateRappel(CocktailUtilities.getDateFromField(myView.getTfDateProchainRappel()));
		currentObject.setTypeVaccinRelationship(getTypeVaccin());
		currentObject.setRappelVaccinRelationship(getRappelVaccin());
		
	}
	
	public void supprimer() throws Exception {
		try {			
			ec.deleteObject(currentObject);
		}
		catch (Exception ex) {
			throw ex;
		}
	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();

	}

	private void updateUI() {
		
		myView.getPopupTypeVaccin().setEnabled(saisieEnabled());
		myView.getPopupTypeRappel().setEnabled(saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateProchainRappel(), false, saisieEnabled());
					
	}

	public void actualiser(EODonneesMedicales donnees) {

		clearTextFields();

		if (donnees != null) {
			
			setCurrentObject(EOVaccin.findForKey(ec, donnees.medId()));
			
			if (currentObject() != null) {	
				
				setCurrentTypeVaccin(currentObject().typeVaccin());
				setCurrentTypeRappel(currentObject().rappelVaccin());
				CocktailUtilities.setDateToField(myView.getTfDateProchainRappel(), currentObject().date());

			}			
		}
	}
		
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	


}

package org.cocktail.mangue.client.individu.medical;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.ExamenMedicalView;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeExamenMedical;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.individu.medical.EODonneesMedicales;
import org.cocktail.mangue.modele.mangue.individu.medical.EOExamenMedical;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class ExamenMedicalCtrl {

	private EOEditingContext ec;

	private ExamenMedicalView myView;

	private DonneesMedicalesCtrl ctrlParent;
	private EOTypeExamenMedical currentTypeExamen;
	private EOExamenMedical currentObject;
	private boolean saisieEnabled;

	public ExamenMedicalCtrl(DonneesMedicalesCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new ExamenMedicalView();

		myView.getTfDateProchain().addFocusListener(new FocusListenerDateTextField(myView.getTfDateProchain()));
		myView.getTfDateProchain().addActionListener(new ActionListenerDateTextField(myView.getTfDateProchain()));

		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeExamen(), NomenclatureFinder.findStatic(ec, EOTypeExamenMedical.ENTITY_NAME), true);

		setSaisieEnabled(false);
		
	}

	public JPanel getView() {
		return myView;
	}	

	
	public EOTypeExamenMedical currentTypeExamen() {
		return currentTypeExamen;
	}

	public void setCurrentTypeExamen(EOTypeExamenMedical currentTypeExamen) {
		this.currentTypeExamen = currentTypeExamen;
		myView.getPopupTypeExamen().setSelectedIndex(0);
		if (currentTypeExamen != null)
			myView.getPopupTypeExamen().setSelectedItem(currentTypeExamen);
	}

	public EOExamenMedical currentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOExamenMedical currentObject) {
		this.currentObject = currentObject;
	}

	public void clearTextFields() {

		myView.getTfDateProchain().setText("");
		setCurrentTypeExamen(null);

	}
	
	public void ajouter(EOExamenMedical newObject) {		
		currentObject = newObject;
	}
	public EOTypeExamenMedical getTypeExamen() {
		if (myView.getPopupTypeExamen().getSelectedIndex() > 0)
			return (EOTypeExamenMedical) myView.getPopupTypeExamen().getSelectedItem();
		return null;
	}

	public void valider() {
		
		currentObject().setMedecinRelationship(ctrlParent.getCurrentMedecin());
		currentObject().setDate(ctrlParent.getDate());
		currentObject().setHeure(ctrlParent.getHeure());

		currentObject.setDateProchain(CocktailUtilities.getDateFromField(myView.getTfDateProchain()));		
		currentObject.setTypeExamenRelationship(getTypeExamen());
				
	}
	
	public void supprimer() throws Exception {
		try {			
			ec.deleteObject(currentObject);
		}
		catch (Exception ex) {
			throw ex;
		}
	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();

	}

	private void updateUI() {
		myView.getPopupTypeExamen().setEnabled(saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateProchain(), false, saisieEnabled());			
	}

	public void actualiser(EODonneesMedicales donnees) {

		clearTextFields();

		if (donnees != null) {
			
			setCurrentObject(EOExamenMedical.findForKey(ec, donnees.medId()));
			
			if (currentObject() != null) {	
				setCurrentTypeExamen(currentObject().typeExamen());
				CocktailUtilities.setDateToField(myView.getTfDateProchain(), currentObject().date());
			}			
		}
	}	
	
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.individu.medical;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.SaisieCertificatMedicalView;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOConclusionMedicale;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.TimeCtrl;
import org.cocktail.mangue.modele.mangue.acc_travail.EOCertificatMedical;
import org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisieCertificatMedicalCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieCertificatMedicalCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieCertificatMedicalView myView;
	
	private CheckBoxListener listenerCheckBox = new CheckBoxListener();
	private EOCertificatMedical currentCertificat;

	public SaisieCertificatMedicalCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieCertificatMedicalView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);

		myView.getTfDateCertificat().addFocusListener(new FocusListenerDateTextField(myView.getTfDateCertificat()));
		myView.getTfDateCertificat().addActionListener(new ActionListenerDateTextField(myView.getTfDateCertificat()));

		myView.getTfDateArret().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArret()));
		myView.getTfDateArret().addActionListener(new ActionListenerDateTextField(myView.getTfDateArret()));
		
		myView.getTfDateSoins().addFocusListener(new FocusListenerDateTextField(myView.getTfDateSoins()));
		myView.getTfDateSoins().addActionListener(new ActionListenerDateTextField(myView.getTfDateSoins()));
		
		myView.getTfDateCertificat().addFocusListener(new FocusListenerDateTextField(myView.getTfDateCertificat()));
		myView.getTfDateCertificat().addActionListener(new ActionListenerDateTextField(myView.getTfDateCertificat()));
		
		myView.getTfDateReprise().addFocusListener(new FocusListenerDateTextField(myView.getTfDateReprise()));
		myView.getTfDateReprise().addActionListener(new ActionListenerDateTextField(myView.getTfDateReprise()));

		myView.getTfDateConclusion().addFocusListener(new FocusListenerDateTextField(myView.getTfDateConclusion()));
		myView.getTfDateConclusion().addActionListener(new ActionListenerDateTextField(myView.getTfDateConclusion()));

		myView.getTfHeureSortieAm().addFocusListener(new FocusListenerHeureTextField(myView.getTfHeureSortieAm()));
		myView.getTfHeureSortieAm().addActionListener(new ActionListenerHeureTextField(myView.getTfHeureSortieAm()));

		myView.getTfHeureSortiePm().addFocusListener(new FocusListenerHeureTextField(myView.getTfHeureSortiePm()));
		myView.getTfHeureSortiePm().addActionListener(new ActionListenerHeureTextField(myView.getTfHeureSortiePm()));

		myView.getCheckArret().addActionListener(listenerCheckBox);
		myView.getCheckSoins().addActionListener(listenerCheckBox);
		myView.getCheckSortie().addActionListener(listenerCheckBox);
		myView.getCheckReprise().addActionListener(listenerCheckBox);
		
		myView.getPopupType().addActionListener(new PopupTypeListener());
		myView.setConclusions(EOConclusionMedicale.fetchAll(ec));
		
		CocktailUtilities.initTextField(myView.getTfDateAccident(), false, false);
		
	}

	public static SaisieCertificatMedicalCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieCertificatMedicalCtrl(editingContext);
		return sharedInstance;
	}



	public EOCertificatMedical currentCertificat() {
		return currentCertificat;
	}

	public void setCurrentCertificat(EOCertificatMedical currentCertificat) {
		this.currentCertificat = currentCertificat;
		updateData();
	}

	private void clearTextFields()	{

		myView.getTfDateAccident().setText("");
		myView.getTfDateArret().setText("");
		myView.getTfDateCertificat().setText("");
		myView.getTfDateArret().setText("");
		myView.getTfDateArret().setText("");
		myView.getTfDateArret().setText("");
		myView.getTaConstatation().setText("");
		
		myView.getTfDateConclusion().setText("");
		myView.getTfTauxIncapacite().setText("");
		myView.getCheckAti().setSelected(false);
		myView.getPopupConclusion().setSelectedIndex(0);
		
	}

	public EOCertificatMedical ajouter(EODeclarationAccident declaration)	{
		setCurrentCertificat(EOCertificatMedical.creer(ec, declaration));
		myView.setVisible(true);
		return currentCertificat();
	}
	public EOCertificatMedical modifier(EOCertificatMedical certificat)	{
		setCurrentCertificat(certificat);
		myView.setVisible(true);
		return currentCertificat();
	}

	
	private void valider()  {

		try {			

			currentCertificat().setEstArretTravail(myView.getCheckArret().isSelected());
			currentCertificat().setEstSoins(myView.getCheckSoins().isSelected());
			currentCertificat().setEstReprise(myView.getCheckReprise().isSelected());
			currentCertificat().setEstAutorisationDeSortie(myView.getCheckSortie().isSelected());

			currentCertificat().setCmedArretDate(CocktailUtilities.getDateFromField(myView.getTfDateArret()));
			currentCertificat().setCmedDate(CocktailUtilities.getDateFromField(myView.getTfDateCertificat()));
			currentCertificat().setCmedRepriseDate(CocktailUtilities.getDateFromField(myView.getTfDateReprise()));
			currentCertificat().setCmedSoinsDate(CocktailUtilities.getDateFromField(myView.getTfDateSoins()));

			currentCertificat().setCmedSortieDebut(CocktailUtilities.getTextFromField(myView.getTfHeureSortieAm()));
			currentCertificat().setCmedSortieFin(CocktailUtilities.getTextFromField(myView.getTfHeureSortiePm()));

			currentCertificat().setCmedConstatation(CocktailUtilities.getTextFromArea(myView.getTaConstatation()));

			switch (myView.getPopupType().getSelectedIndex()) {
			case 0:currentCertificat().setCmedType(null);break;
			case 1:currentCertificat().setEstInitial();break;
			case 2:currentCertificat().setEstProlongation();break;
			case 3:currentCertificat().setEstRechute();break;			
			case 4:currentCertificat().setEstFinal();break;
			}

			if (currentCertificat().estFinal()) {
				currentCertificat().setConclusionRelationship(getConclusion());
				currentCertificat().setCmedConclusionDate(CocktailUtilities.getDateFromField(myView.getTfDateConclusion()));
				currentCertificat().setAAllocationTemporaireInvalidite(myView.getCheckAti().isSelected());
				currentCertificat().setCmedIpp(CocktailUtilities.getBigDecimalFromField(myView.getTfTauxIncapacite()));
			}
			else {
				currentCertificat().setConclusionRelationship(null);
				currentCertificat().setCmedConclusionDate(null);
				currentCertificat().setAAllocationTemporaireInvalidite(false);
				currentCertificat().setCmedIpp(null);				
			}

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		ec.revert();
		setCurrentCertificat(null);
		myView.setVisible(false);
	}

	private void updateData() {
		
		clearTextFields();
		
		if (currentCertificat() != null) {
		
			CocktailUtilities.setTextToArea(myView.getTaConstatation(), currentCertificat().cmedConstatation());

			CocktailUtilities.setDateToField(myView.getTfDateAccident(), currentCertificat().declarationAccident().daccDate());
			CocktailUtilities.setDateToField(myView.getTfDateCertificat(), currentCertificat().cmedDate());
			CocktailUtilities.setDateToField(myView.getTfDateArret(), currentCertificat().cmedArretDate());
			CocktailUtilities.setDateToField(myView.getTfDateSoins(), currentCertificat().cmedSoinsDate());
			CocktailUtilities.setDateToField(myView.getTfDateReprise(), currentCertificat().cmedRepriseDate());

			myView.getCheckArret().setSelected(currentCertificat().estArretTravail());
			myView.getCheckSoins().setSelected(currentCertificat().estSoins());
			myView.getCheckReprise().setSelected(currentCertificat().estReprise());
			myView.getCheckSortie().setSelected(currentCertificat().estAutorisationDeSortie());
			
			if (currentCertificat().estInitial())
				myView.getPopupType().setSelectedIndex(1);
			else
				if (currentCertificat().estProlongation())
					myView.getPopupType().setSelectedIndex(2);
				else
					if (currentCertificat().estRechute())
						myView.getPopupType().setSelectedIndex(3);
					else
						if (currentCertificat().estFinal())
							myView.getPopupType().setSelectedIndex(4);

			CocktailUtilities.setDateToField(myView.getTfDateConclusion(), currentCertificat().cmedConclusionDate());
			myView.getPopupConclusion().setSelectedItem(currentCertificat.conclusion());
			myView.getCheckAti().setSelected(currentCertificat().aAllocationTemporaireInvalidite());
			CocktailUtilities.setNumberToField(myView.getTfTauxIncapacite(), currentCertificat().cmedIpp());
			
		}
		
		updateInterface();
		
	}

	private EOConclusionMedicale getConclusion() {
		if (myView.getPopupConclusion().getSelectedIndex() > 0)
			return (EOConclusionMedicale)myView.getPopupConclusion().getSelectedItem();
		return null;
	}
	private String getType() {
		if (myView.getPopupType().getSelectedIndex() > 0)
			return (String)myView.getPopupType().getSelectedItem();
		return null;
	}

	private class CheckBoxListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			updateInterface();
		}
	}

	/**
	 * 
	 */
	private void updateInterface() {
		
		CocktailUtilities.initTextField(myView.getTfDateArret(), !myView.getCheckArret().isSelected(), myView.getCheckArret().isSelected());
		CocktailUtilities.initTextField(myView.getTfDateSoins(), !myView.getCheckSoins().isSelected(), myView.getCheckSoins().isSelected());
		CocktailUtilities.initTextField(myView.getTfDateReprise(), !myView.getCheckReprise().isSelected(), myView.getCheckReprise().isSelected());

		CocktailUtilities.initTextField(myView.getTfHeureSortieAm(), !myView.getCheckSortie().isSelected(), myView.getCheckSortie().isSelected());
		CocktailUtilities.initTextField(myView.getTfHeureSortiePm(), !myView.getCheckSortie().isSelected(), myView.getCheckSortie().isSelected());
		
		myView.getViewConclusion().setVisible(getType() != null && (getType().substring(0,1)).equals(EOCertificatMedical.TYPE_FINAL));
		
	}
	
	
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

	private class PopupTypeListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			updateInterface();
		}
	}

	private void heureHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = TimeCtrl.timeCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Heure non valide","L'heure saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}

	private class ActionListenerHeureTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			heureHasChanged(myTextField);
		}
	}
	private class FocusListenerHeureTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			heureHasChanged(myTextField);			
		}
	}	



}

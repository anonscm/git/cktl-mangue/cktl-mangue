package org.cocktail.mangue.client.individu.medical;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.individu.DonneesMedicalesView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.TimeCtrl;
import org.cocktail.mangue.modele.InfoPourEditionMedicale;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.individu.medical.EODonneesMedicales;
import org.cocktail.mangue.modele.mangue.individu.medical.EOExamenMedical;
import org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVaccin;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVisiteMedicale;
import org.cocktail.mangue.modele.mangue.individu.medical.InfoMedicalePourIndividu;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class DonneesMedicalesCtrl extends ModelePageGestion {

	private static Boolean MODE_MODAL = Boolean.FALSE;
	private DonneesMedicalesView myView;

	private ListenerDonnees myListener = new ListenerDonnees();
	private NSArray<EODestinataire> destinatairesArrete;
	private InfosComplementairesCtrl ctrlParent;
	private EODisplayGroup eod;
	private EODonneesMedicales currentDonnee;
	private EOIndividu currentIndividu;

	private VisitesCtrl 		ctrlVisites;
	private VaccinsCtrl 		ctrlVaccins;
	private ExamenMedicalCtrl 	ctrlExamens;

	private PopupDonneeListener 	listenerDonneesMedicales = new PopupDonneeListener();

	public DonneesMedicalesCtrl(InfosComplementairesCtrl ctrlParent, EOEditingContext edc) {

		super(edc);
		this.ctrlParent = ctrlParent;

		eod = new EODisplayGroup();
		myView = new DonneesMedicalesView(null, MODE_MODAL.booleanValue(),eod);

		myView.getMyEOTable().addListener(myListener);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDate());

		myView.getBtnImprimerArrete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
				);

		CocktailUtilities.initPopupAvecListe(myView.getPopupMedecins(), EOMedecinTravail.fetchAll(getEdc()), true);

		ctrlVisites = new VisitesCtrl(this);
		ctrlExamens = new ExamenMedicalCtrl(this, getEdc());
		ctrlVaccins = new VaccinsCtrl(this, getEdc());

		// Initialisation des layouts
		myView.getSwapView().add("VIDE",new JPanel(new BorderLayout()));
		myView.getSwapView().add(EODonneesMedicales.TYPE_VISITE,ctrlVisites.getView());
		myView.getSwapView().add(EODonneesMedicales.TYPE_VACCIN,ctrlVaccins.getView());
		myView.getSwapView().add(EODonneesMedicales.TYPE_EXAMEN,ctrlExamens.getView());

		myView.getTfHeure().addFocusListener(new FocusListenerHeureTextField(myView.getTfHeure()));
		myView.getTfHeure().addActionListener(new ActionListenerHeureTextField(myView.getTfHeure()));

		setSaisieEnabled(false);
		myView.getBtnImprimerArrete().setEnabled(false);
		myView.getPopupTypes().addActionListener(listenerDonneesMedicales);

		setSaisieEnabled(false);

	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

	}

	public EODonneesMedicales getCurrentDonnee() {
		return currentDonnee;
	}
	public void setCurrentDonnee(EODonneesMedicales currentDonnee) {
		this.currentDonnee = currentDonnee;
		updateDatas();
	}
	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public EOMedecinTravail getCurrentMedecin() {
		return (myView.getPopupMedecins().getSelectedIndex() > 0)?(EOMedecinTravail)myView.getPopupMedecins().getSelectedItem():null;
	}

	public void actualiser() {		
		eod.setObjectArray(EODonneesMedicales.findForIndividu(getEdc(), currentIndividu));
		myView.getMyEOTable().updateData();
		updateInterface();
	}
	public JPanel getViewMedicale() {
		return myView.getViewDonneesMedicales();
	}

	public NSTimestamp getDate() {
		return CocktailUtilities.getDateFromField(myView.getTfDate());
	}
	public String getHeure() {
		return CocktailUtilities.getTextFromField(myView.getTfHeure());
	}
	/**
	 * Impression des convocations visite, vaccin ou examen
	 */
	private void imprimer() {

		destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				

				InfoMedicalePourIndividu infoMedicale = null;
				String methodeImpression = null;
				String nomImpression = null;

				switch (myView.getPopupTypes().getSelectedIndex()) {
				case 1 : infoMedicale = ctrlVisites.getCurrentObject();
				methodeImpression = "clientSideRequestImprimerConvocationVisite"; 
				nomImpression = "ConvocationVisite";break;
				case 2 : infoMedicale = ctrlVaccins.currentObject();
				methodeImpression = "clientSideRequestImprimerConvocationVaccin"; 
				nomImpression = "ConvocationVaccin";break;
				case 3 : infoMedicale = ctrlExamens.currentObject();
				methodeImpression = "clientSideRequestImprimerConvocationExamen"; 
				nomImpression = "ConvocationExamen";break;
				}

				InfoPourEditionMedicale info = new InfoPourEditionMedicale(InfoPourEditionMedicale.TYPE_CONVOCATION,
						methodeImpression,nomImpression, nomImpression);

				NSMutableArray<EOGlobalID> destinatairesGlobalIds = new NSMutableArray<EOGlobalID>();
				for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(getEdc().globalIDForObject(e.nextElement())));
				Class[] classeParametres = new Class[] {EOGlobalID.class,InfoPourEditionMedicale.class,NSArray.class};
				Object[] parametres = new Object[]{getEdc().globalIDForObject(infoMedicale), info, destinatairesGlobalIds};
				UtilitairesImpression.imprimerSansDialogue(getEdc(),methodeImpression,classeParametres,parametres, nomImpression, nomImpression);
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerDonnees implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}

		public void onSelectionChanged() {
			setCurrentDonnee((EODonneesMedicales)eod.selectedObject());
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupDonneeListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			if (myView.getPopupTypes().getSelectedIndex() > 0) {

				setModeCreation(true);
				setSaisieEnabled(true);
				myView.getLblTypeModalite().setForeground(Color.BLACK);

				switch (myView.getPopupTypes().getSelectedIndex()) {
				case 1 : gererVisite();break;
				case 2 : gererVaccin();break;
				case 3 : gererExamen();break;
				}

				myView.getPopupTypes().setEnabled(false);
			}
		}
	}

	private void traitementsPourCreationVisite() {

	}
	private void traitementsPourCreationVaccin() {

	}
	private void traitementsPourCreationExamen() {

	}

	private void gererVisite() {
		if (isModeCreation()) {
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), EODonneesMedicales.TYPE_VISITE);				
			myView.getPopupMedecins().setSelectedItem(EOMedecinTravail.getDefault(getEdc()));
			traitementsPourCreationVisite();
			ctrlVisites.ajouter(EOVisiteMedicale.creer(getEdc(), getCurrentIndividu()));			
		}
	}
	private void gererVaccin() {
		if (isModeCreation()) {
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), EODonneesMedicales.TYPE_VACCIN);				
			myView.getPopupMedecins().setSelectedItem(EOMedecinTravail.getDefault(getEdc()));
			traitementsPourCreationVaccin();
			ctrlVaccins.ajouter(EOVaccin.creer(getEdc(), getCurrentIndividu()));			
		}		
	}
	private void gererExamen() {
		if (isModeCreation()) {
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), EODonneesMedicales.TYPE_EXAMEN);				
			traitementsPourCreationExamen();
			myView.getPopupMedecins().setSelectedItem(EOMedecinTravail.getDefault(getEdc()));
			ctrlExamens.ajouter(EOExamenMedical.creer(getEdc(), getCurrentIndividu(), EOMedecinTravail.getDefault(getEdc())));
		}		
	}

	/**
	 * 
	 * @param myTextField
	 */
	private void heureHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = TimeCtrl.timeCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Heure non valide","L'heure saisie n'est pas valide (HH:MM) !");
		}
		else {
			myTextField.setText(myDate);
			updateInterface();
		}
	}

	private class ActionListenerHeureTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			heureHasChanged(myTextField);
		}
	}
	private class FocusListenerHeureTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			heureHasChanged(myTextField);			
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getPopupTypes().setSelectedIndex(0);
		myView.getPopupMedecins().setSelectedIndex(0);

		CocktailUtilities.viderTextField(myView.getTfDate());
		CocktailUtilities.viderTextField(myView.getTfHeure());
		ctrlVisites.clearDatas();
		ctrlVaccins.clearTextFields();
		ctrlExamens.clearTextFields();

	}
	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentDonnee() != null) {

			CocktailUtilities.setDateToField(myView.getTfDate(), getCurrentDonnee().medDate());
			CocktailUtilities.setTextToField(myView.getTfHeure(), getCurrentDonnee().medHeure());
			myView.getPopupMedecins().setSelectedItem(getCurrentDonnee().medecinTravail());

			// Swap view
			myView.getPopupTypes().removeActionListener(listenerDonneesMedicales);
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), getCurrentDonnee().medType());				

			myView.getBtnImprimerArrete().setEnabled(false);

			if (EODonneesMedicales.TYPE_VISITE.equals(getCurrentDonnee().medType())) {
				myView.getPopupTypes().setSelectedIndex(1);		
				ctrlVisites.actualiser(getCurrentDonnee());
			}
			if (EODonneesMedicales.TYPE_VACCIN.equals(getCurrentDonnee().medType())) {
				myView.getPopupTypes().setSelectedIndex(2);			
				ctrlVaccins.actualiser(getCurrentDonnee());
			}
			if (EODonneesMedicales.TYPE_EXAMEN.equals(getCurrentDonnee().medType())) {
				myView.getPopupTypes().setSelectedIndex(3);			
				ctrlExamens.actualiser(getCurrentDonnee());
			}

			myView.getPopupTypes().addActionListener(listenerDonneesMedicales);
		}

		updateInterface();
	}
	
	protected boolean isSaisieEnabled() {
		return super.isSaisieEnabled();
	}
	
	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null && !isSaisieEnabled());
		myView.getBtnModifier().setEnabled(getCurrentDonnee() != null && !isSaisieEnabled());
		myView.getBtnSupprimer().setEnabled(getCurrentDonnee() != null && !isSaisieEnabled());

		myView.getBtnImprimerArrete().setEnabled(!isSaisieEnabled() && getCurrentDonnee() != null);

		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		myView.getPopupTypes().setEnabled(isSaisieEnabled() && isModeCreation());	
		myView.getPopupMedecins().setEnabled(isSaisieEnabled());		
		CocktailUtilities.initTextField(myView.getTfDate(), false, isSaisieEnabled());		
		CocktailUtilities.initTextField(myView.getTfHeure(), false, isSaisieEnabled());		

		ctrlVisites.updateInterface();

	}
	@Override
	protected void refresh() {
//		// TODO Auto-generated method stub
        EODonneesMedicales  donnees = getCurrentDonnee();
		getEdc().invalidateObjectsWithGlobalIDs(new NSArray(getEdc().globalIDForObject(getCurrentDonnee())));
		actualiser();
        if (donnees != null) {
            myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(donnees));
        }
	}
	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		
		switch (myView.getPopupTypes().getSelectedIndex()) {
		case 1 : ctrlVisites.valider();break;
		case 2 : ctrlVaccins.valider();break;
		case 3 : ctrlExamens.valider();break;
		}

	}
	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(false);
	}
	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(false);
		myListener.onSelectionChanged();
	}
	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}
	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		clearDatas();
		myView.getLblTypeModalite().setForeground(Color.RED);
		ctrlParent.setIsLocked(true);
		((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), "VIDE");
	}
	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(true);

	}
	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		try {
			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlVisites.supprimer();break;
			case 2 : ctrlVaccins.supprimer();break;
			case 3 : ctrlExamens.supprimer();break;
			}
		}
		catch (Exception e) {

		}

	}
	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub

	}	

}

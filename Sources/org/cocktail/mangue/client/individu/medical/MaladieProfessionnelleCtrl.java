/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.medical;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.conges.CongeAccidentServiceCtrl;
import org.cocktail.mangue.client.conges.CongeAccidentTravailCtrl;
import org.cocktail.mangue.client.gui.individu.MaladieProfessionnelleView;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOLieuAccident;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeAccidentTrav;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.TimeCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.acc_travail.EOCertificatMedical;
import org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident;
import org.cocktail.mangue.modele.mangue.acc_travail.EORepartLesions;
import org.cocktail.mangue.modele.mangue.conges.CongeAccidentTravail;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class MaladieProfessionnelleCtrl {

	private static MaladieProfessionnelleCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;

	private MaladieProfessionnelleView 			myView;

	private EODisplayGroup 			eod, eodCertificat, eodLesion;
	private boolean					saisieEnabled, modeCreation;

	private ListenerAccident 		listenerAccident = new ListenerAccident();
	private InfosComplementairesCtrl ctrlParent;

	private EOTypeAccidentTrav		currentTypeAccident;
	private EODeclarationAccident	currentAccident;
	private EOIndividu 				currentIndividu;
	private EOLieuAccident 			currentLieu;
	private EORepartLesions			currentLesion;
	private EOCertificatMedical		currentCertificat;

	public MaladieProfessionnelleCtrl(InfosComplementairesCtrl ctrl, EOEditingContext editingContext) {

		ec = editingContext;
		ctrlParent = ctrl;
		eod = new EODisplayGroup();
		eodCertificat = new EODisplayGroup();
		eodLesion = new EODisplayGroup();
		myView = new MaladieProfessionnelleView(null, eod, eodCertificat, eodLesion, MODE_MODAL.booleanValue());

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		myView.getBtnConges().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {gererConge();}}
		);

		myView.setTypes(EOTypeAccidentTrav.fetchAll(ec));

		setSaisieEnabled(false);

		myView.getTfDate().addFocusListener(new FocusListenerDateTextField(myView.getTfDate()));
		myView.getTfDate().addActionListener(new ActionListenerDateTextField(myView.getTfDate()));

		myView.getMyEOTable().addListener(listenerAccident);
	}

	public EODeclarationAccident currentAccident() {
		return currentAccident;
	}

	public void setCurrentAccident(EODeclarationAccident currentAccident) {
		this.currentAccident = currentAccident;
		updateData();
	}

	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EOTypeAccidentTrav currentTypeAccident() {
		return currentTypeAccident;
	}
	public void setCurrentTypeAccident(EOTypeAccidentTrav currentTypeAccident) {
		this.currentTypeAccident = currentTypeAccident;
		myView.getPopupType().setSelectedItem(currentTypeAccident);
	}

	public EORepartLesions currentLesion() {
		return currentLesion;
	}
	public void setCurrentLesion(EORepartLesions currentLesion) {
		this.currentLesion = currentLesion;
	}	
	public EOCertificatMedical currentCertificat() {
		return currentCertificat;
	}

	public void setCurrentCertificat(EOCertificatMedical currentCertificat) {
		this.currentCertificat = currentCertificat;
	}

	public void actualiser() {
		eod.setObjectArray(EODeclarationAccident.findForIndividu(ec, currentIndividu()));
		myView.getMyEOTable().updateData();
		updateUI();
	}


	private EOTypeAccidentTrav getTypeAccident() {
		if (myView.getPopupType().getSelectedIndex() > 0)
			return (EOTypeAccidentTrav)myView.getPopupType().getSelectedItem();
		return null;
	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewMaladie() {
		return myView.getViewAccident();
	}
	private void ajouter() {
		setModeCreation(true);
		setCurrentAccident(EODeclarationAccident.creer(ec, currentIndividu()));
		setSaisieEnabled(true);			
	}

	private void modifier() {
		setModeCreation(false);
		setSaisieEnabled(true);
	}
	private void supprimer() {
		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette déclaration ?", "Oui", "Non"))		 {
			return;
		}

		try {
			ec.deleteObject(currentAccident());
			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void gererConge() {

		CongeAccidentTravail conge  = null;

		if (currentIndividu().estTitulaireSurPeriodeComplete(currentAccident().daccDate(), currentAccident().daccDate())) {
			CongeAccidentServiceCtrl.sharedInstance(ec).open(currentAccident());
		}
		else {
			CongeAccidentTravailCtrl.sharedInstance(ec).open(currentAccident());
		}

		//		String nomEntite = "";
		//		NSTimestamp ancienneDateAccident = currentAccident().daccDate();
		////		
		////			nomEntite = "CongeAccidentServ";
		////		} else if (currentIndividu().estContractuelSurPeriodeComplete(currentAccident().daccDate(), currentAccident().daccDate())) {
		////			nomEntite = "CgntAccidentTrav";
		////		} else {
		////			EODialogs.runErrorDialog("ERREUR", "L'agent n'a ni carrière, ni contrat à la date de l'accident");
		////		}
		//		if (nomEntite.length() > 0) {
		//			// Vérifier si il existe déjà un AT à cette date pour rentrer en mode modification
		////			CongeAccidentTravail conge = CongeAccidentTravail.congePourIndividuEtDateAccident(ec, nomEntite, currentIndividu(), currentAccident().daccDate());
		//			// Si on ne le trouve pas et si la date a été modifiée, vérifier si il n'existe pas un congé à l'ancienne date
		////			if (conge == null && ancienneDateAccident != null) {
		////				conge = CongeAccidentTravail.congePourIndividuEtDateAccident(ec, nomEntite, currentIndividu(), ancienneDateAccident);
		////			}
		//			GestionCongeAccidentTravail controleur = new GestionCongeAccidentTravail(nomEntite,ec.globalIDForObject(currentAccident()));
		//			if (conge != null) {			
		//				LogManager.logDetail("Modification d'un conge de type " + nomEntite + " pour " + currentIndividu().identite());
		//				controleur.modifier(ec.globalIDForObject(conge.absence()));
		//			} else {
		//				LogManager.logDetail("Ajout d'un conge de type " + nomEntite + " pour " + currentIndividu().identite());
		//				controleur.ajouter(ec.globalIDForObject(currentIndividu()));
		//			}
		//		}

	}

	private void ajouterLesion() {
		SaisieLesionsCtrl.sharedInstance(ec).ajouter(currentAccident());
		listenerAccident.onSelectionChanged();
	}
	private void ajouterCertificat() {
		SaisieCertificatMedicalCtrl.sharedInstance(ec).ajouter(currentAccident());
		listenerAccident.onSelectionChanged();
	}
	private void modifierCertificat() {
		SaisieCertificatMedicalCtrl.sharedInstance(ec).modifier(currentCertificat());
		listenerAccident.onSelectionChanged();
	}


	private void supprimerLesion() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette lésion ?", "Oui", "Non"))		
			return;

		try {
			ec.deleteObject(currentLesion());
			ec.saveChanges();
			listenerAccident.onSelectionChanged();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}
	private void supprimerCertificat() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer ce certificat ?", "Oui", "Non"))		
			return;

		try {
			ec.deleteObject(currentCertificat());
			ec.saveChanges();
			listenerAccident.onSelectionChanged();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}

	protected boolean traitementsAvantValidation() {


		return true;
	}


	private void refreshWithSelection(ZEOTable myTable, EOEnterpriseObject myObject) {		
		actualiser();
		myTable.forceNewSelectionOfObjects(new NSArray(myObject));
	}

	private void valider() {

		try {

			currentAccident().setTypeAccidentRelationship(getTypeAccident());			
			currentAccident().setDaccDate(CocktailUtilities.getDateFromField(myView.getTfDate()));

			if (!traitementsAvantValidation())
				return;

			ec.saveChanges();

			refreshWithSelection(myView.getMyEOTable(), currentAccident());

			setSaisieEnabled(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {

		try {
			ec.revert();
			ServerProxy.clientSideRequestRevert(ec);
		}
		catch (Exception e){
			e.printStackTrace();
		}

		listenerAccident.onSelectionChanged();
		setSaisieEnabled(false);

		updateUI();

	}

	private void clearTextFields() {
		setCurrentTypeAccident(null);
		myView.getTfDate().setText("");
	}

	private void setModeCreation(boolean yn) {
		modeCreation = yn;
	}
	private boolean modeCreation() {
		return modeCreation;
	}
	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}


	private void updateUI() {

		myView.getMyEOTable().setEnabled(!saisieEnabled());

		myView.getBtnAjouter().setEnabled(!saisieEnabled() && currentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!saisieEnabled() && currentAccident() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && currentAccident() != null);

		myView.getBtnConges().setEnabled(!saisieEnabled() && currentAccident() != null);
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getPopupType().setEnabled(saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDate(), false, saisieEnabled());

	}


	private void updateData() {

		clearTextFields();
		if (currentAccident() != null) {

			myView.setTypes(EOTypeAccidentTrav.fetchForTypeAgent(ec, currentAccident().estPourTitulaire()));
			setCurrentTypeAccident(currentAccident().typeAccident());

			CocktailUtilities.setDateToField(myView.getTfDate(), currentAccident.daccDate());
		}

		updateUI();
	}

	private class ListenerAccident implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentAccident((EODeclarationAccident)eod.selectedObject());
		}
	}
	private class ListenerLesion implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentLesion((EORepartLesions)eodLesion.selectedObject());
		}
	}
	private class ListenerCertificat implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifierCertificat();
		}
		public void onSelectionChanged() {
			setCurrentCertificat((EOCertificatMedical)eodCertificat.selectedObject());
		}
	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	




	private void heureHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = TimeCtrl.timeCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Heure non valide","L'heure saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerHeureTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			heureHasChanged(myTextField);
		}
	}
	private class FocusListenerHeureTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			heureHasChanged(myTextField);			
		}
	}	

}

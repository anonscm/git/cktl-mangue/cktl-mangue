/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.medical;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.conges.CongeAccidentServiceCtrl;
import org.cocktail.mangue.client.conges.CongeAccidentTravailCtrl;
import org.cocktail.mangue.client.gui.individu.DeclarationAccidentView;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOLieuAccident;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeAccidentTrav;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.TimeCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.acc_travail.EOCertificatMedical;
import org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident;
import org.cocktail.mangue.modele.mangue.acc_travail.EORepartLesions;
import org.cocktail.mangue.modele.mangue.conges.EOCgntAccidentTrav;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAccidentServ;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestampFormatter;

public class DeclarationAccidentCtrl extends ModelePageGestion {

	private static Boolean MODE_MODAL = Boolean.FALSE;

	private DeclarationAccidentView 	myView;

	private EODisplayGroup 			eod, eodCertificat, eodLesion;
	private boolean					peutGererModule;

	private ListenerAccident 			listenerAccident = new ListenerAccident();
	private InfosComplementairesCtrl 	ctrlParent;

	private EODeclarationAccident	currentAccident;
	private EOIndividu 				currentIndividu;
	private EOLieuAccident 			currentLieu;
	private EORepartLesions			currentLesion;
	private EOCertificatMedical		currentCertificat;

	public DeclarationAccidentCtrl(InfosComplementairesCtrl ctrlParent, EOEditingContext edc) {

		super(edc);
		this.ctrlParent = ctrlParent;

		eod = new EODisplayGroup();
		eodCertificat = new EODisplayGroup();
		eodLesion = new EODisplayGroup();
		myView = new DeclarationAccidentView(null, eod, eodCertificat, eodLesion, MODE_MODAL.booleanValue());

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnAjouterLesion().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterLesion();}}
		);
		myView.getBtnModifierLesion().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierLesion();}}
		);
		myView.getBtnSupprimerLesion().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerLesion();}}
		);
		
		myView.getBtnAjouterCertificat().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterCertificat();}}
		);
		myView.getBtnModifierCertificat().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierCertificat();}}
		);
		myView.getBtnSupprimerCertificat().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerCertificat();}}
		);

		myView.getBtnConges().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {gererConges();}}
		);

		CocktailUtilities.initPopupAvecListe(myView.getPopupType(), EOTypeAccidentTrav.fetchAll(getEdc()), true);
		CocktailUtilities.initPopupAvecListe(myView.getPopupLieux(), EOLieuAccident.fetchAll(getEdc()), true);

		setSaisieEnabled(false);

		setDateListeners(myView.getTfDate());

		myView.getTfHeure().addFocusListener(new FocusListenerHeureTextField(myView.getTfHeure()));
		myView.getTfHeure().addActionListener(new ActionListenerHeureTextField(myView.getTfHeure()));

		myView.getTfHeureAm1().addFocusListener(new FocusListenerHeureTextField(myView.getTfHeureAm1()));
		myView.getTfHeureAm1().addActionListener(new ActionListenerHeureTextField(myView.getTfHeureAm1()));

		myView.getTfHeureAm2().addFocusListener(new FocusListenerHeureTextField(myView.getTfHeureAm2()));
		myView.getTfHeureAm2().addActionListener(new ActionListenerHeureTextField(myView.getTfHeureAm2()));

		myView.getTfHeurePm1().addFocusListener(new FocusListenerHeureTextField(myView.getTfHeurePm1()));
		myView.getTfHeurePm1().addActionListener(new ActionListenerHeureTextField(myView.getTfHeurePm1()));

		myView.getTfHeurePm2().addFocusListener(new FocusListenerHeureTextField(myView.getTfHeurePm2()));
		myView.getTfHeurePm2().addActionListener(new ActionListenerHeureTextField(myView.getTfHeurePm2()));

		CocktailUtilities.initTextField(myView.getTfJour(), false, false);
		
		myView.getLblConge().setText("");
		myView.getMyEOTable().addListener(listenerAccident);
		myView.getMyEOTableLesion().addListener(new ListenerLesion());
		myView.getMyEOTableCertificat().addListener(new ListenerCertificat());
	}
	
	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		setPeutGererModule(utilisateur.peutAfficherInfosPerso()  && utilisateur.peutGererAgents() && utilisateur.peutGererAccidentTravail() );

		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());
		
		myView.getBtnAjouterLesion().setVisible(peutGererModule());
		myView.getBtnModifierLesion().setVisible(peutGererModule());
		myView.getBtnSupprimerLesion().setVisible(peutGererModule());

		myView.getBtnAjouterCertificat().setVisible(peutGererModule());
		myView.getBtnModifierCertificat().setVisible(peutGererModule());
		myView.getBtnSupprimerCertificat().setVisible(peutGererModule());

		myView.getBtnConges().setVisible(peutGererModule());

	}


	public EODeclarationAccident getCurrentAccident() {
		return currentAccident;
	}

	public void setCurrentAccident(EODeclarationAccident currentAccident) {
		this.currentAccident = currentAccident;
		updateDatas();
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EOTypeAccidentTrav currentTypeAccident() {
		return (myView.getPopupType().getSelectedIndex() > 0)?(EOTypeAccidentTrav)myView.getPopupType().getSelectedItem():null;
	}
	public void setCurrentTypeAccident(EOTypeAccidentTrav currentTypeAccident) {
		myView.getPopupType().setSelectedItem(currentTypeAccident);
	}

	public EORepartLesions getCurrentLesion() {
		return currentLesion;
	}
	public void setCurrentLesion(EORepartLesions currentLesion) {
		this.currentLesion = currentLesion;
		updateInterface();
	}

	public EOLieuAccident getCurrentLieu() {
		return currentLieu;
	}
	public void setCurrentLieu(EOLieuAccident currentLieu) {
		this.currentLieu = currentLieu;
		myView.getPopupLieux().setSelectedItem(currentLieu);
	}

	public EOCertificatMedical getCurrentCertificat() {
		return currentCertificat;
	}

	public void setCurrentCertificat(EOCertificatMedical currentCertificat) {
		this.currentCertificat = currentCertificat;
		updateInterface();
	}

	public void actualiser() {
		eod.setObjectArray(EODeclarationAccident.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}

	private EOTypeAccidentTrav getTypeAccident() {
		if (myView.getPopupType().getSelectedIndex() > 0)
			return (EOTypeAccidentTrav)myView.getPopupType().getSelectedItem();
		return null;
	}
	private EOLieuAccident getLieuAccident() {
		if (myView.getPopupLieux().getSelectedIndex() > 0)
			return (EOLieuAccident)myView.getPopupLieux().getSelectedItem();
		return null;
	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewAccident() {
		return myView.getViewAccident();
	}
	
	public void gererConges() {
		if (getCurrentIndividu().estTitulaireSurPeriodeComplete(getCurrentAccident().daccDate(), getCurrentAccident().daccDate())) {
			CongeAccidentServiceCtrl.sharedInstance(getEdc()).open(getCurrentAccident());
		}
		else {
			CongeAccidentTravailCtrl.sharedInstance(getEdc()).open(getCurrentAccident());
		}	
		listenerAccident.onSelectionChanged();
	}

	private void ajouterLesion() {
		SaisieLesionsCtrl.sharedInstance(getEdc()).ajouter(getCurrentAccident());
		listenerAccident.onSelectionChanged();
	}
	private void ajouterCertificat() {
		SaisieCertificatMedicalCtrl.sharedInstance(getEdc()).ajouter(getCurrentAccident());
		listenerAccident.onSelectionChanged();
	}
	private void modifierLesion() {
		SaisieLesionsCtrl.sharedInstance(getEdc()).modifier(getCurrentLesion());
		listenerAccident.onSelectionChanged();		
	}
	private void modifierCertificat() {
		SaisieCertificatMedicalCtrl.sharedInstance(getEdc()).modifier(getCurrentCertificat());
		listenerAccident.onSelectionChanged();
	}

	
	private void supprimerLesion() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette lésion ?", "Oui", "Non"))		
			return;

		try {
			getEdc().deleteObject(getCurrentLesion());
			getEdc().saveChanges();
			listenerAccident.onSelectionChanged();
		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}
	private void supprimerCertificat() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer ce certificat ?", "Oui", "Non"))		
			return;

		try {
			getEdc().deleteObject(getCurrentCertificat());
			getEdc().saveChanges();
			listenerAccident.onSelectionChanged();
		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}

	protected void traitementsAvantValidation() {

		getCurrentAccident().setTypeAccidentRelationship(getTypeAccident());
		getCurrentAccident().setLieuAccidentRelationship(getLieuAccident());
		
		getCurrentAccident().setDaccDate(CocktailUtilities.getDateFromField(myView.getTfDate()));

		getCurrentAccident().setDaccHeure(CocktailUtilities.getTextFromField(myView.getTfHeure()));
		getCurrentAccident().setDaccHdebAm(CocktailUtilities.getTextFromField(myView.getTfHeureAm1()));
		getCurrentAccident().setDaccHfinAm(CocktailUtilities.getTextFromField(myView.getTfHeureAm2()));

		getCurrentAccident().setDaccHdebPm(CocktailUtilities.getTextFromField(myView.getTfHeurePm1()));
		getCurrentAccident().setDaccHfinPm(CocktailUtilities.getTextFromField(myView.getTfHeurePm2()));

		getCurrentAccident().setDaccLocalite(CocktailUtilities.getTextFromField(myView.getTfLocalite()));
		getCurrentAccident().setDaccJour(CocktailUtilities.getTextFromField(myView.getTfJour()));
		
		getCurrentAccident().setDaccCirconstance(CocktailUtilities.getTextFromArea(myView.getTaCirconstances()));
		getCurrentAccident().setDaccTransport(CocktailUtilities.getTextFromArea(myView.getTaLieuTransport()));

		switch (myView.getPopupSuite().getSelectedIndex()) {
		case 0 : getCurrentAccident().setDaccSuite(null);break;
		case 1 : getCurrentAccident().setSuiteSansArret();break;
		case 2 : getCurrentAccident().setSuiteArret();break;
		case 3 : getCurrentAccident().setSuiteDeces();break;
		}

	}
	
	private class ListenerAccident implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentAccident() != null && peutGererModule())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentAccident((EODeclarationAccident)eod.selectedObject());
		}
	}
	private class ListenerLesion implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentLesion((EORepartLesions)eodLesion.selectedObject());
		}
	}
	private class ListenerCertificat implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentCertificat() != null && peutGererModule())
				modifierCertificat();
		}
		public void onSelectionChanged() {
			setCurrentCertificat((EOCertificatMedical)eodCertificat.selectedObject());
		}
	}

	private void modifierJour() {
		NSTimestampFormatter dateFormat = new NSTimestampFormatter("%A");
		myView.getTfJour().setText(dateFormat.format(CocktailUtilities.getDateFromField(myView.getTfDate())));
	}
	
	private void heureHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myTime = TimeCtrl.timeCompletion(myTextField.getText());
		if ("".equals(myTime))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Heure non valide","L'heure saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myTime);
			updateInterface();
		}
	}

	private class ActionListenerHeureTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			heureHasChanged(myTextField);
		}
	}
	private class FocusListenerHeureTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			heureHasChanged(myTextField);			
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		setCurrentTypeAccident(null);
		setCurrentLieu(null);

		myView.getPopupSuite().setSelectedIndex(0);
		
		myView.getTfDate().setText("");
		myView.getTfHeure().setText("");
		myView.getTfJour().setText("");
		myView.getTfLocalite().setText("");
		myView.getTfHeureAm1().setText("");
		myView.getTfHeureAm2().setText("");
		myView.getTfHeurePm1().setText("");
		myView.getTfHeurePm2().setText("");
		myView.getLblConge().setText("");
		myView.getTaCirconstances().setText("");
		myView.getTaLieuTransport().setText("");
		myView.getLblConge().setText("");
		
		eodLesion.setObjectArray(new NSArray());
		myView.getMyEOTableLesion().updateData();
		eodCertificat.setObjectArray(new NSArray());
		myView.getMyEOTableCertificat().updateData();

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentAccident() != null) {

			CocktailUtilities.initPopupAvecListe(myView.getPopupType(), EOTypeAccidentTrav.fetchForTypeAgent(getEdc(), getCurrentAccident().estPourTitulaire()), true);
			
			setCurrentTypeAccident(getCurrentAccident().typeAccident());
			setCurrentLieu(getCurrentAccident().lieuAccident());
			
			CocktailUtilities.setTextToField(myView.getTfHeure(), getCurrentAccident().daccHeure());
			CocktailUtilities.setTextToField(myView.getTfJour(), getCurrentAccident().daccJour());
			CocktailUtilities.setTextToField(myView.getTfHeureAm1(), getCurrentAccident().daccHdebAm());
			CocktailUtilities.setTextToField(myView.getTfHeureAm2(), getCurrentAccident().daccHfinAm());
			CocktailUtilities.setTextToField(myView.getTfHeurePm1(), getCurrentAccident().daccHdebPm());
			CocktailUtilities.setTextToField(myView.getTfHeurePm2(), getCurrentAccident().daccHfinPm());
			CocktailUtilities.setTextToField(myView.getTfLocalite(), getCurrentAccident().daccLocalite());

			CocktailUtilities.setTextToArea(myView.getTaCirconstances(), getCurrentAccident().daccCirconstance());
			CocktailUtilities.setTextToArea(myView.getTaLieuTransport(), getCurrentAccident().daccTransport());

			CocktailUtilities.setDateToField(myView.getTfDate(), currentAccident.daccDate());

			myView.getPopupSuite().setSelectedIndex(0);
			if (getCurrentAccident().suiteAvecArret())
				myView.getPopupSuite().setSelectedIndex(2);
			else
				if (getCurrentAccident().suiteSansArret())
					myView.getPopupSuite().setSelectedIndex(1);
				else
					if (getCurrentAccident().suiteAvecDeces())
						myView.getPopupSuite().setSelectedIndex(3);

			eodLesion.setObjectArray(EORepartLesions.findForDeclaration(getEdc(), getCurrentAccident(), false));
			myView.getMyEOTableLesion().updateData();

			eodCertificat.setObjectArray(EOCertificatMedical.findForDeclaration(getEdc(), getCurrentAccident()));
			myView.getMyEOTableCertificat().updateData();
			
			NSArray conges = new NSArray();
			if (getCurrentAccident().estPourTitulaire())
				conges = EOCongeAccidentServ.rechercherPourDeclaration(getEdc(), getCurrentAccident());
			else
				conges = EOCgntAccidentTrav.rechercherPourDeclaration(getEdc(), getCurrentAccident());
			
			myView.getBtnConges().setText("Congés ("+conges.count()+")");
			
		}

		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentAccident() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentAccident() != null);

		myView.getBtnConges().setEnabled(!isSaisieEnabled() && getCurrentAccident() != null && getCurrentAccident().suiteAvecArret());

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getBtnAjouterLesion().setEnabled(!isSaisieEnabled() && getCurrentAccident() != null);		
		myView.getBtnModifierLesion().setEnabled(!isSaisieEnabled() && getCurrentAccident() != null  && getCurrentLesion() != null);		
		myView.getBtnSupprimerLesion().setEnabled(!isSaisieEnabled() && getCurrentAccident() != null && getCurrentLesion() != null);		
		myView.getBtnAjouterCertificat().setEnabled(!isSaisieEnabled() && getCurrentAccident() != null);		
		myView.getBtnModifierCertificat().setEnabled(!isSaisieEnabled() && getCurrentAccident() != null && getCurrentCertificat() != null);		
		myView.getBtnSupprimerCertificat().setEnabled(!isSaisieEnabled() && getCurrentAccident() != null && getCurrentCertificat() != null);		

		CocktailUtilities.initTextArea(myView.getTaCirconstances(), false, isSaisieEnabled());
		CocktailUtilities.initTextArea(myView.getTaLieuTransport(), false, isSaisieEnabled());
			
		CocktailUtilities.initTextField(myView.getTfDate(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfHeure(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfLocalite(), false, isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfHeureAm1(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfHeureAm2(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfHeurePm1(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfHeurePm2(), false, isSaisieEnabled());

		myView.getPopupType().setEnabled(isSaisieEnabled());
		myView.getPopupSuite().setEnabled(isSaisieEnabled());
		myView.getPopupLieux().setEnabled(isSaisieEnabled());
	
	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		listenerAccident.onSelectionChanged();
		ctrlParent.setIsLocked(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		modifierJour();

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentAccident(EODeclarationAccident.creer(getEdc(), getCurrentIndividu()));
		ctrlParent.setIsLocked(true);
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(true);
	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		NSArray<EOCongeAccidentServ> congesServ = EOCongeAccidentServ.rechercherTousCongesPourDeclaration(getEdc(), getCurrentAccident());
		NSArray<EOCgntAccidentTrav> congesTrav = EOCgntAccidentTrav.rechercherPourDeclaration(getEdc(), getCurrentAccident());
		for (EOCongeAccidentServ conge : congesServ) {
			conge.absence().setEstValide(false);
			getEdc().deleteObject(conge);
		}
		for (EOCgntAccidentTrav conge : congesTrav) {
			conge.absence().setEstValide(false);
			getEdc().deleteObject(conge);
		}
		for (EOCertificatMedical certificat : (NSArray<EOCertificatMedical>)eodCertificat.displayedObjects()) {
			getEdc().deleteObject(certificat);
		}
		for (EORepartLesions lesion : (NSArray<EORepartLesions>)eodLesion.displayedObjects()) { 
			getEdc().deleteObject(lesion);
		}

		getEdc().deleteObject(getCurrentAccident());

	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}	

}

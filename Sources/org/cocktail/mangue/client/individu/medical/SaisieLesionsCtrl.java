// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.individu.medical;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.individu.SaisieLesionView;
import org.cocktail.mangue.client.select.NatureLesionSelectCtrl;
import org.cocktail.mangue.client.select.SiegeLesionSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EONatureLesions;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOSiegeLesions;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident;
import org.cocktail.mangue.modele.mangue.acc_travail.EORepartLesions;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisieLesionsCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieLesionsCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieLesionView myView;

	private EORepartLesions currentLesion;
	private EONatureLesions currentNature;
	private EOSiegeLesions currentSiege;

	public SaisieLesionsCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieLesionView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
				);
		myView.getBtnAjouterNature().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	ajouterNature();}	}
				);
		myView.getBtnAjouterSiege().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	ajouterSiege();}	}
				);
		myView.getBtnSupprimerSiege().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	supprimerSiege();}	}
				);

		CocktailUtilities.initTextField(myView.getTfNature(), false, false);
		CocktailUtilities.initTextField(myView.getTfSiege(), false, false);

	}

	public static SaisieLesionsCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieLesionsCtrl(editingContext);
		return sharedInstance;
	}


	public EORepartLesions currentLesion() {
		return currentLesion;
	}

	public void setCurrentLesion(EORepartLesions currentLesion) {
		this.currentLesion = currentLesion;
		setCurrentNature(currentLesion.natureLesion());
		setCurrentSiege(currentLesion().siegeLesion());
	}

	public EONatureLesions currentNature() {
		return currentNature;
	}

	public void setCurrentNature(EONatureLesions currentNature) {
		this.currentNature = currentNature;
		myView.getTfNature().setText("");
		if (currentNature != null)
			CocktailUtilities.setTextToField(myView.getTfNature(), currentNature.libelleLong());
	}

	public EOSiegeLesions currentSiege() {
		return currentSiege;
	}

	public void setCurrentSiege(EOSiegeLesions currentSiege) {
		this.currentSiege = currentSiege;
		myView.getTfSiege().setText("");
		if (currentSiege != null)
			CocktailUtilities.setTextToField(myView.getTfSiege(), currentSiege.libelleLong());
	}

	public EORepartLesions ajouter(EODeclarationAccident declaration)	{
		setCurrentLesion(EORepartLesions.creer(ec, declaration));
		myView.setVisible(true);
		return currentLesion();
	}
	public EORepartLesions modifier(EORepartLesions lesion)	{
		setCurrentLesion(lesion);
		myView.setVisible(true);
		return currentLesion();
	}

	private void ajouterNature() {
		EONatureLesions nature = NatureLesionSelectCtrl.sharedInstance(ec).getNature();
		if (nature != null)
			setCurrentNature(nature);
	}
	private void ajouterSiege() {
		EOSiegeLesions siege = SiegeLesionSelectCtrl.sharedInstance(ec).getSiege();
		if (siege != null)
			setCurrentSiege(siege);
	}
	private void supprimerSiege() {
		setCurrentSiege(null);
	}

	private void valider()  {

		try {			

			currentLesion().setNatureLesionRelationship(currentNature());
			currentLesion().setSiegeLesionRelationship(currentSiege());

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		ec.revert();
		setCurrentSiege(null);
		myView.setVisible(false);
	}

}
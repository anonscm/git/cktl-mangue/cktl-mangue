package org.cocktail.mangue.client.individu.medical;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.VisitesView;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOResultatVisiteMedicale;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeVisiteMedicale;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.mangue.individu.medical.EODonneesMedicales;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVisiteMedicale;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class VisitesCtrl {

	private VisitesView myView;

	private DonneesMedicalesCtrl  ctrlParent;
	private EOVisiteMedicale currentObject;

	/*
	 * 
	 */
	public VisitesCtrl(DonneesMedicalesCtrl ctrlParent)	{

		this.ctrlParent = ctrlParent;
		myView = new VisitesView();

		myView.getTfDateConvocation().addFocusListener(new FocusListenerDateTextField(myView.getTfDateConvocation()));
		myView.getTfDateConvocation().addActionListener(new ActionListenerDateTextField(myView.getTfDateConvocation()));
		myView.getTfSurveillance().addFocusListener(new FocusListenerDateTextField(myView.getTfSurveillance()));
		myView.getTfSurveillance().addActionListener(new ActionListenerDateTextField(myView.getTfSurveillance()));

		CocktailUtilities.initPopupAvecListe(myView.getPopupResultatVisite(), NomenclatureFinder.findStatic(getEdc(), EOResultatVisiteMedicale.ENTITY_NAME), true);
		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeVisite(), NomenclatureFinder.findStatic(getEdc(), EOTypeVisiteMedicale.ENTITY_NAME), true);

		myView.getCheckAcmo().setEnabled(false);
		
		myView.getPopupTypeVisite().addActionListener(new PopupTypeVisiteListener());
		CocktailUtilities.initTextField(myView.getTfSurveillance(), false, false);
		
		updateInterface();
	}

	public EOEditingContext getEdc() {
		return ctrlParent.getEdc();
	}
	protected boolean isSaisieEnabled() {
		return ctrlParent.isSaisieEnabled();
	}
	public JPanel getView() {
		return myView;
	}
	public EOVisiteMedicale getCurrentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOVisiteMedicale currentObject) {
		this.currentObject = currentObject;
	}
	private class PopupTypeVisiteListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			if (myView.getPopupTypeVisite().getSelectedIndex() > 0 && isSaisieEnabled()) {
				calculerDateSurveillance();
			}
		}
	}

	private void calculerDateSurveillance() {
		NSTimestamp dateVisite = ctrlParent.getDate();
		if ( getTypeVisite() == null || dateVisite == null ) {
			myView.getTfSurveillance().setText("");
			return;
		}
		CocktailUtilities.setDateToField(myView.getTfSurveillance(),  getTypeVisite().evaluerRythmeSurveillance(dateVisite));
	}

	public void clearDatas() {
		
		CocktailUtilities.viderTextField(myView.getTfLieu());
		CocktailUtilities.viderTextArea(myView.getTfCommentaires());
		CocktailUtilities.viderTextField(myView.getTfDateConvocation());
		CocktailUtilities.viderTextField(myView.getTfSurveillance());
		
		myView.getCheckAcmo().setSelected(false);
		myView.getPopupTypeVisite().setSelectedIndex(0);
		myView.getPopupResultatVisite().setSelectedIndex(0);

	}

	public void ajouter(EOVisiteMedicale newObject) {		
		setCurrentObject(newObject);
	}

	public EOTypeVisiteMedicale getTypeVisite() {
		return (myView.getPopupTypeVisite().getSelectedIndex() > 0)?(EOTypeVisiteMedicale) myView.getPopupTypeVisite().getSelectedItem():null;
	}
	public EOResultatVisiteMedicale getResultatVisite() {
		return (myView.getPopupResultatVisite().getSelectedIndex() > 0)?(EOResultatVisiteMedicale) myView.getPopupResultatVisite().getSelectedItem():null;
	}

	/**
	 * 
	 */
	public void valider() {

		getCurrentObject().setTypeVisiteRelationship(getTypeVisite());
		getCurrentObject().setResultatVisiteRelationship(getResultatVisite());

		getCurrentObject().setMedecinRelationship(ctrlParent.getCurrentMedecin());
		getCurrentObject().setDate(ctrlParent.getDate());
		getCurrentObject().setHeure(ctrlParent.getHeure());
		getCurrentObject().setDateConvocation(CocktailUtilities.getDateFromField(myView.getTfDateConvocation()));
		getCurrentObject().setObservation(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));

		getCurrentObject().setLieu(CocktailUtilities.getTextFromField(myView.getTfLieu()));
		getCurrentObject().setRythmeSurveillance(CocktailUtilities.getDateFromField(myView.getTfSurveillance()));
		if (getCurrentObject().medecin() != null && getCurrentObject().medecin().adressesProfessionnelles().count() > 0)
			getCurrentObject().setAdresseRelationship((EOAdresse)ctrlParent.getCurrentMedecin().adressesProfessionnelles().objectAtIndex(0));
				
	}

	public void supprimer() throws Exception {
		try {			
			getEdc().deleteObject(getCurrentObject());
		}
		catch (Exception ex) {
			throw ex;
		}
	}


	protected void updateInterface() {

		myView.getPopupTypeVisite().setEnabled(isSaisieEnabled());
		myView.getPopupResultatVisite().setEnabled(isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfLieu(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateConvocation(), false, isSaisieEnabled());
		CocktailUtilities.initTextArea(myView.getTfCommentaires(), false, isSaisieEnabled());

	}

	/**
	 * 
	 * @param donnees
	 */
	public void actualiser(EODonneesMedicales donnees) {

		clearDatas();

		if (donnees != null) {

			setCurrentObject(EOVisiteMedicale.findForKey(getEdc(), donnees.medId()));

			if (getCurrentObject() != null) {	

				myView.getPopupTypeVisite().setSelectedItem(getCurrentObject().typeVisite());
				myView.getPopupResultatVisite().setSelectedItem(getCurrentObject().resultatVisite());
				CocktailUtilities.setDateToField(myView.getTfDateConvocation(), getCurrentObject().dateConvocation());
				CocktailUtilities.setDateToField(myView.getTfSurveillance(), getCurrentObject().rythmeSurveillance());
				CocktailUtilities.setTextToField(myView.getTfLieu(), getCurrentObject().lieu());
				CocktailUtilities.setTextToArea(myView.getTfCommentaires(), getCurrentObject().observation());

				myView.getCheckAcmo().setSelected(currentObject.estResponsableAcmo());
				
			}			
		}
	}


	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateInterface();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	


}

package org.cocktail.mangue.client.individu.fonctions;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.fonctions.IndividuFonctionsView;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuAutrFonct;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuFonctInst;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuFonctStruct;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuFonctions;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class IndividuFonctionsCtrl {

	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;
	private IndividuFonctionsView myView;
	private InfosComplementairesCtrl ctrlParent;

	private ListenerFonctions listenerFonctions = new ListenerFonctions();

	private FonctionStructureCtrl ctrlStructure;
	private FonctionInstanceCtrl ctrlInstance;
	private FonctionExpertiseCtrl ctrlExpertise;

	private boolean modeCreation, saisieEnabled;
	private EODisplayGroup eod;
	private EOIndividuFonctions currentFonction;
	private EOIndividu currentIndividu;

	private PopupDonneeListener listenerType = new PopupDonneeListener();

	public IndividuFonctionsCtrl(InfosComplementairesCtrl ctrlParent, EOEditingContext editingContext) {

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		eod = new EODisplayGroup();
		myView = new IndividuFonctionsView(null, MODE_MODAL.booleanValue(),eod);

		myView.getMyEOTable().addListener(listenerFonctions);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		ctrlStructure = new FonctionStructureCtrl(this, ec);
		ctrlInstance = new FonctionInstanceCtrl(this, ec);
		ctrlExpertise = new FonctionExpertiseCtrl(this, ec);

		// Initialisation des layouts
		myView.getSwapView().add("VIDE",new JPanel(new BorderLayout()));
		myView.getSwapView().add(EOIndividuFonctions.TYPE_STRUCTURE,ctrlStructure.getView());
		myView.getSwapView().add(EOIndividuFonctions.TYPE_INSTANCE,ctrlInstance.getView());
		myView.getSwapView().add(EOIndividuFonctions.TYPE_EXPERTISE, ctrlExpertise.getView());

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));

		setSaisieEnabled(false);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		myView.getPopupTypes().addActionListener(listenerType);

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(ec, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		setSaisieEnabled(false);

	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}


	public EOIndividuFonctions currentFonction() {
		return currentFonction;
	}
	public void setCurrentFonction(EOIndividuFonctions currentFonction) {
		this.currentFonction = currentFonction;
		updateData();
	}
	public EOIndividu currentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
	}

	public void employeHasChanged(NSNotification  notification) {
		clearTextFields();
		if (notification != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(ec,(Number)notification.object(), false));
		}
	}

	public void actualiser() {

		eod.setObjectArray(EOIndividuFonctions.findForIndividu(ec, currentIndividu()));
		myView.getMyEOTable().updateData();
		updateUI();

	}
	public JFrame getView() {
		return myView;
	}
	public JPanel getViewFonctions() {
		return myView.getViewProlongations();
	}

	public NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	public NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}
	
	private void ajouter() {

		clearTextFields();

		myView.getBtnAjouter().setEnabled(false);
		myView.getBtnModifier().setEnabled(false);
		myView.getBtnSupprimer().setEnabled(false);
		myView.getMyEOTable().setEnabled(false);
		myView.getLblTypeModalite().setForeground(Color.RED);
		myView.getPopupTypes().setEnabled(true);

		myView.getBtnAnnuler().setEnabled(true);
		((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), "VIDE");

		ctrlParent.setIsLocked(true);

	}

	private void modifier() {

		setModeCreation(false);
		setSaisieEnabled(true);
		ctrlParent.setIsLocked(true);
		updateUI();
	}



	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cet enregistrement ?", "Oui", "Non"))		
			return;			

		try {

			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlStructure.supprimer();break;
			case 2 : ctrlInstance.supprimer();break;
			case 3 : ctrlExpertise.supprimer();break;
			}

			ec.saveChanges();
			actualiser();

		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			ec.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			ec.revert();
		}
	}

	private void valider() {

		try {
			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlStructure.valider();break;
			case 2 : ctrlInstance.valider();break;
			case 3 : ctrlExpertise.valider();break;
			}

			ec.saveChanges();
			setSaisieEnabled(false);

			if (modeCreation) {
				EOIndividuFonctions fonction = currentFonction();
				actualiser();
				if (fonction != null)
					myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(fonction));
			}
			else {
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentFonction())));
				myView.getMyEOTable().updateUI();
				listenerFonctions.onSelectionChanged();
			}

			updateUI();
			ctrlParent.setIsLocked(false);

		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			myView.toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {

		ec.revert();
		setSaisieEnabled(false);
		listenerFonctions.onSelectionChanged();
		ctrlParent.setIsLocked(false);

		updateUI();

	}

	public void nettoyerChamps(NSNotification notification) {

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
		currentIndividu = null;
		updateUI();
	}

	private void clearTextFields() {

		myView.getPopupTypes().setSelectedIndex(0);

		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");

		ctrlStructure.clearTextFields();
		ctrlInstance.clearTextFields();
		ctrlExpertise.clearTextFields();

	}

	private void updateData() {

		clearTextFields();

		if (currentFonction() != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentFonction().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), currentFonction().dateFin());

			// Swap view
			myView.getPopupTypes().removeActionListener(listenerType);
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), currentFonction().fonType());				

			if (EOIndividuFonctions.TYPE_STRUCTURE.equals(currentFonction().fonType())) {
				myView.getPopupTypes().setSelectedIndex(1);		
				ctrlStructure.actualiser(currentFonction());
			}
			if (EOIndividuFonctions.TYPE_INSTANCE.equals(currentFonction().fonType())) {
				myView.getPopupTypes().setSelectedIndex(2);			
				ctrlInstance.actualiser(currentFonction());
			}
			if (EOIndividuFonctions.TYPE_EXPERTISE.equals(currentFonction().fonType())) {
				myView.getPopupTypes().setSelectedIndex(3);			
				ctrlExpertise.actualiser(currentFonction());
			}

			myView.getPopupTypes().addActionListener(listenerType);
		}
		updateUI();

	}

	private class ListenerFonctions implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentFonction((EOIndividuFonctions)eod.selectedObject());
		}
	}

	private void setModeCreation(boolean yn) {
		modeCreation = yn;
	}
	private boolean modeCreation() {
		return modeCreation;
	}

	private class PopupDonneeListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			if (myView.getPopupTypes().getSelectedIndex() > 0) {

				setModeCreation(true);
				setSaisieEnabled(true);
				myView.getLblTypeModalite().setForeground(Color.BLACK);

				switch (myView.getPopupTypes().getSelectedIndex()) {
				case 1 : gererStructure();break;
				case 2 : gererInstance();break;
				case 3 : gererExpertise();break;
				}

				myView.getPopupTypes().setEnabled(false);
			}
		}
	}

	private void setSaisieEnabled(boolean yn) {

		saisieEnabled = yn;

		switch (myView.getPopupTypes().getSelectedIndex()) {
		case 1 : ctrlStructure.setSaisieEnabled(yn);break;
		case 2 : ctrlInstance.setSaisieEnabled(yn);break;
		case 3 : ctrlExpertise.setSaisieEnabled(yn);break;
		}
		
		updateUI();


	}

	private void gererStructure() {
		if (modeCreation) {
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), EOIndividuFonctions.TYPE_STRUCTURE);				
			ctrlStructure.ajouter(EOIndividuFonctStruct.creer(ec, currentIndividu()));			
		}
	}
	private void gererInstance() {
		if (modeCreation) {
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), EOIndividuFonctions.TYPE_INSTANCE);				
			ctrlInstance.ajouter(EOIndividuFonctInst.creer(ec, currentIndividu));			
		}		
	}
	private void gererExpertise() {
		if (modeCreation) {
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), EOIndividuFonctions.TYPE_EXPERTISE);				
			ctrlExpertise.ajouter(EOIndividuAutrFonct.creer(ec, currentIndividu));			
		}		
	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}


	private void updateUI() {

		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getPopupTypes().setEnabled(saisieEnabled() && modeCreation());	
		myView.getMyEOTable().setEnabled(!saisieEnabled());

		myView.getBtnAjouter().setEnabled(currentIndividu() != null);
		myView.getBtnModifier().setEnabled(currentFonction() != null);
		myView.getBtnSupprimer().setEnabled(currentFonction() != null);

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, saisieEnabled());		
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, saisieEnabled());		

	}



	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}

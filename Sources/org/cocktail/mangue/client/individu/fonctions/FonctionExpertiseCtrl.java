// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.individu.fonctions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.fonctions.FonctionExpertiseView;
import org.cocktail.mangue.client.select.FonctionExpertiseSelectCtrl;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOFonctionExpertConseil;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuAutrFonct;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuFonctions;

import com.webobjects.eocontrol.EOEditingContext;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class FonctionExpertiseCtrl {

	private EOEditingContext ec;

	private IndividuFonctionsCtrl ctrlParent;

	private FonctionExpertiseView myView;
	private EOFonctionExpertConseil currentFonction;
	private EOIndividuAutrFonct currentObject;
	private EOIndividu currentIndividu;
	private boolean saisieEnabled;

	public FonctionExpertiseCtrl(IndividuFonctionsCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new FonctionExpertiseView();

		myView.getBtnGetFonction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getFonction();}}
		);

		setSaisieEnabled(false);

		CocktailUtilities.initTextField(myView.getTfFonction(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeDecharge(), false, false);

	}

	public EOFonctionExpertConseil currentFonction() {
		return currentFonction;
	}

	public void setCurrentFonction(EOFonctionExpertConseil currentFonction) {
		this.currentFonction = currentFonction;
		CocktailUtilities.setTextToField(myView.getTfFonction(), "");
		CocktailUtilities.setTextToField(myView.getTfTypeDecharge(), "");
		if (currentFonction != null)  {
			CocktailUtilities.setTextToField(myView.getTfFonction(), currentFonction.llFonction());
			if (currentFonction.typeDecharge() != null)
				CocktailUtilities.setTextToField(myView.getTfTypeDecharge(), currentFonction.typeDecharge().libelleLong());
		}
	}

	public EOIndividuAutrFonct currentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOIndividuAutrFonct currentObject) {
		this.currentObject = currentObject;
		updateData();
	}
	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}


	
	public JPanel getView() {
		return myView;
	}	
	private void getFonction() {
		EOFonctionExpertConseil fonction = FonctionExpertiseSelectCtrl.sharedInstance(ec).getFonction();
		if (fonction != null)
			setCurrentFonction(fonction);
	}

	public void clearTextFields() {

		myView.getTfFonction().setText("");
		myView.getTfTypeDecharge().setText("");

	}

	public void ajouter(EOIndividuAutrFonct newObject) {
		setCurrentIndividu(newObject.individu());
		setCurrentObject(newObject);
	}

	public boolean valider() {

		currentObject().setDateDebut(ctrlParent.getDateDebut());
		currentObject().setDateFin(ctrlParent.getDateFin());
		currentObject().setFonctionRelationship(currentFonction());
		
		return true;
	}

	private void updateData() {

		clearTextFields();
		if (currentObject() != null) {
			setCurrentFonction(currentObject().fonction());
		}

		updateUI();
	}

	public void supprimer() throws Exception {
		try {
			ec.deleteObject(currentObject());
		}
		catch (Exception ex) {
			throw ex;
		}		
	}



	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}


	public void actualiser(EOIndividuFonctions fonction) {
		
		clearTextFields();
		setCurrentIndividu(fonction.individu());
		setCurrentObject(EOIndividuAutrFonct.findForKey(ec, fonction.fonId()));

	}

	public void updateUI() {

		myView.getBtnGetFonction().setEnabled(saisieEnabled());

	}


}
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.individu.fonctions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.fonctions.FonctionStructureView;
import org.cocktail.mangue.client.select.FonctionStructurelleSelectCtrl;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOFonctionStructurelle;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuFonctStruct;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuFonctions;

import com.webobjects.eocontrol.EOEditingContext;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class FonctionStructureCtrl {

	private EOEditingContext ec;

	private IndividuFonctionsCtrl ctrlParent;

	private FonctionStructureView myView;
	private EOFonctionStructurelle currentFonction;
	private EOIndividuFonctStruct currentObject;
	private EOIndividu currentIndividu;
	private EOStructure currentStructure;
	
	private boolean saisieEnabled;

	public FonctionStructureCtrl(IndividuFonctionsCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new FonctionStructureView();

		myView.getBtnGetFonction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getFonction();}}
		);
		myView.getBtnGetStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getStructure();}}
		);

		setSaisieEnabled(false);

		CocktailUtilities.initTextField(myView.getTfFonction(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeDecharge(), false, false);
		CocktailUtilities.initTextField(myView.getTfStructure(), false, false);
		
	}
	
	
	public EOFonctionStructurelle currentFonction() {
		return currentFonction;
	}

	public void setCurrentFonction(EOFonctionStructurelle currentFonction) {
		this.currentFonction = currentFonction;
		CocktailUtilities.setTextToField(myView.getTfFonction(), "");
		CocktailUtilities.setTextToField(myView.getTfTypeDecharge(), "");
		if (currentFonction != null)  {
			CocktailUtilities.setTextToField(myView.getTfFonction(), currentFonction.llFonction());
			if (currentFonction.typeDecharge() != null)
				CocktailUtilities.setTextToField(myView.getTfTypeDecharge(), currentFonction.typeDecharge().libelleLong());
		}
	}

	public EOIndividuFonctStruct currentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOIndividuFonctStruct currentObject) {
		this.currentObject = currentObject;
		updateData();
	}
	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}



	public EOStructure currentStructure() {
		return currentStructure;
	}


	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
		CocktailUtilities.setTextToField(myView.getTfStructure(), "");
		if (currentStructure != null)
			CocktailUtilities.setTextToField(myView.getTfStructure(), currentStructure.llStructure());
	}


	public JPanel getView() {
		return myView;
	}	

	private void getFonction() {
		EOFonctionStructurelle fonction = FonctionStructurelleSelectCtrl.sharedInstance(ec).getFonction();
		if (fonction != null)
			setCurrentFonction(fonction);
	}
	private void getStructure() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(ec);
		if (structure != null)
			setCurrentStructure(structure);
	}

	public void clearTextFields() {

		myView.getTfFonction().setText("");
		myView.getTfStructure().setText("");
		myView.getTfTypeDecharge().setText("");

	}


	public void ajouter(EOIndividuFonctStruct newObject) {
		setCurrentObject(newObject);
		setCurrentIndividu(newObject.individu());
		updateData();
		updateUI();		
	}

	public boolean valider() {

		currentObject().setDateDebut(ctrlParent.getDateDebut());
		currentObject().setDateFin(ctrlParent.getDateFin());

		currentObject().setFonctionRelationship(currentFonction());
		currentObject().setStructureRelationship(currentStructure());
		
		return true;
	}

	private void updateData() {

		clearTextFields();
		if (currentObject() != null) {

			setCurrentFonction(currentObject().fonction());
			setCurrentStructure(currentObject().structure());

		}

		updateUI();

	}

	public void supprimer() throws Exception {
		try {
			ec.deleteObject(currentObject());
		}
		catch (Exception ex) {
			throw ex;
		}		
	}



	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}


	public void actualiser(EOIndividuFonctions fonction) {
		clearTextFields();
		setCurrentIndividu(fonction.individu());
		setCurrentObject(EOIndividuFonctStruct.findForKey(ec, fonction.fonId()));

	}

	public void updateUI() {

		myView.getBtnGetFonction().setEnabled(saisieEnabled());
		myView.getBtnGetStructure().setEnabled(saisieEnabled());

	}


}
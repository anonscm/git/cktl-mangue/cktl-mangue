// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.individu.fonctions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.fonctions.FonctionInstanceView;
import org.cocktail.mangue.client.select.FonctionInstanceSelectCtrl;
import org.cocktail.mangue.client.select.QualiteIndividuSelectCtrl;
import org.cocktail.mangue.client.select.elections.InstanceSelectCtrl;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOFonctionRelevantInstance;
import org.cocktail.mangue.modele.grhum.EOQualiteIndividu;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuFonctInst;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuFonctions;

import com.webobjects.eocontrol.EOEditingContext;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class FonctionInstanceCtrl {

	private EOEditingContext ec;
	private IndividuFonctionsCtrl ctrlParent;

	private FonctionInstanceView myView;
	
	private EOFonctionRelevantInstance currentFonction;
	private EOIndividuFonctInst currentObject;
	private EOIndividu currentIndividu;
	private EOInstance currentInstance;
	private EOQualiteIndividu currentQualite;

	private boolean saisieEnabled;

	public FonctionInstanceCtrl(IndividuFonctionsCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new FonctionInstanceView();

		myView.getBtnGetFonction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getFonction();}}
		);
		myView.getBtnGetInstance().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getInstance();}}
		);
		myView.getBtnGetQualite().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getQualite();}}
		);

		setSaisieEnabled(false);

		CocktailUtilities.initTextField(myView.getTfFonction(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeDecharge(), false, false);
		CocktailUtilities.initTextField(myView.getTfInstance(), false, false);
		CocktailUtilities.initTextField(myView.getTfQualite(), false, false);

	}

	
	public EOFonctionRelevantInstance currentFonction() {
		return currentFonction;
	}
	public void setCurrentFonction(EOFonctionRelevantInstance currentFonction) {
		this.currentFonction = currentFonction;
		CocktailUtilities.setTextToField(myView.getTfFonction(), "");
		CocktailUtilities.setTextToField(myView.getTfTypeDecharge(), "");
		if (currentFonction != null)  {
			CocktailUtilities.setTextToField(myView.getTfFonction(), currentFonction.llFonction());
			if (currentFonction.typeDecharge() != null)
				CocktailUtilities.setTextToField(myView.getTfTypeDecharge(), currentFonction.typeDecharge().libelleLong());
		}
	}
	public EOIndividuFonctInst currentObject() {
		return currentObject;
	}
	public void setCurrentObject(EOIndividuFonctInst currentObject) {
		this.currentObject = currentObject;
		updateData();
	}
	public EOInstance currentInstance() {
		return currentInstance;
	}
	public void setCurrentInstance(EOInstance currentInstance) {
		this.currentInstance = currentInstance;
		CocktailUtilities.setTextToField(myView.getTfInstance(), "");
		if (currentInstance != null)
			CocktailUtilities.setTextToField(myView.getTfInstance(), currentInstance.llInstance());
	}
	public EOQualiteIndividu currentQualite() {
		return currentQualite;
	}
	public void setCurrentQualite(EOQualiteIndividu currentQualite) {
		this.currentQualite = currentQualite;
		CocktailUtilities.setTextToField(myView.getTfQualite(), "");
		if (currentQualite != null)
			CocktailUtilities.setTextToField(myView.getTfQualite(), currentQualite.llQualite());
	}




	public JPanel getView() {
		return myView;
	}	
	
	private void getFonction() {
		EOFonctionRelevantInstance fonction = FonctionInstanceSelectCtrl.sharedInstance(ec).getFonction();
		if (fonction != null)
			setCurrentFonction(fonction);
	}
	private void getInstance() {
		EOInstance instance = InstanceSelectCtrl.sharedInstance(ec).getInstance(null);
		if (instance != null)
			setCurrentInstance(instance);
	}
	private void getQualite() {
		EOQualiteIndividu qualite = QualiteIndividuSelectCtrl.sharedInstance(ec).getQualite();
		if (qualite != null)
			setCurrentQualite(qualite);
	}

	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}

	public void clearTextFields() {

		myView.getTfFonction().setText("");
		myView.getTfTypeDecharge().setText("");
		myView.getTfInstance().setText("");
		myView.getTfQualite().setText("");

	}

	public void ajouter(EOIndividuFonctInst newObject) {
		setCurrentObject(newObject);
		setCurrentIndividu(newObject.individu());
		updateData();
		updateUI();		
	}

	public boolean valider() {

		currentObject().setDateDebut(ctrlParent.getDateDebut());
		currentObject().setDateFin(ctrlParent.getDateFin());

		currentObject().setFonctionRelationship(currentFonction());
		currentObject().setInstanceRelationship(currentInstance());
		currentObject().setQualiteRelationship(currentQualite());
		
		return true;
	}

	private void updateData() {
		clearTextFields();
		if (currentObject() != null) {
			setCurrentFonction(currentObject().fonction());
			setCurrentInstance(currentObject().instance());
			setCurrentQualite(currentObject().qualite());
		}
		updateUI();
	}

	public void supprimer() throws Exception {
		try {
			ec.deleteObject(currentObject());
		}
		catch (Exception ex) {
			throw ex;
		}		
	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}

	public void actualiser(EOIndividuFonctions fonction) {
		clearTextFields();
		setCurrentIndividu(fonction.individu());
		setCurrentObject(EOIndividuFonctInst.findForKey(ec, fonction.fonId()));
	}

	public void updateUI() {
		myView.getBtnGetFonction().setEnabled(saisieEnabled());
		myView.getBtnGetQualite().setEnabled(saisieEnabled());
		myView.getBtnGetInstance().setEnabled(saisieEnabled());
		myView.getBtnDelQualite().setEnabled(saisieEnabled() && currentQualite() != null);
	}

}
//
//  ChoixIndividu.java
//  Mangue
//
//  Created by Christine Buttin on Wed Jul 20 2005.
//  Copyright (c) 2001 __MyCompanyName__. All rights reserved.
//

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JDialog;

import org.cocktail.client.components.ModalDialogWithDisplayGroup;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.common.LogManager;
import org.cocktail.component.COCheckbox;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;

/** Permet de choisir un individu lors de l'identification des homonymes. <BR>
 * Retourne :
 * 	<UL>l'individu passe en parametre dans la methode selectionnerIndividu si il n'y a pas d'homonyne ou que l'utilisateur
 * 	maintient la cretion de l'individu</UL>
 * 	<UL>l'homonyme identifie par l'utilisateur comme le meme individu</UL>
 * 	<UL>null si l'utilisateur a annule</UL>
 */
// 19/01/2011 - Adaptation Netbeans
public class ChoixIndividu extends ModalDialogWithDisplayGroup {
	private static ChoixIndividu sharedInstance = null;
	// Objets Graphiques
	public COCheckbox checkboxPrenom;
	public EOTable listeAffichage;
	private EOIndividu currentIndividu;
	private EOIndividu individuSelectionne;

	/** Méthodes statiques */
	public static ChoixIndividu sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new ChoixIndividu();
		}
		return sharedInstance;
	}
	/** 
	 * Constructeur
	 */
	public ChoixIndividu() {
		super();
		EOArchive.loadArchiveNamed("ChoixIndividu", this,"org.cocktail.mangue.client.individu.interfaces", this.disposableRegistry());
	}
	public void connectionWasEstablished() {
		setWindowResizable(window(),false);
		((JDialog)window()).setModal(true);
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
		GraphicUtilities.rendreNonEditable(listeAffichage);
		listeAffichage.table().addMouseListener(new DoubleClickListener());
	}

	/**
	 * Selection d'un individu
	 *
	 */
	public EOIndividu selectionnerIndividu(EOEditingContext editingContext,EOIndividu unIndividu) {
		setEditingContext(editingContext);
		((EODistributedDataSource)displayGroup().dataSource()).setEditingContext(editingContext); // pour travailler dans cet editing context
		currentIndividu = unIndividu;
		checkboxPrenom.setSelected(false);
		NSArray individus = EOIndividu.rechercherHomonymes(this.editingContext(),currentIndividu.nomUsuel(),null);
		if (individus.count() == 0) {
			return currentIndividu;
		} else {
			displayGroup().setObjectArray(individus);
			displayGroup().setSelectionIndexes(new NSArray(new Integer(0)));
			activateWindow();	
		}

		return individuSelectionne;
	}

	// actions
	public void modifierRecherche() {
		LogManager.logInformation("Modification de la recherche sur les noms et prenoms. Utiliser les prenoms : " + checkboxPrenom.isSelected());
		String prenom = null;
		if (checkboxPrenom.isSelected()) {
			prenom = currentIndividu.prenom();
		}
		NSArray individus = EOIndividu.rechercherHomonymes(this.editingContext(),currentIndividu.nomUsuel(),prenom);
		displayGroup().setObjectArray(individus);
		displayGroup().setSelectionIndexes(new NSArray(new Integer(0)));
	}

	public void choisir() {
		individuSelectionne = (EOIndividu)displayGroup().selectedObject();
		LogManager.logDetail("ChoixIndividu - Individu choisi : " + individuSelectionne.identite());
		closeWindow();
	}

	public void annuler() {
		LogManager.logDetail("ChoixIndividu - Annulation");
		individuSelectionne = null;
		closeWindow();	
	}
	public void nouvelIndividu() {
		individuSelectionne = currentIndividu;
		LogManager.logDetail("ChoixIndividu - Choix individu en cours de creation");
		closeWindow();
	}
	// Classe d'ecoute pour les doubleclics dans la table view  */
	public class DoubleClickListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if(e.getClickCount() == 2) {
				choisir();
			}
		}
	}

}


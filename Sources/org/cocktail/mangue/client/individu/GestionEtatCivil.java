/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.Constantes;
import org.cocktail.common.LogManager;
import org.cocktail.component.COButton;
import org.cocktail.component.COComboBox;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.agents.AgentsCtrl;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.individu.infoscir.InfosRetraiteCtrl;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.mangue.client.individu.infosperso.AutorisationTravailCtrl;
import org.cocktail.mangue.client.individu.infosperso.InfosPersonnellesCtrl;
import org.cocktail.mangue.client.individu.infosperso.SituationFamilialeCtrl;
import org.cocktail.mangue.client.outils_interface.ModelePageGestionIndividu;
import org.cocktail.mangue.client.select.DepartementSelectCtrl;
import org.cocktail.mangue.client.select.NomenclatureSelectCodeLibelleCtrl;
import org.cocktail.mangue.client.select.PaysSelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOCommune;
import org.cocktail.mangue.common.modele.nomenclatures.EODepartement;
import org.cocktail.mangue.common.modele.nomenclatures.EOLimiteAge;
import org.cocktail.mangue.common.modele.nomenclatures.EOLoge;
import org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOCompte;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.sequences.EOSeqMatriculePersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOImageView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
/** pour l'affichage des photos : 3 niveaux de preferences :<BR>
 *  <UL>global dans GrhumParametres : si non, pas d'affichage quelques soient les autres reglages</UL>
 * 	<UL>reglage dans les preferences de l'utilisateur</UL>
 * La gestion automatique des numeros de matricule (positionnee dans GrhumParametres) est faite
 * avec l'aide d'une sequence Oracle<BR>
 * Gestion des numeros Insee :
 * le numero insee et le numero insee provisoire ne peuvent etre saisis en meme temps, l'un des deux
 * est forcement nul. Aucun controle sauf de longueur n'est fait sur le numero insee provisoire.<BR>
 *
 */

public class GestionEtatCivil extends ModelePageGestionIndividu {

	public EOImageView 	grandePhoto;
	public COComboBox 	popupMinisteres;
	public JComboBox 	popupSituationLogement;

	public JLabel labelSS, labelNomPatronymique, lblInfosDeces, lblNumenEpicea;
	public String numenEpicea;

	public EODisplayGroup displayGroupCivilite;
	// Variables locales
	private EOAgentPersonnel currentAgent;
	private NSData image;
	private String ancienNumeroInsee,numeroInsee;
	private Integer cleInsee;
	private String paysNaissance,departementNaissance;
	private String npc, matricule, texteLibre;
	private boolean peutSaisirAutresInfos;		// pour les créations afin de vérifier les homonymes			
	private Number individuPrecedentID;		// au cas où il y a des annulations pour revenir en arrière
	private boolean	creationIndividu,peutModifierMatricule, blocageInsee;

	private EOCompte comptePourIndividu;
	private EOLimiteAge limiteAgeRetraite;

	private NSData dataImageAnonyme;
	private Dimension tailleGrandePhoto;

	public COButton btnCoordonnees;
	private PaysSelectCtrl mySelectorPaysNaissance;
	private PaysSelectCtrl mySelectorNationalite;
	private DepartementSelectCtrl mySelectorDepartement;
	private InfosComplementairesCtrl diversCtrl = null;

	/** Notification envoyee suite a la creation d'un nouvel agent */
	public static String NOUVEL_EMPLOYE = "NotifNouvelEmploye";
	public static String INSPECTION_INDIVIDU = "NotifInspectionIndividu";
	public GestionEtatCivil() {
		super(((ApplicationClient)ApplicationClient.sharedApplication()).getEdc());
	}

	public boolean isBlocageInsee() {
		return blocageInsee;
	}
	public void setBlocageInsee(boolean blocageInsee) {
		this.blocageInsee = blocageInsee;
	}

	// accesseurs
	public String numeroInsee() {
		return numeroInsee;
	}
	public void setNumeroInsee(String numeroInsee) {

		this.numeroInsee = numeroInsee;

		if (this.numeroInsee != null) {
			this.numeroInsee = this.numeroInsee.toUpperCase();
		}
		setEdited(true);
		// Correction pour la DT 1670
		if (currentIndividu() != null && (ancienNumeroInsee == null || numeroInsee == null || numeroInsee.equals(ancienNumeroInsee) == false)) {
			preparerInfosNaissance(numeroInsee);
			ancienNumeroInsee = numeroInsee;
		}
	}

	public String cleInsee() {
		if (cleInsee != null) {
			if (cleInsee.intValue() < 10)
				return "0"+cleInsee;
			return cleInsee.toString();
		}
		return null;
	}
	public void setCleInsee(String cleInsee) {
		if (cleInsee != null)
			this.cleInsee = new Integer(cleInsee);
		else
			this.cleInsee = null;
		setEdited(true);
	}


	public String departementNaissance() {
		return departementNaissance;
	}

	/**
	 * 
	 * @param codeDepartement
	 */
	public void setDepartementNaissance(String codeDepartement) {
		if (codeDepartement == null || codeDepartement.length() > 3) {
			currentIndividu().setToDepartementRelationship(null);
			departementNaissance = "";
		} else {
			if (codeDepartement.equals(departementNaissance) == false) {
				if (codeDepartement.length() == 2) {	// département non DOM-TOM
					codeDepartement = "0" + codeDepartement;
				}
				EODepartement departement = (EODepartement)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"Departement", INomenclature.CODE_KEY,codeDepartement);
				if (departement != null) {
					currentIndividu().setToDepartementRelationship(departement);
					departementNaissance = departement.code();
				} else {
					departementNaissance = "";
				}
			}
		}
		updaterDisplayGroups();
	}

	public String paysNaissance() {
		return paysNaissance;
	}

	/** Modifie le pays de naissance en verifiant la validite du pays selon la valeur du booleen */
	// 19/09/08 - pour vérifier la validité du pays à la date de naissance
	// 12/02/09 - N'utiliser l'accesseur que si on veut provoquer les effets de bord associés
	public void setPaysNaissance(String codePays,boolean verifierValidite) {
		if (codePays == null) {
			setDepartementNaissance(null);		// pour forcer la modification du département si il y en avait un
			currentIndividu().setToPaysNaissanceRelationship(null);
			paysNaissance = "";
		} else if (paysNaissance.equals(codePays) == false) {
			EOPays pays = (EOPays)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),EOPays.ENTITY_NAME,EOPays.CODE_KEY,codePays);
			if (pays != null) {
				currentIndividu().setToPaysNaissanceRelationship(pays);
				if (currentIndividu().toDepartement() != null && pays.code().equals(EOPays.CODE_PAYS_FRANCE) == false) {
					setDepartementNaissance(null);
				}
				this.paysNaissance = codePays;
				if (verifierValidite) {
					verifierValiditePaysNaissance();
				}
			} else {
				this.paysNaissance = "";
			}
		}
		updaterDisplayGroups();
	}
	public void setPaysNaissance(String codePays) {
		setPaysNaissance(codePays,true);
	}
	public String paysNationalite() {
		if (currentIndividu() == null || currentIndividu().toPaysNationalite() == null) {
			return null;
		} else {
			return currentIndividu().toPaysNationalite().code();
		}
	}
	public void setPaysNationalite(String codePays) {
		if (codePays == null) {
			currentIndividu().setToPaysNationaliteRelationship(null);
		} else {
			EOPays pays = (EOPays)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),EOPays.ENTITY_NAME, EOPays.CODE_KEY,codePays);
			// 19/09/08 - on vérifie que le pays est valide à la date du jour
			if (pays != null && EOPays.estPaysValideADate(editingContext(), pays.code(), new NSTimestamp()) == false) {
				pays = null;
			}

			currentIndividu().setToPaysNationaliteRelationship(pays);
		}
		updaterDisplayGroups();
	}
	public String codeSituationFamiliale() {
		if (currentIndividu() == null || currentIndividu().toSituationFamiliale() == null) {
			return null;
		} else {
			return currentIndividu().toSituationFamiliale().code();
		}
	}
	public void setCodeSituationFamiliale(String unCode) {
		if (unCode == null) {
			if (currentIndividu().toSituationFamiliale() != null) {
				currentIndividu().removeObjectFromBothSidesOfRelationshipWithKey(currentIndividu().toSituationFamiliale(),"toSituationFamiliale");
			}
		} else {
			unCode = unCode.toUpperCase();
			EOSituationFamiliale situation = (EOSituationFamiliale)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"SituationFamiliale","code",unCode);
			currentIndividu().setToSituationFamilialeRelationship(situation);
		}
		updaterDisplayGroups();
	}
	public NSData image() {
		if (image == null)
			return dataImageAnonyme;
		return image;
	}
	public String ageMiseOfficeRetraite() {
		if (limiteAgeRetraite == null) {
			return null;
		} else {
			return limiteAgeRetraite.libelleLong();
		}
	}
	public String npc() {
		return npc;
	}
	public void setNpc(String aString) {
		npc = aString;
		if (modificationEnCours()) {
			setEdited(true);
		}
	}
	public String numenEpicea() {
		return numenEpicea;
	}
	public void setNumenEpicea(String aString) {
		numenEpicea = aString;
		if (modificationEnCours()) {
			setEdited(true);
		}
	}

	public String matricule() {
		return matricule;
	}
	public void setMatricule(String aString) {
		matricule = aString;
		if (modificationEnCours()) {
			setEdited(true);
		}
	}
	public String texteLibre() {
		return texteLibre;
	}
	public void setTexteLibre(String aString) {
		texteLibre = aString;
		if (modificationEnCours()) {
			setEdited(true);
		}
	}


	/**
	 * 
	 */
	public void displayGroupDidChangeSelection(EODisplayGroup group) {

		if (currentIndividu() != null && currentIndividu().personnel() != null) {
			matricule = currentIndividu().personnel().noMatricule();
			npc = currentIndividu().personnel().npc();
			
			popupSituationLogement.setSelectedItem(currentIndividu().personnel().toLoge());
			
			popupMinisteres.setSelectedItem(currentIndividu().personnel().toMinistere());

			if (popupMinisteres.getSelectedIndex() > 0) {
				if (((EOMinisteres)popupMinisteres.getSelectedItem()).estMAAF())
					lblNumenEpicea.setText("EPICEA");
				else
					lblNumenEpicea.setText("NUMEN");
			}
		} else {
			matricule = null;
			npc = null;
			popupMinisteres.setSelectedItem(null);
		}

		preparerNumeroInseePourAffichage();

		lblInfosDeces.setVisible(false);
		if (currentIndividu() != null && currentIndividu().dDeces() != null) {

			EODepart departDeces = EODepart.departDecesValidePourIndividu(editingContext(), currentIndividu());
			lblInfosDeces.setVisible(departDeces != null);

			if (departDeces != null) {
				lblInfosDeces.setText("Date de Décès : " + DateCtrl.dateToString(currentIndividu().dDeces()));
			}

		}

		majLimiteAge();

		super.displayGroupDidChangeSelection(group);
	}

	/**
	 * 
	 * @param group
	 * @param value
	 * @param eo
	 * @param key
	 */
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("nomUsuel") && value != null) {
			// 23/01/09 - Nom et prénom ne comportent plus que des caractères majuscules non accentués
			String nomPropre = StringCtrl.chaineClaire((String)value,false);
			if (nomPropre != null) {
				String nomUpper = nomPropre.toUpperCase();
				if (currentIndividu().nomUsuel().equals(nomUpper) == false) {
					currentIndividu().setNomUsuel(nomUpper);
				}
			} else {
				currentIndividu().setNomUsuel(null);
			}
			if (currentIndividu().cCivilite() != null && currentIndividu().cCivilite().equals(ManGUEConstantes.MADAME) == false) {
				if (currentIndividu().nomPatronymique() == null || modeCreation()) {
					currentIndividu().setNomPatronymique(currentIndividu().nomUsuel());
				}
				if (currentIndividu().nomPatronymiqueAffichage() == null || modeCreation()) {
					currentIndividu().setNomPatronymiqueAffichage(StringCtrl.capitalizedString((String)value));
				}
			} else {
				currentIndividu().setNomPatronymique(null);
			}
			if (currentIndividu().nomAffichage() == null || modeCreation()) {
				currentIndividu().setNomAffichage(StringCtrl.capitalizedString((String)value));
			}
		}
		if (key.equals("nomPatronymique")) {
			String nomPropre = StringCtrl.chaineClaire((String)value,false);
			if (nomPropre != null) {
				currentIndividu().setNomPatronymique(nomPropre.toUpperCase());
				currentIndividu().setNomPatronymiqueAffichage(StringCtrl.capitalizedString((String)value));
			} else {
				currentIndividu().setNomPatronymique(null);
				currentIndividu().setNomPatronymiqueAffichage(null);
			}
		}
		if (key.equals("prenom") && value != null) {
			String prenomPropre = StringCtrl.chaineClaire((String)value,false);
			// 14/12/07 - en mode création, repoussé à l'enregistrement pour pouvoir faire les recherches
			//	if (!modeCreation && currentIndividu().prenom().equals(prenomPropre.toUpperCase()) == false) {
			// 23/01/09 - capitalisation faite avant recherche, tous les noms et prénoms sont  capitalisés
			if (prenomPropre != null) {
				String prenomUpper = prenomPropre.toUpperCase();
				if (currentIndividu().prenom().equals(prenomUpper) == false) {
					currentIndividu().setPrenom(prenomUpper); 
				}
			}
			if (currentIndividu().prenomAffichage() == null) {
				String prenomAffichage = StringCtrl.capitalizedString((String)value);
				currentIndividu().setPrenomAffichage(prenomAffichage);
			}
		}

		if (key.equals("dateNaissanceFormatee")) {
			verifierValiditePaysNaissance();
			majLimiteAge();
		}
		if (key.equals(EOIndividu.C_CIVILITE_KEY)) {
			// forcer le nom patronymique pour un homme
			int style = Font.PLAIN;
			if (value.equals(ManGUEConstantes.MADAME)) {
				style = Font.BOLD;
			} else if (value.equals(ManGUEConstantes.MONSIEUR)) {
				if (currentIndividu().nomUsuel() != null && currentIndividu().nomPatronymique() == null) {
					currentIndividu().setNomPatronymique(currentIndividu().nomUsuel());
				}
			} 
			CocktailUtilities.changerFontForLabel(labelNomPatronymique,style);
		} else 
			if (key.equals("peutAfficherPhoto")) {
				//image = null;		// 19/09/08 - pour forcer le raffraîchissement de l'image selon la valeur de peutAfficherPhoto
			}
		if (!peutSaisirAutresInfos) {
			// on est en création et nom, prénom, civilité sont en cours de saisie
			if (currentIndividu().nomUsuel() != null && currentIndividu().nomUsuel().length() > 0 && 
					currentIndividu().prenom() != null && currentIndividu().prenom().length() > 0 && 
					currentIndividu().cCivilite() != null && currentIndividu().cCivilite().length() > 0) {
				peutSaisirAutresInfos = confirmerCreation();
			}
		}

		updaterDisplayGroups();
		
	}

	/**
	 * 
	 */
	public void majLimiteAge() {

		if (currentIndividu() != null && currentIndividu().personnel() != null 
				&& limiteAgeRetraite == null && currentIndividu().personnel().limiteAge() == null) {
			limiteAgeRetraite = EOLimiteAge.limitePourDateNaissance(editingContext(), currentIndividu().dNaissance());
			if (limiteAgeRetraite != null)	
				currentIndividu().personnel().setLimiteAgeRelationship(limiteAgeRetraite);
		}
	}

	/**
	 * 
	 */
	public void ajouter() {

		individuPrecedentID  = individuCourantID();
		peutSaisirAutresInfos = false;
		NSNotificationCenter.defaultCenter().postNotification(ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().postNotification(INSPECTION_INDIVIDU,null);
		super.ajouter();

		String codeMinistere = EOGrhumParametres.getValueParam(ManGUEConstantes.PARAM_KEY_MINISTERE);
		if (codeMinistere != null) {
			popupMinisteres.setSelectedItem(NomenclatureFinder.findForCode(editingContext(),EOMinisteres.ENTITY_NAME, codeMinistere));
		}
		raffraichirChamps();
		popupMinisteres.setEnabled(true);
		popupSituationLogement.setEnabled(true);
	}

	/**
	 * 
	 */
	public void modifier() {

		peutSaisirAutresInfos = true;
		individuPrecedentID  = individuCourantID();
		super.modifier();
		raffraichirChamps();
		popupMinisteres.setEnabled(true);
		popupSituationLogement.setEnabled(true);
		// Dans le cas d'une gestion automatique du matricule, si l'agent n'en a pas lui donner un matricule
		if (matricule == null && !peutModifierMatricule) {
			preparerMatricule();
		}
	}

	/**
	 * 
	 */
	public void annuler() {
		super.annuler();
		popupMinisteres.setEnabled(false);
		popupSituationLogement.setEnabled(false);
		ServerProxy.clientSideRequestRevert(editingContext());
		if (individuCourantID() != individuPrecedentID) {
			NSNotificationCenter.defaultCenter().postNotification(ListeAgents.CHANGER_EMPLOYE,individuPrecedentID);
		}
	}

	private class PopupMinisteresListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			lblNumenEpicea.setText("NUMEN");
			if (popupMinisteres.getSelectedIndex() > 0 && ((EOMinisteres)popupMinisteres.getSelectedItem()).estMAAF())
				lblNumenEpicea.setText("EPICEA");
		}
	}

	/**
	 * 
	 * @return
	 */
	private EOLoge getSituationLogement() {
		return (popupSituationLogement.getSelectedIndex() > 0)?(EOLoge)popupSituationLogement.getSelectedItem():null;
	}

	/**
	 * 
	 */
	public void valider() {

		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		if (!testSaisieValide()) {
			component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			return;
		}
		if (!traitementsAvantValidation()) {
			component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			return;
		}

		if (save()) {
			traitementsApresValidation();
			if (currentIndividu() != null) {
				individuPrecedentID = currentIndividu().noIndividu();
			}
		}
		popupMinisteres.setEnabled(false);
		popupSituationLogement.setEnabled(false);
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

	}
	public void ouvrirInspecteur() {
		SituationFamilialeCtrl.sharedInstance(editingContext()).setCurrentIndividu(currentIndividu());
		SituationFamilialeCtrl.sharedInstance(editingContext()).open();
	}

	/**
	 * 
	 */
	public void agrandir() {
		JFrame frame = new JFrame();
		if (tailleGrandePhoto.getHeight() == 0 || tailleGrandePhoto.getWidth() == 0) {
			frame.setSize(grandePhoto.getSize());
		} else {
			frame.setSize(tailleGrandePhoto);
		}
		frame.getContentPane().add(grandePhoto);
		frame.setVisible(true);
	}


	/**
	 * 
	 */
	public void imprimerFicheIdentite() {

		try {
			Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class,Boolean.class};
			NSMutableArray donneesRecherchees = new NSMutableArray();
			Object[] parametres = new Object[]{editingContext().globalIDForObject(currentIndividu()),donneesRecherchees,new Boolean(currentAgent.peutAfficherInfosPerso())};
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerFicheIdentite",classeParametres,parametres,"FicheIdentite_" + currentIndividu().noIndividu(),"Impression de la fiche d'identité");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}

	public void afficherPaysNaissance() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		if (mySelectorPaysNaissance == null)
			mySelectorPaysNaissance = new PaysSelectCtrl(editingContext());

		EOPays pays = mySelectorPaysNaissance.getPays(null, currentIndividu().dNaissance());
		if (pays != null) {
			paysNaissance = pays.code();
			currentIndividu().setToPaysNaissanceRelationship(pays);
			updaterDisplayGroups();
		}
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}

	public boolean peutAfficherInspecteurs() {
		return currentIndividu() != null && !modeCreation() && !modificationEnCours();
	}

	public boolean peutAfficherInfosPerso() {
		return !modeCreation() && !modificationEnCours() && getCurrentAgent() != null && currentIndividu() != null && getCurrentAgent().peutAfficherInfosPerso() 
				&& (getCurrentAgent().peutAfficherAgents() || getCurrentAgent().peutGererAgents());
	}
	public boolean peutAfficherInfosPro() {
		return !modeCreation() && !modificationEnCours() && getCurrentAgent() != null && currentIndividu() != null && getCurrentAgent().peutAfficherInfosPerso()
				&& (getCurrentAgent().peutAfficherAgents() || getCurrentAgent().peutGererAgents());
	}
	public boolean peutAfficherInfosRetraite() {
		return !modeCreation() && !modificationEnCours() && getCurrentAgent() != null && currentIndividu() != null 
				&& (getCurrentAgent().peutGererCarrieres() || getCurrentAgent().peutAfficherCarrieres());
	}


	public void afficherInfosPerso() {
		CRICursor.setWaitCursor(btnCoordonnees.getParent());
		InfosPersonnellesCtrl.sharedInstance(editingContext()).open(currentIndividu());
		CRICursor.setDefaultCursor(btnCoordonnees.getParent());
	}
	public void afficherDivers() {
		CRICursor.setWaitCursor(btnCoordonnees.getParent());
		if (diversCtrl == null)
			diversCtrl = new InfosComplementairesCtrl(editingContext());
		diversCtrl.open(currentIndividu());
		CRICursor.setDefaultCursor(btnCoordonnees.getParent());
	}
	public void afficherCir() {
		CRICursor.setWaitCursor(btnCoordonnees.getParent());
		InfosRetraiteCtrl.sharedInstance().open(currentIndividu());
		CRICursor.setDefaultCursor(btnCoordonnees.getParent());
	}


	public void afficherPaysNationalite() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		if (mySelectorNationalite == null)
			mySelectorNationalite = new PaysSelectCtrl(editingContext());

		EOPays pays = mySelectorNationalite.getPays(null, currentIndividu().dNaissance());
		if (pays != null) {
			setPaysNationalite(pays.code());
			currentIndividu().setToPaysNationaliteRelationship(pays);
		}
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}

	/**
	 * 
	 */
	public void afficherAgeMiseRetraite() {

		EOLimiteAge limiteAge = (EOLimiteAge)NomenclatureSelectCodeLibelleCtrl.sharedInstance(editingContext()).getObject(NomenclatureFinder.findStatic(editingContext(), EOLimiteAge.ENTITY_NAME));
		if (limiteAge != null) {
			limiteAgeRetraite = limiteAge;
			if (currentIndividu().personnel() != null) {
				currentIndividu().personnel().setLimiteAgeRelationship(limiteAge);
			}
		}
	}
	
	public void supprimerAgeMiseRetraite() {
		limiteAgeRetraite = null;
		if (currentIndividu().personnel() != null && currentIndividu().personnel().limiteAge() != null) {
			currentIndividu().personnel().setLimiteAgeRelationship(null);
		}
		updaterDisplayGroups();
	}

	public void afficherSituationFamiliale() {
		EOSituationFamiliale situation = (EOSituationFamiliale)NomenclatureSelectCodeLibelleCtrl.sharedInstance(editingContext()).getObject(NomenclatureFinder.findStatic(editingContext(), EOSituationFamiliale.ENTITY_NAME));
		if (situation != null) {
			currentIndividu().setToSituationFamilialeRelationship(situation);
		}
	}
	public void afficherAutorisationsTravail() {
		AutorisationTravailCtrl.sharedInstance(editingContext()).setCurrentIndividu(currentIndividu());
		AutorisationTravailCtrl.sharedInstance(editingContext()).open();
	}

	/** affiche les communes d'un departement triees par ordre alpha */
	public void selectCommuneNaissance() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		EOQualifier qualifier = null;
		if (currentIndividu().toDepartement() != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat(EOCommune.C_DEP_KEY + " = %@", new NSArray(currentIndividu().toDepartement().code()));
		} else {
			// cas d'un Corse dont le numéro département dans le numéro Insee est 20
			qualifier = EOQualifier.qualifierWithQualifierFormat(EOCommune.C_POSTAL_KEY + " like '20*'",null);
		}

		EOCommune commune = (EOCommune)NomenclatureSelectCodeLibelleCtrl.sharedInstance(editingContext()).getObject(NomenclatureFinder.findWithQualifier(editingContext(), EOCommune.ENTITY_NAME, qualifier));
		if (commune != null) {

			currentIndividu().setVilleDeNaissance(commune.libelle());
			if (currentIndividu().toDepartement() == null) {
				// cas d'un Corse dont le numéro département dans le numéro Insee est 20
				EODepartement departement = (EODepartement)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),EODepartement.ENTITY_NAME, EODepartement.CODE_KEY,commune.cDep());
				if (departement != null) {
					currentIndividu().setToDepartementRelationship(departement);
					departementNaissance = currentIndividu().toDepartement().code();
				}
			}
			updaterDisplayGroups();

		}

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	public void afficherDepartement() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		if (mySelectorDepartement == null)
			mySelectorDepartement = new DepartementSelectCtrl(editingContext());
		EODepartement departement = mySelectorDepartement.getDepartement(null, currentIndividu().dNaissance());
		if (departement != null) {
			departementNaissance = departement.code();
			currentIndividu().setToDepartementRelationship(departement);
		}
		else 
			departementNaissance = "";
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("setCurrentIndividu", new Class[] {NSNotification.class}),
				AgentsCtrl.NOTIF_SET_INDIVIDU_INFOS,null);
	}

	/**
	 * 
	 */
	public void employeHasChanged(NSNotification aNotif) {

		super.employeHasChanged(aNotif);
		System.err.println("EMPLOYE HAS CHANGED");
		NSNotificationCenter.defaultCenter().postNotification(INSPECTION_INDIVIDU,currentIndividu());

		raffraichirChamps();
	}
	public void setCurrentIndividu(NSNotification aNotif) {
		System.err.println(" SET CURRENT NIDIVIDU INFOS !!!");
		setCurrentIndividu((EOIndividu)aNotif.object());
		raffraichirChamps();
	}

	public EOAgentPersonnel getCurrentAgent() {
		return currentAgent;
	}
	public void setCurrentAgent(EOAgentPersonnel agent) {
		this.currentAgent = agent;
	}

	/** methode du controller display group */
	public boolean modeSaisiePossible() {
		return !modificationEnCours() && conditionSurPageOK() && !estLocke();
	}
	/** methode du controller display group */
	public boolean peutSaisirCivilite() {
		return super.modificationEnCours();
	}
	/** methode du controller display group */
	public boolean modificationEnCours() {
		if (!super.modificationEnCours()) {
			return false;
		} else {
			if (modeCreation()) {
				return peutSaisirAutresInfos;
			} else {
				return true;
			}
		}
	}

	public boolean peutSupprimerLimiteAgeRetraite() {
		return modificationEnCours() && limiteAgeRetraite != null;
	}
	/** La date de deces doit etre fournie */
	public boolean peutSaisirLieuDeces() {
		return modificationEnCours() && currentIndividu().dDeces() != null;
	}
	/** methode du controller display group */
	public boolean peutAgrandir() {
		return currentIndividu() != null && image != null;
	}
	/** On ne peut supprimer les numeros Insee que si ils sont definis */
	public boolean peutEffacerNoSS() {
		return modificationEnCours() && (currentIndividu().indNoInsee() != null || currentIndividu().indNoInseeProv() != null);
	}
	/** on ne peut afficher les communes que pour les personnes nees en France */
	public boolean peutAfficherCommune(){
		return peutAfficherDepartement();
	}
	/** on ne peut afficher les departements que pour les personnes nees en France */
	public boolean peutAfficherDepartement() {
		return modificationEnCours() && currentIndividu().toPaysNaissance() != null && currentIndividu().toPaysNaissance().code().equals(EOPays.CODE_PAYS_FRANCE);
	}
	public boolean peutImprimer() {
		if (currentIndividu() != null && !super.modificationEnCours() && !estLocke() && getCurrentAgent() != null) {
			return getCurrentAgent().peutAfficherIndividu() || getCurrentAgent().peutGererIndividu();
		} else {
			return false;
		}
	}
	/** Retourne true si la gestion des matricules n'est pas automatique (GRhumParametres)  et qu'on est en mode modification */
	public boolean peutModifierMatricule() {
		return modificationEnCours() && peutModifierMatricule;
	}
	public boolean peutModifierMinistere() {
		//		return modificationEnCours();
		return super.modificationEnCours();
	}
	public boolean peutValider() {
		if (modificationEnCours()) {
			if (currentIndividu().cCivilite() == null || currentIndividu().nomUsuel() == null || currentIndividu().prenom() == null) {
				return false;
			}
			if (currentIndividu().cCivilite().equals(Constantes.MADAME) && currentIndividu().nomPatronymique() == null) {
				return false;
			} 
			//Le numéro SS et la clé doivent être fournis si cela est parametre dans Grhum et que le numéro n'est pas provisoire
			if (currentIndividu().prendreEnCompteNumeroProvisoire() == false 
					&& (numeroInsee() == null || cleInsee() == null)) {
				return false;
			}
			if (currentIndividu().dNaissance() == null || currentIndividu().toPaysNationalite() == null) {
				return false;
			}
			if (modeCreation()) {
				if (peutSaisirAutresInfos) {	// pour le cas où on aurait sélectionné un individu existant sans apporter de modification mais qu'on veut ajouter le personnel
					return true;
				} else {
					return false;
				}	
			} else {
				return super.peutValider();
			}
		} else {
			return false;
		}
	}

	/**
	 * 
	 */
	protected void preparerFenetre() {

		super.preparerFenetre();
		setCurrentAgent(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		if (currentIndividu() != null) {
			displayGroup().setSelectedObject(currentIndividu());
		}
		raffraichirChamps();

		dataImageAnonyme = null;
		try {			
			EOClientResourceBundle resourceBundle = new EOClientResourceBundle();
			ImageIcon imgIcon = (ImageIcon)resourceBundle.getObject("anonymous");
			dataImageAnonyme = new NSData(UtilitairesFichier.imageToByteArray(imgIcon.getImage(), "png"));
		}
		catch (Exception e) {
		}

		displayGroupCivilite.fetch();	
		EOGrhumParametres paramBlocageInsee = EOGrhumParametres.findParametre(editingContext(), ManGUEConstantes.PARAM_KEY_BLOCAGE_DOUBLON_INSEE);
		setBlocageInsee(paramBlocageInsee != null && paramBlocageInsee.isParametreVrai());

		String param = EOGrhumParametres.getValueParam(ManGUEConstantes.PARAM_KEY_NO_MATRICULE);
		peutModifierMatricule = param != null && param.equals("M");
		tailleGrandePhoto = grandePhoto.getSize();
		lblInfosDeces.setVisible(false);

		CocktailUtilities.initPopupAvecListe(popupMinisteres, EOMinisteres.findMinisteresValides(editingContext()), true);
		popupMinisteres.addActionListener(new PopupMinisteresListener());
		popupMinisteres.setEnabled(false);
		
		CocktailUtilities.initPopupAvecListe(popupSituationLogement, NomenclatureFinder.findStatic(editingContext(), EOLoge.ENTITY_NAME), true);
		popupSituationLogement.setEnabled(false);

	}

	/** traitements a executer suite a la creation d'un objet */
	protected void traitementsPourCreation() {
		setCurrentIndividu((EOIndividu)displayGroup().selectedObject());
		currentIndividu().init();
		currentIndividu().setToPaysNationaliteRelationship(EOPays.getDefault(editingContext()));
		creationIndividu = true;
	}

	/** traitements a executer avant la destruction d'un objet */
	protected boolean traitementsPourSuppression() {
		// ne devrait jamais être appelée car pas de destruction d'individu
		return true;
	}
	/** retourne le message de confirmation aupres de l'utilisateur avant d'effectuer une suppression  */
	protected String messageConfirmationDestruction() {
		// ne devrait jamais etre appelee car pas de destruction d'individu
		return "";
	}

	/** fetche les individus */
	protected  NSArray fetcherObjets() {
		if (currentIndividu() != null) {
			return new NSArray(currentIndividu());
		} else {
			return null;
		}
	}
	protected  void parametrerDisplayGroup() {
	}

	/** methode a surcharger pour faire des traitements avant l'enregistrement des donnees dans la base
	 * retourne true si on peut enregistrer */
	protected boolean traitementsAvantValidation() {

		// Préparer les champs ad-hoc du numéro Insee de l'individu
		preparerNumeroInseeIndividuAvantValidation();

		// Controle du numero INSEE s'il n'est pas provisoire et d'une longueur de 13 caracteres
		// Blocage si le parametre PARAM_KEY_BLOCAGE_DOUBLON_INSEE est à O sinon alerte
		if (!currentIndividu().prendreEnCompteNumeroProvisoire() 
				&& currentIndividu().estNoInseeProvisoire() == false
				&& currentIndividu().indNoInsee().length() == ManGUEConstantes.LONGUEUR_INSEE) {

			boolean existeDoublons = currentIndividu().aNumeroInseeUnique() == false;

			if (existeDoublons) {
				if (isBlocageInsee()) {
					EODialogs.runErrorDialog("ERREUR", "Ce numéro INSEE est déjà utilisé !");
					return false;
				}
				else {
					if (modeCreation() && !EODialogs.runConfirmOperationDialog("Attention", "Un autre individu a déjà le numéro Insee " + currentIndividu().indNoInsee() + "\nVoulez-vous continuer l'enregistrement des données ?", "Oui", "Non")) {
						return false;
					}
				}
			}

			String codeDepartement = currentIndividu().indNoInsee().substring(5,7);
			if (codeDepartement.equals("99")) {
				String codePays = currentIndividu().indNoInsee().substring(7,10);
				if (currentIndividu().toPaysNaissance() != null && currentIndividu().toPaysNaissance().code().equals(codePays) == false) {
					EODialogs.runInformationDialog("Attention", "Le pays de naissance ne correspond à pas à celui du numéro Insee");
				}
			}
		}
		if (matricule != null && peutModifierMatricule) {
			// On ne vérifie l'unicité du matricule que pour les créations ou pour le cas où le numéro de matricule est modifié
			if (modeCreation() || (currentIndividu().personnel().noMatricule() != null && currentIndividu().personnel().noMatricule().equals(matricule) == false)) {
				if (estMatriculeUnique(matricule) == false) {
					EODialogs.runErrorDialog("Erreur", "Ce numéro de matricule est déjà utilisé");
					return false;
				}
			}
		}

		if (modeCreation()) {

			String prenomPropre = StringCtrl.chaineClaire(currentIndividu().prenom(),false);
			if (currentIndividu().prenom().equals(prenomPropre) == false) {
				currentIndividu().setPrenom(prenomPropre.toUpperCase()); 
			}
			creerPersonnel(true);
			currentIndividu().setPersIdCreation(getCurrentAgent().toIndividu().persId());
			currentIndividu().setPersIdModification(getCurrentAgent().toIndividu().persId());

		} else {	

			// NUMEN / EPICEA
			currentIndividu().personnel().setNumen(null);
			currentIndividu().personnel().setNoEpicea(null);

			if (numenEpicea() != null) {
				setNumenEpicea(numenEpicea().toUpperCase());

				if (currentIndividu().personnel().estMESR()) {
					currentIndividu().personnel().setNumen(numenEpicea());
				}
				else
					if (currentIndividu().personnel().estMAAF()) {
						currentIndividu().personnel().setNoEpicea(numenEpicea());
					}				
			}

			// NO_MATRICULE
			String temp = currentIndividu().personnel().noMatricule();
			if ((matricule == null && temp != null) || (matricule != null && temp == null) || (matricule != null && temp != null && matricule.equals(temp) == false)) {
				currentIndividu().personnel().setNoMatricule(matricule);
			}			
			temp = currentIndividu().personnel().txtLibre();
			
			if ((texteLibre == null && temp != null) || (texteLibre != null && temp == null) || (texteLibre != null && temp != null && texteLibre.equals(temp) == false)) {
				currentIndividu().personnel().setTxtLibre(texteLibre);
			}

			currentIndividu().personnel().setToMinistereRelationship(getSelectedMinistere());
			currentIndividu().personnel().setToLogeRelationship(getSituationLogement());

			if (currentIndividu().persIdCreation() == null)
				currentIndividu().setPersIdCreation(getCurrentAgent().toIndividu().persId());

			currentIndividu().setPersIdModification(getCurrentAgent().toIndividu().persId());
		} 

		currentIndividu().personnel().setNpc(npc());

		return true;
	}

	/**
	 * 
	 */
	protected void traitementsApresValidation() {

		// Creation du compte ! 
		try {

			if (EOGrhumParametres.isCompteAuto()) {

				comptePourIndividu = EOCompte.creerComptePourIndividu(editingContext(), getCurrentAgent(), currentIndividu(),true);
				if (comptePourIndividu == null) {
					LogManager.logDetail("pas de creation");
				} 
				else {
					EOCompte.modifierComptePourIndividu(editingContext(), comptePourIndividu, currentIndividu());
				}
			}

			if (currentIndividu().personnel().noMatricule() == null) {
				preparerMatricule();
				currentIndividu().personnel().setNoMatricule(matricule());
			}

			majLimiteAge();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR", "Erreur de création du compte !");
		}
		setModificationEnCours(false);

		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,null);
		if (creationIndividu) {
			synchroniserAutresFenetres(currentIndividu());
		}
		remettreEtatSelection();
	}
	/** methode a surcharger pour faire des traitements apres un revert des donnees dans la base */
	protected void traitementsApresRevert() {
		super.traitementsApresRevert();
		raffraichirChamps();
		NSNotificationCenter.defaultCenter().postNotification(ListeAgents.CHANGER_EMPLOYE,individuCourantID());	// pour se repositionner sur le dernier employé sélectionné ou aucun
	}
	protected void nettoyer() {
		super.nettoyer();
		raffraichirChamps();
	}
	/** methode du document controller appelee en cas de probleme pendant le save */
	protected void saveFailed(Exception exception,boolean showErrorDialog,String saveOperationTitle) {
		if (exception.getClass().getName().equals("com.webobjects.foundation.NSValidation$ValidationException")) {
			LogManager.logInformation("Erreur lors du validateForSave " + exception.getMessage());
			afficherExceptionValidation(exception.getMessage());
		} else {
			LogManager.logException(exception);
			EODialogs.runErrorDialog("Erreur","Erreur lors de l'enregistrement des donnees !\nMESSAGE : " + exception.getMessage());
			annuler();
		}
	}
	protected void terminer() {
	}
	// méthodes privees
	private void ajouterRelation(NSNotification aNotif,String relation,boolean ajouterAIndividu) {
		// si ajouterAIndividu = true, on ajoute à l'individu sinon au personnel
		if (aNotif.object() != null) {
			EOGenericRecord objet = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			LogManager.logDetail("GestionEtatCivil - ajouterRelation : " + relation + " objet ajoute :" + objet);
			if (objet != null && currentIndividu() != null) {
				if (ajouterAIndividu) {
					currentIndividu().addObjectToBothSidesOfRelationshipWithKey(objet,relation);
				} else {
					if (currentIndividu().personnel() != null)	{
						currentIndividu().personnel().addObjectToBothSidesOfRelationshipWithKey(objet,relation);
					} 
					if (objet instanceof EOLimiteAge) {
						limiteAgeRetraite = (EOLimiteAge)objet;
					}
					if (modificationEnCours()) {
						setEdited(true);
					}
				}
				updaterDisplayGroups();
			}
		}
	}
	private boolean confirmerCreation() {
		EOIndividu individu = ChoixIndividu.sharedInstance().selectionnerIndividu(editingContext(),currentIndividu());	
		if (individu == null) {
			// l'utilisateur a annulé
			// mettre le prénom à null pour ne pas embrancher directement sur la création si on veut changer nom et prénom
			currentIndividu().setPrenom(null);
			return false;
		} else if (individu == currentIndividu()) {
			LogManager.logDetail("Création d'un individu après identification d'homonyme");
			// l'utilisateur maintient la création de l'individu ou l'individu n'existait pas dans la base
			component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			// créer les données de personnel

			currentIndividu().setNoIndividu(EOIndividu.construireNoIndividu(editingContext()));

			currentIndividu().setPersId(SuperFinder.construirePersId(editingContext()));

			//			if (!peutModifierMatricule) {
			//				preparerMatricule();
			//			}
			component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		} else {
			// changement d'individu, on veut réutiliser un individu existant
			LogManager.logDetail("Utilisation d'un individu existant");
			editingContext().revert();	// pour annuler la création précedente
			setCurrentIndividu(individu);
			displayGroup().setObjectArray(new NSArray(individu));
			displayGroup().setSelectedObject(individu);
			if (currentIndividu().toDepartement() != null) {
				setDepartementNaissance(currentIndividu().toDepartement().code());
			}
			if (currentIndividu().toPaysNaissance() != null) {
				setPaysNaissance(currentIndividu().toPaysNaissance().code(),true);
			}
			if (currentIndividu().personnel() == null) {	
				EODialogs.runInformationDialog("Attention","Cette personne ne fait pas partie du personnel. Vous devez valider les données pour l'enregistrer comme personnel");
			} 
			raffraichirChamps();
			if (!peutModifierMatricule && matricule == null) {
				preparerMatricule();
			}
			creerPersonnel(false);
			setModeCreation(false);
		}	
		return true;
	}
	
	/**
	 * 
	 */
	private void preparerNumeroInseePourAffichage() {
		// On utilise pas les accesseurs sur le numéro insee et la clé pour qu'ils ne soient
		// utilisés que par l'interface, ce qui veut dire édition
		numeroInsee = "";
		cleInsee = null;
		if (currentIndividu() != null) {
			if (currentIndividu().prendreEnCompteNumeroProvisoire() == false) {
				numeroInsee = currentIndividu().indNoInsee();
				cleInsee = currentIndividu().indCleInsee();
			} else {
				numeroInsee = currentIndividu().indNoInseeProv();
				cleInsee = currentIndividu().indCleInseeProv();
			}
		}
		ancienNumeroInsee = numeroInsee();
		CocktailUtilities.changerFontForLabel(labelSS, Font.BOLD);
	}

	/**
	 * 
	 */
	private void preparerNumeroInseeIndividuAvantValidation() {

		currentIndividu().setIndNoInseeProv(null);
		currentIndividu().setIndNoInsee(null);
		currentIndividu().setIndCleInseeProv(null);
		currentIndividu().setIndCleInsee(null);

		// No provisoire : le numero REEL est NULL
		if (currentIndividu().prendreEnCompteNumeroProvisoire()) {
			currentIndividu().setIndNoInseeProv(numeroInsee());
			if (cleInsee() != null) {
				currentIndividu().setIndCleInseeProv(new Integer(cleInsee()));
			}
		}  else { // No REEL => Provisoire ==> NULL
			currentIndividu().setIndNoInsee(numeroInsee());
			if (cleInsee() != null) {
				currentIndividu().setIndCleInsee(new Integer(cleInsee()));
			}
		}
	}
	// A REVOIR
	private void preparerInfosNaissance(String nouveauNumeroInsee) {
		// Remettre la clé insee à zéro si le numéro insee est changé
		if (nouveauNumeroInsee == null || nouveauNumeroInsee.equals("") || nouveauNumeroInsee.length() <  EOIndividu.LONGUEUR_NO_INSEE || 
				(ancienNumeroInsee != null && nouveauNumeroInsee.equals(ancienNumeroInsee) == false)) {
			setCleInsee(null);
		}

		if (currentIndividu().prendreEnCompteNumeroProvisoire() || (nouveauNumeroInsee != null && nouveauNumeroInsee.substring(1).equals(EOIndividu.NO_INSEE_PROVISOIRE))) {
			return;
		}
		// On change le numéro Insee qui n'est pas provisoire
		if (nouveauNumeroInsee == null || nouveauNumeroInsee.equals("") || nouveauNumeroInsee.length() <  EOIndividu.LONGUEUR_NO_INSEE) {
			LogManager.logDetail("GestionEtatCivil - Suppression/changement du  numero insee");
			supprimerInfosNaissance();
			return;
		}
		LogManager.logDetail("GestionEtatCivil preparerInfosNaissance avant preparation - paysNaissance : " + paysNaissance + " ,departement : " + departementNaissance + 
				" ,ville : " + currentIndividu().villeDeNaissance());

		// Vérifier si les informations pour les départements de naissance, ville et pays sont correctement saisis
		// d'abord par rapport à l'ancien numéro insee, puis par rapport aux valeurs qui seraient déjà saisies
		try {
			String ancienDepartement = "";
			if (ancienNumeroInsee != null && ancienNumeroInsee.length() >= 8) {
				ancienDepartement = ancienNumeroInsee.substring(5,7);
			}
			String nouveauDepartement = nouveauNumeroInsee.substring(5,7);
			String temp = nouveauNumeroInsee.substring(1,3);
			int anneeNaissance = new Integer(temp).intValue();
			anneeNaissance = EOIndividu.ANNEE_BASE_POUR_NAISSANCE + anneeNaissance;
			// Supprimer la date de naissance si elle est non nulle et ne correspond pas à l'année indiquée ou au mois indiqué
			temp = nouveauNumeroInsee.substring(3,5);
			int moisNaissance = new Integer(temp).intValue();
			if (currentIndividu().dNaissance() != null) {
				int annee = DateCtrl.getYear(currentIndividu().dNaissance());
				int mois = DateCtrl.getMonth(currentIndividu().dNaissance()) + 1;
				if (annee != anneeNaissance || mois != moisNaissance) {
					currentIndividu().setDNaissance(null);
				}
			}
			boolean infosNaissanceSupprimees = false;	// Pour ne pas annuler le calcul sur le département si on le change et qu'on doit changer le pays
			if (ancienDepartement.equals(nouveauDepartement) == false) {
				// Recherche du département
				if (nouveauDepartement.equals("99") == false && nouveauDepartement.equals("96") == false && 
						(anneeNaissance > 1962 || (nouveauDepartement.equals("91") == false && nouveauDepartement.equals("92") == false &&
						nouveauDepartement.equals("93") == false && nouveauDepartement.equals("94") == false && 
						nouveauDepartement.equals("95") == false))) {
					// personne née en France;
					LogManager.logDetail("GestionEtatCivil - departements differents pour une personne nee en France");
					if (nouveauDepartement.equals("97") || nouveauDepartement.equals("98")) {	// DOM - TOM
						nouveauDepartement = nouveauNumeroInsee.substring(5,8);		// le département est codé sur 3 caractères
					} else {
						nouveauDepartement = "0" + nouveauDepartement;		// les départements en France commencent par 0
					}
					// Comparer au departement de naissance original
					if (departementNaissance() == null || nouveauDepartement.equals(departementNaissance()) == false) {
						if (departementNaissance() != null) {
							supprimerInfosNaissance();
							infosNaissanceSupprimees = true;
						}
						EODepartement departement = (EODepartement)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), EODepartement.ENTITY_NAME, INomenclature.CODE_KEY,nouveauDepartement);
						if (departement != null) {
							currentIndividu().setToDepartementRelationship(departement);
							departementNaissance = departement.code();
						} else {
							departementNaissance = "";
						}
					}
				} else {
					// Personne non née en France
					LogManager.logDetail("GestionEtatCivil - departements differents pour une personne nee a l'etranger");
					if (departementNaissance() != null && departementNaissance().length() > 0) {
						// Il y avait un département sélectionné
						supprimerInfosNaissance();
						infosNaissanceSupprimees = true;
						departementNaissance = "";
					}
				} 
			}
			// Recherche du pays si département = 99 ou 96 ou année < 1962 && département = 91 à 95
			EOPays pays = null;
			if (nouveauDepartement.equals("99")) {
				if (nouveauNumeroInsee.length() >= 10) {
					String codePays = nouveauNumeroInsee.substring(7,10);
					pays = (EOPays)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), EOPays.ENTITY_NAME, INomenclature.CODE_KEY,codePays);
				}
			} else if (anneeNaissance <= 1962 && (nouveauDepartement.equals("91") || nouveauDepartement.equals("92") || 
					nouveauDepartement.equals("93") || nouveauDepartement.equals("94")) ||
					nouveauDepartement.equals("95") || nouveauDepartement.equals("96")) {
				if (nouveauDepartement.equals("91") || nouveauDepartement.equals("92") || 
						nouveauDepartement.equals("93") || nouveauDepartement.equals("94")) {
					// Algérie
					pays = (EOPays)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"Pays","code","352"); 
				} else if (nouveauDepartement.equals("95")) {
					// Maroc
					pays = (EOPays)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"Pays","code","350"); 
				} else if (nouveauDepartement.equals("96")) {
					// Tunisie
					pays = (EOPays)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"Pays","code","351");
				}
			} else {
				// Personne née en France
				pays = (EOPays)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"Pays","code",EOPays.CODE_PAYS_FRANCE);
			}
			// Vérifier si le pays de naissance est cohérent avec le pays choisi
			if (pays != null && paysNaissance() != null && pays.code().equals(paysNaissance()) == false && infosNaissanceSupprimees == false) {
				LogManager.logDetail("GestionEtatCivil - pays de naissance differents");
				supprimerInfosNaissance();	// Changement de pays
			}
			if (pays != null) {
				currentIndividu().setToPaysNaissanceRelationship(pays);
				paysNaissance = pays.code();
			} else {
				currentIndividu().setToPaysNaissanceRelationship(null);
				paysNaissance = "";
			}
			if (paysNaissance != null && paysNaissance.equals(EOPays.CODE_PAYS_FRANCE)) {
				// Rechercher la commune
				if (nouveauNumeroInsee.length() >= 10) {
					String codeVille = nouveauNumeroInsee.substring(5,10);
					String ancienCodeVille = "";
					if (ancienNumeroInsee != null && ancienNumeroInsee.length() >= 10) {
						ancienCodeVille = ancienNumeroInsee.substring(5,10);
					}
					if (codeVille.equals(ancienCodeVille) == false) {
						currentIndividu().setVilleDeNaissance(null);
						EOGenericRecord commune = null;
						if (codeVille.indexOf("20") == 0) {
							// DT 1816 - 10/09/09 Rechercher via le code Insee transformé en 2A ou 2B pour la Corse
							// Si on le trouve on force aussi le département de naissance
							nouveauDepartement = "2A";
							codeVille = nouveauDepartement + codeVille.substring(2);
							commune = (EOGenericRecord)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),EOCommune.ENTITY_NAME, INomenclature.CODE_KEY, codeVille);
							if (commune == null) {
								nouveauDepartement = "2B";
								codeVille = nouveauDepartement + codeVille.substring(2);
								commune = (EOGenericRecord)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), EOCommune.ENTITY_NAME, INomenclature.CODE_KEY, codeVille);
							}
							if (commune != null) {
								nouveauDepartement = "0" + nouveauDepartement;
								if (departementNaissance() == null || nouveauDepartement.equals(departementNaissance()) == false) {
									EODepartement departement = (EODepartement)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"Departement", INomenclature.CODE_KEY, nouveauDepartement);
									if (departement != null) {
										currentIndividu().setToDepartementRelationship(departement);
										departementNaissance = departement.code();
									} else {
										departementNaissance = "";
									}
								}
							}
						} else {
							commune = (EOGenericRecord)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),EOCommune.ENTITY_NAME, INomenclature.CODE_KEY, codeVille);
						}
						if (commune != null) {
							currentIndividu().setVilleDeNaissance((String)commune.valueForKey(INomenclature.LIBELLE_LONG_KEY));
						} else {
							currentIndividu().setVilleDeNaissance(null);
						}
					}
				}
			}
		} catch (RuntimeException e) {
			LogManager.logException(e);
		}

	}
	private void supprimerInfosNaissance() {

		currentIndividu().setVilleDeNaissance(null);
		currentIndividu().setToDepartementRelationship(null);
		currentIndividu().setToPaysNaissanceRelationship(null);

		paysNaissance = ""; 
		departementNaissance = "";
	}

	/**
	 * 
	 * @param estValidation
	 */
	private void creerPersonnel(boolean estValidation) {
		LogManager.logDetail("creerPersonnel");
		EOPersonnel personnel = currentIndividu().personnel();
		if (personnel == null) {	
			personnel = new EOPersonnel();
			personnel.initPersonnel(currentIndividu().noIndividu());
			personnel.setToIndividuRelationship(currentIndividu());
			personnel.setToMinistereRelationship(getSelectedMinistere());
			editingContext().insertObject(personnel);			
		}
		if (estValidation) {
			personnel.setToLogeRelationship(getSituationLogement());

			if (limiteAgeRetraite != null) {
				personnel.setLimiteAgeRelationship(limiteAgeRetraite);
			}
			if (numenEpicea() != null) {
				if (popupMinisteres.getSelectedIndex() > 0) {
					if (((EOMinisteres)popupMinisteres.getSelectedItem()).estMESR())
						personnel.setNumen(numenEpicea());
					else
						if (((EOMinisteres)popupMinisteres.getSelectedItem()).estMAAF())
							personnel.setNoEpicea(numenEpicea());

				}
			}
			if (matricule != null) {
				personnel.setNoMatricule(matricule);
			}
			if (texteLibre != null) {
				personnel.setTxtLibre(texteLibre);
			}
			// charger les infos sur les classes pour éviter les boucles ultérieures dans les appels au serveur
			EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName("IndividuIdentite");
			EOClassDescription.registerClassDescription(classDescription,EOIndividuIdentite.class);
		}
	}

	/**
	 * 
	 * @return
	 */
	public EOMinisteres getSelectedMinistere() {

		if (popupMinisteres.getSelectedIndex() > 0)
			return (EOMinisteres)popupMinisteres.getSelectedItem();

		return null;

	}

	/**
	 * 
	 * @return
	 */
	private boolean testSaisieValide() {

		// on fait ici les contrôles avant validateForSave car en cas de création, on ne veut pas avoir une erreur suite à la création
		// de personnel (=> décalage entre individu et personnel)
		if (npc != null && npc.length() > EOPersonnel.LG_NPC) {
			EODialogs.runErrorDialog("ERREUR","Le npc comporte au plus " + EOPersonnel.LG_NPC + " caractères !");
			return false;
		}

		if (currentIndividu().personnel() != null) {

			if (getSelectedMinistere() == null || getSelectedMinistere().estMESR()) {
				if (numenEpicea() != null && numenEpicea().length() != EOPersonnel.LG_NUMEN) {
					EODialogs.runErrorDialog("ERREUR","Le NUMEN comporte " + EOPersonnel.LG_NUMEN + " caractères !");
					return false;
				}
			}
			else {
				if (currentIndividu().personnel().estMAAF()) {
					if (numenEpicea() != null && numenEpicea().length() != EOPersonnel.LG_EPICEA) {
						EODialogs.runErrorDialog("ERREUR","Le n0 EPICEA comporte " + EOPersonnel.LG_EPICEA + " chiffres !");
						return false;
					}				
				}
			}
		}
		if (matricule != null && matricule.length() > EOPersonnel.LG_MATRICULE) {
			EODialogs.runErrorDialog("ERREUR","Le numéro de matricule comporte au plus " + EOPersonnel.LG_MATRICULE + " caractères !");
			return false;
		}
		if (texteLibre != null && texteLibre.length() > EOPersonnel.LG_TXT_LIBRE) {
			EODialogs.runErrorDialog("ERREUR","Le commentaire comporte au plus " + EOPersonnel.LG_TXT_LIBRE + " caractères !");
			return false;
		}

		return true;
	}

	/**
	 * 
	 */
	private void raffraichirChamps() {
		
		paysNaissance = "";
		departementNaissance = "";
		limiteAgeRetraite = null;

		setNumenEpicea(null); 		
		texteLibre = null; 
		setMatricule(null);
		setNpc(null);

		image = null;

		preparerNumeroInseePourAffichage();

		if (currentIndividu() != null) {
			if (currentIndividu().personnel() != null) {

				limiteAgeRetraite = currentIndividu().personnel().limiteAge();

				if (!currentIndividu().personnel().estMAAF()) {
					setNumenEpicea(currentIndividu().personnel().numen());
				}
				else
					if (currentIndividu().personnel().estMAAF()) {
						setNumenEpicea(currentIndividu().personnel().noEpicea());				
					}

				setNpc(currentIndividu().personnel().npc());
				setMatricule(currentIndividu().personnel().noMatricule());
				texteLibre = currentIndividu().personnel().txtLibre();
			}
			if (currentIndividu().toDepartement() != null) {
				departementNaissance = currentIndividu().toDepartement().code();
			}
			if (currentIndividu().toPaysNaissance() != null) {
				paysNaissance = currentIndividu().toPaysNaissance().code();
			}
			if (!modeCreation() && ((ApplicationClient)EOApplication.sharedApplication()).afficherPhoto()) {
				if (image == null && currentIndividu() != null && currentIndividu().noIndividu() != null) {
					// Parametre 
					if ( EOGrhumParametres.isPhotos() || (currentIndividu().peutAfficherPhoto()) ) {
						image = currentIndividu().image();
						if (grandePhoto != null && grandePhoto.isVisible()) {
							grandePhoto.repaint();
						}
					}
				}
			}

			int style = Font.PLAIN;
			if (currentIndividu().cCivilite() != null && currentIndividu().cCivilite().equals(ManGUEConstantes.MADAME)) {
				style = Font.BOLD;
			}
			CocktailUtilities.changerFontForLabel(labelNomPatronymique,style);
		}

		comptePourIndividu = null;
		updaterDisplayGroups();
		
	}

	/**
	 * 
	 * @param individu
	 */
	private void synchroniserAutresFenetres(EOIndividu individu) {
		try {
			NSMutableDictionary userInfo = new NSMutableDictionary();
			userInfo.setObjectForKey(individu.nomUsuel(),"nom");
			userInfo.setObjectForKey(individu.prenom(),"prenom");
			NSNotificationCenter.defaultCenter().postNotification(NOUVEL_EMPLOYE, null,userInfo);
		}
		catch (Exception e) {

		}
	}

	/**
	 * 
	 */
	private void preparerMatricule() {

		boolean estMatriculeUnique = false;
		while (!estMatriculeUnique) {

			Number numeroMatricule = null;
			String typeGestion = EOGrhumParametres.getValueParam(ManGUEConstantes.PARAM_KEY_NO_MATRICULE);

			if (typeGestion.equals(EOPersonnel.PARAM_MATRICULE_SEQUENCE)) {
				numeroMatricule = SuperFinder.numeroSequenceOracle(editingContext(), EOSeqMatriculePersonnel.ENTITY_NAME, false);			
			}
			else {
				if (typeGestion.equals(EOPersonnel.PARAM_MATRICULE_NO_INDIVIDU)) {
					numeroMatricule = currentIndividu().noIndividu();			
				}
				else {
					if (typeGestion.equals(EOPersonnel.PARAM_MATRICULE_ANNEE)) {
						EOPersonnel personnel = EOPersonnel.findLastMatriculeForYear(editingContext(), DateCtrl.getCurrentYear());
						if (personnel != null) {
							Integer sequence = new Integer(personnel.noMatricule().substring(4));
							numeroMatricule = 
									new Integer(String.valueOf( DateCtrl.getCurrentYear() + 
											StringCtrl.stringCompletion(String.valueOf(sequence.intValue() + 1), 4, "0", "G")));
						}
						else
							numeroMatricule = new Integer(String.valueOf(DateCtrl.getCurrentYear()) + "0001");
					}
					else {
						if (typeGestion.equals(EOPersonnel.PARAM_MATRICULE_MANUEL)) {
							return;
						}
					}
				}
			}

			if (numeroMatricule != null) {
				estMatriculeUnique = estMatriculeUnique(numeroMatricule.toString());
				if (estMatriculeUnique) {
					setMatricule(numeroMatricule.toString());
					updaterDisplayGroups();
				}
			}
		}
	}

	/**
	 * 
	 * @param numeroMatricule
	 * @return
	 */
	private boolean estMatriculeUnique(String numeroMatricule) {
		EOPersonnel personnel = EOPersonnel.rechercherPersonnelAvecMatricule(editingContext(),numeroMatricule);
		return (personnel == null || personnel == currentIndividu().personnel());
	}
	
	/**
	 * 
	 */
	private void verifierValiditePaysNaissance() {
		String cPays = paysNaissance;
		if (paysNaissance != null && paysNaissance.length() > 0) {
			NSTimestamp date = new NSTimestamp();
			if (currentIndividu().dNaissance() != null) {
				date = currentIndividu().dNaissance();
			}
			if (EOPays.estPaysValideADate(editingContext(),cPays,date) == false) {
				setPaysNaissance(null,false);
			}
		}
	}
}

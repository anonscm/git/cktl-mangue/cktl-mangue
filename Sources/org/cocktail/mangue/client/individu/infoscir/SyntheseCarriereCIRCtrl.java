package org.cocktail.mangue.client.individu.infoscir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.cir.SyntheseCarriereCIRView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.FicheSynthese;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.conges.EOCld;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAdoption;
import org.cocktail.mangue.modele.mangue.conges.EOCongeFormation;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaternite;
import org.cocktail.mangue.modele.mangue.conges.EOCongePaternite;
import org.cocktail.mangue.modele.mangue.conges.EOCongeSolidariteFamiliale;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

public class SyntheseCarriereCIRCtrl {

	private static SyntheseCarriereCIRCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private EODisplayGroup eodSynthese;
	private SyntheseCarriereCIRView myView;

	private ListenerSynthese listenerSynthese = new ListenerSynthese();

	private EOIndividu currentIndividu;
	private boolean peutGererModule;
	
	public SyntheseCarriereCIRCtrl(EOEditingContext edc) {

		this.edc = edc;

		eodSynthese = new EODisplayGroup();
		myView = new SyntheseCarriereCIRView(null, MODE_MODAL.booleanValue(),eodSynthese);

		myView.getBtnValiderCarriere().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {validerCarriereCIR();}}
		);
		myView.getBtnImprimerFiche().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
		);

		myView.getMyEOTable().addListener(listenerSynthese);

		myView.getTfDateCertification().addFocusListener(new FocusListenerDateTextField(myView.getTfDateCertification()));
		myView.getTfDateCertification().addActionListener(new ActionListenerDateTextField(myView.getTfDateCertification()));

		myView.getTfDateCompletude().addFocusListener(new FocusListenerDateTextField(myView.getTfDateCompletude()));
		myView.getTfDateCompletude().addActionListener(new ActionListenerDateTextField(myView.getTfDateCompletude()));

		myView.getTfDateVerification().addFocusListener(new FocusListenerDateTextField(myView.getTfDateVerification()));
		myView.getTfDateVerification().addActionListener(new ActionListenerDateTextField(myView.getTfDateVerification()));

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		CocktailUtilities.initTextField(myView.getTfDateCertification(), false, false);
		
		updateInterface();
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SyntheseCarriereCIRCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SyntheseCarriereCIRCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 * @return
	 */
	private boolean peutGererModule() {
		return peutGererModule;
	}
	
	/**
	 * 
	 * @param yn
	 */
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
			
		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererCarrieres());
				
		myView.getBtnValiderCarriere().setVisible(peutGererModule());
		myView.getBtnImprimerFiche().setVisible(peutGererModule());

	}

	
	private EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		updateDatas();
	}
	public JPanel getViewSyntheseCarriereCIR() {
		return myView.getViewSyntheseCarriereCIR();
	}

	public void employeHasChanged(NSNotification  notification) {
		if (notification != null && myView.isVisible()) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
		}
	}
	
	private void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfDateCompletude());
		CocktailUtilities.viderTextField(myView.getTfDateVerification());
		CocktailUtilities.viderTextField(myView.getTfDateCertification());
		eodSynthese.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
	}
	
	/**
	 * 
	 */
	public void updateDatas() {

		clearDatas();
		
		if (getCurrentIndividu() != null) {
			NSMutableArray arraySynthese = new NSMutableArray();

			// Recuperation de la periode de prise en charge

			NSTimestamp debutPeriode = null;
			NSTimestamp finPeriode = DateCtrl.finAnnee(new Integer(DateCtrl.getCurrentYear()));
			
			NSArray<EOPasse> passesEAS = EOPasse.rechercherPassesEASPourPeriode(edc, getCurrentIndividu(), null, null);
			NSArray<EOCarriere> carrieres = EOCarriere.findCarrieresCIRPourIndividu(edc, getCurrentIndividu());
			if (passesEAS.size() > 0) {
				debutPeriode = passesEAS.get(0).dateDebut();
			}
			else {
				if (carrieres.size() > 0) {
					debutPeriode = carrieres.get(0).dateDebut();
				}
			}
			
			// Ajout des periodes de carriere
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayCarrieres(edc,getCurrentIndividu(), finPeriode));

			// Ajout des services validés
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayPasseSyntheseCIR(edc,getCurrentIndividu(), finPeriode));

			// Ajout des services validés
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayServicesValides(edc,getCurrentIndividu(), finPeriode));

			// Integration des modalités de service
			if (passesEAS.size() > 0 || carrieres.size() > 0) {
				arraySynthese.addObjectsFromArray(FicheSynthese.getArrayModaliteServices(edc, getCurrentIndividu(), debutPeriode, finPeriode));
			}

			// Integration des conges legaux
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsencesCIR(edc, getCurrentIndividu(), debutPeriode , finPeriode, EOCongeMaternite.ENTITY_NAME , EOTypeAbsence.TYPE_CONGE_MATERNITE));
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsencesCIR(edc, getCurrentIndividu(), debutPeriode , finPeriode, EOCongePaternite.ENTITY_NAME , EOTypeAbsence.TYPE_CONGE_PATERNITE));
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsencesCIR(edc, getCurrentIndividu(), debutPeriode , finPeriode, EOClm.ENTITY_NAME , EOTypeAbsence.TYPE_CLM));
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsencesCIR(edc, getCurrentIndividu(), debutPeriode , finPeriode, EOCld.ENTITY_NAME , EOTypeAbsence.TYPE_CLD));
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsencesCIR(edc, getCurrentIndividu(), debutPeriode , finPeriode, EOCongeFormation.ENTITY_NAME , "CFP"));
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsencesCIR(edc, getCurrentIndividu(), debutPeriode , finPeriode, EOCongeAdoption.ENTITY_NAME , EOTypeAbsence.TYPE_CONGE_ADOPTION));
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsencesCIR(edc, getCurrentIndividu(), debutPeriode , finPeriode, EOCongeSolidariteFamiliale.ENTITY_NAME , "CSF"));

			// Service National
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayServiceNational(edc, getCurrentIndividu()));

			// Integration des bonifications/Etudes rachetees/Benefices d'etudes
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayBonifications(edc, getCurrentIndividu()));
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayBeneficesEtudes(edc, getCurrentIndividu()));
			arraySynthese.addObjectsFromArray(FicheSynthese.getArrayEtudesRachetees(edc, getCurrentIndividu()));

			eodSynthese.setObjectArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(arraySynthese, new NSArray(new EOSortOrdering(FicheSynthese.DATE_DEBUT_KEY, EOSortOrdering.CompareDescending))));
			myView.getMyEOTable().updateData();

			// Affichage de la date de certification, completude et verification
			
			CocktailUtilities.setDateToField(myView.getTfDateCertification(), getCurrentIndividu().personnel().cirDCertification());
			CocktailUtilities.setDateToField(myView.getTfDateVerification(), getCurrentIndividu().personnel().cirDVerification());
			CocktailUtilities.setDateToField(myView.getTfDateCompletude(), getCurrentIndividu().personnel().cirDCompletude());

			myView.getTfMessage().setText("");
			if (eodSynthese.displayedObjects().size() > 0 && getCurrentIndividu().personnel().cirDCertification() == null)
				myView.getTfMessage().setText("ATTENTION - L'identité de cet individu n'est pas certifiée !");

		}
		
		updateInterface();
	}
	
	
	/**
	 * 
	 */
	public void imprimer() {
				
		try {
			Class[] classeParametres =  new Class[] {EOGlobalID.class};
			NSMutableArray donneesRecherchees = new NSMutableArray();
			Object[] parametres = new Object[]{edc.globalIDForObject(getCurrentIndividu())};
			UtilitairesImpression.imprimerSansDialogue(edc,"clientSideRequestImprimerFicheCIR",classeParametres,parametres,"Fiche_CIR_" + getCurrentIndividu().identitePrenomFirst(),"Impression de la fiche CIR");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}


	/**
	 * 
	 */
	private void validerCarriereCIR() {
		try {

			if (myView.getTfDateCompletude().getText().length() == 0 
					&& myView.getTfDateVerification().getText().length() == 0
					&& myView.getTfDateCertification().getText().length() == 0) {				
				EODialogs.runErrorDialog("ERREUR", "Vous devez spécifier une date (Complétude, certification ou vérification) !");
				return;				
			}
			
			//Teste si la date de complétude est avant le 01/01/1900
			if (getDateCompletude() != null && DateCtrl.isBefore(getDateCompletude(), DateCtrl.stringToDate("01/01/1900"))) {			
				EODialogs.runErrorDialog("ERREUR", CocktailMessagesErreur.ERREUR_DATE_AVANT_1900);
				return;				
			}

			getCurrentIndividu().personnel().setCirDCertification(CocktailUtilities.getDateFromField(myView.getTfDateCertification()));
			getCurrentIndividu().personnel().setCirDCompletude(CocktailUtilities.getDateFromField(myView.getTfDateCompletude()));
			getCurrentIndividu().personnel().setCirDVerification(CocktailUtilities.getDateFromField(myView.getTfDateVerification()));
			
			edc.saveChanges();

			EODialogs.runInformationDialog("OK", "Les dates saisies ont bien été enregistrées !");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	private void updateInterface() {
		
		myView.getBtnValiderCarriere().setEnabled(getCurrentIndividu() != null && peutGererModule());
		myView.getBtnImprimerFiche().setEnabled(getCurrentIndividu() != null && peutGererModule());
		
		CocktailUtilities.initTextField(myView.getTfDateCompletude(), false, peutGererModule());
		CocktailUtilities.initTextField(myView.getTfDateVerification(), false, peutGererModule());
	}
	
	private NSTimestamp getDateCompletude()	{
		return CocktailUtilities.getDateFromField(myView.getTfDateCompletude());
	}

	private class ListenerSynthese implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
		}
	}


	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	

}

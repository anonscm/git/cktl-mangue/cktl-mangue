package org.cocktail.mangue.client.individu.infoscir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.BeneficeEtudesView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOEcolesMilitaires;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.cir.EOBeneficeEtudes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSValidation.ValidationException;

public class BeneficeEtudesCtrl {

	private static BeneficeEtudesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;
	private BeneficeEtudesView myView;

	private PopupEcoleListener listenerEcole = new PopupEcoleListener();
	private ListenerBenefices listenerBenefices = new ListenerBenefices();

	private boolean saisieEnabled, peutGererModule;
	private EODisplayGroup 		eod;
	private EOBeneficeEtudes 	currentBenefice;
	private EOIndividu 			currentIndividu;

	public BeneficeEtudesCtrl(EOEditingContext editingContext) {

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new BeneficeEtudesView(null, MODE_MODAL.booleanValue(),eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));

		myView.setListeEcoles(EOEcolesMilitaires.fetchAll(ec));
		myView.getPopupEcoles().addActionListener(listenerEcole);
		myView.getMyEOTable().addListener(listenerBenefices);

		setSaisieEnabled(false);
	}

	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
			
		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererCarrieres());
				
		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());

	}

	
	public EOIndividu currentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public EOBeneficeEtudes currentBenefice() {
		return currentBenefice;
	}
	public void setCurrentBenefice(EOBeneficeEtudes currentBenefice) {
		this.currentBenefice = currentBenefice;
		updateData();
	}
	public void actualiser() {
		eod.setObjectArray(EOBeneficeEtudes.findForIndividu(ec, currentIndividu));
		myView.getMyEOTable().updateData();
		updateUI();
	}
	public JFrame getView() {
		return myView;
	}
	public JPanel getViewBeneficeEtudes() {
		return myView.getViewBeneficeEtudes();
	}

	private void ajouter() {
		setCurrentBenefice(EOBeneficeEtudes.creer(ec, currentIndividu));		
		//clearTextFields();
		setSaisieEnabled(true);
	}

	private void modifier() {
		setSaisieEnabled(true);
	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer ce bénéfice d'études ?", "Oui", "Non"))		
			return;			

		try {
			currentBenefice.setTemValide("N");
			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			ec.revert();
		}
	}

	private void valider() {

		try {

			currentBenefice.setEcoleMilitaireRelationship((EOEcolesMilitaires)myView.getPopupEcoles().getSelectedItem());
			currentBenefice.setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			currentBenefice.setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			currentBenefice.setBetuDuree(CocktailUtilities.getTextFromField(myView.getTfDureeBonification()));

			ec.saveChanges();

			setSaisieEnabled(false);

			actualiser();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			myView.toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {
		
		ec.revert();
		listenerBenefices.onSelectionChanged();
		setSaisieEnabled(false);

	}

//	private void clearTextFields() {
//		myView.getPopupEcoles().setSelectedIndex(0);
//		myView.getTfDateDebut().setText("");
//		myView.getTfDateFin().setText("");
//		myView.getTfDureeBonification().setText("");
//
//	}

	private void updateData() {

		if (currentIndividu() != null && currentBenefice() != null) {
			myView.getPopupEcoles().setSelectedItem(currentBenefice.ecoleMilitaire());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentBenefice().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), currentBenefice().dateFin());
			CocktailUtilities.setTextToField(myView.getTfDureeBonification(), currentBenefice().betuDuree());
		}
		updateUI();
		
	}

	private class ListenerBenefices implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentBenefice((EOBeneficeEtudes)eod.selectedObject());
		}
	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosRetraiteCtrl.NOTIF_LOCK_INFOS_RETRAITE, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosRetraiteCtrl.NOTIF_UNLOCK_INFOS_RETRAITE, null);
	}


	private void updateUI() {

		myView.getMyEOTable().setEnabled(!saisieEnabled());
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getBtnAjouter().setEnabled(currentIndividu() != null && !saisieEnabled());
		myView.getBtnModifier().setEnabled(!saisieEnabled() && currentBenefice() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && currentBenefice() != null);

		myView.getPopupEcoles().setEnabled(saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDureeBonification(), false, saisieEnabled());

	}

	private class PopupEcoleListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){

			if (currentBenefice() != null) {
				if (myView.getPopupEcoles().getSelectedIndex() > 0) {
					currentBenefice().setEcoleMilitaireRelationship((EOEcolesMilitaires)myView.getPopupEcoles().getSelectedItem());
				}
				updateUI();
			}
		}
	}

	

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}

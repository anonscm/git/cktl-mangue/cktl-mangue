// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.individu.infoscir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.individu.SaisieBonifCongesView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl.IntRef;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.mangue.cir.EOBonifications;
import org.cocktail.mangue.modele.mangue.cir.EOBonificationsConges;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieBonifCongesCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieBonifCongesCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieBonifCongesView myView;
	
	private EOBonificationsConges currentConge;

	public SaisieBonifCongesCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieBonifCongesView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));

	}

	public static SaisieBonifCongesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieBonifCongesCtrl(editingContext);
		return sharedInstance;
	}

	public EOBonificationsConges getCurrentConge() {
		return currentConge;
	}

	public void setCurrentConge(EOBonificationsConges currentConge) {
		this.currentConge = currentConge;
	}

	private void clearTextFields()	{

		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
		myView.getTfDuree().setText("");

	}

	public EOBonificationsConges ajouter(EOBonifications bonification)	{
		clearTextFields();
		currentConge = EOBonificationsConges.creer(ec, bonification);
		myView.setVisible(true);
		return currentConge;
	}

	
	private void valider()  {

		try {			

			currentConge.setDateDebut(getDateDebut());
			currentConge.setDateFin(getDateFin());

			if (!StringCtrl.chaineVide(myView.getTfDuree().getText()))
				currentConge.setBonicDuree(myView.getTfDuree().getText());
			else
				currentConge.setBonicDuree(null);
				
			myView.setVisible(false);
			
			ec.saveChanges();

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	private NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}

	private void annuler() {
		ec.revert();
		ServerProxy.clientSideRequestRevert(ec);
		currentConge = null;
		myView.setVisible(false);
	}

	private void calculDuree() {		
		myView.getTfDuree().setText("");
		if (getDateDebut() != null && getDateFin() != null) {
			IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
			DateCtrl.joursMoisAnneesEntre(getDateDebut(), getDateFin(), anneeRef, moisRef, jourRef, true);
			int nbJours = (anneeRef.value * 360) + (moisRef.value * 30) + jourRef.value; 
			myView.getTfDuree().setText(String.valueOf(nbJours));
		}
	}
	
	private class ActionListenerDateTextField implements ActionListener	{
		
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myTextField.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else {
				myTextField.setText(myDate);
				calculDuree();
			}
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else {
				myTextField.setText(myDate);
				calculDuree();
			}
		}
	}	



}

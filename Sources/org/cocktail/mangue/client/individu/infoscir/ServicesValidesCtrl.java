package org.cocktail.mangue.client.individu.infoscir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.individu.ServicesValidesView;
import org.cocktail.mangue.client.select.AvenantSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.manager.Manager;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeService;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOValidationServices;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class ServicesValidesCtrl extends ModelePageGestion{

	private static final int INDEX_FONCTION_PUBLIQUE = 0;
	private ServicesValidesView myView;

	private ListenerPopupTypeFonctionPublique listenerFonctionPublique = new ListenerPopupTypeFonctionPublique();
	private ListenerTempsTravail listenerTempsTravail = new ListenerTempsTravail();
	private ListenerValidations listenerValidations = new ListenerValidations();

	private boolean 				peutGererModule;
	private EODisplayGroup 			eod;

	private EOValidationServices 	currentValidation;
	private EOIndividu 				currentIndividu;
	private EOContratAvenant		currentAvenant;
	private Manager manager;

	public ServicesValidesCtrl(Manager manager) {

		super(manager.getEdc());
		this.manager = manager;
		
		eod = new EODisplayGroup();
		myView = new ServicesValidesView(null, false, eod);

		myView.getMyEOTable().addListener(listenerValidations);
		
		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());
		setDateListeners(myView.getTfDateValidation());
		
		myView.getBtnGetAvenant().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getAvenant();}}
		);

		myView.getCheckTempsComplet().addActionListener(listenerTempsTravail);
		myView.getCheckTempsIncomplet().addActionListener(listenerTempsTravail);
		myView.getCheckTempsPartiel().addActionListener(listenerTempsTravail);

		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeService(), NomenclatureFinder.findForCodes(getEdc(), EOTypeService.ENTITY_NAME, new NSArray<String>(EOTypeService.TYPE_SERVICE_VALIDES)), false);
		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeFonctionPublique(), 
				manager.getNomenclatureFinder().findStatic(getEdc(), EOTypeFonctionPublique.ENTITY_NAME), false);
				
		myView.getPopupTypeFonctionPublique().addActionListener(listenerFonctionPublique);

		setSaisieEnabled(false);
		
	}

	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
			
		setPeutGererModule(utilisateur.peutAfficherCarrieres() && ((ApplicationClient)ApplicationClient.sharedApplication()).isUseServicesValides());
				
		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());
		myView.getBtnGetAvenant().setVisible(peutGererModule());

	}

	public EOIndividu currentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public EOValidationServices getCurrentValidation() {
		return currentValidation;
	}
	public void setCurrentValidation(EOValidationServices currentValidation) {
		this.currentValidation = currentValidation;
		updateDatas();
	}
	public EOContratAvenant currentAvenant() {
		return currentAvenant;
	}
	public void setCurrentAvenant(EOContratAvenant currentAvenant) {
		this.currentAvenant = currentAvenant;
	}

	public void actualiser() {
		eod.setObjectArray(EOValidationServices.findForIndividu(getEdc(), currentIndividu));
		myView.getMyEOTable().updateData();
		updateInterface();
	}

	public JPanel getViewValidations() {
		return myView.getViewValidations();
	}
	public void toFront() {
		myView.toFront();
	}

	/**
	 * 
	 */
	public void getAvenant() {
		
		EOContratAvenant avenant = AvenantSelectCtrl.sharedInstance(getEdc()).getAvenant(currentIndividu());
		
		if (avenant != null) {
			setCurrentAvenant(avenant);
			ajouter();
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentAvenant().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), currentAvenant().dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), currentAvenant().dateFin());
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public EOTypeFonctionPublique getSelectedFonctionPublique() {
		return (EOTypeFonctionPublique)myView.getPopupTypeFonctionPublique().getSelectedItem();
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerValidations implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentValidation() != null && peutGererModule())
				modifier();
		}
		public void onSelectionChanged() {
			clearDatas();			
			setCurrentValidation((EOValidationServices)eod.selectedObject());
			if (getCurrentValidation() != null)
				updateDatas();

			updateInterface();
		}
	}

	private boolean estTempsPartiel() {
		return  myView.getCheckTempsPartiel().isSelected();
	}

	private boolean estFonctionPublique() {
		return myView.getPopupTypeFonctionPublique().getSelectedIndex() == INDEX_FONCTION_PUBLIQUE;
	}

	/**
	 * 
	 */

	private class ListenerTempsTravail implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			if (myView.getCheckTempsComplet().isSelected() || myView.getCheckTempsIncomplet().isSelected())
				myView.getTfQuotiteService().setText("100");
			updateInterface();
		}
	}

	private class ListenerPopupTypeFonctionPublique implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			myView.getTfMinistere().setEnabled(myView.getPopupTypeFonctionPublique().isEnabled() 
					&& myView.getPopupTypeFonctionPublique().getSelectedIndex() == INDEX_FONCTION_PUBLIQUE);		
			updateInterface();
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getPopupTypeFonctionPublique().setSelectedIndex(0);
		myView.getPopupTypeService().setSelectedIndex(0);

		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfDateValidation());
		CocktailUtilities.viderTextField(myView.getTfAnnees());
		CocktailUtilities.viderTextField(myView.getTfMois());
		CocktailUtilities.viderTextField(myView.getTfJours());
		CocktailUtilities.viderTextField(myView.getTfQuotiteService());
		CocktailUtilities.viderTextField(myView.getTfEtablissement());
		CocktailUtilities.viderTextField(myView.getTfMinistere());

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (currentIndividu() != null && getCurrentValidation() != null) {
			
			myView.getPopupTypeFonctionPublique().setSelectedItem(getCurrentValidation().toTypeFonctionPublique());
			myView.getPopupTypeService().setSelectedItem(getCurrentValidation().toTypeService());

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentValidation().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentValidation().dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateValidation(), getCurrentValidation().dateValidation());
			CocktailUtilities.setNumberToField(myView.getTfQuotiteService(), getCurrentValidation().valQuotite());
			CocktailUtilities.setTextToField(myView.getTfEtablissement(), getCurrentValidation().valEtablissement());
			CocktailUtilities.setTextToField(myView.getTfMinistere(), getCurrentValidation().valMinistere());

			myView.getCheckTempsComplet().setSelected(getCurrentValidation().estTempsComplet());
			myView.getCheckTempsIncomplet().setSelected(getCurrentValidation().estTempsIncomplet());
			myView.getCheckTempsPartiel().setSelected(getCurrentValidation().estTempsPartiel());
			myView.getCheckPcAcquitee().setSelected(getCurrentValidation().pcAcquittees());

			
			CocktailUtilities.setNumberToField(myView.getTfAnnees(), getCurrentValidation().valAnnees());
			CocktailUtilities.setNumberToField(myView.getTfMois(), getCurrentValidation().valMois());
			CocktailUtilities.setNumberToField(myView.getTfJours(), getCurrentValidation().valJours());

		}
		updateInterface();
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && currentIndividu() != null);
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentValidation() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentValidation() != null);

		myView.getPopupTypeFonctionPublique().setEnabled(isSaisieEnabled());
		myView.getPopupTypeService().setEnabled(isSaisieEnabled());

		myView.getCheckPcAcquitee().setEnabled(isSaisieEnabled());
		myView.getCheckTempsComplet().setEnabled(isSaisieEnabled());
		myView.getCheckTempsIncomplet().setEnabled(isSaisieEnabled());
		myView.getCheckTempsPartiel().setEnabled(isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfMinistere(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfEtablissement(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfQuotiteService(), false, isSaisieEnabled() && getCurrentValidation() != null && estTempsPartiel());
		CocktailUtilities.initTextField(myView.getTfAnnees(), false, isSaisieEnabled() && getCurrentValidation() != null );
		CocktailUtilities.initTextField(myView.getTfMois(), false, isSaisieEnabled() && getCurrentValidation() != null );
		CocktailUtilities.initTextField(myView.getTfJours(), false, isSaisieEnabled() && getCurrentValidation() != null );
		CocktailUtilities.initTextField(myView.getTfDateValidation(), false, isSaisieEnabled() && getCurrentValidation() != null );

	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
		
		// TODO Auto-generated method stub
		getCurrentValidation().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentValidation().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
		getCurrentValidation().setDateValidation(CocktailUtilities.getDateFromField(myView.getTfDateValidation()));
		getCurrentValidation().setToTypeFonctionPubliqueRelationship(getSelectedFonctionPublique());
		
		getCurrentValidation().setValEtablissement(CocktailUtilities.getTextFromField(myView.getTfEtablissement()));
		getCurrentValidation().setValMinistere(CocktailUtilities.getTextFromField(myView.getTfMinistere()));
		
		if (myView.getCheckTempsComplet().isSelected())
			getCurrentValidation().setTempsComplet();
		if (myView.getCheckTempsIncomplet().isSelected())
			getCurrentValidation().setTempsIncomplet();
		if (myView.getCheckTempsPartiel().isSelected())
			getCurrentValidation().setTempsPartiel();

		getCurrentValidation().setValQuotite(CocktailUtilities.getBigDecimalFromField(myView.getTfQuotiteService()));
		getCurrentValidation().setPcAquitees(myView.getCheckPcAcquitee().isSelected());			

		// Enregistrement de la duree validee
		getCurrentValidation().setValAnnees(CocktailUtilities.getIntegerFromField(myView.getTfAnnees()));
		getCurrentValidation().setValMois(CocktailUtilities.getIntegerFromField(myView.getTfMois()));
		getCurrentValidation().setValJours(CocktailUtilities.getIntegerFromField(myView.getTfJours()));				

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentValidation(null);
		listenerValidations.onSelectionChanged();
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		updateInterface();
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentValidation(EOValidationServices.creer(getEdc(), currentIndividu));
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getCurrentValidation().setEstValide(false);
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}	

}

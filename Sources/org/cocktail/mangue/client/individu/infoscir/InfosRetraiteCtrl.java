package org.cocktail.mangue.client.individu.infoscir;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.WindowConstants;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.individu.InfosRetraiteView;
import org.cocktail.mangue.common.modele.manager.Manager;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.server.cir.FichierCir;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class InfosRetraiteCtrl {

	public static final String NOTIF_LOCK_INFOS_RETRAITE = "NotifLockInfosRetraite";
	public static final String NOTIF_UNLOCK_INFOS_RETRAITE = "NotifUnockInfosRetraite";
	
	public static final String LAYOUT_BONIFICATIONS = "BONIFICATIONS";
	public static final String LAYOUT_BENEFICES = "BENEFICES ETUDES";
	public static final String LAYOUT_ETUDES = "ETUDES RACHETEES";
	public static final String LAYOUT_SYNTHESE = "SYNTHESE CIR";
	public static final String LAYOUT_FICHIER_CIR = "FICHIER CIR";
	public static final String LAYOUT_VALIDATION = "VALIDATIONS";

	private static InfosRetraiteCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.TRUE;

	private InfosRetraiteView 	myView;
	private EOIndividu 			currentIndividu;

	private SyntheseListener syntheseListener = new SyntheseListener();
	private ValidationListener validationsListener = new ValidationListener();
	private BonificationsListener bonificationsListener = new BonificationsListener();
	private BeneficesListener beneficesListener = new BeneficesListener();
	private EtudesListener etudesListener = new EtudesListener();
	private FichierListener fichierListener = new FichierListener();
	
	private boolean isLocked;

	private SyntheseCarriereCIRCtrl syntheseCtrl = null;
	private ServicesValidesCtrl validationsCtrl = null;
	private BonificationsCtrl bonificationsCtrl = null;
	private BeneficeEtudesCtrl beneficesCtrl = null;
	private EtudesRacheteesCtrl etudesCtrl = null;
	private SequenceCIRCtrl fichierCtrl = null;

	private Manager manager;

	public InfosRetraiteCtrl(Manager manager) {
	
		this.manager = manager;
		
		myView = new InfosRetraiteView(null, MODE_MODAL.booleanValue());

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("unlock",new Class[] {NSNotification.class}), NOTIF_UNLOCK_INFOS_RETRAITE,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("lock",new Class[] {NSNotification.class}), NOTIF_LOCK_INFOS_RETRAITE,null);

		syntheseCtrl = new SyntheseCarriereCIRCtrl(getEdc());
		validationsCtrl = new ServicesValidesCtrl(manager);
		bonificationsCtrl = new BonificationsCtrl(getEdc());
		beneficesCtrl = new BeneficeEtudesCtrl(getEdc());
		etudesCtrl = new EtudesRacheteesCtrl(getEdc());
		fichierCtrl = new SequenceCIRCtrl(manager);

		myView.getBtnFermer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {myView.dispose();}}
		);

		myView.getSwapView().add(LAYOUT_SYNTHESE,syntheseCtrl.getViewSyntheseCarriereCIR());
		myView.getSwapView().add(LAYOUT_VALIDATION,validationsCtrl.getViewValidations());
		myView.getSwapView().add(LAYOUT_BONIFICATIONS,bonificationsCtrl.getViewBonifications());
		myView.getSwapView().add(LAYOUT_BENEFICES,beneficesCtrl.getViewBeneficeEtudes());
		myView.getSwapView().add(LAYOUT_ETUDES,etudesCtrl.getViewEtudesRachetees());
		myView.getSwapView().add(LAYOUT_FICHIER_CIR,fichierCtrl.getViewSSequenceCIR());

		myView.getCheckSynthese().setSelected(true);
		myView.getCheckSynthese().addActionListener(syntheseListener);
		myView.getCheckFichier().addActionListener(fichierListener);
		myView.getCheckValidations().addActionListener(validationsListener);
		myView.getCheckBonifications().addActionListener(bonificationsListener);
		myView.getCheckBeneficesEtudes().addActionListener(beneficesListener);
		myView.getCheckEtudesRachetees().addActionListener(etudesListener);
		
		syntheseListener.actionPerformed(null);
		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

	}

	public static InfosRetraiteCtrl sharedInstance()	{
		if(sharedInstance == null) {
			sharedInstance = new InfosRetraiteCtrl(((ApplicationClient)ApplicationClient.sharedApplication()).getManager());
		}
		return sharedInstance;
	}
	
	public EOEditingContext getEdc() {
		return manager.getEdc();
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
			
		syntheseCtrl.setDroitsGestion(currentUtilisateur);
		bonificationsCtrl.setDroitsGestion(currentUtilisateur);
		etudesCtrl.setDroitsGestion(currentUtilisateur);
		beneficesCtrl.setDroitsGestion(currentUtilisateur);
		validationsCtrl.setDroitsGestion(currentUtilisateur);

	}

	
	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		if (getCurrentIndividu() != null)
			myView.setTitle(currentIndividu.identitePrenomFirst() + " - INFORMATIONS RETRAITE");
		else
			myView.setTitle("INFORMATIONS RETRAITE");
		actualiser();
	}
	public void open(EOIndividu individu)	{
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		setCurrentIndividu(individu);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}
	public void open(EOIndividu individu, String layout)	{

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		setCurrentIndividu(individu);
		
		CRICursor.setWaitCursor(myView);
		if (layout.equals(LAYOUT_SYNTHESE)) {myView.getCheckSynthese().setSelected(true);syntheseListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_FICHIER_CIR)) {myView.getCheckFichier().setSelected(true);fichierListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_VALIDATION)) {myView.getCheckValidations().setSelected(true);validationsListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_BONIFICATIONS))  {myView.getCheckBonifications().setSelected(true);bonificationsListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_BENEFICES)) {myView.getCheckBeneficesEtudes().setSelected(true);beneficesListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_ETUDES)) {myView.getCheckEtudesRachetees().setSelected(true);etudesListener.actionPerformed(null);}
		CRICursor.setDefaultCursor(myView);
		
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
		
	}

	public void employeHasChanged(NSNotification  notification) {
		if (notification != null && myView.isVisible()) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(getEdc(),(Number)notification.object(), false));
		}
	}
	public void setEnabled(boolean yn) {
	}
	public void lock(NSNotification notif) {
		setIsLocked(true);
	}
	public void unlock(NSNotification notif) {
		setIsLocked(false);
	}
	public boolean isLocked() {
		return isLocked;
	}
	public void setIsLocked(boolean yn) {
		isLocked = yn;
		updateInterface();
	}
	private void updateInterface() {
		
		myView.getCheckSynthese().setEnabled(!isLocked());
		myView.getCheckValidations().setEnabled(!isLocked() 
				&& ((ApplicationClient)ApplicationClient.sharedApplication()).isUseServicesValides());
		myView.getCheckBonifications().setEnabled(!isLocked());
		myView.getCheckBeneficesEtudes().setEnabled(!isLocked());
		myView.getCheckEtudesRachetees().setEnabled(!isLocked());
		myView.getCheckFichier().setEnabled(!isLocked());
		
		myView.getBtnFermer().setEnabled(!isLocked());

		if (!isLocked())
			myView.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		else
			myView.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	}
	
	public void actualiser() {
		
		if (myView.getCheckSynthese().isSelected())
			syntheseCtrl.setCurrentIndividu(getCurrentIndividu());			
		if (myView.getCheckFichier().isSelected())
			fichierCtrl.setCurrentIndividu(getCurrentIndividu());			
		if (myView.getCheckValidations().isSelected())
			validationsCtrl.setCurrentIndividu(getCurrentIndividu());
		if (myView.getCheckBonifications().isSelected())
			bonificationsCtrl.setCurrentIndividu(getCurrentIndividu());			
		if (myView.getCheckBeneficesEtudes().isSelected())
			beneficesCtrl.setCurrentIndividu(getCurrentIndividu());			
		if (myView.getCheckEtudesRachetees().isSelected())
			etudesCtrl.setCurrentIndividu(getCurrentIndividu());			
	}
	
	public JDialog getView() {
		return myView;
	}
	public void toFront() {
		myView.toFront();
	}
	public void nettoyerChamps(NSNotification notification) {
		setCurrentIndividu(null);
	}
	private void swapViewHasChanged(JRadioButton radioButton, String layout) {
		CRICursor.setWaitCursor(myView);
		myView.getTfTitre().setText(layout);
		myView.getTfTitre().setBackground(radioButton.getBackground());
		((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), layout);				
		actualiser();
		CRICursor.setDefaultCursor(myView);
	}

	private class SyntheseListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckSynthese(), LAYOUT_SYNTHESE);
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), LAYOUT_SYNTHESE);				
		}
	}
	private class ValidationListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckValidations(), LAYOUT_VALIDATION);
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), LAYOUT_VALIDATION);				
		}
	}
	private class BonificationsListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckBonifications(), LAYOUT_BONIFICATIONS);
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), LAYOUT_BONIFICATIONS);				
		}
	}
	private class BeneficesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckBeneficesEtudes(), LAYOUT_BENEFICES);
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), LAYOUT_BENEFICES);				
		}
	}
	private class EtudesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckEtudesRachetees(), LAYOUT_ETUDES);
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), LAYOUT_ETUDES);				
		}
	}
	private class FichierListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckFichier(), LAYOUT_FICHIER_CIR);
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), LAYOUT_FICHIER_CIR);				
		}
	}


}

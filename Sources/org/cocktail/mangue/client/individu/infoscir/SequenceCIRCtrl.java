package org.cocktail.mangue.client.individu.infoscir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ConsoleTraitementCtrl;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.cir.SequenceCIRView;
import org.cocktail.mangue.common.modele.manager.Manager;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriere;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriereData;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class SequenceCIRCtrl {

	private static Boolean MODE_MODAL = Boolean.FALSE;

	private EODisplayGroup eodRubriques;
	private EODisplayGroup eodSousRubriques;
	private SequenceCIRView myView;

	private ListenerRubriques listenerRubriques = new ListenerRubriques();
	private ListenerSousRubriques listenerSousRubriques = new ListenerSousRubriques();
	private boolean 	traitementServeurEnCours, preparationEnCours = false;

	private EOCirCarriere currentCarriereCir;
	
	private EOIndividu currentIndividu;
	private Manager manager;
	private ConsoleTraitementCtrl 		myConsole;

	public SequenceCIRCtrl(Manager manager) {

		this.manager = manager;

		eodRubriques = new EODisplayGroup();
		eodSousRubriques = new EODisplayGroup();
		myView = new SequenceCIRView(null, MODE_MODAL.booleanValue(), eodRubriques, eodSousRubriques);

		myConsole = new ConsoleTraitementCtrl(getEdc());
		myConsole.setCtrlParent(this, "Traitement des données CIR ...");

		myView.getBtnCalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {recalculer();}	}
		);

		myView.getMyEOTableRubriques().addListener(listenerRubriques);
		myView.getMyEOTableSousRubriques().addListener(listenerSousRubriques);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(getEdc(), ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));
		
		CocktailUtilities.initTextArea(myView.getConsole(),false, false);
		updateInterface();
	}
	
	private EOEditingContext getEdc() {
		return manager.getEdc();
	}
	
	private EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		updateDatas();
	}
	public JPanel getViewSSequenceCIR() {
		return myView.getViewSequenceCIR();
	}

	public EOCirCarriere getCurrentCarriereCir() {
		return currentCarriereCir;
	}

	public void setCurrentCarriereCir(EOCirCarriere currentCarriereCir) {
		this.currentCarriereCir = currentCarriereCir;
	}

	public void employeHasChanged(NSNotification  notification) {
		if (notification != null && myView.isVisible()) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(getEdc(),(Number)notification.object(), false));
		}
	}
	
	private void clearDatas() {

	}

	/**
	 * 
	 * @param nomMethode
	 * @param individus
	 * @param estPreparation
	 */
	private void lancerTraitementServeur(String nomMethode, NSArray<EOIndividuIdentite> individus, boolean estPreparation) {

		boolean estRapportDetaille = true;

		setTraitementServeurEnCours(true);

		Class classeParametres[];
		Object parametres[];
		if(estPreparation) {
			classeParametres = (new Class[] {EOGlobalID.class, NSArray.class, Boolean.class});
			parametres = (new Object[] {
					getEdc().globalIDForObject(getCurrentCarriereCir().cirFichierImport()), 
					Utilitaires.tableauDeGlobalIDs(individus, getEdc()), 
					new Boolean(estRapportDetaille)
			});
		} else		// preparation du fichier
		{
			classeParametres = (new Class[] {
					EOGlobalID.class, NSArray.class
			});
			parametres = (new Object[] {
					getEdc().globalIDForObject(getCurrentCarriereCir().cirFichierImport()), Utilitaires.tableauDeGlobalIDs(individus, getEdc())
			});
		}

		myConsole.lancerTraitement(nomMethode, classeParametres, parametres);

	}


	/**
	 * 
	 */
	private void recalculer() {
		
			CRICursor.setWaitCursor(myView);
			try {
				preparationEnCours = true;
				lancerTraitementServeur("clientSideRequestPreparerRecordsCir",  new NSArray(getCurrentIndividu()), true);
			}
			catch(Exception ex) {
				getEdc().revert();
				ex.printStackTrace();
			}
			CRICursor.setDefaultCursor(myView);
		}

	public boolean isTraitementServeurEnCours() {
		return traitementServeurEnCours;
	}

	public void setTraitementServeurEnCours(boolean traitementServeurEnCours) {
		this.traitementServeurEnCours = traitementServeurEnCours;
	}

	public boolean isPreparationEnCours() {
		return preparationEnCours;
	}

	public void setPreparationEnCours(boolean preparationEnCours) {
		this.preparationEnCours = preparationEnCours;
	}
	/**
	 * 
	 */
	public void terminerTraitementServeur() {		

		try {
			setTraitementServeurEnCours(false);
			setPreparationEnCours(false);
			myConsole.addToMessage("\n >> TRAITEMENT TERMINE !");
			updateDatas();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}


	
	/**
	 * 
	 */
	public void updateDatas() {

		clearDatas();
		
		if (getCurrentIndividu() != null) {
			
			setCurrentCarriereCir(EOCirCarriere.carrierePourFichierEtIndividu(getEdc(), null, getCurrentIndividu()));
			if (getCurrentCarriereCir() != null) {

				CocktailUtilities.setTextToField(myView.getTfInfosFichier(), "Séquence CIR année " + getCurrentCarriereCir().cirFichierImport().cfimAnnee());
				CocktailUtilities.setTextToArea(myView.getConsole(), getCurrentCarriereCir().circCommentaires());

				NSArray<EOCirCarriereData> datas = EOCirCarriereData.recordsPourCirCarriere(getEdc(), getCurrentCarriereCir());
				
				NSMutableArray nomRubriques = new NSMutableArray();
				for(EOCirCarriereData data : datas) {

					if(!nomRubriques.containsObject(data.circdRubrique())) {
						NSMutableDictionary parametres = new NSMutableDictionary();
						parametres.setObjectForKey(data.circdClassement(), EOCirCarriereData.CIRCD_CLASSEMENT_KEY);
						parametres.setObjectForKey(data.circdRubrique(), EOCirCarriereData.CIRCD_RUBRIQUE_KEY);
						parametres.setObjectForKey(data.circdData(), EOCirCarriereData.CIRCD_DATA_KEY);
						nomRubriques.addObject(parametres);
					}
				}

				eodRubriques.setObjectArray(nomRubriques);
				myView.getMyEOTableRubriques().updateData();

			}
			
			
		}
		
		updateInterface();
	}

	/**
	 * 
	 */
	private void updateInterface() {

	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerRubriques implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick()	{
		}

		public void onSelectionChanged() {
			try {
				NSDictionary dico = (NSDictionary)eodRubriques.selectedObject();
				if(dico != null) {
					eodSousRubriques.setObjectArray(ServerProxy.clientSideRequestGetSousRubriques(getEdc(), (String)dico.objectForKey(EOCirCarriereData.CIRCD_RUBRIQUE_KEY), (String)dico.objectForKey(EOCirCarriereData.CIRCD_DATA_KEY)));
					myView.getMyEOTableSousRubriques().updateData();
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerSousRubriques implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick()	{
		}

		public void onSelectionChanged() {
			try	{
				NSDictionary dico = (NSDictionary)eodSousRubriques.selectedObject();
			}
			catch(Exception e)	{
				e.printStackTrace();
			}
		}
	}
}

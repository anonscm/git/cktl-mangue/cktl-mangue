package org.cocktail.mangue.client.individu.infoscir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.EtudesRacheteesView;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.NumberCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.cir.EOEtudesRachetees;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class EtudesRacheteesCtrl {

	private static EtudesRacheteesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;
	private EtudesRacheteesView myView;

	private ListenerEtudes listenerEtudes = new ListenerEtudes();

	private boolean saisieEnabled, peutGererModule;
	private EODisplayGroup eod;
	private EOEtudesRachetees currentEtude;
	private EOIndividu currentIndividu;

	public EtudesRacheteesCtrl(EOEditingContext editingContext) {

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new EtudesRacheteesView(null, MODE_MODAL.booleanValue(),eod);

		myView.getMyEOTable().addListener(listenerEtudes);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateRachat().addFocusListener(new FocusListenerDateTextField(myView.getTfDateRachat()));
		myView.getTfDateRachat().addActionListener(new ActionListenerDateTextField(myView.getTfDateRachat()));

		setSaisieEnabled(false);
	}

	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
			
		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererCarrieres());
				
		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());

	}


	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public EOEtudesRachetees currentEtude() {
		return currentEtude;
	}

	public void setCurrentEtude(EOEtudesRachetees currentEtude) {
		this.currentEtude = currentEtude;
		updateData();
	}

	public void open(EOIndividu individu)	{
		clearTextFields();
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}
	public void actualiser() {
		eod.setObjectArray(EOEtudesRachetees.findForIndividu(ec, currentIndividu));
		myView.getMyEOTable().updateData();
		updateUI();
	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewEtudesRachetees() {
		return myView.getViewEtudesRachetees();
	}
	public void toFront() {
		myView.toFront();
	}

	private void ajouter() {
		setCurrentEtude(EOEtudesRachetees.creer(ec, currentIndividu));
		clearTextFields();
		setSaisieEnabled(true);
	}

	private void modifier() {
		setSaisieEnabled(true);
	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette période d'études rachetées ?", "Oui", "Non"))		
			return;			

		try {
			currentEtude().setTemValide(CocktailConstantes.FAUX);
			ec.saveChanges();
			actualiser();
			toFront();
		}
		catch (Exception e) {
			ec.revert();
		}
	}


	public NSTimestamp getDateDebut() {
		try {
			return DateCtrl.stringToDate(myView.getTfDateDebut().getText());
		}
		catch (Exception e) {
			return null;
		}
	}
	public void setDateDebut(NSTimestamp myDate) {
		CocktailUtilities.setDateToField(myView.getTfDateDebut(), myDate);
	}
	public NSTimestamp getDateFin() {
		try {
			return DateCtrl.stringToDate(myView.getTfDateFin().getText());
		}
		catch (Exception e) {
			return null;
		}
	}
	public void setDateFin(NSTimestamp myDate) {
		CocktailUtilities.setDateToField(myView.getTfDateFin(), myDate);
	}
	public NSTimestamp getDateRachat() {
		try {
			return DateCtrl.stringToDate(myView.getTfDateRachat().getText());
		}
		catch (Exception e) {
			return null;
		}
	}
	public void setDateRachat(NSTimestamp myDate) {
		CocktailUtilities.setDateToField(myView.getTfDateRachat(), myDate);
	}


	/**
	 * 
	 */
	private void valider() {

		try {

			currentEtude.setDateDebut(getDateDebut());
			currentEtude.setDateFin(getDateFin());
			currentEtude.setDateRachat(getDateRachat());

			currentEtude.setEstPcAcquittees(myView.getCheckPcAcquitees().isSelected());

			if (myView.getTfDureeLiquidable().getText().length() > 0) {
				if (!NumberCtrl.isANumber(myView.getTfDureeLiquidable().getText()))
					throw new ValidationException("La durée liquidable doit être saisie en jours !");
				currentEtude.setErDureeLiquidable(new Integer(myView.getTfDureeLiquidable().getText()));
			}
			else
				currentEtude.setErDureeLiquidable(null);

			if (myView.getTfDureeConstitutive().getText().length() > 0) {
				if (!NumberCtrl.isANumber(myView.getTfDureeConstitutive().getText()))
					throw new ValidationException("La durée constitutive doit être saisie en jours !");
				currentEtude.setErDureeConstitutive(new Integer(myView.getTfDureeConstitutive().getText()));
			}
			else
				currentEtude.setErDureeConstitutive(null);

			if (myView.getTfDureeAssurance().getText().length() > 0) {
				if (!NumberCtrl.isANumber(myView.getTfDureeAssurance().getText()))
					throw new ValidationException("La durée d'assurance doit être saisie en jours !");
				currentEtude.setErDureeAssurance(new Integer(myView.getTfDureeAssurance().getText()));
			}
			else
				currentEtude.setErDureeAssurance(null);

			ec.saveChanges();
			setSaisieEnabled(false);
			actualiser();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			myView.toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void annuler() {
		ec.revert();
		listenerEtudes.onSelectionChanged();
		setSaisieEnabled(false);
	}
	private void clearTextFields() {
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
		myView.getTfDateRachat().setText("");
		myView.getTfDureeLiquidable().setText("");
		myView.getTfDureeConstitutive().setText("");
		myView.getTfDureeAssurance().setText("");
	}
	private void updateData() {

		clearTextFields();
		if (currentIndividu() != null && currentEtude() != null) {
			setDateDebut(currentEtude.dateDebut());
			setDateFin(currentEtude.dateFin());
			setDateRachat(currentEtude.dateRachat());

			if (currentEtude.erDureeLiquidable() != null)
				myView.getTfDureeLiquidable().setText(currentEtude.erDureeLiquidable().toString());

			if (currentEtude.erDureeConstitutive() != null)
				myView.getTfDureeConstitutive().setText(currentEtude.erDureeConstitutive().toString());

			if (currentEtude.erDureeAssurance() != null)
				myView.getTfDureeAssurance().setText(currentEtude.erDureeAssurance().toString());

			myView.getCheckPcAcquitees().setSelected(currentEtude().estPcAcquittees());
		}

		updateUI();
		
	}

	private class ListenerEtudes implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentEtude() != null && peutGererModule())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentEtude((EOEtudesRachetees)eod.selectedObject());
		}
	}

	private boolean saisieEnabled() {
		return saisieEnabled && currentIndividu != null;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosRetraiteCtrl.NOTIF_LOCK_INFOS_RETRAITE, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosRetraiteCtrl.NOTIF_UNLOCK_INFOS_RETRAITE, null);
	}

	private void updateUI() {

		myView.getMyEOTable().setEnabled(!saisieEnabled());
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getBtnAjouter().setEnabled(!saisieEnabled());
		myView.getBtnModifier().setEnabled(!saisieEnabled() && currentEtude() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && currentEtude() != null);

		myView.getCheckPcAcquitees().setEnabled(saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateRachat(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDureeLiquidable(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDureeConstitutive(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDureeAssurance(), false, saisieEnabled());

	}



	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	

}

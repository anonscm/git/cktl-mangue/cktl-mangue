package org.cocktail.mangue.client.individu.infoscir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.individu.BonificationsView;
import org.cocktail.mangue.client.select.TerritoireSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.cir.EONatureBonifTaux;
import org.cocktail.mangue.common.modele.nomenclatures.cir.EONatureBonifTerritoire;
import org.cocktail.mangue.common.modele.nomenclatures.cir.EONatureBonification;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl.IntRef;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.cir.EOBonifications;
import org.cocktail.mangue.modele.mangue.cir.EOBonificationsConges;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class BonificationsCtrl {

	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;
	private BonificationsView myView;

	private PopupTypeBonificationListener listenerTypeBonification = new PopupTypeBonificationListener();
	private PopupTauxListener listenerTaux = new PopupTauxListener();

	private ListenerBonifications listenerBonifications = new ListenerBonifications();
	private ListenerConges listenerConges = new ListenerConges();

	private boolean saisieEnabled, peutGererModule;
	private EODisplayGroup eod, eodConges;

	private EOBonifications 		currentBonification;
	private EONatureBonification 	currentNatureBonification;
	private EOBonificationsConges 	currentConge;
	private EOIndividu 				currentIndividu;
	private EONatureBonifTerritoire currentTerritoire;


	public BonificationsCtrl(EOEditingContext editingContext) {

		ec = editingContext;

		eod = new EODisplayGroup();
		eodConges = new EODisplayGroup();
		myView = new BonificationsView(null, MODE_MODAL.booleanValue(),eod, eodConges);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		myView.getBtnAjouterConge().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterConge();}}
		);
		myView.getBtnSupprimerConge().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerConge();}}
		);

		myView.getBtnGetTerritoire().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectTerritoire();}}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateArrivee().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArrivee()));
		myView.getTfDateArrivee().addActionListener(new ActionListenerDateTextField(myView.getTfDateArrivee()));

		myView.getMyEOTable().addListener(new ListenerBonifications());
		myView.getMyEOTableConges().addListener(listenerConges);

		myView.setListeTypesBonifications(EONatureBonification.fetchAll(ec));

		myView.getPopupTypes().addActionListener(listenerTypeBonification);
		myView.getPopupTaux().addActionListener(listenerTaux);

		myView.setListeTaux(EONatureBonifTaux.fetchAll(ec, EONatureBonifTaux.SORT_ARRAY_TAUX_ASC));
		myView.getPopupTaux().setEnabled(false);
		CocktailUtilities.initTextField(myView.getTfTerritoire(), false, false);
		
		myView.getBtnAjouterConge().setEnabled(false);
		myView.getBtnSupprimerConge().setEnabled(false);

		setSaisieEnabled(false);
	}

	
	
	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererCarrieres());

		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnAjouterConge().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());

	}



	public EOBonifications getCurrentBonification() {
		return currentBonification;
	}

	public void setCurrentBonification(EOBonifications currentBonification) {
		this.currentBonification = currentBonification;
		updateDatas();
	}

	public EONatureBonification getCurrentNatureBonification() {
		return currentNatureBonification;
	}

	public void setCurrentNatureBonification(
			EONatureBonification currentNatureBonification) {
		this.currentNatureBonification = currentNatureBonification;
		updateInterface();
	}

	public EOBonificationsConges getCurrentConge() {
		return currentConge;
	}

	public void setCurrentConge(EOBonificationsConges currentConge) {
		this.currentConge = currentConge;
		updateInterface();
	}

	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EONatureBonifTerritoire getCurrentTerritoire() {
		return currentTerritoire;
	}
	public void setCurrentTerritoire(EONatureBonifTerritoire currentTerritoire) {
		this.currentTerritoire = currentTerritoire;
		CocktailUtilities.setTextToField(myView.getTfTerritoire(), "");
		myView.getPopupTaux().setSelectedIndex(0);
		if (currentTerritoire != null) {
			CocktailUtilities.setTextToField(myView.getTfTerritoire(), currentTerritoire.nbotLibelle());
			myView.getPopupTaux().setSelectedItem(currentTerritoire.toTaux());
		}
	}



	public void actualiser() {
		eod.setObjectArray(EOBonifications.findForIndividu(ec, currentIndividu));
		myView.getMyEOTable().updateData();
		updateInterface();
	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewBonifications() {
		return myView.getViewBonifications();
	}
	public void toFront() {
		InfosRetraiteCtrl.sharedInstance().toFront();
	}

	private void selectTerritoire() {
		EONatureBonifTerritoire territoire = TerritoireSelectCtrl.sharedInstance(ec).getTerritoire();
		if (territoire != null)
			setCurrentTerritoire(territoire);
	}
	
	private void ajouter() {
		setCurrentBonification(EOBonifications.creer(ec, currentIndividu()));
		updateDatas();
		setSaisieEnabled(true);
	}

	private void ajouterConge() {

		SaisieBonifCongesCtrl.sharedInstance(ec).ajouter(getCurrentBonification());
		listenerBonifications.onSelectionChanged();

		try {
			calculDuree();
			getCurrentBonification().setBoniDuree(CocktailUtilities.getTextFromField(myView.getTfDuree()));
			ec.saveChanges();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void modifier() {
		setSaisieEnabled(true);
	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette bonification ?", "Oui", "Non"))		
			return;			

		try {
			currentBonification.setTemValide("N");
			ec.saveChanges();
			actualiser();
			toFront();
		}
		catch (Exception e) {
			ec.revert();
		}
	}

	private void supprimerConge() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette période de congé ?", "Oui", "Non"))		
			return;			

		try {
			currentConge.setTemValide(CocktailConstantes.FAUX);
			eodConges.deleteSelection();
			calculDuree();
			getCurrentBonification().setBoniDuree(CocktailUtilities.getTextFromField(myView.getTfDuree()));
			ec.saveChanges();
			listenerBonifications.onSelectionChanged();
		}
		catch (Exception e) {
			ec.revert();
		}
	}


	private NSTimestamp getDateDebut() {
		if (StringCtrl.chaineVide(myView.getTfDateDebut().getText()))
			return null;
		return DateCtrl.stringToDate(myView.getTfDateDebut().getText());
	}
	private NSTimestamp getDateFin() {
		if (StringCtrl.chaineVide(myView.getTfDateFin().getText()))
			return null;
		return DateCtrl.stringToDate(myView.getTfDateFin().getText());
	}
	private NSTimestamp getDateArrivee() {
		if (StringCtrl.chaineVide(myView.getTfDateArrivee().getText()))
			return null;
		return DateCtrl.stringToDate(myView.getTfDateArrivee().getText());
	}


	/**
	 * 
	 * @return
	 */
	private String calculDuree() {

		String duree = "";

		if (getDateDebut() != null && getDateFin() != null) {

			IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
			if (getDateArrivee() == null) {
				DateCtrl.joursMoisAnneesEntre(getDateDebut(), getDateFin(), anneeRef, moisRef, jourRef, true);
			}
			else {
				DateCtrl.joursMoisAnneesEntre(getDateArrivee(), getDateFin(), anneeRef, moisRef, jourRef, true);
			}

			int nbJours = (anneeRef.value*360) + (moisRef.value*30) + jourRef.value;

			nbJours = nbJours - joursDeConges();
			if (myView.getPopupTaux().getSelectedIndex() > 0) {
				EONatureBonifTaux taux = (EONatureBonifTaux)myView.getPopupTaux().getSelectedItem();
				nbJours = nbJours / taux.nbtDenTaux().intValue();								
			}

			int nbAnnees = nbJours / 360;
			nbJours = nbJours - (nbAnnees * 360);
			int nbMois = nbJours / 30;
			nbJours = nbJours - (nbMois * 30);

			// Traduction en annee/mois/jours
			myView.getTfDuree().setText(DateCtrl.formaterDuree(nbAnnees,  nbMois, nbJours));

		}

		return duree;
	}


	/**
	 * Calcul de la duree des periodes de conges. Transformation au format AAMMJJ
	 */
	private int joursDeConges() {

		int nbJours = 0;
		for (Enumeration< EOBonificationsConges> e = eodConges.displayedObjects().objectEnumerator();e.hasMoreElements();) {
			EOBonificationsConges bonif = (EOBonificationsConges)e.nextElement();			
			if (bonif.bonicDuree() != null)	
				nbJours = nbJours + new Integer(bonif.bonicDuree()).intValue();
		}

		return nbJours;

	}


	private void valider() {

		try {

			currentBonification.setIndividuRelationship(currentIndividu());
			currentBonification.setNatureBonificationRelationship(currentNatureBonification);
			currentBonification.setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			currentBonification.setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			currentBonification.setDateArrivee(CocktailUtilities.getDateFromField(myView.getTfDateArrivee()));

			currentBonification.setBoniDuree(CocktailUtilities.getTextFromField(myView.getTfDuree()));

			currentBonification.setTerritoireRelationship(getCurrentTerritoire());

			ec.saveChanges();
			setSaisieEnabled(false);

			actualiser();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			myView.toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {

		ec.revert();
		ServerProxy.clientSideRequestRevert(ec);

		listenerBonifications.onSelectionChanged();
		setSaisieEnabled(false);
	}

	/**
	 * 
	 */
	private void clearDatas() {

		myView.getPopupTypes().setSelectedIndex(0);
		myView.getTfDuree().setText("");
		myView.getPopupTaux().setSelectedIndex(0);
		
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfDateArrivee());

		
		setCurrentTerritoire(null);
		eodConges.setObjectArray(new NSArray());
		myView.getMyEOTableConges().updateData();

	}

	/**
	 * 
	 */
	private void updateDatas() {
		clearDatas();

		if (getCurrentBonification() != null && currentIndividu() != null) {

			myView.getPopupTypes().setSelectedItem(currentBonification.natureBonification());

			setCurrentTerritoire(getCurrentBonification().territoire());
			
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentBonification.dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), currentBonification.dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateArrivee(), currentBonification.dateArrivee());
			CocktailUtilities.setTextToField(myView.getTfDuree(), currentBonification.boniDuree());
			myView.getPopupTaux().removeActionListener(listenerTaux);
			
			if (currentBonification.territoire() != null)
				myView.getPopupTaux().setSelectedItem(currentBonification.territoire().toTaux());
			myView.getPopupTaux().addActionListener(listenerTaux);

			eodConges.setObjectArray(EOBonificationsConges.getForBonification(ec, currentBonification));
			myView.getMyEOTableConges().updateData();
		}
		updateInterface();
	}

	private class ListenerBonifications implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentBonification() != null && peutGererModule())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentBonification((EOBonifications)eod.selectedObject());
		}
	}
	private class ListenerConges implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentConge((EOBonificationsConges)eodConges.selectedObject());
		}
	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosRetraiteCtrl.NOTIF_LOCK_INFOS_RETRAITE, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosRetraiteCtrl.NOTIF_UNLOCK_INFOS_RETRAITE, null);
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getMyEOTable().setEnabled(!saisieEnabled());
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getBtnAjouter().setEnabled(!saisieEnabled() && currentIndividu() != null);
		myView.getBtnModifier().setEnabled(!saisieEnabled() && getCurrentBonification() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && getCurrentBonification() != null);

		myView.getBtnAjouterConge().setEnabled(!saisieEnabled() && getCurrentBonification() != null && getCurrentNatureBonification() != null && getCurrentNatureBonification().estHorsEurope());
		myView.getBtnSupprimerConge().setEnabled(!saisieEnabled() && getCurrentBonification() != null && currentConge != null);

		myView.getPopupTypes().setEnabled(saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDuree(), false, false);//saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateArrivee(), false, saisieEnabled());

		myView.getBtnGetTerritoire().setEnabled(
				saisieEnabled() 
				&& getCurrentNatureBonification() != null 
				&& getCurrentNatureBonification().estHorsEurope());

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupTypeBonificationListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){

			setCurrentNatureBonification(null);
			if (getCurrentBonification() != null) {

				if (myView.getPopupTypes().getSelectedIndex() > 0) {
					setCurrentNatureBonification((EONatureBonification)myView.getPopupTypes().getSelectedItem());
					if (getCurrentNatureBonification().estHorsEurope() == false)
						setCurrentTerritoire(null);
				}

				updateInterface();
			}
		}
	}

	private class PopupTauxListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			calculDuree();
		}
	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			calculDuree();
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	

}

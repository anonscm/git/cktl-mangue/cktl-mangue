package org.cocktail.mangue.client.individu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.common.utilities.StringCtrl;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.individu.FicheSituationAgentView;
import org.cocktail.mangue.client.individu.infoscir.InfosRetraiteCtrl;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.mangue.client.individu.infosperso.InfosPersonnellesCtrl;
import org.cocktail.mangue.client.select.ServiceGestionnaireSelectCtrl;
import org.cocktail.mangue.client.situation.AffectationOccupationCtrl;
import org.cocktail.mangue.client.specialisations.SpecialisationCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonneTelephone;
import org.cocktail.mangue.modele.grhum.referentiel.EOPhotosEmployes;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOGestionnaires;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOArrivee;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

public class FicheSituationAgentCtrl {

	private static FicheSituationAgentCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private FicheSituationAgentView myView;
	public static String CHANGER_EMPLOYE = "NotifEmployeHasChanged";

	private SpecialisationCtrl myCtrlSpec;

	private EOIndividu 				currentIndividu;
	private EOAgentPersonnel		currentUtilisateur;
	private EOArrivee				currentArrivee;
	private EODepart				currentDepart;
	private EOSituationFamiliale	currentSituationFamiliale;
	private EOAffectation			currentAffectation;
	private EOOccupation			currentOccupation;
	private EOAdresse				currentAdresse;
	private EOGestionnaires			currentGestionnaire;	
	private EOPersonneTelephone		currentTelephone;
	private JFileChooser 			fileChooser;		
	private	File					fichierPhoto;

	public FicheSituationAgentCtrl(EOEditingContext editingContext) {

		edc = editingContext;

		myView = new FicheSituationAgentView(null, MODE_MODAL.booleanValue());
		myCtrlSpec = new SpecialisationCtrl(edc);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		myView.getBtnArriveeDepart().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {					
				InfosComplementairesCtrl.sharedInstance(edc).open(getCurrentIndividu(), InfosComplementairesCtrl.LAYOUT_ARRIVEE);
				updateDatas();
			}}
				);
		myView.getBtnTelecharger().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {telecharger();}}
				);
		myView.getBtnSelectGestionnaire().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectServiceGestionnaire();}}
				);
		myView.getBtnAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				CRICursor.setWaitCursor(myView);
				AffectationOccupationCtrl.sharedInstance(edc).open(getCurrentIndividu());
				CRICursor.setDefaultCursor(myView);
			}}
				);

		myView.getBtnInfosPersonnelles().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				CRICursor.setWaitCursor(myView);
				InfosPersonnellesCtrl.sharedInstance(edc).open(getCurrentIndividu());
				CRICursor.setDefaultCursor(myView);
			}}
				);
		myView.getBtnInfosComplémentaires().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				CRICursor.setWaitCursor(myView);
				InfosComplementairesCtrl.sharedInstance(edc).open(getCurrentIndividu());
				CRICursor.setDefaultCursor(myView);
			}}
				);
		myView.getBtnInfosRetraite().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				CRICursor.setWaitCursor(myView);
				InfosRetraiteCtrl.sharedInstance().open(getCurrentIndividu());
				CRICursor.setDefaultCursor(myView);
			}}
				);

		// Creation d'un fileChooser pour choisir les fichiers et recuperer les icones		
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		CocktailUtilities.initTextField(myView.getTfArrivee(), false, false);
		CocktailUtilities.initTextField(myView.getTfDepart(), false, false);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		
		myView.getLblGestionnaire().setVisible(EOGrhumParametres.isUseGestionnaire());
		myView.getBtnSelectGestionnaire().setVisible(EOGrhumParametres.isUseGestionnaire());
		
		myView.getTemSauvadet().addActionListener(new ListenerSauvadet());
		
		clearTextFields();
		updateInterface();
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {

		this.currentUtilisateur = currentUtilisateur;

		myView.getBtnInfosPersonnelles().setEnabled(
				currentUtilisateur != null && currentUtilisateur.peutAfficherInfosPerso() 
				&& (currentUtilisateur.peutAfficherAgents() || currentUtilisateur.peutGererAgents())
				);
		myView.getBtnInfosComplémentaires().setEnabled(
				currentUtilisateur != null && currentUtilisateur.peutAfficherInfosPerso() 
				&& (currentUtilisateur.peutAfficherAgents() || currentUtilisateur.peutGererAgents())
				);
		myView.getBtnInfosRetraite().setEnabled(currentUtilisateur.peutGererCarrieres());

	}
	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}

	public static FicheSituationAgentCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new FicheSituationAgentCtrl(editingContext);
		return sharedInstance;
	}
	private EOEditingContext getEdc() {
		return edc;
	}	
	public void employeHasChanged(NSNotification  notification) {
		clearTextFields();
		if (notification != null && notification.object() != null && myView.isVisible()) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
		}
	}

	private EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		updateDatas();
	}

	public EOGestionnaires getCurrentGestionnaire() {
		return currentGestionnaire;
	}

	public void setCurrentGestionnaire(EOGestionnaires currentGestionnaire) {
		this.currentGestionnaire = currentGestionnaire;
		CocktailUtilities.setTextToLabel(myView.getLblGestionnaire(), "Service RH gestionnaire : ");
		if (currentGestionnaire != null && currentGestionnaire.toStructure() != null) {
			CocktailUtilities.setTextToLabel(myView.getLblGestionnaire(), "Service RH gestionnaire : " + currentGestionnaire.toStructure().llStructure());
		}
	}

	public EOArrivee currentArrivee() {
		return currentArrivee;
	}

	public void setCurrentArrivee(EOArrivee currentArrivee) {
		this.currentArrivee = currentArrivee;
		CocktailUtilities.setTextToField(myView.getTfArrivee(), "");
		if (currentArrivee != null) {
			String stringArrivee = "";
			stringArrivee += DateCtrl.dateToString(currentArrivee.dateDebut());			
			if (currentArrivee.typeAcces() != null)
				stringArrivee += " - " + currentArrivee().typeAcces().libelleLong();
			CocktailUtilities.setTextToField(myView.getTfArrivee(), stringArrivee);			
		}
	}


	public EOSituationFamiliale currentSituationFamiliale() {
		return currentSituationFamiliale;
	}


	public void setCurrentSituationFamiliale(
			EOSituationFamiliale currentSituationFamiliale) {
		this.currentSituationFamiliale = currentSituationFamiliale;
		CocktailUtilities.setTextToLabel(myView.getLblSituationFamiliale(), "");
		if (currentSituationFamiliale != null) {
			CocktailUtilities.setTextToLabel(myView.getLblSituationFamiliale(), currentSituationFamiliale.libelleLong());
		}
	}




	public EOAdresse currentAdresse() {
		return currentAdresse;
	}
	public void setCurrentAdresse(EOAdresse currentAdresse) {
		this.currentAdresse = currentAdresse;
		CocktailUtilities.setTextToLabel(myView.getLblAdresse(), "");
		if (currentAdresse != null) {
			CocktailUtilities.setTextToLabel(myView.getLblAdresse(), currentAdresse.adresseComplete());
		}
	}


	public EOPersonneTelephone currentTelephone() {
		return currentTelephone;
	}
	public void setCurrentTelephone(EOPersonneTelephone currentTelephone) {
		this.currentTelephone = currentTelephone;
		CocktailUtilities.setTextToLabel(myView.getLblTelephone(), "Tél : ");
		if (currentTelephone != null) {
			CocktailUtilities.setTextToLabel(myView.getLblTelephone(),"Tél : " + currentTelephone.noTelephoneAffichage() + " (" + currentTelephone.toTypeTel().code() + ")");
		}
	}


	public EODepart currentDepart() {
		return currentDepart;
	}
	public void setCurrentDepart(EODepart currentDepart) {
		this.currentDepart = currentDepart;
		CocktailUtilities.setTextToField(myView.getTfDepart(), "");
		if (currentDepart != null) {
			String stringDepart = DateCtrl.dateToString(currentDepart.dateDebut());			
			stringDepart +=  " - " + currentDepart().motifDepart().libelleCourt();			
			CocktailUtilities.setTextToField(myView.getTfDepart(), stringDepart);			
		}
	}


	public EOAffectation currentAffectation() {
		return currentAffectation;
	}


	public void setCurrentAffectation(EOAffectation currentAffectation) {
		this.currentAffectation = currentAffectation;
		CocktailUtilities.setTextToLabel(myView.getLblAffectation(), "Pas d'affectation ...");
		if (currentAffectation != null) {
			String texte = "Affectation : " + StringCtrl.capitalizedString(currentAffectation.toStructureUlr().llStructure()) + "  (" + DateCtrl.dateToString(currentAffectation.dateDebut()) + " - " + DateCtrl.dateToString(currentAffectation.dateFin()) + ")";
			CocktailUtilities.setTextToLabel(myView.getLblAffectation(), texte);			
		}
	}

	public EOOccupation getCurrentOccupation() {
		return currentOccupation;
	}
	
	/**
	 * 
	 * @param currentOccupation
	 */
	public void setCurrentOccupation(EOOccupation currentOccupation) {

		try {
			this.currentOccupation = currentOccupation;
			CocktailUtilities.setTextToLabel(myView.getLblOccupation(), " Pas d'occupation d'emploi ...");
			if (currentOccupation != null) {
				String texte = "Occupation : " + currentOccupation.toEmploi().getNoEmploi() + "  (" + DateCtrl.dateToString(currentOccupation.dateDebut()) + " - " + DateCtrl.dateToString(currentOccupation.dateFin()) + ")";;
				CocktailUtilities.setTextToLabel(myView.getLblOccupation(), texte);			
			}
		}
		catch (Exception e) {
		}
	}


	public JFrame getView() {
		return myView;
	}
	public JPanel getViewFiche() {
		return myView.getViewFicheAgent();
	}
	public void open(EOIndividu individu) {
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}
	public void nettoyerChamps(NSNotification notification) {
	}

	/**
	 * 
	 */
	private void clearTextFields() {

		CocktailUtilities.setTextToLabel(myView.getLblSecu(), "");
		CocktailUtilities.setTextToLabel(myView.getTfIdentite(), "");
		CocktailUtilities.setTextToLabel(myView.getLblAge(), "");
		CocktailUtilities.setTextToLabel(myView.getLblSituationFamiliale(), "");
		CocktailUtilities.setTextToLabel(myView.getLblAdresse(), "");
		CocktailUtilities.setTextToField(myView.getTfArrivee(), "");
		CocktailUtilities.setTextToField(myView.getTfDepart(), "");

		CocktailUtilities.setTextToLabel(myView.getLblType(), "");
		CocktailUtilities.setTextToLabel(myView.getLblDetail(), "");
		CocktailUtilities.setTextToLabel(myView.getLblPeriode(), "");
		CocktailUtilities.setTextToLabel(myView.getLblGrade(), "");
		CocktailUtilities.setTextToLabel(myView.getLblAffectation(), "");
		CocktailUtilities.setTextToLabel(myView.getLblOccupation(), "");

	}


	public NSData imageAgent() {
		try {
			if (getCurrentIndividu() != null)
				return currentIndividu.image();
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 */
	public void telecharger() {

		try {

			if (fileChooser.showOpenDialog(myView) == JFileChooser.APPROVE_OPTION)	{

				fichierPhoto = fileChooser.getSelectedFile();

				NSData res = null;
				final ImageIcon imgIcon = new ImageIcon(fichierPhoto.getAbsolutePath());
				if (imgIcon != null) {
					try {
						FileInputStream stream = new FileInputStream(fichierPhoto);
						int size = stream.available();
						res = new NSData(stream, size);
					} catch (Exception e) {
						e.printStackTrace();
					}

					EOPhotosEmployes photoEmploye = EOPhotosEmployes.findForIndividu(edc, getCurrentIndividu());
					if (photoEmploye == null)
						photoEmploye = EOPhotosEmployes.creer(edc, getCurrentIndividu());

					photoEmploye.setDatasPhoto(res);
					photoEmploye.setDatePrise(new NSTimestamp());
					edc.saveChanges();

					myView.getPhotoAgent().setImage(imgIcon.getImage());


					myView.getPhotoAgent().repaint();
				}

			}
		}
		catch (Exception e) {

			edc.revert();
			e.printStackTrace();

		}
	}

	/**
	 * 
	 */
	private void selectServiceGestionnaire() {

		EOStructure structure = ServiceGestionnaireSelectCtrl.sharedInstance(getCurrentIndividu().editingContext()).getServiceGestionnaire();
		
		if (structure != null) {
			
			try {

				if (getCurrentGestionnaire() == null) {
					setCurrentGestionnaire(EOGestionnaires.creer(getCurrentIndividu().editingContext(), getCurrentIndividu(), structure));
				}
				else
					getCurrentGestionnaire().setToStructureRelationship(structure);
				
				getCurrentIndividu().editingContext().saveChanges();
				setCurrentGestionnaire(getCurrentGestionnaire());
				NSNotificationCenter.defaultCenter().postNotification(CHANGER_EMPLOYE, getCurrentIndividu().noIndividu(),null);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearTextFields();

		if (getCurrentIndividu() != null) {

			NSTimestamp dateReference = new NSTimestamp(); 

			setCurrentGestionnaire(EOGestionnaires.getPersonneGestionnaire(edc, getCurrentIndividu()));
			
			// photo
			NSData datasImage = getCurrentIndividu().image();
			if (datasImage != null) {
				myView.getPhotoAgent().setImage(new ImageIcon(datasImage.bytes()).getImage());
			}
			else {
				myView.getPhotoAgent().setImage(null);
			}
			myView.getPhotoAgent().repaint();

			String stringIdentite = getCurrentIndividu().cCivilite() + " " + getCurrentIndividu().nomUsuel();
			if (getCurrentIndividu().nomPatronymique() != null 
					&& !getCurrentIndividu().nomPatronymique().equals(getCurrentIndividu().nomUsuel()) )
				stringIdentite += " ("+getCurrentIndividu().nomPatronymique()+")";
			stringIdentite += " " + getCurrentIndividu().prenom();
			CocktailUtilities.setTextToLabel(myView.getTfIdentite(), stringIdentite);

			if (getCurrentIndividu().indNoInsee() != null) {
				CocktailUtilities.setTextToLabel(myView.getLblSecu(), " No sécu : " + getCurrentIndividu().indNoInsee());
			if (getCurrentIndividu().indCleInsee() != null)
				CocktailUtilities.setTextToLabel(myView.getLblSecu(), myView.getLblSecu().getText() + " " + StringCtrl.stringCompletion(getCurrentIndividu().indCleInsee().toString(), 2 , "0", "G") );
			}
			else {

				if (getCurrentIndividu().indNoInseeProv() != null) {
					CocktailUtilities.setTextToLabel(myView.getLblSecu(), " No sécu PROVISOIRE : " + getCurrentIndividu().indNoInseeProv());
				if (getCurrentIndividu().indCleInseeProv() != null)
					CocktailUtilities.setTextToLabel(myView.getLblSecu(), myView.getLblSecu().getText() + " " + StringCtrl.stringCompletion(getCurrentIndividu().indCleInseeProv().toString(), 2 , "0", "G") );
				}

			}
			if (getCurrentIndividu().personnel() != null && getCurrentIndividu().personnel().numen() != null)
				CocktailUtilities.setTextToLabel(myView.getLblSecu(), myView.getLblSecu().getText() + " - NUMEN : " + getCurrentIndividu().personnel().numen());

			String stringAge = "";
			if (getCurrentIndividu().estHomme())
				stringAge += "Né le ";
			else
				stringAge += "Née le ";

			stringAge += getCurrentIndividu().dateNaissanceFormatee() + " ( " + getCurrentIndividu().age() + " Ans )";
			if (getCurrentIndividu().villeDeNaissance() != null)
				stringAge += " à " + currentIndividu.villeDeNaissance();

			CocktailUtilities.setTextToLabel(myView.getLblAge(), stringAge);

			setCurrentSituationFamiliale(getCurrentIndividu().toSituationFamiliale());
			NSArray<EORepartEnfant> enfants =EORepartEnfant.rechercherPourParent(getEdc(), getCurrentIndividu());
			if (enfants.count() > 0)
				CocktailUtilities.setTextToLabel(myView.getLblSituationFamiliale(), myView.getLblSituationFamiliale().getText() + " - " + enfants.count() + " enfants");				

			myView.getTemSauvadet().setSelected(getCurrentIndividu().personnel().estSauvadet());
			
			// Adresse
			setCurrentAdresse(EORepartPersonneAdresse.rechercherAdresseCourante(edc, getCurrentIndividu()));

			// Telephone
			NSArray<EOPersonneTelephone> tel = EOPersonneTelephone.rechercherTelPersonnels(edc, getCurrentIndividu());
			if (tel.count() > 0) {
				setCurrentTelephone(tel.get(0));				
			}
			else
				setCurrentTelephone(null);				

			setCurrentArrivee(EOArrivee.findForIndividu(getEdc(), getCurrentIndividu()));
			setCurrentDepart(EODepart.dernierDepartPourIndividu(getEdc(), getCurrentIndividu()));

			NSArray<EOAffectation> affectations = EOAffectation.rechercherAffectationsADate(edc, getCurrentIndividu(), DateCtrl.today());
			if (affectations.count()>0)
				setCurrentAffectation(affectations.get(0));
			else
				setCurrentAffectation(null);

			NSArray<EOOccupation> occupations = EOOccupation.findForIndividuAndDate(edc, getCurrentIndividu(), dateReference);
			if (occupations.size() > 0)
				setCurrentOccupation(occupations.get(0));
			else
				setCurrentOccupation(null);

			// Type d'agent (Héberge, Non Titulaire, Titulaire)
			if (getCurrentIndividu().estTitulaire())
				setEstTitulaire();
			else
				if (getCurrentIndividu().estContractuelSurPeriodeComplete(dateReference, dateReference))
					setEstContractuel();
				else {
					NSArray<EOContratHeberges> contrats = EOContratHeberges.findForIndividuAndPeriode(getEdc(), getCurrentIndividu(), dateReference, dateReference);
					if (contrats.count() > 0)
						setEstHeberge((EOContratHeberges)contrats.get(0));
					else
						setAncienAgent();
				}
		}

		updateInterface();

	}

	/**
	 * 
	 */
	private void setEstTitulaire() {
		NSTimestamp dateReference = new NSTimestamp();

		// Position
		String stringPosition = "";
		EOChangementPosition position = EOChangementPosition.positionPourDate(getEdc(), getCurrentIndividu(), dateReference);
		if (position != null) {
			stringPosition += position.toPosition().libelleLong();
			// Recherche des modalites de service
			EOModalitesService modalite = EOModalitesService.modalitePourDate(getEdc(), getCurrentIndividu(), dateReference);
			if (modalite != null) {
			
				if (modalite.estCrct() == false)
					stringPosition += " - " + modalite.modLibelle() + "( " + modalite.quotite() + " % )";
				else
					stringPosition += " - " + modalite.modLibelle();
			}
			else
				stringPosition += " ( 100 %) ";

			// CONGES - CLD, CFP , CMO, CLM, etc ...
			NSArray absences = EOAbsences.rechercherAbsencesLegalesPourIndividuEtDates(edc, getCurrentIndividu(), DateCtrl.today(), DateCtrl.today());
			for (Enumeration<EOAbsences> e = absences.objectEnumerator();e.hasMoreElements();) {
				EOAbsences absence = e.nextElement();
				if (absence.toTypeAbsence().estCongeLegal()) {
					stringPosition += " ("+absence.toTypeAbsence().libelleCourt() + ")";						
				}
			}

			CocktailUtilities.setTextToLabel(myView.getLblType(), "TITULAIRE - " + stringPosition);

		}
		else {
			CocktailUtilities.setTextToLabel(myView.getLblType(), "ANCIEN AGENT");
		}			

		// Element de carriere
		EOElementCarriere element = EOElementCarriere.rechercherElementPourIndividuADate(getEdc(), getCurrentIndividu(), dateReference);
		if (element != null) {
			CocktailUtilities.setTextToLabel(myView.getLblPeriode(), DateCtrl.dateToString(element.dateDebut()) + " - " + DateCtrl.dateToString(element.dateFin()));
			CocktailUtilities.setTextToLabel(myView.getLblDetail(), element.toCarriere().toTypePopulation().libelleCourt() + " ( " + element.toCarriere().toTypePopulation().libelleLong() + " )");

			CocktailUtilities.setTextToLabel(myView.getLblSpecialisation(), myCtrlSpec.getStringSpecForCarriereSpec(element.toCarriere().derniereSpecialisation()));                

			CocktailUtilities.setTextToLabel(myView.getLblGrade(), element.toGrade().llGrade());			
			if (element.cEchelon() != null)
				CocktailUtilities.setTextToLabel(myView.getLblGrade(), myView.getLblGrade().getText() + " ( Echelon : " + element.cEchelon() + ")");
			if (element.indiceMajore() != null)
				CocktailUtilities.setTextToLabel(myView.getLblGrade(), myView.getLblGrade().getText() + " ( INM : " + element.indiceMajore() + ")");
		}
	}

	/**
	 * 
	 */
	private void setEstContractuel() {
		NSTimestamp dateReference = new NSTimestamp();
		if (getCurrentIndividu().estHomme())
			CocktailUtilities.setTextToLabel(myView.getLblType(), "CONTRACTUEL");
		else
			CocktailUtilities.setTextToLabel(myView.getLblType(), "CONTRACTUELLE");

		// Activite
		String stringPosition = " - ACTIVITE";

		//  Avenant
		EOContratAvenant avenant = EOContratAvenant.rechercherAvenantPourIndividuADate(getEdc(), getCurrentIndividu(), dateReference);
		if (avenant != null) {

			stringPosition += " ( " + avenant.quotite().toString() + " % )";
			CocktailUtilities.setTextToLabel(myView.getLblType(), myView.getLblType().getText() + stringPosition);

			CocktailUtilities.setTextToLabel(myView.getLblPeriode(), DateCtrl.dateToString(avenant.dateDebut()) + " - " + DateCtrl.dateToString(avenant.dateFin()));
			CocktailUtilities.setTextToLabel(myView.getLblDetail(), avenant.contrat().toTypeContratTravail().libelleCourt());
			if (avenant.toGrade() != null)
				CocktailUtilities.setTextToLabel(myView.getLblGrade(), avenant.toGrade().llGrade());
			if (avenant.cEchelon() != null)
				CocktailUtilities.setTextToLabel(myView.getLblGrade(), myView.getLblGrade().getText() + " ( Echelon : " + avenant.cEchelon() + ")");

			CocktailUtilities.setTextToLabel(myView.getLblSpecialisation(), myCtrlSpec.getStringSpecForAvenant(avenant));				

		}
	}

	private class ListenerSauvadet implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			try {
				getCurrentIndividu().personnel().setEstSauvadet(myView.getTemSauvadet().isSelected());
				getEdc().saveChanges();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 
	 * @param contrat
	 */
	private void setEstHeberge(EOContratHeberges contrat) {
		CocktailUtilities.setTextToLabel(myView.getLblType(), "HEBERGE");
		CocktailUtilities.setTextToLabel(myView.getLblPeriode(), contrat.dateDebutFormatee() + " - " + contrat.dateFinFormatee());
	}

	/**
	 * 
	 */
	private void setAncienAgent() {
		CocktailUtilities.setTextToLabel(myView.getLblType(), "ANCIEN AGENT");
	}


	private void updateInterface() {
		myView.getBtnInfosPersonnelles().setEnabled(getCurrentIndividu() != null);
		myView.getBtnInfosComplémentaires().setEnabled(getCurrentIndividu() != null);
		myView.getBtnInfosRetraite().setEnabled(getCurrentIndividu() != null);
	}

}

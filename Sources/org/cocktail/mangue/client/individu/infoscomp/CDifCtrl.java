/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.individu.CDifView;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EODif;
import org.cocktail.mangue.modele.mangue.individu.EODifDetail;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class CDifCtrl {

	private EOEditingContext ec;
	private CDifView 			myView;

	private EODisplayGroup 			eod, eodDetail;
	private boolean					saisieEnabled;
	private InfosComplementairesCtrl ctrlParent;

	private TableListener 			listenerDif = new TableListener();
	private TableDetailListener 	listenerDetail = new TableDetailListener();
	private EOIndividu 		currentIndividu;
	private EODif			currentDif;
	private EODifDetail		currentDetail;

	public CDifCtrl(InfosComplementairesCtrl ctrl, EOEditingContext editingContext) {

		ec = editingContext;
		ctrlParent = ctrl;
		eod = new EODisplayGroup();
		eodDetail = new EODisplayGroup();
		myView = new CDifView(null, eod, eodDetail, false);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnAjouterDetail().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterDetail();}}
				);
		myView.getBtnModifierDetail().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierDetail();}}
				);
		myView.getBtnSupprimerDetail().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerDetail();}}
				);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
				);
		myView.getBtnToutRecalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {toutRecalculer();}}
				);
		myView.getBtnRecalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {calculerCurrentDif();}}
				);

		myView.setListeAnnees(CocktailConstantes.LISTE_ANNEES_DESC);

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, false);
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, false);
		myView.getPopupAnnees().addActionListener(new PopupAnneeListener());
		setSaisieEnabled(false);

		myView.getMyEOTable().addListener(listenerDif);						
		myView.getMyEOTableDetail().addListener(listenerDetail);

	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnAjouterDetail().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifierDetail().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimerDetail().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}

	public EODif getCurrentDif() {
		return currentDif;
	}

	public void setCurrentDif(EODif currentDif) {
		this.currentDif = currentDif;
		updateData();
	}

	public EODifDetail getCurrentDetail() {
		return currentDetail;
	}

	public void setCurrentDetail(EODifDetail currentDetail) {
		this.currentDetail = currentDetail;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public void actualiser() {
		eod.setObjectArray(EODif.findForIndividu(ec, getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateUI();
	}

	public JPanel getView() {
		return myView.getViewCDif();
	}
	public void open(EOIndividu individu) {
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupAnneeListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){				

			clearTextFields();

			if (getAnnee() != null) {

				getCurrentDif().setDateDebut(DateCtrl.debutAnnee(new Integer(getAnnee().intValue() - 1)));
				getCurrentDif().setDateFin(DateCtrl.finAnnee(new Integer(getAnnee().intValue() - 1)));
				getCurrentDif().setAnneeCalcul(getAnnee());

				getCurrentDif().calculerCredit();

				CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentDif().dateDebut());
				CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentDif().dateFin());
				CocktailUtilities.setNumberToField(myView.getTfCredit(), getCurrentDif().difCreditCumule());

			}
		}
	}

	/**
	 * 
	 */
	private void calculerCurrentDif() {
		if (getCurrentDif() != null) {
			getCurrentDif().calculerCredit();
			myView.getMyEOTable().updateUI();
		}
	}

	/**
	 * 
	 */
	public void toutRecalculer() {

		try {

			if(!EODialogs.runConfirmOperationDialog("Attention","Le droit à la formation va être recalculé entièrement pour cet agent.\n Continuer ?", "Oui", "Non"))		
				return;

			NSMutableArray<EODif> difs = new NSMutableArray<EODif>(EODif.findForIndividu(ec, getCurrentIndividu()));
			EOSortOrdering.sortArrayUsingKeyOrderArray(difs, EODif.SORT_ARRAY_DATE_DEBUT);

			int debits = 0;
			for (EODif dif : difs) {

				NSArray<EODifDetail> details = EODifDetail.findForDif(ec, dif);
				debits += CocktailUtilities.computeSumIntegerForKey(details, EODifDetail.DIF_DETAIL_DEBIT_KEY);

				dif.setDifDebitCumule(debits);

			}

			int premiereAnnee = 2007;
			int derniereAnnee = DateCtrl.getCurrentYear();

			//			if (difs.size() == 0) {
			NSArray<EOCarriere> carrieres = EOCarriere.findForIndividu(ec, getCurrentIndividu());
			if (carrieres.size() > 0) {
				EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
				premiereAnnee = DateCtrl.getYear(carrieres.get(0).dateDebut()) + 1;
			}
			else {
				NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividu(ec, getCurrentIndividu(), false);
				EOSortOrdering.sortedArrayUsingKeyOrderArray(contrats, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
				if (contrats.size() > 0) {
					premiereAnnee = DateCtrl.getYear(contrats.get(0).dateDebut()) + 1;
				}
			}
			//			}
			//			else {
			//				premiereAnnee = difs.get(0).anneeCalcul().intValue();
			//			}

			if (premiereAnnee > 1900) {

				if (premiereAnnee < ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF)
					premiereAnnee = ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF;
				
				for (int i=premiereAnnee;i<=derniereAnnee;i++) {
					EODif dif = EODif.findForAnneeEtAgent(ec, getCurrentIndividu(), new Integer(i));
					if (dif == null) {
						EODif newDif = EODif.creer(ec, getCurrentIndividu());
						newDif.setAnneeCalcul(i);
						newDif.setDateDebut(DateCtrl.debutAnnee(i-1));
						newDif.setDateFin(DateCtrl.finAnnee(i-1));
						newDif.calculerCredit();
					}
				}

				ec.saveChanges();

				difs = new NSMutableArray<EODif>(EODif.findForIndividu(ec, getCurrentIndividu()));
				EOSortOrdering.sortArrayUsingKeyOrderArray(difs, EODif.SORT_ARRAY_DATE_DEBUT);
				for (EODif dif : difs) {
					dif.calculerDebit();
					dif.calculerCredit();
				}

				ec.saveChanges();
				actualiser();
			}

		} catch (ValidationException ex)	{
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		} catch (Exception e)	{
			e.printStackTrace();
		}

	}

	private Integer getAnnee() {
		if (myView.getPopupAnnees().getSelectedIndex() ==0 )
			return null;
		return (Integer)myView.getPopupAnnees().getSelectedItem();
	}

	/**
	 * 
	 */
	private void ajouter() {
		setCurrentDif(EODif.creer(ec, getCurrentIndividu()));
		updateData();
		setSaisieEnabled(true);
		lockCtrlParent(true);

	}
	private void modifier() {
		setSaisieEnabled(true);
		lockCtrlParent(true);
	}
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention","Voulez-vous vraiment supprimer ce droit à formation ?", "Oui", "Non"))		
			return;
		try {

			for ( EODifDetail detail : (NSArray<EODifDetail>)eodDetail.displayedObjects()) {
				ec.deleteObject(detail);
			}

			ec.deleteObject(getCurrentDif());			
			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}


	private void ajouterDetail() {
		try {
			SaisieDetailDifCtrl.sharedInstance(ec).ajouter(getCurrentDif());
			getCurrentDif().calculerDebit();
			ec.saveChanges();

			myView.getMyEOTable().updateUI();
			listenerDif.onSelectionChanged();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void modifierDetail() {
		try {
			SaisieDetailDifCtrl.sharedInstance(ec).modifier(getCurrentDetail());
			getCurrentDif().calculerDebit();
			ec.saveChanges();

			myView.getMyEOTable().updateUI();
			myView.getMyEOTableDetail().updateUI();
			listenerDif.onSelectionChanged();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void supprimerDetail() {
		if(!EODialogs.runConfirmOperationDialog("Attention","Voulez-vous vraiment supprimer de détail ?", "Oui", "Non"))		
			return;
		try {
			ec.deleteObject(getCurrentDetail());			
			ec.saveChanges();
			listenerDif.onSelectionChanged();
		}
		catch (Exception e) {
			ec.revert();
		}
	}

	/**
	 * 
	 * @return
	 */
	private void traitementsAvantValidation() throws ValidationException {

		// Controle de l'unicite des periodes
		NSMutableArray<EODif> difs = new NSMutableArray<EODif>(eod.displayedObjects());
		if (difs.containsObject(getCurrentDif()) == false)
			difs.addObject(getCurrentDif());
		difs = new NSMutableArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(difs, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC));

		for (Enumeration<EODif> e = difs.objectEnumerator();e.hasMoreElements();) {

			EODif dif = e.nextElement();
			if (dif != getCurrentDif()) {

				if (dif.anneeCalcul().intValue() == currentDif.anneeCalcul().intValue() && dif.difCreditCumule() != null && getCurrentDif().difCreditCumule() != null)
					throw new ValidationException("Des crédits DIF ont déjà été saisis pour l'année " + getCurrentDif().anneeCalcul() + " !");

			}
		}

	}


	/**
	 * 
	 */
	private void valider() {

		try {

			getCurrentDif().setAnneeCalcul(getAnnee());
			getCurrentDif().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			getCurrentDif().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			getCurrentDif().setDifCreditCumule(CocktailUtilities.getIntegerFromField(myView.getTfCredit()));

			traitementsAvantValidation();

			ec.saveChanges();
			actualiser();
			setSaisieEnabled(false);
			lockCtrlParent(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {

		ec.revert();
		ServerProxy.clientSideRequestRevert(ec);
		listenerDif.onSelectionChanged();
		setSaisieEnabled(false);
		lockCtrlParent(false);
		updateUI();
	}
	private void clearTextFields() {
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
		myView.getTfCredit().setText("");
		myView.getTfDebit().setText("");	
		myView.getTfBalance().setText("");	

		eodDetail.setObjectArray(new NSArray());
		myView.getMyEOTableDetail().updateData();

	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}

	private void updateUI() {

		myView.getMyEOTable().setEnabled(!saisieEnabled());
		myView.getPopupAnnees().setEnabled(saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfCredit(), false, saisieEnabled());

		myView.getBtnAjouter().setEnabled(!saisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!saisieEnabled() && getCurrentDif() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && getCurrentDif() != null);
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getBtnAjouterDetail().setEnabled(getCurrentDif() != null);
		myView.getBtnModifierDetail().setEnabled(getCurrentDetail() != null);
		myView.getBtnSupprimerDetail().setEnabled(getCurrentDetail() != null);

	}

	/**
	 * 
	 */
	private void updateData() {

		clearTextFields();
		CocktailUtilities.removeActionListeners(myView.getPopupAnnees());
		if (getCurrentDif() != null) {

			myView.getPopupAnnees().setSelectedItem(getCurrentDif().anneeCalcul());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentDif().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentDif().dateFin());
			CocktailUtilities.setNumberToField(myView.getTfCredit(), getCurrentDif().difCreditCumule());
			CocktailUtilities.setNumberToField(myView.getTfDebit(), getCurrentDif().difDebitCumule());
			CocktailUtilities.setNumberToField(myView.getTfBalance(), getCurrentDif().balance());

			eodDetail.setObjectArray(EODifDetail.findForDif(ec, getCurrentDif()));
			myView.getMyEOTableDetail().updateData();
		}
		myView.getPopupAnnees().addActionListener(new PopupAnneeListener());
		updateUI();
	}

	private void lockCtrlParent(boolean value) {
		if (ctrlParent != null)
			ctrlParent.setIsLocked(value);
	}

	private class TableListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentDif() != null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentDif((EODif)eod.selectedObject());
		}
	}
	private class TableDetailListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentDetail() != null)
				modifierDetail();
		}
		public void onSelectionChanged() {
			setCurrentDetail((EODifDetail)eodDetail.selectedObject());
		}
	}

}

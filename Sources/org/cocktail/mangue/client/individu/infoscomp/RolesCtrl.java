package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.individu.RolesView;
import org.cocktail.mangue.client.select.GroupeSelectCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartStructure;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSValidation.ValidationException;

public class RolesCtrl {

	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private EODisplayGroup eodGroupes, eodAssociations;
	private RolesView myView;
	private InfosComplementairesCtrl ctrlParent;
	private ActionListenerTypeGroupe listenerTypeGroupe = new ActionListenerTypeGroupe();
	private ActionListenerTypeRole listenerTypeRole = new ActionListenerTypeRole();
	private ListenerGroupe 		listenerGroupe = new ListenerGroupe();
	private ListenerRole 		listenerRole = new ListenerRole();

	private EORepartStructure currentStructure;
	private EOIndividu currentIndividu;
	private EORepartAssociation currentAssociation;

	public RolesCtrl(InfosComplementairesCtrl ctrlParent, EOEditingContext edc) {

		this.edc = edc;
		this.ctrlParent = ctrlParent;

		eodGroupes = new EODisplayGroup();
		eodAssociations = new EODisplayGroup();
		myView = new RolesView(null, MODE_MODAL.booleanValue(),eodGroupes, eodAssociations);

		myView.getBtnAJouterGroupe().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterGroupe();}}
		);
		myView.getBtnSupprimerGroupe().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerGroupe();}}
		);
		myView.getBtnAJouterAssociation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterAssociation();}}
		);
		myView.getBtnModifierAssociation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierAssociation();}}
		);
		myView.getBtnSupprimerAssociation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerAssociation();}}
		);

		//		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		//		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);
		// 	Si un individu est selectionne, on met a jour les donnees
		//		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null) {
		//			currentIndividu = EOIndividu.rechercherIndividuAvecID(ec, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false);
		//			actualiser();
		//		}

		myView.getCheckServices().setSelected(true);
		myView.getCheckRolesStructure().setSelected(true);

		eodGroupes.setSortOrderings(EORepartStructure.SORT_LIBELLE_STRUCTURE_ASC);

		myView.getMyEOTableGroupes().addListener(listenerGroupe);
		myView.getMyEOTableRoles().addListener(listenerRole);

		myView.getCheckServices().addActionListener(listenerTypeGroupe);
		myView.getCheckAllGroupes().addActionListener(listenerTypeGroupe);
		myView.getCheckRolesStructure().addActionListener(listenerTypeRole);
		myView.getCheckAllRoles().addActionListener(listenerTypeRole);

		updateUI();

	}
	
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAJouterGroupe().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnAJouterAssociation().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifierAssociation().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimerGroupe().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimerAssociation().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}


	public EOIndividu currentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EORepartStructure currentStructure() {
		return currentStructure;
	}
	public void setCurrentStructure(EORepartStructure currentStructure) {
		this.currentStructure = currentStructure;
	}
	public EORepartAssociation currentAssociation() {
		return currentAssociation;
	}
	public void setCurrentAssociation(EORepartAssociation currentAssociation) {
		this.currentAssociation = currentAssociation;
	}
	public JFrame getView() {
		return myView;
	}
	public JPanel getViewRoles() {
		return myView.getViewRoles();
	}

	public void open(EOIndividu individu)	{

		clearTextFields();
		setCurrentIndividu(individu);
		if (currentIndividu() != null) {
			actualiser();
		}

		myView.setVisible(true);
	}

	public void employeHasChanged(NSNotification  notification) {

		if (notification != null && notification.object() != null) {
			currentIndividu = EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false);
			actualiser();
		}
	}

	private class ActionListenerTypeGroupe implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			actualiser();
		}
	}
	private class ActionListenerTypeRole implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			listenerGroupe.onSelectionChanged();
		}
	}


	public void actualiser() {

		if (currentIndividu != null) {
			if (myView.getCheckServices().isSelected() == false && myView.getCheckAllGroupes().isSelected() == false)
				myView.getCheckServices().setSelected(true);
			eodGroupes.setObjectArray(EORepartStructure.rechercherRepartStructuresRolesPourIndividu(edc, currentIndividu(), myView.getCheckServices().isSelected()));
			myView.getMyEOTableGroupes().updateData();
		}
		
		updateUI();

	}

	/**
	 * Selection d'un nouveau groupe d'appartenance
	 */
	private void ajouterGroupe() {

		try {

			EOStructure structure = GroupeSelectCtrl.sharedInstance().selectionnerGroupe(edc);

			if (structure != null) {

				NSArray structureExistante = EORepartStructure.rechercherRepartStructuresPourIndividuEtCodeStructure(edc, currentIndividu, structure.cStructure());
				if (structureExistante.count() > 0)
					throw new ValidationException("Cette structure est déjà définie pour cet agent !");								

				EORepartStructure.creer(edc, currentIndividu().persId(), structure);
				edc.saveChanges();
				actualiser();

			}

		}			
		catch (ValidationException ve) {
			EODialogs.runErrorDialog("ERREUR", ve.getMessage());
			edc.revert();
			return;
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", e.getMessage());
			edc.revert();
			e.printStackTrace();
		}

		clearTextFields();
		actualiser();
	}

	private void supprimerGroupe() {

		if (eodAssociations.displayedObjects().count() > 0) {

			EODialogs.runInformationDialog("ATTENTION","Veuillez d'abord supprimer les associations avant de supprimer le groupe !");
			return;
		}

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer ce groupe ?", "Oui", "Non"))		
			return;			

		try {			
			edc.deleteObject(currentStructure);
			edc.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			edc.revert();
		}
	}



	private void ajouterAssociation() {

		SaisieAssociationCtrl.sharedInstance(edc).ajouter(currentIndividu, currentStructure.structure());
		listenerGroupe.onSelectionChanged();		

	}
	private void modifierAssociation() {

		SaisieAssociationCtrl.sharedInstance(edc).modifier(currentAssociation, currentIndividu);
		listenerGroupe.onSelectionChanged();

	}
	private void supprimerAssociation() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer le ou les rôles sélectionnés ?", "Oui", "Non"))		
			return;			

		try {

			for (Enumeration<EORepartAssociation> e = eodAssociations.selectedObjects().objectEnumerator();e.hasMoreElements();) {

				EORepartAssociation repart = e.nextElement();
				edc.deleteObject(repart);

			}

			edc.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", "Erreur de suppression des rôles !");
			edc.revert();
		}
	}

	public void nettoyerChamps(NSNotification notification) {

		clearTextFields();
		currentIndividu = null;
		currentStructure = null;
		currentAssociation = null;
		updateUI();
	}

	private void clearTextFields() {

		eodGroupes.setObjectArray(new NSArray());
		eodAssociations.setObjectArray(new NSArray());

		myView.getMyEOTableGroupes().updateData();
		myView.getMyEOTableRoles().updateData();

	}

	private void updateUI() {

		myView.getBtnAJouterGroupe().setEnabled(currentIndividu() != null);
		myView.getBtnAJouterAssociation().setEnabled(currentStructure() != null);
		myView.getBtnModifierAssociation().setEnabled(currentAssociation() != null);
		myView.getBtnSupprimerAssociation().setEnabled(currentAssociation() != null);

		boolean isStructureAffectation = false;
		if (currentStructure() != null) {
			NSArray affectations = EOAffectation.rechercherAffectationsADate(edc, currentIndividu(), DateCtrl.today());
			for (Enumeration<EOAffectation> e = affectations.objectEnumerator();e.hasMoreElements();) {
				isStructureAffectation= e.nextElement().toStructureUlr().cStructure().equals(currentStructure().cStructure());
			}
		}

		myView.getBtnSupprimerGroupe().setEnabled(currentStructure() != null && isStructureAffectation == false);


	}

	private class ListenerGroupe implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			setCurrentStructure((EORepartStructure)eodGroupes.selectedObject());
			if (currentStructure() != null) {
				if (myView.getCheckRolesStructure().isSelected())
					eodAssociations.setObjectArray(EORepartAssociation.rechercherAssociationsPourIndividuEtGroupe(edc, currentIndividu(), currentStructure().structure()));
				else
					eodAssociations.setObjectArray(EORepartAssociation.rechercherAssociationsPourIndividuEtGroupe(edc, currentIndividu(), null));

				myView.getMyEOTableRoles().updateData();
			}
			updateUI();
		}
	}


	private class ListenerRole implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentAssociation != null)
				modifierAssociation();
		}
		public void onSelectionChanged() {
			currentAssociation = (EORepartAssociation)eodAssociations.selectedObject();
			updateUI();
		}
	}

}

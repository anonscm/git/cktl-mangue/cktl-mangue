/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infoscomp;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.individu.DistinctionsView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.nomenclatures.EODistinction;
import org.cocktail.mangue.common.modele.nomenclatures.EODistinctionNiveau;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuDistinction;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class DistinctionsCtrl extends ModelePageGestion {

	private DistinctionsView 		myView;
	private EODisplayGroup 			eod, eodNiveau, eodDistinction;
	private InfosComplementairesCtrl ctrlParent;

	private ListenerIndividuDistinction	listenerIndividuDistinction = new ListenerIndividuDistinction();
	private ListenerDistinction 		listenerDistinction = new ListenerDistinction();
	private ListenerNiveau		 		listenerNiveau = new ListenerNiveau();
	
	private EOIndividuDistinction	currentIndividuDistinction;
	private EODistinction			currentDistinction;
	private EODistinctionNiveau		currentNiveau;
	private EOIndividu 				currentIndividu;

	public DistinctionsCtrl(InfosComplementairesCtrl ctrlParent) {

		super(ctrlParent.getEdc());
		this.ctrlParent = ctrlParent;
		eod = new EODisplayGroup();
		eodNiveau = new EODisplayGroup();
		eodDistinction = new EODisplayGroup();
		myView = new DistinctionsView(null, eod, eodNiveau, eodDistinction, false);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateDistinction());

		setSaisieEnabled(false);

		myView.getMyEOTable().addListener(listenerIndividuDistinction);							
		myView.getMyEOTableDistinction().addListener(listenerDistinction);							
		myView.getMyEOTableNiveau().addListener(listenerNiveau);							

		eodDistinction.setObjectArray(EODistinction.fetchAll(getEdc()));
		myView.getMyEOTableDistinction().updateData();
		eodNiveau.setObjectArray(EODistinctionNiveau.fetchAll(getEdc()));
		myView.getMyEOTableNiveau().updateData();
	}
	
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}


	public EOIndividuDistinction getCurrentIndividuDistinction() {
		return currentIndividuDistinction;
	}

	public void setCurrentIndividuDistinction(EOIndividuDistinction currentIndividuDistinction) {
		this.currentIndividuDistinction = currentIndividuDistinction;
		updateDatas();
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EODistinction getCurrentDistinction() {
		return currentDistinction;
	}

	public void setCurrentDistinction(EODistinction currentDistinction) {
		this.currentDistinction = currentDistinction;
	}

	public EODistinctionNiveau getCurrentNiveau() {
		return currentNiveau;
	}

	public void setCurrentNiveau(EODistinctionNiveau currentNiveau) {
		this.currentNiveau = currentNiveau;
	}

	public void actualiser() {
		eod.setObjectArray(EOIndividuDistinction.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}
	public JPanel getViewDistinctions() {
		return myView.getViewDistinctions();
	}

	public NSTimestamp getDateDistinction() {
		try {
			return DateCtrl.stringToDate(myView.getTfDateDistinction().getText());
		}
		catch (Exception e) {
			return null;
		}
	}
	public void setDateDistinction(NSTimestamp myDate) {
		myView.getTfDateDistinction().setText(DateCtrl.dateToString(myDate));
	}

	private class ListenerIndividuDistinction implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentIndividuDistinction() != null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentIndividuDistinction((EOIndividuDistinction)eod.selectedObject());
		}
	}
	private class ListenerDistinction implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {}
		public void onSelectionChanged() {
			setCurrentDistinction((EODistinction)eodDistinction.selectedObject());
		}
	}
	private class ListenerNiveau implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {}
		public void onSelectionChanged() {
			setCurrentNiveau((EODistinctionNiveau)eodNiveau.selectedObject());
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getTfDateDistinction().setText("");
		myView.getMyTableModelDistinction().fireTableDataChanged();
		myView.getMyTableModelNiveau().fireTableDataChanged();

	}
	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentIndividuDistinction() != null) {

			CocktailUtilities.setDateToField(myView.getTfDateDistinction(), getCurrentIndividuDistinction().dateDistinction(), "%d/%m/%Y");
			setCurrentDistinction(getCurrentIndividuDistinction().toDistinction());
			setCurrentNiveau(getCurrentIndividuDistinction().toDistinctionNiveau());
			
			if (getCurrentNiveau() != null)
				myView.getMyEOTableNiveau().forceNewSelectionOfObjects(new NSArray(getCurrentNiveau()));
			if (getCurrentDistinction() != null)
				myView.getMyEOTableDistinction().forceNewSelectionOfObjects(new NSArray(getCurrentDistinction()));
			
		}
		updateInterface();

	}
	@Override
	protected void updateInterface() {
		
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());
		
		CocktailUtilities.initTextField(myView.getTfDateDistinction(), false, isSaisieEnabled());

		myView.getMyEOTableDistinction().setEnabled(isSaisieEnabled());
		myView.getMyEOTableNiveau().setEnabled(isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentIndividuDistinction() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentIndividuDistinction() != null);
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

	}
	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentIndividuDistinction().setDateDistinction(getDateDistinction());
		getCurrentIndividuDistinction().setToDistinctionRelationship(getCurrentDistinction());
		getCurrentIndividuDistinction().setToDistinctionNiveauRelationship(getCurrentNiveau());

	}
	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(false);
	}
	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(false);
		listenerIndividuDistinction.onSelectionChanged();
	}
	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentIndividuDistinction(EOIndividuDistinction.creer(getEdc(), getCurrentIndividu()));
		ctrlParent.setIsLocked(true);

	}
	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(true);
		
	}
	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(getCurrentIndividuDistinction());
	}
	
	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}
}

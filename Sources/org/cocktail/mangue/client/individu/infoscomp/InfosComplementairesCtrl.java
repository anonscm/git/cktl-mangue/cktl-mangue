package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;
import javax.swing.WindowConstants;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.individu.InfosComplementairesView;
import org.cocktail.mangue.client.individu.fonctions.IndividuFonctionsCtrl;
import org.cocktail.mangue.client.individu.medical.DeclarationAccidentCtrl;
import org.cocktail.mangue.client.individu.medical.DonneesMedicalesCtrl;
import org.cocktail.mangue.client.individu.medical.MaladieProfessionnelleCtrl;
import org.cocktail.mangue.client.individu.medical.PeriodesHandicapCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class InfosComplementairesCtrl {

	public static final String NOTIF_LOCK_INFOS_COMPLEMENTAIRES = "NotifLock";
	public static final String NOTIF_UNLOCK_INFOS_COMPLEMENTAIRES = "NotifUnlock";

	public static final String LAYOUT_ARRIVEE = "ARRIVEES / DEPARTS";
	public static final String LAYOUT_SERVICE_NATIONAL = "SERVICE NATIONAL";
	public static final String LAYOUT_DIPLOMES = "DIPLOMES";
	public static final String LAYOUT_FORMATIONS = "FORMATIONS";
	public static final String LAYOUT_DISTINCTIONS = "DISTINCTIONS";
	public static final String LAYOUT_HANDICAP = "HANDICAP";
	public static final String LAYOUT_ACCIDENT = "DECLARATION ACCIDENT";
	public static final String LAYOUT_MALADIE = "MALADIE PROFESSIONNELLE";
	public static final String LAYOUT_MEDICAL = "INFORMATIONS MEDICALES";
	public static final String LAYOUT_DECHARGES = "DECHARGES DE SERVICE";
	public static final String LAYOUT_HCOMP = "HEURES COMPLEMENTAIRES";
	public static final String LAYOUT_CDIF = "DROIT INDIVIDUEL A FORMATIONS";
	public static final String LAYOUT_FONCTIONS = "FONCTIONS";
	public static final String LAYOUT_ROLES = "GROUPES / RÔLES";

	private static InfosComplementairesCtrl 	sharedInstance;
	private static Boolean 		MODE_MODAL = Boolean.TRUE;
	private EOEditingContext 	edc;
	private InfosComplementairesView 			myView;
	private EOIndividu 			currentIndividu;
	private boolean isLocked;

	private ArriveeListener arriveeListener = new ArriveeListener();
	private ServiceListener serviceListener = new ServiceListener();
	private DiplomesListener diplomesListener = new DiplomesListener();
	private FormationsListener formationsListener = new FormationsListener();
	private AccidentListener accidentsListener = new AccidentListener();
	private HandicapListener handicapListener = new HandicapListener();
	private DistinctionsListener distinctionsListener = new DistinctionsListener();
	private MedicalListener medicalListener = new MedicalListener();
	private DechargesListener dechargesListener = new DechargesListener();
	private HcompListener hcompListener = new HcompListener();
	private CDifListener cdifListener = new CDifListener();
	private FonctionsListener fonctionsListener = new FonctionsListener();
	private RolesListener rolesListener = new RolesListener();

	private PeriodesMilitairesCtrl serviceNationalCtrl = null;
	private DiplomesCtrl diplomeCtrl = null;
	private FormationsCtrl formationCtrl = null;
	private DistinctionsCtrl distinctionCtrl = null;
	private PeriodesHandicapCtrl handicapCtrl = null;
	private DonneesMedicalesCtrl medicalCtrl = null;
	private ArriveesDepartsCtrl arriveeCtrl = null;
	private DeclarationAccidentCtrl accidentCtrl = null;
	private MaladieProfessionnelleCtrl maladieCtrl = null;
	private DechargesCtrl dechargesCtrl = null;
	private HeuresCompCtrl hcompCtrl = null;
	private CDifCtrl cdifCtrl = null;
	private IndividuFonctionsCtrl fonctionsCtrl = null;
	private RolesCtrl rolesCtrl = null;


	public InfosComplementairesCtrl(EOEditingContext edc) {

		setEdc(edc);
		myView = new InfosComplementairesView(null, MODE_MODAL.booleanValue());

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("unlock",new Class[] {NSNotification.class}), NOTIF_UNLOCK_INFOS_COMPLEMENTAIRES,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("lock",new Class[] {NSNotification.class}), NOTIF_LOCK_INFOS_COMPLEMENTAIRES,null);

		myView.getBtnFermer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {myView.dispose();}}
		);

		arriveeCtrl = new ArriveesDepartsCtrl(this, edc);
		serviceNationalCtrl = new PeriodesMilitairesCtrl(this, edc);
		diplomeCtrl = new DiplomesCtrl(this,edc);
		formationCtrl = new FormationsCtrl(this,edc);
		distinctionCtrl = new DistinctionsCtrl(this);
		handicapCtrl = new PeriodesHandicapCtrl(this);
		medicalCtrl = new DonneesMedicalesCtrl(this,edc);
		accidentCtrl = new DeclarationAccidentCtrl(this, edc);
		maladieCtrl = new MaladieProfessionnelleCtrl(this,edc);
		dechargesCtrl = new DechargesCtrl(this, edc);
		hcompCtrl = new HeuresCompCtrl(this, edc);
		cdifCtrl = new CDifCtrl(this, edc);
		fonctionsCtrl = new IndividuFonctionsCtrl(this, edc);
		rolesCtrl = new RolesCtrl(this, edc);

		myView.getSwapView().add(LAYOUT_ARRIVEE,arriveeCtrl.getViewMouvements());
		myView.getSwapView().add(LAYOUT_SERVICE_NATIONAL,serviceNationalCtrl.getViewPeriodesMilitaires());
		myView.getSwapView().add(LAYOUT_DIPLOMES,diplomeCtrl.getViewDiplomes());
		myView.getSwapView().add(LAYOUT_FORMATIONS, formationCtrl.getViewFormations());
		myView.getSwapView().add(LAYOUT_DISTINCTIONS, distinctionCtrl.getViewDistinctions());
		myView.getSwapView().add(LAYOUT_HANDICAP, handicapCtrl.getViewHandicap());
		myView.getSwapView().add(LAYOUT_MEDICAL, medicalCtrl.getViewMedicale());
		myView.getSwapView().add(LAYOUT_ACCIDENT,accidentCtrl.getViewAccident());
		myView.getSwapView().add(LAYOUT_MALADIE,maladieCtrl.getViewMaladie());
		myView.getSwapView().add(LAYOUT_DECHARGES,dechargesCtrl.getView());
		myView.getSwapView().add(LAYOUT_HCOMP,hcompCtrl.getView());
		myView.getSwapView().add(LAYOUT_CDIF,cdifCtrl.getView());
		myView.getSwapView().add(LAYOUT_FONCTIONS,fonctionsCtrl.getViewFonctions());
		myView.getSwapView().add(LAYOUT_ROLES,rolesCtrl.getViewRoles());

		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		myView.getCheckArrivee().setSelected(true);
		myView.getCheckArrivee().addActionListener(arriveeListener);
		myView.getCheckServiceNational().addActionListener(serviceListener);
		myView.getCheckDiplomes().addActionListener(diplomesListener);
		myView.getCheckFormations().addActionListener(formationsListener);
		myView.getCheckDistinctions().addActionListener(distinctionsListener);
		myView.getCheckHandicap().addActionListener(handicapListener);
		myView.getCheckMedical().addActionListener(medicalListener);
		myView.getCheckAccident().addActionListener(accidentsListener);
		myView.getCheckMaladieProf().addActionListener(new MaladieListener());
		myView.getCheckDecharges().addActionListener(dechargesListener);
		myView.getCheckHcomp().addActionListener(hcompListener);
		myView.getCheckCDif().addActionListener(cdifListener);
		myView.getCheckFonctions().addActionListener(fonctionsListener);
		myView.getCheckRoles().addActionListener(rolesListener);

		arriveeListener.actionPerformed(null);
		
		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

	}

	public static InfosComplementairesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new InfosComplementairesCtrl(editingContext);
		return sharedInstance;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {

		myView.getCheckMaladieProf().setVisible(false);
		myView.getCheckMedical().setEnabled(currentUtilisateur.peutAfficherInfosPerso());
		myView.getCheckHandicap().setEnabled(currentUtilisateur.peutAfficherInfosPerso());

		arriveeCtrl.setDroitsGestion(currentUtilisateur);
		serviceNationalCtrl.setDroitsGestion(currentUtilisateur);
		diplomeCtrl.setDroitsGestion(currentUtilisateur);
		formationCtrl.setDroitsGestion(currentUtilisateur);
		distinctionCtrl.setDroitsGestion(currentUtilisateur);
		handicapCtrl.setDroitsGestion(currentUtilisateur);
		medicalCtrl.setDroitsGestion(currentUtilisateur);
		accidentCtrl.setDroitsGestion(currentUtilisateur);
		//maladieCtrl.setDroitsGestion(currentUtilisateur);
		dechargesCtrl.setDroitsGestion(currentUtilisateur);
		hcompCtrl.setDroitsGestion(currentUtilisateur);
		cdifCtrl.setDroitsGestion(currentUtilisateur);
		fonctionsCtrl.setDroitsGestion(currentUtilisateur);
		rolesCtrl.setDroitsGestion(currentUtilisateur);

	}

	
	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		if (currentIndividu() != null)
			myView.setTitle(currentIndividu.identitePrenomFirst() + " - INFORMATIONS COMPLEMENTAIRES");
		else
			myView.setTitle("INFORMATIONS COMPLEMENTAIRES");
		actualiser();
	}

	public void open(EOIndividu individu)	{
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		setCurrentIndividu(individu);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}

	public void open(EOIndividu individu, String layout)	{

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		setCurrentIndividu(individu);

		CRICursor.setWaitCursor(myView);
		if (layout.equals(LAYOUT_ARRIVEE)) {myView.getCheckArrivee().setSelected(true);arriveeListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_SERVICE_NATIONAL)) {myView.getCheckServiceNational().setSelected(true);serviceListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_DIPLOMES))  {myView.getCheckDiplomes().setSelected(true);diplomesListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_FORMATIONS)) {myView.getCheckFormations().setSelected(true);formationsListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_ACCIDENT)) {myView.getCheckAccident().setSelected(true);accidentsListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_HANDICAP)) {myView.getCheckHandicap().setSelected(true);handicapListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_DISTINCTIONS)) {myView.getCheckDistinctions().setSelected(true);distinctionsListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_MEDICAL)) {myView.getCheckMedical().setSelected(true);medicalListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_DECHARGES)) {myView.getCheckDecharges().setSelected(true);dechargesListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_HCOMP)) {myView.getCheckHcomp().setSelected(true);hcompListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_CDIF)) {myView.getCheckCDif().setSelected(true);cdifListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_FONCTIONS)) {myView.getCheckFonctions().setSelected(true);fonctionsListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_ROLES)) {myView.getCheckRoles().setSelected(true);rolesListener.actionPerformed(null);}

		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}


	public void employeHasChanged(NSNotification  notification) {
		if (notification != null && myView.isVisible()) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
		}
	}
	public void setEnabled(boolean yn) {
	}

	public void actualiser() {
		if (myView.getCheckArrivee().isSelected())
			arriveeCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckServiceNational().isSelected())
			serviceNationalCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckDiplomes().isSelected())
			diplomeCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckFormations().isSelected())
			formationCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckHandicap().isSelected())
			handicapCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckDistinctions().isSelected())
			distinctionCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckMedical().isSelected())
			medicalCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckAccident().isSelected())
			accidentCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckMaladieProf().isSelected())
			maladieCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckDecharges().isSelected())
			dechargesCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckHcomp().isSelected())
			hcompCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckCDif().isSelected())
			cdifCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckFonctions().isSelected())
			fonctionsCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckRoles().isSelected())
			rolesCtrl.setCurrentIndividu(currentIndividu());			
	}

	public void toFront() {
		myView.toFront();
	}
	public void nettoyerChamps(NSNotification notification) {
		setCurrentIndividu(null);
	}
	public void lock(NSNotification notif) {
		setIsLocked(true);
	}
	public void unlock(NSNotification notif) {
		setIsLocked(false);
	}
	public boolean isLocked() {
		return isLocked;
	}
	public void setIsLocked(boolean yn) {
		isLocked = yn;
		updateUI();
	}
	private void updateUI() {
		myView.getCheckArrivee().setEnabled(!isLocked());
		myView.getCheckServiceNational().setEnabled(!isLocked());
		myView.getCheckDiplomes().setEnabled(!isLocked());
		myView.getCheckFormations().setEnabled(!isLocked());
		myView.getCheckDistinctions().setEnabled(!isLocked());
		myView.getCheckHandicap().setEnabled(!isLocked());
		myView.getCheckMaladieProf().setEnabled(!isLocked());
		myView.getCheckAccident().setEnabled(!isLocked());
		myView.getCheckMedical().setEnabled(!isLocked());
		myView.getCheckDecharges().setEnabled(!isLocked());
		myView.getCheckHcomp().setEnabled(!isLocked());
		myView.getCheckCDif().setEnabled(!isLocked());
		myView.getCheckFonctions().setEnabled(!isLocked());
		myView.getCheckRoles().setEnabled(!isLocked());
		myView.getBtnFermer().setEnabled(!isLocked());

		if (!isLocked())
			myView.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		else
			myView.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	}
	private void swapViewHasChanged(JRadioButton radioButton, String layout) {

		CRICursor.setWaitCursor(myView);
		myView.getTfTitre().setText(layout);
		myView.getTfTitre().setBackground(radioButton.getBackground());
		((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), layout);				
		actualiser();
		CRICursor.setDefaultCursor(myView);
	}

	private class AccidentListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckAccident(), LAYOUT_ACCIDENT);
		}
	}
	private class MaladieListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckMaladieProf(), LAYOUT_MALADIE);
		}
	}
	private class ArriveeListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckArrivee(), LAYOUT_ARRIVEE);
		}
	}
	private class ServiceListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckServiceNational(), LAYOUT_SERVICE_NATIONAL);
		}
	}
	private class DiplomesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckDiplomes(), LAYOUT_DIPLOMES);
		}
	}
	private class FormationsListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckFormations(), LAYOUT_FORMATIONS);
		}
	}
	private class DistinctionsListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckDistinctions(), LAYOUT_DISTINCTIONS);
		}
	}
	private class HandicapListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckHandicap(), LAYOUT_HANDICAP);
		}
	}
	private class MedicalListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckMedical(), LAYOUT_MEDICAL);
		}
	}
	private class DechargesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckDecharges(), LAYOUT_DECHARGES);
		}
	}
	private class HcompListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckHcomp(), LAYOUT_HCOMP);
		}
	}
	private class CDifListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckCDif(), LAYOUT_CDIF);
		}
	}
	private class FonctionsListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckFonctions(), LAYOUT_FONCTIONS);
		}
	}
	private class RolesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckRoles(), LAYOUT_ROLES);
		}
	}

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.HeuresCompView;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOEnseignement;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSValidation.ValidationException;

public class HeuresCompCtrl {

	private static HeuresCompCtrl sharedInstance;

	private EOEditingContext ec;
	private HeuresCompView	myView;

	private EODisplayGroup 				eod;
	private boolean						saisieEnabled;
	private InfosComplementairesCtrl	ctrlParent;
	private boolean estIndividuIatoss;
	private boolean estIndividuEnseignant;

	private ListenerEnseignement 	listenerDecharge = new ListenerEnseignement();
	private EOStructure				currentStructure;
	private EOEnseignement			currentEnseignement;
	private EOIndividu 				currentIndividu;

	public HeuresCompCtrl(InfosComplementairesCtrl ctrl, EOEditingContext editingContext) {

		ec = editingContext;
		ctrlParent = ctrl;

		eod = new EODisplayGroup();
		myView = new HeuresCompView(null, eod, false);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		myView.getBtnGetStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getStructure();}}
		);
		myView.getBtnDelStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delStructure();}}
		);

		myView.getTfDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDebut()));
		myView.getTfDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDebut()));
		myView.getTfFin().addFocusListener(new FocusListenerDateTextField(myView.getTfFin()));
		myView.getTfFin().addActionListener(new ActionListenerDateTextField(myView.getTfFin()));

		myView.getMyEOTable().addListener(listenerDecharge);		
		CocktailUtilities.initTextField(myView.getTfStructure(), false, false);

		setSaisieEnabled(false);
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}



	public EOStructure currentStructure() {
		return currentStructure;
	}



	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
		CocktailUtilities.setTextToField(myView.getTfStructure(), "");
		if (currentStructure != null)
			CocktailUtilities.setTextToField(myView.getTfStructure(), currentStructure.llStructure());
	}



	public EOEnseignement getCurrentEnseignement() {
		return currentEnseignement;
	}



	public void setCurrentEnseignement(EOEnseignement currentEnseignement) {
		this.currentEnseignement = currentEnseignement;
		updateDatas();
	}



	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}



	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		preparerTypeIndividu();
		actualiser();
	}



	public JPanel getView() {
		return myView.getViewHeuresComp();
	}
	public void toFront() {
		myView.toFront();
	}
	private void ajouter() {
		setCurrentEnseignement(EOEnseignement.creer(ec, getCurrentIndividu()));
		updateDatas();
		setSaisieEnabled(true);
	}
	private void modifier() {
		setSaisieEnabled(true);
	}
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention","Voulez-vous vraiment supprimer cette saisie ?", "Oui", "Non"))		
			return;
		try {
			ec.deleteObject(getCurrentEnseignement());			
			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			ec.revert();
		}
	}
	public void actualiser() {	
		eod.setObjectArray(EOEnseignement.rechercherPourIndividu(ec, getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}

	private void getStructure() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(ec);
		if (structure != null)
			setCurrentStructure(structure);
	}
	private void delStructure() {
		setCurrentStructure(null);
	}
	
	/**
	 * 
	 * @return
	 */
	private boolean traitementsAvantValidation() {

		return true;
	}
	
	private void preparerTypeIndividu() {
		
		estIndividuIatoss = false;
		estIndividuEnseignant = false;

		if (getCurrentIndividu() != null) {
			// Rechercher si l'individu est un BIATSS
			NSArray<EOCarriere> carrieres = EOCarriere.findForIndividu(ec, getCurrentIndividu());
			for (EOCarriere myCarriere : carrieres) {
				if ( (myCarriere.toTypePopulation().estAtos()) || (myCarriere.toTypePopulation().estItarf()) ) {
					estIndividuIatoss = true;
					break;
				}
			}
			if (!estIndividuIatoss) {
				// Vérifier si l'individu a un contrat de Iatoss
				EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("contrat.toIndividu = %@ " +
						"AND toGrade.toCorps.toTypePopulation.temAtos = 'O'", new NSArray(getCurrentIndividu()));
				NSArray<EOContratAvenant> avenants = EOContratAvenant.rechercherAvenantsAvecCriteres(ec, qualifier, false, true);
				estIndividuIatoss = avenants.count() > 0;
			}
			
			// Vérifier si l'individu a un contrat de professeur du 1er ou 2nd degré
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("contrat.toIndividu = %@ " +
					"AND (toGrade.toCorps.cTypeCorps = '" + EOEnseignement.ENSEIGNANT_DEGRE_1 + "' OR toGrade.toCorps.cTypeCorps = '" + EOEnseignement.ENSEIGNANT_DEGRE_2 + "')", 
					new NSArray(getCurrentIndividu()));
			
			NSArray<EOContratAvenant> avenants = EOContratAvenant.rechercherAvenantsAvecCriteres(ec, qualifier, false, true);
			estIndividuEnseignant = avenants.count() > 0;
		}
	}

	/**
	 * 
	 */
	private void valider() {

		try {

			getCurrentEnseignement().setStructureRelationship(currentStructure());
			getCurrentEnseignement().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
			getCurrentEnseignement().setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
			getCurrentEnseignement().setNbHEnseignement(CocktailUtilities.getBigDecimalFromField(myView.getTfHeures()));

			if (!traitementsAvantValidation())
				return;

			ec.saveChanges();
			actualiser();
			setSaisieEnabled(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {
		ec.revert();
		listenerDecharge.onSelectionChanged();
		setSaisieEnabled(false);
		updateInterface();
	}

	private void clearTextFields() {
		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		myView.getTfStructure().setText("");
		myView.getTfHeures().setText("");		
	}
	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosComplementairesCtrl.NOTIF_LOCK_INFOS_COMPLEMENTAIRES, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosComplementairesCtrl.NOTIF_UNLOCK_INFOS_COMPLEMENTAIRES, null);
	}

	/**
	 * 
	 * Gestion de l'activation de tous les objets de l'interface.
	 * Modification en mode saisie uniquement.
	 * Les adresses de type FACTURATION ne peuvent être modifiees.
	 * 
	 * La ville est déduite automatiquement s'il s'agit d'un pasy ETRANGER 
	 * 
	 */
	private void updateInterface() {

		myView.getMyEOTable().setEnabled(!saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDebut(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfFin(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfHeures(), false, saisieEnabled());

		myView.getBtnGetStructure().setEnabled(saisieEnabled());
		myView.getBtnDelStructure().setEnabled(saisieEnabled() && currentStructure() != null);

		myView.getBtnAjouter().setEnabled(!saisieEnabled() && getCurrentIndividu() != null && (estIndividuIatoss || estIndividuEnseignant));		
		myView.getBtnModifier().setEnabled(!saisieEnabled() && getCurrentEnseignement() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && getCurrentEnseignement() != null);
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearTextFields();
		if (getCurrentEnseignement() != null) {
			setCurrentStructure(currentEnseignement.structure());

			CocktailUtilities.setDateToField(myView.getTfDebut(), currentEnseignement.dateDebut());
			CocktailUtilities.setDateToField(myView.getTfFin(), currentEnseignement.dateFin());
			CocktailUtilities.setNumberToField(myView.getTfHeures(), currentEnseignement.nbHEnseignement());
		}
		updateInterface();
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerEnseignement implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentEnseignement((EOEnseignement)eod.selectedObject());
		}
	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateInterface();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}

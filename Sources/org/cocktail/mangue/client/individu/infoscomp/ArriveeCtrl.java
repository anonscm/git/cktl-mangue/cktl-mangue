// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.ArriveeView;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.client.select.TypeAccesSelectCtrl;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOArrivee;
import org.cocktail.mangue.modele.mangue.individu.EOArriveesDeparts;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class ArriveeCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private EOEditingContext ec;
	private ArriveeView myView;
	private ArriveesDepartsCtrl ctrlParent;

	private PopupEchelonListener listenerEchelons = new PopupEchelonListener();

	private EOArrivee			currentArrivee;
	private EORne				currentUai;
	private EOTypeAcces 		currentAcces;
	private EOCorps 			currentCorps;
	private EOGrade 			currentGrade;
	private boolean 			saisieEnabled;

	private GradeSelectCtrl 	myGradeSelector;

	public ArriveeCtrl(ArriveesDepartsCtrl ctrl, EOEditingContext globalEc) {

		ec = globalEc;
		ctrlParent = ctrl;

		myView = new ArriveeView(new JFrame(), true);

		myView.getBtnGetUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectUai();}}
				);
		myView.getBtnDelUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delUai();}}
				);
		myView.getBtnGetCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectCorps();}}
				);
		myView.getBtnGetGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectGrade();}}
				);
		myView.getBtnDelCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delCorps();}}
				);
		myView.getBtnDelGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delGrade();}}
				);

		myView.getBtnGetAcces().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectTypeAcces();}}
				);
		myView.getBtnEvaluerAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {evaluerAffectation();}}
				);

		myView.getPopupEchelons().addActionListener(listenerEchelons);

		myView.getTfDatePremiereAffectation().addFocusListener(new FocusListenerDateTextField(myView.getTfDatePremiereAffectation()));
		myView.getTfDatePremiereAffectation().addActionListener(new ActionListenerDateTextField(myView.getTfDatePremiereAffectation()));

		myView.getCheckFonctionPublique().addActionListener(new CheckFonctionPubliqueListener());

		CocktailUtilities.initTextField(myView.getTfLibelleCorps(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleGrade(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeAcces(), false, false);
		CocktailUtilities.initTextField(myView.getTfUai(), false, false);

	}

	private EOEditingContext getEdc() {
		return ec;
	}

	public JPanel getView() {
		return myView.getViewArrivee();
	}

	public void imprimerArrete() {

	}
	public EOArrivee getCurrentArrivee() {
		return currentArrivee;
	}
	public void setCurrentArrivee(EOArrivee currentArrivee) {
		this.currentArrivee = currentArrivee;
		updateDatas();
	}

	public EORne getCurrentUai() {
		return currentUai;
	}
	public void setCurrentUai(EORne currentUai) {
		this.currentUai = currentUai;
		myView.getTfUai().setText("");
		if (currentUai != null)
			CocktailUtilities.setTextToField(myView.getTfUai(), currentUai.libelleLong());
	}

	private EOCorps getCurrentCorps() {
		return currentCorps;
	}

	private void setCurrentCorps(EOCorps corps) {
		currentCorps = corps;
		CocktailUtilities.viderTextField(myView.getTfLibelleCorps());
		if (currentCorps != null) {
			getCurrentArrivee().setCorpsRelationship(currentCorps);
			CocktailUtilities.setTextToField(myView.getTfLibelleCorps(), currentCorps.llCorps());
		}
	}

	public void actualiser(EOArriveesDeparts mouvement) {
		setCurrentArrivee(EOArrivee.findForKey(ec, mouvement.vadId()));
	}

	private EOGrade getCurrentGrade() {
		return currentGrade;
	}
	private void setCurrentGrade(EOGrade grade) {
		currentGrade = grade;
		CocktailUtilities.viderTextField(myView.getTfLibelleGrade());
		myView.getPopupEchelons().removeAllItems();
		if (currentGrade != null) {

			getCurrentArrivee().setGradeRelationship(getCurrentGrade());
			CocktailUtilities.setTextToField(myView.getTfLibelleGrade(), currentGrade.llGrade());
			majEchelons();	

			if (getCurrentCorps() == null) {
				setCurrentCorps(currentGrade.toCorps());
			}
		}
	}
	public EOTypeAcces currentAcces() {
		return currentAcces;
	}

	public void setCurrentAcces(EOTypeAcces currentAcces) {
		this.currentAcces = currentAcces;
		CocktailUtilities.viderTextField(myView.getTfTypeAcces());
		if (currentAcces != null) {
			CocktailUtilities.setTextToField(myView.getTfTypeAcces(), currentAcces.toString());
		}
	}

	/**
	 * 
	 */
	public void clearDatas()	{

		setCurrentCorps(null);
		setCurrentGrade(null);
		setCurrentUai(null);
		setCurrentAcces(null);

		CocktailUtilities.viderTextField(myView.getTfDatePremiereAffectation());
		CocktailUtilities.viderTextField(myView.getTfIndiceMajore());
		myView.getPopupEchelons().removeAllItems();
		CocktailUtilities.viderTextField(myView.getTfCommentaires());
		CocktailUtilities.viderTextField(myView.getTfLieu());

	}

	private boolean isSaisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	/**
	 * 
	 */
	private void evaluerAffectation() {
		EOAffectation premiereAffectation = EOAffectation.rechercherPremiereAffectationPourIndividu(getEdc(), getCurrentArrivee().individu());
		if (premiereAffectation != null) {
			CocktailUtilities.setDateToField(myView.getTfDatePremiereAffectation(), premiereAffectation.dateDebut());
		}
	}
	public void ajouter(EOArrivee arrivee)	{
		setCurrentArrivee(arrivee);
	}
	public void supprimer() throws Exception {
		try {			
			getCurrentArrivee().setTemValide(CocktailConstantes.FAUX);
		}
		catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearDatas();
		if (getCurrentArrivee() != null) {

			setCurrentCorps(getCurrentArrivee().corps());
			setCurrentAcces(getCurrentArrivee().typeAcces());
			setCurrentUai(getCurrentArrivee().rneProvenance());

			CocktailUtilities.setDateToField(myView.getTfDatePremiereAffectation(), getCurrentArrivee().d1Affectation());			
			CocktailUtilities.setNumberToField(myView.getTfIndiceMajore(), getCurrentArrivee().inm());
			String echelon = getCurrentArrivee().cEchelon();
			setCurrentGrade(getCurrentArrivee().grade());
			if (echelon != null)
				myView.getPopupEchelons().setSelectedItem(echelon);

			majIndice();
			myView.getCheckFonctionPublique().setSelected(getCurrentArrivee().vientDuPrive() == false);

			CocktailUtilities.setTextToField(myView.getTfLieu(), currentArrivee.lieuArrivee());
			CocktailUtilities.setTextToField(myView.getTfCommentaires(), currentArrivee.commentaire());

		}
		updateInterface();

	}

	/**
	 * 
	 * @return
	 */
	private String getCurrentEchelon() {		
		if (myView.getPopupEchelons().countComponents() > 0) {
			return (String)myView.getPopupEchelons().getSelectedItem();
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public void majIndice() {
		CocktailUtilities.viderTextField(myView.getTfIndiceMajore());

		if (getCurrentArrivee() != null) {
			EOIndice indice = EOIndice.indiceMajorePourGradeEtEchelon(ec, getCurrentGrade(), getCurrentEchelon(), ctrlParent.getDate());
			if (indice != null) {
				CocktailUtilities.setNumberToField(myView.getTfIndiceMajore(), indice.cIndiceMajore());
			}
			else
				CocktailUtilities.setNumberToField(myView.getTfIndiceMajore(), getCurrentArrivee().inm());
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupEchelonListener implements ActionListener {		
		public void actionPerformed(ActionEvent anAction){
			majIndice();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class CheckFonctionPubliqueListener implements ActionListener {		
		public void actionPerformed(ActionEvent anAction){

			if (vientDuPrive()) {
				setCurrentCorps(null);
				setCurrentGrade(null);
				CocktailUtilities.viderTextField( myView.getTfIndiceMajore());
			}
			updateInterface();
		}
	}

	public boolean traitementAvantValidation() {
		return true;
	}

	public void valider()  {

		try {

			getCurrentArrivee().setDateDebut(ctrlParent.getDate());
			getCurrentArrivee().setDateArrete(ctrlParent.getDateArrete());
			getCurrentArrivee().setNoArrete(ctrlParent.getNumeroArrete());

			getCurrentArrivee().setVientDuPrive(vientDuPrive());
			getCurrentArrivee().setD1Affectation(CocktailUtilities.getDateFromField(myView.getTfDatePremiereAffectation()));
			getCurrentArrivee().setCorpsRelationship(getCurrentCorps());
			getCurrentArrivee().setGradeRelationship(getCurrentGrade());
			getCurrentArrivee().setTypeAccesRelationship(currentAcces());
			getCurrentArrivee().setRneProvenanceRelationship(getCurrentUai());
			getCurrentArrivee().setLieuArrivee(CocktailUtilities.getTextFromField(myView.getTfLieu()));
			getCurrentArrivee().setCommentaire(CocktailUtilities.getTextFromField(myView.getTfCommentaires()));

			getCurrentArrivee().setCEchelon(getCurrentEchelon());

			getCurrentArrivee().setInm(CocktailUtilities.getIntegerFromField(myView.getTfIndiceMajore()));

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void selectCorps() {
		EOCorps corps = CorpsSelectCtrl.sharedInstance(ec).getCorps();
		if (corps != null)	{
			setCurrentGrade(null);
			setCurrentCorps(corps);
			updateInterface();
		}
	}
	private void selectGrade() {

		if (myGradeSelector == null)
			myGradeSelector = new GradeSelectCtrl(ec);

		NSTimestamp dateReference = ctrlParent.getDate();
		EOGrade grade = myGradeSelector.getGradePourCorps(getCurrentCorps(), dateReference);
		if (grade != null) {
			setCurrentGrade(grade);
			updateInterface();
		}
	}

	private void selectTypeAcces() {
		EOTypeAcces typeAcces = TypeAccesSelectCtrl.sharedInstance(ec).getTypeAccesValideADate(ctrlParent.getDate());
		if (typeAcces != null) {
			getCurrentArrivee().setTypeAccesRelationship(typeAcces);
			setCurrentAcces(typeAcces);
		}
	}
	private void selectUai() {
		EORne rne = (EORne)UAISelectCtrl.sharedInstance(ec).getObject();
		if (rne != null)
			setCurrentUai(rne);
		updateInterface();
	}
	private void delCorps() {
		setCurrentCorps(null);
		updateInterface();
	}
	private void delGrade() {
		setCurrentGrade(null);
		updateInterface();
	}
	private void delUai() {
		setCurrentUai(null);
		updateInterface();
	}

	/**
	 * 
	 */
	private void majEchelons() {

		NSArray<EOPassageEchelon> listeEchelons = EOPassageEchelon.rechercherPassagesEchelonPourGradesValidesPourPeriode(ec, getCurrentGrade(), ctrlParent.getDate(), ctrlParent.getDate());

		myView.getPopupEchelons().removeActionListener(listenerEchelons);
		myView.getPopupEchelons().removeAllItems();
		for (int i = 0;i< listeEchelons.count();i++)	{
			myView.getPopupEchelons().addItem(listeEchelons.get(i).cEchelon());
		}
		myView.getPopupEchelons().addActionListener(listenerEchelons);
		listenerEchelons.actionPerformed(null);
	}

	private boolean vientDuPrive() {
		return vientDuPublic() == false;
	}
	private boolean vientDuPublic() {
		return myView.getCheckFonctionPublique().isSelected();
	}

	/**
	 * 
	 */
	protected void updateInterface() {

		myView.getBtnGetAcces().setEnabled(isSaisieEnabled() && getCurrentArrivee() != null);

		CocktailUtilities.initTextField(myView.getTfDatePremiereAffectation(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfLieu(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfCommentaires(), false, isSaisieEnabled());

		myView.getCheckFonctionPublique().setEnabled(isSaisieEnabled());
		myView.getBtnDelUai().setEnabled(isSaisieEnabled() && getCurrentUai() != null);

		myView.getBtnEvaluerAffectation().setEnabled(isSaisieEnabled());
		myView.getBtnGetAcces().setEnabled(isSaisieEnabled() && ctrlParent.getDate() != null);
		myView.getBtnGetCorps().setEnabled(isSaisieEnabled() && vientDuPublic());
		myView.getBtnDelCorps().setEnabled(isSaisieEnabled() && getCurrentCorps() != null && vientDuPublic());
		myView.getBtnGetGrade().setEnabled(isSaisieEnabled() && vientDuPublic());
		myView.getBtnDelGrade().setEnabled(isSaisieEnabled() && getCurrentGrade() != null && vientDuPublic());
		myView.getBtnGetUai().setEnabled(isSaisieEnabled() && vientDuPublic());
		myView.getPopupEchelons().setEnabled(isSaisieEnabled() && getCurrentGrade() != null && vientDuPublic());
		CocktailUtilities.initTextField(myView.getTfIndiceMajore(), false, isSaisieEnabled() && vientDuPublic());		
	}

	/**
	 * 
	 * @param myTextField
	 */
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateInterface();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.individu.DiplomesView;
import org.cocktail.mangue.client.select.DiplomeSelectCtrl;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.EOTitulaireDiplome;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSValidation.ValidationException;

public class DiplomesCtrl {

	private static DiplomesCtrl sharedInstance;

	private EOEditingContext	 	edc;
	private DiplomesView 			myView;

	private EODisplayGroup 			eod;
	private boolean					saisieEnabled;
	private InfosComplementairesCtrl ctrlParent;

	private ListenerDiplome 		listenerDiplome = new ListenerDiplome();
	private EOIndividuDiplomes	 	currentIndividuDiplome;
	private EODiplomes	 			currentDiplome;
	private EORne	 				currentUai;
	private EOIndividu 				currentIndividu;
	private UAISelectCtrl 			myUAISelectCtrl;

	/**
	 * 
	 * @param ctrl
	 * @param editingContext
	 */
	public DiplomesCtrl(InfosComplementairesCtrl ctrl, EOEditingContext edc) {

		setEdc(edc);
		ctrlParent = ctrl;
		eod = new EODisplayGroup();
		
		myView = new DiplomesView(new JFrame(), eod, true);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
				);
		myView.getBtnGetDiplome().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectDiplome();}
		});
		myView.getBtnGetRne().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectUai();}
		});
		myView.getBtnDelRne().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {delUai();}
		});

		setSaisieEnabled(false);

		myView.getMyEOTable().addListener(listenerDiplome);

		CocktailUtilities.initTextField(myView.getTfDiplome(), false, false);
		CocktailUtilities.initTextField(myView.getTfUai(), false, false);

		CocktailUtilities.initPopupAvecListe(myView.getPopupTitulaire(), EOTitulaireDiplome.fetchAll(edc), false);
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}



	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	/**
	 * 
	 * @return
	 */
	public EOIndividuDiplomes getCurrentIndividuDiplome() {
		return currentIndividuDiplome;
	}

	/**
	 * 
	 * @param currentIndividuDiplome
	 */
	public void setCurrentIndividuDiplome(EOIndividuDiplomes currentIndividuDiplome) {
		this.currentIndividuDiplome = currentIndividuDiplome;
		updateDatas();
	}

	/**
	 * 
	 * @return
	 */
	public EODiplomes getCurrentDiplome() {
		return currentDiplome;
	}

	/**
	 * 
	 * @param individu
	 */
	public void open(EOIndividu individu) {
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}

	/**
	 * 
	 * @param currentDiplome
	 */
	public void setCurrentDiplome(EODiplomes currentDiplome) {
		this.currentDiplome = currentDiplome;
		myView.getTfDiplome().setText("");
		if (currentDiplome != null)
			myView.getTfDiplome().setText(currentDiplome.codeEtLibelle());
	}

	public EORne getCurrentUai() {
		return currentUai;
	}

	public void setCurrentUai(EORne currentUai) {
		this.currentUai = currentUai;
		myView.getTfUai().setText("");
		if (currentUai != null)
			myView.getTfUai().setText(currentUai.libelleLong());
	}
	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public void actualiser() {
		eod.setObjectArray(EOIndividuDiplomes.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}
	private void selectDiplome() {
		EODiplomes diplome = DiplomeSelectCtrl.sharedInstance(getEdc()).getDiplome();
		if (diplome != null)
			setCurrentDiplome(diplome);
	}	
	private void selectUai() {
		if (myUAISelectCtrl == null)
			myUAISelectCtrl = new UAISelectCtrl(getEdc());
		EORne rne = (EORne)myUAISelectCtrl.getObject();
		if (rne != null)
			setCurrentUai(rne);
		updateInterface();
	}
	public void delUai() {
		setCurrentUai(null);
	}
	public JPanel getViewDiplomes() {
		return myView.getViewDiplomes();
	}
	public void toFront() {
		myView.toFront();
	}

	private void ajouter() {
		setCurrentIndividuDiplome(EOIndividuDiplomes.creer(getEdc(), getCurrentIndividu()));
		updateDatas();
		ctrlParent.setIsLocked(true);
		setSaisieEnabled(true);
		selectDiplome();
	}

	/**
	 * 
	 */
	private void modifier() {
		ctrlParent.setIsLocked(true);
		setSaisieEnabled(true);
	}

	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention","Voulez-vous vraiment supprimer ce diplôme ?", "Oui", "Non"))		
			return;
		try {
			getEdc().deleteObject(getCurrentIndividuDiplome());			
			getEdc().saveChanges();
			actualiser();
		}
		catch (Exception e) {
			getEdc().revert();
		}
	}

	/**
	 * 
	 * @return
	 */
	private boolean traitementsAvantValidation() {		
		return true;
	}

	/**
	 * 
	 */
	private void valider() {

		try {

			getCurrentIndividuDiplome().setDiplomeRelationship(getCurrentDiplome());
			getCurrentIndividuDiplome().setToTitulaireRelationship((EOTitulaireDiplome)myView.getPopupTitulaire().getSelectedItem());
			getCurrentIndividuDiplome().setUaiObtentionRelationship(getCurrentUai());

			if (!StringCtrl.chaineVide(myView.getTfAnnee().getText()))
				getCurrentIndividuDiplome().setDDiplome(DateCtrl.stringToDate(myView.getTfAnnee().getText(), "%Y"));
			else
				getCurrentIndividuDiplome().setLieuDiplome(null);

			if (!StringCtrl.chaineVide(myView.getTfLieu().getText()))
				getCurrentIndividuDiplome().setLieuDiplome(myView.getTfLieu().getText());
			else
				getCurrentIndividuDiplome().setLieuDiplome(null);

			if (!StringCtrl.chaineVide(myView.getTfSpecialite().getText()))
				getCurrentIndividuDiplome().setSpecialite(myView.getTfSpecialite().getText());
			else
				getCurrentIndividuDiplome().setSpecialite(null);

			if (!traitementsAvantValidation())
				return;

			getEdc().saveChanges();
			actualiser();
			setSaisieEnabled(false);
			ctrlParent.setIsLocked(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void annuler() {
		getEdc().revert();
		listenerDiplome.onSelectionChanged();
		setSaisieEnabled(false);
		ctrlParent.setIsLocked(false);
		updateInterface();
	}

	/**
	 * 
	 */
	private void clearDatas() {

		CocktailUtilities.viderTextField(myView.getTfDiplome());
		CocktailUtilities.viderTextField(myView.getTfLieu());
		CocktailUtilities.viderTextField(myView.getTfSpecialite());
		CocktailUtilities.viderTextField(myView.getTfAnnee());
		CocktailUtilities.viderTextField(myView.getTfUai());
		CocktailUtilities.viderTextField(myView.getTfNiveau());

	}

	/**
	 * 
	 * @return
	 */
	private boolean isSaisieEnabled() {
		return saisieEnabled;
	}

	/**
	 * 
	 * @param yn
	 */
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfAnnee(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfLieu(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfSpecialite(), false, isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentIndividuDiplome() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentIndividuDiplome() != null);

		myView.getPopupTitulaire().setEnabled(isSaisieEnabled());
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getBtnGetDiplome().setEnabled(isSaisieEnabled());
		myView.getBtnGetRne().setEnabled(isSaisieEnabled());
		myView.getBtnDelRne().setEnabled(isSaisieEnabled() && getCurrentUai() != null);

	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearDatas();
		if (getCurrentIndividuDiplome() != null) {

			setCurrentDiplome(getCurrentIndividuDiplome().diplome());
			setCurrentUai(getCurrentIndividuDiplome().uaiObtention());

			myView.getPopupTitulaire().setSelectedItem(getCurrentIndividuDiplome().toTitulaire());

			CocktailUtilities.setDateToField(myView.getTfAnnee(), getCurrentIndividuDiplome().dDiplome(), "%Y");
			CocktailUtilities.setTextToField(myView.getTfLieu(), getCurrentIndividuDiplome().lieuDiplome());
			CocktailUtilities.setTextToField(myView.getTfSpecialite(), getCurrentIndividuDiplome().specialite());
			if (getCurrentDiplome() != null && getCurrentDiplome().toNiveauDiplome() != null) {
				CocktailUtilities.setTextToField(myView.getTfNiveau(), getCurrentDiplome().toNiveauDiplome().libelleLong());
			}

		}
		updateInterface();
	}


	private class ListenerDiplome implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentDiplome != null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentIndividuDiplome((EOIndividuDiplomes)eod.selectedObject());
		}
	}

}
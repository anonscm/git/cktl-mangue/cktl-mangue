/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infoscomp;

import java.util.Enumeration;

import javax.swing.JPanel;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.client.gui.individu.DechargesView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeDechargeService;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.IndividuFonction;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOFonctionStructurelle;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuAutrFonct;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuFonctInst;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuFonctStruct;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EODecharge;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class DechargesCtrl extends ModelePageGestion {

	private DechargesView	myView;

	private EODisplayGroup 			eod;
	private InfosComplementairesCtrl ctrlParent;

	private ListenerDecharge 		listenerDecharge = new ListenerDecharge();
	private EODecharge				currentDecharge;
	private EOIndividu 				currentIndividu;

	public DechargesCtrl(InfosComplementairesCtrl ctrl, EOEditingContext edc) {

		super(edc);

		ctrlParent = ctrl;

		eod = new EODisplayGroup();
		myView = new DechargesView(null, eod, false);
		
		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		
		CocktailUtilities.initPopupAvecListe(myView.getPopupType(), EOTypeDechargeService.fetchAll(edc, EOTypeDechargeService.SORT_ARRAY_LIBELLE), true);
		myView.getMyEOTable().addListener(listenerDecharge);		

		setSaisieEnabled(false);
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}


	public EOTypeDechargeService getCurrentTypeDecharge() {
		if (myView.getPopupType().getSelectedIndex() == 0)
			return null;
		return (EOTypeDechargeService)myView.getPopupType().getSelectedItem();
	}

	public EODecharge getCurrentDecharge() {
		return currentDecharge;
	}
	public void setCurrentDecharge(EODecharge currentDecharge) {
		this.currentDecharge = currentDecharge;
		updateDatas();
	}
	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public JPanel getView() {
		return myView.getViewDecharges();
	}
	public void toFront() {
		myView.toFront();
	}
	public void open(EOIndividu individu) {
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}

	public void actualiser() {	
		eod.setObjectArray(EODecharge.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}

	private void lockCtrlParent(boolean value) {
		if (ctrlParent != null)
			ctrlParent.setIsLocked(value);
	}

	private class ListenerDecharge implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentDecharge((EODecharge)eod.selectedObject());
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfAnnee());
		CocktailUtilities.viderTextField(myView.getTfHeures());
	}
	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentDecharge() != null) {
			myView.getPopupType().setSelectedItem(getCurrentTypeDecharge());
			CocktailUtilities.setNumberToField(myView.getTfAnnee(), getCurrentDecharge().anneeUniversitaire());
			CocktailUtilities.setNumberToField(myView.getTfHeures(), getCurrentDecharge().nbHDecharge());
		}
		updateInterface();

	}
	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfHeures(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfAnnee(), false, isSaisieEnabled());
		myView.getPopupType().setEnabled(isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentDecharge() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentDecharge() != null);
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

	}
	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void traitementsAvantValidation() {
		
		getCurrentDecharge().setTypeDechargeRelationship(getCurrentTypeDecharge());
		getCurrentDecharge().setAnneeUniversitaire(CocktailUtilities.getIntegerFromField(myView.getTfAnnee()));
		getCurrentDecharge().setNbHDecharge(CocktailUtilities.getBigDecimalFromField(myView.getTfHeures()));

		// TODO Auto-generated method stub
		if (getCurrentDecharge().anneeUniversitaire() != null && getCurrentDecharge().nbHDecharge() != null && getCurrentDecharge().nbHDecharge().floatValue() > 0) {
			int annee = getCurrentDecharge().anneeUniversitaire().intValue();
			NSTimestamp dateDebut = DateCtrl.stringToDate("01/09/" + annee);
			NSTimestamp dateFin = DateCtrl.stringToDate("31/08/" + (annee + 1));

			// Verifier qu'il n'existe pas deja une decharge de ce type sur la meme periode
			NSMutableArray<EODecharge> decharges = new NSMutableArray(eod.displayedObjects());
			if (decharges.containsObject(getCurrentDecharge()) == false)
				decharges.addObject(getCurrentDecharge());
			decharges = new NSMutableArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(decharges, EODecharge.SORT_ARRAY_ANNEE_DESC));
			for (Enumeration<EODecharge> e = decharges.objectEnumerator();e.hasMoreElements();) {
				EODecharge decharge = e.nextElement();
				if (decharge != getCurrentDecharge()) {

					if (decharge.typeDecharge().equals(getCurrentDecharge().typeDecharge()) 
							&& decharge.periodeDecharge().equals(getCurrentDecharge().periodeDecharge())) {
						throw new ValidationException("Il existe déjà une décharge de ce type pour cette période !");
					}
				}
			}

			// vérifier si il existe des fonctions pour la période pour ce type de décharge et sinon afficher un warning
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(EOIndividuFonctStruct.INDIVIDU_KEY, getCurrentIndividu()));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(EOIndividuFonctStruct.FONCTION_KEY+"."+EOFonctionStructurelle.TYPE_DECHARGE_KEY, getCurrentTypeDecharge()));
			andQualifiers.addObject(SuperFinder.qualifierPourPeriode("dateDebut",dateDebut,"dateFin",dateFin));

			NSMutableArray individuFoncts = new NSMutableArray(PeriodePourIndividu.rechercherDureePourEntiteAvecCriteres(getEdc(),EOIndividuFonctStruct.ENTITY_NAME, new EOAndQualifier(andQualifiers)));
			individuFoncts.addObjectsFromArray(PeriodePourIndividu.rechercherDureePourEntiteAvecCriteres(getEdc(), EOIndividuFonctInst.ENTITY_NAME, new EOAndQualifier(andQualifiers)));
			individuFoncts.addObjectsFromArray(PeriodePourIndividu.rechercherDureePourEntiteAvecCriteres(getEdc(), EOIndividuAutrFonct.ENTITY_NAME, new EOAndQualifier(andQualifiers)));

			if (individuFoncts.size() == 0) {
				EODialogs.runInformationDialog("Attention","Vous avez défini une décharge mais il n'existe aucune fonction définie pour cet individu à cette période avec ce type de décharge");
			} else {
				// vérifier si la période complète est couverte
				// Trier le tableau par date début croissante
				EOSortOrdering.sortArrayUsingKeyOrderArray(individuFoncts,new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));
				for (java.util.Enumeration<IndividuFonction> e = individuFoncts.objectEnumerator();e.hasMoreElements();) {
					IndividuFonction individuFonct = e.nextElement();
					if (individuFonct.dateFin() == null || DateCtrl.isAfterEq(individuFonct.dateFin(),dateFin)) {
						if (DateCtrl.isAfter(individuFonct.dateDebut(),dateDebut) && DateCtrl.isSameDay(DateCtrl.jourSuivant(dateDebut),individuFonct.dateDebut()) == false) {
							// il y a un trou sur la période
							EODialogs.runInformationDialog("Attention","Vous avez défini une décharge mais aucune fonction définie avec ce type de décharge ne couvre la période complète");
						}
					} else if (DateCtrl.isAfter(individuFonct.dateDebut(),dateDebut)) {
						// il y a un trou sur la période
						EODialogs.runInformationDialog("Attention","Vous avez défini une décharge mais aucune fonction définie avec ce type de décharge ne couvre la période complète");
					} if (DateCtrl.isAfter(individuFonct.dateFin(),dateDebut)) {
						// décaler la date de début à la date de fin de cette fonction
						dateDebut = individuFonct.dateFin();
					}
				}
				// on n'est pas sorti avant, il y a un trou sur la période
				EODialogs.runInformationDialog("Attention","Vous avez défini une décharge mais aucune fonction définie avec ce type de décharge ne couvre la période complète");
			}
		}
	}
	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		lockCtrlParent(false);

	}
	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		lockCtrlParent(false);

	}
	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentDecharge(EODecharge.creer(getEdc(), getCurrentIndividu()));
		updateDatas();
		lockCtrlParent(true);
	}
	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		lockCtrlParent(true);
	}
	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(getCurrentDecharge());			
	}
	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}


}

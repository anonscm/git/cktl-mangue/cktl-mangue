// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.SaisieAssociationView;
import org.cocktail.mangue.client.select.AssociationSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieAssociationCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieAssociationCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieAssociationView myView;
	private boolean modeModification;

	private EORepartAssociation currentRepartAssociation;
	private EOAssociation currentAssociation;
	private EOIndividu currentIndividu;

	public SaisieAssociationCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		myView = new SaisieAssociationView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		myView.getBtnGetRole().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getRole();}}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateDebut());
		myView.getTfDateDebut().addActionListener(new ActionListenerDateDebut());
		myView.getTfDateFin().addFocusListener(new FocusListenerDateFin());
		myView.getTfDateFin().addActionListener(new ActionListenerDateFin());
		
		CocktailUtilities.initTextField(myView.getTfRole(), false, false);

	}

	public static SaisieAssociationCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieAssociationCtrl(editingContext);
		return sharedInstance;
	}

	private void clearTextFields()	{

		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
		myView.getTfCommentaires().setText("");
		myView.getTfRole().setText("");

	}

	public void getRole() {
		CRICursor.setWaitCursor(myView);
		EOAssociation association = AssociationSelectCtrl.sharedInstance(ec).getAssociation();

		if (association != null)	{
			currentAssociation = association;
			currentRepartAssociation.setAssociationRelationship(association);
			myView.getTfRole().setText(association.libelleLong());
		}
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 * @param individu
	 * @param structure
	 * @return
	 */
	public EORepartAssociation ajouter(EOIndividu individu, EOStructure structure)	{

		currentAssociation = null;

		clearTextFields();

		modeModification = false;

		currentIndividu = individu;
		currentRepartAssociation = EORepartAssociation.creer(ec, structure, individu, ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		myView.getTfTitre().setText("Ajout Rôle - " + individu.identitePrenomFirst());
		myView.setVisible(true);
		return currentRepartAssociation;
	}

	/**
	 * 
	 * @param repartAss
	 * @param individu
	 * @return
	 */
	public boolean modifier(EORepartAssociation repartAss, EOIndividu individu) {

		clearTextFields();

		currentRepartAssociation = repartAss;
		currentAssociation = currentRepartAssociation.association();
		currentIndividu = individu;
		myView.getTfTitre().setText("Modification Rôle - " + individu.identitePrenomFirst());

		updateData();
		modeModification = true;
		myView.setVisible(true);
		return currentRepartAssociation != null;
	}

	private void updateData() {

		if (currentAssociation != null)
			CocktailUtilities.setTextToField(myView.getTfRole(), currentAssociation.libelleLong());
		CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentRepartAssociation.dateDebut());
		CocktailUtilities.setDateToField(myView.getTfDateFin(), currentRepartAssociation.dateFin());
		CocktailUtilities.setTextToField(myView.getTfCommentaires(), currentRepartAssociation.rasCommentaire());
	
	}

	/**
	 * 
	 * @throws ValidationException
	 */
	protected void traitementsAvantValidation() throws ValidationException {			
		try {
			EORepartAssociation role = EORepartAssociation.rechercherPourIndividuStructureAssociationEtPeriode(ec, currentIndividu, currentRepartAssociation.structure(), currentAssociation.libelleLong(), currentRepartAssociation.dateDebut(), currentRepartAssociation.dateFin());
			if (role != null && role != currentRepartAssociation)
				throw new ValidationException("Ce rôle est déjà défini sur cette période ou une partie de celle-ci !");								
		}
		catch (Exception e) {
		}
		
	}

	/**
	 * 
	 */
	private void valider()  {

		try {
			
			currentRepartAssociation.setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			currentRepartAssociation.setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			currentRepartAssociation.setRasCommentaire(CocktailUtilities.getTextFromField(myView.getTfCommentaires()));

			traitementsAvantValidation();

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		if(!modeModification)
			ec.deleteObject(currentRepartAssociation);
		currentRepartAssociation = null;
		myView.setVisible(false);
	}
	
	
	private class ActionListenerDateDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateDebut().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de début saisie n'est pas valide !");
				myView.getTfDateDebut().selectAll();
			}
			else	{
				myView.getTfDateDebut().setText(myDate);
			}
		}
	}
	private class FocusListenerDateDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateDebut().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de début saisie n'est pas valide !");
				myView.getTfDateDebut().selectAll();
			}
			else
				myView.getTfDateDebut().setText(myDate);
		}
	}

	
	private class ActionListenerDateFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateFin().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin saisie n'est pas valide !");
				myView.getTfDateFin().selectAll();
			}
			else	{
				myView.getTfDateFin().setText(myDate);
			}
		}
	}
	private class FocusListenerDateFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateFin().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin saisie n'est pas valide !");
				myView.getTfDateFin().selectAll();
			}
			else
				myView.getTfDateFin().setText(myDate);
		}
	}

}

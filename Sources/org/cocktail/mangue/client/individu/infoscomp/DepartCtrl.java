// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.DepartView;
import org.cocktail.mangue.client.select.MotifDepartSelectCtrl;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOMotifDepart;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOArriveesDeparts;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.prolongations.EOEmeritat;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class DepartCtrl {
	
	private static final long serialVersionUID = 0x7be9cfa4L;
	private EOEditingContext edc;
	private DepartView myView;
	private ArriveesDepartsCtrl ctrlParent;
	private EODepart			currentDepart;
	private EOMotifDepart 		currentMotif;
	private EORne 				currentUai;
	private boolean 			saisieEnabled, modeCreation;

	/**
	 * 
	 * @param ctrl
	 * @param edc
	 */
	public DepartCtrl(ArriveesDepartsCtrl ctrl, EOEditingContext edc) {

		setEdc(edc);
		ctrlParent = ctrl;
		myView = new DepartView(new JFrame(), true);

		myView.getBtnSelectMotif().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectMotif();}}
				);
		myView.getBtnGetUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectUai();}}
				);
		myView.getBtnDelUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delUai();}}
				);

		myView.getTfDateCessation().addFocusListener(new FocusListenerDateTextField(myView.getTfDateCessation()));
		myView.getTfDateCessation().addActionListener(new ActionListenerDateTextField(myView.getTfDateCessation()));
		myView.getTfDateDecisionRadiation().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDecisionRadiation()));
		myView.getTfDateDecisionRadiation().addActionListener(new ActionListenerDateTextField(myView.getTfDateDecisionRadiation()));
		myView.getTfDateEffetRadiation().addFocusListener(new FocusListenerDateTextField(myView.getTfDateEffetRadiation()));
		myView.getTfDateEffetRadiation().addActionListener(new ActionListenerDateTextField(myView.getTfDateEffetRadiation()));

		CocktailUtilities.initTextField(myView.getTfMotifDepart(), false, false);
		CocktailUtilities.initTextField(myView.getTfUaiAcceuil(), false, false);
		CocktailUtilities.initTextField(myView.getTfDateCessation(), false, false);
		CocktailUtilities.initTextField(myView.getTfDateDeces(), false, false);

	}

	public JPanel getView() {
		return myView.getViewDepart();
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EODepart getCurrentDepart() {
		return currentDepart;
	}
	public void setCurrentDepart(EODepart currentDepart) {
		this.currentDepart = currentDepart;
		updateDatas();
	}

	public EORne currentUai() {
		return currentUai;
	}
	public void setCurrentUai(EORne currentUai) {
		this.currentUai = currentUai;
		myView.getTfUaiAcceuil().setText("");
		if (currentUai != null)
			CocktailUtilities.setTextToField(myView.getTfUaiAcceuil(), currentUai.libelleLong());
	}

	public void actualiser(EOArriveesDeparts mouvement) {
		setCurrentDepart(EODepart.findForKey(getEdc(), mouvement.vadId()));
	}

	public EOMotifDepart getCurrentMotif() {
		return currentMotif;
	}

	public void setCurrentMotif(EOMotifDepart currentMotif) {
		this.currentMotif = currentMotif;
		CocktailUtilities.viderTextField(myView.getTfMotifDepart());
		if (currentMotif != null) {
			updateDates();
			CocktailUtilities.setTextToField(myView.getTfMotifDepart(), getCurrentMotif().libelleLong());
			if (getCurrentMotif() != null && getCurrentMotif().doitFournirLieu() == false)
				setCurrentUai(null);
		}

	}

	/**
	 * 
	 */
	public void clearDatas()	{
		setCurrentMotif(null);
		CocktailUtilities.viderTextField(myView.getTfDateCessation());
		CocktailUtilities.viderTextField(myView.getTfDateDecisionRadiation());
		CocktailUtilities.viderTextField(myView.getTfDateEffetRadiation());
		CocktailUtilities.viderTextField(myView.getTfUaiAcceuil());
		CocktailUtilities.viderTextField(myView.getTfCommentaires());
	}

	public void setModeCreation(boolean yn) {
		modeCreation = yn;
	}
	private boolean isModeCreation() {
		return modeCreation;
	}
	private boolean isSaisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	public void ajouter(EODepart depart)	{
		setCurrentDepart(depart);
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void supprimer() throws Exception {
		try {			
			traitementsPourSuppression();
			getCurrentDepart().setTemValide(CocktailConstantes.FAUX);
		}
		catch (Exception ex) {
			throw ex;
		}
	}


	/**
	 * 
	 * @return
	 */
	private boolean estDepartPrevisionnel() {
		return false;
	}

	/**
	 * Mise a jour automatique des dates de radiation de et de cessation de service en fonction du motif
	 */
	public void updateDates() {

		CocktailUtilities.viderTextField(myView.getTfDateCessation());
		CocktailUtilities.viderTextField(myView.getTfDateEffetRadiation());

		if (getCurrentMotif() != null ) {

			if (getCurrentMotif().metFinACarriere() 
					&& getDateCessation() == null 
					&& ctrlParent.getDate() != null ) {

				// Pour un déces , l'agent est payé le jour du décès
				if (getCurrentMotif().estDeces()) {
					CocktailUtilities.setDateToField(myView.getTfDateCessation(), ctrlParent.getDate());
					CocktailUtilities.setDateToField(myView.getTfDateDeces(), DateCtrl.dateAvecAjoutJours(ctrlParent.getDate(), -1));
				}
				else {
					CocktailUtilities.setDateToField(myView.getTfDateCessation(), DateCtrl.jourPrecedent(ctrlParent.getDate()));
				}
			}


			if (ctrlParent.getDate() != null ) {
				NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresPourIndividuAnterieursADate(getEdc(), getCurrentDepart().individu(), ctrlParent.getDate());

				if ( carrieres.count() > 0 
						&& (estDepartPrevisionnel() == false || ctrlParent.getDateArrete() != null) 
						//&& getCurrentMotif().estDeces() == false 
						&& getCurrentMotif().metFinACarriere() 
						&& getCurrentMotif().cMotifDepartOnp() != null)

					CocktailUtilities.setDateToField(myView.getTfDateEffetRadiation(), ctrlParent.getDate());
			}
		}
	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearDatas();

		if (getCurrentDepart() != null) {

			setCurrentMotif(getCurrentDepart().motifDepart());
			setCurrentUai(getCurrentDepart().rne());

			CocktailUtilities.setDateToField(myView.getTfDateCessation(), getCurrentDepart().dCessationService());
			CocktailUtilities.setDateToField(myView.getTfDateDeces(), getCurrentDepart().individu().dDeces());
			CocktailUtilities.setDateToField(myView.getTfDateDecisionRadiation(), getCurrentDepart().dRadiationEmploi());
			CocktailUtilities.setDateToField(myView.getTfDateEffetRadiation(), getCurrentDepart().dEffetRadiation());

			if (getCurrentDepart().rne() != null)
				CocktailUtilities.setTextToField(myView.getTfUaiAcceuil(), getCurrentDepart().rne().libelleLong());
			CocktailUtilities.setTextToField(myView.getTfCommentaires(), getCurrentDepart().commentaire());
			CocktailUtilities.setTextToField(myView.getTfLieu(), getCurrentDepart().lieuDepart());

		}

		updateInterface();

	}

	/**
	 * 
	 * @return
	 */
	private NSTimestamp getDateCessation() {
		return CocktailUtilities.getDateFromField(myView.getTfDateCessation());
	}

	/**
	 * 
	 */
	public void traitementApresValidation() {

		try {

			NSTimestamp dateFin = getDateCessation();
			if (dateFin == null)
				dateFin = getCurrentDepart().dateDebut();

			enregistrerAbsencePourDepart(dateFin, getCurrentDepart());

			if (isModeCreation()) {
				if ( EODialogs.runConfirmOperationDialog("ATTENTION","Toutes les informations liées à cet agent doivent être fermées. \nAppliquer la fermeture automatique ?","Oui","Non") ) {
					EORepartAssociation.fermerAssociationsADate(getEdc() , getCurrentDepart().individu(), dateFin);
					EODepart.fermerInformationsPourAgent(getEdc() ,getCurrentDepart().individu(), dateFin, getCurrentDepart());
				}
			} else {
				getCurrentDepart().signalerDepartDansCarriereContrat();
			}

			getEdc().saveChanges();

		}
		catch (Exception e) {
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR", "Erreur de fermeture de la carrière de l'agent, merci de mettre ces données à jour manuellement.\n"+e.getMessage());
		}

	}

	/**
	 * Creation de l'absence associee au depart
	 * @param dateFin
	 */
	private void enregistrerAbsencePourDepart(NSTimestamp date, EODepart depart) {

		EOAbsences absence = depart.absence();
		if (absence == null) {
			absence = EOAbsences.creer(getEdc(), getCurrentDepart().individu(), (EOTypeAbsence)NomenclatureFinder.findForCode(getEdc(), EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_DEPART));
			getCurrentDepart().setAbsenceRelationship(absence);
		}			
		absence.setDateDebut(date);
		absence.setDateFin(null);
		absence.setAbsDureeTotale(null);

		getEdc().saveChanges();
	}

	/**
	 * 
	 * @return
	 */
	public EOEditingContext getNewEditingContext() {
		return new EOEditingContext();
	}

	/**
	 * 
	 * @return
	 */
	public void traitementAvantValidation() throws Exception {

		try {

			getCurrentDepart().setDateDebut(ctrlParent.getDate());
			getCurrentDepart().setDateArrete(ctrlParent.getDateArrete());
			getCurrentDepart().setNoArrete(ctrlParent.getNumeroArrete());

			getCurrentDepart().setDRadiationEmploi(CocktailUtilities.getDateFromField(myView.getTfDateDecisionRadiation()));
			getCurrentDepart().setDEffetRadiation(CocktailUtilities.getDateFromField(myView.getTfDateEffetRadiation()));
			getCurrentDepart().setDCessationService(CocktailUtilities.getDateFromField(myView.getTfDateCessation()));

			getCurrentDepart().setRneRelationship(currentUai());
			getCurrentDepart().setLieuDepart(CocktailUtilities.getTextFromField(myView.getTfLieu()));
			getCurrentDepart().setCommentaire(CocktailUtilities.getTextFromField(myView.getTfCommentaires()));
			getCurrentDepart().setMotifDepartRelationship(getCurrentMotif());


		}
		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}


	/**
	 * La suppression d un depart entraine la suppression des relations avec les contrats, carrieres, absences et emeritat.
	 * @return
	 */
	protected boolean traitementsPourSuppression() {

		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresPourDepart(getEdc(), getCurrentDepart(), false);
		for (Enumeration<EOCarriere> e = carrieres.objectEnumerator();e.hasMoreElements();) {
			e.nextElement().setDepartRelationship(null);
		}
		NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourDepart(getEdc(), getCurrentDepart(), false);
		for (Enumeration<EOContrat> e = contrats.objectEnumerator();e.hasMoreElements();) {
			e.nextElement().setDepartRelationship(null);
		}
		NSArray<EOEmeritat> emeritats = EOEmeritat.fetchForIndividu(getEdc(), getCurrentDepart().individu());
		for (Enumeration<EOEmeritat> e = emeritats.objectEnumerator();e.hasMoreElements();) {
			getEdc().deleteObject(e.nextElement());
		}		

		if (getCurrentDepart().absence() != null)
			getCurrentDepart().absence().setEstValide(false);
		else {
			EOAbsences absence = EOAbsences.rechercherAbsencePourIndividuDateEtTypeAbsence(getEdc(), getCurrentDepart().individu(), 
					getCurrentDepart().dateDebut(), getCurrentDepart().dateFin(), (EOTypeAbsence)NomenclatureFinder.findForCode(getEdc(), EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_DEPART));
			if (absence != null)
				absence.setEstValide(false);
		}

		return true;
	}

	/**
	 * 
	 */
	private void selectMotif() {
		INomenclature motif = MotifDepartSelectCtrl.sharedInstance(getEdc()).getObject(ctrlParent.getDate());
		if (motif != null) {
			setCurrentMotif((EOMotifDepart)motif);
		}
		updateInterface();
	}

	/**
	 * 
	 */
	private void selectUai() {
		EORne rne = (EORne)UAISelectCtrl.sharedInstance(getEdc()).getObject();
		if (rne != null)
			setCurrentUai(rne);
		updateInterface();
	}

	/**
	 * 
	 */
	private void delUai() {
		setCurrentUai(null);
		updateInterface();
	}

	/**
	 * 
	 * @return
	 */
	private boolean peutSaisirRadiation() {
		return isSaisieEnabled() 
				&& getCurrentDepart() != null 
				&& getCurrentDepart().individu() != null
				&& getCurrentDepart().individu().estTitulaire();
	}

	/**
	 * 
	 */
	public void updateInterface() {

		myView.getPanelDeces().setVisible(getCurrentDepart() != null && getCurrentMotif() != null && getCurrentMotif().estDeces());
		
		CocktailUtilities.initTextField(myView.getTfDateDecisionRadiation(), false, peutSaisirRadiation());	
		CocktailUtilities.initTextField(myView.getTfDateEffetRadiation(), false, peutSaisirRadiation());

		CocktailUtilities.initTextField(myView.getTfLieu(), false, isSaisieEnabled() && getCurrentMotif() != null 
				&& (getCurrentMotif().doitFournirLieu() || getCurrentMotif().estDeces()));
		CocktailUtilities.initTextField(myView.getTfCommentaires(), false, isSaisieEnabled());

		myView.getBtnSelectMotif().setEnabled(isSaisieEnabled() && ctrlParent.getDate() != null);
		myView.getBtnGetUai().setEnabled(isSaisieEnabled() && getCurrentMotif() != null && getCurrentMotif().doitFournirLieu());
		myView.getBtnDelUai().setEnabled(isSaisieEnabled() && currentUai() != null);

	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateInterface();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}

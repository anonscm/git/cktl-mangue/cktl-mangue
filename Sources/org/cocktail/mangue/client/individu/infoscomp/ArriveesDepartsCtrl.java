package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.individu.ArriveesDepartsView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOArrivee;
import org.cocktail.mangue.modele.mangue.individu.EOArriveesDeparts;
import org.cocktail.mangue.modele.mangue.individu.EODepart;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class ArriveesDepartsCtrl {

	private static final int INDEX_TYPE_ARRIVEE = 1;
	private static final int INDEX_TYPE_DEPART = 2;

	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private ArriveesDepartsView myView;

	private InfosComplementairesCtrl ctrlParent;
	private DepartCtrl	ctrlDepart;
	private ArriveeCtrl	ctrlArrivee;
	private ListenerMouvements myListener = new ListenerMouvements();

	private boolean modeCreation, saisieEnabled;
	private EODisplayGroup eod;
	private EOArriveesDeparts currentMouvement;
	private EOIndividu currentIndividu;

	private MyPopupListener myPopupListener = new MyPopupListener();

	public ArriveesDepartsCtrl(InfosComplementairesCtrl ctrl, EOEditingContext edc) {

		setEdc(edc);

		eod = new EODisplayGroup();
		ctrlParent = ctrl;

		myView = new ArriveesDepartsView(null, MODE_MODAL.booleanValue(),eod);
		ctrlDepart = new DepartCtrl(this, edc);
		ctrlArrivee = new ArriveeCtrl(this, edc);

		myView.getMyEOTable().addListener(myListener);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		myView.getBtnImprimerArrete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
		);

		// Initialisation des layouts
		myView.getSwapView().add("VIDE",new JPanel(new BorderLayout()));
		myView.getSwapView().add(EOArriveesDeparts.TYPE_ARRIVEE,ctrlArrivee.getView());
		myView.getSwapView().add(EOArriveesDeparts.TYPE_DEPART,ctrlDepart.getView());

		myView.getTfDate().addFocusListener(new FocusListenerDateTextField(myView.getTfDate()));
		myView.getTfDate().addActionListener(new ActionListenerDateTextField(myView.getTfDate()));
		myView.getTfDateArrete().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArrete()));
		myView.getTfDateArrete().addActionListener(new ActionListenerDateTextField(myView.getTfDateArrete()));
		myView.getBtnImprimerArrete().setEnabled(false);
		setSaisieEnabled(false);
		myView.getBtnImprimerArrete().setEnabled(false);
		myView.getPopupTypes().addActionListener(myPopupListener);

		setSaisieEnabled(false);

	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

	}

	public EOEditingContext getEdc() {
		return edc;
	}
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
	public EOArriveesDeparts getCurrentMouvement() {
		return currentMouvement;
	}

	public void setCurrentMouvement(EOArriveesDeparts currentMouvement) {
		this.currentMouvement = currentMouvement;
		updateDatas();
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public void actualiser() {
		eod.setObjectArray(EOArriveesDeparts.findForIndividu(edc, getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}
	public JPanel getViewMouvements() {
		return myView.getViewArriveesDeparts();
	}
	public void open(EOIndividu individu) {
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}
	public NSTimestamp getDate() {
		return CocktailUtilities.getDateFromField(myView.getTfDate());
	}
	public NSTimestamp getDateArrete() {
		return CocktailUtilities.getDateFromField(myView.getTfDateArrete());
	}
	public String getNumeroArrete() {
		return CocktailUtilities.getTextFromField(myView.getTfNoArrete());
	}

	public void toFront() {
		myView.toFront();
	}
	private void setModeCreation(boolean yn) {
		modeCreation = yn;
		ctrlDepart.setModeCreation(yn);
	}
	private boolean modeCreation() {
		return modeCreation;
	}

	/**
	 * 
	 */
	private void imprimer() {

		try {

			switch (myView.getPopupTypes().getSelectedIndex()) {
			case INDEX_TYPE_ARRIVEE : ctrlArrivee.imprimerArrete();break;
			}

			edc.saveChanges();
			actualiser();
			toFront();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			edc.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
		}

	}

	/**
	 * 
	 */
	private void ajouter() {

		clearTextFields();
		if (ctrlParent != null)
			ctrlParent.setIsLocked(true);
		myView.getBtnAjouter().setEnabled(false);
		myView.getBtnModifier().setEnabled(false);
		myView.getBtnSupprimer().setEnabled(false);

		myView.getMyEOTable().setEnabled(false);

		myView.getLblTypeModalite().setForeground(Color.RED);

		myView.getBtnAnnuler().setEnabled(true);
		((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), "VIDE");

		myView.getPopupTypes().setEnabled(true);

		myView.getBtnImprimerArrete().setEnabled(false);

	}

	/**
	 * 
	 */
	private void modifier() {
		setModeCreation(false);
		if (ctrlParent != null)
			ctrlParent.setIsLocked(true);
		setSaisieEnabled(true);
	}

	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer ce mouvement ?", "Oui", "Non"))		
			return;			

		try {

			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlArrivee.supprimer();break;
			case 2 : ctrlDepart.supprimer();break;
			}

			edc.saveChanges();
			actualiser();
			toFront();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			edc.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
		}
	}

	
	/**
	 * 
	 */
	private void valider() {

		CRICursor.setWaitCursor(getViewMouvements());
		try {
			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : 
				ctrlArrivee.valider();
				if (!traitementsAvantValidationArrivee())
					return;
				edc.saveChanges();
				break;
			case 2 :
				
				ctrlDepart.traitementAvantValidation();
				edc.saveChanges();
				
				ctrlDepart.traitementApresValidation();
				break;
			}

			Number noIndividu = getCurrentIndividu().noIndividu();
			NSNotificationCenter.defaultCenter().postNotification(ListeAgents.CHANGER_EMPLOYE, noIndividu,null);

			setSaisieEnabled(false);

			if (modeCreation) {
				EOArriveesDeparts  donnees = getCurrentMouvement();
				edc.invalidateObjectsWithGlobalIDs(new NSArray(edc.globalIDForObject(getCurrentMouvement())));
			actualiser();
				if (donnees != null)
					myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(donnees));
			}
			else {
				edc.invalidateObjectsWithGlobalIDs(new NSArray(edc.globalIDForObject(getCurrentMouvement())));
				myView.getMyEOTable().updateUI();
				myListener.onSelectionChanged();
			}

			if (ctrlParent != null)
				ctrlParent.setIsLocked(false);
			
			updateInterface();

		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			myView.toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR", "Erreur d'enregistrement du mouvement !\n" + e.getMessage());
		}


		CRICursor.setDefaultCursor(getViewMouvements());
		
	}

	/**
	 * 
	 */
	private void annuler() {

		try {
			edc.revert();
			ServerProxy.clientSideRequestRevert(edc);

			setSaisieEnabled(false);
			if (ctrlParent != null)
				ctrlParent.setIsLocked(false);
			myView.getLblTypeModalite().setForeground(Color.BLACK);
			myListener.onSelectionChanged();
			updateInterface();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void clearTextFields() {

		myView.getPopupTypes().setSelectedIndex(0);
		myView.getTfDate().setText("");
		myView.getTfDateArrete().setText("");
		myView.getTfNoArrete().setText("");
		ctrlArrivee.clearDatas();
		ctrlDepart.clearDatas();

	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearTextFields();

		if (getCurrentMouvement() != null) {

			CocktailUtilities.setDateToField(myView.getTfDate(), getCurrentMouvement().vadDate());
			CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentMouvement().vadDateArrete());
			CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentMouvement().vadNoArrete());

			// Swap view
			myView.getPopupTypes().removeActionListener(myPopupListener);
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), getCurrentMouvement().vadType());				

			myView.getBtnImprimerArrete().setEnabled(false);

			if (getCurrentMouvement().estArrivee()) {
				myView.getPopupTypes().setSelectedIndex(INDEX_TYPE_ARRIVEE);		
				ctrlArrivee.actualiser(getCurrentMouvement());
			}
			if (getCurrentMouvement().estDepart()) {
				myView.getPopupTypes().setSelectedIndex(INDEX_TYPE_DEPART);			
				ctrlDepart.actualiser(getCurrentMouvement());
			}

			myView.getPopupTypes().addActionListener(myPopupListener);
		}

		updateInterface();
	}

	private class ListenerMouvements implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentMouvement((EOArriveesDeparts)eod.selectedObject());
		}
	}

	private class MyPopupListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			if (myView.getPopupTypes().getSelectedIndex() > 0) {

				setModeCreation(true);
				setSaisieEnabled(true);
				myView.getLblTypeModalite().setForeground(Color.BLACK);

				switch (myView.getPopupTypes().getSelectedIndex()) {
				case INDEX_TYPE_ARRIVEE : gererArrivee();break;
				case INDEX_TYPE_DEPART : gererDepart();break;
				}

				myView.getPopupTypes().setEnabled(false);
			}
		}
	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {

		saisieEnabled = yn;
		switch (myView.getPopupTypes().getSelectedIndex()) {
		case INDEX_TYPE_ARRIVEE : ctrlArrivee.setSaisieEnabled(saisieEnabled());break;
		case INDEX_TYPE_DEPART : ctrlDepart.setSaisieEnabled(saisieEnabled());break;
		}

		updateInterface();
	}


	private void traitementsPourCreationDepart() {

	}

	private void traitementsPourCreationArrivee() {
	}
	private boolean traitementsAvantValidationArrivee() {
		return ctrlArrivee.traitementAvantValidation();
	}

	private void gererArrivee() {
		if (modeCreation) {
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), EOArriveesDeparts.TYPE_ARRIVEE);				
			traitementsPourCreationArrivee();
			ctrlArrivee.ajouter(EOArrivee.creer(edc, getCurrentIndividu()));			
		}
	}
	
	/**
	 * 
	 */
	private void gererDepart() {
		if (modeCreation) {
			((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), EOArriveesDeparts.TYPE_DEPART);				
			traitementsPourCreationDepart();
			ctrlDepart.ajouter(EODepart.creer(edc, getCurrentIndividu()));			
		}		
	}

	private boolean estTypeDepart() {
		return myView.getPopupTypes().getSelectedIndex() == INDEX_TYPE_DEPART;
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null);
		myView.getBtnModifier().setEnabled(getCurrentMouvement() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentMouvement() != null);

		//	myView.getBtnImprimerArrete().setEnabled(!saisieEnabled() && !modeCreation() && currentMouvement() != null);

		myView.getMyEOTable().setEnabled(!saisieEnabled());

		myView.getPopupTypes().setEnabled(saisieEnabled() && modeCreation());	
		CocktailUtilities.initTextField(myView.getTfDate(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateArrete(), false, saisieEnabled());		
		CocktailUtilities.initTextField(myView.getTfNoArrete(), false, saisieEnabled());	
		
		ctrlArrivee.updateInterface();
		ctrlDepart.updateInterface();
		
	}

	/**
	 * 
	 * @param myTextField
	 */
	private void dateHasChanged(JTextField myTextField) {
		
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			if (myTextField.equals(myView.getTfDate()))
				ctrlDepart.updateDates();
			updateInterface();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}
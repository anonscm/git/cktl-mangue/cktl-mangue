/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.individu.FormationsView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeUniteTemps;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuFormations;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class FormationsCtrl extends ModelePageGestion {

	private FormationsView 			myView;
	private EODisplayGroup 			eod;
	private InfosComplementairesCtrl ctrlParent;

	private TableListener 			listenerFormation = new TableListener();
	private EOIndividuFormations	currentIndividuFormation;
	private EOTypeUniteTemps		currentUniteTemps;
	private EOIndividu 				currentIndividu;

	public FormationsCtrl(InfosComplementairesCtrl ctrl, EOEditingContext edc) {

		super(edc);
		ctrlParent = ctrl;
		eod = new EODisplayGroup();
		myView = new FormationsView(null, eod, false);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		
		myView.getBtnCalculerDuree().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {calculerDuree();}}
		);
		
		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		setSaisieEnabled(false);

		myView.setDurees(EOTypeUniteTemps.fetchAll(getEdc()));
		myView.getMyEOTable().addListener(listenerFormation);						
		myView.getPopupDuree().addActionListener(new PopupDureeListener());						
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}
	public void open(EOIndividu individu) {
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}
	public EOIndividuFormations getCurrentIndividuFormation() {
		return currentIndividuFormation;
	}

	public void setCurrentIndividuFormation(EOIndividuFormations currentIndividuFormation) {
		this.currentIndividuFormation = currentIndividuFormation;
		updateDatas();
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EOTypeUniteTemps currentUniteTemps() {
		return currentUniteTemps;
	}

	public void setCurrentUniteTemps(EOTypeUniteTemps currentUniteTemps) {
		this.currentUniteTemps = currentUniteTemps;
		myView.getPopupDuree().setSelectedIndex(0);
		if (currentUniteTemps != null)
			myView.getPopupDuree().setSelectedItem(currentUniteTemps);
			
	}

	private class PopupDureeListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			if (myView.getPopupDuree().getSelectedIndex() > 0)	
				setCurrentUniteTemps((EOTypeUniteTemps)myView.getPopupDuree().getSelectedItem());
			else
				setCurrentUniteTemps(null);
			updateInterface();
		}
	}

	public void actualiser() {
		eod.setObjectArray(EOIndividuFormations.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}

	public void calculerDuree() {
		if ( (getDateDebut() == null || getDateFin() == null ||
				DateCtrl.isAfter(getDateDebut(), getDateFin()))
				|| currentUniteTemps() == null || !currentUniteTemps().estDureeEnJours() )
			myView.getTfDuree().setText("");
		else {
			int nbJours = DateCtrl.nbJoursEntre(getDateDebut(), getDateFin(), true);
			myView.getTfDuree().setText(String.valueOf(nbJours));
		}
	}
	
	public JPanel getViewFormations() {
		return myView.getViewFormations();
	}
	public void toFront() {
		myView.toFront();
	}

	public NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	public void setDateDebut(NSTimestamp myDateDebut) {
		myView.getTfDateDebut().setText(DateCtrl.dateToString(myDateDebut));
	}

	public NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}
	public void setDateFin(NSTimestamp myDateFin) {
		myView.getTfDateFin().setText(DateCtrl.dateToString(myDateFin));
	}

	private void lockCtrlParent(boolean value) {
		if (ctrlParent != null)
			ctrlParent.setIsLocked(value);
	}

	private class TableListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentIndividuFormation() != null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentIndividuFormation((EOIndividuFormations)eod.selectedObject());
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfDuree());
		CocktailUtilities.viderTextField(myView.getTfFormation());	

	}
	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentIndividuFormation() != null) {

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentIndividuFormation().dateDebut(), "%d/%m/%Y");
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentIndividuFormation().dateFin(), "%d/%m/%Y");
			CocktailUtilities.setTextToField(myView.getTfDuree(), getCurrentIndividuFormation().duree());
			CocktailUtilities.setTextToField(myView.getTfFormation(), getCurrentIndividuFormation().llFormation());
			setCurrentUniteTemps(getCurrentIndividuFormation().typeDuree());
			
		}
		updateInterface();

	}
	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());
		
		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfFormation(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDuree(), false, isSaisieEnabled());
		myView.getPopupDuree().setEnabled(isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentIndividuFormation() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentIndividuFormation() != null);
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());
		myView.getBtnCalculerDuree().setEnabled(isSaisieEnabled() && currentUniteTemps() != null && currentUniteTemps().estDureeEnJours());

	}
	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
	}
	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		
		getCurrentIndividuFormation().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentIndividuFormation().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
		getCurrentIndividuFormation().setTypeDureeRelationship(currentUniteTemps());
		
		getCurrentIndividuFormation().setDuree(CocktailUtilities.getTextFromField(myView.getTfDuree()));
		getCurrentIndividuFormation().setLlFormation(CocktailUtilities.getTextFromField(myView.getTfFormation()));

		// Teste si les périodes se chevauchent : ALERTE, pas de blocage
		for ( EOIndividuFormations formation : (NSArray<EOIndividuFormations>) eod.displayedObjects()) {
			if (formation != getCurrentIndividuFormation()) {
				if (getCurrentIndividuFormation().dateFin() == null || 
						(DateCtrl.isBetween(formation.dateDebut(), formation.dateFin(),
								getCurrentIndividuFormation().dateDebut(), getCurrentIndividuFormation().dateFin()) ))
					throw new ValidationException("Cette personne a déjà une formation pendant cette période !");
			}
		}
	}
	
	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		lockCtrlParent(false);
	}
	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		lockCtrlParent(false);
	}
	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentIndividuFormation(EOIndividuFormations.creer(getEdc(), getCurrentIndividu()));
		lockCtrlParent(true);
		updateDatas();
	}
	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(getCurrentIndividuFormation());
	}
	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		actualiser();
	}
	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		lockCtrlParent(true);
	}
	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}
}

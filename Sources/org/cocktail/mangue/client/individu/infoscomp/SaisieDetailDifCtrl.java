// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.SaisieDetailDifView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.individu.EODif;
import org.cocktail.mangue.modele.mangue.individu.EODifDetail;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisieDetailDifCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieDetailDifCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieDetailDifView myView;

	private EODifDetail currentDetail;

	public SaisieDetailDifCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieDetailDifView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));

	}

	public static SaisieDetailDifCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieDetailDifCtrl(editingContext);
		return sharedInstance;
	}



	public EODifDetail currentDetail() {
		return currentDetail;
	}

	public void setCurrentDetail(EODifDetail currentDetail) {
		this.currentDetail = currentDetail;
		updateData();
	}

	public EODifDetail ajouter(EODif dif)	{
		
		setCurrentDetail(EODifDetail.creer(ec, dif));		
		myView.setVisible(true);
		return currentDetail();
		
	}
	public void modifier(EODifDetail detail)	{
		setCurrentDetail(detail);
		myView.setVisible(true);
	}

	private void updateData() {
		if (currentDetail() != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentDetail().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), currentDetail().dateFin());
			CocktailUtilities.setNumberToField(myView.getTfDuree(), currentDetail().difDetailDebit());
			CocktailUtilities.setTextToField(myView.getTfFormation(), currentDetail().difDetailLibelle());
		}
	}

	private void valider()  {
		try {
			currentDetail().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			currentDetail().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			currentDetail().setDifDetailDebit(CocktailUtilities.getIntegerFromField(myView.getTfDuree()));
			currentDetail().setDifDetailLibelle(CocktailUtilities.getTextFromField(myView.getTfFormation()));

			ec.saveChanges();
			myView.setVisible(false);
		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		ec.revert();
		setCurrentDetail(null);
		myView.setVisible(false);
	}

	private class ActionListenerDateTextField implements ActionListener	{

		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myTextField.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else {
				myTextField.setText(myDate);
			}
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else {
				myTextField.setText(myDate);
			}
		}
	}	



}

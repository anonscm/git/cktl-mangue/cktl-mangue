/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infoscomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.PeriodesMilitairesView;
import org.cocktail.mangue.client.select.TypePeriodeMilitSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOSituationMilitaire;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypePeriodeMilit;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodesMilitaires;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class PeriodesMilitairesCtrl {

	private static PeriodesMilitairesCtrl sharedInstance;

	private EOEditingContext ec;
	private PeriodesMilitairesView 			myView;

	private EODisplayGroup 			eod;

	private boolean					modeSaisie;

	private InfosComplementairesCtrl ctrlParent;
	private ListenerPeriode 		listenerPeriode = new ListenerPeriode();
	private ListenerSituation 		listenerSituation = new ListenerSituation();
	private EOPeriodesMilitaires	currentPeriode;
	private EOTypePeriodeMilit		currentType;
	private EOSituationMilitaire	currentSituation;
	private EOIndividu 				currentIndividu;

	public PeriodesMilitairesCtrl(InfosComplementairesCtrl ctrl, EOEditingContext editingContext) {

		ec = editingContext;
		ctrlParent = ctrl;
		eod = new EODisplayGroup();
		myView = new PeriodesMilitairesView(null, eod, false);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
				);
		myView.getBtnGetType().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getTypePeriode();}
		});

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));

		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));

		setSaisieEnabled(false);

		myView.getMyEOTable().addListener(listenerPeriode);
		CocktailUtilities.initTextField(myView.getTfType(), false, false);
		myView.getPopupSituations().addActionListener(listenerSituation);
		myView.setSituations(EOSituationMilitaire.fetchAll(ec));
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
	}

	private EOPeriodesMilitaires currentPeriode() {
		return currentPeriode;
	}
	private void setCurrentPeriode(EOPeriodesMilitaires periode) {
		currentPeriode = periode;
	}
	private EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public EOTypePeriodeMilit getCurrentType() {
		return currentType;
	}

	public void setCurrentType(EOTypePeriodeMilit currentType) {
		this.currentType = currentType;
	}

	public EOSituationMilitaire getCurrentSituation() {
		return currentSituation;
	}

	public void setCurrentSituation(EOSituationMilitaire currentSituation) {
		this.currentSituation = currentSituation;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public void open(EOIndividu individu)	{

		currentIndividu = individu;
		if (currentIndividu != null) {
			actualiser();
		}

		myView.setVisible(true);
	}

	private class ListenerSituation implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {

			try {

				setCurrentSituation(null);

				if (getCurrentIndividu() != null) {

					if (myView.getPopupSituations().getSelectedIndex() > 0)
						setCurrentSituation((EOSituationMilitaire)myView.getPopupSituations().getSelectedItem());

					getCurrentIndividu().setToSituationMilitaireRelationship(currentSituation);

					ec.saveChanges();

				}

				updateUI();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void actualiser() {

		if (currentIndividu() == null)
			setCurrentSituation(null);
		else {
			setCurrentSituation(currentIndividu().toSituationMilitaire());
			myView.getPopupSituations().setSelectedItem(currentSituation);	
		}

		eod.setObjectArray(EOPeriodesMilitaires.findForIndividu(ec, currentIndividu()));
		myView.getMyEOTable().updateData();
		updateUI();
	}

	private void getTypePeriode() {
		EOTypePeriodeMilit type = TypePeriodeMilitSelectCtrl.sharedInstance(ec).getType();

		if (type != null) {
			currentType =type;
			myView.getTfType().setText(currentType.libelleLong());
		}
	}

	public JPanel getViewPeriodesMilitaires() {
		return myView.getViewPeriodesMilitaires();
	}
	public void toFront() {
		myView.toFront();
	}

	private void ajouter() {

		setCurrentPeriode(EOPeriodesMilitaires.creer(ec, currentIndividu()));
		ctrlParent.setIsLocked(true);
		updateData();
		setSaisieEnabled(true);
	}

	private void modifier() {
		ctrlParent.setIsLocked(true);
		setSaisieEnabled(true);
	}

	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention","Voulez-vous vraiment supprimer cette période ?", "Oui", "Non"))		
			return;

		try {

			ec.deleteObject(currentPeriode());

			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			ec.revert();
		}
	}

	private boolean traitementsAvantValidation() {

		if (currentPeriode.dateDebut() == null || currentPeriode.dateFin() == null)
			return true;

		// Teste si la periode saisie ne chevauche pas une autre période
		NSArray periodesMil = eod.displayedObjects();
		for (java.util.Enumeration<EOPeriodesMilitaires> e = periodesMil.objectEnumerator();e.hasMoreElements();) {
			EOPeriodesMilitaires periode = e.nextElement();
			if (periode != currentPeriode()) {
				if ((DateCtrl.isAfter(periode.dateDebut(),currentPeriode().dateDebut()) &&
						DateCtrl.isBefore(periode.dateDebut(),currentPeriode().dateFin())) ||
						(DateCtrl.isAfter(currentPeriode().dateDebut(),periode.dateDebut()) &&
								DateCtrl.isBefore(currentPeriode().dateDebut(),periode.dateFin()))) {
					EODialogs.runErrorDialog("Alerte","Les périodes se chevauchent");
					return false;
				}
			}
		}
		// Vérifier si il existe des changements de position avec service national
		NSArray changements = EOChangementPosition.rechercherChangementsServiceNationalPourIndividu(ec, currentIndividu());
		if (changements != null && changements.count() > 0) {
			boolean periodeTrouvee = false;
			for (java.util.Enumeration<EOChangementPosition> e = changements.objectEnumerator();e.hasMoreElements();) {
				EOChangementPosition changement = e.nextElement();
				if (DateCtrl.isSameDay(changement.dateDebut(), currentPeriode().dateDebut()) &&
						DateCtrl.isSameDay(changement.dateFin(), currentPeriode().dateFin())) {
					periodeTrouvee = true;
					break;
				}
			}
			if (!periodeTrouvee) {
				EODialogs.runInformationDialog("Attention", "Il existe dans les changements de position, d'autres périodes que celle-ci");
			}
		}
		return true;
	}


	private void valider() {

		try {

			if (!StringCtrl.chaineVide(myView.getTfDateDebut().getText()))
				currentPeriode.setDateDebut(DateCtrl.stringToDate(myView.getTfDateDebut().getText()));
			else
				currentPeriode.setDateDebut(null);

			if (!StringCtrl.chaineVide(myView.getTfDateFin().getText()))
				currentPeriode.setDateFin(DateCtrl.stringToDate(myView.getTfDateFin().getText()));
			else
				currentPeriode.setDateFin(null);

			currentPeriode.setToTypePeriodeRelationship(currentType);

			if (!traitementsAvantValidation())
				return;

			ec.saveChanges();
			actualiser();
			setSaisieEnabled(false);
			ctrlParent.setIsLocked(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {
		ec.revert();
		listenerPeriode.onSelectionChanged();
		setSaisieEnabled(false);
		ctrlParent.setIsLocked(false);
		updateUI();
	}

	private void clearTextFields() {
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
		myView.getTfType().setText("");	
	}
	private void setSaisieEnabled(boolean yn) {
		modeSaisie = yn;
		updateUI();
	}



	/**
	 * 
	 * Gestion de l'activation de tous les objets de l'interface.
	 * Modification en mode saisie uniquement.
	 * Les adresses de type FACTURATION ne peuvent être modifiees.
	 * 
	 * La ville est déduite automatiquement s'il s'agit d'un pasy ETRANGER 
	 * 
	 */
	private void updateUI() {

		myView.getMyEOTable().setEnabled(!modeSaisie);

		myView.getPopupSituations().setEnabled(currentIndividu() != null && eod.displayedObjects().count() == 0);

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, modeSaisie);
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, modeSaisie);

		myView.getBtnAjouter().setEnabled(!modeSaisie && currentSituation != null && currentSituation.temPeriodeMilitair().equals("O"));		
		myView.getBtnModifier().setEnabled(!modeSaisie && currentPeriode != null);
		myView.getBtnSupprimer().setEnabled(!modeSaisie && currentPeriode != null);

		myView.getBtnValider().setEnabled(modeSaisie);
		myView.getBtnAnnuler().setEnabled(modeSaisie);

		myView.getBtnGetType().setEnabled(modeSaisie);

	}

	/**
	 * 
	 */
	private void updateData() {

		clearTextFields();

		if (currentPeriode != null) {

			setCurrentType(currentPeriode.toTypePeriode());			

			if (currentType != null)
				CocktailUtilities.setTextToField(myView.getTfType(), currentPeriode.toTypePeriode().libelleLong());

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentPeriode.dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), currentPeriode.dateFin());
		}
	}


	private class ListenerPeriode implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentPeriode != null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentPeriode((EOPeriodesMilitaires)eod.selectedObject());
			updateData();
			updateUI();
		}
	}


	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infosperso;
/** Interface pour gerer les adresses Personnelles ou Professionnelles des agents */

/* Une même adresse peut être associée à plusieurs RepartPersonneAdresse différenciés par le type d'adresse. Une seule rpa correspond à l'adresse principale
 * Ici, on ne gère que les adresses de type personnel ou professionnel. Un agent peut donc avoir au plus 2 rpas.
 * Le display group contient toutes les rpas professionnelles et personnels d'un même individu mais il n'affiche que les rpa valides
 * et ils ôtent les doublons dans le cas où une même adresse est associée à deux rpas.
 * Lorsque l'utilisateur crée une adresse ou la modifie, on crée  éventuellement des rpas si l'utilisateur a choisi 
 * les rpas pour les deux types professionnel et personnel
 * Lorsque l'utilisateur supprime une adresse, on invalide les rpas associées à cette adresse si elle a d'autres rpas (fournis,…) associées sinon
 * on détruit les rpas et l'adresse
 * Lorsque l'utilisateur choisit une rpa comme adresse principale, la rpa de l'adresse principale précédente est désactivée comme adresse princpale.
 *
 * Si une adresse apparaît avec un code postal et un popup vide, c'est que la commune n'est pas choisie
 * ou qu'elle est invalide
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.individu.TelephonesView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeNoTel;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeTel;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonneTelephone;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSValidation.ValidationException;

public class TelephonesCtrl extends ModelePageGestion {

	private static Boolean MODE_MODAL = Boolean.FALSE;
	private boolean					peutGererModule;

	private TelephonesView 			myView;

	private EODisplayGroup 			eod;
	private ListenerTelephone 		listenerTelephone = new ListenerTelephone();

	private EOPersonneTelephone		currentTelephone;
	private EOPersonneTelephone		oldTelephone;
	private EOIndividu 				currentIndividu;
	private InfosPersonnellesCtrl	ctrlParent;

	public TelephonesCtrl(EOEditingContext edc, InfosPersonnellesCtrl ctrlParent, boolean loadNotifications) {

		super(edc);
		this.ctrlParent = ctrlParent;

		eod = new EODisplayGroup();
		myView = new TelephonesView(null, eod, MODE_MODAL.booleanValue());

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());


		setSaisieEnabled(false);
		
		myView.getMyEOTable().addListener(listenerTelephone);
		CocktailUtilities.initPopupAvecListe(myView.getPopupOutils(), EOTypeNoTel.rechercher(getEdc()), false);
		CocktailUtilities.initPopupAvecListe(myView.getPopupTypes(), EOTypeTel.rechercher(getEdc()), false);

		CocktailUtilities.viderLabel(myView.getLblMessage());
		
	}

	public void open(EOIndividu individu)	{
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}
	
	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
				
		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		
		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());

	}

	private EOPersonneTelephone getCurrentTelephone() {
		return currentTelephone;
	}
	public void setCurrentTelephone(EOPersonneTelephone telephone) {
		currentTelephone = telephone;
		updateDatas();
	}
	private EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu individu) {
		currentIndividu = individu;
		actualiser();
	}
	public void actualiser() {
		
		eod.setObjectArray(EOPersonneTelephone.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		
		EOPersonneTelephone telPrincipal = EOPersonneTelephone.rechercheTelPrincipal(getEdc(), getCurrentIndividu());
		
		CocktailUtilities.viderLabel(myView.getLblMessage());
		if (telPrincipal == null && eod.displayedObjects().count() > 0)
			CocktailUtilities.setTextToLabel(myView.getLblMessage(), "ATTENTION ! Cet agent n'a pas de téléphone principal.");
		
		updateInterface();
	}
	
	public JPanel getViewTelephones() {
		return myView.getViewTelephone();
	}

	/** methode a surcharger pour faire des traitements avant l'enregistrement des donnees dans la base
	 * retourne true si on peut enregistrer */
	protected void traitementsAvantValidation() {

		if (myView.getTfNumero().getText().length() == 0)
			throw new ValidationException("Veuillez entrer un numéro !");

		if (myView.getTfNumero().getText().length() > 0)
			getCurrentTelephone().setNoTelephone(EOPersonneTelephone.nettoyerEtFormater(myView.getTfNumero().getText()));
		else
			getCurrentTelephone().setNoTelephone(null);
		if (myView.getTfIndicatif().getText().length() > 0)
			getCurrentTelephone().setIndicatif(new Integer(EOPersonneTelephone.nettoyerEtFormater(myView.getTfIndicatif().getText())));
		else
			getCurrentTelephone().setIndicatif(null);

		getCurrentTelephone().setToTypeNoRelationship((EOTypeNoTel)myView.getPopupOutils().getSelectedItem());				
		getCurrentTelephone().setTypeNo(((EOTypeNoTel)myView.getPopupOutils().getSelectedItem()).code());
		getCurrentTelephone().setToTypeTelRelationship((EOTypeTel)myView.getPopupTypes().getSelectedItem());
		getCurrentTelephone().setTypeTel(((EOTypeTel)myView.getPopupTypes().getSelectedItem()).code());

		getCurrentTelephone().setEstSurListeRouge(myView.getCheckListeRouge().isSelected());
		getCurrentTelephone().setEstTelPrincipal(myView.getCheckPrincipal().isSelected());

		String typeNo = ((EOTypeNoTel)myView.getPopupOutils().getSelectedItem()).code();

		String numeroFormate = EOPersonneTelephone.nettoyerEtFormater(getCurrentTelephone().noTelephone());
		EOTypeTel typeTel = getCurrentTelephone().toTypeTel();
		// vérifier si ce numéro de téléphone existe déjà car clé primaire. On prend allObjects à cause des filtres sur le dg
		//java.util.Enumeration e = displayGroup().allObjects().objectEnumerator();
		// 22/07/09- Demande P. Olive : la contrainte de clé primaire se fait sur le type no, type tel, numéro et persID
		// on ne vérifie que les displayed objects et on vérifie typeTel et TypeNo
		for (java.util.Enumeration<EOPersonneTelephone> e = eod.displayedObjects().objectEnumerator();e.hasMoreElements();) {
			EOPersonneTelephone telephone = e.nextElement();
			if (telephone != getCurrentTelephone() && telephone.noTelephone().equals(numeroFormate) && 
					telephone.typeNo().equals(typeNo) &&
					telephone.typeTel().equals(typeTel.code())) {
				throw new ValidationException("Ce numéro de téléphone est déjà défini pour cette personne");
			}
		}
		if (getCurrentTelephone().estTelPrincipal()) {
			for (java.util.Enumeration<EOPersonneTelephone> e1 = eod.displayedObjects().objectEnumerator();e1.hasMoreElements();) {
				EOPersonneTelephone telephone = e1.nextElement();
				if (telephone != getCurrentTelephone() && telephone.typeTel().equals(typeTel.code()) && telephone.estTelPrincipal())
					telephone.setEstTelPrincipal(false);
			}
		}

		/**
		 *  Dans le cas d'une modification, on recree le numero de telephone afin d'eviter le blocage de modification de cle primaire
		 */
		if (!isModeCreation()) {
			boolean recreerNumero = 
				(oldTelephone.typeTel().equals(currentTelephone.typeTel())) == false
				|| (EOPersonneTelephone.nettoyerEtFormater(oldTelephone.noTelephone()).equals(EOPersonneTelephone.nettoyerEtFormater(currentTelephone.noTelephone()))) == false
				|| (oldTelephone.typeNo().equals(currentTelephone.typeNo())) == false;

			if (recreerNumero) {
				EOPersonneTelephone newTelephone = new EOPersonneTelephone();
				newTelephone.takeValuesFromDictionary(getCurrentTelephone().snapshot());
				getEdc().insertObject(newTelephone);
				getEdc().deleteObject(getCurrentTelephone());
				setCurrentTelephone(newTelephone);
			}
		}
	}
	private class ListenerTelephone implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentTelephone != null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentTelephone((EOPersonneTelephone)eod.selectedObject());
		}
	}
	
	private void lockParent(boolean value) {
		if (ctrlParent != null) {
			ctrlParent.setIsLocked(value);
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getPopupOutils().setSelectedIndex(0);
		myView.getPopupTypes().setSelectedIndex(0);
		myView.getCheckPrincipal().setSelected(false);
		myView.getCheckListeRouge().setSelected(false);
		myView.getTfNumero().setText("");
		myView.getTfIndicatif().setText("");

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentTelephone() != null) {

			CocktailUtilities.setTextToField(myView.getTfNumero(), getCurrentTelephone().noTelephone());
			CocktailUtilities.setNumberToField(myView.getTfIndicatif(), getCurrentTelephone().indicatif());

			myView.getPopupTypes().setSelectedItem(getCurrentTelephone().toTypeTel());
			myView.getPopupOutils().setSelectedItem(getCurrentTelephone().toTypeNo());

			myView.getCheckListeRouge().setSelected(getCurrentTelephone().estSurListeRouge());
			myView.getCheckPrincipal().setSelected(getCurrentTelephone().estTelPrincipal());
		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentTelephone() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentTelephone() != null);

		CocktailUtilities.initTextField(myView.getTfNumero(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfIndicatif(), false, isSaisieEnabled());

		myView.getPopupOutils().setEnabled(isSaisieEnabled());
		myView.getPopupTypes().setEnabled(isSaisieEnabled());
		myView.getCheckListeRouge().setEnabled(isSaisieEnabled());
		myView.getCheckPrincipal().setEnabled(isSaisieEnabled());

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		lockParent(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		listenerTelephone.onSelectionChanged();
		lockParent(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentTelephone(EOPersonneTelephone.creer(getEdc(), getCurrentIndividu()));
		lockParent(true);
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		oldTelephone = new EOPersonneTelephone();
		oldTelephone.takeValuesFromDictionary(getCurrentTelephone().snapshot());
		lockParent(true);
	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(getCurrentTelephone());
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}
}

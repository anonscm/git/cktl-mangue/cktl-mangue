/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infosperso;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.individu.AutorisationTravailView;
import org.cocktail.mangue.client.gui.individu.SituationFamilialeView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOAutorisationTravail;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuFamiliale;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class AutorisationTravailCtrl extends ModelePageGestion {

	private static AutorisationTravailCtrl sharedInstance;

	private AutorisationTravailView	myView;

	private EODisplayGroup 			eod;
	private boolean					peutGererModule;

	private ListenerAutorisation 	listenerAutorisation = new ListenerAutorisation();
	private EOAutorisationTravail	currentAutorisation;
	private EOIndividu 				currentIndividu;

	public AutorisationTravailCtrl(EOEditingContext edc) {

		super(edc);
		eod = new EODisplayGroup();
		myView = new AutorisationTravailView(null, eod, false);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		myView.getMyEOTable().addListener(listenerAutorisation);		
		myView.setTitle("Autorisations de travail");

		setSaisieEnabled(false);
	}

	public static AutorisationTravailCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new AutorisationTravailCtrl(editingContext);
		return sharedInstance;
	}



	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());

	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public EOAutorisationTravail getCurrentAutorisation() {
		return currentAutorisation;
	}

	public void setCurrentAutorisation(EOAutorisationTravail currentAutorisation) {
		this.currentAutorisation = currentAutorisation;
		updateDatas();
	}

	public void actualiser() {	
		eod.setObjectArray(EOAutorisationTravail.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}

	public JPanel getViewAutorisations() {
		return myView.getViewSituations();
	}
	public void open() {
		myView.setVisible(true);
	}

	private class ListenerAutorisation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentAutorisation((EOAutorisationTravail)eod.selectedObject());
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());		
		myView.getCheckRenouvellement().setSelected(false);
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentAutorisation() != null) {

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentAutorisation().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentAutorisation().dateFin());
			myView.getCheckRenouvellement().setSelected(getCurrentAutorisation().isRenouvellement());

		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentAutorisation() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentAutorisation() != null);
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsAvantValidation() {
		
		// TODO Auto-generated method stub
		if (CocktailUtilities.getDateFromField(myView.getTfDateDebut()) != null) {

			getCurrentAutorisation().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			getCurrentAutorisation().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			getCurrentAutorisation().setEstRenouvellement(myView.getCheckRenouvellement().isSelected());
		}
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		listenerAutorisation.onSelectionChanged();

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentAutorisation(EOAutorisationTravail.creer(getEdc(), getCurrentIndividu()));
		myView.getTfDateDebut().requestFocus();
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(getCurrentAutorisation());			
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub

	}	

}

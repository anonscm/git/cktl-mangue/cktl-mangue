/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infosperso;
/** Interface pour gerer les adresses Personnelles ou Professionnelles des agents */

/* Une même adresse peut être associée à plusieurs RepartPersonneAdresse différenciés par le type d'adresse. Une seule rpa correspond à l'adresse principale
 * Ici, on ne gère que les adresses de type personnel ou professionnel. Un agent peut donc avoir au plus 2 rpas.
 * Le display group contient toutes les rpas professionnelles et personnels d'un même individu mais il n'affiche que les rpa valides
 * et ils ôtent les doublons dans le cas où une même adresse est associée à deux rpas.
 * Lorsque l'utilisateur crée une adresse ou la modifie, on crée  éventuellement des rpas si l'utilisateur a choisi 
 * les rpas pour les deux types professionnel et personnel
 * Lorsque l'utilisateur supprime une adresse, on invalide les rpas associées à cette adresse si elle a d'autres rpas (fournis,…) associées sinon
 * on détruit les rpas et l'adresse
 * Lorsque l'utilisateur choisit une rpa comme adresse principale, la rpa de l'adresse principale précédente est désactivée comme adresse princpale.
 *
 * Si une adresse apparaît avec un code postal et un popup vide, c'est que la commune n'est pas choisie
 * ou qu'elle est invalide
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.individu.AdressesNormeeView;
import org.cocktail.mangue.client.select.PaysSelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOCommune;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.mangue.common.modele.nomenclatures.EOVoirie;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AdressesNormeesCtrl {

	private static final int INDEX_ADRESSES_VALIDES = 0;
	private static AdressesNormeesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;

	private AdressesNormeeView 			myView;
	private AdressesRenderer		monRendererColor = new AdressesRenderer();

	private EODisplayGroup 			eod;
	private boolean					peutGererModule;
	private boolean					saisieEnabled, modeCreation, estNormalien;
	private FiltreTypeListener 		listenerType = new FiltreTypeListener();
	private ListenerAdresse 		listenerAdresse = new ListenerAdresse();
	private PaysSelectCtrl 			myPaysSelectCtrl;
	private EORepartPersonneAdresse currentRepart;
	private EOAdresse				currentAdresse;
	private EOIndividu 				currentIndividu;
	private EOPays 					currentPays;
	private EOAgentPersonnel		currentUtilisateur;

	public AdressesNormeesCtrl(EOEditingContext editingContext, boolean loadNotifications) {

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new AdressesNormeeView(null, eod, MODE_MODAL.booleanValue(), monRendererColor);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
				);
		myView.getBtnGetPays().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectPays();}}
				);

		setSaisieEnabled(false);

		myView.getMyEOTable().addListener(listenerAdresse);
		CocktailUtilities.initTextField(myView.getTfPays(), false, false);
		myView.getTfCodePostal().addFocusListener(new ListenerTextFieldCodePostal());
		myView.getTfCodePostal().addActionListener(new ActionListenerCodePostal());

		myView.getPopupVilles().addActionListener(new CommuneListener());
		myView.getCheckFiltrePerso().addActionListener(listenerType);
		myView.getCheckFiltrePro().addActionListener(listenerType);
		myView.getCheckFiltreFact().addActionListener(listenerType);
		myView.getCheckFiltreAutres().addActionListener(new FiltreTypeAutreListener());
		myView.getPopupValidite().addActionListener(new ValiditeListener());

		CocktailUtilities.initPopupAvecListe(myView.getPopupVoies(), NomenclatureFinder.findStatic(ec, EOVoirie.ENTITY_NAME), true);
		CocktailUtilities.initTextField(myView.getTfCedex(), false, false);
		myView.getCheckFiltreAutres().setEnabled(false);
		myView.getCheckEtudiant().setVisible(false);
		myView.getCheckParent().setVisible(false);

	}

	public static AdressesNormeesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new AdressesNormeesCtrl(editingContext, true);
		return sharedInstance;
	}

	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		setCurrentUtilisateur(utilisateur);

		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());

		myView.getCheckFiltrePerso().setEnabled(utilisateur.peutAfficherInfosPerso());
		myView.getCheckFiltrePro().setEnabled(utilisateur.peutAfficherInfosPerso());
		myView.getCheckFiltreFact().setEnabled(utilisateur.peutAfficherInfosPerso());
		myView.getCheckFiltrePerso().setSelected(utilisateur.peutAfficherInfosPerso());
		myView.getCheckFiltrePro().setSelected(!utilisateur.peutAfficherInfosPerso());

	}


	private EORepartPersonneAdresse getCurrentRepart() {
		return currentRepart;
	}

	public EOPays getCurrentPays() {
		return currentPays;
	}

	public void setCurrentPays(EOPays currentPays) {
		this.currentPays = currentPays;
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}

	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	public void setCurrentRepart(EORepartPersonneAdresse currentRepart) {
		this.currentRepart = currentRepart;
	}

	private EOAdresse getCurrentAdresse() {
		return currentAdresse;
	}
	public void setCurrentAdresse(EOAdresse adresse) {
		currentAdresse = adresse;
	}
	private EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu individu) {
		currentIndividu = individu;
		actualiser();
	}

	/**
	 * 
	 * @param individu
	 */
	public void open(EOIndividu individu)	{
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.PERS_ID_KEY+"=%@", new NSArray(getCurrentIndividu().persId())));

		// Restriction sur les types d'adresse
		if (currentUtilisateur.peutAfficherInfosPerso() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"!=%@", new NSArray(EOTypeAdresse.TYPE_PERSONNELLE)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"!=%@", new NSArray(EOTypeAdresse.TYPE_FACTURATION)));
		}
		if (!estNormalien) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"!=%@", new NSArray(EOTypeAdresse.TYPE_ETUDIANT)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"!=%@", new NSArray(EOTypeAdresse.TYPE_PARENT)));
		}

		if (myView.getPopupValidite().getSelectedIndex() == INDEX_ADRESSES_VALIDES)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
		else
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_VALIDE_KEY+"=%@", new NSArray("N")));

		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

		if (myView.getCheckFiltrePerso().isSelected())	{
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_PERSONNELLE)));
		}
		if (myView.getCheckFiltrePro().isSelected())	{
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_PROFESSIONNELLE)));
		}
		if (myView.getCheckFiltreFact().isSelected())	{
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_FACTURATION)));
		}
		if (myView.getCheckFiltreAutres().isSelected())	{
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_ETUDIANT)));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_PARENT)));
		}

		if (orQualifiers.count() > 0)
			qualifiers.addObject (new EOOrQualifier(orQualifiers));

		return new EOAndQualifier(qualifiers);
	}



	private class CommuneListener implements ActionListener
	{
		public void actionPerformed(ActionEvent anAction) {
			myView.getTfMessage().setText("");
		}
	}
	private class ValiditeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}
	private class FiltreTypeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}
	private class FiltreTypeAutreListener implements ActionListener
	{
		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}

	/**
	 * 
	 */
	public void getCommune()	{

		NSArray<EOCommune> communes = EOCommune.rechercherCommunes(ec, myView.getTfCodePostal().getText());
		myView.getPopupVilles().removeAllItems();
		myView.getPopupVilles().addItem("");
		for (Enumeration<EOCommune> e =  communes.objectEnumerator();e.hasMoreElements();)
			myView.getPopupVilles().addItem(e.nextElement().libelle());
		updateInterface();

	}


	public class ActionListenerCodePostal implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if (saisieEnabled && !myView.getCheckFiltreFact().isSelected())
				getCommune();
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ListenerTextFieldCodePostal implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e)		{
		}

		public void focusLost(FocusEvent e)	{
			if (saisieEnabled && !myView.getCheckFiltreFact().isSelected())
				getCommune();
		}
	}

	/**
	 * 
	 */
	public void actualiser() {

		eod.setObjectArray(new NSArray());
		if (getCurrentIndividu() != null) {
			eod.setObjectArray(EORepartPersonneAdresse.findForQualifier(ec, getFilterQualifier()));
			estNormalien = getCurrentIndividu() != null && EOGrhumParametres.isGestionEns() && getCurrentIndividu().estNormalienSurPeriode(null, null);
			myView.getCheckFiltreAutres().setEnabled(estNormalien);
		}

		myView.getMyEOTable().updateData();

		updateInterface();

	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewAdresse() {
		return myView.getViewAdresse();
	}
	public void toFront() {
		myView.toFront();
	}

	private void ajouter() {

		modeCreation = true;

		// On recherche si l'agent a deja une adresse principale
		EORepartPersonneAdresse adrPrincipale = EORepartPersonneAdresse.adressePrincipale(ec, currentIndividu.persId());

		setCurrentAdresse(EOAdresse.creer(ec, currentUtilisateur));

		// Ajout d une adresse personnelle
		if (getCurrentUtilisateur().peutAfficherInfosPerso() && myView.getCheckFiltrePerso().isSelected()) {
			myView.getCheckPerso().setSelected(true);
			setCurrentRepart(EORepartPersonneAdresse.creer(ec, getCurrentIndividu().persId(), getCurrentAdresse(), EOTypeAdresse.TYPE_PERSONNELLE));
		}
		else {	// Ajout d'une adresse professionnelle
			setCurrentRepart(EORepartPersonneAdresse.creer(ec, getCurrentIndividu().persId(), getCurrentAdresse(), EOTypeAdresse.TYPE_PROFESSIONNELLE));
			myView.getCheckPro().setSelected(true);
		}

		if (adrPrincipale== null)
			getCurrentRepart().setRpaPrincipal("O");
		else
			getCurrentRepart().setRpaPrincipal("N");			

		// Mise a jour de l'affichage
		updateDatas();
		setSaisieEnabled(true);
	}

	private void modifier() {

		modeCreation = false;		
		myView.getTfMessage().setText("");
		setSaisieEnabled(true);

	}

	/**
	 *  On invalide tous les repart_personne_adresse associes a l adresse selectionnee
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment annuler cette adresse ?", "Oui", "Non"))		
			return;

		try {

			getCurrentRepart().setRpaValide("N");
			getCurrentRepart().setRpaPrincipal("N");

			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			ec.revert();
		}
	}


	public EOVoirie getVoirie() {

		if (myView.getPopupVoies().getSelectedIndex() > 0) {
			return (EOVoirie)myView.getPopupVoies().getSelectedItem();
		}

		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String getExtensionVoie() {

		if (myView.getPopupExtension().getSelectedIndex() > 0) {
			return ((String)myView.getPopupExtension().getSelectedItem()).substring(0,1);
		}

		return null;
	}

	public void setExtensionVoie(String extension) {

		if (extension != null) {
			if (extension.equals("B")) 
				myView.getPopupExtension().setSelectedIndex(1);
			else
				if (extension.equals("T")) 
					myView.getPopupExtension().setSelectedIndex(2);
				else
					if (extension.equals("Q")) 
						myView.getPopupExtension().setSelectedIndex(3);
		}
	}
	public void setVoie(EOVoirie voie) {		
		myView.getPopupVoies().setSelectedItem(voie);
	}

	/**
	 * 
	 */
	private void valider() {

		try {
			if (!myView.getCheckPerso().isSelected() && !myView.getCheckPro().isSelected() && !myView.getCheckFiltreFact().isSelected())
				throw new ValidationException("Veuillez sélectionner au moins un type PERSONNEL ou PROFESSIONNEL !");

			getCurrentAdresse().setAdrAdresse1(CocktailUtilities.getTextFromField(myView.getTfAdresse()));
			getCurrentAdresse().setAdrAdresse2(CocktailUtilities.getTextFromField(myView.getTfComplement()));
			getCurrentAdresse().setCodePostal(CocktailUtilities.getTextFromField(myView.getTfCodePostal()));
			getCurrentAdresse().setCpEtranger(CocktailUtilities.getTextFromField(myView.getTfCodePostalEtranger()));
			getCurrentAdresse().setAdrBp(CocktailUtilities.getTextFromField(myView.getTfBoitePostale()));

			getCurrentAdresse().setAdrUrlPere(CocktailUtilities.getTextFromField(myView.getTfUrl()));
			getCurrentRepart().setEMail(CocktailUtilities.getTextFromField(myView.getTfMail()));

			getCurrentAdresse().setToVoieRelationship(getVoirie());
			getCurrentAdresse().setNoVoie(CocktailUtilities.getTextFromField(myView.getTfNumero()));
			getCurrentAdresse().setNomVoie(CocktailUtilities.getTextFromField(myView.getTfNomVoie()));
			getCurrentAdresse().setBisTer(getExtensionVoie());

			if (getCurrentPays().isDefault())
				getCurrentAdresse().setVille((String)myView.getPopupVilles().getSelectedItem());	
			else
				getCurrentAdresse().setVille(CocktailUtilities.getTextFromField(myView.getTfVille()));

			// Possibilite de typer une adresse de facturation en tant qu'adresse personnelle
			if (!modeCreation && myView.getCheckFiltreFact().isSelected()) {
				// Verifier que l'adresse n'existe pas deja
				EORepartPersonneAdresse adrExistante = EORepartPersonneAdresse.findForAdresseEtType(ec, currentIndividu.persId(), currentAdresse, EOTypeAdresse.TYPE_PERSONNELLE);

				if (adrExistante == null && myView.getCheckPerso().isSelected())
					EORepartPersonneAdresse.creer(ec, currentIndividu.persId(), getCurrentAdresse(), EOTypeAdresse.TYPE_PERSONNELLE);				
				else
					adrExistante.setRpaValide(CocktailConstantes.VRAI);
			}

			// Si l'adresse actuelle est principale, les autres sont passees a rpa_principale = 'N'

			if (myView.getCheckPrincipale().isSelected()) {
				NSArray reparts = EORepartPersonneAdresse.adresses(ec, currentIndividu);
				for (Enumeration<EORepartPersonneAdresse> e = reparts.objectEnumerator();e.hasMoreElements();) {
					e.nextElement().setRpaPrincipal(CocktailConstantes.FAUX);				
				}
			}

			getCurrentRepart().setEstAdressePrincipale(myView.getCheckPrincipale().isSelected());

			// Enregistrement de l'adresse
			getCurrentAdresse().initAdrAdresse1();

			ec.saveChanges();
			actualiser();
			setSaisieEnabled(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void annuler() {

		ec.revert();
		ServerProxy.clientSideRequestRevert(ec);
		listenerAdresse.onSelectionChanged();
		setSaisieEnabled(false);
		updateInterface();
	}

	private void clearDatas() {

		CocktailUtilities.viderTextField(myView.getTfAdresse());
		CocktailUtilities.viderTextField(myView.getTfComplement());
		CocktailUtilities.viderTextField(myView.getTfCodePostal());
		CocktailUtilities.viderTextField(myView.getTfCodePostalEtranger());
		CocktailUtilities.viderTextField(myView.getTfBoitePostale());
		CocktailUtilities.viderTextField(myView.getTfVille());

		CocktailUtilities.viderLabel(myView.getTfMessage());
		CocktailUtilities.viderTextField(myView.getTfUrl());
		CocktailUtilities.viderTextField(myView.getTfMail());

		myView.getCheckPerso().setSelected(false);
		myView.getCheckPro().setSelected(false);
		myView.getCheckFact().setSelected(false);
		myView.getCheckEtudiant().setSelected(false);
		myView.getCheckParent().setSelected(false);
		myView.getPopupVilles().removeAllItems();

		myView.getPopupExtension().setSelectedIndex(0);
		myView.getPopupVoies().setSelectedIndex(0);

	}

	public boolean isSaisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_LOCK_INFOS_PERSONNELLES, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_UNLOCK_INFOS_PERSONNELLES, null);
		updateInterface();
	}

	/**
	 * 
	 */
	private void selectPays() {

		if (myPaysSelectCtrl == null)
			myPaysSelectCtrl = new PaysSelectCtrl(ec);

		EOPays pays = myPaysSelectCtrl.getPays(null, null);
		if (pays != null) {
			setCurrentPays(pays);
			getCurrentAdresse().setToPaysRelationship(currentPays);
			CocktailUtilities.setTextToField(myView.getTfPays(), currentPays.libelleLong());
			updateInterface();
		}
	}

	private boolean peutAjouterAdresse() {
		return !isSaisieEnabled()
				&& myView.getPopupValidite().getSelectedIndex() == INDEX_ADRESSES_VALIDES
				&& !myView.getCheckFiltreFact().isSelected() 
				&& getCurrentIndividu() != null;
	}
	private boolean peutModifierAdresse() {		
		return !isSaisieEnabled() && getCurrentAdresse() != null
				&& myView.getPopupValidite().getSelectedIndex() == INDEX_ADRESSES_VALIDES
				&& getCurrentIndividu() != null;
	}
	private boolean peutSupprimerAdresse() {
		return !isSaisieEnabled() 
				&& getCurrentRepart() != null 
				&& getCurrentRepart().estValide() 
				&& !myView.getCheckFiltreFact().isSelected();
	}

	/**
	 * 
	 * Gestion de l'activation de tous les objets de l'interface.
	 * Modification en mode saisie uniquement.
	 * Les adresses de type FACTURATION ne peuvent être modifiees.
	 * 
	 * La ville est déduite automatiquement s'il s'agit d'un pays ETRANGER 
	 * 
	 */
	private void updateInterface() {

		myView.getMyEOTable().setEnabled(!isSaisieEnabled());
		myView.getBtnAjouter().setEnabled(peutAjouterAdresse());		
		myView.getBtnModifier().setEnabled(peutModifierAdresse());
		myView.getBtnSupprimer().setEnabled(peutSupprimerAdresse());

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getBtnGetPays().setEnabled(isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfNumero(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfNomVoie(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfAdresse(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfComplement(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfCodePostal(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfCodePostalEtranger(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfBoitePostale(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected()) ;
		CocktailUtilities.initTextField(myView.getTfUrl(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfMail(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());		
		CocktailUtilities.initTextField(myView.getTfVille(), false, isSaisieEnabled() && getCurrentPays() != null && !getCurrentPays().isDefault() && !myView.getCheckFiltreFact().isSelected());

		myView.getPopupVilles().setEnabled(isSaisieEnabled() && getCurrentPays() != null && getCurrentPays().isDefault() && !myView.getCheckFiltreFact().isSelected());

		myView.getPanelFrance().setVisible(getCurrentPays() != null && getCurrentPays().isDefault());
		myView.getPanelEtranger().setVisible(getCurrentPays() != null && !getCurrentPays().isDefault());

		myView.getPopupExtension().setEnabled(isSaisieEnabled());
		myView.getPopupVoies().setEnabled(isSaisieEnabled());

		myView.getCheckValide().setEnabled(false);
		myView.getCheckPrincipale().setEnabled(getCurrentIndividu() != null && isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());

		myView.getCheckPerso().setEnabled(getCurrentIndividu() != null && isSaisieEnabled() && currentUtilisateur.peutAfficherInfosPerso() && (myView.getCheckFiltreFact().isSelected() && ! myView.getCheckFiltrePerso().isSelected()) );
		myView.getCheckPro().setEnabled(false);
		myView.getCheckFact().setEnabled(false);
		myView.getCheckEtudiant().setEnabled(getCurrentIndividu() != null && isSaisieEnabled());
		myView.getCheckParent().setEnabled(getCurrentIndividu() != null && isSaisieEnabled());

	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearDatas();

		if (getCurrentRepart() != null) {

			setCurrentAdresse(getCurrentRepart().toAdresse());
			setCurrentPays(getCurrentAdresse().toPays());

			myView.getCheckValide().setSelected(getCurrentRepart().estValide());
			myView.getCheckPrincipale().setSelected(getCurrentRepart().estAdressePrincipale());

			CocktailUtilities.setTextToField(myView.getTfAdresse(), getCurrentAdresse().nomVoie());
			CocktailUtilities.setTextToLabel(myView.getLblAdresseNonNormee(), getCurrentAdresse().adrAdresse1());
			CocktailUtilities.setTextToField(myView.getTfComplement(), getCurrentAdresse().adrAdresse2());
			CocktailUtilities.setTextToField(myView.getTfCodePostal(), getCurrentAdresse().codePostal());
			CocktailUtilities.setTextToField(myView.getTfCodePostalEtranger(), getCurrentAdresse().cpEtranger());
			CocktailUtilities.setTextToField(myView.getTfBoitePostale(), getCurrentAdresse().adrBp());
			CocktailUtilities.setTextToField(myView.getTfUrl(), getCurrentAdresse().adrUrlPere());
			CocktailUtilities.setTextToField(myView.getTfMail(), getCurrentRepart().eMail());
			CocktailUtilities.setTextToField(myView.getTfPays(), getCurrentPays().libelleLong());
			CocktailUtilities.setTextToField(myView.getTfVille(), getCurrentAdresse().ville());

			CocktailUtilities.setTextToField(myView.getTfNumero(), getCurrentAdresse().noVoie());
			setExtensionVoie(getCurrentAdresse().bisTer());

			myView.getPopupVoies().setSelectedItem(getCurrentAdresse().toVoie());

			if (getCurrentAdresse().codePostal() != null) {
				myView.getTfCodePostal().setText(getCurrentAdresse().codePostal());
				getCommune();
			}
			if (getCurrentAdresse().ville() != null)
				myView.getPopupVilles().setSelectedItem(getCurrentAdresse().ville());

			// On verifie que la commune saisie soit bien dans la liste des communes connues
			EOCommune commune = EOCommune.findForCodePostalAndLibelle(ec, getCurrentAdresse().codePostal(), getCurrentAdresse().ville());

			if (commune == null && getCurrentAdresse().codePostal() != null && currentPays.isDefault() && currentAdresse.ville() != null)
				myView.getTfMessage().setText("La commune saisie n'est pas connue. Merci de choisir dans la liste!");

			myView.getCheckPerso().setSelected(currentRepart.estPersonnelle());
			myView.getCheckPro().setSelected(currentRepart.estProfessionnelle());
			myView.getCheckFact().setSelected(currentRepart.estFacturation());

			if (currentRepart.estFacturation()) {
				EORepartPersonneAdresse repart = EORepartPersonneAdresse.findForAdresseEtType(ec, currentIndividu.persId(), currentRepart.toAdresse(), EOTypeAdresse.TYPE_PERSONNELLE);
				if (repart != null) {
					myView.getCheckPerso().setSelected(true);
				}
			}
			else
				if (currentRepart.estPersonnelle()) {
					EORepartPersonneAdresse repart = EORepartPersonneAdresse.findForAdresseEtType(ec, currentIndividu.persId(), currentRepart.toAdresse(), EOTypeAdresse.TYPE_FACTURATION);
					if (repart != null) {
						myView.getCheckFact().setSelected(true);
					}					
				}

		}

		updateInterface();

	}


	private class ListenerAdresse implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			if (peutModifierAdresse() && peutGererModule())
				modifier();
		}

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			setCurrentRepart((EORepartPersonneAdresse)eod.selectedObject());
			updateDatas();

		}
	}



	private class AdressesRenderer extends ZEOTableCellRenderer	{
		private static final long serialVersionUID = -2907930349355563787L;
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EORepartPersonneAdresse obj = (EORepartPersonneAdresse) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().get(mdlRow);           

			if (obj.estValide())
				leComposant.setForeground(new Color(0,0,0));
			else
				leComposant.setForeground(new Color(150, 150, 150));

			return leComposant;
		}
	}


}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infosperso;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;

import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.individu.RibsView;
import org.cocktail.mangue.client.select.BanqueSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOBanque;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAgent;
import org.cocktail.mangue.modele.grhum.referentiel.EOFournis;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartStructure;
import org.cocktail.mangue.modele.grhum.referentiel.EORib;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class RibsCtrl extends ModelePageGestion {

	private static Boolean MODE_MODAL = Boolean.FALSE;

	private RibsView 		myView;
	private RibsRenderer	monRendererColor = new RibsRenderer();

	private EOAgent 				agent;
	private EORepartPersonneAdresse repartPourIndividu;	// Contient la repart à utiliser en cas de création de fournisseur
	private boolean					peutGererModule;

	private EODisplayGroup 			eod;
	private boolean					peutCreerFournisseur;
	private ListenerRib 			listenerRib = new ListenerRib();
	private EORib					currentRib;
	private EOIndividu 				currentIndividu;
	private EOBanque				currentBanque;
	private InfosPersonnellesCtrl 	ctrlParent;

	public RibsCtrl(InfosPersonnellesCtrl ctrlParent) {

		super(ctrlParent.getEdc());
		this.ctrlParent = ctrlParent;

		eod = new EODisplayGroup();
		myView = new RibsView(null, eod, MODE_MODAL.booleanValue(), monRendererColor);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnGetBanque().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectBanque();}}
				);

		setSaisieEnabled(false);

		myView.getMyEOTable().addListener(listenerRib);

		myView.getTfIban().addFocusListener(new FocusListenerIban());
		myView.getTfIban().addActionListener(new ActionListenerIban());
		myView.getTfNoCompte().addFocusListener(new FocusListenerCompte());
		myView.getTfNoCompte().addActionListener(new ActionListenerCompte());

		CocktailUtilities.initTextField(myView.getTfBanque(), false, false);
		CocktailUtilities.initTextField(myView.getTfGuichet(), false, false);
		CocktailUtilities.initTextField(myView.getTfDomiciliation(), false, false);
		CocktailUtilities.initTextField(myView.getTfBic(), false, false);

		agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgent();

		myView.getCheckLocal().setEnabled(false);
		myView.getCheckValide().setEnabled(false);
	}

	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());

		peutCreerFournisseur = (agent != null && (agent.peutCreerFournisseur() || agent.peutValiderFournisseur()));

		if (!peutCreerFournisseur)
			myView.getLblMessage().setText("Traitement des RIBS impossible. Vous n'avez pas de droits sur les fournisseurs");
		else
			myView.getLblMessage().setText("");

	}

	public EORib getCurrentRib() {
		return currentRib;
	}
	public void setCurrentRib(EORib currentRib) {
		this.currentRib = currentRib;
		updateDatas();
	}
	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EOBanque getCurrentBanque() {
		return currentBanque;
	}

	public void setCurrentBanque(EOBanque currentBanque) {

		this.currentBanque = currentBanque;

		CocktailUtilities.viderTextField(myView.getTfBanque());
		CocktailUtilities.viderTextField(myView.getTfGuichet());
		CocktailUtilities.viderTextField(myView.getTfDomiciliation());
		CocktailUtilities.viderTextField(myView.getTfBic());

		if (currentBanque != null) {

			CocktailUtilities.setTextToField(myView.getTfBanque(), getCurrentBanque().cBanque());
			CocktailUtilities.setTextToField(myView.getTfGuichet(), getCurrentBanque().cGuichet());
			CocktailUtilities.setTextToField(myView.getTfDomiciliation(), getCurrentBanque().domiciliation());
			CocktailUtilities.setTextToField(myView.getTfBic(), getCurrentBanque().bic());

			myView.getCheckLocal().setSelected(currentBanque.estLocale());
		}		
	}

	public void actualiser() {
		eod.setObjectArray(EORib.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}
	public JPanel getViewRib() {
		return myView.getViewRib();
	}

	/**
	 * 
	 * @throws ValidationException
	 */
	protected void traitementsPourCreation() throws ValidationException {

		ctrlParent.setIsLocked(true);

		setCurrentRib(EORib.creer(getEdc(), getCurrentIndividu().fournis() , getCurrentIndividu().identitePrenomFirst()));

		EOFournis fournisseur = getCurrentIndividu().fournisValideOuNon();
		if (fournisseur == null) {
			fournisseur = ajouterFournisseur();
			if (fournisseur == null) {
				throw new ValidationException("Impossible de créer le fournisseur !");
			}
		} else if (fournisseur.estValide()) {
			fournisseur.setEstValide(true);
		}
		if (fournisseur != null) {
			getCurrentRib().initRib(fournisseur,getCurrentIndividu().identite());
		}		
	}

	/**
	 * Controle de la presence du RIB pour un autre agent
	 * @return
	 */
	protected void traitementsAvantValidation() {

		// Verification de l'unicite du RIB
		EORib ribExistant = EORib.ribExiste(getEdc(), getCurrentRib().iban(), getCurrentRib().cBanque(), 
				getCurrentRib().cGuichet(), getCurrentRib().noCompte(), getCurrentRib().cleRib());
		if (ribExistant != null && (ribExistant.toFournis() != getCurrentRib().toFournis())) {
			EOIndividu individu = EOIndividu.rechercherIndividuPersId(getEdc(), ribExistant.toFournis().persId());
			if (individu != null) {
				if(!EODialogs.runConfirmOperationDialog("Attention", "Ce RIB existe déjà (" + individu.identitePrenomFirst() + ") , voulez-vous continuer ?", "Oui", "Non"))
					throw new ValidationException ("Opération annulée !");
			}
		}

		getCurrentRib().setToBanqueRelationship(getCurrentBanque());
		getCurrentRib().setRibTitco(CocktailUtilities.getTextFromField(myView.getTfTitulaire()));
		getCurrentRib().setIban(CocktailUtilities.getTextFromField(myView.getTfIban()));

		if (getCurrentBanque() != null) {
			getCurrentRib().setCBanque(getCurrentBanque().cBanque());
			getCurrentRib().setCGuichet(getCurrentBanque().cGuichet());
		}

		if (CocktailUtilities.getTextFromField(myView.getTfNoCompte()) != null) {
			getCurrentRib().setNoCompte(CocktailUtilities.getTextFromField(myView.getTfNoCompte()).toUpperCase());
		}
		getCurrentRib().setCleRib(CocktailUtilities.getTextFromField(myView.getTfCle()));

	}

	/**
	 *  On cherche l'adresse de facturation valide puis l'adresse principale puis l'adresse pro et enfin l'adresse perso
	 */
	private void preparerRepart() {
		if (getCurrentIndividu() == null) {
			repartPourIndividu = null;
		} else {
			repartPourIndividu = getCurrentIndividu().adresseFacturationValide();
			if (repartPourIndividu == null) {
				repartPourIndividu = getCurrentIndividu().repartAdressePrincipale();
				if (repartPourIndividu == null) {
					NSArray<EORepartPersonneAdresse> reparts = EORepartPersonneAdresse.adressesProValides(getEdc(), getCurrentIndividu());
					if (reparts.size() > 0) {
						repartPourIndividu = (EORepartPersonneAdresse)reparts.get(0);
					} else {
						reparts = EORepartPersonneAdresse.adressesPersoValides(getEdc(), getCurrentIndividu());
						if (reparts.size() > 0) {
							repartPourIndividu = (EORepartPersonneAdresse)reparts.get(0);
						}
					}
				}
			}
		}
	}

	/**
	 * Ajout d'un fournisseur VALIDE pour l'agent en question. 
	 * Controle de la presence du groupe des fournisseurs valides et creation d'une entree pour l'agent dans ce groupe
	 * @return
	 */
	private EOFournis ajouterFournisseur() throws ValidationException {
		
		String cStructure = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_ANNUAIRE_FOU_VALIDE);
		if (cStructure == null) {
			throw new ValidationException("Pas de groupe de fournisseur valide");
		}
		EOStructure structure = (EOStructure)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(getEdc(), EOStructure.ENTITY_NAME, EOStructure.C_STRUCTURE_KEY, cStructure);
		if (structure == null) {
			throw new ValidationException("Pas de structure valide pour le groupe " + cStructure);
		}

		// Recuperation du repartPersonneAdresse
		preparerRepart();

		if (repartPourIndividu == null)
			throw new ValidationException("Vous devez d'abord saisir une adresse valide pour cet agent !");

		EORepartPersonneAdresse rpa = new EORepartPersonneAdresse();
		rpa.initRepartFournisseurPourIndividuEtAdresse(getCurrentIndividu(), repartPourIndividu.toAdresse());
		getEdc().insertObject(rpa);

		EOFournis fournis =  new EOFournis();
		fournis.initFournis(repartPourIndividu.toAdresse(), getCurrentIndividu(), EOFournis.getFouCode(getEdc(), getCurrentIndividu().nomUsuel()), agent);
		getEdc().insertObject(fournis);

		NSArray<EORepartStructure> rpsIndividu = EORepartStructure.rechercherRepartStructuresPourIndividuEtCodeStructure(getEdc(), getCurrentIndividu(), cStructure);
		if (rpsIndividu.size() == 0)	{
			EORepartStructure rps = new EORepartStructure();
			rps.init(getCurrentIndividu(), structure);	
			getEdc().insertObject(rps);
		}
		return fournis;

	}

	private void selectBanque() {

		EOBanque banque = BanqueSelectCtrl.sharedInstance(getEdc()).getBanque();
		if (banque != null) {
			setCurrentBanque(banque);
		}
	}

	private boolean peutAjouterRib() {
		return getCurrentIndividu() != null
				&& !isSaisieEnabled() && peutCreerFournisseur;
	}
	private boolean peutModifierRib() {
		return getCurrentIndividu() != null 
				&& !isSaisieEnabled()
				&& peutCreerFournisseur
				&& getCurrentRib() != null
				&& getCurrentRib().estValide();
	}
	private boolean peutSupprimerRib() {
		return getCurrentIndividu() != null 
				&& !isSaisieEnabled() 
				&& peutCreerFournisseur
				&& getCurrentRib() != null 
				&& getCurrentRib().estValide();
	}

	private class ListenerRib implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentRib() != null && peutGererModule())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentRib((EORib)eod.selectedObject());
		}
	}



	private class RibsRenderer extends org.cocktail.application.client.swing.ZEOTableCellRenderer	{

		private static final long serialVersionUID = -2907930349355563787L;

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EORib obj = (EORib) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().get(mdlRow);           

			if (obj.estValide())
				leComposant.setForeground(new Color(0,0,0));
			else
				leComposant.setForeground(new Color(150, 150, 150));

			return leComposant;
		}
	}

	private void deductionBanqueIban() {		
		if (myView.getTfIban().getText().length() >= 15) {
			CocktailUtilities.setTextToField(myView.getTfIban(), myView.getTfIban().getText().toUpperCase());
			EOBanque banque = EOBanque.rechercherBanque(getEdc(), 
					myView.getTfIban().getText().substring(4,9), 
					myView.getTfIban().getText().substring(9,14));
			setCurrentBanque(banque);
		}
	}
	public class ActionListenerIban implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			deductionBanqueIban();
		}
	}
	public class FocusListenerIban implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e)		{
		}
		public void focusLost(FocusEvent e)	{
			deductionBanqueIban();
		}
	}
	public class ActionListenerCompte implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			CocktailUtilities.setTextToField(myView.getTfNoCompte(), myView.getTfNoCompte().getText().toUpperCase());
		}
	}
	public class FocusListenerCompte implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e)		{
		}
		public void focusLost(FocusEvent e)	{
			CocktailUtilities.setTextToField(myView.getTfNoCompte(), myView.getTfNoCompte().getText().toUpperCase());
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfTitulaire());
		CocktailUtilities.viderTextField(myView.getTfBanque());
		CocktailUtilities.viderTextField(myView.getTfGuichet());
		CocktailUtilities.viderTextField(myView.getTfBic());
		CocktailUtilities.viderTextField(myView.getTfDomiciliation());
		CocktailUtilities.viderTextField(myView.getTfIban());
		CocktailUtilities.viderTextField(myView.getTfNoCompte());
		CocktailUtilities.viderTextField(myView.getTfCle());
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentRib() != null) {

			setCurrentBanque(getCurrentRib().toBanque());
			myView.getCheckValide().setSelected(getCurrentRib().estValide());

			CocktailUtilities.setTextToField(myView.getTfTitulaire(), getCurrentRib().ribTitco());
			CocktailUtilities.setTextToField(myView.getTfIban(), getCurrentRib().iban());
			CocktailUtilities.setTextToField(myView.getTfNoCompte(), getCurrentRib().noCompte());
			CocktailUtilities.setTextToField(myView.getTfCle(), getCurrentRib().cleRib());

		}

		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(peutAjouterRib());	
		myView.getBtnModifier().setEnabled(peutModifierRib());
		myView.getBtnSupprimer().setEnabled(peutSupprimerRib());

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());
		myView.getBtnGetBanque().setEnabled(isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfIban(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfNoCompte(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfCle(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfTitulaire(), false, isSaisieEnabled());

	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(false);		
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		listenerRib.onSelectionChanged();
		ctrlParent.setIsLocked(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		ctrlParent.setIsLocked(true);		
	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getCurrentRib().setEstValide(false);
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub

	}

}
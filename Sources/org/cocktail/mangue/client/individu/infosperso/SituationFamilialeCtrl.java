/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infosperso;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.individu.SituationFamilialeView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuFamiliale;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class SituationFamilialeCtrl extends ModelePageGestion {

	private static SituationFamilialeCtrl sharedInstance;

	private SituationFamilialeView	myView;

	private EODisplayGroup 			eod;
	private boolean					peutGererModule;

	private ListenerSituation 		listenerSituation = new ListenerSituation();
	private EOIndividuFamiliale		currentIndividuSituation;
	private EOSituationFamiliale	currentSituation;
	private EOIndividu 				currentIndividu;

	public SituationFamilialeCtrl(EOEditingContext edc) {

		super(edc);
		eod = new EODisplayGroup();
		myView = new SituationFamilialeView(null, eod, false);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		CocktailUtilities.initPopupAvecListe(myView.getPopupSituation(), EOSituationFamiliale.fetchAll(getEdc(), EOSituationFamiliale.SORT_ARRAY_LIBELLE), true);
		myView.getMyEOTable().addListener(listenerSituation);		
		myView.setTitle("SITUATION FAMILIALE");

		setSaisieEnabled(false);
	}

	public static SituationFamilialeCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SituationFamilialeCtrl(editingContext);
		return sharedInstance;
	}



	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());

	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public EOIndividuFamiliale getCurrentIndividuSituation() {
		return currentIndividuSituation;
	}

	public void setCurrentIndividuSituation(EOIndividuFamiliale currentIndividuSituation) {
		this.currentIndividuSituation = currentIndividuSituation;
		updateDatas();
	}

	public EOSituationFamiliale getCurrentSituation() {
		return currentSituation;
	}
	public void setCurrentSituation(EOSituationFamiliale currentSituation) {
		this.currentSituation = currentSituation;
		myView.getPopupSituation().setSelectedItem(currentSituation);
	}
	public void actualiser() {	
		eod.setObjectArray(EOIndividuFamiliale.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateInterface();
	}

	public JPanel getViewSituations() {
		return myView.getViewSituations();
	}
	public void open() {
		myView.setVisible(true);
	}

	private class ListenerSituation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentIndividuSituation((EOIndividuFamiliale)eod.selectedObject());
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getPopupSituation().setSelectedIndex(0);
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");		

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentIndividuSituation() != null) {

			setCurrentSituation(getCurrentIndividuSituation().situationFamiliale());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentIndividuSituation().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentIndividuSituation().dateFin());

		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());
		myView.getPopupSituation().setEnabled(isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentIndividuSituation() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentIndividuSituation() != null);
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsAvantValidation() {
		
		// TODO Auto-generated method stub
		if (CocktailUtilities.getDateFromField(myView.getTfDateDebut()) != null) {

			getCurrentIndividuSituation().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			getCurrentIndividuSituation().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			getCurrentIndividuSituation().setSituationFamilialeRelationship((EOSituationFamiliale)myView.getPopupSituation().getSelectedItem());

			// Trouver la situation précédente
			EOIndividuFamiliale lastSituation = null;
			EOIndividuFamiliale situationActuelle = null;
			NSTimestamp dateJour = new NSTimestamp();

			NSArray<EOIndividuFamiliale> situations = EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(),EOIndividuFamiliale.SORT_ARRAY_DEBUT_ASC);
			for (EOIndividuFamiliale mySituation : situations) {

				if (DateCtrl.isBetween(dateJour, mySituation.dateDebut(), mySituation.dateFin())) {
					situationActuelle = mySituation;				
				}

				if (mySituation != getCurrentIndividuSituation() 
						&& DateCtrl.isBefore(mySituation.dateDebut(), getCurrentIndividuSituation().dateDebut()))
					lastSituation = mySituation;
			}

			// La periode en cours de saisie englobe t elle la situation actuelle
			if (DateCtrl.isBetween(dateJour, getCurrentIndividuSituation().dateDebut(), getCurrentIndividuSituation().dateFin()))
				situationActuelle = getCurrentIndividuSituation();
			if (situationActuelle != null) {
				getCurrentIndividu().setToSituationFamilialeRelationship(situationActuelle.situationFamiliale());
			}

			if (lastSituation != null) {

				//	vérifier si la date de fin est nulle que la date de début est bien la dernière (i.e postérieure à la date de début du dernier segment)
				if (getCurrentIndividuSituation().dateFin() == null && DateCtrl.isBefore(getCurrentIndividuSituation().dateDebut(),lastSituation.dateDebut())) {
					throw new NSValidation.ValidationException("Vous n'avez pas fourni de date de fin, or il existe une autre situation commençant après cette date");
				}

				// clore la situation précédente si nécessaire
				if (lastSituation.dateFin() == null || DateCtrl.isAfterEq(lastSituation.dateFin(), 
						getCurrentIndividuSituation().dateDebut())) {
					String message = "La situation commençant le " + lastSituation.dateDebutFormatee();
					if (lastSituation.dateFin() == null) {
						message = message + " n'a pas de date de fin.";
					} else {
						message = message + " se termine le " + lastSituation.dateFinFormatee() + ".";
					}
					message = message + " Sa date de fin va être modifiée.\nEst-ce ce que vous voulez ?";
					if (EODialogs.runConfirmOperationDialog("Attention",message,"OK","Annuler")) {
						lastSituation.setDateFin(DateCtrl.jourPrecedent(getCurrentIndividuSituation().dateDebut()));
					}
				}
			}
		}
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub

		NSTimestamp dateJour = DateCtrl.today();
		NSArray<EOIndividuFamiliale> situations = EOIndividuFamiliale.findForIndividu(getEdc(), getCurrentIndividu());
		for (EOIndividuFamiliale mySituation : situations) {
			if (DateCtrl.isBetween(dateJour, mySituation.dateDebut(), mySituation.dateFin()))
				getCurrentIndividu().setToSituationFamilialeRelationship(mySituation.situationFamiliale());
		}

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		listenerSituation.onSelectionChanged();

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		myView.getTfDateDebut().requestFocus();
		EOIndividuFamiliale newObject = EOIndividuFamiliale.creer(getEdc(), getCurrentIndividu());
		if (eod.displayedObjects().size() == 0) {
			newObject.setDateDebut(getCurrentIndividu().dNaissance());
			newObject.setSituationFamilialeRelationship(EOSituationFamiliale.getSituationCelibataire(getEdc()));
			myView.getTfDateFin().requestFocus();
		}
		setCurrentIndividuSituation(newObject);
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(getCurrentIndividuSituation());			
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub

	}	

}

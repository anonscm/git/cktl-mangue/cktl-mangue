package org.cocktail.mangue.client.individu.infosperso;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.WindowConstants;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.individu.InfosPersonnellesView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class InfosPersonnellesCtrl {

	public static final String NOTIF_LOCK_INFOS_PERSONNELLES = "NotifLockInfosPerso";
	public static final String NOTIF_UNLOCK_INFOS_PERSONNELLES = "NotifUnockInfosPerso";

	public static final String LAYOUT_ETAT_CIVIL = " ETAT CIVIL";
	public static final String LAYOUT_COMPTE = "MESSAGERIE";
	public static final String LAYOUT_RIB = "RIBs";
	public static final String LAYOUT_TELEPHONE = "TELEPHONES";
	public static final String LAYOUT_ADRESSE = "ADRESSES";
	public static final String LAYOUT_ENFANT = "ENFANTS";
	public static final String LAYOUT_SITUATION = "SITUATION FAMILIALE";
	public static final String LAYOUT_CONJOINT = "CONJOINT";
	public static final String LAYOUT_URGENCE = "URGENCE";
	public static final String LAYOUT_NOTES = "BLOC NOTES";

	private EtatCivilListener etatCivilListener = new EtatCivilListener();
	private AdressesListener adressesListener = new AdressesListener();
	private TelephonesListener telephonesListener = new TelephonesListener();
	private EnfantsListener enfantsListener = new EnfantsListener();
	private RibsListener ribsListener = new RibsListener();
	private ConjointListener conjointListener = new ConjointListener();
	private UrgenceListener urgenceListener = new UrgenceListener();
	private ComptesListener comptesListener = new ComptesListener();
	private SituationsListener situationListener = new SituationsListener();
	private NotesListener notesListener = new NotesListener();

	private static InfosPersonnellesCtrl 	sharedInstance;
	private static Boolean 					MODE_MODAL = Boolean.TRUE;
	private EOEditingContext 				edc;
	private InfosPersonnellesView 			myView;
	private EOIndividu 						currentIndividu;

	private boolean isLocked;
	
	private EtatCivilCtrl etatCivilCtrl = null;
	private AdressesCtrl adresseCtrl = null;
	private AdressesNormeesCtrl adresseNormeeCtrl = null;
	private TelephonesCtrl telephoneCtrl = null;
	private RibsCtrl ribsCtrl = null;
	private ComptesCtrl compteCtrl = null;
	private EnfantsCtrl enfantCtrl = null;
	private SituationFamilialeCtrl situationCtrl = null;
	private IndividuConjointCtrl conjointCtrl = null;
	private IndividuUrgenceCtrl urgenceCtrl = null;
	private BlocNotesCtrl blocNotesCtrl = null;

	public InfosPersonnellesCtrl(EOEditingContext editingContext) {

		edc = editingContext;		
		JFrame mainFrame = ((ApplicationClient)EOApplication.sharedApplication()).activeFrame();
		myView = new InfosPersonnellesView(mainFrame, MODE_MODAL.booleanValue());

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("unlock",new Class[] {NSNotification.class}), NOTIF_UNLOCK_INFOS_PERSONNELLES,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("lock",new Class[] {NSNotification.class}), NOTIF_LOCK_INFOS_PERSONNELLES,null);

		myView.getBtnFermer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {myView.dispose();}}
		);

		//etatCivilCtrl = new EtatCivilCtrl(ec);
		adresseCtrl = new AdressesCtrl(edc, true);
		adresseNormeeCtrl = new AdressesNormeesCtrl(edc, true);
		telephoneCtrl = new TelephonesCtrl(edc, this, true);
		ribsCtrl = new RibsCtrl(this);
		compteCtrl = new ComptesCtrl(edc);
		enfantCtrl = new EnfantsCtrl(edc, true);
		situationCtrl = new SituationFamilialeCtrl(edc);
		conjointCtrl = new IndividuConjointCtrl(edc);
		urgenceCtrl = new IndividuUrgenceCtrl(edc);
		blocNotesCtrl = new BlocNotesCtrl(edc);

		//myView.getSwapView().add(LAYOUT_ETAT_CIVIL,etatCivilCtrl.getViewEtatCivil());
		
		if (! ((ApplicationClient)ApplicationClient.sharedApplication()).isUseAdressesNormees()) {
			myView.getSwapView().add(LAYOUT_ADRESSE,adresseCtrl.getViewAdresse());
		}
		else {
			myView.getSwapView().add(LAYOUT_ADRESSE,adresseNormeeCtrl.getViewAdresse());			
		}
		myView.getSwapView().add(LAYOUT_TELEPHONE,telephoneCtrl.getViewTelephones());
		myView.getSwapView().add(LAYOUT_RIB, ribsCtrl.getViewRib());
		myView.getSwapView().add(LAYOUT_URGENCE, urgenceCtrl.getViewUrgence());
		myView.getSwapView().add(LAYOUT_COMPTE, compteCtrl.getViewCompte());
		myView.getSwapView().add(LAYOUT_ENFANT, enfantCtrl.getViewEnfants());
		myView.getSwapView().add(LAYOUT_SITUATION, situationCtrl.getViewSituations());
		myView.getSwapView().add(LAYOUT_CONJOINT, conjointCtrl.getViewConjoint());
		myView.getSwapView().add(LAYOUT_NOTES, blocNotesCtrl.getViewNote());

		myView.getCheckAdresses().setSelected(true);
		myView.getCheckEtatCivil().setVisible(false);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		myView.getCheckAdresses().addActionListener(adressesListener);
		myView.getCheckEtatCivil().addActionListener(etatCivilListener);
		myView.getCheckTelephones().addActionListener(telephonesListener);
		myView.getCheckRibs().addActionListener(ribsListener);
		myView.getCheckComptes().addActionListener(comptesListener);
		myView.getCheckEnfants().addActionListener(enfantsListener);
		myView.getCheckSituation().addActionListener(situationListener);
		myView.getCheckConjoint().addActionListener(conjointListener);
		myView.getCheckNumeroUrgence().addActionListener(urgenceListener);
		myView.getCheckBlocNotes().addActionListener(notesListener);

		adressesListener.actionPerformed(null);

		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

	}

	public static InfosPersonnellesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new InfosPersonnellesCtrl(editingContext);
		return sharedInstance;
	}
	
	
	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
				
		adresseCtrl.setDroitsGestion(currentUtilisateur);
		adresseNormeeCtrl.setDroitsGestion(currentUtilisateur);
		telephoneCtrl.setDroitsGestion(currentUtilisateur);
		ribsCtrl.setDroitsGestion(currentUtilisateur);
		enfantCtrl.setDroitsGestion(currentUtilisateur);
		situationCtrl.setDroitsGestion(currentUtilisateur);
		conjointCtrl.setDroitsGestion(currentUtilisateur);
		urgenceCtrl.setDroitsGestion(currentUtilisateur);
		blocNotesCtrl.setDroitsGestion(currentUtilisateur);
		compteCtrl.setCurrentUtilisateur(currentUtilisateur);


	}


	public EOIndividu currentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		if (currentIndividu() != null)
			myView.setTitle(currentIndividu.identitePrenomFirst() + " - INFORMATIONS PERSONNELLES");
		else
			myView.setTitle("INFORMATIONS PERSONNELLES");
		actualiser();
	}
	
	public void open(EOIndividu individu)	{
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		setCurrentIndividu(individu);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}
	
	public void open(EOIndividu individu, String layout)	{

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		setCurrentIndividu(individu);
		
		CRICursor.setWaitCursor(myView);
		if (layout.equals(LAYOUT_ETAT_CIVIL)) {myView.getCheckEtatCivil().setSelected(true);etatCivilListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_ADRESSE)) {myView.getCheckAdresses().setSelected(true);adressesListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_ENFANT)) {myView.getCheckEnfants().setSelected(true);enfantsListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_TELEPHONE))  {myView.getCheckTelephones().setSelected(true);telephonesListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_RIB)) {myView.getCheckRibs().setSelected(true);ribsListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_COMPTE)) {myView.getCheckComptes().setSelected(true);comptesListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_SITUATION)) {myView.getCheckSituation().setSelected(true);situationListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_CONJOINT)) {myView.getCheckConjoint().setSelected(true);conjointListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_URGENCE)) {myView.getCheckNumeroUrgence().setSelected(true);urgenceListener.actionPerformed(null);}
		if (layout.equals(LAYOUT_NOTES)) {myView.getCheckBlocNotes().setSelected(true);notesListener.actionPerformed(null);}
		
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
		
	}

	
	public void employeHasChanged(NSNotification  notification) {
		if (notification != null && myView.isVisible()) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
		}
	}

	public void setEnabled(boolean yn) {
	}
	
	public void lock(NSNotification notif) {
		setIsLocked(true);
	}
	public void unlock(NSNotification notif) {
		setIsLocked(false);
	}

	public boolean isLocked() {
		return isLocked;
	}
	public void setIsLocked(boolean yn) {
		isLocked = yn;
		updateUI();
	}
	private void updateUI() {
		
		myView.getCheckEtatCivil().setEnabled(!isLocked());
		myView.getCheckAdresses().setEnabled(!isLocked());
		myView.getCheckTelephones().setEnabled(!isLocked());
		myView.getCheckEnfants().setEnabled(!isLocked());
		myView.getCheckNumeroUrgence().setEnabled(!isLocked());
		myView.getCheckSituation().setEnabled(!isLocked());
		myView.getCheckConjoint().setEnabled(!isLocked());
		myView.getCheckRibs().setEnabled(!isLocked());
		myView.getCheckComptes().setEnabled(!isLocked());
		myView.getCheckBlocNotes().setEnabled(!isLocked());
		
		myView.getBtnFermer().setEnabled(!isLocked());

		if (!isLocked())
			myView.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		else
			myView.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		
	}

	public void actualiser() {
			
		if (myView.getCheckEtatCivil().isSelected())
			etatCivilCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckAdresses().isSelected()) {
			adresseCtrl.setCurrentIndividu(currentIndividu());			
			adresseNormeeCtrl.setCurrentIndividu(currentIndividu());			
		}
		if (myView.getCheckTelephones().isSelected())
			telephoneCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckEnfants().isSelected())
			enfantCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckSituation().isSelected())
			situationCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckNumeroUrgence().isSelected())
			urgenceCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckConjoint().isSelected())
			conjointCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckRibs().isSelected())
			ribsCtrl.setCurrentIndividu(currentIndividu());			
		if (myView.getCheckComptes().isSelected())
			compteCtrl.setCurrentIndividu(currentIndividu());	
		if (myView.getCheckBlocNotes().isSelected())
			blocNotesCtrl.setCurrentIndividu(currentIndividu());	
	}
	public void toFront() {
		myView.toFront();
	}
	public void nettoyerChamps(NSNotification notification) {
		setCurrentIndividu(null);
	}
	private void swapViewHasChanged(JRadioButton radioButton, String layout) {
		CRICursor.setWaitCursor(myView);
		myView.getTfTitre().setText(layout);
		myView.getTfTitre().setBackground(radioButton.getBackground());
		((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), layout);				
		actualiser();
		CRICursor.setDefaultCursor(myView);
	}
	
	private class EtatCivilListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckEtatCivil(), LAYOUT_ETAT_CIVIL);
		}
	}
	private class AdressesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckAdresses(), LAYOUT_ADRESSE);
		}
	}
	private class TelephonesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckTelephones(), LAYOUT_TELEPHONE);
		}
	}
	private class EnfantsListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckEnfants(), LAYOUT_ENFANT);
		}
	}
	private class SituationsListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckSituation(), LAYOUT_SITUATION);
		}
	}
	private class ConjointListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			System.out
					.println("InfosPersonnellesCtrl.ConjointListener.actionPerformed()");
			swapViewHasChanged(myView.getCheckConjoint(), LAYOUT_CONJOINT);
		}
	}
	private class UrgenceListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckNumeroUrgence(), LAYOUT_URGENCE);
		}
	}
	private class RibsListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckRibs(), LAYOUT_RIB);
		}
	}
	private class ComptesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			swapViewHasChanged(myView.getCheckComptes(), LAYOUT_COMPTE);
		}
	}
	private class NotesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			System.out
					.println("InfosPersonnellesCtrl.NotesListener.actionPerformed()");
			swapViewHasChanged(myView.getCheckBlocNotes(), LAYOUT_NOTES);
		}
	}

}

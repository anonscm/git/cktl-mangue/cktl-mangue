/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infosperso;
/** Interface pour gerer les adresses Personnelles ou Professionnelles des agents */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.individu.BlocNotesView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.referentiel.EOBlocNotes;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSValidation.ValidationException;

public class BlocNotesCtrl {

	private EOEditingContext 		ec;
	private ListenerNote 			myListener = new ListenerNote();
	private BlocNotesView 			myView;
	private EODisplayGroup 			eod;
	private EOIndividu 				currentIndividu;
	private EOBlocNotes 			currentNote;
	private boolean 				saisieEnabled;

	public BlocNotesCtrl(EOEditingContext editingContext) {
		ec = editingContext;
		eod = new EODisplayGroup();
		myView = new BlocNotesView(null, eod, false);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		
		myView.getMyEOTable().addListener(myListener);
		setSaisieEnabled(false);
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		
		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		
	}

	private EOIndividu currentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu individu) {
		this.currentIndividu = individu;
		actualiser();
	}
	public EOBlocNotes currentNote() {
		return currentNote;
	}
	public void setCurrentNote(EOBlocNotes currentNote) {
		this.currentNote = currentNote;
		myView.getTfMessaqge().setText("");
		if (currentNote != null)
			CocktailUtilities.setTextToArea(myView.getTfMessaqge(), currentNote.llBlocNotes());
	}
	public void actualiser() {
		eod.setObjectArray(EOBlocNotes.rechercherPourIndividu(ec, currentIndividu()));
		myView.getMyEOTable().updateData();
		updateUI();
	}
	public JPanel getViewNote() {
		return myView.getViewNote();
	}
	
	private void ajouter() {
		setCurrentNote(EOBlocNotes.creer(ec, currentIndividu()));
		setSaisieEnabled(true);
	}
	private void modifier() {
		setSaisieEnabled(true);
	}
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette note ?", "Oui", "Non"))		
			return;			
		try {
			ec.deleteObject(currentNote);
			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			ec.revert();
		}
	}

	private String getMessage() {
		return myView.getTfMessaqge().getText();
	}
	
	private void valider() {

		try {
			currentNote().setLlBlocNotes(getMessage());
			ec.saveChanges();
			setSaisieEnabled(false);
			actualiser();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			myView.toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {
		ec.revert();
		myListener.onSelectionChanged();
		setSaisieEnabled(false);
	}

	
	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_LOCK_INFOS_PERSONNELLES, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_UNLOCK_INFOS_PERSONNELLES, null);
	}

	private class ListenerNote implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentNote((EOBlocNotes)eod.selectedObject());
			updateUI();
		}
	}

	private void updateUI() {
		
		LogManager.logDetail(" BLOC NOTES UPDATE UI " + currentIndividu());

		myView.getMyEOTable().setEnabled(!saisieEnabled());
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());
		
		myView.getBtnAjouter().setEnabled(!saisieEnabled() && currentIndividu() != null);
		myView.getBtnModifier().setEnabled(!saisieEnabled() && currentNote() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && currentNote() != null);

		CocktailUtilities.initTextArea(myView.getTfMessaqge(), false, saisieEnabled());

	}

}

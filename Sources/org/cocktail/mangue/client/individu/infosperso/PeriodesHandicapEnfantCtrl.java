/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.individu.infosperso;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.individu.PeriodesHandicapEnfantView;
import org.cocktail.mangue.client.select.SituationHandicapSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOSituationHandicap;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeHandicap;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodeHandicap;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSValidation.ValidationException;

public class PeriodesHandicapEnfantCtrl
{

	private static final long serialVersionUID = -6490623926473207900L;

	private static PeriodesHandicapEnfantCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	
	private EODisplayGroup eod;
	private PeriodesHandicapEnfantView myView;
			
	private	boolean modeModification;
	private ListenerPeriode listenerPeriode = new ListenerPeriode();
	
	private EOEnfant currentEnfant;
	private EOPeriodeHandicap currentPeriode;
	
	private EOSituationHandicap currentSituation;
	private EOTypeHandicap currentType;
	
	boolean peutModifierDateDebut = true;
	
	/** 
	 *
	 */
	public PeriodesHandicapEnfantCtrl (EOEditingContext globalEc) {

		ec = globalEc;

		eod = new EODisplayGroup();
		myView = new PeriodesHandicapEnfantView(new JFrame(), true, eod);
		
		
		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {valider();}
		});

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();	}
		});
		myView.getBtnAjouter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {ajouter();}
		});
		myView.getBtnModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {modifier();}
		});

		myView.getBtnSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {supprimer();}
		});

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));

		CocktailUtilities.initTextField(myView.getTfTaux(), false, true);
		myView.getMyEOTable().addListener(listenerPeriode);

		
		modeModification = false;
		updateUI();
		
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static PeriodesHandicapEnfantCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new PeriodesHandicapEnfantCtrl(editingContext);
		return sharedInstance;
	}
				
	private EOPeriodeHandicap currentPeriode() {
		return currentPeriode;
	}
	private void setCurrentPeriode(EOPeriodeHandicap periode) {
		currentPeriode = periode;
	}

	/**
	 * 
	 *
	 */
	private void clearTextFields()	{
		
		myView.getTfTaux().setText("");
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
	
	}
	
	private void ajouter()	{

		clearTextFields();
		
		setCurrentPeriode(EOPeriodeHandicap.creer(ec, currentEnfant));

		modeModification = true;
		updateUI();
		
	}
	private void modifier() {
		
		modeModification = true;
		updateUI();
		
	}
	
	private void supprimer() {

		try {
		
			if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette période de handicap ?", "Oui", "Non"))
				return;

			ec.deleteObject(currentPeriode());
			ec.saveChanges();
						
			actualiser();
			
		}
		catch (Exception e) {

			ec.revert();
			
		}
		
	}
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public void open(EOEnfant enfant)	{
				
		currentEnfant = enfant;
		clearTextFields();
	
		myView.getTfTitre().setText(enfant.nom() + " " + currentEnfant.prenom() + " - PERIODES DE HANDICAP");
		modeModification = false;
				
		actualiser();
		
		myView.setVisible(true);
	}

	private void actualiser() {
		eod.setObjectArray(EOPeriodeHandicap.rechercherPourEnfant(ec, currentEnfant));
		myView.getMyEOTable().updateData();
	}
		
	private void getSituationHandicap() {

		EOSituationHandicap situation = SituationHandicapSelectCtrl.sharedInstance(ec).getSituation();

		if (situation != null) {
			currentSituation =situation;
		}
	}

	private void delSituationHandicap() {
		currentSituation = null;
		//myView.getTfSituation().setText("");
	}

	private void valider()	{
			
		try {
						
			
			currentPeriode.setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			currentPeriode.setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			currentPeriode.setTauxHandicap(CocktailUtilities.getDoubleFromField(myView.getTfTaux()));
											
			currentPeriode().setSituationHandicapRelationship(currentSituation);
			currentPeriode().setTypeHandicapRelationship(currentType);

			ec.saveChanges();

			modeModification = false;			
			actualiser();
			
		}
		catch (ValidationException ex)	{
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}

	}
	
	
	/**
	 *
	 */
	private void annuler()	{
		
		ec.revert();
		
		setCurrentPeriode(null);
		modeModification = false;

		listenerPeriode.onSelectionChanged();
		
	}
	
	private class ListenerPeriode implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentPeriode() != null)
				modifier();
		}
		public void onSelectionChanged() {

			setCurrentPeriode((EOPeriodeHandicap)eod.selectedObject());

			clearTextFields();
			
			if (currentPeriode() != null) {
				
				currentType = currentPeriode().typeHandicap();
				currentSituation = currentPeriode().situationHandicap();
			
				if (currentPeriode().tauxHandicap() != null)
					myView.getTfTaux().setText(currentPeriode().tauxHandicap().toString());
				
				myView.getTfDateDebut().setText(DateCtrl.dateToString(currentPeriode().dateDebut()));
				myView.getTfDateFin().setText(DateCtrl.dateToString(currentPeriode().dateFin()));

			}

			updateUI();

		}
	}

	private void updateUI() {
		
		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, modeModification);
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, modeModification);
		CocktailUtilities.initTextField(myView.getTfTaux(), false, modeModification);

//		myView.getBtnGetSituation().setVisible(false);//setEnabled(modeModification);
//		myView.getBtnDelSituation().setVisible(false);//modeModification && currentSituation != null);

		myView.getBtnAjouter().setEnabled(!modeModification);
		myView.getBtnModifier().setEnabled(currentPeriode() != null && !modeModification);
		myView.getBtnSupprimer().setEnabled(currentPeriode() != null && !modeModification);

		myView.getBtnAnnuler().setEnabled(currentPeriode() != null && modeModification);
		myView.getBtnValider().setEnabled(currentPeriode() != null && modeModification);
				
	}
	
	private class ActionListenerDateTextField implements ActionListener	{

		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myTextField.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else {
				myTextField.setText(myDate);
				updateUI();
			}
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else {
				myTextField.setText(myDate);
				updateUI();
			}
		}
	}	

}
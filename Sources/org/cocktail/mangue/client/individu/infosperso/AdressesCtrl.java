/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infosperso;
/** Interface pour gerer les adresses Personnelles ou Professionnelles des agents */

/* Une même adresse peut être associée à plusieurs RepartPersonneAdresse différenciés par le type d'adresse. Une seule rpa correspond à l'adresse principale
 * Ici, on ne gère que les adresses de type personnel ou professionnel. Un agent peut donc avoir au plus 2 rpas.
 * Le display group contient toutes les rpas professionnelles et personnels d'un même individu mais il n'affiche que les rpa valides
 * et ils ôtent les doublons dans le cas où une même adresse est associée à deux rpas.
 * Lorsque l'utilisateur crée une adresse ou la modifie, on crée  éventuellement des rpas si l'utilisateur a choisi 
 * les rpas pour les deux types professionnel et personnel
 * Lorsque l'utilisateur supprime une adresse, on invalide les rpas associées à cette adresse si elle a d'autres rpas (fournis,…) associées sinon
 * on détruit les rpas et l'adresse
 * Lorsque l'utilisateur choisit une rpa comme adresse principale, la rpa de l'adresse principale précédente est désactivée comme adresse princpale.
 *
 * Si une adresse apparaît avec un code postal et un popup vide, c'est que la commune n'est pas choisie
 * ou qu'elle est invalide
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.individu.AdressesView;
import org.cocktail.mangue.client.select.PaysSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOCommune;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AdressesCtrl {

	private static final int INDEX_ADRESSES_VALIDES = 0;
	private static AdressesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;

	private AdressesView 			myView;
	private EODisplayGroup 			eod;
	private boolean					peutGererModule;
	private boolean					saisieEnabled, modeCreation, estNormalien;
	private FiltreTypeListener 		listenerType = new FiltreTypeListener();
	private ListenerAdresse 		listenerAdresse = new ListenerAdresse();
	private PaysSelectCtrl 			myPaysSelectCtrl;

	private EORepartPersonneAdresse currentRepart;
	private EOAdresse				currentAdresse;
	private EOIndividu 				currentIndividu;
	private EOPays 					currentPays;
	private EOAgentPersonnel		currentUtilisateur;

	public AdressesCtrl(EOEditingContext editingContext, boolean loadNotifications) {

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new AdressesView(null, eod, MODE_MODAL.booleanValue());

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
				);
		myView.getBtnGetPays().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectPays();}}
				);

		setSaisieEnabled(false);

		myView.getMyEOTable().addListener(listenerAdresse);
		CocktailUtilities.initTextField(myView.getTfPays(), false, false);
		myView.getTfCodePostal().addFocusListener(new ListenerTextFieldCodePostal());
		myView.getTfCodePostal().addActionListener(new ActionListenerCodePostal());

		myView.getPopupVilles().addActionListener(new CommuneListener());
		myView.getCheckFiltrePerso().addActionListener(listenerType);
		myView.getCheckFiltrePro().addActionListener(listenerType);
		myView.getCheckFiltreFact().addActionListener(listenerType);
		myView.getCheckFiltreAutres().addActionListener(new FiltreTypeAutreListener());
		myView.getPopupValidite().addActionListener(new ValiditeListener());

		// Droit de l'utilisateur. 
		// Cacher les filtres de types d'adresse selon les droits de l'agent
		myView.getCheckFiltreAutres().setEnabled(false);
		//myView.getTfVille().setVisible(false);

	}

	public static AdressesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new AdressesCtrl(editingContext, true);
		return sharedInstance;
	}


	public EORepartPersonneAdresse getCurrentRepart() {
		return currentRepart;
	}

	public void setCurrentRepart(EORepartPersonneAdresse currentRepart) {
		this.currentRepart = currentRepart;
	}

	public EOPays getCurrentPays() {
		return currentPays;
	}

	public void setCurrentPays(EOPays currentPays) {
		this.currentPays = currentPays;
		CocktailUtilities.viderTextField(myView.getTfPays());
		if (currentPays != null) {
			CocktailUtilities.setTextToField(myView.getTfPays(), getCurrentPays().libelleLong());
			updateInterface();
		}
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}

	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	public EOAdresse getCurrentAdresse() {
		return currentAdresse;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}


	public void setCurrentAdresse(EOAdresse currentAdresse) {
		this.currentAdresse = currentAdresse;
	}

	/**
	 * 
	 * @return
	 */
	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		setCurrentUtilisateur(utilisateur);

		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());

		myView.getCheckFiltrePerso().setEnabled(utilisateur.peutAfficherInfosPerso());
		myView.getCheckFiltrePro().setEnabled(utilisateur.peutAfficherInfosPerso());
		myView.getCheckFiltreFact().setEnabled(utilisateur.peutAfficherInfosPerso());
		myView.getCheckFiltrePerso().setSelected(utilisateur.peutAfficherInfosPerso());
		myView.getCheckFiltrePro().setSelected(!utilisateur.peutAfficherInfosPerso());

	}

	public void setCurrentIndividu(EOIndividu individu) {
		currentIndividu = individu;
		actualiser();
	}

	public void open(EOIndividu individu)	{
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}

	private EOQualifier getFilterQualifier()	{

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.PERS_ID_KEY+"=%@", new NSArray(getCurrentIndividu().persId())));

		// Restriction sur les types d'adresse
		if (currentUtilisateur.peutAfficherInfosPerso() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"!=%@", new NSArray(EOTypeAdresse.TYPE_PERSONNELLE)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"!=%@", new NSArray(EOTypeAdresse.TYPE_FACTURATION)));
		}
		if (!estNormalien) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"!=%@", new NSArray(EOTypeAdresse.TYPE_ETUDIANT)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"!=%@", new NSArray(EOTypeAdresse.TYPE_PARENT)));
		}

		if (myView.getPopupValidite().getSelectedIndex() == INDEX_ADRESSES_VALIDES)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
		else
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_VALIDE_KEY+"=%@", new NSArray("N")));

		NSMutableArray orQualifiers = new NSMutableArray();

		if (myView.getCheckFiltrePerso().isSelected())	{
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_PERSONNELLE)));
		}
		if (myView.getCheckFiltrePro().isSelected())	{
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_PROFESSIONNELLE)));
		}
		if (myView.getCheckFiltreFact().isSelected())	{
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_FACTURATION)));
		}
		if (myView.getCheckFiltreAutres().isSelected())	{
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_ETUDIANT)));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY+"=%@", new NSArray(EOTypeAdresse.TYPE_PARENT)));
		}

		if (orQualifiers.count() > 0)
			qualifiers.addObject (new EOOrQualifier(orQualifiers));

		return new EOAndQualifier(qualifiers);
	}



	private class CommuneListener implements ActionListener
	{
		public void actionPerformed(ActionEvent anAction) {
			myView.getTfMessage().setText("");
		}
	}
	private class ValiditeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}
	private class FiltreTypeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}
	private class FiltreTypeAutreListener implements ActionListener
	{
		public void actionPerformed(ActionEvent anAction) {
			actualiser();
		}
	}

	/**
	 * 
	 */
	public void getCommune()	{

		NSArray<EOCommune> communes = EOCommune.rechercherCommunes(ec, myView.getTfCodePostal().getText());
		myView.getPopupVilles().removeAllItems();
		myView.getPopupVilles().addItem("");
		for (Enumeration<EOCommune> e =  communes.objectEnumerator();e.hasMoreElements();)
			myView.getPopupVilles().addItem(e.nextElement().libelle());
		updateInterface();

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class ActionListenerCodePostal implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if (isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected())
				getCommune();
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ListenerTextFieldCodePostal implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e)		{
		}

		public void focusLost(FocusEvent e)	{
			if (isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected())
				getCommune();
		}
	}

	/**
	 * 
	 */
	public void actualiser() {

		eod.setObjectArray(new NSArray());
		if (getCurrentIndividu() != null) {
			eod.setObjectArray(EORepartPersonneAdresse.findForQualifier(ec, getFilterQualifier()));
			estNormalien = getCurrentIndividu() != null && EOGrhumParametres.isGestionEns() && getCurrentIndividu().estNormalienSurPeriode(null, null);
			myView.getCheckFiltreAutres().setEnabled(estNormalien);
		}

		myView.getMyEOTable().updateData();

		updateInterface();

	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewAdresse() {
		return myView.getViewAdresse();
	}
	public void toFront() {
		myView.toFront();
	}

	private void ajouter() {

		modeCreation = true;

		// On recherche si l'agent a deja une adresse principale
		EORepartPersonneAdresse adrPrincipale = EORepartPersonneAdresse.adressePrincipale(ec, currentIndividu.persId());

		setCurrentAdresse(EOAdresse.creer(ec, getCurrentUtilisateur()));

		// Ajout d une adresse personnelle
		if (currentUtilisateur.peutAfficherInfosPerso() && myView.getCheckFiltrePerso().isSelected()) {
			myView.getCheckPerso().setSelected(true);
			setCurrentRepart(EORepartPersonneAdresse.creer(ec, getCurrentIndividu().persId(), getCurrentAdresse(), EOTypeAdresse.TYPE_PERSONNELLE));
		}
		else {	// Ajout d'une adresse professionnelle
			setCurrentRepart(EORepartPersonneAdresse.creer(ec, getCurrentIndividu().persId(), getCurrentAdresse(), EOTypeAdresse.TYPE_PROFESSIONNELLE));
			myView.getCheckPro().setSelected(true);
		}

		if (adrPrincipale== null)
			getCurrentRepart().setEstAdressePrincipale(true);
		else
			getCurrentRepart().setEstAdressePrincipale(false);			

		updateDatas();
		setSaisieEnabled(true);
	}

	private void modifier() {
		modeCreation = false;		
		setSaisieEnabled(true);
		CocktailUtilities.viderLabel(myView.getTfMessage());
	}

	/**
	 *  On invalide tous les repart_personne_adresse associes a l adresse selectionnee
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment annuler cette adresse ?", "Oui", "Non"))		
			return;

		try {

			getCurrentRepart().setEstValide(false);
			getCurrentRepart().setEstAdressePrincipale(false);

			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			ec.revert();
		}
	}


	private void valider() {

		try {
			if (!myView.getCheckPerso().isSelected() && !myView.getCheckPro().isSelected() && !myView.getCheckFiltreFact().isSelected())
				throw new ValidationException("Veuillez sélectionner au moins un type PERSONNEL ou PROFESSIONNEL !");

			getCurrentAdresse().setAdrAdresse1(CocktailUtilities.getTextFromField(myView.getTfAdresse()));
			getCurrentAdresse().setAdrAdresse2(CocktailUtilities.getTextFromField(myView.getTfComplement()));
			getCurrentAdresse().setCodePostal(CocktailUtilities.getTextFromField(myView.getTfCodePostal()));
			getCurrentAdresse().setCpEtranger(CocktailUtilities.getTextFromField(myView.getTfCodePostalEtranger()));
			getCurrentAdresse().setAdrBp(CocktailUtilities.getTextFromField(myView.getTfBoitePostale()));

			getCurrentAdresse().setAdrUrlPere(CocktailUtilities.getTextFromField(myView.getTfUrl()));
			getCurrentRepart().setEMail(CocktailUtilities.getTextFromField(myView.getTfMail()));

			if (currentPays.isDefault())
				getCurrentAdresse().setVille((String)myView.getPopupVilles().getSelectedItem());	
			else
				getCurrentAdresse().setVille(CocktailUtilities.getTextFromField(myView.getTfVille()));

			// Possibilite de typer une adresse de facturation en tant qu'adresse personnelle
			if (!modeCreation && myView.getCheckFiltreFact().isSelected()) {
				// Verifier que l'adresse n'existe pas deja
				EORepartPersonneAdresse adrExistante = EORepartPersonneAdresse.findForAdresseEtType(ec, currentIndividu.persId(), currentAdresse, EOTypeAdresse.TYPE_PERSONNELLE);

				if (adrExistante == null && myView.getCheckPerso().isSelected())
					EORepartPersonneAdresse.creer(ec, currentIndividu.persId(), getCurrentAdresse(), EOTypeAdresse.TYPE_PERSONNELLE);				
				else
					adrExistante.setRpaValide(CocktailConstantes.VRAI);
			}

			// Si l'adresse actuelle est principale, les autres sont passees a rpa_principale = 'N'

			if (myView.getCheckPrincipale().isSelected()) {
				NSArray reparts = EORepartPersonneAdresse.adresses(ec, currentIndividu);
				for (Enumeration<EORepartPersonneAdresse> e = reparts.objectEnumerator();e.hasMoreElements();) {
					e.nextElement().setRpaPrincipal(CocktailConstantes.FAUX);				
				}
			}
			currentRepart.setEstAdressePrincipale(myView.getCheckPrincipale().isSelected());

			ec.saveChanges();
			actualiser();
			setSaisieEnabled(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {

		ec.revert();
		ServerProxy.clientSideRequestRevert(ec);
		listenerAdresse.onSelectionChanged();
		setSaisieEnabled(false);
		updateInterface();
	}

	private void clearTextFields() {

		myView.getTfAdresse().setText("");
		myView.getTfComplement().setText("");
		myView.getTfCodePostal().setText("");
		myView.getTfCodePostalEtranger().setText("");
		myView.getTfBoitePostale().setText("");
		myView.getTfVille().setText("");

		myView.getTfMessage().setText("");
		myView.getTfUrl().setText("");
		myView.getTfMail().setText("");

		myView.getCheckPerso().setSelected(false);
		myView.getCheckPro().setSelected(false);
		myView.getCheckFact().setSelected(false);
		myView.getPopupVilles().removeAllItems();

	}

	public boolean isSaisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_LOCK_INFOS_PERSONNELLES, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_UNLOCK_INFOS_PERSONNELLES, null);
		updateInterface();
	}

	private void selectPays() {

		if (myPaysSelectCtrl == null)
			myPaysSelectCtrl = new PaysSelectCtrl(ec);

		EOPays pays = myPaysSelectCtrl.getPays(null, null);
		if (pays != null) {
			currentPays = pays;
			getCurrentAdresse().setToPaysRelationship(currentPays);
			CocktailUtilities.setTextToField(myView.getTfPays(), currentPays.libelleLong());
			if (currentPays.isDefault()) {
				CocktailUtilities.viderTextField(myView.getTfCodePostalEtranger());				
			}
			else {
				CocktailUtilities.viderTextField(myView.getTfCodePostal());				
			}

			if (currentPays.isDefault()) {
				CocktailUtilities.viderTextField(myView.getTfCodePostalEtranger());				
			}
			else {
				CocktailUtilities.viderTextField(myView.getTfCodePostal());				
			}

			updateInterface();
		}
	}

	private boolean peutAjouterAdresse() {
		return !isSaisieEnabled()
				&& myView.getPopupValidite().getSelectedIndex() == INDEX_ADRESSES_VALIDES
				&& !myView.getCheckFiltreFact().isSelected() 
				&& getCurrentIndividu() != null;
	}
	private boolean peutModifierAdresse() {		
		return !isSaisieEnabled() && getCurrentAdresse() != null
				&& myView.getPopupValidite().getSelectedIndex() == INDEX_ADRESSES_VALIDES
				&& getCurrentIndividu() != null;
	}
	private boolean peutSupprimerAdresse() {
		return !saisieEnabled 
				&& currentRepart != null 
				&& currentRepart.estValide() 
				&& !myView.getCheckFiltreFact().isSelected();
	}

	/**
	 * 
	 * Gestion de l'activation de tous les objets de l'interface.
	 * Modification en mode saisie uniquement.
	 * Les adresses de type FACTURATION ne peuvent être modifiees.
	 * 
	 * La ville est déduite automatiquement s'il s'agit d'un pays ETRANGER 
	 * 
	 */
	private void updateInterface() {

		myView.getMyEOTable().setEnabled(!isSaisieEnabled());
		myView.getBtnAjouter().setEnabled(peutAjouterAdresse());		
		myView.getBtnModifier().setEnabled(peutModifierAdresse());
		myView.getBtnSupprimer().setEnabled(peutSupprimerAdresse());

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getBtnGetPays().setEnabled(isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfAdresse(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfComplement(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfCodePostal(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfCodePostalEtranger(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfBoitePostale(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected()) ;
		CocktailUtilities.initTextField(myView.getTfUrl(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfMail(), false, isSaisieEnabled() && !myView.getCheckFiltreFact().isSelected());
		CocktailUtilities.initTextField(myView.getTfVille(), false, isSaisieEnabled() && !currentPays.isDefault() && !myView.getCheckFiltreFact().isSelected());

		myView.getPopupVilles().setEnabled(isSaisieEnabled() && getCurrentPays() != null && getCurrentPays().isDefault() && !myView.getCheckFiltreFact().isSelected());

		myView.getPanelFrance().setVisible(getCurrentPays() != null && getCurrentPays().isDefault());
		myView.getPanelEtranger().setVisible(getCurrentPays() != null && !getCurrentPays().isDefault());

		myView.getCheckValide().setEnabled(false);
		myView.getCheckPrincipale().setEnabled(getCurrentIndividu() != null && saisieEnabled && !myView.getCheckFiltreFact().isSelected());

		myView.getCheckPerso().setEnabled(getCurrentIndividu() != null && saisieEnabled && currentUtilisateur.peutAfficherInfosPerso() && (myView.getCheckFiltreFact().isSelected() && ! myView.getCheckFiltrePerso().isSelected()) );
		myView.getCheckPro().setEnabled(false);
		myView.getCheckFact().setEnabled(false);

	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearTextFields();
		if (getCurrentRepart() != null) {

			setCurrentAdresse(getCurrentRepart().toAdresse());
			if (getCurrentAdresse() != null) {
				setCurrentPays(getCurrentAdresse().toPays());
			}

			myView.getCheckValide().setSelected(getCurrentRepart().estValide());
			myView.getCheckPrincipale().setSelected(getCurrentRepart().estAdressePrincipale());

			if (getCurrentAdresse() != null) {
				CocktailUtilities.setTextToField(myView.getTfAdresse(), getCurrentAdresse().adrAdresse1());
				CocktailUtilities.setTextToField(myView.getTfComplement(), getCurrentAdresse().adrAdresse2());
				CocktailUtilities.setTextToField(myView.getTfCodePostal(), getCurrentAdresse().codePostal());
				CocktailUtilities.setTextToField(myView.getTfCodePostalEtranger(), getCurrentAdresse().cpEtranger());
				CocktailUtilities.setTextToField(myView.getTfBoitePostale(), getCurrentAdresse().adrBp());
				CocktailUtilities.setTextToField(myView.getTfUrl(), getCurrentAdresse().adrUrlPere());
				CocktailUtilities.setTextToField(myView.getTfVille(), getCurrentAdresse().ville());
				if (getCurrentAdresse().codePostal() != null) {
					myView.getTfCodePostal().setText(getCurrentAdresse().codePostal());
					getCommune();
				}
				if (getCurrentAdresse().ville() != null) {
					myView.getPopupVilles().setSelectedItem(getCurrentAdresse().ville());
				}
				// On verifie que la commune saisie soit bien dans la liste des communes connues
				EOCommune commune = EOCommune.findForCodePostalAndLibelle(ec, getCurrentAdresse().codePostal(), getCurrentAdresse().ville());

				if (commune == null && getCurrentAdresse().codePostal() != null && getCurrentPays().isDefault() && getCurrentAdresse().ville() != null)
					myView.getTfMessage().setText("La commune saisie n'est pas connue. Merci de choisir dans la liste!");
			}

			CocktailUtilities.setTextToField(myView.getTfMail(), getCurrentRepart().eMail());

			myView.getCheckPerso().setSelected(getCurrentRepart().estPersonnelle());
			myView.getCheckPro().setSelected(getCurrentRepart().estProfessionnelle());
			myView.getCheckFact().setSelected(getCurrentRepart().estFacturation());

			try {
				if (getCurrentRepart().estFacturation()) {
					EORepartPersonneAdresse repart = EORepartPersonneAdresse.findForAdresseEtType(ec, getCurrentIndividu().persId(), getCurrentRepart().toAdresse(), EOTypeAdresse.TYPE_PERSONNELLE);
					if (repart != null) {
						myView.getCheckPerso().setSelected(true);
					}
				}
				else
					if (getCurrentRepart().estPersonnelle()) {
						EORepartPersonneAdresse repart = EORepartPersonneAdresse.findForAdresseEtType(ec, getCurrentIndividu().persId(), getCurrentRepart().toAdresse(), EOTypeAdresse.TYPE_FACTURATION);
						if (repart != null) {
							myView.getCheckFact().setSelected(true);
						}					
					}
			}
			catch (Exception e) {

			}

		}

	}


	private class ListenerAdresse implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			if (peutModifierAdresse() && peutGererModule())
				modifier();
		}

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentRepart = (EORepartPersonneAdresse)eod.selectedObject();
			updateDatas();
			updateInterface();

		}
	}

}

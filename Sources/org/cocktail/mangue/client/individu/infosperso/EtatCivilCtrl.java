package org.cocktail.mangue.client.individu.infosperso;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;

import javax.swing.JDialog;
import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.individu.EtatCivilView;
import org.cocktail.mangue.client.select.DepartementSelectCtrl;
import org.cocktail.mangue.client.select.PaysSelectCtrl;
import org.cocktail.mangue.client.select.SaisieIndividuCtrl;
import org.cocktail.mangue.client.select.SituationFamilialeSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EODepartement;
import org.cocktail.mangue.common.modele.nomenclatures.EOLimiteAge;
import org.cocktail.mangue.common.modele.nomenclatures.EOLoge;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCivilite;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.swing.EOImageView;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSValidation.ValidationException;

public class EtatCivilCtrl {

	private static EtatCivilCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private EtatCivilView myView;
	private NSData image;

	private boolean saisieEnabled, modeCreation;
	public EOImageView photoAgent;

	private EOIndividu 				currentIndividu;
	private EOPersonnel 			currentPersonnel;
	private EOSituationFamiliale 	currentSituationFamiliale;
	private EOPays 					currentPaysNaissance;
	private EOPays 					currentNationalite;
	private EOLoge 					currentLoge;
	private EOLimiteAge 			currentLimiteAge;
	private EODepartement 			currentDepartement;

	private SituationFamilialeSelectCtrl mySelectorSituationFamiliale;
	private PaysSelectCtrl mySelectorPaysNaissance;
	private PaysSelectCtrl mySelectorNationalite;
	private DepartementSelectCtrl mySelectorDepartement;

	public EtatCivilCtrl(EOEditingContext editingContext) {

		edc = editingContext;

		myView = new EtatCivilView(null, MODE_MODAL.booleanValue());

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		myView.getBtnGetDepartement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectDepartement();}}
		);
		myView.getBtnGetPaysNaissance().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectPaysNaissance();}}
		);
		myView.getBtnGetNationalite().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectNationalite();}}
		);

		setSaisieEnabled(false);

		myView.getTfCodePays().addActionListener(new ActionListenerPaysNaissance());
		myView.getTfCodePays().addFocusListener(new FocusListenerPaysNaissance());

		myView.getTfCodeNationalite().addActionListener(new ActionListenerNationalite());
		myView.getTfCodeNationalite().addFocusListener(new FocusListenerNationalite());

		myView.getTfCodeDepartement().addActionListener(new ActionListenerDepartement());
		myView.getTfCodeDepartement().addFocusListener(new FocusListenerDepartement());

		myView.getTfCodeSituationFamiliale().addActionListener(new ActionListenerSituationFamiliale());
		myView.getTfCodeSituationFamiliale().addFocusListener(new FocusListenerSituationFamiliale());

		myView.getTfCodePays().addActionListener(new ActionListenerPaysNaissance());
		myView.getTfCodePays().addFocusListener(new FocusListenerPaysNaissance());

		myView.setListeCivilites(EOCivilite.find(edc));

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));
		}

		CocktailUtilities.initTextField(myView.getTfDepartement(), false, false);
		CocktailUtilities.initTextField(myView.getTfPaysNaissance(), false, false);
		CocktailUtilities.initTextField(myView.getTfNationalite(), false, false);
		CocktailUtilities.initTextField(myView.getTfSituationFamiliale(), false, false);

		setSaisieEnabled(false);
	}

	public static EtatCivilCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new EtatCivilCtrl(editingContext);
		return sharedInstance;
	}


	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		updateDatas();
	}
	public EOSituationFamiliale getCurrentSituationFamiliale() {
		return currentSituationFamiliale;
	}

	public void setCurrentSituationFamiliale(
			EOSituationFamiliale currentSituationFamiliale) {
		this.currentSituationFamiliale = currentSituationFamiliale;
		myView.getTfCodeSituationFamiliale().setText("");
		myView.getTfSituationFamiliale().setText("");
		if (currentSituationFamiliale != null) {
			myView.getTfCodeSituationFamiliale().setText(currentSituationFamiliale.code());
			myView.getTfSituationFamiliale().setText(currentSituationFamiliale.code());
		}
	}

	public EOPays getCurrentPaysNaissance() {
		return currentPaysNaissance;
	}

	public void setCurrentPaysNaissance(EOPays currentPaysNaissance) {
		this.currentPaysNaissance = currentPaysNaissance;
		myView.getTfCodePays().setText("");
		myView.getTfPaysNaissance().setText("");
		if (currentPaysNaissance != null) {
			myView.getTfCodePays().setText(currentPaysNaissance.code());
			myView.getTfPaysNaissance().setText(currentPaysNaissance.libelleLong());
		}
	}

	public EOPays getCurrentNationalite() {
		return currentNationalite;
	}

	public void setCurrentNationalite(EOPays currentNationalite) {
		this.currentNationalite = currentNationalite;
		myView.getTfCodeNationalite().setText("");
		myView.getTfNationalite().setText("");
		if (currentNationalite != null) {
			myView.getTfCodeNationalite().setText(currentNationalite.code());
			myView.getTfNationalite().setText(currentNationalite.libelleLong());
		}
	}

	public EOLoge getCurrentLoge() {
		return currentLoge;
	}

	public void setCurrentLoge(EOLoge currentLoge) {
		this.currentLoge = currentLoge;
		myView.getPopupLoge().setSelectedItem(currentLoge);
	}

	public EOLimiteAge getCurrentLimiteAge() {
		return currentLimiteAge;
	}

	public void setCurrentLimiteAge(EOLimiteAge currentLimiteAge) {
		this.currentLimiteAge = currentLimiteAge;
		myView.getPopupRetraite().setSelectedItem(currentLimiteAge);
	}

	public EODepartement getCurrentDepartement() {
		return currentDepartement;
	}

	public void setCurrentDepartement(EODepartement currentDepartement) {
		this.currentDepartement = currentDepartement;
		myView.getTfCodeDepartement().setText("");
		myView.getTfDepartement().setText("");
		if (currentDepartement != null) {
			myView.getTfCodeDepartement().setText(currentDepartement.code());
			myView.getTfDepartement().setText(currentDepartement.libelleLong());
		}
	}

	public EOPersonnel getCurrentPersonnel() {
		return currentPersonnel;
	}

	public void setCurrentPersonnel(EOPersonnel currentPersonnel) {
		this.currentPersonnel = currentPersonnel;
	}

	public void open(EOIndividu individu)	{

		clearTextFields();
		setCurrentIndividu(individu);

		myView.setVisible(true);
	}

	public void employeHasChanged(NSNotification  notification) {

		if (notification != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearTextFields();

		if (getCurrentIndividu() != null) {

			myView.getPopupCivilite().setSelectedItem(getCurrentIndividu().cCivilite());

			CocktailUtilities.setTextToField(myView.getTfNomUsuel(), getCurrentIndividu().nomUsuel());
			CocktailUtilities.setTextToField(myView.getTfNomFamille(), getCurrentIndividu().nomPatronymique());
			CocktailUtilities.setTextToField(myView.getTfPrenom(), getCurrentIndividu().prenom());
			CocktailUtilities.setTextToField(myView.getTfPrenoms(), getCurrentIndividu().prenoms());
			CocktailUtilities.setTextToField(myView.getTfNomAffichage(), getCurrentIndividu().nomAffichage());
			CocktailUtilities.setTextToField(myView.getTfPrenomAffichage(), getCurrentIndividu().prenomAffichage());

			CocktailUtilities.setTextToField(myView.getTfInsee(), getCurrentIndividu().indNoInsee());
			CocktailUtilities.setNumberToField(myView.getTfCleInsee(), getCurrentIndividu().indCleInsee());
			CocktailUtilities.setDateToField(myView.getTfDateNaissance(), getCurrentIndividu().dNaissance());
			CocktailUtilities.setTextToField(myView.getTfLieuNaissance(), getCurrentIndividu().villeDeNaissance());

			setCurrentDepartement(getCurrentIndividu().toDepartement());
			setCurrentPaysNaissance(getCurrentIndividu().toPaysNaissance());
			setCurrentNationalite(getCurrentIndividu().toPaysNationalite());
			setCurrentSituationFamiliale(getCurrentIndividu().toSituationFamiliale());
			setCurrentDepartement(getCurrentIndividu().toDepartement());

			if (getCurrentIndividu().personnel() != null) {
				CocktailUtilities.setTextToField(myView.getTfNumen(), getCurrentIndividu().personnel().numen());
				CocktailUtilities.setTextToField(myView.getTfMatricule(), getCurrentIndividu().personnel().noMatricule());
				setCurrentLoge(getCurrentIndividu().personnel().toLoge());
				setCurrentLimiteAge(getCurrentIndividu().personnel().limiteAge());
			}

			if (image == null && getCurrentIndividu().noIndividu() != null) {
				// Parametre 
				if ( EOGrhumParametres.isPhotos() && (getCurrentIndividu().peutAfficherPhoto()) ) {
					image = getCurrentIndividu().image();
				}
			}
		}		

		updateInterface();

	}



	public class ActionListenerDepartement implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
		}
	}
	public class FocusListenerDepartement implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e) {
		}
		public void focusLost(FocusEvent e)	{
		}
	}


	public class ActionListenerPaysNaissance implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
		}
	}
	public class FocusListenerPaysNaissance implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e) {
		}
		public void focusLost(FocusEvent e)	{
		}
	}

	public class ActionListenerNationalite implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
		}
	}
	public class FocusListenerNationalite implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e) {
		}
		public void focusLost(FocusEvent e)	{
		}
	}

	public class ActionListenerSituationFamiliale implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
		}
	}
	public class FocusListenerSituationFamiliale implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e) {
		}
		public void focusLost(FocusEvent e)	{
		}
	}

	public JDialog getView() {
		return myView;
	}
	public JPanel getViewEtatCivil() {
		return myView.getViewEtatCivil();
	}
	public void toFront() {
		myView.toFront();
	}

	private void actualiser() {
		updateDatas();
	}

	/**
	 * 
	 */
	private void ajouter() {

		modeCreation = true;
		EOIndividu selectedIndividu = SaisieIndividuCtrl.sharedInstance(edc).getIndividu(true, true);
		if (selectedIndividu != null) {
			try {
				setCurrentIndividu(selectedIndividu);
				getCurrentIndividu().setNoIndividu(EOIndividu.construireNoIndividu(edc));
				getCurrentIndividu().setPersId(SuperFinder.construirePersId(edc));
				
				// Creation du personnel
				creerPersonnel();
				
				setSaisieEnabled(true);
			}
			catch (Exception e) {
				edc.revert();
				e.printStackTrace();
			}
		}		
	}
	
	
	/**
	 * 
	 * @param estValidation
	 */
	private void creerPersonnel() {
		EOPersonnel personnel = getCurrentIndividu().personnel();
		if (modeCreation && personnel == null) {	
			personnel = new EOPersonnel();
			personnel.initPersonnel(getCurrentIndividu().noIndividu());
			personnel.setToIndividuRelationship(getCurrentIndividu());
			edc.insertObject(personnel);
		}
	}

	/**
	 * 
	 */
	private void modifier() {
		setSaisieEnabled(true);
	}
	
	/**
	 * 
	 */
	private void traitementsAvantValidation() {
		
		getCurrentIndividu().setToPaysNaissanceRelationship(getCurrentPaysNaissance());
		getCurrentIndividu().setToPaysNationaliteRelationship(getCurrentNationalite());
		getCurrentIndividu().setToDepartementRelationship(getCurrentDepartement());
		
	}
	
	/**
	 * 
	 */
	private void valider() {
		try {
			
			traitementsAvantValidation();
			
			edc.saveChanges();
			setSaisieEnabled(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			myView.toFront();
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
	}

	private void annuler() {
		edc.revert();
		actualiser();
		setSaisieEnabled(false);
	}

	public void nettoyerChamps(NSNotification notification) {
		clearTextFields();
		setCurrentIndividu(null);
		updateInterface();
	}

	private void clearTextFields() {

		CocktailUtilities.viderTextField(myView.getTfNomUsuel());
		CocktailUtilities.viderTextField(myView.getTfNomAffichage());
		CocktailUtilities.viderTextField(myView.getTfPrenom());
		CocktailUtilities.viderTextField(myView.getTfPrenoms());
		CocktailUtilities.viderTextField(myView.getTfPrenomAffichage());
		CocktailUtilities.viderTextField(myView.getTfNomFamille());

		myView.getTfDateNaissance().setText("");
		myView.getTfInsee().setText("");
		myView.getTfCleInsee().setText("");
		myView.getTfNumen().setText("");
		myView.getTfMatricule().setText("");
		myView.getTfLieuNaissance().setText("");

		myView.getTfCodeDepartement().setText("");
		myView.getTfCodePays().setText("");
		myView.getTfCodeNationalite().setText("");
		myView.getTfCodeSituationFamiliale().setText("");

		myView.getPopupLoge().setSelectedIndex(0);
		myView.getPopupRetraite().setSelectedIndex(0);
		
		setCurrentDepartement(null);
		setCurrentPaysNaissance(null);
		setCurrentNationalite(null);
		setCurrentSituationFamiliale(null);
		setCurrentLoge(null);
		setCurrentLimiteAge(null);
		setCurrentDepartement(null);


	}


	/**
	 * 
	 * @return
	 */
	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	/**
	 * 
	 */
	public void selectSituationFamiliale() {

		if (mySelectorSituationFamiliale == null)
			mySelectorSituationFamiliale = new SituationFamilialeSelectCtrl(edc);
		EOSituationFamiliale situation = mySelectorSituationFamiliale.getSituation(null);
		if (situation != null) {
			setCurrentSituationFamiliale(situation);
		}
	}

	/**
	 * 
	 */
	public void selectDepartement() {

		if (mySelectorDepartement == null)
			mySelectorDepartement = new DepartementSelectCtrl(edc);
		EODepartement departement = mySelectorDepartement.getDepartement(null, getCurrentIndividu().dNaissance());
		if (departement != null) {
			setCurrentDepartement(departement);
		}
	}
	/**
	 * 
	 */
	public void selectPaysNaissance() {

		if (mySelectorPaysNaissance == null)
			mySelectorPaysNaissance = new PaysSelectCtrl(edc);
		EOPays pays = mySelectorPaysNaissance.getPays(null, getCurrentIndividu().dNaissance());
		if (pays != null) {
			setCurrentPaysNaissance(pays);
		}
	}
	/**
	 * 
	 */
	public void selectNationalite() {

		if (mySelectorNationalite == null)
			mySelectorNationalite = new PaysSelectCtrl(edc);
		EOPays pays = mySelectorNationalite.getPays(null, getCurrentIndividu().dNaissance());
		if (pays != null) {
			setCurrentNationalite(pays);
		}
	}

	
	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnValider().setEnabled(saisieEnabled);
		myView.getBtnAnnuler().setEnabled(saisieEnabled);

		myView.getBtnAjouter().setVisible(!saisieEnabled());
		myView.getBtnModifier().setVisible(!saisieEnabled && getCurrentIndividu() != null);

		CocktailUtilities.initTextField(myView.getTfNomUsuel(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfNomFamille(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfNomAffichage(), false, saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfPrenom(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfPrenomAffichage(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfPrenoms(), false, saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfNumen(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateNaissance(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfInsee(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfCleInsee(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfMatricule(), false, saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfCodePays(), false, false);
		CocktailUtilities.initTextField(myView.getTfCodeNationalite(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfCodeDepartement(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfCodeSituationFamiliale(), false, saisieEnabled());

		myView.getBtnGetDepartement().setVisible(saisieEnabled());
		myView.getBtnGetPaysNaissance().setVisible(saisieEnabled());
		myView.getBtnGetNationalite().setVisible(saisieEnabled());
		myView.getBtnGetSituationFamiliale().setVisible(saisieEnabled());

		myView.getPopupLoge().setEnabled(saisieEnabled());
		myView.getPopupRetraite().setEnabled(saisieEnabled());

	}

}

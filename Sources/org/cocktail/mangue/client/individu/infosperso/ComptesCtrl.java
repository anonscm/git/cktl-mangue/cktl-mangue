/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infosperso;
/** Interface pour gerer les adresses Personnelles ou Professionnelles des agents */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.individu.ComptesView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOCompte;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;

public class ComptesCtrl {

	private EOEditingContext 		ec;
	private ListenerCompte 			listenerCompte = new ListenerCompte();
	private ComptesView 			myView;
	private EODisplayGroup 			eod;

	private EOIndividu 				currentIndividu;
	private EOAgentPersonnel		currentUtilisateur;
	private EOCompte 				currentCompte;
	private EOCompte 				currentCompteUtilisateur;

	public ComptesCtrl(EOEditingContext editingContext) {

		ec = editingContext;
		eod = new EODisplayGroup();
		myView = new ComptesView(null, eod, false);

		myView.getBtnEnvoyer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {envoyerMail();}}
				);

		myView.getMyEOTable().addListener(listenerCompte);

		// Initialisation de l'email emetteur 
		CocktailUtilities.initTextField(myView.getTfEmailFrom(), false, false);
	}
	private EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu individu) {
		this.currentIndividu = individu;
		actualiser();
	}
	public EOCompte getCurrentCompte() {
		return currentCompte;
	}
	public void setCurrentCompte(EOCompte currentCompte) {
		this.currentCompte = currentCompte;
	}


	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	
	/**
	 * 
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		try {
			setCurrentCompteUtilisateur(EOCompte.comptePourIndividu(ec,currentUtilisateur.toIndividu()));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return
	 */
	public EOCompte getCurrentCompteUtilisateur() {
		return currentCompteUtilisateur;
	}
	
	/**
	 * 
	 * @param currentCompteUtilisateur
	 */
	public void setCurrentCompteUtilisateur(EOCompte currentCompteUtilisateur) {
		this.currentCompteUtilisateur = currentCompteUtilisateur;
		CocktailUtilities.viderTextField(myView.getTfEmailFrom());
		if (currentCompteUtilisateur != null && currentCompteUtilisateur.email() != null) {
			myView.getTfEmailFrom().setText(currentCompteUtilisateur.email());
		}
	}
	
	/**
	 * 
	 */
	public void actualiser() {
		try {
			CocktailUtilities.viderTextField(myView.getTfObjet());
			CocktailUtilities.viderTextArea(myView.getTfMessaqge());
			eod.setObjectArray(EOCompte.rechercherPourIndividu(ec, getCurrentIndividu()));
			myView.getMyEOTable().updateData();
			updateUI();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public JPanel getViewCompte() {
		return myView.getViewCompte();
	}
	private class ListenerCompte implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentCompte((EOCompte)eod.selectedObject());
			updateUI();
		}
	}

	/**
	 * 
	 */
	private void updateUI() {
		myView.getBtnEnvoyer().setEnabled(getCurrentCompte() != null && getCurrentCompte().email() != null && myView.getTfEmailFrom().getText().length() > 0);
		CocktailUtilities.initTextField(myView.getTfObjet(), false, getCurrentCompte() != null && getCurrentCompte().email() != null);
		CocktailUtilities.initTextArea(myView.getTfMessaqge(), false, getCurrentCompte() != null && getCurrentCompte().email() != null);
	}

	/**
	 * 
	 */
	private void envoyerMail() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Confirmez-vous l'envoi de ce mail à l'adresse suivante : " + getCurrentCompte().email() + " ?", "Oui", "Non"))		
			return;			

		if (StringCtrl.chaineVide(myView.getTfObjet().getText()) && StringCtrl.chaineVide(myView.getTfObjet().getText())) {
			EODialogs.runErrorDialog("ERREUR", "Vous devez saisir un objet ET un message !");
			return;
		}

		try {
			String sujet  = myView.getTfObjet().getText();
			String	message = myView.getTfMessaqge().getText();
			String emailFrom = myView.getTfEmailFrom().getText();

			// Destinataires : AGENT + UTILISATEUR

			String destinataire = currentCompte.email();

			EOAgentPersonnel agent = ((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel();
			EOCompte compteUtilisateur = EOCompte.comptePourIndividu(ec, agent.toIndividu());
			if (compteUtilisateur != null && compteUtilisateur.email() != null)
				destinataire += ","+compteUtilisateur.email();

			String result = (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, null, "clientSideRequestEnvoyerMailAgent", 
					new Class[] {String.class,String.class,String.class,String.class}, 
					new Object[] {emailFrom, destinataire , sujet, message }, false);
			EODialogs.runInformationDialog("ENVOI MAIL",result);
		} catch (Exception e) {
			LogManager.logException(e);
		}
	}

}

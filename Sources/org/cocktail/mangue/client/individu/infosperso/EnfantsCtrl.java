/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.individu.infosperso;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.individu.EnfantsView;
import org.cocktail.mangue.client.select.SaisieEnfantCtrl;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOLienFiliationEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartEnfant;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodeHandicap;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class EnfantsCtrl {

	private static final int INDEX_SFT_PERE = 0;
	private static final int INDEX_SFT_MERE = 1;

	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;

	private EnfantsView 			myView;

	private EODisplayGroup 			eod;
	private boolean					saisieEnabled, modeCreation;
	private boolean					peutGererModule;

	private PopupFiliationListener 	listenerFiliation = new PopupFiliationListener();
	private ListenerEnfant 			listenerEnfant = new ListenerEnfant();

	private EORepartEnfant			currentRepartEnfant;
	private EOEnfant				currentEnfant;
	private EOIndividu 				currentIndividu;
	private EOLienFiliationEnfant 	currentFiliation;

	public EnfantsCtrl(EOEditingContext edc, boolean loadNotifications) {

		this.edc = edc;

		eod = new EODisplayGroup();
		myView = new EnfantsView(null, eod, MODE_MODAL.booleanValue());

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		myView.getBtnHandicap().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherHandicap();}}
		);

		myView.setFiliations(EOLienFiliationEnfant.fetchAll(edc));
		myView.getPopupLienFiliation().addActionListener(listenerFiliation);
		myView.getCheckSft().addActionListener(new ListenerSft());
		myView.getCheckHandicap().setEnabled(false);

		setSaisieEnabled(false);

		myView.getTfDateNaissance().addFocusListener(new FocusListenerDateTextField(myView.getTfDateNaissance()));
		myView.getTfDateNaissance().addActionListener(new ActionListenerDateTextField(myView.getTfDateNaissance()));
		myView.getTfDateAdoption().addFocusListener(new FocusListenerDateTextField(myView.getTfDateAdoption()));
		myView.getTfDateAdoption().addActionListener(new ActionListenerDateTextField(myView.getTfDateAdoption()));
		myView.getTfDatePec().addFocusListener(new FocusListenerDateTextField(myView.getTfDatePec()));
		myView.getTfDatePec().addActionListener(new ActionListenerDateTextField(myView.getTfDatePec()));
		myView.getTfDateArrivee().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArrivee()));
		myView.getTfDateArrivee().addActionListener(new ActionListenerDateTextField(myView.getTfDateArrivee()));
		myView.getTfDateDeces().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDeces()));
		myView.getTfDateDeces().addActionListener(new ActionListenerDateTextField(myView.getTfDateDeces()));

		myView.getMyEOTable().addListener(listenerEnfant);
	}

	
	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
				
		setPeutGererModule(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());
		myView.getBtnHandicap().setVisible(peutGererModule());

	}

	public EORepartEnfant getCurrentRepartEnfant() {
		return currentRepartEnfant;
	}

	public void setCurrentRepartEnfant(EORepartEnfant currentRepartEnfant) {
		this.currentRepartEnfant = currentRepartEnfant;
		updateData();
	}

	public EOEnfant getCurrentEnfant() {
		return currentEnfant;
	}

	public void setCurrentEnfant(EOEnfant currentEnfant) {
		this.currentEnfant = currentEnfant;
	}

	public EOLienFiliationEnfant getCurrentFiliation() {
		return currentFiliation;
	}

	public void setCurrentFiliation(EOLienFiliationEnfant currentFiliation) {
		this.currentFiliation = currentFiliation;
		myView.getPopupLienFiliation().setSelectedItem(currentFiliation);
	}

	private EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu individu) {
		LogManager.logDetail("EnfantsCtrl.setCurrentIndividu");
		currentIndividu = individu;
		actualiser();
	}

	public EOLienFiliationEnfant getLienFiliation() {
		
		if (myView.getPopupLienFiliation().getSelectedIndex() == 0) {
			return null;
		}
		return (EOLienFiliationEnfant)myView.getPopupLienFiliation().getSelectedItem();
	}

	public NSTimestamp getDateNaissance() {
		return CocktailUtilities.getDateFromField(myView.getTfDateNaissance());
	}
	public void setDateNaissance(NSTimestamp myDate) {
		myView.getTfDateNaissance().setText(DateCtrl.dateToString(myDate));
	}

	public NSTimestamp getDateDeces() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDeces());
	}
	public void setDateDeces(NSTimestamp myDate) {
		myView.getTfDateDeces().setText(DateCtrl.dateToString(myDate));
	}

	public NSTimestamp getDateArriveeFoyer() {
		return CocktailUtilities.getDateFromField(myView.getTfDateArrivee());
	}
	public void setDateArriveeFoyer(NSTimestamp myDate) {
		myView.getTfDateArrivee().setText(DateCtrl.dateToString(myDate));
	}

	public NSTimestamp getDateAdoption() {
		try {
			return DateCtrl.stringToDate(myView.getTfDateAdoption().getText());
		}
		catch (Exception e) {
			return null;
		}
	}
	public void setDateAdoption(NSTimestamp myDate) {
		myView.getTfDateAdoption().setText(DateCtrl.dateToString(myDate));
	}

	public NSTimestamp getDatePec() {
		try {
			return DateCtrl.stringToDate(myView.getTfDatePec().getText());
		}
		catch (Exception e) {
			return null;
		}
	}
	public void setDatePec(NSTimestamp myDate) {
		myView.getTfDatePec().setText(DateCtrl.dateToString(myDate));
	}


	public void afficherHandicap() {
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		PeriodesHandicapEnfantCtrl.sharedInstance(edc).open(getCurrentEnfant());
		listenerEnfant.onSelectionChanged();
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);	
	}

	private class PopupFiliationListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {
			try {
				setCurrentFiliation((EOLienFiliationEnfant)myView.getPopupLienFiliation().getSelectedItem());
			}
			catch (Exception e) {
				setCurrentFiliation(null);
			}			
			updateUI();
		}
	}

	public void actualiser() {
		eod.setObjectArray(EORepartEnfant.rechercherPourParent(edc, getCurrentIndividu()));
		myView.getMyEOTable().updateData();
		updateUI();
	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewEnfants() {
		return myView.getViewEnfants();
	}
	public void toFront() {
		myView.toFront();
	}

	private void ajouter() {

		modeCreation = true;

		EOEnfant selectedEnfant = SaisieEnfantCtrl.sharedInstance(edc).getEnfant(getCurrentIndividu());

		if (selectedEnfant != null) {
			setCurrentEnfant(selectedEnfant);
			setCurrentRepartEnfant(EORepartEnfant.creer(edc, getCurrentIndividu(), getCurrentEnfant()));

			updateData();
			setSaisieEnabled(true);			
			CocktailUtilities.setFocusOn(myView.getTfDateNaissance());
		}

	}

	private void modifier() {
		modeCreation = false;
		setSaisieEnabled(true);
	}


	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment invalider cet enfant ?", "Oui", "Non"))		
			return;

		try {

//			NSArray parents = EORepartEnfant.rechercherRepartsPourEnfant(ec, currentEnfant());
//			if (parents.count() == 1)
//				currentEnfant().setTemValide(CocktailConstantes.FAUX);
//			else
			edc.deleteObject(getCurrentRepartEnfant());

			edc.saveChanges();
			actualiser();
			modifierNombreEnfants();
		}
		catch (Exception e) {
			edc.revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void modifierNombreEnfants() {
		try {

			int nbACharges = 0; int nbEnfants = 0;
			java.util.Enumeration<EORepartEnfant> e = eod.displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EORepartEnfant repart = e.nextElement();
				nbEnfants++;
				if (repart.estACharge())
					nbACharges++;
			}					
			getCurrentIndividu().personnel().setNbEnfantsACharge(new Integer(nbACharges));
			getCurrentIndividu().personnel().setNbEnfants(new Integer(nbEnfants));
			edc.saveChanges();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
			EODialogs.runErrorDialog("ERREUR", "Erreur de la mise à jour du nombre d'enfants à charge !");
		}
	}

	/**
	 * 
	 * @return
	 */
	protected boolean traitementsAvantValidation() {

		// Gestion du SFT. Il ne doit etre accorde qu'a un seul des deux parents
		// On annule tous les SFT existant pour l'enfant selectionne
		NSArray<EORepartEnfant> reparts = EORepartEnfant.rechercherRepartsPourEnfant(edc, getCurrentEnfant());
		for (EORepartEnfant myRepart : reparts) {
			myRepart.setProcureSFT(false);
			myRepart.setLienFiliationRelationship(getCurrentFiliation());
		}

		if (aDroitsSft()) {

			// Si le droit est accorde au pere et que l'agent est un homme on place le temoin
			if (getCurrentIndividu().estHomme() && myView.getPopupDroitsSft().getSelectedIndex() == INDEX_SFT_PERE) {				
				getCurrentRepartEnfant().setProcureSFT(true);
			}
			else {
				if ( !getCurrentIndividu().estHomme() && myView.getPopupDroitsSft().getSelectedIndex() == INDEX_SFT_MERE) {
					getCurrentRepartEnfant().setProcureSFT(true);
				}
				else {
					for (EORepartEnfant myRepart : reparts) {						

						if (myRepart.parent().estHomme() && myView.getPopupDroitsSft().getSelectedIndex() == INDEX_SFT_PERE)
							myRepart.setProcureSFT(true);
						else
							if (!myRepart.parent().estHomme() && myView.getPopupDroitsSft().getSelectedIndex() == INDEX_SFT_MERE)
								myRepart.setProcureSFT(true);						
					}
				}
			}
		}

		return true;	
	}

	/**
	 * 
	 * @param myTable
	 * @param myObject
	 */
	private void refreshWithSelection(ZEOTable myTable, EOEnterpriseObject myObject) {		
		actualiser();
		myTable.forceNewSelectionOfObjects(new NSArray(myObject));
	}
	
	/**
	 * 
	 */
	private void valider() {

		try {

			getCurrentEnfant().setSexe((String)myView.getPopupSexe().getSelectedItem());

			getCurrentEnfant().setNom(CocktailUtilities.getTextFromField(myView.getTfNom()));
			getCurrentEnfant().setPrenom(CocktailUtilities.getTextFromField(myView.getTfPrenom()));
			getCurrentEnfant().setLieuNaissance(CocktailUtilities.getTextFromField(myView.getTfLieuNaissance()));

			getCurrentEnfant().setEstMortPourLaFrance(myView.getCheckMortFrance().isSelected());

			getCurrentEnfant().setDNaissance(getDateNaissance());
			getCurrentEnfant().setDAdoption(getDateAdoption());
			getCurrentEnfant().setDMaxACharge(getDatePec());
			getCurrentEnfant().setDArriveeFoyer(getDateArriveeFoyer());
			getCurrentEnfant().setDDeces(getDateDeces());

			if (!traitementsAvantValidation())
				return;
		
			getCurrentRepartEnfant().setProcureSFT(aDroitsSft());
			getCurrentRepartEnfant().setEstACharge(myView.getCheckACharge().isSelected());
			getCurrentRepartEnfant().setLienFiliationRelationship(getLienFiliation());
			edc.saveChanges();
			refreshWithSelection(myView.getMyEOTable(), getCurrentRepartEnfant());

			modifierNombreEnfants();
			setSaisieEnabled(false);
			
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {

		edc.revert();
		ServerProxy.clientSideRequestRevert(edc);
		listenerEnfant.onSelectionChanged();
		setSaisieEnabled(false);
		updateUI();
	}

	private void clearTextFields() {

		setCurrentEnfant(null);
		setCurrentFiliation(null);

		myView.getTfDateNaissance().setText("");
		myView.getTfDateAdoption().setText("");
		myView.getTfDateDeces().setText("");
		myView.getTfDateArrivee().setText("");
		myView.getTfDatePec().setText("");

		myView.getTfLieuNaissance().setText("");
		myView.getTfNom().setText("");
		myView.getTfPrenom().setText("");

		myView.getCheckACharge().setSelected(false);
		myView.getCheckACharge().setSelected(false);
		myView.getCheckACharge().setSelected(false);
		myView.getCheckACharge().setSelected(false);

	}

	private boolean modeCreation() {
		return modeCreation;
	}
	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_LOCK_INFOS_PERSONNELLES, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_UNLOCK_INFOS_PERSONNELLES, null);
	}
	private boolean aDroitsSft() {
		return myView.getCheckSft().isSelected();
	}

	private void updateUI() {

		myView.getMyEOTable().setEnabled(!saisieEnabled());

		myView.getBtnAjouter().setEnabled(!saisieEnabled() && getCurrentIndividu() != null);		
		myView.getBtnModifier().setEnabled(!saisieEnabled() && getCurrentRepartEnfant() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && getCurrentRepartEnfant() != null);

		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfNom(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfPrenom(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfLieuNaissance(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateNaissance(), false, saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDeces(), false, 
				saisieEnabled() && getDateNaissance() != null);
		CocktailUtilities.initTextField(myView.getTfDateArrivee(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDatePec(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateArrivee(), false, 
				saisieEnabled() && getCurrentFiliation() != null && !getCurrentFiliation().estEnfantLegitime());
		CocktailUtilities.initTextField(myView.getTfDateAdoption(), false, 
				saisieEnabled() && getCurrentFiliation() != null && getCurrentFiliation().estAdopte());

		myView.getPopupSexe().setEnabled(saisieEnabled());
		myView.getPopupDroitsSft().setEnabled(saisieEnabled());
		myView.getPopupDroitsSft().setVisible(aDroitsSft());
		myView.getPopupLienFiliation().setEnabled(saisieEnabled());

		myView.getCheckMortFrance().setEnabled(saisieEnabled && getDateDeces() != null);
		myView.getCheckSft().setEnabled(saisieEnabled);
		myView.getCheckACharge().setEnabled(saisieEnabled);

	}

	private void updateData() {

		clearTextFields();
		if (getCurrentRepartEnfant() != null) {

			setCurrentEnfant(getCurrentRepartEnfant().enfant());
			setCurrentFiliation(getCurrentRepartEnfant().lienFiliation());

			myView.getCheckMortFrance().setSelected(getCurrentEnfant().estMortPourLaFrance());
			myView.getCheckACharge().setSelected(getCurrentRepartEnfant().estACharge());

			CocktailUtilities.setTextToField(myView.getTfNom(), getCurrentEnfant().nom());
			CocktailUtilities.setTextToField(myView.getTfPrenom(), getCurrentEnfant().prenom());
			CocktailUtilities.setTextToField(myView.getTfLieuNaissance(), getCurrentEnfant().lieuNaissance());

			myView.getPopupSexe().setSelectedItem(getCurrentEnfant().sexe());

			NSArray periodesHandicap = EOPeriodeHandicap.rechercherPourEnfant(edc, getCurrentEnfant());
			myView.getCheckHandicap().setSelected(periodesHandicap.count() > 0);

			CocktailUtilities.setDateToField(myView.getTfDateNaissance(), getCurrentEnfant().dNaissance());
			CocktailUtilities.setDateToField(myView.getTfDatePec(), getCurrentEnfant().dMaxACharge());
			CocktailUtilities.setDateToField(myView.getTfDateArrivee(), getCurrentEnfant().dArriveeFoyer());
			CocktailUtilities.setDateToField(myView.getTfDateAdoption(), getCurrentEnfant().dAdoption());
			CocktailUtilities.setDateToField(myView.getTfDateDeces(), getCurrentEnfant().dDeces());

			// Gestion du popup des droits SFT
			myView.getCheckSft().setSelected(false);
			myView.getPopupDroitsSft().setVisible(false);

			NSArray reparts = EORepartEnfant.rechercherRepartsPourEnfant(edc, getCurrentEnfant());
			for (Enumeration<EORepartEnfant> e=reparts.objectEnumerator();e.hasMoreElements();) {
				EORepartEnfant repart = e.nextElement();
				if (repart.procureSFT()) {
					myView.getPopupDroitsSft().setVisible(true);
					myView.getCheckSft().setSelected(true);
					if (repart.parent().estHomme())
						myView.getPopupDroitsSft().setSelectedIndex(INDEX_SFT_PERE);
					else
						myView.getPopupDroitsSft().setSelectedIndex(INDEX_SFT_MERE);
				}
			}
		}

		updateUI();
	}

	private class ListenerSft implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			updateUI();
		}
	}


	private class ListenerEnfant implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			if (getCurrentRepartEnfant() != null && peutGererModule())
				modifier();
		}

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			setCurrentRepartEnfant((EORepartEnfant)eod.selectedObject());
		}
	}



	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			if (myTextField == myView.getTfDateNaissance())
				setDatePec(DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutAnnees(getDateNaissance(), 16)));
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	

}
package org.cocktail.mangue.client.individu.infosperso;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.util.Enumeration;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.individu.IndividuUrgenceView;
import org.cocktail.mangue.client.select.IndividuSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOCommune;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOCivilite;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonneTelephone;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuUrgence;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSValidation.ValidationException;

public class IndividuUrgenceCtrl {

	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private IndividuUrgenceView myView;

	private boolean saisieEnabled;

	private EOIndividuUrgence 	currentUrgence;
	private EOIndividu 			currentIndividu;
	private EOIndividu 			currentIndividuUrgence;
	private EOAdresse 			currentAdresseUrgence;

	public IndividuUrgenceCtrl(EOEditingContext editingContext) {

		edc = editingContext;

		myView = new IndividuUrgenceView(null, MODE_MODAL.booleanValue());

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
				);
		myView.getBtnDelAdresse().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delAdresse();}}
				);
		myView.getBtnSelectIndividu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectIndividu();}}
				);

		setSaisieEnabled(false);
		myView.getTfCodePostal().addFocusListener(new ListenerTextFieldCodePostal());
		myView.getTfCodePostal().addActionListener(new ActionListenerCodePostal());

		myView.getCheckPersonnel().addActionListener(new ActionListenerPersonnel());

		CocktailUtilities.initTextField(myView.getTfNomUsuel(), false, false);
		CocktailUtilities.initTextField(myView.getTfPrenom(), false, false);
		CocktailUtilities.initPopupAvecListe(myView.getPopupCivilite(), EOCivilite.find(edc), true);

	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

	}




	public EOIndividuUrgence getCurrentUrgence() {
		return currentUrgence;
	}

	public void setCurrentUrgence(EOIndividuUrgence currentUrgence) {
		this.currentUrgence = currentUrgence;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EOIndividu getCurrentIndividuUrgence() {
		return currentIndividuUrgence;
	}

	public void setCurrentIndividuUrgence(EOIndividu currentIndividuUrgence) {
		this.currentIndividuUrgence = currentIndividuUrgence;
		if (currentIndividuUrgence != null) {
			myView.getPopupCivilite().setSelectedItem(currentIndividuUrgence.cCivilite());
			CocktailUtilities.setTextToField(myView.getTfNomUsuel(),currentIndividuUrgence.nomUsuel());
			CocktailUtilities.setTextToField(myView.getTfPrenom(),currentIndividuUrgence.prenom());

			CocktailUtilities.viderTextField(myView.getTfTelephone1());
			CocktailUtilities.viderTextField(myView.getTfTelephone2());

			NSArray<EOPersonneTelephone> telephonesPerso = EOPersonneTelephone.rechercherTelEtMobilePersonnels(edc, currentIndividuUrgence);
			if (telephonesPerso.size() > 0) {
				CocktailUtilities.setTextToField(myView.getTfTelephone1(), telephonesPerso.get(0).noTelephoneAffichage());
				if (telephonesPerso.size() > 1) {
					CocktailUtilities.setTextToField(myView.getTfTelephone2(), telephonesPerso.get(1).noTelephoneAffichage());
				}
			}

			clearAdresseUrgence();
			NSArray<EORepartPersonneAdresse> adressesPerso = EORepartPersonneAdresse.adressesPersoValides(edc, currentIndividuUrgence);
			if (adressesPerso.size() > 0) {
				afficherAdresse(adressesPerso.get(0).toAdresse());
			}

		}
	}

	public EOAdresse getCurrentAdresseUrgence() {
		return currentAdresseUrgence;
	}

	public void setCurrentAdresseUrgence(EOAdresse currentAdresseUrgence) {
		this.currentAdresseUrgence = currentAdresseUrgence;
	}

	public void selectIndividu() {

		EOIndividu selectedIndividu = IndividuSelectCtrl.sharedInstance(edc).getIndividu();
		if (selectedIndividu != null) {
			setCurrentIndividuUrgence(selectedIndividu);
		}
	}

	public void actualiser() {
		setCurrentUrgence(EOIndividuUrgence.findForIndividu(edc, currentIndividu));
		updateData();
	}

	/**
	 * 
	 * @param adresse
	 */
	private void afficherAdresse(EOAdresse adresse) {

		CocktailUtilities.setTextToField(myView.getTfAdresse(),adresse.adrAdresse1());
		CocktailUtilities.setTextToField(myView.getTfComplement(),adresse.adrAdresse2());

		if (adresse.codePostal() != null) {
			CocktailUtilities.setTextToField(myView.getTfCodePostal(),adresse.codePostal());
			getCommune();
		}
		if (adresse.ville() != null)
			myView.getPopupVilles().setSelectedItem(adresse.ville());

	}

	private void updateData() {

		clearTextFields();

		if (getCurrentUrgence() != null) {

			setCurrentIndividuUrgence(getCurrentUrgence().toIndividuUrgence());

			myView.getCheckPersonnel().setSelected(getCurrentUrgence().estPersonnel());

			if ( getCurrentUrgence().estPersonnel() == false ) {
				myView.getPopupCivilite().setSelectedItem(getCurrentUrgence().urgCivilite());
				CocktailUtilities.setTextToField(myView.getTfNomUsuel(),getCurrentUrgence().urgNom());
				CocktailUtilities.setTextToField(myView.getTfPrenom(),getCurrentUrgence().urgPrenom());
				CocktailUtilities.setTextToField(myView.getTfTelephone1(),getCurrentUrgence().urgTelephone1());
				CocktailUtilities.setTextToField(myView.getTfTelephone2(),getCurrentUrgence().urgTelephone2());

				// Adresse
				setCurrentAdresseUrgence(getCurrentUrgence().toAdresse());
				if (getCurrentAdresseUrgence() != null) {
					afficherAdresse(getCurrentAdresseUrgence());
				}

			}

			CocktailUtilities.setTextToField(myView.getTfCommentaires(),getCurrentUrgence().commentaire());

		}

		updateUI();

	}

	public void getCommune()	{
		NSArray communes = EOCommune.rechercherCommunes(edc, myView.getTfCodePostal().getText());
		myView.getPopupVilles().removeAllItems();
		for (Enumeration<EOCommune> e =  communes.objectEnumerator();e.hasMoreElements();)
			myView.getPopupVilles().addItem(e.nextElement().libelle());
	}

	public class ActionListenerPersonnel implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{

			if (myView.getCheckPersonnel().isSelected() == false) {
				setCurrentIndividuUrgence(null);
			}
			else {
				myView.getPopupCivilite().setSelectedIndex(0);
				myView.getTfNomUsuel().setText("");
				myView.getTfPrenom().setText("");
				myView.getTfTelephone1().setText("");
				myView.getTfTelephone2().setText("");
			}

			updateUI();
		}
	}


	public class ActionListenerCodePostal implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			getCommune();
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ListenerTextFieldCodePostal implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e)		{
		}

		public void focusLost(FocusEvent e)	{
			getCommune();
		}
	}
	public JPanel getViewUrgence() {
		return myView.getViewConjoint();
	}
	private void delAdresse() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Confirmez-vous la suppression de cette adresse  ?", "Oui", "Non"))		
			return;			

		getCurrentUrgence().setToAdresseRelationship(null);
		setCurrentAdresseUrgence(null);

		clearAdresseUrgence();

		updateUI();

	}

	private void clearAdresseUrgence() {

		myView.getTfAdresse().setText("");
		myView.getTfComplement().setText("");
		myView.getTfCodePostal().setText("");
		myView.getPopupVilles().removeAllItems();

	}


	/**
	 * Ajout d un nouveau numero d'urgence
	 */
	private void ajouter() {
		setCurrentIndividuUrgence(null);
		setCurrentUrgence(EOIndividuUrgence.creer(edc, currentIndividu));
		setSaisieEnabled(true);

	}

	private void modifier() {
		setSaisieEnabled(true);
	}

	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer ces données ?", "Oui", "Non"))		
			return;			

		try {
			edc.deleteObject(getCurrentUrgence());
			edc.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			edc.revert();
		}
	}


	private void valider() {

		try {

			if (getCurrentIndividu() == null)
				throw new ValidationException("Veuillez sélectionner un agent !");

			getCurrentUrgence().setCommentaire(CocktailUtilities.getTextFromField(myView.getTfCommentaires()));

			getCurrentUrgence().setToIndividuRelationship(getCurrentIndividu());
			getCurrentUrgence().setToIndividuUrgenceRelationship(getCurrentIndividuUrgence());

			if (getCurrentUrgence().estPersonnel() == false) {
				if (myView.getPopupCivilite().getSelectedIndex() > 0) {
					getCurrentUrgence().setUrgCivilite(((EOCivilite)myView.getPopupCivilite().getSelectedItem()).cCivilite());
				}
				getCurrentUrgence().setUrgNom(CocktailUtilities.getTextFromField(myView.getTfNomUsuel()));
				getCurrentUrgence().setUrgPrenom(CocktailUtilities.getTextFromField(myView.getTfPrenom()));
				getCurrentUrgence().setUrgTelephone1(CocktailUtilities.getTextFromField(myView.getTfTelephone1()));
				getCurrentUrgence().setUrgTelephone2(CocktailUtilities.getTextFromField(myView.getTfTelephone2()));

				// Enregistrement de l'adresse
				if (getCurrentAdresseUrgence() == null) {
					if (myView.getTfCodePostal().getText().length() > 0 && myView.getPopupVilles().getComponentCount() > 0) {
						// Creation de l'adresse
						setCurrentAdresseUrgence(EOAdresse.creer(edc, ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel()));
					}
					//					else
					//						throw new ValidationException("Veuillez entrer au minimum le code postal et la ville pour une nouvelle adresse !");
				}
				if (getCurrentAdresseUrgence() != null) {

					getCurrentAdresseUrgence().setAdrAdresse1(CocktailUtilities.getTextFromField(myView.getTfAdresse()));
					getCurrentAdresseUrgence().setAdrAdresse2(CocktailUtilities.getTextFromField(myView.getTfComplement()));				
					getCurrentAdresseUrgence().setCodePostal(CocktailUtilities.getTextFromField(myView.getTfCodePostal()));

					if (myView.getPopupVilles().getItemCount() > 0)
						getCurrentAdresseUrgence().setVille((String)myView.getPopupVilles().getSelectedItem());

					getCurrentAdresseUrgence().setToPaysRelationship(EOPays.getDefault(edc));

				}

			}
			else {
				getCurrentUrgence().setUrgCivilite(null);
				getCurrentUrgence().setUrgNom(null);
				getCurrentUrgence().setUrgPrenom(null);				
				getCurrentUrgence().setUrgTelephone1(null);
				getCurrentUrgence().setUrgTelephone2(null);

			}

			edc.saveChanges();

			// FIXME - Pb Contrainte deferrable
			getCurrentUrgence().setToAdresseRelationship(getCurrentAdresseUrgence());

			edc.saveChanges();

			setSaisieEnabled(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
	}

	private void annuler() {
		edc.revert();
		actualiser();
		setSaisieEnabled(false);
	}

	public void nettoyerChamps(NSNotification notification) {

		clearTextFields();
		setCurrentIndividu(null);
		setCurrentUrgence(null);
		updateUI();
	}

	private void clearTextFields() {

		myView.getCheckPersonnel().setSelected(false);
		myView.getPopupCivilite().setSelectedIndex(0);
		CocktailUtilities.viderTextField(myView.getTfNomUsuel());
		CocktailUtilities.viderTextField(myView.getTfPrenom());
		CocktailUtilities.viderTextField(myView.getTfTelephone1());
		CocktailUtilities.viderTextField(myView.getTfTelephone2());
		myView.getTfCommentaires().setText("");

		clearAdresseUrgence();

	}

	/**
	 * 
	 * @return
	 */
	private boolean saisieEnabled() {
		return saisieEnabled;
	}

	/**
	 * 
	 * @param yn
	 */
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_LOCK_INFOS_PERSONNELLES, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_UNLOCK_INFOS_PERSONNELLES, null);
	}


	/**
	 * 
	 * @return
	 */
	private boolean peutModifierUrgence() {
		try {
			return saisieEnabled() && getCurrentUrgence() != null ;
		}
		catch (Exception e) {
			return true;
		}

	}

	/**
	 * 
	 */
	private void updateUI() {

		boolean peutModifierConjoint = peutModifierUrgence();
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null && !saisieEnabled() && getCurrentUrgence() == null);
		myView.getBtnModifier().setEnabled(!saisieEnabled() && getCurrentUrgence() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && getCurrentUrgence() != null);

		myView.getBtnSelectIndividu().setEnabled(saisieEnabled() && getCurrentUrgence() != null && myView.getCheckPersonnel().isSelected() == true);

		myView.getCheckPersonnel().setEnabled(saisieEnabled());
		myView.getTfCommentaires().setEnabled(saisieEnabled());
		myView.getTfTelephone1().setEnabled(saisieEnabled());
		myView.getTfTelephone2().setEnabled(saisieEnabled());

		myView.getPopupCivilite().setEnabled(saisieEnabled() && peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfNomUsuel(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfPrenom(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfTelephone1(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfTelephone2(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);

		CocktailUtilities.initTextField(myView.getTfAdresse(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfComplement(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfCodePostal(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		myView.getPopupVilles().setEnabled(saisieEnabled() && peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		myView.getBtnDelAdresse().setEnabled(saisieEnabled() && peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);

	}

}

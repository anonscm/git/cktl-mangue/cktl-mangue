package org.cocktail.mangue.client.individu.infosperso;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.util.Enumeration;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.contrats.EmployeursCtrl;
import org.cocktail.mangue.client.gui.individu.ConjointView;
import org.cocktail.mangue.client.select.IndividuSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOCommune;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOCivilite;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOConjoint;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSValidation.ValidationException;

public class IndividuConjointCtrl {

	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private ConjointView myView;

	private boolean saisieEnabled;

	private EOConjoint 			currentConjoint;
	private EOIndividu 			currentIndividu;
	private EOIndividu 			currentIndividuConjoint;
	private EOAdresse 			currentAdresseConjoint;
	private EOStructure 		currentStructure;

	EmployeursCtrl employeursCtrl = null;

	public IndividuConjointCtrl(EOEditingContext editingContext) {

		edc = editingContext;

		myView = new ConjointView(null, MODE_MODAL.booleanValue());

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
				);
		myView.getBtnDelAdresse().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delAdresse();}}
				);
		myView.getBtnSelectIndividu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectIndividu();}}
				);

		employeursCtrl = new EmployeursCtrl(edc, "EMPLOYEUR PRINCIPAL");
		employeursCtrl.setClasseParent(getClass().getName());
		employeursCtrl.setCtrlConjoint(this);
		myView.getSwapViewEmployeur().add("EMPLOYEURS",employeursCtrl.getView());
		((CardLayout)myView.getSwapViewEmployeur().getLayout()).show(myView.getSwapViewEmployeur(), "EMPLOYEURS");				

		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("associerEmployeur", new Class[] { NSNotification.class }), "NotifSelectedStructureHasChanged", null);

		setSaisieEnabled(false);
		myView.getTfCodePostal().addFocusListener(new ListenerTextFieldCodePostal());
		myView.getTfCodePostal().addActionListener(new ActionListenerCodePostal());

		myView.getCheckPersonnel().addActionListener(new ActionListenerPersonnel());

		CocktailUtilities.initPopupAvecListe(myView.getPopupCivilite(), EOCivilite.find(edc), true);

		CocktailUtilities.initTextField(myView.getTfNomUsuel(), false, false);
		CocktailUtilities.initTextField(myView.getTfPrenom(), false, false);

	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {

		myView.getBtnAjouter().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnModifier().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());
		myView.getBtnSupprimer().setVisible(utilisateur.peutAfficherInfosPerso() && utilisateur.peutGererAgents());

	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOConjoint getCurrentConjoint() {
		return currentConjoint;
	}

	public void setCurrentConjoint(EOConjoint currentConjoint) {
		this.currentConjoint = currentConjoint;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EOIndividu getCurrentIndividuConjoint() {
		return currentIndividuConjoint;
	}
	public void setCurrentIndividuConjoint(EOIndividu currentIndividuConjoint) {
		this.currentIndividuConjoint = currentIndividuConjoint;
		if (currentIndividuConjoint != null) {

			EOCivilite civilite = EOCivilite.findForCode(getEdc(), currentIndividuConjoint.cCivilite());
			myView.getPopupCivilite().setSelectedItem(civilite);

			CocktailUtilities.setTextToField(myView.getTfNomUsuel(),currentIndividuConjoint.nomUsuel());
			CocktailUtilities.setTextToField(myView.getTfPrenom(),currentIndividuConjoint.prenom());

			clearAdresseConjoint();
			NSArray<EORepartPersonneAdresse> adressesPerso = EORepartPersonneAdresse.adressesPersoValides(edc, currentIndividuConjoint);
			if (adressesPerso.size() > 0) {
				afficherAdresse(adressesPerso.get(0).toAdresse());
			}

		}
	}

	/**
	 * 
	 * @param adresse
	 */
	private void afficherAdresse(EOAdresse adresse) {

		CocktailUtilities.setTextToField(myView.getTfAdresse(),adresse.adrAdresse1());
		CocktailUtilities.setTextToField(myView.getTfComplement(),adresse.adrAdresse2());

		if (adresse.codePostal() != null) {
			CocktailUtilities.setTextToField(myView.getTfCodePostal(),adresse.codePostal());
			getCommune();
		}
		if (adresse.ville() != null)
			myView.getPopupVilles().setSelectedItem(adresse.ville());

	}

	public EOAdresse getCurrentAdresseConjoint() {
		return currentAdresseConjoint;
	}

	public void setCurrentAdresseConjoint(EOAdresse currentAdresseConjoint) {
		this.currentAdresseConjoint = currentAdresseConjoint;
	}

	public EOStructure currentStructure() {
		return currentStructure;
	}

	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
	}

	public void selectIndividu() {

		EOIndividu selectedIndividu = IndividuSelectCtrl.sharedInstance(edc).getIndividu();
		if (selectedIndividu != null) {

			setCurrentIndividuConjoint(selectedIndividu);

		}

	}

	/**
	 * 
	 */
	public void actualiser() {
		setCurrentConjoint(EOConjoint.findForIndividu(getEdc(), getCurrentIndividu()));
		updateDatas();
	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearTextFields();

		if (getCurrentConjoint() != null) {

			setCurrentStructure(getCurrentConjoint().employeur());
			employeursCtrl.update(currentStructure(), null);

			setCurrentIndividuConjoint(getCurrentConjoint().individuConjoint());

			myView.getCheckPersonnel().setSelected(getCurrentConjoint().estPersonnel());

			if ( getCurrentConjoint().estPersonnel() == false ) {
				
				EOCivilite civilite = EOCivilite.findForCode(getEdc(), getCurrentConjoint().conjCivilite());
				
				myView.getPopupCivilite().setSelectedItem(civilite);
				CocktailUtilities.setTextToField(myView.getTfNomUsuel(),getCurrentConjoint().conjNom());
				CocktailUtilities.setTextToField(myView.getTfPrenom(),getCurrentConjoint().conjPrenom());
			}

			CocktailUtilities.setTextToField(myView.getTfCommentaires(),getCurrentConjoint().commentaire());

			// Adresse vacation
			setCurrentAdresseConjoint(getCurrentConjoint().adresseEmployeur());
			if (getCurrentAdresseConjoint() != null)
				afficherAdresse(getCurrentAdresseConjoint());

		}

		updateUI();

	}

	public void getCommune()	{
		NSArray communes = EOCommune.rechercherCommunes(edc, myView.getTfCodePostal().getText());
		myView.getPopupVilles().removeAllItems();
		for (Enumeration<EOCommune> e =  communes.objectEnumerator();e.hasMoreElements();)
			myView.getPopupVilles().addItem(e.nextElement().libelle());
	}

	public class ActionListenerPersonnel implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{

			if (myView.getCheckPersonnel().isSelected() == false) {
				setCurrentIndividuConjoint(null);
			}
			else {
				myView.getPopupCivilite().setSelectedIndex(0);
				myView.getTfNomUsuel().setText("");
				myView.getTfPrenom().setText("");
			}

			updateUI();
		}
	}


	public class ActionListenerCodePostal implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			getCommune();
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ListenerTextFieldCodePostal implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e)		{
		}

		public void focusLost(FocusEvent e)	{
			getCommune();
		}
	}
	public JPanel getViewConjoint() {
		return myView.getViewConjoint();
	}
	private void delAdresse() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Confirmez-vous la suppression de l'adresse du conjoint ?", "Oui", "Non"))		
			return;			

		currentConjoint.setAdresseEmployeurRelationship(null);
		currentAdresseConjoint = null;

		clearAdresseConjoint();

		updateUI();

	}

	private void clearAdresseConjoint() {

		myView.getTfAdresse().setText("");
		myView.getTfComplement().setText("");
		myView.getTfCodePostal().setText("");
		myView.getPopupVilles().removeAllItems();

	}


	/**
	 * Ajout d un nouveau conjoint
	 */
	private void ajouter() {
		try {
			currentIndividu.peutAvoirConjoint();
		}			
		catch (ValidationException ve) {
			EODialogs.runErrorDialog("ERREUR", ve.getMessage());
			return;
		}

		setCurrentIndividuConjoint(null);
		setCurrentConjoint(EOConjoint.creer(edc, currentIndividu));
		setSaisieEnabled(true);

	}

	private void modifier() {
		setSaisieEnabled(true);
	}

	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer ce conjoint ?", "Oui", "Non"))		
			return;			

		try {
			edc.deleteObject(getCurrentConjoint());
			edc.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			edc.revert();
		}
	}

	public void associerEmployeur(NSNotification aNotif) {
		try {
			currentConjoint.setEmployeurRelationship((EOStructure)aNotif.object());
			edc.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			edc.revert();
			e.printStackTrace();
		}		
	}
	public void supprimerEmployeur() {
		try {
			currentConjoint.setEmployeurRelationship(null);
			edc.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			edc.revert();
		}
	}

	private void valider() {

		try {

			if (getCurrentIndividu() == null)
				throw new ValidationException("Veuillez sélectionner un agent !");

			getCurrentConjoint().setCommentaire(CocktailUtilities.getTextFromField(myView.getTfCommentaires()));

			getCurrentConjoint().setIndividuConjointRelationship(getCurrentIndividuConjoint());

			if (getCurrentConjoint().estPersonnel() == false) {
				if (myView.getPopupCivilite().getSelectedIndex() > 0) {
					getCurrentConjoint().setConjCivilite(((EOCivilite)myView.getPopupCivilite().getSelectedItem()).cCivilite());
				}
				getCurrentConjoint().setConjNom(CocktailUtilities.getTextFromField(myView.getTfNomUsuel()));
				getCurrentConjoint().setConjPrenom(CocktailUtilities.getTextFromField(myView.getTfPrenom()));
			}
			else {
				getCurrentConjoint().setConjCivilite(null);
				getCurrentConjoint().setConjNom(null);
				getCurrentConjoint().setConjPrenom(null);				
			}

			// Enregistrement de l'adresse
			if (getCurrentAdresseConjoint() == null) {
				if (myView.getTfCodePostal().getText().length() > 0 && myView.getPopupVilles().getComponentCount() > 0) {
					// Creation de l'adresse
					setCurrentAdresseConjoint(EOAdresse.creer(edc, ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel()));
				}
			}
			if (getCurrentAdresseConjoint() != null) {

				getCurrentAdresseConjoint().setAdrAdresse1(CocktailUtilities.getTextFromField(myView.getTfAdresse()));
				getCurrentAdresseConjoint().setAdrAdresse2(CocktailUtilities.getTextFromField(myView.getTfComplement()));				
				getCurrentAdresseConjoint().setCodePostal(CocktailUtilities.getTextFromField(myView.getTfCodePostal()));

				if (myView.getPopupVilles().getItemCount() > 0)
					getCurrentAdresseConjoint().setVille((String)myView.getPopupVilles().getSelectedItem());

				getCurrentAdresseConjoint().setToPaysRelationship(EOPays.getDefault(edc));

			}

			edc.saveChanges();

			// FIXME - Pb Contrainte deferrable
			getCurrentConjoint().setEmployeurRelationship(employeursCtrl.getSelectedStructure());
			getCurrentConjoint().setAdresseEmployeurRelationship(getCurrentAdresseConjoint());

			edc.saveChanges();

			setSaisieEnabled(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();			
		}
	}

	private void annuler() {
		edc.revert();
		actualiser();
		setSaisieEnabled(false);
	}

	public void nettoyerChamps(NSNotification notification) {

		clearTextFields();
		currentIndividu = null;
		currentConjoint = null;
		currentStructure = null;
		updateUI();
	}

	private void clearTextFields() {

		myView.getCheckPersonnel().setSelected(false);
		myView.getPopupCivilite().setSelectedIndex(0);
		myView.getTfNomUsuel().setText("");
		myView.getTfPrenom().setText("");

		myView.getTfCommentaires().setText("");

		clearAdresseConjoint();

		employeursCtrl.clearDatas();

	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_LOCK_INFOS_PERSONNELLES, null);
		else
			NSNotificationCenter.defaultCenter().postNotification(InfosPersonnellesCtrl.NOTIF_UNLOCK_INFOS_PERSONNELLES, null);
	}


	private boolean peutModifierConjoint() {
		try {
			return saisieEnabled() && getCurrentConjoint() != null ;
		}
		catch (Exception e) {
			return true;
		}

	}

	private void updateUI() {

		boolean peutModifierConjoint = peutModifierConjoint();
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getBtnAjouter().setEnabled(currentIndividu != null && !saisieEnabled() && getCurrentConjoint() == null);
		myView.getBtnModifier().setEnabled(!saisieEnabled() && getCurrentConjoint() != null);
		myView.getBtnSupprimer().setEnabled(!saisieEnabled() && getCurrentConjoint() != null);

		myView.getBtnSelectIndividu().setEnabled(saisieEnabled() && getCurrentConjoint() != null && myView.getCheckPersonnel().isSelected() == true);

		employeursCtrl.setSaisieEnabled(getCurrentConjoint() != null && saisieEnabled());

		myView.getCheckPersonnel().setEnabled(saisieEnabled());
		myView.getTfCommentaires().setEnabled(saisieEnabled());

		myView.getPopupCivilite().setEnabled(saisieEnabled() && peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfNomUsuel(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfPrenom(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfAdresse(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfComplement(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		CocktailUtilities.initTextField(myView.getTfCodePostal(), false, peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		myView.getPopupVilles().setEnabled(saisieEnabled() && peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);
		myView.getBtnDelAdresse().setEnabled(saisieEnabled() && peutModifierConjoint && myView.getCheckPersonnel().isSelected() == false);

	}

}

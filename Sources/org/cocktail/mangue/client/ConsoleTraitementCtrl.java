/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.ConsoleTraitementsView;
import org.cocktail.mangue.client.outils_interface.utilitaires.TimerForAsynchronousRemoteCalls;
import org.cocktail.mangue.client.outils_interface.utilitaires.TimerForAsynchronousRemoteCalls.RemoteActionEvent;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EOAssociation;
import com.webobjects.foundation.NSDictionary;

public class ConsoleTraitementCtrl extends EOInterfaceController implements ActionListener {

	private EOEditingContext edc;
	private TimerForAsynchronousRemoteCalls timer;
	private ConsoleTraitementsView 	myView;
	private Object ctrlParent;

	public ConsoleTraitementCtrl(EOEditingContext edc) {

		super();

		this.edc = edc;
		myView = new ConsoleTraitementsView(new JFrame(), true);

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {myView.setVisible(false);}
		});

		CocktailUtilities.initTextArea(myView.getConsole(), false, false);

		myView.getConsole().setText("");
		myView.getConsole().setBackground(CocktailConstantes.BG_COLOR_GREY);
		myView.getConsole().setForeground(CocktailConstantes.BG_COLOR_CYAN);
		myView.getConsole().setFont(new Font("Helvetica",Font.PLAIN,11));

	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public TimerForAsynchronousRemoteCalls getTimer() {
		return timer;
	}

	public void setTimer(TimerForAsynchronousRemoteCalls timer) {
		this.timer = timer;
	}

	public void open() {
		myView.getConsole().setText("");
		myView.setVisible(true);
	}
	public void toFront() {
		myView.toFront();
	}

	/** Lance un traitement sur le serveur
	 * @param nomMethode nom de la methode a invoquer dans la session
	 * @param classeParametres classe des parametres transmis a la methode
	 * @param parametres parametres transmis a la methode
	 */
	public void lancerTraitement(String nomMethode, Class[] classeParametres,Object[] parametres) {

		myView.getBtnFermer().setEnabled(false);

		try {

			CocktailUtilities.viderTextArea(myView.getConsole());

			initTimer();

			Boolean result = ServerProxy.clientSideRequestLaunchMethode(getEdc(), nomMethode, classeParametres, parametres);

			if (result.booleanValue() == false) {
				throw new Exception("Erreur lors du lancement du traitement");
			}

			myView.toFront();

		} catch (Exception e) {
			e.printStackTrace();
			afficherMessage("Exception pendant le lancement de la préparation\n" + e.getMessage());
			terminer();
		}

		myView.setVisible(true);

	}

	/**
	 * 
	 */
	private void initTimer() {
		setTimer(new TimerForAsynchronousRemoteCalls(8,this));
		getTimer().setInitialDelay(8);
		getTimer().setCoalesce(true);
		getTimer().start();
	}	

	// Notifications
	public  void recupererMessage(String message) {
		afficherMessage(message);
	}
	public void recupererResultat(NSDictionary resultat) {
		// Pas de résultat dans la gestion du Cir
	}

	public void addToMessage(String value) {
		myView.getConsole().setText(myView.getConsole().getText() + "\n" + value);
	}
	public void recupererErreur(String message) {
		afficherMessage("Erreur lors du traitement sur le serveur\n" + message);
		terminer();
	}

	
	public Object getCtrlParent() {
		return ctrlParent;
	}
	public void setCtrlParent(Object ctrl, String title) {
		ctrlParent = ctrl;
		CocktailUtilities.setTextToField(myView.getTfTitre(), title);

	}

	/**
	 * 
	 */
	public void terminer() {

		try {
			myView.getBtnFermer().setEnabled(true);
			CocktailUtilities.lancerOperation( getCtrlParent() , "terminerTraitementServeur", new Class[]{}, new Object[]{});
//			if (isTypeTraitementCirCarriere()) {
//				CocktailUtilities.lancerOperation( getCtrlParent() , "terminerTraitementServeur", new Class[]{}, new Object[]{});
//			}
//			else
//				if (isTypeTraitementCirIdentite()) {
//					//CirIdentiteCtrl.sharedInstance(edc).terminerTraitementServeur();
//				}
//				else 
//					if (isTypeTraitementSupinfo()) {
//						SupInfoCtrl.sharedInstance(edc).terminerTraitementServeur();
//					}
//					else 
//						if (isTypeTraitementPromoEc()) {
//							PromotionsECCtrl.sharedInstance(edc).terminerTraitementServeur();
//						}
//						else 
//							if (isTypeTraitementDif()) {
//								ListeDifsCtrl.sharedInstance(edc).terminerTraitementServeur();
//							}
			controllerDisplayGroup().redisplay();
			// Rafraichir les associations car ce n'est pas fait systematiquement
			for (java.util.Enumeration<EOAssociation> e = controllerDisplayGroup().observingAssociations().objectEnumerator();e.hasMoreElements();) {
				EOAssociation association = e.nextElement();
				association.subjectChanged();
			}
		}
		catch (Exception e) {
		}
		myView.toFront();
	}

	/**
	 * 
	 * @param message
	 */
	private void afficherMessage(String message) {
		if (message != null && message.length() > 0) {
			myView.getConsole().append("\n" + message);
		}
		myView.getConsole().setCaretPosition(myView.getConsole().getDocument().getLength());
	}

	/**
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		if (e instanceof TimerForAsynchronousRemoteCalls.RemoteActionEvent) {
			String commande = e.getActionCommand();
			if (e.getID() == TimerForAsynchronousRemoteCalls.ID_MESSAGE) {
				String message = ((RemoteActionEvent)e).message();
				if (commande.equals(TimerForAsynchronousRemoteCalls.MESSAGE_INFORMATION)) {
					recupererMessage(message);
				} else if (commande.equals(TimerForAsynchronousRemoteCalls.COMMANDE_ERREUR))  {
					recupererErreur(message);
				} else if (commande.equals(TimerForAsynchronousRemoteCalls.FIN_TRAITEMENT)) {
					terminer();
				}
			} else {
				NSDictionary resultat = ((RemoteActionEvent)e).resultat();
				recupererResultat(resultat);
			}
		}
	}
}
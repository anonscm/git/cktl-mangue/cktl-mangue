// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.supinfo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.supinfo.SupInfoSaisieFichierView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.EOSupInfoFichier;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SupInfoSaisieFichierCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SupInfoSaisieFichierCtrl sharedInstance;
	private EOEditingContext edc;
	private SupInfoSaisieFichierView myView;
	private EOSupInfoFichier currentFichier;

	private final int INDEX_REMONTEE_TITULAIRES = 0;
	private final int INDEX_REMONTEE_ANT = 1;
	private final int INDEX_REMONTEE_CRCT = 2;
	
	public SupInfoSaisieFichierCtrl(EOEditingContext edc) {

		this.edc = edc;
		myView = new SupInfoSaisieFichierView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		myView.getTfDateObservation().addFocusListener(new FocusListenerDateTextField(myView.getTfDateObservation()));
		myView.getTfDateObservation().addActionListener(new ActionListenerDateTextField(myView.getTfDateObservation()));
	}

	public static SupInfoSaisieFichierCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SupInfoSaisieFichierCtrl(editingContext);
		return sharedInstance;
	}

	public EOSupInfoFichier getCurrentFichier() {
		return currentFichier;
	}

	public void setCurrentFichier(EOSupInfoFichier currentFichier) {
		this.currentFichier = currentFichier;
	}

	private void clearTextFields()	{
		myView.getTfDateObservation().setText("");
	}
	
	/**
	 * 
	 * @param fichier
	 * @return
	 */
	public boolean modifier(EOSupInfoFichier fichier) {
		
		clearTextFields();
		setCurrentFichier(fichier);

		CocktailUtilities.setDateToField(myView.getTfDateObservation(), getCurrentFichier().supfDateObs());

		if ( getCurrentFichier().estRemonteeSupinfo() )
			myView.getPopupTypeRemontee().setSelectedIndex(INDEX_REMONTEE_TITULAIRES); 	
		else
			if ( getCurrentFichier().estRemonteeCrct() )
				myView.getPopupTypeRemontee().setSelectedIndex(INDEX_REMONTEE_CRCT); 	
			else
				if ( getCurrentFichier().estRemonteeAnt() )
					myView.getPopupTypeRemontee().setSelectedIndex(INDEX_REMONTEE_ANT); 	
						
		myView.setVisible(true);
		return getCurrentFichier() != null;
	}

	/**
	 * Traitements a effectuer AVANT la validation en base
	 * @throws ValidationException
	 */
	private void traitementsAvantValidation() throws ValidationException {

	}
	/**
	 * Traitements a effectuer APRES la validation en base
	 * @throws ValidationException
	 */
	private void traitementsApresValidation() throws ValidationException {

	}
	private void valider()  {

		try {
			getCurrentFichier().setSupfDateObs(CocktailUtilities.getDateFromField(myView.getTfDateObservation()));

			if (getCurrentFichier().supfDateObs() != null)
				getCurrentFichier().setSupfAnnee(DateCtrl.getYear(getCurrentFichier().supfDateObs()));
				
			switch (myView.getPopupTypeRemontee().getSelectedIndex()) {
			case INDEX_REMONTEE_TITULAIRES : getCurrentFichier().setSupfObjet(EOSupInfoFichier.OBJET_SUP_INFO);break;
			case INDEX_REMONTEE_ANT : getCurrentFichier().setSupfObjet(EOSupInfoFichier.OBJET_SUP_INFO_ANT);break;
			case INDEX_REMONTEE_CRCT : getCurrentFichier().setSupfObjet(EOSupInfoFichier.OBJET_CRCT);
			}
						
			traitementsAvantValidation();
			edc.saveChanges();
			traitementsApresValidation();
			
			myView.setVisible(false);
		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler()	{
		edc.revert();
		setCurrentFichier(null);
		myView.setVisible(false);
	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFicheIdentiteCtrl.java

package org.cocktail.mangue.client.supinfo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.supinfo.SupInfoDetailView;
import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.EOSupInfoData;

import com.webobjects.eocontrol.EOEditingContext;

public class SupInfoDetailCtrl {
	
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SupInfoDetailCtrl sharedInstance;
	private EOEditingContext edc;
	private SupInfoDetailView myView;

	private EOSupInfoData currentSupInfo;

	public SupInfoDetailCtrl(EOEditingContext edc) {
		
		setEdc(edc);
		myView = new SupInfoDetailView(new JFrame(), true);

		myView.getBtnFermer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){fermer();}}
		);

		myView.getTfObservations().setEnabled(false);
		
		myView.getBtnRecalculer().setVisible(false);
		myView.getPopupEtats().addActionListener(new PopupEtatListener());

	}

	public static SupInfoDetailCtrl sharedInstance(EOEditingContext editingContext)
	{
		if(sharedInstance == null)
			sharedInstance = new SupInfoDetailCtrl(editingContext);
		return sharedInstance;
	}
	
	

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOSupInfoData getCurrentSupInfo() {
		return currentSupInfo;
	}

	public void setCurrentSupInfo(EOSupInfoData currentSupInfo) {
		this.currentSupInfo = currentSupInfo;
	}

	private void clearDatas() {
				
		CocktailUtilities.viderTextField(myView.getTfDiplome());
		
		myView.getTfNom().setText("");
		myView.getTfPrenom().setText("");
		myView.getTfNumen().setText("");
		myView.getTfDateNaissance().setText("");
		myView.getTfCodePosition().setText("");
		myView.getTfDebutPosition().setText("");
		myView.getTfFinPosition().setText("");
		myView.getTfQuotite().setText("");
		myView.getTfLieuPosition().setText("");
		myView.getTfTitularisation().setText("");
		myView.getTfLibelleGrade().setText("");
		myView.getTfDateNaissance().setText("");
		myView.getTfDateGrade().setText("");
		myView.getTfEchelon().setText("");
		myView.getTfDateEchelon().setText("");
		myView.getTfChevron().setText("");
		myView.getTfObservations().setText("");

	}

	/**
	 * 
	 * @param supInfo
	 */
	public void open(EOSupInfoData supInfo) {
		
		clearDatas();
		
		setCurrentSupInfo(supInfo);
		
		myView.setTitle("DETAIL SUP INFO - " + getCurrentSupInfo().toIndividu().identitePrenomFirst());

		updateData();
		
		myView.setVisible(true);

	}

	private class PopupEtatListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {			
			
			try {
				
				switch (myView.getPopupEtats().getSelectedIndex()) {
				
				case 0:getCurrentSupInfo().setEficEtat(null);break;
				case 1:getCurrentSupInfo().setEficEtat(EOSupInfoData.STATUT_PROVISOIRE);break;
				case 2:getCurrentSupInfo().setEficEtat(EOSupInfoData.STATUT_TRANSMIS_MINISTERE);break;
				case 3:getCurrentSupInfo().setEficEtat(EOSupInfoData.STATUT_PROMU);break;
				case 4:getCurrentSupInfo().setEficEtat(EOSupInfoData.STATUT_NEGATIF);break;
				
				}
				
				getEdc().saveChanges();
				
			}
			catch (Exception ex) {				
			}
		}
	}

	
	private void updateData() {

		if(getCurrentSupInfo().eficEtat() != null) {
			if (getCurrentSupInfo().eficEtat().equals(EOSupInfoData.STATUT_PROVISOIRE))
				myView.getPopupEtats().setSelectedIndex(1);
			if (getCurrentSupInfo().eficEtat().equals(EOSupInfoData.STATUT_TRANSMIS_MINISTERE))
				myView.getPopupEtats().setSelectedIndex(2);
			if (getCurrentSupInfo().eficEtat().equals(EOSupInfoData.STATUT_PROMU))
				myView.getPopupEtats().setSelectedIndex(3);
			if (getCurrentSupInfo().eficEtat().equals(EOSupInfoData.STATUT_NEGATIF))
				myView.getPopupEtats().setSelectedIndex(4);
		}
		else
			myView.getPopupEtats().setSelectedIndex(0);
		
		CocktailUtilities.setTextToArea(myView.getTfObservations(), getCurrentSupInfo().eficObservations());
		CocktailUtilities.setTextToField(myView.getTfNom(), getCurrentSupInfo().eficNomUsuel());
		CocktailUtilities.setTextToField(myView.getTfPrenom(), getCurrentSupInfo().eficPrenom());
		CocktailUtilities.setTextToField(myView.getTfNomFamille(), getCurrentSupInfo().eficNomPatronymique());
		CocktailUtilities.setTextToField(myView.getTfNumen(), getCurrentSupInfo().eficNumen());
		CocktailUtilities.setDateToField(myView.getTfDateNaissance(), getCurrentSupInfo().eficDNaissance());
		CocktailUtilities.setDateToField(myView.getTfArrivee(), getCurrentSupInfo().eficDEffetArrivee());
		CocktailUtilities.setTextToField(myView.getTfCodePosition(), getCurrentSupInfo().eficCPosition());
		CocktailUtilities.setDateToField(myView.getTfDebutPosition(), getCurrentSupInfo().eficDDebPostion());
		CocktailUtilities.setDateToField(myView.getTfFinPosition(), getCurrentSupInfo().eficDFinPostion());
		CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentSupInfo().eficQuotite());
		CocktailUtilities.setTextToField(myView.getTfLieuPosition(), getCurrentSupInfo().eficLieuPosition());
		CocktailUtilities.setDateToField(myView.getTfTitularisation(), getCurrentSupInfo().eficDTitularisation());
		
		if (getCurrentSupInfo().eficDiplCode() != null) {
			
			EODiplomes diplome = EODiplomes.findForCode(edc, getCurrentSupInfo().eficDiplCode());
			if (diplome != null) {
				CocktailUtilities.setTextToField(myView.getTfDiplome(), diplome.libelleLong() + "(" + getCurrentSupInfo().eficDiplAnnee() + ")");
			}
			
		}
		
		if (getCurrentSupInfo().eficCTypeAccesCorps() != null) {
			EOTypeAcces acces = EOTypeAcces.findForCode(getEdc(), getCurrentSupInfo().eficCTypeAccesCorps());
			CocktailUtilities.setTextToField(myView.getTfAccesCorps(), acces.code() + " - " + acces.libelleLong());
		}
		
		if (getCurrentSupInfo().eficCTypeAccesGrade() != null) {
			EOTypeAcces acces = EOTypeAcces.findForCode(getEdc(), getCurrentSupInfo().eficCTypeAccesGrade());
			CocktailUtilities.setTextToField(myView.getTfAccesGrade(), acces.code() + " - " + acces.libelleLong());
		}

		if(getCurrentSupInfo().toGrade() != null)						
			CocktailUtilities.setTextToField(myView.getTfLibelleGrade(), getCurrentSupInfo().toGrade().llGrade());
		
		CocktailUtilities.setDateToField(myView.getTfDateGrade(), getCurrentSupInfo().eficDGrade());
		CocktailUtilities.setDateToField(myView.getTfDateNominationCorps(), getCurrentSupInfo().eficDNominationCorps());
		CocktailUtilities.setTextToField(myView.getTfEchelon(), getCurrentSupInfo().eficCEchelon());
		CocktailUtilities.setDateToField(myView.getTfDateEchelon(), getCurrentSupInfo().eficDEchelon());
		CocktailUtilities.setTextToField(myView.getTfChevron(), getCurrentSupInfo().eficCChevron());

		CocktailUtilities.setTextToField(myView.getTfSpecialisation(), getCurrentSupInfo().eficCSectionCnu());
		
	}
	
	private void fermer() {

		myView.setVisible(false);
		
	}



}

package org.cocktail.mangue.client.supinfo;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ConsoleTraitementCtrl;
import org.cocktail.mangue.client.cir.CirCtrl;
import org.cocktail.mangue.client.gui.supinfo.SupInfoView;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOSupInfoData;
import org.cocktail.mangue.modele.mangue.EOSupInfoFichier;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class SupInfoCtrl {

	private static final String CODE_PLAFOND_PROPRE = "2";
	private static final String CODE_PLAFOND_ETAT = "1";
	private static SupInfoCtrl 	sharedInstance;
	private static Boolean 		MODE_MODAL = Boolean.FALSE;
	private EOEditingContext 	edc;
	private SupInfoView 		myView;
	private EODisplayGroup 		eodData, eodFichier;
	private ListenerData 		listenerData= new ListenerData();
	private ListenerFichier 	listenerFichier= new ListenerFichier();
	private SupinfoRenderer		monRendererColor = new SupinfoRenderer();
	private boolean 			traitementServeurEnCours, preparationEnCours = false;

	private String 				nomFichierExport;
	private EOSupInfoData	 	currentData;
	private EOSupInfoFichier	currentFichier;
	private EOAgentPersonnel 	currentUtilisateur;

	private ConsoleTraitementCtrl 		myConsole;

	public SupInfoCtrl(EOEditingContext edc) {

		this.edc = edc;
		eodData = new EODisplayGroup();
		eodFichier = new EODisplayGroup();

		myConsole = new ConsoleTraitementCtrl(edc);
		myConsole.setCtrlParent(this, "Traitement des données SUPINFO ...");

		myView = new SupInfoView(new JFrame(), MODE_MODAL.booleanValue(), eodFichier, eodData, monRendererColor);

		myView.getBtnRechercher().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {actualiser();}}
				);
		myView.getBtnCalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){preparer();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){supprimer();}}
				);
		myView.getBtnDetail().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){afficherDetail();}}
				);
		myView.getBtnFichier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){exporter();}}
				);

		myView.getBtnAjouterFichier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){ajouterFichier();}});
		myView.getBtnModifierFichier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){modifierFichier();}});
		myView.getBtnSupprimerFichier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){supprimerFichier();}});

		myView.getValidite().addActionListener(new PopupFiltreListener());
		myView.getPopupCrct().addActionListener(new PopupFiltreListener());
		myView.getTfFiltreNom().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreNumen().getDocument().addDocumentListener(new ADocumentListener());
		myView.getMyEOTable().addListener(listenerData);
		myView.getMyEOTableFichier().addListener(listenerFichier);

		myView.getLblMessage().setText("");
		currentUtilisateur = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();

		myView.getBtnRecalculer().setVisible(false);

	}



	public EOSupInfoData getCurrentData() {
		return currentData;
	}



	public void setCurrentData(EOSupInfoData currentData) {
		this.currentData = currentData;
	}



	public EOSupInfoFichier getCurrentFichier() {
		return currentFichier;
	}



	public void setCurrentFichier(EOSupInfoFichier currentFichier) {
		this.currentFichier = currentFichier;
	}


	public boolean isTraitementServeurEnCours() {
		return traitementServeurEnCours;
	}



	public void setTraitementServeurEnCours(boolean traitementServeurEnCours) {
		this.traitementServeurEnCours = traitementServeurEnCours;
	}



	public boolean isPreparationEnCours() {
		return preparationEnCours;
	}



	public void setPreparationEnCours(boolean preparationEnCours) {
		this.preparationEnCours = preparationEnCours;
	}



	/**
	 * 
	 */
	public void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer les données SUPINFO des agents sélectionnés ?", "Oui", "Non"))
			return;

		CRICursor.setWaitCursor(myView);
		try {

			for (EOSupInfoData myData : new NSArray<EOSupInfoData>(eodData.selectedObjects())) {
				edc.deleteObject(myData);
			}

			edc.saveChanges();
			eodData.deleteSelection();
			myView.getMyEOTable().updateData();
			myView.toFront();

		}
		catch (Exception e){
			e.printStackTrace();
		}
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @param nomMethode
	 * @param individus
	 * @param estPreparation
	 */
	private void lancerTraitementServeur(String nomMethode) {

		setTraitementServeurEnCours(true);

		NSMutableDictionary dico = new NSMutableDictionary();
		dico.setObjectForKey(getCurrentFichier(), EOSupInfoFichier.ENTITY_TABLE_NAME);
		Class classeParametres[] = {NSDictionary.class};
		Object parametres[] = {dico.immutableClone()};

		myConsole.lancerTraitement(nomMethode, classeParametres, parametres);

		myConsole.toFront();

	}

	/**
	 * 
	 */
	public void terminerTraitementServeur() {
		try {
			setTraitementServeurEnCours(false);	
			setPreparationEnCours(false);

			myConsole.addToMessage("\n >> TRAITEMENT TERMINE !");
			actualiser();

			myView.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void preparer() {

		try {
			lancerTraitementServeur("clientSideRequestPreparerSupInfo");
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.toFront();

	}

	/**
	 * 
	 */
	public void exporter() {

		CRICursor.setWaitCursor(myView);
		if (getCurrentFichier().estRemonteeSupinfo())
			exporterFichierTitulaires();
		else
			if (getCurrentFichier().estRemonteeCrct())
				exporterFichierCrct();
			else
				if (getCurrentFichier().estRemonteeAnt())
					exporterFichierAnt();

		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.toFront();

	}

	public String pathFichierSupInfo() {

		String pathDir = (((ApplicationClient)EOApplication.sharedApplication()).directoryExports()) +
				System.getProperty("file.separator") + 
				"SUPINFO" + 
				System.getProperty("file.separator");
		UtilitairesFichier.verifierPathEtCreer(pathDir);

		return pathDir;

	}
	public String pathFichierCrct()
	{
		String pathDir = ((ApplicationClient)EOApplication.sharedApplication()).directoryExports() + System.getProperty("file.separator") + "CRCT" + System.getProperty("file.separator");
		UtilitairesFichier.verifierPathEtCreer(pathDir);
		return pathDir;
	}
	public String pathFichierAnt()
	{
		String pathDir = ((ApplicationClient)EOApplication.sharedApplication()).directoryExports() + System.getProperty("file.separator") + "SUPINFO_ANT" + System.getProperty("file.separator");
		UtilitairesFichier.verifierPathEtCreer(pathDir);
		return pathDir;
	}


	/**
	 * 
	 */
	private void exporterFichierTitulaires() {

		JFileChooser saveDialog= new JFileChooser(pathFichierSupInfo());
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		String rne = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE);

		nomFichierExport = rne + "_";

		nomFichierExport +=	DateCtrl.dateToString(getCurrentFichier().supfDateObs(), "%Y%m%d");

		saveDialog.setSelectedFile(new File((new StringBuilder(String.valueOf(nomFichierExport))).append(".csv").toString()));

		if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

			File file = saveDialog.getSelectedFile();
			CRICursor.setWaitCursor(myView);
			String texte = "";
			NSArray<EOSupInfoData> sortedDatas = EOSortOrdering.sortedArrayUsingKeyOrderArray(eodData.displayedObjects(), new NSArray(new EOSortOrdering("eficNomPatronymique", EOSortOrdering.CompareAscending)));

			for (EOSupInfoData myData : sortedDatas) {
				texte += texteExportPourRecordSupinfoTitulaires(myData) + "\n";
			}

			UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

			EODialogs.runInformationDialog("Fichier SUP INFO", "Le fichier Sup INFO a été généré dans le répertoire " + pathFichierSupInfo() +
					".\nMerci de conserver l'original pour envoi. Si vous désirez l'ouvrir, il serait préférable de le dupliquer.");

			CRICursor.setDefaultCursor(myView);
			myView.toFront();
		}
		return;
	}

	/**
	 * 
	 * @param record
	 * @param estExportElectra
	 * @return
	 */
	private String texteExportPourRecordSupinfoTitulaires(EOSupInfoData record)    {

		String texte = "";

		// 1 - UAI
		texte += CocktailExports.ajouterChamp(record.eficUaiAffectation());

		// 2 - No etablissement de rattachement
		texte += CocktailExports.ajouterChamp(record.eficUai());

		// 3 - Indentifiant national de l'unite de recherche d'exercice (Ex : 199213317P)
		texte += CocktailExports.ajouterChampVide();

		// 4 - NUMEN
		texte += CocktailExports.ajouterChamp(record.eficNumen());

		// 5 - CIVILITE
		texte += CocktailExports.ajouterChamp(record.eficCivilite());

		// 6 - NOM DE FAMILLE
		texte += CocktailExports.ajouterChamp(record.eficNomPatronymique());

		// 7 - NOM D'USAGE
		texte += CocktailExports.ajouterChamp(record.eficNomUsuel());

		// 8 - PRENOM
		texte += CocktailExports.ajouterChamp(record.eficPrenom());

		// 9 - DATE NAISSANCE
		texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.eficDNaissance()));

		// 10 - NATIONALITE
		texte += CocktailExports.ajouterChamp(record.eficCPaysNationalite());

		// 11 - POSITION ADMIN
		texte += CocktailExports.ajouterChamp(record.eficCPosition());

		// 12 - MOTIF CESSATION ACTIVITE
		texte += CocktailExports.ajouterChamp(record.eficCMotifDepart());

		// 13 - DATE CESSATION
		texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.eficDEffetDepart()));

		// 14 - DATE DEBUT POSITION
		texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.eficDDebPostion()));

		// 15 - DATE FIN POSITION
		if(record.eficDFinPostion() != null)
			texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.eficDFinPostion()));
		else
			texte += CocktailExports.ajouterChamp(null);

		// 16 - QUOTITE
		if(record.eficQuotite() != null)
			texte += CocktailExports.ajouterChamp((record.eficQuotite().setScale(0)).toString());
		else
			texte += CocktailExports.ajouterChamp(null);

		// 17 - MODALITE TEMPS DE TRAVAIL
		texte += CocktailExports.ajouterChamp(record.eficCMotifTempsPartiel());

		// 18 - LIEU POSITION
		texte += CocktailExports.ajouterChamp(record.eficLieuPosition());

		// 19 - SECTION CNU
		texte += CocktailExports.ajouterChamp(record.eficCSectionCnu());

		// 20 - SPECIALITE
		texte += CocktailExports.ajouterChamp(record.eficCSectionCnuEc());

		// 21 - CORPS
		texte += CocktailExports.ajouterChamp(record.eficCCorps());

		// 22 - STATUT
		texte += CocktailExports.ajouterChamp(record.eficStatut());

		// 23 - DATE NOMINATION CORPS
		texte += CocktailExports.ajouterChampDate(record.eficDNominationCorps());

		// 24 - DATE TITULARISATION
		texte += CocktailExports.ajouterChampDate(record.eficDTitularisation());

		// 25 - GRADE
		texte += CocktailExports.ajouterChamp(record.eficCGrade() );

		// 26 - DATE ENTREE GRADE
		texte += CocktailExports.ajouterChampDate(record.eficDGrade());

		// 27 - ECHELON
		texte += CocktailExports.ajouterChamp(record.eficCEchelon() );

		// 28 - CHEVRON
		texte += CocktailExports.ajouterChamp(record.eficCChevron() );

		// 29 - DATE ENTREE ECHELON
		texte += CocktailExports.ajouterChampDate(record.eficDEchelon());

		// 30 - ANCIENNETE
		texte += CocktailExports.ajouterChamp(record.eficAncConservee() );

		// 31 - DATE ENTREE ETAB
		texte += CocktailExports.ajouterChampDate(record.eficDEffetArrivee());

		// 32 - DATE ENTREE CATEGORIE
		texte += CocktailExports.ajouterChampDate(record.eficDEntreeCategorie());

		// 33 - MODE ACCES CORPS
		texte += CocktailExports.ajouterChamp(record.eficCTypeAccesCorps() );

		// 34 - MODE ACCES GRADE
		texte += CocktailExports.ajouterChamp(record.eficCTypeAccesGrade() );

		// 35 - DIPLOME LE PLUS ELEVE
		texte += CocktailExports.ajouterChamp(record.eficDiplCode() );

		// 36 - ANNEE OBTENTION DIPLOME LE PLUS ELEVE
		texte += CocktailExports.ajouterChampNumber(record.eficDiplAnnee() );

		// 37 - CORE RNE OBTENTION (8)
		texte += CocktailExports.ajouterChamp(record.eficDiplCodeUAI() );

		// 38 - LIBELLE ETABLISSEMENT OBTENTION (50)
		texte += CocktailExports.ajouterChamp(record.eficDiplLibelleUAI() );

		// 39 - CODE GRADE PRECEDENT
		texte += CocktailExports.ajouterChamp(record.eficCGradePrev() );

		// 40 - CODE TYPE CONGE
		texte += CocktailExports.ajouterChamp(record.eficCTypeAbsence() );

		// 41 - DATE DEBUT CONGE
		texte += CocktailExports.ajouterChampDate(record.eficDDebAbsence());

		// 42 - DATE FIN CONGE
		texte += CocktailExports.ajouterChampDate(record.eficDFinAbsence());

		// 43 - BOE_RQTH
		texte += CocktailExports.ajouterChamp("N");

		// 44 - Type de BOE RQTH
		texte += CocktailExports.ajouterChampVide();

		// 45 - DATE OBSERVATION
		texte += CocktailExports.ajouterChampDate(getCurrentFichier().supfDateObs());

		return texte;
	}

	/**
	 * 
	 * @param record
	 * @param estExportElectra
	 * @return
	 */
	private String texteExportPourRecordSupinfoAnt(EOSupInfoData record)    {

		String texte = "";
		EOTypeContratTravail typeContrat = null;
		if (record.eficStatut() != null) {
			typeContrat = (EOTypeContratTravail) NomenclatureFinder.findForCode(edc, EOTypeContratTravail.ENTITY_NAME, record.eficStatut());
		}

		// 1 - UAI
		texte += CocktailExports.ajouterChamp(record.eficUaiAffectation());

		// 2 - No etablissement de rattachement
		texte += CocktailExports.ajouterChamp(record.eficUai());

		// 3 - Indentifiant national de l'unite de recherche d'exercice (Ex : 199213317P)
		texte += CocktailExports.ajouterChampVide();

		// 4 - NUMEN
		texte += CocktailExports.ajouterChamp(record.eficNumen());

		// 5 - No ordre interne au SI de l'etablissement
		texte += CocktailExports.ajouterChampNumber(record.toIndividu().noIndividu());

		// 6 - CIVILITE
		texte += CocktailExports.ajouterChamp(record.eficCivilite());

		// 7 - NOM DE FAMILLE
		texte += CocktailExports.ajouterChamp(record.eficNomPatronymique());

		// 8 - NOM D'USAGE
		texte += CocktailExports.ajouterChamp(record.eficNomUsuel());

		// 9 - PRENOM
		texte += CocktailExports.ajouterChamp(record.eficPrenom());

		// 10 - DATE NAISSANCE
		texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.eficDNaissance()));

		// 11 - NATIONALITE
		texte += CocktailExports.ajouterChamp(record.eficCPaysNationalite());

		// 12 - MODALITE TEMPS DE TRAVAIL
		texte += CocktailExports.ajouterChamp(record.eficCMotifTempsPartiel());

		// 13 - QUOTITE
		if(record.eficQuotite() != null)
			texte += CocktailExports.ajouterChamp((record.eficQuotite().setScale(0)).toString());
		else
			texte += CocktailExports.ajouterChampVide();

		// 14 - POSITION ADMIN
		texte += CocktailExports.ajouterChamp(record.eficCPosition());

		// 15 - MOTIF CESSATION ACTIVITE
		texte += CocktailExports.ajouterChamp(record.eficCMotifDepart());

		// 16 - DATE CESSATION
		texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.eficDEffetDepart()));

		// 17 - STATUT
		if (typeContrat != null)
			texte += CocktailExports.ajouterChamp(typeContrat.codeSupinfo());
		else
			texte += CocktailExports.ajouterChampVide();

		// 18 - Mission d'enseignement
		if (typeContrat.estDoctorant())
			texte += CocktailExports.ajouterChamp("O");
		else
			texte += CocktailExports.ajouterChamp("N");

		// 19 - Fonction exercee
		texte += CocktailExports.ajouterChamp(record.eficStatut());

		// 20 - GRADE
		texte += CocktailExports.ajouterChamp(record.eficCGrade() );

		// 21 - GRADE TG (Lien_grade_men_tg)
		texte += CocktailExports.ajouterChamp(record.eficCGrade());

		// 22 - Post doctorant
		if (typeContrat.estPostDoctorant())
			texte += CocktailExports.ajouterChamp("O");
		else
			texte += CocktailExports.ajouterChamp("N");

		// 23 - SECTION CNU
		texte += CocktailExports.ajouterChamp(record.eficCSectionCnu());

		// 24 - SPECIALITE
		texte += CocktailExports.ajouterChamp(record.eficCSectionCnuEc());

		// 25 - Type de contrat
		if (typeContrat.estCdi())
			texte += CocktailExports.ajouterChampNumber(new Integer(2));				
		else
			texte += CocktailExports.ajouterChampNumber(new Integer(1));				

		// 26 - Type de plafond
		if (record.toOrigineFinancement() != null) {
			if (record.toOrigineFinancement().estBudgetEtat())
				texte += CocktailExports.ajouterChamp(CODE_PLAFOND_ETAT);
			else
				if (record.toOrigineFinancement().estBudgetEtat())
					texte += CocktailExports.ajouterChamp(CODE_PLAFOND_PROPRE);
				else
					texte += CocktailExports.ajouterChampVide();
		}
		else
			texte += CocktailExports.ajouterChampVide();

		// 27 - Indice BRUT de remuneration
		texte += CocktailExports.ajouterChamp(record.eficIndiceBrut());

		// 28 - Taux horaire
		texte += CocktailExports.ajouterChampDecimal(record.eficRemunHoraire());

		// 29 - Forfait
		texte += CocktailExports.ajouterChampDecimal(record.eficForfait());

		// 30 - Date debut contrat
		texte += CocktailExports.ajouterChampDate(record.eficDDebPostion());

		// 31 - Date fin contrat
		texte += CocktailExports.ajouterChampDate(record.eficDFinPostion());

		// 32 - DATE ENTREE ETAB
		texte += CocktailExports.ajouterChampDate(record.eficDEffetArrivee());

		// 33 - CDIs Sauvadet
		if (typeContrat.estCdiSauvadet())
			texte += CocktailExports.ajouterChamp("O");				
		else
			texte += CocktailExports.ajouterChamp("N");				

		// 34 - DIPLOME LE PLUS ELEVE
		texte += CocktailExports.ajouterChamp(record.eficDiplCode() );

		// 35 - ANNEE OBTENTION DIPLOME LE PLUS ELEVE
		texte += CocktailExports.ajouterChampNumber(record.eficDiplAnnee() );

		// 36 - CORE RNE OBTENTION (8)
		texte += CocktailExports.ajouterChamp(record.eficDiplCodeUAI() );

		// 37 - LIBELLE ETABLISSEMENT OBTENTION (50)
		texte += CocktailExports.ajouterChamp(record.eficDiplLibelleUAI() );

		// 38 - BOE_RQTH
		texte += CocktailExports.ajouterChamp("N");

		// 39 - Type de BOE RQTH
		texte += CocktailExports.ajouterChampVide();

		// 40 - DATE OBSERVATION
		texte += CocktailExports.ajouterChampDate(getCurrentFichier().supfDateObs());

		return texte;
	}



	public static SupInfoCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SupInfoCtrl(editingContext);
		return sharedInstance;
	}

	public void open()	{

		// Controle des droits
		if (currentUtilisateur.peutUtiliserOutils() == false) {
			EODialogs.runInformationDialog("ERREUR", "Vous n'avez pas les droits nécessaires pour la gestion les remontées SUPINFO !");
			return;
		}

		actualiser();
		updateUI();

		if (!myView.isVisible()) {
			myView.setVisible(true);
		}
		else {
			myView.setVisible(true);
			myView.toFront();
		}
	}

	private void clean()
	{
		eodData.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
		myView.getLblMessage().setText("");
	}

	private void afficherDetail() {

		SupInfoDetailCtrl.sharedInstance(edc).open(currentData);

	}

	private void actualiser() {

		clean();

		NSMutableArray fichiers = new NSMutableArray();

		fichiers.addObjectsFromArray(EOSupInfoFichier.fetchForType(edc, EOSupInfoFichier.OBJET_SUP_INFO));
		fichiers.addObjectsFromArray(EOSupInfoFichier.fetchForType(edc, EOSupInfoFichier.OBJET_SUP_INFO_ANT));
		fichiers.addObjectsFromArray(EOSupInfoFichier.fetchForType(edc, EOSupInfoFichier.OBJET_CRCT));

		EOSortOrdering.sortArrayUsingKeyOrderArray(fichiers, EOSupInfoFichier.SORT_ARRAY_DATE_DESC);

		eodFichier.setObjectArray(fichiers);
		myView.getMyEOTableFichier().updateData();

	}

	private EOQualifier filterQualifier() {
		NSMutableArray mesQualifiers = new NSMutableArray();
		if(myView.getValidite().getSelectedIndex() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOSupInfoData.EFIC_TEM_VALIDE_KEY + " = %@", new NSArray(myView.getValidite().getSelectedIndex() != 1 ? "N" : "O")));
		if(getCurrentFichier() != null && getCurrentFichier().estRemonteeCrct() && myView.getPopupCrct().getSelectedIndex() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOSupInfoData.EFIC_TEM_CRCT_KEY + " = %@", new NSArray(myView.getPopupCrct().getSelectedIndex() == 1 ? "O" : "N")));
		if(myView.getTfFiltreNom().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOSupInfoData.EFIC_NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray((new StringBuilder("*")).append(myView.getTfFiltreNom().getText()).append("*").toString())));
		if(myView.getTfFiltreNumen().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOSupInfoData.EFIC_NUMEN_KEY + " caseInsensitiveLike %@", new NSArray((new StringBuilder("*")).append(myView.getTfFiltreNumen().getText()).append("*").toString())));

		return new EOAndQualifier(mesQualifiers);
	}

	private void filter() {

		eodData.setQualifier(filterQualifier());
		eodData.updateDisplayedObjects();
		myView.getMyEOTable().updateData();
		myView.getLblMessage().setText((new StringBuilder(String.valueOf(eodData.displayedObjects().count()))).append(" Agents").toString());
		updateUI();

	}

	private void updateUI() {
		myView.getBtnAjouter().setEnabled(false);
		myView.getBtnModifier().setEnabled(false);//currentSupInfo != null);
		myView.getBtnSupprimer().setEnabled(getCurrentData() != null);
		myView.getBtnCalculer().setEnabled(getCurrentFichier() != null);
		myView.getBtnRecalculer().setEnabled(getCurrentFichier() != null && getCurrentData() != null && !getCurrentFichier().estRemonteeCrct());
		myView.getBtnFichier().setEnabled(getCurrentFichier() != null && eodData.displayedObjects().count() > 0);
		myView.getBtnDetail().setEnabled(getCurrentFichier() != null && getCurrentData() != null);

		myView.getPopupCrct().setEnabled(currentFichier != null && currentFichier.estRemonteeCrct());

	}


	/**
	 * 
	 */
	private void ajouterFichier() {

		EOSupInfoFichier newFichier = EOSupInfoFichier.creer(edc, EOSupInfoFichier.OBJET_SUP_INFO);
		if (SupInfoSaisieFichierCtrl.sharedInstance(edc).modifier(newFichier)) {
			actualiser();
			myView.getMyEOTableFichier().forceNewSelectionOfObjects(new NSArray(newFichier));
		}

	}

	private void modifierFichier()	{
		SupInfoSaisieFichierCtrl.sharedInstance(edc).modifier(getCurrentFichier());
		myView.getMyEOTableFichier().updateUI();
	}

	/**
	 * 
	 */
	private void supprimerFichier()	{

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer le fichier, toutes les données associées seront supprimées ?", "Oui", "Non"))
			return;

		CirCtrl.sharedInstance(edc).toFront();
		CRICursor.setWaitCursor(myView);

		try {

			NSArray<EOSupInfoData> datas = EOSupInfoData.findForFichier(edc, getCurrentFichier());
			for (EOSupInfoData myData : datas) {
				edc.deleteObject(myData);
			}

			edc.deleteObject(getCurrentFichier());

			edc.saveChanges();

			EODialogs.runInformationDialog("","La suppression du fichier a bien été effectuée !");

			actualiser();
		}
		catch (Exception e) {
			edc.revert();
			e.printStackTrace();
		}

		CRICursor.setDefaultCursor(myView);

	}





	private class ADocumentListener
	implements DocumentListener
	{

		public void changedUpdate(DocumentEvent e)
		{
			filter();
		}

		public void insertUpdate(DocumentEvent e)
		{
			filter();
		}

		public void removeUpdate(DocumentEvent e)
		{
			filter();
		}

	}

	private class ListenerData
	implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{

		public void onDbClick()
		{
			afficherDetail();
		}

		public void onSelectionChanged()
		{
			setCurrentData((EOSupInfoData)eodData.selectedObject());
			updateUI();
		}

	}
	private class ListenerFichier implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{

		public void onDbClick()	{
		}

		public void onSelectionChanged()	{

			setCurrentFichier((EOSupInfoFichier)eodFichier.selectedObject());
			eodData.setObjectArray(new NSArray());

			if (getCurrentFichier() != null) {
				CRICursor.setWaitCursor(myView);
				eodData.setObjectArray(EOSupInfoData.findForFichier(edc, getCurrentFichier()));
				CRICursor.setDefaultCursor(myView);
			}

			filter();

		}

	}

	private class PopupFiltreListener
	implements ActionListener{
		public void actionPerformed(ActionEvent anAction)
		{
			filter();
		}

	}

	/**
	 * 
	 */
	private void exporterFichierCrct() 	{

		JFileChooser saveDialog = new JFileChooser(pathFichierCrct());
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(1);
		String rne = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE);
		nomFichierExport = rne;
		saveDialog.setSelectedFile(new File((new StringBuilder(String.valueOf(nomFichierExport))).append(CocktailConstantes.EXTENSION_CSV).toString()));

		if(saveDialog.showSaveDialog(myView) == 0) {

			File file = saveDialog.getSelectedFile();
			CRICursor.setWaitCursor(myView);
			String texte = "";
			NSArray<EOSupInfoData> sortedDatas = EOSortOrdering.sortedArrayUsingKeyOrderArray(eodData.displayedObjects(), new NSArray(new EOSortOrdering("eficNomPatronymique", EOSortOrdering.CompareAscending)));
			for(EOSupInfoData myData : sortedDatas) {
				texte =  texte + texteExportPourRecordCRCT(myData) + "\n";
			}

			UtilitairesFichier.afficherFichier(texte, file.getParent(), nomFichierExport, "csv", false);
			CRICursor.setDefaultCursor(myView);
		}
	}

	/**
	 * 
	 */
	private void exporterFichierAnt() 	{

		JFileChooser saveDialog = new JFileChooser(pathFichierAnt());
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(1);
		String rne = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE);
		nomFichierExport = rne;
		saveDialog.setSelectedFile(new File(nomFichierExport + CocktailConstantes.EXTENSION_CSV));

		if(saveDialog.showSaveDialog(myView) == 0) {

			File file = saveDialog.getSelectedFile();
			CRICursor.setWaitCursor(myView);
			String texte = "";
			NSArray<EOSupInfoData> sortedDatas = EOSortOrdering.sortedArrayUsingKeyOrderArray(eodData.displayedObjects(), new NSArray(new EOSortOrdering(EOSupInfoData.EFIC_NOM_PATRONYMIQUE_KEY, EOSortOrdering.CompareAscending)));
			for(EOSupInfoData myData : sortedDatas) {
				texte =  texte + texteExportPourRecordSupinfoAnt(myData) + "\n";
			}

			UtilitairesFichier.afficherFichier(texte, file.getParent(), nomFichierExport, "csv", false);
			CRICursor.setDefaultCursor(myView);
		}
	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecordCRCT(EOSupInfoData record) {

		String texte = "";

		texte += CocktailExports.ajouterChamp(record.eficUai());

		texte += CocktailExports.ajouterChamp(record.eficNumen());

		texte += CocktailExports.ajouterChamp(record.eficCivilite());

		texte += CocktailExports.ajouterChamp(record.eficNomPatronymique());

		texte += CocktailExports.ajouterChamp(record.eficPrenom());

		texte += CocktailExports.ajouterChamp(record.eficNomUsuel());

		texte += CocktailExports.ajouterChampDate(record.eficDNaissance());

		texte += CocktailExports.ajouterChamp(record.eficCPaysNationalite());

		texte += CocktailExports.ajouterChamp(record.eficCPosition());

		texte += CocktailExports.ajouterChampDate(record.eficDDebPostion());

		texte += CocktailExports.ajouterChampDate(record.eficDFinPostion());

		texte += CocktailExports.ajouterChamp(record.eficCSectionCnu());

		texte += CocktailExports.ajouterChamp(record.eficCCorps());

		texte += CocktailExports.ajouterChamp(record.eficStatut());

		texte += CocktailExports.ajouterChampDate(record.eficDNominationCorps());

		texte += CocktailExports.ajouterChampDate(record.eficDTitularisation());

		texte += CocktailExports.ajouterChamp(record.eficCGrade());

		texte += CocktailExports.ajouterChampDate(record.eficDGrade());

		texte += CocktailExports.ajouterChamp(record.eficCEchelon());

		texte += CocktailExports.ajouterChamp(record.eficCChevron());

		texte += CocktailExports.ajouterChampDate(record.eficDEchelon());

		texte += CocktailExports.ajouterChamp(record.eficAncConservee());

		return texte;
	}

	/**
	 * Classe servant a colorer les cellules selon l'etat de l'engagement
	 */
	private class SupinfoRenderer extends ZEOTableCellRenderer	{

		private static final long serialVersionUID = -2907930349355563787L;
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOSupInfoData obj = (EOSupInfoData) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.estCrct() == true)
				leComposant.setBackground(new Color(255,151,96));
			else
				leComposant.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);

			return leComposant;
		}
	}

}
package org.cocktail.mangue.client.specialisations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.specialisations.SpecialisationCnuView;
import org.cocktail.mangue.client.select.NomenclatureSelectCodeLibelleCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;

/**
 * Controleur de la classe des spécialisations CNU
 * 
 * @author Cyril PINSARD
 * 
 */
public class SpecialisationCnuCtrl {

	private EOEditingContext edc;

	private SpecialisationCnuView myView;

	private EOCnu currentSpec;

	private EOContratAvenant 			currentContratAvenant;
	private IEmploiSpecialisation 		currentEmploiSpec;
	private EOCarriereSpecialisations 	currentCarSpec;

	private boolean saisieEnabled;

	/**
	 * 
	 * @param edc
	 */
	public SpecialisationCnuCtrl(EOEditingContext edc) {

		setEdc(edc);
		myView = new SpecialisationCnuView();

		myView.getBtnSelect().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectCnu();}});
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delSpec();}});

		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelle(), false, false);

		updateInterface();
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOCnu getCurrentSpec() {
		return currentSpec;
	}

	public void setCurrentSpec(EOCnu currentSpec) {
		this.currentSpec = currentSpec;
	}

	public JPanel getView() {
		return myView;
	}

	public boolean isSaisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	private void actualiser() {

		clearDatas();
		if (getCurrentSpec() != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentSpec().code());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentSpec().libelle());
		}
		updateInterface();

	}

	public void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfLibelle());
	}


	/**
	 * 
	 * @param spec
	 * @return
	 */
	public String getString(EOGenericRecord spec) {

		setCurrentSpec((EOCnu)spec.valueForKey(EOCarriereSpecialisations.TO_CNU_KEY));
		if (getCurrentSpec() != null)
			return "CNU : " + getCurrentSpec().code() + " - " + getCurrentSpec().libelle();

		return "";
	}

	/**
	 * @param spec : spécialisation de l'emploi {@link IEmploiSpecialisation}
	 * @return libellé
	 */
	public String getString(IEmploiSpecialisation object) {
		currentSpec = object.getToCnu();
		if (currentSpec != null)
			return currentSpec.code() + " - " + currentSpec.libelle();

		return "";
	}

	/**
	 * 
	 */
	private void selectCnu() {

		EOCnu spec = (EOCnu)NomenclatureSelectCodeLibelleCtrl.sharedInstance(getEdc()).getObject(NomenclatureFinder.find(getEdc(), EOCnu.ENTITY_NAME, Nomenclature.SORT_ARRAY_CODE));

		if (spec != null) {
			currentSpec = spec;
			actualiser();

			if (currentContratAvenant != null) {
				currentContratAvenant.setToCnuRelationship(spec);
			} else if (currentCarSpec != null) {
				currentCarSpec.setToCnuRelationship(spec);
			} else if (currentEmploiSpec != null) {
				currentEmploiSpec.setToCnuRelationship(currentSpec);
			}
		}
		updateInterface();

	}

	/**
	 * 
	 */
	private void delSpec() {

		setCurrentSpec(null);
		clearDatas();

		if (currentContratAvenant != null) {
			currentContratAvenant.setToCnuRelationship(null);
		} else if (currentCarSpec != null) {
			currentCarSpec.setToCnuRelationship(null);
		} else if (currentEmploiSpec != null) {
			currentEmploiSpec.setToDisciplineSdDegreRelationship(null);
		}

		updateInterface();

	}

	/**
	 * 
	 */
	private void clean() {

		currentContratAvenant = null;
		currentCarSpec = null;
		currentEmploiSpec = null;
	}

	/**
	 * Actualise les données pour l'affichage de la spécialisation CNU
	 * 
	 * @param record
	 *            : un emploi spécialisation {@link IEmploiSpecialisation}
	 */
	public void actualiser(IEmploiSpecialisation record) {
		clean();
		currentEmploiSpec = record;
		setCurrentSpec(record.getToCnu());
		actualiser();
	}

	public void actualiser(EOCarriereSpecialisations record) {
		clean();
		currentCarSpec = record;
		setCurrentSpec(record.toCnu());
		actualiser();
	}

	public void actualiser(EOContratAvenant record) {
		clean();
		currentContratAvenant = record;
		setCurrentSpec(record.toCnu());
		actualiser();
	}

	private void updateInterface() {
		myView.getBtnSelect().setVisible(isSaisieEnabled());
		myView.getBtnSupprimer().setVisible(isSaisieEnabled());
		myView.getBtnSupprimer().setEnabled(getCurrentSpec() != null);
	}

}
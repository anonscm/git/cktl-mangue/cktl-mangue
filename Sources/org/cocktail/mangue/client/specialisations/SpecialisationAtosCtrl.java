package org.cocktail.mangue.client.specialisations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.specialisations.SpecialisationAtosView;
import org.cocktail.mangue.client.select.specialisations.SpecialiteAtosSelectCtrl;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Controleur de la classe des spécialisations ATOS
 * 
 * @author Cyril PINSARD
 * 
 */
public class SpecialisationAtosCtrl {

	private EOEditingContext ec;

	private SpecialisationAtosView myView;
	private EOSpecialiteAtos currentSpec;
	private EOElementCarriere currentElement;
	private EOContratAvenant currentContratAvenant;
	private EOCarriereSpecialisations currentCarSpec;
	private IEmploiSpecialisation currentEmploiSpec;

	/**
	 * Constructeur
	 * 
	 * @param editingContext : editingContext
	 */
	public SpecialisationAtosCtrl(EOEditingContext editingContext) {
		ec = editingContext;
		myView = new SpecialisationAtosView();

		myView.getBtnSelect().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				getSpec();
			}
		});

		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delSpec();
			}
		});

		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelle(), false, false);

		updateUI();
	}

	public JPanel getView() {
		return myView;
	}

	public void setSaisieEnabled(boolean yn) {
		myView.getBtnSelect().setVisible(yn);
		myView.getBtnSupprimer().setVisible(yn);
	}

	public void clearTextFields() {

		myView.getTfCode().setText("");
		myView.getTfLibelle().setText("");

	}

	/**
	 * @param record : spécialisation de l'emploi {@link IEmploiSpecialisation}
	 * @return libellé
	 */
	public String getString(IEmploiSpecialisation record) {
		currentSpec = record.getToSpecialiteAtos();
		if (currentSpec != null) {
			return "ATOS : " + currentSpec.code() + " - " + currentSpec.libelle();
		}

		return "";
	}

	public String getString(EOContratAvenant avenant) {
		currentSpec = avenant.toSpecialiteAtos();
		if (currentSpec != null)
			return "ATOS : " + currentSpec.code() + " - " + currentSpec.libelle();

		return "";
	}

	public String getString(EOCarriereSpecialisations spec) {
		currentSpec = spec.toSpecialiteAtos();
		if (currentSpec != null)
			return "ATOS : " + currentSpec.code() + " - " + currentSpec.libelle();

		return "";
	}

	private void getSpec() {
		EOSpecialiteAtos spec = SpecialiteAtosSelectCtrl.sharedInstance(ec).getSpec();

		if (spec != null) {
			currentSpec = spec;
			actualiser();

			if (currentContratAvenant != null) {
				currentContratAvenant.setToSpecialiteAtosRelationship(currentSpec);
			} else 
				if (currentCarSpec != null) {
					currentCarSpec.setToSpecialiteAtosRelationship(currentSpec);
				} else if (currentEmploiSpec != null) {
					currentEmploiSpec.setToSpecialiteAtosRelationship(currentSpec);
				}
		}
		updateUI();

	}

	private void delSpec() {
		currentSpec = null;
		clearTextFields();

		if (currentContratAvenant != null) {
			currentContratAvenant.setToSpecialiteAtosRelationship(null);
		} else 
			if (currentCarSpec != null) {
			currentCarSpec.setToSpecialiteAtosRelationship(null);
		} else if (currentEmploiSpec != null) {
			currentEmploiSpec.setToSpecialiteAtosRelationship(null);
		}
		updateUI();

	}

	private void clean() {

		currentContratAvenant = null;
		currentElement = null;
		currentCarSpec = null;
		currentEmploiSpec = null;
	}

	/**
	 * Actualise les données pour l'affichage de la spécialisation ATOS
	 * 
	 * @param record  : un emploi spécialisation {@link IEmploiSpecialisation}
	 */
	public void actualiser(IEmploiSpecialisation record) {
		clean();
		currentEmploiSpec = record;
		currentSpec = record.getToSpecialiteAtos();
		actualiser();
	}

	public void actualiser(EOCarriereSpecialisations record) {
		clean();
		currentCarSpec = record;
		currentSpec = record.toSpecialiteAtos();
		actualiser();
	}

	public void actualiser(EOContratAvenant record) {
		clean();
		currentContratAvenant = record;
		currentSpec = record.toSpecialiteAtos();
		actualiser();
	}

	private void actualiser() {

		myView.getTfCode().setText("");
		myView.getTfLibelle().setText("");
		if (currentSpec != null) {
			myView.getTfCode().setText(currentSpec.code());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), currentSpec.libelle());
		}
		updateUI();

	}

	private void updateUI() {
		myView.getBtnSupprimer().setEnabled(currentSpec != null);
	}

}

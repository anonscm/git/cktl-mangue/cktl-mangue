package org.cocktail.mangue.client.specialisations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.specialisations.SpecialisationItarfView;
import org.cocktail.mangue.client.select.specialisations.ReferensEmploisSelectCtrl;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Controleur de la classe des spécialisations ITARF
 * 
 * @author Cyril PINSARD
 * 
 */
public class SpecialisationItarfCtrl {

	private EOEditingContext ec;

	private SpecialisationItarfView myView;
	private EOReferensEmplois currentReferensEmploi;
	private EOContratAvenant currentContratAvenant;
	private EOCarriereSpecialisations currentCarSpec;
	private IEmploiSpecialisation currentEmploiSpec;

	/**
	 * Constructeur
	 * 
	 * @param editingContext : editingContext
	 */
	public SpecialisationItarfCtrl(EOEditingContext editingContext) {
		ec = editingContext;
		myView = new SpecialisationItarfView();

		myView.getBtnSelect().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				selectReferensEmploi();
			}
		});
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delSpecialite();
			}
		});

		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelle(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleFamille(), false, false);

		updateInterface();
	}

	public JPanel getView() {
		return myView;
	}

	public void setSaisieEnabled(boolean yn) {
		myView.getBtnSelect().setVisible(yn);
		myView.getBtnSelectFamille().setVisible(yn);
		myView.getBtnSupprimer().setVisible(yn);
	}

	public void clearTextFields() {
		myView.getTfCode().setText("");
		myView.getTfLibelle().setText("");
		myView.getTfLibelleFamille().setText("");
	}

	/**
	 * 
	 */
	private void selectReferensEmploi() {
		EOReferensEmplois referens = (EOReferensEmplois)ReferensEmploisSelectCtrl.sharedInstance(ec).getObject(DateCtrl.today());

		if (referens != null) {
			currentReferensEmploi = referens;
			actualiser();
			
			if (currentContratAvenant != null) {
				currentContratAvenant.setToReferensEmploiRelationship(referens);
			} else if (currentCarSpec != null) {
				currentCarSpec.setToReferensEmploiRelationship(referens);
			} else if (currentEmploiSpec != null) {
				currentEmploiSpec.setToEmploiReferensRelationship(referens);
			}
		}

		updateInterface();
	}

	private void delSpecialite() {
		currentReferensEmploi = null;
		clearTextFields();
		
		if (currentContratAvenant != null) {
			currentContratAvenant.setToReferensEmploiRelationship(null);
		} else if (currentCarSpec != null) {
			currentCarSpec.setToReferensEmploiRelationship(null);
		} else if (currentEmploiSpec != null) {
			currentEmploiSpec.setToEmploiReferensRelationship(null);
		}
		
		updateInterface();
	}

	/**
	 * @param spec : spécialisation de l'emploi {@link IEmploiSpecialisation}
	 * @return libellé
	 */
	public String getString(IEmploiSpecialisation spec) {
		currentReferensEmploi = spec.getToEmploiReferens();
		
		if (currentReferensEmploi != null) {
			return "REFERENS : " + currentReferensEmploi.code() + " - " + currentReferensEmploi.libelle();
		}
		
		return "";
	}

	public String getString(EOContratAvenant avenant) {
		currentReferensEmploi = avenant.toReferensEmploi();
		if (currentReferensEmploi != null)
			return "REFERENS : " + currentReferensEmploi.code() + " - " + currentReferensEmploi.libelle();
		return "";
	}

	public String getString(EOCarriereSpecialisations element) {
		currentReferensEmploi = element.toReferensEmploi();
		if (currentReferensEmploi != null)
			return "REFERENS : " + currentReferensEmploi.code() + " - " + currentReferensEmploi.libelle();

		return "";
	}

	/**
	 * 
	 */
	private void clean() {
		currentContratAvenant = null;
		currentCarSpec = null;
		currentEmploiSpec = null;
	}

	public void actualiser(EOContratAvenant record) {
		clean();
		currentContratAvenant = record;
		currentReferensEmploi = record.toReferensEmploi();
		actualiser();
	}

	public void actualiser(EOCarriereSpecialisations record) {
		clean();
		currentCarSpec = record;
		currentReferensEmploi = record.toReferensEmploi();
		actualiser();
	}

	/**
	 * Actualise les données pour l'affichage de la spécialisation ITARF
	 * 
	 * @param record : un emploi spécialisation {@link IEmploiSpecialisation}
	 */
	public void actualiser(IEmploiSpecialisation record) {
		clean();
		currentEmploiSpec = record;
		currentReferensEmploi = record.getToEmploiReferens();
		actualiser();
	}

	public void actualiser() {
		clearTextFields();
		if (currentReferensEmploi != null) {
			myView.getTfCode().setText(currentReferensEmploi.code());
			myView.getTfLibelle().setText(currentReferensEmploi.libelle());
			myView.getTfLibelleFamille().setText(currentReferensEmploi.libelleBap());
		}
		updateInterface();
	}

	private void updateInterface() {
		myView.getBtnSupprimer().setEnabled(currentReferensEmploi != null);
		myView.getBtnSelectFamille().setEnabled(false);
	}

}

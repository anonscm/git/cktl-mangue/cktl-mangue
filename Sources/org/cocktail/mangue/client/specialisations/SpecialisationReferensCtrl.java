package org.cocktail.mangue.client.specialisations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.specialisations.SpecialisationReferensView;
import org.cocktail.mangue.client.select.specialisations.ReferensEmploisSelectCtrl;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Controleur de la classe des spécialisations ITARF
 * 
 * @author Cyril PINSARD
 * 
 */
public class SpecialisationReferensCtrl {

	private EOEditingContext ec;

	private SpecialisationReferensView myView;
	
	private EOReferensEmplois currentSpec;
	
	private EOContratAvenant 			currentContratAvenant;
	private EOCarriereSpecialisations 	currentCarSpec;
	private IEmploiSpecialisation 		currentEmploiSpec;

	private boolean 	saisieEnabled;
	private NSTimestamp dateReference;
	
	/**
	 * Constructeur
	 * 
	 * @param editingContext : editingContext
	 */
	public SpecialisationReferensCtrl(EOEditingContext editingContext) {
		ec = editingContext;
		myView = new SpecialisationReferensView();

		myView.getBtnSelect().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				selectReferensEmploi();
			}
		});
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delReferens();
			}
		});

		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelle(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleBap(), false, false);

		updateInterface();
	}
	public NSTimestamp getDateReference() {
		return dateReference;
	}

	public void setDateReference(NSTimestamp dateReference) {
		this.dateReference = dateReference;
	}
	
	public EOReferensEmplois getCurrentSpec() {
		return currentSpec;
	}
	public void setCurrentSpec(EOReferensEmplois currentReferens) {
		this.currentSpec = currentReferens;
	}


	public JPanel getView() {
		return myView;
	}

	public boolean isSaisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	/**
	 * 
	 */
	public void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfLibelle());
		CocktailUtilities.viderTextField(myView.getTfLibelleBap());
	}

	/**
	 * 
	 */
	private void selectReferensEmploi() {
		
		EOReferensEmplois selectedReferens = (EOReferensEmplois)ReferensEmploisSelectCtrl.sharedInstance(ec).getObject(getDateReference());

		if (selectedReferens != null) {
			setCurrentSpec(selectedReferens);
			updateDatas();
			
			if (currentContratAvenant != null) {
				currentContratAvenant.setToReferensEmploiRelationship(selectedReferens);
			} else if (currentCarSpec != null) {
				currentCarSpec.setToReferensEmploiRelationship(selectedReferens);
			} else if (currentEmploiSpec != null) {
				currentEmploiSpec.setToEmploiReferensRelationship(selectedReferens);
			}
		}

		updateInterface();
	}

	/**
	 * 
	 */
	private void delReferens() {
		setCurrentSpec(null);
		clearDatas();
		
		if (currentContratAvenant != null) {
			currentContratAvenant.setToReferensEmploiRelationship(null);
		} else if (currentCarSpec != null) {
			currentCarSpec.setToReferensEmploiRelationship(null);
		} else if (currentEmploiSpec != null) {
			currentEmploiSpec.setToEmploiReferensRelationship(null);
		}
		
		updateInterface();
	}

	/**
	 * @param spec : spécialisation de l'emploi {@link IEmploiSpecialisation}
	 * @return libellé
	 */
	public String getString(IEmploiSpecialisation spec) {
		setCurrentSpec(spec.getToEmploiReferens());
		
		if (currentSpec != null) {
			return "REFERENS : " + currentSpec.code() + " - " + currentSpec.libelle();
		}
		
		return "";
	}

	public String getString(EOContratAvenant avenant) {
		setCurrentSpec(avenant.toReferensEmploi());
		if (getCurrentSpec() != null)
			return "REFERENS : " + getCurrentSpec().code() + " - " + getCurrentSpec().libelle();
		return "";
	}

	public String getString(EOCarriereSpecialisations element) {
		setCurrentSpec(element.toReferensEmploi());
		if (getCurrentSpec() != null)
			return "REFERENS : " + getCurrentSpec().code() + " - " + getCurrentSpec().libelle();

		return "";
	}

	/**
	 * 
	 */
	private void clean() {
		currentContratAvenant = null;
		currentCarSpec = null;
		currentEmploiSpec = null;
	}

	public void actualiser(EOContratAvenant record) {
		clean();
		currentContratAvenant = record;
		setCurrentSpec(record.toReferensEmploi());
		updateDatas();
	}

	/**
	 * 
	 * @param record
	 */
	public void actualiser(EOCarriereSpecialisations record) {
		clean();
		currentCarSpec = record;
		setCurrentSpec(record.toReferensEmploi());
		updateDatas();
	}

	/**
	 * Actualise les données pour l'affichage de la spécialisation REFERENS
	 * 
	 * @param record : un emploi spécialisation {@link IEmploiSpecialisation}
	 */
	public void actualiser(IEmploiSpecialisation record) {
		
		clean();
		currentEmploiSpec = record;
		setCurrentSpec(record.getToEmploiReferens());
		updateDatas();
	}

	/**
	 * 
	 */
	public void updateDatas() {
		clearDatas();
		if (getCurrentSpec() != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentSpec().code());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentSpec().libelle());
			CocktailUtilities.setTextToField(myView.getTfLibelleBap(), getCurrentSpec().libelleBap());
		}
		updateInterface();
	}

	/**
	 * 
	 */
	private void updateInterface() {
		myView.getBtnSelect().setVisible(isSaisieEnabled());
		myView.getBtnSupprimer().setEnabled(isSaisieEnabled() && getCurrentSpec() != null);
	}

}

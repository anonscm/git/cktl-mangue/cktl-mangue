package org.cocktail.mangue.client.specialisations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.specialisations.SpecialisationCnecaView;
import org.cocktail.mangue.client.select.specialisations.SectionCnecaSelectCtrl;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSectionsCneca;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;

public class SpecialisationCnecaCtrl {

	private EOEditingContext ec;

	private SpecialisationCnecaView myView;

	private EOSectionsCneca currentSpec;

	private EOElementCarriere currentElement;
	private EOContratAvenant currentContratAvenant;
	private IEmploiSpecialisation 		currentEmploiSpec;
	private EOCarriereSpecialisations currentCarSpec;

	public SpecialisationCnecaCtrl(EOEditingContext editingContext)	{

		ec = editingContext;
		myView = new SpecialisationCnecaView();

		myView.getBtnSelect().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getSpec();}});

		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delSpec();}});

		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelle(), false, false);

		updateUI();
	}

	public JPanel getView() {
		return myView;
	}	

	public void setSaisieEnabled(boolean yn) {
		myView.getBtnSelect().setVisible(yn);
		myView.getBtnSupprimer().setVisible(yn);
	}
	public void clearTextFields() {

		myView.getTfCode().setText("");
		myView.getTfLibelle().setText("");

	}

	public String getString(EOCarriereSpecialisations element) {
		currentSpec = element.toCneca();
		if (currentSpec != null)
			return "CNECA : " + currentSpec.libelleCourt() + " - " + currentSpec.libelleLong();

		return "";
	}

	/**
	 * 
	 */
	private void getSpec() {

		EOSectionsCneca spec = SectionCnecaSelectCtrl.sharedInstance(ec).getSpec();

		if (spec  != null) {

			currentSpec = spec;
			actualiser();
			if (currentCarSpec != null)
				currentCarSpec.setToCnecaRelationship(spec);
		}
		updateUI();

	}

	private void delSpec() {

		currentSpec = null;
		clearTextFields();
		if (currentContratAvenant != null)
			currentContratAvenant.setToCnuRelationship(null);
		else
			if (currentEmploiSpec != null)
				currentEmploiSpec.setToCnuRelationship(null);
			else
					if (currentCarSpec != null)
						currentCarSpec.setToCnuRelationship(null);

		updateUI();

	}

	private void clean() {

		currentContratAvenant = null;
		currentElement = null;
		currentCarSpec = null;
		currentEmploiSpec = null;

	}

	public void actualiser(EOCarriereSpecialisations record) {
		clean();
		currentCarSpec = record;
		currentSpec = record.toCneca();
		actualiser();
	}

	/**
	 * 
	 */
	private void actualiser() {

		clearTextFields();
		if (currentSpec != null) {
			myView.getTfCode().setText(currentSpec.libelleCourt());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), currentSpec.libelleLong());
		}		
		updateUI();

	}
	private void updateUI(){
		myView.getBtnSupprimer().setEnabled(currentSpec != null);
	}

}

package org.cocktail.mangue.client.specialisations;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.specialisations.SpecialisationView;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiCategorieFinder;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Gestion des specialisation. CNU, BAP, DISC Second Degré
 * 
 * @author cpinsard
 * 
 */

public class SpecialisationCtrl {

	private static final String ID_VIEW_VIDE = "VIDE";
	private static final String ID_VIEW_CNU = "CNU";
	private static final String ID_VIEW_CNECA = "CNECA";
	private static final String ID_VIEW_ATOS = "ATOS";
	private static final String ID_VIEW_2ND_DEGRE = "2ND_DEGRE";
	private static final String ID_VIEW_REFERENS = "REFERENS";
	private static final String ID_VIEW_ITARF = "ITARF";
	private EOEditingContext ec;
	private SpecialisationView myView;

	private String idView;

	private SpecialisationCnecaCtrl ctrlCneca;
	private SpecialisationCnuCtrl ctrlCnu;
	private Specialisation2ndDegreCtrl ctrl2ndDegre;
	private SpecialisationAtosCtrl ctrlAtos;
	private SpecialisationReferensCtrl ctrlReferens;
	private SpecialisationItarfCtrl ctrlItarf;

	private EOCarriereSpecialisations 	currentCarSpec;
	private IEmploiSpecialisation 		currentEmploiSpecialisation;
	private EOContratAvenant 			currentAvenant;
	private EOTypePopulation 			currentTypePopulation;
	private NSTimestamp					dateReference;

	/**
	 * Constructeur
	 * 
	 * @param editingContext : editingContext
	 */
	public SpecialisationCtrl(EOEditingContext editingContext) {

		ec = editingContext;

		ctrlCnu = new SpecialisationCnuCtrl(ec);
		ctrl2ndDegre = new Specialisation2ndDegreCtrl(ec);
		ctrlAtos = new SpecialisationAtosCtrl(ec);
		ctrlItarf = new SpecialisationItarfCtrl(ec);
		ctrlCneca = new SpecialisationCnecaCtrl(ec);
		ctrlReferens = new SpecialisationReferensCtrl(ec);

		myView = new SpecialisationView();

		myView.getSwapView().add("VIDE",new JPanel(new BorderLayout()));
		myView.getSwapView().add(ID_VIEW_REFERENS, ctrlReferens.getView());
		myView.getSwapView().add(ID_VIEW_ITARF,ctrlItarf.getView());
		myView.getSwapView().add(ID_VIEW_ATOS,ctrlAtos.getView());
		myView.getSwapView().add(ID_VIEW_2ND_DEGRE,ctrl2ndDegre.getView());
		myView.getSwapView().add(ID_VIEW_CNU,ctrlCnu.getView());
		myView.getSwapView().add(ID_VIEW_CNECA,ctrlCneca.getView());

		((CardLayout) myView.getSwapView().getLayout()).show(myView.getSwapView(), ID_VIEW_VIDE);

	}

	public void clean() {

		((CardLayout) myView.getSwapView().getLayout()).show(myView.getSwapView(), ID_VIEW_VIDE);

	}

	public NSTimestamp getDateReference() {
		return dateReference;
	}

	public void setDateReference(NSTimestamp dateReference) {
		this.dateReference = dateReference;
		ctrlReferens.setDateReference(dateReference);
	}

	public void setSaisieEnabled(boolean yn) {

		ctrlReferens.setSaisieEnabled(yn);
		ctrlCnu.setSaisieEnabled(yn);
		ctrl2ndDegre.setSaisieEnabled(yn);
		ctrlAtos.setSaisieEnabled(yn);
		ctrlCneca.setSaisieEnabled(yn);

	}

	private String idView() {
		return idView;
	}

	private void setIdView(String idView) {
		this.idView = idView;
	}

	public EOContratAvenant getCurrentAvenant() {
		return currentAvenant;
	}

	public void setCurrentAvenant(EOContratAvenant currentAvenant) {
		this.currentAvenant = currentAvenant;
	}

	public EOTypePopulation getCurrentTypePopulation() {
		return currentTypePopulation;
	}

	public void setCurrentTypePopulation(EOTypePopulation currentTypePopulation) {
		this.currentTypePopulation = currentTypePopulation;
	}

	public JPanel getView() {
		return myView;
	}

	public EOCarriereSpecialisations currentCarSpec() {
		return currentCarSpec;
	}

	public void setCurrentCarSpec(EOCarriereSpecialisations currentCarSpec) {
		this.currentCarSpec = currentCarSpec;
	}

	public String getStringSpecForCarriereSpec(EOCarriereSpecialisations carSpec) {

		String spec = "";

		try {
			if (carSpec.toCarriere().toIndividu().personnel().estMAAF()) {

				spec = ctrlCneca.getString(carSpec);
			}
			else {
				if (carSpec != null) {
					currentTypePopulation = carSpec.toCarriere().toTypePopulation();
					if (currentTypePopulation != null) {
						if (currentTypePopulation.estItarf() || currentTypePopulation.estBiblio()) {
							if (DateCtrl.isAfterEq(carSpec.specDebut(), DateCtrl.stringToDate(ManGUEConstantes.DEBUT_VALIDITE_REFERENS))) {
								spec = ctrlReferens.getString(carSpec);    
							}
							else {
								spec = ctrlReferens.getString(carSpec);
							}
						}
						else
							if (currentTypePopulation.estAtos())
								spec = ctrlAtos.getString(carSpec);                
							else
								if (currentTypePopulation.est2Degre())
									spec = ctrl2ndDegre.getString(carSpec);                
								else
									if (currentTypePopulation.estAtos())
										spec = ctrlAtos.getString(carSpec);                
									else
										if (currentTypePopulation.est2Degre())
											spec = ctrl2ndDegre.getString(carSpec);                
										else
											if (currentTypePopulation.estEnseignant())
												spec = ctrlCnu.getString(carSpec);                
					}
				}
			}
		}
		catch (Exception e) {
		}

		return spec;

	}

	/**
	 * 
	 * @param element
	 * @return
	 */
	public String getStringSpecForAvenant(EOContratAvenant avenant) {

		String spec = "";

		if (avenant != null && avenant.toGrade() != null)
			currentTypePopulation = avenant.toGrade().toCorps().toTypePopulation();
		if (currentTypePopulation != null) {
			if (currentTypePopulation.estItarf() || currentTypePopulation.estBiblio())
				spec = ctrlReferens.getString(avenant);
			else if (currentTypePopulation.estAtos())
				spec = ctrlAtos.getString(avenant);
			else if (currentTypePopulation.est2Degre())
				spec = ctrl2ndDegre.getString(avenant);
			else if (currentTypePopulation.estEnseignant())
				spec = ctrlCnu.getString(avenant);
		}

		return spec;

	}

	/**
	 * Affiche les spécialisations
	 * 
	 * @param specialisation : un emploi spécialisation {@link IEmploiSpecialisation}
	 */
	public void showSpecForEmploiSpecialisation(IEmploiSpecialisation specialisation) {
		if ((specialisation != null) && (specialisation.getToEmploi() != null)
				&& (specialisation.getToEmploi().getListeEmploiCategories().size() > 0)) {
			setCurrentEmploiSpecialisation(specialisation);
			LogManager.logDetail(" > spécialisation : " + specialisation.libelleSpecialisationAffiche());
			setCurrentTypePopulation(EmploiCategorieFinder.sharedInstance().rechercherTypePopulationPourEmploi(
					new EOEditingContext(), specialisation.getToEmploi(), specialisation.getDateDebut()));
			LogManager.logDetail(" > type de population de l'emploi : " + currentTypePopulation);


			showView(false);
			if (getCurrentTypePopulation() != null) {
				if (getCurrentTypePopulation().estItarf() || getCurrentTypePopulation().estBiblio()) {
					ctrlReferens.actualiser(getCurrentEmploiSpecialisation());
				} else if (getCurrentTypePopulation().estAtos()) {
					ctrlAtos.actualiser(getCurrentEmploiSpecialisation());
				} else if (getCurrentTypePopulation().est2Degre()) {
					ctrl2ndDegre.actualiser(getCurrentEmploiSpecialisation());
				} else if (getCurrentTypePopulation().estEnseignant()) {
					ctrlCnu.actualiser(getCurrentEmploiSpecialisation());
				}
			}
		}
	}

	/**
	 * 
	 * @param specialisation
	 */
	public void showSpecForCarriereSpecialisation(EOCarriereSpecialisations specialisation) {

		setCurrentCarSpec(specialisation);
		setCurrentTypePopulation(specialisation.toCarriere().toTypePopulation());

		if (specialisation.toCarriere().toIndividu().personnel().estMAAF() && getCurrentTypePopulation().estEnseignant()) {
			ctrlCneca.actualiser(currentCarSpec());	
			showView(true);
		}
		else {
			if (specialisation.toCarriere().toTypePopulation() != null) {
				showView(false);
				if (getCurrentTypePopulation().estItarf() || getCurrentTypePopulation().estBiblio()) {
					ctrlReferens.actualiser(currentCarSpec());
				}
				else if (getCurrentTypePopulation().estAtos())
					ctrlAtos.actualiser(currentCarSpec());
				else if (getCurrentTypePopulation().est2Degre())
					ctrl2ndDegre.actualiser(currentCarSpec());
				else if (getCurrentTypePopulation().estEnseignant())
					ctrlCnu.actualiser(currentCarSpec());
			}
		}
	}

	/**
	 * 
	 * @param avenant
	 */
	public void showSpecForAvenant(EOContratAvenant avenant) {

		setCurrentTypePopulation(null);
		setCurrentAvenant(avenant);

		if (currentAvenant.toGrade() != null && currentAvenant.toGrade().estDoctorantEns()) {
			setIdView(ID_VIEW_CNU);
			((CardLayout) myView.getSwapView().getLayout()).show(myView.getSwapView(), idView());
			ctrlCnu.actualiser(getCurrentAvenant());
		} else {
			if (currentAvenant != null && currentAvenant.toGrade() != null)
				setCurrentTypePopulation(currentAvenant.toGrade().toCorps().toTypePopulation());

			showView(false);

			if (getCurrentTypePopulation() != null) {

				if (getCurrentTypePopulation().estItarf() || getCurrentTypePopulation().estBiblio())
					ctrlReferens.actualiser(getCurrentAvenant());
				else if (getCurrentTypePopulation().estAtos())
					ctrlAtos.actualiser(getCurrentAvenant());
				else if (getCurrentTypePopulation().est2Degre())
					ctrl2ndDegre.actualiser(getCurrentAvenant());
				else if (getCurrentTypePopulation().estEnseignant() || getCurrentAvenant().toGrade().estDoctorantEns())
					ctrlCnu.actualiser(getCurrentAvenant());
			}
		}
	}

	/**
	 * Affichage de la liste des specialisations en fonction du type de population
	 */
	private void showView(boolean estMaaf) {

		if (estMaaf) {
			setIdView(ID_VIEW_CNECA);				
		}
		else {
			
			setIdView(ID_VIEW_VIDE);

			if (getCurrentTypePopulation() != null) {
				if (getCurrentTypePopulation().estItarf() || getCurrentTypePopulation().estBiblio()) {
					if ( getDateReference() == null || DateCtrl.isAfterEq(getDateReference(), DateCtrl.stringToDate(ManGUEConstantes.DEBUT_VALIDITE_REFERENS)) )
						setIdView(ID_VIEW_REFERENS);
					else
						setIdView(ID_VIEW_ITARF);
				} else if (getCurrentTypePopulation().est2Degre()) {
					setIdView(ID_VIEW_2ND_DEGRE);
				} else if (getCurrentTypePopulation().estAtos()) {
					setIdView(ID_VIEW_ATOS);
				} else if (getCurrentTypePopulation().estEnseignant() || (getCurrentAvenant() != null && getCurrentAvenant().toGrade().estDoctorantEns())) {
					setIdView(ID_VIEW_CNU);
				}
			}
		}

		((CardLayout) myView.getSwapView().getLayout()).show(myView.getSwapView(), idView());

	}

	/**
	 * @param specialisation : spécialisation de l'emploi {@link IEmploiSpecialisation}
	 * @return libellé
	 */
	public String getStringSpecForEmploi(IEmploiSpecialisation specialisation) {
		String spec = "";

		currentTypePopulation = specialisation.getToEmploi().getListeEmploiCategories().get(0).getToCategorieEmploi().typePopulation();
		if (currentTypePopulation != null) {
			if (currentTypePopulation.estItarf() || currentTypePopulation.estBiblio()) {
				spec = ctrlReferens.getString(specialisation);
			} else if (currentTypePopulation.estAtos()) {
				spec = ctrlAtos.getString(specialisation);
			} else if (currentTypePopulation.est2Degre()) {
				spec = ctrl2ndDegre.getString(specialisation);
			} else if (currentTypePopulation.estEnseignant()) {
				spec = ctrlCnu.getString(specialisation);
			}
		}

		return spec;
	}

	public IEmploiSpecialisation getCurrentEmploiSpecialisation() {
		return currentEmploiSpecialisation;
	}

	public void setCurrentEmploiSpecialisation(IEmploiSpecialisation currentEmploiSpecialisation) {
		this.currentEmploiSpecialisation = currentEmploiSpecialisation;
	}

}

package org.cocktail.mangue.client.specialisations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.specialisations.Specialisation2ndDegreView;
import org.cocktail.mangue.client.select.NomenclatureSelectCodeLibelleCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Controleur de la classe des spécialisations ATOS
 * 
 * @author Cyril PINSARD
 * 
 */
public class Specialisation2ndDegreCtrl {

	private EOEditingContext edc;

	private Specialisation2ndDegreView myView;
	private EODiscSecondDegre currentSpec;
	private EOContratAvenant currentContratAvenant;
	private EOCarriereSpecialisations currentCarSpec;
	private IEmploiSpecialisation currentEmploiSpec;

	/**
	 * Constructeur
	 * 
	 * @param editingContext : editingContext
	 */
	public Specialisation2ndDegreCtrl(EOEditingContext editingContext) {

		edc = editingContext;
		myView = new Specialisation2ndDegreView();

		myView.getBtnSelect().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				selectSpec();
			}
		});

		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				delSpec();
			}
		});

		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelle(), false, false);

		updateInterface();
	}

	public JPanel getView() {
		return myView;
	}

	public void setSaisieEnabled(boolean yn) {
		myView.getBtnSelect().setVisible(yn);
		myView.getBtnSupprimer().setVisible(yn);
	}

	public void clearTextFields() {
		myView.getTfCode().setText("");
		myView.getTfLibelle().setText("");
	}

	public String getString(EOContratAvenant avenant) {
		currentSpec = avenant.toDiscSecondDegre();
		if (currentSpec != null)
			return "DISC : " + currentSpec.code() + " - " + currentSpec.libelle();

		return "";
	}

	public String getString(EOCarriereSpecialisations spec) {
		currentSpec = spec.toDiscSecondDegre();
		if (currentSpec != null)
			return "DISC : " + currentSpec.code() + " - " + currentSpec.libelle();

		return "";
	}

	/**
	 * @param spec : spécialisation de l'emploi {@link IEmploiSpecialisation}
	 * @return libellé
	 */
	public String getString(IEmploiSpecialisation spec) {
		currentSpec = spec.getToDisciplineSdDegre();

		if (currentSpec != null) {
			return "DISC : " + currentSpec.code() + " - " + currentSpec.libelle();
		}

		return "";
	}

	private void selectSpec() {
		EODiscSecondDegre spec = (EODiscSecondDegre)NomenclatureSelectCodeLibelleCtrl.sharedInstance(edc).getObject(NomenclatureFinder.findStatic(edc, EODiscSecondDegre.ENTITY_NAME));

		if (spec != null) {
			currentSpec = spec;
			actualiser();

			if (currentContratAvenant != null) {
				currentContratAvenant.setToDiscSecondDegreRelationship(currentSpec);
			} else 
				if (currentCarSpec != null) {
					currentCarSpec.setToDiscSecondDegreRelationship(currentSpec);
			} else if (currentEmploiSpec != null) {
				currentEmploiSpec.setToDisciplineSdDegreRelationship(currentSpec);
			}
		}
		updateInterface();

	}

	private void delSpec() {
		currentSpec = null;
		clearTextFields();
		
		if (currentContratAvenant != null) {
			currentContratAvenant.setToDiscSecondDegreRelationship(null);
		} 
		else if (currentCarSpec != null) {
			currentCarSpec.setToDiscSecondDegreRelationship(null);
		} else if (currentEmploiSpec != null) {
			currentEmploiSpec.setToDisciplineSdDegreRelationship(null);
		}

		updateInterface();
	}

	private void clean() {
		currentContratAvenant = null;
		currentCarSpec = null;
		currentEmploiSpec = null;
	}

	public void actualiser(EOCarriereSpecialisations record) {
		clean();
		currentCarSpec = record;
		currentSpec = record.toDiscSecondDegre();
		actualiser();
	}

	public void actualiser(EOContratAvenant record) {
		clean();
		currentContratAvenant = record;
		currentSpec = record.toDiscSecondDegre();
		actualiser();
	}

	/**
	 * Actualise les données pour l'affichage de la spécialisation Discipline second degré
	 * 
	 * @param record : un emploi spécialisation {@link IEmploiSpecialisation}
	 */
	public void actualiser(IEmploiSpecialisation record) {
		clean();
		currentEmploiSpec = record;
		currentSpec = record.getToDisciplineSdDegre();
		actualiser();
	}

	public void actualiser() {

		myView.getTfCode().setText("");
		myView.getTfLibelle().setText("");
		if (currentSpec != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), currentSpec.code());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), currentSpec.libelle());
		}
	}

	private void updateInterface() {
		myView.getBtnSupprimer().setEnabled(currentSpec != null);
	}

}

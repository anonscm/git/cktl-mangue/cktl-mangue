/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.extraction;

import java.awt.Cursor;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.component.COView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.EtatComparatifCarriere;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Prepare une extraction comportant les grades, echelon, indice des individus du type de personnel selectionne
 * a la date de debut et a la date de fin. Les traitements sont effectues sur le serveur.
 * @author christine
 *
 */
// 03/11/2010 - Adaptation Netbeans
public class ExtractionEtatComparatif extends ModelePage {
	public COView vuePeriode;
	private GestionPeriode controleurPeriode;
	private NSArray etatsComparatifs;
	private boolean selectionModifiee,estComparaisonDebut;
	private int typeEnseignant,typePersonnel;


	// Accesseurs
	public int typePersonnel() {
		return typePersonnel;
	}
	public void setTypePersonnel(int typePersonnel) {
		this.typePersonnel = typePersonnel;
	}
	public int typeEnseignant() {
		return typeEnseignant;
	}
	public void setTypeEnseignant(int typeEnseignant) {
		this.typeEnseignant = typeEnseignant;
	}
	public boolean estComparaisonDebut() {
		return estComparaisonDebut;
	}
	public void setEstComparaisonDebut(boolean aBool) {
		this.estComparaisonDebut = aBool;
	}
	/** retourne le nombre total d'elements affiches */
	public int nbTotalElements() {
		return displayGroup().displayedObjects().count();
	}
	// Actions
	public void preparer() {
		LogManager.logDetail("ExtractionEtatComparatif - preparer");
		controllerDisplayGroup().redisplay();
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		preparerEtatsComparatifs();
		displayGroup().setObjectArray(etatsComparatifs);
		displayGroup().updateDisplayedObjects();
		selectionModifiee = false;
		controllerDisplayGroup().redisplay();
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	public void annuler() {
		LogManager.logDetail("ExtractionEtatComparatif - annuler");
		displayGroup().setObjectArray(etatsComparatifs);
		displayGroup().updateDisplayedObjects();
		selectionModifiee = false;
		controllerDisplayGroup().redisplay();
	}
	public void supprimer() {
		LogManager.logDetail("ExtractionEtatComparatif - supprimer");
		NSMutableArray temp = new NSMutableArray(displayGroup().displayedObjects());
		temp.removeObjectsInArray(displayGroup().selectedObjects());
		displayGroup().setObjectArray(temp);
		displayGroup().updateDisplayedObjects();
		selectionModifiee = true;
		controllerDisplayGroup().redisplay();
	}
	public void restreindre() {
		LogManager.logDetail("ExtractionEtatComparatif - restreindre");
		displayGroup().setObjectArray(displayGroup().selectedObjects());
		displayGroup().updateDisplayedObjects();
		selectionModifiee = true;
		controllerDisplayGroup().redisplay();
	}
	public void exporter() {
		LogManager.logDetail("ExtractionEtatComparatif - exporter");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			String texte = headerExport();
			java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EtatComparatifCarriere etat = (EtatComparatifCarriere)e.nextElement();
				texte = texte + texteExport(etat);
			}
			UtilitairesImpression.afficherFichierExport(texte,file.getParent(),file.getName());
		}
	}
	// Notifications
	public void synchroniserDates(NSNotification aNotif) {
		LogManager.logDetail("ExtractionEtatComparatif - synchroniserDates");
		controllerDisplayGroup().redisplay();	
	}
	// méthodes du controller DG
	/** Les dates de debut et fin doivent etre saisies */
	public boolean peutPreparer() {
		return modeSaisiePossible() && controleurPeriode.dateDebut() != null && controleurPeriode.dateFin() != null;
	}
	public boolean peutExporter() {
		return displayGroup().displayedObjects().count() > 0;
	}
	public boolean selectionModifiee() {
		return peutExporter() && selectionModifiee;
	}
	public boolean elementsSelectionnes() {
		return displayGroup().selectedObjects().count() > 0;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		typeEnseignant = EtatComparatifCarriere.TOUT_TYPE_ENSEIGNANT;
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		String today = DateCtrl.dateToString(new NSTimestamp());
		controleurPeriode = new GestionPeriode(today,today,GestionPeriode.TOUTE_PERIODE);
		controleurPeriode.init();
		GraphicUtilities.swaperView(vuePeriode,controleurPeriode.component());
		loadNotifications();
	}
	protected void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserDates", new Class[] {NSNotification.class}),
				GestionPeriode.NOTIFICATION_PERIODE_HAS_CHANGED,null);
	}
	protected void afficherExceptionValidation(String message) {
		// pas d'enregistrement des données
	}
	protected boolean conditionsOKPourFetch() {
		// pas de fetch
		return false;
	}
	protected NSArray fetcherObjets() {
		// pas de fetch
		return null;
	}
	protected String messageConfirmationDestruction() {
		// pas de suppression
		return null;
	}
	/** Tri par ordre alphabetique de nom et prenom */
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("nom", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("prenom", EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
	}
	protected void traitementsApresValidation() {
		// pas d'enregistrement des données

	}
	protected boolean traitementsAvantValidation() {
		// pas d'enregistrement des données
		return false;
	}
	protected boolean traitementsPourSuppression() {
		// Pas de suppression des données
		return false;
	}
	protected void terminer() {
	}
	// Méthodes privées
	private String headerExport() {
		return "\t\t\tSituation au " + controleurPeriode.debutPeriode() + "\t\t\tSituation au " + controleurPeriode.finPeriode() + "\t\t\t\n" +
		"N° Insee\tNom\tPrénom\tGrade\tEchelon\tINM\tGrade\tEchelon\tINM\t% Service\n";
	}
	private String texteExport(EtatComparatifCarriere etat) {
		String result = "";
		if (etat.noInsee() != null) {
			result = etat.noInsee();
		}
		result += "\t" + etat.nom() + "\t" + etat.prenom() + "\t";
		if (etat.gradeDebut() != null) {
			result += etat.gradeDebut();
		}
		result += "\t";
		if (etat.echelonDebut() != null) {
			result += etat.echelonDebut();
		}
		result += "\t";
		if (etat.indiceDebut() != null) {
			result += etat.indiceDebut();
		}
		result += "\t";
		if (etat.gradeFin() != null) {
			result += etat.gradeFin();
		}
		result += "\t";
		if (etat.echelonFin() != null) {
			result += etat.echelonFin();
		}
		result += "\t";
		if (etat.indiceFin() != null) {
			result += etat.indiceFin();
		}
		result += "\t";
		if (etat.quotiteServiceFin() != null) {
			result += etat.quotiteServiceFin();
		}
		result += "\n";
		return result;
	}

	private void preparerEtatsComparatifs() {
		EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
		Class[] classeParametres = new Class[] {NSTimestamp.class,NSTimestamp.class,Integer.class,Integer.class,Boolean.class};
		Object[] parametres = new Object[] {controleurPeriode.dateDebut(),controleurPeriode.dateFin(),new Integer(typePersonnel()),new Integer(typeEnseignantPourRecherche()),new Boolean(estComparaisonDebut())};
		etatsComparatifs = (NSArray)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestPreparerEtatsComparatifs",classeParametres,parametres,false);
	}
	private int typeEnseignantPourRecherche() {
		if (typeEnseignant() == EtatComparatifCarriere.TOUT_TYPE_ENSEIGNANT) {
			return ManGUEConstantes.TOUT_PERSONNEL;
		} else if (typeEnseignant() == EtatComparatifCarriere.ENSEIGNANT) {
			return ManGUEConstantes.ENSEIGNANT;
		} else if (typeEnseignant() == EtatComparatifCarriere.NON_ENSEIGNANT) {
			return ManGUEConstantes.NON_ENSEIGNANT;
		} else {	// ne devrait pas arrivé
			return ManGUEConstantes.TOUT_PERSONNEL;
		}
	}
	
}

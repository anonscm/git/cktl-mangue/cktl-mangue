package org.cocktail.mangue.client.agents;

import java.util.Enumeration;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class AgentsFinderCtrl {

	private String 		filtreNom, filtrePrenom, filtreMatricule, filtreInsee;
	private NSArray<EOStructure>  	selectedStructures;

	private int indexSituation, indexAgent, indexType;
	private Object currentPosition;
	private NSTimestamp dateReference;
	private boolean useNomPatronymique, exclusionHeberges;
	private AgentsCtrl ctrlParent;

	public AgentsFinderCtrl(AgentsCtrl ctrlParent) {
		this.ctrlParent = ctrlParent;
	}

	public EOEditingContext getEdc() {
		return ctrlParent.getEdc();
	}

	public String getFiltreNom() {
		return filtreNom;
	}

	public String getFiltreMatricule() {
		return filtreMatricule;
	}

	public String getFiltreInsee() {
		return filtreInsee;
	}

	public void setFiltreInsee(String filtreInsee) {
		this.filtreInsee = filtreInsee;
	}

	public void setFiltreMatricule(String filtreMatricule) {
		this.filtreMatricule = filtreMatricule;
	}

	public Object getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(Object currentPosition) {
		this.currentPosition = currentPosition;
	}

	public void setFiltreNom(String filtreNom) {
		this.filtreNom = filtreNom;
	}

	public String getFiltrePrenom() {
		return filtrePrenom;
	}

	public void setFiltrePrenom(String filtrePrenom) {
		this.filtrePrenom = filtrePrenom;
	}

	public NSArray getSelectedStructures() {
		return selectedStructures;
	}
	public void setSelectedStructures(NSArray selectedStructures) {
		this.selectedStructures = selectedStructures;
	}

	public int getIndexSituation() {
		return indexSituation;
	}

	public void setIndexSituation(int indexSituation) {
		this.indexSituation = indexSituation;
	}

	public int getIndexAgent() {
		return indexAgent;
	}

	public void setIndexAgent(int indexAgent) {
		this.indexAgent = indexAgent;
	}

	public int getIndexType() {
		return indexType;
	}

	public void setIndexType(int indexType) {
		this.indexType = indexType;
	}

	public boolean isUseNomPatronymique() {
		return useNomPatronymique;
	}

	public void setUseNomPatronymique(boolean useNomPatronymique) {
		this.useNomPatronymique = useNomPatronymique;
	}

	public boolean isExclusionHeberges() {
		return exclusionHeberges;
	}

	public void setExclusionHeberges(boolean exclusionHeberges) {
		this.exclusionHeberges = exclusionHeberges;
	}

	public NSTimestamp getDateReference() {
		return dateReference;
	}

	public void setDateReference(NSTimestamp dateReference) {
		this.dateReference = dateReference;
	}

	/**
	 * 
	 * @return
	 */
	public EOIndividuIdentite getIndividu(EOIndividu individu) {
		return EOIndividuIdentite.findForId(getEdc(), individu.noIndividu());
	}

	
	/**
	 * 
	 * @return
	 */
	public NSArray<EOIndividuIdentite> getIndividus() {

		setDateReference(DateCtrl.today());

		NSMutableArray<EOIndividuIdentite> individus = new NSMutableArray<EOIndividuIdentite>(rechercherAgents());

		return individus.immutableClone();

	}

	/**
	 * 
	 * @return
	 */
	private NSArray<EOIndividuIdentite> rechercherAgents() {

		NSMutableArray<EOIndividuIdentite> individus = new NSMutableArray<EOIndividuIdentite>();

		if (!ctrlParent.isFiltreVide()) {

			// Titulaires - 
			if (getIndexSituation() == AgentsCtrl.INDEX_SITUATION_TITULAIRE 
					|| getIndexSituation() == AgentsCtrl.INDEX_TOUS) {
				ajouterIndividus(individus, getTitulaires());
			}

			// Contractuels
			if (getIndexSituation() == AgentsCtrl.INDEX_SITUATION_CONTRACTUEL 
					|| getIndexSituation() == AgentsCtrl.INDEX_TOUS) {
				ajouterIndividus(individus, getContractuels());
			}
			// Vacataires
			if (getIndexSituation() == AgentsCtrl.INDEX_SITUATION_VACATAIRES) {
				ajouterIndividus(individus, getVacataires());
			}

			// Heberges
			if (isExclusionHeberges() == false || getIndexSituation() == AgentsCtrl.INDEX_SITUATION_HEBERGES) {
				ajouterIndividus(individus, getHeberges());
			}

			// Restriction aux structures
			individus = new NSMutableArray(restreindreAffectations(individus, getSelectedStructures()));
			// Restriction aux positions
			if (getCurrentPosition() != null) {
				individus = new NSMutableArray(restreindrePositions(individus, getPositions()));
			}
		}
		else {
			ajouterIndividus(individus, getAllPersonnels());
		}

		return individus.immutableClone();

	}


	/**
	 * Gestion des contractuels
	 * @return Contractuels répondant aux criteres de recherche
	 */
	public NSArray<EOIndividuIdentite> getPositions() {
		return EOIndividuIdentite.fetchAll(getEdc(), getQualifierPosition());
	}	

	/**
	 * Gestion des contractuels
	 * @return Contractuels répondant aux criteres de recherche
	 */
	public NSArray<EOIndividuIdentite> getAllPersonnels() {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(getQualifierIdentite(""));
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuIdentite.PERSONNELS_KEY+"."+EOPersonnel.NO_DOSSIER_PERS_KEY + " >= 0", null));
		
		return EOIndividuIdentite.fetchAll(getEdc(), new EOAndQualifier(andQualifiers));
	}

	/**
	 * Gestion des contractuels
	 * @return Contractuels répondant aux criteres de recherche
	 */
	public NSArray<EOIndividuIdentite> getContractuels() {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(getQualifierIdentite(""));
		andQualifiers.addObject(getQualifierContratValidite());
		andQualifiers.addObject(getQualifierContratPeriode());
		andQualifiers.addObject(getQualifierContratTypePersonnel());

		return EOIndividuIdentite.fetchAll(getEdc(), new EOAndQualifier(andQualifiers));
	}

	private EOQualifier getQualifierContratValidite() {
		return EOQualifier.qualifierWithQualifierFormat(EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.TEM_ANNULATION_KEY + " = %@", new NSArray(CocktailConstantes.FAUX));
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getQualifierContratPeriode() {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		if (getIndexAgent() == AgentsCtrl.INDEX_AGENT_ACTUEL) {
			andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.DATE_DEBUT_KEY, getDateReference(), EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.DATE_FIN_KEY, getDateReference()));
		}
		else {
			if (getIndexAgent() == AgentsCtrl.INDEX_AGENT_ANCIEN) {
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.DATE_FIN_KEY + " < %@", new NSArray(getDateReference())));
			}			
			else {
				if (getIndexAgent() == AgentsCtrl.INDEX_AGENT_FUTUR) {
					andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.DATE_DEBUT_KEY + " > %@", new NSArray(getDateReference())));
				}
			}
		}

		return new EOAndQualifier(andQualifiers);
	}

	private EOQualifier getQualifierContratTypePersonnel() {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		if (getIndexType() != AgentsCtrl.INDEX_TOUS) {

			if (getIndexType() == AgentsCtrl.INDEX_TYPE_ENSEIGNANT) {
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.TEM_ENSEIGNANT_KEY + " =%@", new NSArray(CocktailConstantes.VRAI)));
			} else if (getIndexType() == AgentsCtrl.INDEX_TYPE_NON_ENSEIGNANT) {
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.TEM_ENSEIGNANT_KEY + " =%@", new NSArray(CocktailConstantes.FAUX)));
			}

		}

		return new EOAndQualifier(andQualifiers);
	}



	/**
	 * Gestion des titulaires
	 * @return Titulaires répondant aux criteres de recherche
	 */
	public NSArray<EOIndividuIdentite> getTitulaires() {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(getQualifierIdentite(""));
		andQualifiers.addObject(getQualifierCarriereValidite());
		if (getIndexAgent() != AgentsCtrl.INDEX_TOUS) {
			andQualifiers.addObject(getQualifierCarrierePeriode());
		}
		andQualifiers.addObject(getQualifierCarriereTypePersonnel());

		return EOIndividuIdentite.fetchAll(getEdc(), new EOAndQualifier(andQualifiers));
	}

	private EOQualifier getQualifierCarriereValidite() {
		return CocktailFinder.getQualifierEqual(EOIndividuIdentite.CARRIERES_KEY + "." + EOCarriere.TEM_VALIDE_KEY, "O");
	}
	private EOQualifier getQualifierCarrierePeriode() { 
		return CocktailFinder.getQualifierForPeriode(EOIndividuIdentite.CARRIERES_KEY + "." + EOCarriere.DATE_DEBUT_KEY, getDateReference(), EOIndividuIdentite.CARRIERES_KEY + "." + EOCarriere.DATE_FIN_KEY, getDateReference());
	}

	/**
	 * Qualifier sur la position de l'agent
	 * @return
	 */
	private EOQualifier getQualifierPosition() {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		if (getCurrentPosition() instanceof EOPosition) {

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(EOIndividuIdentite.CARRIERES_KEY + "." + EOCarriere.TEM_VALIDE_KEY, "O"));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(EOIndividuIdentite.CARRIERES_KEY + "." + EOCarriere.CHANGEMENTS_POSITION_KEY + "." + EOChangementPosition.TO_POSITION_KEY, getCurrentPosition()));

			if (getIndexAgent() == AgentsCtrl.INDEX_AGENT_ACTUEL) {
				andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(
						EOIndividuIdentite.CARRIERES_KEY + "." + EOCarriere.CHANGEMENTS_POSITION_KEY + "."+EOChangementPosition.DATE_DEBUT_KEY,
						dateReference,
						EOIndividuIdentite.CARRIERES_KEY + "." + EOCarriere.CHANGEMENTS_POSITION_KEY + "."+EOChangementPosition.DATE_FIN_KEY,
						dateReference));
			}
		}
		else {

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.TEM_ANNULATION_KEY, "N"));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.TEM_CDI_KEY, (getCurrentPosition().equals("CDI"))?"O":"N"));
			if (getIndexAgent() == AgentsCtrl.INDEX_AGENT_ACTUEL) {
				andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(
						EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.DATE_DEBUT_KEY,
						dateReference,
						EOIndividuIdentite.CONTRATS_KEY + "." + EOContrat.DATE_FIN_KEY,
						dateReference));
			}


		}

		return new EOAndQualifier(andQualifiers);
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getQualifierCarriereTypePersonnel() {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		if (getIndexType() != AgentsCtrl.INDEX_TOUS) {

			if (getIndexType() == AgentsCtrl.INDEX_TYPE_ENSEIGNANT) {
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOIndividuIdentite.CARRIERES_KEY + "." + EOCarriere.ELEMENTS_KEY + "." + EOElementCarriere.TO_CORPS_KEY + "." + EOCorps.TO_TYPE_POPULATION_KEY  + "." + EOTypePopulation.TEM_ENSEIGNANT_KEY + " =%@", new NSArray(CocktailConstantes.VRAI)));
			} else if (getIndexType() == AgentsCtrl.INDEX_TYPE_NON_ENSEIGNANT) {
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOIndividuIdentite.CARRIERES_KEY + "." + EOCarriere.ELEMENTS_KEY + "." + EOElementCarriere.TO_CORPS_KEY + "." + EOCorps.TO_TYPE_POPULATION_KEY  + "." + EOTypePopulation.TEM_ENSEIGNANT_KEY + " =%@", new NSArray(CocktailConstantes.FAUX)));
			}				
		}

		return new EOAndQualifier(andQualifiers);
	}

	/**
	 * Gestion des vacataires
	 * @return Vacataires repondant aux criteres de recherche
	 */
	public NSArray<EOIndividuIdentite> getVacataires() {

		NSArray<EOVacataires> vacataires = null;

		if (getIndexAgent() == AgentsCtrl.INDEX_AGENT_ACTUEL)
			vacataires = EOVacataires.findForStructureAndPeriode(getEdc(), getSelectedStructures(), getDateReference(), getDateReference());
		else
			vacataires = EOVacataires.findForStructureAndPeriode(getEdc(), getSelectedStructures(), null, null);

		NSMutableArray<EOIndividuIdentite> individusVacataires = new NSMutableArray<EOIndividuIdentite>((NSArray<EOIndividuIdentite>)vacataires.valueForKey(EOVacataires.TO_INDIVIDU_IDENTITE_KEY));

		// Restreindre les individus au nomPourRecherche et prenomPourRecherche
		EOQualifier.filterArrayWithQualifier(individusVacataires, getQualifierIdentite(""));

		return individusVacataires.immutableClone();

	}

	/**
	 * Gestion des heberges
	 * @return Heberges repondant aux criteres de recheche
	 */
	public NSArray<EOIndividuIdentite> getHeberges() {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(getQualifierIdentite(""));
		andQualifiers.addObject(getQualifierHebergeValidite());
		andQualifiers.addObject(getQualifierHebergePeriode());

		return EOIndividuIdentite.fetchAll(getEdc(), new EOAndQualifier(andQualifiers));
	}

	private EOQualifier getQualifierHebergeValidite() {
		return EOQualifier.qualifierWithQualifierFormat(EOIndividuIdentite.CONTRATS_HEBERGES_KEY + "." + EOContratHeberges.TEM_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI));
	}

	private EOQualifier getQualifierHebergePeriode() {
		return CocktailFinder.getQualifierForPeriode(EOIndividuIdentite.CONTRATS_HEBERGES_KEY + "." + EOContratHeberges.DATE_DEBUT_KEY, getDateReference(), EOIndividuIdentite.CONTRATS_HEBERGES_KEY + "." + EOContratHeberges.DATE_FIN_KEY, getDateReference());
	}


	/**
	 * 
	 * @param individus
	 * @param listeStructures
	 * @return
	 */
	private NSArray<EOIndividuIdentite> restreindreAffectations(NSArray<EOIndividuIdentite> individus, NSArray<EOStructure> listeStructures) {

		NSMutableArray<EOIndividuIdentite> retour = new NSMutableArray<EOIndividuIdentite>();

		if (listeStructures != null && listeStructures.size() > 0) {

			NSArray<EOIndividuIdentite> individusAffectes = getAffectations(listeStructures);
			for (EOIndividuIdentite individu : individusAffectes) {
				if (individus.containsObject(individu)) {
					retour.addObject(individu);
				}
			}

			return retour;
		}

		return individus;

	}

	/**
	 * 
	 * @param individus
	 * @param listeStructures
	 * @return
	 */
	private NSArray<EOIndividuIdentite> restreindrePositions(NSArray<EOIndividuIdentite> individus, NSArray<EOIndividuIdentite> individusFiltres) {

		NSMutableArray<EOIndividuIdentite> retour = new NSMutableArray<EOIndividuIdentite>();
		for (EOIndividuIdentite individu : individusFiltres) {
			if (individus.containsObject(individu) && !retour.containsObject(individu)) {
				retour.addObject(individu);
			}
		}

		return retour;

	}

	/**
	 * 
	 * @return
	 */
	public NSArray<EOIndividuIdentite> getAffectations(NSArray<EOStructure> listeStructures) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(getQualifierIdentite("personne."));
		andQualifiers.addObject(getQualifierAffectationValidite());
		andQualifiers.addObject(getQualifierAffectationPeriode());
		andQualifiers.addObject(getQualifierAffectationStructure(listeStructures));

		NSArray<EOAffectation> affectations = EOAffectation.fetchAll(getEdc(), new EOAndQualifier(andQualifiers));

		return (NSArray<EOIndividuIdentite>) affectations.valueForKey(EOAffectation.PERSONNE_KEY);
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getQualifierAffectationValidite() {
		return EOQualifier.qualifierWithQualifierFormat(EOAffectation.TEM_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI));
	}

	private EOQualifier getQualifierAffectationPeriode() {
		return CocktailFinder.getQualifierForPeriode(EOAffectation.DATE_DEBUT_KEY, getDateReference(), EOAffectation.DATE_FIN_KEY, getDateReference());
	}

	private EOQualifier getQualifierAffectationStructure(NSArray<EOStructure> listeStructures) {

		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

		for (EOStructure structure : listeStructures) {
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAffectation.TO_STRUCTURE_ULR_KEY + " = %@", new NSArray(structure)));
		}

		return new EOOrQualifier(orQualifiers);
	}


	/**
	 * 
	 * @param individus
	 * @param nouveauxIndividus
	 */
	private void ajouterIndividus(NSMutableArray individus,NSArray nouveauxIndividus) {
		modifierIndividus(individus,nouveauxIndividus,true);
	}

	/**
	 * 
	 * @param individus
	 * @param nouveauxIndividus
	 * @param ajouter
	 */
	private void modifierIndividus(NSMutableArray individus,NSArray nouveauxIndividus,boolean ajouter) {
		if (nouveauxIndividus != null && nouveauxIndividus.count() > 0) {
			for (Enumeration<EOIndividuIdentite> e = nouveauxIndividus.objectEnumerator();e.hasMoreElements();) {
				try {
					EOIndividuIdentite individu = e.nextElement();
					if (ajouter && !individus.containsObject(individu)) 
						individus.addObject(individu);

					if (!ajouter && individus.containsObject(individu))
						individus.removeObject(individu);

				} catch (Exception exc) {
					exc.printStackTrace();
				}
			}
		}
	}

	/**
	 * 
	 * @param individus
	 * @param nouveauxIndividus
	 */
	private void supprimerIndividus(NSMutableArray individus,NSArray nouveauxIndividus) {
		modifierIndividus(individus,nouveauxIndividus,false);
	}


	private String getFiltreLike(String value) {
		return "*" + value + "*";
	}

	/**
	 * 
	 * @param nom
	 * @param prenom
	 * @param estNomPatronymique
	 * @return
	 */
	public EOQualifier getQualifierIdentite(String relation) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(EOIndividuIdentite.TEM_VALIDE_KEY, "O"));

		if (getFiltreNom() != null) {
			orQualifiers.removeAllObjects();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + EOIndividuIdentite.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray(getFiltreLike(getFiltreNom()))));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + EOIndividuIdentite.NOM_AFFICHAGE_KEY + " caseInsensitiveLike %@", new NSArray(getFiltreLike(getFiltreNom()))));
			if (isUseNomPatronymique()) {
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + EOIndividuIdentite.NOM_PATRONYMIQUE_KEY + " caseInsensitiveLike %@", new NSArray(getFiltreLike(getFiltreNom()))));
			}
			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		if (getFiltrePrenom() != null) {
			orQualifiers.removeAllObjects();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + EOIndividuIdentite.PRENOM_KEY + " caseInsensitiveLike %@", new NSArray(getFiltreLike(getFiltrePrenom()))));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + EOIndividuIdentite.PRENOM_AFFICHAGE_KEY + " caseInsensitiveLike %@", new NSArray(getFiltreLike(getFiltrePrenom()))));

			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		if (getFiltreMatricule() != null) {
			orQualifiers.removeAllObjects();
			try {
				orQualifiers.addObject(CocktailFinder.getQualifierEqual(relation + EOIndividuIdentite.NO_INDIVIDU_KEY, new Integer(getFiltreMatricule())));
			}
			catch (Exception e) {
			}
			orQualifiers.addObject(CocktailFinder.getQualifierEqual(relation + EOIndividuIdentite.PERSONNELS_KEY+"."+EOPersonnel.NO_MATRICULE_KEY, getFiltreMatricule()));
			orQualifiers.addObject(CocktailFinder.getQualifierEqual(relation + EOIndividuIdentite.PERSONNELS_KEY+"."+EOPersonnel.NUMEN_KEY, getFiltreMatricule()));
			orQualifiers.addObject(CocktailFinder.getQualifierEqual(relation + EOIndividuIdentite.PERSONNELS_KEY+"."+EOPersonnel.NO_EPICEA_KEY, getFiltreMatricule()));

			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}


		if (getFiltreInsee() != null) {
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(relation + EOIndividuIdentite.IND_NO_INSEE_KEY, getFiltreInsee()));
		}


		return new EOAndQualifier(andQualifiers);
	}

}

package org.cocktail.mangue.client.agents;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.carrieres.CarrieresCtrl;
import org.cocktail.mangue.client.carrieres.PasseCtrl;
import org.cocktail.mangue.client.contrats.ContratsCtrl;
import org.cocktail.mangue.client.gui.AgentsView;
import org.cocktail.mangue.client.heberge.ContratsHebergeCtrl;
import org.cocktail.mangue.client.individu.FicheSituationAgentCtrl;
import org.cocktail.mangue.client.individu.GestionEtatCivil;
import org.cocktail.mangue.client.individu.infoscir.InfosRetraiteCtrl;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.mangue.client.individu.infosperso.InfosPersonnellesCtrl;
import org.cocktail.mangue.client.modalites_services.ModalitesServicesCtrl;
import org.cocktail.mangue.client.onglets.AgentsMouseListener;
import org.cocktail.mangue.client.prolongations_activite.ProlongationsActivitesCtrl;
import org.cocktail.mangue.client.vacations.ContratsVacationsCtrl;
import org.cocktail.mangue.common.modele.manager.Manager;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOPrefsPersonnel;

import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/**
 * Classe du controleur des agents
 * 
 */
public class AgentsCtrl  {

	public static int INDEX_TOUS = 0;

	public static int INDEX_TYPE_ENSEIGNANT = 1;
	public static int INDEX_TYPE_NON_ENSEIGNANT = 2;

	public static int INDEX_AGENT_ACTUEL = 1;
	public static int INDEX_AGENT_ANCIEN = 2;
	public static int INDEX_AGENT_FUTUR = 3;

	public static int INDEX_SITUATION_TITULAIRE = 1;
	public static int INDEX_SITUATION_CONTRACTUEL = 2;
	public static int INDEX_SITUATION_HEBERGES = 3;
	public static int INDEX_SITUATION_VACATAIRES = 4;

	public static String NETTOYER_CHAMPS = "NotifCleanInfos";
	/** Notification envoyee pour signaler que l'employe selectionne a change. La notification envoie
	 * un noIndividu */
	public static String CHANGER_EMPLOYE = "NotifEmployeHasChanged";
	/** Notification envoyee pour signaler qu'il faut changer la vue de recherche des employes */
	public static String CHANGER_VUE_RECHERCHE = "NotifChangerVue";
	/** Notification envoyee pour signaler qu'il faut rafraichir la liste des agents affiches */
	public static String SYNCHRONISER_INDIVIDUS = "ListeAgentsSynchroniser";


	public static String NOTIF_SET_INDIVIDU_INFOS = "NotifSetIndividuInfos";
	public static String NOTIF_SET_INDIVIDU_ABSENCES = "NotifSetIndividuAbsences";
	public static String NOTIF_SET_INDIVIDU_BUDGET = "NotifSetIndividuBudget";

	private static AgentsCtrl sharedInstance;

	private EODisplayGroup 		eod;
	private AgentsView 			myView;
	private ListenerAgent 		listenerAgent = new ListenerAgent();

	private EOIndividuIdentite 	currentIndividuIdentite;
	private EOIndividu	 		currentIndividu;

	private EOAgentPersonnel currentUtilisateur;
	private EOPrefsPersonnel currentPreferences;
	private EOStructure selectedStructure;
	private AgentsMouseListener edtListener;
	private boolean locked, exclureVacataires, selectionNouvelIndividu, exclureHeberges;
	private AgentsFinderCtrl myFinder;
	private NSMutableArray sortIdentite = new NSMutableArray();

	private String activePane;

	private MyActionFiltreListener listenerRecherche = new MyActionFiltreListener();
	private Manager manager;

	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	@SuppressWarnings("unchecked")
	public AgentsCtrl(Manager manager) {

		this.manager = manager;

		myFinder = new AgentsFinderCtrl(this);

		// Initialisation du displayGroup et de l'interface
		eod = new EODisplayGroup();
		//On passe à la vue un boolean pour l'utilisation ou non du numéro d'emploi formatté à partir du numéro d'emploi national
		myView = new AgentsView(null, true, eod);

		myView.getBtnFind().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { rechercher(); } }
				);
		myView.getBtnInfosPerso().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherInfosPerso(); } }
				);
		myView.getBtnInfosPro().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherInfosPro(); } }
				);
		myView.getBtnInfosCir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherInfosCir(); } }
				);
		myView.getBtnFicheIdentite().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherFicheIdentite(); } }
				);

		CocktailUtilities.initPopupAvecListe(myView.getPopupPosition(), EOPosition.findPositions(getEdc(), null), true, "*");
		myView.getPopupPosition().addItem("CDI");
		myView.getPopupPosition().addItem("CDD");

		myView.getMyEOTable().addListener(listenerAgent);
		edtListener = new AgentsMouseListener(getEdc(), myView.getMyEOTable());
		myView.getMyEOTable().addMouseListener(edtListener);
		myView.getMyEOTable().addMouseMotionListener(edtListener);
		myView.getPopupAgent().setSelectedIndex(INDEX_AGENT_ACTUEL);

		sortIdentite.addObject(new EOSortOrdering(EOIndividuIdentite.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
		sortIdentite.addObject(new EOSortOrdering(EOIndividuIdentite.PRENOM_KEY, EOSortOrdering.CompareAscending));

		setCurrentUtilisateur(((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());

		myView.getTfFiltreNom().addActionListener(listenerRecherche);
		myView.getTfFiltrePrenom().addActionListener(listenerRecherche);
		myView.getTfInsee().addActionListener(listenerRecherche);
		myView.getTfMatricule().addActionListener(listenerRecherche);
		myView.getPopupAgent().addActionListener(listenerRecherche);
		myView.getPopupSituation().addActionListener(listenerRecherche);
		myView.getPopupStructure().addActionListener(listenerRecherche);
		myView.getPopupType().addActionListener(listenerRecherche);
		myView.getPopupPosition().addActionListener(listenerRecherche);

		loadNotifications();

	}

	public AgentsView getView() {
		return myView;
	}

	public void setMyView(AgentsView myView) {
		this.myView = myView;
	}

	public void setLocation(int X, int Y) {
		myView.setLocation(X, Y);
	}

	public EOEditingContext getEdc() {
		return manager.getEdc();
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}

	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		setCurrentPreferences(EOPrefsPersonnel.rechercherPreferencesPourIndividu(getEdc(), currentUtilisateur.toIndividu()));
	}

	public EOPrefsPersonnel getCurrentPreferences() {
		return currentPreferences;
	}

	public void setCurrentPreferences(EOPrefsPersonnel currentPreferences) {
		this.currentPreferences = currentPreferences;
	}

	public EOStructure getSelectedStructure() {
		if (myView.getPopupStructure().getSelectedIndex() > 0) {
			return (EOStructure)myView.getPopupStructure().getSelectedItem();
		}

		return null;
	}

	public void setSelectedStructure(EOStructure selectedStructure) {
		this.selectedStructure = selectedStructure;
	}


	public boolean isFiltreVide() {

		return 
				myView.getPopupType().getSelectedIndex() == INDEX_TOUS 
				&& myView.getPopupAgent().getSelectedIndex() == INDEX_TOUS
				&& myView.getPopupSituation().getSelectedIndex() == INDEX_TOUS
				&& myView.getPopupPosition().getSelectedIndex() == INDEX_TOUS
				&& myView.getPopupStructure().getSelectedIndex() == INDEX_TOUS;

	}

	public boolean isExclureVacataires() {
		return exclureVacataires;
	}

	public void setExclureVacataires(boolean exclureVacataires) {
		this.exclureVacataires = exclureVacataires;
	}

	public boolean isSelectionNouvelIndividu() {
		return selectionNouvelIndividu;
	}

	public void setSelectionNouvelIndividu(boolean selectionNouvelIndividu) {
		this.selectionNouvelIndividu = selectionNouvelIndividu;
	}

	public boolean isExclureHeberges() {
		return exclureHeberges;
	}

	public void setExclureHeberges(boolean exclureHeberges) {
		this.exclureHeberges = exclureHeberges;
	}

	/**
	 * Permet d'acceder a cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploisCtrl.sharedInstance(edc).methode
	 * 
	 * @param edc : editingContext
	 * @return une instance de la classe
	 */
	public static AgentsCtrl sharedInstance()	{
		if (sharedInstance == null) {
			sharedInstance = new AgentsCtrl(((ApplicationClient)ApplicationClient.sharedApplication()).getManager());
		}
		return sharedInstance;
	}


	public EOIndividuIdentite getCurrentIndividuIdentite() {
		return currentIndividuIdentite;
	}
	public void setCurrentIndividuIdentite(EOIndividuIdentite currentIndividuIdentite) {

		this.currentIndividuIdentite = currentIndividuIdentite;

		if (getCurrentIndividuIdentite() != null) {

			setCurrentIndividu(EOIndividu.rechercherIndividuNoIndividu(getEdc(), getCurrentIndividuIdentite().noIndividu()));			

			edtListener.setCurrentIndividu(getCurrentIndividu());
			NSNotificationCenter.defaultCenter().postNotification(CHANGER_EMPLOYE, getCurrentIndividu().noIndividu(), null);
//			if (getActivePane().equals("CONGÉS") || getActivePane().equals("INFOS")) {
//			NSNotificationCenter.defaultCenter().postNotification(CHANGER_EMPLOYE, getCurrentIndividu().noIndividu(), null);
//			}
//			else 
//				refreshPane();
		}
		else {
			NSNotificationCenter.defaultCenter().postNotification(NETTOYER_CHAMPS, null);
		}
	}
	public String getActivePane() {
		return activePane;
	}

	public void setActivePane(String activePane) {
		this.activePane = activePane;
//		refreshPane();
	}

	/**
	 * Rafraichir seulement l'onglet selectionne
	 */
	private void refreshPane() {

		if (getActivePane() != null) {

			if (getActivePane().toUpperCase().equals("INFOS")) {
			}
			if (getActivePane().toUpperCase().equals("CONGÉS")) {
			}
			if (getActivePane().toUpperCase().equals("SYNTHESES") || getActivePane().toUpperCase().equals("Ancienneté")) {
				// Envoi d'une notification
			}

			
			if (getActivePane().toUpperCase().equals("CONTRATS")) {
				ContratsCtrl.sharedInstance(getEdc()).setCurrentIndividu(getCurrentIndividu());
			}
			if (getActivePane().toUpperCase().equals("Vacations")) {
				ContratsVacationsCtrl.sharedInstance(getEdc()).setCurrentIndividu(getCurrentIndividu());
			}
			if (getActivePane().toUpperCase().equals("Hébergé")) {
				ContratsHebergeCtrl.sharedInstance(getEdc()).setCurrentIndividu(getCurrentIndividu());
			}
			if (getActivePane().toUpperCase().equals("CARRIERE") || getActivePane().toUpperCase().equals("Segments")) {
				CarrieresCtrl.sharedInstance(getEdc()).setCurrentIndividu(getCurrentIndividu());
			}
			if (getActivePane().toUpperCase().equals("MAD")) {
				// Envoi d'une notification
			}
			if (getActivePane().toUpperCase().equals("Prolongations d'activité")) {
				ProlongationsActivitesCtrl.sharedInstance(getEdc()).setCurrentIndividu(getCurrentIndividu());
			}
			if (getActivePane().toUpperCase().equals("Promotions")) {
				// Envoi d'une notification
			}
			if (getActivePane().toUpperCase().equals("PASSÉ")) {
				PasseCtrl.sharedInstance(getEdc()).setCurrentIndividu(getCurrentIndividu());
			}
			if (getActivePane().toUpperCase().equals("MODALITES")) {
				ModalitesServicesCtrl.sharedInstance(getEdc()).setCurrentIndividu(getCurrentIndividu());
			}
			if (getActivePane().toUpperCase().equals("AFF. - OCC.")) {
				ModalitesServicesCtrl.sharedInstance(getEdc()).setCurrentIndividu(getCurrentIndividu());
			}
			if (getActivePane().toUpperCase().equals("BUDGET")) {
				// Envoi d'une notification
			}

		}

	}


	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}

	/**
	 * 
	 */
	protected void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockEcran", new Class[] {NSNotification.class}), ModelePage.LOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("delockEcran", new Class[] {NSNotification.class}),ModelePage.DELOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("selectionnerNouvelIndividu", new Class[] {NSNotification.class}), GestionEtatCivil.NOUVEL_EMPLOYE, null);
		//		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("changerVueAgents", new Class[] {NSNotification.class}), CHANGER_VUE_RECHERCHE, null);
		//		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("changerTaille", new Class[] {NSNotification.class}), ApplicationClient.CHANGER_TAILLE_FENETRE, null);
		//		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("changerCategoriePersonnel", new Class[] {NSNotification.class}), GestionPreferences.PREFERENCES_CHANGEES, null);
		//		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniserAgent", new Class[] {NSNotification.class}), GestionAgents.SYNCHRONISER_AGENT, null);
		//		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniserIndividus", new Class[] {NSNotification.class}), SYNCHRONISER_INDIVIDUS, null);
	}

	/** Notification de lock de l'ecran */
	public void lockEcran(NSNotification aNotif) {
		setLocked(true);
		updateInterface();
	} 

	/** Notification d'unlock de l'ecran */
	public void delockEcran(NSNotification aNotif) {
		setLocked(false);
		updateInterface();
	} 

	/**
	 * 
	 */
	private void cleanFiltres() {

		CocktailUtilities.removeActionListeners(myView.getPopupAgent());
		CocktailUtilities.removeActionListeners(myView.getPopupSituation());
		CocktailUtilities.removeActionListeners(myView.getPopupType());
		CocktailUtilities.removeActionListeners(myView.getPopupPosition());
		CocktailUtilities.removeActionListeners(myView.getPopupStructure());
		CocktailUtilities.removeActionListeners(myView.getCheckNomFamille());

		myView.getPopupAgent().setSelectedIndex(0);
		myView.getPopupType().setSelectedIndex(0);
		myView.getPopupSituation().setSelectedIndex(0);
		myView.getPopupPosition().setSelectedIndex(0);
		myView.getPopupStructure().setSelectedIndex(0);

		CocktailUtilities.viderTextField(myView.getTfFiltreNom());
		CocktailUtilities.viderTextField(myView.getTfFiltrePrenom());
		CocktailUtilities.viderTextField(myView.getTfMatricule());
		CocktailUtilities.viderTextField(myView.getTfInsee());

		myView.getCheckNomFamille().setSelected(false);

		myView.getCheckNomFamille().addActionListener(listenerRecherche);
		myView.getPopupAgent().addActionListener(listenerRecherche);
		myView.getPopupSituation().addActionListener(listenerRecherche);
		myView.getPopupStructure().addActionListener(listenerRecherche);
		myView.getPopupType().addActionListener(listenerRecherche);
		myView.getPopupPosition().addActionListener(listenerRecherche);

	}

	/** Selection d'un nouvel agent : le userInfo contient le nom et prenom */
	public void selectionnerNouvelIndividu(NSNotification aNotif) {

		myView.getCheckNomFamille().setSelected(false);

		cleanFiltres();

		CocktailUtilities.setTextToField(myView.getTfFiltreNom(), (String)aNotif.userInfo().objectForKey("nom"));
		CocktailUtilities.setTextToField(myView.getTfFiltrePrenom(), StringCtrl.capitalizedString((String)aNotif.userInfo().objectForKey("prenom")));

		//myView.getMyTableModel().fireTableDataChanged();

		myView.getMyEOTable().updateData();

		rechercher();

	}

	/** Selection d'un nouvel agent : le userInfo contient le nom et prenom */
	public void selectionnerNouvelIndividu(EOIndividu individu) {

		cleanFiltres();

		CocktailUtilities.setTextToField(myView.getTfFiltreNom(), individu.nomUsuel());
		CocktailUtilities.setTextToField(myView.getTfFiltrePrenom(), individu.prenom());

		eod.setObjectArray(new NSArray(myFinder.getIndividu(individu)));
		myView.getMyEOTable().updateData();

	}

	/**
	 * Ouverture de la fenetre
	 */
	public void open()	{
		initStructures();
		myView.setVisible(true);
	}

	/**
	 * 
	 */
	private void initStructures() {

		CocktailUtilities.removeActionListeners(myView.getPopupStructure());
		NSArray<EOStructure> structures;
		if (getCurrentUtilisateur().gereTouteStructure()) {
			structures = EOStructure.rechercherStructuresEtablissements(getEdc());
		}
		else {
			structures = getCurrentUtilisateur().structuresGerees();
		}
		myView.getPopupStructure().removeAllItems();
		myView.getPopupStructure().addItem("*");
		for (EOStructure structure : structures) {	
			myView.getPopupStructure().addItem(structure);
		}

		myView.getPopupStructure().addActionListener(listenerRecherche);

	}


	/**
	 * Actualisation des donnees
	 */
	protected void actualiser()	{
		CRICursor.setWaitCursor(myView);
		rechercher();
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * Qualifier de filtre automatique
	 * @return
	 */
	protected EOQualifier filterQualifier() {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		return new EOAndQualifier(qualifiers);
	}



	/** 
	 * Lancement de la recherche - Mise a jour du displaygroup par un Finder specifique
	 */
	private void rechercher() {

		if ( isLocked() ) {
			return;
		}

		CRICursor.setWaitCursor(myView);

		String nomPourRecherche = CocktailUtilities.getTextFromField(myView.getTfFiltreNom());
		String prenomPourRecherche = CocktailUtilities.getTextFromField(myView.getTfFiltrePrenom());

		if (nomPourRecherche != null) {
			nomPourRecherche = nomPourRecherche.trim();
		}
		if (prenomPourRecherche != null) {
			prenomPourRecherche = prenomPourRecherche.trim();
		}


		// Initialisation du finder
		myFinder.setFiltreNom(nomPourRecherche);
		myFinder.setFiltrePrenom(prenomPourRecherche);
		myFinder.setFiltreMatricule(CocktailUtilities.getTextFromField(myView.getTfMatricule()));
		myFinder.setFiltreInsee(CocktailUtilities.getTextFromField(myView.getTfInsee()));

		if (getSelectedStructure() != null)	
			myFinder.setSelectedStructures(new NSArray(getSelectedStructure()));
		else
			myFinder.setSelectedStructures(new NSArray());

		myFinder.setIndexAgent(myView.getPopupAgent().getSelectedIndex());
		myFinder.setIndexType(myView.getPopupType().getSelectedIndex());
		myFinder.setIndexSituation(myView.getPopupSituation().getSelectedIndex());
		myFinder.setUseNomPatronymique(myView.getCheckNomFamille().isSelected());
		myFinder.setCurrentPosition(getSelectedPosition());
		myFinder.setExclusionHeberges(getCurrentPreferences() != null 
				|| getCurrentPreferences().exclureHeberges() 
				|| myView.getPopupSituation().getSelectedIndex() == INDEX_SITUATION_VACATAIRES
				|| (myView.getPopupSituation().getSelectedIndex() != INDEX_TOUS
				|| myView.getPopupPosition().getSelectedIndex() != INDEX_TOUS
				|| myView.getPopupType().getSelectedIndex() != INDEX_TOUS));

		eod.setObjectArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(myFinder.getIndividus(), sortIdentite));
		//myView.getMyTableModel().fireTableDataChanged();

		myView.getMyEOTable().updateData();

		CocktailUtilities.setTextToLabel(myView.getLblInfos(), eod.displayedObjects().size() + ((eod.displayedObjects().size() == 1)?" Agent":" Agents"));

		CRICursor.setDefaultCursor(myView);

	}

	public boolean isLocked() {
		return locked;
	}
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	protected void updateDatas() {
	}

	/**
	 * Gestion de l'accessibilite de tous les objets graphiques de l'ecran
	 */
	protected void updateInterface() {

		myView.getBtnFicheIdentite().setEnabled(getCurrentIndividuIdentite() != null && !isLocked());
		myView.getBtnInfosPerso().setEnabled(getCurrentIndividuIdentite() != null && !isLocked());
		myView.getBtnInfosPro().setEnabled(getCurrentIndividuIdentite() != null && !isLocked());
		myView.getBtnInfosCir().setEnabled(getCurrentIndividuIdentite() != null && !isLocked());
		myView.getBtnFind().setEnabled(!isLocked());

		myView.getPopupAgent().setEnabled(!isLocked());
		myView.getPopupStructure().setEnabled(!isLocked());
		myView.getPopupSituation().setEnabled(!isLocked());
		myView.getPopupPosition().setEnabled(!isLocked());
		myView.getPopupType().setEnabled(!isLocked());

		myView.getMyEOTable().setEnabled(!isLocked());
		myView.getCheckNomFamille().setEnabled(!isLocked());
		CocktailUtilities.initTextField(myView.getTfFiltreNom(), false, !isLocked());
		CocktailUtilities.initTextField(myView.getTfFiltrePrenom(), false, !isLocked());
		CocktailUtilities.initTextField(myView.getTfInsee(), false, !isLocked());
		CocktailUtilities.initTextField(myView.getTfMatricule(), false, !isLocked());

	}

	/**
	 * Classe d'écoute pour lancer la recherche
	 * @author cpinsard
	 */
	private class MyActionFiltreListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			rechercher();
		}
	}

	/**
	 * Classe d'ecoute sur le display group.
	 * Permet de lancer des actions lors d'un clic ou double clic sur une ligne de donnees
	 * @author cpinsard
	 *
	 */
	private class ListenerAgent implements ZEOTableListener {

		public void onDbClick() {
			NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,"INFOS");
		}
		public void onSelectionChanged() {
			CRICursor.setWaitCursor(myView);
			// Mise a jour de l'objet courant
			setCurrentIndividuIdentite((EOIndividuIdentite) eod.selectedObject());
			//maj des 4 textfield du bas
			((ApplicationClient)ApplicationClient.sharedApplication()).refreshInterface(getCurrentIndividuIdentite());
			updateDatas();
			updateInterface();
			myView.setVisible(true);
			CRICursor.setDefaultCursor(myView);
		}
	}

	private void afficherFicheIdentite() {
		CRICursor.setWaitCursor(myView);
		FicheSituationAgentCtrl.sharedInstance(getEdc()).open(EOIndividu.rechercherIndividuNoIndividu(getEdc(), getCurrentIndividuIdentite().noIndividu()));
		CRICursor.setDefaultCursor(myView);				
	}
	private void afficherInfosPerso() {
		CRICursor.setWaitCursor(myView);
		InfosPersonnellesCtrl.sharedInstance(getEdc()).open(EOIndividu.rechercherIndividuNoIndividu(getEdc(), getCurrentIndividuIdentite().noIndividu()));
		CRICursor.setDefaultCursor(myView);		
	}
	private void afficherInfosPro() {
		CRICursor.setWaitCursor(myView);
		InfosComplementairesCtrl.sharedInstance(getEdc()).open(EOIndividu.rechercherIndividuNoIndividu(getEdc(), getCurrentIndividuIdentite().noIndividu()));
		CRICursor.setDefaultCursor(myView);				
	}
	private void afficherInfosCir() {
		CRICursor.setWaitCursor(myView);
		InfosRetraiteCtrl.sharedInstance().open(EOIndividu.rechercherIndividuNoIndividu(getEdc(), getCurrentIndividuIdentite().noIndividu()));
		CRICursor.setDefaultCursor(myView);						
	}

	/**
	 * 
	 * @return
	 */
	public Object getSelectedPosition() {
		return (myView.getPopupPosition().getSelectedIndex() > 0)?(Object)myView.getPopupPosition().getSelectedItem():null;
	}


	/**
	 * 
	 * @param individus
	 * @return
	 */
	private NSArray restreindre(NSArray individus) {
		NSMutableArray newIndividus = new NSMutableArray();
		for (java.util.Enumeration<EOIndividuIdentite> e = individus.objectEnumerator();e.hasMoreElements();) {
			EOIndividuIdentite individu = e.nextElement();
			if (newIndividus.containsObject(individu) == false)
				newIndividus.addObject(individu);
		}
		return newIndividus;
	}


}

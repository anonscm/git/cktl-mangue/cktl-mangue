
//ListeAgents.java
//Mangue

//Created by Christine Buttin on Thu May 19 2005.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.agents;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.component.COLabel;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.administration.GestionAgents;
import org.cocktail.mangue.client.individu.GestionEtatCivil;
import org.cocktail.mangue.client.onglets.AgentsMouseListener;
import org.cocktail.mangue.client.outils_interface.utilitaires.CategoriePersonnelPourAffichage;
import org.cocktail.mangue.client.preferences.GestionPreferences;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOMatrix;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Affiche la liste des employes. L'affichage a lieu dans une fenetre ou non.<BR>
 * Utilise deux notifications pour signaler que la selection a change : <BR>
 *    - CHANGER_EMPLOYE, envoie un seul objet (l'employe courant ou nul)<BR>
 *    - CHANGER_SELECTION : envoie un tableau d'objets. Cette notification est envoyee uniquement dans le cas ou la liste doit rester toujours active
 * (parmetre d'initialisation).<BR>
 * Le double-clic sur un individu affiche l'etat civil si aucune autre fenetre que la liste des agents n'est ouverte<BR>
 *
 * Affiche une fenetre comportant la liste des employes	
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class ListeAgents extends ModelePage {
	public EOMatrix matricePourNotes;
	public EOView vueAgents,vueAgentsGenerale,vueAgentsPourNotes,vueRecherche,vueNbAgents;
	public JComboBox popupTypeAgent, popupCategorie,popupStructures;
	public COLabel labelNbAgents;
	public JLabel labelAgents;
	private String nomPourRecherche,prenomPourRecherche;
	private boolean afficherMatrices;
	private boolean afficherAgentsAuDemarrage,preparationAffichage,supporteSelectionMultiple,toujoursActif;
	private boolean locked,estNomPatronymique;
	private boolean exclureVacataires,exclureHeberges;
	private boolean estSelectionNouvelIndividu;		// utilisé pour sélectionner des nouveaux individus lorsque l'agent n'a pas accès à tous les types de population
	private EOAgentPersonnel agent;
	private int typeAgent,categoriePersonnel;
	private EOGlobalID structureID;
	private EOStructure selectedStructure;
	/** Notification envoyee pour signaler qu'il faut effacer les donnees des champs des vues affichees */
	public static String NETTOYER_CHAMPS = "NotifCleanInfos";
	/** Notification envoyee pour signaler que l'employe selectionne a change. La notification envoie
	 * un noIndividu */
	public static String CHANGER_EMPLOYE = "NotifEmployeHasChanged";
	/** Notification envoyee pour signaler que les employes selectiones ont change. La notification envoie
	 * un tableau de noIndividu. Cette notification n'est envoyee que lorsque le controleur reste actif apres lockage des fenetres */
	public static String CHANGER_SELECTION = "NotifChangementSelection";
	/** Notification envoyee pour signaler qu'il faut changer la vue de recherche des employes */
	public static String CHANGER_VUE_RECHERCHE = "NotifChangerVue";
	/** Notification envoyee pour signaler qu'il faut rafraichir la liste des agents affiches */
	public static String SYNCHRONISER_INDIVIDUS = "ListeAgentsSynchroniser";
	private AgentsMouseListener edtListener;

	/**	Initialiser
	 * @param afficherAgentsAuDemarrage true ou false
	 * @param categoriePersonnel categorie a afficher par defaut
	 * @param structureID global id de la structure a afficher par defaut (peut etre nulle => toutes)
	 * @param afficherMatrices true ou false
	 * @param supporteSelectionMultiple true si la selection multiple est autorisee
	 * @param toujoursActif true si doit rester actif quand les fenetres sont lockees
	 * @param exclureHeberges true si les heberges ne sont pas affiches lorsqu'on liste tous les personnels
	 */
	public void initialiser(boolean afficherAgentsAuDemarrage,int categoriePersonnel,EOGlobalID structureID,boolean afficherMatrices,boolean supporteSelectionMultiple,boolean toujoursActif,boolean exclureHeberges) {
		super.initialiser(false,false,null,null,false);
		this.afficherMatrices = afficherMatrices;
		this.afficherAgentsAuDemarrage = afficherAgentsAuDemarrage;
		this.supporteSelectionMultiple = supporteSelectionMultiple;
		typeAgent = ManGUEConstantes.EMPLOYE_ACTUEL;
		this.categoriePersonnel = categoriePersonnel;
		this.structureID = structureID;
		preparationAffichage = true;
		locked = false;
		this.toujoursActif = toujoursActif;
		this.exclureHeberges = exclureHeberges;

		EOArchive.loadArchiveNamed("ListeAgents",this,"org.cocktail.mangue.client.outils_interface.interfaces",this.disposableRegistry());
	}
	public void initialiser(boolean afficherAgentsAuDemarrage,int categoriePersonnel,EOGlobalID structureID,boolean afficherMatrices,boolean supporteSelectionMultiple,boolean toujoursActif) {
		initialiser(afficherAgentsAuDemarrage,categoriePersonnel,structureID,afficherMatrices,supporteSelectionMultiple,toujoursActif,true);
	}
	/**	Initialiser sans selection multiple et avec lockage des fenetres */
	public void initialiser(boolean afficherAgentsAuDemarrage,int categoriePersonnel,EOGlobalID structureID,boolean afficherMatrices) {
		initialiser(afficherAgentsAuDemarrage,categoriePersonnel,structureID,afficherMatrices,false,false);
	}

	public boolean estNomPatronymique() {
		return estNomPatronymique;
	}
	public void setEstNomPatronymique(boolean estNomPatronymique) {
		this.estNomPatronymique = estNomPatronymique;
	}
	public int nbAgents() {
		return displayGroup().displayedObjects().count();
	}
	public boolean displayGroupShouldChangeSelection(EODisplayGroup displaygroup, NSArray newIndexes) {
		return !locked;
	}
	
	public void displayGroupDidChangeSelection(EODisplayGroup aDisplayGroup) {

		CRICursor.setWaitCursor(listeAffichage);

		if (aDisplayGroup == displayGroup()) {
			if (displayGroup().selectedObjects().count() == 0) {
				NSNotificationCenter.defaultCenter().postNotification(NETTOYER_CHAMPS, null);
			}
			else {
				if (toujoursActif) {
					NSArray selections = null;
					if (displayGroup().selectedObjects().count() > 0)						
						selections = (NSArray)displayGroup().selectedObjects().valueForKey(EOIndividu.NO_INDIVIDU_KEY);

					NSNotificationCenter.defaultCenter().postNotification(CHANGER_SELECTION, selections,null);
				} else {
					EOIndividuIdentite individu = ((EOIndividuIdentite)displayGroup().selectedObject());
					Number noIndividu = individu.noIndividu();
					edtListener.setCurrentIndividu(EOIndividu.rechercherIndividuNoIndividu(editingContext(), noIndividu));
					NSNotificationCenter.defaultCenter().postNotification(CHANGER_EMPLOYE, noIndividu,null);
				}
			}
		}

		CRICursor.setDefaultCursor(listeAffichage);

	}


	// actions
	public void rechercher()	{

		if (peutRechercher())
			afficherResultat();

	}
	/** action : choisit la categorie de personnel */
	public void choisirCategoriePersonnel() {
		LogManager.logDetail("ListeAgents - choisirCategoriePersonnel");
		if (preparationAffichage) {
			return;
		}
		// Determiner la categorie de personnel recherché
		categoriePersonnel = CategoriePersonnelPourAffichage.categoriePersonnelPour((String)popupCategorie.getSelectedItem());
		if (typeAgent != ManGUEConstantes.NON_AFFECTE) {
			exclureVacataires = false;
		}

		afficherResultat();
	}
	/** action : choisit le type d'agent (passe,present,...) */
	public void choisirTypeAgent() {

		if (preparationAffichage)
			return;

		typeAgent = popupTypeAgent.getSelectedIndex();

		if (typeAgent == ManGUEConstantes.NON_AFFECTE) {

			if (agent.gereVacataires())
				exclureVacataires = EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous exclure les vacataires ?","Oui","Non");
			else
				exclureVacataires = true;

			categoriePersonnel = constanteToutPersonnel();
			popupCategorie.setSelectedIndex(categoriePersonnel);
			popupCategorie.setEnabled(false);

		} else {	// pour ne pas que cela soit fait deux fois à cause du changement dans le popupCategorie
			exclureVacataires = false;
			popupCategorie.setEnabled(true);
			if (typeAgent == ManGUEConstantes.TOUT_EMPLOYE) {
				// Changer le popup de structure pour qu'on voit toutes structures confondues puisque non-affectes pris en compte
				popupStructures.setSelectedIndex(0);
			} else {
				CRICursor.setWaitCursor(vueAgents);
				afficherResultat();	
				CRICursor.setDefaultCursor(vueAgents);
			}
		}
	}


	public void choisirStructure() {

		if (preparationAffichage)
			return;

		selectedStructure = null;

		if (popupStructures.getSelectedItem().getClass().getName().equals(EOStructure.class.getName()))
			selectedStructure = (EOStructure)popupStructures.getSelectedItem();

		afficherResultat();

	}

	/** Notification de lock de l'ecran */
	public void lockEcran(NSNotification aNotif) {
		LogManager.logDetail("ListeAgents - lockEcran");
		if (toujoursActif == false) {
			locked = true;
			autoriserSelection(!locked);
			controllerDisplayGroup().redisplay();
		}
	} 

	/** Notification d'unlock de l'ecran */
	public void delockEcran(NSNotification aNotif) {
		LogManager.logDetail("ListeAgents - delockEcran");
		locked = false;
		autoriserSelection(!locked);
		controllerDisplayGroup().redisplay();
	} 
	// pour swaper la vue des agents et afficher une selection propre à la selection des notes
	public void changerVueAgents(NSNotification aNotif) {
		if (aNotif.object() != null && ((String)aNotif.object()).equals("VueNotes")) {
			GraphicUtilities.swaperView(vueAgents,vueAgentsPourNotes);
			vueRecherche.setVisible(false);
		} else {
			GraphicUtilities.swaperView(vueAgents,vueAgentsGenerale);
			vueRecherche.setVisible(true);
		}
		EOIndividuIdentite individu = null;
		if (displayGroup().selectedObject() != null) {
			individu = ((EOIndividuIdentite)displayGroup().selectedObject());
		}
		typeAgent = ManGUEConstantes.EMPLOYE_ACTUEL;
		popupTypeAgent.setSelectedIndex(typeAgent);
		if (individu != null) {
			displayGroup().selectObject(individu);		// la sélection est modifiée par l'appel à setSelectedIndex sur le popup
		}
	}
	/** Notification envoyee suite a un changement de taille de la fenetre */
	public void changerTaille(NSNotification aNotif) {
		changerTaille();
	}
	/** Notification envoyee suite a modification des preferences */
	public void changerCategoriePersonnel(NSNotification aNotif) {
		preparationAffichage = true;	// pour éviter les activations des actions de popups
		// Vérifier si la catégorie de personnel ou la structure a changé
		int categorie = ((ApplicationClient)EOApplication.sharedApplication()).categoriePersonnel();
		EOGlobalID globalID = ((ApplicationClient)EOApplication.sharedApplication()).prefStructureID();
		EOStructure structure = null;
		if (globalID != null) {
			structure = (EOStructure)SuperFinder.objetForGlobalIDDansEditingContext(globalID, editingContext());
		}
		boolean changement = false;
		if (categorie != this.categoriePersonnel) {
			this.categoriePersonnel = categorie;
			popupCategorie.setSelectedItem(CategoriePersonnelPourAffichage.nomCategoriePersonnel(categoriePersonnel));
			changement = true;
		}
		if (structure != selectedStructure) {
			this.selectedStructure = structure;
			popupStructures.setSelectedItem(structure);
			changement = true;
		}
		boolean aExclure = ((ApplicationClient)EOApplication.sharedApplication()).exclureHeberges();
		if (exclureHeberges != aExclure) {
			exclureHeberges = aExclure;
			changement = true;
		}
		if (changement) {
			afficherResultat();
		}
		preparationAffichage = false;
	}

	/** Selection d'un nouvel agent : le userInfo contient le nom et prenom */
	public void selectionnerNouvelIndividu(NSNotification aNotif) {

		setEstNomPatronymique(false);
		String nom = (String)aNotif.userInfo().objectForKey("nom");
		String prenom = (String)aNotif.userInfo().objectForKey("prenom");
		if (nom != null && prenom != null) {
			estSelectionNouvelIndividu = true;
			preparationAffichage = true;
			setNomPourRecherche(nom);
			setPrenomPourRecherche(prenom);
			// On affiche la liste de tous les personnels pour être sur d'avoir le personnel recherché
			selectedStructure = null;
			// 03/02/2010 - correction pour suppression de constanteHeberge car pas encore de contrat hébergé créé
			categoriePersonnel = constanteToutPersonnel();	// On sélectionne le dernier item du popup = "tous"
			/*	if (aNotif.userInfo().objectForKey("heberge") != null) {
				categoriePersonnel = constanteHeberge();
			} else {
				categoriePersonnel = constanteToutPersonnel();	// On sélectionne le dernier item du popup = "tous"
			}*/
			typeAgent = ManGUEConstantes.TOUT_EMPLOYE;	// On sélectionne le dernier item du popup = "tous"

			// afficher toutes structures
			selectedStructure = null;
			popupCategorie.setSelectedIndex(categoriePersonnel);
			popupTypeAgent.setSelectedIndex(typeAgent);
			afficherResultat();
			preparationAffichage = false;

			// selectionner le nouvel individu
			java.util.Enumeration<EOIndividuIdentite> e = displayGroup().displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOIndividuIdentite agent = e.nextElement();
				if (agent.nomUsuel().equalsIgnoreCase(nom) && agent.prenom().equalsIgnoreCase(prenom)) {
					displayGroup().selectObject(agent);
					break;
				}
			}
			displayGroup().redisplay();
			controllerDisplayGroup().redisplay();
		}
	}
	/** Notification envoyee quand dans l'administration, on change les caracteristiques de l'agent
	 * courant
	 * @return
	 */
	public void synchroniserAgent(NSNotification aNotif) {

		preparationAffichage = true;
		if (!afficherMatrices) {
			agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
			autoriserSelection(true);
			popupTypeAgent.setSelectedIndex(typeAgent);
			preparerPopupCategorie();
			// vérifier si l'utilisateur peut afficher cette catégorie de personnel
			if ((categoriePersonnel == ManGUEConstantes.ENSEIGNANT && agent.gereEnseignants() == false) ||
					(categoriePersonnel == ManGUEConstantes.NON_ENSEIGNANT && agent.gereNonEnseignants() == false) ||
					(categoriePersonnel == ManGUEConstantes.VACATAIRE && agent.gereVacataires() == false) ||
					(categoriePersonnel == ManGUEConstantes.HEBERGE && agent.gereHeberges() == false)) {
				categoriePersonnel = ManGUEConstantes.TOUT_PERSONNEL;
			}
			popupCategorie.setSelectedItem(CategoriePersonnelPourAffichage.nomCategoriePersonnel(categoriePersonnel));

			// préparer le popup de structures
			NSArray<EOStructure> structures;
			if (agent.gereTouteStructure()) {
				structures = EOStructure.rechercherStructuresEtablissements(editingContext());
			}
			else {
				structures = agent.structuresGerees();
			}

			for (EOStructure structure : structures) 	
				popupStructures.addItem(structure);

			if (structureID != null) { // un ancien choix ne correspond plus à ce qui est possible
				selectedStructure = (EOStructure)SuperFinder.objetForGlobalIDDansEditingContext(structureID,editingContext());
				if (structures.containsObject(selectedStructure) == false) {
					structureID = null;
					selectedStructure = null;
				}
				popupStructures.setSelectedItem(selectedStructure);
			}

			afficherResultat();
		}
		controllerDisplayGroup().redisplay();
		preparationAffichage = false;
	}
	/** Notification pour synchroniser la liste des individus affiches */
	public void synchroniserIndividus(NSNotification aNotif) {
		rechercher();
	}
	public boolean peutSaisir() {
		return !locked;
	}
	public boolean peutAfficherFiche() {
		return displayGroup().selectedObject() != null;
	}
	public boolean peutRechercher() {
		return peutSaisir();		
	}

	public String nomPourRecherche() {
		return nomPourRecherche;
	}
	public void setNomPourRecherche(String nomPourRecherche) {
		this.nomPourRecherche = nomPourRecherche;
	}
	public String prenomPourRecherche() {
		return prenomPourRecherche;
	}
	public void setPrenomPourRecherche(String prenomPourRecherche) {
		this.prenomPourRecherche = prenomPourRecherche;
	}
	// Autres
	public void changerTaille() {
		int tailleListe,tailleParent;
		if (((ApplicationClient)EOApplication.sharedApplication()).modeFenetre()) {
			tailleListe = 338;
			tailleParent = ApplicationClient.TAILLE_POUR_MODE_FENETRE;
		} else {
			tailleListe = 490;
			tailleParent = ApplicationClient.TAILLE_POUR_MODE_ONGLET;
		}
		changerTaille(tailleListe,tailleParent);
	}

	/**
	 * 
	 * @param tailleListe
	 * @param tailleParent
	 */
	public void changerTaille(int tailleListe,int tailleParent) {
		component().setVisible(false);
		// Changer la taille du composant
		component().setSize(component().getWidth(),tailleParent);
		listeAffichage.setSize(listeAffichage.getWidth(),tailleListe);
		// 12/12/2010 - il faut recréer toute la vue pour avoir la liste à la bonne hauteur
		component().removeAll();
		component().add(vueAgents);
		component().add(vueRecherche);
		component().add(labelNbAgents);
		component().add(labelAgents);
		component().add(listeAffichage);
		component().setVisible(true);
		component().validate();
	}
	// 15/12/2010 - on supprime la vue agents pour que la liste soit positionnée correctement
	public void preparerVuePourHierarchie() {
		component().remove(vueAgents);
	}

	// méthodes protégées
	protected void preparerFenetre() {
		GraphicUtilities.swaperView(vueAgents,vueAgentsGenerale);
		vueRecherche.setVisible(!afficherMatrices);
		GraphicUtilities.changerHauteurLigne(listeAffichage,14);
		if (!supporteSelectionMultiple)
			listeAffichage.table().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		categoriePersonnel = ManGUEConstantes.TOUT_PERSONNEL;
		edtListener = new AgentsMouseListener(editingContext(), listeAffichage.table());
		listeAffichage.table().addMouseListener(edtListener);
		listeAffichage.table().addMouseMotionListener(edtListener);

		listeAffichage.table().addMouseListener(new DoubleClickListener());
		if (!afficherMatrices) {
			agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
			autoriserSelection(true);
			popupTypeAgent.setSelectedIndex(typeAgent);
			preparerPopupCategorie();
			// vérifier si l'utilisateur peut afficher cette catégorie de personnel
			if ((categoriePersonnel == ManGUEConstantes.ENSEIGNANT && agent.gereEnseignants() == false) ||
					(categoriePersonnel == ManGUEConstantes.NON_ENSEIGNANT && agent.gereNonEnseignants() == false) ||
					(categoriePersonnel == ManGUEConstantes.VACATAIRE && agent.gereVacataires() == false) ||
					(categoriePersonnel == ManGUEConstantes.HEBERGE && agent.gereHeberges() == false)) {
				categoriePersonnel = ManGUEConstantes.TOUT_PERSONNEL;
			}
						
			popupCategorie.setSelectedItem(CategoriePersonnelPourAffichage.nomCategoriePersonnel(categoriePersonnel));
			// préparer le popup de structures
			NSArray<EOStructure> structures;
			if (agent.gereTouteStructure()) {
				structures = EOStructure.rechercherStructuresEtablissements(editingContext());
			} else {
				structures = agent.structuresGerees();
			}


			for (java.util.Enumeration<EOStructure> e = structures.objectEnumerator();e.hasMoreElements();
			popupStructures.addItem(e.nextElement()));
			if (structureID != null) {
				selectedStructure = (EOStructure)SuperFinder.objetForGlobalIDDansEditingContext(structureID,editingContext());
				popupStructures.setSelectedItem(selectedStructure);
			}


		}
		super.preparerFenetre();
		lockerFenetre(true);
		loadNotifications();
		preparationAffichage = false;
	}



	protected boolean conditionsOKPourFetch() {
		return true;
	}
	/** methode a surcharger pour retourner les objets affiches par le display group */
	protected NSArray fetcherObjets() {
		if (afficherAgentsAuDemarrage) {
			if (afficherMatrices) {
				return EOIndividuIdentite.rechercherPersonnelsDeTypes(editingContext(),categoriePersonnel,typeAgent,null,null);
			}
			else {
				return preparerIndividus();
			}
		} else {
			return null;
		}
	}
	/** methode a surcharger pour parametrer le display group (tri ou filtre) */
	protected void parametrerDisplayGroup() {}
	/** pas de saisie */
	protected  boolean traitementsAvantValidation() {
		return true;
	}
	/** pas de validation */
	protected void traitementsApresValidation() {}
	/** pas de revert */
	protected void traitementsApresRevert() {}
	/** pas de suppression */
	protected String messageConfirmationDestruction() {return "";}
	/** pas de suppression */
	protected boolean traitementsPourSuppression() {return true;}
	/** pas de validation */
	protected  void afficherExceptionValidation(String message) {}
	protected void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockEcran", new Class[] {NSNotification.class}), ModelePage.LOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("delockEcran", new Class[] {NSNotification.class}),ModelePage.DELOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("selectionnerNouvelIndividu", new Class[] {NSNotification.class}), GestionEtatCivil.NOUVEL_EMPLOYE, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("changerVueAgents", new Class[] {NSNotification.class}), CHANGER_VUE_RECHERCHE, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("changerTaille", new Class[] {NSNotification.class}), ApplicationClient.CHANGER_TAILLE_FENETRE, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("changerCategoriePersonnel", new Class[] {NSNotification.class}), GestionPreferences.PREFERENCES_CHANGEES, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniserAgent", new Class[] {NSNotification.class}), GestionAgents.SYNCHRONISER_AGENT, null);
		// 31/03/2011 - Synchronisation de la liste des individus afficher
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniserIndividus", new Class[] {NSNotification.class}), SYNCHRONISER_INDIVIDUS, null);
	}
	/** pas de traitement specifique a la fermeture de la fenetre */
	protected void terminer() {}
	// methodes privees
	private void autoriserSelection(boolean enabled) {
		popupTypeAgent.setEnabled(enabled);
		popupStructures.setEnabled(enabled);
		popupCategorie.setEnabled(enabled);
	}
	
	/**
	 * 
	 */
	private void preparerPopupCategorie() {
		popupCategorie.removeAllItems();
		for (java.util.Enumeration e = CategoriePersonnelPourAffichage.nomCategoriesPourAgent(agent).objectEnumerator();
		e.hasMoreElements();) {
			popupCategorie.addItem(e.nextElement());
		}
	}

	private void afficherResultat() {

		boolean changementSaisie = false;
		if (nomPourRecherche != null) {
			nomPourRecherche = nomPourRecherche.trim();
			if (nomPourRecherche.length() == 0) {
				nomPourRecherche = null;
				changementSaisie = true;
			} 
		}
		if (prenomPourRecherche != null) {
			prenomPourRecherche = prenomPourRecherche.trim();
			if (prenomPourRecherche.length() == 0) {
				prenomPourRecherche = null;
				changementSaisie = true;
			}
		}
		if (nomPourRecherche == null && prenomPourRecherche == null) {
			// On avait un des champs remplis avec des espaces, on ne fait rien
			if (changementSaisie)
				return;
			else {
				if (typeAgent == ManGUEConstantes.NON_AFFECTE 
						|| (categoriePersonnel == constanteToutPersonnel() && typeAgent == ManGUEConstantes.TOUT_EMPLOYE)) {
					if (!EODialogs.runConfirmOperationDialog("Attention", "Vous n'avez pas fourni de nom et de prénom, la recherche va être longue.\nVoulez-vous continuer ?", "Oui", "Non"))
						return;					
				}
			}
		}

		CRICursor.setWaitCursor(listeAffichage);

		//logInformationRequete(categoriePersonnel,typeAgent);

		modifierDisplayGroupAvecObjets(preparerIndividus());
		controllerDisplayGroup().redisplay();

		CRICursor.setDefaultCursor(listeAffichage);
	}

	public EOQualifier getQualifierIdentite(String nom, String prenom, boolean estNomPatronymique) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		if (nom != null) {
			if (estNomPatronymique) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividu.NOM_USUEL_KEY + " like '*'",null));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividu.NOM_PATRONYMIQUE_KEY + " caseInsensitiveLike %@", new NSArray(nom + "*")));
			} else {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@",new NSArray(nom + "*")));
			}
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividu.NOM_USUEL_KEY + " like '*'",null));
		}
		if (prenom != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividu.PRENOM_KEY + " caseInsensitiveLike %@",new NSArray(prenom + "*")));
		}

		return new EOAndQualifier(qualifiers);
	}


	/**
	 * Preparation des individus vacataires
	 * @return
	 */
	private NSArray<EOIndividuIdentite> preparerIndividusVacataires() {

		CRICursor.setWaitCursor(vueAgents);

		NSArray<EOVacataires> vacataires = null;

		if (typeAgent == ManGUEConstantes.EMPLOYE_ACTUEL)
			vacataires = EOVacataires.findForStructureAndPeriode(editingContext(), null, DateCtrl.today(), DateCtrl.today());
		else
			vacataires = EOVacataires.findForStructureAndPeriode(editingContext(), null, DateCtrl.today(), DateCtrl.today());

		NSMutableArray<EOIndividuIdentite> individusVacataires = new NSMutableArray<EOIndividuIdentite>((NSArray<EOIndividuIdentite>)vacataires.valueForKey(EOVacataires.TO_INDIVIDU_IDENTITE_KEY));

		// Restreindre les individus au nomPourRecherche et prenomPourRecherche
		EOQualifier.filterArrayWithQualifier(individusVacataires, getQualifierIdentite(nomPourRecherche,prenomPourRecherche,estNomPatronymique()));

		CRICursor.setDefaultCursor(vueAgents);

		return individusVacataires.immutableClone();

	}

	// prepare la liste des agents en fonction de la categorie de personnel et du type de population
	// Dans le cas où un type de population est recherché, on recherche les carrieres et contrats
	// et on ne retient que les personnels correspondant à la categorie recherchée (passe, present, futur)
	// Dans le cas où le type de population est quelconque, on recherche tous les personnels et on ne retient
	// que ceux correspondant à la categorie recherchée (passe, present, futur, non-affectes, tous)
	// On limite dans tous les cas (pour les personnels affectes) à la structure selectionnée ou aux structures
	// visibles par l'agent
	private NSArray preparerIndividus() {
		NSArray individus = null;

		// Gestion des vacataires
		if (categoriePersonnel == ManGUEConstantes.VACATAIRE) {
			individus = preparerIndividusVacataires();
		}
		else
		{
			if (typeAgent == ManGUEConstantes.NON_AFFECTE) {
				// on recherche tous les personnels, on restreint aux non-affectes
				if (agent.gereTousAgents())
					individus = EOIndividuIdentite.rechercherPersonnels(editingContext(),nomPourRecherche,prenomPourRecherche,estNomPatronymique(), categoriePersonnel);
				else // ne rechercher que les individus que peut voir cet agent
					individus = EOIndividuIdentite.rechercherPourAgentPersonnelsDeTypes(editingContext(),agent,constanteToutPersonnel(),ManGUEConstantes.TOUT_EMPLOYE,nomPourRecherche,prenomPourRecherche,null,estNomPatronymique(),exclureHeberges);

				individus = EOIndividuIdentite.restreindreIndividusNonAffectes(editingContext(),nomPourRecherche,prenomPourRecherche,estNomPatronymique(),individus);
				if (exclureVacataires)
					individus = EOIndividuIdentite.restreindreNonVacataires(editingContext(),nomPourRecherche,prenomPourRecherche,estNomPatronymique(),individus);

			} else  {
				
				NSArray structures = null;

				if (selectedStructure != null || !agent.gereTouteStructure()) {	
					if (selectedStructure != null)
						structures = new NSArray(selectedStructure);
					else 
						structures = agent.structuresGereesEtFilles();
				}

				// on recherche tous les personnels pour tous les types de population 
				// car l'agent peut tous les voir ou bien on vient de créer un individu
				if (categoriePersonnel == constanteToutPersonnel() && typeAgent == ManGUEConstantes.TOUT_EMPLOYE 
						&& (agent.gereTousAgents() || estSelectionNouvelIndividu)) {

					individus = EOIndividuIdentite.rechercherPersonnels(editingContext(), nomPourRecherche, prenomPourRecherche, estNomPatronymique(), !estSelectionNouvelIndividu && exclureHeberges, categoriePersonnel);	// dans le cas d'une création on inclut les hébergés, sinon on applique la demande de l'utilisateur
					estSelectionNouvelIndividu = false;

					// restreindre au personnel sans affectation ou affecté aux structures

					if (structures != null)
						individus = EOIndividuIdentite.restreindrePersonnelsNonAffectesOuAvecStructure(editingContext(),nomPourRecherche,prenomPourRecherche,estNomPatronymique(),individus,structures);

				} else {	

					boolean exclusionHeberges = true;
					if (categoriePersonnel == constanteToutPersonnel()) {
						exclusionHeberges = exclureHeberges;
					} else if (categoriePersonnel == ManGUEConstantes.HEBERGE) {
						exclusionHeberges = false;
					}
					// l'agent veut un certain type de personnel ou il ne peut en voir qu'un certain type ou
					// l'agent veut une certaine catégorie de personnel (passé/présent/futur)
					//	restreindre au type de personnel et à la catégorie recherchée pour les structures choisies
					individus = EOIndividuIdentite.rechercherPourAgentPersonnelsDeTypes(editingContext(), agent, categoriePersonnel, typeAgent, nomPourRecherche, prenomPourRecherche, structures,estNomPatronymique(), exclusionHeberges);
				}
			}
		}
		// Supprimer tous les doublons (peuvent se produire si des agents ont un contrat principal et
		// un segment de carrière en même temps)
		individus = restreindre(individus);

		return individus;

	}


	// lorsque les agents ne peuvent pas accéder à tous les types de personnel, le popup est limité avec moins d'items
	// le dernier item correspond toujours à "Tous"
	private int constanteToutPersonnel() {
		return popupCategorie.getItemCount() - 1;
	}

	private NSArray restreindre(NSArray individus) {
		NSMutableArray newIndividus = new NSMutableArray();
		for (java.util.Enumeration<EOIndividuIdentite> e = individus.objectEnumerator();e.hasMoreElements();) {
			EOIndividuIdentite individu = e.nextElement();
			if (newIndividus.containsObject(individu) == false)
				newIndividus.addObject(individu);
		}
		return newIndividus;
	}

	// Classe d'ecoute pour les doubleclics dans la table view  */
	public class DoubleClickListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2 && !locked && ((ApplicationClient)EOApplication.sharedApplication()).estUniquementListeEmployes()) {
				// Seule fenêtre affichée
				NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,"INFOS");
			}
			if (e.getClickCount() == 2 && !locked)
				NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,"INFOS");
		}
	}
}



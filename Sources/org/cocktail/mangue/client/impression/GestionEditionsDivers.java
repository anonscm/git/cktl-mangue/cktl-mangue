/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.impression;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.impression.EOModuleImpression;
import org.cocktail.mangue.modele.mangue.impression.EOParamModuleImpression;
import org.cocktail.mangue.modele.mangue.impression.ParametreImpression;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/** Gestion des editions dont les modules Jasper comportent des requetes SQL.<BR>
 * Avant de pouvoir imprimer, l'utilisateur doit saisir toutes les valeurs des parametres;. La saisie se fait directement dans la table
 * @author christine
 *
 */
// 06/03/2011 - Correction d'un bug dans le tri des paramètres (suppression de la commande de tri)
public class GestionEditionsDivers extends ModelePage {
	public EODisplayGroup displayGroupParametres;
	public EOTable listeParametres;
	// méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroup()) {
			afficherParametres();
			updaterDisplayGroups();
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
	// méthodes de délégation du DG
    public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
    		if (group == displayGroupParametres && key.equals("valeur")) {
    			if (currentParametre().valeurValide() == false) {
    				if (value == null) {
    					EODialogs.runErrorDialog("Erreur", "Vous devez fournir une valeur pour ce paramètre");
    				} else {
    					EODialogs.runErrorDialog("Erreur", "On attend un paramètre de type " + ParametreImpression.typeParametre(currentParametre().parametreImpression().typeParametre()));
    				}
			}
    		}	
    		updaterDisplayGroups();
    }
    // actions
    public void exportExcel() {
    	Class[] classeParametres =  new Class[] {String.class,NSDictionary.class};
		NSMutableDictionary dictionnaireParametres = new NSMutableDictionary();
		java.util.Enumeration e = displayGroupParametres.displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			ParametreImpression valeurPourParametre = (ParametreImpression)e.nextElement();
			dictionnaireParametres.setObjectForKey(valeurPourParametre.valeur(),valeurPourParametre.parametreImpression().nomParametre());
		}
		Object[] parametres = new Object[]{currentModule().nom(),dictionnaireParametres};
		try {
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerFichierExcelAvecModule",classeParametres,parametres,"Edition_" + currentModule().nom(),currentModule().descriptionModule(),ManGUEConstantes.IMPRESSION_EXCEL);
		} catch (Exception exc) {
			LogManager.logException(exc);
			EODialogs.runErrorDialog("Erreur",exc.getMessage());
		}
    }
    public void imprimer() {
		Class[] classeParametres =  new Class[] {String.class,NSDictionary.class};
	NSMutableDictionary dictionnaireParametres = new NSMutableDictionary();
	java.util.Enumeration e = displayGroupParametres.displayedObjects().objectEnumerator();
	while (e.hasMoreElements()) {
		ParametreImpression valeurPourParametre = (ParametreImpression)e.nextElement();
		dictionnaireParametres.setObjectForKey(valeurPourParametre.valeur(),valeurPourParametre.parametreImpression().nomParametre());
	}
	Object[] parametres = new Object[]{currentModule().nom(),dictionnaireParametres};
	try {
		UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerAvecModule",classeParametres,parametres,"Edition_" + currentModule().nom(),currentModule().descriptionModule());
	} catch (Exception exc) {
		LogManager.logException(exc);
		EODialogs.runErrorDialog("Erreur",exc.getMessage());
	}
}
    // méthodes du controller DG
    public boolean peutImprimer() {
	    	if (currentModule() == null) {
	    		return false;
	    	}
	    	if (currentModule().parametres().count() > 0) {
		    	java.util.Enumeration e =  displayGroupParametres.displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				ParametreImpression parametre = (ParametreImpression)e.nextElement();
				if (parametre.valeur() == null) {
					return false;
				}
			}
	    	}
		return true;
    }
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
		GraphicUtilities.changerTaillePolice(listeParametres,11);
	}
	protected boolean traitementsPourSuppression() {
		return true;		// pas de suppression
	}
	protected String messageConfirmationDestruction() {
	// pas de destruction
		return null;
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"ModuleImpression");
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("nom",EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		// pas de validation
		return true;
	}
	protected void traitementsApresValidation() {}
	protected void afficherExceptionValidation(String message) {}
	protected void terminer() {
	}
	// méthodes privées
	private void afficherParametres() {
		displayGroupParametres.setObjectArray(null);
		displayGroupParametres.setSelectedObject(null);
		if (currentModule() != null) {
			NSMutableArray valeurs = new NSMutableArray();
			java.util.Enumeration e =  currentModule().parametres().objectEnumerator();
			while (e.hasMoreElements()) {
				EOParamModuleImpression parametre = (EOParamModuleImpression)e.nextElement();
				ParametreImpression parametreAvecValeur = new ParametreImpression(parametre);
				valeurs.addObject(parametreAvecValeur);
			}
			displayGroupParametres.setObjectArray(valeurs);
		}
	}
	private EOModuleImpression currentModule() {
		return (EOModuleImpression)displayGroup().selectedObject();
	}
	private ParametreImpression currentParametre() {
		return (ParametreImpression)displayGroupParametres.selectedObject();
	}
	/** raffraichissement des display groups */
	private void updaterDisplayGroups() {
		displayGroupParametres.updateDisplayedObjects();
		controllerDisplayGroup().redisplay();
	}
}

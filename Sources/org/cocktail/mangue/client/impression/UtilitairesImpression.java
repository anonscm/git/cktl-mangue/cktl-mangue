/*
 * Created on 11 mai 2006
 *
 * Contient des utilitaires pour l'impression des données
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.impression;

import java.awt.event.WindowListener;

import javax.swing.JDialog;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

/**
 * @author christine
 *
 * Contient des utilitaires pour l'impression des donnees cote client
 */
public class UtilitairesImpression {	

	private static String SESSION_NAME = "session";
	private static String SESSION_PRINT_NAME = "session.remoteCallPrint";

	/** Declenche la methode d'impression passe en parametre sur le serveur avec retour immediat (utilisation
	 * de threads) en affichant un dialogue d'impression. La methode invoquee sur le serveur ne doit rien retourner. <BR>
	 * Lance une exception si le declenchement de l'impression s'est mal passe
	 * @param editingContext
	 * @param nomMethode nom de la methode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des parametres
	 * @param objets	tableau contenant les parametres la methode d'impression
	 * @param typeImpression (fichier pdf ou fichier Excel)
	 * 
	 */
	public static void imprimerAvecDialogue(EOEditingContext editingContext,String nomMethode,
			Class[] classeParametres,
			Object[] parametres,
			String nomFichierPdf,
			String titreFenetre,
			int typeImpression) 
					throws Exception {

		GestionImpression controleur = new GestionImpression(editingContext);
		EODialogController.runControllerInNewDialog(controleur,titreFenetre);

		controleur.preparerPourTraitementAsynchrone();
		((JDialog)((EODialogController)controleur.supercontroller()).window()).setModal(true);	// pour le cas où d'autres dialogues masqueraient la fenêtre
		WindowListener[] listeners = ((EODialogController)controleur.supercontroller()).window().getWindowListeners();
		for (int i = 0; i < listeners.length;i++)
			((EODialogController)controleur.supercontroller()).window().removeWindowListener((WindowListener)listeners[i]);

		controleur.lancerImpression(nomMethode,classeParametres,parametres,nomFichierPdf,"Préparation...",typeImpression);

	}


	public static void imprimerSansDialogue(EOEditingContext editingContext,String nomMethode,
			Class[] classeParametres,
			Object[] parametres,
			String nomFichierPdf,
			String titreFenetre,
			int typeImpression) 
					throws Exception {

		GestionImpression controleur = new GestionImpression(editingContext);

		controleur.preparerPourTraitementAsynchrone();
		controleur.lancerImpressionSansDialogue(nomMethode,classeParametres,parametres,nomFichierPdf,"Préparation...",typeImpression);
	}



	/** Declenche la methode d'impression passee en parametre sur le serveur avec retour immediat (utilisation
	 * de threads) en affichant un dialogue d'impression. La methode invoquee sur le serveur ne doit rien retourner. <BR>
	 * Lance une exception si le declenchement de l'impression s'est mal passe.
	 * Genere un fichier pdf
	 * @param editingContext
	 * @param nomMethode nom de la methode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des parametres
	 * @param objets	tableau contenant les parametres la methode d'impression
	 * 
	 */
	public static void imprimerAvecDialogue(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres,String nomFichierPdf,String titreFenetre) throws Exception {
		imprimerAvecDialogue(editingContext, nomMethode, classeParametres, parametres, nomFichierPdf, titreFenetre,ManGUEConstantes.IMPRESSION_PDF);
	}
	public static void imprimerSansDialogue(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres,String nomFichierPdf,String titreFenetre) throws Exception {
		imprimerSansDialogue(editingContext, nomMethode, classeParametres, parametres, nomFichierPdf, titreFenetre,ManGUEConstantes.IMPRESSION_PDF);
	}
	/** Declenche la methode d'impression passee en parametre sur le serveur avec retour immediat (utilisation
	 * de threads). La methode invoquee sur le serveur ne doit rien retourner. Cette methode est a utiliser lorsqu'on
	 * ne veut pas passer par le dialogue d'impression<BR>
	 * Lance une exception si le declenchement de l'impression s'est mal passe
	 * @param editingContext
	 * @param nomMethode nom de la methode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des parametres
	 * @param objets tableau contenant les parametres la methode d'impression
	 * 
	 */
	public static void preparerPdfAvecMethode(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres) throws Exception {
		try {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
			Boolean result = (Boolean)store.invokeRemoteMethodWithKeyPath(editingContext, SESSION_NAME, nomMethode, classeParametres, parametres, true);
			if (result == null || result.booleanValue() == false) {
				throw new Exception("Erreur lors du lancement de l'impression");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	/** Declenche la methode d'impression passe en parametre sur le serveur avec retour immediat (utilisation
	 * de threads). La methode invoquee sur le serveur ne doit rien retourner. Cette methode est a utiliser lorsqu'on
	 * ne veut pas passer par le dialogue d'impression<BR>
	 * Lance une exception si le declenchement de l'impression s'est mal passe
	 * @param editingContext
	 * @param nomMethode nom de la methode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des parametres
	 * @param objets	tableau contenant les parametres la methode d'impression
	 * 
	 */
	public static void preparerFichierExcelAvecMethode(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres) throws Exception {

		try {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
			Boolean result = new Boolean (false);

			result = (Boolean)store.invokeRemoteMethodWithKeyPath(editingContext, SESSION_NAME, nomMethode, classeParametres, parametres, true);

			if (result.booleanValue() == false) {
				throw new Exception("Erreur lors du lancement de l'impression");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	/** Declenche la methode d'impression passe en parametre sur le serveur. N'utilise pas de threads. La methode
	 * invoquee sur le serveur doit retourner un dictionnaire contenant les data (cle: data, valeur : NSData)
	 * @param editingContext
	 * @param nomMethode nom de la methode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des parametres
	 * @param objets	tableau contenant les parametres la methode d'impression
	 * @param nomFichierPdf nom du fichier Pdf genere sur le poste client (sans extension .pdf)
	 */
	public static void afficherPdfAvecMethode(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres,String nomFichierPdf) throws Exception {
		LogManager.logDetail("afficherPdfAvecMethode : " + nomMethode);
		try {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
			NSDictionary dictionary = (NSDictionary)store.invokeRemoteMethodWithKeyPath(editingContext, SESSION_NAME,nomMethode,classeParametres,parametres,true);
			NSData datas = (NSData)dictionary.objectForKey("data");
			System.out
			.println("UtilitairesImpression.afficherPdfAvecMethode() " + datas.length());
			if (datas == null) {
				throw new Exception("Erreur lors de la génération du pdf de " + nomFichierPdf);
			} else {
				afficherPdf(datas,nomFichierPdf);
			}
		} catch (Exception e) {
			LogManager.logErreur("afficherPdfAvecMethode - Erreur lors de la génération du pdf de " + nomFichierPdf);
			throw e;
		}
	}
	/** Declenche la methode d'impression passe en parametre sur le serveur. N'utilise pas de threads. La methode
	 * invoquee sur le serveur doit retourner un dictionnaire contenant les data (cle : data, valeur : NSData)
	 * @param editingContext
	 * @param nomMethode nom de la methode d'impression sur le serveur
	 * @param classeParametres tableau contenant la classe des parametres
	 * @param objets	tableau contenant les parametres la methode d'impression
	 * @param nomFichierExcel nom du fichier Excel genere sur le poste client (sans extension .pdf)
	 */
	public static void afficherFichierExcelAvecMethode(EOEditingContext editingContext,String nomMethode,Class[] classeParametres,Object[] parametres,String nomFichierExcel) throws Exception {
		LogManager.logDetail("afficherPdfAvecMethode : " + nomMethode);
		try {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
			NSDictionary dictionary = (NSDictionary)store.invokeRemoteMethodWithKeyPath(editingContext, SESSION_NAME,nomMethode,classeParametres,parametres,true);
			NSData datas = (NSData)dictionary.objectForKey("data");
			if (datas == null) {
				throw new Exception("Erreur lors de la génération du fichier Excel de " + nomFichierExcel);
			} else {
				afficherFichierExcel(datas,nomFichierExcel);
			}
		} catch (Exception e) {
			LogManager.logErreur("afficherFichierExcelAvecMethode - Erreur lors de la génération du pdf de " + nomFichierExcel);
			throw e;
		}
	}
	/** affiche des datas qui contiennent du pdf
	 * @param datas
	 * @param fileName nom du fichier Pdf sans extension
	 */
	public static void afficherPdf(NSData datas, String fileName)	{
		afficherFichier(datas,null,fileName,"pdf");
	}
	/** affiche des datas qui contiennent des donnees pour Excel
	 * @param datas
	 * @param fileName nom du fichier excel sans extension
	 */
	public static void afficherFichierExcel(NSData datas, String fileName)	{
		afficherFichier(datas,null,fileName,"xls");
	}
	/** affiche des donnees a exporter au format tab-tab-return
	 * @param texte texte a exporter
	 * @param dir directory export
	 * @param fileName nom du fichier sans extension
	 */
	public static void afficherFichierExport(String texte, String dir,String fileName)	{
		NSData data = new NSData(texte,"ISO-8859-1");
		afficherFichier(data,dir,fileName,"xls");
	}
	/** affiche des donnees a exporter au format tab-tab-return
	 * @param texte texte a exporter
	 * @param fileName nom du fichier sans extension
	 */
	public static void afficherFichierExport(String texte, String fileName)	{
		afficherFichierExport(texte,null,fileName);
	}
	// méthodes privées
	private static void afficherFichier(NSData datas, String dir,String fileName,String extension) {
		String cheminDir = ((ApplicationClient)EOApplication.sharedApplication()).directoryImpression();

		if (dir != null)
			cheminDir = dir;

		UtilitairesFichier.afficherFichier(datas, cheminDir, fileName, extension, true);
	}
}

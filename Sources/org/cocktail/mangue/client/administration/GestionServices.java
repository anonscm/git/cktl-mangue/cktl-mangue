/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import org.cocktail.client.components.DialogueSimpleSansFetch;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.component.COTreeView;
import org.cocktail.component.utilities.CRITreeView;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EONaf;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EOTypeGroupe;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EOTypeStructure;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOGestionnaires;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Cette classe permet d'ajouter/modifier/supprimer des structures de type service dans l'arborescence des services */
public class GestionServices extends ModelePageComplete {

	public COTreeView treeView;
	public JComboBox popupTypesStructure;
	public JCheckBox checkServiceGestionnaire;
	private NSArray<EOTypeStructure> typesStructure;
	private EOStructure structurePere;
	private boolean isPreparingPopup;

	public  GestionServices() {
		super();
		isPreparingPopup = true;	// pour éviter des stacks overflow pendant le chargement de l'archive
	}

	/** Pour gerer les autorisations sur l'arbre et le popup */
	public void setModificationEnCours(boolean aBool) {
		if (aBool) {
			// On n'autorise pas le changement de type structure pour l'établissement
			popupTypesStructure.setEnabled(currentStructure() != currentStructure().toStructurePere());
		} else {
			popupTypesStructure.setEnabled(false);
		}
		treeView.setEnabled(!aBool);
		super.setModificationEnCours(aBool);
	}

	// Actions
	public void afficherRne() {
		EORne rne = (EORne)UAISelectCtrl.sharedInstance(editingContext()).getObject();
		if (rne != null)
			currentStructure().setRneRelationship(rne);
	}

	/**
	 * 
	 * @return
	 */
	public boolean isServiceGestionnaire() {

		if (currentStructure() != null) {

			NSArray<EORepartTypeGroupe> typesGroupe = EORepartTypeGroupe.findForStructure(editingContext(), currentStructure());	
			if ( ((NSArray<String>)typesGroupe.valueForKey(EORepartTypeGroupe.TGRP_CODE_KEY)).containsObject(EOTypeGroupe.TYPE_GROUPE_SERVICE_GESTIONNAIRE)) {
				return true;
			}

		}

		return false;
	}

	/**
	 * 
	 */
	public void supprimerRne() {
		currentStructure().setRneRelationship(null);
	}

	/**
	 * 
	 */
	public void afficherNaf() {
		UtilitairesDialogue.afficherDialogue(this, EONaf.ENTITY_NAME, "getNaf",true,null,false);
	}

	/**
	 * 
	 */
	public void supprimerNaf() {
		currentStructure().setNafRelationship(null);
	}

	/**
	 * 
	 */
	public void afficherResponsable() {
		DialogueSimpleSansFetch controleur = new DialogueSimpleSansFetch(EOIndividu.ENTITY_NAME,"GestionService_SelectionIndividu", EOIndividu.NOM_USUEL_KEY, "Sélection d'un responsable", false, true, false, false, false);
		controleur.init();
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOIndividu.TEM_VALIDE_KEY + " = 'O' AND personnels.noDossierPers <> nil",null);
		controleur.setQualifier(qualifier);
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getResponsable",new Class[] {NSNotification.class}),"GestionService_SelectionIndividu",null);
		controleur.afficherFenetre();
	}

	/**
	 * 
	 */
	public void imprimerServices() {
		LogManager.logDetail("Impression Services");
		try {
			Class[] classeParametres =  new Class[] {EOGlobalID.class};
			Object[] parametres = new Object[]{editingContext().globalIDForObject(currentStructure())};
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerServices",classeParametres,parametres,"Organigramme_Services","Impression des services");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	public void popupHasChanged() {
		if (isPreparingPopup || currentStructure() == null) {
			return;
		}
		String libelleType = (String)popupTypesStructure.getSelectedItem();
		java.util.Enumeration e = typesStructure.objectEnumerator();
		while (e.hasMoreElements()) {
			EOGenericRecord type = (EOGenericRecord)e.nextElement();
			if (libelleType.equals(type.valueForKey(INomenclature.LIBELLE_LONG_KEY))) {
				currentStructure().setCTypeStructure((String)type.valueForKey(INomenclature.CODE_KEY));
			}
		}
	}
	// Notifications
	public void selectionnerService(NSNotification aNotif) {

		NSDictionary result = (NSDictionary)aNotif.userInfo();
		EOStructure currentStructure = (EOStructure)result.objectForKey("selectedRecord");

		NSMutableArray structures = new NSMutableArray();
		structures.addObject(currentStructure);
		structures.addObject(currentStructure.structuresFils());
		displayGroup().setObjectArray(structures);
		displayGroup().setSelectedObject(currentStructure);
		structurePere = currentStructure;
		preparerPopupStructure();

		checkServiceGestionnaire.setSelected(isServiceGestionnaire());
		updaterDisplayGroups();
	}

	/**
	 * 
	 * @param aNotif
	 */
	public void modifierService(NSNotification aNotif) {
		selectionnerService(aNotif);
		if (boutonModificationAutorise()) {
			modifier();
		}
	}

	/**
	 * 
	 * @param aNotif
	 */
	public void getResponsable(NSNotification aNotif) {
		ajouterRelation(currentStructure(), aNotif.object(), EOStructure.RESPONSABLE_KEY);
	}
	public void getNaf(NSNotification aNotif) {
		ajouterRelation(currentStructure(), aNotif.object(), EOStructure.NAF_KEY);
	}
	/** Le libelle long et le type de structure sont obligatoires<BR>
	 * Pour une structure de l'etablissement le owner et le responsable sont obligatoires.
	 */
	public boolean peutValider() {
		return modificationEnCours() && currentStructure() != null && currentStructure().llStructure() != null && 
				currentStructure().cTypeStructure() != null && currentStructure().responsable() != null;
	}
	public boolean peutSupprimerRne() {
		return modificationEnCours() && currentStructure().rne() != null;
	}
	public boolean peutSupprimerNaf() {
		return modificationEnCours() && currentStructure().naf() != null;
	}
	/** On peut modifier tous les types de structure sauf l'établissement */
	public boolean peutModifierTypeStructure() {
		return modificationEnCours() && currentStructure() != structurePere;
	}

	// Méthodes protégées
	protected void preparerFenetre() {
		isPreparingPopup = true;
		super.preparerFenetre();
		preparerTypesStructure();	// Il faut préparer les types de structure avant de créer le tree view à cause des clicks du tree view

		checkServiceGestionnaire.setVisible(EOGrhumParametres.isUseGestionnaire());

		initTreeView();
		isPreparingPopup = false;
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("selectionnerService",new Class[] {NSNotification.class}), CRITreeView.TREE_VIEW_DID_CLICK,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("modifierService",new Class[] {NSNotification.class}), CRITreeView.TREE_VIEW_DID_DOUBLE_CLICK,null);		// Chargement de l'organigramme
	}
	protected void traitementsPourCreation() {
		currentStructure().initAvecParent(structurePere);
		EOAgentPersonnel personnel = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(), editingContext());
		currentStructure().addObjectToBothSidesOfRelationshipWithKey(personnel.toIndividu(), "owner");
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR", message);
	}
	protected boolean conditionsOKPourFetch() {
		EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
		return agent.peutAdministrer();
	}
	protected NSArray fetcherObjets() {
		return new NSArray(EOStructure.rechercherEtablissement(editingContext()));
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce service ?";
	}
	protected void parametrerDisplayGroup() {
	}
	protected boolean traitementsAvantValidation() {

		if (EOGrhumParametres.isUseGestionnaire()) {

			EOTypeGroupe typeGroupeGestionnaire = EOTypeGroupe.getTypeGroupeServiceGestionnaire(editingContext());

			if (checkServiceGestionnaire.isSelected()) {
				EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
				EORepartTypeGroupe.creer(editingContext(), currentStructure(), typeGroupeGestionnaire, agent);
			}
			else {

				if (typeGroupeGestionnaire != null) {

					EORepartTypeGroupe repart = EORepartTypeGroupe.rechercherPourTypeEtStructure(editingContext(), typeGroupeGestionnaire , currentStructure());

					if (repart != null) {
						NSArray<EOGestionnaires> structuresGestionnaires = EOGestionnaires.findForStructure(editingContext(), currentStructure());

						if (structuresGestionnaires.size() > 0) {

							if(EODialogs.runConfirmOperationDialog("Attention", 
									structuresGestionnaires.size() + " agents sont associés à cette structure. Voulez-vous continuer ?", "Oui", "Non")){

								for (EOGestionnaires gestionnaire : structuresGestionnaires) {
									editingContext().deleteObject(gestionnaire);
								}

							}
						}

						editingContext().deleteObject(repart);
					}				
				}
			}
		}

		return true;
	}

	/** Creation de toutes les donnees liees aux structures (repartTypeGroupe, repartPersonneAdresse, Secretariat)
	 * si elles n'existent pas, une fois la structure reellement dans la base (probleme des contraintes Oracle
	 * qui exigent l'existence de la structure pour ces donn&eacute;es)
	 */
	protected void traitementsApresValidation() {
		// On est obligé de préparer les repartPersonneAdresse et les secrétariats à ce stade pour être sûr de ne pas tomber 
		// dans les contraintes d'Oracle. On le fait qu'on soit en création ou en modification au cas où ils n'existeraient pas
		try {
			boolean rtgCreees = currentStructure().preparerRepartTypeGroupes();
			boolean rpaCreee = currentStructure().preparerRepartPersonneAdresse();
			if (rtgCreees || rpaCreee) {
				editingContext().saveChanges();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		treeView.updateAndSelect(currentStructure());
		super.traitementsApresValidation();
	}
	/** Pas de suppression */
	protected boolean traitementsPourSuppression() {
		return false;
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOStructure currentStructure() {
		return (EOStructure)displayGroup().selectedObject();
	}
	private void initTreeView() {
		try {
			treeView.initialize(editingContext(),EOStructure.ENTITY_NAME,true,true,false);
		} catch (Exception e) {
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite : " + e.getMessage());
		}
	}

	/**
	 * 
	 */
	private void preparerPopupStructure() {
		isPreparingPopup = true;
		popupTypesStructure.removeAllItems();
		if (currentStructure() != null) {
			int indexDebut = 1;
			if (currentStructure() == currentStructure().toStructurePere()) {
				indexDebut = 0;	
			}
			for (int i = indexDebut;i < typesStructure.count();i++) {
				EOGenericRecord typeStructure = (EOGenericRecord)typesStructure.objectAtIndex(i);
				popupTypesStructure.addItem(typeStructure.valueForKey(INomenclature.LIBELLE_LONG_KEY));
				if (currentStructure().cTypeStructure().equals(typeStructure.valueForKey(INomenclature.CODE_KEY))) {
					popupTypesStructure.setSelectedItem(typeStructure.valueForKey(INomenclature.LIBELLE_LONG_KEY));
				}
			}
		}
		isPreparingPopup = false;
	}

	/**
	 * 
	 */
	private void preparerTypesStructure() {

		try {

			NSArray<EOTypeStructure> types = EOTypeStructure.fetchAll(editingContext());
			EOGenericRecord result[] = new EOGenericRecord[5];

			for (EOTypeStructure type : types) {

				if (type.estEtablissement())
					result[0] = type;
				else	if (type.estEtablissementSecondaire())
					result[1] = type;
				else if (type.estComposante())
					result[2] = type;
				else if (type.estCompposanteStatutaire())
					result[3] = type;
				else if (type.estAutre())
					result[4] = type;

			}

			typesStructure = new NSArray(result);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}

/*
 * Created on 13 avr. 2006
 *
 * Gestion des familles professionnelles
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.lolf.EOEmploiType;
import org.cocktail.mangue.modele.mangue.lolf.EOFamilleProfessionnelle;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;


/** Gestion des familles professionnelles<BR>
 * @author christine
 *
 */
// 19/10/2010 - Adaptation Netbeans
public class GestionFamillesProfessionnelles extends ModelePageComplete {

	// actions
	public void afficherDomaines() {
		UtilitairesDialogue.afficherDialogue(this,"DomaineCompetence","getDomaine",true,null,true);
	}
	// Notifications
	public void getDomaine(NSNotification aNotif) {
		ajouterRelation(currentFamille(),aNotif.object(),"toDomaineCompetence");
	}
	// méthodes du controller DG
	 /** Nomenclature non modifiable */
	public boolean conditionSurPageOK() {
		return false;
	}
	public boolean peutAjouter() {
		return !modificationEnCours() && conditionSurPageOK();
	}
	public boolean peutSupprimer() {
		
		NSArray emploisTypes = EOEmploiType.rechercherPourFamille(editingContext(), currentFamille());
		
		return boutonModificationAutorise() & emploisTypes.count() == 0;
	}
	// méthodes protégées
	protected void traitementsPourCreation() {
	}
	protected boolean traitementsPourSuppression() {
		// on ne peut supprimer que si la relation aux emplois type n'existe plus
		deleteSelectedObjects();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette famille professionnelle ?";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"FamilleProfessionnelle");
	}

	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("fprLibelle",EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}

	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOFamilleProfessionnelle currentFamille() {
		return (EOFamilleProfessionnelle)displayGroup().selectedObject();
	}
}

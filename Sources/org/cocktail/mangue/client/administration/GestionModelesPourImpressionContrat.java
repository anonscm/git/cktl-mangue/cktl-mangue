/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.AideUtilisateur;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.impression.EOContratImpression;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;

/** Gestion des mod&egraves;les d'impression pour les contrats<BR>
 * Les fichiers de mod&egrave;les doivent &ecirc;tre rang&eacute;s dans le directory des modules d'impression/Contrats. Ce sont des fichiers
 * Jasper dont les donn&eacute;es sont remplies &agrave; partir de requ&ecirc;tes SQL et admettant en param&egrave;tres d'entr&eacute;e : le noIndividu, le noContrat et
 * un num&acute;ro automatique form&eacute; comme les num&eacute;ros d'arr&ecirc;t&eacute; &agrave; partir de l'ann&eacute;e courante.
 * @author christine
 *
 */
public class GestionModelesPourImpressionContrat extends ModelePageComplete {

	// Accesseurs
	public String compteurDescription() {
		if (currentContratImpression() == null || currentContratImpression().cimpDescription() == null) {
			return "";
		} else {
			return "" + currentContratImpression().cimpDescription().length() + "/" + EOContratImpression.LG_DESCRIPTION;
		}
	}
	// Actions
	/** Affiche le dialogue de s&eacute;lection des types de contrat de travail ne comportant que les types de contrat qui ne sont
	 * pas encore param&eacute;tr&eacute;s
	 */
	public void afficherTypeContrat() {
		NSMutableArray qualifiers = new NSMutableArray();
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOContratImpression contrat = (EOContratImpression)e.nextElement();
			if (contrat.typeContratTravail() != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("code != %@", new NSArray(contrat.typeContratTravail().code())));
			}
		}
		UtilitairesDialogue.afficherDialogue(this, "TypeContratTravail", "getTypeContrat", false, new EOAndQualifier(qualifiers), false);
	}
	public void afficherAide() {
		AideUtilisateur controleur = new AideUtilisateur("AideModelesPourContrat",null);
		controleur.afficherFenetre();
	}
	// Notifications
	public void getTypeContrat(NSNotification aNotif) {
		ajouterRelation(currentContratImpression(), aNotif.object(), "typeContratTravail");
	}
	// Méthodes du controller DG
	public boolean peutValider() {
		return super.peutValider() && currentContratImpression().cimpFichier() != null && currentContratImpression().typeContratTravail() != null;
	}
	// Méthodes protégées
	protected void traitementsPourCreation() {
		// Pas de traitement spécifique
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur", message);
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(), "ContratImpression");
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette entrée";
	}
	/** Tri par code ascendant */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("typeContratTravail.code", EOSortOrdering.CompareAscending)));
	}
	protected void terminer() {
	}
	/** Pas de traitement sp&eacute;ficique */
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected boolean traitementsPourSuppression() {
		currentContratImpression().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	// Méthodes privées
	private EOContratImpression currentContratImpression() {
		return (EOContratImpression)displayGroup().selectedObject();
	}
}

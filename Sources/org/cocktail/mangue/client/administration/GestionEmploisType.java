/*
 * Created on 13 avr. 2006
 *
 * Gestion des emplois type
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import java.awt.Window;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.lolf.EOEmploiType;
import org.cocktail.mangue.modele.mangue.lolf.EORepartEmploiActivite;
import org.cocktail.mangue.modele.mangue.lolf.EORepartEmploiCompetence;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;


/** Gestion des emplois type<BR>
 * Une fois un emploi-type cr&eacute;&eacute; avec un code, on ne peut changer ce dernier, il faut alors supprimer l'emploi-type et en recr&eacute;er un nouveau<BR>
 * @author christine
 *
 */
// 18/10/2010 - Adaptation Netbeans
public class GestionEmploisType extends ModelePageComplete {
	public EOTable listeActivites,listeCompetences;
	public EODisplayGroup displayGroupActivites,displayGroupCompetences;
	private AffichageLibelle controleurPourInfo;
	
	public void terminer() {
		if (controleurPourInfo != null) {
			controleurPourInfo.fermerFenetre();
		}
	}
	// méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroupActivites) {
			modifierLibelle(true);
		} else if (aGroup == displayGroupCompetences) {
			modifierLibelle(false);
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
	// actions
	public void afficherFamilles() {
		UtilitairesDialogue.afficherDialogue(this,"FamilleProfessionnelle","getFamille",false,null,true);
	}
	public void ajouterActivite() {
		UtilitairesDialogue.afficherDialogue(this,"Activite","getActivite",true,null,true);
	}
	public void supprimerActivite() {
		currentEmploi().removeFromTosRepartEmploiActiviteRelationship(currentRepartActivite());
		editingContext().deleteObject(currentRepartActivite());
		displayGroupActivites.updateDisplayedObjects();
	}
	public void afficherActivite() {
		afficherLibelle(true);
	}
	public void ajouterCompetence() {
		UtilitairesDialogue.afficherDialogue(this,"Competence","getCompetence",true,null,true);
	}
	public void supprimerCompetence() {
		currentEmploi().removeFromTosRepartEmploiCompetenceRelationship(currentRepartCompetence());
		editingContext().deleteObject(currentRepartCompetence());
		displayGroupCompetences.updateDisplayedObjects();
	}
	public void afficherCompetence() {
		afficherLibelle(false);
	}
	// Notifications
	public void getFamille(NSNotification aNotif) {
		ajouterRelation(currentEmploi(),aNotif.object(),"toFamilleProfessionnelle");
	}
	public void getActivite(NSNotification aNotif) {
//		if (aNotif.object() != null) {
//			EOActivite activite = (EOActivite)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
//			if (((NSArray)displayGroupActivites.displayedObjects().valueForKey("toActivite")).containsObject(activite) == false) {
//				displayGroupActivites.insert();
//				currentRepartActivite().initAvecActiviteEtEmploi(activite,currentEmploi());
//				modifierLibelle(true);
//			}
//		}
	}
	public void getCompetence(NSNotification aNotif) {
//		if (aNotif.object() != null) {
//			EOCompetence competence = (EOCompetence)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
//			if (((NSArray)displayGroupCompetences.displayedObjects().valueForKey("toCompetence")).containsObject(competence) == false) {
//				displayGroupCompetences.insert();
//				currentRepartCompetence().initAvecCompetenceEtEmploi(competence,currentEmploi());
//			//	displayGroupCompetences.updateDisplayedObjects();
//				modifierLibelle(false);
//			}
//		}
	}
	// méthodes du controller DG
	public String longueurDefinition() {
		if (currentEmploi() != null && currentEmploi().etyDefinition() != null) {
			return "" + currentEmploi().etyDefinition().length() + "/" + EOEmploiType.LONGUEUR_DEFINITION;
		} else {
			return "";
		}
	}
	 /** Nomenclature non modifiable */
	public boolean conditionSurPageOK() {
		return false;
	}
	public boolean peutAjouter() {
		return !modificationEnCours() && conditionSurPageOK();
	}
	public boolean peutChangerCode() {
		return modeCreation() && modificationEnCours();
	}
	public boolean peutValider() {
		return super.peutValider() && currentEmploi() != null && currentEmploi().etyCode() != null;
	}
	public boolean peutSupprimer() {
		return boutonModificationAutorise() && currentEmploi().tosRepartEmploiActivite().count() == 0 && currentEmploi().tosRepartEmploiCompetence().count() == 0;
	}
	public boolean peutSupprimerActivite() {
		return modificationEnCours() && currentRepartActivite() != null;
	}
	public boolean peutSupprimerCompetence() {
		return modificationEnCours() && currentRepartCompetence() != null;
	}
	// méthodes protégées
	protected void traitementsPourCreation() {
	}

	protected boolean traitementsPourSuppression() {
		// on ne peut supprimer que si la relation aux emplois type n'existe plus
		deleteSelectedObjects();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cet emploi-type ?";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"EmploiType");
	}

	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("etyLibelle",EOSortOrdering.CompareAscending)));
		displayGroupActivites.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("toActivite.actLibelle",EOSortOrdering.CompareAscending)));
		displayGroupCompetences.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("toCompetence.comLibelle",EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}

	protected boolean traitementsAvantValidation() {
		if (modeCreation()) {
			// vérifier si un emploi-type avec ce code est déjà utilisé
			EOEmploiType emploi = (EOEmploiType)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"EmploiType","etyCode",currentEmploi().code());
			if (emploi != null) {
				EODialogs.runErrorDialog("Erreur","Ce code est déjà utilisé");
				return false;
			}
		}
		return true;
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	// méthodes privées
	private EOEmploiType currentEmploi() {
		return (EOEmploiType)displayGroup().selectedObject();
	}
	private EORepartEmploiActivite currentRepartActivite() {
		return (EORepartEmploiActivite)displayGroupActivites.selectedObject();
	}
	private EORepartEmploiCompetence currentRepartCompetence() {
		return (EORepartEmploiCompetence)displayGroupCompetences.selectedObject();
	}
	private void afficherLibelle(boolean estActivite) {
		if (controleurPourInfo == null) {
			Window window = EOApplication.sharedApplication().windowObserver().activeWindow();
			int Y = window.getHeight() + window.getY() + 5;
			controleurPourInfo = new AffichageLibelle(window.getX(),Y,"","");
			controleurPourInfo.afficherFenetre();
		} else {
			controleurPourInfo.changerEtat();
		}
		modifierLibelle(estActivite);
	}
	private void modifierLibelle(boolean estActivite) {
		if (controleurPourInfo != null) {
			String titre = estActivite ? "Activité" : "Compétence";
			String message = "";
			if (estActivite) {
				if (currentRepartActivite() != null && currentRepartActivite().toActivite() != null) {
					message = currentRepartActivite().toActivite().actLibelle();
				}
			} else {
				if (currentRepartCompetence() != null && currentRepartCompetence().toCompetence() != null) {
					message = currentRepartCompetence().toCompetence().comLibelle();
				}
			}
			controleurPourInfo.setLabel(titre);
			controleurPourInfo.setText(message);
		}
	}
}


/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.mangue.client.gui.AdminPromotionView;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.EOParamPromotion;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AdminPromotionCtrl {

	private static final int INDEX_SFT_PERE = 0;
	private static final int INDEX_SFT_MERE = 1;

	private static AdminPromotionCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;

	private AdminPromotionView 			myView;

	private EODisplayGroup 			eod, eodGrades;
	private boolean					saisieEnabled, modeCreation;

	private ListenerPromotion 			listenerPromotion = new ListenerPromotion();

	private EOParamPromotion			currentPromotion;

	public AdminPromotionCtrl(EOEditingContext editingContext, boolean loadNotifications) {

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new AdminPromotionView(eod, eodGrades);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		setSaisieEnabled(false);

		myView.getTfDateOuverture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateOuverture().addActionListener(new ActionListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateFermeture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFermeture()));
		myView.getTfDateFermeture().addActionListener(new ActionListenerDateTextField(myView.getTfDateFermeture()));

		myView.getMyEOTable().addListener(listenerPromotion);

	}

	public static AdminPromotionCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new AdminPromotionCtrl(editingContext, true);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}
	public EOParamPromotion currentPromotion() {
		return currentPromotion;
	}

	public void setCurrentPromotion(EOParamPromotion currentPromotion) {
		this.currentPromotion = currentPromotion;
	}

	public NSTimestamp getDateOuverture() {
		try {
			return DateCtrl.stringToDate(myView.getTfDateOuverture().getText());
		}
		catch (Exception e) {
			return null;
		}
	}
	public void setDateOuverture(NSTimestamp myDate) {
		myView.getTfDateOuverture().setText(DateCtrl.dateToString(myDate));
	}

	public NSTimestamp getDateFermeture() {
		try {
			return DateCtrl.stringToDate(myView.getTfDateFermeture().getText());
		}
		catch (Exception e) {
			return null;
		}
	}
	public void setDateFermeture(NSTimestamp myDate) {
		myView.getTfDateFermeture().setText(DateCtrl.dateToString(myDate));
	}

	public void actualiser() {

		updateUI();

	}

	private void ajouter() {
		modeCreation = true;
		setSaisieEnabled(true);			
	}

	private void modifier() {
		modeCreation = false;
		setSaisieEnabled(true);
	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment ce paramètre ?", "Oui", "Non"))		
			return;

		try {
			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			ec.revert();
		}
	}


	protected boolean traitementsAvantValidation() {

		return true;	
	}


	private void refreshWithSelection(ZEOTable myTable, EOEnterpriseObject myObject) {		
		actualiser();
		myTable.forceNewSelectionOfObjects(new NSArray(myObject));
	}
	
	private void valider() {

		try {

			if (!traitementsAvantValidation())
				return;

			ec.saveChanges();
			refreshWithSelection(myView.getMyEOTable(), null);
			setSaisieEnabled(false);
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {
		ec.revert();
		listenerPromotion.onSelectionChanged();
		setSaisieEnabled(false);
		updateUI();
	}

	private void clearTextFields() {
		
	}
	
	private boolean modeCreation() {
		return modeCreation;
	}
	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}

	private void updateUI() {

		myView.getMyEOTable().setEnabled(!saisieEnabled());

	}

	private void updateData() {

		clearTextFields();
		updateUI();
	}

	private class ListenerPromotion implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			if (currentPromotion() != null)
				modifier();
		}

		/* (non-Javadoc)
		 * @see mangue.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			setCurrentPromotion((EOParamPromotion)eod.selectedObject());
		}
	}



	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.impression.EOModuleImpressionIndividu;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

// 19/10/2010 - Adaptation Netbeans
public class GestionModulesIndividu extends ModelePageComplete {
	private int oldNumMenu;
	  // méthodes de délégation du DG
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (currentModule() == null || modeCreation()) {
			oldNumMenu = 0;
		} else {
			oldNumMenu = currentModule().noOrdreMenu().intValue();
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
	// méthodes du controller DG
	public boolean peutValider() {
		if (super.peutValider()) {
			return currentModule() != null && currentModule().nom() != null && currentModule().texteMenu() != null && 
			currentModule().noOrdreMenu() != null && currentModule().noOrdreMenu().intValue() <= displayGroup().displayedObjects().count();	
		} else {
			return false;
		}
	}
	// Méthodes protégées
	protected void traitementsPourCreation() {
		int numModules = displayGroup().displayedObjects().count();
		currentModule().initAvecPosition(numModules);
	}
	protected boolean traitementsPourSuppression() {
		NSArray indexes = displayGroup().selectionIndexes();
		int positionCourante = ((Integer)indexes.objectAtIndex(0)).intValue();
		deleteSelectedObjects();
		if (positionCourante < displayGroup().displayedObjects().count()) {
			renumeroterModules(positionCourante);
		}
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce module ?";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"ModuleImpressionIndividu");
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("noOrdreMenu",EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		// supprimer tous les caractères non alphanumériques du nom
		currentModule().setNom(StringCtrl.toBasicString(currentModule().nom(),"-_",'_'));
		renumeroterModules(-1);
		return true;
	}

	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOModuleImpressionIndividu currentModule() {
		return (EOModuleImpressionIndividu)displayGroup().selectedObject();
	}
	private void renumeroterModules(int positionCourante) {
		for (int i = 0; i < displayGroup().displayedObjects().count();i++) {
			EOModuleImpressionIndividu module = (EOModuleImpressionIndividu)displayGroup().displayedObjects().objectAtIndex(i);
			if (positionCourante >= 0) {	// destruction
				if (i >= positionCourante) {	
					// décaler vers la valeur précédente tous les éléments suivants
					module.setNoOrdreMenu(new Integer(module.noOrdreMenu().intValue() - 1));
				}
			} else {
				int nouvellePosition = -1, anciennePosition = oldNumMenu - 1;
				if (currentModule() != null) {
					nouvellePosition = currentModule().noOrdreMenu().intValue() - 1;
				}
				if (anciennePosition < 0) { // création
					if (i >= nouvellePosition && module != currentModule()) {
						// décaler vers la valeur suivante tous les éléments suivants
						module.setNoOrdreMenu(new Integer(module.noOrdreMenu().intValue() + 1));
					}
				} else if (nouvellePosition > anciennePosition) {
					// décaler vers la valeur précédente tous les éléments compris entre ces deux positions
					if (i >= anciennePosition && i <= nouvellePosition && module != currentModule()) {
						module.setNoOrdreMenu(new Integer(module.noOrdreMenu().intValue() - 1));
					}
				} else if (nouvellePosition < anciennePosition) {
					// décaler vers la valeur suivante tous les éléments compris entre ces deux positions
					if (i >= nouvellePosition && i < anciennePosition && module != currentModule()) {
						module.setNoOrdreMenu(new Integer(module.noOrdreMenu().intValue() + 1));
					}
				}
			}
		}
	}
}

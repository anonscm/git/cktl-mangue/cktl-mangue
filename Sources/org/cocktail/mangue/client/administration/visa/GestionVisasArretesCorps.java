/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration.visa;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.visa.EOVisaTypeArreteCorps;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;

public class GestionVisasArretesCorps extends GestionVisaType {

	public void afficherTypePopulation() {
		NSMutableArray qualifiers = new NSMutableArray();

		if (EOGrhumParametres.isGestionHu() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.TEM_HOSPITALIER_KEY + " = 'N'", null));
		}
		if (EOGrhumParametres.isGestionEns() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.CODE_KEY + " != 'N'",null));
		}
		
		UtilitairesDialogue.afficherDialogue(this, EOTypePopulation.ENTITY_NAME, "getTypePopulation", false, new EOAndQualifier(qualifiers),true);
	}
	
	public void afficherTypeArrete() {
		UtilitairesDialogue.afficherDialogue(this, "TypeArrete", "getTypeArrete", false,null, false);
	}
	public void afficherCorps() {
		UtilitairesDialogue.afficherDialogue(this, EOCorps.ENTITY_NAME, "getCorps", true,EOQualifier.qualifierWithQualifierFormat("cTypeCorps = %@", new NSArray(currentVisaType().typePopulation().code())), false);
	}
	public void supprimerCorps() {
		currentVisaType().setCorpsRelationship(null);
	}
	// Notifications
	public void getTypePopulation(NSNotification aNotif) {
		if (modificationEnCours()) {
			ajouterRelation(currentVisaType(), aNotif.object(), EOVisaTypeArreteCorps.TYPE_POPULATION_KEY);
		}
	}
	public void getTypeArrete(NSNotification aNotif) {
		if (modificationEnCours()) {
			ajouterRelation(currentVisaType(), aNotif.object(), EOVisaTypeArreteCorps.TYPE_ARRETE_KEY);
		}
	}
	public void getCorps(NSNotification aNotif) {
		if (modificationEnCours()) {
			ajouterRelation(currentVisaType(), aNotif.object(), EOVisaTypeArreteCorps.CORPS_KEY);
		}
	}
	
	// Methodes du controller DG
	public boolean peutValider() {
		return super.peutValider() && currentVisaType().typeArrete() != null && currentVisaType().typePopulation() != null;
	}
	public boolean peutAjouterCorps() {
		return modificationEnCours() && currentVisaType().typePopulation() != null;
	}
	public boolean peutSupprimerCorps() {
		return modificationEnCours() && currentVisaType().corps() != null;
	}

	protected NSArray fetcherObjets() {
		return EOVisaTypeArreteCorps.rechercherVisasTypes(editingContext());
	}
	
	protected String nomEntite() {
		return EOVisaTypeArreteCorps.ENTITY_NAME;
	}
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOVisaTypeArreteCorps.TYPE_ARRETE_KEY + ".llTypeArrete",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOVisaTypeArreteCorps.TYPE_POPULATION_KEY + ".code",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOVisaTypeArreteCorps.CORPS_KEY + ".cCorps",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOVisaTypeArreteCorps.VISA_KEY + ".type.niveauPriorite",EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);

	}
	// MÆthodes privÆes
	private EOVisaTypeArreteCorps currentVisaType() {
		return (EOVisaTypeArreteCorps)displayGroup().selectedObject();
	}

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration.visa;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.visa.EOVisaContrat;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;

// 16/02/2011 - Restriction du qualifier sur les types de population pour les établissements qui ne gèrent pas les populations HU
public class GestionVisasContrats extends GestionVisaType {
	//	 Actions
	public void afficherTypeContratTravail() {
		EOQualifier qualifier = null;
		if (EOGrhumParametres.isGestionHu() == false) {
			qualifier = EOQualifier.qualifierWithQualifierFormat("temAhCuAo = 'N'", null);
		}		
		UtilitairesDialogue.afficherDialogue(this, "TypeContratTravail", "getTypeContrat", false, qualifier, false);
	}
	public void supprimerTypeContrat() {
		currentVisaType().removeObjectFromBothSidesOfRelationshipWithKey(currentVisaType().typeContrat(), "typeContrat");
	}
	// Notifications
	public void getTypeContrat(NSNotification aNotif) {
		if (modificationEnCours()) {
			ajouterRelation(currentVisaType(), aNotif.object(), "typeContrat");
		}
	}
	// MÆthodes du controller DG
	public boolean peutSupprimerTypeContrat() {
		return modificationEnCours() && currentVisaType() != null && currentVisaType().typeContrat() != null;
	}
	// MÆthodes protÆgÆes
	protected String nomEntite() {
		return "VisaContrat";
	}
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("typeContrat.code",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("visa.type.niveauPriorite",EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);

	}
	// MÆthodes privÆes
	private EOVisaContrat currentVisaType() {
		return (EOVisaContrat)displayGroup().selectedObject();
	}
}

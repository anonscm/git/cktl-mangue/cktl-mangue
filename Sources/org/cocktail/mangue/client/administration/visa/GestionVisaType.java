/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration.visa;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.visa.VisaPourType;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;

// 27/10/2010 - Adaptation Netbeans
//28/01/2011 - Raffraichissement des associations au démarrage
public abstract class GestionVisaType extends ModelePageComplete {
	// Actions
	public void afficherVisas() {
		UtilitairesDialogue.afficherDialogue(this,"Visa", "getVisa", false, null,false);
	}
	// Notifications
	public void getVisa(NSNotification aNotif) {
		ajouterRelation(currentVisaType(), aNotif.object(), "visa");
	}
	// Méthodes du controller DG
	public boolean peutAjouter() {
		return modeSaisiePossible();
	}
	public boolean peutValider() {
		return super.peutValider() && currentVisaType().visa() != null;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		raffraichirAssociations();
	}
	protected void traitementsPourCreation() {
		// rien de spécial
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR",message);
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cet élément ?";
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(), nomEntite());
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected boolean traitementsPourSuppression() {
		currentVisaType().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	protected void terminer() {
	}
	protected abstract String nomEntite();
	// Méthodes privées
	private VisaPourType currentVisaType() {
		return (VisaPourType)displayGroup().selectedObject();
	}
}

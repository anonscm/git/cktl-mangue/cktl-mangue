// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.administration.visa;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.visas.SaisieVisaView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.visa.EOTypeVisa;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisieVisaCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieVisaCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieVisaView myView;
	private EOVisa currentVisa;
	
	public SaisieVisaCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieVisaView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));

		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));

		CocktailUtilities.initTextField(myView.getTfType(), false, false);
	}

	public static SaisieVisaCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieVisaCtrl(editingContext);
		return sharedInstance;
	}



	public EOVisa currentVisa() {
		return currentVisa;
	}

	public void setCurrentVisa(EOVisa currentVisa) {
		this.currentVisa = currentVisa;
		updateData();
	}

	private void clearTextFields()	{
		myView.getTaVisa().setText("");
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
	}

	public EOVisa ajouter(EOTypeVisa typeVisa)	{

		myView.setTitle("VISA / AJOUT");

		setCurrentVisa(EOVisa.creer(ec, typeVisa));
		myView.setVisible(true);

		return currentVisa();
	}

	public boolean modifier(EOVisa visa) {
		setCurrentVisa(visa);
		myView.setTitle("VISA");
		
		myView.setVisible(true);
		return currentVisa() != null;
	}

	private void updateData() {

		clearTextFields();

		CocktailUtilities.setTextToField(myView.getTfType(), currentVisa().type().llTypeVisa());
		CocktailUtilities.setTextToArea(myView.getTaVisa(), currentVisa().lVisa());
		CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentVisa().dDebVal());
		CocktailUtilities.setDateToField(myView.getTfDateFin(), currentVisa().dFinVal());
		myView.getTemLocal().setSelected(currentVisa().estLocal());
		myView.getTemDeconcentration().setSelected(currentVisa().estDeconcentre());
		updateUI();
	}

	/**
	 * 
	 */
	private void updateUI() {

	}

	protected boolean traitementsAvantValidation() {
		return true;
	}

	/** methode a surcharger pour faire des traitements apres l'enregistrement des donnees dans la base */
	protected  void traitementsApresValidation() {
	}

	/**
	 * 
	 */
	private void valider()  {

		try {			

			if (!traitementsAvantValidation())
				return;

			currentVisa().setLVisa(CocktailUtilities.getTextFromArea(myView.getTaVisa()));
			currentVisa().setDDebVal(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			currentVisa().setDFinVal(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			
			currentVisa().setEstLocal(myView.getTemLocal().isSelected());
			currentVisa().setEstDeconcentre(myView.getTemDeconcentration().isSelected());
			
			ec.saveChanges();

			traitementsApresValidation();

			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		ec.revert();
		setCurrentVisa(null);
		myView.setVisible(false);
	}


	
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}


}

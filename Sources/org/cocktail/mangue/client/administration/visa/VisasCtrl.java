package org.cocktail.mangue.client.administration.visa;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.gui.visas.VisasView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.mangue.visa.EOTypeVisa;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class VisasCtrl {

	private static VisasCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.TRUE;
	private EOEditingContext ec;
	private VisasView myView;

	private ListenerVisa listenerVisa = new ListenerVisa();

	private EODisplayGroup eod;
	private EOVisa currentVisa;
	private EOTypeVisa currentType;

	public VisasCtrl(EOEditingContext editingContext) {

		ec = editingContext;
		eod = new EODisplayGroup();

		myView = new VisasView(null, MODE_MODAL.booleanValue(),eod);

		myView.getMyEOTable().addListener(listenerVisa);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		myView.setTypes(EOTypeVisa.fetchAll(ec, EOTypeVisa.SORT_ARRAY_PRIORITE_ASC));
		myView.getTfFiltreLibelle().getDocument().addDocumentListener(new ADocumentListener());
		myView.getPopupTypes().addActionListener(new MyPopupListener());

		actualiser();
		updateUI();

	}

	public static VisasCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new VisasCtrl(editingContext);
		return sharedInstance;
	}


	public EOVisa currentVisa() {
		return currentVisa;
	}

	public void setCurrentVisa(EOVisa currentVisa) {
		
		this.currentVisa = currentVisa;
		CocktailUtilities.setTextToArea(myView.getTaVisa(), "");
		if (currentVisa != null)
			CocktailUtilities.setTextToArea(myView.getTaVisa(), currentVisa.lVisa());
		
	}

	public EOTypeVisa currentType() {
		try {
			return (EOTypeVisa)myView.getPopupTypes().getSelectedItem();
		}
		catch (Exception e) {
			return null;			
		}
	}
	public void setCurrentType(EOTypeVisa currentType) {
		this.currentType = currentType;
	}

	private class MyPopupListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			actualiser();
		}
	}

	
	public void actualiser() {
		eod.setObjectArray(EOVisa.fetchAll(ec));
		filter();
	}
	
	
	private EOQualifier getQualifier() {
		NSMutableArray qualifiers = new NSMutableArray();
		
		if (currentType() != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisa.TYPE_KEY+" = %@", new NSArray(currentType())));
		if (StringCtrl.chaineVide(myView.getTfFiltreLibelle().getText()) == false)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisa.L_VISA_KEY+" caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreLibelle().getText()+"*")));
			
		return new EOAndQualifier(qualifiers);
	}
	
	private void filter() {
		eod.setQualifier(getQualifier());
		eod.updateDisplayedObjects();
		
		myView.getMyEOTable().updateData();
	}
	
	
	public JDialog getView() {
		return myView;
	}
	public JPanel getViewVisa() {
		return myView.getViewVisas();
	}

	private void ajouter() {
		if (SaisieVisaCtrl.sharedInstance(ec).ajouter(currentType()) != null)
			actualiser();
	}
	private void modifier() {
		updateUI();
	}
	private void supprimer() {
		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer le visa sélectionné ?", "Oui", "Non"))		
			return;			
		try {
			ec.deleteObject(currentVisa());
			ec.saveChanges();
			actualiser();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			ec.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			ec.revert();
		}
	}

	private class ListenerVisa implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentVisa((EOVisa)eod.selectedObject());
		}
	}
	private void updateUI() {
		myView.getBtnAjouter().setEnabled(currentType() != null);
		myView.getBtnModifier().setEnabled(currentVisa() != null && currentVisa().estLocal());
		myView.getBtnSupprimer().setEnabled(currentVisa() != null && currentVisa().estLocal());
	}
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

}
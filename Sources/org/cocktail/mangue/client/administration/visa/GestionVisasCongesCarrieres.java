/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration.visa;

import org.cocktail.mangue.client.select.TypePopulationSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.modele.mangue.visa.EOVisaCgmodPop;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class GestionVisasCongesCarrieres extends GestionVisaPourConge {
	
	private TypePopulationSelectCtrl myTypePopulationSelectCtrl;

	//	 Actions
	public void afficherTypePopulation() {
		NSMutableArray qualifiers = new NSMutableArray();
		
		if (myTypePopulationSelectCtrl == null)
			myTypePopulationSelectCtrl = new TypePopulationSelectCtrl(editingContext());

		EOTypePopulation typePopulation = myTypePopulationSelectCtrl.getTypePopulation();

		if (typePopulation != null) {
			currentVisaType().setTypePopulationRelationship(typePopulation);
		}
	}
	
	public void supprimerTypePopulation() {
		currentVisaType().setTypePopulationRelationship(null);
	}

	// Méthodes du controller DG
	public boolean peutSupprimerTypePopulation() {
		return modificationEnCours() && currentVisaType().typePopulation() != null;
	}
	// Méthodes protégées
	/** Retourne la liste des cong&eacute;s de titulaires */
	protected NSArray typesConges() {
		NSMutableArray typesConges = new NSMutableArray(EOTypeAbsence.rechercherTypesAbsencesGeres(editingContext()));
		typesConges.addObject(EOTypeAbsence.rechercherTypeAbsencePourTable(editingContext(), "TEMPS_PARTIEL", "TPAR"));
		return typesConges;
	}
	protected String nomEntite() {
		return "VisaCgmodPop";
	}
	/** tri par type conge,type de population, priorite des visas */
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("nomTableCgmod",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("typePopulation.code",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("visa.type.niveauPriorite",EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
	}
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) {
			// Vérifier si ce type de congé correspond à ce type de population
			if (currentVisaType().typePopulation() != null) {
				EOTypeAbsence typeAbsence = EOTypeAbsence.rechercherTypeAbsencePourTable(editingContext(), currentVisaType().nomTableCgmod(), null);
				if (typeAbsence.estCongeCommun() == false) {
					if (currentVisaType().typePopulation().estFonctionnaire() && typeAbsence.estCongePourFonctionnaire() == false) {

						if ( typeAbsence.estTempsPartiel() == false && typeAbsence.estTempsPartielTherapeutique() == false) {
							EODialogs.runErrorDialog("ERREUR", "Ce type d'absence ne s'applique pas aux titulaires");
							return false;
						}
					}
					if (currentVisaType().typePopulation().estFonctionnaire() == false && typeAbsence.estCongePourContractuel() == false) {
						EODialogs.runErrorDialog("ERREUR", "Ce type d'absence ne s'applique pas aux contractuels");
						return false;
					}
				}
			}
			// Vérifier si unicité de la combinaison car sinon problème de contrainte Oracle
			java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOVisaCgmodPop visaType = (EOVisaCgmodPop)e.nextElement();
				if (visaType != currentVisaType() && 
						visaType.nomTableCgmod().equals(currentVisaType().nomTableCgmod()) &&
						visaType.visa() == currentVisaType().visa() &&
						currentVisaType().typePopulation() == visaType.typePopulation()) {
					EODialogs.runErrorDialog("ERREUR", "Ce visa est déjà sélectionné pour ce type de congé et de population");
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}
	// Méthodes privées
	private EOVisaCgmodPop currentVisaType() {
		return (EOVisaCgmodPop)displayGroup().selectedObject();
	}

}

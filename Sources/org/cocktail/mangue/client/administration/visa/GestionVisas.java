/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration.visa;

import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.visa.EOTypeVisa;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// 26/10/2010 - Adaptation Netbeans
// 28/01/2011 - Raffraichissement des associations au démarrage
public class GestionVisas extends ModelePageComplete {
	public EODisplayGroup displayGroupTypes;
	private EOTypeVisa typeVisaCourant;

	// Accesseurs
	public String typeVisaCourant() {
		if (typeVisaCourant != null) {
			return typeVisaCourant.llTypeVisa();
		} else {
			return null;
		}
	}
	public void setTypeVisaCourant(String aStr) {
		java.util.Enumeration e = displayGroupTypes.displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOTypeVisa typeVisa = (EOTypeVisa)e.nextElement();
			if (typeVisa.llTypeVisa().equals(aStr)) {
				typeVisaCourant = typeVisa;
			}
		}
		
		displayGroup().setObjectArray(fetcherObjets());
		displayGroup().updateDisplayedObjects();
	}
	// Méthodes du controller DG
	public boolean peutAjouter() {
		return modeSaisiePossible() && typeVisaCourant != null;
	}
	public boolean peutSupprimer() {
		return boutonModificationAutorise() && currentVisa().estLocal();
	}
	public boolean peutValider() {
		return modificationEnCours() && currentVisa().lVisa() != null && currentVisa().lVisa().length() > 0 &&
		currentVisa().dDebVal() != null;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		displayGroupTypes.fetch();
		displayGroupTypes.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("niveauPriorite", EOSortOrdering.CompareAscending)));
		typeVisaCourant = (EOTypeVisa)displayGroupTypes.displayedObjects().objectAtIndex(0);
		super.preparerFenetre();
		raffraichirAssociations();
	}
	protected void traitementsPourCreation() {
		currentVisa().initAvecType(typeVisaCourant);
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected NSArray fetcherObjets() {
		if (typeVisaCourant == null) {
			return SuperFinder.rechercherEntite(editingContext(), "Visa");
		} else {
			return SuperFinder.rechercherAvecAttributEtValeurEgale(editingContext(), "Visa", "type", typeVisaCourant);
		}
	}
	/** Tri des visas par ordre de priorit&eacute; et date d&eacute;but croisante */
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("type.niveauPriorite",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("dDebVal",EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce visa ?";
	}
	protected boolean traitementsAvantValidation() {
		if (modeCreation()) {
			String nomSequence = null;
			// Calculer la clé primaire
			if (currentVisa().estLocal()) {
				nomSequence = "VisaLocalSeq";
			} else {
				nomSequence = "VisaSeq";
			}
			currentVisa().setNoSeqVisa(SuperFinder.clePrimairePour(editingContext(), "Visa", "noSeqVisa", nomSequence, false));
		}
		return true;
	}
	protected boolean traitementsPourSuppression() {
		currentVisa().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOVisa currentVisa() {
		return (EOVisa)displayGroup().selectedObject();
	}
}

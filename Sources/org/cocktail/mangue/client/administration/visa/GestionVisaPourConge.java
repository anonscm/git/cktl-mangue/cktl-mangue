/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration.visa;

import org.cocktail.client.components.SimpleDialogWithStrings;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.modele.mangue.visa.VisaPourConge;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public abstract class GestionVisaPourConge extends GestionVisaType {
	private NSArray nomsTypesConges;	// On ne garde que les congés correspondant à une table => Arrêté donc visa
	
	public GestionVisaPourConge() {
		super();
		preparerNomsTypesConges();
	}
	// Actions
	public void afficherTypeConge() {
		SimpleDialogWithStrings controleur = new SimpleDialogWithStrings("SelectionTypeConge","Sélectionner le type de congés",false,false,nomsTypesConges);
		controleur.init();
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getTypeConge",new Class[] {NSNotification.class}),"SelectionTypeConge",null);
		controleur.afficherFenetre();
	}
	// Notifications
	public void getTypeConge(NSNotification aNotif) {
		if (aNotif.object() != null && !estLocke()) {	// pour éviter que d'autres composants le traitent
			currentVisaTypeConge().setNomTableCgmod((String)aNotif.object());
		}
	}
	// Méthodes du controller DG
	public boolean peutValider() {
		return super.peutValider() && currentVisaTypeConge().nomTableCgmod() != null;
	}
	// Méthodes protégées
	/** Retourne la liste des conges valides pour cette table */
	protected abstract NSArray typesConges();
	//	 Méthodes privées
	private VisaPourConge currentVisaTypeConge() {
		return (VisaPourConge)displayGroup().selectedObject();
	}
	
	/**
	 * 
	 */
	private void preparerNomsTypesConges(){
		NSMutableArray types = new NSMutableArray();
		java.util.Enumeration e = typesConges().objectEnumerator();
		while (e.hasMoreElements()) {
			EOTypeAbsence typeConge = (EOTypeAbsence)e.nextElement();
			if (typeConge.codeHarpege() != null) {
				types.addObject(typeConge.codeHarpege());
			}
		}
		nomsTypesConges = new NSArray(types);
	}
}

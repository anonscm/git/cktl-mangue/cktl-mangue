/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.cocktail.client.composants.AideUtilisateur;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.impression.EOContratImpression;
import org.cocktail.mangue.modele.mangue.impression.EOModuleImpression;
import org.cocktail.mangue.modele.mangue.impression.EOParamModuleImpression;
import org.cocktail.mangue.modele.mangue.impression.ParametreImpression;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

/** Gestion des modules d'impression<BR>
 * Les modules d'impression sont des composants Jasper qui comportent des requ&ecirc;tes SQL et auxquels on passe des param&egrave;tres
 * d&eacute;finis dans cette page. La valeur des param&egrave;tres sera fournie par l'utilisateur au moment de l'impression (voir GestionEditionsDivers.java)<BR>
 * Pour ajouter des types de param&egrave;tres, modifier le popup de l'interface et le tableau statique de la classe ParametreImpression.
 * @author christine
 *
 */
// 04/03/2011 - ajout d'une aide utilisateur et de compteurs pour les vues de description
public class GestionModulesImpression extends ModelePageComplete {
	public EODisplayGroup displayGroupParametres;
	public EOTable listeParametres;
	public EOView vueBoutonsValidationParametre,vueBoutonsModificationParametre;
	private boolean modificationEnCoursParametre;
	
	// méthodes de délégation du display group
	// accesseurs
	public String compteurDescription() {
		if (currentModule() == null || currentModule().descriptionModule() == null) {
			return "";
		} else {
			return "" + currentModule().descriptionModule().length() + "/" + EOContratImpression.LG_DESCRIPTION;
		}
	}
	public String compteurDescriptionParametre() {
		if (currentParametre() == null || currentParametre().descriptionParametre() == null) {
			return "";
		} else {
			return "" + currentParametre().descriptionParametre().length() + "/" + EOContratImpression.LG_DESCRIPTION;
		}
	}
	public String typeParametre() {
		if (displayGroupParametres == null) {
			// préparation de la fenêtre
			return null;
		}
		if (currentParametre() == null) {
			return null;
		} else {
			return ParametreImpression.typeParametre(currentParametre().typeParametre());
		}
	}
	public void setTypeParametre(String aString) {
		if (displayGroupParametres != null && currentParametre() != null && modificationEnCoursParametre()) {
			currentParametre().setTypeParametre(aString.substring(0,1));
		}
	}
	// actions
    /** action pour ajouter un param&egrave;tre */
	public void ajouterParametre() {
		setModificationEnCours(true);
		modificationEnCoursParametre = true;
		displayGroupParametres.insert();
		setEdited(true);
		currentParametre().initAvecModule(currentModule());
		setModificationEnCours(true);
		// afficher les boutons de validation
		afficherBoutonsParametres(true);
	    NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		updaterDisplayGroups();
	}
	/** action pour modifier un param&egrave;tre */
	public void modifierParametre() {
		setModificationEnCours(true);
		modificationEnCoursParametre = true;
		setEdited(true);
		// afficher les boutons de validation
		afficherBoutonsParametres(true);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		controllerDisplayGroup().redisplay();
	}
	/** action pour supprimer un param&egrave;tre. La suppression sera enregistr&eacute;e dans la base */
	public void supprimerParametre() {
         if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous vraiment supprimer ce paramètre","Oui","Non")) {
        	 	currentModule().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre(),"parametres");
        	 	save();
        	 	updaterDisplayGroups();
         }
	}
	public void afficherAide() {
		AideUtilisateur controleur = new AideUtilisateur("AideModelesPourContrat",null);
		controleur.afficherFenetre();
	}
	// méthodes du controller DG
	public boolean modificationEnCoursParametre() {
		return modificationEnCoursParametre;
	}
	public boolean boutonModificationAutorise(){
		if (super.boutonModificationAutorise()) {
			return modificationEnCoursParametre == false;
		} else {
			return false;
		}
	}
	public boolean peutValider() {
		if (currentModule() != null && currentModule().nom() != null && currentModule().nom().length() > 0) {
			return super.peutValider();
		} else {
			return false;
		}
	}
	public boolean peutAjouterParametre() {
		return boutonModificationAutorise() ;
	}
	public boolean peutModifierParametre() {
		return boutonModificationAutorise() && currentParametre() != null;
	}
	public boolean peutValiderParametre() {
		if (modificationEnCoursParametre() == false) {
			return false;
		}
		if (currentParametre() != null && currentParametre().nomParametre() != null && currentParametre().nomParametre().length() > 0 &&
				currentParametre().descriptionParametre() != null && currentParametre().descriptionParametre().length() > 0) {
			return super.peutValider();
		} else {
			return false;
		}
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
  		listeParametres.table().addMouseListener(new DoubleClickParametreListener());
	}
	protected void traitementsPourCreation() {	}

	protected boolean traitementsPourSuppression() {
		displayGroup().deleteSelection();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce module ?";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"ModuleImpression");
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("nom",EOSortOrdering.CompareAscending)));	
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		// supprimer tous les caractères non alphanumériques du nom
		currentModule().setNom(StringCtrl.toBasicString(currentModule().nom(),"-_",'_'));
		return true;
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupParametres.updateDisplayedObjects();
	}
	protected void remettreEtatSelection() {
		super.remettreEtatSelection();
		modificationEnCoursParametre = false;
		afficherBoutonsParametres(false);
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOModuleImpression currentModule() {
		return (EOModuleImpression)displayGroup().selectedObject();
	}
	private EOParamModuleImpression currentParametre() {
		return (EOParamModuleImpression)displayGroupParametres.selectedObject();
	}
	  /** affiche les boutons de validation ou les boutons de modification selon la valeur du param&egrave;tre */
	private void afficherBoutonsParametres(boolean estValidation) {
		vueBoutonsValidationParametre.setVisible(estValidation);
		vueBoutonsModificationParametre.setVisible(!estValidation);
	}
	 // Classe d'ecoute pour les doubleclics dans la table view  */
    public class DoubleClickParametreListener extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
            		if (!boutonModificationAutorise()) {
            			modifierParametre();
            		}
            }
        }
    }
	
  

}

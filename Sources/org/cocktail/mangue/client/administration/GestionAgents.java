/*
 * Created on 30 mars 2006
 *
 * Gestion des agents
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import org.cocktail.client.components.DialogueSimpleSansFetch;
import org.cocktail.client.components.SimpleDialogNoFetch;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.component.COCheckbox;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.select.GroupeSelectCtrl;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOCompte;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOAgentsDroitsServices;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

 /** Permet d'affecter aux agents des droits ainsi que des acc&egrave;s &agrave; des services ou bien
  * permet d'affecter des individus a des groupes. Pour des raisons de contraintes de validit&eacute;, on ne peut plus
  * ajouter de services ou de groupes, une fois qu'on en a supprim&eacute;. Il faut d'abord sauvegarder les donnees */

public class GestionAgents extends ModelePageComplete {

	public COCheckbox checkModuleVacation;
	public EOTable listeServices;
	public EODisplayGroup displayGroupServices;
	private int typeProfil;

	private boolean demarrageEnCours,profilModifie, serviceSupprime;
	private static final int PROFIL_GESTIONNAIRE = 0;
	private static final int PROFIL_PERSONNEL = 1;
	public static final String SYNCHRONISER_AGENT = "SynchroniserAgent";
	
	public int typeProfil() {
		return typeProfil;
	}
	public void setTypeProfil(int unType) {
		if (!demarrageEnCours) {
			typeProfil = unType;
			displayGroup().setObjectArray(fetcherObjets());
		}
	}
	/** 
	 * Methode de delegation des EODisplayGroup
	 *	Selection d'un agent et mise a jour des services.
	 *
	 * @param eod DisplayGroup dans lequel la selection a change. 
	 */
	public void displayGroupDidChangeSelection(EODisplayGroup eod)	{
		if (eod == displayGroup()) {
			super.displayGroupDidChangeSelection(eod);
			profilModifie = false;
			serviceSupprime = false;
			if (currentAgent() != null) {
				displayGroupServices.setObjectArray(currentAgent().toAgentsDroitsServices());
			}
			else {
				displayGroupServices.setObjectArray(null);
			}
			updaterDisplayGroups();
			displayGroupServices.updateDisplayedObjects();
		}
	}
	public void displayGroupDidSetValueForObject(EODisplayGroup group, Object value, Object eo, String key) {
		if (group == displayGroup() && key.equals("peutConsulterDossier")) {
			if (currentAgent() != null && currentAgent().peutConsulterDossier()) {
				// On a transformé un gestionnaire en personnel, réinitialiser l'agent
				if (currentAgent().toAgentsDroitsServices() != null && currentAgent().toAgentsDroitsServices().count() > 0) {
					serviceSupprime = true;
				}
				currentAgent().transformerEnPersonnel();
			}
			profilModifie = true;
			updaterDisplayGroups();
		}
	}

	// actions
	public void toutSelectionner() {
		changerSelection(true);
	}
	public void toutDeselectionner() {
		changerSelection(false);
	}
	public void afficherAide() {
		EODialogs.runInformationDialog("Aide", "Le bouton \"Ajouter Groupe\" n'est actif que si l'utilisateur gère tous les agents et l'agent en cours de modification ne gère pas tous les agents.");
	}
	public void ajouterService() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(editingContext(), DateCtrl.today());
		if (ajouterStructure(structure)) {
			displayGroupServices.insert();
			currentService().initAvecAgentEtStructure(currentAgent(),structure);	
		}
		setEdited(true);
	}
	public void ajouterGroupe() {
		EOStructure structure = ((GroupeSelectCtrl)GroupeSelectCtrl.sharedInstance()).selectionnerGroupe(editingContext());
		if (ajouterStructure(structure)) {
			displayGroupServices.insert();
			currentService().initAvecAgentEtStructure(currentAgent(),structure);	
		}
		setEdited(true);
	}
	
	public void supprimerService() {
		LogManager.logDetail("supprimerService");
		setEdited(true);
		currentAgent().removeObjectFromBothSidesOfRelationshipWithKey(currentService(),"toAgentsDroitsServices");
		serviceSupprime = true;
		displayGroupServices.deleteSelection();
	}
	// Notifications
	public void annulerSelection(NSNotification aNotif) {
		LogManager.logDetail("Notification annulerSelection");
		annuler();
	}
	
	/**
	 * 
	 * @param aNotif
	 */
	public void getAgent(NSNotification aNotif) {
		LogManager.logDetail("Notification getAgent");
		if (modeCreation() && aNotif.object() != null) {
			String message = null;
			EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			// vérifier si l'agent est déjà défini
			NSArray individus = (NSArray)displayGroup().displayedObjects().valueForKey("toIndividu");
			if (individus.containsObject(individu)) {
				message = "Les droits de cet agent sont déjà définis !";
			} else {
				// Vérifier si c'est agent est défini dans l'autre type de profil
				if (typeProfil == PROFIL_GESTIONNAIRE) {
					NSArray agents =  EOAgentPersonnel.rechercherPersonnels(editingContext());
					if (((NSArray)agents.valueForKey(EOAgentPersonnel.TO_INDIVIDU_KEY)).containsObject(individu)) {
						message = "Les droits de cet agent sont déjà définis dans le profil Personnel";
					}
				} else {
					NSArray agents = EOAgentPersonnel.rechercherGestionnairesMangue(editingContext());
					if (((NSArray)agents.valueForKey(EOAgentPersonnel.TO_INDIVIDU_KEY)).containsObject(individu)) {
						message = "Les droits de cet agent sont déjà définis dans le profil Gestionnaires Mangue";
					}
				}
				if (message == null) {
					try {
						EOCompte currentCompte = EOCompte.comptePourIndividu(editingContext(),individu);
						if (currentCompte == null) {
							message = "Cet individu n'a pas de compte de login";

						}
						if (currentCompte != null) {
							currentAgent().setCptOrdre(currentCompte.cptOrdre());
							currentAgent().addObjectToBothSidesOfRelationshipWithKey(individu,EOAgentPersonnel.TO_INDIVIDU_KEY);
							currentAgent().addObjectToBothSidesOfRelationshipWithKey(currentCompte,EOAgentPersonnel.COMPTE_KEY);
						}
					} catch (Exception e) {
						message = "Cet agent a plusieurs comptes";
					}
				}
			} 
			if (message != null) {
				EODialogs.runErrorDialog("Erreur",message);
				annuler();
			}
		}
	}
	// méthodes du controleur DG
	public boolean peutAjouter() {
		return modeSaisiePossible() && !super.modificationEnCours();
	}
	
	/** retourne true si on est mode modification */
	public boolean peutModifierTypeAgent() {
		return false;
	}
	
	public boolean peutModifierModuleVacation() {
		return super.modificationEnCours();
	}

	/** On ne peut modifier les donnees que pour les gestionnaires Mangue */
	public boolean modificationEnCours() {
		return super.modificationEnCours() && currentAgent().peutConsulterDossier() == false && checkModuleVacation.isSelected() == false;
	}
	
	/** on ne peut valider que si un individu a ete choisi pour cet agent et si au moins un type de population est choisi
	 */
	public boolean peutValider() {
		return super.peutValider() && currentAgent() != null && currentAgent().toIndividu() != null 
				&&
				( currentAgent().agtModVacation() != null ||
				currentAgent().agtTypePopulation() != null && currentAgent().agtTypePopulation().length() > 0) ;
	}
	public boolean peutAjouterService() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return modificationEnCours() && !modeCreation();
	}
	public boolean peutSupprimerService() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return modificationEnCours() && agent.gereTouteStructure() && currentService() != null;
	}
	public boolean peutChangerGestionStructure() {
		return modificationEnCours() && displayGroupServices.allObjects().count() == 0;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		demarrageEnCours = true;
		typeProfil = PROFIL_GESTIONNAIRE;
		super.preparerFenetre();
	//	GraphicUtilities.changerTaillePolice(listeServices,11);
		demarrageEnCours = false;
		serviceSupprime = false;
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("annulerSelection",new Class[] {NSNotification.class}),SimpleDialogNoFetch.CANCEL_NOTIFICATION,null);
	}
	protected void traitementsPourCreation() {
		currentAgent().init();
		if (typeProfil == PROFIL_PERSONNEL) {
			currentAgent().initPourPersonnel();
		}
		// afficher la liste des agents pour sélection d'un agent
		DialogueSimpleSansFetch controleur = new DialogueSimpleSansFetch(EOIndividu.ENTITY_NAME,"GestionAgents_SelectionIndividu","nomUsuel","Sélection d'un individu",false,true,false,false,false);
		controleur.init();
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("temValide = 'O' AND personnels.noDossierPers <> nil",null);
		controleur.setQualifier(qualifier);
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getAgent",new Class[] {NSNotification.class}),"GestionAgents_SelectionIndividu",null);
		controleur.afficherFenetre();
	}
	protected boolean traitementsPourSuppression() {
		currentAgent().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	protected void traitementsApresSuppression() {
		serviceSupprime = false;
		super.traitementsApresSuppression();	
	}
	protected void traitementsApresRevert() {
		serviceSupprime = false;
		if (currentAgent() != null) {
			displayGroupServices.setObjectArray(currentAgent().toAgentsDroitsServices());
			displayGroupServices.updateDisplayedObjects();
		}
		super.traitementsApresRevert();	
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cet agent ?";
	}
	protected NSArray fetcherObjets() {
		if (typeProfil == PROFIL_GESTIONNAIRE) {
			return EOAgentPersonnel.rechercherGestionnairesMangue(editingContext());
		} else {
			return EOAgentPersonnel.rechercherPersonnels(editingContext());
		}
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("toIndividu.nomUsuel",EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutGererAgents();
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void traitementsApresValidation() {

		super.traitementsApresValidation();
		serviceSupprime = false;

		// 09/10/2009 - suite infos S. Brison
		if (profilModifie) {
			if (currentAgent().peutConsulterDossier()) {
				setTypeProfil(PROFIL_PERSONNEL);
			} else {
				setTypeProfil(PROFIL_GESTIONNAIRE);
			}
					}	
		// 27/02/09 - Si il s'agit de l'utilisateur courant, envoyer une notification pour la liste des agents
		if (((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID() == editingContext().globalIDForObject(currentAgent())) {
		NSNotificationCenter.defaultCenter().postNotification(SYNCHRONISER_AGENT,null);
		}
		updaterDisplayGroups();
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOAgentPersonnel currentAgent() {
		return (EOAgentPersonnel)displayGroup().selectedObject();
	}
	private EOAgentsDroitsServices currentService() {
		return (EOAgentsDroitsServices)displayGroupServices.selectedObject();
	}
	private void changerSelection(boolean enabled) {
		currentAgent().setGereEnseignants(enabled);
		currentAgent().setGereHeberges(enabled);
		currentAgent().setGereNonEnseignants(enabled);
		currentAgent().setGereVacataires(enabled);
		currentAgent().setGereTouteStructure(enabled);
		currentAgent().setPeutAdministrer(enabled);
		currentAgent().setPeutAfficherAccidentTravail(enabled);
		currentAgent().setPeutAfficherAgents(enabled);
		currentAgent().setPeutAfficherCarrieres(enabled);
		currentAgent().setPeutAfficherConges(enabled);
		currentAgent().setPeutAfficherContrats(enabled);
		currentAgent().setPeutAfficherEmplois(enabled);
		currentAgent().setPeutAfficherIndividu(enabled);
		currentAgent().setPeutAfficherInfosPerso(enabled);
		currentAgent().setPeutAfficherOccupation(enabled);
		currentAgent().setPeutAfficherPostes(enabled);
		currentAgent().setPeutCreerListesElectorales(enabled);
		currentAgent().setPeutGererAccidentTravail(enabled);
		currentAgent().setPeutGererAgents(enabled);
		currentAgent().setPeutGererCarrieres(enabled);
		currentAgent().setPeutGererConges(enabled);
		currentAgent().setPeutGererContrats(enabled);
		currentAgent().setPeutGererEmplois(enabled);
		currentAgent().setPeutGererIndividu(enabled);
		currentAgent().setPeutGererOccupation(enabled);
		currentAgent().setPeutGererPostes(enabled);
		currentAgent().setPeutGererPromouvabilites(enabled);
		currentAgent().setPeutGererPrimes(enabled);
		currentAgent().setPeutUtiliserOutils(enabled);
		currentAgent().setPeutUtiliserRequetes(enabled);
		currentAgent().setPeutGererNomenclatures(enabled);
		currentAgent().setPeutGererBudget(enabled);
		currentAgent().setPeutGererCir(enabled);
		currentAgent().setPeutGererVacations(enabled);
		controllerDisplayGroup().redisplay();
	}
	private boolean ajouterStructure(EOStructure structure) {
		// On peut ajouter une structure si elle n'est pas nulle et qu'elle n'est pas déjà défini dans les services que gère/accède l'agent
		if (structure == null) {
			return false;
		}
		if (currentAgent().toAgentsDroitsServices() == null || currentAgent().toAgentsDroitsServices().count() == 0) {
			return true;
		}
		NSArray structures = (NSArray)currentAgent().toAgentsDroitsServices().valueForKey("toStructure");
		return structures.containsObject(structure) == false;
	}
	
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageChevron;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;

// 23/11/2010 - bug lors de la sélection d'un grade par un dialogue (on recherchait les passages échelon)
//16/02/2011 - Restriction du qualifier sur les grades pour les établissements qui ne gèrent pas les populations HU et les ENS
public class GestionPassagesChevron extends ModelePageComplete {
	private EOGrade gradeRecherche;
	private String echelon, chevron;
	private String dateOuverture;
	private EOPassageChevron derniereSelection;
	public EODisplayGroup displayGroupEchelons;
	public EODisplayGroup displayGroupChevrons;

	// Accesseurs
	public EOGrade gradeRecherche() {
		return gradeRecherche;
	}
	public String cGradeRecherche() {
		if (gradeRecherche() == null) {
			return null;
		} else {
			return gradeRecherche.cGrade();
		}
	}
	public void setCGradeRecherche(String cGrade) {
		if (cGrade == null) {
			gradeRecherche = null;
		} else {
			gradeRecherche = (EOGrade)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "Grade", "cGrade", cGrade);
			if (gradeRecherche() == null) {
				displayGroup().setObjectArray(null);
			} else if ((EOGrhumParametres.isGestionEns() == false && gradeRecherche.toCorps().toTypePopulation().code().equals("N")) || 
			(EOGrhumParametres.isGestionHu() == false && gradeRecherche.toCorps().toTypePopulation().estHospitalier())) {
				gradeRecherche = null;
				displayGroup().setObjectArray(null);
			} else {
				displayGroup().setObjectArray(EOPassageChevron.rechercherPassagesChevronPourGrade(editingContext(), new NSArray(gradeRecherche),true));
			}
		}
		updaterDisplayGroups();
	}
	public String echelon() {
		return echelon;
	}
	public void setEchelon(String echelon) {
		this.echelon = echelon;
		setEdited(true);
	}
	public String chevron() {
		return chevron;
	}
	public void setChevron(String chevron) {
		this.chevron = chevron;
		setEdited(true);
	}
	public String dateOuverture() {
		return dateOuverture;
	}
	public void setDateOuverture(String dateOuverture) {
		if (dateOuverture != null) {
			this.dateOuverture = DateCtrl.dateCompletion(dateOuverture);
		} else {
			this.dateOuverture = dateOuverture;
		}
		setEdited(true);
	}
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		if (currentPassage() == null) {
			echelon = null;
			chevron = null;
			dateOuverture = null;
		} else {
			echelon = currentPassage().cEchelon();
			chevron = currentPassage().cChevron();
			dateOuverture = currentPassage().dateDebutFormatee();
		}
		derniereSelection = currentPassage();

		super.displayGroupDidChangeSelection(group);
	}
	// Actions
	public void afficherGradesRecherche() {
		LogManager.logDetail("GestionEquivalencesGrade - afficherGradesRecherche");
		NSMutableArray qualifiers = new NSMutableArray();
		// 16/02/2011
		if (!EOGrhumParametres.isGestionHu()) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps.toTypePopulation.temHospitalier = 'N'", null));
		}
		if (!EOGrhumParametres.isGestionEns()) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps.toTypePopulation.code != 'N'",null));
		}

		UtilitairesDialogue.afficherDialogue(this,"Grade","getGradeRecherche",true,new EOAndQualifier(qualifiers),false);
	}
	// Notifications
	public void getGradeRecherche(NSNotification aNotif) {
		if (aNotif.object() != null) {
			gradeRecherche = (EOGrade)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(), editingContext());
			displayGroup().setObjectArray(EOPassageChevron.rechercherPassagesChevronPourGrade(editingContext(), new NSArray(gradeRecherche),true));
			updaterDisplayGroups();
		}
	}
	// Méthodes du controller DG
	public boolean peutAjouter() {
		return modeSaisiePossible() && gradeRecherche() != null;
	}
	public boolean peutAfficherGradeRecherche() {
		return modeSaisiePossible() && !modificationEnCours();
	}
	public boolean peutValider() {
		return super.peutValider() && echelon != null && chevron != null && dateOuverture != null;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();

		displayGroupEchelons.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cEchelon", EOSortOrdering.CompareAscending)));
		displayGroupEchelons.setObjectArray(SuperFinder.rechercherEntite(editingContext(), "Echelon"));
		displayGroupChevrons.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cChevron", EOSortOrdering.CompareAscending)));
		displayGroupChevrons.setObjectArray(SuperFinder.rechercherEntite(editingContext(), "Chevron"));

	}
	protected void traitementsPourCreation() {
		currentPassage().initAvecGrade(gradeRecherche);
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur", message);
	}
	protected boolean conditionsOKPourFetch() {
		return false;
	}
	/** les passages &eacute;chelon seront fetch&eacute; sur une recherche */
	protected NSArray fetcherObjets() {
		return null;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce passage chevron ?";
	}
	protected void parametrerDisplayGroup() {
		// Trier par échelon croissant et date d'ouverture décroissante
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("cEchelon", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("cChevron", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("dOuverture", EOSortOrdering.CompareDescending));
		displayGroup().setSortOrderings(sorts);
	}
	/** Si les donn&eacute;es; de la cl&eacute; ont &eacute;t&eacute; modifi&eacute;es (&eacute;chelon, date ouverture),
	 *  on recr&eacute;e un passage &eacute;chelon
	 */
	protected boolean traitementsAvantValidation() {
		// Vérifier si il existe un passage échelon avec le même échelon dont les dates se chevauchent
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPassageChevron passage = (EOPassageChevron)e.nextElement();
			if (passage != currentPassage() && passage.cEchelon().equals(echelon) && passage.cChevron().equals(chevron)) {
				if (IntervalleTemps.intersectionPeriodes(passage.dOuverture(), passage.dFermeture(), DateCtrl.stringToDate(dateOuverture), currentPassage().dFermeture()) != null) {
					EODialogs.runErrorDialog("Erreur", "Il existe déjà un passage chevron pour ce grade, cet échelon et ce chevron défini sur cette période");
					return false;
				}
			}
		}
		EOPassageChevron passage = currentPassage();
		derniereSelection = currentPassage();
		// l'échelon, le chevron et la date d'ouverture sont nécessairement non nuls sinon on ne peut pas valider
		if (!modeCreation() && 
			(echelon.equals(currentPassage().cEchelon()) == false || chevron.equals(currentPassage().cChevron()) == false || dateOuverture.equals(currentPassage().dateDebutFormatee()) == false)) {
			passage = new EOPassageChevron();
			passage.takeValuesFromDictionary(currentPassage().snapshot());
			editingContext().insertObject(passage);
			editingContext().deleteObject(currentPassage());
			derniereSelection = passage;
		}
		// Modifier la clé
		passage.setCEchelon(echelon);
		passage.setCChevron(chevron);
		passage.setDateDebutFormatee(dateOuverture);
		// Vérifier si l'indice existe pour afficher un warning
		if (passage.cIndiceBrut() != null && EOIndice.indiceMajorePourIndiceBrutEtDate(editingContext(), passage.cIndiceBrut(), null) == null) {
			EODialogs.runInformationDialog("Attention", "Cet indice n'est pas défini dans la table des indices");
		}
		return true;
	}
	protected void traitementsApresValidation() {
		if (derniereSelection != currentPassage()) {
			// pour réafficher dans le dg le passage échelon modifié
			displayGroup().setObjectArray(EOPassageChevron.rechercherPassagesChevronPourGrade(editingContext(), new NSArray(gradeRecherche),true));
			displayGroup().selectObject(derniereSelection);
		}
		super.traitementsApresValidation();
	}
	/** Suppression du passage &eacute;chelon */
	protected boolean traitementsPourSuppression() {
		deleteSelectedObjects();
		return true;
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOPassageChevron currentPassage() {
		return (EOPassageChevron)displayGroup().selectedObject();
	}

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EODestinataire;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/** G&egrave; la cr&eacute;ation/modification/suppression des destinataires pour les arr&ecirc;t&eacute;s.<BR>
 * Ils sont affich&eacute;s &agrave; par ordre de priorit&eacute; croissante.
 * @author christine
 *
 */
public class GestionDestinataires extends ModelePageComplete {
	// Méthodes du controller DG
	public boolean peutValider() {
		return super.peutValider() && currentDestinaire().desLibelle() != null && currentDestinaire().desLibelle().length() > 0
		&& currentDestinaire().desPriorite() != null && currentDestinaire().desPriorite().intValue() > 0;
	}
	// méthodes protégées
	protected void traitementsPourCreation() {
		// Mettre comme numéro de destinataire, le dernier numéro de destinataire + 1
		EODestinataire lastDestinaire = null;
		java.util.Enumeration e = displayGroup().displayedObjects().reverseObjectEnumerator();
		// on parcourt l'énumération à l'envers car le destinataire inséré est le dernier
		// on prend le premier différent
		while (e.hasMoreElements()) {
			EODestinataire destinataire = (EODestinataire)e.nextElement();
			if (destinataire != currentDestinaire()) {
				lastDestinaire = destinataire;
				break;
			}
		}
		int priorite = 1;
		if (lastDestinaire != null) {
			priorite = lastDestinaire.desPriorite().intValue() + 1;
		}
		currentDestinaire().setDesPriorite(new Integer(priorite));
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur", message);
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(), "Destinataire");
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce destinataire ?";
	}
	protected void parametrerDisplayGroup() {
		// trier les destinataires par ordre de priorité croissante
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("desPriorite", EOSortOrdering.CompareAscending)));
	}
	protected boolean traitementsAvantValidation() {
		// Vérifier si il y a déjà un objet avec le même numéro de priorité, décaler alors le numéro
		// de priorité plus grande
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		EODestinataire destinataireComparaison = currentDestinaire();
		int numeroComparaison = destinataireComparaison.desPriorite().intValue();
		while (e.hasMoreElements()) {
			EODestinataire destinataire = (EODestinataire)e.nextElement();
			if (destinataire != destinataireComparaison) {
				if (destinataire.desPriorite().intValue() >= numeroComparaison) {
					if (destinataire.desPriorite().intValue() == numeroComparaison) {
						// décaler ce destinataire d'1
						destinataire.setDesPriorite(new Integer(numeroComparaison + 1));
						// le prendre comme référence
						destinataireComparaison = destinataire;
						numeroComparaison = destinataireComparaison.desPriorite().intValue();
					}
				} /*else {
					// on a trouvé un numéro plus grand, on n'a plus besoin de décaler
					break;
				}*/
			}
		}
		return true;
	}
	protected boolean traitementsPourSuppression() {
		//	Décaler d'1 en décroissant tous les destinataires de priorité plus grande
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		int numeroComparaison = currentDestinaire().desPriorite().intValue();
		while (e.hasMoreElements()) {
			EODestinataire destinataire = (EODestinataire)e.nextElement();
			if (destinataire.desPriorite().intValue() > numeroComparaison) {
				// décaler ce destinataire d'1
				destinataire.setDesPriorite(new Integer(destinataire.desPriorite().intValue() - 1));
			}
		}
		// supprimer l'élément courant
		deleteSelectedObjects();
		return true;
	}
	protected void terminer() {
	}
	// méthodes privées
	private EODestinataire currentDestinaire() {
		return (EODestinataire)displayGroup().selectedObject();
	}

}

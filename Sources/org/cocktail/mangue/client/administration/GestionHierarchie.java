/* Gestion de la hiérarchie
 * Created on 21 avr. 2006
 *
*/
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.component.COTreeView;
import org.cocktail.component.utilities.CRITreeView;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;
import org.cocktail.mangue.modele.mangue.lolf.EOHierarchie;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;


/** Gestion de la hi&eacute;rarchie
 * @author christine
 *
 */
// 19/10/2010 - Adaptation Netbeans
// 15/12/2010 - modification de la taille de la vueAgents
public class GestionHierarchie extends ModelePageComplete {
	public COTreeView vueHierarchie;
	public EOView vueAgents;
	private EOHierarchie parent;
	private EOIndividuIdentite currentResponsable;
	private NSArray individusChoisis;
	
	// actions
	public void ajouterAgents() {
		if (individusChoisis != null) {
			parent = trouverParent();
			for (int i = 0; i < individusChoisis.count();i++) {
				Number individuCourantID = (Number)individusChoisis.objectAtIndex(i);
				ajouterIndividu(individuCourantID);
			}
			vueHierarchie.setObjetsGeres(displayGroup().allObjects());
			vueHierarchie.update(vueHierarchie.selectedPath());
			individusChoisis = null;
		}
	}
	// Notifications
	public void selectionHasChanged(NSNotification aNotif) {
		individusChoisis = (NSArray)aNotif.object();
		updaterDisplayGroups();
	}
    public void employeHasChanged(NSNotification aNotif) {
		if (!modificationEnCours()) {
			EOHierarchie hierarchie = null;
			if (aNotif.object() != null) {
				Number individuCourantID = (Number)aNotif.object();
				EOIndividuIdentite currentIndividu = EOIndividuIdentite.findForId(editingContext(),individuCourantID);
				hierarchie = trouverElementPourIndividu(currentIndividu);
			}
			vueHierarchie.selectObject(hierarchie);
		}
    }
    public void nettoyer(NSNotification aNotif) {
	    	if (!modificationEnCours())  {
	    		vueHierarchie.selectObject(null);
	    	}
    }
	/** Notification revue lors d'un simple click sur le tree view : une personne a &eacute;t&eacute; selectionn&eacute;e */
	public void notificationSimpleClic(NSNotification aNotif) {
		NSDictionary result = (NSDictionary)aNotif.userInfo();
		EOGenericRecord object = (EOGenericRecord)result.objectForKey("selectedRecord"); 
		if (object instanceof EOHierarchie) {		// cette notification est générale, elle peut venir d'autres treeView
			//EOHierarchie previousSelection = currentHierarchie();
			if (modificationEnCours() == false) {
				displayGroup().selectObject(object);
				if (currentHierarchie() != null) {
					currentResponsable = currentHierarchie().toIndividu();
				} else {
					currentResponsable = null;
				}
				updaterDisplayGroups();
				raffraichirAssociations();	// pour forcer le raffraîchissement du DG
			}
		}
	}
	// méthodes du controller DG
	public boolean peutAjouter() {
		return modeSaisiePossible() && (currentHierarchie() != null || displayGroup().allObjects().count() == 0);
	}
	public boolean peutAjouterAgents() {
		return modificationEnCours() && individusChoisis != null;
	}
	public boolean peutValider() {
		return modificationEnCours() && currentHierarchie().toIndividu() != null;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		ListeAgents controleur = new ListeAgents();
	 	controleur.initialiser(false,ManGUEConstantes.TOUT_PERSONNEL,null,false,true,true);		// support de la sélection multiple, le composant doit rester actif
	 	// 15/12/2010 - modification de la vueAgents
	 	controleur.preparerVuePourHierarchie();
	 	GraphicUtilities.swaperViewAvecVueLayout(vueAgents,controleur.component());
	 	controleur.activer();
		initVue();

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("notificationSimpleClic",new Class[] {NSNotification.class}), CRITreeView.TREE_VIEW_DID_CLICK,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("notificationSimpleClic",new Class[] {NSNotification.class}), CRITreeView.TREE_VIEW_DID_DOUBLE_CLICK,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyer",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("selectionHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_SELECTION,null);
	}
	protected void traitementsPourCreation() {
		if (currentHierarchie() != null && currentResponsable != null) {
			currentHierarchie().initAvecResponsable(currentResponsable);
		}
	}
	protected boolean traitementsPourSuppression() {
		parent = trouverParent();
		currentHierarchie().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}

	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer " + currentHierarchie().toIndividu().identiteInverse() + " de la hiérarchie ?";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"Hierarchie");
	}

	protected void parametrerDisplayGroup() {
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	protected void traitementsApresSuppression() {
		super.traitementsApresSuppression();
		vueHierarchie.setObjetsGeres(displayGroup().allObjects());
		if (parent != null) {
			vueHierarchie.selectObject(parent);
			vueHierarchie.update(vueHierarchie.selectedPath());
		} else {
			currentResponsable = null;
			vueHierarchie.update((javax.swing.tree.TreePath)null);
		}
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOHierarchie currentHierarchie() {
		return (EOHierarchie)displayGroup().selectedObject();
	}
	private void initVue() {
		vueHierarchie.setObjetsGeres(displayGroup().allObjects());
		try {
			vueHierarchie.initialize(editingContext(),"Hierarchie",false,false,false);
		} catch (Exception e) {
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite : " + e.getMessage());
		}
	}
	private EOHierarchie trouverParent() {
		if (currentHierarchie() == null) {
			return null;
		}
		java.util.Enumeration e = displayGroup().allObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOHierarchie hierarchie = (EOHierarchie)e.nextElement();
			if (hierarchie.toIndividu() == currentHierarchie().toIndividuResp()) {
				return hierarchie;
			}
		}
		return null;
	}
	private EOHierarchie trouverElementPourIndividu(EOIndividuIdentite individu) {
		java.util.Enumeration e = displayGroup().allObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOHierarchie hierarchie = (EOHierarchie)e.nextElement();
			if (hierarchie.toIndividu() == individu) {
				return hierarchie;
			}
		}
		return null;
	}
	private void ajouterIndividu(Number individuID) {
		EOIndividuIdentite currentIndividu = EOIndividuIdentite.findForId(editingContext(),individuID);
		if (currentIndividu != null) {
			if (((NSArray)displayGroup().allObjects().valueForKey("toIndividu")).containsObject(currentIndividu)) {
				EODialogs.runErrorDialog("Erreur",currentIndividu.identiteInverse() + " est déjà dans la hiérarchie");
			} else {
				if (currentHierarchie().toIndividu() != null) {		// on a déjà ajouté un individu, il faut donc en créer un nouveau
					insertObject();
					traitementsPourCreation();
				}
				currentHierarchie().addObjectToBothSidesOfRelationshipWithKey(currentIndividu,"toIndividu");
				
			} 
		}
	}
	
}

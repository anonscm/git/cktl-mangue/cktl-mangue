/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.administration;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.ParametresView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class ParametresCtrl extends ModelePageGestion {

	private static final long serialVersionUID = -6490623926473207900L;

	private static ParametresCtrl sharedInstance;

	private EODisplayGroup eod;
	private ParametresView myView;

	private ListenerParam listenerParam = new ListenerParam();

	private EOGrhumParametres	currentParametre;
	private EOAgentPersonnel currentUtilisateur;
	boolean peutGererModule;

	/** 
	 *
	 */
	public ParametresCtrl (EOEditingContext edc) {

		super(edc);

		eod = new EODisplayGroup();
		myView = new ParametresView(new JFrame(), true, eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());

		myView.getMyEOTable().addListener(listenerParam);
		CocktailUtilities.initPopupOuiNon(myView.getPopupCodeActivation(), false);
		CocktailUtilities.initTextField(myView.getTfDescription(), false, false);

		myView.getPopupTypeParametre().removeAllItems();
		myView.getPopupTypeParametre().addItem("*");
		myView.getPopupTypeParametre().addItem("Génériques");
		myView.getPopupTypeParametre().addItem("Congés");
		myView.getPopupTypeParametre().addItem("Modalités");

		myView.getPopupTypeParametre().addActionListener(new TypeParametreListener());
		myView.getTfFiltre().getDocument().addDocumentListener(new ADocumentListener());

		setCurrentUtilisateur(((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());
		
		CocktailUtilities.viderTextField(myView.getTfFiltre());
		updateInterface();	

	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		setPeutGererModule(currentUtilisateur.peutAdministrer());
	}
	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ParametresCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ParametresCtrl(editingContext);
		return sharedInstance;
	}



	public EOGrhumParametres getCurrentParametre() {
		return currentParametre;
	}

	public void setCurrentParametre(EOGrhumParametres currentParametre) {
		this.currentParametre = currentParametre;
		updateDatas();
	}

	public void open()	{

		setSaisieEnabled(false);				
		actualiser();

		myView.setVisible(true);
	}

	/**
	 * 
	 * @throws ValidationException
	 */
	protected void traitementsAvantValidation() throws ValidationException {

		if (getCurrentParametre().toTypeParametre().isValeurNumerique()) {
			if (CocktailUtilities.getIntegerFromField(myView.getTfValeur()) == null) {
				throw new ValidationException("Vous devez entrer une valeur numérique !");
			}
		}
		
		if (getCurrentParametre().toTypeParametre().isCodeActivation())
			getCurrentParametre().setParamValue((String)myView.getPopupCodeActivation().getSelectedItem());
		else {
			getCurrentParametre().setParamValue(CocktailUtilities.getTextFromField(myView.getTfValeur()));
			
			if (getCurrentParametre().paramKey().equals(ManGUEConstantes.PARAM_KEY_AFF_TYPE_CONTRAT)) {
				if (!new  NSArray(EOGrhumParametres.LISTE_PARAMS_TYPES_CONTRAT).contains(getCurrentParametre().paramValue())) {
						throw new ValidationException("Cette valeur n'est pas autorisée !");						
				}
			}
			if (getCurrentParametre().paramKey().equals(ManGUEConstantes.PARAM_KEY_NO_MATRICULE)) {
				if (!new  NSArray(EOGrhumParametres.LISTE_PARAMS_NO_MATRICULE).contains(getCurrentParametre().paramValue())) {
						throw new ValidationException("Cette valeur n'est pas autorisée !");						
				}
			}
			
		}

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerParam implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentParametre((EOGrhumParametres)eod.selectedObject());
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfCle());
		CocktailUtilities.viderTextField(myView.getTfValeur());
		CocktailUtilities.viderTextField(myView.getTfDescription());

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentParametre() != null) {

			CocktailUtilities.setTextToField(myView.getTfCle(), getCurrentParametre().paramKey());
			CocktailUtilities.setTextToField(myView.getTfDescription(), getCurrentParametre().paramCommentaires());

			if (getCurrentParametre().toTypeParametre().isCodeActivation())
				myView.getPopupCodeActivation().setSelectedItem(getCurrentParametre().paramValue());
			else
				CocktailUtilities.setTextToField(myView.getTfValeur(), getCurrentParametre().paramValue());

		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {

		myView.getBtnAjouter().setEnabled(false);
		myView.getBtnModifier().setEnabled(getCurrentParametre() != null && !isSaisieEnabled()  && peutGererModule());
		myView.getBtnSupprimer().setEnabled(false);

		CocktailUtilities.initTextField(myView.getTfCle(), false, getCurrentParametre() != null && isModeCreation());

		CocktailUtilities.initTextField(myView.getTfValeur(), false, isSaisieEnabled());
		myView.getPopupCodeActivation().setEnabled(isSaisieEnabled());

		myView.getTfValeur().setVisible(getCurrentParametre() != null && !getCurrentParametre().toTypeParametre().isCodeActivation());
		myView.getPopupCodeActivation().setVisible(getCurrentParametre() != null && getCurrentParametre().toTypeParametre().isCodeActivation());

		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());
		myView.getBtnValider().setEnabled(isSaisieEnabled());

	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		eod.setObjectArray(EOGrhumParametres.findParametresMangue(getEdc()));
		filter();
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		if (CocktailUtilities.getTextFromField(myView.getTfFiltre()) != null) {
			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrhumParametres.PARAM_KEY_KEY + " caseInsensitiveLike %@", new NSArray("*"+CocktailUtilities.getTextFromField(myView.getTfFiltre())+"*")));			
		}

		if (myView.getPopupTypeParametre().getSelectedIndex() == 1) {
			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("NOT (" + EOGrhumParametres.PARAM_KEY_KEY + " caseInsensitiveLike %@ )", new NSArray(EOGrhumParametres.TYPE_PARAMETRE_MANGUE_CONGES+"*")));
		}
		else
			if (myView.getPopupTypeParametre().getSelectedIndex() == 2)
				andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrhumParametres.PARAM_KEY_KEY + " caseInsensitiveLike %@", new NSArray(EOGrhumParametres.TYPE_PARAMETRE_MANGUE_CONGES + "*")));
			else
				if (myView.getPopupTypeParametre().getSelectedIndex() == 3)
					andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrhumParametres.PARAM_KEY_KEY + " caseInsensitiveLike %@", new NSArray(EOGrhumParametres.TYPE_PARAMETRE_MANGUE_MODALITES + "*")));

		return new EOAndQualifier(andQualifiers);       

	}

	private class TypeParametreListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			filter();
		}
	}
	protected void filter() {
		// TODO Auto-generated method stub
		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();
	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		if (isModeCreation()) {
			actualiser();
		}
		else {
			myView.getMyEOTable().updateUI();
		}
	}

	@Override
	protected void traitementsApresValidation() {
		EOGrhumParametres.initParametres(getEdc());
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		getEdc().revert();
		setCurrentParametre(null);
		setSaisieEnabled(false);
		listenerParam.onSelectionChanged();

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentParametre(EOGrhumParametres.creer(getEdc()));
		setSaisieEnabled(true);

	}

	@Override
	protected void traitementsPourSuppression() {
		getEdc().deleteObject(getCurrentParametre());
	}

	@Override
	protected void traitementsApresSuppression() {

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub

	}
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}
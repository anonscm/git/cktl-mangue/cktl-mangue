/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import java.awt.Font;
import java.io.File;

import javax.swing.JFileChooser;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.AideUtilisateur;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.component.COComboBox;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOParamPromotion;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTextArea;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
/** Saisie des parametres pour les promotions : si le corps d'arrivee est fourni, on force le grade d'arrivee comme le 1er grade 
 * de ce corps. Les grades d'arrivee sont toujours d&eacute;finis, ils permettent ainsi de retrouver tous les parametres de
 * promotion qui concernent une meme promotion. La coh&eacute;rence des corps et grades est verifiee<BR>
 * Le couple (code, grade d'arrivee) doit etre unique pour un meme type de parametre. Cette fonctionnalite est
 * utilisee dans la gestion des promotions pour identifier tous les parametres ayant trait a une meme promotion.
 *
 */

public class GestionParametresPromotion extends ModelePageComplete {
	public EOTextArea vueTexte;
	public EODisplayGroup displayGroupCategories;
	public COComboBox popupType;
	private String typePromotionCourt;
	private boolean preparationEnCours,peutAjouterEchelon;
	private boolean gereHospitaloUniv, gereENS;

	// Accesseurs
	public String typePromotion() {
		if (preparationEnCours || typePromotionCourt == null) {
			return null;
		}

		return EOParamPromotion.typePromotionLongPourTypeCourt(typePromotionCourt);
	}
	public void setTypePromotion(String aStr) {
		if (preparationEnCours) {
			return;
		}
		typePromotionCourt = EOParamPromotion.typePromotionCoursPourTypeLong(aStr);

		displayGroup().setObjectArray(fetcherObjets());
		displayGroup().updateDisplayedObjects();
	}
	public String lgReference() {
		if (currentParametre() == null || currentParametre().parpRefReglementaire() == null) {
			return "";
		} else {
			return "" + currentParametre().parpRefReglementaire().length() + "/2000";
		}
	}
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		peutAjouterEchelon = currentParametre() != null && currentParametre().parpDOuverture() != null &&
		currentParametre().gradeDepart() != null && existePassageEchelon(false);
		super.displayGroupDidChangeSelection(group);
	}
	public void displayGroupDidSetValueForObject(EODisplayGroup group, Object value, Object eo, String key) {
		if (group == displayGroup()) {
			if (key.equals("cEchelon")) {
				if (currentParametre().cEchelon() == null) {
					currentParametre().setParpDureeEchelon(null);
					currentParametre().setParpDureeMaxAncienneteCons(null);
				} else if (currentParametre().cEchelon().length() == 1) {
					currentParametre().setCEchelon("0" + value);
				}
			} else if (key.equals("cEchelonArrivee") && (currentParametre().cEchelonArrivee().length() == 1)) {
				currentParametre().setCEchelonArrivee("0" + value);
			}
		}
	}		
	// Actions
	public void afficherAide() {
		LogManager.logDetail("GestionParametresPromotion - afficherAide");
		AideUtilisateur controleur = new AideUtilisateur("Aide_GestionPromotions",null);
		controleur.afficherFenetre();
	}
	public void afficherAideCategorie() {
		EODialogs.runInformationDialog("Aide", "Pour une remise à zéro des catégories, choisir la catégorie Z (sans catégorie)");
	}
	public void afficherPopulationDepart() {
		LogManager.logDetail("GestionParametresPromotion - afficherPopulationDepart");
		NSNotificationCenter.defaultCenter().removeObserver(this,UtilitairesDialogue.nomNotificationPourCallerEtEntite(this, EOTypePopulation.ENTITY_NAME), null);	
		NSMutableArray qualifiers = new NSMutableArray();
		// 16/02/2011
		if (!gereHospitaloUniv) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temHospitalier = 'N'", null));
		}
		if (!gereENS) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.CODE_KEY + " != 'N'",null));
		}
		if (currentParametre().corpsDepart() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.CODE_KEY + " = %@", new NSArray(currentParametre().corpsDepart().toTypePopulation().code())));
		} else if (currentParametre().gradeDepart() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.CODE_KEY + " = %@", new NSArray(currentParametre().gradeDepart().toCorps().toTypePopulation().code())));
		}
		UtilitairesDialogue.afficherDialogue(this, EOTypePopulation.ENTITY_NAME, "getPopulationDepart",false,new EOAndQualifier(qualifiers),false);
	}
	public void afficherCorpsDepart() {
		LogManager.logDetail("GestionParametresPromotion - afficherCorpsDepart");
		afficherCorps(true);
	}
	public void afficherGradeDepart() {
		LogManager.logDetail("GestionParametresPromotion - afficherGradeDepart");
		afficherGrade(true);
	}
	public void afficherCorpsArrivee() {
		LogManager.logDetail("GestionParametresPromotion - afficherCorpsArrivee");
		afficherCorps(false);
	}
	public void afficherGradeArrivee() {
		LogManager.logDetail("GestionParametresPromotion - afficherGradeArrivee");
		afficherGrade(false);
	}
	public void afficherPopulation() {
		LogManager.logDetail("GestionParametresPromotion - afficherPopulation");
		NSNotificationCenter.defaultCenter().removeObserver(this,UtilitairesDialogue.nomNotificationPourCallerEtEntite(this, "TypePopulation"), null);	// pour le cas où l'utilisateur aurait annulé un précédent dialogue de type de population
		// 16/02/2011
		EOQualifier qualifier = null;
		if (!gereHospitaloUniv) {
			qualifier = EOQualifier.qualifierWithQualifierFormat("temHospitalier = 'N'", null);
		}
		UtilitairesDialogue.afficherDialogue(this, EOTypePopulation.ENTITY_NAME, "getPopulation",false,qualifier,false);
	}
	public void supprimerPopulationDepart() {
		LogManager.logDetail("GestionParametresPromotion - supprimerPopulationDepart");
		currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().typePopulationDepart(), "typePopulationDepart");
	}
	public void supprimerCorpsDepart() {
		LogManager.logDetail("GestionParametresPromotion - supprimerCorpsDepart");
		currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().corpsDepart(),"corpsDepart");
		currentParametre().setParpDureeCorps(null);
	}
	public void supprimerGradeDepart() {
		LogManager.logDetail("GestionParametresPromotion - supprimerGradeDepart");
		currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().gradeDepart(),"gradeDepart");
		currentParametre().setCEchelon(null);
		currentParametre().setParpDureeEchelon(null);
		currentParametre().setParpDureeGrade(null);
		if (currentParametre().cCategorieServEffectifs() == null && currentParametre().parpDureeServEffectifs() != null) {
			currentParametre().setParpDureeServEffectifs(null);
		}
		updaterDisplayGroups();
	}
	public void supprimerCorpsArrivee() {
		LogManager.logDetail("GestionParametresPromotion - supprimerCorpsArrivee");
		currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().corpsArrivee(),"corpsArrivee");
	}
	public void supprimerGradeArrivee() {
		LogManager.logDetail("GestionParametresPromotion - supprimerGradeArrivee");
		currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().gradeArrivee(),"gradeArrivee");
	}
	public void supprimerPopulation() {
		LogManager.logDetail("GestionParametresPromotion - supprimerPopulation");
		currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().typePopulationServPublics(), "typePopulationServPublics");
	}
	public void exporterParametres() {
		LogManager.logDetail("GestionParametresPromotion - exporter");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(texteExport(),file.getParent(),file.getName());
		}
	}
	// Notifications
	public void getPopulationDepart(NSNotification aNotif) {
		ajouterRelation(currentParametre(), aNotif.object(), "typePopulationDepart");
		if (currentParametre().corpsDepart() != null && currentParametre().corpsDepart().toTypePopulation() != currentParametre().typePopulationDepart()) {
			EODialogs.runErrorDialog("Erreur", "Le type de population et le corps de départ ne sont pas cohérents");
			currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().typePopulationDepart(), "typePopulationDepart");
			updaterDisplayGroups();
		}
	}

	public void getCorpsArrivee(NSNotification aNotif) {
		ajouterRelation(currentParametre(), aNotif.object(), "corpsArrivee");
		if (currentParametre().gradeArrivee() == null) {
			EOGrade gradeArrivee = EOGrade.rechercherGradeMinimumPourCorps(editingContext(), currentParametre().corpsArrivee().cCorps(),currentParametre().parpDOuverture());
			if (gradeArrivee != null) {
				currentParametre().addObjectToBothSidesOfRelationshipWithKey(gradeArrivee, "gradeArrivee");
			}
			updaterDisplayGroups();
		} else {
			if (currentParametre().gradeArrivee() != null && currentParametre().gradeArrivee().toCorps() != currentParametre().corpsArrivee()) {
				EODialogs.runErrorDialog("Erreur", "Le corps et le grade d'arrivée ne sont pas cohérents");
				currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().corpsArrivee(), "corpsArrivee");
				updaterDisplayGroups();
			}
		}
	}

	public void getPopulation(NSNotification aNotif) {
		ajouterRelation(currentParametre(), aNotif.object(), "typePopulationServPublics");
	}
	// Méthodes du controller DG
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && typePromotion() != null;
	}
	public boolean peutAfficherCorpsOuGrade() {
		return modificationEnCours() && currentParametre().parpDOuverture() != null;
	}
	public boolean peutChoisirEchelon() {
		return peutAfficherCorpsOuGrade() && currentParametre().gradeDepart() != null && peutAjouterEchelon;
	}
	public boolean peutSaisirDureeCorps() {
		return peutAfficherCorpsOuGrade() && currentParametre().corpsDepart() != null;
	}
	public boolean peutSaisirDureeGrade() {
		return peutAfficherCorpsOuGrade() && currentParametre().gradeDepart() != null;
	}
	public boolean peutSaisirDureeEchelon() {
		return peutAfficherCorpsOuGrade() && currentParametre().cEchelon() != null;
	}
	public boolean peutSaisirDureeMaxEchelon() {
		return peutAfficherCorpsOuGrade() && currentParametre().cEchelon() != null && typePromotionCourt.equals("EC");
	}
	public boolean peutSaisirChevronDepart() {
		return peutAfficherCorpsOuGrade() && currentParametre().cEchelon() != null && typePromotionCourt.equals("EC");
	}
	public boolean peutSaisirChevronArrivee() {
		return peutAfficherCorpsOuGrade() && currentParametre().cEchelonArrivee() != null && typePromotionCourt.equals("EC");
	}
	public boolean peutSupprimerCorpsArrivee() {
		return peutAfficherCorpsOuGrade() && currentParametre().corpsArrivee() != null;
	}
	public boolean peutSupprimerCorpsDepart() {
		return peutAfficherCorpsOuGrade() && currentParametre().corpsDepart() != null;
	}
	public boolean peutSupprimerGradeArrivee() {
		return peutAfficherCorpsOuGrade() && currentParametre().gradeArrivee() != null && currentParametre().corpsArrivee() == null;
	}
	public boolean peutSupprimerGradeDepart() {
		return peutAfficherCorpsOuGrade() && currentParametre().gradeDepart() != null;
	}
	public boolean peutSupprimerPopulationDepart() {
		return peutAfficherCorpsOuGrade() && currentParametre().typePopulationDepart() != null;
	}
	public boolean peutSupprimerPopulation() {
		return peutAfficherCorpsOuGrade() && currentParametre().typePopulationServPublics() != null;
	}
	public boolean peutSaisirDureeCategSvcPublic() {
		return peutAfficherCorpsOuGrade() && (currentParametre().cCategorieServPublics() != null || currentParametre().typePopulationServPublics() != null);
	}
	public boolean peutValider() {
		return super.peutValider() && currentParametre().parpType() != null && currentParametre().parpDOuverture() != null &&
		currentParametre().parpCode() != null && currentParametre().gradeArrivee() != null; 
	}
	// méthodes protégées
	protected void preparerFenetre() {
		preparationEnCours = true;
		typePromotionCourt = "LA";

		gereHospitaloUniv = EOGrhumParametres.isGestionHu();
		gereENS = EOGrhumParametres.isGestionEns();
		
		Font font = vueTexte.getFont();
		vueTexte.setFont(new Font(font.getFontName(),font.getStyle(),11));
		displayGroupCategories.setObjectArray(SuperFinder.rechercherEntite(editingContext(), EOCategorie.ENTITY_NAME));
		displayGroupCategories.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey( EOCategorie.CODE_KEY, EOSortOrdering.CompareAscending)));
		displayGroupCategories.updateDisplayedObjects();
		super.preparerFenetre();
		preparationEnCours = false;
		setTypePromotion((String)popupType.getItemAt(0));	// pour afficher les paramètres
	}
	protected void traitementsPourCreation() {
		currentParametre().initAvecType(typePromotionCourt);	// on prend la variable car elle contient les libellés courts
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur", message);
	}
	protected boolean conditionsOKPourFetch() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(), editingContext());
		return agent.peutGererPromouvabilites() && agent.peutAdministrer() && typePromotion() != null;
	}
	protected NSArray fetcherObjets() {
		if (typePromotion() == null) {
			return null;
		}
		// 01/03/2011 - ne pas garder les paramètres liés aux type de population HU de population HU si pas d'HU
		NSArray params = EOParamPromotion.rechercherParametresPourType(editingContext(),typePromotionCourt); // on prend la variable car elle contient les libellés courts;
		NSMutableArray paramsAGarder = new NSMutableArray();
		java.util.Enumeration e = params.objectEnumerator();
		while (e.hasMoreElements()) {
			EOParamPromotion param = (EOParamPromotion)e.nextElement();
			boolean shouldKeep = true;
			if (!gereHospitaloUniv) {
				if ((param.typePopulationDepart() != null && param.typePopulationDepart().estHospitalier()) ||
						(param.corpsDepart() != null && param.corpsDepart().toTypePopulation().estHospitalier()) ||
						(param.corpsArrivee() != null && param.corpsArrivee().toTypePopulation().estHospitalier()) ||
						(param.gradeDepart() != null && param.gradeDepart().toCorps().toTypePopulation().estHospitalier()) ||
						(param.gradeArrivee() != null && param.gradeArrivee().toCorps().toTypePopulation().estHospitalier())) {
					shouldKeep = false;
				}
			}	
			if (shouldKeep && !gereENS) {
				if ((param.typePopulationDepart() != null && param.typePopulationDepart().estNormalien()) ||
						(param.corpsDepart() != null && param.corpsDepart().toTypePopulation().estNormalien()) ||
						(param.corpsArrivee() != null && param.corpsArrivee().toTypePopulation().estNormalien()) ||
						(param.gradeDepart() != null && param.gradeDepart().toCorps().toTypePopulation().estNormalien()) ||
						(param.gradeArrivee() != null && param.gradeArrivee().toCorps().toTypePopulation().estNormalien())) {
					shouldKeep = false;
				}
			}
			if (shouldKeep) {
				paramsAGarder.addObject(param);
			}
		}
		return new NSArray(paramsAGarder);
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce paramètre ?";
	}
	protected void parametrerDisplayGroup() {
		// Masquer les paramètres invalides
		displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("parpTemValide = %@", new NSArray(CocktailConstantes.VRAI)));
		// Trier par code et par grade/échelon d'arrivée desc
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("parpCode", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("gradeArrivee.cGrade", EOSortOrdering.CompareAscending));
		sorts.addObjectsFromArray(EOParamPromotion.triParametres(""));
		displayGroup().setSortOrderings(sorts);
	}
	protected boolean traitementsAvantValidation() {
		// Vérifier si le code du paramètre est unique ou si il correspond à un autre paramètre ayant le même grade d'arrivée et réciproquement
		// si il existe un autre code pour le même grade d'arrivée
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOParamPromotion parametre = (EOParamPromotion)e.nextElement();
			if (parametre != currentParametre()) {
				if (parametre.parpCode().equals(currentParametre().parpCode()) && parametre.gradeArrivee() != currentParametre().gradeArrivee()) {
					EODialogs.runErrorDialog("Erreur", "Il existe un autre paramètre avec le même code et un grade d'arrivée différent, veuillez changer le code");
					return false;
				}
				if (parametre.gradeArrivee() == currentParametre().gradeArrivee() && parametre.parpCode().equals(currentParametre().parpCode()) == false) {
					boolean result = EODialogs.runConfirmOperationDialog("Erreur", "Il existe un paramètre avec un code différent pour le même grade d'arrivée. Voulez-vous remplacer le code par cet autre code ?", "Oui","Non");
					if (result) {
						currentParametre().setParpCode(parametre.parpCode());
					} else {
						return false;
					}
				}
			}
		}
		// Etablir tous les booléens en fonction de ce qui est saisi
		currentParametre().setUtiliserCategorie(currentParametre().cCategorieServEffectifs() != null || currentParametre().cCategorieServPublics() != null);
		currentParametre().setAConditionServiceEffectif(currentParametre().cCategorieServEffectifs() != null || currentParametre().parpDureeServEffectifs() != null);
		currentParametre().setAConditionServicePublic(currentParametre().cCategorieServPublics() != null || currentParametre().parpDureeServPublics() != null);
		currentParametre().setUtiliserCorps(currentParametre().corpsDepart() != null || currentParametre().corpsArrivee() != null);
		currentParametre().setUtiliserGrade(currentParametre().gradeDepart() != null || currentParametre().gradeArrivee() != null);
		currentParametre().setUtiliserEchelon(currentParametre().cEchelon() != null);
		currentParametre().setExisteCondition(currentParametre().utiliserCategorie() || currentParametre().utiliserCorps() ||
				currentParametre().utiliserGrade() || currentParametre().utiliserEchelon() ||
				currentParametre().aConditionServiceEffectif() || currentParametre().aConditionServicePublic());
		return true;
	}
	/** On invalide les param&egrave;tres */
	protected boolean traitementsPourSuppression() {
		currentParametre().invalider();
		return true;
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOParamPromotion currentParametre() {
		return (EOParamPromotion)displayGroup().selectedObject();
	}

	private void afficherCorps(boolean estDepart) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOCorps.D_OUVERTURE_CORPS_KEY, currentParametre().parpDOuverture(), EOCorps.D_FERMETURE_CORPS_KEY, currentParametre().parpDFermeture()));

		qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOCorps.TO_TYPE_POPULATION_KEY+"."+EOTypePopulation.DATE_OUVERTURE_KEY, currentParametre().parpDOuverture(),EOCorps.TO_TYPE_POPULATION_KEY+"."+EOTypePopulation.DATE_FERMETURE_KEY, currentParametre().parpDFermeture()));

		if (estDepart) {
			if (currentParametre().gradeDepart() != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.C_CORPS_KEY + "=%@", new NSArray(currentParametre().gradeDepart().toCorps().cCorps())));
			} else if (currentParametre().typePopulationDepart() != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.TO_TYPE_POPULATION_KEY + "=%@", new NSArray(currentParametre().typePopulationDepart())));
			}
		} else {
			if (currentParametre().gradeArrivee() != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.C_CORPS_KEY + " = %@", new NSArray(currentParametre().gradeArrivee().toCorps().cCorps())));
		}

		if (!gereHospitaloUniv) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.TO_TYPE_POPULATION_KEY+"."+EOTypePopulation.TEM_HOSPITALIER_KEY +  "=%@", new NSArray(CocktailConstantes.FAUX)));
		}
		if (!gereENS) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.TO_TYPE_POPULATION_KEY+"."+EOTypePopulation.CODE_KEY +  "!=%@", new NSArray("N")));
		}

		EOCorps corps = CorpsSelectCtrl.sharedInstance(editingContext()).getCorpsPourQualifier(new EOAndQualifier(qualifiers));

		if (corps != null) {

			if (estDepart) {
				
				currentParametre().setCorpsDepartRelationship(corps);				
				if (currentParametre().typePopulationDepart() != null) {
					if (currentParametre().corpsDepart().toTypePopulation() != currentParametre().typePopulationDepart()) {
						EODialogs.runErrorDialog("Erreur", "Le corps et le type de population de départ ne sont pas cohérents");
						currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().corpsDepart(), "corpsDepart");
					}
				} else {
					currentParametre().addObjectToBothSidesOfRelationshipWithKey(currentParametre().corpsDepart().toTypePopulation(), "typePopulationDepart");
				}
				if (currentParametre().gradeDepart() != null && currentParametre().gradeDepart().toCorps() != currentParametre().corpsDepart()) {
					EODialogs.runErrorDialog("Erreur", "Le corps et le grade de départ ne sont pas cohérents");
					currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().corpsDepart(), "corpsDepart");
				}
				updaterDisplayGroups();

			}
			else {
				currentParametre().setCorpsArriveeRelationship(corps);

				if (currentParametre().gradeArrivee() == null) {
					EOGrade gradeArrivee = EOGrade.rechercherGradeMinimumPourCorps(editingContext(), currentParametre().corpsArrivee().cCorps(),currentParametre().parpDOuverture());
					if (gradeArrivee != null) {
						currentParametre().addObjectToBothSidesOfRelationshipWithKey(gradeArrivee, "gradeArrivee");
					}
					updaterDisplayGroups();
				} else {
					if (currentParametre().gradeArrivee() != null && currentParametre().gradeArrivee().toCorps() != currentParametre().corpsArrivee()) {
						EODialogs.runErrorDialog("Erreur", "Le corps et le grade d'arrivée ne sont pas cohérents");
						currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().corpsArrivee(), "corpsArrivee");
						updaterDisplayGroups();
					}
				}
			}
		}
	}

	private void afficherGrade(boolean estDepart) {
		
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOGrade.D_OUVERTURE_KEY, currentParametre().parpDOuverture(), EOGrade.D_FERMETURE_KEY, currentParametre().parpDFermeture()));

		if (estDepart) {
			if (currentParametre().typePopulationDepart() != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.TO_CORPS_KEY+"."+EOCorps.TO_TYPE_POPULATION_KEY + "=%@", new NSArray(currentParametre().typePopulationDepart())));
			if (currentParametre().corpsDepart() != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.TO_CORPS_KEY + "=%@", new NSArray(currentParametre().corpsDepart())));
		} else {
			if (currentParametre().corpsArrivee() != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.TO_CORPS_KEY + "=%@", new NSArray(currentParametre().corpsArrivee())));
		}

		if (!gereHospitaloUniv) {			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.TO_CORPS_KEY+".toTypePopulation.temHospitalier = 'N'", null));
		}
		if (!gereENS) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.TO_CORPS_KEY+".toTypePopulation.code != 'N'",null));
		}

		// Selection du grade
		EOGrade grade = GradeSelectCtrl.sharedInstance(editingContext()).getGradePourQualifier(new EOAndQualifier(qualifiers));

		if (grade != null) {
			if (estDepart) {	// GRADE DEPART
				currentParametre().setGradeDepartRelationship(grade);
				currentParametre().setCorpsDepartRelationship(grade.toCorps());
				currentParametre().setTypePopulationDepartRelationship(grade.toCorps().toTypePopulation());			
//				if (currentParametre().corpsDepart() != null) {
//					if (currentParametre().gradeDepart().toCorps() != currentParametre().corpsDepart()) {
//						EODialogs.runErrorDialog("Erreur", "Le corps et le grade de départ ne sont pas cohérents");
//						currentParametre().setGradeDepartRelationship(null);
//					}
//				} else {
//					currentParametre().setCorpsDepartRelationship(currentParametre().gradeDepart().toCorps());
//					if (currentParametre().typePopulationDepart() != null) {
//						if (currentParametre().corpsDepart().toTypePopulation() != currentParametre().typePopulationDepart()) {
//							currentParametre().setCorpsDepartRelationship(null);
//						}
//					} else {
//						currentParametre().setTypePopulationDepartRelationship(currentParametre().corpsDepart().toTypePopulation());
//					}
//				}
				updaterDisplayGroups();
				if (currentParametre().gradeDepart() != null) {
					peutAjouterEchelon = existePassageEchelon(true);
				}				
			}
			else {	// GRADE ARRIVEE
				currentParametre().setGradeArriveeRelationship(grade);
				currentParametre().setCorpsArriveeRelationship(grade.toCorps());
//				if (currentParametre().corpsArrivee() != null) {
//					if (currentParametre().gradeArrivee().toCorps() != currentParametre().corpsArrivee()) {
//						EODialogs.runErrorDialog("Erreur", "Le corps et le grade d'arrivée ne sont pas cohérents");
//						currentParametre().setGradeArriveeRelationship(null);
//					} else {
//						currentParametre().setCorpsArriveeRelationship(currentParametre().gradeArrivee().toCorps());
//					}
					updaterDisplayGroups();
//				}
			}
		}		
	}
	
	private boolean existePassageEchelon(boolean estAjoutGrade) {
		NSArray passagesEchelon = EOPassageEchelon.rechercherPassagesEchelonPourGradesValidesPourPeriode(editingContext(), currentParametre().gradeDepart(), currentParametre().parpDOuverture(), currentParametre().parpDFermeture());
		if (passagesEchelon.count() > 0) {
			if (estAjoutGrade) {
				// Déterminer l'échelon maximum
				passagesEchelon = EOSortOrdering.sortedArrayUsingKeyOrderArray(passagesEchelon, new NSArray(EOSortOrdering.sortOrderingWithKey("cEchelon", EOSortOrdering.CompareDescending)));
				currentParametre().setCEchelon(((EOPassageEchelon)passagesEchelon.objectAtIndex(0)).cEchelon());
			}
			return true;
		} else {
			return false;
		}
	}
	private String texteExport() {
		String texte = typePromotion() + "\n";
		String header = "Paramètre\tGrade Arrivée\tGA\tCorps Arrivée\tCA\tGrade Départ\tGD\tCorps Départ\tCD\tConditions\tEchelon\tDurée Echelon\tAncien. Grade\tSvc Eff Grade\tSvc Pub\tCatég Svc Pub\tType Pop Svc Pub\tDurée Catég Svc Pub\tSvc Eff\tCatég Sv Eff\tDate Ouverture\tDate Fermeture\n";
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("gradeArrivee.toCorps.toTypePopulation.code", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("parpCode", EOSortOrdering.CompareAscending));
		NSArray params = EOSortOrdering.sortedArrayUsingKeyOrderArray(displayGroup().displayedObjects(), sorts);
		EOTypePopulation typePopulation = null;
		// Identifier les types de population
		java.util.Enumeration e = params.objectEnumerator();
		while (e.hasMoreElements()) {
			EOParamPromotion param = (EOParamPromotion)e.nextElement();
			if (typePopulation == null || typePopulation != param.gradeArrivee().toCorps().toTypePopulation()) {
				typePopulation = param.gradeArrivee().toCorps().toTypePopulation();
				texte += "\nType Population - " + typePopulation.libelleCourt() + "\n" + header;
			}
			texte += param.parpCode() + "\t" + param.gradeArrivee().lcGrade() + "\t" + param.gradeArrivee().cGrade() + "\t";
			if (param.corpsArrivee() != null) {
				texte += param.corpsArrivee().lcCorps() + "\t" + param.corpsArrivee().cCorps();
			} else {
				texte += "\t";
			}
			texte += "\t";
			if (param.gradeDepart() != null) {
				texte += param.gradeDepart().lcGrade() + "\t" + param.gradeDepart().cGrade();
			} else {
				texte += "\t";
			}
			texte += "\t";
			if (param.corpsDepart() != null) {
				texte += param.corpsDepart().lcCorps() + "\t" + param.corpsDepart().cCorps();
			} else {
				texte += "\t";
			}
			texte += "\t" + param.description() + "\t";
			if (param.cEchelon() != null) {
				texte += param.cEchelon();
			} 
			texte += "\t";
			if (param.parpDureeEchelon() != null) {
				texte += param.parpDureeEchelon();
			} 
			texte += "\t";
			if (param.parpDureeGrade() != null) {
				texte += param.parpDureeGrade();
			} 
			texte += "\t";
			if (param.parpDureeServEffectifs() != null && param.cCategorieServEffectifs() == null && param.typePopulationServPublics() == null) {
				// Service effectif dans le grade
				texte += param.parpDureeServEffectifs();
			}
			texte += "\t";
			if (param.parpDureeServPublics() != null) {
				texte += param.parpDureeServPublics();
			} 
			texte += "\t";
			if (param.cCategorieServPublics() != null) {
				texte += param.cCategorieServPublics();
			} 
			texte += "\t";
			if (param.typePopulationServPublics() != null) {
				texte += param.typePopulationServPublics().code();
			} 
			texte += "\t";
			if (param.parpDureeCategServPublics() != null) {
				texte += param.parpDureeCategServPublics();
			} 
			texte += "\t";
			// Service effectif de catégorie ou de population
			if (param.parpDureeServEffectifs() != null && (param.cCategorieServEffectifs() != null || param.typePopulationServPublics() != null)) {
				texte += param.parpDureeServEffectifs();
				texte += "\t";
				if (param.cCategorieServEffectifs() != null) {
					texte += param.cCategorieServEffectifs();
				}
				texte += "\t";
			} else {
				texte += "\t\t";
			}
			texte += DateCtrl.dateToString(param.parpDOuverture()) + "\t";
			if (param.parpDFermeture() != null) {
				texte += DateCtrl.dateToString(param.parpDFermeture());
			}
			texte += "\n";
		}
		return texte;
	}

}

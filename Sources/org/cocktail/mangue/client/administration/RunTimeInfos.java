
//RunTimeInfos.java
//Mangue

//Created by Christine Buttin on Tue Mar 28 2006.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.


/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.administration;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.outils_interface.InterfaceAvecFenetre;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class RunTimeInfos extends InterfaceAvecFenetre {
	private String nomBase;
	private EOAgentPersonnel agent;
	private EOIndividu employe;
	private Number individuCourantID;

	public RunTimeInfos(String nomBase,Number individuCourantID,String titreFenetre) {
		super(titreFenetre);
		this.nomBase = nomBase;
		this.individuCourantID = individuCourantID;
	}

	public String versionCourante() {
		return "Mangue " + " - " + ServerProxy.clientSideRequestAppVersion(editingContext()) + " - Base : " + nomBase();
	}
	public String nomBase() {
		return nomBase;
	}
	public String jreVersion() {
		return "JRE " + System.getProperty("java.version");
	}
	public String identite() {
		if (agent != null) {
			return agent.toIndividu().identite();
		} else {
			return null;
		}
	}
	public EOIndividu employe() {
		return employe;
	}
	public void connectionWasEstablished() {
		agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		if (individuCourantID != null) {
			employe = EOIndividu.rechercherIndividuAvecID(editingContext(),individuCourantID,false);
		} else {
			employe = null;
		}
		loadNotifications();
	}
	public void connectionWasBroken() {
		super.connectionWasBroken();
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}
	// Notifications
	public void nettoyer(NSNotification aNotif) {
		employe = null;
		controllerDisplayGroup().redisplay();
	}
	public void employeHasChanged(NSNotification aNotif) {
		if (aNotif.object() == null) {
			employe = null;
		} else {
			individuCourantID = (Number)aNotif.object();
			employe = EOIndividu.rechercherIndividuAvecID(editingContext(),individuCourantID,false);
		}
		controllerDisplayGroup().redisplay();
	}
	
	public boolean nonModifiable()
	{
		return false;
	}
	// méthodes privées
	private void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("employeHasChanged", new Class[] { NSNotification.class }), ListeAgents.CHANGER_EMPLOYE, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("nettoyer", new Class[] { NSNotification.class }), ListeAgents.NETTOYER_CHAMPS, null);

	}

}

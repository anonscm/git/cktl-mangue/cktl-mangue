/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.gpeec;

import java.awt.BorderLayout;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.mangue.common.utilities.CocktailIcones;

import com.webobjects.eointerface.EODisplayGroup;

/**
 * Classe de gestion de l'interface des spécialisations d'un emploi (liste + détail de l'affichage 
 * en bas de page en fonction de la catégorie d'emploi)
 *
 * @author  cpinsard
 * @author Chama LAATIK
 */
public class EmploiSpecialisationGestionView extends javax.swing.JDialog {
	
	private static final long serialVersionUID = -5164766421999011561L;
	
	protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;
	
	
	/** Creates new form TemplateJDialog 
	 * @param parent : parent de la fenetre
	 * @param modal : modal? oui/non
	 * @param dg : displayGroup
	 */
    public EmploiSpecialisationGestionView(java.awt.Frame parent, boolean modal, EODisplayGroup dg) {
        super(parent, modal);
        
        eod = dg;
        
        initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        viewTable = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnValider = new javax.swing.JButton();
        tfDateDebut = new javax.swing.JTextField();
        tfDateFin = new javax.swing.JTextField();
        btnAnnuler = new javax.swing.JButton();
        swapView = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnAjouter = new javax.swing.JButton();
        btnModifier = new javax.swing.JButton();
        btnSupprimer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Saisie / Modification des spécialisations");
        setResizable(false);

        org.jdesktop.layout.GroupLayout viewTableLayout = new org.jdesktop.layout.GroupLayout(viewTable);
        viewTable.setLayout(viewTableLayout);
        viewTableLayout.setHorizontalGroup(
            viewTableLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 632, Short.MAX_VALUE)
        );
        viewTableLayout.setVerticalGroup(
            viewTableLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 191, Short.MAX_VALUE)
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnValider.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnValider.setText("Valider");
        btnValider.setToolTipText("Valider la saisie");

        tfDateDebut.setEditable(false);
        tfDateDebut.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tfDateDebut.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateDebut.setToolTipText("");
        tfDateDebut.setNextFocusableComponent(tfDateFin);

        tfDateFin.setEditable(false);
        tfDateFin.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tfDateFin.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateFin.setToolTipText("");

        btnAnnuler.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnAnnuler.setText("Annuler");
        btnAnnuler.setToolTipText("Annuler la saisie");

        swapView.setLayout(new java.awt.CardLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Fin");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Début");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, swapView, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 638, Short.MAX_VALUE)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(9, 9, 9)
                        .add(jLabel1)
                        .add(18, 18, 18)
                        .add(tfDateDebut, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 104, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfDateFin, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 107, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createSequentialGroup()
                        .add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(btnValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(tfDateDebut, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel1)
                    .add(jLabel2)
                    .add(tfDateFin, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(13, 13, 13)
                .add(swapView, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                .add(18, 18, 18)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnAnnuler)
                    .add(btnValider))
                .addContainerGap())
        );

        btnAjouter.setToolTipText("Ajout d'une nouvelle période");

        btnModifier.setToolTipText("Modification");

        btnSupprimer.setToolTipText("Suppression");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-696)/2, (screenSize.height-427)/2, 696, 427);
    }// </editor-fold>//GEN-END:initComponents

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	EmploiSpecialisationGestionView dialog = new EmploiSpecialisationGestionView(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjouter;
    private javax.swing.JButton btnAnnuler;
    private javax.swing.JButton btnModifier;
    private javax.swing.JButton btnSupprimer;
    private javax.swing.JButton btnValider;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel swapView;
    private javax.swing.JTextField tfDateDebut;
    private javax.swing.JTextField tfDateFin;
    private javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

    
    private void initGui() {
        btnAjouter.setIcon(CocktailIcones.ICON_ADD);
    	btnModifier.setIcon(CocktailIcones.ICON_UPDATE);
    	btnSupprimer.setIcon(CocktailIcones.ICON_DELETE);

    	btnValider.setIcon(CocktailIcones.ICON_VALID);
    	btnAnnuler.setIcon(CocktailIcones.ICON_CANCEL);
    	
    	Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

    	ZEOTableModelColumn	col = new ZEOTableModelColumn(eod, EOEmploiSpecialisation.DATE_DEBUT_KEY, "Début", 75);
    	col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOEmploiSpecialisation.DATE_FIN_KEY, "Fin", 75);
    	col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOEmploiSpecialisation.LIBELLE_SPEC_KEY, "Spécialisation", 250);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);

    	myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

    }

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	public javax.swing.JButton getBtnAjouter() {
		return btnAjouter;
	}

	public void setBtnAjouter(javax.swing.JButton btnAjouter) {
		this.btnAjouter = btnAjouter;
	}

	public javax.swing.JButton getBtnAnnuler() {
		return btnAnnuler;
	}

	public void setBtnAnnuler(javax.swing.JButton btnAnnuler) {
		this.btnAnnuler = btnAnnuler;
	}

	public javax.swing.JButton getBtnModifier() {
		return btnModifier;
	}

	public void setBtnModifier(javax.swing.JButton btnModifier) {
		this.btnModifier = btnModifier;
	}

	public javax.swing.JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	public void setBtnSupprimer(javax.swing.JButton btnSupprimer) {
		this.btnSupprimer = btnSupprimer;
	}

	public javax.swing.JButton getBtnValider() {
		return btnValider;
	}

	public void setBtnValider(javax.swing.JButton btnValider) {
		this.btnValider = btnValider;
	}

	public javax.swing.JPanel getSwapView() {
		return swapView;
	}

	public void setSwapView(javax.swing.JPanel swapView) {
		this.swapView = swapView;
	}

	public javax.swing.JTextField getTfDateDebut() {
		return tfDateDebut;
	}

	public void setTfDateDebut(javax.swing.JTextField tfDateDebut) {
		this.tfDateDebut = tfDateDebut;
	}

	public javax.swing.JTextField getTfDateFin() {
		return tfDateFin;
	}

	public void setTfDateFin(javax.swing.JTextField tfDateFin) {
		this.tfDateFin = tfDateFin;
	}
    
}

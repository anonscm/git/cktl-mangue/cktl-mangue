/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.individu;

import org.cocktail.mangue.client.gui.carriere.StagesView;
import org.cocktail.mangue.common.utilities.CocktailIcones;

/**
 *
 * @author  cpinsard
 */
public class ArriveeView extends javax.swing.JDialog {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = -7274843962894206085L;

	/** Creates new form TemplateJDialog */
    public ArriveeView(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
                
        initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewArrivee = new javax.swing.JPanel();
        tfUai = new javax.swing.JTextField();
        btnGetAcces = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        tfTypeAcces = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnGetGrade = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        btnDelCorps = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        tfIndiceMajore = new javax.swing.JTextField();
        btnDelGrade = new javax.swing.JButton();
        btnGetCorps = new javax.swing.JButton();
        popupEchelons = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        tfLibelleGrade = new javax.swing.JTextField();
        tfLibelleCorps = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        checkFonctionPublique = new javax.swing.JCheckBox();
        jLabel17 = new javax.swing.JLabel();
        tfLieu = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        tfDatePremiereAffectation = new javax.swing.JTextField();
        btnEvaluerAffectation = new javax.swing.JButton();
        btnGetUai = new javax.swing.JButton();
        btnDelUai = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        tfCommentaires = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SAISIE / MODIFICATION des éléments de carriere");
        setResizable(false);

        viewArrivee.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tfUai.setEditable(false);
        tfUai.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfUai.setToolTipText("Provenance");

        btnGetAcces.setToolTipText("Sélection du type d'accès");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Type d'accès");

        tfTypeAcces.setEditable(false);
        tfTypeAcces.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfTypeAcces.setToolTipText("Type d'accès / Motif d'arrivée");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("UAI Provenance");

        btnGetGrade.setToolTipText("Ajouter Grade");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Grade");

        btnDelCorps.setToolTipText("Supprimer le corps");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Corps");

        tfIndiceMajore.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfIndiceMajore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfIndiceMajoreActionPerformed(evt);
            }
        });

        btnDelGrade.setToolTipText("Supprimer le grade");

        btnGetCorps.setToolTipText("Ajouter Corps");

        popupEchelons.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Echelon");

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("INM");

        tfLibelleGrade.setEditable(false);
        tfLibelleGrade.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfLibelleGrade.setToolTipText("Libellé GRADE");

        tfLibelleCorps.setEditable(false);
        tfLibelleCorps.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfLibelleCorps.setToolTipText("Libellé CORPS");

        checkFonctionPublique.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        checkFonctionPublique.setText("Fonction publique");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jSeparator1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(jLabel12, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLabel11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(10, 10, 10)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(popupEchelons, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 76, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(18, 18, 18)
                                .add(jLabel13, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(tfIndiceMajore, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                                .add(tfLibelleGrade, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 533, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnGetGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnDelGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(jLabel10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(63, 63, 63)
                        .add(tfLibelleCorps, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 533, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnGetCorps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnDelCorps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(checkFonctionPublique, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 225, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(checkFonctionPublique)
                .add(3, 3, 3)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel10)
                        .add(tfLibelleCorps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(btnGetCorps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(btnDelCorps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel11)
                        .add(tfLibelleGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(btnDelGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(btnGetGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(20, 20, 20)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel12)
                    .add(popupEchelons, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel13)
                    .add(tfIndiceMajore, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Lieu provenance");

        tfLieu.setEditable(false);
        tfLieu.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfLieu.setToolTipText("");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("1ère affectation");

        tfDatePremiereAffectation.setEditable(false);
        tfDatePremiereAffectation.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDatePremiereAffectation.setToolTipText("Date de première affectation");

        btnEvaluerAffectation.setToolTipText("Evaluation de la première affectation");

        btnGetUai.setToolTipText("Ajouter UAI");

        btnDelUai.setToolTipText("Supprimer l'UAI");

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Commentaires");

        tfCommentaires.setEditable(false);
        tfCommentaires.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfCommentaires.setToolTipText("Commentaires");

        org.jdesktop.layout.GroupLayout viewArriveeLayout = new org.jdesktop.layout.GroupLayout(viewArrivee);
        viewArrivee.setLayout(viewArriveeLayout);
        viewArriveeLayout.setHorizontalGroup(
            viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewArriveeLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(viewArriveeLayout.createSequentialGroup()
                        .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(jLabel18, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel19, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel9, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLabel17, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel14, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(viewArriveeLayout.createSequentialGroup()
                                .add(tfDatePremiereAffectation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnEvaluerAffectation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(viewArriveeLayout.createSequentialGroup()
                                .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, tfCommentaires)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, tfLieu)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, tfUai)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, tfTypeAcces, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(viewArriveeLayout.createSequentialGroup()
                                        .add(btnGetUai, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(btnDelUai, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(btnGetAcces, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        viewArriveeLayout.setVerticalGroup(
            viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewArriveeLayout.createSequentialGroup()
                .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel9)
                        .add(tfTypeAcces, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewArriveeLayout.createSequentialGroup()
                        .add(btnGetAcces, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(1, 1, 1)))
                .add(8, 8, 8)
                .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewArriveeLayout.createSequentialGroup()
                        .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel14)
                            .add(tfUai, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel17)
                            .add(tfLieu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(btnGetUai, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnDelUai, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel19)
                    .add(tfCommentaires, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewArriveeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel18)
                        .add(tfDatePremiereAffectation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(btnEvaluerAffectation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(11, 11, 11)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewArrivee, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewArrivee, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(42, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-734)/2, (screenSize.height-461)/2, 734, 461);
    }// </editor-fold>//GEN-END:initComponents

private void tfIndiceMajoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfIndiceMajoreActionPerformed
    // TODO add your handling code here:
}//GEN-LAST:event_tfIndiceMajoreActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	StagesView dialog = new StagesView(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelCorps;
    private javax.swing.JButton btnDelGrade;
    private javax.swing.JButton btnDelUai;
    private javax.swing.JButton btnEvaluerAffectation;
    private javax.swing.JButton btnGetAcces;
    private javax.swing.JButton btnGetCorps;
    private javax.swing.JButton btnGetGrade;
    private javax.swing.JButton btnGetUai;
    private javax.swing.JCheckBox checkFonctionPublique;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JComboBox popupEchelons;
    protected javax.swing.JTextField tfCommentaires;
    protected javax.swing.JTextField tfDatePremiereAffectation;
    private javax.swing.JTextField tfIndiceMajore;
    protected javax.swing.JTextField tfLibelleCorps;
    protected javax.swing.JTextField tfLibelleGrade;
    protected javax.swing.JTextField tfLieu;
    protected javax.swing.JTextField tfTypeAcces;
    protected javax.swing.JTextField tfUai;
    private javax.swing.JPanel viewArrivee;
    // End of variables declaration//GEN-END:variables

    
    
    
    private void initGui() {
    	
    	btnEvaluerAffectation.setIcon(CocktailIcones.ICON_CALCULATE_16);
    	btnGetCorps.setIcon(CocktailIcones.ICON_LOUPE_16);
    	btnGetGrade.setIcon(CocktailIcones.ICON_LOUPE_16);
    	btnGetUai.setIcon(CocktailIcones.ICON_LOUPE_16);
    	btnGetAcces.setIcon(CocktailIcones.ICON_LOUPE_16);

    	btnDelCorps.setIcon(CocktailIcones.ICON_DELETE);
    	btnDelGrade.setIcon(CocktailIcones.ICON_DELETE);
    	btnDelUai.setIcon(CocktailIcones.ICON_DELETE);
    	
    }

	public javax.swing.JButton getBtnDelCorps() {
		return btnDelCorps;
	}

	public void setBtnDelCorps(javax.swing.JButton btnDelCorps) {
		this.btnDelCorps = btnDelCorps;
	}

	public javax.swing.JButton getBtnDelGrade() {
		return btnDelGrade;
	}

	public void setBtnDelGrade(javax.swing.JButton btnDelGrade) {
		this.btnDelGrade = btnDelGrade;
	}

	public javax.swing.JButton getBtnDelUai() {
		return btnDelUai;
	}

	public void setBtnDelUai(javax.swing.JButton btnDelUai) {
		this.btnDelUai = btnDelUai;
	}

	public javax.swing.JButton getBtnEvaluerAffectation() {
		return btnEvaluerAffectation;
	}

	public void setBtnEvaluerAffectation(javax.swing.JButton btnEvaluerAffectation) {
		this.btnEvaluerAffectation = btnEvaluerAffectation;
	}

	public javax.swing.JButton getBtnGetAcces() {
		return btnGetAcces;
	}

	public void setBtnGetAcces(javax.swing.JButton btnGetAcces) {
		this.btnGetAcces = btnGetAcces;
	}

	public javax.swing.JButton getBtnGetCorps() {
		return btnGetCorps;
	}

	public void setBtnGetCorps(javax.swing.JButton btnGetCorps) {
		this.btnGetCorps = btnGetCorps;
	}

	public javax.swing.JButton getBtnGetGrade() {
		return btnGetGrade;
	}

	public void setBtnGetGrade(javax.swing.JButton btnGetGrade) {
		this.btnGetGrade = btnGetGrade;
	}

	public javax.swing.JButton getBtnGetUai() {
		return btnGetUai;
	}

	public void setBtnGetUai(javax.swing.JButton btnGetUai) {
		this.btnGetUai = btnGetUai;
	}

	public javax.swing.JCheckBox getCheckFonctionPublique() {
		return checkFonctionPublique;
	}

	public void setCheckFonctionPublique(javax.swing.JCheckBox checkFonctionPublique) {
		this.checkFonctionPublique = checkFonctionPublique;
	}

	public javax.swing.JComboBox getPopupEchelons() {
		return popupEchelons;
	}

	public void setPopupEchelons(javax.swing.JComboBox popupEchelons) {
		this.popupEchelons = popupEchelons;
	}

	public javax.swing.JTextField getTfCommentaires() {
		return tfCommentaires;
	}

	public void setTfCommentaires(javax.swing.JTextField tfCommentaires) {
		this.tfCommentaires = tfCommentaires;
	}

	public javax.swing.JTextField getTfDatePremiereAffectation() {
		return tfDatePremiereAffectation;
	}

	public void setTfDatePremiereAffectation(
			javax.swing.JTextField tfDatePremiereAffectation) {
		this.tfDatePremiereAffectation = tfDatePremiereAffectation;
	}

	public javax.swing.JTextField getTfIndiceMajore() {
		return tfIndiceMajore;
	}

	public void setTfIndiceMajore(javax.swing.JTextField tfIndiceMajore) {
		this.tfIndiceMajore = tfIndiceMajore;
	}

	public javax.swing.JTextField getTfLibelleCorps() {
		return tfLibelleCorps;
	}

	public void setTfLibelleCorps(javax.swing.JTextField tfLibelleCorps) {
		this.tfLibelleCorps = tfLibelleCorps;
	}

	public javax.swing.JTextField getTfLibelleGrade() {
		return tfLibelleGrade;
	}

	public void setTfLibelleGrade(javax.swing.JTextField tfLibelleGrade) {
		this.tfLibelleGrade = tfLibelleGrade;
	}

	public javax.swing.JTextField getTfLieu() {
		return tfLieu;
	}

	public void setTfLieu(javax.swing.JTextField tfLieu) {
		this.tfLieu = tfLieu;
	}

	public javax.swing.JTextField getTfTypeAcces() {
		return tfTypeAcces;
	}

	public void setTfTypeAcces(javax.swing.JTextField tfTypeAcces) {
		this.tfTypeAcces = tfTypeAcces;
	}

	public javax.swing.JTextField getTfUai() {
		return tfUai;
	}

	public void setTfUai(javax.swing.JTextField tfUai) {
		this.tfUai = tfUai;
	}

	public javax.swing.JPanel getViewArrivee() {
		return viewArrivee;
	}

	public void setViewArrivee(javax.swing.JPanel viewArrivee) {
		this.viewArrivee = viewArrivee;
	}



}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.individu;

import org.cocktail.mangue.client.gui.carriere.StagesView;
import org.cocktail.mangue.common.utilities.CocktailIcones;

/**
 *
 * @author  cpinsard
 */
public class DepartView extends javax.swing.JDialog {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = -7274843962894206085L;

	/** Creates new form TemplateJDialog */
    public DepartView(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
                
        initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewDepart = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        tfMotifDepart = new javax.swing.JTextField();
        btnSelectMotif = new javax.swing.JButton();
        panelDeces = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        tfDateDeces = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        tfDateCessation = new javax.swing.JTextField();
        btnDelUai = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        tfUaiAcceuil = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tfCommentaires = new javax.swing.JTextField();
        tfDateDecisionRadiation = new javax.swing.JTextField();
        tfDateEffetRadiation = new javax.swing.JTextField();
        btnGetUai = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        tfLieu = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SAISIE / MODIFICATION des éléments de carriere");
        setResizable(false);

        viewDepart.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Motif départ :");

        tfMotifDepart.setEditable(false);
        tfMotifDepart.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfMotifDepart.setToolTipText("UAI accueil");

        btnSelectMotif.setToolTipText("Sélection de l'UAI d'accueil");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Date du décès :");

        tfDateDeces.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateDeces.setText("jTextField1");

        jLabel5.setForeground(new java.awt.Color(102, 102, 102));
        jLabel5.setText("** Pou un départ \"pour Décès\", la date de départ doit être le lendemain du jour du décès");

        org.jdesktop.layout.GroupLayout panelDecesLayout = new org.jdesktop.layout.GroupLayout(panelDeces);
        panelDeces.setLayout(panelDecesLayout);
        panelDecesLayout.setHorizontalGroup(
            panelDecesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelDecesLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 139, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tfDateDeces, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 112, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 449, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelDecesLayout.setVerticalGroup(
            panelDecesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panelDecesLayout.createSequentialGroup()
                .addContainerGap()
                .add(panelDecesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel10)
                    .add(tfDateDeces, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel5))
                .addContainerGap())
        );

        tfDateCessation.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateCessation.setText("jTextField1");

        btnDelUai.setToolTipText("Suppression");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("UAI d'accueil");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Commentaires");

        tfUaiAcceuil.setEditable(false);
        tfUaiAcceuil.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfUaiAcceuil.setToolTipText("UAI accueil");

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Lieu");

        jLabel2.setText("Date de décision :");

        tfCommentaires.setEditable(false);
        tfCommentaires.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfCommentaires.setToolTipText("Commentaires");

        tfDateDecisionRadiation.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateDecisionRadiation.setText("jTextField1");

        tfDateEffetRadiation.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateEffetRadiation.setText("jTextField1");

        btnGetUai.setToolTipText("Sélection de l'UAI d'accueil");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 13));
        jLabel4.setText("Cessation de service");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Date d'effet : ");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel1.setText("Radiation des cadres");

        tfLieu.setEditable(false);
        tfLieu.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfLieu.setToolTipText("Lieu");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1Layout.createSequentialGroup()
                        .add(65, 65, 65)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(jLabel2)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(tfDateDecisionRadiation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 112, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jLabel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 104, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(tfDateEffetRadiation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 112, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jLabel1)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(jLabel4)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(tfDateCessation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 112, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(26, 26, 26)
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(jLabel14, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jLabel17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 112, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                            .add(jLabel18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 138, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(18, 18, 18)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(tfCommentaires, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(tfUaiAcceuil, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 437, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnGetUai, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnDelUai, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(tfLieu, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel4)
                    .add(tfDateCessation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                        .add(jLabel1)
                        .add(18, 18, 18)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel2)
                            .add(tfDateDecisionRadiation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel3)
                            .add(tfDateEffetRadiation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(30, 30, 30)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel14)
                            .add(tfUaiAcceuil, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(btnGetUai, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(btnDelUai, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel17)
                    .add(tfLieu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel18)
                    .add(tfCommentaires, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout viewDepartLayout = new org.jdesktop.layout.GroupLayout(viewDepart);
        viewDepart.setLayout(viewDepartLayout);
        viewDepartLayout.setHorizontalGroup(
            viewDepartLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewDepartLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewDepartLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewDepartLayout.createSequentialGroup()
                        .add(jLabel9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 139, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfMotifDepart, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 437, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSelectMotif, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewDepartLayout.createSequentialGroup()
                        .add(viewDepartLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(panelDeces, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 714, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        viewDepartLayout.setVerticalGroup(
            viewDepartLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewDepartLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewDepartLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnSelectMotif, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(viewDepartLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(tfMotifDepart, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jLabel9)))
                .add(18, 18, 18)
                .add(panelDeces, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(viewDepartLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewDepartLayout.createSequentialGroup()
                        .add(64, 64, 64)
                        .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewDepartLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(113, 113, 113))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewDepart, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewDepart, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 374, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(51, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-782)/2, (screenSize.height-475)/2, 782, 475);
    }// </editor-fold>//GEN-END:initComponents

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	StagesView dialog = new StagesView(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelUai;
    private javax.swing.JButton btnGetUai;
    private javax.swing.JButton btnSelectMotif;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel panelDeces;
    protected javax.swing.JTextField tfCommentaires;
    private javax.swing.JTextField tfDateCessation;
    private javax.swing.JTextField tfDateDeces;
    private javax.swing.JTextField tfDateDecisionRadiation;
    private javax.swing.JTextField tfDateEffetRadiation;
    protected javax.swing.JTextField tfLieu;
    protected javax.swing.JTextField tfMotifDepart;
    protected javax.swing.JTextField tfUaiAcceuil;
    private javax.swing.JPanel viewDepart;
    // End of variables declaration//GEN-END:variables
    
    private void initGui() {
    	btnSelectMotif.setIcon(CocktailIcones.ICON_SELECT_16);
    	btnGetUai.setIcon(CocktailIcones.ICON_LOUPE_16);
    	btnDelUai.setIcon(CocktailIcones.ICON_DELETE);    	
    }

	public javax.swing.JButton getBtnDelUai() {
		return btnDelUai;
	}

	public void setBtnDelUai(javax.swing.JButton btnDelUai) {
		this.btnDelUai = btnDelUai;
	}

	public javax.swing.JButton getBtnGetUai() {
		return btnGetUai;
	}

	public void setBtnGetUai(javax.swing.JButton btnGetUai) {
		this.btnGetUai = btnGetUai;
	}

	public javax.swing.JButton getBtnSelectMotif() {
		return btnSelectMotif;
	}

	public void setBtnSelectMotif(javax.swing.JButton btnSelectMotif) {
		this.btnSelectMotif = btnSelectMotif;
	}

	public javax.swing.JTextField getTfCommentaires() {
		return tfCommentaires;
	}

	public void setTfCommentaires(javax.swing.JTextField tfCommentaires) {
		this.tfCommentaires = tfCommentaires;
	}

	public javax.swing.JTextField getTfDateCessation() {
		return tfDateCessation;
	}

	public void setTfDateCessation(javax.swing.JTextField tfDateCessation) {
		this.tfDateCessation = tfDateCessation;
	}

	public javax.swing.JTextField getTfDateDecisionRadiation() {
		return tfDateDecisionRadiation;
	}

	public void setTfDateDecisionRadiation(
			javax.swing.JTextField tfDateDecisionRadiation) {
		this.tfDateDecisionRadiation = tfDateDecisionRadiation;
	}

	public javax.swing.JTextField getTfDateEffetRadiation() {
		return tfDateEffetRadiation;
	}

	public void setTfDateEffetRadiation(javax.swing.JTextField tfDateEffetRadiation) {
		this.tfDateEffetRadiation = tfDateEffetRadiation;
	}

	public javax.swing.JTextField getTfLieu() {
		return tfLieu;
	}

	public void setTfLieu(javax.swing.JTextField tfLieu) {
		this.tfLieu = tfLieu;
	}

	public javax.swing.JTextField getTfMotifDepart() {
		return tfMotifDepart;
	}

	public void setTfMotifDepart(javax.swing.JTextField tfMotifDepart) {
		this.tfMotifDepart = tfMotifDepart;
	}

	public javax.swing.JTextField getTfUaiAcceuil() {
		return tfUaiAcceuil;
	}

	public void setTfUaiAcceuil(javax.swing.JTextField tfUaiAcceuil) {
		this.tfUaiAcceuil = tfUaiAcceuil;
	}

	public javax.swing.JPanel getViewDepart() {
		return viewDepart;
	}

	public void setViewDepart(javax.swing.JPanel viewDepart) {
		this.viewDepart = viewDepart;
	}

	public javax.swing.JPanel getPanelDeces() {
		return panelDeces;
	}

	public void setPanelDeces(javax.swing.JPanel panelDeces) {
		this.panelDeces = panelDeces;
	}

	public javax.swing.JTextField getTfDateDeces() {
		return tfDateDeces;
	}

	public void setTfDateDeces(javax.swing.JTextField tfDateDeces) {
		this.tfDateDeces = tfDateDeces;
	}


}

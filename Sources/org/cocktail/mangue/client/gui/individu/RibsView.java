/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.individu;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailIcones;
import org.cocktail.mangue.modele.grhum.EOBanque;
import org.cocktail.mangue.modele.grhum.referentiel.EORib;

import com.webobjects.eointerface.EODisplayGroup;


/**
 *
 * @author  cpinsard
 */
public class RibsView extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6195866169790317637L;
	protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;
	ZEOTableCellRenderer myRenderer;

	/** Creates new form TemplateJDialog */
    public RibsView(java.awt.Frame parent, EODisplayGroup dg, boolean modal, ZEOTableCellRenderer renderer) {
       // super(parent, modal);

    	eod = dg;
        myRenderer = renderer;
        initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewRib = new javax.swing.JPanel();
        btnModifier = new javax.swing.JButton();
        btnAjouter = new javax.swing.JButton();
        btnSupprimer = new javax.swing.JButton();
        viewTable = new javax.swing.JPanel();
        btnAnnuler = new javax.swing.JButton();
        btnValider = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        tfTitulaire = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        checkValide = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        cOView2 = new org.cocktail.component.COView();
        jLabel1 = new javax.swing.JLabel();
        tfBanque = new org.cocktail.component.COTextField();
        jLabel2 = new javax.swing.JLabel();
        tfGuichet = new org.cocktail.component.COTextField();
        jLabel3 = new javax.swing.JLabel();
        tfBic = new org.cocktail.component.COTextField();
        tfDomiciliation = new org.cocktail.component.COTextField();
        jLabel4 = new javax.swing.JLabel();
        btnGetBanque = new org.cocktail.component.COButton();
        checkLocal = new javax.swing.JCheckBox();
        jLabel9 = new javax.swing.JLabel();
        tfIban = new org.cocktail.component.COTextField();
        jLabel8 = new javax.swing.JLabel();
        tfNoCompte = new org.cocktail.component.COTextField();
        tfCle = new org.cocktail.component.COTextField();
        jLabel10 = new javax.swing.JLabel();
        lblMessage = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("GESTION DES CONJOINTS");

        btnModifier.setToolTipText("Modification du RIB sélectionné");

        btnAjouter.setToolTipText("Ajout d'un RIB");

        btnSupprimer.setToolTipText("Suppression du RIB sélectionné");

        viewTable.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        viewTable.setLayout(new java.awt.BorderLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tfTitulaire.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfTitulaire.setToolTipText("Libellé du code");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Titulaire");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel6.setText("DETAIL RIB");

        checkValide.setText("VALIDE");
        checkValide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkValideActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Banque");

        cOView2.setIsBox(true);
        cOView2.setTitle("");

        jLabel1.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Code Banque");

        tfBanque.setEnabledMethod("nonEditable");
        tfBanque.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfBanque.setSupportsBackgroundColor(true);
        tfBanque.setValueName("cBanque");

        jLabel2.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel2.setText("Guichet ");

        tfGuichet.setEnabledMethod("nonEditable");
        tfGuichet.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfGuichet.setSupportsBackgroundColor(true);
        tfGuichet.setValueName("cGuichet");

        jLabel3.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Bic");

        tfBic.setEnabledMethod("nonEditable");
        tfBic.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfBic.setSupportsBackgroundColor(true);
        tfBic.setValueName("toBanque.bic");

        tfDomiciliation.setEnabledMethod("nonEditable");
        tfDomiciliation.setSupportsBackgroundColor(true);
        tfDomiciliation.setValueName("toBanque.domiciliation");

        jLabel4.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Domiciliation");

        btnGetBanque.setActionName("afficherBanque");
        btnGetBanque.setBorderPainted(false);
        btnGetBanque.setEnabledMethod("modificationEnCours");
        btnGetBanque.setIconName("loupe16.gif");

        checkLocal.setText("VALIDE");
        checkLocal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkLocalActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout cOView2Layout = new org.jdesktop.layout.GroupLayout(cOView2);
        cOView2.setLayout(cOView2Layout);
        cOView2Layout.setHorizontalGroup(
            cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView2Layout.createSequentialGroup()
                .addContainerGap()
                .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jLabel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE))
                .add(10, 10, 10)
                .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(tfDomiciliation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                    .add(cOView2Layout.createSequentialGroup()
                        .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, tfBic, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, cOView2Layout.createSequentialGroup()
                                .add(tfBanque, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 56, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jLabel2)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(tfGuichet, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 56, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(18, 18, 18)
                        .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(btnGetBanque, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(checkLocal))
                        .add(5, 5, 5)))
                .addContainerGap())
        );
        cOView2Layout.setVerticalGroup(
            cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView2Layout.createSequentialGroup()
                .addContainerGap()
                .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, btnGetBanque, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(tfGuichet, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                            .add(jLabel2)))
                    .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel1)
                        .add(tfBanque, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)))
                .add(13, 13, 13)
                .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel3)
                    .add(tfBic, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .add(checkLocal))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel4)
                    .add(tfDomiciliation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel9.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("IBAN");

        tfIban.setEnabledMethod("modificationEnCours");
        tfIban.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfIban.setSupportsBackgroundColor(true);
        tfIban.setValueName("iban");

        jLabel8.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("No Compte");

        tfNoCompte.setEnabledMethod("modificationEnCours");
        tfNoCompte.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfNoCompte.setSupportsBackgroundColor(true);
        tfNoCompte.setValueName("noCompte");

        tfCle.setEnabledMethod("modificationEnCours");
        tfCle.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfCle.setSupportsBackgroundColor(true);
        tfCle.setValueName("cleRib");

        jLabel10.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Clé");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 526, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jPanel2Layout.createSequentialGroup()
                            .add(jLabel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 143, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(checkValide)))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 78, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(tfTitulaire, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 415, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(jLabel8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLabel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLabel9, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel2Layout.createSequentialGroup()
                                .add(tfNoCompte, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 120, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jLabel10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(tfCle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 44, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(cOView2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(tfIban, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 240, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .add(75, 75, 75))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel6)
                    .add(checkValide))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel17)
                    .add(tfTitulaire, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(17, 17, 17)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel9)
                    .add(tfIban, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel7)
                    .add(cOView2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(11, 11, 11)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel8)
                    .add(tfNoCompte, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                    .add(jLabel10)
                    .add(tfCle, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE))
                .add(27, 27, 27))
        );

        lblMessage.setForeground(new java.awt.Color(255, 102, 102));
        lblMessage.setText("** Gestion réservée aux agents ayant les droits sur les fournisseurs (Jefy_Admin)");

        org.jdesktop.layout.GroupLayout viewRibLayout = new org.jdesktop.layout.GroupLayout(viewRib);
        viewRib.setLayout(viewRibLayout);
        viewRibLayout.setHorizontalGroup(
            viewRibLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, viewRibLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewRibLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 578, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, viewRibLayout.createSequentialGroup()
                        .add(lblMessage, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 455, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 53, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanel2, 0, 578, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(viewRibLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(viewRibLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        viewRibLayout.setVerticalGroup(
            viewRibLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewRibLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewRibLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewRibLayout.createSequentialGroup()
                        .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 139, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(viewRibLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewRibLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(btnValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(lblMessage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewRib, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewRib, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-705)/2, (screenSize.height-598)/2, 705, 598);
    }// </editor-fold>//GEN-END:initComponents

    private void checkFiltrePersoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkFiltrePersoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkFiltrePersoActionPerformed

    private void checkFiltreProActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkFiltreProActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkFiltreProActionPerformed

    private void checkFiltreFactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkFiltreFactActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkFiltreFactActionPerformed

    private void checkPersoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkPersoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkPersoActionPerformed

    private void checkProActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkProActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkProActionPerformed

    private void checkFactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkFactActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkFactActionPerformed

    private void checkEtudiantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkEtudiantActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkEtudiantActionPerformed

    private void checkParentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkParentActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkParentActionPerformed

    private void checkValideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkValideActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkValideActionPerformed

    private void checkPrincipaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkPrincipaleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkPrincipaleActionPerformed

    private void checkFiltreAutresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkFiltreAutresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkFiltreAutresActionPerformed

    private void checkLocalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkLocalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkLocalActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	RibsView dialog = new RibsView(new javax.swing.JFrame(), null, true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjouter;
    private javax.swing.JButton btnAnnuler;
    public org.cocktail.component.COButton btnGetBanque;
    private javax.swing.JButton btnModifier;
    private javax.swing.JButton btnSupprimer;
    private javax.swing.JButton btnValider;
    private org.cocktail.component.COView cOView2;
    private javax.swing.JCheckBox checkLocal;
    private javax.swing.JCheckBox checkValide;
    public javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel17;
    public javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblMessage;
    public org.cocktail.component.COTextField tfBanque;
    public org.cocktail.component.COTextField tfBic;
    public org.cocktail.component.COTextField tfCle;
    public org.cocktail.component.COTextField tfDomiciliation;
    public org.cocktail.component.COTextField tfGuichet;
    public org.cocktail.component.COTextField tfIban;
    public org.cocktail.component.COTextField tfNoCompte;
    protected javax.swing.JTextField tfTitulaire;
    private javax.swing.JPanel viewRib;
    private javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

    private void initGui() {
    	
    	btnAjouter.setIcon(CocktailIcones.ICON_ADD);
    	btnModifier.setIcon(CocktailIcones.ICON_UPDATE);
    	btnSupprimer.setIcon(CocktailIcones.ICON_DELETE);

    	btnGetBanque.setIcon(CocktailIcones.ICON_SELECT_16);
    	btnValider.setIcon(CocktailIcones.ICON_VALID);
    	btnAnnuler.setIcon(CocktailIcones.ICON_CANCEL);

    	Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

    	ZEOTableModelColumn col = new ZEOTableModelColumn(eod, EORib.RIB_VALIDE_KEY, "Val", 20);
    	col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EORib.IBAN_KEY, "IBAN", 120);
    	col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EORib.TO_BANQUE_KEY+"."+EOBanque.C_BANQUE_KEY, "Banque", 45);
    	col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EORib.TO_BANQUE_KEY+"."+EOBanque.C_GUICHET_KEY, "Guichet", 45);
    	col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EORib.TO_BANQUE_KEY+"."+EOBanque.DOMICILIATION_KEY, "Domiciliation", 120);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);

    	myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
		
    }

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	public ZEOTableCellRenderer getMyRenderer() {
		return myRenderer;
	}

	public void setMyRenderer(ZEOTableCellRenderer myRenderer) {
		this.myRenderer = myRenderer;
	}

	public javax.swing.JButton getBtnAjouter() {
		return btnAjouter;
	}

	public void setBtnAjouter(javax.swing.JButton btnAjouter) {
		this.btnAjouter = btnAjouter;
	}

	public javax.swing.JButton getBtnAnnuler() {
		return btnAnnuler;
	}

	public void setBtnAnnuler(javax.swing.JButton btnAnnuler) {
		this.btnAnnuler = btnAnnuler;
	}

	public org.cocktail.component.COButton getBtnGetBanque() {
		return btnGetBanque;
	}

	public void setBtnGetBanque(org.cocktail.component.COButton btnGetBanque) {
		this.btnGetBanque = btnGetBanque;
	}

	public javax.swing.JButton getBtnModifier() {
		return btnModifier;
	}

	public void setBtnModifier(javax.swing.JButton btnModifier) {
		this.btnModifier = btnModifier;
	}

	public javax.swing.JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	public void setBtnSupprimer(javax.swing.JButton btnSupprimer) {
		this.btnSupprimer = btnSupprimer;
	}

	public javax.swing.JButton getBtnValider() {
		return btnValider;
	}

	public void setBtnValider(javax.swing.JButton btnValider) {
		this.btnValider = btnValider;
	}

	public javax.swing.JCheckBox getCheckLocal() {
		return checkLocal;
	}

	public void setCheckLocal(javax.swing.JCheckBox checkLocal) {
		this.checkLocal = checkLocal;
	}

	public javax.swing.JCheckBox getCheckValide() {
		return checkValide;
	}

	public void setCheckValide(javax.swing.JCheckBox checkValide) {
		this.checkValide = checkValide;
	}

	public org.cocktail.component.COTextField getTfBanque() {
		return tfBanque;
	}

	public void setTfBanque(org.cocktail.component.COTextField tfBanque) {
		this.tfBanque = tfBanque;
	}

	public org.cocktail.component.COTextField getTfBic() {
		return tfBic;
	}

	public void setTfBic(org.cocktail.component.COTextField tfBic) {
		this.tfBic = tfBic;
	}

	public org.cocktail.component.COTextField getTfCle() {
		return tfCle;
	}

	public void setTfCle(org.cocktail.component.COTextField tfCle) {
		this.tfCle = tfCle;
	}

	public org.cocktail.component.COTextField getTfDomiciliation() {
		return tfDomiciliation;
	}

	public void setTfDomiciliation(
			org.cocktail.component.COTextField tfDomiciliation) {
		this.tfDomiciliation = tfDomiciliation;
	}

	public org.cocktail.component.COTextField getTfGuichet() {
		return tfGuichet;
	}

	public void setTfGuichet(org.cocktail.component.COTextField tfGuichet) {
		this.tfGuichet = tfGuichet;
	}

	public org.cocktail.component.COTextField getTfIban() {
		return tfIban;
	}

	public void setTfIban(org.cocktail.component.COTextField tfIban) {
		this.tfIban = tfIban;
	}

	public org.cocktail.component.COTextField getTfNoCompte() {
		return tfNoCompte;
	}

	public void setTfNoCompte(org.cocktail.component.COTextField tfNoCompte) {
		this.tfNoCompte = tfNoCompte;
	}

	public javax.swing.JTextField getTfTitulaire() {
		return tfTitulaire;
	}

	public void setTfTitulaire(javax.swing.JTextField tfTitulaire) {
		this.tfTitulaire = tfTitulaire;
	}

	public javax.swing.JPanel getViewRib() {
		return viewRib;
	}

	public void setViewRib(javax.swing.JPanel viewRib) {
		this.viewRib = viewRib;
	}

	public javax.swing.JLabel getLblMessage() {
		return lblMessage;
	}

	public void setLblMessage(javax.swing.JLabel lblMessage) {
		this.lblMessage = lblMessage;
	}


}

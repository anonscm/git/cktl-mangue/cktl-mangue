/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.individu;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.promotions.PromouvabilitesView;
import org.cocktail.mangue.common.utilities.CocktailIcones;


/**
 *
 * @author  cpinsard
 */
public class FicheSituationAgentView extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6195866169790317637L;

	/** Creates new form TemplateJDialog */
    public FicheSituationAgentView(java.awt.Frame parent, boolean modal) {
       // super(parent, modal);
           initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewFicheAgent = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        tfIdentite = new javax.swing.JLabel();
        tfTitreDetailContrat = new javax.swing.JTextField();
        lblAge = new javax.swing.JLabel();
        lblSecu = new javax.swing.JLabel();
        lblSituationFamiliale1 = new javax.swing.JLabel();
        tfArrivee = new javax.swing.JTextField();
        lblSituationFamiliale2 = new javax.swing.JLabel();
        tfDepart = new javax.swing.JTextField();
        photoAgent = new org.cocktail.component.COImageView();
        btnArriveeDepart = new javax.swing.JButton();
        lblSituationFamiliale = new javax.swing.JLabel();
        lblAdresse = new javax.swing.JLabel();
        lblTelephone = new javax.swing.JLabel();
        btnTelecharger = new javax.swing.JButton();
        lblGestionnaire = new javax.swing.JLabel();
        btnSelectGestionnaire = new javax.swing.JButton();
        btnInfosRetraite = new org.cocktail.component.COButton();
        btnInfosComplémentaires = new org.cocktail.component.COButton();
        btnInfosPersonnelles = new org.cocktail.component.COButton();
        jPanel1 = new javax.swing.JPanel();
        lblType = new javax.swing.JLabel();
        lblGrade = new javax.swing.JLabel();
        Jlabel150 = new javax.swing.JLabel();
        lblDetail = new javax.swing.JLabel();
        lblPeriode = new javax.swing.JLabel();
        tfTitreDetailContrat1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblAffectation = new javax.swing.JLabel();
        btnAffectation = new javax.swing.JButton();
        lblOccupation = new javax.swing.JLabel();
        Jlabel151 = new javax.swing.JLabel();
        lblSpecialisation = new javax.swing.JLabel();
        temSauvadet = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("FICHE IDENTITE");
        setResizable(false);

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tfIdentite.setFont(new java.awt.Font("Tahoma", 0, 18));
        tfIdentite.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        tfIdentite.setText("Mr PINSARD Cyril");

        tfTitreDetailContrat.setBackground(new java.awt.Color(198, 223, 212));
        tfTitreDetailContrat.setEditable(false);
        tfTitreDetailContrat.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfTitreDetailContrat.setText("ETAT CIVIL");
        tfTitreDetailContrat.setAutoscrolls(false);
        tfTitreDetailContrat.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblAge.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAge.setText("Marié - 2 enfants");

        lblSecu.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSecu.setText("Sécu");

        lblSituationFamiliale1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSituationFamiliale1.setText("ARRIVEE :");

        tfArrivee.setFont(new java.awt.Font("Tahoma", 0, 10));
        tfArrivee.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfArrivee.setText("jTextField1");
        tfArrivee.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblSituationFamiliale2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSituationFamiliale2.setText("DEPART :");

        tfDepart.setFont(new java.awt.Font("Tahoma", 0, 10));
        tfDepart.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfDepart.setText("jTextField1");
        tfDepart.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        photoAgent.setImageScale("EOImageView.ScaleProportionallyIfTooLarge");
        photoAgent.setMethodForImage("imageAgent");

        org.jdesktop.layout.GroupLayout photoAgentLayout = new org.jdesktop.layout.GroupLayout(photoAgent);
        photoAgent.setLayout(photoAgentLayout);
        photoAgentLayout.setHorizontalGroup(
            photoAgentLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 159, Short.MAX_VALUE)
        );
        photoAgentLayout.setVerticalGroup(
            photoAgentLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 209, Short.MAX_VALUE)
        );

        btnArriveeDepart.setToolTipText("Gestion des arrivées / départs");
        btnArriveeDepart.setBorderPainted(false);
        btnArriveeDepart.setContentAreaFilled(false);

        lblSituationFamiliale.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSituationFamiliale.setText("ADRESSE");

        lblAdresse.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblAdresse.setText("TELEPHONE");

        lblTelephone.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTelephone.setText("TELEPHONE");

        btnTelecharger.setText("Télécharger");
        btnTelecharger.setToolTipText("Télécharger une nouvelle photo");

        lblGestionnaire.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblGestionnaire.setText("GESTIONNAIRE");

        btnSelectGestionnaire.setToolTipText("Modifier le service RH gestionnaire");
        btnSelectGestionnaire.setBorderPainted(false);
        btnSelectGestionnaire.setContentAreaFilled(false);

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, btnSelectGestionnaire, 0, 0, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, btnArriveeDepart, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, Short.MAX_VALUE))
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(lblSituationFamiliale2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(lblSituationFamiliale1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(tfDepart)
                            .add(tfArrivee, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(21, 21, 21)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblSecu, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
                            .add(tfIdentite, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
                            .add(lblAge, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
                            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, lblTelephone, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, lblSituationFamiliale, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, lblAdresse, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, lblGestionnaire, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 476, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                .add(51, 51, 51)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(btnTelecharger, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(photoAgent, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .add(tfTitreDetailContrat, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 818, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(tfTitreDetailContrat, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfIdentite)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lblSecu)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lblAge)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lblSituationFamiliale)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lblAdresse)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lblTelephone)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(btnSelectGestionnaire, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(lblGestionnaire, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 44, Short.MAX_VALUE)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnArriveeDepart, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(lblSituationFamiliale1)
                                .add(tfArrivee, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(9, 9, 9)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(lblSituationFamiliale2)
                            .add(tfDepart, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(photoAgent, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnTelecharger, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        btnInfosRetraite.setActionName("afficherDivers");
        btnInfosRetraite.setEnabledMethod("peutAfficherEnfants");
        btnInfosRetraite.setIconName("mangue_dossier.png");
        btnInfosRetraite.setText("Infos retraite");
        btnInfosRetraite.setToolTipText("Arrivée, Service national, formations, diplômes, etc ...");

        btnInfosComplémentaires.setActionName("afficherDivers");
        btnInfosComplémentaires.setEnabledMethod("peutAfficherEnfants");
        btnInfosComplémentaires.setIconName("mangue_dossier.png");
        btnInfosComplémentaires.setText("Infos complémentaires");
        btnInfosComplémentaires.setToolTipText("Arrivée, Service national, formations, diplômes, etc ...");

        btnInfosPersonnelles.setActionName("afficherInfosPerso");
        btnInfosPersonnelles.setEnabledMethod("peutAfficherBoutons");
        btnInfosPersonnelles.setIconName("mangue_lock.png");
        btnInfosPersonnelles.setText("Infos personnelles");
        btnInfosPersonnelles.setToolTipText("Adresses, Téléphones, Comptes");

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 374));

        lblType.setFont(new java.awt.Font("Tahoma", 0, 16));
        lblType.setText("Non Titulaire");

        lblGrade.setFont(new java.awt.Font("Arial", 0, 14));
        lblGrade.setText("INGENIEUR D'ETUDES RF, 8ème échelon");

        Jlabel150.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Jlabel150.setText("Grade : ");

        lblDetail.setFont(new java.awt.Font("Arial", 0, 14));
        lblDetail.setText("Contrat (CDD) - ASU");

        lblPeriode.setFont(new java.awt.Font("Arial", 0, 14));
        lblPeriode.setText("01/04/2000 au ");

        tfTitreDetailContrat1.setBackground(new java.awt.Color(198, 223, 212));
        tfTitreDetailContrat1.setEditable(false);
        tfTitreDetailContrat1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfTitreDetailContrat1.setText("SITUATION");
        tfTitreDetailContrat1.setAutoscrolls(false);
        tfTitreDetailContrat1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        tfTitreDetailContrat1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfTitreDetailContrat1ActionPerformed(evt);
            }
        });

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Détail : ");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Période : ");

        lblAffectation.setFont(new java.awt.Font("Tahoma", 0, 14));
        lblAffectation.setText("Affectation");

        btnAffectation.setToolTipText("Gestion des affectations / occupations");
        btnAffectation.setBorderPainted(false);
        btnAffectation.setContentAreaFilled(false);
        btnAffectation.setFocusable(false);

        lblOccupation.setFont(new java.awt.Font("Tahoma", 0, 14));
        lblOccupation.setText("Occupation");

        Jlabel151.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        Jlabel151.setText("Spécialisation :");

        lblSpecialisation.setFont(new java.awt.Font("Arial", 0, 14));
        lblSpecialisation.setText("INGENIEUR D'ETUDES RF, 8ème échelon");

        temSauvadet.setText("Eligibilité Sauvadet :    ");
        temSauvadet.setToolTipText("Agent titularisé au titre de la loi sauvadet");
        temSauvadet.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        temSauvadet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                temSauvadetActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(tfTitreDetailContrat1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 818, Short.MAX_VALUE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(lblType, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 699, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(73, 73, 73))
            .add(jPanel1Layout.createSequentialGroup()
                .add(48, 48, 48)
                .add(lblOccupation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
                .add(81, 81, 81))
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(Jlabel151, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(Jlabel150, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jLabel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jPanel1Layout.createSequentialGroup()
                            .add(24, 24, 24)
                            .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 105, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .add(18, 18, 18)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lblDetail, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, lblPeriode, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, lblGrade, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, lblSpecialisation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE))
                .add(73, 73, 73))
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(btnAffectation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(lblAffectation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 697, Short.MAX_VALUE)
                .add(73, 73, 73))
            .add(jPanel1Layout.createSequentialGroup()
                .add(40, 40, 40)
                .add(temSauvadet, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 257, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(521, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(tfTitreDetailContrat1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(22, 22, 22)
                .add(lblType)
                .add(18, 18, 18)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel2)
                    .add(lblPeriode))
                .add(18, 18, 18)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel3)
                    .add(lblDetail))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(Jlabel150)
                    .add(lblGrade))
                .add(13, 13, 13)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(Jlabel151)
                    .add(lblSpecialisation))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(temSauvadet)
                .add(12, 12, 12)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(lblAffectation)
                    .add(btnAffectation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(35, 35, 35)
                .add(lblOccupation)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout viewFicheAgentLayout = new org.jdesktop.layout.GroupLayout(viewFicheAgent);
        viewFicheAgent.setLayout(viewFicheAgentLayout);
        viewFicheAgentLayout.setHorizontalGroup(
            viewFicheAgentLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewFicheAgentLayout.createSequentialGroup()
                .add(viewFicheAgentLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewFicheAgentLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(viewFicheAgentLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE)
                            .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .add(viewFicheAgentLayout.createSequentialGroup()
                        .add(117, 117, 117)
                        .add(btnInfosPersonnelles, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 176, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(btnInfosComplémentaires, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 205, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnInfosRetraite, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 205, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        viewFicheAgentLayout.setVerticalGroup(
            viewFicheAgentLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewFicheAgentLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 316, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(viewFicheAgentLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnInfosComplémentaires, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnInfosPersonnelles, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnInfosRetraite, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(76, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(viewFicheAgent, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(viewFicheAgent, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-880)/2, (screenSize.height-765)/2, 880, 765);
    }// </editor-fold>//GEN-END:initComponents

    private void tfTitreDetailContrat1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfTitreDetailContrat1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfTitreDetailContrat1ActionPerformed

    private void temSauvadetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_temSauvadetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_temSauvadetActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                PromouvabilitesView dialog = new PromouvabilitesView(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Jlabel150;
    private javax.swing.JLabel Jlabel151;
    private javax.swing.JButton btnAffectation;
    private javax.swing.JButton btnArriveeDepart;
    public org.cocktail.component.COButton btnInfosComplémentaires;
    public org.cocktail.component.COButton btnInfosPersonnelles;
    public org.cocktail.component.COButton btnInfosRetraite;
    private javax.swing.JButton btnSelectGestionnaire;
    private javax.swing.JButton btnTelecharger;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblAdresse;
    private javax.swing.JLabel lblAffectation;
    private javax.swing.JLabel lblAge;
    private javax.swing.JLabel lblDetail;
    private javax.swing.JLabel lblGestionnaire;
    private javax.swing.JLabel lblGrade;
    private javax.swing.JLabel lblOccupation;
    private javax.swing.JLabel lblPeriode;
    private javax.swing.JLabel lblSecu;
    private javax.swing.JLabel lblSituationFamiliale;
    private javax.swing.JLabel lblSituationFamiliale1;
    private javax.swing.JLabel lblSituationFamiliale2;
    private javax.swing.JLabel lblSpecialisation;
    private javax.swing.JLabel lblTelephone;
    private javax.swing.JLabel lblType;
    public org.cocktail.component.COImageView photoAgent;
    private javax.swing.JCheckBox temSauvadet;
    private javax.swing.JTextField tfArrivee;
    private javax.swing.JTextField tfDepart;
    private javax.swing.JLabel tfIdentite;
    private javax.swing.JTextField tfTitreDetailContrat;
    private javax.swing.JTextField tfTitreDetailContrat1;
    private javax.swing.JPanel viewFicheAgent;
    // End of variables declaration//GEN-END:variables

    
    /**
     * 
     */
    private void initGui() {
   	
    	photoAgent.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    	btnAffectation.setIcon(CocktailIcones.ICON_REFRESH_16);
    	btnArriveeDepart.setIcon(CocktailIcones.ICON_REFRESH_16);
    	btnTelecharger.setIcon(CocktailIcones.ICON_DISQUETTE_22);
    	btnSelectGestionnaire.setIcon(CocktailIcones.ICON_REFRESH_16);
    	
    }

	public javax.swing.JButton getBtnAffectation() {
		return btnAffectation;
	}

	public void setBtnAffectation(javax.swing.JButton btnAffectation) {
		this.btnAffectation = btnAffectation;
	}

	public javax.swing.JButton getBtnArriveeDepart() {
		return btnArriveeDepart;
	}

	public void setBtnArriveeDepart(javax.swing.JButton btnArriveeDepart) {
		this.btnArriveeDepart = btnArriveeDepart;
	}

	public org.cocktail.component.COButton getBtnInfosComplémentaires() {
		return btnInfosComplémentaires;
	}

	public void setBtnInfosComplémentaires(
			org.cocktail.component.COButton btnInfosComplémentaires) {
		this.btnInfosComplémentaires = btnInfosComplémentaires;
	}

	public org.cocktail.component.COButton getBtnInfosPersonnelles() {
		return btnInfosPersonnelles;
	}

	public void setBtnInfosPersonnelles(
			org.cocktail.component.COButton btnInfosPersonnelles) {
		this.btnInfosPersonnelles = btnInfosPersonnelles;
	}

	public org.cocktail.component.COButton getBtnInfosRetraite() {
		return btnInfosRetraite;
	}

	public void setBtnInfosRetraite(org.cocktail.component.COButton btnInfosRetraite) {
		this.btnInfosRetraite = btnInfosRetraite;
	}

	public javax.swing.JLabel getLblAdresse() {
		return lblAdresse;
	}

	public void setLblAdresse(javax.swing.JLabel lblAdresse) {
		this.lblAdresse = lblAdresse;
	}

	public javax.swing.JLabel getLblAffectation() {
		return lblAffectation;
	}

	public void setLblAffectation(javax.swing.JLabel lblAffectation) {
		this.lblAffectation = lblAffectation;
	}

	public javax.swing.JLabel getLblAge() {
		return lblAge;
	}

	public void setLblAge(javax.swing.JLabel lblAge) {
		this.lblAge = lblAge;
	}

	public javax.swing.JLabel getLblDetail() {
		return lblDetail;
	}

	public void setLblDetail(javax.swing.JLabel lblDetail) {
		this.lblDetail = lblDetail;
	}

	public javax.swing.JLabel getLblGrade() {
		return lblGrade;
	}

	public void setLblGrade(javax.swing.JLabel lblGrade) {
		this.lblGrade = lblGrade;
	}

	public javax.swing.JLabel getLblOccupation() {
		return lblOccupation;
	}

	public void setLblOccupation(javax.swing.JLabel lblOccupation) {
		this.lblOccupation = lblOccupation;
	}

	public javax.swing.JLabel getLblPeriode() {
		return lblPeriode;
	}

	public void setLblPeriode(javax.swing.JLabel lblPeriode) {
		this.lblPeriode = lblPeriode;
	}

	public javax.swing.JLabel getLblSecu() {
		return lblSecu;
	}

	public void setLblSecu(javax.swing.JLabel lblSecu) {
		this.lblSecu = lblSecu;
	}

	public javax.swing.JLabel getLblSituationFamiliale() {
		return lblSituationFamiliale;
	}

	public void setLblSituationFamiliale(javax.swing.JLabel lblSituationFamiliale) {
		this.lblSituationFamiliale = lblSituationFamiliale;
	}

	public javax.swing.JLabel getLblSituationFamiliale1() {
		return lblSituationFamiliale1;
	}

	public void setLblSituationFamiliale1(javax.swing.JLabel lblSituationFamiliale1) {
		this.lblSituationFamiliale1 = lblSituationFamiliale1;
	}

	public javax.swing.JLabel getLblSituationFamiliale2() {
		return lblSituationFamiliale2;
	}

	public void setLblSituationFamiliale2(javax.swing.JLabel lblSituationFamiliale2) {
		this.lblSituationFamiliale2 = lblSituationFamiliale2;
	}

	public javax.swing.JLabel getLblTelephone() {
		return lblTelephone;
	}

	public void setLblTelephone(javax.swing.JLabel lblTelephone) {
		this.lblTelephone = lblTelephone;
	}

	public javax.swing.JLabel getLblType() {
		return lblType;
	}

	public void setLblType(javax.swing.JLabel lblType) {
		this.lblType = lblType;
	}

	public org.cocktail.component.COImageView getPhotoAgent() {
		return photoAgent;
	}

	public void setPhotoAgent(org.cocktail.component.COImageView photoAgent) {
		this.photoAgent = photoAgent;
	}

	public javax.swing.JTextField getTfArrivee() {
		return tfArrivee;
	}

	public void setTfArrivee(javax.swing.JTextField tfArrivee) {
		this.tfArrivee = tfArrivee;
	}

	public javax.swing.JTextField getTfDepart() {
		return tfDepart;
	}

	public void setTfDepart(javax.swing.JTextField tfDepart) {
		this.tfDepart = tfDepart;
	}

	public javax.swing.JLabel getTfIdentite() {
		return tfIdentite;
	}

	public void setTfIdentite(javax.swing.JLabel tfIdentite) {
		this.tfIdentite = tfIdentite;
	}

	public javax.swing.JPanel getViewFicheAgent() {
		return viewFicheAgent;
	}

	public void setViewFicheAgent(javax.swing.JPanel viewFicheAgent) {
		this.viewFicheAgent = viewFicheAgent;
	}

	public javax.swing.JLabel getLblSpecialisation() {
		return lblSpecialisation;
	}

	public void setLblSpecialisation(javax.swing.JLabel lblSpecialisation) {
		this.lblSpecialisation = lblSpecialisation;
	}

	public javax.swing.JButton getBtnTelecharger() {
		return btnTelecharger;
	}

	public void setBtnTelecharger(javax.swing.JButton btnTelecharger) {
		this.btnTelecharger = btnTelecharger;
	}

	public javax.swing.JButton getBtnSelectGestionnaire() {
		return btnSelectGestionnaire;
	}

	public void setBtnSelectGestionnaire(javax.swing.JButton btnSelectGestionnaire) {
		this.btnSelectGestionnaire = btnSelectGestionnaire;
	}

	public javax.swing.JLabel getLblGestionnaire() {
		return lblGestionnaire;
	}

	public void setLblGestionnaire(javax.swing.JLabel lblGestionnaire) {
		this.lblGestionnaire = lblGestionnaire;
	}

	public javax.swing.JCheckBox getTemSauvadet() {
		return temSauvadet;
	}

	public void setTemSauvadet(javax.swing.JCheckBox temSauvadet) {
		this.temSauvadet = temSauvadet;
	}


}

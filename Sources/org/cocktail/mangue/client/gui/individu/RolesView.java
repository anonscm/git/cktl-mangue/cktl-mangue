/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.individu;

import java.awt.BorderLayout;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.mangue.common.utilities.CocktailIcones;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartStructure;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eointerface.EODisplayGroup;


/**
 *
 * @author  cpinsard
 */
public class RolesView extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6195866169790317637L;

	protected EODisplayGroup eodGroupes, eodRoles;
	protected ZEOTable myEOTableGroupes, myEOTableRoles;
	protected ZEOTableModel myTableModelGroupes, myTableModelRoles;
	protected TableSorter myTableSorterGroupes, myTableSorterRoles;

	/** Creates new form TemplateJDialog */
    public RolesView(java.awt.Frame parent, boolean modal, EODisplayGroup dgGroupes, EODisplayGroup dgRoles) {
       // super(parent, modal);
    	
    	eodGroupes = dgGroupes;
    	eodRoles = dgRoles;
    	
        initComponents();
        initGui();
        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        viewRoles = new javax.swing.JPanel();
        btnAJouterGroupe = new javax.swing.JButton();
        viewTableAssociation = new javax.swing.JPanel();
        btnSupprimerGroupe = new javax.swing.JButton();
        viewTableGroupes = new javax.swing.JPanel();
        checkServices = new javax.swing.JCheckBox();
        checkAllGroupes = new javax.swing.JCheckBox();
        btnModifierAssociation = new javax.swing.JButton();
        btnAJouterAssociation = new javax.swing.JButton();
        btnSupprimerAssociation = new javax.swing.JButton();
        checkRolesStructure = new javax.swing.JCheckBox();
        checkAllRoles = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("GESTION DES ROLES");

        btnAJouterGroupe.setToolTipText("Modification du conjoint");

        viewTableAssociation.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        viewTableAssociation.setLayout(new java.awt.CardLayout());

        btnSupprimerGroupe.setToolTipText("Sélection d'un individu existant");

        viewTableGroupes.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        viewTableGroupes.setLayout(new java.awt.CardLayout());

        buttonGroup1.add(checkServices);
        checkServices.setText("Services / Labos");

        buttonGroup1.add(checkAllGroupes);
        checkAllGroupes.setText("Tous les groupes");

        btnModifierAssociation.setToolTipText("Modifier le rôle sélectionné");

        btnAJouterAssociation.setToolTipText("Ajouter un rôle");

        btnSupprimerAssociation.setToolTipText("Supprimer le rôle sélectionné");

        buttonGroup2.add(checkRolesStructure);
        checkRolesStructure.setSelected(true);
        checkRolesStructure.setText("Rôles de la structure");

        buttonGroup2.add(checkAllRoles);
        checkAllRoles.setText("Tous les Rôles");

        jLabel1.setText("** Le service d'affectation de l'agent ne peut être supprimé");
        jLabel1.setEnabled(false);

        org.jdesktop.layout.GroupLayout viewRolesLayout = new org.jdesktop.layout.GroupLayout(viewRoles);
        viewRoles.setLayout(viewRolesLayout);
        viewRolesLayout.setHorizontalGroup(
            viewRolesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewRolesLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewRolesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewRolesLayout.createSequentialGroup()
                        .add(checkRolesStructure)
                        .add(18, 18, 18)
                        .add(checkAllRoles))
                    .add(viewRolesLayout.createSequentialGroup()
                        .add(checkServices)
                        .add(18, 18, 18)
                        .add(checkAllGroupes))
                    .add(viewRolesLayout.createSequentialGroup()
                        .add(viewRolesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, viewTableAssociation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 818, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, viewTableGroupes, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 818, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(viewRolesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(btnSupprimerAssociation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnModifierAssociation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnAJouterAssociation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnSupprimerGroupe, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnAJouterGroupe, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 438, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        viewRolesLayout.setVerticalGroup(
            viewRolesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewRolesLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewRolesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(checkServices)
                    .add(checkAllGroupes))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(viewRolesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewRolesLayout.createSequentialGroup()
                        .add(viewTableGroupes, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 295, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jLabel1)
                        .add(11, 11, 11)
                        .add(viewRolesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(checkRolesStructure)
                            .add(checkAllRoles)))
                    .add(viewRolesLayout.createSequentialGroup()
                        .add(btnAJouterGroupe, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSupprimerGroupe, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(viewRolesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewRolesLayout.createSequentialGroup()
                        .add(btnAJouterAssociation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnModifierAssociation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSupprimerAssociation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewTableAssociation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 178, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewRoles, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewRoles, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-903)/2, (screenSize.height-637)/2, 903, 637);
    }// </editor-fold>//GEN-END:initComponents

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	RolesView dialog = new RolesView(new javax.swing.JFrame(), true, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAJouterAssociation;
    private javax.swing.JButton btnAJouterGroupe;
    private javax.swing.JButton btnModifierAssociation;
    private javax.swing.JButton btnSupprimerAssociation;
    private javax.swing.JButton btnSupprimerGroupe;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JCheckBox checkAllGroupes;
    private javax.swing.JCheckBox checkAllRoles;
    private javax.swing.JCheckBox checkRolesStructure;
    private javax.swing.JCheckBox checkServices;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel viewRoles;
    private javax.swing.JPanel viewTableAssociation;
    private javax.swing.JPanel viewTableGroupes;
    // End of variables declaration//GEN-END:variables

    private void initGui() {

    	btnAJouterGroupe.setIcon(CocktailIcones.ICON_ADD);
    	btnSupprimerGroupe.setIcon(CocktailIcones.ICON_DELETE);
    	btnAJouterAssociation.setIcon(CocktailIcones.ICON_ADD);
    	btnModifierAssociation.setIcon(CocktailIcones.ICON_UPDATE);
    	btnSupprimerAssociation.setIcon(CocktailIcones.ICON_DELETE);

		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

		ZEOTableModelColumn col = new ZEOTableModelColumn(eodRoles, EORepartAssociation.ASSOCIATION_KEY+"."+INomenclature.LIBELLE_LONG_KEY, "Rôle", 175);
		col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodRoles, EORepartAssociation.DATE_DEBUT_KEY, "Début", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodRoles, EORepartAssociation.DATE_FIN_KEY, "Fin", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodRoles, EORepartAssociation.STRUCTURE_KEY+"."+EOStructure.LL_STRUCTURE_KEY, "Structure", 200);
		col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);

		myTableModelRoles = new ZEOTableModel(eodRoles, myCols);
		myTableSorterRoles = new TableSorter(myTableModelRoles);

		myEOTableRoles = new ZEOTable(myTableSorterRoles);
		//myEOTable.addListener(myListenerContrat);
		myTableSorterRoles.setTableHeader(myEOTableRoles.getTableHeader());		

		myEOTableRoles.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTableRoles.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTableRoles.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableAssociation.setLayout(new BorderLayout());
		viewTableAssociation.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableAssociation.removeAll();
		viewTableAssociation.add(new JScrollPane(myEOTableRoles), BorderLayout.CENTER);

		myCols = new Vector<ZEOTableModelColumn>();

		col = new ZEOTableModelColumn(eodGroupes, EORepartStructure.STRUCTURE_KEY+"."+EOStructure.LL_STRUCTURE_KEY, "Libellé Groupes", 150);
		col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);

    	myTableModelGroupes = new ZEOTableModel(eodGroupes, myCols);
		myTableSorterGroupes = new TableSorter(myTableModelGroupes);

		myEOTableGroupes = new ZEOTable(myTableSorterGroupes);
		//myEOTableGroupes.addListener(myListenerContrat);
		myTableSorterGroupes.setTableHeader(myEOTableGroupes.getTableHeader());		

		myEOTableGroupes.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTableGroupes.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTableGroupes.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		
		viewTableGroupes.setLayout(new BorderLayout());
		viewTableGroupes.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableGroupes.removeAll();
		viewTableGroupes.add(new JScrollPane(myEOTableGroupes), BorderLayout.CENTER);

		
		
    }

	public ZEOTable getMyEOTableGroupes() {
		return myEOTableGroupes;
	}

	public void setMyEOTableGroupes(ZEOTable myEOTableGroupes) {
		this.myEOTableGroupes = myEOTableGroupes;
	}

	public ZEOTable getMyEOTableRoles() {
		return myEOTableRoles;
	}

	public void setMyEOTableRoles(ZEOTable myEOTableRoles) {
		this.myEOTableRoles = myEOTableRoles;
	}

	public javax.swing.JButton getBtnAJouterAssociation() {
		return btnAJouterAssociation;
	}

	public void setBtnAJouterAssociation(javax.swing.JButton btnAJouterAssociation) {
		this.btnAJouterAssociation = btnAJouterAssociation;
	}

	public javax.swing.JButton getBtnAJouterGroupe() {
		return btnAJouterGroupe;
	}

	public void setBtnAJouterGroupe(javax.swing.JButton btnAJouterGroupe) {
		this.btnAJouterGroupe = btnAJouterGroupe;
	}

	public javax.swing.JButton getBtnModifierAssociation() {
		return btnModifierAssociation;
	}

	public void setBtnModifierAssociation(javax.swing.JButton btnModifierAssociation) {
		this.btnModifierAssociation = btnModifierAssociation;
	}

	public javax.swing.JButton getBtnSupprimerAssociation() {
		return btnSupprimerAssociation;
	}

	public void setBtnSupprimerAssociation(
			javax.swing.JButton btnSupprimerAssociation) {
		this.btnSupprimerAssociation = btnSupprimerAssociation;
	}

	public javax.swing.JButton getBtnSupprimerGroupe() {
		return btnSupprimerGroupe;
	}

	public javax.swing.JCheckBox getCheckAllRoles() {
		return checkAllRoles;
	}

	public void setCheckAllRoles(javax.swing.JCheckBox checkAllRoles) {
		this.checkAllRoles = checkAllRoles;
	}

	public javax.swing.JCheckBox getCheckRolesStructure() {
		return checkRolesStructure;
	}

	public void setCheckRolesStructure(javax.swing.JCheckBox checkRolesStructure) {
		this.checkRolesStructure = checkRolesStructure;
	}

	public void setBtnSupprimerGroupe(javax.swing.JButton btnSupprimerGroupe) {
		this.btnSupprimerGroupe = btnSupprimerGroupe;
	}

	public javax.swing.JCheckBox getCheckAllGroupes() {
		return checkAllGroupes;
	}

	public void setCheckAllGroupes(javax.swing.JCheckBox checkAllGroupes) {
		this.checkAllGroupes = checkAllGroupes;
	}

	public javax.swing.JCheckBox getCheckServices() {
		return checkServices;
	}

	public void setCheckServices(javax.swing.JCheckBox checkServices) {
		this.checkServices = checkServices;
	}

	public javax.swing.JPanel getViewRoles() {
		return viewRoles;
	}

	public void setViewRoles(javax.swing.JPanel viewRoles) {
		this.viewRoles = viewRoles;
	}

}

//Created on Wed Jun 11 12:24:00 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.gui.swapviews;

import java.awt.CardLayout;

import javax.swing.JPanel;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.client.carrieres.CarrieresCtrl;

import com.webobjects.foundation.NSArray;

/** 

 * @author christine
 *
 */
// 18/11/09 - modification de la suppression des attributions pour supprimer la personnalisation (si elle existe) liée à l'attribution à supprimer
// 22/12/2010 - Adaptation Netbeans
// 16/03/2011 - rajout d'un dialogue dans l'impression pour signaler que la fonctionnalité n'est pas disponible
// 23/03/2011 - impression des primes
public class GestionSwapViewCarrieres  extends ModelePage {

	private static final String ID_CARD_LAYOUT = "MODALITES";
	public JPanel swapView;

	// Méthodes protégées
	protected void preparerFenetre() {
		swapView.add(ID_CARD_LAYOUT,CarrieresCtrl.sharedInstance(editingContext()).getViewCarrieres());
		((CardLayout)swapView.getLayout()).show(swapView, ID_CARD_LAYOUT);				
		super.preparerFenetre(); // à la fin pour que le contrôleur d'attribution soit initialisé
	}
	
	protected void afficherExceptionValidation(String message) {
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected NSArray fetcherObjets() {
		return new NSArray();
	}
	protected void parametrerDisplayGroup() {
	}
	protected boolean conditionSurPageOK() {
		return true;
	}
	protected void traitementsPourCreation() {
	}
	protected  boolean traitementsPourSuppression() {
		return true;
	}
	protected void traitementsApresSuppression() {
	}
	/** methode a surcharger pour faire des traitements avant l'enregistrement des donnees dans la base
	 * retourne true si on peut enregistrer */
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void traitementsApresValidation() {
	}
	protected void traitementsApresRevert() {
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ?";
	}
	protected void terminer() {
	}
}
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.nomenclatures;

import org.cocktail.mangue.client.gui.cir.CirSaisieFicheIdentiteView;
import org.cocktail.mangue.common.utilities.CocktailIcones;

/**
 *
 * @author  cpinsard
 */
public class SaisieTypePopulationView extends javax.swing.JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7274843962894206085L;

	/** Creates new form TemplateJDialog */
	public SaisieTypePopulationView(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
		initGui();
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnValider = new javax.swing.JButton();
        btnAnnuler = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        tfCode = new javax.swing.JTextField();
        tfLibelleCourt = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        tfLibelle = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        tfDateFermeture = new javax.swing.JTextField();
        tfDateOuverture = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        checkVisible = new javax.swing.JCheckBox();
        tfTitre = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Saisie / Modification d'un type de contrat");
        setResizable(false);

        btnValider.setText("Valider");
        btnValider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValiderActionPerformed(evt);
            }
        });

        btnAnnuler.setText("Annuler");
        btnAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnnulerActionPerformed(evt);
            }
        });

        tfCode.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfCode.setToolTipText("");
        tfCode.setFocusable(false);

        tfLibelleCourt.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfLibelleCourt.setToolTipText("");

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Code");

        tfLibelle.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        tfLibelle.setToolTipText("");
        tfLibelle.setFocusable(false);

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Libellé");

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Libellé Court");

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Fermeture");

        tfDateFermeture.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateFermeture.setToolTipText("");

        tfDateOuverture.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateOuverture.setToolTipText("");

        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Ouverture");

        checkVisible.setText("Visible");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel17, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel14, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                    .add(jLabel12, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(tfLibelleCourt, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 190, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(jPanel1Layout.createSequentialGroup()
                            .add(tfDateOuverture, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 105, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jLabel18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 88, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(tfDateFermeture, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 104, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(checkVisible))
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1Layout.createSequentialGroup()
                            .add(tfCode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 52, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jLabel13, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 63, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(tfLibelle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 356, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel14)
                    .add(jLabel13)
                    .add(tfCode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfLibelle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(27, 27, 27)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel12)
                    .add(tfLibelleCourt, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(29, 29, 29)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel17)
                    .add(jLabel18)
                    .add(tfDateFermeture, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfDateOuverture, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(checkVisible))
                .add(43, 43, 43))
        );

        tfTitre.setBackground(new java.awt.Color(102, 102, 255));
        tfTitre.setEditable(false);
        tfTitre.setFont(new java.awt.Font("Times New Roman", 0, 14));
        tfTitre.setForeground(new java.awt.Color(255, 255, 204));
        tfTitre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfTitre.setText("TYPE POPULATION");
        tfTitre.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        tfTitre.setFocusable(false);
        tfTitre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfTitreActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(tfTitre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 637, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(layout.createSequentialGroup()
                                .add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .add(94, 94, 94))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(tfTitre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 173, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnValider)
                    .add(btnAnnuler))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-654)/2, (screenSize.height-317)/2, 654, 317);
    }// </editor-fold>//GEN-END:initComponents

	private void btnValiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValiderActionPerformed
		// TODO add your handling code here:
	}//GEN-LAST:event_btnValiderActionPerformed

	private void btnAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnnulerActionPerformed
		// TODO add your handling code here:
	}//GEN-LAST:event_btnAnnulerActionPerformed

        private void tfTitreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfTitreActionPerformed
            // TODO add your handling code here:
}//GEN-LAST:event_tfTitreActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				CirSaisieFicheIdentiteView dialog = new CirSaisieFicheIdentiteView(new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JButton btnAnnuler;
    protected javax.swing.JButton btnValider;
    private javax.swing.JCheckBox checkVisible;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JPanel jPanel1;
    protected javax.swing.JTextField tfCode;
    protected javax.swing.JTextField tfDateFermeture;
    protected javax.swing.JTextField tfDateOuverture;
    protected javax.swing.JTextField tfLibelle;
    protected javax.swing.JTextField tfLibelleCourt;
    private javax.swing.JTextField tfTitre;
    // End of variables declaration//GEN-END:variables

    
	private void initGui() {

		setTitle("Modification d'un type de population");
		btnValider.setIcon(CocktailIcones.ICON_VALID);
		btnAnnuler.setIcon(CocktailIcones.ICON_CANCEL);

	}

	public javax.swing.JButton getBtnAnnuler() {
		return btnAnnuler;
	}

	public void setBtnAnnuler(javax.swing.JButton btnAnnuler) {
		this.btnAnnuler = btnAnnuler;
	}

	public javax.swing.JButton getBtnValider() {
		return btnValider;
	}

	public void setBtnValider(javax.swing.JButton btnValider) {
		this.btnValider = btnValider;
	}

	public javax.swing.JTextField getTfCode() {
		return tfCode;
	}

	public javax.swing.JCheckBox getCheckVisible() {
		return checkVisible;
	}

	public void setCheckVisible(javax.swing.JCheckBox checkVisible) {
		this.checkVisible = checkVisible;
	}

	public void setTfCode(javax.swing.JTextField tfCode) {
		this.tfCode = tfCode;
	}

	public javax.swing.JTextField getTfDateFermeture() {
		return tfDateFermeture;
	}

	public void setTfDateFermeture(javax.swing.JTextField tfDateFermeture) {
		this.tfDateFermeture = tfDateFermeture;
	}

	public javax.swing.JTextField getTfDateOuverture() {
		return tfDateOuverture;
	}

	public void setTfDateOuverture(javax.swing.JTextField tfDateOuverture) {
		this.tfDateOuverture = tfDateOuverture;
	}

	public javax.swing.JTextField getTfLibelle() {
		return tfLibelle;
	}

	public void setTfLibelle(javax.swing.JTextField tfLibelle) {
		this.tfLibelle = tfLibelle;
	}

	public javax.swing.JTextField getTfLibelleCourt() {
		return tfLibelleCourt;
	}

	public void setTfLibelleCourt(javax.swing.JTextField tfLibelleCourt) {
		this.tfLibelleCourt = tfLibelleCourt;
	}

	public javax.swing.JTextField getTfTitre() {
		return tfTitre;
	}

	public void setTfTitre(javax.swing.JTextField tfTitre) {
		this.tfTitre = tfTitre;
	}


}

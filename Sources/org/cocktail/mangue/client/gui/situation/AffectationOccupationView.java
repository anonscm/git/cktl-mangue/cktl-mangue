/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.situation;

import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.emploi._EOProgramme;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.mangue.common.utilities.CocktailIcones;
import org.cocktail.mangue.common.utilities.ManGUEIcones;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.grhum.referentiel._EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.EOSituationGeographique;

import com.webobjects.eointerface.EODisplayGroup;


/**
 *
 * @author  cpinsard
 */
public class AffectationOccupationView extends JDialog {

protected EODisplayGroup eodAffectation, eodOccupation, eodSituationGeo;
	protected ZEOTable myEOTableAffectation, myEOTableOccupation, myEOTableSituationGeo;
	protected ZEOTableModel myTableModelAffectation, myTableModelOccupation, myTableModelSituationGeo;
	protected TableSorter myTableSorterAffectation,  myTableSorterOccupation, myTableSorterSituationGeo;
	private ZEOTableCellRenderer myRendererSituation;

	private static final long serialVersionUID = -6195866169790317637L;

	/** Creates new form TemplateJDialog */
    public AffectationOccupationView(java.awt.Frame parent, boolean modal, EODisplayGroup eodAffectation, 
    		EODisplayGroup eodOccupation, EODisplayGroup eodSituationGeo, ZEOTableCellRenderer rendererSituation) {
        super(parent, modal);
    	
    	this.eodAffectation = eodAffectation;
    	this.eodOccupation = eodOccupation;
    	this.eodSituationGeo = eodSituationGeo;
    	myRendererSituation = rendererSituation;
    	
        initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewAffectation = new javax.swing.JPanel();
        btnAjouter = new javax.swing.JButton();
        viewTableAffectation = new javax.swing.JPanel();
        btnSupprimer = new javax.swing.JButton();
        btnModifier = new javax.swing.JButton();
        btnRenouveler = new javax.swing.JButton();
        btnRenouvelerOccupation = new javax.swing.JButton();
        btnModifierOccupation = new javax.swing.JButton();
        btnSupprimerOccupation = new javax.swing.JButton();
        viewTableOccupation = new javax.swing.JPanel();
        btnAjouterOccupation = new javax.swing.JButton();
        viewTableSituationGeo = new javax.swing.JPanel();
        btnAjouterSituation = new javax.swing.JButton();
        btnModifierSituation = new javax.swing.JButton();
        btnSupprimerSituation = new javax.swing.JButton();
        tfTitreDetailContrat3 = new javax.swing.JTextField();
        tfTitreDetailContrat4 = new javax.swing.JTextField();
        tfTitreDetailContrat5 = new javax.swing.JTextField();
        btnAfficherEmplois = new javax.swing.JButton();
        tfStructurePere = new javax.swing.JTextField();
        tfAdresseSituation = new javax.swing.JTextField();
        tfDetailOccupation = new javax.swing.JTextField();
        btnClose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("GESTION DES AFFECTATIONS");
        setResizable(false);

        btnAjouter.setToolTipText("Ajouter une nouvelle affectation");

        viewTableAffectation.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        viewTableAffectation.setLayout(new java.awt.BorderLayout());

        btnSupprimer.setToolTipText("Suppression de l'affectation courante");

        btnModifier.setToolTipText("Modifier l'affectation courante");

        btnRenouveler.setToolTipText("Renouveler l'affectation courante");

        btnRenouvelerOccupation.setToolTipText("Renouveler l'occupation courante");

        btnModifierOccupation.setToolTipText("Modifier l'occupation courante");

        btnSupprimerOccupation.setToolTipText("Suppression de l'occupation courante");

        viewTableOccupation.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        viewTableOccupation.setLayout(new java.awt.BorderLayout());

        btnAjouterOccupation.setToolTipText("Ajouter une nouvelle occupation");

        viewTableSituationGeo.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        viewTableSituationGeo.setLayout(new java.awt.BorderLayout());

        btnAjouterSituation.setToolTipText("Ajouter une nouvelle situation");

        btnModifierSituation.setToolTipText("Modifier la situation courante");

        btnSupprimerSituation.setToolTipText("Suppression de la situation courante");

        tfTitreDetailContrat3.setBackground(new java.awt.Color(255, 204, 204));
        tfTitreDetailContrat3.setEditable(false);
        tfTitreDetailContrat3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfTitreDetailContrat3.setText("AFFECTATIONS");
        tfTitreDetailContrat3.setAutoscrolls(false);
        tfTitreDetailContrat3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tfTitreDetailContrat4.setBackground(new java.awt.Color(255, 204, 204));
        tfTitreDetailContrat4.setEditable(false);
        tfTitreDetailContrat4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfTitreDetailContrat4.setText("OCCUPATIONS");
        tfTitreDetailContrat4.setAutoscrolls(false);
        tfTitreDetailContrat4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tfTitreDetailContrat5.setBackground(new java.awt.Color(255, 204, 204));
        tfTitreDetailContrat5.setEditable(false);
        tfTitreDetailContrat5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfTitreDetailContrat5.setText("SITUATION GEOGRAPHIQUE");
        tfTitreDetailContrat5.setAutoscrolls(false);
        tfTitreDetailContrat5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnAfficherEmplois.setToolTipText("Affichage des emplois");

        org.jdesktop.layout.GroupLayout viewAffectationLayout = new org.jdesktop.layout.GroupLayout(viewAffectation);
        viewAffectation.setLayout(viewAffectationLayout);
        viewAffectationLayout.setHorizontalGroup(
            viewAffectationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewAffectationLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewAffectationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewAffectationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(tfStructurePere, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 639, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(viewTableAffectation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 639, Short.MAX_VALUE)
                        .add(viewAffectationLayout.createSequentialGroup()
                            .add(tfTitreDetailContrat5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 548, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(btnAjouterSituation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnModifierSituation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnSupprimerSituation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(viewAffectationLayout.createSequentialGroup()
                            .add(tfTitreDetailContrat3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 517, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnRenouveler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(viewTableSituationGeo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(tfAdresseSituation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 639, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(viewAffectationLayout.createSequentialGroup()
                            .add(tfTitreDetailContrat4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 493, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(3, 3, 3)
                            .add(btnAfficherEmplois, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnAjouterOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnModifierOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnSupprimerOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(btnRenouvelerOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(viewAffectationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, viewTableOccupation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, tfDetailOccupation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 631, Short.MAX_VALUE)))
                .add(36, 36, 36))
        );
        viewAffectationLayout.setVerticalGroup(
            viewAffectationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewAffectationLayout.createSequentialGroup()
                .add(5, 5, 5)
                .add(viewAffectationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(tfTitreDetailContrat3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnRenouveler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(viewTableAffectation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 172, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(3, 3, 3)
                .add(tfStructurePere, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(viewAffectationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(btnRenouvelerOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnModifierOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnSupprimerOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnAjouterOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(tfTitreDetailContrat4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnAfficherEmplois, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(viewTableOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 158, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tfDetailOccupation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(19, 19, 19)
                .add(viewAffectationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(tfTitreDetailContrat5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnSupprimerSituation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnModifierSituation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnAjouterSituation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(13, 13, 13)
                .add(viewTableSituationGeo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 108, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tfAdresseSituation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        btnClose.setText("Fermer");
        btnClose.setToolTipText("Fermer la fenêtre");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(btnClose, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 132, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(viewAffectation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 685, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(viewAffectation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 672, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(btnClose)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-711)/2, (screenSize.height-766)/2, 711, 766);
    }// </editor-fold>//GEN-END:initComponents

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                AffectationOccupationView dialog = new AffectationOccupationView(new javax.swing.JFrame(), true, null, null, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
					public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAfficherEmplois;
    private javax.swing.JButton btnAjouter;
    private javax.swing.JButton btnAjouterOccupation;
    private javax.swing.JButton btnAjouterSituation;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnModifier;
    private javax.swing.JButton btnModifierOccupation;
    private javax.swing.JButton btnModifierSituation;
    private javax.swing.JButton btnRenouveler;
    private javax.swing.JButton btnRenouvelerOccupation;
    private javax.swing.JButton btnSupprimer;
    private javax.swing.JButton btnSupprimerOccupation;
    private javax.swing.JButton btnSupprimerSituation;
    private javax.swing.JTextField tfAdresseSituation;
    private javax.swing.JTextField tfDetailOccupation;
    private javax.swing.JTextField tfStructurePere;
    private javax.swing.JTextField tfTitreDetailContrat3;
    private javax.swing.JTextField tfTitreDetailContrat4;
    private javax.swing.JTextField tfTitreDetailContrat5;
    private javax.swing.JPanel viewAffectation;
    protected javax.swing.JPanel viewTableAffectation;
    protected javax.swing.JPanel viewTableOccupation;
    protected javax.swing.JPanel viewTableSituationGeo;
    // End of variables declaration//GEN-END:variables

   
    private void initGui() {

    	setTitle("GESTION DES AFFECTATIONS ET OCCUPATIONS");
    	
    	btnAjouter.setIcon(CocktailIcones.ICON_ADD);
    	btnModifier.setIcon(CocktailIcones.ICON_UPDATE);
    	btnSupprimer.setIcon(CocktailIcones.ICON_DELETE);
    	btnRenouveler.setIcon(CocktailIcones.ICON_RENOUVELER);
    	btnClose.setIcon(CocktailIcones.ICON_CLOSE);
    	
    	btnAjouterOccupation.setIcon(CocktailIcones.ICON_ADD);
    	btnModifierOccupation.setIcon(CocktailIcones.ICON_UPDATE);
    	btnSupprimerOccupation.setIcon(CocktailIcones.ICON_DELETE);
    	btnRenouvelerOccupation.setIcon(CocktailIcones.ICON_RENOUVELER);

    	btnAjouterSituation.setIcon(CocktailIcones.ICON_ADD);
    	btnModifierSituation.setIcon(CocktailIcones.ICON_UPDATE);
    	btnSupprimerSituation.setIcon(CocktailIcones.ICON_DELETE);
    	btnAfficherEmplois.setIcon(ManGUEIcones.ICON_EMPLOI);

		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

    	ZEOTableModelColumn col = new ZEOTableModelColumn(eodAffectation, EOAffectation.DATE_DEBUT_KEY, "Début", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(new SimpleDateFormat("dd/MM/yyyy"));
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodAffectation,  EOAffectation.DATE_FIN_KEY, "Fin", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(new SimpleDateFormat("dd/MM/yyyy"));
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodAffectation, EOAffectation.TO_STRUCTURE_ULR_KEY+"."+_EOStructure.LL_STRUCTURE_KEY, "Structure", 250);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodAffectation, EOAffectation.QUOTITE_KEY, "Quot", 50);
    	col.setAlignment(SwingConstants.RIGHT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodAffectation, EOAffectation.TEM_PRINCIPALE_KEY, "Princ.", 50);
    	col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);

    	myTableModelAffectation = new ZEOTableModel(eodAffectation, myCols);
		myTableSorterAffectation = new TableSorter(myTableModelAffectation);

		myEOTableAffectation = new ZEOTable(myTableSorterAffectation);
		myTableSorterAffectation.setTableHeader(myEOTableAffectation.getTableHeader());		

//		myEOTable.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTableAffectation.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTableAffectation.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableAffectation.setLayout(new BorderLayout());
		viewTableAffectation.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableAffectation.removeAll();
		viewTableAffectation.add(new JScrollPane(myEOTableAffectation), BorderLayout.CENTER);

		
		myCols = new Vector<ZEOTableModelColumn>();

    	col = new ZEOTableModelColumn(eodOccupation, EOOccupation.DATE_DEBUT_KEY, "Début", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodOccupation,  EOOccupation.DATE_FIN_KEY, "Fin", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodOccupation, EOOccupation.TO_EMPLOI_KEY+"."+ EOEmploi.NO_EMPLOI_AFFICHAGE_KEY, "Emploi", 100);
    	col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodOccupation, EOOccupation.QUOTITE_KEY, "Quot", 50);
    	col.setAlignment(SwingConstants.RIGHT);
    	col.setFormatDisplay(CocktailFormats.FORMAT_DECIMAL);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodOccupation, EOOccupation.TO_EMPLOI_KEY+"."+ EOEmploi.TO_PROGRAMME_KEY+"."+_EOProgramme.C_PROGRAMME_KEY, "Prog", 75);
    	col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodOccupation, EOOccupation.TO_EMPLOI_KEY+"."+ EOEmploi.TO_RNE_KEY+"."+ INomenclature.LIBELLE_COURT_KEY, "Imp", 75);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
//    	col = new ZEOTableModelColumn(eodOccupation, EOOccupation.TO_EMPLOI_KEY+"."+ EOEmploi.LISTE_EMPLOI_NATURE_BUDGETS_KEY+"."+ EOEmploiNatureBudget.TO_NATURE_BUDGET_KEY
//    			+ "." + EONatureBudget.LC_BUDGET_KEY , "Budget", 75);
//    	col.setAlignment(SwingConstants.CENTER);
//    	myCols.add(col);

    	myTableModelOccupation = new ZEOTableModel(eodOccupation, myCols);
		myTableSorterOccupation = new TableSorter(myTableModelOccupation);

		myEOTableOccupation = new ZEOTable(myTableSorterOccupation);
		myTableSorterOccupation.setTableHeader(myEOTableOccupation.getTableHeader());		
		myEOTableOccupation.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTableOccupation.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableOccupation.setLayout(new BorderLayout());
		viewTableOccupation.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableOccupation.removeAll();
		viewTableOccupation.add(new JScrollPane(myEOTableOccupation), BorderLayout.CENTER);

		
		
		myCols = new Vector<ZEOTableModelColumn>();

    	col = new ZEOTableModelColumn(eodSituationGeo, EOSituationGeographique.DATE_DEBUT_KEY, "Début", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
		col.setTableCellRenderer(myRendererSituation);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodSituationGeo,  EOSituationGeographique.DATE_FIN_KEY, "Fin", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
		col.setTableCellRenderer(myRendererSituation);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodSituationGeo, EOSituationGeographique.STRUCTURE_KEY+"."+EOStructure.LL_STRUCTURE_KEY, "Structure", 300);
    	col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(myRendererSituation);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodSituationGeo, EOSituationGeographique.QUOTITE_KEY, "Quotité", 50);
    	col.setAlignment(SwingConstants.RIGHT);
		col.setTableCellRenderer(myRendererSituation);
    	myCols.add(col);

    	myTableModelSituationGeo = new ZEOTableModel(eodSituationGeo, myCols);
		myTableSorterSituationGeo = new TableSorter(myTableModelSituationGeo);

		myEOTableSituationGeo = new ZEOTable(myTableSorterSituationGeo);
		myTableSorterSituationGeo.setTableHeader(myEOTableSituationGeo.getTableHeader());		
		myEOTableSituationGeo.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTableSituationGeo.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableSituationGeo.setLayout(new BorderLayout());
		viewTableSituationGeo.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableSituationGeo.removeAll();
		viewTableSituationGeo.add(new JScrollPane(myEOTableSituationGeo), BorderLayout.CENTER);

    }

	public ZEOTable getMyEOTableAffectation() {
		return myEOTableAffectation;
	}

	public void setMyEOTableAffectation(ZEOTable myEOTableAffectation) {
		this.myEOTableAffectation = myEOTableAffectation;
	}

	public ZEOTable getMyEOTableOccupation() {
		return myEOTableOccupation;
	}

	public void setMyEOTableOccupation(ZEOTable myEOTableOccupation) {
		this.myEOTableOccupation = myEOTableOccupation;
	}

	public javax.swing.JButton getBtnAjouter() {
		return btnAjouter;
	}

	public void setBtnAjouter(javax.swing.JButton btnAjouter) {
		this.btnAjouter = btnAjouter;
	}

	public javax.swing.JButton getBtnAjouterOccupation() {
		return btnAjouterOccupation;
	}

	public void setBtnAjouterOccupation(javax.swing.JButton btnAjouterOccupation) {
		this.btnAjouterOccupation = btnAjouterOccupation;
	}

	public javax.swing.JButton getBtnClose() {
		return btnClose;
	}

	public void setBtnClose(javax.swing.JButton btnClose) {
		this.btnClose = btnClose;
	}

	public javax.swing.JButton getBtnModifier() {
		return btnModifier;
	}

	public void setBtnModifier(javax.swing.JButton btnModifier) {
		this.btnModifier = btnModifier;
	}

	public javax.swing.JButton getBtnModifierOccupation() {
		return btnModifierOccupation;
	}

	public void setBtnModifierOccupation(javax.swing.JButton btnModifierOccupation) {
		this.btnModifierOccupation = btnModifierOccupation;
	}

	public javax.swing.JButton getBtnRenouveler() {
		return btnRenouveler;
	}

	public void setBtnRenouveler(javax.swing.JButton btnRenouveler) {
		this.btnRenouveler = btnRenouveler;
	}

	public javax.swing.JButton getBtnRenouvelerOccupation() {
		return btnRenouvelerOccupation;
	}

	public void setBtnRenouvelerOccupation(
			javax.swing.JButton btnRenouvelerOccupation) {
		this.btnRenouvelerOccupation = btnRenouvelerOccupation;
	}

	public javax.swing.JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	public void setBtnSupprimer(javax.swing.JButton btnSupprimer) {
		this.btnSupprimer = btnSupprimer;
	}

	public javax.swing.JButton getBtnSupprimerOccupation() {
		return btnSupprimerOccupation;
	}

	public void setBtnSupprimerOccupation(javax.swing.JButton btnSupprimerOccupation) {
		this.btnSupprimerOccupation = btnSupprimerOccupation;
	}

	public javax.swing.JPanel getViewAffectation() {
		return viewAffectation;
	}

	public void setViewAffectation(javax.swing.JPanel viewAffectation) {
		this.viewAffectation = viewAffectation;
	}

	public ZEOTable getMyEOTableSituationGeo() {
		return myEOTableSituationGeo;
	}

	public void setMyEOTableSituationGeo(ZEOTable myEOTableSituationGeo) {
		this.myEOTableSituationGeo = myEOTableSituationGeo;
	}

	public javax.swing.JButton getBtnAjouterSituation() {
		return btnAjouterSituation;
	}

	public void setBtnAjouterSituation(javax.swing.JButton btnAjouterSituation) {
		this.btnAjouterSituation = btnAjouterSituation;
	}

	public javax.swing.JButton getBtnModifierSituation() {
		return btnModifierSituation;
	}

	public void setBtnModifierSituation(javax.swing.JButton btnModifierSituation) {
		this.btnModifierSituation = btnModifierSituation;
	}

	public javax.swing.JButton getBtnSupprimerSituation() {
		return btnSupprimerSituation;
	}

	public void setBtnSupprimerSituation(javax.swing.JButton btnSupprimerSituation) {
		this.btnSupprimerSituation = btnSupprimerSituation;
	}

	public javax.swing.JButton getBtnAfficherEmplois() {
		return btnAfficherEmplois;
	}

	public void setBtnAfficherEmplois(javax.swing.JButton btnAfficherEmplois) {
		this.btnAfficherEmplois = btnAfficherEmplois;
	}

	public javax.swing.JTextField getTfAdresseSituation() {
		return tfAdresseSituation;
	}

	public void setTfAdresseSituation(javax.swing.JTextField tfAdresseSituation) {
		this.tfAdresseSituation = tfAdresseSituation;
	}

	public javax.swing.JTextField getTfStructurePere() {
		return tfStructurePere;
	}

	public void setTfStructurePere(javax.swing.JTextField tfStructurePere) {
		this.tfStructurePere = tfStructurePere;
	}

	public javax.swing.JTextField getTfDetailOccupation() {
		return tfDetailOccupation;
	}

	public void setTfDetailOccupation(javax.swing.JTextField tfDetailOccupation) {
		this.tfDetailOccupation = tfDetailOccupation;
	}

    
}

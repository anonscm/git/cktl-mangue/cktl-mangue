/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.visas;

import org.cocktail.mangue.common.utilities.CocktailIcones;

/**
 *
 * @author  cpinsard
 */
public class SaisieVisaView extends javax.swing.JDialog {

	private static final long serialVersionUID = -7274843962894206085L;

	/** Creates new form TemplateJDialog */
	public SaisieVisaView(java.awt.Frame parent,  boolean modal) {
		super(parent, modal);
		initComponents();
		initGui();
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel17 = new javax.swing.JLabel();
        tfDateDebut = new javax.swing.JTextField();
        btnAnnuler = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        tfDateFin = new javax.swing.JTextField();
        btnValider = new javax.swing.JButton();
        lblTypeModalite2 = new javax.swing.JLabel();
        temDeconcentration = new javax.swing.JCheckBox();
        scroll = new javax.swing.JScrollPane();
        taVisa = new javax.swing.JTextArea();
        lblTypeModalite3 = new javax.swing.JLabel();
        tfType = new javax.swing.JTextField();
        temLocal = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Saisie / Modification des employeurs");

        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Début");

        tfDateDebut.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateDebut.setToolTipText("Libellé du code");

        btnAnnuler.setText("Annuler");
        btnAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnnulerActionPerformed(evt);
            }
        });

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Fin ");

        tfDateFin.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateFin.setToolTipText("Libellé du code");

        btnValider.setText("Valider");
        btnValider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValiderActionPerformed(evt);
            }
        });

        lblTypeModalite2.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblTypeModalite2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTypeModalite2.setText("TYPE");

        temDeconcentration.setText("Déconcentration");
        temDeconcentration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                temDeconcentrationActionPerformed(evt);
            }
        });

        taVisa.setColumns(20);
        taVisa.setRows(5);
        scroll.setViewportView(taVisa);

        lblTypeModalite3.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblTypeModalite3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTypeModalite3.setText("VISA");

        tfType.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfType.setToolTipText("Libellé du code");

        temLocal.setText("Local");
        temLocal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                temLocalActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(lblTypeModalite2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(tfType, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 296, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jLabel17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 72, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfDateDebut, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 98, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tfDateFin, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 93, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 239, Short.MAX_VALUE)
                        .add(temLocal)
                        .add(10, 10, 10)
                        .add(temDeconcentration))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(6, 6, 6)
                        .add(lblTypeModalite3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(scroll, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 660, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblTypeModalite2)
                    .add(tfType, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(31, 31, 31)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lblTypeModalite3)
                    .add(scroll, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 77, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 14, Short.MAX_VALUE)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(temDeconcentration)
                            .add(temLocal))
                        .add(1, 1, 1))
                    .add(layout.createSequentialGroup()
                        .add(18, 18, 18)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel17)
                            .add(tfDateDebut, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel18)
                            .add(tfDateFin, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .add(78, 78, 78)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnValider)
                    .add(btnAnnuler))
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-772)/2, (screenSize.height-327)/2, 772, 327);
    }// </editor-fold>//GEN-END:initComponents

	private void btnValiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValiderActionPerformed
		// TODO add your handling code here:
	}//GEN-LAST:event_btnValiderActionPerformed

	private void btnAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnnulerActionPerformed
		// TODO add your handling code here:
	}//GEN-LAST:event_btnAnnulerActionPerformed

        private void temDeconcentrationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_temDeconcentrationActionPerformed
            // TODO add your handling code here:
        }//GEN-LAST:event_temDeconcentrationActionPerformed

        private void temLocalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_temLocalActionPerformed
            // TODO add your handling code here:
        }//GEN-LAST:event_temLocalActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				SaisieVisaView dialog = new SaisieVisaView(new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JButton btnAnnuler;
    protected javax.swing.JButton btnValider;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel lblTypeModalite2;
    private javax.swing.JLabel lblTypeModalite3;
    private javax.swing.JScrollPane scroll;
    private javax.swing.JTextArea taVisa;
    private javax.swing.JCheckBox temDeconcentration;
    private javax.swing.JCheckBox temLocal;
    protected javax.swing.JTextField tfDateDebut;
    protected javax.swing.JTextField tfDateFin;
    protected javax.swing.JTextField tfType;
    // End of variables declaration//GEN-END:variables


	private void initGui() {

		setTitle("Saisie / Modification d'un VISA");
		btnValider.setIcon(CocktailIcones.ICON_VALID);
		btnAnnuler.setIcon(CocktailIcones.ICON_CANCEL);

	}

	public javax.swing.JButton getBtnAnnuler() {
		return btnAnnuler;
	}

	public void setBtnAnnuler(javax.swing.JButton btnAnnuler) {
		this.btnAnnuler = btnAnnuler;
	}

	public javax.swing.JButton getBtnValider() {
		return btnValider;
	}

	public void setBtnValider(javax.swing.JButton btnValider) {
		this.btnValider = btnValider;
	}

	public javax.swing.JLabel getLblTypeModalite2() {
		return lblTypeModalite2;
	}

	public void setLblTypeModalite2(javax.swing.JLabel lblTypeModalite2) {
		this.lblTypeModalite2 = lblTypeModalite2;
	}

	public javax.swing.JCheckBox getTemDeconcentration() {
		return temDeconcentration;
	}

	public void setTemDeconcentration(javax.swing.JCheckBox temDeconcentration) {
		this.temDeconcentration = temDeconcentration;
	}

	public javax.swing.JTextField getTfDateDebut() {
		return tfDateDebut;
	}

	public void setTfDateDebut(javax.swing.JTextField tfDateDebut) {
		this.tfDateDebut = tfDateDebut;
	}

	public javax.swing.JTextField getTfDateFin() {
		return tfDateFin;
	}

	public void setTfDateFin(javax.swing.JTextField tfDateFin) {
		this.tfDateFin = tfDateFin;
	}

	public javax.swing.JTextArea getTaVisa() {
		return taVisa;
	}

	public void setTaVisa(javax.swing.JTextArea taVisa) {
		this.taVisa = taVisa;
	}

	public javax.swing.JTextField getTfType() {
		return tfType;
	}

	public void setTfType(javax.swing.JTextField tfType) {
		this.tfType = tfType;
	}

	public javax.swing.JCheckBox getTemLocal() {
		return temLocal;
	}

	public void setTemLocal(javax.swing.JCheckBox temLocal) {
		this.temLocal = temLocal;
	}


}

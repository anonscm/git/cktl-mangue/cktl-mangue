/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.contrats;

import java.awt.BorderLayout;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.mangue.common.utilities.CocktailIcones;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.eointerface.EODisplayGroup;

/**
 *
 * @author  cpinsard
 */
public class ContratsAvenantsView extends javax.swing.JPanel {

	private static final long serialVersionUID = -7770031412248058797L;
	protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;

    /** Creates new form TemplateJPanel */
    public ContratsAvenantsView(EODisplayGroup dg) {
                
    	eod = dg;
        initComponents();
        initGui();
        
    }

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewTable = new javax.swing.JPanel();
        btnAjouter = new javax.swing.JButton();
        btnModifier = new javax.swing.JButton();
        btnSupprimer = new javax.swing.JButton();
        btnRenouveler = new javax.swing.JButton();
        btnDetailPaie = new javax.swing.JButton();
        btnImprimer = new javax.swing.JButton();
        btnActivites = new javax.swing.JButton();
        lblSpecialite = new javax.swing.JLabel();

        setAutoscrolls(true);

        viewTable.setLayout(new java.awt.BorderLayout());

        btnAjouter.setToolTipText("Ajouter un nouvel avenant");

        btnModifier.setToolTipText("Modifier l'avenant sélectionné");

        btnSupprimer.setToolTipText("Annuler l'avenant sélectionné");

        btnRenouveler.setToolTipText("Renouveler l'avenant");
        btnRenouveler.setFocusable(false);

        btnDetailPaie.setText("Détail Paie");
        btnDetailPaie.setToolTipText("Détail des données PAIE");
        btnDetailPaie.setFocusable(false);
        btnDetailPaie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetailPaieActionPerformed(evt);
            }
        });

        btnImprimer.setText("Imprimer");
        btnImprimer.setToolTipText("Imprimer le contrat");
        btnImprimer.setFocusable(false);
        btnImprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimerActionPerformed(evt);
            }
        });

        btnActivites.setText("Activités (2)");
        btnActivites.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActivitesActionPerformed(evt);
            }
        });

        lblSpecialite.setText("jLabel1");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(layout.createSequentialGroup()
                                .add(3, 3, 3)
                                .add(viewTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 613, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createSequentialGroup()
                                .addContainerGap()
                                .add(btnActivites, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnDetailPaie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 116, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(btnImprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 111, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnRenouveler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(lblSpecialite, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 635, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnRenouveler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 126, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnActivites)
                    .add(btnDetailPaie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnImprimer))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(lblSpecialite)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnDetailPaieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetailPaieActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnDetailPaieActionPerformed

    private void btnImprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnImprimerActionPerformed

    private void btnActivitesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActivitesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnActivitesActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActivites;
    private javax.swing.JButton btnAjouter;
    private javax.swing.JButton btnDetailPaie;
    private javax.swing.JButton btnImprimer;
    private javax.swing.JButton btnModifier;
    private javax.swing.JButton btnRenouveler;
    private javax.swing.JButton btnSupprimer;
    private javax.swing.JLabel lblSpecialite;
    private javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables
    
    private void initGui() {

    	btnAjouter.setIcon(CocktailIcones.ICON_ADD);
    	btnModifier.setIcon(CocktailIcones.ICON_UPDATE);
    	btnSupprimer.setIcon(CocktailIcones.ICON_DELETE);
    	btnRenouveler.setIcon(CocktailIcones.ICON_RENOUVELER);
    	btnDetailPaie.setIcon(CocktailIcones.ICON_EURO);
    	btnImprimer.setIcon(CocktailIcones.ICON_PRINTER_16);
    	
		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

    	ZEOTableModelColumn	col = new ZEOTableModelColumn(eod, EOContratAvenant.DATE_DEBUT_KEY, "Début", 75);
    	col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOContratAvenant.DATE_FIN_KEY, "Fin", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOContratAvenant.TO_GRADE_KEY+"."+EOGrade.LL_GRADE_KEY, "Grade", 160);
		col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOContratAvenant.C_ECHELON_KEY, "Ech.", 40);
		col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOContratAvenant.INDICE_CONTRAT_KEY, "INM", 40);
		col.setAlignment(SwingConstants.RIGHT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOContratAvenant.QUOTITE_KEY, "Quot.", 50);
		col.setAlignment(SwingConstants.RIGHT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOContratAvenant.NO_ARRETE_KEY, "Arrêté", 75);
		col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOContratAvenant.DUREE_TOTALE_KEY, "Heures", 50);
		col.setAlignment(SwingConstants.RIGHT);
    	myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(CocktailConstantes.BG_COLOR_WHITE);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

    }

	public javax.swing.JButton getBtnAjouter() {
		return btnAjouter;
	}

	public void setBtnAjouter(javax.swing.JButton btnAjouter) {
		this.btnAjouter = btnAjouter;
	}

	public javax.swing.JButton getBtnDetailPaie() {
		return btnDetailPaie;
	}

	public javax.swing.JButton getBtnActivites() {
		return btnActivites;
	}

	public void setBtnActivites(javax.swing.JButton btnActivites) {
		this.btnActivites = btnActivites;
	}

	public void setBtnDetailPaie(javax.swing.JButton btnDetailPaie) {
		this.btnDetailPaie = btnDetailPaie;
	}

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	public javax.swing.JButton getBtnModifier() {
		return btnModifier;
	}

	public void setBtnModifier(javax.swing.JButton btnModifier) {
		this.btnModifier = btnModifier;
	}

	public javax.swing.JButton getBtnImprimer() {
		return btnImprimer;
	}

	public void setBtnImprimer(javax.swing.JButton btnImprimer) {
		this.btnImprimer = btnImprimer;
	}

	public javax.swing.JButton getBtnRenouveler() {
		return btnRenouveler;
	}

	public void setBtnRenouveler(javax.swing.JButton btnRenouveler) {
		this.btnRenouveler = btnRenouveler;
	}

	public javax.swing.JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	public void setBtnSupprimer(javax.swing.JButton btnSupprimer) {
		this.btnSupprimer = btnSupprimer;
	}

	public javax.swing.JLabel getLblSpecialite() {
		return lblSpecialite;
	}

	public void setLblSpecialite(javax.swing.JLabel lblSpecialite) {
		this.lblSpecialite = lblSpecialite;
	}

	
    
    
}

/*******************************************************************************
    	btnModifier.setIcon(CocktailIcones.ICON_UPDATE);
    	btnSupprimer.setIcon(CocktailIcones.ICON_LOUPE_16);
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.cir;

import java.awt.BorderLayout;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.mangue.common.utilities.CocktailIcones;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriere;
import org.cocktail.mangue.modele.mangue.cir.EOCirFichierImport;
import org.cocktail.mangue.modele.mangue.cir.EOCirIdentite;

import com.webobjects.eointerface.EODisplayGroup;

/**
 *
 * @author  cpinsard
 */
public class CirCarriereView extends javax.swing.JPanel {


	private static final long serialVersionUID = -7770031412248058797L;
	private ZEOTableCellRenderer myRenderer;

	protected EODisplayGroup eodFichier, eodIndividu, eodCir;
	protected ZEOTable myEOTableFichier, myEOTableIndividu, myEOTableCir;
	protected ZEOTableModel myTableModelFichier, myTableModelIndividu, myTableModelCir;
	protected TableSorter myTableSorterFichier, myTableSorterIndividu, myTableSorterCir;

    /** Creates new form TemplateJPanel */
    public CirCarriereView(EODisplayGroup dgFichier, EODisplayGroup dgIndividu, EODisplayGroup dgCir, ZEOTableCellRenderer renderer ) {

        eodFichier = dgFichier;
        eodIndividu = dgIndividu;
        eodCir = dgCir;
        
        myRenderer = renderer;
                
        initComponents();
        initGui();
        
    }

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        viewTableFichier = new javax.swing.JPanel();
        btnPreparerDonneesCir = new javax.swing.JButton();
        viewTableIndividu = new javax.swing.JPanel();
        lblTableIndividus = new javax.swing.JLabel();
        lblTableCir = new javax.swing.JLabel();
        viewTableCir = new javax.swing.JPanel();
        btnAddFichier = new javax.swing.JButton();
        btnUpdateFichier = new javax.swing.JButton();
        btnSelectAllIndividu = new javax.swing.JButton();
        btnDelDonneeCir = new javax.swing.JButton();
        btnDetailAgentCir = new javax.swing.JButton();
        tfFiltreIndividu = new javax.swing.JTextField();
        btnRefreshIndividus = new javax.swing.JButton();
        btnPreparerFichierCir = new javax.swing.JButton();
        btnExport = new javax.swing.JButton();
        tfFiltreCir = new javax.swing.JTextField();
        btnRecalculer = new javax.swing.JButton();
        popupValidite = new javax.swing.JComboBox();
        btnDeleteFichier = new javax.swing.JButton();

        viewTableFichier.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        viewTableFichier.setLayout(new java.awt.BorderLayout());

        btnPreparerDonneesCir.setText("Préparer Données");
        btnPreparerDonneesCir.setToolTipText("Préparation des données CIR");

        viewTableIndividu.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        viewTableIndividu.setLayout(new java.awt.BorderLayout());

        lblTableIndividus.setFont(new java.awt.Font("Arial", 0, 12));
        lblTableIndividus.setForeground(new java.awt.Color(51, 51, 255));
        lblTableIndividus.setText("jLabel2");

        lblTableCir.setFont(new java.awt.Font("Arial", 0, 12));
        lblTableCir.setForeground(new java.awt.Color(51, 51, 255));
        lblTableCir.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTableCir.setText("jLabel2");

        viewTableCir.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        viewTableCir.setLayout(new java.awt.BorderLayout());

        btnAddFichier.setToolTipText("Ajout d'un nouveau fichier");

        btnUpdateFichier.setToolTipText("Modification du fichier sélectionné");

        btnSelectAllIndividu.setToolTipText("Sélection de tous les agents");

        btnDelDonneeCir.setToolTipText("Suppression des données des agents sélectionnés");

        btnDetailAgentCir.setToolTipText("Détail des rubriques CIR de l'agent sélectionné");

        tfFiltreIndividu.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnRefreshIndividus.setToolTipText("Sélection de tous les agents");
        btnRefreshIndividus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshIndividusActionPerformed(evt);
            }
        });

        btnPreparerFichierCir.setText("Préparer Fichier");
        btnPreparerFichierCir.setToolTipText("Préparation du fichier Carrières");

        btnExport.setToolTipText("Exporter le fichier au format Excel");

        tfFiltreCir.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnRecalculer.setToolTipText("Re-calcul des données CIR du ou des agents sélectionnés");

        popupValidite.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnDeleteFichier.setToolTipText("Modification du fichier sélectionné");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewTableFichier, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 807, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, viewTableIndividu, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE)
                                    .add(layout.createSequentialGroup()
                                        .add(lblTableIndividus, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                                        .add(18, 18, 18)
                                        .add(tfFiltreIndividu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 73, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .add(75, 75, 75))
                            .add(layout.createSequentialGroup()
                                .add(btnSelectAllIndividu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnRefreshIndividus, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(48, 48, 48)
                                .add(btnPreparerDonneesCir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 169, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)))
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(btnPreparerFichierCir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 176, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(viewTableCir, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                            .add(layout.createSequentialGroup()
                                .add(lblTableCir, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                                .add(18, 18, 18)
                                .add(tfFiltreCir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 73, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(18, 18, 18)
                                .add(popupValidite, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 92, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(5, 5, 5)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnUpdateFichier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnAddFichier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnRecalculer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnDetailAgentCir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnDelDonneeCir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnDeleteFichier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnExport))
                .add(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(btnAddFichier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnUpdateFichier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnDeleteFichier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(viewTableFichier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 85, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(lblTableIndividus, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(tfFiltreIndividu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(tfFiltreCir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(popupValidite, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(lblTableCir)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .add(btnRecalculer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(btnDelDonneeCir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(7, 7, 7)
                        .add(btnDetailAgentCir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 375, Short.MAX_VALUE)
                        .add(btnExport, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 32, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, viewTableCir, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, viewTableIndividu, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(btnPreparerFichierCir, 0, 0, Short.MAX_VALUE)
                            .add(btnPreparerDonneesCir, 0, 0, Short.MAX_VALUE)
                            .add(btnSelectAllIndividu, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                            .add(btnRefreshIndividus, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshIndividusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshIndividusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRefreshIndividusActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JButton btnAddFichier;
    protected javax.swing.JButton btnDelDonneeCir;
    protected javax.swing.JButton btnDeleteFichier;
    protected javax.swing.JButton btnDetailAgentCir;
    protected javax.swing.JButton btnExport;
    protected javax.swing.JButton btnPreparerDonneesCir;
    protected javax.swing.JButton btnPreparerFichierCir;
    protected javax.swing.JButton btnRecalculer;
    protected javax.swing.JButton btnRefreshIndividus;
    protected javax.swing.JButton btnSelectAllIndividu;
    protected javax.swing.JButton btnUpdateFichier;
    private javax.swing.JLabel lblTableCir;
    private javax.swing.JLabel lblTableIndividus;
    private javax.swing.JComboBox popupValidite;
    private javax.swing.JTextField tfFiltreCir;
    private javax.swing.JTextField tfFiltreIndividu;
    protected javax.swing.JPanel viewTableCir;
    protected javax.swing.JPanel viewTableFichier;
    protected javax.swing.JPanel viewTableIndividu;
    // End of variables declaration//GEN-END:variables

    
    
    
    private void initGui() {
                	
    	btnAddFichier.setIcon(CocktailIcones.ICON_ADD);
    	btnUpdateFichier.setIcon(CocktailIcones.ICON_UPDATE);
    	btnDeleteFichier.setIcon(CocktailIcones.ICON_DELETE);
    	btnDetailAgentCir.setIcon(CocktailIcones.ICON_LOUPE_16);
    	btnRefreshIndividus.setIcon(CocktailIcones.ICON_UPDATE);
    	btnDelDonneeCir.setIcon(CocktailIcones.ICON_DELETE);
    	btnRecalculer.setIcon(CocktailIcones.ICON_CALCULATE_16);
    	btnPreparerDonneesCir.setIcon(CocktailIcones.ICON_CALCULATE_16);

    	btnExport.setIcon(CocktailIcones.ICON_EXCEL_16);

    	btnPreparerFichierCir.setIcon(CocktailIcones.ICON_PARAMS_16);
    	btnSelectAllIndividu.setIcon(CocktailIcones.ICON_SELECT_ALL);

    	popupValidite.removeAllItems();
    	popupValidite.addItem("*");
    	popupValidite.addItem("VALIDES");
    	popupValidite.addItem("INVALIDES");
    	
		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

    	ZEOTableModelColumn	col = new ZEOTableModelColumn(eodFichier, EOCirFichierImport.CFIM_FICHIER_KEY, "Fichier", 200);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodFichier, EOCirFichierImport.CFIM_FICHIER_ENVOI_KEY, "Fichier Envoi", 200);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodFichier, EOCirFichierImport.CFIM_DATE_KEY, "Date", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY_HHMM);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodFichier, "typeFichierLong", "Type", 75);
    	col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodFichier, EOCirFichierImport.CFIM_ANNEE_KEY, "Année", 50);
		col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodFichier, EOCirFichierImport.CFIM_GENERATION_KEY, "Généré", 50);
		col.setAlignment(SwingConstants.CENTER);
//    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodFichier, EOCirFichierImport.CFIM_EXPEDITION_KEY, "Expédié", 50);
		col.setAlignment(SwingConstants.CENTER);
//    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodFichier, EOCirFichierImport.CFIM_HEURE_CALCUL_KEY, "Progr.", 50);
		col.setAlignment(SwingConstants.CENTER);
    	myCols.add(col);

		myTableModelFichier = new ZEOTableModel(eodFichier, myCols);
		myTableSorterFichier = new TableSorter(myTableModelFichier);

		myEOTableFichier = new ZEOTable(myTableSorterFichier);
		//myEOTable.addListener(myListenerContrat);
		myTableSorterFichier.setTableHeader(myEOTableFichier.getTableHeader());		

		myEOTableFichier.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTableFichier.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTableFichier.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableFichier.setLayout(new BorderLayout());
		viewTableFichier.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableFichier.removeAll();
		viewTableFichier.add(new JScrollPane(myEOTableFichier), BorderLayout.CENTER);

		// INDIVIDUS
		myCols = new Vector<ZEOTableModelColumn>();

    	col = new ZEOTableModelColumn(eodIndividu, EOCirIdentite.NOM_USUEL_KEY, "Nom", 130);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodIndividu, EOCirIdentite.PRENOM_USUEL_KEY, "Prénom", 120);
		col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodIndividu, EOCirIdentite.TO_INDIVIDU_KEY+"."+EOIndividu.DATE_CERTIFICATION_KEY, "Certif", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodIndividu, EOCirIdentite.TO_INDIVIDU_KEY+"."+EOIndividu.DATE_COMPLETUDE_KEY, "Complétude", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);

		myTableModelIndividu = new ZEOTableModel(eodIndividu, myCols);
		myTableSorterIndividu = new TableSorter(myTableModelIndividu);

		myEOTableIndividu = new ZEOTable(myTableSorterIndividu);
		//myEOTableIndividu.addListener(myListenerIndividu);
		myTableSorterIndividu.setTableHeader(myEOTableIndividu.getTableHeader());		

		myEOTableIndividu.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTableIndividu.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTableIndividu.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableIndividu.setLayout(new BorderLayout());
		viewTableIndividu.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableIndividu.removeAll();
		viewTableIndividu.add(new JScrollPane(myEOTableIndividu), BorderLayout.CENTER);

		// CIR
		myCols = new Vector<ZEOTableModelColumn>();

    	col = new ZEOTableModelColumn(eodCir, EOCirCarriere.TO_INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, "Nom", 130);
    	col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodCir, EOCirCarriere.TO_INDIVIDU_KEY+"."+EOIndividu.PRENOM_KEY, "Prénom", 110);
		col.setAlignment(SwingConstants.LEFT);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodCir, EOCirCarriere.TEM_VALIDE_KEY, "Val", 30);
		col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(myRenderer);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eodCir, EOCirCarriere.D_MODIFICATION_KEY, "Calc", 80);
		col.setAlignment(SwingConstants.CENTER);
		col.setTableCellRenderer(myRenderer);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY_HHMM);
		col.setColumnClass(Date.class);
    	myCols.add(col);

		myTableModelCir = new ZEOTableModel(eodCir, myCols);
		myTableSorterCir = new TableSorter(myTableModelCir);

		myEOTableCir = new ZEOTable(myTableSorterCir);
		//myEOTableCir.addListener(myListenerIndividu);
		myTableSorterCir.setTableHeader(myEOTableCir.getTableHeader());		

		myEOTableCir.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTableCir.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTableCir.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTableCir.setLayout(new BorderLayout());
		viewTableCir.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTableCir.removeAll();
		viewTableCir.add(new JScrollPane(myEOTableCir), BorderLayout.CENTER);
			
    }
 

	public ZEOTable getMyEOTableFichier() {
		return myEOTableFichier;
	}

	public void setMyEOTableFichier(ZEOTable myEOTableFichier) {
		this.myEOTableFichier = myEOTableFichier;
	}

	public ZEOTable getMyEOTableIndividu() {
		return myEOTableIndividu;
	}

	public void setMyEOTableIndividu(ZEOTable myEOTableIndividu) {
		this.myEOTableIndividu = myEOTableIndividu;
	}

	public javax.swing.JButton getBtnExport() {
		return btnExport;
	}

	public void setBtnExport(javax.swing.JButton btnExport) {
		this.btnExport = btnExport;
	}

	public javax.swing.JButton getBtnPreparerFichierCir() {
		return btnPreparerFichierCir;
	}

	public void setBtnPreparerFichierCir(javax.swing.JButton btnPreparerFichierCir) {
		this.btnPreparerFichierCir = btnPreparerFichierCir;
	}

	public ZEOTable getMyEOTableCir() {
		return myEOTableCir;
	}

	public void setMyEOTableCir(ZEOTable myEOTableCir) {
		this.myEOTableCir = myEOTableCir;
	}

	public ZEOTableModel getMyTableModelFichier() {
		return myTableModelFichier;
	}

	public void setMyTableModelFichier(ZEOTableModel myTableModelFichier) {
		this.myTableModelFichier = myTableModelFichier;
	}

	public ZEOTableModel getMyTableModelIndividu() {
		return myTableModelIndividu;
	}

	public void setMyTableModelIndividu(ZEOTableModel myTableModelIndividu) {
		this.myTableModelIndividu = myTableModelIndividu;
	}

	public ZEOTableModel getMyTableModelCir() {
		return myTableModelCir;
	}

	public void setMyTableModelCir(ZEOTableModel myTableModelCir) {
		this.myTableModelCir = myTableModelCir;
	}

	public javax.swing.JButton getBtnAddFichier() {
		return btnAddFichier;
	}

	public void setBtnAddFichier(javax.swing.JButton btnAddFichier) {
		this.btnAddFichier = btnAddFichier;
	}

	public javax.swing.JButton getBtnDelDonneeCir() {
		return btnDelDonneeCir;
	}

	public void setBtnDelDonneeCir(javax.swing.JButton btnDelDonneeCir) {
		this.btnDelDonneeCir = btnDelDonneeCir;
	}

	public javax.swing.JButton getBtnDetailAgentCir() {
		return btnDetailAgentCir;
	}

	public javax.swing.JComboBox getPopupValidite() {
		return popupValidite;
	}

	public javax.swing.JButton getBtnDeleteFichier() {
		return btnDeleteFichier;
	}

	public void setBtnDeleteFichier(javax.swing.JButton btnDeleteFichier) {
		this.btnDeleteFichier = btnDeleteFichier;
	}

	public void setPopupValidite(javax.swing.JComboBox popupValidite) {
		this.popupValidite = popupValidite;
	}

	public void setBtnDetailAgentCir(javax.swing.JButton btnDetailAgentCir) {
		this.btnDetailAgentCir = btnDetailAgentCir;
	}

	public javax.swing.JButton getBtnSelectAllIndividu() {
		return btnSelectAllIndividu;
	}

	public void setBtnSelectAllIndividu(javax.swing.JButton btnSelectAllIndividu) {
		this.btnSelectAllIndividu = btnSelectAllIndividu;
	}

	public javax.swing.JButton getBtnUpdateFichier() {
		return btnUpdateFichier;
	}

	public javax.swing.JButton getBtnPreparerDonneesCir() {
		return btnPreparerDonneesCir;
	}

	public void setBtnPreparerDonneesCir(javax.swing.JButton btnPreparerDonneesCir) {
		this.btnPreparerDonneesCir = btnPreparerDonneesCir;
	}

	public void setBtnUpdateFichier(javax.swing.JButton btnUpdateFichier) {
		this.btnUpdateFichier = btnUpdateFichier;
	}

	public javax.swing.JLabel getLblTableCir() {
		return lblTableCir;
	}

	public void setLblTableCir(javax.swing.JLabel lblTableCir) {
		this.lblTableCir = lblTableCir;
	}

	public javax.swing.JButton getBtnRefreshIndividus() {
		return btnRefreshIndividus;
	}

	public void setBtnRefreshIndividus(javax.swing.JButton btnRefreshIndividus) {
		this.btnRefreshIndividus = btnRefreshIndividus;
	}

	public javax.swing.JButton getBtnRecalculer() {
		return btnRecalculer;
	}

	public void setBtnRecalculer(javax.swing.JButton btnRecalculer) {
		this.btnRecalculer = btnRecalculer;
	}

	public javax.swing.JLabel getLblTableIndividus() {
		return lblTableIndividus;
	}

	public void setLblTableIndividus(javax.swing.JLabel lblTableIndividus) {
		this.lblTableIndividus = lblTableIndividus;
	}


	public javax.swing.JTextField getTfFiltreIndividu() {
		return tfFiltreIndividu;
	}

	public void setTfFiltreIndividu(javax.swing.JTextField tfFiltreIndividu) {
		this.tfFiltreIndividu = tfFiltreIndividu;
	}

	public javax.swing.JTextField getTfFiltreCir() {
		return tfFiltreCir;
	}

	public void setTfFiltreCir(javax.swing.JTextField tfFiltreCir) {
		this.tfFiltreCir = tfFiltreCir;
	}

    
    
}

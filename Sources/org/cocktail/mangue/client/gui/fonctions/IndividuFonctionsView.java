/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.gui.fonctions;

import java.awt.BorderLayout;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.mangue.client.gui.promotions.PromouvabilitesView;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailFormats;
import org.cocktail.mangue.common.utilities.CocktailIcones;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuFonctions;

import com.webobjects.eointerface.EODisplayGroup;


/**
 *
 * @author  cpinsard
 */
public class IndividuFonctionsView extends JFrame {

	protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;

    /**
	 * 
	 */
	private static final long serialVersionUID = -6195866169790317637L;

	/** Creates new form TemplateJDialog */
    public IndividuFonctionsView(java.awt.Frame parent, boolean modal, EODisplayGroup dg) {
       // super(parent, modal);
    	
    	eod = dg;
        initComponents();
        initGui();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfTitre = new javax.swing.JTextField();
        viewProlongations = new javax.swing.JPanel();
        viewTable = new javax.swing.JPanel();
        tfDateDebut = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        btnModifier = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        lblTypeModalite = new javax.swing.JLabel();
        btnAjouter = new javax.swing.JButton();
        popupTypes = new javax.swing.JComboBox();
        tfDateFin = new javax.swing.JTextField();
        btnSupprimer = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        swapView = new javax.swing.JPanel();
        btnAnnuler = new javax.swing.JButton();
        btnValider = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("GESTION DES NOMENCLATURES");

        tfTitre.setBackground(new java.awt.Color(153, 153, 255));
        tfTitre.setEditable(false);
        tfTitre.setFont(new java.awt.Font("Times New Roman", 0, 14));
        tfTitre.setForeground(new java.awt.Color(255, 255, 204));
        tfTitre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfTitre.setText("FONCTIONS");
        tfTitre.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        tfTitre.setFocusable(false);
        tfTitre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfTitreActionPerformed(evt);
            }
        });

        viewTable.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        viewTable.setLayout(new java.awt.BorderLayout());

        tfDateDebut.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateDebut.setToolTipText("Date de début de la fonction");

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Fin ");

        btnModifier.setToolTipText("Modification de la fonction sélectionnée");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Début");

        lblTypeModalite.setFont(new java.awt.Font("Tahoma", 1, 11));
        lblTypeModalite.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTypeModalite.setText("TYPE");

        btnAjouter.setToolTipText("Ajout 'une nouvelle fonction");

        popupTypes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        popupTypes.setToolTipText("Type de fonction");

        tfDateFin.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tfDateFin.setToolTipText("Date de fin de la fonction");

        btnSupprimer.setToolTipText("Supprimer");

        swapView.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        swapView.setLayout(new java.awt.CardLayout());

        btnValider.setToolTipText("Valider la saisie");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                        .add(btnAnnuler)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnValider))
                    .add(swapView, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 799, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(swapView, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(btnValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout viewProlongationsLayout = new org.jdesktop.layout.GroupLayout(viewProlongations);
        viewProlongations.setLayout(viewProlongationsLayout);
        viewProlongationsLayout.setHorizontalGroup(
            viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewProlongationsLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(viewProlongationsLayout.createSequentialGroup()
                        .add(viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(viewProlongationsLayout.createSequentialGroup()
                                .add(lblTypeModalite, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 39, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(popupTypes, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 203, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jLabel17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 72, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(tfDateDebut, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 98, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jLabel18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(tfDateFin, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 93, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, viewProlongationsLayout.createSequentialGroup()
                                .add(viewTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap())))
        );
        viewProlongationsLayout.setVerticalGroup(
            viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(viewProlongationsLayout.createSequentialGroup()
                .addContainerGap()
                .add(viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(viewProlongationsLayout.createSequentialGroup()
                        .add(viewTable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 174, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(lblTypeModalite)
                                .add(popupTypes, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(jLabel18)
                                .add(tfDateFin, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(viewProlongationsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(jLabel17)
                                .add(tfDateDebut, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(viewProlongationsLayout.createSequentialGroup()
                        .add(btnAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSupprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(tfTitre, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 918, Short.MAX_VALUE)
            .add(layout.createSequentialGroup()
                .add(10, 10, 10)
                .add(viewProlongations, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(tfTitre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(viewProlongations, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(119, Short.MAX_VALUE))
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-934)/2, (screenSize.height-799)/2, 934, 799);
    }// </editor-fold>//GEN-END:initComponents

    private void tfTitreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfTitreActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_tfTitreActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                PromouvabilitesView dialog = new PromouvabilitesView(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAjouter;
    private javax.swing.JButton btnAnnuler;
    private javax.swing.JButton btnModifier;
    private javax.swing.JButton btnSupprimer;
    private javax.swing.JButton btnValider;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblTypeModalite;
    private javax.swing.JComboBox popupTypes;
    private javax.swing.JPanel swapView;
    protected javax.swing.JTextField tfDateDebut;
    protected javax.swing.JTextField tfDateFin;
    private javax.swing.JTextField tfTitre;
    private javax.swing.JPanel viewProlongations;
    protected javax.swing.JPanel viewTable;
    // End of variables declaration//GEN-END:variables

    
    private void initGui() {

    	btnAjouter.setIcon(CocktailIcones.ICON_ADD);
    	btnModifier.setIcon(CocktailIcones.ICON_UPDATE);
    	btnSupprimer.setIcon(CocktailIcones.ICON_DELETE);
    	btnValider.setIcon(CocktailIcones.ICON_VALID);
    	btnAnnuler.setIcon(CocktailIcones.ICON_CANCEL);
               	
    	popupTypes.removeAllItems();
    	popupTypes.addItem("");
    	popupTypes.addItem("STR - Structure");
    	popupTypes.addItem("INS - Instance");
    	popupTypes.addItem("EXP - Expertise");
    	
    	tfDateDebut.setToolTipText("Date de début de la fonction");
    	tfDateFin.setToolTipText("Date de fin de la fonction");
    	
		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

    	ZEOTableModelColumn	col = new ZEOTableModelColumn(eod, EOIndividuFonctions.FON_LIBELLE_KEY, "Type", 75);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOIndividuFonctions.LL_FONCTION_KEY, "Fonction", 150);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOIndividuFonctions.DATE_DEBUT_KEY, "Début", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOIndividuFonctions.DATE_FIN_KEY, "Fin", 75);
		col.setAlignment(SwingConstants.CENTER);
		col.setFormatDisplay(CocktailFormats.FORMAT_DATE_DDMMYYYY);
		col.setColumnClass(Date.class);
    	myCols.add(col);
    	col = new ZEOTableModelColumn(eod, EOIndividuFonctions.FON_INFOS_KEY, "Infos", 200);
    	col.setAlignment(SwingConstants.LEFT);
    	myCols.add(col);

   	
		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

//		myEOTable.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);
		myEOTable.setSelectionBackground(CocktailConstantes.COLOR_SELECTED_ROW);
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

    }

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	public javax.swing.JButton getBtnAjouter() {
		return btnAjouter;
	}

	public void setBtnAjouter(javax.swing.JButton btnAjouter) {
		this.btnAjouter = btnAjouter;
	}

	public javax.swing.JButton getBtnAnnuler() {
		return btnAnnuler;
	}

	public void setBtnAnnuler(javax.swing.JButton btnAnnuler) {
		this.btnAnnuler = btnAnnuler;
	}

	public javax.swing.JButton getBtnModifier() {
		return btnModifier;
	}

	public void setBtnModifier(javax.swing.JButton btnModifier) {
		this.btnModifier = btnModifier;
	}

	public javax.swing.JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	public void setBtnSupprimer(javax.swing.JButton btnSupprimer) {
		this.btnSupprimer = btnSupprimer;
	}

	public javax.swing.JButton getBtnValider() {
		return btnValider;
	}

	public void setBtnValider(javax.swing.JButton btnValider) {
		this.btnValider = btnValider;
	}

	public javax.swing.JLabel getLblTypeModalite() {
		return lblTypeModalite;
	}

	public void setLblTypeModalite(javax.swing.JLabel lblTypeModalite) {
		this.lblTypeModalite = lblTypeModalite;
	}

	public javax.swing.JPanel getSwapView() {
		return swapView;
	}

	public void setSwapView(javax.swing.JPanel swapView) {
		this.swapView = swapView;
	}

	public javax.swing.JTextField getTfDateDebut() {
		return tfDateDebut;
	}

	public void setTfDateDebut(javax.swing.JTextField tfDateDebut) {
		this.tfDateDebut = tfDateDebut;
	}

	public javax.swing.JTextField getTfDateFin() {
		return tfDateFin;
	}

	public void setTfDateFin(javax.swing.JTextField tfDateFin) {
		this.tfDateFin = tfDateFin;
	}

	public javax.swing.JTextField getTfTitre() {
		return tfTitre;
	}

	public void setTfTitre(javax.swing.JTextField tfTitre) {
		this.tfTitre = tfTitre;
	}

	public javax.swing.JPanel getViewProlongations() {
		return viewProlongations;
	}

	public void setViewProlongations(javax.swing.JPanel viewProlongations) {
		this.viewProlongations = viewProlongations;
	}

	public javax.swing.JComboBox getPopupTypes() {
		return popupTypes;
	}

	public void setPopupTypes(javax.swing.JComboBox popupTypes) {
		this.popupTypes = popupTypes;
	}


}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.outils.nbi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.nbi.SaisieNbiView;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiPointsTranches;
import org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTranches;
import org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTypes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.mangue.nbi.EONbi;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class SaisieNbiCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieNbiCtrl sharedInstance;
	private SaisieNbiView myView;

	private EONbi currentNbi;
	private EONbiTypes currentType;
	private EONbiTranches currentTranche;

	public SaisieNbiCtrl(EOEditingContext edc) {

		super(edc);

		myView = new SaisieNbiView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		CocktailUtilities.initPopupAvecListe(myView.getPopupTypes(), EONbiTypes.fetchAll(getEdc(), EONbiTypes.SORT_ARRAY_CODE), true);

		myView.getPopupTypes().addActionListener(new PopupTypeListener());
		myView.getPopupTranches().addActionListener(new PopupTrancheListener());

		CocktailUtilities.initTextField(myView.getTfDateDebut(),false, false);

	}


	public static SaisieNbiCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieNbiCtrl(editingContext);
		return sharedInstance;
	}


	private EONbi getCurrentNbi() {
		return currentNbi;
	}
	private void setCurrentNbi(EONbi nbi) {
		currentNbi = nbi;

	}	


	private EONbiTypes getCurrentType() {
		return currentType;
	}
	private void setCurrentType(EONbiTypes type) {

		currentType = type;
		myView.getPopupTypes().setSelectedItem(currentType);

	}	


	public EONbiTranches getCurrentTranche() {
		return currentTranche;
	}
	public void setCurrentTranche(EONbiTranches currentTranche) {
		this.currentTranche = currentTranche;
		myView.getTfDateDebut().setText("");
		if (currentTranche != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentTranche.dEffetTranche());
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupTypeListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){

			setCurrentType((EONbiTypes)myView.getPopupTypes().getSelectedItem());

			if (getCurrentType() != null) {
				setTranches(getCurrentType());
				setPoints(getCurrentType());
				myView.getTfCode().setText(getCurrentType().code());
			}

			updateInterface();
		}
	}
	
	/**
	 */
	private class PopupTrancheListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			setCurrentTranche((EONbiTranches)myView.getPopupTranches().getSelectedItem());
		}
	}

	/**
	 * 
	 * @param type
	 */
	private void setTranches(EONbiTypes type) {

		myView.getPopupTranches().removeAllItems();		
		NSArray<EONbiPointsTranches> tranches = EONbiPointsTranches.findForType(getEdc(), getCurrentType());
		for (EONbiTranches tranche : (NSArray<EONbiTranches>)tranches.valueForKey(EONbiPointsTranches.TRANCHE_KEY)) {
			myView.getPopupTranches().addItem(tranche);
		}

	}

	/**
	 * 
	 * @param type
	 */
	private void setPoints(EONbiTypes type) {
		myView.getPopupPoints().removeAllItems();		

		if (type != null) {
			for (int i=5;i<=type.pointsMaxi().intValue();i+=5) {			
				myView.getPopupPoints().addItem(new Integer(i));
			}
		}
	}

	/**
	 * 
	 * @param nbi
	 * @return
	 */
	public boolean modifier(EONbi nbi, boolean modeCreation) {

		setCurrentNbi(nbi);
		setCurrentType(getCurrentNbi().toNbiTypes());
		setTranches(getCurrentType());
		setPoints(getCurrentType());

		myView.getPopupTranches().setSelectedItem(getCurrentNbi().tranche());
		myView.getPopupPoints().setSelectedItem(getCurrentNbi().nbPoints());

		myView.setTitle("MODIFICATION NBI " + getCurrentNbi().cNbi());

		setModeCreation(modeCreation);

		updateDatas();

		myView.setVisible(true);

		return getCurrentNbi() != null;
	}

	/**
	 * 
	 * @return
	 */
	private NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	private NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}


	/** methode a surcharger pour faire des traitements apres l'enregistrement des donnees dans la base */
	protected  void traitementsApresValidation() {
		myView.setVisible(false);
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());

	}


	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentNbi() != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentNbi().cNbi());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentNbi().dDebNbi());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentNbi().dFinNbi());
		}

		updateInterface();

	}


	@Override
	protected void updateInterface() {

		// TODO Auto-generated method stub
		myView.getPopupPoints().setEnabled(getCurrentType() != null);
		myView.getPopupTranches().setEnabled(getCurrentType() != null);
		CocktailUtilities.initTextField(myView.getTfCode(), false, getCurrentType() != null && isModeCreation());

		myView.getBtnValider().setEnabled(getCurrentType() != null);

	}


	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentNbi(null);
		myView.setVisible(false);
	}


	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}


	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}


	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentNbi().setDDebNbi(getDateDebut());
		getCurrentNbi().setDFinNbi(getDateFin());

		getCurrentNbi().setToNbiTypesRelationship(getCurrentType());

		if (isModeCreation()) {
			if (myView.getTfCode().getText().length() > 0)
				getCurrentNbi().setCNbi(myView.getTfCode().getText());
			else
				getCurrentNbi().setCNbi(null);
		}

		getCurrentNbi().setNoTranche(getCurrentTranche().noTranche());
		getCurrentNbi().setNbPoints((Integer)myView.getPopupPoints().getSelectedItem());

	}	

}

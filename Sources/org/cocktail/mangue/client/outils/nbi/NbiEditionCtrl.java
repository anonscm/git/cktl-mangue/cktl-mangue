
package org.cocktail.mangue.client.outils.nbi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.nbi.NbiEditionView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class NbiEditionCtrl {

	private static NbiEditionCtrl sharedInstance;

	private EOEditingContext ec;
	private NbiEditionView myView;

	public NbiEditionCtrl(EOEditingContext editingContext) {

		ec = editingContext;
		myView = new NbiEditionView();

		myView.getBtnPrintBeneficiaires().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerBeneficiaires();}}
				);
		myView.getBtnPrintFonctions().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerFonctions();}}
				);
		myView.getBtnPrintHistorique().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerHistorique();}}
				);


		myView.getTfDebutPeriode().addFocusListener(new FocusListenerDateTextField(myView.getTfDebutPeriode()));
		myView.getTfDebutPeriode().addActionListener(new ActionListenerDateTextField(myView.getTfDebutPeriode()));

		myView.getTfFinPeriode().addFocusListener(new FocusListenerDateTextField(myView.getTfFinPeriode()));
		myView.getTfFinPeriode().addActionListener(new ActionListenerDateTextField(myView.getTfFinPeriode()));

		myView.getTfDateBeneficiaire().addFocusListener(new FocusListenerDateTextField(myView.getTfDateBeneficiaire()));
		myView.getTfDateBeneficiaire().addActionListener(new ActionListenerDateTextField(myView.getTfDateBeneficiaire()));

		myView.getTfDateFonction().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFonction()));
		myView.getTfDateFonction().addActionListener(new ActionListenerDateTextField(myView.getTfDateFonction()));

	}

	public static NbiEditionCtrl sharedInstance(EOEditingContext editingContext)
	{
		if(sharedInstance == null)
			sharedInstance = new NbiEditionCtrl(editingContext);
		return sharedInstance;
	}


	public JPanel getView() {
		return myView;
	}	

	private NSTimestamp getDateBeneficiaire() {
		return CocktailUtilities.getDateFromField(myView.getTfDateBeneficiaire());
	}
	private NSTimestamp getDatefFonction() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFonction());
	}
	private NSTimestamp getDateDebutPeriode() {
		return CocktailUtilities.getDateFromField(myView.getTfDebutPeriode());
	}
	private NSTimestamp getDateFinPeriode() {
		return CocktailUtilities.getDateFromField(myView.getTfFinPeriode());
	}

	// actions
	private void imprimerBeneficiaires() {
		if (getDateBeneficiaire() == null) {
			EODialogs.runErrorDialog("ERREUR", "Veuillez entrer une date de référence !");
			return;
		}
		imprimer("clientSideRequestImprimerBeneficiairesNbi", getDateBeneficiaire(), "BeneficiairesNbi", "Impression des bénéficiaires de la NBI");
	}
	private void imprimerFonctions() {
		if (getDatefFonction() == null) {
			EODialogs.runErrorDialog("ERREUR", "Veuillez entrer une date de référence !");
			return;
		}
		imprimer("clientSideRequestImprimerFonctionsNbi", getDatefFonction(), "FonctionsNbi", "Impression des fonctions NBI");
	}
	private void imprimerHistorique() {
		try {
			CRICursor.setWaitCursor(myView);

			if (getDateDebutPeriode() == null || getDateFinPeriode() == null) {
				EODialogs.runErrorDialog("ERREUR", "Veuillez entrer une date de référence !");
				return;
			}

			Class[] classeParametres =  new Class[] {NSTimestamp.class,NSTimestamp.class};
			Object[] parametres = new Object[]{getDateDebutPeriode(),getDateFinPeriode()};
			UtilitairesImpression.imprimerSansDialogue(ec,"clientSideRequestImprimerHistoriqueNbi",classeParametres,parametres,"HistoriqueNbi" ,"Impression des bénéficiaires de la Nbi");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		finally {
			CRICursor.setDefaultCursor(myView);
		}
	}

	/**
	 * 
	 * @param remoteMethod
	 * @param dateRef
	 * @param fichierPdf
	 * @param titreEdition
	 */
	private void imprimer(String remoteMethod, NSTimestamp dateRef, String fichierPdf, String titreEdition) {
		try {
			CRICursor.setWaitCursor(myView);
			Class[] classeParametres =  new Class[] {NSTimestamp.class};
			Object[] parametres = new Object[]{dateRef};
			UtilitairesImpression.imprimerSansDialogue(ec,remoteMethod,classeParametres,parametres,fichierPdf ,titreEdition);
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		finally {
			CRICursor.setDefaultCursor(myView);
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{

		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myTextField.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else {
				myTextField.setText(myDate);
			}
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText()))	return;

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else {
				myTextField.setText(myDate);
			}
		}
	}	

}

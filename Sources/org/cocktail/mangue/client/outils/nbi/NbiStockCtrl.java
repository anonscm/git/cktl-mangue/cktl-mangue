
package org.cocktail.mangue.client.outils.nbi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nbi.NbiStockView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiFonctions;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.nbi.EONbi;
import org.cocktail.mangue.modele.mangue.nbi.EONbiImpressions;
import org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation;
import org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class NbiStockCtrl {

	private static NbiStockCtrl sharedInstance;

	private EOEditingContext edc;
	private NbiStockView myView;
	private EODisplayGroup eod, eodBeneficiaires, eodFonctions;

	private ListenerNbi 			listenerNbi = new ListenerNbi();
	private ListenerBeneficiaire 	listenerBeneficiaire = new ListenerBeneficiaire();
	private ListenerFonction 		listenerFonction = new ListenerFonction();
	private TypeEditionListener 	listenerTypeEdition = new TypeEditionListener();

	private EONbi currentNbi;
	private EONbiOccupation currentBeneficiaire;
	private EONbiRepartFonctions currentFonction;

	public NbiStockCtrl(EOEditingContext editingContext) {

		edc = editingContext;
		eod = new EODisplayGroup();
		eodBeneficiaires = new EODisplayGroup();
		eodFonctions = new EODisplayGroup();

		myView = new NbiStockView(eod, eodBeneficiaires, eodFonctions);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);


		myView.getBtnAjouterFonction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterFonction();}}
				);
		myView.getBtnModifierFonction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierFonction();}}
				);
		myView.getBtnSupprimerFonction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerFonction();}}
				);


		myView.getBtnAjouterBeneficiaire().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterBeneficiaire();}}
				);
		myView.getBtnModifierBeneficiaire().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierBeneficiaire();}}
				);
		myView.getBtnSupprimerBeneficiaire().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerBeneficiaire();}}
				);

		myView.getBtnPrintArrete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerArrete();}}
				);

		CocktailUtilities.initTextField(myView.getTfTranche(), false, false);
		CocktailUtilities.initTextField(myView.getTfPoints(), false, false);
		CocktailUtilities.initTextField(myView.getTfType(), false, false);
		CocktailUtilities.initTextField(myView.getTfCode(), false, false);

		myView.getMyEOTable().addListener(listenerNbi);
		myView.getMyEOTableBeneficiaires().addListener(listenerBeneficiaire);
		myView.getMyEOTableFonctions().addListener(listenerFonction);

		myView.getCheckArretePriseFonction().addActionListener(listenerTypeEdition);
		myView.getCheckArreteCessationFonction().addActionListener(listenerTypeEdition);
		myView.getCheckArreteRegularisation().addActionListener(listenerTypeEdition);

		myView.getTfFiltreCode().getDocument().addDocumentListener(new ADocumentListener());

		myView.getCheckFiltreOuvertes().addActionListener(new FiltreNbiListener());
		myView.getCheckFiltreOuvertes().setSelected(true);
		myView.getCheckFiltreSupprimees().addActionListener(new FiltreNbiListener());
		myView.getCheckFiltreToutes().addActionListener(new FiltreNbiListener());

		myView.getCheckArretePriseFonction().setSelected(true);

		updateInterface();
	}



	public EOEditingContext getEdc() {
		return edc;
	}



	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}



	public EONbiOccupation getCurrentBeneficiaire() {
		return currentBeneficiaire;
	}



	public void setCurrentBeneficiaire(EONbiOccupation currentBeneficiaire) {
		this.currentBeneficiaire = currentBeneficiaire;
	}



	public EONbiRepartFonctions getCurrentFonction() {
		return currentFonction;
	}



	public void setCurrentFonction(EONbiRepartFonctions currentFonction) {
		this.currentFonction = currentFonction;
	}



	public EONbi getCurrentNbi() {
		return currentNbi;
	}



	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static NbiStockCtrl sharedInstance(EOEditingContext editingContext)
	{
		if(sharedInstance == null)
			sharedInstance = new NbiStockCtrl(editingContext);
		return sharedInstance;
	}

	private EONbi currentNbi() {
		return currentNbi;
	}
	private void setCurrentNbi(EONbi nbi) {
		currentNbi = nbi;
		updateDatas();
	}

	public JPanel getView() {
		return myView;
	}	

	public NSArray<EONbiOccupation> getOccupations() {
		return eodBeneficiaires.displayedObjects();
	}
	public NSArray<EONbiFonctions> getFonctions() {
		return eodFonctions.displayedObjects();
	}

	/**
	 * 
	 */
	public void actualiser()	{
		eod.setObjectArray(EONbi.rechercherNBI(edc, new NSTimestamp()));
		filter();
	}

	private void ajouter() {
		SaisieNbiCtrl.sharedInstance(edc).modifier(EONbi.creer(getEdc()), true);
		actualiser();
	}

	private void modifier() {
		SaisieNbiCtrl.sharedInstance(edc).modifier(currentNbi(), false);
		myView.getMyEOTable().updateUI();
		listenerNbi.onSelectionChanged();
	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer la NBI sélectionnée ?", "Oui", "Non"))		
			return;			

		try {

			for (Enumeration<EONbiRepartFonctions> e = eodFonctions.displayedObjects().objectEnumerator();e.hasMoreElements();) {
				edc.deleteObject(e.nextElement());
			}
			for (Enumeration<EONbiOccupation> e = eodBeneficiaires.displayedObjects().objectEnumerator();e.hasMoreElements();) {
				edc.deleteObject(e.nextElement());
			}

			edc.deleteObject(currentNbi());

			edc.saveChanges();
			actualiser();
			NbiCtrl.sharedInstance(edc).toFront();
		}
		catch(Exception e) {
			edc.revert();
			e.printStackTrace();
		}
	}


	private void supprimerFonction() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette fonction ?", "Oui", "Non"))		
			return;			

		try {

			edc.deleteObject(currentFonction);
			edc.saveChanges();
			listenerNbi.onSelectionChanged();
			NbiCtrl.sharedInstance(edc).toFront();
		}
		catch(Exception e) {
			edc.revert();
			e.printStackTrace();
		}
	}

	private void supprimerBeneficiaire() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer ce bénéficiaire ?", "Oui", "Non"))		
			return;			

		try {
			edc.deleteObject(currentBeneficiaire);
			edc.saveChanges();
			listenerNbi.onSelectionChanged();
			NbiCtrl.sharedInstance(edc).toFront();
		}
		catch(Exception e) {
			edc.revert();
			e.printStackTrace();
		}
	}


	private void ajouterFonction() {

		SaisieNbiFonctionCtrl.sharedInstance(edc).ajouter(currentNbi());
		listenerNbi.onSelectionChanged();

	}
	private void modifierFonction() {

		SaisieNbiFonctionCtrl.sharedInstance(edc).modifier(currentFonction);
		myView.getMyEOTableFonctions().updateUI();

	}



	private void ajouterBeneficiaire() {

		SaisieNbiBeneficiaireCtrl.sharedInstance(edc).ajouter(currentNbi());
		listenerNbi.onSelectionChanged();

	}
	private void modifierBeneficiaire() {

		SaisieNbiBeneficiaireCtrl.sharedInstance(edc).modifier(currentBeneficiaire);
		myView.getMyEOTableBeneficiaires().updateUI();

	}


	/**
	 * 
	 */
	private void imprimerArrete() {

		NSArray destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(edc).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {

			NSMutableArray destinatairesGlobalIds = new NSMutableArray();
			for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(edc.globalIDForObject(e.nextElement())));

			CRICursor.setWaitCursor(myView);
			NSMutableDictionary dict = new NSMutableDictionary(2);
			String typeArrete = "";

			if (myView.getCheckArretePriseFonction().isSelected()) 
				typeArrete = ManGUEConstantes.TYPE_ARRETE_NBI_PRISE_DE_FONCTION;
			else
				if (myView.getCheckArreteCessationFonction().isSelected()) 
					typeArrete = ManGUEConstantes.TYPE_ARRETE_NBI_CESSATION;
				else
					if (myView.getCheckArreteRegularisation().isSelected()) 
						typeArrete = ManGUEConstantes.TYPE_ARRETE_NBI_REGULARISATION;

			dict.setObjectForKey(typeArrete, "typeArrete");
			// ajouter l'auteur pour le record nbiImpression
			EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(), edc);
			dict.setObjectForKey(agent.toIndividu().noIndividu(),"auteur");
			try {
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class,NSDictionary.class};
				Object[] parametres = new Object[]{edc.globalIDForObject(currentBeneficiaire), destinatairesGlobalIds, dict};
				CRICursor.setDefaultCursor(myView);
				UtilitairesImpression.imprimerSansDialogue(edc,"clientSideRequestImprimerArreteNbi",classeParametres,parametres,"ArreteNbi" + typeArrete + "_" + currentBeneficiaire.toIndividu().noIndividu(),"Impression de l'arrêté de Nbi");
				updateInterface();
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			} 
		}

	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray qualifiers = new NSMutableArray();
		NSTimestamp dateReference = new NSTimestamp();

		if (myView.getCheckFiltreOuvertes().isSelected()) {
			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EONbi.D_FIN_NBI_KEY + ">=%@", new NSArray(dateReference)));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EONbi.D_FIN_NBI_KEY + "=nil", null));

			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		else
			if (myView.getCheckFiltreSupprimees().isSelected()) {

				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EONbi.D_FIN_NBI_KEY + "<%@", new NSArray(dateReference)));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EONbi.D_FIN_NBI_KEY + "!=nil", null));

			}

		if(myView.getTfFiltreCode().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EONbi.C_NBI_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreCode().getText()+"*")));

		return new EOAndQualifier(qualifiers);
	}

	private void filter() 	{

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();

		myView.getLblMessage().setText(eod.displayedObjects().count() + " NBIs");

		updateInterface();
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnModifier().setEnabled(currentNbi() != null);
		myView.getBtnSupprimer().setEnabled(currentNbi() != null);

		myView.getBtnAjouterBeneficiaire().setEnabled(currentNbi() != null);
		myView.getBtnModifierBeneficiaire().setEnabled(currentNbi() != null && getCurrentBeneficiaire() != null);
		myView.getBtnSupprimerBeneficiaire().setEnabled(currentNbi() != null && getCurrentBeneficiaire() != null);

		myView.getBtnModifierFonction().setEnabled(currentNbi() != null);
		myView.getBtnModifierFonction().setEnabled(currentNbi() != null && getCurrentFonction() != null);
		myView.getBtnSupprimerFonction().setEnabled(currentNbi() != null && getCurrentFonction() != null);

		// Arretes
		myView.getCheckArretePriseFonction().setEnabled(currentNbi() != null && getCurrentBeneficiaire() != null && !EONbiImpressions.arretePriseDeFonctionImprimePourNbiEtPersonne(edc, currentNbi(), currentBeneficiaire.toIndividu()));
		myView.getCheckArreteCessationFonction().setEnabled(getCurrentBeneficiaire() != null && !EONbiImpressions.arreteCessationImprimePourNbiEtPersonne(edc, currentNbi(), currentBeneficiaire.toIndividu()));

		boolean peutImprimer = true;

		if (myView.getCheckArretePriseFonction().isSelected() && !myView.getCheckArretePriseFonction().isEnabled())
			peutImprimer = false;
		if (myView.getCheckArreteCessationFonction().isSelected() && !myView.getCheckArreteCessationFonction().isEnabled())
			peutImprimer = false;

		myView.getBtnPrintArrete().setEnabled(peutImprimer);

	}

	private class ADocumentListener
	implements DocumentListener
	{

		public void changedUpdate(DocumentEvent e)
		{
			filter();
		}

		public void insertUpdate(DocumentEvent e)
		{
			filter();
		}

		public void removeUpdate(DocumentEvent e)
		{
			filter();
		}

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerNbi implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			modifier();
		}

		public void onSelectionChanged() {

			setCurrentNbi((EONbi)eod.selectedObject());

			eodBeneficiaires.setObjectArray(new NSArray<EONbiOccupation>());		
			eodFonctions.setObjectArray(new NSArray<EONbiFonctions>());

			if (getCurrentNbi() != null) {

				eodBeneficiaires.setObjectArray(EONbiOccupation.rechercherNbiOccupations(edc, getCurrentNbi()));		
				eodFonctions.setObjectArray(EONbiRepartFonctions.rechercherNbiRepartFonctions(edc, getCurrentNbi()));

				updateDatas();

			}

			myView.getMyEOTableBeneficiaires().updateData();
			myView.getMyEOTableFonctions().updateData();

			updateInterface();
		}
	}

	/**
	 * 
	 */
	private void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfPoints());
		CocktailUtilities.viderTextField(myView.getTfTranche());
		CocktailUtilities.viderTextField(myView.getTfType());
	}
	
	/**
	 * 
	 */
	private void updateDatas() {

		clearDatas();

		if (getCurrentNbi() != null) {

			CocktailUtilities.setTextToField(myView.getTfTranche(), getCurrentNbi().noTranche().toString());
			CocktailUtilities.setNumberToField(myView.getTfPoints(), getCurrentNbi().nbPoints());
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentNbi().cNbi());
			CocktailUtilities.setTextToField(myView.getTfType(), getCurrentNbi().toNbiTypes().libelleLong());

		}

		updateInterface();
	}


	private class ListenerBeneficiaire implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick()	{
			modifierBeneficiaire();
		}

		public void onSelectionChanged() {
			setCurrentBeneficiaire((EONbiOccupation)eodBeneficiaires.selectedObject());						
			updateInterface();
		}
	}


	private class ListenerFonction implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick()	{
			modifierFonction();
		}

		public void onSelectionChanged() {
			setCurrentFonction((EONbiRepartFonctions)eodFonctions.selectedObject());
			updateInterface();
		}
	}


	private class FiltreNbiListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {
			filter();
		}
	}
	private class TypeEditionListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {
			updateInterface();
		}
	}


}

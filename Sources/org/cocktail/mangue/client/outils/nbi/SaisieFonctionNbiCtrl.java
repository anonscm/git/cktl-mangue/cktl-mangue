// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.outils.nbi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nbi.SaisieFonctionNbiView;
import org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiFonctions;
import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisieFonctionNbiCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieFonctionNbiCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieFonctionNbiView myView;
	private EONbiFonctions currentFonction;

	public SaisieFonctionNbiCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieFonctionNbiView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);
	}

	public static SaisieFonctionNbiCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieFonctionNbiCtrl(editingContext);
		return sharedInstance;
	}

	private EONbiFonctions currentFonction() {
		return currentFonction;
	}
	private void setCurrentFonction(EONbiFonctions fonction) {
		currentFonction = fonction;
		myView.getTfFonction().setText("");
		if (currentFonction != null)
			CocktailUtilities.setTextToField(myView.getTfFonction(), currentFonction.llNbiFonction());
	}

	private void clearTextFields()	{
		myView.getTfFonction().setText("");
	}

	public EONbiFonctions ajouter()	{

		myView.setTitle("AJOUT FONCTION NBI");

		setCurrentFonction(EONbiFonctions.creer(ec));
		myView.setVisible(true);

		return currentFonction;
	}


	public boolean modifier(EONbiFonctions fonction) {

		myView.setTitle("Modification fonction NBI");
		setCurrentFonction(fonction);

		myView.setVisible(true);

		return currentFonction() != null;
	}

	private void updateData() {
		clearTextFields();
		CocktailUtilities.setTextToField(myView.getTfFonction(), currentFonction().llNbiFonction());
	}

	/**
	 * 
	 */
	private void updateUI() {

	}


	/**
	 * 
	 */
	private void valider()  {

		try {				
			
			currentFonction().setLlNbiFonction(myView.getTfFonction().getText());
			ec.saveChanges();
			myView.setVisible(false);
		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			myView.toFront();
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		ec.revert();
		setCurrentFonction(null);
		myView.setVisible(false);
	}


}

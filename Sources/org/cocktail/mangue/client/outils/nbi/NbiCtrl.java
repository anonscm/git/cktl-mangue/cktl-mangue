package org.cocktail.mangue.client.outils.nbi;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nbi.NbiView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class NbiCtrl {

	private static NbiCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;
	private NbiView myView;
	private OngletChangeListener listenerOnglets;
	private EOAgentPersonnel currentUtilisateur;

	public NbiCtrl(EOEditingContext editingContext) {

		listenerOnglets = new OngletChangeListener();
		ec = editingContext;

		myView = new NbiView(null, MODE_MODAL.booleanValue());

		myView.getOnglets().addTab("STOCK ", null, NbiStockCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab("EDITIONS ", null, NbiEditionCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addTab("ADMIN ", null, NbiAdminCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addChangeListener(listenerOnglets);
		
		currentUtilisateur = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();

	}

	public static NbiCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new NbiCtrl(editingContext);
		return sharedInstance;
	}

	public void open()	{
		
		// Controle ds droits
		if (currentUtilisateur.peutGererPrimes() == false) {
			EODialogs.runInformationDialog("ERREUR", "Vous n'avez pas les droits nécessaires pour gérer les NBI !");
			return;
		}

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		listenerOnglets.stateChanged(null);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}

	public JFrame getView() {
		return myView;
	}
	public void toFront() {
		myView.toFront();
	}
	private void actualiser()	{
		CRICursor.setWaitCursor(myView);
		switch(myView.getOnglets().getSelectedIndex())		{
		case 0:	NbiStockCtrl.sharedInstance(ec).actualiser();break;
		case 2:	NbiAdminCtrl.sharedInstance(ec).actualiser();break;
		}
		CRICursor.setDefaultCursor(myView);
	}

	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e) {
			actualiser();
		}
	}

}

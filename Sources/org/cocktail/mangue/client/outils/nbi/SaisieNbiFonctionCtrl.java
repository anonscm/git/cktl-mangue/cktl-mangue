// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.outils.nbi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.nbi.SaisieNbiFonctionView;
import org.cocktail.mangue.client.select.FonctionNbiSelectCtrl;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiFonctions;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.nbi.EONbi;
import org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieNbiFonctionCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieNbiFonctionCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieNbiFonctionView myView;

	private EONbi currentNbi;
	private EONbiFonctions currentFonction;
	private EOStructure currentStructure;
	private EONbiRepartFonctions currentRepartFonction;

	public SaisieNbiFonctionCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieNbiFonctionView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);
		myView.getBtnGetComposante().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getComposante();}}
		);
		myView.getBtnDelComposante().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delComposante();}}
		);
		myView.getBtnGetFonction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getFonction();}}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDate(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDate(myView.getTfDateDebut()));

		myView.getTfDateFin().addFocusListener(new FocusListenerDate(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDate(myView.getTfDateFin()));

		CocktailUtilities.initTextField(myView.getTfFonction(), false, false);
		CocktailUtilities.initTextField(myView.getTfComposante(), false, false);

	}

	public static SaisieNbiFonctionCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieNbiFonctionCtrl(editingContext);
		return sharedInstance;
	}


	private EONbi currentNbi() {
		return currentNbi;
	}
	private void setCurrentNbi(EONbi nbi) {
		currentNbi = nbi;
	}	
	private EONbiRepartFonctions currentRepartFonction() {
		return currentRepartFonction;
	}
	private void setCurrentRepartFonction(EONbiRepartFonctions repart) {
		currentRepartFonction = repart;
	}	
	private EONbiFonctions currentFonction() {
		return currentFonction;
	}
	private void setCurrentFonction(EONbiFonctions fonction) {
		currentFonction = fonction;
		myView.getTfFonction().setText("");
		if (currentFonction != null)
			CocktailUtilities.setTextToField(myView.getTfFonction(), currentFonction.llNbiFonction());
	}
	private EOStructure currentStructure() {
		return currentStructure;
	}
	private void setCurrentStructure(EOStructure structure) {
		currentStructure = structure;
		myView.getTfComposante().setText("");
		if (currentStructure != null)
			CocktailUtilities.setTextToField(myView.getTfComposante(), currentStructure.llStructure());
	}
	private void clearTextFields()	{
		myView.getTfComposante().setText("");
		myView.getTfFonction().setText("");
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
	}

	public EONbiRepartFonctions ajouter(EONbi nbi)	{

		setCurrentNbi(nbi);
		myView.setTitle("AJOUT FONCTION NBI - " + currentNbi().cNbi());

		setCurrentRepartFonction(EONbiRepartFonctions.creer(ec, nbi));
		
		updateData();		
		myView.setVisible(true);

		return currentRepartFonction;
	}


	public boolean modifier(EONbiRepartFonctions repart) {

		setCurrentNbi(repart.toNbi());
		setCurrentRepartFonction(repart);

		myView.setTitle("FONCTION NBI " + currentNbi().cNbi());

		updateData();

		myView.setVisible(true);

		return currentRepartFonction != null;
	}

	private void updateData() {

		clearTextFields();

		setCurrentFonction(currentRepartFonction().toNbiFonctions());
		setCurrentStructure(currentRepartFonction().toStructure());

		CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentRepartFonction().dDebNbiFonc());
		CocktailUtilities.setDateToField(myView.getTfDateFin(), currentRepartFonction().dFinNbiFonc());

		updateUI();
		
	}

	/**
	 * 
	 */
	private void updateUI() {

	}

	/**
	 * 
	 * @return
	 */
	private NSTimestamp getDateDebut() {
		if (!StringCtrl.chaineVide(myView.getTfDateDebut().getText()))
			return DateCtrl.stringToDate(myView.getTfDateDebut().getText());
		
		return null;
	}
	private NSTimestamp getDateFin() {
		if (!StringCtrl.chaineVide(myView.getTfDateFin().getText()))
			return DateCtrl.stringToDate(myView.getTfDateFin().getText());
		
		return null;
	}

	/**
	 * 
	 */
	public void getFonction() {

		EONbiFonctions fonction = FonctionNbiSelectCtrl.sharedInstance(ec).getFonction();

		if (fonction != null)
			setCurrentFonction(fonction);

		updateUI();

	}
	private void getComposante() {

		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(ec);
		if (structure != null)
			setCurrentStructure(structure);

		updateUI();

	}
	private void delComposante() {
		setCurrentStructure(null);
	}


	/**
	 * 
	 * Traitements a effectuer avant la validation de la carriere. 
	 * A de moment, les enregistrements ne sont pas effectues dans l'object carriere.
	 * 
	 * @return
	 */
	protected boolean traitementsAvantValidation() {
		
		if (currentRepartFonction().dFinNbiFonc() != null) {
			if (DateCtrl.isAfter(currentRepartFonction().dDebNbiFonc(),currentRepartFonction().dFinNbiFonc())) {
				EODialogs.runInformationDialog("Attention","La date d'effet doit être antérieure au " + DateCtrl.dateToString(currentRepartFonction().dFinNbiFonc()));
				return false;
			}
		} 

		return true;	
	}

	/**
	 * 
	 */
	private void valider()  {

		try {				

			currentRepartFonction().setToStructureRelationship(currentStructure());
			currentRepartFonction().setToNbiFonctionsRelationship(currentFonction());
			currentRepartFonction().setCNbiFonction(currentFonction().cNbiFonction());
			
			currentRepartFonction().setDDebNbiFonc(getDateDebut());
			currentRepartFonction().setDFinNbiFonc(getDateFin());

			if (!traitementsAvantValidation())
				return;

			ec.saveChanges();

			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			myView.toFront();
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		ec.revert();
		setCurrentRepartFonction(null);
		myView.setVisible(false);
	}


	private class ActionListenerDate implements ActionListener	{

		private JTextField myTextField;
		private ActionListenerDate(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText())) {
				updateUI();
				return;
			}

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myTextField.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else {
				myTextField.setText(myDate);
				updateUI();
			}
		}
	}
	private class FocusListenerDate implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDate(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText())) {	
				updateUI();
				return;
			}

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else {
				myTextField.setText(myDate);
				updateUI();
			}
		}
	}	

}

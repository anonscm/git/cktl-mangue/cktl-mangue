// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.outils.nbi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.nbi.SaisieNbiBeneficiaireView;
import org.cocktail.mangue.client.select.IndividuSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTypes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.nbi.EONbi;
import org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class SaisieNbiBeneficiaireCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieNbiBeneficiaireCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieNbiBeneficiaireView myView;

	private EONbi currentNbi;
	private EONbiOccupation currentOccupation;
	private EOIndividu currentIndividu;

	private boolean modeCreation;

	public SaisieNbiBeneficiaireCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieNbiBeneficiaireView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);
		myView.getBtnGetBeneficiaire().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getBeneficiaire();}}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDate(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDate(myView.getTfDateDebut()));

		myView.getTfDateFin().addFocusListener(new FocusListenerDate(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDate(myView.getTfDateFin()));

		CocktailUtilities.initTextField(myView.getTfBeneficiaire(), false, false);

	}

	public static SaisieNbiBeneficiaireCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieNbiBeneficiaireCtrl(editingContext);
		return sharedInstance;
	}


	private EONbi currentNbi() {
		return currentNbi;
	}

	private void setCurrentNbi(EONbi nbi) {
		currentNbi = nbi;
		setPoints(nbi.toNbiTypes());
	}	
	private void setPoints(EONbiTypes type) {
		myView.getPopupPoints().removeAllItems();	
		myView.getPopupPoints().addItem("");
		for (int i=5;i<=type.pointsMaxi().intValue();i+=5) {			
			myView.getPopupPoints().addItem(new Integer(i));
		}
	}

	private EONbiOccupation currentOccupation() {
		return currentOccupation;
	}
	private void setCurrentOccupation(EONbiOccupation occupation) {
		currentOccupation = occupation;
	}	

	private EOIndividu currentIndividu() {
		return currentIndividu;
	}
	private void setCurrentIndividu(EOIndividu individu) {
		currentIndividu = individu;
		myView.getTfBeneficiaire().setText("");
		if (currentIndividu != null)
			CocktailUtilities.setTextToField(myView.getTfBeneficiaire(), currentIndividu().identitePrenomFirst());
	}

	private boolean modeCreation()   {
		return modeCreation;
	}

	private void clearTextFields()	{
		myView.getTfBeneficiaire().setText("");
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
	}

	public EONbiOccupation ajouter(EONbi nbi)	{

		setCurrentNbi(nbi);

		modeCreation = true;
		myView.setTitle("AJOUT BENEFICIAIRE NBI - " + currentNbi().cNbi());

		setCurrentOccupation(EONbiOccupation.creer(ec, nbi));

		updateData();		
		myView.setVisible(true);

		return currentOccupation();
	}


	public boolean modifier(EONbiOccupation occupation) {

		setCurrentNbi(occupation.toNbi());
		setCurrentOccupation(occupation);

		myView.setTitle("BENEIFICAIRE NBI " + currentNbi().cNbi());
		modeCreation = false;

		updateData();

		myView.setVisible(true);

		return currentOccupation() != null;
	}


	private void updateData() {

		clearTextFields();

		setCurrentIndividu(currentOccupation().toIndividu());

		CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentOccupation().dDebNbiOcc());
		CocktailUtilities.setDateToField(myView.getTfDateFin(), currentOccupation().dFinNbiOcc());

		myView.getPopupPoints().setSelectedItem(currentOccupation().nbPoints());

	}

	public void getBeneficiaire() {
		EOIndividu individu = IndividuSelectCtrl.sharedInstance(ec).getIndividu();
		if (individu != null)
			setCurrentIndividu(individu);
	}

	/**
	 * 
	 * Traitements a effectuer avant la validation de la carriere. 
	 * A de moment, les enregistrements ne sont pas effectues dans l'object carriere.
	 * 
	 * @return
	 */
	protected boolean traitementsAvantValidation() {

		if (currentOccupation().dDebNbiOcc() == null) {
			EODialogs.runErrorDialog("Erreur","Vous devez renseigner une date de début d'occupation !");
			return false;
		}

		// Vérifier que le beneficiaire n'aie pas déjà une nbi pendant cette période
		NSMutableArray occupations = new NSMutableArray(EONbiOccupation.rechercherNbiOccupationsPourIndividuEtPeriode(ec, currentIndividu(), currentOccupation().dDebNbiOcc(), currentOccupation().dFinNbiOcc()));		
		occupations.removeIdenticalObject(currentOccupation());
		if (occupations.count() > 0) {
			EONbiOccupation occupation = (EONbiOccupation)occupations.objectAtIndex(0);
			EODialogs.runErrorDialog("Erreur","Cette personne occupe déjà une autre nbi pendant cette période ! (" + occupation.toNbi().cNbi() + " du " + DateCtrl.dateToString(occupation.dDebNbiOcc()) + " au " + DateCtrl.dateToString(occupation.dFinNbiOcc()) + ")");
			return false;
		}

		// Vérifier si cette nbi n'est pas deja été affectée pendant la période
		occupations = new NSMutableArray(NbiStockCtrl.sharedInstance(ec).getOccupations());
		//NSMutableArray(EONbiOccupation.rechercherNbiOccupationsPourPeriode(ec, currentNbi(), getDateDebut(), getDateFin()));		
		occupations.removeIdenticalObject(currentOccupation());
		for (Enumeration<EONbiOccupation> e=occupations.objectEnumerator();e.hasMoreElements();) {
			EONbiOccupation occupation = e.nextElement();
			if (DateCtrl.chevauchementPeriode(occupation.dDebNbiOcc(), occupation.dFinNbiOcc(), currentOccupation().dDebNbiOcc(), currentOccupation().dFinNbiOcc() )) {
				EODialogs.runErrorDialog("Erreur","Cette NBI est déjà occupée pendant cette période par " + occupation.toIndividu().identitePrenomFirst() + " !");
				return false;
			}
		}

		NSArray conges = Duree.rechercherDureesPourIndividuEtPeriode(ec,EOClm.ENTITY_NAME, currentIndividu(),currentOccupation().dDebNbiOcc(),currentOccupation().dFinNbiOcc());
		if (conges != null && conges.count() > 0) {
			EODialogs.runInformationDialog("Attention","Cette personne est en congé longue maladie pendant la période");
		}
		return true;
	}


	/**
	 * 
	 */
	private void valider()  {

		try {				

			currentOccupation().setToIndividuRelationship(currentIndividu());
			if (myView.getPopupPoints().getSelectedIndex() > 0)
				currentOccupation.setNbPoints((Integer)myView.getPopupPoints().getSelectedItem());
			else
				currentOccupation.setNbPoints(null);

			currentOccupation().setDDebNbiOcc(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			currentOccupation().setDFinNbiOcc(CocktailUtilities.getDateFromField(myView.getTfDateFin()));

			if (!traitementsAvantValidation())
				return;

			ec.saveChanges();

			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		ec.revert();
		setCurrentOccupation(null);
		myView.setVisible(false);
	}


	private class ActionListenerDate implements ActionListener	{

		private JTextField myTextField;
		private ActionListenerDate(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myTextField.getText())) {
				return;
			}

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				myTextField.selectAll();
				EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
			}
			else {
				myTextField.setText(myDate);
			}
		}
	}
	private class FocusListenerDate implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDate(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myTextField.getText())) {	
				return;
			}

			String myDate = DateCtrl.dateCompletion(myTextField.getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myTextField.selectAll();
			}
			else {
				myTextField.setText(myDate);
			}
		}
	}	

}


package org.cocktail.mangue.client.outils.nbi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.gui.nbi.NbiAdminView;
import org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiFonctions;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class NbiAdminCtrl {

	private static NbiAdminCtrl sharedInstance;

	private EOEditingContext ec;
	private NbiAdminView myView;
	private EODisplayGroup eod, eodNbi;

	private ListenerFonction 	listenerFonction = new ListenerFonction();
	private EONbiFonctions 		currentFonction;
	private EONbiRepartFonctions 	currentNbi;

	public NbiAdminCtrl(EOEditingContext editingContext) {

		ec = editingContext;
		eod = new EODisplayGroup();
		eodNbi = new EODisplayGroup();

		myView = new NbiAdminView(eod, eodNbi);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		myView.getTfFiltre().getDocument().addDocumentListener(new ADocumentListener());

		myView.getMyEOTable().addListener(listenerFonction);		
		updateUI();
	}

	public static NbiAdminCtrl sharedInstance(EOEditingContext editingContext)
	{
		if(sharedInstance == null)
			sharedInstance = new NbiAdminCtrl(editingContext);
		return sharedInstance;
	}


	private EONbiFonctions currentFonction() {
		return currentFonction;
	}
	private void setCurrentFonction(EONbiFonctions fonction) {
		this.currentFonction = fonction;
	}
	public JPanel getView() {
		return myView;
	}	
	public void toFront() {
		NbiCtrl.sharedInstance(ec).toFront();
	}


	private void clean() {
	}

	public void actualiser()	{

		clean();

		eod.setObjectArray(EONbiFonctions.fetchAll(ec, EONbiFonctions.SORT_ARRAY_LIBELLE_ASC));
		filter();

		CRICursor.setDefaultCursor(myView);

	}

	private void ajouter() {
		SaisieFonctionNbiCtrl.sharedInstance(ec).ajouter();
		actualiser();
	}

	private void modifier() {

		SaisieFonctionNbiCtrl.sharedInstance(ec).modifier(currentFonction());
		myView.getMyEOTable().updateUI();
	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer la fonction sélectionnée ?", "Oui", "Non"))		
			return;			

		try {
			ec.deleteObject(currentFonction());
			ec.saveChanges();
			actualiser();
		}
		catch(Exception e) {
			ec.revert();
			e.printStackTrace();
		}
		toFront();
	}


	private EOQualifier filterQualifier() {

		NSMutableArray qualifiers = new NSMutableArray();

		if(myView.getTfFiltre().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EONbiFonctions.LL_NBI_FONCTION_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltre().getText()+"*")));

		return new EOAndQualifier(qualifiers);
	}

	private void filter() 	{

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();

		updateUI();
	}

	private void updateUI() {
		myView.getBtnModifier().setEnabled(currentFonction() != null);
		myView.getBtnSupprimer().setEnabled(currentFonction() != null && eodNbi.displayedObjects().count() == 0);	
	}

	private class ADocumentListener implements DocumentListener
	{
		public void changedUpdate(DocumentEvent e)
		{
			filter();
		}
		public void insertUpdate(DocumentEvent e)
		{
			filter();
		}
		public void removeUpdate(DocumentEvent e)
		{
			filter();
		}

	}


	private class ListenerFonction implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick()	{
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentFonction((EONbiFonctions)eod.selectedObject());
			eodNbi.setObjectArray(EONbiRepartFonctions.rechercherNbiRepartFonctions(ec, currentFonction()));
			myView.getMyEOTableNbi().updateData();
			updateUI();
		}
	}

}

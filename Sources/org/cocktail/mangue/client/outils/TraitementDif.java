/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils;

import javax.swing.JProgressBar;

import org.cocktail.mangue.client.impression.ThreadInterfaceController;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.swing.EOTextField;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;

// 20/12/2010 - Adaptation Netbeans
public class TraitementDif extends ThreadInterfaceController {
	public JProgressBar progressBar;
	public EOTextField champMessage;
	public final static String SYNCHRONISER_DIFS = "SynchroniserDifs";
	public final static String TERMINER_TRAITEMENT_DIFS = "TerminerDifs";
	private Integer annee,typePopulation, typePersonnel;
	private EOEditingContext editingContext;
	private boolean archiveLoaded;

	public TraitementDif(EOEditingContext editingContext,Integer annee,Integer typePopulation,Integer typePersonnel) {
		this.annee = annee;
		this.typePersonnel = typePersonnel;
		this.typePopulation = typePopulation;
		this.editingContext = editingContext;
		preparerPourTraitementAsynchrone();
	}
	// 20/12/2010
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void prepareComponent() {
		// archiveLoaded est setté en 1er afin d'éviter un multi-chargement de l'archive dans certains cas
		// où le chargement entraîne un appel direct à establishConnection
		if (archiveLoaded == false) {
			archiveLoaded = true;
			try {
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				super.prepareComponent();
			}
		}
	}
	// 20/12/2010
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void establishConnection() {
		// archiveLoaded est setté en 1er afin d'éviter un multi-chargement de l'archive dans certains cas
		// où le chargement entraîne un appel direct à prepareComponent
		if (archiveLoaded == false) {
			archiveLoaded = true;
			try {
				controllerWillLoadArchive();
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				NSDictionary namedObjects = EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
				controllerDidLoadArchive(namedObjects);
				setConnected(true);
				connectionWasEstablished();
			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				super.establishConnection();
			}
			archiveLoaded = true;
		}
	}
	public void setMessage(String aString) {
		if (champMessage != null)
			champMessage.setText(aString);
	}
	/** m&eacute;thode vide pour emp&circ;cher le comportement par d&eacute;faut */
	public void setEdited(boolean aBool) {
		// on ne fait rien pour empêcher de marquer la fenêtre comme éditée
		super.setEdited(false);
	}
	public void lancerTraitement() {
		progressBar.setIndeterminate(true);
		try {
			// s'assurer qu'on travaille dans le même editing context que l'appelant
			setEditingContext(editingContext);
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			Class[] classeParametres = new Class[] {Integer.class,Integer.class,Integer.class};
			Object[] parametres = new Object[] {annee,typePopulation,typePersonnel};
			Boolean result = (Boolean)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestCreerDifs",classeParametres,parametres,false);
			if (result.booleanValue() == false) {
				throw new Exception("Erreur lors du lancement du traitement");
			}
		} catch (Exception e) {
			terminerTraitementDif(e.getMessage(),false);
			terminer();
	}
	}
	public void recupererMessage(NSNotification aNotif) {
		String message = (String)aNotif.object();
		if (StringCtrl.isBasicDigit(message.charAt(0)) && message.indexOf("/") > 0) {
			try {
				// Il s'agit du nombre de difs créés
				if (progressBar.isIndeterminate()) {
					int total = new Integer(message.substring(message.indexOf("/") + 1)).intValue();
					progressBar.setMaximum(total);
					progressBar.setIndeterminate(false);
				}
				int nbElements = new Integer(message.substring(0,message.indexOf("/"))).intValue();
				progressBar.setValue(nbElements);
			} catch (Exception e) {
				// Pas de total ou nbElements car non entier
				setMessage(message);
			}
		} else {
			setMessage(message);
		}
	}
	/** Methode de notification pour recuperer le resultat */
	public void recupererResultat(NSNotification aNotif) {
		String message = (String)objetResultatAction(aNotif);
		terminerTraitementDif(message,true);
		terminer();
	}

	/** Methode de notification pour recuperer une erreur */
	public void recupererErreur(NSNotification aNotif) {
		terminerTraitementDif("Erreur lors de la préparation des difs." + aNotif.object(),false);
		terminer();
	}
	public void terminerTraitement(NSNotification aNotif) {
	}
	// Méthodes privées
	// 11/02/09 - On n'utilise pas de notification pour signaler la fin du traitement car elles cassent toutes les
	// associations des fenêtres
	private void terminerTraitementDif(String message,boolean estFinTraitement) {
		if (estFinTraitement) {
			message += "\nPour les afficher, cliquer sur la loupe";
		}
		EODialogs.runInformationDialog("", message);
		((EODialogController)supercontroller()).closeWindow();	
	}
	/** Methode statique utilisee par les outils de dif pour notifier que les difs doivent etre resynchronisees */
	public static void notifierSynchronisationDifsPourAnnee(String classeEmetteur,Integer annee) {

		NSMutableDictionary userInfo = new NSMutableDictionary();
		userInfo.setObjectForKey(annee,"Annee");
		userInfo.setObjectForKey(classeEmetteur, "ClasseEmetteur");
		NSNotificationCenter.defaultCenter().postNotification(SYNCHRONISER_DIFS,null,userInfo);
	}
	
	/**
	 * 
	 * Methode statique utilisee par les outils de dif pour savoir si ils doivent traiter une notification de synchronisation :
	 * c'est le cas si le recepteur n'est pas l'emetteur de la notification
	 * 
	 * @param destinataire
	 * @param aNotif
	 * @return
	 */
	public static boolean destinataireDoitSynchroniserDifs(Object destinataire,NSNotification aNotif) {
		String classeEmetteur = (String)aNotif.userInfo().objectForKey("ClasseEmetteur");
		return (classeEmetteur != null && destinataire != null && classeEmetteur.getClass().getName().equals(destinataire.getClass().getName()) == false);
	}
	
	/**
	 * 
	 * Methode statique utilisee par les outils de dif pour savoir si ils doivent traiter une notification de synchronisation :
	 * c'est le cas si l'annee passee en parametre correspond a l'annee de la notification
	 * et que le recepteur n'est pas l'emetteur de la notification
	 * 
	 * @param destinataire
	 * @param annee
	 * @param aNotif
	 * @return
	 */
	public static boolean destinataireDoitSynchroniserDifsPourAnnee(Object destinataire,Integer annee,NSNotification aNotif) {
		if (destinataireDoitSynchroniserDifs(destinataire, aNotif)) {
			Integer anneeASynchroniser = (Integer)aNotif.userInfo().objectForKey("Annee");
			return annee != null && anneeASynchroniser != null && annee.intValue() == anneeASynchroniser.intValue();
		} else {
			return false;
		}
	}
}

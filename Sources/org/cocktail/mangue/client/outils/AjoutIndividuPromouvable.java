/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils;

import org.cocktail.client.components.DialogueSimpleAvecStrings;
import org.cocktail.client.components.DialogueWithDisplayGroup;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.IndividuPromouvable;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/** Permet la s&eacute;lection d'un individu ayant un des grades requis dans la promotion et la saisie de la date de promotion.
 * Retourne un individuPromouvable via une notification
 * @author christine
 *
 */
//18/03/2010 - la liste des promouvables peut être vide, les grades de promotion contient au moins un objet, la période correspond à la période
//de promotion. Travaille dans le même editing context que le parent
// 16/12/2010 - Adaptation Netbeans
public class AjoutIndividuPromouvable extends DialogueWithDisplayGroup {
	private NSArray promouvables;
	private NSTimestamp debutPeriode, finPeriode, datePromotion;
	private NSArray gradesPromotion;
	private NSArray elementsCarrierePourPromouvables;
	private IndividuPromouvable individuPromouvable;
	private NSMutableArray listeIndividus;
	private DialogueSimpleAvecStrings controleurSelection;
	public final static String NOTIFICATION_SELECTION_INDIVIDU_PROMOUVABLE = "NotificationSelectionIndividuPromouvable";
	private final static String NOTIFICATION_SELECTION_DIALOGUE = "SelectionIndividuPromouvablePourAjout";
	
	public AjoutIndividuPromouvable() {
		super(null,NOTIFICATION_SELECTION_INDIVIDU_PROMOUVABLE,null,"Ajout d'un individu promouvable",false);
		EOArchive.loadArchiveNamed("AjoutIndividuPromouvable", this,"org.cocktail.mangue.client.outils.interfaces", this.disposableRegistry());
	}
	public void connectionWasEstablished() {
		super.connectionWasEstablished();
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
	}
	public boolean init(NSArray gradesPromotion, NSArray promouvables,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		this.promouvables = promouvables;
		this.debutPeriode = debutPeriode;
		this.finPeriode = finPeriode;
		this.gradesPromotion = gradesPromotion;
		individuPromouvable = null;
		datePromotion = null;
		listeIndividus = null;
		controleurSelection = null;
		return preparerIndividus();
	}
	// Accesseurs
	public String datePromotionFormatee() {
		if (datePromotion == null) {
			return "";
		} else {
			NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y");
			return formatter.format(datePromotion);
		}
	}
	public void setDatePromotionFormatee(String uneDate) {
		if (uneDate == null) {
			datePromotion = null;
		} else {
			String myDate = DateCtrl.dateCompletion((String)uneDate);
			if (myDate.equals("")) {
				datePromotion = null;
			} else {
				datePromotion = DateCtrl.stringToDate(myDate);
			} 
		}
	}
	public String identiteIndividu() {
		if (individuPromouvable == null) {
			return null;
		} else {
			return individuPromouvable.individu().identite();
		}
	}
	public EOEditingContext editingContext() {
		if (gradesPromotion == null) {
			return null;
		}
		return ((EOGrade)gradesPromotion.objectAtIndex(0)).editingContext();
	}
	// Actions
	/** Affiche la liste de tous les individus qui ont le grade de promotion et qui ne sont pas dans la liste des promouvables */
	public void selectionnerIndividu() {
		if (controleurSelection == null) {
			controleurSelection = new DialogueSimpleAvecStrings(NOTIFICATION_SELECTION_DIALOGUE,"Sélectionner un individu",false,true,listeIndividus);
			controleurSelection.init();
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getIndividuPromouvable",new Class[] {NSNotification.class}),NOTIFICATION_SELECTION_DIALOGUE,null);
		}
		// s'enregistrer pour recevoir les notifications
		controleurSelection.afficherFenetre();
	}
	public void annuler() {
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);
	}
	public void valider() {
		LogManager.logInformation("valider");

		NSArray echelons = EOPassageEchelon.rechercherPassagesEchelonPourGradesValidesPourPeriode(editingContext(), individuPromouvable.elementCarriere().toGrade(), debutPeriode,finPeriode);
		echelons = EOSortOrdering.sortedArrayUsingKeyOrderArray(echelons,new NSArray(EOSortOrdering.sortOrderingWithKey("cEchelon", EOSortOrdering.CompareAscending))); 
		// Rechercher l'échelon suivant
		boolean takeNext = false;
		for (java.util.Enumeration<EOPassageEchelon> e1 = echelons.objectEnumerator();e1.hasMoreElements();) {
			EOPassageEchelon echelon = e1.nextElement(); 
			if (individuPromouvable.elementCarriere().cEchelon().equals(echelon.cEchelon())) {
				takeNext = true;
			} else if (takeNext) {
				individuPromouvable.setEchelonSuivant(echelon.cEchelon());
				break;
			}
		}
		individuPromouvable.setDatePromotion(datePromotionFormatee());
		NSNotificationCenter.defaultCenter().removeObserver(this,NOTIFICATION_SELECTION_DIALOGUE,null);
		NSNotificationCenter.defaultCenter().postNotification(validationNotification(),individuPromouvable);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);
	}
	// Notifications
	public void getIndividuPromouvable(NSNotification aNotif) {
		// La notification contient une string
		int indexIndividu = indexIndividu((String)aNotif.object());
		EOElementCarriere elementCarriere = (EOElementCarriere)elementsCarrierePourPromouvables.objectAtIndex(indexIndividu);
		individuPromouvable = new IndividuPromouvable(elementCarriere);
		updateDisplayGroups();
	}
	// Méthodes du controller DG
	public boolean peutValider() {
		return datePromotion != null && individuPromouvable != null;
	}
	// Méthodes protégées
	protected void prepareInterface() {
	}
	protected Object selectedObject() {
		// TODO Auto-generated method stub
		return null;
	}

	// Méthodes privées
	// Recherche les individus ayant un des grades et qui n'ont pas atteint l'échelon maximum
	private boolean preparerIndividus() {
		NSMutableArray	qualifiers = new NSMutableArray(),qualifiersOR = new NSMutableArray();
		// On recherche les éléments de carrière valides sur la période
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOElementCarriere.DATE_DEBUT_KEY, debutPeriode, EOElementCarriere.DATE_FIN_KEY, finPeriode));
		// Qualifier sur les grades
		java.util.Enumeration e = gradesPromotion.objectEnumerator();
		while (e.hasMoreElements()) {
			EOGrade grade = (EOGrade)e.nextElement();
			qualifiersOR.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.C_GRADE_KEY + " = %@", new NSArray(grade.cGrade())));
		}
		qualifiers.addObject(new EOOrQualifier(qualifiersOR));
		NSArray elementsCarriere = EOElementCarriere.rechercherElementsAvecCriteres(editingContext(), new EOAndQualifier(qualifiers), true, true);
		// Trier par ordre alphabétique croissant
		elementsCarriere = EOSortOrdering.sortedArrayUsingKeyOrderArray(elementsCarriere, new NSArray(EOSortOrdering.sortOrderingWithKey("toIndividu.nomUsuel", EOSortOrdering.CompareAscending)));
		// Créer la liste des individus qui ne font pas partie des promouvables
		NSMutableArray elements = new NSMutableArray();
		listeIndividus = new NSMutableArray();
		e = elementsCarriere.objectEnumerator();
		NSArray individus = (NSArray)promouvables.valueForKey("individu");
		int nbIndividu = 0;
		while (e.hasMoreElements()) {
			EOElementCarriere element = (EOElementCarriere)e.nextElement();
			if (individus.containsObject(element.toIndividu()) == false) {
				// Vérifier si l'individu a atteint l'échelon maximum, si ce n'est pas le cas, on le garde
				NSArray echelons = EOPassageEchelon.rechercherPassagesEchelonPourGradesValidesPourPeriode(editingContext(), element.toGrade(), debutPeriode,finPeriode);
				echelons = EOSortOrdering.sortedArrayUsingKeyOrderArray(echelons,new NSArray(EOSortOrdering.sortOrderingWithKey("cEchelon", EOSortOrdering.CompareAscending))); 
				EOPassageEchelon echelon = (EOPassageEchelon)echelons.lastObject();
				if (element.cEchelon() != null && element.cEchelon().equals(echelon.cEchelon()) == false)	{
					elements.addObject(element);
					// Dans la liste des individus pour sélection. On met un index pour le cas très peu probable où il existe des synomymes
					nbIndividu++;
					listeIndividus.addObject("" + nbIndividu + " - " + element.toIndividu().identite());
				}
			}
		}
		elementsCarrierePourPromouvables = new NSArray(elements);
		return elementsCarrierePourPromouvables.count() > 0;
	}
	private int indexIndividu(String nomIndividu) {
		int index = nomIndividu.indexOf(" - ");
		String temp = nomIndividu.substring(0,index);
		return new Integer(temp).intValue() - 1;	// on a numéroté de 1 à n
	}
	
}

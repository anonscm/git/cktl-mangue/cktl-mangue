
package org.cocktail.mangue.client.outils.medical;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.medical.MedicalMedecinsView;
import org.cocktail.mangue.client.outils.nbi.NbiCtrl;
import org.cocktail.mangue.client.select.SaisieIndividuCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.medical.EODonneesMedicales;
import org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class MedicalMedecinsCtrl {

	private static MedicalMedecinsCtrl sharedInstance;

	private EOEditingContext ec;
	private MedicalMedecinsView myView;
	private EODisplayGroup eod;

	private ListenerMedecins listenerMedecin = new ListenerMedecins();
	private EOMedecinTravail currentMedecin;

	public MedicalMedecinsCtrl(EOEditingContext editingContext) {

		ec = editingContext;
		eod = new EODisplayGroup();
		myView = new MedicalMedecinsView(eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		myView.getMyEOTable().addListener(listenerMedecin);
		updateUI();
		
	}

	public static MedicalMedecinsCtrl sharedInstance(EOEditingContext editingContext)
	{
		if(sharedInstance == null)
			sharedInstance = new MedicalMedecinsCtrl(editingContext);
		return sharedInstance;
	}

	

	public EOMedecinTravail currentMedecin() {
		return currentMedecin;
	}

	public void setCurrentMedecin(EOMedecinTravail currentMedecin) {
		this.currentMedecin = currentMedecin;
	}

	public JPanel getView() {
		return myView;
	}	

	private void clean() {
		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
	}

	public void actualiser()	{

		clean();

		eod.setObjectArray(EOMedecinTravail.fetchAll(ec));
		myView.getMyEOTable().updateData();
		
		CRICursor.setDefaultCursor(myView);

	}

	private void ajouter() {

		try {
			EOIndividu individu = SaisieIndividuCtrl.sharedInstance(ec).getIndividu(true, false);
			if (individu != null) {
				if (individu.dNaissance() == null)
					individu.setDNaissance(DateCtrl.stringToDate("01/01/1900"));
				if (individu.indNoInsee() == null && individu.indNoInseeProv() == null)						
					individu.construireNoInseeProvisoire();
				ec.saveChanges();
				EOMedecinTravail.creer(ec, individu);
			}			
			ec.saveChanges();
			actualiser();
	}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer le médecin sélectionné ?", "Oui", "Non"))		
			return;

		try {

			NSArray donneesMedicales = EODonneesMedicales.findForMedecin(ec, currentMedecin());
			if (donneesMedicales.count() > 0) {
				EODialogs.runErrorDialog("ERREUR", "Ce médecin est associé à plusieurs dossiers médicaux, vous ne pouvez pas le supprimer !");
				return;
			}
			
			ec.deleteObject(currentMedecin());
			
			ec.saveChanges();
			actualiser();
			NbiCtrl.sharedInstance(ec).toFront();
		}
		catch(Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}

	private void updateUI() {

		myView.getBtnSupprimer().setEnabled(currentMedecin() != null);
		
	}

	private class ListenerMedecins implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick()	{
		}
		public void onSelectionChanged() {

			currentMedecin = (EOMedecinTravail)eod.selectedObject();
			updateUI();
		}
	}

}
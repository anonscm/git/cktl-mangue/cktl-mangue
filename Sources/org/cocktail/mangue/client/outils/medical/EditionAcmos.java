/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils.medical;


//import groovy.model.DefaultTableModel;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.Window;
import java.io.File;

import javax.swing.JFileChooser;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.modele.IndividuAcmoPourEdition;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTextArea;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Affiche dans une liste des IndividuAcmoPourEdition : les dates de fin sont modifiables pour prolonger leur fonction */
// 20/12/2010 - Adaptation Netbeans
public class EditionAcmos extends ModelePageComplete {
	public EOView vuePeriode;
	public EOTextArea listeEmails, listeTelephones;
	private GestionPeriode controleurPeriode;
	private String titreEdition;

	// accesseurs
	public String titreEdition() {
		return titreEdition;
	}
	public void setTitreEdition(String titreEdition) {
		if (titreEdition == null) {
			this.titreEdition = "";		// pour ne pas avoir de problème lors de l'impression
		} else {
			this.titreEdition = titreEdition;
		}
	}
	/** retourne le nombre total d'elements affiches */
	public int nbTotalElements() {
		return displayGroup().displayedObjects().count();
	}
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		if (group == displayGroup()) {
			if (currentIndividuAcmo() == null) {
				listeEmails.setText("");
				listeTelephones.setText("");
			} else {
				listeEmails.setText(currentIndividuAcmo().emailsFormates());
				listeTelephones.setText(currentIndividuAcmo().telephonesFormates());
			}
		}
		updaterDisplayGroups();
		super.displayGroupDidChangeSelection(group);
	}
	// Actions
	public void rechercher() {
		LogManager.logDetail("EditionAcmos - rechercher");
		Window activeWindow = EOApplication.sharedApplication().windowObserver().activeWindow();
		if (activeWindow != null) {
			activeWindow.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		} 
		displayGroup().setObjectArray(infosPourEditionAcmo());
		displayGroup().updateDisplayedObjects();
		controllerDisplayGroup().redisplay();
		if (activeWindow != null) {
			activeWindow.setCursor(Cursor.getDefaultCursor());
		} 
	}
	public void exporter() {
		LogManager.logDetail("EditionAcmos - exporter");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourExport(),file.getParent(),file.getName());
		}
	}
	public void imprimer() {
		LogManager.logDetail("EditionAcmos - imprimer");
	
		Class[] classeParametres = new Class[] {NSArray.class,String.class};
		Object[] parametres = new Object[]{displayGroup().displayedObjects(),titreEdition()};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerListeAcmos",classeParametres,parametres,"ListeAcmos","Impression des Acmos");
		} catch (Exception exc) {
			LogManager.logException(exc);
			EODialogs.runErrorDialog("Erreur",exc.getMessage());
		}
	}
	// Notifications
	public void synchroniserDates(NSNotification aNotif) {
		LogManager.logDetail("EditionAcmos - synchroniserDates");
		updaterDisplayGroups();
	}
	// méthodes du controller DG
	public boolean peutRechercher() {
		return controleurPeriode != null && datesValides(controleurPeriode.dateDebut(),controleurPeriode.dateFin()) && !modificationEnCours();
	}
	public boolean peutExporter() {
		return displayGroup().displayedObjects().count() > 0 && !modificationEnCours();
	}
	public boolean peutImprimer() {
		return peutExporter() && titreEdition() != null;
	}
	public boolean peutValider() {
		return modificationEnCours() && currentIndividuAcmo() != null && datesValides(currentIndividuAcmo().repartAssociation().dateDebut(),currentIndividuAcmo().repartAssociation().dateFin());
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		controleurPeriode = new GestionPeriode(null,null,((ApplicationClient)EOApplication.sharedApplication()).periodeAbsenceParDefaut());
		controleurPeriode.init();
		GraphicUtilities.swaperView(vuePeriode,controleurPeriode.component());
		Font font = listeEmails.getFont();	
		listeEmails.textArea().setFont(new Font(font.getName(),font.getStyle(),11));
		font = listeTelephones.getFont();
		listeTelephones.textArea().setFont(new Font(font.getName(),font.getStyle(),11));
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserDates", new Class[] {NSNotification.class}),
				   GestionPeriode.NOTIFICATION_PERIODE_HAS_CHANGED,null);
	}
	/** Pas de cr&eacute;ation */
	protected void traitementsPourCreation() {
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runInformationDialog("Erreur", message);
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	/** Pas de fetch immédiat des objets */
	protected NSArray fetcherObjets() {
		return null;
	}
	/** Pas de destruction */
	protected String messageConfirmationDestruction() {
		return null;
	}
	/** Tri des IndividuAcmoPourEdition sur les dates d'habilitation decroissantes */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("repartAssociation.dateDebut", EOSortOrdering.CompareDescending)));
	}
	
	protected boolean traitementsAvantValidation() {
		return true;
	}
	/** Pas de suppression */
	protected boolean traitementsPourSuppression() {
		return false;
	}
	/** Retourne tous les individus qui ont la fonction d'Acmo */
	protected NSArray infosPourEditionAcmo() {
		NSArray reparts = EORepartAssociation.rechercherRepartAssociationsPourAssociationEtPeriode(editingContext(), "ACMO",controleurPeriode.dateDebut(),controleurPeriode.dateFin());
		java.util.Enumeration e = reparts.objectEnumerator();
		NSMutableArray result = new NSMutableArray(reparts.count());
		while (e.hasMoreElements()) {
			EORepartAssociation repart = (EORepartAssociation)e.nextElement();
			IndividuAcmoPourEdition individu = new IndividuAcmoPourEdition(repart);
			result.addObject(individu);
		}
		return result;
	}

	protected String textePourExport() {
		String texte = "Nom\tPrenom\tDebut Habilitation\tFin Habilitation\tTéléphone\tEmail\n";
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			IndividuAcmoPourEdition individuAcmo = (IndividuAcmoPourEdition)e.nextElement();
			int maxCount = individuAcmo.emails().count();
			if (individuAcmo.telephonesProfessionnels().count() > maxCount) {
				maxCount = individuAcmo.telephonesProfessionnels().count();
			}
			EOIndividu individu = individuAcmo.repartAssociation().individu();
			for (int i = 0; i < maxCount;i++) {
				texte += individu.nomUsuel() + "\t" + individu.prenom() + "\t" + individuAcmo.repartAssociation().dateDebutFormatee() + "\t";
				if (individuAcmo.repartAssociation().dateFin() != null) {
					texte += individuAcmo.repartAssociation().dateFinFormatee() + "\t";
				} else {
					texte += "\t";
				}
				if (i < individuAcmo.telephonesProfessionnels().count()) {
					texte += individuAcmo.telephonesProfessionnels().objectAtIndex(i) + "\t";
				} else {
					texte += "\t";
				}
				if (i < individuAcmo.emails().count()) {
					texte += individuAcmo.emails().objectAtIndex(i);
				}
				texte += "\n";
			}
		}
		return texte;
	}
	protected void terminer() {
	}
	// Méthodes privées
	private IndividuAcmoPourEdition currentIndividuAcmo() {
		return (IndividuAcmoPourEdition)displayGroup().selectedObject();
	}
	private boolean datesValides(NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (dateDebut != null && dateFin != null) {
			return dateFin.after(dateDebut);
		} else if (dateDebut != null || (dateDebut == null && dateFin == null)) {
			return true;
		} else {
			return false;
		}
	}
	
}

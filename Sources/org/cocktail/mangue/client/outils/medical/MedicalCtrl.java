package org.cocktail.mangue.client.outils.medical;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nbi.NbiView;
import org.cocktail.mangue.common.utilities.CRICursor;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;

public class MedicalCtrl {

	private static MedicalCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.TRUE;
	private EOEditingContext ec;
	private NbiView myView;
	private OngletChangeListener listenerOnglets;

	public MedicalCtrl(EOEditingContext editingContext) {

		listenerOnglets = new OngletChangeListener();
		ec = editingContext;

		myView = new NbiView(null, MODE_MODAL.booleanValue());

		myView.getOnglets().addTab(" Médecins du travail ", null, MedicalMedecinsCtrl.sharedInstance(ec).getView());
		//myView.getOnglets().addTab(" Editions ", null, MedicalEditionsCtrl.sharedInstance(ec).getView());
		myView.getOnglets().addChangeListener(listenerOnglets);
		
	}

	public static MedicalCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new MedicalCtrl(editingContext);
		return sharedInstance;
	}

	public void open()	{
		
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		listenerOnglets.stateChanged(null);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}

	public JFrame getView() {
		return myView;
	}
	public void toFront() {
		myView.toFront();
	}
	private void actualiser()	{
		CRICursor.setWaitCursor(myView);
		switch(myView.getOnglets().getSelectedIndex())		{
		case 0:	MedicalMedecinsCtrl.sharedInstance(ec).actualiser();break;
		}
		CRICursor.setDefaultCursor(myView);
	}

	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e) {
			actualiser();
		}
	}

}

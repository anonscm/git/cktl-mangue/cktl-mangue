/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils.medical;

import java.awt.Cursor;
import java.awt.Window;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.InfoPourEditionMedicale;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

public abstract class EditionMedicale extends ModelePageComplete {
	public EOView vuePeriode;
	private GestionPeriode controleurPeriode;
	private String titreEdition,nomRecherche,prenomRecherche;

	// accesseurs
	public String titreEdition() {
		return titreEdition;
	}
	public void setTitreEdition(String titreEdition) {
		if (titreEdition == null) {
			this.titreEdition = "";		// pour ne pas avoir de problème lors de l'impression
		} else {
			this.titreEdition = titreEdition;
		}
	}
	public String nomRecherche() {
		return nomRecherche;
	}
	public void setNomRecherche(String nomRecherche) {
		this.nomRecherche = nomRecherche;
	}
	public String prenomRecherche() {
		return prenomRecherche;
	}
	public void setPrenomRecherche(String prenomRecherche) {
		this.prenomRecherche = prenomRecherche;
	}
	/** retourne le nombre total d'&eacute;l&eacute;ments affich&eacute;s */
	public int nbTotalElements() {
		return displayGroup().displayedObjects().count();
	}
	// actions
	public void rechercher() {
		LogManager.logDetail(this.getClass().getName() + " rechercher");
		Window activeWindow = EOApplication.sharedApplication().windowObserver().activeWindow();
		if (activeWindow != null) {
			activeWindow.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		} 
		
		displayGroup().setObjectArray(infosPourNomEtPrenom(nomRecherche(),prenomRecherche()));
		displayGroup().updateDisplayedObjects();
		controllerDisplayGroup().redisplay();
		if (activeWindow != null) {
			activeWindow.setCursor(Cursor.getDefaultCursor());
		} 
	}
	public void exporter() {
		LogManager.logDetail(this.getClass().getName() + " exporter");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourExport(),file.getParent(),file.getName());
		}
	}
	public void imprimer() {
		LogManager.logDetail(this.getClass().getName() + " imprimer");
		
		if (titreEdition() == null || titreEdition().length() == 0) {
			// Sélectionner les destinataires
			UtilitairesDialogue.afficherDialogue(this,"Destinataire", "getDestinataires", false,true,null,true,false);
		} else {
			imprimerListeConvocations();
		}
	}
	// Notifications
	public void synchroniserDates(NSNotification aNotif) {
		LogManager.logDetail("EditionAcmos - synchroniserDates");
		updaterDisplayGroups();
	}
	public void getDestinataires(NSNotification aNotif) {
		// Récupérer les destinataires (c'est un tableau de globalIDs de EODestinataire)
		imprimerConvocations((NSArray)aNotif.object());
		NSNotificationCenter.defaultCenter().removeObserver(this,"SelectionDestinataire",null);
	}
	// méthodes du controller DG
	public boolean peutRechercher() {
		return controleurPeriode != null && datesValides(controleurPeriode.dateDebut(),controleurPeriode.dateFin());
	}
	public boolean peutImprimer() {
		return displayGroup().displayedObjects().count() > 0;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		//   préparer le contrôleur de période et l'afficher dans la vue des périodes en sélectionnant l'affichage des périodes par défaut
		controleurPeriode = new GestionPeriode(null,null,((ApplicationClient)EOApplication.sharedApplication()).periodeAbsenceParDefaut());
		controleurPeriode.init();
		GraphicUtilities.swaperView(vuePeriode,controleurPeriode.component());
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserDates", new Class[] {NSNotification.class}),
				   GestionPeriode.NOTIFICATION_PERIODE_HAS_CHANGED,null);
	}
	protected void traitementsPourCreation() {
		// pas de création
	}
	protected boolean traitementsPourSuppression() {
		// pas de suppression
		return false;
	}
	protected String messageConfirmationDestruction() {
		// // pas de suppression
		return null;
	}
	protected NSArray fetcherObjets() {
		// pas d'affichage au démarrage
		return null;
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		// pas de validation
		return true;
	}
	protected NSTimestamp dateDebut() {
		if (controleurPeriode != null && controleurPeriode.dateDebut() != null) {
			return controleurPeriode.dateDebut();
		} else {
			return null;
		}
	}
	protected NSTimestamp dateFin() {
		if (controleurPeriode != null && controleurPeriode.dateFin() != null) {
			return controleurPeriode.dateFin();
		} else {
			return null;
		}
	}
	protected NSArray infosAffichees() {
		return displayGroup().displayedObjects();
	}
	protected void parametrerDisplayGroup() {
		// pas de traitement spécifique : les tris sont faits par la méthode de recherche
	}
	protected void traitementsApresValidation() {}
	protected void afficherExceptionValidation(String message) {}
	/** m&eacute;thode &agrave; d&eacute;finir dans les sous-classes. Retourne le texte &agrave; exporter dans un fichier */
	protected abstract String textePourExport();
	/** m&eacute;thode &agrave; d&eacute;finir dans les sous-classes. Retourne la liste des InfosPourEditionMedicales */
	protected abstract NSArray infosEditions();
	/** m&eeacute;thode &grave; d&eacute;finir dans les sous-classes. Retourne le titre affich&eacute; dans la fen&ecirc;tre d'attente
	 * pendant l'impression
	 */
	/** Retourne les infos pour les noms et pr&eacute;noms recherch&eacute;s */
	protected abstract NSArray infosPourNomEtPrenom(String nom,String prenom);
	protected abstract String titreAttente(int typeImpression);
	protected String ajouterStringToString(String stringAjoutee,String stringDestin) {
		if (stringAjoutee != null) {
			return stringDestin + stringAjoutee + "\t";
		} else {
			return stringDestin + "\t";
		}
	}
	protected void terminer() {
	}
	// Méthodes privées
	private void imprimerConvocations(NSArray destinataires) {
		LogManager.logDetail(this.getClass().getName() + " : impression des convocations");
		InfoPourEditionMedicale infoImpression = null;
		java.util.Enumeration e = infosEditions().objectEnumerator();
		while (e.hasMoreElements()) {
			InfoPourEditionMedicale info = (InfoPourEditionMedicale)e.nextElement();
			if (info.typeEdition() == InfoPourEditionMedicale.TYPE_CONVOCATION) {
				infoImpression = info;
				break;
			}	
		}
		if (infoImpression == null) {
			LogManager.logDetail(this.getClass().getName() + "Pas d'information correspondant à ce type d'impression");
			return;
		}
		NSArray infos = Utilitaires.tableauDeGlobalIDs(infosAffichees(), editingContext());
		Class[] classeParametres = new Class[] {NSArray.class,InfoPourEditionMedicale.class,NSArray.class};
		Object[] parametres = new Object[]{infos,infoImpression,destinataires};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),infoImpression.methodeImpression(),classeParametres,parametres,infoImpression.nomFichierImpression(),titreAttente(infoImpression.typeEdition()));
		} catch (Exception exc) {
			LogManager.logException(exc);
			EODialogs.runErrorDialog("Erreur",exc.getMessage());
		}
	
	}
	private void imprimerListeConvocations() {
		LogManager.logDetail(this.getClass().getName() + " : impression de la liste des convocations");
		InfoPourEditionMedicale infoImpression = null;
		java.util.Enumeration e = infosEditions().objectEnumerator();
		while (e.hasMoreElements()) {
			InfoPourEditionMedicale info = (InfoPourEditionMedicale)e.nextElement();
			if (info.typeEdition() == InfoPourEditionMedicale.TYPE_LISTE) {
				infoImpression = info;
				break;
			}	 
		}
		if (infoImpression == null) {
			LogManager.logDetail(this.getClass().getName() + "Pas d'information correspondant à ce type d'impression");
			return;
		}
		Class[] classeParametres = new Class[] {NSArray.class,InfoPourEditionMedicale.class,String.class};
		NSArray infosMedicales = Utilitaires.tableauDeGlobalIDs(displayGroup().displayedObjects(), editingContext());
		Object[] parametres = new Object[]{infosMedicales,infoImpression,titreEdition()};

		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),infoImpression.methodeImpression(),classeParametres,parametres,infoImpression.nomFichierImpression(),titreAttente(infoImpression.typeEdition()));
		} catch (Exception exc) {
			LogManager.logException(exc);
			EODialogs.runErrorDialog("Erreur",exc.getMessage());
		}
	}
	private boolean datesValides(NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (dateDebut != null && dateFin != null) {
			return dateFin.after(dateDebut);
		} else if (dateDebut != null || (dateDebut == null && dateFin == null)) {
			return true;
		} else {
			return false;
		}
	}
}

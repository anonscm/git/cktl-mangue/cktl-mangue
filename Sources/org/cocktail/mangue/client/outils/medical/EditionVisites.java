/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils.medical;

import java.io.File;

import javax.swing.JFileChooser;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.modele.InfoPourEditionMedicale;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.medical.EOVisiteMedicale;
import org.cocktail.mangue.modele.mangue.individu.medical.InfoMedicalePourIndividu;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

//20/12/2010 - Adaptation Netbeans (suppression de preparerFenetre)
public class EditionVisites extends EditionMedicale {
	private String lieuRecherche;
	
	// Accesseurs
	public String lieuRecherche() {
		return lieuRecherche;
	}
	public void setLieuRecherche(String lieuRecherche) {
		this.lieuRecherche = lieuRecherche;
	}
	// Méthodes du controller DG
	public void exporterBilan() {
		super.exporter();
	}
	public void exporterHistorique() {
		LogManager.logDetail(this.getClass().getName() + "exporterHistorique");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourHistorique(),file.getParent(),file.getName());
		}
	}
	// Méthodes protégées
	protected NSArray infosEditions() {
		NSMutableArray infos = new NSMutableArray(2);
		infos.addObject(new InfoPourEditionMedicale(InfoPourEditionMedicale.TYPE_CONVOCATION,"clientSideRequestImprimerConvocationsVisite","ConvocationsVisite","ConvocationsVisite"));
		infos.addObject(new InfoPourEditionMedicale(InfoPourEditionMedicale.TYPE_LISTE,"clientSideRequestImprimerListeConvocationsVisite","ListeConvocationsVisite","ListeConvocationsVisite"));
		return infos;
	}
	protected NSArray infosPourNomEtPrenom(String nom, String prenom) {
		if (lieuRecherche() == null || lieuRecherche().length() == 0) {
			return InfoMedicalePourIndividu.rechercherInfosMedicalesPourPeriodeEtIndividus(editingContext(), "VisiteMedicale", dateDebut(), dateFin(), nom,prenom);
		} else {
			return EOVisiteMedicale.rechercherVisitesMedicalesPourPeriodeLieuEtIndividus(editingContext(), dateDebut(), dateFin(), nom,prenom,lieuRecherche());
		}
	}
	protected String textePourExport() {
		String texte = "Nom\tPrenom\tResponsable Acmo\tDate Visite\tHeure Visite" +
				"\tRésultat Visite\tRythme Surveillance\tType Visite\tComposante\n";
		java.util.Enumeration e = infosAffichees().objectEnumerator();
		while (e.hasMoreElements()) {
			EOVisiteMedicale visite = (EOVisiteMedicale)e.nextElement();
			texte += visite.individu().nomUsuel() + "\t" + visite.individu().prenom() + "\t";
			if (visite.estResponsableAcmo()) {
				texte += "Oui";
			}
			texte += "\t";
			texte += visite.dateFormatee() + "\t";
			texte = ajouterStringToString(visite.heure(),texte);
			texte = ajouterStringToString((String)visite.resultatVisite().valueForKey("libelle"), texte);
			texte = ajouterStringToString(visite.rythmeSurveillanceFormate(), texte);
			texte = ajouterStringToString(visite.libelleType(), texte);
			texte += libelleStructureAffectationADate(visite);
			texte += "\n";
		}

		return texte;
	}
	protected String titreAttente(int typeImpression) {
		if (typeImpression == InfoPourEditionMedicale.TYPE_CONVOCATION) {
			return "Impression des convocations pour les Visites";
		} else if (typeImpression == InfoPourEditionMedicale.TYPE_LISTE) {
			return "Impression de la liste des agents convoqués à la visite médicale";
		} else {
			return "Impression";
		}
	}
	// Méthodes privées
	private String textePourHistorique() {
		String texte = "Site Géographique\tPosition actuelle\tPosition pour visite\tAbsence actuelle\tAbsence pour Visite\tCivilité\tNom\tPrenom\tDate Visite\tHeure Visite" +
				"\tType Visite\tRésultat visite\tRythme Surveillance\tComposante actuelle\tService actuel\tComposante Visite\tService Visite\tStatut Actuel\tStatut Visite\tObservations\tMédecin\n";
		java.util.Enumeration e = infosAffichees().objectEnumerator();
		while (e.hasMoreElements()) {
			EOVisiteMedicale visite = (EOVisiteMedicale)e.nextElement();
			if (visite.adresse() != null) {
				texte = ajouterStringToString(visite.adresse().ville(), texte);
			} else {
				texte += "\t";
			}
			NSTimestamp today = new NSTimestamp();
			texte += visite.positionPourDate(today) + "\t" + visite.positionPourDate(visite.date()) + "\t" + visite.congePourDate(today) + "\t" +
			visite.congePourDate(visite.date()) + "\t" + visite.individu().civilite() + "\t" +
			visite.individu().nomUsuel() + "\t" + visite.individu().prenom() + "\t" + visite.dateFormatee() + "\t";
			texte = ajouterStringToString(visite.heure(),texte);
			texte = ajouterStringToString(visite.libelleType(), texte);
			if (visite.resultatVisite() != null) {
				texte = ajouterStringToString((String)visite.resultatVisite().valueForKey("libelle"), texte);
			} else {
				texte += "\t";
			}
			texte = ajouterStringToString(visite.rythmeSurveillanceFormate(), texte);
			texte += visite.composantePourDate(today) + "\t" + visite.servicePourDate(today) + "\t" + visite.composantePourDate(visite.date()) + 
			"\t" + visite.servicePourDate(visite.date()) + "\t" + visite.statutPourDate(today) + "\t" + visite.statutPourDate(visite.date()) + "\t";
			if (visite.observation() != null) {
				texte += visite.observation();
			}
			texte +="\t";
			if (visite.medecin() != null) {
				texte += visite.medecin().individu().identite();
			}
			texte += "\n";
		}

		return texte;
	}
	private String libelleStructureAffectationADate(EOVisiteMedicale visite) {
		NSArray affectations = EOAffectation.findForIndividu(editingContext(),visite.individu(),visite.date());
		try {
			return ((EOAffectation)affectations.objectAtIndex(0)).toStructureUlr().llStructure();
		} catch (Exception e) {
			return "";
		}
	}



}

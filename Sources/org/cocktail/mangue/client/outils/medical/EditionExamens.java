/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils.medical;

import org.cocktail.mangue.modele.InfoPourEditionMedicale;
import org.cocktail.mangue.modele.mangue.individu.medical.EOExamenMedical;
import org.cocktail.mangue.modele.mangue.individu.medical.InfoMedicalePourIndividu;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

//20/12/2010 - Adaptation Netbeans (suppression de preparerFenetre)
public class EditionExamens extends EditionMedicale {
	
	protected NSArray infosEditions() {
		NSMutableArray infos = new NSMutableArray(2);
		infos.addObject(new InfoPourEditionMedicale(InfoPourEditionMedicale.TYPE_CONVOCATION,"clientSideRequestImprimerConvocationsExamen","ConvocationsExamen","ConvocationsExamen"));
		infos.addObject(new InfoPourEditionMedicale(InfoPourEditionMedicale.TYPE_LISTE,"clientSideRequestImprimerListeConvocationsExamen","ListeConvocationsExamen","ListeConvocationsExamen"));
		return infos;
	}
	protected NSArray infosPourNomEtPrenom(String nom, String prenom) {
		return InfoMedicalePourIndividu.rechercherInfosMedicalesPourPeriodeEtIndividus(editingContext(), "ExamenMedical", dateDebut(), dateFin(), nom, prenom);

	}
	protected String textePourExport() {
		String texte = "Site Géographique\tPosition actuelle\tPosition pour visite\tAbsence actuelle\tAbsence pour Visite\tCivilité\tNom\tPrenom\tDate\tType Examen\tDate Prochain examen\n";
		for (java.util.Enumeration<EOExamenMedical> e = infosAffichees().objectEnumerator();e.hasMoreElements();) {
			EOExamenMedical examen = e.nextElement();
			if (examen.adresse() != null) {
				texte = ajouterStringToString(examen.adresse().ville(), texte);
			} else {
				texte += "\t";
			}
			NSTimestamp today = new NSTimestamp();
			texte += examen.positionPourDate(today) + "\t" + examen.positionPourDate(examen.date()) + "\t" + 
			examen.congePourDate(today) + "\t" + examen.congePourDate(examen.date()) + "\t" +
			examen.individu().civilite() + "\t" + examen.individu().nomUsuel() + "\t" + examen.individu().prenom() + "\t" +
			examen.dateFormatee() + "\t";
			if (examen.libelleType() != null) {
				texte += examen.libelleType() + "\t";
			} else {
				texte += "\t";
			}
			if (examen.dateProchain() != null) {
				texte += examen.dateProchainFormatee();
			}
			texte += "\n";
		}

		return texte;
	}

	protected String titreAttente(int typeImpression) {
		if (typeImpression == InfoPourEditionMedicale.TYPE_CONVOCATION) {
			return "Impression des convocations pour les examens";
		} else if (typeImpression == InfoPourEditionMedicale.TYPE_LISTE) {
			return "Impression de la liste des agents convoqués à un examen";
		} else {
			return "Impression";
		}
	}

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils.medical;

import javax.swing.ListSelectionModel;

import org.cocktail.client.components.DialogueSimpleSansFetch;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.individu.GestionEtatCivil;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class GestionMedecins extends ModelePageComplete {
	public EODisplayGroup displayGroupAdresses;
	public EOTable listeAdresses;
	
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		if (currentMedecin() == null || modeCreation()) {
			displayGroupAdresses.setObjectArray(null);
		} else {
			displayGroupAdresses.setObjectArray(currentMedecin().adressesProfessionnelles());
		}
		super.displayGroupDidChangeSelection(group);
	}
	
	// Notifications
	public void getMedecin(NSNotification aNotif) {
		LogManager.logDetail("GestionMedecins - getMedecin");
		if (modeCreation() && aNotif.object() != null) {
			String message = null;
			EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			// vérifier si l'agent est déjà défini
			NSArray individus = (NSArray)displayGroup().displayedObjects().valueForKey("individu");
			if (individus.containsObject(individu)) {
				message = "Cette personne est déjà dans la liste des médecins";
				EODialogs.runErrorDialog("Erreur",message);
				annuler();
				displayGroupAdresses.setObjectArray(null);
			} else {
				setEdited(true);
				ajouterRelation(currentMedecin(), aNotif.object(), "individu");
				displayGroupAdresses.setObjectArray(currentMedecin().adressesProfessionnelles());
			}
		}
		updaterDisplayGroups();
		NSNotificationCenter.defaultCenter().removeObserver(this,"SelectionMedecin",null);
	}
	// Méthodes du controller DG
	protected void preparerFenetre() {
		super.preparerFenetre();
		if (listeAdresses != null) {
			GraphicUtilities.changerTaillePolice(listeAdresses,11);
			listeAdresses.table().getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}

	}
	public boolean peutValider() {
		return super.peutValider() && currentMedecin().individu() != null;
	}
	public boolean peutAjouterAdresse() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return boutonModificationAutorise() && agent.peutGererIndividu();
	}
	// Méthodes protégées
	protected void traitementsPourCreation() {
		afficherIndividus();
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR", message);
	}
	protected boolean conditionsOKPourFetch() {
		// pas de condition spécifique
		return true;
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntiteAvecRefresh(editingContext(), "MedecinTravail");
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce médecin ?";
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareAscending)));
	}
	protected boolean traitementsAvantValidation() {
		return currentMedecin().individu() != null;
	}
	protected boolean traitementsPourSuppression() {
		currentMedecin().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	protected void updaterDisplayGroups() {
		displayGroupAdresses.updateDisplayedObjects();
		super.updaterDisplayGroups();
	}
	protected void terminer() {
	}

	// Méthodes privées
	private void afficherIndividus() {
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getMedecin",new Class[] {NSNotification.class}),"SelectionMedecin",null);
		// afficher la liste des agents pour sélection d'un agent
		DialogueSimpleSansFetch controleur = new DialogueSimpleSansFetch(EOIndividu.ENTITY_NAME,"SelectionMedecin","nomUsuel","Sélection d'un individu",false,true,false,false,false);
		controleur.init();
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOIndividu.TEM_VALIDE_KEY + "='O'",null);
		controleur.setQualifier(qualifier);
		controleur.afficherFenetre();
	}
	private EOMedecinTravail currentMedecin() {
		return (EOMedecinTravail)displayGroup().selectedObject();
	}
}
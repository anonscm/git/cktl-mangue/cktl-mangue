/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils;

import java.awt.Cursor;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.JDialog;
import javax.swing.JFileChooser;

import org.cocktail.client.components.DialogueSimpleSansFetch;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EODif;

import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EOAssociation;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Permet la gestion des difs pour tous les agents ainsi que leur generation sur le serveur.
 * On ne peut preparer les difs que si il n'y a pas encore eu de difs cres pour l'annee selectionnee */

public class OutilDif extends ModelePageComplete {
	private Integer annee;
	private int typePopulation, typePersonnel;
	private String titreEdition,nom,prenom;
	private boolean estNomPatronymique;

	// Accesseurs
	public Integer annee() {
		return annee;
	}
	public void setAnnee(Integer annee) {
		this.annee = annee;
		// Changement d'année, vider le display group
		displayGroup().setObjectArray(null);
		updaterDisplayGroups();
	}
	public int typePersonnel() {
		return typePersonnel;
	}
	public void setTypePersonnel(int typePersonnel) {
		this.typePersonnel = typePersonnel;
	}
	public int typePopulation() {
		return typePopulation;
	}
	public void setTypePopulation(int typePopulation) {
		this.typePopulation = typePopulation;
	}
	public String titreEdition() {
		return titreEdition;
	}
	public void setTitreEdition(String aString) {
		titreEdition = aString;
	}
	public String nom() {
		return nom;
	}
	public void setNom(String aString) {
		nom = aString;
	}
	public String prenom() {
		return prenom;
	}
	public void setPrenom(String aString) {
		prenom = aString;
	}
	public boolean estNomPatronymique() {
		return estNomPatronymique;
	}
	public void setEstNomPatronymique(boolean aBool) {
		estNomPatronymique = aBool;
	}
	public Integer balance() {
		if (currentDif() != null) {
			int difference = 0;
			difference = currentDif().difCreditCumule().intValue();
			difference -= currentDif().difDebitCumule().intValue();
			return new Integer(difference);
		} else {
			return null;
		}
	}

	/**
	 * 
	 */
	public void preparerDifs() {

		if (nom() != null || prenom() != null) {
			EODialogs.runInformationDialog("", "Les noms et prénoms ne sont pas pris en compte lors de la préparation des difs");
		}

		TraitementDif controleur = new TraitementDif(editingContext(),annee(),new Integer(typePopulation()),new Integer(typePersonnel()));
		EODialogController.runControllerInNewDialog(controleur,"Traitement des difs");
		controleur.preparerPourTraitementAsynchrone();
		((JDialog)((EODialogController)controleur.supercontroller()).window()).setModal(true);	// pour le cas où d'autres dialogues masqueraient la fenêtre
		WindowListener[] listeners = ((EODialogController)controleur.supercontroller()).window().getWindowListeners();
		for (int i = 0; i < listeners.length;i++) {
			((EODialogController)controleur.supercontroller()).window().removeWindowListener((WindowListener)listeners[i]);
		}
		controleur.lancerTraitement();
		updaterDisplayGroups();
	}

	/**
	 * 
	 */
	public void afficher() {
		LogManager.logDetail("OutilDif - afficher");
		component().setCursor(new Cursor(Cursor.WAIT_CURSOR));
		if (annee != null) {
			NSArray difs = EODif.rechercherDifsPourAnneeTypePopulationTypePersonnel(editingContext(), 
					annee.intValue(),nom,prenom,typePopulation,typePersonnel,estNomPatronymique());
			LogManager.logDetail("nb difs affiches " + difs.count());
			displayGroup().setObjectArray(difs);
		} else {
			displayGroup().setObjectArray(null);
		}
		updaterDisplayGroups();

		component().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		LogManager.logDetail("OutilDif - finAfficher");

	}

	/**
	 * 
	 */
	public void afficherIndividus() {
		LogManager.logDetail("OutilDif - afficherIndividus");
		DialogueSimpleSansFetch controleur = new DialogueSimpleSansFetch(EOIndividu.ENTITY_NAME,"SelectionIndividu","nomUsuel","Sélection d'un individu",false,true,false,false,false);
		controleur.init();
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("temValide = 'O' AND personnels.noDossierPers <> nil",null);
		controleur.setQualifier(qualifier);
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getIndividu",new Class[] {NSNotification.class}),"SelectionIndividu",null);
		controleur.afficherFenetre();
	}
	public void exporter() {
		LogManager.logDetail(this.getClass().getName() + "exporter");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourExport(),file.getParent(),file.getName());
		}
	}
	public void imprimer() {
		LogManager.logDetail(this.getClass().getName() + "imprimer");
		InfoPourEditionRequete infoEdition = new InfoPourEditionRequete("Difs",titreEdition,DateCtrl.stringToDate("01/01/" + annee),DateCtrl.stringToDate("31/12/" + annee));
		Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,NSArray.class};
		Object[] parametres = new Object[]{infoEdition,Utilitaires.tableauDeGlobalIDs(displayGroup().displayedObjects(), editingContext())};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerDifs",classeParametres,parametres,"Difs","Impression des difs");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}

	}
	// Notifications
	public void getIndividu(NSNotification aNotif) {
		if (modificationEnCours()) {
			EOGlobalID globalID = (EOGlobalID)aNotif.object();
			// avant d'ajouter, vérifier que l'individu est bien titulaire ou contractuel pendant la période
			EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(globalID,editingContext());
			NSArray individus = (NSArray)displayGroup().displayedObjects().valueForKey("individu");
			if (individus.containsObject(individu)) {
				EODialogs.runErrorDialog("ERREUR", "Cet individu a déjà un dif défini");
			} else  {
				ajouterRelation(currentDif(),aNotif.object(),"individu");
				currentDif().calculerCredit();
				if (currentDif().difCreditCumule().doubleValue() == 0) {
					currentDif().removeObjectFromBothSidesOfRelationshipWithKey(currentDif().individu(), "individu");
					EODialogs.runErrorDialog("ERREUR",individu.identitePrenomFirst() + " n'est pas titulaire ou contractuel pendant cette période");
				}	
				updaterDisplayGroups();
			} 
		}
	}
	/** Synchronisation des difs si modifiees par un autre composant et concernent la meme annee. */
	public void synchroniserDifs(NSNotification aNotif) {
		LogManager.logDetail("Synchroniser les difs");
		if (TraitementDif.destinataireDoitSynchroniserDifsPourAnnee(this, annee(), aNotif)) {
			afficher();
		}
	}

	// Méthodes du controller DG
	public boolean peutAfficher() {
		return annee != null && annee.intValue() >= ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF;
	}
	public boolean peutPreparer() {
		return peutAfficher();
	}
	public boolean peutAjouter() {
		return modeSaisiePossible() && annee != null;
	}
	public boolean peutAfficherIndividus() {
		return modeCreation() && currentDif().dateDebut() != null && currentDif().dateFin() != null;
	}
	public boolean peutValider() {
		return super.peutValider() && currentDif() != null && currentDif().dateDebut() != null && currentDif().dateFin() != null;
	}
	public boolean peutImprimer() {
		return peutAjouter() && displayGroup().displayedObjects().count() > 0;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		typePopulation = EODif.TOUT_TYPE;
		typePersonnel = EODif.TOUT_TYPE;
		super.preparerFenetre();
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniserDifs", new Class[] { NSNotification.class }), TraitementDif.SYNCHRONISER_DIFS, null);
	}
	protected void traitementsPourCreation() {
		int anneePeriode = annee.intValue() -1 ;
		currentDif().initAvecDates(DateCtrl.stringToDate("01/01/" + anneePeriode), DateCtrl.stringToDate("31/12/" + anneePeriode));
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR", message);
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected NSArray fetcherObjets() {
		// pas d'affichage direct, il faut d'abord sélectionner l'année
		return null;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce dif ?";
	}
	/** Affichage des difs tries par nom d'individu */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareAscending)));
	}
	protected boolean traitementsAvantValidation() {
		return true;	// pas de traitement spécifique
	}
	/** Pour notifier que les difs ont change */
	protected void traitementsApresValidation() {
		TraitementDif.notifierSynchronisationDifsPourAnnee(this.getClass().getName(), annee());
		super.traitementsApresValidation();
	}
	protected boolean traitementsPourSuppression() {
		currentDif().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	/** Pour notifier que les difs ont change */
	protected void traitementsApresSuppression() {
		TraitementDif.notifierSynchronisationDifsPourAnnee(this.getClass().getName(), annee());
		super.traitementsApresSuppression();
	}
	protected void raffraichirAssociations() {
		super.raffraichirAssociations();
		for (java.util.Enumeration<EOAssociation> e = displayGroup().observingAssociations().objectEnumerator();e.hasMoreElements();) {
			EOAssociation association = e.nextElement();
			association.subjectChanged();
		}	
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EODif currentDif() {
		return (EODif)displayGroup().selectedObject();
	}
	private String textePourExport() {
		String texte =  "Nom\tPrénom\tDébut\tFin\tCrédit\tDébit\n";
		for (java.util.Enumeration<EODif> e = displayGroup().displayedObjects().objectEnumerator();e.hasMoreElements();) {
			EODif dif = e.nextElement();
			texte = texte + dif.individu().nomUsuel() + "\t" + dif.individu().prenom() + "\t" + dif.dateDebutFormatee() + "\t" + 
					dif.dateFinFormatee() + "\t" + dif.difCreditCumule() +  "\t";
			texte = texte + dif.difDebitCumule() ;
			texte = texte + "\n";
		}
		return texte;
	}


}
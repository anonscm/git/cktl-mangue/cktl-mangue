/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils;

import java.awt.Cursor;
import java.awt.Window;
import java.io.File;

import javax.swing.JFileChooser;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.IDureePourIndividu;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.StatutIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

public class GestionEtatPersonnels extends ModelePage {
	
	public EOView vuePeriode;
	private GestionPeriode controleurPeriode;
	private EOStructure currentStructure;
	private boolean inclureStructuresFilles;
	
	// Accesseurs
	public boolean inclureStructuresFilles() {
		return inclureStructuresFilles;
	}
	public void setInclureStructuresFilles(boolean inclureStructuresFilles) {
		this.inclureStructuresFilles = inclureStructuresFilles;
	}
	public String currentStructure() {
		if (currentStructure != null) {
			return currentStructure.llStructure();
		} else {
			return null;
		}
	}
	public Integer nbTotalElements() {
		if (displayGroup() != null) {
			int total = 0;
			EOIndividu currentIndividu = null;
			java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				StatutIndividu statut = (StatutIndividu)e.nextElement();
				if (currentIndividu == null || currentIndividu != statut.individu()) {
					total++;
					currentIndividu = statut.individu();
				}
			}
			return new Integer(total);
		} else {
			return new Integer(0);
		}
	}
	// Actions
	public void afficherStructures() {
		currentStructure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(editingContext());
		displayGroup().setObjectArray(null);
		displayGroup().updateDisplayedObjects();
		controllerDisplayGroup().redisplay();
	}
	public void rechercher() {
		Window activeWindow = EOApplication.sharedApplication().windowObserver().activeWindow();
		if (activeWindow != null) {
			activeWindow.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		} 
		LogManager.logInformation("Recherche du statut des agents");

		NSArray statutAgents = preparerEtats();
		displayGroup().setObjectArray(statutAgents);
		displayGroup().updateDisplayedObjects();
		controllerDisplayGroup().redisplay();
		if (activeWindow != null) {
			activeWindow.setCursor(Cursor.getDefaultCursor());
		} 
	}
	public void imprimer() {
		String titre = "Etats du personnel pour la structure " + currentStructure();
		if (inclureStructuresFilles && currentStructure.toutesStructuresFils().count() > 0) {
			titre += " et ses sous-structures";
		}
		InfoPourEditionRequete infoEdition = new InfoPourEditionRequete("EtatPersonnel",titre,controleurPeriode.dateDebut(),controleurPeriode.dateFin());
		Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,NSArray.class};
		Object[] parametres = new Object[]{infoEdition,displayGroup().displayedObjects()};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerEtatPersonnel",classeParametres,parametres,"EtatPersonnel_" + currentStructure.cStructure(),"Impression des états du personnel");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	public void exporter() {
		LogManager.logDetail(this.getClass().getName() + "exporter");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourExport(),file.getParent(),file.getName());
		}
	}
	// Notifications
	public void synchroniserDates(NSNotification aNotif) {
		controllerDisplayGroup().redisplay();
	}
	// Méthodes du controller DG
	/** on ne peut rechercher que si les dates d&eacute;but et fin sont fournies et que si la structure est s&eacute;lectionn&eacute;e */
	public boolean peutRechercher() {
		return controleurPeriode != null && controleurPeriode.debutPeriode() != null && controleurPeriode.finPeriode() != null && currentStructure != null;
	}

	public boolean peutImprimer() {
		return displayGroup().displayedObjects().count() > 0;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		//   préparer le contrôleur de période et l'afficher dans la vue des périodes en sélectionnant l'affichage des périodes par défaut
		controleurPeriode = new GestionPeriode(null,null,((ApplicationClient)EOApplication.sharedApplication()).periodeAbsenceParDefaut());
		controleurPeriode.init();
		GraphicUtilities.swaperView(vuePeriode,controleurPeriode.component());
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserDates", new Class[] {NSNotification.class}),
				GestionPeriode.NOTIFICATION_PERIODE_HAS_CHANGED,null);
	}
	protected boolean conditionsOKPourFetch() {
		// pas de condition
		return true;
	}

	protected NSArray fetcherObjets() {
		// les fetchs sont fais après sélection de la structure et dates
		return null;
	}
	protected void afficherExceptionValidation(String message) {
		// pas de validation
	}
	protected String messageConfirmationDestruction() {
		// pas de validation
		return null;
	}
	/** trie les donn&eacute;es par ordre de structure , de nom et de date d&eacute;but */
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("structureAffectation.llStructure", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.prenom", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
	}
	protected void traitementsApresValidation() {
		// pas de validation

	}
	protected boolean traitementsAvantValidation() {
		// pas de validation
		return true;
	}
	protected boolean traitementsPourSuppression() {
		// pas de validation
		return true;
	}
	protected void terminer() {
	}
	// Méthodes privées
	private NSArray preparerEtats() {
		// Rechercher les affectations pour la ou les structures sélectionnées
		java.util.Enumeration e = affectationsPourPeriode(controleurPeriode.dateDebut(), controleurPeriode.dateFin()).objectEnumerator();
		ListeEtats listeEtats = new ListeEtats();
		EOIndividu currentIndividu = null;
		EOStructure structureAffectation = null;
		NSTimestamp debutPeriode = null,finPeriode = null;
		while (e.hasMoreElements()) {
			EOAffectation affectation = (EOAffectation)e.nextElement();
			boolean shouldResetPeriodeEtStructure = false;
			if (currentIndividu == null || affectation.individu() != currentIndividu) {
				if (currentIndividu != null) {
					// préparer les états pour l'individu précédent
					preparerEtatsPourIndividu(listeEtats, currentIndividu, structureAffectation, debutPeriode, finPeriode);
				}
				// Réinitialiser
				currentIndividu = affectation.individu();
				shouldResetPeriodeEtStructure = true;
			} else {
				// même individu, vérifier si les structures d'affectations sont identiques et si il y a continuité dans les affectations
				if (structureAffectation == affectation.toStructureUlr()) {
					if (finPeriode != null && DateCtrl.isSameDay(DateCtrl.jourSuivant(finPeriode), affectation.dateDebut()))  {
						finPeriode = affectation.dateFin();
						// limiter à la date de fin sélectionnée si on la dépasse
						if (finPeriode == null || DateCtrl.isAfter(finPeriode,controleurPeriode.dateFin())) {
							finPeriode = controleurPeriode.dateFin();
						}
					} else if (affectation.dateFin() != null && DateCtrl.isSameDay(DateCtrl.jourSuivant(affectation.dateFin()),debutPeriode)) {
						debutPeriode = affectation.dateDebut();
					} else {
						// Discontinuité dans les périodes
						preparerEtatsPourIndividu(listeEtats, currentIndividu, structureAffectation, debutPeriode, finPeriode);
						shouldResetPeriodeEtStructure = true;
					}
				} else {
					// Changement de structure d'affectation
					preparerEtatsPourIndividu(listeEtats, currentIndividu, structureAffectation, debutPeriode, finPeriode);
					shouldResetPeriodeEtStructure = true;
				}		
			}
			if (shouldResetPeriodeEtStructure) {
				structureAffectation = affectation.toStructureUlr();
				if (DateCtrl.isBefore(affectation.dateDebut(), controleurPeriode.dateDebut())) {
					debutPeriode = controleurPeriode.dateDebut();
				} else {
					debutPeriode = affectation.dateDebut();
				}
				if (affectation.dateFin() == null || 
					(controleurPeriode.dateFin() != null && DateCtrl.isAfter(affectation.dateFin(), controleurPeriode.dateFin()))) {
					finPeriode = controleurPeriode.dateFin();
				} else {
					finPeriode = affectation.dateFin();
				}
			}
		}
		if (currentIndividu != null) {		// Correction DT 1275 23/02/09
			// préparer les états pour le dernier individu
			preparerEtatsPourIndividu(listeEtats, currentIndividu, structureAffectation, debutPeriode, finPeriode);
		}
		return listeEtats.statutsReordonnes();
	}
	private NSArray affectationsPourPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		EOQualifier qualifier = qualifierAffectation(debutPeriode,finPeriode);
		// Rechercher les affectations pour la ou les structures sélectionnées
		NSArray affectations = EOAffectation.rechercherAffectationsAvecCriteres(editingContext(), qualifier);
		// Classer les affectations par ordre d'individu et par ordre d'affectation croissante
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("individu.noIndividu", EOSortOrdering.CompareAscending));
		sorts.addObject(PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		affectations = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations, sorts);

		return affectations;
	}
	private void preparerEtatsPourIndividu(ListeEtats listeEtats,EOIndividu individu,EOStructure structureAffectation,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		// Rechercher les changements de position en activité, les contrats et les absences (i.e événements, comprend aussi les changements de position pas en activité) pour la période d'affectation
		// Tous ces objets implémentent l'interface InterfaceDureePourIndividu, on les regroupe tous dans un même tableau
		LogManager.logDetail("individu " + individu.identite() + ", structure d'affectation " + structureAffectation.llStructure());
		String message = "debut periode " + DateCtrl.dateToString(debutPeriode);
		if (finPeriode != null) {
			message += " - fin periode " + DateCtrl.dateToString(finPeriode);
		}
		LogManager.logDetail(message);
		NSMutableArray evenements = new NSMutableArray(EOChangementPosition.rechercherChangementsEnActivitePourIndividuEtPeriode(editingContext(), individu,debutPeriode, finPeriode));
		evenements.addObjectsFromArray(EOContrat.rechercherTousContratsPourIndividuEtPeriode(editingContext(), individu,debutPeriode,finPeriode));
		evenements.addObjectsFromArray(EOAbsences.rechercherAbsencesPourIndividuEtPeriode(editingContext(), individu,debutPeriode,finPeriode));
		if (evenements.count() > 0) {
			LogManager.logDetail(" nombre événements trouvés: " + evenements.count());
			// Les événements peuvent se chevaucher et s'emboîter les uns dans les autres, on est donc obligé
			// de gérer une pile d'événements (evenementsEnCours). Les événements en cours sont dépilés dès
			// qu'ils se terminent avant la date de début d'un nouveau segment
			NSMutableArray evenementsEnCours = new NSMutableArray();
			// les trier par ordre de date croissante
			EOSortOrdering.sortArrayUsingKeyOrderArray(evenements,new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
			for (int i = 0; i < evenements.count();i++) {
				IDureePourIndividu evenementCourant = (IDureePourIndividu)evenements.objectAtIndex(i);
				IDureePourIndividu evenementPrecedent = (IDureePourIndividu)evenementsEnCours.lastObject();
				if (evenementPrecedent == null) {	// Démarrage
					logDescriptionEvenement("evenement courant\n",evenementCourant);
					evenementsEnCours.addObject(evenementCourant);
				} else {
					logDescriptionEvenement("evenement courant\n",evenementCourant);
					logDescriptionEvenement("evenement precedent\n", evenementPrecedent);
					if (seTermineAvant(evenementPrecedent,evenementCourant)) {
						LogManager.logDetail("Se termine avant");
						logDescriptionEvenement("Ajout d'une entree puis depilement de\n", evenementPrecedent);
						// ajouter le statut précédent aux etats
						StatutIndividu statut = new StatutIndividu(evenementPrecedent,structureAffectation);
						listeEtats.ajouterStatut(statut);
						// dépiler l'événement précédent des éléments en cours
						evenementsEnCours.removeObject(evenementPrecedent);
						// dépiler tous les événements en cours qui se terminent avant ou pendant
						// on crée un statut pour ceux qui se terminent
						if (evenementsEnCours.count() > 0) {
							LogManager.logDetail("Parcours evenements precedents");
							java.util.Enumeration e1 = evenementsEnCours.reverseObjectEnumerator();
							while (e1.hasMoreElements()) {
								NSTimestamp dateDebut = DateCtrl.jourSuivant(evenementPrecedent.dateFin());
								evenementPrecedent = (IDureePourIndividu)e1.nextElement();	
								logDescriptionEvenement("evenement precedent\n", evenementPrecedent);
								if (seTermineAvant(evenementPrecedent,evenementCourant)) { 
									LogManager.logDetail("Se termine avant");
									//	on ajoute une entrée pour l'événement précédent si il se termine avant et que la nouvelle date début
									// n'est pas postérieure à sa date de fin
									if (evenementPrecedent.dateFin() == null || (dateDebut != null && DateCtrl.isAfter(evenementPrecedent.dateFin(), dateDebut))){
										logDescriptionEvenement("Ajout d'une entree pour\n", evenementPrecedent);
										statut = new StatutIndividu(evenementPrecedent,structureAffectation);
										if (dateDebut != null) {
											statut.setDateDebut(dateDebut);
										}
										listeEtats.ajouterStatut(statut);
									}
									LogManager.logDetail("Depilement evenement precedent");
									// Dans tous les cas on le dépile puisqu'il est terminé
									evenementsEnCours.removeObject(evenementPrecedent);
								} else if (seTerminePendant(evenementPrecedent,evenementCourant)) { 
									LogManager.logDetail("Se termine pendant");
									// on le dépile puisqu'il est terminé
									evenementsEnCours.removeObject(evenementPrecedent);
								} else {	
									LogManager.logDetail("Se termine apres");
									// créer sinon un statut pour le dernier élément qui continue
									// si la date de début est antérieure au jour précédent le début de l'élément courant
									// et la date de fin postérieure à la date de début
									// et mettre comme date de fin, le jour précédent de l'élément courant
									if (dateDebut != null && DateCtrl.isBefore(dateDebut, evenementCourant.dateDebut()) &&
										(evenementPrecedent.dateFin() == null || DateCtrl.isAfter(evenementPrecedent.dateFin(),dateDebut))) {
										logDescriptionEvenement("Ajout d'une entree pour\n", evenementPrecedent);
										statut = new StatutIndividu(evenementPrecedent,structureAffectation);
										if (dateDebut != null) {
											statut.setDateDebut(dateDebut);
										}
										statut.setDateFin(DateCtrl.jourPrecedent(evenementCourant.dateDebut()));
										listeEtats.ajouterStatut(statut);
									}
									break;
								}
							}
						}
						LogManager.logDetail("Fin parcours evenements precedents\n");
						// ajouter cet événement dans les événements en cours
						evenementsEnCours.addObject(evenementCourant);
					} else {
						LogManager.logDetail("Segments a cheval, création d'une entrée");
						// l'événement est à cheval sur l'événement précédent
						//	créer le statut pour l'événement précédent avec comme date fin le jour précédent le début de l'élément courant
						StatutIndividu statut = new StatutIndividu(evenementPrecedent,structureAffectation);
						statut.setDateFin(DateCtrl.jourPrecedent(evenementCourant.dateDebut()));
						listeEtats.ajouterStatut(statut);

						// dépiler tous les éléments en cours qui se terminent avant l'événement courant car ils ne sont plus à prendre en compte
						java.util.Enumeration e1 = evenementsEnCours.reverseObjectEnumerator();
						while (e1.hasMoreElements()) {
							IDureePourIndividu unEvenement = (IDureePourIndividu)e1.nextElement();
							if (seTermineAvant(unEvenement,evenementCourant)) {
								logDescriptionEvenement("Depilage de l'element\n", unEvenement);
								evenementsEnCours.removeObject(unEvenement);
							}
						}
						// ajouter l'événement à la pile des en cours
						evenementsEnCours.addObject(evenementCourant);
					}
				}
			}
			// traiter tous les événements en cours restant
			if (evenementsEnCours.count() > 0) {
				LogManager.logDetail("Nombre d'evenements restant dans la pile : " + evenementsEnCours.count());
				NSTimestamp dateDebut = null;
				if (listeEtats.nbElements() > 0) {
					StatutIndividu dernierStatut = listeEtats.dernierStatutPourIndividu(individu);
					if (dernierStatut != null && dernierStatut.dateFin() != null) {
						dateDebut = DateCtrl.jourSuivant(dernierStatut.dateFin());
						LogManager.logDetail("date debut :" + DateCtrl.dateToString(dateDebut));
					}
				} 
				// Dépiler les événements
				java.util.Enumeration e1 = evenementsEnCours.reverseObjectEnumerator();
				while (e1.hasMoreElements()) {
					IDureePourIndividu unEvenement = (IDureePourIndividu)e1.nextElement();
					logDescriptionEvenement("Element traite\n", unEvenement);
					// N'ajouter que si la date de fin est postérieure à la date de début
					if (dateDebut == null || unEvenement.dateFin() == null || DateCtrl.isBeforeEq(dateDebut, unEvenement.dateFin())) {
						logDescriptionEvenement("Ajout d'une entree pour\n", unEvenement);
						StatutIndividu statut = new StatutIndividu(unEvenement,structureAffectation);
						if (dateDebut != null) {
							statut.setDateDebut(dateDebut);
						}
						listeEtats.ajouterStatut(statut);
					}
					if (unEvenement.dateFin() != null) {
						dateDebut = DateCtrl.jourSuivant(unEvenement.dateFin());
					} else {
						break;	// on s'arrête sur le premier élément sans date de fin
					}
				}
			}	
		}
	}
	private EOQualifier qualifierAffectation(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray myQualifiers = new NSMutableArray();
		NSMutableArray structures = new NSMutableArray();
		if (inclureStructuresFilles()) {
			structures.addObjectsFromArray((NSArray)currentStructure.toutesStructuresFils().valueForKey("cStructure"));
		}
		structures.addObject(currentStructure.cStructure());
		EOQualifier qualifier = SuperFinder.construireORQualifier("toStructureUlr.cStructure",structures);
		myQualifiers.addObject(qualifier);

		qualifier =  SuperFinder.qualifierPourPeriode(EOAffectation.DATE_DEBUT_KEY, debutPeriode, EOAffectation.DATE_FIN_KEY, finPeriode);
		if (qualifier != null) {
			myQualifiers.addObject(qualifier);
		}
		return new EOAndQualifier(myQualifiers);
	}
	private boolean seTermineAvant(IDureePourIndividu evenement1,IDureePourIndividu evenement2) {
		return evenement1.dateFin() != null && DateCtrl.isBefore(evenement1.dateFin(),evenement2.dateDebut());
	}
	private boolean seTerminePendant(IDureePourIndividu evenement1,IDureePourIndividu evenement2) {
		return evenement2.dateFin() == null || (evenement1.dateFin() != null && DateCtrl.isBefore(evenement1.dateFin(),evenement2.dateFin()));
	}
	private String textePourExport() {
		String texte =  "Civilité\tNom\tPrénom\tStructure Affectation\tStatut\tDate début\tDate fin\n";
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			StatutIndividu statutIndividu = (StatutIndividu)e.nextElement();
			// formatter avec , comme séparateur décimal pour excel
			texte = texte + statutIndividu.individu().cCivilite() + "\t" + statutIndividu.individu().nomUsuel() + "\t" + statutIndividu.individu().prenom() + "\t" + 
			statutIndividu.structureAffectation().llStructure() + "\t" + statutIndividu.statut() + "\t" + statutIndividu.dateDebutFormatee() + "\t" + 
			statutIndividu.dateFinFormatee() + "\n";
		}
		return texte;
	}
	private void logDescriptionEvenement(String message, IDureePourIndividu evenement) {
		message += "type  " + evenement.typeEvenement() + ", debut :" + DateCtrl.dateToString(evenement.dateDebut());
		if (evenement.dateFin() != null) {
			message += " - fin : " + DateCtrl.dateToString(evenement.dateFin());
		}
		LogManager.logDetail(message);
	}
	private class ListeEtats {
		private NSMutableArray etats;

		public ListeEtats() {
			etats = new NSMutableArray();
		}
		public void ajouterStatut(StatutIndividu statut) {
			if (statut.dateFin() == null ||  DateCtrl.isAfter(statut.dateFin(), controleurPeriode.dateDebut())) {
				// on ne retient que les etats commençant après la date de début
				// on met comme date de début, celle de la période si la date de début est antérieure
				if (DateCtrl.isBefore(statut.dateDebut(), controleurPeriode.dateDebut())) {
					statut.setDateDebut(controleurPeriode.dateDebut());
				}
				etats.addObject(statut);
			}
		}
		public StatutIndividu dernierStatutPourIndividu(EOIndividu individu) {
			StatutIndividu statut =  (StatutIndividu)etats.lastObject();
			if (statut != null && statut.individu() == individu) {
				return statut;
			} else {
				return null;
			}
		}
		public int nbElements() {
			return etats.count();
		}
		public NSArray statutsReordonnes() {
			NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("individu.noIndividu", EOSortOrdering.CompareAscending));
			sorts.addObject(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending));
			EOSortOrdering.sortArrayUsingKeyOrderArray(etats, sorts);
			// Supprimer maintenant tous les etats qui sont de même type et contigues
			NSMutableArray nouveauxStatuts = new NSMutableArray();
			StatutIndividu statutCourant = null;
			java.util.Enumeration e = etats.objectEnumerator();
			while (e.hasMoreElements()) {
				StatutIndividu statut = (StatutIndividu)e.nextElement();
				if (statutCourant == null) { 	// démarrage
					statutCourant = statut;
				} else if (statut.individu() != statutCourant.individu() || statut.statut().equals(statutCourant.statut()) == false) {
					// Changement d'individu ou de statut, garder le statut courant
					nouveauxStatuts.addObject(statutCourant);
					statutCourant = statut;
				} else {
					// même type d'événement pour un même individu,
					// regrouper en un seul statut en prenant comme date de fin la date de fin du statut
					statutCourant.setDateFin(statut.dateFin());
				}
			}
			if (statutCourant != null) {
				nouveauxStatuts.addObject(statutCourant);
			}
			return nouveauxStatuts;
		}
	}
}

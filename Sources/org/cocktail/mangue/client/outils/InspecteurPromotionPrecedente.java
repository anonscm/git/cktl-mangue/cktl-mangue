/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.client.outils;

import java.awt.Window;

import javax.swing.JComboBox;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.carrieres.GestionPromotionIndividu;
import org.cocktail.mangue.client.outils_interface.InspecteurAvecDisplayGroup;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.mangue.individu.EOHistoPromotion;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

// 19/12/2010 - Adaptation Netbeans
public class InspecteurPromotionPrecedente extends InspecteurAvecDisplayGroup {
	public JComboBox popupType;
	public EOTable listeAffichage;
	private Integer anneePrecedente;
	private NSArray parametresPromotion;
	private boolean preparationEnCours;
	private final static int TYPE_TOUS = 0;
	private final static int TYPE_DOSSIER_REMPLIS = 1;
	private final static int TYPE_DOSSIER_TRANSMIS_MINISTERE = 2;
	private final static int TYPE_PROMUS = 3;
	private final static int TYPE_REFUSES = 4;

	public InspecteurPromotionPrecedente(EOEditingContext editingContext, Integer anneePrecedente,NSArray parametresPromotion) {
		super(editingContext);
		this.anneePrecedente = anneePrecedente;
		this.parametresPromotion = parametresPromotion;
	}
	public void setAnneePrecedente(Integer anneePrecedente) {
		this.anneePrecedente = anneePrecedente;
		displayGroup.setObjectArray(fetcherObjets());
		displayGroup.updateDisplayedObjects();
	}
	public void setParametresPromotion(NSArray parametresPromotion) {
		this.parametresPromotion = parametresPromotion;
		displayGroup.setObjectArray(fetcherObjets());
		displayGroup.updateDisplayedObjects();
	}
	public void connectionWasEstablished() {
		Window window = ((ApplicationClient)EOApplication.sharedApplication()).windowObserver().activeWindow();
		int positionHorizontale = window.getX() + window.getWidth() + 5;
		window().setLocation(positionHorizontale,window().getY());
		popupType.setSelectedIndex(TYPE_PROMUS);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniserPromotions", new Class[] { NSNotification.class }), GestionPromotionIndividu.SYNCHRONISER_PROMOTION, null);
	}
	public void connectionWasBroken() {
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}
	// Actions
	public void popupTypeHasChanged() {
		if (popupType == null || preparationEnCours) {
			return;
		}

		int type = popupType.getSelectedIndex();
		NSMutableArray qualifiers = new NSMutableArray();
		if (type == TYPE_TOUS || type == TYPE_DOSSIER_REMPLIS) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("hproStatut <> %@",new NSArray(EOHistoPromotion.STATUT_SUPPRIME)));
		} else if (type == TYPE_DOSSIER_TRANSMIS_MINISTERE) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("hproStatut = %@",new NSArray(EOHistoPromotion.STATUT_TRANSMIS_MINISTERE)));
		} else if (type == TYPE_PROMUS){
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("hproStatut = %@",new NSArray(EOHistoPromotion.STATUT_PROMU)));
		} else if (type == TYPE_REFUSES){
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("hproStatut = %@",new NSArray(EOHistoPromotion.STATUT_NEGATIF)));
		}
		if (type == TYPE_DOSSIER_REMPLIS || type == TYPE_DOSSIER_TRANSMIS_MINISTERE || type == TYPE_PROMUS || type == TYPE_REFUSES) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("hproRempli = %@",new NSArray(CocktailConstantes.VRAI)));
		}
		displayGroup.setQualifier(new EOAndQualifier(qualifiers));
		displayGroup.updateDisplayedObjects();
	}
	// Notifications
	/** Synchronisation des promotions si elles ont &eacute;t&eacute; modifi&eacute;es pour un individud par l'utilisateur */
	public void synchroniserPromotions(NSNotification aNotif) {
		Integer annee = (Integer)aNotif.object();
		if (anneePrecedente != null && anneePrecedente.intValue() == annee.intValue()) {
			displayGroup.setObjectArray(fetcherObjets());
			displayGroup.updateDisplayedObjects();
		}
	}
	// Méthodes protégées
	public NSArray fetcherObjets() {
		if (anneePrecedente == null || parametresPromotion == null || parametresPromotion.count() == 0 ) {
			return null;
		}
		return EOHistoPromotion.rechercherPromotionsReellesPourParametresEtAnnee(editingContext,parametresPromotion,anneePrecedente);
	}
	public void init() {
		preparationEnCours = true;
		// 19/12/2010 - Adaptation Netbeans
		EOArchive.loadArchiveNamed("InspecteurPromotionPrecedente",this,"org.cocktail.mangue.client.outils.interfaces",this.disposableRegistry());
		if (listeAffichage != null) {
			GraphicUtilities.changerTaillePolice(listeAffichage,11);
			GraphicUtilities.rendreNonEditable(listeAffichage);
		}
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.prenom", EOSortOrdering.CompareAscending));
		displayGroup.setSortOrderings(sorts);
		preparationEnCours = false;
	}

}

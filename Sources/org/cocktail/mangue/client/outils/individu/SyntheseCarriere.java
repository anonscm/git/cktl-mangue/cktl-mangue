/*
 * Created on 22 sept. 2005
 *
 * Gestion de la synthèse de carrière d'un agent
 * Travaille avec un objet qui n'est pas un objet de la base mais est constitué à la volée (FicheSynthese)
 * Window - Preferences - Java - Code Style - Code Templates
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils.individu;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.carrieres.GestionChangementsPosition;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.occupations.GestionAvecCarriereContrat;
import org.cocktail.mangue.client.outils_interface.ModelePageAvecIndividu;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.FicheSynthese;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author christine<BR>
 *
 * Synthese de la carriere d'un agent
 */
// 20/12/2010 - Adaptation Netbeans (suppression de preparerFenetre)
public class SyntheseCarriere extends ModelePageAvecIndividu {
	public SyntheseCarriere() {
		super();
	}
	public void initialiser(Number individuID) {
		super.initialiser(individuID,false,false);
	}
	// actions
	public void imprimer() {
		try {
			Class[] classeParametres =  new Class[] {EOGlobalID.class};
			Object[] parametres = new Object[]{editingContext().globalIDForObject(currentIndividu())};
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerSyntheseCarriere",classeParametres,parametres,"SyntheseCarriere_" + currentIndividu().noIndividu(),"Impression de la synthèse");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	// Notifications
	public void synchroniser(NSNotification aNotif) {
		displayGroup().setObjectArray(fetcherObjets());
		updaterDisplayGroups();
	}
	// méthodes du controller dg
	public boolean peutImprimer() {
		return currentIndividu() != null;
	}
	// méthodes protégées
	protected NSArray fetcherObjets() {
		NSMutableArray arraySynthese = new NSMutableArray();
		// pour ramener à une date sans prendre en compte les heures
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayCarrieres(editingContext(),currentIndividu(),today));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayContrats(editingContext(),currentIndividu(),today));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext(),currentIndividu(),today,"Stage","STA"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext(),currentIndividu(),today,"CessProgActivite","CPA"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext(),currentIndividu(),today,"CgFinActivite","CFA"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext(),currentIndividu(),today,"Cld","CLD"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext(),currentIndividu(),today,"Clm","CLM"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext(),currentIndividu(),today,"CongeFormation","CFP"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayAbsences(editingContext(),currentIndividu(),today,"Mad","MAD"));
		arraySynthese.addObjectsFromArray(FicheSynthese.getArrayPasseSyntheseCarriere(editingContext(),currentIndividu(),today));
		return arraySynthese;
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("periodeDeb",EOSortOrdering.CompareDescending)));
	}
	protected void traitementsPourCreation() {
		// pas de création

	}
	protected boolean traitementsPourSuppression() {
		// pas de suppresson
		return true;
	}
	protected String messageConfirmationDestruction() {
		// pas de suppression
		return "";
	}
	/** methode a surcharger pour faire des traitements avant l'enregistrement des donnees dans la base
	 * retourne true si on peut enregistrer */
	protected boolean traitementsAvantValidation() {
		// pas de validation
		return true;
	}
	protected void terminer() {
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniser", new Class[] { NSNotification.class }),GestionChangementsPosition.CHANGER_POSITION, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniser", new Class[] { NSNotification.class }),GestionAvecCarriereContrat.RAFFRAICHIR_CC, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniser", new Class[] { NSNotification.class }),ModelePage.SYNCHRONISER, null);	
	}

}

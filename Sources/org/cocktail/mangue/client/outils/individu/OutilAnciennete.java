/*
 * Created on 21 sept. 2005
 *
 * Calcule l'ancienneté
 * Travaille avec un objet qui n'est pas un objet de la base mais est constitué à la volée (FicheAnciennete)
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils.individu;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.outils_interface.ModelePageAvecIndividu;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.FicheAnciennete;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author christine<BR>
 *
 * Calcule et affiche l'anciennet&eacute; d'un agent
 */
// 19/12/2010 - Adaptation Netbeans (suppression de preparerFenetre)
public class OutilAnciennete extends ModelePageAvecIndividu {
	private NSTimestamp dateReference;


	public OutilAnciennete() {
		super();
	}
	public void initialiser(Number individuID) {
		super.initialiser(individuID,false,false);
	}
	// accesseurs
	public NSTimestamp dateReference() {
		return dateReference;
	}
	// Actions
	// CALCUL ANCIENNETE
	public void calculerAnciennete() {

		CRICursor.setWaitCursor(component());
		
		boolean continuer = true;
		NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext(),currentIndividu(),null,null);
		if (changements.count() > 1) {
			boolean aChevauchement = false;
			// Regarder si il y a un chevauchement des changements de position. Ils sont triés par ordre croissant
			NSTimestamp dateFin = ((EOChangementPosition)changements.objectAtIndex(0)).dateFin();
			for (int i = 1; i < changements.count();i++) {
				EOChangementPosition changement = (EOChangementPosition)changements.objectAtIndex(i);
				if (dateFin == null) {
					aChevauchement = true;
				} else {
					if (DateCtrl.isBefore(changement.dateDebut(),dateFin) || (changement.dateFin() != null && DateCtrl.isBefore(changement.dateFin(), dateFin))) {
						aChevauchement = true;
					} else {
						dateFin = changement.dateFin();
					}
				}
			}
		}
		if (continuer) {
			displayGroup().setObjectArray(FicheAnciennete.calculerAnciennete(editingContext(), currentIndividu(), dateReference()));
			updaterDisplayGroups();
		}
		CRICursor.setDefaultCursor(component());
	}	
	public void imprimer()  {
		imprimer(true);
	}
	public void imprimerSynthese() {
		imprimer(false);
	}
	// méthodes du controller dg
	public boolean peutImprimer() {
		return displayGroup().displayedObjects().count() > 0;
	}
	public String dateReferenceFormatee() {
		if (dateReference == null) {
			dateReference = new NSTimestamp();
		}
		return DateCtrl.dateToString(dateReference);
	}
	public void setDateReferenceFormatee(String uneDate) {
		String myDate = DateCtrl.dateCompletion((String)uneDate);
		if (myDate.equals("")) {
			dateReference = new NSTimestamp();
		} else {
			dateReference = DateCtrl.stringToDate(myDate);
		} 
	}

	/** calcule l'anciennete totale en services auxiliaires */
	public String ancienneteServicesAuxiliaires() {
		return FicheAnciennete.calculerTotal(displayGroup().displayedObjects(),FicheAnciennete.ANCIENNETE_AUXILIAIRE);
	}
	
	/** calcule l'anciennete totale en services auxiliaires */
	public String ancienneteServicesValides() {
		return FicheAnciennete.calculerTotal(displayGroup().displayedObjects(),FicheAnciennete.ANCIENNETE_VALIDES);
	}

	
	/** calcule l'anciennete totale comme titulaire */
	public String ancienneteTitulaire() {
		return FicheAnciennete.calculerTotal(displayGroup().displayedObjects(),FicheAnciennete.ANCIENNETE_TITULAIRE);

	}
	/** calcule l'anciennete totale en service effectif */
	public String ancienneteService() {
		return FicheAnciennete.calculerTotal(displayGroup().displayedObjects(),FicheAnciennete.TOUTE_ANCIENNETE);
	}
	/** calcule l'anciennete generale */
	public String ancienneteGenerale() {
		return FicheAnciennete.ancienneteGenerale(displayGroup().displayedObjects());
	}
	// méthodes protégées
	protected NSArray fetcherObjets() {
		// on ne retourne rien, voir si on le fait pour la date du jour
		return null;
	}
	protected void parametrerDisplayGroup() {
		// trier par date décroissante
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("periodeDeb",EOSortOrdering.CompareDescending)));
	}
	protected void traitementsPourCreation() {
		// pas de création
	}
	protected boolean traitementsPourSuppression() {
		// pas de suppression
		return true;
	}
	protected String messageConfirmationDestruction() {
		// pas de suppression
		return null;
	}
	/** methode a surcharger pour faire des traitements avant l'enregistrement des donnees dans la base
	 * retourne true si on peut enregistrer */
	protected boolean traitementsAvantValidation() {
		// pas de validation
		return true;
	}
	protected void terminer() {
	}
	// méthodes privees
	private void imprimer(boolean estAncienneteComplete) {

		Class[] classeParametres =  new Class[] {EOGlobalID.class,NSTimestamp.class,Boolean.class};
		Object[] parametres = new Object[]{editingContext().globalIDForObject(currentIndividu()),dateReference(),new Boolean(estAncienneteComplete)};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerAnciennete",classeParametres,parametres,"FicheAnciennete_" + currentIndividu().noIndividu(),"Impression de la fiche d'ancienneté");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}

	}
}

package org.cocktail.mangue.client.heberge;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.contrats.SaisieEmployeurCtrl;
import org.cocktail.mangue.client.gui.contrats.SaisieContratHebergeView;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.client.select.SaisieIndividuCtrl;
import org.cocktail.mangue.client.select.StructureSelectCtrl;
import org.cocktail.mangue.client.select.TypeContratArbreSelectCtrl;
import org.cocktail.mangue.client.select.TypeContratListeSelectCtrl;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.NomenclatureAvecDate;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCivilite;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieContratHebergeCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieContratHebergeCtrl sharedInstance;
	private EOEditingContext edc;
	private SaisieContratHebergeView myView;

	private EOIndividu currentIndividu;
	private EOContratHeberges currentHeberge;
	private EOCorps currentCorps;
	private EOGrade	currentGrade;
	private EORne	currentUai;
	private EOTypeContratTravail	currentTypeContrat;
	private EOStructure				currentStructure;
	private EOStructure				currentStructureOrigine;

    private TypeContratArbreSelectCtrl     myTypeContratArbreSelectCtrl;
    private TypeContratListeSelectCtrl         myTypeContratListeSelectCtrl;

	public SaisieContratHebergeCtrl(EOEditingContext edc) {

		setEdc(edc);
		myView = new SaisieContratHebergeView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		myView.getBtnGetCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectCorps();}}
		);
		myView.getBtnGetGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectGrade();}}
		);
		myView.getBtnDelCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delCorps();}}
		);
		myView.getBtnDelGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delGrade();}}
		);
		myView.getBtnGetTypeContrat().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectTypeContrat();}}
		);
		myView.getBtnDelTypeContrat().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delTypeContrat();}}
		);
		myView.getBtnGetStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectStructureHebergement();}}
		);
		myView.getBtnDelStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delStructure();}}
		);
		myView.getBtnGetUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectUai();}}
		);
		myView.getBtnDelUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delUai();}}
		);
		myView.getBtnAddStructureOrigine().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectStructureOrigine();}}
		);
		myView.getBtnDelStructureOrigine().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delStructureOrigine();}}
		);

		CocktailUtilities.initTextField(myView.getTfUai(), false, false);
		CocktailUtilities.initTextField(myView.getTfStructure(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeContrat(), false, false);
		CocktailUtilities.initTextField(myView.getTfCorps(), false, false);
		CocktailUtilities.initTextField(myView.getTfGrade(), false, false);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));

		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));

		myView.setListeCivilites(EOCivilite.find(edc));

		myView.getBtnIdentite().setVisible(false);

		updateInterface();

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieContratHebergeCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieContratHebergeCtrl(editingContext);
		return sharedInstance;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		if (currentIndividu != null) {
			String identite = currentIndividu.cCivilite() + " " +  currentIndividu.identitePrenomFirst();
			identite += "  ( Sécu : " + currentIndividu.indNoInsee();
			if (currentIndividu.personnel() != null && currentIndividu.personnel().noMatricule() != null)
				identite += "  , Matricule : " + currentIndividu.personnel().noMatricule();
			identite += " )";

			myView.getPopupCivilite().setSelectedItem(currentIndividu.cCivilite());
			CocktailUtilities.setTextToField(myView.getTfNom(), currentIndividu.nomUsuel());
			CocktailUtilities.setTextToField(myView.getTfPrenom(), currentIndividu.prenom());

		}
	}

	public EOContratHeberges getCurrentHeberge() {
		return currentHeberge;
	}

	public void setCurrentHeberge(EOContratHeberges currentHeberge) {
		this.currentHeberge = currentHeberge;
		updateDatas();
	}

	public EOCorps getCurrentCorps() {
		return currentCorps;
	}
	public void setCurrentCorps(EOCorps currentCorps) {
		this.currentCorps = currentCorps;
		myView.getTfCorps().setText("");
		if (currentCorps != null) {
			CocktailUtilities.setTextToField(myView.getTfCorps(), currentCorps.llCorps());
		}
		updateInterface();
	}

	public EOGrade getCurrentGrade() {
		return currentGrade;
	}
	public void setCurrentGrade(EOGrade currentGrade) {
		this.currentGrade = currentGrade;
		CocktailUtilities.viderTextField(myView.getTfGrade());
		if (currentGrade != null) {
			CocktailUtilities.setTextToField(myView.getTfGrade(), currentGrade.llGrade());
			if (getCurrentCorps() == null)
				setCurrentCorps(currentGrade.toCorps());
			updateInterface();
		}
	}

	public EORne getCurrentUai() {
		return currentUai;
	}

	public void setCurrentUai(EORne currentUai) {
		this.currentUai = currentUai;
		myView.getTfUai().setText("");
		if (currentUai != null) {
			CocktailUtilities.setTextToField(myView.getTfUai(), currentUai.libelleLong());
		}
		updateInterface();
	}

	public EOTypeContratTravail getCurrentTypeContrat() {
		return currentTypeContrat;
	}

	public void setCurrentTypeContrat(EOTypeContratTravail currentTypeContrat) {
		this.currentTypeContrat = currentTypeContrat;
		CocktailUtilities.viderTextField(myView.getTfTypeContrat());
		if (currentTypeContrat != null) {
			CocktailUtilities.setTextToField(myView.getTfTypeContrat(), currentTypeContrat.libelleLong());
		}
		updateInterface();
	}

	public EOStructure getCurrentStructure() {
		return currentStructure;
	}

	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
		myView.getTfStructure().setText("");
		if (currentStructure != null) {
			CocktailUtilities.setTextToField(myView.getTfStructure(), currentStructure.llStructure());
		}
		updateInterface();
	}


	public EOStructure getCurrentStructureOrigine() {
		return currentStructureOrigine;
	}

	public void setCurrentStructureOrigine(EOStructure currentStructureOrigine) {
		this.currentStructureOrigine = currentStructureOrigine;
		myView.getTfStructureOrigine().setText("");
		if (currentStructureOrigine != null) {
			CocktailUtilities.setTextToField(myView.getTfStructureOrigine(), currentStructureOrigine.llStructure());
		}
		updateInterface();
	}

	private void clearTextFields()	{

	}

	/**
	 * 
	 * @param individu
	 * @return
	 */
	public EOContratHeberges ajouter(EOIndividu individu)	{

		myView.setTitle("Contrat HEBERGE - AJOUT - ");

		EOIndividu selectedIndividu = null;
		
		if (individu == null) {
			//selectedIndividu = IndividuSelectCtrl.sharedInstance(ec).getIndividu();
			selectedIndividu = SaisieIndividuCtrl.sharedInstance(getEdc()).getIndividu(true,true);
		}
		else {
			selectedIndividu = individu;
		}

		if (selectedIndividu != null) {

			if (selectedIndividu.dNaissance() == null) {
				selectedIndividu.setIndQualite("HEBERGE");
				selectedIndividu.setDNaissance(DateCtrl.stringToDate("01/01/1900"));
			}

			if (selectedIndividu.indNoInsee() == null && selectedIndividu.indNoInseeProv() == null) {
				selectedIndividu.setIndNoInseeProv("999999999999");
				selectedIndividu.setEstNoInseeProvisoire(true);
			}

			setCurrentIndividu(selectedIndividu);
			setCurrentHeberge(EOContratHeberges.creer(getEdc(), getCurrentIndividu()));

			myView.setVisible(true);	// Lancement en mode MODAL

			return getCurrentHeberge();
		}

		return null;
	}

	/**
	 * 
	 * @param element
	 * @return
	 */
	public EOContratHeberges renouveler(EOContratHeberges contrat) {

		clearTextFields();

		setCurrentHeberge(EOContratHeberges.renouveler(getEdc(), contrat));
		updateDatas();

		updateInterface();
		myView.setVisible(true);		

		return getCurrentHeberge();
	}

	
	/**
	 * 
	 * @param estValidation
	 */
	private void creerPersonnel(EOIndividu individu) {

		EOPersonnel personnel = individu.personnel();
		if (personnel == null) {	
			personnel = new EOPersonnel();
			personnel.initPersonnel(individu.noIndividu());
			personnel.setToIndividuRelationship(individu);
			getEdc().insertObject(personnel);
			Number numeroMatricule = SuperFinder.numeroSequenceOracle(getEdc(), "SeqMatriculePersonnel", false);
			personnel.setNoMatricule(numeroMatricule.toString());
		}
		// charger les infos sur les classes pour éviter les boucles ultérieures dans les appels au serveur
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(EOIndividuIdentite.ENTITY_NAME);
		EOClassDescription.registerClassDescription(classDescription,EOIndividuIdentite.class);
	}

	
	/**
	 * 
	 * @param heberge
	 * @return
	 */
	public boolean modifier(EOContratHeberges heberge) {

		myView.setTitle("Contrat HEBERGE - MODIFICATION");

		setCurrentHeberge(heberge);
		setCurrentIndividu(getCurrentHeberge().individu());

		myView.setVisible(true);

		return getCurrentHeberge() != null;

	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearTextFields();

		if (getCurrentHeberge() != null) {

			setCurrentUai(getCurrentHeberge().toUai());
			setCurrentCorps(getCurrentHeberge().toCorps());
			setCurrentGrade(getCurrentHeberge().toGrade());
			setCurrentTypeContrat(getCurrentHeberge().typeContratTravail());
			setCurrentStructure(getCurrentHeberge().toStructureHebergement());
			setCurrentStructureOrigine(getCurrentHeberge().toStructureOrigine());

			CocktailUtilities.setNumberToField(myView.getTfHeures(),getCurrentHeberge().ctrhHeure());
			CocktailUtilities.setNumberToField(myView.getTfIndice(),getCurrentHeberge().ctrhInm());

			CocktailUtilities.setDateToField(myView.getTfDateDebut(),getCurrentHeberge().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(),getCurrentHeberge().dateFin());

			CocktailUtilities.setTextToField(myView.getTfDetailOrigine(),getCurrentHeberge().detailOrigine());
			CocktailUtilities.setTextToArea(myView.getTaCommentaires(),getCurrentHeberge().commentaire());
		}

		updateInterface();

	}

	/**
	 * 
	 * @throws ValidationException
	 */
	private void traitementsAvantValidation() throws ValidationException {

		creerPersonnel(getCurrentIndividu());

	}
	
	/**
	 * 
	 * @throws ValidationException
	 */
	private void traitementsApresValidation() throws ValidationException {
	}

	/**
	 * 
	 */
	private void valider() {

		try {

			getCurrentHeberge().setIndividuRelationship(getCurrentIndividu());
			getCurrentHeberge().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
			getCurrentHeberge().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));

			getCurrentHeberge().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTaCommentaires()));
			getCurrentHeberge().setDetailOrigine(CocktailUtilities.getTextFromField(myView.getTfDetailOrigine()));

			getCurrentHeberge().setToUaiRelationship(getCurrentUai());
			getCurrentHeberge().setToCorpsRelationship(getCurrentCorps());
			getCurrentHeberge().setToGradeRelationship(getCurrentGrade());
			getCurrentHeberge().setTypeContratTravailRelationship(getCurrentTypeContrat());
			getCurrentHeberge().setToStructureHebergementRelationship(getCurrentStructure());
			getCurrentHeberge().setToStructureOrigineRelationship(getCurrentStructureOrigine());

			getCurrentHeberge().setCtrhInm(CocktailUtilities.getIntegerFromField(myView.getTfIndice()));
			getCurrentHeberge().setCtrhHeure(CocktailUtilities.getIntegerFromField(myView.getTfHeures()));

			traitementsAvantValidation();

			getEdc().saveChanges();

			traitementsApresValidation();

			myView.setVisible(false);

		}
		catch (ValidationException ex) {
			EODialogs.runInformationDialog("ERREUR",ex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void selectCorps() {

		CRICursor.setWaitCursor(myView);
		EOCorps corps = CorpsSelectCtrl.sharedInstance(getEdc()).getCorps();

		if (corps != null)	{
			setCurrentCorps(corps);
		}
		CRICursor.setDefaultCursor(myView);
	}

	private void delCorps() {
		setCurrentCorps(null);
	}

	private void selectGrade() {
		EOGrade grade = GradeSelectCtrl.sharedInstance(getEdc()).getGradePourCorps(getCurrentCorps(), CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		if (grade != null) {
			setCurrentGrade(grade);
		}
	}
	private void delGrade() {
		setCurrentGrade(null);
	}

	
	/**
	 * 
	 */
	private void selectStructureOrigine() {
		EOStructure newStructure = SaisieEmployeurCtrl.sharedInstance(getEdc()).ajouter();
		if (newStructure != null) {				
			setCurrentStructureOrigine(newStructure);
			updateInterface();
		}
	}
	private void delStructureOrigine() {
		setCurrentStructureOrigine(null);
	}
	
	private void annuler() {
		getEdc().revert();
		setCurrentHeberge(null);
		myView.setVisible(false);
	}


	/**
	 * 
	 */
	private void updateInterface() {

		myView.getPopupCivilite().setEnabled(false);
		CocktailUtilities.initTextField(myView.getTfNom(), false, false);
		CocktailUtilities.initTextField(myView.getTfPrenom(), false, false);

		myView.getBtnDelCorps().setEnabled(getCurrentCorps() != null);
		myView.getBtnDelGrade().setEnabled(getCurrentGrade() != null);
		myView.getBtnDelUai().setEnabled(getCurrentUai() != null);
		myView.getBtnDelTypeContrat().setEnabled(getCurrentTypeContrat() != null);
		myView.getBtnDelStructure().setEnabled(getCurrentStructure() != null);
		myView.getBtnDelStructureOrigine().setEnabled(getCurrentStructureOrigine() != null);

	}

    /**
     * 
     */
    private void selectTypeContrat() {

        CRICursor.setWaitCursor(myView);

        EOTypeContratTravail typeContrat = null;

        NSTimestamp myDateFin = CocktailUtilities.getDateFromField(myView.getTfDateFin());
        
        EOQualifier qualifier = EOTypeContratTravail.getQualifierVacataires(false);

        // Est t on dans le cadre de la nouvelle nomenclature des contrats
        if (myDateFin == null || 
                DateCtrl.isAfterEq(myDateFin, DateCtrl.stringToDate(ManGUEConstantes.NOMENCLATURE_CONTRATS))) {

            if (myTypeContratArbreSelectCtrl == null) {
                myTypeContratArbreSelectCtrl = new TypeContratArbreSelectCtrl(getEdc());
            }
            if (myTypeContratListeSelectCtrl == null) {
                myTypeContratListeSelectCtrl = new TypeContratListeSelectCtrl(getEdc());
            }

            String typeAffichage = EOGrhumParametres.getValueParam(ManGUEConstantes.PARAM_KEY_AFF_TYPE_CONTRAT);

            if (typeAffichage.equals("A")) {
                typeContrat = myTypeContratArbreSelectCtrl.getTypeContrat(myDateFin, qualifier, false);
            } else {
                typeContrat = (EOTypeContratTravail) myTypeContratListeSelectCtrl.getObject(EOTypeContratTravail.findForSelection(getEdc(), myDateFin));            
            }

        } else {
            
            NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
            
            qualifiers.addObject(EOTypeContratTravail.getQualifierVisible(true));
            qualifiers.addObject(EOTypeContratTravail.getQualifierVacataires(false));
            qualifiers.addObject(NomenclatureAvecDate.qualifierPourPeriode(myDateFin, myDateFin));
            NSArray<INomenclature> typesContrat = NomenclatureFinder.findWithQualifier(getEdc(), EOTypeContratTravail.ENTITY_NAME, new EOAndQualifier(qualifiers));
            
            typeContrat = (EOTypeContratTravail) TypeContratListeSelectCtrl.sharedInstance(getEdc()).getObject(typesContrat);
            
        }

        if (typeContrat != null) {
            setCurrentTypeContrat(typeContrat);
        }

        updateInterface();
        CRICursor.setDefaultCursor(myView);

    }

    
    
	private void selectStructureHebergement() {
		EOStructure structureHebergement = StructureSelectCtrl.sharedInstance(getEdc()).getStructureService();		
		if (structureHebergement != null) {
			setCurrentStructure(structureHebergement);
		}
	}
	private void selectUai() {
		EORne rne = (EORne)UAISelectCtrl.sharedInstance(getEdc()).getObject();
		if (rne != null) {
			setCurrentUai(rne);
		}
	}

	private void delUai() {
		setCurrentUai(null);
	}
	private void delTypeContrat() {
		setCurrentTypeContrat(null);
	}
	private void delStructure() {
		setCurrentStructure(null);
	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateInterface();
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}

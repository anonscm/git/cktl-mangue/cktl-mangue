package org.cocktail.mangue.client.heberge;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ConsoleTraitementCtrl;
import org.cocktail.mangue.client.gui.ListeDifsView;
import org.cocktail.mangue.client.individu.infoscomp.CDifCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EODif;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ListeDifsCtrl {

	private static ListeDifsCtrl 	sharedInstance;
	private static Boolean 			MODE_MODAL = Boolean.FALSE;
	private EOEditingContext		edc;
	private ListeDifsView 			myView;
	private EODisplayGroup 			eod;

	private ListenerDif 			listenerDif = new ListenerDif();
	private boolean 				isLocked;
	private boolean 				traitementServeurEnCours, preparationEnCours = false;

	private EODif					currentDif;
	private EOAgentPersonnel		currentUtilisateur;
	private ConsoleTraitementCtrl 		myConsole;

	public ListeDifsCtrl(EOEditingContext edc) {

		setEdc(edc);

		myConsole = new ConsoleTraitementCtrl(edc);
		myConsole.setCtrlParent(this, "Traitement des DIFs ...");

		eod = new EODisplayGroup();
		myView = new ListeDifsView(null, MODE_MODAL.booleanValue(), eod);

		myView.getBtnRechercher().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {rechercher();}}
				);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporter();}}
				);
		myView.getBtnCalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {calculer();}}
				);
		myView.getBtnToutCalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {toutCalculer();}}
				);

		myView.getMyEOTable().addListener(listenerDif);

		CocktailUtilities.initPopupAvecListe(myView.getPopupAnnees(), CocktailUtilities.getListeAnnees(new Integer(DateCtrl.getCurrentYear()), new Integer(ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF)), true);
		myView.getPopupAnnees().setSelectedItem(DateCtrl.getCurrentYear());

		myView.getTfFiltreNom().addActionListener(new MyActionListener());
		myView.getPopupAnnees().addActionListener(new MyActionListener());

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		myView.getBtnExporter().setEnabled(false);
		updateInterface();
	}

	public static ListeDifsCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ListeDifsCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 */
	public void open()	{

		if (!currentUtilisateur.peutGererHeberges()) {
			EODialogs.runErrorDialog("ERREUR", "Vous n'avez pas les droits de gestion des hébergés !");
			return;
		}

		CRICursor.setWaitCursor(myView);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	public EODif getCurrentDif() {
		return currentDif;
	}

	public void setCurrentDif(EODif currentDif) {
		this.currentDif = currentDif;
	}

	public void toFront() {
		myView.toFront();
	}

	private void actualiser()	{

		CRICursor.setWaitCursor(myView);
		rechercher();
		CRICursor.setDefaultCursor(myView);

	}

	public boolean isLocked() {
		return isLocked;
	}
	public void setIsLocked(boolean yn) {
		isLocked = yn;
		updateInterface();
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(CocktailFinder.getQualifierEqual(EODif.ANNEE_CALCUL_KEY, getSelectedAnnee()));

		if (!StringCtrl.chaineVide(myView.getTfFiltreNom().getText()))
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODif.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreNom().getText()+"*")));
		
		if (myView.getCheckDebit().isSelected()) {
			qualifiers.addObject(CocktailFinder.getQualifierAfter(EODif.DIF_DEBIT_CUMULE_KEY, new Integer(0)));			
		}

		return new EOAndQualifier(qualifiers);
	}

	/**
	 * 
	 * @return
	 */
	private Integer getSelectedAnnee() {
		return (Integer)myView.getPopupAnnees().getSelectedItem();
	}

	/**
	 * 
	 */
	private void rechercher() {

		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EODif.fetchAll(getEdc(), filterQualifier(), EODif.SORT_ARRAY_NOM));
		myView.getMyEOTable().updateData();
		CocktailUtilities.setTextToLabel(myView.getLblMessage(), eod.displayedObjects().count() + " Comptes");
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void calculer() {
		try {
			lancerTraitementServeurIndividuel("clientSideRequestPreparerDifPourIndividus");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		myView.getMyEOTable().updateUI();
	}

	/**
	 * 
	 */
	private void toutCalculer() {

		try {
			lancerTraitementServeurGlobal("clientSideRequestPreparerDif");
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		actualiser();
		myView.toFront();

	}

	public boolean isTraitementServeurEnCours() {
		return traitementServeurEnCours;
	}



	public void setTraitementServeurEnCours(boolean traitementServeurEnCours) {
		this.traitementServeurEnCours = traitementServeurEnCours;
	}



	public boolean isPreparationEnCours() {
		return preparationEnCours;
	}



	public void setPreparationEnCours(boolean preparationEnCours) {
		this.preparationEnCours = preparationEnCours;
	}
	
	/**
	 * 
	 * @param nomMethode
	 */
	private void lancerTraitementServeurGlobal(String nomMethode) {

		setTraitementServeurEnCours(true);

		Class classeParametres[] = {Integer.class};
		Object parametres[] = {getSelectedAnnee()};

		myConsole.lancerTraitement(nomMethode, classeParametres, parametres);

		myConsole.toFront();

	}
	/**
	 * 
	 * @param nomMethode
	 * @param individus
	 * @param estPreparation
	 */
	private void lancerTraitementServeurIndividuel(String nomMethode) {

		setTraitementServeurEnCours(true);

		NSMutableDictionary dico = new NSMutableDictionary();
		dico.setObjectForKey(getSelectedAnnee(), "anneeCalcul");
		dico.setObjectForKey((NSArray<EOIndividu>)(eod.selectedObjects()).valueForKey(EODif.INDIVIDU_KEY), "individus");
		Class classeParametres[] = {NSDictionary.class};
		Object parametres[] = {dico.immutableClone()};

		myConsole.lancerTraitement(nomMethode, classeParametres, parametres);

		myConsole.toFront();

	}
	/**
	 * 
	 */
	public void terminerTraitementServeur() {

		try {

			setTraitementServeurEnCours(false);	
			setPreparationEnCours(false);

			myConsole.addToMessage("\n >> TRAITEMENT TERMINE !");
			actualiser();

			myView.setVisible(true);

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 
	 */
	private void exporter() {

		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "FORMATIONS";
			
			saveDialog.setSelectedFile(new File(nomFichierExport + CocktailConstantes.EXTENSION_CSV));
			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = enteteExport();

				for (EODif myRecord : new NSArray<EODif>(EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(), EODif.SORT_ARRAY_NOM) ))
					texte += texteExportPourRecord(myRecord) + CocktailExports.SAUT_DE_LIGNE;
				
				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

				CRICursor.setDefaultCursor(myView);

				UtilitairesFichier.openFile(file.getPath());
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @return
	 */
	private String enteteExport() {

		String entete = "";

		entete += "NOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PRENOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "No SECU";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "DATE NAISS";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "SERVICE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "ORIGINE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "GRADE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "TYPE CONTRAT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "HEURES";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "INDICE";

		entete += CocktailExports.SAUT_DE_LIGNE;

		return entete;
	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EODif record)    {

		String texte = "";

		// NOM
		texte += CocktailExports.ajouterChamp(record.individu().nomUsuel());
		// PRENOM
		texte += CocktailExports.ajouterChamp(record.individu().prenom());
		// DEBUT
		texte += CocktailExports.ajouterChampDate(record.dateDebut());
		// FIN
		texte += CocktailExports.ajouterChampDate(record.dateFin());
		// CREDIT CUMULE
		texte += CocktailExports.ajouterChampNumber(record.difCreditCumule());
		// DEBIT CUMULE
		texte += CocktailExports.ajouterChampNumber(record.difDebitCumule());
		// CREDIT
		texte += CocktailExports.ajouterChampNumber(record.difCreditAnnuel());
		// DEBIT
		texte += CocktailExports.ajouterChampNumber(record.difDebitAnnuel());

		return texte;
	}

	/**
	 * 
	 */
	private void filter() 	{

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();
		CocktailUtilities.setTextToLabel(myView.getLblMessage(), eod.displayedObjects().count() + " Comptes");

		updateInterface();

	}

	private void updateInterface() {
	}

	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			actualiser();
		}
	}


	private class ListenerDif implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			CDifCtrl ctrlDif = new CDifCtrl(null, getEdc());
			ctrlDif.open(getCurrentDif().individu());
		}

		public void onSelectionChanged() {
			setCurrentDif((EODif)eod.selectedObject());
			updateInterface();
		}
	}

}

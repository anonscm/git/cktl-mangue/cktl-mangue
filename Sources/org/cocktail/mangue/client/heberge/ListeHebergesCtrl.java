package org.cocktail.mangue.client.heberge;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.heberges.ListeHebergesView;
import org.cocktail.mangue.client.individu.infoscomp.DiplomesCtrl;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.mangue.client.individu.infosperso.InfosPersonnellesCtrl;
import org.cocktail.mangue.client.situation.SaisieAffectationCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOCompte;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class ListeHebergesCtrl {

	private static ListeHebergesCtrl 	sharedInstance;
	private static Boolean 				MODE_MODAL = Boolean.TRUE;
	private EOEditingContext			ec;
	private ListeHebergesView 			myView;
	private EODisplayGroup 				eod, eodAffectations;
	private boolean 					saisieEnabled, isLocked;

	private ListenerHeberge 		listenerHeberge = new ListenerHeberge();
	private ListenerAffectation 	listenerAffectation = new ListenerAffectation();

	private SaisieAffectationCtrl 	ctrlSaisieAff;
	private EOContratHeberges		currentHeberge;
	private EOAffectation			currentAffectation;
	private EOAgentPersonnel		currentUtilisateur;

	public ListeHebergesCtrl(EOEditingContext editingContext) {

		ec = editingContext;
		ctrlSaisieAff = new SaisieAffectationCtrl(ec);

		eod = new EODisplayGroup();
		eodAffectations = new EODisplayGroup();
		myView = new ListeHebergesView(null, MODE_MODAL.booleanValue(), eod, eodAffectations);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnAjouterAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterAffectation();}}
				);
		myView.getBtnModifierAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierAffectation();}}
				);
		myView.getBtnSupprimerAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerAffectation();}}
				);
		myView.getBtnRechercher().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {rechercher();}}
				);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporter();}}
				);
		myView.getBtnRenouveler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {renouveler();}}
				);
		myView.getBtnInfosPerso().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				CRICursor.setWaitCursor(myView);
				InfosPersonnellesCtrl.sharedInstance(ec).open(getCurrentHeberge().individu());
				CRICursor.setDefaultCursor(myView);
			}}
				);
		myView.getBtnInfosComplementaires().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				CRICursor.setWaitCursor(myView);
				DiplomesCtrl diplomes = new DiplomesCtrl(InfosComplementairesCtrl.sharedInstance(ec), ec);
				diplomes.open(getCurrentHeberge().individu());
				CRICursor.setDefaultCursor(myView);
			}}
				);

		myView.getMyEOTable().addListener(listenerHeberge);
		myView.getMyEOTableAff().addListener(listenerAffectation);

		CocktailUtilities.setDateToField(myView.getTfFiltrePeriodeDebut(), DateCtrl.today());
		CocktailUtilities.setDateToField(myView.getTfFiltrePeriodeFin(), DateCtrl.today());

		myView.getTfFiltreNom().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreObservations().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltrePeriodeDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfFiltrePeriodeDebut()));
		myView.getTfFiltrePeriodeDebut().addActionListener(new ActionListenerDateTextField(myView.getTfFiltrePeriodeDebut()));
		myView.getTfFiltrePeriodeFin().addFocusListener(new FocusListenerDateTextField(myView.getTfFiltrePeriodeFin()));
		myView.getTfFiltrePeriodeFin().addActionListener(new ActionListenerDateTextField(myView.getTfFiltrePeriodeFin()));

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		myView.getBtnImprimer().setEnabled(false);
		
		setSaisieEnabled(false);
		updateUI();
	}

	public static ListeHebergesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ListeHebergesCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 */
	public void open()	{

		if (!currentUtilisateur.peutGererHeberges()) {
			EODialogs.runErrorDialog("ERREUR", "Vous n'avez pas les droits de gestion des hébergés !");
			return;
		}

		CRICursor.setWaitCursor(myView);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	public EOContratHeberges getCurrentHeberge() {
		return currentHeberge;
	}

	public void setCurrentHeberge(EOContratHeberges currentHeberge) {
		this.currentHeberge = currentHeberge;
	}

	public EOAffectation getCurrentAffectation() {
		return currentAffectation;
	}
	public void setCurrentAffectation(EOAffectation currentAffectation) {
		this.currentAffectation = currentAffectation;
	}

	public void toFront() {
		myView.toFront();
	}
	public boolean saisieEnabled() {
		return saisieEnabled;
	}
	public boolean isLocked() {
		return isLocked;
	}
	public void setIsLocked(boolean yn) {
		isLocked = yn;
		updateUI();
	}

	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}

	private void actualiser()	{

		CRICursor.setWaitCursor(myView);
		rechercher();
		CRICursor.setDefaultCursor(myView);

	}

	private NSTimestamp getFiltreDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfFiltrePeriodeDebut());
	}
	private NSTimestamp getFiltreFin() {
		return CocktailUtilities.getDateFromField(myView.getTfFiltrePeriodeFin());
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray qualifiers = new NSMutableArray();
		NSTimestamp dateReference = new NSTimestamp();

		if (!StringCtrl.chaineVide(myView.getTfFiltreNom().getText()))
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContratHeberges.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreNom().getText()+"*")));
		if (!StringCtrl.chaineVide(myView.getTfFiltreObservations().getText()))
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContratHeberges.COMMENTAIRE_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreObservations().getText()+"*")));

		return new EOAndQualifier(qualifiers);
	}


	/**
	 * 
	 */
	private void rechercher() {

		CRICursor.setWaitCursor(myView);
		NSArray<EOContratHeberges> heberges = EOContratHeberges.findForPeriode(ec, getFiltreDebut(), getFiltreFin() );
		eod.setObjectArray(heberges);
		filter();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void exporter() {

		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "HEBERGES_" + StringCtrl.replace(myView.getTfFiltrePeriodeDebut().getText(), "/","")+"-"+StringCtrl.replace(myView.getTfFiltrePeriodeFin().getText(), "/","");

			saveDialog.setSelectedFile(new File(nomFichierExport + CocktailConstantes.EXTENSION_CSV));

			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = enteteExport();

				for (EOContratHeberges myRecord : new NSArray<EOContratHeberges>(EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(), EOContratHeberges.SORT_ARRAY_DATE_DEBUT) ))
					texte += texteExportPourRecord(myRecord) + CocktailExports.SAUT_DE_LIGNE;

				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

				CRICursor.setDefaultCursor(myView);

				CocktailUtilities.openFile(file.getPath());			
				myView.toFront();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @return
	 */
	private String enteteExport() {

		String entete = "";

		entete += "NOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PRENOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "No SECU";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "DATE NAISS";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "SERVICE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "ORIGINE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "GRADE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "TYPE CONTRAT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "HEURES";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "INDICE";

		entete += CocktailExports.SAUT_DE_LIGNE;

		return entete;
	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EOContratHeberges record)    {

		String texte = "";

		NSArray<EOAffectation> affectations = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(ec, record.individu(), record.dateDebut(), record.dateFin());

		int myIndex = 0;
		if (affectations.count() > 0) {
			for (EOAffectation myAffectation : affectations) {			
				// NOM
				texte += CocktailExports.ajouterChamp(record.individu().nomUsuel());
				// PRENOM
				texte += CocktailExports.ajouterChamp(record.individu().prenom());
				// SECU
				if (record.individu().indNoInsee() != null) {
					String chaineInsee = record.individu().indNoInsee();
					if (record.individu().indCleInsee() != null) {
						chaineInsee += " " + StringCtrl.stringCompletion(record.individu().indCleInsee().toString(),2,"0", "G");
					}
					texte += CocktailExports.ajouterChamp(chaineInsee);
				}
				else
					texte += CocktailExports.ajouterChampVide();

				// NAISSANCE
				texte += CocktailExports.ajouterChampDate(record.individu().dNaissance());
				// DEBUT
				texte += CocktailExports.ajouterChampDate(record.dateDebut());
				// FIN
				texte += CocktailExports.ajouterChampDate(record.dateFin());
				// SERVICE
				texte += CocktailExports.ajouterChamp(myAffectation.toStructureUlr().llStructure());
				// UAI
				if (record.toUai() != null) {
					texte += CocktailExports.ajouterChamp(record.toUai().libelleLong());
				}
				else {
					if (record.toStructureOrigine() != null) {
						texte += CocktailExports.ajouterChamp(record.toStructureOrigine().llStructure());						
					}
					else
						texte += CocktailExports.ajouterChampVide();
				}
				// GRADE
				if (record.toGrade() != null)
					texte += CocktailExports.ajouterChamp(record.toGrade().llGrade());
				else
					texte += CocktailExports.ajouterChampVide();
				
				// TYPE CONTRAT
				if (record.typeContratTravail() != null)
					texte += CocktailExports.ajouterChamp(record.typeContratTravail().libelleLong());
				else
					texte += CocktailExports.ajouterChampVide();

				texte += CocktailExports.ajouterChampNumber(record.ctrhHeure());

				if (record.ctrhInm() != null)
					texte += CocktailExports.ajouterChampNumber(record.ctrhInm());
				else
					texte += CocktailExports.ajouterChampVide();

				myIndex ++;
				if (myIndex < affectations.count())
					texte += CocktailExports.SAUT_DE_LIGNE;
			}
		}
		else {
			// NOM
			texte += CocktailExports.ajouterChamp(record.individu().nomUsuel());
			// PRENOM
			texte += CocktailExports.ajouterChamp(record.individu().prenom());
			// DEBUT
			texte += CocktailExports.ajouterChampDate(record.dateDebut());
			// FIN
			texte += CocktailExports.ajouterChampDate(record.dateFin());
			// SERVICE
			texte += CocktailExports.ajouterChamp("");
			// UAI
			if (record.toUai() != null)
				texte += CocktailExports.ajouterChamp(record.toUai().libelleLong());
			else
				texte += CocktailExports.ajouterChamp("");
			// GRADE
			if (record.toGrade() != null)
				texte += CocktailExports.ajouterChamp(record.toGrade().llGrade());
			else
				texte += CocktailExports.ajouterChamp("");
			// TYPE CONTRAT
			if (record.typeContratTravail() != null)
				texte += CocktailExports.ajouterChamp(record.typeContratTravail().libelleLong());
			else
				texte += CocktailExports.ajouterChamp("");

			if (record.ctrhHeure() != null)
				texte += CocktailExports.ajouterChamp(record.ctrhHeure().toString());
			else
				texte += CocktailExports.ajouterChamp("");

			if (record.ctrhInm() != null)
				texte += CocktailExports.ajouterChamp(record.ctrhInm().toString());
			else
				texte += CocktailExports.ajouterChamp("");

		}

		return texte;
	}

	/**
	 * 
	 */
	private void filter() 	{

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();
		CocktailUtilities.setTextToLabel(myView.getLblNbHeberges(), eod.displayedObjects().count() + " Hébergés");

		updateUI();

	}

	/**
	 * 
	 */
	private void ajouter() {

		EOContratHeberges nouvelHeberge = SaisieContratHebergeCtrl.sharedInstance(ec).ajouter(null);

		actualiser();

		if (nouvelHeberge != null) {

			setCurrentHeberge(nouvelHeberge);
			myView.getMyEOTable().scrollToSelection(nouvelHeberge);

			gestionCompteInformatique(nouvelHeberge.individu());
			
		}

	}
	
	private void renouveler() {
		SaisieContratHebergeCtrl.sharedInstance(ec).renouveler(getCurrentHeberge());
	}


	/**
	 * 
	 */
	public void gestionCompteInformatique(EOIndividu individu) {
		try {
			if (EOGrhumParametres.isCompteAuto()) {
				EOCompte comptePourIndividu = EOCompte.creerComptePourIndividu(ec, getCurrentUtilisateur(), individu, true);
				if (comptePourIndividu == null) {
					LogManager.logDetail("pas de creation");
				} 
				else {
					EOCompte.modifierComptePourIndividu(ec, comptePourIndividu, individu);
					ec.saveChanges();
				}
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			EODialogs.runErrorDialog("ERREUR", "Erreur de création du compte !");
		}
	}

	/**
	 * 
	 */
	private void modifier() {
		SaisieContratHebergeCtrl.sharedInstance(ec).modifier(getCurrentHeberge());
		myView.getMyEOTable().updateUI();
		myView.getMyEOTableAff().updateUI();
	}

	/**
	 * 
	 */
	private void ajouterAffectation() {
		ctrlSaisieAff.setAffectationsExistantes(eodAffectations.displayedObjects());
		ctrlSaisieAff.ajouter(getCurrentHeberge().individu());
		listenerHeberge.onSelectionChanged();
	}
	private void modifierAffectation() {		
		ctrlSaisieAff.setAffectationsExistantes(eodAffectations.displayedObjects());
		ctrlSaisieAff.modifier(getCurrentAffectation(), false);
		myView.getMyEOTableAff().updateUI();
	}


	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cet hébergé ?", "Oui", "Non"))		
			return;

		try {
			getCurrentHeberge().setEstValide(false);
			ec.saveChanges();
			actualiser();

		}
		catch(Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}
	private void supprimerAffectation() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette période d'affectation ?", "Oui", "Non"))		
			return;			

		try {
			ec.deleteObject(getCurrentAffectation());
			ec.saveChanges();
			eodAffectations.deleteSelection();
			myView.getMyEOTableAff().updateData();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			ec.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			ec.revert();
		}
	}

	private void updateUI() {

		myView.getBtnAjouter().setEnabled(true);
		myView.getBtnModifier().setEnabled(getCurrentHeberge() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentHeberge() != null);
		myView.getBtnImprimer().setEnabled(getCurrentHeberge() != null);
		myView.getBtnInfosPerso().setEnabled(getCurrentHeberge() != null);

		myView.getBtnAjouterAffectation().setEnabled(getCurrentHeberge() != null);
		myView.getBtnModifierAffectation().setEnabled(getCurrentAffectation() != null);
		myView.getBtnSupprimerAffectation().setEnabled(getCurrentAffectation() != null);

		if (saisieEnabled() || isLocked())
			myView.getMyEOTable().setForeground(CocktailConstantes.BG_COLOR_GREY);
		else
			myView.getMyEOTable().setForeground(CocktailConstantes.BG_COLOR_BLACK);

	}

	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			actualiser();
		}
	}

	private class ListenerHeberge implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			modifier();
		}

		public void onSelectionChanged() {

			setCurrentHeberge((EOContratHeberges)eod.selectedObject());
			eodAffectations.setObjectArray(new NSArray());

			if (getCurrentHeberge() != null) {
				eodAffectations.setObjectArray(EOAffectation.findForIndividu(ec, getCurrentHeberge().individu(), null));
			}

			myView.getMyEOTableAff().updateData();
			updateUI();
		}
	}
	private class ListenerAffectation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			modifierAffectation();
		}

		public void onSelectionChanged() {
			setCurrentAffectation((EOAffectation)eodAffectations.selectedObject());
			updateUI();
		}
	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		public void removeUpdate(DocumentEvent e) {
			filter();		
		}
	}
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			//actualiser();
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}

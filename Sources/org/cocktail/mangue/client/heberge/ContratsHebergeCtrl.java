package org.cocktail.mangue.client.heberge;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.contrats.ContratsHebergeView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class ContratsHebergeCtrl {

	private static ContratsHebergeCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private ContratsHebergeView myView;

	private ListenerHeberge listenerHeberge = new ListenerHeberge();

	private EODisplayGroup 			eod;

	private EOContratHeberges 		currentHeberge;
	private EOIndividu 				currentIndividu;
	private EOAgentPersonnel		currentUtilisateur;
	private String noArrete;

	/**
	 * 
	 * @param editingContext
	 */
	public ContratsHebergeCtrl(EOEditingContext edc) {

		this.edc = edc;

		eod = new EODisplayGroup();

		myView = new ContratsHebergeView(null, MODE_MODAL.booleanValue(),eod);

		myView.getMyEOTable().addListener(listenerHeberge);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnImprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
		);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		CocktailUtilities.initTextField(myView.getTfUai(), false, false);
		CocktailUtilities.initTextField(myView.getTfCorps(), false, false);
		CocktailUtilities.initTextField(myView.getTfGrade(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeContrat(), false, false);
		CocktailUtilities.initTextField(myView.getTfStructure(), false, false);
		CocktailUtilities.initTextField(myView.getTfHeures(), false, false);
		CocktailUtilities.initTextField(myView.getTfIndice(), false, false);
		CocktailUtilities.initTextArea(myView.getTaCommentaires(), false, false);
		
		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		updateInterface();
	}


	public static ContratsHebergeCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ContratsHebergeCtrl(editingContext);
		return sharedInstance;
	}

	public void employeHasChanged(NSNotification  notification) {
		clearTextDatas();
		if (notification != null && notification.object() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
	}

	public void actualiser() {

		eod.setObjectArray(EOContratHeberges.findForIndividuAndPeriode(edc, getCurrentIndividu(), null, null));
		myView.getMyEOTable().updateData();

		updateInterface();

	}

	/**
	 * @param currentIndividu
	 */
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EOContratHeberges getCurrentHeberge() {
		return currentHeberge;
	}


	public void setCurrentHeberge(EOContratHeberges currentHeberge) {
		this.currentHeberge = currentHeberge;
		updateDatas();
	}


	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}


	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {

		this.currentUtilisateur = currentUtilisateur;

		// Gestion des droits
		myView.getBtnAjouter().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnModifier().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnSupprimer().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnImprimer().setVisible(currentUtilisateur.peutGererCarrieres());

	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewHeberge() {
		return myView.getViewHeberge();
	}

	/**
	 * 
	 * @param notification
	 */
	public void nettoyerChamps(NSNotification notification) {

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();

		setCurrentIndividu(null);

		updateInterface();

	}

	private void clearTextDatas() {
		CocktailUtilities.viderTextField(myView.getTfUai());
		CocktailUtilities.viderTextField(myView.getTfCorps());
		CocktailUtilities.viderTextField(myView.getTfGrade());
		CocktailUtilities.viderTextField(myView.getTfTypeContrat());
		CocktailUtilities.viderTextField(myView.getTfStructure());
		CocktailUtilities.viderTextArea(myView.getTaCommentaires());
	}

	/**
	 * 
	 */
	private void updateDatas() {

		clearTextDatas();
		
		if (getCurrentHeberge() != null) {

			CocktailUtilities.setTextToField(myView.getTfUai(), getCurrentHeberge().libelleOrigine());
			if (getCurrentHeberge().toCorps() != null)
				CocktailUtilities.setTextToField(myView.getTfCorps(), getCurrentHeberge().toCorps().llCorps());
			if (getCurrentHeberge().toGrade() != null)
				CocktailUtilities.setTextToField(myView.getTfGrade(), getCurrentHeberge().toGrade().llGrade());
			if (getCurrentHeberge().typeContratTravail() != null)
				CocktailUtilities.setTextToField(myView.getTfTypeContrat(), getCurrentHeberge().typeContratTravail().libelleLong());
			if (getCurrentHeberge().toStructureHebergement() != null)
				CocktailUtilities.setTextToField(myView.getTfStructure(), getCurrentHeberge().toStructureHebergement().llStructure());

			CocktailUtilities.setTextToField(myView.getTfDetail(),getCurrentHeberge().detailOrigine());

			CocktailUtilities.setNumberToField(myView.getTfHeures(), getCurrentHeberge().ctrhHeure());
			CocktailUtilities.setNumberToField(myView.getTfIndice(), getCurrentHeberge().ctrhInm());

			CocktailUtilities.setTextToArea(myView.getTaCommentaires(),getCurrentHeberge().commentaire());
			
		}
		
	}

	private void ajouter() {
		SaisieContratHebergeCtrl.sharedInstance(edc).ajouter(getCurrentIndividu());
		actualiser();
	}
	private void modifier() {
		SaisieContratHebergeCtrl.sharedInstance(edc).modifier(getCurrentHeberge());
		myView.getMyEOTable().updateUI();
		listenerHeberge.onSelectionChanged();
	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette période d'hébergement ?", "Oui", "Non"))		
			return;

		try {
			getCurrentHeberge().setEstValide(false);
			edc.saveChanges();
			actualiser();

		}
		catch(Exception e) {
			edc.revert();
			e.printStackTrace();
		}
	}

	/**
	 * Impression d'un contrat de vacations avec sélection de destinatires
	 */
	private void imprimer() {}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerHeberge implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererContrats())
				modifier();
		}

		public void onSelectionChanged() {

			clearTextDatas();
			setCurrentHeberge((EOContratHeberges)eod.selectedObject());

			updateInterface();

		}
	}


	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null);
		myView.getBtnModifier().setEnabled(getCurrentHeberge() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentHeberge() != null);
		myView.getBtnImprimer().setEnabled(getCurrentHeberge() != null);

	}

}
package org.cocktail.mangue.client.syntheses;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.anciennete.FicheSynthesesView;
import org.cocktail.mangue.modele.FicheAnciennete;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class FicheSynthesesCtrl {
	
	private static final int INDEX_ANCIENNETE = 0;
	private static final int INDEX_CARRIERE = 1;
	private static final int INDEX_CIR = 2;
	
	private static Boolean MODE_MODAL = Boolean.TRUE;
	private EOEditingContext edc;
	private FicheSynthesesView myView;

	private ListenerFiche listenerFiche = new ListenerFiche();

	private boolean peutGererModule;
	private EODisplayGroup eod;

	private FicheAnciennete currentFiche;
	private EOIndividu 		currentIndividu;

	/**
	 * 
	 * @param edc
	 */
	public FicheSynthesesCtrl(EOEditingContext edc) {

		this.edc = edc;

		eod = new EODisplayGroup();

		myView = new FicheSynthesesView(null, MODE_MODAL.booleanValue(),eod);

		myView.getBtnCalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {calculer();}}
		);
		myView.getBtnImprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
		);

		myView.getMyEOTable().addListener(listenerFiche);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);
		
		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));
		}

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		
		updateInterface();

	}

	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	
	public void employeHasChanged(NSNotification  notification) {
		if (notification != null && notification.object() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
		}
	}

	/**
	 * 
	 * @param notification
	 */
	public void nettoyerChamps(NSNotification notification) {
		setCurrentIndividu(null);
	}
	
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		setPeutGererModule(currentUtilisateur.peutGererCarrieres());
	}

	public FicheAnciennete getCurrentFiche() {
		return currentFiche;
	}

	public void setCurrentFiche(FicheAnciennete currentFiche) {
		this.currentFiche = currentFiche;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}

	public int getTypeFiche() {
		return myView.getPopupTypeSynthese().getSelectedIndex();
	}
	
	/**
	 * 
	 */
	public void updateDatas() {
	}

	public JDialog getView() {
		return myView;
	}

	public JPanel getViewSyntheses() {
		return myView.getViewSyntheses();
	}

	private void calculer() {
		
	}
	private void imprimer() {
		
	}

	private class ListenerFiche implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
		}
	}
	
	/**
	 * 
	 */
	private void updateInterface() {

	}

}
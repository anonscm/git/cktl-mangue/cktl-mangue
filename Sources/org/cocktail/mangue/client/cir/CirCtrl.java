package org.cocktail.mangue.client.cir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.cir.CirView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirIdentiteCtrl, CirCarriereCtrl

public class CirCtrl {
	
	private static CirCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private CirView myView;
	private OngletChangeListener listenerOnglets;

	private Integer 			exerciceCir;
	private EOAgentPersonnel 	currentUtilisateur;
	
	private CirIdentiteCtrl ctrlIdentite;
	private CirCarriereCtrl ctrlCarriere;

	public CirCtrl(EOEditingContext edc) {

		setEdc(edc);
		listenerOnglets = new OngletChangeListener();

		myView = new CirView(null, MODE_MODAL.booleanValue());

		ctrlIdentite = new CirIdentiteCtrl(getEdc(), this);
		ctrlCarriere = new CirCarriereCtrl(getEdc(), this);
		
		myView.getOnglets().addTab("Fichiers Identité ", null, ctrlIdentite.getView());
		myView.getOnglets().addTab("Fichiers Carrière ", null, ctrlCarriere.getView());
		myView.getOnglets().addChangeListener(listenerOnglets);

		myView.setListeExercices(CocktailConstantes.LISTE_ANNEES_DESC);
		initExerciceCourant();
		myView.getPopupExercices().addActionListener(new PopupExerciceListener());

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
	}
	
	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public Integer getCurrentExercice() {
		return (Integer)myView.getPopupExercices().getSelectedItem();
	}
	public Integer getCurrentExerciceIdentite() {
		return new Integer( ((Integer)myView.getPopupExercices().getSelectedItem()).intValue() - 1);
	}

	public Integer getExerciceCir() {
		return exerciceCir;
	}

	public void setExerciceCir(Integer exerciceCir) {
		this.exerciceCir = exerciceCir;
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}

	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	/**
	 * L'exercice de gestion du CIR est l'annee courante SAUF pour Décembre ou on prend l'annee suivante.
	 */
	private void initExerciceCourant() {
		
		Integer exerciceCir = DateCtrl.getYear(new NSTimestamp());
		
		if (DateCtrl.getCurrentMonth() == 11)
			exerciceCir = new Integer(exerciceCir.intValue()+1);
		
		setExerciceCir(exerciceCir);
		
		myView.getPopupExercices().setSelectedItem(getExerciceCir());
		
	}

	public static CirCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new CirCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 */
	public void open()	{
		
		CRICursor.setWaitCursor(myView);
		// Controle des droits
		if (getCurrentUtilisateur().peutGererCir() == false) {
			EODialogs.runInformationDialog("ERREUR", "Vous n'avez pas les droits nécessaires pour la gestion du Compte Individuel Retraite !");
			return;
		}

		listenerOnglets.stateChanged(null);
		
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);
		
	}

	public JFrame getView() {
		return myView;
	}
	public void toFront() {
		myView.toFront();
	}

	private void actualiser()	{
		CRICursor.setWaitCursor(myView);
		switch(myView.getOnglets().getSelectedIndex())		{
		case 0:	ctrlIdentite.actualiser();break;
		case 1: ctrlCarriere.actualiser();break;
		}
		CRICursor.setDefaultCursor(myView);
	}

	private class OngletChangeListener
	implements ChangeListener	{

		public void stateChanged(ChangeEvent e) {
			CRICursor.setWaitCursor(myView);
			switch(myView.getOnglets().getSelectedIndex())	{
			case 0:	ctrlIdentite.actualiser();break;
			case 1:	ctrlCarriere.actualiser();break;
			}
			CRICursor.setDefaultCursor(myView);
		}
	}

	private class PopupExerciceListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			actualiser();
		}
	}
}
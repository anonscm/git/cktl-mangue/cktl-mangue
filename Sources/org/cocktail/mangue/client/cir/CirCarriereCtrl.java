
package org.cocktail.mangue.client.cir;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ConsoleTraitementCtrl;
import org.cocktail.mangue.client.gui.cir.CirCarriereView;
import org.cocktail.mangue.client.onglets.AgentsMouseListener;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriere;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriereData;
import org.cocktail.mangue.modele.mangue.cir.EOCirFichierImport;
import org.cocktail.mangue.modele.mangue.cir.EOCirIdentite;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;


public class CirCarriereCtrl {

	private EOEditingContext 	edc;
	private EODisplayGroup 		eodFichier;
	private EODisplayGroup 		eodIndividu;
	private EODisplayGroup 		eodCir;
	private CirCarriereView 	myView;
	private boolean 			traitementServeurEnCours, preparationEnCours = false;
	private CarriereRenderer	monRendererColor = new CarriereRenderer();

	private EOCirFichierImport currentFichier;
	private EOCirCarriere currentCarriere;

	private ListenerFichier listenerFichier = new ListenerFichier();
	private ListenerIndividu listenerIndividu = new ListenerIndividu();
	private ListenerCir 				listenerCir = new ListenerCir();
	private AgentsMouseListener 		edtListener;
	private ConsoleTraitementCtrl 		myConsole;
	private CirCtrl ctrlParent;

	public CirCarriereCtrl(EOEditingContext edc, CirCtrl ctrlParent) {
		setEdc(edc);

		this.ctrlParent = ctrlParent;

		eodFichier = new EODisplayGroup();
		eodIndividu = new EODisplayGroup();
		eodCir = new EODisplayGroup();
		myView = new CirCarriereView(eodFichier, eodIndividu, eodCir, monRendererColor);

		myConsole = new ConsoleTraitementCtrl(edc);
		myConsole.setCtrlParent(this, "Traitement des données CIR ...");

		myView.getBtnRefreshIndividus().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				refresh();
			}
		});
		myView.getBtnPreparerDonneesCir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {preparerDonneesCir();}}
				);
		myView.getBtnDelDonneeCir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerDonneesCir();}}
				);
		myView.getBtnDetailAgentCir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherDonneesIndividu();}}
				);

		myView.getBtnAddFichier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterFichier();}}
				);
		myView.getBtnUpdateFichier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierFichier();}}
				);
		myView.getBtnDeleteFichier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerFichier();}}
				);

		myView.getBtnSelectAllIndividu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectAllIndividus();}}
				);
		myView.getBtnExport().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporter();}}
				);
		myView.getBtnPreparerFichierCir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){preparerFichier();}}
				);
		myView.getBtnRecalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){recalculerDonnees();}}
				);

		edtListener = new AgentsMouseListener(getEdc(), myView.getMyEOTableCir());
		myView.getMyEOTableCir().addMouseListener(edtListener);
		myView.getMyEOTableCir().addMouseMotionListener(edtListener);

		myView.getMyEOTableFichier().addListener(listenerFichier);
		myView.getMyEOTableIndividu().addListener(listenerIndividu);
		myView.getMyEOTableCir().addListener(listenerCir);
		myView.getLblTableIndividus().setText("");
		myView.getLblTableCir().setText("");
		myView.getTfFiltreIndividu().getDocument().addDocumentListener(new FilterIndividuListener());
		myView.getTfFiltreCir().getDocument().addDocumentListener(new FilterCirListener());

		myView.getBtnRefreshIndividus().setVisible(false);

		myView.getBtnPreparerFichierCir().setEnabled(false);
		myView.getPopupValidite().addActionListener(new PopupValiditeListener());

		myView.getBtnExport().setVisible(false);

		updateInterface();
	}



	public EOEditingContext getEdc() {
		return edc;
	}



	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public JPanel getView() {
		return myView;
	}

	public void refresh()	{
		listenerFichier.onSelectionChanged();
	}

	private EOCirFichierImport getCurrentFichier() {
		return currentFichier;
	}

	public EOCirCarriere getCurrentCarriere() {
		return currentCarriere;
	}
	public void setCurrentCarriere(EOCirCarriere currentCarriere) {
		this.currentCarriere = currentCarriere;
	}
	public void setCurrentFichier(EOCirFichierImport currentFichier) {
		this.currentFichier = currentFichier;
	}

	public boolean isTraitementServeurEnCours() {
		return traitementServeurEnCours;
	}

	public void setTraitementServeurEnCours(boolean traitementServeurEnCours) {
		this.traitementServeurEnCours = traitementServeurEnCours;
	}

	public boolean isPreparationEnCours() {
		return preparationEnCours;
	}

	public void setPreparationEnCours(boolean preparationEnCours) {
		this.preparationEnCours = preparationEnCours;
	}



	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifierIndividu() {
		NSMutableArray mesQualifiers = new NSMutableArray();
		if(myView.getTfFiltreIndividu().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.TO_INDIVIDU_KEY + "." + EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray("*" + myView.getTfFiltreIndividu().getText() + "*")));
		return new EOAndQualifier(mesQualifiers);
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifierCir() {
		NSMutableArray mesQualifiers = new NSMutableArray();
		if(myView.getTfFiltreCir().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirCarriere.TO_INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray("*" + myView.getTfFiltreCir().getText() + "*")));

		int indexValidite = myView.getPopupValidite().getSelectedIndex();
		if ( indexValidite== 1)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirCarriere.TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		else
			if ( indexValidite == 2)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirCarriere.TEM_VALIDE_KEY + "=%@", new NSArray("N")));

		return new EOAndQualifier(mesQualifiers);
	}

	private void clean() {
		eodIndividu.setObjectArray(new NSArray<EOCirIdentite>());
		myView.getMyEOTableIndividu().updateData();
	}

	/**
	 * 
	 */
	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		clean();

		eodFichier.setObjectArray(EOCirFichierImport.findForAnnee(getEdc(), ctrlParent.getCurrentExercice()));
		myView.getMyEOTableFichier().updateData();
		updateInterface();

		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * Re-calcul des donnees des agents selectionnes dans la partie CIR
	 */
	private void recalculerDonnees() {
		CRICursor.setWaitCursor(myView);
		try {
			preparationEnCours = true;
			lancerTraitementServeur("clientSideRequestPreparerRecordsCir",  (NSArray)eodCir.selectedObjects().valueForKey(EOCirIdentite.TO_INDIVIDU_KEY), true);
		}
		catch(Exception ex) {
			getEdc().revert();
			ex.printStackTrace();
		}
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void afficherDonneesIndividu() {
		CirEditionDonneesCtrl.sharedInstance(getEdc()).open(getCurrentCarriere(), EOCirCarriereData.recordsPourCirCarriere(getEdc(), getCurrentCarriere()));
	}

	/**
	 * 
	 */
	private void filterCir()	{

		eodCir.setQualifier(filterQualifierCir());
		eodCir.updateDisplayedObjects();
		myView.getMyEOTableCir().updateData();
		myView.getLblTableCir().setText( "Fichier CIR " + (ctrlParent.getCurrentExercice().intValue() - 1) + " (" + eodCir.displayedObjects().size() + ")");

	}

	/**
	 * 
	 */
	private void filterIndividus() {
		eodIndividu.setQualifier(filterQualifierIndividu());
		eodIndividu.updateDisplayedObjects();
		myView.getMyEOTableIndividu().updateData();
		myView.getLblTableIndividus().setText("Individus avec carrière en " + (ctrlParent.getCurrentExercice().intValue() - 1) + " (" + eodIndividu.displayedObjects().size() + ")");
	}

	/**
	 * 
	 * @param edc
	 * @param nomRubrique
	 * @return
	 */
	public static NSArray recupererSousRubriques(EOEditingContext edc,String nomRubrique) {
		EODistributedObjectStore store = (EODistributedObjectStore)edc.parentObjectStore();
		Class[] classeParametres = new Class[] {String.class};
		Object[] parametres = new Object[] {nomRubrique};
		return (NSArray)store.invokeRemoteMethodWithKeyPath(edc,"session","clientSideRequestSousRubriquesPourRubrique",classeParametres,parametres,false);
	}


	/**
	 * 
	 */
	public void exporter() {
		CRICursor.setWaitCursor(myView);
		String texteComplet = "";
		for(EOCirCarriere carriere : (NSArray<EOCirCarriere>)eodCir.selectedObjects()) {

			String texte = "Donnees CIR de : " + carriere.toIndividu().identitePrenomFirst() + "\n";
			String rubriqueCourante = null;
			NSArray datasForIndividu = EOCirCarriereData.recordsPourCirCarriere(getEdc(), carriere);
			datasForIndividu = EOSortOrdering.sortedArrayUsingKeyOrderArray(datasForIndividu, new NSArray(EOSortOrdering.sortOrderingWithKey(EOCirCarriereData.CIRCD_DATA_KEY, EOSortOrdering.CompareAscending)));
			for(Enumeration<EOCirCarriereData> e1 = datasForIndividu.objectEnumerator(); e1.hasMoreElements();)	{
				EOCirCarriereData data = (EOCirCarriereData)e1.nextElement();
				if(rubriqueCourante == null || !data.circdRubrique().equals(rubriqueCourante)) {
					rubriqueCourante = data.circdRubrique();
					NSArray sousRubriques = recupererSousRubriques(getEdc(), rubriqueCourante);
					for(Enumeration<SousRubriqueCir> e2 = sousRubriques.objectEnumerator(); e2.hasMoreElements();) {
						SousRubriqueCir sousRubriqueCir = e2.nextElement();
						texte = texte + sousRubriqueCir.cle() + "\t";
					}

					texte = texte + "\n";
				}

				for(String valeurs = data.circdData(); valeurs.length() > 0;) {
					int index = valeurs.indexOf("$");
					String valeur = "";
					if(index > 0)
						valeurs.substring(0, index);
					else
						valeurs.substring(0, valeurs.length());
					texte = texte + valeur + "\t";
					if(index < valeurs.length())
						valeurs = valeurs.substring(index + 1);
					else
						valeurs = "";
				}

				texte = texte + "\n";
			}

			texteComplet = texteComplet + texte + "\n";
		}

		UtilitairesFichier.afficherFichierExcel(texteComplet, pathFichierCirCAR(), "ExportCIR_" + getCurrentFichier().cfimFichier(), false);
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 * @return
	 */
	public String pathFichierCirCAR() {

		String pathDir = (((ApplicationClient)EOApplication.sharedApplication()).directoryExports()) +
				System.getProperty("file.separator") + 
				"CIR" + 
				System.getProperty("file.separator") +
				"CAR" + 
				System.getProperty("file.separator");
		UtilitairesFichier.verifierPathEtCreer(pathDir);

		return pathDir;

	}

	private void ajouterFichier() {
		EOCirFichierImport newFichier = CirSaisieFichieImportCtrl.sharedInstance(getEdc()).ajouter(ctrlParent.getCurrentExercice());
		if(newFichier != null)
			actualiser();
	}

	private void modifierFichier() {
		if(CirSaisieFichieImportCtrl.sharedInstance(getEdc()).modifier(getCurrentFichier())) {
			myView.getMyEOTableFichier().updateUI();
		}
	}

	/**
	 *  Suppression de toutes les données CIR associees au fichier. 
	 *  CIR_CARRIERE_DATE et CIR_CARRIERE
	 */
	private void supprimerFichier()	{

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer le fichier " + getCurrentFichier().cfimFichier() + ", toutes les données CIR associées seront supprimées ?", "Oui", "Non"))
			return;

		CirCtrl.sharedInstance(getEdc()).toFront();
		CRICursor.setWaitCursor(myView);

		try {

			NSArray<EOCirCarriere> carrieres = EOCirCarriere.individusPourFichierImport(getEdc(), getCurrentFichier());
			for (EOCirCarriere carriere : carrieres) {
				NSArray<EOCirCarriereData> datas = EOCirCarriereData.recordsPourCirCarriere(getEdc(), carriere);				
				for (EOCirCarriereData data : datas) {
					getEdc().deleteObject(data);
				}
				edc.deleteObject(carriere);
			}

			getEdc().deleteObject(getCurrentFichier());
			getEdc().saveChanges();

			EODialogs.runInformationDialog("","La suppression du fichier a bien été effectuée !");

			actualiser();

		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}

		CRICursor.setDefaultCursor(myView);

	}

	private void selectAllIndividus() {
		CRICursor.setWaitCursor(myView);
		eodIndividu.setSelectedObjects(eodIndividu.displayedObjects());
		eodIndividu.updateDisplayedObjects();
		myView.getMyEOTableIndividu().updateData();
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void supprimerDonneesCir()	{

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer les données CIR des agents sélectionnés ?", "Oui", "Non"))
			return;

		CirCtrl.sharedInstance(getEdc()).toFront();
		try	{

			CRICursor.setWaitCursor(myView);

			for(EOCirCarriere carriere : (NSArray<EOCirCarriere>)eodCir.selectedObjects()) {

				NSArray<EOCirCarriereData> datas = EOCirCarriereData.recordsPourCirCarriere(getEdc(), carriere);
				for (EOCirCarriereData data : datas) {
					getEdc().deleteObject(data);
				}

				getEdc().deleteObject(carriere);

			}

			getEdc().saveChanges();
			EODialogs.runInformationDialog("OK", "Les données CIR ont bien été supprimées pour les agents sélectionnés !");

			CirCtrl.sharedInstance(getEdc()).toFront();
			actualiser();

		}
		catch(Exception e)	{
			getEdc().revert();
			e.printStackTrace();
		}

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void preparerDonneesCir() {

		try {
			if(getCurrentFichier().isProgramme() && eodIndividu.selectedObjects().size() > 50) {
				EODialogs.runInformationDialog("Attention", "Vous ne pouvez préparer plus de 50 dossiers simultanément pour des raisons de surcharge serveur !\n" +
						"Pour une préparation quotidienne des dossiers CIR pensez à la programmation du calcul.");
				CRICursor.setDefaultCursor(myView);
				return;
			}

			NSArray individus = (NSArray)eodIndividu.selectedObjects().valueForKey(EOCirIdentite.TO_INDIVIDU_KEY);
			preparationEnCours = true;
			lancerTraitementServeur("clientSideRequestPreparerRecordsCir", individus, true);

		}
		catch(Exception ex) {
			getEdc().revert();
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void preparerFichier() {
		try	{

			String nomBase = EOGrhumParametres.getUniteGestionCir();
			if(nomBase == null || !nomBase.startsWith("EM") ) {
				EODialogs.runErrorDialog("Erreur", "Le paramètre 'UNITE_GESTION' n'est pas ou mal défini dans les paramètres de Mangue (Admin / Paramètres / Mangue)");
				return;
			}

			// On ne récupère que les individus valides
			NSMutableArray<EOIndividu> individus = new NSMutableArray<EOIndividu>();
			for (EOCirCarriere myCarriere : (NSArray<EOCirCarriere>) eodCir.allObjects()) {
				if (myCarriere.estValide())
					individus.addObject(myCarriere.toIndividu());
			}

			lancerTraitementServeur("clientSideRequestPreparerFichierCir", individus.immutableClone(), false);

		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 
	 * @param nomMethode
	 * @param individus
	 * @param estPreparation
	 */
	private void lancerTraitementServeur(String nomMethode, NSArray individus, boolean estPreparation) {

		boolean estRapportDetaille = true;

		setTraitementServeurEnCours(true);

		Class classeParametres[];
		Object parametres[];
		if(estPreparation) {
			classeParametres = (new Class[] {
					EOGlobalID.class, 
					NSArray.class, Boolean.class
			});
			parametres = (new Object[] {
					getEdc().globalIDForObject(getCurrentFichier()), 
					Utilitaires.tableauDeGlobalIDs(individus, getEdc()), new Boolean(estRapportDetaille)
			});
		} else		// preparation du fichier
		{
			classeParametres = (new Class[] {
					EOGlobalID.class, NSArray.class
			});
			parametres = (new Object[] {
					getEdc().globalIDForObject(getCurrentFichier()), 
					Utilitaires.tableauDeGlobalIDs(individus, getEdc())
			});
		}

		myConsole.lancerTraitement(nomMethode, classeParametres, parametres);

	}

	/**
	 * 
	 */
	private void recupererFichiers() {

		EODistributedObjectStore store = (EODistributedObjectStore)getEdc().parentObjectStore();
		// le dictionnaire contient comme cle le nom du fichier recupere et comme valeur un tableau de lignes du fichier

		try {

			if (!isPreparationEnCours()) {

				NSDictionary dicoFichiers = (NSDictionary)store.invokeRemoteMethodWithKeyPath(getEdc(),"session","clientSideRequestRecupererFichiersCIR",null,null,true);
				for (java.util.Enumeration<String> e = dicoFichiers.allKeys().objectEnumerator();e.hasMoreElements();) {

					String nomFichier = (String)e.nextElement();

					LogManager.logInformation(" >> Recup fichier " + nomFichier);
					NSArray lignes = (NSArray)dicoFichiers.valueForKey(nomFichier);
					File aFile = new File(pathFichierCirCAR() + nomFichier);
					try	{
						FileOutputStream outputStream = new FileOutputStream(aFile);
						BufferedWriter fichier = new BufferedWriter(new OutputStreamWriter(outputStream,Charset.forName("ISO-8859-1")));
						try {
							for (java.util.Enumeration<String> e1 = lignes.objectEnumerator();e1.hasMoreElements();) {
								String ligne = e1.nextElement();
								fichier.write(ligne,0,ligne.length());
								fichier.newLine();
							} 
							fichier.flush();
						} catch (Exception exc) {
							System.out.println(exc.getMessage());
						} finally {
							fichier.close();
						}
					} catch(Exception exception)	{
						System.out.println(exception.getMessage());
					}
				}}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 
	 */
	public void terminerTraitementServeur() {		

		try {

			getEdc().invalidateObjectsWithGlobalIDs(new NSArray(getEdc().globalIDForObject(getCurrentFichier())));
			NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN, this);

			setTraitementServeurEnCours(false);		

			recupererFichiers();

			setPreparationEnCours(false);

			myConsole.addToMessage("\n >> TRAITEMENT TERMINE !");

			EOCirCarriere agentSelectionne = getCurrentCarriere();

			actualiser();
			if (agentSelectionne != null) {
				CocktailUtilities.forceSelection(myView.getMyEOTableCir(), new NSArray(agentSelectionne));
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Au mois de décembre, on prepare le fichier N+1, sinon on ne peut preparer un fichier que pour l'exercice en cours.
	 * @return
	 */
	public boolean peutPreparerFichier() {

		if (getCurrentFichier() == null) {
			return false;
		}

		int anneeCourante = DateCtrl.getCurrentYear();
		int moisCourant = DateCtrl.getCurrentMonth();
		int jourCourant = DateCtrl.getDayOfMonth(DateCtrl.today());
		int anneeFichier = getCurrentFichier().cfimAnnee().intValue();

		if (moisCourant == 11 && jourCourant >= 1) {
			return anneeFichier == anneeCourante + 1;
		}

		return (anneeCourante == anneeFichier);
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnSelectAllIndividu().setEnabled(eodIndividu.displayedObjects().size() > 0);
		myView.getBtnDelDonneeCir().setEnabled(eodCir.selectedObjects().size() > 0);
		myView.getBtnDetailAgentCir().setEnabled(eodCir.selectedObjects().size() == 1);

		myView.getBtnAddFichier().setEnabled(true);
		myView.getBtnUpdateFichier().setEnabled(getCurrentFichier() != null);
		myView.getBtnDeleteFichier().setEnabled(getCurrentFichier() != null);

		myView.getBtnPreparerDonneesCir().setEnabled(getCurrentFichier() != null && eodIndividu.selectedObjects().size() > 0);
		myView.getBtnExport().setEnabled(eodCir.displayedObjects().size() > 0);
		myView.getBtnRecalculer().setEnabled(eodCir.selectedObjects().size() > 0);

		myView.getBtnPreparerFichierCir().setEnabled(peutPreparerFichier());

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class FilterCirListener implements DocumentListener 	{

		public void changedUpdate(DocumentEvent e) {
			filterCir();
		}

		public void insertUpdate(DocumentEvent e) {
			filterCir();
		}

		public void removeUpdate(DocumentEvent e) {
			filterCir();
		}
	}

	private class FilterIndividuListener implements DocumentListener {

		public void changedUpdate(DocumentEvent e) {
			filterIndividus();
		}

		public void insertUpdate(DocumentEvent e) {
			filterIndividus();
		}

		public void removeUpdate(DocumentEvent e) {
			filterIndividus();
		}

	}

	public class FiltrePourFichierTexte extends FileFilter {

		public boolean accept(File aFile) {
			return aFile.getPath().indexOf(".txt") > 0;
		}

		public String getDescription() {
			return "Sélection du fichier généré pour le Cir";
		}

	}


	private class PopupValiditeListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {
			filterCir();
		}
	}



	public class IndividuCir implements NSKeyValueCoding {
		private EOIndividu individu;
		private boolean aCarriereAvecTrous;

		public IndividuCir(EOIndividu individu) {
			this.individu = individu;
			this.aCarriereAvecTrous = false;
		}
		public EOIndividu individu() {
			return individu;
		}

		public boolean aCarriereAvecTrous() {
			return this.aCarriereAvecTrous;
		}
		public void setACarriereAvecTrous(boolean aBool) {
			aCarriereAvecTrous = aBool;
		}
		public String identite() {
			if (!aCarriereAvecTrous) {
				return individu.identite();
			} else {
				return individu.identite() + "**";
			}
		}
		public String nomUsuel() {
			return individu.nomUsuel();
		}
		public String prenom() {
			return individu.prenom();
		}
		public String toString() {
			if (individu != null)
				return individu.identite();
			else
				return "individu inconnu";
		}
		//	 interface keyValueCoding
		public void takeValueForKey(Object valeur,String cle) {
			NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
		}
		public Object valueForKey(String cle) {
			return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
		}
	}

	private class ListenerCir implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			afficherDonneesIndividu();	
		}

		public void onSelectionChanged() {
			setCurrentCarriere((EOCirCarriere)eodCir.selectedObject());
			if (getCurrentCarriere() != null) {
				edtListener.setCurrentIndividu(getCurrentCarriere().toIndividu());

				if (getCurrentCarriere().estValide() == false)
					myView.getMyEOTableCir().setToolTipText(getCurrentCarriere().circCommentaires());

			}
			updateInterface();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerFichier implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener	{

		public void onDbClick() {
			modifierFichier();
		}

		public void onSelectionChanged() {

			CRICursor.setWaitCursor(myView);
			setCurrentFichier((EOCirFichierImport)eodFichier.selectedObject());

			Integer anneeCarriere = new Integer(ctrlParent.getCurrentExercice().intValue() - 1);
			eodIndividu.setObjectArray(EOCirIdentite.comptesValidesPourAnnee(getEdc(), anneeCarriere));

			filterIndividus();

			NSArray<EOCirCarriere> individus = EOCirCarriere.individusPourFichierImport(getEdc(), getCurrentFichier());
			eodCir.setObjectArray(individus);
			filterCir();

			updateInterface();
			CRICursor.setDefaultCursor(myView);

		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerIndividu implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged(){
			updateInterface();
		}
	}



	/**
	 * Classe servant a colorer les cellules selon l'etat de l'engagement
	 */
	private class CarriereRenderer extends ZEOTableCellRenderer	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2907930349355563787L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOCirCarriere obj = (EOCirCarriere) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().get(mdlRow);           

			if (obj.estValide() == false)
				leComposant.setBackground(new Color(255,151,96));
			else
				leComposant.setBackground(new Color(142,255,134));

			return leComposant;
		}
	}



}

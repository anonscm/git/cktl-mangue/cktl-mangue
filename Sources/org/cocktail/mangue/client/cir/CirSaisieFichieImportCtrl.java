// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.cir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.cir.CirSaisieFichierImportView;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.TimeCtrl;
import org.cocktail.mangue.modele.mangue.cir.EOCirFichierImport;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class CirSaisieFichieImportCtrl extends ModelePageSaisie
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static CirSaisieFichieImportCtrl sharedInstance;
	private CirSaisieFichierImportView myView;
	private EOCirFichierImport currentFichier;

	public CirSaisieFichieImportCtrl(EOEditingContext edc) {

		super(edc);

		myView = new CirSaisieFichierImportView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		// Pour le moment,on ne peut faire autre chose qu'une remontee de type CAMPAGNE
		myView.getPopupTypes().setEnabled(false);

		myView.getCheckProgrammation().addActionListener(new ProgrammationListener());

		myView.getTfHeureCalcul().addFocusListener(new FocusListenerHeureTextField(myView.getTfHeureCalcul()));
		myView.getTfHeureCalcul().addActionListener(new ActionListenerHeureTextField(myView.getTfHeureCalcul()));

	}

	public static CirSaisieFichieImportCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new CirSaisieFichieImportCtrl(editingContext);
		return sharedInstance;
	}

	public EOCirFichierImport getCurrentFichier() {
		return currentFichier;
	}

	public void setCurrentFichier(EOCirFichierImport currentFichier) {
		this.currentFichier = currentFichier;
	}

	private class ProgrammationListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {
			updateInterface();
		}
	}

	/**
	 * 
	 * @param exercice
	 * @return
	 */
	public EOCirFichierImport ajouter(Integer exercice)	{

		setModeCreation(true);		
		setCurrentFichier(EOCirFichierImport.creer(getEdc(), exercice));
		updateDatas();

		myView.setVisible(true);

		return getCurrentFichier();
	}

	public boolean modifier(EOCirFichierImport fichier) {

		setCurrentFichier(fichier);
		updateDatas();

		setModeCreation(false);
		myView.setVisible(true);

		return getCurrentFichier() != null;
	}

	/**
	 * 
	 * @param myTextField
	 */
	private void heureHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myTime = TimeCtrl.timeCompletion(myTextField.getText());
		if ("".equals(myTime))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Heure non valide","L'heure saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myTime);

			if (new Integer(myTime.substring(0,2)).intValue() > 8 
					&& new Integer(myTime.substring(0,2)).intValue() < 21 )
				EODialogs.runInformationDialog("Heure non valide","Vous ne pouvez pas programmer le calcul des comptes retraite entre 8H et 21H !");

			updateInterface();
		}
	}

	private class ActionListenerHeureTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			heureHasChanged(myTextField);
		}
	}
	private class FocusListenerHeureTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerHeureTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			heureHasChanged(myTextField);			
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.setTextToField(myView.getTfNom(), getCurrentFichier().cfimFichier());

		myView.getCheckProgrammation().setSelected(getCurrentFichier().estProgramme());

		CocktailUtilities.setTextToField(myView.getTfHeureCalcul(), getCurrentFichier().cfimHeureCalcul());

		if(getCurrentFichier().cfimType().equals(EOCirFichierImport.TYPE_FICHIER_CAMPAGNE))
			myView.getPopupTypes().setSelectedIndex(1);
		else
			if(getCurrentFichier().cfimType().equals(EOCirFichierImport.TYPE_FICHIER_IDENTITE))
				myView.getPopupTypes().setSelectedIndex(2);
			else
				myView.getPopupTypes().setSelectedIndex(3);

		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		CocktailUtilities.initTextField(myView.getTfHeureCalcul(), myView.getCheckProgrammation().isSelected() == false, myView.getCheckProgrammation().isSelected());
	}

	@Override
	protected void traitementsAvantValidation() {

		getCurrentFichier().setCfimHeureCalcul(CocktailUtilities.getTextFromField(myView.getTfHeureCalcul()));
		getCurrentFichier().setCfimFichier(CocktailUtilities.getTextFromField(myView.getTfNom()));
		getCurrentFichier().setCfimDate(DateCtrl.today());

		if (myView.getPopupTypes().getSelectedIndex() > 0) {
			switch(myView.getPopupTypes().getSelectedIndex()) {
			case 1: getCurrentFichier().setCfimType(EOCirFichierImport.TYPE_FICHIER_CAMPAGNE); break;
			case 2: getCurrentFichier().setCfimType(EOCirFichierImport.TYPE_FICHIER_IDENTITE);break;
			case 3: getCurrentFichier().setCfimType(EOCirFichierImport.TYPE_FICHIER_RECTIFICATIF);break;
			}
		}
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		// Programmation du calcul automatique
		if (getCurrentFichier().estProgramme()) {
			ServerProxy.clientSideRequestStartProgCir(getEdc(), getEdc().globalIDForObject(getCurrentFichier()));
		}
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentFichier(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}	

}

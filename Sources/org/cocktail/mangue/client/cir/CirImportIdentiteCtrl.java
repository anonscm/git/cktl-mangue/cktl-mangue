/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.cir;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.cir.CirImportIdentiteView;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.mangue.cir.EOCirIdentite;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class CirImportIdentiteCtrl extends ModelePageSaisie {

	private static CirImportIdentiteCtrl sharedInstance;
	private String messageImport = "";

	private CirImportIdentiteView myView;

	protected  	JFileChooser 		fileChooser, directoryChooser;
	public	FileReader				fileReader;
	public	BufferedReader			reader;		
	public	int						indexLigne;
	public	File					fichier;
	public	String					rapportErreurs;
	

	public CirImportIdentiteCtrl(EOEditingContext edc, CirIdentiteCtrl ctrlIdentite) {

		super(edc);		
		myView = new CirImportIdentiteView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		
		myView.getBtnGetFile().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getFile();
			}
		});
		myView.getBtnImporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				importer();
			}
		});

		// Creation d'un fileChooser pour choisir les fichiers et recuperer les icones
		fileChooser = new JFileChooser(ctrlIdentite.pathFichierCirIDE());
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		fileChooser.setMultiSelectionEnabled(false);

		CocktailUtilities.initTextField(myView.getTfDate(), false, false);
		myView.getTfDate().setText(DateCtrl.dateToString(new NSTimestamp()));

		myView.getBtnImporter().setEnabled(false);
		myView.getBtnGetDate().setEnabled(false);

	}

	public void open(Integer exercice  ) {
		CocktailUtilities.viderTextArea(myView.getConsole());
		myView.setVisible(true);
	}

	private void getFile() {

		if(fileChooser.showOpenDialog(myView) == JFileChooser.APPROVE_OPTION)	{
			myView.getTfFile().setText(fileChooser.getSelectedFile().getPath());

			if ( (myView.getTfFile().getText().toUpperCase()).indexOf("IDEXA") == -1) {

				EODialogs.runErrorDialog("ERREUR", "Le fichier à importer doit contenir la chaine de caractères 'idexa'.\nMerci de vérifier.");
				return;

			}

			myView.getBtnImporter().setEnabled(true);
		}

	}

	/**
	 * Import du fichier IDE 
	 */
	private void importer() {

		try  {
			fichier		= new File(myView.getTfFile().getText());
			fileReader 	= new FileReader(fichier);
			reader		= new BufferedReader(fileReader);
		}
		catch (Exception e)  {
			EODialogs.runErrorDialog("ERREUR","Impossible de lire le fichier sélectionné !");
			return;
		}

		CRICursor.setWaitCursor(myView);

		// On analyse chaque ligne du fichier ==> On affiche le resultat dans le rapport d'analyse.
		indexLigne = 0;
		try  {
			String ligne;
			NSArray champs = new NSArray();

			// recuperation de la premiere ligne du fichier
			ligne = reader.readLine();
			while (ligne != null) {				
				champs = StringCtrl.componentsSeparatedByString(ligne, CocktailExports.SEPARATEUR_EXPORT);
				traiterLigne(champs);
				ligne = reader.readLine();
				indexLigne ++;
			}

			EODialogs.runInformationDialog("OK","L'import du fichier identité a été effectué avec succès.\n Merci de valider ou d'annuler cette mise à jour.");
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR","Fichier erroné !\nImpossible de lire la ligne en cours.\n"+ e.getMessage());
			e.printStackTrace();
		}

		try {reader.close();fileReader.close();}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR","Impossible de refermer le fichier ouvert !");
		}

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * Traitement d'une ligne du fichier IDE. Recuperation de la date de certification si existante
	 * 	
	 * @param champs
	 * @throws Exception
	 */
	private void traiterLigne(NSArray champs) throws Exception {

		messageImport = "";
		String noInsee = (String)champs.get(0);
		String nomUsuel = (String)champs.get(4);
		String prenom = (String)champs.get(5);

		// On met a jour tous les comptes connus de l'individu
		NSArray<EOCirIdentite> identites = EOCirIdentite.rechercherComptesPourInsee(getEdc(), noInsee);
		for (EOCirIdentite identite : identites) {

			String valeurChamp = (String)champs.get((champs.size()) - 1);

			NSTimestamp dateCertification = DateCtrl.stringToDate(valeurChamp, "%d-%m-%Y");
			if (dateCertification == null)
				dateCertification = DateCtrl.stringToDate(valeurChamp, "%d/%m/%Y");

			if (identite.toIndividu().personnel().cirDCertification() == null) {
				identite.toIndividu().personnel().setCirDCertification(dateCertification);
				addToMessage(nomUsuel+" "+prenom+" \t- DATE CERTIFICATION INSEREE (" + DateCtrl.dateToString(dateCertification) + ")");
			}
			else {	
				if (!DateCtrl.isSameDay(dateCertification, identite.toIndividu().personnel().cirDCertification())) {
					addToMessage(nomUsuel+" "+prenom+" \t- DATE CERTIFICATION MODIFIEE (" + DateCtrl.dateToString(dateCertification) + " / " + DateCtrl.dateToString(identite.toIndividu().personnel().cirDCertification()) + ")");						
					identite.toIndividu().personnel().setCirDCertification(dateCertification);
				}
			}
		}	

		myView.getConsole().setText(myView.getConsole().getText() + messageImport+"\n");

	}


	/**
	 * 
	 * @param message
	 */
	private void addToMessage(String message) {
		messageImport = messageImport.concat(message);
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getConsole().setText("");

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		
	}

}
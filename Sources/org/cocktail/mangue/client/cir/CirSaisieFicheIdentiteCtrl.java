// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFicheIdentiteCtrl.java

package org.cocktail.mangue.client.cir;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.cir.CirSaisieFicheIdentiteView;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.cir.EOCirIdentite;

import com.webobjects.eocontrol.EOEditingContext;

public class CirSaisieFicheIdentiteCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;
	private static CirSaisieFicheIdentiteCtrl sharedInstance;
	private CirSaisieFicheIdentiteView myView;
	private EOCirIdentite currentCir;

	public CirSaisieFicheIdentiteCtrl(EOEditingContext edc) {

		super(edc);

		myView = new CirSaisieFicheIdentiteView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setDateListeners(myView.getTfDateCertif());
		setDateListeners(myView.getTfDateValidation());

		CocktailUtilities.initTextField(myView.getTfNomUsuel(), false, false);
		CocktailUtilities.initTextField(myView.getTfNomFamille(), false, false);
		CocktailUtilities.initTextField(myView.getTfDateCertif(), false, true);
		CocktailUtilities.initTextField(myView.getTfDateValidation(), false, true);

		myView.getCheckValide().setEnabled(false);
	}

	public static CirSaisieFicheIdentiteCtrl sharedInstance(EOEditingContext editingContext)
	{
		if(sharedInstance == null)
			sharedInstance = new CirSaisieFicheIdentiteCtrl(editingContext);
		return sharedInstance;
	}

	public EOCirIdentite getCurrentCir() {
		return currentCir;
	}

	public void setCurrentCir(EOCirIdentite currentCir) {
		this.currentCir = currentCir;
		updateDatas();
	}

	/**
	 * 
	 * @param cir
	 * @return
	 */
	public boolean modifier(EOCirIdentite cir) {

		setModeCreation(false);

		setCurrentCir(cir);
		myView.setTitle("DONNEES CIR - " + getCurrentCir().nomUsuel() + " " + getCurrentCir().prenomUsuel());

		myView.setVisible(true);
		return getCurrentCir() != null;
	}


	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfCommentaires());

		CocktailUtilities.viderTextField(myView.getTfInsee());
		CocktailUtilities.viderTextField(myView.getTfNomUsuel());
		CocktailUtilities.viderTextField(myView.getTfNomFamille());
		CocktailUtilities.viderTextField(myView.getTfDateCertif());
		CocktailUtilities.viderTextField(myView.getTfDateValidation());

		CocktailUtilities.viderTextField(myView.getTfNumen());
		CocktailUtilities.viderTextField(myView.getTfDateNaissance());
		CocktailUtilities.viderTextField(myView.getTfPaysNaissance());
		CocktailUtilities.viderTextField(myView.getTfDepartementNaissance());
		CocktailUtilities.viderTextField(myView.getTfVilleNaissance());

	}

	@Override
	protected void updateDatas() {

		clearDatas();

		if (getCurrentCir() != null) {

			CocktailUtilities.setTextToField(myView.getTfNomUsuel(), getCurrentCir().nomUsuel());
			CocktailUtilities.setTextToField(myView.getTfNomFamille(), getCurrentCir().nomPatronymique());

			CocktailUtilities.setTextToField(myView.getTfInsee(), getCurrentCir().indNoInsee());
			CocktailUtilities.setTextToField(myView.getTfNumen(), getCurrentCir().numen());
			CocktailUtilities.setTextToField(myView.getTfVilleNaissance(), getCurrentCir().villeDeNaissance());
			CocktailUtilities.setTextToField(myView.getTfCommentaires(), getCurrentCir().cirCommentaires());

			myView.getCheckValide().setSelected(getCurrentCir().estValide());

			if (currentCir.paysNaissance() != null)
				CocktailUtilities.setTextToField(myView.getTfPaysNaissance(), currentCir.paysNaissance().libelleLong());

			if (currentCir.departement() != null)
				CocktailUtilities.setTextToField(myView.getTfDepartementNaissance(), currentCir.departement().libelleLong());

			CocktailUtilities.setDateToField(myView.getTfDateCertif(), currentCir.toIndividu().personnel().cirDCertification());
			CocktailUtilities.setDateToField(myView.getTfDateValidation(), currentCir.toIndividu().personnel().cirDCompletude());
			CocktailUtilities.setTextToField(myView.getTfDateNaissance(), currentCir.dNaissance());
		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		//getCurrentCir().setNomUsuel(myView.getTfNomFamille().getText());
		getCurrentCir().toIndividu().personnel().setCirDCertification(CocktailUtilities.getDateFromField(myView.getTfDateCertif()));
		getCurrentCir().toIndividu().personnel().setCirDCompletude(CocktailUtilities.getDateFromField(myView.getTfDateValidation()));

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		setCurrentCir(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
	}
}
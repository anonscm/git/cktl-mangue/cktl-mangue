// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirEditionDonneesCtrl.java

package org.cocktail.mangue.client.cir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.JFrame;

import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.cir.CirEditionDonneesView;
import org.cocktail.mangue.common.utilities.AskForString;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriere;
import org.cocktail.mangue.modele.mangue.cir.EOCirCarriereData;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class CirEditionDonneesCtrl
{

	private static CirEditionDonneesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.TRUE;
	private EOEditingContext ec;
	private EODisplayGroup eodRubriques;
	private EODisplayGroup eodSousRubriques;
	private EOCirCarriereData currentRubrique;
	private CirEditionDonneesView myView;
	private boolean aCarriereATrous;

	private ListenerRubriques listenerRubriques = new ListenerRubriques();
	private ListenerSousRubriques listenerSousRubriques = new ListenerSousRubriques();

	private EOCirCarriere currentCarriere;
	private NSArray<EOCirCarriereData> currentCirDatas;


	public CirEditionDonneesCtrl(EOEditingContext editingContext) {

		ec = editingContext;
		eodRubriques = new EODisplayGroup();
		eodSousRubriques = new EODisplayGroup();
		myView = new CirEditionDonneesView(new JFrame(), MODE_MODAL.booleanValue(), eodRubriques, eodSousRubriques);

		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){valider();}}
		);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){exporter();}}
		);
		myView.getMyEOTableRubriques().addListener(listenerRubriques);
		myView.getMyEOTableSousRubriques().addListener(listenerSousRubriques);

		myView.getBtnExporter().setEnabled(false);

		CocktailUtilities.initTextArea(myView.getConsole(), false, false);
		
	}


	public static CirEditionDonneesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new CirEditionDonneesCtrl(editingContext);
		return sharedInstance;
	}

	public void open(EOCirCarriere carriere, NSArray cirDatas) {
		currentCarriere = carriere;
		currentCirDatas = cirDatas;

		CocktailUtilities.setTextToArea(myView.getConsole(), carriere.circCommentaires());

		actualiser();
		myView.setVisible(true);
	}

	private void actualiser()	{

		CRICursor.setWaitCursor(myView);

		aCarriereATrous = false;
		if (currentCirDatas. count() > 0) {
			EOIndividu individu = currentCirDatas.get(0).cirCarriere().toIndividu();
			myView.getTfTitre().setText("CIR - " + individu.prenom() + " " + individu.nomUsuel() + " - Détail des données carrière");
			NSMutableArray nomRubriques = new NSMutableArray();
			for(Enumeration<EOCirCarriereData> e = currentCirDatas.objectEnumerator(); e.hasMoreElements();) {

				EOCirCarriereData data = e.nextElement();
				if(!nomRubriques.containsObject(data.circdRubrique())) {
					NSMutableDictionary parametres = new NSMutableDictionary();
					parametres.setObjectForKey(data.circdClassement(), EOCirCarriereData.CIRCD_CLASSEMENT_KEY);
					parametres.setObjectForKey(data.circdRubrique(), EOCirCarriereData.CIRCD_RUBRIQUE_KEY);
					parametres.setObjectForKey(data.circdData(), EOCirCarriereData.CIRCD_DATA_KEY);
					nomRubriques.addObject(parametres);
				} else
					if(data.circdRubrique().equals("IdentiteAgent"))
						aCarriereATrous = true;

			}

			eodRubriques.setObjectArray(nomRubriques);
			myView.getMyEOTableRubriques().updateData();

		}
		CRICursor.setDefaultCursor(myView);

	}


	private void modifierSousRubrique() {

		NSDictionary dico = (NSDictionary)eodRubriques.selectedObject();
		NSDictionary dicoSousRubrique = (NSDictionary)eodSousRubriques.selectedObject();

		// On ne peut pour le moment modifier que les valeurs non nulles. Pas d'ajout possible. A FAIRE !!!
		String ancienneValeur = (String)dicoSousRubrique.objectForKey("valeur");
		if (ancienneValeur.length() > 0) {

			currentRubrique = EOCirCarriereData.findForCarriereEtClassement(ec, currentCarriere, (Integer)dico.objectForKey(EOCirCarriereData.CIRCD_CLASSEMENT_KEY));

			try {

				String datas = currentRubrique.circdData();			
				String nouvelleValeur = AskForString.sharedInstance().getString("VALEUR", "Nouvelle Valeur : ", (String)dicoSousRubrique.objectForKey("valeur"));
				datas = datas.replace("$"+ dicoSousRubrique.objectForKey("valeur") +"$", "$"+nouvelleValeur+"$");

				currentRubrique.setCircdData(datas);			
				ec.saveChanges();
				myView.setVisible(false);

			}
			catch (Exception ex) {
				ex.printStackTrace();
				ec.revert();
			}
		}

	}

	private void exporter()
	{
	}

	private void valider()
	{
		try
		{
			myView.setVisible(false);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private void annuler()
	{
		myView.setVisible(false);
	}

	private class ListenerRubriques implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick()
		{
		}

		public void onSelectionChanged() {
			try {
				NSDictionary dico = (NSDictionary)eodRubriques.selectedObject();
				if(dico != null) {
					eodSousRubriques.setObjectArray(ServerProxy.clientSideRequestGetSousRubriques(ec, (String)dico.objectForKey(EOCirCarriereData.CIRCD_RUBRIQUE_KEY), (String)dico.objectForKey(EOCirCarriereData.CIRCD_DATA_KEY)));
					myView.getMyEOTableSousRubriques().updateData();
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class ListenerSousRubriques implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick()	{
			modifierSousRubrique();
		}

		public void onSelectionChanged()
		{
			try	{
				NSDictionary dico = (NSDictionary)eodSousRubriques.selectedObject();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	private class PopupTypeRubriqueListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			actualiser();
		}
	}

}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.cir;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ConsoleTraitementCtrl;
import org.cocktail.mangue.client.gui.cir.CirIdentiteView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.mangue.common.utilities.CocktailIcones;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel;
import org.cocktail.mangue.modele.mangue.cir.EOCirFichierImport;
import org.cocktail.mangue.modele.mangue.cir.EOCirIdentite;
import org.cocktail.mangue.server.cir.ConstantesCir;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class CirIdentiteCtrl  {

	private static final String DELIMITEUR_IDE = "$";

	private EOEditingContext edc;
	private EODisplayGroup eod;
	private ListenerCir listenerCir;
	private CirIdentiteView myView;
	private EOCirIdentite currentCir;
	private ConsoleTraitementCtrl 		myConsole;
	private boolean 	traitementServeurEnCours, preparationEnCours = false;
	private CirImportIdentiteCtrl ctrlImport;
	private CirCtrl ctrlParent;

	public CirIdentiteCtrl(EOEditingContext edc, CirCtrl ctrlParent) {

		this.edc = edc;

		eod = new EODisplayGroup();
		myView = new CirIdentiteView(eod);
		
		this.ctrlParent = ctrlParent;
		
		ctrlImport = new CirImportIdentiteCtrl(edc, this);

		listenerCir = new ListenerCir();
		
		myConsole = new ConsoleTraitementCtrl(edc);
		myConsole.setCtrlParent(this, "Traitement des données de certification ...");

		myView.getBtnPreparerDonnees().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){preparerDonnees();}}
				);
		myView.getBtnInvalider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){invalider();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){supprimer();}}
				);
		myView.getBtnPreparerFichier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){preparerFichier();}}
				);
		myView.getBtnRepreparer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){repreparer();}}
				);
		myView.getBtnImprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
				);
		myView.getBtnImporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {importerFichierIDE();}}
				);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporter();}}
				);
		myView.getBtnRechercher().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {rechercher();}}
				);

		eod.setSortOrderings(new NSArray(new EOSortOrdering(EOCirIdentite.NOM_USUEL_KEY, EOSortOrdering.CompareAscending)));
		myView.getMyEOTable().addListener(listenerCir);

		myView.getBtnSupprimer().setVisible(false);

		myView.getCheckValide().setSelected(true);
		myView.getCheckValide().addActionListener(new PopupValiditeListener());
		myView.getPopupCertifies().addActionListener(new PopupCertifiesListener());
		myView.getPopupCompletudes().addActionListener(new PopupCertifiesListener());

		myView.getTfFiltreNom().addActionListener(new MyActionFiltreListener());
		myView.getTfFiltrePrenom().addActionListener(new MyActionFiltreListener());

	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public JPanel getView() {
		return myView;
	}

	public EOCirIdentite getCurrentCir() {
		return currentCir;
	}
	public void setCurrentCir(EOCirIdentite currentCir) {
		this.currentCir = currentCir;
	}
	public boolean isTraitementServeurEnCours() {
		return traitementServeurEnCours;
	}

	public void setTraitementServeurEnCours(boolean traitementServeurEnCours) {
		this.traitementServeurEnCours = traitementServeurEnCours;
	}

	public boolean isPreparationEnCours() {
		return preparationEnCours;
	}

	public void setPreparationEnCours(boolean preparationEnCours) {
		this.preparationEnCours = preparationEnCours;
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier() {

		NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.CIR_ANNEE_KEY + " = %@", new NSArray<Integer>(ctrlParent.getCurrentExerciceIdentite())));

		if(myView.getCheckValide().isSelected())
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.CIR_VALIDE_KEY + " = %@", new NSArray<String>(CocktailConstantes.VRAI)));
		else
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.CIR_VALIDE_KEY + " = %@", new NSArray<String>(CocktailConstantes.FAUX)));

		if(myView.getTfFiltreNom().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray<String>("*" + myView.getTfFiltreNom().getText() + "*")));

		if(myView.getTfFiltrePrenom().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.PRENOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray<String>("*" + myView.getTfFiltrePrenom().getText() + "*")));

		if(myView.getPopupCertifies().getSelectedIndex() > 0) {

			if ("OUI".equals(myView.getPopupCertifies().getSelectedItem()))
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.TO_INDIVIDU_KEY+"."+EOIndividu.PERSONNELS_KEY+"."+EOPersonnel.CIR_D_CERTIFICATION_KEY + " != nil", null));
			else
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.TO_INDIVIDU_KEY+"."+EOIndividu.PERSONNELS_KEY+"."+EOPersonnel.CIR_D_CERTIFICATION_KEY + " = nil", null));
		}

		if(myView.getPopupCompletudes().getSelectedIndex() > 0) {

			if ("OUI".equals(myView.getPopupCompletudes().getSelectedItem()))
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.TO_INDIVIDU_KEY+"."+EOIndividu.PERSONNELS_KEY+"."+EOPersonnel.CIR_D_COMPLETUDE_KEY + " != nil", null));
			else
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCirIdentite.TO_INDIVIDU_KEY+"."+EOIndividu.PERSONNELS_KEY+"."+EOPersonnel.CIR_D_CERTIFICATION_KEY + " = nil", null));

		}

		return new EOAndQualifier(mesQualifiers);

	}


	/**
	 * 
	 */
	private void invalider() {

		try {

			if (getCurrentCir().estValide() == false) {

				boolean dialog = EODialogs.runConfirmOperationDialog("Validation ...","Voulez vous vraiment revalider ce compte retraite ?","OUI","NON");
				if (dialog)
					getCurrentCir().setEstValide(true);
			}
			else {
				boolean dialog = EODialogs.runConfirmOperationDialog("Suppression ...","Voulez vous vraiment historiser ce compte retraite ?","OUI","NON");
				if (dialog)
					getCurrentCir().setEstValide(false);

			}

			getEdc().saveChanges();

			actualiser();
			CirCtrl.sharedInstance(getEdc()).getView().toFront();
		}
		catch(ValidationException ex) {

		}
		catch(Exception ex) {
			getEdc().revert();
			ex.printStackTrace();
		}
	}


	/**
	 * 
	 */
	private void repreparer() {

		CRICursor.setWaitCursor(myView);

		try {

			for (EOCirIdentite myIdentite : (NSArray<EOCirIdentite>)eod.selectedObjects()) {
				myIdentite.reinitAvecIndividu(myIdentite.toIndividu(), ctrlParent.getCurrentExerciceIdentite());
			}

			getEdc().saveChanges();

			EODialogs.runInformationDialog("CIR","Les données ont été recalculées pour les individus sélectionnés !");
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		myView.getMyEOTable().updateData();

		CirCtrl.sharedInstance(getEdc()).getView().toFront();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void preparerDonnees() {
		try {
			
			if (ctrlParent.getCurrentExerciceIdentite() == null) {
				EODialogs.runErrorDialog("ERREUR", "Vous devez spécifier une année !");
				return;
			}
			
			preparationEnCours = true;
			setTraitementServeurEnCours(true);

			Class classeParametres[];
			Object parametres[];
			classeParametres = (new Class[] {
					Integer.class
			});
			parametres = (new Object[] {ctrlParent.getCurrentExerciceIdentite()});

			myConsole.lancerTraitement("clientSideRequestPreparerCertifications", classeParametres, parametres);

		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void actualiser() {

		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(new NSArray());
		rechercher();

		myView.getMyEOTable().updateData();

		updateInterface();
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void importerFichierIDE() {
		ctrlImport.open(ctrlParent.getCurrentExerciceIdentite());
		myView.getMyEOTable().updateData();
	}

	/**
	 * 
	 */
	private void modifier() {
		if(CirSaisieFicheIdentiteCtrl.sharedInstance(edc).modifier(getCurrentCir())) {
			myView.getMyEOTable().updateUI();
		}
	}

	/**
	 * On ne supprime que les comptes qui n'ont pas ete certifies.
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer ce(s) Compte(s) Retraite ?", "Oui", "Non"))
			return;

		CRICursor.setWaitCursor(myView);
		try {

			for(EOCirIdentite myIdentite : (NSArray<EOCirIdentite>)eod.selectedObjects()) {
				if (myIdentite.toIndividu().personnel().cirDCertification() == null 
						&& myIdentite.toIndividu().personnel().cirDCompletude() == null)
					getEdc().deleteObject(myIdentite);
			}

			getEdc().saveChanges();

			EODialogs.runInformationDialog("OK", "Les comptes sélectionnés (Non Certifiés ET Non Validés) ont bien été supprimés");

			actualiser();
			myView.getMyEOTable().updateUI();
			myView.getMyEOTable().updateData();

			myView.getTfMessage().setText("** " + eod.displayedObjects().size() + " Agents ( Année de référence : " + ctrlParent.getCurrentExerciceIdentite() + ")");
			CirCtrl.sharedInstance(getEdc()).getView().toFront();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 * @return
	 */
	private String enteteFichier() {

		return  "00$" + EOGrhumParametres.getUniteGestionCir() + "$" + DateCtrl.dateToString(new NSTimestamp(), "%Y%m%d") + "$" + "V1.11" + "\n";

	}
	/**
	 * 
	 * @return
	 */
	private String finDeFichier(int nbIndividus) {

		String texte = "99$" + String.valueOf(nbIndividus) + "\n";

		return texte;
	}

	/**
	 * 
	 * @return
	 */
	private String texteExport() {

		String texte = enteteFichier();

		int nbIndividus = 0;
		for(EOCirIdentite identite : (NSArray<EOCirIdentite>)eod.displayedObjects()) {

			if(identite.toIndividu().personnel().isIdentiteCertifiee() == false) {
				nbIndividus++;
				texte = texte.concat(texteExportPourIndividu(identite));
			}
		}

		texte = texte.concat(finDeFichier(nbIndividus));
		return texte;
	}

	/**
	 * 
	 * @param compte
	 * @return
	 */
	private String texteExportPourIndividu(EOCirIdentite compte) {

		String texte = ConstantesCir.CODE_ARTICLE_IDENTITE_AGENT;

		texte += DELIMITEUR_IDE;		
		if(compte.indNoInsee() != null)
			texte += compte.indNoInsee();

		texte += DELIMITEUR_IDE;		
		if(compte.numen() != null)
			texte += compte.numen();

		texte += DELIMITEUR_IDE;		
		if(compte.nomPatronymique() != null)
			texte+=compte.nomPatronymique();

		texte += DELIMITEUR_IDE;					
		texte += compte.nomUsuel();

		texte += DELIMITEUR_IDE;		
		texte += compte.nomUsuel();

		texte += DELIMITEUR_IDE;		
		texte += compte.prenoms();

		texte += DELIMITEUR_IDE;		
		texte += compte.prenomUsuel();

		texte += DELIMITEUR_IDE;		
		texte += compte.sexe();		

		texte += DELIMITEUR_IDE;		
		if(compte.dNaissance() != null)
			texte += DateCtrl.dateToString(DateCtrl.stringToDate(compte.dNaissance()), "%Y%m%d");

		texte += DELIMITEUR_IDE;		
		if(compte.departement() != null)
			texte += compte.departement().code();
		else
			texte += ConstantesCir.DEPT_ETRANGER;

		texte += DELIMITEUR_IDE;		
		if(compte.villeDeNaissance() != null)
			texte += compte.villeDeNaissance();

		texte += DELIMITEUR_IDE;		
		if(compte.paysNaissance() != null && !compte.toIndividu().neEnFrance())
			texte += "99" + compte.paysNaissance().code().replaceAll("999", "000");

		texte += DELIMITEUR_IDE;		
		texte += "\n";

		return texte;
	}

	/**
	 * 
	 */
	private void exporter() {

		try {

			CRICursor.setWaitCursor(myView);
			String texte = enteteExport();

			for (EOCirIdentite myRecord : (NSArray<EOCirIdentite>)eod.displayedObjects()) {
				texte = texte.concat(texteExportPourRecord(myRecord) + CocktailExports.SAUT_DE_LIGNE);
			}

			UtilitairesFichier.enregistrerFichier(texte, pathFichierCirIDE(), "ExportIDE", "csv", false);

			CocktailUtilities.openFile(pathFichierCirIDE() + "/ExportIDE.csv");
			CRICursor.setDefaultCursor(myView);

		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @return
	 */
	private String enteteExport() {

		String entete = "";

		entete += "NOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "NOM FAMILLE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PRENOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "INSEE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "NAISSANCE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "COMPLETUDE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "CERTIFICATION";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "VERIFICATION";
		entete += CocktailExports.SAUT_DE_LIGNE;

		return entete;

	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EOCirIdentite record)    {

		String texte = "";

		// NOM
		texte += CocktailExports.ajouterChamp(record.toIndividu().nomUsuel());
		// PRENOM
		texte += CocktailExports.ajouterChamp(record.toIndividu().nomPatronymique());
		// NOM DE FAMILLE
		texte += CocktailExports.ajouterChamp(record.toIndividu().prenom());
		// INSEE
		texte += CocktailExports.ajouterChamp("'"+record.toIndividu().indNoInsee()+"'");
		// DATE NAISSANCE
		texte += CocktailExports.ajouterChamp(record.dNaissance());

		texte += CocktailExports.ajouterChampDate(record.toIndividu().personnel().cirDCompletude());
		texte += CocktailExports.ajouterChampDate(record.toIndividu().personnel().cirDCertification());
		texte += CocktailExports.ajouterChampDate(record.toIndividu().personnel().cirDVerification());

		return texte;
	}



	/**	
	 *
	 */
	private void imprimer() {

		CRICursor.setWaitCursor(myView);

		NSTimestamp dateOuverture = DateCtrl.debutAnnee(ctrlParent.getCurrentExerciceIdentite());
		NSTimestamp dateCloture = DateCtrl.finAnnee(ctrlParent.getCurrentExerciceIdentite());

		InfoPourEditionRequete infoEdition = new InfoPourEditionRequete("CirIde","CIR - Fichier Identité", 
				dateOuverture,
				dateCloture);

		try {

			UtilitairesImpression.imprimerSansDialogue(
					edc,
					"clientSideRequestImprimerCirIdentite",
					new Class[] {InfoPourEditionRequete.class, NSArray.class,Boolean.class},
					new Object[]{infoEdition, Utilitaires.tableauDeGlobalIDs(eod.displayedObjects(),edc), true},
					"Cir-IDE", "CIR Fichier Identité");

			CRICursor.setDefaultCursor(myView);

		} catch (Exception e) {

			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());

		}
	}


	/**
	 * Export du fichier texte IDE
	 */
	private void preparerFichier()	{

		CRICursor.setWaitCursor(myView);
		try {
			String nomFichierPourEnvoi = EOCirFichierImport.nomFichierEnvoi(edc, null);
			if(nomFichierPourEnvoi.startsWith("EM000")) {
				EODialogs.runErrorDialog("Erreur", "Le paramètre 'UNITE_GESTION' n'est pas ou mal défini dans les paramètres de Mangue");
				CRICursor.setDefaultCursor(myView);
				return;
			}

			String result = texteExport();
			if(result != null) {
				UtilitairesFichier.enregistrerFichier(result, pathFichierCirIDE(), nomFichierPourEnvoi, "", false);
				EODialogs.runInformationDialog("Fichier IDE", "Le fichier de certification d'identités a été préparé et copié dans le répertoire : " + pathFichierCirIDE());
			}	
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 * @return
	 */
	public String pathFichierCirIDE() {

		String pathDir = (((ApplicationClient)EOApplication.sharedApplication()).directoryExports()) +
				System.getProperty("file.separator") + 
				"CIR" + 
				System.getProperty("file.separator") +
				"IDE" + 
				System.getProperty("file.separator");
		UtilitairesFichier.verifierPathEtCreer(pathDir);

		return pathDir;

	}

	/**
	 * 
	 */
	private void rechercher() {

		CRICursor.setWaitCursor(myView);

		eod.setObjectArray(EOCirIdentite.findForQualifier(getEdc(), getFilterQualifier()));
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();

		int totalAgents = eod.allObjects().size();

		myView.getTfMessage().setText("** " + eod.displayedObjects().size() + " Agents / " + totalAgents + " ( Année de référence : " + ctrlParent.getCurrentExerciceIdentite().intValue() + ")");

		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void updateInterface() {

		if (!myView.getCheckValide().isSelected()) {
			myView.getBtnInvalider().setIcon(CocktailIcones.ICON_COCHE);
			myView.getBtnInvalider().setToolTipText("Re-valider le compte retraite sélectionné");
		}
		else {
			myView.getBtnInvalider().setIcon(CocktailIcones.ICON_CANCEL);
			myView.getBtnInvalider().setToolTipText("Invalider le compte retraite sélectionné");
		}

		myView.getBtnModifier().setEnabled(getCurrentCir() != null);
		myView.getBtnImporter().setEnabled(true);
		myView.getBtnSupprimer().setEnabled(getCurrentCir() != null);
		myView.getBtnInvalider().setEnabled(getCurrentCir() != null);
		myView.getBtnRepreparer().setEnabled(getCurrentCir() != null);
		myView.getBtnPreparerDonnees().setEnabled(true);
		myView.getBtnPreparerFichier().setEnabled(eod.displayedObjects().size() > 0);

	}

	private class PopupValiditeListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {

			if (myView.getCheckValide().isSelected())
				myView.getBtnInvalider().setIcon(CocktailIcones.ICON_DELETE);
			else
				myView.getBtnInvalider().setIcon(CocktailIcones.ICON_VALID);

			rechercher();
		}
	}
	private class PopupCertifiesListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {
			rechercher();
		}
	}

	private class ListenerCir
	implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
			if(getCurrentCir() != null)
				modifier();
		}

		public void onSelectionChanged() {
			setCurrentCir((EOCirIdentite)eod.selectedObject());
			updateInterface();
		}
	}
	
	/**
	 * 
	 */
	public void terminerTraitementServeur() {		

		try {
						
			setTraitementServeurEnCours(false);
			setPreparationEnCours(false);
			
			myConsole.addToMessage("\n >> TRAITEMENT TERMINE !");
			actualiser();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * Classe d'écoute pour lancer la recherche
	 * @author cpinsard
	 */
	private class MyActionFiltreListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			rechercher();
		}
	}

}

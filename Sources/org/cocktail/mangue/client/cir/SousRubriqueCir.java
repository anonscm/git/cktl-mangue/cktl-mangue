// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SousRubriqueCir.java

package org.cocktail.mangue.client.cir;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            SousRubriqueCirAvecValeur

public class SousRubriqueCir
    implements NSKeyValueCoding, NSCoding
{

    public SousRubriqueCir(int numeroOrdre, String cle, boolean estObligatoire, boolean aLongueurFixe, int longueur)
    {
        this.numeroOrdre = numeroOrdre;
        this.cle = cle;
        this.estObligatoire = estObligatoire;
        this.aLongueurFixe = aLongueurFixe;
        this.longueur = longueur;
        classeAssociee = null;
    }

    public String classeAssociee()
    {
        return classeAssociee;
    }

    public void setClasseAssociee(String classeAssociee)
    {
        this.classeAssociee = classeAssociee;
    }

    public int numeroOrdre()
    {
        return numeroOrdre;
    }

    public int longueur()
    {
        return longueur;
    }

    public String nature()
    {
        return nature;
    }

    public void setNature(String nature)
    {
        this.nature = nature;
    }

    public String typeDonnee()
    {
        return typeDonnee;
    }

    public void setTypeDonnee(String typeDonnee)
    {
        this.typeDonnee = typeDonnee;
    }

    public String valeur()
    {
        return valeur;
    }

    public void setValeur(String valeur)
    {
        this.valeur = valeur;
    }

    public boolean aLongueurFixe()
    {
        return aLongueurFixe;
    }

    public String cle()
    {
        return cle;
    }

    public String aide()
    {
        return aide;
    }

    public void setAide(String uneAide)
    {
        aide = uneAide;
    }

    public boolean estObligatoire()
    {
        return estObligatoire;
    }

    public SousRubriqueCirAvecValeur preparer(Object parametres[], Object classes[])
        throws Exception
    {
        Object resultat = null;
        if(nature().equals("constante"))
        {
            resultat = new String(valeur());
            if(resultat.equals("Vide"))
                resultat = null;
        } else
        if(nature().equals("champ"))
            resultat = trouverValeurReelle(parametres, classes);
        else
            resultat = invoquerMethode(parametres, classes);
        if(resultat != null)
        {
            String stringResultat = validerValeur(resultat);
            SousRubriqueCirAvecValeur sousRubrique = new SousRubriqueCirAvecValeur(cle(), stringResultat);
            return sousRubrique;
        }
        if(estObligatoire())
            throw new Exception((new StringBuilder("\tVous devez fournir une valeur pour ")).append(valeur()).append(" pour la sous-rubrique ").append(cle()).toString());
        else
            return null;
    }

    public String validerValeur(Object resultat)
        throws Exception
    {
        String stringResultat = null;
        if(resultat != null)
            if(nature().equals("constante"))
                stringResultat = (String)resultat;
            else
            if(typeDonnee() != null)
                if(typeDonnee().equals("booleen"))
                {
                    if(resultat instanceof Boolean)
                    {
                        if(((Boolean)resultat).booleanValue())
                            stringResultat = "1";
                        else
                            stringResultat = "0";
                    } else
                    if(resultat instanceof Number)
                        stringResultat = resultat.toString();
                    else
                    if(resultat.equals("O"))
                        stringResultat = "1";
                    else
                    if(resultat.equals("N"))
                        stringResultat = "0";
                } else
                if(typeDonnee().equals("date"))
                {
                    if(resultat.getClass().getName().equals("com.webobjects.foundation.NSTimestamp"))
                    {
                        stringResultat = DateCtrl.dateToString((NSTimestamp)resultat, "%Y%m%d");
                    } else
                    {
                        if(!resultat.getClass().getName().equals("java.lang.String"))
                            throw new Exception((new StringBuilder("Valeur Date attendue pour la sous-rubrique ")).append(cle()).append(" et la valeur ").append(valeur()).toString());
                        stringResultat = (String)resultat;
                    }
                } else
                {
                    if(!resultat.getClass().getName().equals("java.lang.String"))
                        stringResultat = resultat.toString();
                    else
                        stringResultat = (String)resultat;
                    stringResultat = formaterString(stringResultat);
                }
        if((stringResultat == null || stringResultat.equals("")) && estObligatoire())
            throw new Exception((new StringBuilder("\tPour la sous-rubrique ")).append(cle()).append(" et la valeur ").append(valeur()).append(" le resultat ne peut etre nul.").toString());
        if(stringResultat != null)
            if(aLongueurFixe())
            {
                if(stringResultat.length() != longueur())
                    throw new Exception((new StringBuilder("\tLongueur invalide pour la sous-rubrique ")).append(cle()).append(" et la valeur ").append(valeur()).append(" : ").append(stringResultat).toString());
            } else
            if(stringResultat.length() > longueur())
                if(typeDonnee().equals("texte"))
                {
                    stringResultat = stringResultat.substring(0, longueur());
                    stringResultat = stringResultat.trim();
                } else
                {
                    throw new Exception("Le résultat évalué pour la sous-rubrique " + cle() + "( " + stringResultat.length() + ") et la valeur " + valeur() + " dépasse la longueur attendue (" + longueur() + ")");
                }
        return stringResultat;
    }

    public String toString()
    {
        String texte = (new StringBuilder(String.valueOf(shortString()))).append(", valeur \"").append(valeur()).append("\" nature \"").append(nature()).append("\" longueur ").append(longueur()).append(" type \"").append(typeDonnee()).toString();
        if(classeAssociee() != null)
            texte = texte + "\" classe \"" + classeAssociee() + "\"";
        else
            texte = (new StringBuilder(String.valueOf(texte))).append("\"").toString();
        return texte;
    }

    public String shortString()
    {
        String texte = (new StringBuilder("cle \"")).append(cle).toString();
        if(estObligatoire())
            texte = (new StringBuilder(String.valueOf(texte))).append("\", obligatoire").toString();
        else
            texte = (new StringBuilder(String.valueOf(texte))).append("\"").toString();
        if(aLongueurFixe())
            texte = (new StringBuilder(String.valueOf(texte))).append(", longueur fixe").toString();
        return texte;
    }

    public void takeValueForKey(Object valeur, String cle)
    {
        com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, valeur, cle);
    }

    public Object valueForKey(String cle)
    {
        return com.webobjects.foundation.NSKeyValueCoding.DefaultImplementation.valueForKey(this, cle);
    }

    public Class classForCoder()
    {
        return SousRubriqueCir.class;
    }

    public void encodeWithCoder(NSCoder coder)
    {
        coder.encodeInt(numeroOrdre());
        coder.encodeObject(classeAssociee());
        coder.encodeInt(longueur());
        coder.encodeObject(nature());
        coder.encodeObject(typeDonnee());
        coder.encodeObject(valeur());
        coder.encodeBoolean(aLongueurFixe());
        coder.encodeObject(cle());
        coder.encodeObject(aide());
        coder.encodeBoolean(estObligatoire());
    }

    public static Object decodeObject(NSCoder coder)
    {
        int numeroOrdre = coder.decodeInt();
        String classe = (String)coder.decodeObject();
        int longueur = coder.decodeInt();
        String nature = (String)coder.decodeObject();
        String typeDonnee = (String)coder.decodeObject();
        String valeur = (String)coder.decodeObject();
        boolean aLongueurFixe = coder.decodeBoolean();
        String cle = (String)coder.decodeObject();
        String aide = (String)coder.decodeObject();
        boolean estObligatoire = coder.decodeBoolean();
        SousRubriqueCir sousRubrique = new SousRubriqueCir(numeroOrdre, cle, estObligatoire, aLongueurFixe, longueur);
        sousRubrique.setAide(aide);
        sousRubrique.setNature(nature);
        sousRubrique.setClasseAssociee(classe);
        sousRubrique.setTypeDonnee(typeDonnee);
        sousRubrique.setValeur(valeur);
        return sousRubrique;
    }

    private Object trouverValeurReelle(Object parametres[], Object classes[])
        throws Exception
    {
        Object parametre = trouverParametre(parametres, classes);
        if(parametre != null)
        {
            Object valeur = ((NSKeyValueCoding)parametre).valueForKey(valeur());
            if(valeur != null && (valeur instanceof String) && valeur.equals(""))
                valeur = null;
            return valeur;
        } else
        {
            return null;
        }
    }

    private Object invoquerMethode(Object parametres[], Object classes[])
        throws Exception
    {
        Object parametre = trouverParametre(parametres, classes);
        if(parametre != null)
            try
            {
                Method methode = parametre.getClass().getMethod(valeur(), null);
                Object resultat = methode.invoke(parametre, null);
                if(resultat != null && (resultat instanceof String) && resultat.equals(""))
                    resultat = null;
                return resultat;
            }
            catch(SecurityException e1)
            {
                throw new Exception((new StringBuilder("La methode ")).append(valeur()).append(" de la classe ").append(parametre.getClass().getName()).append(" dans la sous-rubrique ").append(cle()).append(" n'est pas publique").toString());
            }
            catch(NoSuchMethodException e1)
            {
                throw new Exception((new StringBuilder("La methode ")).append(valeur()).append(" de la classe ").append(parametre.getClass().getName()).append(" dans la sous-rubrique ").append(cle()).append(" inconnue").toString());
            }
            catch(IllegalArgumentException e)
            {
                throw new Exception((new StringBuilder("Arguments erronnes pour la methode ")).append(valeur()).append(" de la classe ").append(parametre.getClass().getName()).append(" dans la sous-rubrique ").append(cle()).toString());
            }
            catch(IllegalAccessException e)
            {
                throw new Exception((new StringBuilder("La methode ")).append(valeur()).append(" de la classe ").append(parametre.getClass().getName()).append(" dans la sous-rubrique ").append(cle()).append(" n'est pas publique").toString());
            }
            catch(InvocationTargetException e)
            {
                throw new Exception(e.getCause().getMessage());
            }
        else
            return null;
    }

    private Object trouverParametre(Object parametres[], Object classes[])
        throws Exception
    {
        if(classeAssociee() == null)
            throw new Exception((new StringBuilder("Classe non definie pour la sous-rubrique ")).append(cle()).append(" et la valeur ").append(valeur()).toString());
        for(int i = 0; i < classes.length; i++)
        {
            Class classeParametre = (Class)classes[i];
            if(classeParametre.getName().equals(classeAssociee()))
                return parametres[i];
        }

        return null;
    }

    private String formaterString(String uneChaine)
    {
        if(uneChaine.equals(" "))
            return null;
        String resultat = uneChaine;
        if(resultat.indexOf(" -") >= 0)
            resultat = resultat.replaceAll(" -", "-");
        if(resultat.indexOf("- ") >= 0)
            resultat = resultat.replaceAll("- ", "-");
        if(resultat.indexOf(" - ") >= 0)
            resultat = resultat.replaceAll(" - ", "-");
        if(resultat.indexOf(":") >= 0)
            resultat = resultat.replaceAll(":", " ");
        if(resultat.indexOf("\"") >= 0)
            resultat = resultat.replaceAll("\"", "");
        if(resultat.indexOf("&") >= 0)
            resultat = resultat.replaceAll("&", "");
        if(resultat.indexOf("+") >= 0)
            resultat = resultat.replaceAll("+", "");
        if(resultat.indexOf("=") >= 0)
            resultat = resultat.replaceAll("=", "");
        if(resultat.indexOf(">") >= 0)
            resultat = resultat.replaceAll(">>", "");
        if(resultat.indexOf("<") >= 0)
            resultat = resultat.replaceAll("<", "");
        if(resultat.indexOf("'") >= 0)
            resultat = resultat.replaceAll("'", " ");
        if(resultat.indexOf("'") >= 0)
            resultat = resultat.replaceAll("'", " ");
        if(resultat.indexOf("$") >= 0)
            resultat = resultat.replaceAll("$", "");
        if(!typeDonnee.equals("texte") && !typeDonnee.equals("adresse") && !typeDonnee.equals("adresse_email") && resultat.indexOf(".") >= 0)
            resultat = resultat.replaceAll("\\.", " ");
        if(typeDonnee().equals("texte"))
        {
            resultat = resultat.replaceAll("0", "");
            resultat = resultat.replaceAll("1", "");
            resultat = resultat.replaceAll("2", "");
            resultat = resultat.replaceAll("3", "");
            resultat = resultat.replaceAll("4", "");
            resultat = resultat.replaceAll("5", "");
            resultat = resultat.replaceAll("6", "");
            resultat = resultat.replaceAll("7", "");
            resultat = resultat.replaceAll("8", "");
            resultat = resultat.replaceAll("9", "");
        }
        if(resultat.indexOf("__") >= 0)
            resultat = resultat.replaceAll("__", "_");
        resultat = resultat.replaceAll("\\(", "");
        resultat = resultat.replaceAll("\\)", "");
        resultat = resultat.replaceAll("\\[", "");
        resultat = resultat.replaceAll("\\]", "");
        for(resultat = resultat.trim(); resultat.indexOf("  ") >= 0; resultat = resultat.replaceAll("  ", " "));
        return resultat;
    }

    private String cle;
    private boolean estObligatoire;
    private boolean aLongueurFixe;
    private String valeur;
    private String nature;
    private String typeDonnee;
    private String aide;
    private int longueur;
    private int numeroOrdre;
    private String classeAssociee;
    private static final String SOUS_RUBRIQUE_VIDE = "Vide";
}

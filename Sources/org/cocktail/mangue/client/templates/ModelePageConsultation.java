package org.cocktail.mangue.client.templates;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Classe d'un modèle de page de consultation
 * 
 * @author Chama LAATIK
 *
 */
public abstract class ModelePageConsultation {
	private EOEditingContext edc;
	private boolean peutGererModule;
	
	protected abstract void updateInterface();
	/** Rafraichit toute l'interface */
	protected abstract void actualiser();
	protected abstract void updateDatas();
	protected abstract EOQualifier filterQualifier();

	protected abstract void traitementsPourCreation();
	protected abstract void traitementsApresCreation();
	protected abstract void traitementsPourModification();
	protected abstract void traitementsPourSuppression();
	protected abstract void traitementsApresSuppression();
	
	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	public ModelePageConsultation(EOEditingContext edc) {
		this.edc = edc;
	}
	
	/**
	 * Action d'ajouter
	 */
	protected void ajouter()	{
		traitementsPourCreation();
		actualiser();
		traitementsApresCreation();
	}
	
	/**
	 * Action de modifier
	 */
	protected void modifier() {
		traitementsPourModification();
	}
	
	/**
	 * Action de supprimer
	 */
	protected void supprimer() {
		if (!EODialogs.runConfirmOperationDialog("Attention", 
				"Confirmez-vous cette suppression ?", "Oui", "Non")) {		
			return;			
		}
		
		try {
			traitementsPourSuppression();

			edc.saveChanges();

			traitementsApresSuppression();
			actualiser();
			
		} catch (Exception e) {
			edc.revert();	
		}
	}
	
	protected boolean peutGererModule() {
		return peutGererModule;
	}
	
	protected void setPeutGererModule(boolean yn) {
		peutGererModule = true;
	}
	
	/**
	 * Action d'écoute sur le bouton ajouter
	 * @param myButton : bouton
	 */
	public void setActionBoutonAjouterListener(JButton myButton) {
		myButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { ajouter(); } });
	}
	
	/**
	 * Action d'écoute sur le bouton modifier
	 * @param myButton : bouton
	 */
	public void setActionBoutonModifierListener(JButton myButton) {
		myButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { modifier(); } });
	}
	
	/**
	 * Action d'écoute sur le bouton supprimer
	 * @param myButton : bouton
	 */
	public void setActionBoutonSupprimerListener(JButton myButton) {
		myButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { supprimer(); } });
	}
	
	/**
	 * Met à jour la date
	 * @param myTextField : date saisie
	 */
	public void setDateListeners(JTextField myTextField) {
		setFocusDateListener(myTextField);
		setActionDateListener(myTextField);
	}
	
	/**
	 * Met à jour la date 
	 * @param myTextField : date saisie
	 */
	public void setActionDateListener(JTextField myTextField) {
		myTextField.addActionListener(new ActionListenerDateTextField(myTextField));
	}
	
	/**
	 * Met à jour la date + controle sur la validité de la date saisie
	 * @param myTextField : date saisie
	 */
	public void setFocusDateListener(JTextField myTextField) {
		myTextField.addFocusListener(new FocusListenerDateTextField(myTextField));
	}
	
	
	/**
	 * Gestion de la completion automatique des champs de format date
	 * 
	 * @param myTextField
	 */
	private void dateHasChanged(JTextField myTextField) {
		//Tenter le StringUtils.isEmpty
		if ("".equals(myTextField.getText())) {
			return;
		}

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate)) {
			myTextField.selectAll();
			EODialogs.runInformationDialog(CocktailConstantes.DATE_NON_VALIDE_TITRE, CocktailConstantes.DATE_NON_VALIDE_MESSAGE);
		} else {
			myTextField.setText(myDate);
		}
	}
	
	/** 
	 * Classe d'ecoute sur les dates
	 * Permet d'effectuer la completion de cette date 
	 */
	private final class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	
	/** 
	 * Classe d'ecoute sur les dates
	 * Permet d'effectuer la completion de cette date 
	 */
	private final class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) { }
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
	
	public EOEditingContext getEdc() {
		return edc;
	}
	
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
}

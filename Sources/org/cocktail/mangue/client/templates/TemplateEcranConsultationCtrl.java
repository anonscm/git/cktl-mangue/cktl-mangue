package org.cocktail.mangue.client.templates;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class TemplateEcranConsultationCtrl {

	private static TemplateEcranConsultationCtrl 		sharedInstance;

	private static Boolean 			MODE_MODAL = Boolean.TRUE;
	private EOEditingContext		edc;
	private TemplateEcranConsultationView myView;
	private EODisplayGroup 			eod, eodAffectations;
	private boolean 				saisieEnabled, isLocked;

	private ListenerObject 		listenerObject = new ListenerObject();

	private EOEnterpriseObject currentObject;

	public TemplateEcranConsultationCtrl(EOEditingContext edc) {

		this.edc = edc;

		// Initialisation du displayGroup et de la VIEW
		eod = new EODisplayGroup();
		//myView = new VIEW(null, MODE_MODAL.booleanValue(), eod, eodAffectations);

		//		myView.getBtn().addActionListener(new ActionListener() {
		//			public void actionPerformed(ActionEvent evt) {action();}}
		//		);

		// Ajout d'un listener sur les champs dates pour les completions automatiques
		//		myView.getTfFiltrePeriodeDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfFiltrePeriodeDebut()));
		//		myView.getTfFiltrePeriodeFin().addActionListener(new ActionListenerDateTextField(myView.getTfFiltrePeriodeFin()));

		myView.getMyEOTable().addListener(listenerObject);
	}

	/**
	 * 
	 * Permet d'acceder a cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploisCtrl.sharedInstance(edc).methode
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TemplateEcranConsultationCtrl sharedInstance(EOEditingContext edc)	{
		if(sharedInstance == null)
			sharedInstance = new TemplateEcranConsultationCtrl(edc);
		return sharedInstance;
	}

	public void open()	{

		CRICursor.setWaitCursor(myView);

		// Actualisation des donneess
		actualiser();

		CRICursor.setDefaultCursor(myView);

		// Affichage de l'ecran
		myView.setVisible(true);


	}

	// Getters / Setters des objets metiers necessaires
	public EOEnterpriseObject currentObject() {
		return currentObject;
	}
	public void setCurrentObject(EOEnterpriseObject currentObject) {
		this.currentObject = currentObject;
	}

	/**
	 * Actualisation des donnes a partir d'une methode d'un Finder
	 */
	private void actualiser()	{

		CRICursor.setWaitCursor(myView);
		rechercher();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * Qualifier de filtre automatique
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray qualifiers = new NSMutableArray();

		return new EOAndQualifier(qualifiers);
	}


	/** 
	 * Lancement de la recherche - Mise a jour du displaygroup par un Finder specifique
	 */
	private void rechercher() {

		CRICursor.setWaitCursor(myView);
		NSArray objects = new NSArray();
		eod.setObjectArray(objects);
		filter();
		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * A utiliser si la fenetre propose des champs de filtre automatique
	 */
	private void filter() 	{

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();

		// Affichage des donnees
		myView.getMyEOTable().updateData();

		updateUI();

	}

	/**
	 * Ouverture de la fenetre de saisie / modification
	 */
	private void ajouter() {
		actualiser();
	}
	/**
	 * Ouverture de la fenetre de saisie / modification
	 */
	private void modifier() {
		myView.getMyEOTable().updateUI();
	}
	/**
	 * Suppression d'un objet avec confirmation
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cet objet ?", "Oui", "Non"))		
			return;

		try {

			edc.deleteObject(currentObject());
			edc.saveChanges();
			eod.deleteSelection();
			// Rafraichissement de l'affichage
			myView.getMyEOTable().updateUI();
		}
		catch(Exception e) {
			edc.revert();
			e.printStackTrace();
		}
	}


	/**
	 * Gestion de l'accessibilite de tous les objets graphiques de l'ecran
	 * 
	 */
	private void updateUI() {

		//		myView.getBtnxxx().setEnabled(true);
		//		CocktailUtilities.initTextField(myView. 

	}

	/**
	 * Classe d'ecoute sur le display group.
	 * Permet de lancer des actions lors d'un clic ou double clic sur une ligne de donnees
	 * @author cpinsard
	 *
	 */
	private class ListenerObject implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
		}

		public void onSelectionChanged() {

			// Mise a jour de l'objet courant
			setCurrentObject((EOEnterpriseObject)eod.selectedObject());
			updateUI();
		}
	}


	/**
	 * Gestion de la completion automatique des champs de format date
	 * 
	 * TO_DO : A factoriser pour eviter la duplication de code
	 * 
	 * @param myTextField
	 */
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			//actualiser();
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.templates;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSMutableArray;

/**
 * 
 * Template Ctrl de selection d'une nomenclature
 * 
 * Gere une fenetre de selection d'une nomenclature avec filtre
 * 
 * Appel à la methode ObjectSelectCtrl.sharedInstance(edc).getObject() renvoyant soit un objet selectionne soit NULL si annulation
 * 
 * @author cpinsard
 *
 */

public class ObjectSelectCtrl  {

	private static ObjectSelectCtrl sharedInstance;

	private EOEditingContext edc;

	private ObjectSelectView myView;

	private EODisplayGroup 		eod  = new EODisplayGroup();
	private EOEnterpriseObject 	currentObject;

	/**
	 * 
	 * @param editingContext
	 */
	public ObjectSelectCtrl(EOEditingContext edc)	{

		super();

		this.edc = edc;
		myView = new ObjectSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		// Listener de gestion des clics / double clic
		myView.getMyEOTable().addListener(new ListenerObject());
		
		// Listener sur le champ de filtrage
		myView.getTfFiltreLibelle().getDocument().addDocumentListener(new ADocumentListener());

		CocktailUtilities.viderTextField(myView.getTfFiltreLibelle());
		
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ObjectSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ObjectSelectCtrl(editingContext);
		return sharedInstance;
	}

	private EOEnterpriseObject getCurrentObject() {
		return currentObject;
	}
	private void setCurrentObject(EOEnterpriseObject currentObject) {
		this.currentObject = currentObject;
	}

	/**
	 * Qualifier a appliquer en fonction du filtre
	 * Renseigner les colonnes de la nomenclature a prendre en compte
	 * @return
	 */
	private EOQualifier getQualifier() {
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		
		if (CocktailUtilities.getTextFromField(myView.getTfFiltreLibelle()) != null) {
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
//			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEnterpriseObject.xxx_KEY + " caseInsensitiveLike %@", new NSArray("*"+CocktailUtilities.getTextFromField(myView.getTfFiltreLibelle())+"*")));			
//			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEnterpriseObject.xxx_KEY + " caseInsensitiveLike %@", new NSArray("*"+CocktailUtilities.getTextFromField(myView.getTfFiltreLibelle())+"*")));			
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
				
		return new EOAndQualifier(qualifiers);
		
	}
	
	/**
	 * Methode appelle a chaque saisie dand le textField
	 */
	private void filter() {
		
		eod.setQualifier(getQualifier());
		eod.updateDisplayedObjects();
		
		myView.getMyEOTable().updateData();
		
	}
	

	/**
	 * Ouverture de la fenetre de selection avec fetch des donnees
	 * 
	 * @return Objet selectionne
	 */
	public EOEnterpriseObject getObject()	{
	
		//eod.setObjectArray(EOEnterpriseObject.find(edc));		
		//eod.setObjectArray(EOEnterpriseObject.fetchAll(edc, EOEnterpriseObject.SORT_ARRAY_xxx));		
		filter();
		
		myView.setVisible(true);

		return getCurrentObject();
	}

	/**
	 * Annulation de la selection - 
	 */
	private void annuler() {

		setCurrentObject(null);
		myView.dispose();

	}  

	/**
	 * Gestion des clics / double clics sur la table view
	 * 
	 * A double clic confirme la selection
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerObject implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			setCurrentObject((EOEnterpriseObject)eod.selectedObject());
		}
	}
	
	
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}
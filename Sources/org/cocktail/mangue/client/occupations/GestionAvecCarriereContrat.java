/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.occupations;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.client.outils_interface.ModelePageAvecIndividu;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.InfoCarriereContrat;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Affiche les carrieres/contrats valides pendant la periode
 *
 */
public abstract class GestionAvecCarriereContrat extends ModelePageAvecIndividu {
	public EODisplayGroup displayGroupCarriereContrat;
	public EOTable listeCarriereContrat;
	private EOGlobalID carriereID, contratID;
	private boolean estAffectation;
	/** Notification recue pour raffraichir la liste des carrieres/contrats */
	public static String RAFFRAICHIR_CC = "RefreshCarriereContrat";
	/** Notification recue pour ajouter un element (affectation/occupation) */
	public static String AJOUTER_ITEM = "AjouterItem";

	public GestionAvecCarriereContrat(boolean estAffectation) {
		super();
		this.estAffectation = estAffectation;
	}
	public GestionAvecCarriereContrat() {
		this(false);
	}
	// accesseurs
	public EOContrat contratCourant() {
		if (contratID == null) {
			return null;
		} else {
			return (EOContrat)SuperFinder.objetForGlobalIDDansEditingContext(contratID,editingContext());
		}
	}
	public EOCarriere carriereCourante() {
		if (carriereID == null) {
			return null;
		} else {
			return (EOCarriere)SuperFinder.objetForGlobalIDDansEditingContext(carriereID,editingContext());
		}
	}
	// méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroup() && !modeCreation()) {
			preparerCarrieresContrats(true);
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value,Object eo,String key) {
		if (key.equals("dateDebut") || key.equals("dateFin")) {
			preparerCarrieresContrats(true);
		}
		controllerDisplayGroup().redisplay();
	}
	// Notifications
	public void ajouter(NSNotification aNotif) {
		// l'objet de la notification indique la fonctionnalite pour lequel est effectué l'ajout
		// le userInfo contient le globalID de l'objet passé en paramètre et la clé indique son type
		if (aNotif.object() != null && aNotif.userInfo() != null) {
			String type = (String)aNotif.object();
		}
	}
	public void raffraichir(NSNotification aNotif) {
		preparerCarrieresContrats(false);
	}
	// Méthodes du controller DG
	/** on ne peut valider que il y a au moins une carri&grave;re ou un contrat pour la periode */
	public boolean peutValider() {
		return super.peutValider() && displayGroupCarriereContrat.displayedObjects().count() > 0;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		GraphicUtilities.changerTaillePolice(listeCarriereContrat,11);
		GraphicUtilities.rendreNonEditable(listeCarriereContrat);
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("raffraichir", new Class[] { NSNotification.class }), RAFFRAICHIR_CC, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("ajouter", new Class[] { NSNotification.class }), AJOUTER_ITEM, null);
	}
	/** prepare les carrieres-contrats si ils ont ete passes en parametre */
	protected void traitementsPourCreation() {
		if (carriereID == null && contratID  == null) {
			return;
		}
		InfoCarriereContrat cc = null;
		if (carriereID != null) {
			cc = new InfoCarriereContrat(carriereCourante());
		} else if (contratID != null) {
			cc = new InfoCarriereContrat(contratCourant());
		}
		displayGroupCarriereContrat.setObjectArray(new NSArray(cc));

		updaterDisplayGroups();	
	}
	protected void parametrerDisplayGroup() {
		displayGroupCarriereContrat.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareDescending)));
	}

	/** methode a surcharger pour faire des traitements avant l'enregistrement des donnees dans la base
	 * retourne true si on peut enregistrer */
	protected boolean traitementsAvantValidation() {
		// vérifie si les contrats/carrières couvrent toute la période de l'affectation
		if (verifierCouverturePeriode() == false) {
			if (estAffectation) {
				EODialogs.runInformationDialog("Attention","Cette affectation n'est pas couverte pendant toute la période par des carrières, contrats ou contrats d'hébergé");
			} else {
				EODialogs.runInformationDialog("Attention","Cette occupation n'est pas couverte pendant toute la période par des carrières ou contrats");
			};
		}
		return true;
	}
	protected void traitementsApresValidation() {
		super.traitementsApresValidation();
		repreparer();
		displayGroup().updateDisplayedObjects();
	}
	protected void traitementsApresRevert() {
		super.traitementsApresRevert();
		repreparer();
	}
	protected void traitementsApresSuppression() {
		super.traitementsApresSuppression();
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.RAFFRAICHIR,null);	// pour synchronisation des contrats/carrières
	}
	protected void preparerCarrieresContrats(boolean informerUtilisateur) {
		NSArray carrieresContrats = null;
		if (displayGroup().selectedObject() != null && dateDebut() != null) {
			carrieresContrats = InfoCarriereContrat.carrieresContratsPourPeriode(editingContext(), currentIndividu(), dateDebut(), dateFin(),estAffectation);
			if (carrieresContrats.count() == 0 && informerUtilisateur) {
				if (estAffectation) {
					EODialogs.runInformationDialog("Attention", "pas de segment de carrière, de contrat ou de contrat hébergé pendant cette période");
				}  else {
					EODialogs.runInformationDialog("Attention", "pas de segment de carrière ou de contrat pendant cette période");
				}
			}
		}
		displayGroupCarriereContrat.setObjectArray(carrieresContrats);
		displayGroupCarriereContrat.updateDisplayedObjects();

	}


	// vérifie si il y a des contrats-carrières sur toute la période, sinon affiche un warning à l'utilisateur
	// il y a au moins un carrière ou contrat sur la période sinon on ne pourrait pas valider
	protected boolean verifierCouverturePeriode() {
		// on les classe par ordre de date début croissant pour vérifier les trous
		NSArray ccCroissants = EOSortOrdering.sortedArrayUsingKeyOrderArray(displayGroupCarriereContrat.allObjects(), new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));
		NSTimestamp dateDeb = dateDebut();
		// vérifier si le premier cc ne crée pas un trou
		InfoCarriereContrat cc = (InfoCarriereContrat)ccCroissants.objectAtIndex(0);	
		if (DateCtrl.isAfterEq(cc.dateDebut(),DateCtrl.jourSuivant(dateDeb))) {	// on compare avec le jour suivant pour éviter le problème des heures
			return false;

		} else {
			dateDeb = cc.dateFin();
		}
		if (dateDeb == null) {	// vérification terminée
			return true;
		} 
		for (int i = 1; i < ccCroissants.count();i++) {
			cc = (InfoCarriereContrat)ccCroissants.objectAtIndex(i);
			if (cc.dateFin() != null) {
				if (DateCtrl.isAfter(cc.dateFin(),dateDeb)) {
					// si la date de début de cet élément est postérieure à dateDeb et ne correspond pas au jour suivant, 
					// il y a un trou
					NSTimestamp jourSuivant = DateCtrl.jourSuivant(dateDeb);
					if (DateCtrl.isAfterEq(cc.dateDebut(),jourSuivant) && DateCtrl.isSameDay(cc.dateDebut(), jourSuivant) == false) {
						return false;
					} else {
						dateDeb = cc.dateFin();
						if (dateDeb == null) {	// vérification terminée
							break;
						}
					}
				}
			} else {
				dateDeb = cc.dateFin();
				break;	// on a trouvé un élément sans date fin, ce n'est pas la peine de continuer
			}
		}
		if (dateDeb != null && (dateFin() == null || DateCtrl.isAfter(dateFin(),dateDeb))) {
			// on n'a pas trouvé de contrat/carrière qui couvrait la fin
			return false;
		}
		return true;
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupCarriereContrat.updateDisplayedObjects();
	}  
	/** methode a surcharger par la sous-classe pour retourner la date debut de l'objet du display
	 * group selectionne */
	protected abstract NSTimestamp dateDebut();
	/** methode a surcharger par la sous-classe pour retourner la date fin de l'objet du display
	 * group selectionne */
	protected abstract NSTimestamp dateFin();
	// Méthodes privées
	private void reset() {
		carriereID = null;
		contratID = null;
	}
	private void repreparer() {
		reset();	// pour pouvoir créer des affectations/occupations nouvelles
		preparerCarrieresContrats(true);
		updaterDisplayGroups();
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.RAFFRAICHIR,null);	// pour synchronisation des contrats/carrières
	}
}

/*
 * Created on 20 mai 2005
 *
 * Classe Application client
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme, le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement, à l'utilisation, à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant des connaissances informatiques approfondies. Les
 * utilisateurs sont donc invités à charger et tester l'adéquation du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowListener;
import java.io.File;
import java.net.UnknownHostException;
import java.util.TimeZone;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.tools.CocktailIcones;
import org.cocktail.application.client.tools.ConnectedUsersCtrl;
import org.cocktail.client.InterfaceApplication;
import org.cocktail.client.InterfaceApplicationAvecImpression;
import org.cocktail.client.InterfaceApplicationAvecPreferences;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.client.composants.ModelePageAvecIndividu;
import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlpersonne.common.metier.EODiplome;
import org.cocktail.fwkcktlwebapp.common.util.SystemCtrl;
import org.cocktail.mangue.client.administration.GestionAgents;
import org.cocktail.mangue.client.administration.GestionDCP;
import org.cocktail.mangue.client.administration.GestionDestinataires;
import org.cocktail.mangue.client.administration.GestionEmploisType;
import org.cocktail.mangue.client.administration.GestionHierarchie;
import org.cocktail.mangue.client.administration.GestionModelesPourImpressionContrat;
import org.cocktail.mangue.client.administration.GestionModulesImpression;
import org.cocktail.mangue.client.administration.GestionModulesIndividu;
import org.cocktail.mangue.client.administration.GestionServices;
import org.cocktail.mangue.client.administration.ParametresCtrl;
import org.cocktail.mangue.client.administration.RunTimeInfos;
import org.cocktail.mangue.client.administration.visa.GestionVisas;
import org.cocktail.mangue.client.administration.visa.GestionVisasArretesCorps;
import org.cocktail.mangue.client.administration.visa.GestionVisasCongesCarrieres;
import org.cocktail.mangue.client.administration.visa.GestionVisasCongesContractuels;
import org.cocktail.mangue.client.administration.visa.GestionVisasContrats;
import org.cocktail.mangue.client.agents.AgentsCtrl;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.carrieres.GestionCFA;
import org.cocktail.mangue.client.carrieres.GestionCongeBonifie;
import org.cocktail.mangue.client.carrieres.GestionMAD;
import org.cocktail.mangue.client.cir.CirCtrl;
import org.cocktail.mangue.client.conges.GestionAbsences;
import org.cocktail.mangue.client.decharges.ListeDechargesCtrl;
import org.cocktail.mangue.client.emplois.GestionStocks;
import org.cocktail.mangue.client.extraction.ExtractionEtatComparatif;
import org.cocktail.mangue.client.formations.ListeFormationsCtrl;
import org.cocktail.mangue.client.gpeec.EmploisCtrl;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewAbsences;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewAdresses;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewAnciennete;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewBudget;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewCarrieres;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewContrats;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewEtatCivil;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewFicheSituationAgent;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewHeberge;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewModalites;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewOccupation;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewPasse;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewProlongations;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewSyntheseCarriereCIR;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewVacations;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewVisas;
import org.cocktail.mangue.client.heberge.ListeDifsCtrl;
import org.cocktail.mangue.client.heberge.ListeHebergesCtrl;
import org.cocktail.mangue.client.impression.GestionEditionsDivers;
import org.cocktail.mangue.client.individu.GestionEtatCivil;
import org.cocktail.mangue.client.nomenclatures.NomenclaturesCtrl;
import org.cocktail.mangue.client.onglets.NomenclatureOngletMenu;
import org.cocktail.mangue.client.onglets.VueAvecOnglets;
import org.cocktail.mangue.client.outils.GestionEtatPersonnels;
import org.cocktail.mangue.client.outils.OutilDif;
import org.cocktail.mangue.client.outils.individu.OutilAnciennete;
import org.cocktail.mangue.client.outils.medical.EditionAcmos;
import org.cocktail.mangue.client.outils.medical.EditionExamens;
import org.cocktail.mangue.client.outils.medical.EditionVaccins;
import org.cocktail.mangue.client.outils.medical.EditionVisites;
import org.cocktail.mangue.client.outils.medical.GestionMedecins;
import org.cocktail.mangue.client.outils.medical.MedicalCtrl;
import org.cocktail.mangue.client.outils.nbi.NbiCtrl;
import org.cocktail.mangue.client.outils_interface.GestionTypeConge;
import org.cocktail.mangue.client.outils_interface.InterfaceAvecFenetre;
import org.cocktail.mangue.client.postes.GestionPostes;
import org.cocktail.mangue.client.preferences.GestionPreferences;
import org.cocktail.mangue.client.primes.AdministrationPrimes;
import org.cocktail.mangue.client.primes.GestionExclusionsConge;
import org.cocktail.mangue.client.primes.GestionExclusionsPosition;
import org.cocktail.mangue.client.primes.GestionExclusionsPrime;
import org.cocktail.mangue.client.primes.GestionFonctions;
import org.cocktail.mangue.client.primes.GestionParametres;
import org.cocktail.mangue.client.primes.GestionPersonnalisations;
import org.cocktail.mangue.client.primes.GestionPopulations;
import org.cocktail.mangue.client.primes.GestionPrimes;
import org.cocktail.mangue.client.primes.GestionPrimesIndividu;
import org.cocktail.mangue.client.primes.GestionProgrammes;
import org.cocktail.mangue.client.primes.GestionStructures;
import org.cocktail.mangue.client.primes.ListePrimesCtrl;
import org.cocktail.mangue.client.promouvabilites.PromouvabilitesCtrl;
import org.cocktail.mangue.client.requetes.GestionRequetesActivite;
import org.cocktail.mangue.client.requetes.GestionRequetesAgent;
import org.cocktail.mangue.client.requetes.GestionRequetesEmploi;
import org.cocktail.mangue.client.requetes.InterfaceRequeteCarriere;
import org.cocktail.mangue.client.requetes.InterfaceRequeteContrat;
import org.cocktail.mangue.client.requetes.InterfaceRequeteIndividu;
import org.cocktail.mangue.client.requetes.InterfaceRequeteOccupation;
import org.cocktail.mangue.client.requetes.InterfaceRequeteSpecialisation;
import org.cocktail.mangue.client.supinfo.SupInfoCtrl;
import org.cocktail.mangue.client.vacations.VacationsCtrl;
import org.cocktail.mangue.common.modele.manager.Manager;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.BusyGlassPane;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOAgent;
import org.cocktail.mangue.modele.grhum.referentiel.EOCompte;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOPrefsPersonnel;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaladie;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOArrivee;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;
import org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudget;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;
import org.cocktail.mangue.modele.mangue.modalites.EORepriseTempsPlein;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eoapplication.EOAction;
import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eoapplication._EOMRJUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EOTextAssociation;
import com.webobjects.eointerface.swing.EOSwingUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimeZone;

/* Actions : les actions sont toutes construites sur le même schéma. Elles invoquent une méthode definie dans la
 * nomenclature et implémentée dans le superviseur.
 * Cette méthode appelle une autre méthode (2 types selon qu'il s'agit d'une page de gestion d'un individu ou non)
 * qui passe en paramètre son nom et la classe du contrôleur à instancier.
 * Exemple : afficherEtatCivil action qui invoque afficherInfosPourIndividu("afficherEtatCivil","org.cocktail.mangue.client.individu.GestionEtatCivil");
 * Les noms de méthode correspondant à des actions doivent etre definies dans la nomenclature si elles requièrent une
 * gestion spécifique liée à l'affichage en mode fenêtre ou onglets.
 * Dans le cas de la gestion des préférences, il n'y a pas de gestion spécifique => pas de définition dans la nomenclature
 *
 * Selon les droits de l'agent, les items de menu sont autorises ou non. La nomenclature contient les attributs a verifier pour ces droits.
 *
 * Gestion du menu de fenêtres et de l'activation des contrôleurs :
 * On utilise un dictionnaire qui contient comme clé le titre associé à l'action du menu "Fenêtres" et comme valeur le contrôleur:
 * - dans le cas d'un affichage en mode "Fenetre", le contrôleur correspond au contrôleur du type d'infos géré
 * - dans le cas d'un affichage en mode "Onglet", c'est une sous-classe de VueAvecOnglets.
 * - les actions générales du menu "Fenêtres" ont aussi une entrée dans le dictionnaire, l'objet est le Superviseur
 * Les actions générales ("Tout Ramener au Premier Plan", "Tout fermer") sont définies dans le Superviseur, les actions affichant les fenêtres de contrôleur
 * d'interface sont définies dans ModelePage
 * Toutes les actions du menu "Fenêtres" sont stockées dans les additionalActions() de EOApplication afin que le menu soit affiché.
 * Le dictionnaire est utilisé pour savoir si un contrôleur est déjà affiché suite à une autre commande de menu ou si il faut l'afficher directement
 * Lorsqu'on affiche les vues avec des onglets : on détermine l'onglet dans lequel on affiche le composant par le chemin d'accès complet de l'item
 * de menu. Exemple : Employé/Infos/Etat Civil => on veut afficher dans la fenêtre Employé, l'onglet Etat Civil de la vue à onglets Infos
 *
 * Gère les notifications de sélection des onglets (dans le cas d'un affichage en onglets) : la notification contient le titre de l'onglet.
 * Il est utilisé pour déterminer l'action des defaultActions() à déclencher.
 */

public class ApplicationClient extends ApplicationCocktail implements InterfaceApplication, InterfaceApplicationAvecImpression, InterfaceApplicationAvecPreferences {

	private static final String FEATURE_SERVICES_VALIDES = "cocktail.feature.servicesValides";
	private static final String FEATURE_ADRESSES_NORMEES = "cocktail.feature.adressesNormees";

	private EOAgent 			currentAgent;
	private EOAgentPersonnel 	currentAgentPersonnel;
	private EOStructure		 	etablissementPrincipal;

	private EOGlobalID agentGlobalID;
	private Number individuCourantID;
	private EOFrameController controleurEmployes;
	private EOPrefsPersonnel preferencesUtilisateur;
	private boolean estLocke;
	private boolean modeFenetre, estListeAgentsAGauche;
	private boolean actionEnCours;
	private String nomBase;
	private String derniereAction;
	private ListeControleurs listeControleurs;	// gère la liste des contrôleurs secondaires
	private ListeAgents listeAgents;
	private static JTextComponent lastActivedTextField;
	private static String TOUT_AFFICHER = "Tout Ramener au Premier Plan";
	private static String DEPLACER_LISTE = "Déplacer la liste des employés";
	private static int DECALAGE_FENETRE = 75;
	/** Notification a envoyer pour demander le declenchement d'une action */
	public static String ACTIVER_ACTION = "ActiverAction";
	/** Notification envoyee pour le changement de taille de la fenetre Agents */
	public static String CHANGER_TAILLE_FENETRE = "ChangerTailleFenetre";
	/** taille de la liste des agents en mode fenetre */
	public static int TAILLE_POUR_MODE_FENETRE = 539;
	/** taille de la liste des agents en mode onglet */
	public static int TAILLE_POUR_MODE_ONGLET = 690;

	private	BusyGlassPane	myBusyGlassPane;
	private JFrame activeFrame;
	private Window activeWindow;

	//private ActionsListeCtrl myListeAction;

	private boolean useAdressesNormees;
	private boolean useServicesValides;
	private boolean showListeAgents;
	private LogsErreursCtrl ctrlLogs;

	private Manager manager;

	public ApplicationClient() {
		super();
		setTYAPSTRID("MANGUE");
	}

	/**
	 * 
	 */
	public void finishInitialization() {
		try {
			super.finishInitialization();
			try {
				compareJarVersionsClientAndServer();
			} catch (Exception e) {
				e.printStackTrace();
				this.fenetreDeDialogueInformation(e.getMessage());
				quit();
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}

		myBusyGlassPane = new BusyGlassPane();

		// Indiquer la couleur de fond quand le champ est actif et inactif
		// régler ensuite chaque champ texteavec IB dans l'inspecteur en sélectionnant dans le popup "Association" et en cochant "Uses Default..."
		EOTextAssociation.setDefaultBackgroundColors(GraphicUtilities.COL_FOND_ACTIF,GraphicUtilities.COL_FOND_INACTIF);
		setQuitsOnLastWindowClose(true);
		if (EOSwingUtilities._isRunningOnMacPlatform()) {
			_EOMRJUtilities._registerQuitHandler();
		}
	}

	/**
	 * Initialisation de l'interface et controleur principal
	 */
	public void initMonApplication() {

		super.initMonApplication();
		CocktailUtilities.fixWoBug_responseToMessage(getEntityList());

		EOGrhumParametres.initParametres(getAppEditingContext());

		manager = new Manager();
		manager.setEdc(getAppEditingContext());
		manager.setUserInfo(getUserInfos());

		LogManager.preparerLog("LogMangueClient", (String)arguments().valueForKey("modeConsole"), (String)arguments().valueForKey("modeDebug"));
		if (getUserInfos().login() != null) {

			ServerProxy.clientSideRequestSetLoginParametres(getAppEditingContext(), getUserInfos().login() ,getIpAdress());

			String nomBase = ServerProxy.serverBdConnexionName(getEdc());
			setNomBase((String)(NSArray.componentsSeparatedByString(NSArray.componentsSeparatedByString(nomBase, ":").lastObject().toString(), "@")).objectAtIndex(0));

			ctrlLogs = new LogsErreursCtrl(manager);

			String timeZone = defaultTimeZone();
			TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
			NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName(timeZone,false));
			System.out.println(" >>>   TIME ZONE CLIENT : " + defaultTimeZone());

			setEtablissementPrincipal(EOStructure.rechercherEtablissementPrincipal(getAppEditingContext()));

			try {
				setUseAdressesNormees(ServerProxy.clientSideRequestGetBooleanParam(getAppEditingContext(), FEATURE_ADRESSES_NORMEES));
			}
			catch (Exception e) {
				setUseAdressesNormees(false);
			}
			try {
				setUseServicesValides(ServerProxy.clientSideRequestGetBooleanParam(getAppEditingContext(), FEATURE_SERVICES_VALIDES));
			}
			catch (Exception e) {
				setUseServicesValides(false);
			}
			//			try {
			//				setShowListeAgents(ServerProxy.clientSideRequestGetBooleanParam(getAppEditingContext(), FEATURE_LISTE_AGENTS));
			//			}
			//			catch (Exception e) {
			//				setShowListeAgents(false);
			//			}

			// vérifier si l'individu a des droits
			EOAgentPersonnel agent = EOAgentPersonnel.rechercherAgentAvecLogin(getEdc(),getUserInfos().login());
			if (agent == null) {
				EODialogs.runErrorDialog("Erreur","Vous n'êtes pas autorisé(e) à utiliser cette application ( login " + getUserInfos().login() + ") ...");
				EOApplication.sharedApplication().quit();
			} else {
				// Poster le nom de l'agent côté serveur pour s'en servir dans les logs
				ServerProxy.clientSideRequestSetAgent(getAppEditingContext(), getUserInfos().login());
				continuerAvecAgent(getEdc().globalIDForObject(agent));
			}	
		}


	}


	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public void refreshInterface(EOIndividuIdentite individu) {
		java.util.Enumeration<EOController> e = listeControleurs.controleurs().objectEnumerator();
		while (e.hasMoreElements()) {
			EOController controleur = e.nextElement();
			if (controleur instanceof VueAvecOnglets) {
				((VueAvecOnglets)controleur).afficherFenetre();
			}
		}
	}

	/**
	 * 
	 */
	public void refreshAllObjects() {
		CRICursor.setWaitCursor(windowObserver().activeWindow());
		getAppEditingContext().refaultAllObjects();		
		getAppEditingContext().refreshAllObjects();		
		CRICursor.setDefaultCursor(windowObserver().activeWindow());	
	}

	/**
	 * 
	 * @return
	 */
	private final String[] getEntityList()	{
		return new String[] {

				EOIndividu.ENTITY_NAME,
				EOAbsences.ENTITY_NAME,
				EOAdresse.ENTITY_NAME,
				EOArrivee.ENTITY_NAME,
				EODepart.ENTITY_NAME,
				EOCorps.ENTITY_NAME, 
				EOGrade.ENTITY_NAME,
				EOIndividuDiplomes.ENTITY_NAME,
				EOContrat.ENTITY_NAME,
				EOContratAvenant.ENTITY_NAME,
				EODiplome.ENTITY_NAME,

				EOCarriere.ENTITY_NAME,
				EOChangementPosition.ENTITY_NAME,
				EOElementCarriere.ENTITY_NAME,
				EOPasse.ENTITY_NAME,
				EOAffectation.ENTITY_NAME,
				EOOccupation.ENTITY_NAME,
				EOStructure.ENTITY_NAME,
				EOTypeContratTravail.ENTITY_NAME,

				EOPersBudget.ENTITY_NAME,
				EOPersBudgetAction.ENTITY_NAME,
				EOPersBudgetAnalytique.ENTITY_NAME,
				EOPersBudgetConvention.ENTITY_NAME,

				EOVacataires.ENTITY_NAME,
				EOVacatairesAffectation.ENTITY_NAME,

				EOCongeMaladie.ENTITY_NAME
		};		
	}
	public boolean isUseAdressesNormees() {
		return useAdressesNormees;
	}

	public void setUseAdressesNormees(boolean useAdressesNormees) {
		this.useAdressesNormees = useAdressesNormees;
	}
	public boolean isUseServicesValides() {
		return useServicesValides;
	}

	public void setUseServicesValides(boolean useServicesValides) {
		this.useServicesValides = useServicesValides;
	}

	public boolean isShowListeAgents() {
		return showListeAgents;
	}

	public void setShowListeAgents(boolean showListeAgents) {
		this.showListeAgents = showListeAgents;
	}

	public LogsErreursCtrl getCtrlLogs() {
		return ctrlLogs;
	}

	public void setCtrlLogs(LogsErreursCtrl ctrlLogs) {
		this.ctrlLogs = ctrlLogs;
	}

	/**
	 * 
	 * @param agentID
	 */
	public void continuerAvecAgent(EOGlobalID agentID) {

		try {

			agentGlobalID = agentID;

			setCurrentAgentPersonnnel((EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(agentGlobalID(), getEdc()));
			setCurrentAgent(EOAgent.rechercherAgentAvecLogin(getEdc(), getAgentPersonnel().agtLogin()));

			setPreferencesUtilisateur(EOPrefsPersonnel.rechercherPreferencesPourIndividu(getEdc(), getAgentPersonnel().toIndividu()));

			setShowListeAgents(preferencesUtilisateur !=  null && preferencesUtilisateur.afficherNouvelleListe());

			manager.setUtilisateur(getAgentPersonnel());

			if (getAgentPersonnel() != null) {

				System.out.println(" >>> Utilisateur : " + getAgentPersonnel().toIndividu().identitePrenomFirst());

				loadNotifications(getAgentPersonnel().peutConsulterDossier());
				listeControleurs = new ListeControleurs();

				if (getAgentPersonnel().isModuleVacation()) {
					VacationsCtrl.sharedInstance(getAppEditingContext()).open();
				}
				else {
					if (getAgentPersonnel().peutConsulterDossier()) {
						individuCourantID = getAgentPersonnel().toIndividu().noIndividu();
						preparerEtAfficherEtatCivil();
						AgentsCtrl.sharedInstance().setActivePane("INFOS");
					} else {
						initAvecPreferences(true);
					}

					// Affichage de la nouvelle liste des agents
					if (isShowListeAgents()) {
											
						EOController controleurPourFenetre = (EOController)listeControleurs.controleurAvecNom("Employé");
						Window mainWindow = ((EOFrameController)controleurPourFenetre.supercontroller()).window();
						JFrame viewAgents = AgentsCtrl.sharedInstance().getView();

						viewAgents.setSize(viewAgents.getWidth(), mainWindow.getHeight() + 9);
						viewAgents.setLocation((viewAgents.getX() + viewAgents.getWidth()) , viewAgents.getY());

						AgentsCtrl.sharedInstance().open();

						int width = mainWindow.getWidth();
						int positionX = viewAgents.getX();
						int positionY = viewAgents.getY();
						mainWindow.setLocation( (positionX - width - 5), (positionY + 4));
						
					}
				}
			}
			else
				System.out.println(" >>>>>>>>>  AGENT personnel non renseigné");

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	public EOPrefsPersonnel getPreferencesUtilisateur() {
		return preferencesUtilisateur;
	}

	public void setPreferencesUtilisateur(EOPrefsPersonnel preferencesUtilisateur) {
		this.preferencesUtilisateur = preferencesUtilisateur;
	}

	/**
	 * @return retourne l'agent de login
	 */
	public EOGlobalID agentGlobalID() {
		return agentGlobalID;
	}
	public Number individuCourantID() {
		return individuCourantID;
	}
	public EOAgentPersonnel getAgentPersonnel() {
		return currentAgentPersonnel;	
	}

	public EOAgent getAgent() {
		return currentAgent;
	}
	public void setCurrentAgent(EOAgent currentAgent) {
		this.currentAgent = currentAgent;
	}

	public void setCurrentAgentPersonnnel(EOAgentPersonnel currentAgentPersonnel) {
		this.currentAgentPersonnel = currentAgentPersonnel;
	}




	public EOStructure getEtablissementPrincipal() {
		return etablissementPrincipal;
	}

	public void setEtablissementPrincipal(EOStructure etablissementPrincipal) {
		this.etablissementPrincipal = etablissementPrincipal;
	}

	public JFrame activeFrame() {
		if (windowObserver().activeWindow() != null)
			return (JFrame)(windowObserver().activeWindow());
		return new JFrame();
	}

	/**
	 * @param agentGlobalID globalID de l'agent a affecter
	 */
	public void setAgent(EOGlobalID agentGlobalID) {
		this.agentGlobalID = agentGlobalID;
	}

	private void setActiveFrame(JFrame frame) {
		activeFrame = frame;
	}

	public void setGlassPane(boolean bool) {

		try {
			if (bool) {
				setActiveFrame((JFrame)(windowObserver().activeWindow()));
				activeFrame.setGlassPane(myBusyGlassPane);
			}
			else
				if (bool == false && activeFrame != null) {
					activeFrame = null;
				}

			myBusyGlassPane.setVisible(bool);
		}
		catch (Exception e) {

		}
	}

	public int positionPourFenetre(boolean estGestionAgent) {
		if (!isShowListeAgents() && estGestionAgent && estListeAgentsAGauche) {
			if (controleurEmployes.window().getWidth() < 300)
				return 300 + DECALAGE_FENETRE;	// Au démarrage
			else
				return controleurEmployes.window().getWidth() + DECALAGE_FENETRE;
		} else
			return 0;
	}
	public boolean modeFenetre() {
		return modeFenetre;
	}
	public boolean canQuit() {
		if (hasEditedDocuments()) {
			return false;
		} else {
			return super.canQuit();
		}
	}
	public boolean estControlleurPourListeAgents(EOController controleur) {
		return controleur == controleurEmployes;
	}

	// Gestion des préférences
	public NSDictionary preferencesParDefaut() {
		NSMutableDictionary valeursParDefaut = new NSMutableDictionary();

		for (java.util.Enumeration<String> e = arguments().keyEnumerator();e.hasMoreElements();) {
			String cle = (String)e.nextElement();
			if (cle.startsWith("prefs"))
				valeursParDefaut.setObjectForKey(arguments().objectForKey(cle),cle);

		}
		return valeursParDefaut;
	}

	/** retourne true si l'affichage des photos est souhaite dans l'Etat Civil. */
	public boolean afficherPhoto() {

		if (EOGrhumParametres.isPhotos()) {
			if (preferencesUtilisateur != null) {
				return preferencesUtilisateur.afficherPhoto();
			} else {
				String valeurParDefaut = (String)arguments().objectForKey("prefsAfficherPhotos");
				return valeurParDefaut != null && valeurParDefaut.equals(CocktailConstantes.VRAI);
			}
		} else {
			return false;
		}
	}

	public String directoryExports() {

		// Si on a des preferences , exports, puis impression, impression par defaut.
		if (preferencesUtilisateur != null) {
			if( preferencesUtilisateur.directoryExports() != null)
				return preferencesUtilisateur.directoryExports();
			else
				return directoryImpression();
		}

		return directoryImpressionParDefaut();
	}


	/** retourne le directory d'impression */
	public String directoryImpression() {

		String directory = null;

		if (preferencesUtilisateur != null) {
			if( preferencesUtilisateur.directoryImpression() != null)
				directory = preferencesUtilisateur.directoryImpression();
			else
				directory = directoryImpressionParDefaut();
		} else {
			directory = directoryImpressionParDefaut();
		}
		return directory;
	}

	/** retourne le directory d'impression fourni par defaut dans les reglages de l'application (Mangue.config) */
	public String directoryImpressionParDefaut() {
		String directory = null;
		EODistributedObjectStore store = (EODistributedObjectStore)getEdc().parentObjectStore();
		directory = (String)store.invokeRemoteMethodWithKeyPath(getEdc(), "session", "clientSideRequestGetParam", new Class[] {String.class}, new Object[] {"DIRECTORY_IMPRESSION_CLIENT"}, false);
		if (directory == null || directory.length() == 0) {
			directory = SystemCtrl.tempDir();
		} else {
			if (directory.substring(directory.length() - 1).equals(File.separator) == false) {
				directory += File.separator;
			}
		}
		return directory;
	}
	/** retourne le type d'adresse à afficher par defaut dans la gestion des adresses. */
	public String typeAdresseParDefaut() {
		if (preferencesUtilisateur != null && preferencesUtilisateur.typeAdresse() != null) {
			return preferencesUtilisateur.typeAdresse().code();
		} else {
			EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(agentGlobalID(),getEdc());
			if (!agent.peutAfficherInfosPerso()) {
				return EOTypeAdresse.TYPE_PROFESSIONNELLE;
			} else {
				String valeurParDefaut = (String)arguments().objectForKey("prefsTypeAdresse");
				if (valeurParDefaut != null) {
					return valeurParDefaut;
				} else {
					return EOTypeAdresse.TYPE_PERSONNELLE;
				}
			}
		}
	}

	/** retourne true si l'affichage des postes est souhaite a l'ouverture de la fenetre */
	public boolean afficherPostesAuDemarrage() {
		if (preferencesUtilisateur != null) {
			return preferencesUtilisateur.afficherPostes();
		} else {
			String valeurParDefaut = (String)arguments().objectForKey("prefsAfficherPostes");
			return valeurParDefaut != null && valeurParDefaut.equals(CocktailConstantes.VRAI);
		}
	}
	/** retourne le type de poste a afficher */
	public int typePosteAuDemarrage() {
		if (preferencesUtilisateur != null) {
			return preferencesUtilisateur.typePosteAuDemarrage().intValue();
		} else {
			Number valeurParDefaut = (Number)arguments().objectForKey("prefsTypePoste");
			if (valeurParDefaut != null) {
				return valeurParDefaut.intValue();
			} else {
				return ManGUEConstantes.TOUT_POSTE;
			}
		}
	}
	/** retourne la liste des types de population geres definie dans les preferences utilisateur
	 * ou null si pas de preferences. */
	public NSArray populationsGerees(EOEditingContext unEditingContext) {
		if (preferencesUtilisateur != null) {
			return preferencesUtilisateur.populationsGerees(unEditingContext);
		} else {
			return null;
		}
	}
	/** retourne le type d'absence a afficher par defaut ou GestionAbsence.TOUT CONGE si pas de preference utilisateur */
	public String typeAbsenceParDefaut() {
		if (preferencesUtilisateur != null && preferencesUtilisateur.typeAbsence() != null) {
			return preferencesUtilisateur.typeAbsence().code();
		} else {
			String valeurParDefaut = (String)arguments().objectForKey("prefsTypeConge");
			if (valeurParDefaut == null) {
				return GestionTypeConge.TOUS_CONGES;
			} else {
				return valeurParDefaut;
			}
		}
	}
	/** retourne la periode d'absence a afficher par defaut ou GestionAbsence.ANNEE_UNIVERSITAIRE
si pas de preference utilisateur */
	public int periodeAbsenceParDefaut() {
		if (preferencesUtilisateur != null) {
			if (preferencesUtilisateur.afficherCongePourAnneeUniv() == false) {
				return GestionPeriode.ANNEE_CIVILE;
			} else {
				return GestionPeriode.ANNEE_UNIVERSITAIRE;
			}
		} else {
			String valeurParDefaut = (String)arguments().objectForKey("prefsAbsenceAnneeUniversitaireParDefaut");
			if (valeurParDefaut != null) {
				if (valeurParDefaut.equals(CocktailConstantes.VRAI)) {
					return GestionPeriode.ANNEE_UNIVERSITAIRE;
				} else {
					return GestionPeriode.ANNEE_CIVILE;
				}
			} else {
				return GestionPeriode.ANNEE_UNIVERSITAIRE;
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public int categoriePersonnel() {
		int categoriePersonnel = ManGUEConstantes.TOUT_PERSONNEL;
		if (preferencesUtilisateur != null) {
			categoriePersonnel = preferencesUtilisateur.typePersonnelAuDemarrage().intValue();
		} else {
			Integer valeurParDefaut = (Integer)arguments().objectForKey("prefsTypePersonnel");
			try {
				if (valeurParDefaut.intValue() > ManGUEConstantes.TOUT_PERSONNEL) {	// en dehors des valeurs légales
					throw new Exception("");
				}
				categoriePersonnel = valeurParDefaut.intValue();
			} catch (Exception e) {}	// pas de constante ou de valeur illégale définie
		}
		return categoriePersonnel;
	}
	/** Retourne le choix d'exclusion des heberges dans les preferences utilisateur (la valeur par defaut est false) */
	public boolean exclureHeberges() {
		if (preferencesUtilisateur != null) {
			return preferencesUtilisateur.exclureHeberges();
		} else {
			return false;
		}
	}
	/** retourne le globalID de la structure choisie dans les preferences utilisateur */
	public EOGlobalID prefStructureID() {
		if (preferencesUtilisateur != null) {
			return getEdc().globalIDForObject(preferencesUtilisateur.structure());
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	private final String getIpAdress() {
		try {
			final String s = java.net.InetAddress.getLocalHost()
					.getHostAddress();
			final String s2 = java.net.InetAddress.getLocalHost().getHostName();
			return s + " / " + s2;
		} catch (UnknownHostException e) {
			return "Machine inconnue";

		}
	}

	public EOEditingContext getEdc() {
		return getAppEditingContext();
	}
	// méthode appelée en cas de time-out de session
	public void quitWithMessage(String title,String message) {
		super.quitWithMessage("Erreur","La session avec le serveur est interrompue. L'application va quitter automatiquement");
	}
	// Pour fournir des infos générales
	public String infosPourApplication() {
		return "MANGUE " + " - " + ServerProxy.clientSideRequestAppVersion(getAppEditingContext()) + " - Base : " + getNomBase();
	}

	// NOTIFICATIONS
	// LOCK ECRAN
	public void lockSaisie(NSNotification aNotif) {
		estLocke = true;
		changerVerrou(!estLocke);
	}
	// DELOCK ECRAN
	public void unlockSaisie(NSNotification aNotif) {
		estLocke = false;
		changerVerrou(!estLocke);
	}
	public void afficherEmployes(NSNotification aNotif) {
		controleurEmployes.activate();
	}
	public void nettoyer(NSNotification aNotif) {
		individuCourantID = null;
	}
	public void employeHasChanged(NSNotification aNotif) {
		if (aNotif.object() == null) {
			individuCourantID = null;
		} else {
			individuCourantID = (Number)aNotif.object();
		}
	}
	public void changerPreferences() {
		// lire les préférences et effectuer les traitements ad-hoc
		initAvecPreferences(false);
	}
	public void arreterControleur(NSNotification aNotif) {
		EOController controleur = listeControleurs.controleurAvecNom((String)aNotif.object());
		supprimerControleurPourAction(controleur);	
	}


	// Notification reçue lorsque l'utilisateur active une fenêtre ou un dialogue (envoyée par le WindowObserver)
	public void activeWindowDidChange(NSNotification notification) {
		// le menu Fenêtre n'étant pas automatiquement mis à jour par WO quand on ouvre une nouvelle fenêtre
		// on le fait manuellement en java
		if (windowObserver().activeWindow() != null && windowObserver().activeWindow() instanceof JFrame) {
			JMenuBar menubar = ((JFrame)windowObserver().activeWindow()).getJMenuBar();
			JMenu menu = menubar.getMenu(menubar.getMenuCount() - 1);
			menu.removeAll();

			for (java.util.Enumeration<EOAction> e = EOAction.sortedActions(additionalActions()).objectEnumerator();e.hasMoreElements();) {
				EOAction action = e.nextElement();
				JMenuItem menuItem = new JMenuItem(action);
				menuItem.setText(action.actionTitle());
				menuItem.setEnabled(action.actionTitle().equals(TOUT_AFFICHER) || action.actionTitle().equals(DEPLACER_LISTE) || !estLocke);
				menu.add(menuItem);
			}

			if (!isShowListeAgents() && windowObserver().activeWindow() == controleurEmployes.window()) {
				modifierPolice(menubar);
				/* 16/05/08 - pour que la fenêtre ne revienne pas automatiquement à gauche alors qu'on l'a déplacée, sauf si
				 * elle est sur une position horizontale négative */
				if (estListeAgentsAGauche && controleurEmployes.window().getLocation().x < 0) {
					controleurEmployes.window().setLocation(0,0);
					changerPositionFenetres();	// Pour caler correctement la position de la fenêtre Etat Civil ou Hébergé
					// après la construction des menus
				}
			}
		}
	}

	/**
	 * 
	 */
	public NSArray<EOAction> defaultActions() {

		EOAgentPersonnel agent = null;

		if (agentGlobalID() != null)
			agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(agentGlobalID(),getEdc());

		NSMutableArray<EOAction> actions = new NSMutableArray<EOAction>();
		if (agent != null && agent.peutConsulterDossier() == false) {
			// Il s'agit d'un gestionnaire de Mangue
			actions.addObject(EOAction.actionForObject("afficherParametresMangue","App/Paramètres","Gestion des paramètres MANGUE",null, null, null, 100,900, this));
			actions.addObject(EOAction.actionForObject("afficherPreferences","App/Préférences utilisateur","Gestion des préférences personnelles",null, null, null, 100,1000, this));
			actions.addObject(EOAction.actionForObject("afficherConnectedUsers","App/Utilisateurs connectés","Utilisateurs Connectés",null, null, null, 100,1100, this));
			actions.addObject(EOAction.actionForObject("refreshAllObjects","App/Rafraichir les données","",null, null, null, 100,1100, this));
			actions.addObject(EOAction.actionForObject("modeNormal","App/Logs/Mode Normal","Désactivation du mode debug de l'application",null, null, null, 100,1500, this));
			actions.addObject(EOAction.actionForObject("modeDebug","App/Logs/Mode Debug","Activation du mode debug de l'application",null, null, null, 100,1600, this));
			actions.addObject(EOAction.actionForObject("afficherLogs","App/Logs/Afficher Logs","Affichage des logs de l'application",null, null, null, 100,1700, this));
		}
		actions.addObjectsFromArray(NomenclatureOngletMenu.actionsPourMenus(agentGlobalID(), this));
		actions.addObject(EOAction.actionForObject("afficherInfosExecution","App/Infos Application","Informations générales",null, null, null, 100,1400, this));
		actions.addObject(EOAction.actionForObject("quitter","App/Quitter","Quitter l'application",null, null, KeyStroke.getKeyStroke('Q',acceleratorForOS()), 100,1800, this));

		return actions;
	}
	// recherche dans les defaults actions l'action correspondant au titre passé en paramètre et déclenche l'action
	public void declencherAction(NSNotification aNotif) {

		//		if (this == null) {
		//			this = new ActionsListeCtrl(getAppEditingContext());
		//		}

		if (!actionEnCours) {	// pour gérer le problème des notifications suite à la sélection d'un onglet résultant d'une action utilisateur sur un menu
			String nomMethode = null;
			if (aNotif.object() != null) {
				nomMethode = methodePourNomAction((String)aNotif.object());
			} else {
				// Déclenchée par les préférences lors du changement mode fenêtre/modeOnglet
				nomMethode = derniereAction;
			}
			if (nomMethode != null && canPerformActionNamed(nomMethode)) {
				try {
					java.lang.reflect.Method methode = this.getClass().getMethod(nomMethode);	
					methode.invoke(this);
				} catch (Exception e) {
					LogManager.logException(e);
				}
			}
		}
	}	
	private void preparerEtAfficherEtatCivil() {
		modeFenetre = false;
		controleurEmployes = new EOFrameController();	// Pour ne pas avoir de pb puisqu'on considérait auparavant que cette fenêtre était toujours là
		this.afficherEtatCivil();
	}



	/**
	 * 
	 */
	public boolean canPerformActionNamed(String unNom) {

		if (estLocke) {
			if (unNom.equals("toutAfficher")) {
				return true;
			} else {
				return false;
			}
		} else {
			if (unNom.equals("modeNormal")) {
				return LogManager.estModeDebug();
			}
			if (unNom.equals("modeDebug")) {
				return !LogManager.estModeDebug();
			}
			if (agentGlobalID() == null) {
				return true;
			}
			EOAgentPersonnel agent = getAgentPersonnel();
			if (unNom.equals("afficherAgents")) {
				return agent.peutAfficherAgents();
			} else if (unNom.equals("afficherAccidentsTravail")) {
				return agent.peutAfficherAccidentTravail();
			} else if (unNom.equals("afficherTypeElectionHU")) {	
				return agent.peutCreerListesElectorales() && EOGrhumParametres.isGestionHu();
			} else {
				String[] droitsAVerifier = NomenclatureOngletMenu.droitsPourMethode(unNom);
				if (droitsAVerifier != null) {
					boolean result = verifierDroits(agent,droitsAVerifier);
					return result;
				} else {	// si on ne trouve pas de valeur on vérifie
					if (unNom.equals("afficherEditionDivers")) {
						return SuperFinder.rechercherEntite(getEdc(),"ModuleImpression").count() > 0;
					} else {
						return true;
					}
				}
			}
		}
	}

	public void couper() {
		if (lastActivedTextField != null) {
			lastActivedTextField.cut();
		}
	}
	public void copier() {
		if (lastActivedTextField != null) {
			lastActivedTextField.copy();
		}
	}
	public void coller() {
		if (lastActivedTextField != null) {
			lastActivedTextField.paste();
		}
	}
	public void toutSelectionner() {
		if (lastActivedTextField != null) {
			lastActivedTextField.selectAll();
		}
	}
	public void changerPositionListeAgents() {
		if (!isShowListeAgents()) {
			estListeAgentsAGauche = !estListeAgentsAGauche;
			if (estListeAgentsAGauche) {
				controleurEmployes.window().setLocation(0,0);
			}
			changerPositionFenetres();
		}
	}
	public void toutAfficher() {
		EOFrameController controleur = null;
		// vérifier si il y a une fenêtre en cours d'édition
		if (editedDocuments().count() > 0) {
			controleur = (EOFrameController)((EOInterfaceController)editedDocuments().objectAtIndex(0)).supercontroller();
		}
		// ne pas utiliser la méthode du WindowObserver.bringAllToFront car elle casse les menus
		java.util.Enumeration<EOInterfaceController> e = documents().objectEnumerator();
		while (e.hasMoreElements()) {
			((EOFrameController)(e.nextElement()).supercontroller()).activate();
		}
		if (controleur != null) {
			((EOFrameController)controleur).activateWindow();
		}
	}


	/**
	 *
	 */
	public void toutFermer() {
		java.util.Enumeration<EOController> e = listeControleurs.controleurs().objectEnumerator();
		while (e.hasMoreElements()) {
			EOController controleur = e.nextElement();
			if (controleur instanceof ModelePage) {
				if (controleur instanceof ListeAgents == false) {
					((ModelePage)controleur).arreter();
					supprimerControleurPourAction(controleur);
				}
			} else if (controleur instanceof VueAvecOnglets) {
				((VueAvecOnglets)controleur).arreter();
				supprimerControleurPourAction(controleur);
			} else if (controleur instanceof InterfaceAvecFenetre) {
				((InterfaceAvecFenetre)controleur).arreter();
				supprimerControleurPourAction(controleur);
			}
		}
	}
	public void afficherPreferences() {
		EOGlobalID preferencesID = null;
		if (preferencesUtilisateur != null) {
			preferencesID = getEdc().globalIDForObject(preferencesUtilisateur);
		}
		GestionPreferences preferences = new GestionPreferences(preferencesID,agentGlobalID);
		EOFrameController.runControllerInNewFrame(preferences,"Préférences Utilisateur");
		((EOFrameController)preferences.supercontroller()).window().setLocationRelativeTo(null);	// centrage sur l'écran
	}

	private void setNomBase(String nomBase) {
		this.nomBase = nomBase;
	}
	private String getNomBase() {
		return nomBase;
	}

	public void afficherInfosExecution() {
		String titre = "Infos Application";
		RunTimeInfos controleur = (RunTimeInfos)listeControleurs.controleurAvecNom(titre);
		if (controleur == null) {
			controleur = new RunTimeInfos(nomBase,individuCourantID,titre);
			listeControleurs.ajouterControleur(controleur,titre);
			EOFrameController.runControllerInNewFrame(controleur,titre);
			((EOFrameController)controleur.supercontroller()).window().setLocation(0,0);	// centrage sur l'écran
			ajouterAction(controleur,titre,"afficherFenetre",50);	// l'action sera exécutée par le contrôleur lui-même
		} else {
			((EOFrameController)controleur.supercontroller()).activateWindow();
		}
	}

	public void forceQuit() {
		super.quit();
	}

	/** Surcharge de la m&acute;thode de EOApplication pour gerer le Ctrl-Q
	 * On ne permet de quitter que si on peut utiliser le menu "quitter"
	 */	
	public void quit() {
		if (!actionEnCours && canPerformActionNamed("quitter")) {
			actionEnCours = true;
			quitter();
		}
	}
	public void quitter() {
		if (windowObserver().activeWindow() == null) {
			super.quit();
			actionEnCours = false;
		} else if (EODialogs.runConfirmOperationDialog("Attention","Voulez-vous vraiment quitter l'application?","Oui","Non")) {
			super.quit();
		} else
			actionEnCours = false;

	}

	/**
	 * 
	 */
	public void afficherListeEmployes() {
		controleurEmployes.activate();
	}
	/**
	 * 
	 */
	public void afficherListeEmployesNEW() {
		AgentsCtrl.sharedInstance().open();
	}

	/**
	 * 
	 * @return
	 */
	public boolean estUniquementListeEmployes() {
		return (windowObserver().visibleWindows().count() == 1);
	}
	public void supprimerControleurPourAction(EOController controleur) {
		for (java.util.Enumeration<String> e = listeControleurs.nomControleurs().objectEnumerator();e.hasMoreElements();) {
			String titre = (String)e.nextElement();
			EOController controller = (EOController)listeControleurs.controleurAvecNom(titre);
			if (controller == controleur) {
				EOAction action = actionPourTitre(titre);
				NSMutableArray<EOAction> actions = new NSMutableArray<EOAction>(additionalActions());
				actions.removeObject(action);
				setAdditionalActions(actions);
				listeControleurs.supprimerControleur(titre);
				break;
			}
		}
	}
	/** retourne le controleur de document associe a la fenetre dont le nom est passe en parametre */
	public EOController controleurDocumentPourFenetre(String nomFenetre) {
		return (EOController)listeControleurs.controleurAvecNom(nomFenetre);
	}

	/** retourne le nom de la methode associee a une action */
	public String methodePourNomAction(String nomAction) {
		for (EOAction action : defaultActions()) {
			if (action.actionTitle().equals(nomAction))
				return action.actionName();
		}
		return null;
	}


	/**
	 * 
	 * @param estDemarrage
	 */
	private void initAvecPreferences(boolean estDemarrage) {
		boolean exclureHeberges = false;
		if (preferencesUtilisateur != null) {
			modeFenetre = preferencesUtilisateur.modeFenetre();
			estListeAgentsAGauche = preferencesUtilisateur.afficherListeAgentsAGauche();
			exclureHeberges = preferencesUtilisateur.exclureHeberges();

		} else {
			String valeurParDefaut = (String)arguments().objectForKey("prefsModeFenetre");
			modeFenetre = valeurParDefaut != null && valeurParDefaut.equals(CocktailConstantes.VRAI);
			valeurParDefaut = (String)arguments().objectForKey("prefsPositionListeAgents");
			estListeAgentsAGauche = valeurParDefaut != null && valeurParDefaut.equals("G");
		}
		System.out.println(" >>> Repertoire impression : " + directoryImpression());
		if (estDemarrage) {
			boolean afficherAgentsAuDemarrage = false;
			if (preferencesUtilisateur != null) {
				afficherAgentsAuDemarrage = preferencesUtilisateur.afficherAgentsAuDemarrage();
			} else {
				String valeurParDefaut = (String)arguments().objectForKey("prefsAfficherAgents");
				afficherAgentsAuDemarrage = valeurParDefaut != null && valeurParDefaut.equals(CocktailConstantes.VRAI);
			}

			EOGlobalID structureID = null;
			if (preferencesUtilisateur != null && preferencesUtilisateur.structure() != null) {
				structureID = getEdc().globalIDForObject(preferencesUtilisateur.structure());
			}
			ajouterAction(this,DEPLACER_LISTE,"changerPositionListeAgents",10);
			ajouterAction(this,"----------------","",20);
			ajouterAction(this,TOUT_AFFICHER,"toutAfficher",30);
			ajouterAction(this,"Tout Fermer","toutFermer",40);
			ajouterAction(this,"----------------","",50);
			if (!isShowListeAgents()) {
				listeAgents = new ListeAgents();
				listeAgents.initialiser(afficherAgentsAuDemarrage,categoriePersonnel(),structureID,false,false,false,exclureHeberges);
				ajouterAction("Liste des Employés");
			}
			else {
				ajouterAction(this, "Liste des employés ", "afficherListeEmployesNEW", 50);
			}
			ajouterAction(this,"----------------","",50);
			ajouterAction(this, "DIFs", "afficherDifs", 50);
			if (!isShowListeAgents()) {
				controleurEmployes = new EOFrameController();
				((JFrame)controleurEmployes.window()).getContentPane().add(listeAgents.component());
				changerTailleFenetre();
				listeAgents.activer();	
				modifierPolice(((JFrame)controleurEmployes.window()).getJMenuBar());
				controleurEmployes.setDisposeIfDeactivated(false);
				controleurEmployes.activateWindow();
				controleurEmployes.window().setLocation(0,0);
			}
		} else {
			if (!isShowListeAgents() && estListeAgentsAGauche) {
				controleurEmployes.window().setLocation(0,0);
			}
			changerTailleFenetre();
			changerPositionFenetres();
		}
		String preferencesAffichage = "INFOS";
		if (preferencesUtilisateur != null) {
			preferencesAffichage = preferencesUtilisateur.ongletParDefaut();
			AgentsCtrl.sharedInstance().setActivePane(preferencesUtilisateur.ongletParDefaut());
		}
		if (preferencesAffichage != null) {
			NSNotificationCenter.defaultCenter().postNotification(ACTIVER_ACTION,preferencesAffichage);	// déclencher l'action associée à la fenêtre sélectionnée
		}
	}
	private void loadNotifications(boolean estAccesRestreint) {
		if (!estAccesRestreint) {
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockSaisie", new Class[] { NSNotification.class }), ModelePage.LOCKER_ECRAN, null);
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("unlockSaisie", new Class[] { NSNotification.class }), ModelePage.DELOCKER_ECRAN, null);
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("employeHasChanged", new Class[] { NSNotification.class }), ListeAgents.CHANGER_EMPLOYE, null);
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("nettoyer", new Class[] { NSNotification.class }), ListeAgents.NETTOYER_CHAMPS, null);
		}
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("declencherAction", new Class[] { NSNotification.class }), ACTIVER_ACTION, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("arreterControleur", new Class[] { NSNotification.class }), ModelePage.ARRET_CONTROLEUR, null);
	}
	public void afficherInfos(String nomMethode,String nomClasse) {
		derniereAction = nomMethode;
		preparerPourAction(true);
		boolean composantAffiche = true;
		String titre = NomenclatureOngletMenu.titrePourMethode(nomMethode);
		// retourne le titre de l'onglet ou la fenetre à afficher pour cette méthode
		if (titre == null)	// cas d'une methode non definie dans la nomenclature
			return;

		if (afficherFenetre(titre) == false || (!modeFenetre && !(composantAffiche = estAfficheComposant(titre,nomClasse)))) {

			try {

				ModelePage controleur = (ModelePage)Class.forName(nomClasse).newInstance();

				// on attend un constructeur du type new GestionEtatCivil()
				if (controleur instanceof ModelePageAvecIndividu)
					((ModelePageAvecIndividu)controleur).initialiser(individuCourantID);

				if (composantAffiche) {
					afficherFenetre(controleur,titre);
				}
				else {
					afficherComposant(controleur,titre);
				}

			} catch (Exception e) {
				LogManager.logErreur("nomClasse inconnu " + nomClasse);
				LogManager.logException(e);
			}

		}
		preparerPourAction(false);
	}

	// affiche la fenêtre dont le titre est passé en paramètre
	private boolean afficherFenetre(String titre) {
		String titreFenetre;
		String path = null;
		if (!modeFenetre) {
			path = pathPourTitre(titre);
			if (path != null) {
				titreFenetre = (String)NSArray.componentsSeparatedByString(path,"/").objectAtIndex(0);
			} else {
				titreFenetre = null;
			}
		} else {
			titreFenetre = titre;
		}
		EOController controleur = null;
		if (titreFenetre != null) {
			controleur = (EOController)listeControleurs.controleurAvecNom(titreFenetre);
			if (controleur != null && controleur instanceof EOInterfaceController) {
				((EOFrameController)controleur.supercontroller()).activate();
			}
		}
		if (controleur != null && !modeFenetre) {
			if (path != null && controleur instanceof VueAvecOnglets) {
				((VueAvecOnglets)controleur).activerOngletPourPath(path,"/");
			}
		}
		return controleur != null;
	}

	// affiche le composant dont le titre est passé en paramètre
	// dans le cas d'une fenêtre, affiche la fenêtre, dans le cas d'une vue à Onglets, instancie le contrôleur de fenêtre
	// et affiche la vue choisie en se basant sur le path de la commande de menu
	private void afficherFenetre(ModelePage controleur,String titre) {
		String titreFenetre;
		String path = null;
		EOInterfaceController controleurPourFenetre;
		if (!modeFenetre) {
			path = pathPourTitre(titre);
			if (path != null) {
				titreFenetre = (String)NSArray.componentsSeparatedByString(path,"/").objectAtIndex(0);
				controleurPourFenetre = instancierControleurAvecNom(titreFenetre);
			} else {
				titreFenetre = null;
				controleurPourFenetre = null;
			}
		} else {
			titreFenetre = titre;
			controleurPourFenetre = controleur;
		}
		if (controleurPourFenetre != null) {
			ajouterAction(controleurPourFenetre,titreFenetre);
			EOFrameController.runControllerInNewFrame(controleurPourFenetre,titreFenetre);
			if (controleurPourFenetre != null && controleurPourFenetre.supercontroller() instanceof EOFrameController) {
				((EOFrameController)controleurPourFenetre.supercontroller()).window().setVisible(true);
				JFrame frame = (JFrame)((EOFrameController)controleurPourFenetre.supercontroller()).window();
				ImageIcon icon = CocktailIcones.ICON_APP_LOGO;
				icon.setDescription(titreFenetre);
				frame.setIconImage(icon.getImage());
			}

		}
		changerPositionFenetre(controleurPourFenetre);
		if (controleurPourFenetre instanceof VueAvecOnglets) {
			((VueAvecOnglets)controleurPourFenetre).ajouterControleurPourOngletAvecPath(controleur,path,"/");
		}

	}
	private void afficherComposant(ModelePage controleur,String titre) {

		if (modeFenetre) {
			return;
		}
		String path = pathPourTitre(titre);

		if (path != null) {
			String titreFenetre = (String)NSArray.componentsSeparatedByString(path,"/").objectAtIndex(0);
			EOController controleurPourFenetre = (EOController)listeControleurs.controleurAvecNom(titreFenetre);

			if (controleur != null && controleurPourFenetre instanceof VueAvecOnglets) {
				((VueAvecOnglets)controleurPourFenetre).ajouterControleurPourOngletAvecPath(controleur,path,"/");
			}

		}
	}
	private EOInterfaceController instancierControleurAvecNom(String nomFenetre) {
		String nom = nomFenetre;
		try {
			if (nomFenetre.equals("Employé")) {
				nom = "Employe";
			} else if (nomFenetre.equals("Rémun")) {
				nom = "Remun";
			}
			VueAvecOnglets controleur = (VueAvecOnglets)Class.forName("org.cocktail.mangue.client.onglets.Gestion" + nom).newInstance();	// on attend un constructeur du type new GestionEmploye()
			controleur.initialiser(nomFenetre,100, 100 , individuCourantID);
			return controleur;
		} catch (Exception e) {
			LogManager.logErreur("nomClasse inconnu : org.cocktail.mangue.client.onglets.Gestion" + nom);
			LogManager.logException(e);
			return null;
		}
	}
	// retourne true si le composant de cette classe est affichée
	private boolean estAfficheComposant(String titre,String nomClasse) {
		String titreFenetre;
		String path = pathPourTitre(titre);
		if (path == null) {
			return false;
		}
		titreFenetre = (String)NSArray.componentsSeparatedByString(path,"/").objectAtIndex(0);

		EOController controleur = (EOController)listeControleurs.controleurAvecNom(titreFenetre);
		if (controleur != null && controleur instanceof VueAvecOnglets && ((VueAvecOnglets)controleur).aChargeControleurPourClasse(nomClasse)) {
			return true;
		} else {
			return false;
		}
	}
	private void ajouterAction(EOController controleur,String titre,String nomAction,int priorite) {
		EOAction action = EOAction.actionForObject(nomAction,"Fen/" + titre, titre, null, null, null, 2000,priorite, controleur);
		listeControleurs.ajouterControleur(controleur,titre);
		setAdditionalActions(EOAction.mergedActions(additionalActions(),new NSArray(action)));	// pour que le menu "Fenêtres" apparaisse

	}
	private void ajouterAction(String titre) {
		ajouterAction(this,titre,"afficherListeEmployes",100);
	}
	private void ajouterAction(EOController controleur,String titre) {
		ajouterAction(controleur,titre,"afficherFenetre",100);
	}
	private EOAction actionPourTitre(String titre) {
		for (java.util.Enumeration<EOAction> e = additionalActions().objectEnumerator();e.hasMoreElements();) {
			EOAction action = (EOAction)e.nextElement();
			if (action.actionTitle().equals(titre))
				return action;
		}
		return null;
	}
	private String pathPourTitre(String titre) {
		for (java.util.Enumeration<EOAction> e = defaultActions().objectEnumerator();e.hasMoreElements();) {
			EOAction action = (EOAction)e.nextElement();
			if (action.actionTitle().equals(titre))
				return action.descriptionPath();
		}
		return null;
	}
	private void changerVerrou(boolean enabled) {
		if (windowObserver().activeWindow() != null) {
			if (windowObserver().activeWindow() instanceof JFrame) {
				JMenuBar menubar = ((JFrame)windowObserver().activeWindow()).getJMenuBar();
				JMenu menu = menubar.getMenu(menubar.getMenuCount() - 1);
				for (int i = 0; i < menu.getItemCount(); i ++) {
					JMenuItem menuItem = menu.getItem(i);
					if (menuItem.getText().equals(((JFrame)windowObserver().activeWindow()).getTitle()) == false) {
						// les articles de menu et les fenêtres sont identiques
						menuItem.setEnabled(menuItem.getText().equals(TOUT_AFFICHER) || menuItem.getText().equals(DEPLACER_LISTE) || enabled);
					}
				}
			}
		}
		// locker les fenêtres en cas de mode onglet
		if (!modeFenetre) {
			for (java.util.Enumeration<EOController> e = listeControleurs.controleurs().objectEnumerator();e.hasMoreElements();) {
				EOController controleur = e.nextElement();
				if (controleur instanceof ListeAgents == false && controleur.supercontroller() != null && controleur.supercontroller() instanceof EOFrameController) {
					EOFrameController controller = (EOFrameController)controleur.supercontroller();
					if (!enabled) {
						WindowListener[] listeners = controller.window().getWindowListeners();
						for (int i = 0; i < listeners.length;i++) {
							if (listeners[i] == controller)
								controller.window().removeWindowListener((WindowListener)listeners[i]);
						}
					} else
						controller.window().addWindowListener(controller);
				}
			}
		}
	}
	// change la position de toutes les fenetres
	private void changerPositionFenetres() {

		for (java.util.Enumeration<EOController> e = listeControleurs.controleurs().objectEnumerator();e.hasMoreElements();) {
			EOController controleur = e.nextElement();
			if (controleur instanceof EOInterfaceController
					&& controleur instanceof ListeAgents == false
					&& controleur instanceof InterfaceAvecFenetre == false) {
				changerPositionFenetre((EOInterfaceController)controleur);
			}
		}
	}

	/**
	 * 
	 * @param controleurPourFenetre
	 */
	private void changerPositionFenetre(EOInterfaceController controleurPourFenetre) {

		if (modeFenetre) {
			boolean decalerFenetre = controleurPourFenetre instanceof ModelePageAvecIndividu || controleurPourFenetre instanceof GestionHierarchie;
			((ModelePage)controleurPourFenetre).decalerFenetre(positionPourFenetre(decalerFenetre),0);
		} else {
			String titre = ((JFrame)((EOFrameController)controleurPourFenetre.supercontroller()).window()).getTitle();
			boolean decalerFenetre = titre.equals("Employé");
			if (Toolkit.getDefaultToolkit().getScreenSize().getWidth() > 1024) {
				decalerFenetre = decalerFenetre || titre.equals("Outil");
			}
			if (decalerFenetre) {
				((VueAvecOnglets)controleurPourFenetre).decalerFenetre(positionPourFenetre(decalerFenetre),0);
			}
		}

		if (!isShowListeAgents() && !estListeAgentsAGauche) {
			int position = ((EOFrameController)controleurPourFenetre.supercontroller()).window().getWidth();
			int positionActuelle = controleurEmployes.window().getX();
			if (positionActuelle < position + DECALAGE_FENETRE) {
				controleurEmployes.window().setLocation(position + DECALAGE_FENETRE, 0);
			}
		}
	}

	/**
	 * 
	 * @param estDemarrage
	 */
	private void preparerPourAction(boolean estDemarrage) {
		Window activeWindow = windowObserver().activeWindow();
		if (activeWindow != null) {
			Cursor cursor;
			if (estDemarrage) {
				cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
			} else {
				cursor = Cursor.getDefaultCursor();
			}
			activeWindow.setCursor(cursor);
		}
		actionEnCours = estDemarrage;	// pour gérer le problème des notifications suite à la sélection d'un onglet résultant d'une action utilisateur sur un menu
	}

	/**
	 * 
	 */
	private void changerTailleFenetre() {
		if (!isShowListeAgents() && listeAgents != null) {

			listeAgents.changerTaille();
			JComponent component = (JComponent)((JFrame)controleurEmployes.window()).getContentPane();
			component.setSize(listeAgents.component().getSize());
			controleurEmployes.window().pack();

			// Pour remettre la fenêtre à la bonne position et largeur
			listeAgents.component().setLocation(0,0);
			controleurEmployes.window().setSize(listeAgents.component().getWidth(),controleurEmployes.window().getHeight());

		}
	}
	private void modifierPolice(JMenuBar menubar) {
		for (int i = 0; i < menubar.getMenuCount();i++) {
			JMenu menu = menubar.getMenu(i);
			modifierPolice(menu);
		}
		Font font = menubar.getFont();
		menubar.setFont(new Font(font.getName(),font.getStyle(),10));
	}
	private void modifierPolice(JMenu menu) {
		for (int i = 0; i < menu.getMenuComponentCount();i++) {
			JMenuItem menuItem = menu.getItem(i);
			if (menuItem != null) {	// a l'initialisation
				if (menuItem instanceof JMenu) {
					modifierPolice((JMenu)menuItem);
				} else {
					Font font = menuItem.getFont();
					menuItem.setFont(new Font(font.getName(),font.getStyle(),10));
				}

			}
			Font font = menu.getFont();
			menu.setFont(new Font(font.getName(),font.getStyle(),10));
		}
	}

	/**
	 * 
	 * @param agent
	 * @param droitsAVerifier
	 * @return
	 */
	private boolean verifierDroits(EOAgentPersonnel agent,String[] droitsAVerifier) {
		String droit = (String)droitsAVerifier[0];
		if (droit.equals("agtPrime")) {
			// Faire une vérification spéciale pour les primes (pour l'administration, on veut
			// que deux droits soient valides pour autoriser
			for (int i = 0; i < droitsAVerifier.length;i++) {
				droit = (String)agent.valueForKey(droitsAVerifier[i]);
				if (droit == null || droit.equals(CocktailConstantes.FAUX)) {
					return false;	// Si on trouve un seul des droits qui n'est pas respecté
				}
			}
			return true;
		} else {
			boolean peutAfficher = false;
			for (int i = 0; i < droitsAVerifier.length;i++) {
				droit = (String)agent.valueForKey(droitsAVerifier[i]);
				if (droit != null && droit.equals(CocktailConstantes.VRAI)) {
					peutAfficher = true;
					if (droitsAVerifier[i].equals("agtElection")) {
						peutAfficher = peutAfficher && EOGrhumParametres.isUtilisationListeElectorale();
					}
					break;
				}
			}
			return peutAfficher;
		}

	}

	// Classe privée pour gérer les contrôleurs
	private class ListeControleurs {
		private NSMutableDictionary dictionnaireControleurs;

		public ListeControleurs() {
			dictionnaireControleurs = new NSMutableDictionary();
		}
		// nom associé au contrôleur
		public void ajouterControleur(EOController controleur,String nom) {
			dictionnaireControleurs.setObjectForKey(controleur,nom);
		}
		public void supprimerControleur(String nom) {
			dictionnaireControleurs.removeObjectForKey(nom);
		}
		public EOController controleurAvecNom(String nom) {
			return (EOController)dictionnaireControleurs.objectForKey(nom);
		}
		public NSArray controleurs() {
			return dictionnaireControleurs.allValues();
		}
		public NSArray nomControleurs() {
			return dictionnaireControleurs.allKeys();
		}
	}
	private int acceleratorForOS() {
		String operationSystem = System.getProperty("os.name");
		if (operationSystem != null) {
			operationSystem = operationSystem.toLowerCase();
			if (operationSystem.startsWith("mac")) {
				return java.awt.event.InputEvent.META_MASK;
			}
		}
		return java.awt.event.InputEvent.CTRL_MASK;
	}
	// InterfaceApplication : pour rendre cette classe compatible avec le framework CktlApplicationJavaClient
	public EOGlobalID compteLoginGlobalID() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(agentGlobalID(),getEdc());
		EOCompte compte = EOCompte.rechercherComptePourLogin(getEdc(),agent.agtLogin());
		return getEdc().globalIDForObject(compte);
	}

	public Number noIndividuUtilisateurApplication() {
		return individuCourantID;
	}
	public String nomApplication() {
		return "Mangue";
	}
	// Utilitaires de temps
	String defaultTimeZone() {

		String valeur = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_TIME_ZONE);
		if (valeur == null) {
			EODistributedObjectStore store = (EODistributedObjectStore)getEdc().parentObjectStore();
			valeur = (String)store.invokeRemoteMethodWithKeyPath(getEdc(), "session", "clientSideRequestGetParam", new Class[] {String.class}, new Object[] {"DEFAULT_TIME_ZONE"}, false);
			if (valeur == null) {
				valeur = "GMT";
			}
		}
		return valeur;
	}

	/**
	 * 
	 * @return
	 */
	public String getSituationAgent(EOIndividu individu) {

		String chaineSituation = "";
		EOCarriere carriere = EOCarriere.carrierePourDate(getAppEditingContext(), individu, DateCtrl.today());
		if (carriere != null) {
			EOChangementPosition position = EOChangementPosition.positionPourDate(getAppEditingContext(), individu, DateCtrl.today());
			if (position != null)  {

				chaineSituation += position.toPosition().libelleLong();

				EOModalitesService modalite = EOModalitesService.modalitePourDate(getAppEditingContext(), individu, DateCtrl.today());
				if (modalite != null) {

					if (modalite.estTempsPartiel() == false) {
						if (modalite.estCrct() == false)
							chaineSituation += " ( " + modalite.modType() +  " à " + modalite.quotite() + " % )";
						else
							chaineSituation += " ( " + modalite.modType()+ " )";
					}
					else {

						String chaineComplement = " ( " + modalite.modType() +  " à " + modalite.quotite() + " % )";

						EOTempsPartiel tempsPartiel = EOTempsPartiel.findForIndividuAndDate(getAppEditingContext(), individu, DateCtrl.today());
						if (tempsPartiel != null) {

							if (tempsPartiel.dFinExecution() != null) {
								if (DateCtrl.isBefore(tempsPartiel.dFinExecution(), DateCtrl.today()))
									chaineComplement = " à 100 % ";
							}
							EORepriseTempsPlein reprise = EORepriseTempsPlein.rechercherReprisePourTempsPartiel(getAppEditingContext(), tempsPartiel);
							if (reprise != null) {
								if (DateCtrl.isBeforeEq(reprise.dRepriseTempsPlein(), DateCtrl.today()))
									chaineComplement = " à 100 % ";
							}
						}

						chaineSituation += chaineComplement;

					}

				}
			}
			else
				chaineSituation += " ! PAS DE POSITION ! ";
		}	
		else {

			EOContrat contrat = EOContrat.contratPrincipalPourDate(getAppEditingContext(), individu, DateCtrl.today());
			if (contrat != null) {
				if (individu.estHomme())
					chaineSituation += " Contractuel ";
				else
					chaineSituation += " Contractuelle ";
			}
			else {

				NSArray<EOCarriere> carrieres = EOCarriere.findForIndividu(getAppEditingContext(), individu);
				if (carrieres.size() > 0) {
					chaineSituation = "Ancien agent";
				}
				else {
					NSArray<EOContratHeberges> heberges = EOContratHeberges.findForIndividuAndPeriode(getAppEditingContext(), individu, DateCtrl.today(), DateCtrl.today());
					if (heberges.size() > 0) {
						chaineSituation = "Hébergé";
					}
					else {
						NSArray<EOVacataires> vacations = EOVacataires.findForIndividuAndPeriode(getAppEditingContext(), individu, DateCtrl.today(), DateCtrl.today());
						if (vacations != null && vacations.size() > 0) {
							chaineSituation = "Vacataire";
						}
						else {
							chaineSituation = "";
						}
					}
				}
			}
		}

		// CONGES - CLD, CFP , CMO, CLM, etc ...
		NSArray<EOAbsences> absences = EOAbsences.rechercherAbsencesLegalesPourIndividuEtDates(getAppEditingContext(), individu, DateCtrl.today(), DateCtrl.today());
		for (EOAbsences myAbsence : absences) {
			if (myAbsence.toTypeAbsence().estCongeLegal() && myAbsence.estValide()) {
				chaineSituation += " ("+myAbsence.toTypeAbsence().libelleCourt() + ")";						
			}
		}

		return chaineSituation;

	}

	public void modeNormal() {
		preparerLogPourDebug("N");
	}
	public void modeDebug() {
		preparerLogPourDebug("O");
	}
	private void preparerLogPourDebug(String modeDebug) {
		LogManager.changerModeDebug(modeDebug);
	}
	public void afficherParametresMangue() {
		ParametresCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherConnectedUsers() {
		ConnectedUsersCtrl win = new ConnectedUsersCtrl(getEdc());
		try {
			win.openDialog(new JFrame(), true);
		} catch (Exception e1) {
			EODialogs.runErrorDialog("ERREUR", e1.getMessage());
		}
	}
	public void afficherAnciennete() {
		afficherInfos("afficherAnciennete", OutilAnciennete.class.getName());
	}
	public void afficherEtatCivil() {
		afficherInfos("afficherEtatCivil",GestionEtatCivil.class.getName());
	}
	public void afficherConges() {
		afficherInfos("afficherConges",GestionAbsences.class.getName());
	}
	public void afficherLogs() {
		new LogsApplication(getManager());
	}

	public void afficherPrimesIndividu() {
		afficherInfos("afficherPrimesIndividu",GestionPrimesIndividu.class.getName());
	}
	public void afficherLbudsIndividu() {
		afficherSwapViewBudget();
	}
	public void afficherCFA() {
		afficherInfos("afficherCFA",GestionCFA.class.getName());
	}
	public void afficherCongeNbi() {
		afficherInfos("afficherCongeNbi",GestionCongeBonifie.class.getName());
	}
	public void afficherMAD() {
		afficherInfos("afficherMAD",GestionMAD.class.getName());
	}
	public void afficherPromotionsIndividu() {
		afficherInfos("afficherPromotionsIndividu","org.cocktail.mangue.client.carrieres.GestionPromotionIndividu");
	}
	public void afficherOccupations() {
		afficherInfos("afficherOccupations","org.cocktail.mangue.client.occupations.GestionOccupation");
	}
	public void afficherSituationGeographique() {
		afficherInfos("afficherSituationGeographique","org.cocktail.mangue.client.occupations.GestionSituationGeo");
	}
	//	public void afficherAnciennete() {
	//		afficherInfos("afficherAnciennete",OutilAnciennete.class.getName());
	//	}
	public void afficherSynthese() {
		afficherInfos("afficherSynthese","org.cocktail.mangue.client.outils.individu.SyntheseCarriere");
	}
	public void afficherHeberges() {
		ListeHebergesCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherDecharges() {
		ListeDechargesCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherListePrimes() {
		ListePrimesCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherFormations() {
		ListeFormationsCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherDifs() {
		ListeDifsCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherVacations() {
		VacationsCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherEmplois() {
		EmploisCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherStocks() {
		afficherInfos("afficherStocks",GestionStocks.class.getName());
	}
	public void afficherEtatPersonnels() {
		afficherInfos("afficherEtatPersonnels",GestionEtatPersonnels.class.getName());
	}
	public void afficherCifs() {
		afficherInfos("afficherCifs", OutilDif.class.getName());
	}
	public void afficherMedecins() {
		afficherInfos("afficherMedecins", GestionMedecins.class.getName());
	}
	public void afficherResponsablesAcmo() {
		afficherInfos("afficherResponsablesAcmo", EditionAcmos.class.getName());
	}
	public void afficherEditionExamens() {
		afficherInfos("afficherEditionExamens", EditionExamens.class.getName());
	}
	public void afficherEditionVaccins() {
		afficherInfos("afficherEditionVaccins", EditionVaccins.class.getName());
	}
	public void afficherEditionsVisites() {
		afficherInfos("afficherEditionsVisites", EditionVisites.class.getName());
	}
	public void afficherEditeurRequetes() {
		afficherInfos("afficherEditeurRequetes",GestionRequetesAgent.class.getName());
	}
	public void afficherEditionAgentsIdentite() {
		afficherInfos("afficherEditionAgentsIdentite",InterfaceRequeteIndividu.class.getName());
	}
	public void afficherEditionAgentsContrat() {
		afficherInfos("afficherEditionAgentsContrat",InterfaceRequeteContrat.class.getName());
	}
	public void afficherEditionAgentsCarriere() {
		afficherInfos("afficherEditionAgentsCarriere",InterfaceRequeteCarriere.class.getName());
	}
	public void afficherEditionAgentsOccupation() {
		afficherInfos("afficherEditionAgentsOccupation",InterfaceRequeteOccupation.class.getName());
	}
	public void afficherEditionAgentsSpecialisation() {
		afficherInfos("afficherEditionAgentsSpecialisation",InterfaceRequeteSpecialisation.class.getName());
	}
	public void afficherEditionConges() {
		afficherInfos("afficherEditionConges", "org.cocktail.mangue.client.requetes.GestionRequetesConge");
	}
	public void afficherEditionActivites() {
		afficherInfos("afficherEditionActivites", GestionRequetesActivite.class.getName());
	}
	public void afficherEditionServices() {
		afficherInfos("afficherEditionServices", GestionRequetesEmploi.class.getName());
	}
	public void afficherEditionDivers() {
		afficherInfos("afficherEditionDivers", GestionEditionsDivers.class.getName());
	}
	public void afficherPostes() {
		afficherInfos("afficherPostes",GestionPostes.class.getName());
	}
	public void afficherExtractionEtatComparatif() {
		afficherInfos("afficherExtractionEtatComparatif",ExtractionEtatComparatif.class.getName());
	}
	public void afficherAdministrationPrimes() {
		afficherInfos("afficherAdministrationPrimes",AdministrationPrimes.class.getName());
	}
	public void afficherPrimeExclusions() {
		afficherInfos("afficherPrimeExclusions",GestionExclusionsPrime.class.getName());
	}
	public void afficherPrimeExclusionsPosition() {
		afficherInfos("afficherPrimeExclusionsPosition",GestionExclusionsPosition.class.getName());

	}
	public void afficherPrimeExclusionsConge() {
		afficherInfos("afficherPrimeExclusionsConge",GestionExclusionsConge.class.getName());
	}
	public void afficherPrimeFonctions() {
		afficherInfos("afficherPrimeFonctions",GestionFonctions.class.getName());
	}
	public void afficherPrimePopulations() {
		afficherInfos("afficherPrimePopulations",GestionPopulations.class.getName());
	}
	public void afficherPrimeParametres() {
		afficherInfos("afficherPrimeParametres",GestionParametres.class.getName());
	}
	public void afficherPrimeProgrammes() {
		afficherInfos("afficherPrimeProgrammes",GestionProgrammes.class.getName());
	}
	public void afficherPrimeStructures() {
		afficherInfos("afficherPrimeStructures",GestionStructures.class.getName());
	}
	public void afficherPrimes() {
		afficherInfos("afficherPrimes",GestionPrimes.class.getName());
	}

	public void afficherArretesPrime() {
		afficherInfos("afficherArretesPrime",GestionPersonnalisations.class.getName());
	}
	public void afficherAgents() {
		afficherInfos("afficherAgents",GestionAgents.class.getName());
	}
	public void afficherServices() {
		afficherInfos("afficherServices",GestionServices.class.getName());
	}
	public void afficherDestinataires() {
		afficherInfos("afficherDestinataires",GestionDestinataires.class.getName());
	}
	public void afficherModulesImpression() {
		afficherInfos("afficherModulesImpression",GestionModulesImpression.class.getName());
	}
	public void afficherModulesImpressionIndividu() {
		afficherInfos("afficherModulesImpressionIndividu",GestionModulesIndividu.class.getName());
	}
	public void afficherModelesPourContrat() {
		afficherInfos("afficherModelesPourContrat",GestionModelesPourImpressionContrat.class.getName());
	}
	public void afficherDCP() {
		afficherInfos("afficherDCP",GestionDCP.class.getName());
	}
	public void afficherEmploisType() {
		afficherInfos("afficherEmploisType",GestionEmploisType.class.getName());
	}
	public void afficherFamillesProfessionnelles() {
		afficherInfos("afficherFamillesProfessionnelles","org.cocktail.mangue.client.administration.GestionFamillesProfessionnelles");
	}
	public void afficherHierarchie() {
		afficherInfos("afficherHierarchie","org.cocktail.mangue.client.administration.GestionHierarchie");
	}
	public void afficherParametresPromotion() {
		afficherInfos("afficherParametresPromotion","org.cocktail.mangue.client.administration.GestionParametresPromotion");
	}
	public void afficherVisas() {
		afficherInfos("afficherVisas", GestionVisas.class.getName());
	}

	/* GESTION DES VISAS */

	public void afficherVisasCongesTitulaires() {
		afficherInfos("afficherVisasCongesTitulaires",GestionVisasCongesCarrieres.class.getName());
	}
	public void afficherVisasCongesContractuels() {
		afficherInfos("afficherVisasCongesContractuels",GestionVisasCongesContractuels.class.getName());
	}

	public void afficherVisasArretesCorps() {
		afficherInfos("afficherVisasArretesCorps",GestionVisasArretesCorps.class.getName());
	}
	public void afficherVisasContrats() {
		afficherInfos("afficherVisasContrats",GestionVisasContrats.class.getName());
	}

	/* GESTION DES ELECTIONS */

	public void afficherTypeElection() {
		afficherInfos("afficherTypeElection","org.cocktail.mangue.client.elections.nomenclature.GestionTypesElection");
	}
	public void afficherElection() {
		afficherInfos("afficherElection","org.cocktail.mangue.client.elections.nomenclature.GestionElections");
	}
	public void afficherCollege() {
		afficherInfos("afficherCollege","org.cocktail.mangue.client.elections.nomenclature.GestionColleges");
	}
	public void afficherComposante() {
		afficherInfos("afficherComposante","org.cocktail.mangue.client.elections.nomenclature.GestionComposantes");
	}
	public void afficherSection() {
		afficherInfos("afficherSection","org.cocktail.mangue.client.elections.nomenclature.GestionSections");
	}
	public void afficherCollegeSection() {
		afficherInfos("afficherCollegeSection","org.cocktail.mangue.client.elections.nomenclature.GestionCollegeSections");
	}
	public void afficherSecteur() {
		afficherInfos("afficherSecteur","org.cocktail.mangue.client.elections.nomenclature.GestionSecteurs");
	}
	public void afficherBureau() {
		afficherInfos("afficherBureau","org.cocktail.mangue.client.elections.nomenclature.GestionBureauxVote");
	}
	public void afficherBudget() {
		afficherInfos("afficherBudget", "org.cocktail.mangue.client.primes.GestionBudget");
	}
	public void afficherExclusion() {
		afficherInfos("afficherExclusion","org.cocktail.mangue.client.elections.nomenclature.GestionExclusions");
	}
	public void afficherInclusionCorps() {
		afficherInfos("afficherInclusionCorps","org.cocktail.mangue.client.elections.nomenclature.GestionInclusionsCorps");
	}
	public void afficherInclusionTypeContrat() {
		afficherInfos("afficherInclusionTypeContrat","org.cocktail.mangue.client.elections.nomenclature.GestionInclusionsTypeContrat");
	}
	public void afficherInclusionDiplome() {
		afficherInfos("afficherInclusionDiplome","org.cocktail.mangue.client.elections.nomenclature.GestionInclusionsDiplome");
	}
	public void afficherParametrageElection() {
		afficherInfos("afficherParametrageElection","org.cocktail.mangue.client.elections.parametrage.GestionParametrages");
	}
	public void afficherListesElectorales() {
		afficherInfos("afficherListesElectorales","org.cocktail.mangue.client.elections.GestionListesElectorales");
	}


	// Gestion des swapviews
	public void afficherSwapViewContrats() {
		afficherInfos("afficherSwapViewContrats",GestionSwapViewContrats.class.getName());
	}
	public void afficherSwapViewBudget() {
		afficherInfos("afficherSwapViewBudget",GestionSwapViewBudget.class.getName());
	}
	public void afficherSwapViewSyntheses() {
		afficherInfos("afficherSwapViewSyntheses",GestionSwapViewAnciennete.class.getName());
	}
	public void afficherSwapViewHeberge() {
		afficherInfos("afficherSwapViewHeberge",GestionSwapViewHeberge.class.getName());
	}
	public void afficherSwapViewCarriere() {
		afficherInfos("afficherSwapViewCarriere",GestionSwapViewCarrieres.class.getName());
	}
	public void afficherSwapViewModalites() {
		afficherInfos("afficherSwapViewModalites",GestionSwapViewModalites.class.getName());
	}
	public void afficherSwapViewProlongations() {
		afficherInfos("afficherSwapViewProlongations",GestionSwapViewProlongations.class.getName());
	}
	public void afficherSwapViewPasse() {
		afficherInfos("afficherSwapViewPasse",GestionSwapViewPasse.class.getName());
	}
	public void afficherSwapViewOccupations() {
		afficherInfos("afficherSwapViewOccupations",GestionSwapViewOccupation.class.getName());
	}
	public void afficherSwapViewAffOcc() {
		afficherInfos("afficherSwapViewAffOcc",GestionSwapViewOccupation.class.getName());
	}
	public void afficherSwapViewVisas() {
		afficherInfos("afficherSwapViewVisas",GestionSwapViewVisas.class.getName());
	}
	public void afficherSwapViewEtatCivil() {
		afficherInfos("afficherSwapViewEtatCivil",GestionSwapViewEtatCivil.class.getName());
	}
	public void afficherSwapViewAbsences() {
		afficherInfos("afficherSwapViewAbsences",GestionSwapViewAbsences.class.getName());
	}
	public void afficherSwapViewAdresses() {
		afficherInfos("afficherSwapViewAdresses",GestionSwapViewAdresses.class.getName());
	}
	public void afficherSwapViewFicheSituationAgent() {
		afficherInfos("afficherSwapViewFicheSituationAgent",GestionSwapViewFicheSituationAgent.class.getName());
	}

	public void afficherFicheSituationAgent() {
		afficherInfos("afficherFicheSituationAgent",GestionSwapViewFicheSituationAgent.class.getName());
	}

	public void afficherSwapViewSyntheseCarriereCIR() {
		afficherInfos("afficherSwapViewSyntheseCarriereCIR",GestionSwapViewSyntheseCarriereCIR.class.getName());
	}

	public void afficherEditionCIR() {
		afficherInfos("afficherSwapViewRequeteCIR",GestionSwapViewAnciennete.class.getName());
	}
	public void afficherSwapViewVacations() {
		afficherInfos("afficherSwapViewVacations",GestionSwapViewVacations.class.getName());
	}
	public void afficherNomenclatures() {
		NomenclaturesCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherPromouvabilites() {	
		PromouvabilitesCtrl.sharedInstance().open();
	}
	public void afficherNbis() {	
		NbiCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherDonneesMedicales() {	
		MedicalCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherCIR() {	
		CirCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherSupInfo() {	
		SupInfoCtrl.sharedInstance(getEdc()).open();	
	}
	public void afficherEvalEC() {	
		EODialogs.runInformationDialog("INFO","Ce menu sera disponible lors de la diffusion " +
				"de la note officielle concernant les évaluations des enseignants chercheurs.");
		//SupInfoEvalECCtrl.sharedInstance(editingContext).open();
	}


}
package org.cocktail.mangue.client.decharges;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.decharges.ListeDechargesView;
import org.cocktail.mangue.client.individu.infoscomp.DechargesCtrl;
import org.cocktail.mangue.client.select.IndividuSelectCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EODecharge;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class ListeDechargesCtrl {

	private static ListeDechargesCtrl 	sharedInstance;
	private static Boolean 			MODE_MODAL = Boolean.FALSE;
	private EOEditingContext		edc;
	private ListeDechargesView 			myView;
	private EODisplayGroup 			eod;
	private boolean 				saisieEnabled, isLocked;

	private ListenerDecharge 		listenerDecharge = new ListenerDecharge();

	private EODecharge				currentDecharge;
	
	private EOAgentPersonnel		currentUtilisateur;
	private boolean 				gereModule;

	public ListeDechargesCtrl(EOEditingContext edc) {

		setEdc(edc);

		eod = new EODisplayGroup();
		myView = new ListeDechargesView(null, MODE_MODAL.booleanValue(), eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnRechercher().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {rechercher();}}
		);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporter();}}
		);

		myView.setListeAnnees(CocktailConstantes.LISTE_ANNEES_DESC);
		myView.getPopupAnnees().setSelectedItem(EODecharge.stringAnneeUniversitaire(new Integer(DateCtrl.getCurrentYear() - 1)));
		
		myView.getMyEOTable().addListener(listenerDecharge);

		myView.getTfFiltreNom().getDocument().addDocumentListener(new ADocumentListener());

		setCurrentUtilisateur(((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());
		
		setSaisieEnabled(false);
		updateInterface();
	}

	
	
	public EOEditingContext getEdc() {
		return edc;
	}
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		setGereModule(currentUtilisateur.peutGererCarrieres() || currentUtilisateur.peutGererVacations());
	}
	public boolean isGereModule() {
		return gereModule;
	}
	public void setGereModule(boolean gereModule) {
		this.gereModule = gereModule;
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ListeDechargesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ListeDechargesCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 */
	public void open()	{

		// Controle ds droits
		if (!isGereModule()) {
			EODialogs.runInformationDialog("ERREUR", "Vous n'avez pas les droits nécessaires pour la gestion des décharges !");
			return;
		}

		CRICursor.setWaitCursor(myView);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	/**
	 * 
	 * @return
	 */
	public EODecharge getCurrentDecharge() {
		return currentDecharge;
	}
	/**
	 * 
	 * @param currentDecharge
	 */
	public void setCurrentDecharge(EODecharge currentDecharge) {
		this.currentDecharge = currentDecharge;
	}

	public void toFront() {
		myView.toFront();
	}
	public boolean saisieEnabled() {
		return saisieEnabled;
	}
	public boolean isLocked() {
		return isLocked;
	}
	public void setIsLocked(boolean yn) {
		isLocked = yn;
		updateInterface();
	}

	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	/**
	 * 
	 */
	private void actualiser()	{

		CRICursor.setWaitCursor(myView);
		rechercher();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray qualifiers = new NSMutableArray();
		NSTimestamp dateReference = new NSTimestamp();

		if (!StringCtrl.chaineVide(myView.getTfFiltreNom().getText()))
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODecharge.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreNom().getText()+"*")));

		if (myView.getPopupAnnees().getSelectedIndex() > 0) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODecharge.PERIODE_DECHARGE_KEY + " = %@", new NSArray(myView.getPopupAnnees().getSelectedItem())));			
		}
		
		return new EOAndQualifier(qualifiers);
	}

	/**
	 * 
	 */
	private void rechercher() {

		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EODecharge.findForQualifier(getEdc(), filterQualifier()));
		filter();
		CRICursor.setDefaultCursor(myView);

	}
	
	/**
	 * 
	 */
	private void exporter() {

		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "DECHARGES_"+StringCtrl.replace((String)myView.getPopupAnnees().getSelectedItem(), "/","-");
			
			saveDialog.setSelectedFile(new File(nomFichierExport + CocktailConstantes.EXTENSION_CSV));
			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = enteteExport();

				for (EODecharge myRecord : new NSArray<EODecharge>(EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(), EODecharge.SORT_ARRAY_NOM_ASC) ))
					texte += texteExportPourRecord(myRecord) + CocktailExports.SAUT_DE_LIGNE;
				
				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

				CRICursor.setDefaultCursor(myView);

				UtilitairesFichier.openFile(file.getPath());
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @return
	 */
	private String enteteExport() {

		String entete = "";

		entete += "NOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PRENOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "ANNEE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "TYPE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "HEURES";
		entete += CocktailExports.SAUT_DE_LIGNE;

		return entete;
	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EODecharge record)    {

		String texte = "";

		// NOM
		texte += CocktailExports.ajouterChamp(record.individu().nomUsuel());
		// PRENOM
		texte += CocktailExports.ajouterChamp(record.individu().prenom());
		// ANNEE
		texte += CocktailExports.ajouterChamp(record.periodeDecharge());
		// TYPE
		texte += CocktailExports.ajouterChamp(record.typeDecharge().libelleLong());
		// HEURES
		texte += CocktailExports.ajouterChampDecimal(record.nbHDecharge());


		return texte;
	}

	/**
	 * 
	 */
	private void filter() 	{

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();
		CocktailUtilities.setTextToLabel(myView.getLblNBDecharges(), eod.displayedObjects().count() + " Décharges");

		updateInterface();

	}

	/**
	 * 
	 */
	private void ajouter() {
		EOIndividu selectedIndividu = IndividuSelectCtrl.sharedInstance(getEdc()).getIndividu();
		if (selectedIndividu != null) {
			DechargesCtrl ctrlDecharges = new DechargesCtrl(null, getEdc());
			ctrlDecharges.open(selectedIndividu);
		}
	}
	/**
	 * 
	 */
	private void modifier() {
		DechargesCtrl ctrlDecharges = new DechargesCtrl(null, getEdc());
		ctrlDecharges.open(getCurrentDecharge().individu());
	}


	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette décharge ?", "Oui", "Non"))		
			return;

		try {
			getEdc().deleteObject(getCurrentDecharge());
			getEdc().saveChanges();
			actualiser();

		}
		catch(Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnAjouter().setVisible(true && isGereModule());
		myView.getBtnModifier().setEnabled(getCurrentDecharge() != null && isGereModule());
		myView.getBtnSupprimer().setVisible(getCurrentDecharge() != null && isGereModule());

		if (saisieEnabled() || isLocked())
			myView.getMyEOTable().setForeground(CocktailConstantes.BG_COLOR_GREY);
		else
			myView.getMyEOTable().setForeground(CocktailConstantes.BG_COLOR_BLACK);

	}

	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			actualiser();
		}
	}

	private class ListenerDecharge implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			modifier();
		}

		public void onSelectionChanged() {
			setCurrentDecharge((EODecharge)eod.selectedObject());
			updateInterface();
		}
	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		public void removeUpdate(DocumentEvent e) {
			filter();		
		}
	}
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			//actualiser();
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}

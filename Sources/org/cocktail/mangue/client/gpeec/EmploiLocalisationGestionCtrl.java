package org.cocktail.mangue.client.gpeec;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.gpeec.EmploiLocalisationGestionView;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiLocalisationFactory;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiLocalisationFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiLocalisation;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Controleur de la gestion des affectations par emploi
 * 
 * @author Chama LAATIK
 */
public class EmploiLocalisationGestionCtrl extends ModelePageGestion {

	private static EmploiLocalisationGestionCtrl sharedInstance;

	private EmploiLocalisationGestionView myView;
	private EODisplayGroup eod;
	private ListenerEmploiLocalisation listenerEmploiLocalisation;

	// Objet principal géré dans la fenetre
	private IEmploi currentEmploi;
	private IEmploiLocalisation currentEmploiLocalisation;
	private EOStructure currentStructure;

	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	public EmploiLocalisationGestionCtrl(EOEditingContext edc) {
		super(edc);
		eod = new EODisplayGroup();
		listenerEmploiLocalisation = new ListenerEmploiLocalisation();

		// Generation / Initialisation de l'interface
		myView = new EmploiLocalisationGestionView(null, true, eod);

		// Affectation d'une methode a chaque bouton de l'interface
		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setActionBoutonGetStructureListener();

		// Controle de saisie et completion d'une date
		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		// Desactivation d'un text field correspondant a une nomenclature - Pas de saisie libre
		CocktailUtilities.initTextField(myView.getTfStructure(), false, false);

		// Ajout d'un listener sur la table pour surveiller les clics / double clics
		myView.getMyEOTable().addListener(listenerEmploiLocalisation);

		// Mode saisie desactive par defaut
		setSaisieEnabled(false);
		updateInterface();
	}

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiAffectationGestionCtrl.sharedInstance(edc).methode
	 * 
	 * @param edc : editingContext
	 * @return une instance de la classe
	 */
	public static EmploiLocalisationGestionCtrl sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null) {
			sharedInstance = new EmploiLocalisationGestionCtrl(edc);
		}
		return sharedInstance;
	}

	/**
	 * Ouverture de l'interface de gestion
	 * 
	 * @param emploi : emploi géré
	 */
	public void open(IEmploi emploi) {
		setCurrentEmploi(emploi);
		myView.setTitle("Gestion des localisations d'emploi - Emploi " + emploi.getNoEmploi());
		setSaisieEnabled(false);
		actualiser();
		// Affichage de la fenetre en mode modal ou pas selon le parametrage
		myView.setVisible(true);
	}

	@Override
	protected void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfQuotite());
		setCurrentStructure(null);
	}

	@Override
	protected void updateDatas() {
		clearDatas();

		if (getCurrentEmploiLocalisation() != null) {
			setCurrentStructure(getCurrentEmploiLocalisation().getToStructure());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentEmploiLocalisation().getDateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentEmploiLocalisation().getDateFin());

			if (getCurrentEmploiLocalisation().getQuotite() != null) {
				CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentEmploiLocalisation().getQuotite());
			} else {
				myView.getTfQuotite().setText("100");
			}
		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {
		myView.getBtnAjouter().setEnabled(getCurrentEmploi() != null && !isSaisieEnabled());
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentEmploiLocalisation() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentEmploiLocalisation() != null);

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfQuotite(), false, isSaisieEnabled());

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getBtnGetStructure().setEnabled(isSaisieEnabled());
	}

	@Override
	protected void actualiser() {
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EmploiLocalisationFinder.sharedInstance().findForEmploi(getEdc(), getCurrentEmploi()));
		myView.getMyEOTable().updateData();

		CRICursor.setDefaultCursor(myView);
	}

	@Override
	protected void refresh() {
		if (isModeCreation()) {
			IEmploiLocalisation newSelection = getCurrentEmploiLocalisation();
			actualiser();
			CocktailUtilities.forceSelection(myView.getMyEOTable(), new NSArray(newSelection));
		}
		else
			myView.getMyEOTable().updateUI();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void traitementsAvantValidation() {
		// MAJ objet
		getCurrentEmploiLocalisation().setDateDebut(getDateDebut());
		getCurrentEmploiLocalisation().setDateFin(getDateFin());
		getCurrentEmploiLocalisation().setToStructureRelationship(getCurrentStructure());
		getCurrentEmploiLocalisation().setQuotite(CocktailUtilities.getIntegerFromField(myView.getTfQuotite()));

		//FIXME : Sortir dans validateObject
		// Controle de saisie
		if (getDateDebut() == null) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE);
		} else if (getDateFin() != null && getDateDebut().after(getDateFin())) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT);
		}

		if (DateCtrl.isBefore(getDateDebut(), getCurrentEmploi().getDateDebutEmploi())) {
			String date = DateCtrl.dateToString(getCurrentEmploi().getDateDebutEmploi());
			throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_DEBUT_AVANT_DEBUT_EMPLOI, date));
		}

		if ((getCurrentEmploi().getDateFermetureEmploi() != null) && (getDateFin() == null)) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_ELEMENTS_DATE_FIN_NON_RENSEIGNE);
		}

		if ((getCurrentEmploi().getDateFermetureEmploi() != null) && (getDateFin() != null)
				&& DateCtrl.isBefore(getCurrentEmploi().getDateFermetureEmploi(), getDateFin())) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_DATE_FIN_APRES_FERMETURE);
		}

		// Vérifier que la structure est renseignée
		if (getCurrentStructure() == null) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_STRUCTURE_NON_RENSEIGNE);
		}

		if (getCurrentEmploiLocalisation().getQuotite() == null) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_QUOTITE_NON_RENSEIGNE);
		}

		// Controle des chevauchements de périodes avec quotite > 100 ou même structure
		ArrayList<IEmploiLocalisation> listeEmploiAffectations = new ArrayList<IEmploiLocalisation>(eod.displayedObjects());
		if (isModeCreation()) {
			listeEmploiAffectations.add(getCurrentEmploiLocalisation());
		}

		Integer quotiteTotale = getCurrentEmploiLocalisation().getQuotite();
		for (IEmploiLocalisation unEmploiAffectation : listeEmploiAffectations) {
			if (unEmploiAffectation != getCurrentEmploiLocalisation()) {
				if (DateCtrl.chevauchementPeriode(unEmploiAffectation.getDateDebut(), unEmploiAffectation.getDateFin(), getDateDebut(), getDateFin())) {
					quotiteTotale = quotiteTotale + unEmploiAffectation.getQuotite();

					if (quotiteTotale.doubleValue() > 100.0) {
						throw new ValidationException(MangueMessagesErreur.ERREUR_AFFECTATION_QUOTITE_SUPERIEUR_CENT);
					}

					if (unEmploiAffectation.getToStructure() == getCurrentStructure()) {
						throw new ValidationException(MangueMessagesErreur.ERREUR_AFFECTATION_MEME_STRUCTURE);
					}
				}
			}
		}
	}

	@Override
	protected void traitementsApresValidation() {
	}

	@Override
	protected void traitementsPourAnnulation() {
		// Réactualisation des données de l'objet selectionné
		listenerEmploiLocalisation.onSelectionChanged();
	}

	@Override
	protected void traitementsPourCreation() {
		setCurrentEmploiLocalisation(EmploiLocalisationFactory.sharedInstance().creer(getEdc(), getCurrentEmploi(),
				((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel()));

		// S'il n'y a pas de localisation, on initialise avec la date de publication de l'emploi
		// sinon avec la date d'effet
		if (getCurrentEmploi().getListeEmploiLocalisations().size() == 1) {
			if (getCurrentEmploi().getDatePublicationEmploi() != null) {
				getCurrentEmploiLocalisation().setDateDebut(getCurrentEmploi().getDatePublicationEmploi());
			} else if (getCurrentEmploi().getDateEffetEmploi() != null) {
				getCurrentEmploiLocalisation().setDateDebut(getCurrentEmploi().getDateEffetEmploi());
			}
		}

		updateDatas();
	}

	@Override
	protected void traitementsPourSuppression() {
		getEdc().deleteObject((EOEmploiLocalisation) getCurrentEmploiLocalisation());
	}

	@Override
	protected void traitementsApresSuppression() {

	}

	/**
	 * Affichage de la liste des structures de l'établissement sous forme d'arbre
	 */
	private void selectListeStructure() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(getEdc());

		if (structure != null) {
			setCurrentStructure(structure);
		}
	}

	public IEmploi getCurrentEmploi() {
		return currentEmploi;
	}

	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
	}

	public IEmploiLocalisation getCurrentEmploiLocalisation() {
		return currentEmploiLocalisation;
	}

	/**
	 * Met à jour l'affectation d'emploi courante + les autres données reliées à celle-ci
	 * 
	 * @param currentEmploiLocalisation : la localisation d'emploi courante
	 */
	public void setCurrentEmploiLocalisation(IEmploiLocalisation currentEmploiLocalisation) {
		this.currentEmploiLocalisation = currentEmploiLocalisation;
		updateDatas();
	}

	public EOStructure getCurrentStructure() {
		return currentStructure;
	}

	/**
	 * Met à jour la structure
	 * 
	 * @param currentStructure : la structure d'affectation
	 */
	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
		CocktailUtilities.viderTextField(myView.getTfStructure());

		if (currentStructure != null) {
			CocktailUtilities.setTextToField(myView.getTfStructure(), currentStructure.llStructure());
		}
	}

	private NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}

	private NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}

	/**
	 * Action d'écoute sur le bouton de selecion des structures
	 */
	public void setActionBoutonGetStructureListener() {
		myView.getBtnGetStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				selectListeStructure();
			}
		});
	}

	/**
	 * Classe d'écoute sur les affectations d'emploi 
	 * 	=> Permet de mettre à jour l'interface
	 */
	private class ListenerEmploiLocalisation implements ZEOTableListener {

		public void onDbClick() {
			modifier();
		}

		public void onSelectionChanged() {
			setCurrentEmploiLocalisation((EOEmploiLocalisation) eod.selectedObject());
			updateInterface();
		}
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		
	}
}

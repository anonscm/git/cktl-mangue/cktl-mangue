package org.cocktail.mangue.client.gpeec;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.mangue.client.gui.gpeec.OccupationGestionView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.metier.finder.gpeec.OccupationFinder;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOQuotite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Controleur de la gestion des occupations par emploi
 * 
 * @author Chama LAATIK
 */
public class OccupationGestionCtrl extends ModelePageGestion {
	
	private static OccupationGestionCtrl sharedInstance;
	
	private OccupationGestionView myView;
	private EODisplayGroup eod;
	
	// Objet principal géré dans la fenetre
	private IEmploi currentEmploi;
	private EOOccupation currentOccupation;
	private EOIndividu currentIndividu;
	private ListenerOccupation listenerOccupation;

	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	public OccupationGestionCtrl(EOEditingContext edc) {
		super(edc);
		eod = new EODisplayGroup();
		listenerOccupation = new ListenerOccupation();

		// Generation / Initialisation de l'interface
		myView = new OccupationGestionView(null, true, eod);
		
		// Ajout d'un listener sur la table pour surveiller les clics / double clics
		myView.getMyEOTable().addListener(listenerOccupation);
		
		// Desactivation d'un text field correspondant a une nomenclature - Pas de saisie libre
		CocktailUtilities.initTextField(myView.getTfIndividu(), false, false);

		// Mode saisie desactive par defaut
		setSaisieEnabled(false);
		updateInterface();		
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : OccupationGestionCtrl.sharedInstance(edc).methode
	 * 
	 * @param edc : editingContext
	 * @return une instance de la classe
	 */
	public static OccupationGestionCtrl sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null) {
			sharedInstance = new OccupationGestionCtrl(edc);
		}
		return sharedInstance;
	}
	
	/**
	 * Ouverture de l'interface de gestion
	 * 
	 * @param emploi : emploi géré
	 */
	public void open(IEmploi emploi) {
		setCurrentEmploi(emploi);
		myView.setTitle("Gestion des occupations d'emploi - Emploi " + emploi.getNoEmploi());
		setSaisieEnabled(false);
		actualiser();
		// Affichage de la fenetre en mode modal ou pas selon le parametrage
		myView.setVisible(true);
	}

	@Override
	protected void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfQuotite());
		CocktailUtilities.viderTextField(myView.getTfQuotiteFinanciere());
		CocktailUtilities.viderTextField(myView.getTfIndividu());
		CocktailUtilities.viderTextField(myView.getTfStatut());
		setCurrentIndividu(null);
	}

	@Override
	protected void updateDatas() {
		clearDatas();

		if (getCurrentOccupation() != null) {
			setCurrentIndividu(getCurrentOccupation().individu());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentOccupation().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentOccupation().dateFin());
			CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentOccupation().quotite());
			CocktailUtilities.setNumberToField(myView.getTfQuotiteFinanciere(), EOQuotite.quotiteFinanciere(getEdc(), getCurrentOccupation().quotite()));
			CocktailUtilities.setTextToField(myView.getTfStatut(), getCurrentOccupation().getStatutIndividu());
		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {
	}

	@Override
	protected void actualiser() {
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(OccupationFinder.sharedInstance().findForEmploi(getEdc(), getCurrentEmploi()));
		
		myView.getMyEOTable().updateData();
		CRICursor.setDefaultCursor(myView);
	}
	
	@Override
	protected void refresh() {
	}

	@Override
	protected void traitementsAvantValidation() throws ValidationException {
	}

	@Override
	protected void traitementsApresValidation() {
	}

	@Override
	protected void traitementsPourAnnulation() {
	}

	@Override
	protected void traitementsPourCreation() {
	}

	@Override
	protected void traitementsPourSuppression() {
	}

	@Override
	protected void traitementsApresSuppression() {
	}
	
	public IEmploi getCurrentEmploi() {
		return currentEmploi;
	}

	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
	}

	public EOOccupation getCurrentOccupation() {
		return currentOccupation;
	}

	/**
	 * Met à jour l'occupation courante de l'emploi
	 * @param currentOccupation : occupation
	 */
	public void setCurrentOccupation(EOOccupation currentOccupation) {
		this.currentOccupation = currentOccupation;
		updateDatas();
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	} 

	/**
	 * Met à jour l'individu
	 * @param currentIndividu : l'individu
	 */
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		if (currentIndividu != null) {
			CocktailUtilities.setTextToField(myView.getTfIndividu(), currentIndividu.identite());
		}
	}

	/** 
	 * Classe d'ecoute sur les occupations d'emploi
	 * Permet de mettre à jour l'interface
	 */
	private class ListenerOccupation implements ZEOTableListener {

		public void onDbClick() {
			modifier();
		}

		public void onSelectionChanged() {
			setCurrentOccupation((EOOccupation) eod.selectedObject());
			updateInterface();
		}
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		
	}

}

package org.cocktail.mangue.client.gpeec;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.gpeec.EmploiSaisieView;
import org.cocktail.mangue.client.select.CategorieEmploiSelectCtrl;
import org.cocktail.mangue.client.select.NomenclatureSelectCodeLibelleCtrl;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.client.select.specialisations.CnuSelectCtrl;
import org.cocktail.mangue.client.select.specialisations.ReferensEmploisSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiCategorieFactory;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiFactory;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiLocalisationFactory;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiNatureBudgetFactory;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiSpecialisationFactory;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiCategorieFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiLocalisationFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiNatureBudgetFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiSpecialisationFinder;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiCategorie;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiLocalisation;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiNatureBudget;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitre;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitreArticle;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EONatureBudget;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgramme;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgrammeTitre;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Classe du controleur de la saisie d'un emploi
 * 
 * @author Chama LAATIK
 */
public class EmploiSaisieCtrl extends ModelePageSaisie {
	
	private static EmploiSaisieCtrl sharedInstance;
	private EmploiSaisieView myView;
	
	private IEmploi currentEmploi;
	private EOCategorieEmploi currentCategorie;
	private EOStructure currentStructure;
	private EOCnu currentCNU;
	private EODiscSecondDegre currentDiscSecondDegre;
	private EOReferensEmplois currentReferensEmploi;
	
	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	@SuppressWarnings("unchecked")
	public EmploiSaisieCtrl(EOEditingContext edc) {
		super(edc);
		myView = new EmploiSaisieView(new JFrame(), true);
		
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		
		//Controle de saisie et completion d'une date
		setDateListeners(myView.getTfDateEffetEmploi());
		setDateListeners(myView.getTfDateFermetureEmploi());
		setDateListeners(myView.getTfDatePublicationEmploi());
		
		myView.getCheckEnseignant().setEnabled(false);
		myView.getCheckArbitrage().setSelected(false);
		myView.getCheckConcours().setSelected(false);
		myView.getCheckContractuel().setSelected(false);
		myView.getCheckNational().setSelected(true);
		
		//Initialisation des popups
		myView.setListeDurabilite(EmploiFinder.sharedInstance().getListeDurabiliteEnLettre());
		myView.setListeProgramme(EOProgramme.fetchAll(getEdc()));
		myView.setListeTitre(EOProgrammeTitre.fetchAll(getEdc()));
		myView.setListeChapitre(EOChapitre.fetchAll(getEdc()));
		myView.setListeArticle(EOChapitreArticle.fetchAll(getEdc()));
		myView.setListeUAI(EOStructure.rechercherListeServicesAvecRneNonNull(getEdc()));
		
		myView.getPopupDurabilite().addActionListener(new ActionListenerDurabilite());
		
		//A la création d'un emploi => saisie possible des différents éléments d'un emploi
		myView.setListeBudgets(EONatureBudget.fetchAll(edc));
		setActionBoutonGetStructureListener();
		setActionBoutonGetCategorieEmploiListener();
		setActionBoutonGetSpecialisationListener();
		
		myView.getTfNoEmploi().addFocusListener(new ListenerTextFieldNoEmploi());
		myView.getCheckNational().addFocusListener(new ListenerTemoinNational());
		myView.getTfDateEffetEmploi().addFocusListener(new ListenerAffichageChapitreProgramme());
		
		updateInterface();
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiSaisieCtrl.sharedInstance(edc).methode
	 * 
	 * @param edc : editingContext
	 * @return une instance de la classe
	 */
	public static EmploiSaisieCtrl sharedInstance(EOEditingContext edc)	{
		if (sharedInstance == null) {
			sharedInstance = new EmploiSaisieCtrl(edc);
		}
		return sharedInstance;
	}
	
	/**
	 * Action d'ajouter
	 * 
	 * @return un emploi
	 */
	public IEmploi ajouter() {
		myView.setTitle("Emploi / AJOUT");
		setModeCreation(true);
		clearDatas();
		traitementsPourCreation();
		
		myView.setVisible(true);	// Lancement en mode MODAL

		return getCurrentEmploi();
	}
	
	/**
	 * Action de modifier
	 * @param unEmploi : l'emploi à modifier
	 * @return l'emploi
	 */
	public boolean modifier(IEmploi unEmploi) {
		myView.setTitle("Emploi / MODIFICATION");
		setModeCreation(false);
		setCurrentEmploi(unEmploi);
		
		//traitement
		myView.setVisible(true);

		return getCurrentEmploi() != null;
	}

	@Override
	protected void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfNoEmploi());
		CocktailUtilities.viderTextField(myView.getTfNoEmploiFormatte());
		CocktailUtilities.viderTextField(myView.getTfQuotite());
		CocktailUtilities.viderTextField(myView.getTfDateEffetEmploi());
		CocktailUtilities.viderTextField(myView.getTfDateFermetureEmploi());
		CocktailUtilities.viderTextField(myView.getTfDatePublicationEmploi());
		CocktailUtilities.viderTextArea(myView.getTaCommentaires());
		
		myView.getCheckArbitrage().setSelected(false);
		myView.getCheckConcours().setSelected(false);
		myView.getCheckContractuel().setSelected(false);
		myView.getCheckNational().setSelected(true);
		
		setCurrentTitre(null);
		setCurrentChapitre(null);
		setCurrentArticle(null);
		
		//Elements de l'emploi (présent à la création)
		setCurrentNatureBudget(null);
		setCurrentCategorie(null);
		setCurrentStructure(null);
		setCurrentCNU(null);
		setCurrentDiscSecondDegre(null);
		setCurrentReferensEmploi(null);
	}

	@Override
	protected void updateDatas() {
		clearDatas();

		if (getCurrentEmploi() != null) {
			//Relations
			setCurrentUAI(getCurrentEmploi().getToRne());
			setCurrentProgramme(getCurrentEmploi().getToProgramme());
			setCurrentTitre(getCurrentEmploi().getToTitre());
			setCurrentChapitre(getCurrentEmploi().getToChapitre());
			setCurrentArticle(getCurrentEmploi().getToChapitreArticle());
			
			//Attributs
			CocktailUtilities.setTextToField(myView.getTfNoEmploi(), getCurrentEmploi().getNoEmploi());
			CocktailUtilities.setTextToField(myView.getTfNoEmploiFormatte(), getCurrentEmploi().getNoEmploiFormatte());
			CocktailUtilities.setTextToArea(myView.getTaCommentaires(), getCurrentEmploi().getCommentaire());
			CocktailUtilities.setDateToField(myView.getTfDateEffetEmploi(), getCurrentEmploi().getDateEffetEmploi());
			CocktailUtilities.setDateToField(myView.getTfDatePublicationEmploi(), getCurrentEmploi().getDatePublicationEmploi());
			CocktailUtilities.setDateToField(myView.getTfDateFermetureEmploi(), getCurrentEmploi().getDateFermetureEmploi());
			
			//Cases à cocher
			myView.getCheckEnseignant().setSelected(getCurrentEmploi().isEmploiEnseignant());
			myView.getCheckArbitrage().setSelected(getCurrentEmploi().isEnArbitrage());
			myView.getCheckConcours().setSelected(getCurrentEmploi().isEnConcours());
			myView.getCheckContractuel().setSelected(getCurrentEmploi().isEmploiContractuel());
			myView.getCheckNational().setSelected(getCurrentEmploi().isEmploiNational());
			
			//Quotité par défaut à 100
			if (getCurrentEmploi().getQuotite() != null) {
				CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentEmploi().getQuotite());
			} else {
				CocktailUtilities.setTextToField(myView.getTfQuotite(), "100");
			}
			
			//Durabilité par défaut à Permanent
			if (getCurrentEmploi().getTemDurabilite() != null) {
				myView.getPopupDurabilite().setSelectedItem(getCurrentEmploi().getTemDurabiliteEnLettre());
			} else {
				myView.getPopupDurabilite().setSelectedItem(IEmploi.PERMANENT);
			}
		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {
		//Uniquement en modification
		myView.getPanelDateFermeture().setVisible(!isModeCreation());
		myView.getCheckEnseignant().setVisible(!isModeCreation());
		myView.getCheckContractuel().setEnabled(isModeCreation());
		
		if (EOGrhumParametres.isNoEmploiFormatte()) {
			if (isModeCreation()) {
				myView.getTfNoEmploiFormatte().setEnabled(false);
			} else {
				myView.getPanelNoEmploiFormatte().setVisible(!isModeCreation());
				myView.getTfNoEmploiFormatte().setEnabled(true);
			}
		} else {
			//On n'affiche pas le panel du no formatté si l'établissement ne l'autorise pas
			myView.getPanelNoEmploiFormatte().setVisible(false);
		}
		
		//Uniquement en création
		myView.getPanelElementsEmploi().setVisible(isModeCreation());
		
		if (getCurrentEmploi() != null) {
			myView.getPanelChapitreArticle().setVisible(!getCurrentEmploi().emploiPosterieurA2006());
			myView.getPanelProgrammeTitre().setVisible(getCurrentEmploi().emploiPosterieurA2006());
			
			if (IEmploi.TEMPORAIRE.equals(myView.getPopupDurabilite().getSelectedItem())) {
				myView.getPanelDateFermeture().setVisible(true);
			}
		} 
		
		//Forcer la taille de l'écran sinon écran disproportionné
		if (isModeCreation()) {
			myView.setSize(805, 610);
		} else {
			myView.setSize(805, 450);
		}
		
		//Activer la loupe des specialisations si la catégorie non null
		myView.getBtnGetSpecialisation().setEnabled(getCurrentCategorie() != null && CocktailUtilities.getDateFromField(myView.getTfDateEffetEmploi()) != null);
	}

	@Override
	protected void traitementsAvantValidation() {
		//MAJ objet
		getCurrentEmploi().setNoEmploi(CocktailUtilities.getTextFromField(myView.getTfNoEmploi()));
		getCurrentEmploi().setNoEmploiFormatte(CocktailUtilities.getTextFromField(myView.getTfNoEmploiFormatte()));
		getCurrentEmploi().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTaCommentaires()));
		getCurrentEmploi().setDateEffetEmploi(CocktailUtilities.getDateFromField(myView.getTfDateEffetEmploi()));
		getCurrentEmploi().setDatePublicationEmploi(CocktailUtilities.getDateFromField(myView.getTfDatePublicationEmploi()));
		getCurrentEmploi().setDateFermetureEmploi(CocktailUtilities.getDateFromField(myView.getTfDateFermetureEmploi()));
		getCurrentEmploi().setQuotite(CocktailUtilities.getBigDecimalFromField(myView.getTfQuotite()));
		
		getCurrentEmploi().setIsEnArbitrage(myView.getCheckArbitrage().isSelected());
		getCurrentEmploi().setIsEnConcours(myView.getCheckConcours().isSelected());
		getCurrentEmploi().setIsEmploiContractuel(myView.getCheckContractuel().isSelected());
		getCurrentEmploi().setIsEmploiNational(myView.getCheckNational().isSelected());
		getCurrentEmploi().setTemDurabiliteConvertisseur(myView.getPopupDurabilite().getSelectedItem().toString());
		
		getCurrentEmploi().setToRneRelationship(getCurrentUAI());
		
		if (getCurrentEmploi().emploiPosterieurA2006()) {
			getCurrentEmploi().setToProgrammeRelationship(getCurrentProgramme());
			getCurrentEmploi().setToTitreRelationship(getCurrentTitre());
		} else {
			getCurrentEmploi().setToChapitreRelationship(getCurrentChapitre());
			getCurrentEmploi().setToChapitreArticleRelationship(getCurrentArticle());
		}
		
		// Vérifier que le numéro d'emploi est renseigné
		if (getCurrentEmploi().getNoEmploi() == null) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_NO_NON_RENSEIGNE);
		}
		
		//Vérifier que la quotité est inférieure à 100
		if (getCurrentEmploi().getQuotite().doubleValue() > 100.0) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_QUOTITE_SUPERIEUR_CENT);
		}
		
		// Date de création de l'emploi non null
		if (getCurrentEmploi().getDateEffetEmploi() == null) {
			throw new ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "EMPLOIS"));
		}
		
		traitementDateFermeture();
		
		// Vérifier que la structure UAI est renseignée
		if (getCurrentUAI() == null) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_UAI_NON_RENSEIGNE);
		}
		
		// Vérifier que le programme est renseigné pour les emplois postérieurs à 01/01/2006
		if ((getCurrentProgramme() == null) && getCurrentEmploi().emploiPosterieurA2006()) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_PROGRAMME_NON_RENSEIGNE);
		}
		
		// Vérifier que le titre est renseigné pour les emplois postérieurs à 01/01/2006
		if ((getCurrentTitre() == null) && getCurrentEmploi().emploiPosterieurA2006()) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_TITRE_NON_RENSEIGNE);
		}
		
		// Vérifier que le chapitre est renseigné pour les emplois avant le 01/01/2006
		if ((getCurrentChapitre() == null) && !getCurrentEmploi().emploiPosterieurA2006()) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_CHAPITRE_NON_RENSEIGNE);
		}
		
		// Vérifier que l'article est renseigné pour les emplois avant le 01/01/2006
		if ((getCurrentArticle() == null) && !getCurrentEmploi().emploiPosterieurA2006()) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_ARTICLE_NON_RENSEIGNE);
		}
		
		// Vérifier que le numéro d'emploi est unique
		if (!getCurrentEmploi().verifierNoEmploiUnique()) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_EXISTE_DEJA);
		}
		
		//Vérifier que la date de fermeture est renseignée si l'emploi est temporaire
		if (IEmploi.TEM_DURABILITE_T.equals(getCurrentEmploi().getTemDurabilite()) && (getCurrentEmploi().getDateFermetureEmploi() == null)) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_TEMPORAIRE_DATE_FERMETURE);
		}
		
		if (isModeCreation()) {
			//Vérifier que le type de budget est renseigné à la création
			if (myView.getPopupNatureBudget().getSelectedIndex() == 0) {
				throw new ValidationException(MangueMessagesErreur.ERREUR_BUDGET_NON_RENSEIGNE);
			}
			
			//Vérifier que la catégorie d'emploi est renseignée à la création
			if (getCurrentCategorie() == null) {
				throw new ValidationException(MangueMessagesErreur.ERREUR_CATEGORIE_EMPLOI_NON_RENSEIGNE);
			}
		}
	}

	/**
	 * Contrôle concernant la date de fermeture
	 */
	private void traitementDateFermeture() {
		if (getCurrentEmploi().getDateFermetureEmploi() != null) {
			
			NSTimestamp dateEmploi = getCurrentEmploi().getDateEffetEmploi();
			// Date de fermeture de l'emploi doit être postérieure à la date de l'emploi
			if (getCurrentEmploi().getDatePublicationEmploi() != null) {
				dateEmploi = DateCtrl.getDateSuperieur(getCurrentEmploi().getDateEffetEmploi(), getCurrentEmploi().getDatePublicationEmploi());
			}
			if (DateCtrl.isBefore(getCurrentEmploi().getDateFermetureEmploi(), dateEmploi)) {
				String date = DateCtrl.dateToString(dateEmploi);
				throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_FERMETURE_AVANT_DATE_EMPLOI, date));
			} 
			
			NSArray<EOOccupation> listeOccupations = EOOccupation.rechercherOccupationsPourEmploi(getEdc(), getCurrentEmploi(), 
					getCurrentEmploi().getDateFermetureEmploi(), false);
			
			for (EOOccupation uneOccupation : listeOccupations) {
				if ((uneOccupation.dateFin() == null) || DateCtrl.isAfter(uneOccupation.dateFin(), getCurrentEmploi().getDateFermetureEmploi())) {
					throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_OCCUPATION_DATE_FERMETURE_KO);
				}
			}
		}
	}

	@Override
	protected void traitementsApresValidation() {
		if (isModeCreation()) {
			creationElementsEmploi();
			if (getCurrentCategorie().typePopulation() != null) {
				//MAJ du témoin enseignant de l'emploi
				getCurrentEmploi().setTemEnseignant(getCurrentCategorie().typePopulation().temEnseignant());
			}
		} else {
			if (getCurrentEmploi().getDateFermetureEmploi() != null) {
				IEmploiCategorie emploiCategorie = EmploiCategorieFinder.sharedInstance().findEmploiCategorieCourant(getEdc(), getCurrentEmploi());
				IEmploiSpecialisation emploiSpec = EmploiSpecialisationFinder.sharedInstance().findEmploiSpecialisationCourante(getEdc(), getCurrentEmploi());
				IEmploiNatureBudget emploiBudget = EmploiNatureBudgetFinder.sharedInstance().findEmploiNatureBudgetCourant(getEdc(), getCurrentEmploi());
				NSArray<IEmploiLocalisation> listeEmploiLocalisation = EmploiLocalisationFinder.sharedInstance().findListeAffectationsCourantes(getEdc(), getCurrentEmploi());
				
				//MAJ de la date de fin des catégories d’emploi, des natures de budget, des spécialisations et des localisations
				if (emploiCategorie != null) {
					emploiCategorie.setDateFin(getCurrentEmploi().getDateFermetureEmploi());
				}
				
				if (emploiSpec != null) {
					emploiSpec.setDateFin(getCurrentEmploi().getDateFermetureEmploi());
				}
				
				if (emploiBudget != null) {
					emploiBudget.setDateFin(getCurrentEmploi().getDateFermetureEmploi());
				}
				
				if (listeEmploiLocalisation.size() > 0) {
					for (IEmploiLocalisation unEmploiLocalisation : listeEmploiLocalisation) {
						unEmploiLocalisation.setDateFin(getCurrentEmploi().getDateFermetureEmploi());
					}
				}
			}
		}
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		setCurrentEmploi(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourCreation() {
		setCurrentEmploi(EmploiFactory.sharedInstance().creer(getEdc(),  
				((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel()));
	}
	
	public IEmploi getCurrentEmploi() {
		return currentEmploi;
	}
	
	/**
	 * Met à jour l'emploi courant
	 * @param currentEmploi : l'emploi courant
	 */
	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
		updateDatas();
	}

	/**
	 * @return l'UAI selectionné
	 */
	public EORne getCurrentUAI() {
		return (EORne) myView.getPopupUAI().getSelectedItem();
	}

	/**
	 * Met à jour la structure UAI
	 * @param currentUAI : la structure UAI
	 */
	public void setCurrentUAI(EORne currentUAI) {
		if (currentUAI != null) {
			myView.getPopupUAI().setSelectedItem(currentUAI);
		} else {
			myView.getPopupUAI().setSelectedItem(EOStructure.rechercherEtablissement(getEdc()).rne());
		}
	}

	/**
	 * @return le programme selectionné
	 */
	public EOProgramme getCurrentProgramme() {
		return (EOProgramme) myView.getPopupProgramme().getSelectedItem();
	}

	/**
	 * Met à jour le programme
	 * @param currentProgramme : le programme
	 */
	public void setCurrentProgramme(EOProgramme currentProgramme) {
		if (currentProgramme != null) {
			myView.getPopupProgramme().setSelectedItem(currentProgramme);
		} else {
			myView.getPopupProgramme().setSelectedItem(EOProgramme.defaultProgramme(getEdc()));
		}
	}

	/**
	 * @return le titre selectionné
	 */
	public EOProgrammeTitre getCurrentTitre() {
		if (myView.getPopupTitre().getSelectedIndex()  > 0) {
			return (EOProgrammeTitre) myView.getPopupTitre().getSelectedItem();
		}
		return null;
	}

	/**
	 * Met à jour le titre
	 * @param currentTitre : le titre
	 */
	public void setCurrentTitre(EOProgrammeTitre currentTitre) {
		myView.getPopupTitre().setSelectedIndex(0);
		
		if (currentTitre != null) {
			myView.getPopupTitre().setSelectedItem(currentTitre);
		}
	}

	/**
	 * @return le chapitre selectionné
	 */
	public EOChapitre getCurrentChapitre() {
		if (myView.getPopupChapitre().getSelectedIndex()  > 0) {
			return (EOChapitre) myView.getPopupChapitre().getSelectedItem();
		}
		return null;
	}

	/**
	 * Met à jour le chapitre
	 * @param currentChapitre : le chapitre
	 */
	public void setCurrentChapitre(EOChapitre currentChapitre) {
		myView.getPopupChapitre().setSelectedIndex(0);
		
		if (currentChapitre != null) {
			myView.getPopupChapitre().setSelectedItem(currentChapitre);
		}
	}

	/**
	 * @return l'article selectionné
	 */
	public EOChapitreArticle getCurrentArticle() {
		if (myView.getPopupArticle().getSelectedIndex()  > 0) {
			return (EOChapitreArticle) myView.getPopupArticle().getSelectedItem();
		}
		return null;
	}

	/**
	 * Met à jour l'article
	 * @param currentArticle : l'article
	 */
	public void setCurrentArticle(EOChapitreArticle currentArticle) {
		myView.getPopupArticle().setSelectedIndex(0);
		
		if (currentArticle != null) {
			myView.getPopupArticle().setSelectedItem(currentArticle);
		}
	}
	
	/**
	 * Affichage de la nomenclature des catégories d'emploi
	 */
	private void selectCategorie() {
		EOCategorieEmploi categorie =  CategorieEmploiSelectCtrl.sharedInstance(getEdc()).getCategorie();
		if (categorie != null) {
			setCurrentCategorie(categorie);
		}
	}
	
	/**
	 * Affichage de la nomenclature des catégories d'emploi
	 */
	private void selectSpecialisation() {
		if (getCurrentCategorie() != null) {
			if (getCurrentCategorie().typePopulation().estItarf() || getCurrentCategorie().typePopulation().estBiblio()) {
				setCurrentReferensEmploi((EOReferensEmplois) ReferensEmploisSelectCtrl.sharedInstance(getEdc()).getObject(
						CocktailUtilities.getDateFromField(myView.getTfDateEffetEmploi())));
			} else if (getCurrentCategorie().typePopulation().est2Degre()) {
				EODiscSecondDegre spec = (EODiscSecondDegre) NomenclatureSelectCodeLibelleCtrl.sharedInstance(
						getEdc()).getObject(NomenclatureFinder.findStatic(getEdc(), EODiscSecondDegre.ENTITY_NAME));
				setCurrentDiscSecondDegre(spec);
			} else if (getCurrentCategorie().typePopulation().estEnseignantSuperieur()) {	
				setCurrentCNU((EOCnu) CnuSelectCtrl.sharedInstance(getEdc()).getObject());
			}
		}
	}
	
	/**
	 * Affichage de la liste des structures de l'établissement sous forme d'arbre
	 */
	private void selectStructure() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(getEdc());

		if (structure != null) {
			setCurrentStructure(structure);
		}
	}
	
	public EOStructure getCurrentStructure() {
		return currentStructure;
	}

	/**
	 * Met à jour la structure
	 * 
	 * @param currentStructure : la structure d'affectation
	 */
	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
		CocktailUtilities.viderTextField(myView.getTfStructure());

		if (currentStructure != null) {
			CocktailUtilities.setTextToField(myView.getTfStructure(), currentStructure.llStructure());
		}
	}
	
	public EOCategorieEmploi getCurrentCategorie() {
		return currentCategorie;
	}

	/**
	 * Met à jour la catégorie d'emploi (nomenclature) courante 
	 * @param currentCategorie : la catégorie d'emploi (nomenclature)
	 */
	public void setCurrentCategorie(EOCategorieEmploi currentCategorie) {
		this.currentCategorie = currentCategorie;
		CocktailUtilities.viderTextField(myView.getTfCategorieEmploi());
		
		if (currentCategorie != null) {
			CocktailUtilities.setTextToField(myView.getTfCategorieEmploi(), currentCategorie.codeEtLibelle());
			//Met à jour le no d'emploi formatté
			if (currentCategorie.typePopulation() != null) {
				CocktailUtilities.setTextToField(myView.getTfNoEmploiFormatte(), majNoEmploiFormatte(currentCategorie, null, 
						currentCategorie.typePopulation().est2Degre(), currentCategorie.typePopulation().estEnseignantSuperieur()));
			} else {
				EODialogs.runInformationDialog(CocktailConstantes.ATTENTION, MangueMessagesErreur.ATTENTION_CATEGORIE_EMPLOI_SANS_CORPS);
				CocktailUtilities.setTextToField(myView.getTfNoEmploiFormatte(), majNoEmploiFormatte(currentCategorie, null, 
						 false, false));
			}
		}
		
		updateInterface();
	}
	
	/**
	 * @return le budget selectionné
	 */
	public EONatureBudget getCurrentNatureBudget() {
		return (EONatureBudget) myView.getPopupNatureBudget().getSelectedItem();
	}

	/**
	 * Met à jour le budget
	 * @param currentBudget : la nature de budget
	 */
	public void setCurrentNatureBudget(EONatureBudget currentBudget) {
		myView.getPopupNatureBudget().setSelectedIndex(0);
		
		if (currentBudget != null) {
			myView.getPopupNatureBudget().setSelectedItem(currentBudget);
		} 
	}
	
	/**
	 * Action d'écoute sur le bouton de selecion des structures
	 */
	public void setActionBoutonGetStructureListener() {
		myView.getBtnGetStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				selectStructure();
			}
		});
	}
	
	/**
	 * Action d'écoute sur le bouton de selecion des structures
	 */
	public void setActionBoutonGetCategorieEmploiListener() {
		myView.getBtnGetCategorieEmploi().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { selectCategorie(); } }
		);
	}
	
	/**
	 * Action d'écoute sur le bouton de selecion des structures
	 */
	public void setActionBoutonGetSpecialisationListener() {
		myView.getBtnGetSpecialisation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { selectSpecialisation(); } }
		);
	}

	public EOCnu getCurrentCNU() {
		return currentCNU;
	}

	/**
	 * Met à jour la section CNU
	 * @param currentCNU : la section CNU (nomenclature)
	 */
	public void setCurrentCNU(EOCnu currentCNU) {
		this.currentCNU = currentCNU;
		CocktailUtilities.viderTextField(myView.getTfSpecialisation());
		
		if (currentCNU != null) {
			CocktailUtilities.setTextToField(myView.getTfSpecialisation(), currentCNU.codeEtLibelle());
			//Met à jour le no d'emploi formatté
			CocktailUtilities.setTextToField(myView.getTfNoEmploiFormatte(), majNoEmploiFormatte(getCurrentCategorie(), 
					currentCNU.code(), false, true));
		}
	}

	public EODiscSecondDegre getCurrentDiscSecondDegre() {
		return currentDiscSecondDegre;
	}

	/**
	 * Met à jour la discipline du second degré 
	 * @param currentDiscSecondDegre : la discipline du second degré (nomenclature)
	 */
	public void setCurrentDiscSecondDegre(EODiscSecondDegre currentDiscSecondDegre) {
		this.currentDiscSecondDegre = currentDiscSecondDegre;
		CocktailUtilities.viderTextField(myView.getTfSpecialisation());
		
		if (currentDiscSecondDegre != null) {
			CocktailUtilities.setTextToField(myView.getTfSpecialisation(), currentDiscSecondDegre.codeEtLibelle());
			//Met à jour le no d'emploi formatté
			CocktailUtilities.setTextToField(myView.getTfNoEmploiFormatte(), majNoEmploiFormatte(getCurrentCategorie(), 
					currentDiscSecondDegre.code(), true, false));
		}
	}

	public EOReferensEmplois getCurrentReferensEmploi() {
		return currentReferensEmploi;
	}

	/**
	 * Met à jour la référence emploi
	 * @param currentReferensEmploi : la nomenclature Referens Emploi
	 */
	public void setCurrentReferensEmploi(EOReferensEmplois currentReferensEmploi) {
		this.currentReferensEmploi = currentReferensEmploi;
		CocktailUtilities.viderTextField(myView.getTfSpecialisation());
		
		if (currentReferensEmploi != null) {
			CocktailUtilities.setTextToField(myView.getTfSpecialisation(), currentReferensEmploi.codeEtLibelle());
		}
	}
	
	private void creationElementsEmploi() {
		NSTimestamp debut = getCurrentEmploi().getDateDebutEmploi();
		
		//Création des sous élements
		if (myView.getPopupNatureBudget().getSelectedIndex() > 0) {
			EmploiNatureBudgetFactory.sharedInstance().creer(getEdc(), getCurrentEmploi(), debut, getCurrentEmploi().getDateFermetureEmploi(),
					getCurrentNatureBudget(), ((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel()); 
		}
		
		if (getCurrentStructure() != null) {
			EmploiLocalisationFactory.sharedInstance().creer(getEdc(), getCurrentEmploi(), debut, getCurrentEmploi().getDateFermetureEmploi(),
					getCurrentStructure(), ((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel()); 
		}
		
		if (getCurrentCategorie() != null) {
			EmploiCategorieFactory.sharedInstance().creer(getEdc(), getCurrentEmploi(), debut, getCurrentEmploi().getDateFermetureEmploi(),
					getCurrentCategorie(), ((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel());
			
			if ((getCurrentCNU() != null) || (getCurrentDiscSecondDegre() != null) || (getCurrentReferensEmploi() != null)) {
				EmploiSpecialisationFactory.sharedInstance().creer(getEdc(), getCurrentEmploi(), debut, getCurrentEmploi().getDateFermetureEmploi(), null, getCurrentCNU(),
						getCurrentDiscSecondDegre(), getCurrentReferensEmploi(), ((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel());
			}
		}
	}
	
	/**
	 * On met à jour que si l'établissement autorise l'utilisation du numéro d'emploi formatté
	 */
	private String majNoEmploiFormatte(EOCategorieEmploi categorieEmploi, String codeSpec, boolean isDiscSecondDegre, boolean isCnu) {
		if (EOGrhumParametres.isNoEmploiFormatte() && isModeCreation() && myView.getCheckNational().isSelected()) {
			LogManager.logDetail("noEmploi : " + CocktailUtilities.getTextFromField(myView.getTfNoEmploi()));

			return getCurrentEmploi().formatterNoEmploi(CocktailUtilities.getTextFromField(myView.getTfNoEmploi()), 
					categorieEmploi, codeSpec, isDiscSecondDegre, isCnu);
		}
		return null;
	}

	@Override
	protected void traitementsChangementDate() {
		updateInterface();
	}
	
	/**
	 * On affiche la date de fermeture si la durabilité est temporaire
	 */
	public void majAffichageDateFermeture() {
		if (IEmploi.TEMPORAIRE.equals((String) myView.getPopupDurabilite().getSelectedItem())) {
			myView.getPanelDateFermeture().setVisible(true);
		}
	}
	
	/**
	 * On met à jour le no d'emploi formatté si la catégorie d'emploi change
	 */
	private void majNoEmploiFormatte() {
		if (isModeCreation() && getCurrentCategorie() != null) {
			//Met à jour le no d'emploi formatté
			CocktailUtilities.setTextToField(myView.getTfNoEmploiFormatte(), majNoEmploiFormatte(currentCategorie, null, getCurrentCategorie().typePopulation().est2Degre(), 
					getCurrentCategorie().typePopulation().estEnseignantSuperieur()));
			
		}
	}
	
	/**
	 * On vide le no d'emploi formatté si l'emploi est de type local (pas national)
	 */
	private void viderNoEmploiFormatte() {
		if (EOGrhumParametres.isNoEmploiFormatte()) {
			if (!myView.getCheckNational().isSelected()) {
				CocktailUtilities.viderTextField(myView.getTfNoEmploiFormatte());
			} else {
				majNoEmploiFormatte();
			}
		}
	}
	
	/**
	 * On met à jour l'interface pour afficher Programme ou Chapitre en fonction de la date d'effet
	 */
	private void majAffichageChapitreProgramme() {
		if (getCurrentEmploi() != null) {
			getCurrentEmploi().setDateEffetEmploi(CocktailUtilities.getDateFromField(myView.getTfDateEffetEmploi()));
			myView.getPanelChapitreArticle().setVisible(!getCurrentEmploi().emploiPosterieurA2006());
			myView.getPanelProgrammeTitre().setVisible(getCurrentEmploi().emploiPosterieurA2006());
		}
	}
	
	/**
	 * Classe d'ecoute sur le popup "Durabilité".
	 * Permet d'actualiser l'affichage de la date de fermeture
	 * 
	 * @author Chama LAATIK
	 */
	private class ActionListenerDurabilite implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			majAffichageDateFermeture();
		}
	}
	
	/** 
	 * Classe d'ecoute sur le no d'emploi.
	 * Permet de mettre à jour le no d'emploi formatté
	 */
	private class ListenerTextFieldNoEmploi implements FocusListener	{
		public void focusGained(FocusEvent e) {
		}

		public void focusLost(FocusEvent e) {
			majNoEmploiFormatte();
		}
	}
	
	/** 
	 * Classe d'ecoute sur le témoin national.
	 * Permet de vider le no emploi formatté ou de le mettre à jour
	 */
	private class ListenerTemoinNational implements FocusListener {
		public void focusGained(FocusEvent e) {
		}

		public void focusLost(FocusEvent e) {
			viderNoEmploiFormatte();
		}
	}
	
	/** 
	 * Classe d'ecoute sur la date d'effet.
	 * Permet d'actualiser l'affichage de l'interface : Programme ou Chapitre
	 */
	private class ListenerAffichageChapitreProgramme implements FocusListener {
		public void focusGained(FocusEvent e) {
		}

		public void focusLost(FocusEvent e) {
			majAffichageChapitreProgramme();
		}
	}
}

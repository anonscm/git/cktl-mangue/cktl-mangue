package org.cocktail.mangue.client.gpeec;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JPanel;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.gpeec.EmploiSpecialisationGestionView;
import org.cocktail.mangue.client.specialisations.SpecialisationCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiSpecialisationFactory;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiSpecialisationFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Controleur de la gestion des spécialisations par emploi
 * 
 * @author Chama LAATIK
 */
public class EmploiSpecialisationGestionCtrl extends ModelePageGestion {

	private static EmploiSpecialisationGestionCtrl sharedInstance;

	private EmploiSpecialisationGestionView myView;
	private EODisplayGroup eod;
	private ListenerEmploiSpecialisation listenerEmploiSpecialisation;

	// Objet principal géré dans la fenetre
	private IEmploi currentEmploi;
	private IEmploiSpecialisation currentEmploiSpecialisation;
	private SpecialisationCtrl myCtrlSpecialisation;


	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	public EmploiSpecialisationGestionCtrl(EOEditingContext edc) {
		super(edc);
		eod = new EODisplayGroup();
		listenerEmploiSpecialisation = new ListenerEmploiSpecialisation();
		myCtrlSpecialisation = new SpecialisationCtrl(edc);

		// Generation / Initialisation de l'interface
		myView = new EmploiSpecialisationGestionView(null, true, eod);

		// Affectation d'une methode a chaque bouton de l'interface
		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		//Gestion de l'affichage de la spécialisation
		myView.getSwapView().add("VIDE", new JPanel(new BorderLayout()));
		myView.getSwapView().add("SPEC", myCtrlSpecialisation.getView());
		((CardLayout) myView.getSwapView().getLayout()).show(myView.getSwapView(), "SPEC");

		//Controle de saisie et completion d'une date
		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		// Ajout d'un listener sur la table pour surveiller les clics / double clics
		myView.getMyEOTable().addListener(listenerEmploiSpecialisation);

		// Mode saisie desactive par defaut
		//setSaisieEnabled(false);
		updateInterface();
	}

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiSpecialisationGestionCtrl.sharedInstance(edc).methode
	 * 
	 * @param edc : editingContext
	 * @return une instance de la classe
	 */
	public static EmploiSpecialisationGestionCtrl sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null) {
			sharedInstance = new EmploiSpecialisationGestionCtrl(edc);
		}
		return sharedInstance;
	}

	/**
	 * Ouverture de l'interface de gestion
	 * 
	 * @param emploi : emploi géré
	 */
	public void open(IEmploi emploi) {
		setCurrentEmploi(emploi);
		myView.setTitle("Gestion des spécialisations d'emploi - Emploi " + emploi.getNoEmploi());
		setSaisieEnabled(false);
		actualiser();
		// Affichage de la fenetre en mode modal ou pas selon le parametrage
		myView.setVisible(true);
	}

	@Override
	protected void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		myCtrlSpecialisation.clean();
	}

	@Override
	protected void updateDatas() {
		clearDatas();

		if (getCurrentEmploiSpecialisation() != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentEmploiSpecialisation().getDateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentEmploiSpecialisation().getDateFin());

			// Specialisation - Affichage de la specialisation
			myCtrlSpecialisation.showSpecForEmploiSpecialisation(getCurrentEmploiSpecialisation());
			myCtrlSpecialisation.setDateReference(getDateDebut());
		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {
		myView.getBtnAjouter().setEnabled(getCurrentEmploi() != null && !isSaisieEnabled());
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentEmploiSpecialisation() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentEmploiSpecialisation() != null);

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());
	}

	@Override
	protected void actualiser() {
		CRICursor.setWaitCursor(myView);

		eod.setObjectArray(EmploiSpecialisationFinder.sharedInstance().findForEmploi(getEdc(), getCurrentEmploi()));
		myView.getMyEOTable().updateData();

		CRICursor.setDefaultCursor(myView);
	}

	@Override
	protected void refresh() {
		
		// TODO Auto-generated method stub
		if (isModeCreation()) {
			IEmploiSpecialisation newSpec = getCurrentEmploiSpecialisation();
			actualiser();
			CocktailUtilities.forceSelection(myView.getMyEOTable(), new NSArray(newSpec));
		}
		else {
			myView.getMyEOTable().updateUI();
		}
	}

	@Override
	protected void traitementsAvantValidation() {
		//MAJ objet
		getCurrentEmploiSpecialisation().setDateDebut(getDateDebut());
		getCurrentEmploiSpecialisation().setDateFin(getDateFin());

		// Controle de saisie
		if (getDateDebut() == null) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE);
		} else if (getDateFin() != null && getDateDebut().after(getDateFin())) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT);
		}

		if (DateCtrl.isBefore(getDateDebut(), getCurrentEmploi().getDateDebutEmploi())) {
			String date = DateCtrl.dateToString(getCurrentEmploi().getDateDebutEmploi());
			throw new ValidationException(String.format(
					MangueMessagesErreur.ERREUR_EMPLOI_DATE_DEBUT_AVANT_DEBUT_EMPLOI, date));
		}

		if ((getCurrentEmploi().getDateFermetureEmploi() != null) && (getDateFin() == null)) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_ELEMENTS_DATE_FIN_NON_RENSEIGNE);
		}

		if ((getCurrentEmploi().getDateFermetureEmploi() != null) && (getDateFin() != null)
				&& DateCtrl.isBefore(getCurrentEmploi().getDateFermetureEmploi(), getDateFin())) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_DATE_FIN_APRES_FERMETURE);
		}

		traitementsPourHistorisationDate();	
	}

	@Override
	protected void traitementsApresValidation() {
	}

	@Override
	protected void traitementsPourAnnulation() {
		// Reactualisation des donnees de l'objet selectionné
		listenerEmploiSpecialisation.onSelectionChanged();
	}

	@Override
	protected void traitementsPourCreation() {
		setCurrentEmploiSpecialisation(EmploiSpecialisationFactory.sharedInstance().creer(getEdc(), getCurrentEmploi(), 
				((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel()));

		// S'il n'y a pas de spécialisation, on initialise avec la date de publication de l'emploi
		// sinon avec la date d'effet
		if (getCurrentEmploi().getListeEmploiSpecialisations().size() == 1) {
			if (getCurrentEmploi().getDatePublicationEmploi() != null) {
				getCurrentEmploiSpecialisation().setDateDebut(getCurrentEmploi().getDatePublicationEmploi());
			} else if (getCurrentEmploi().getDateEffetEmploi() != null) {
				getCurrentEmploiSpecialisation().setDateDebut(getCurrentEmploi().getDateEffetEmploi());
			}
		} else {
			//S'il existe des spécialisations, on initialise la date de début avec la date de fin + 1j du dernier élément
			getCurrentEmploiSpecialisation().setDateDebut(DateCtrl.jourSuivant(EmploiSpecialisationFinder.sharedInstance()
					.findForEmploi(new EOEditingContext(), getCurrentEmploi()).get(0).getDateFin()));
		}

		updateDatas();
	}

	/**
	 * Permet de ne pas avoir de trous dans l'historisation des spécialisations d'emploi
	 *  => Met à jour la date de début et de fin des éléments précédents et suivants
	 * de l'élement modifié/créé 
	 */
	protected void traitementsPourHistorisationDate() {
		IEmploiSpecialisation elementPrecedent = null;
		IEmploiSpecialisation elementSuivant = null;
		NSArray<IEmploiSpecialisation> liste = null;
		EOEditingContext nouveauEdc = new EOEditingContext();

		if (isModeCreation()) {
			//A la création, on initialise avec un nouveau EditingContext, sinon erreur EOCheapMutableArray
			liste = EmploiSpecialisationFinder.sharedInstance().findForEmploi(nouveauEdc, getCurrentEmploi());
		} else {
			liste = EmploiSpecialisationFinder.sharedInstance().findForEmploi(getEdc(), getCurrentEmploi());
		}

		if (liste.indexOf(getCurrentEmploiSpecialisation()) >= 0) {
			int index = liste.indexOf(getCurrentEmploiSpecialisation());
			LogManager.logDetail("date de début : " + DateCtrl.dateToString(getCurrentEmploiSpecialisation().getDateDebut()));
			LogManager.logDetail("date de fin : " + DateCtrl.dateToString(getCurrentEmploiSpecialisation().getDateFin()));

			//Si l'élément est le dernier de la liste => on n'a pas d'élément précédent
			if (getCurrentEmploiSpecialisation() != liste.get(liste.size() - 1)) {	
				elementPrecedent = liste.get(index + 1);
				LogManager.logDetail("la spécialisation précédente : " + elementPrecedent.libelleSpecialisationAffiche());
				LogManager.logDetail("  > date de début : " + DateCtrl.dateToString(elementPrecedent.getDateDebut()));
				LogManager.logDetail("  > date de fin : " + DateCtrl.dateToString(elementPrecedent.getDateFin()));
			}

			//Si l'élément est le premier de la liste => on n'a pas d'élément suivant
			if (getCurrentEmploiSpecialisation() != liste.get(0)) {	
				elementSuivant = liste.get(index - 1);
				LogManager.logDetail("la spécialisation suivante : " + elementSuivant.libelleSpecialisationAffiche());
				LogManager.logDetail("  > date de début : " + DateCtrl.dateToString(elementSuivant.getDateDebut()));
				LogManager.logDetail("  > date de fin : " + DateCtrl.dateToString(elementSuivant.getDateFin()));
			}

			//elementPrecedent.dateDebut < element.dateDebut (=> permet de ne pas avoir de chevauchement sur d'autres éléments)
			if (elementPrecedent != null) {
				if (DateCtrl.isBefore(elementPrecedent.getDateDebut(), getCurrentEmploiSpecialisation().getDateDebut())) {
					elementPrecedent.setDateFin(DateCtrl.jourPrecedent(getCurrentEmploiSpecialisation().getDateDebut()));
				} else {
					throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_DEBUT_HISTO_KO, 
							DateCtrl.dateToString(elementPrecedent.getDateDebut())));
				}
			}

			//element.dateFin < elementSuivant.dateFin (=> permet de ne pas avoir de chevauchement sur d'autres éléments)
			if (elementSuivant != null) {
				traitementDateElementSuivant(elementSuivant);
			}
		} else {
			//En création, si la date de fin de l'élement précédent est null, on la met à jour avec la date de début
			//de la catégorie d'emploi - 1j
			if (liste.size() > 0 && liste.get(0) != null && liste.get(0).getDateFin() == null) {
				liste.get(0).setDateFin(DateCtrl.jourPrecedent(getCurrentEmploiSpecialisation().getDateDebut()));
				nouveauEdc.saveChanges();
			}
		}
	}
	
	/**
	 * Permet de gérer les dates de l'élement qui suit la spécialisation modifiée
	 * element.dateFin < elementSuivant.dateFin (=> permet de ne pas avoir de chevauchement sur d'autres éléments)
	 * @param elementSuivant : élement suivant
	 */
	private void traitementDateElementSuivant(IEmploiSpecialisation elementSuivant) {
		if (elementSuivant.getDateFin() == null) {
			elementSuivant.setDateDebut(DateCtrl.jourSuivant(getCurrentEmploiSpecialisation().getDateFin()));
		} else {
			if (DateCtrl.isBefore(getCurrentEmploiSpecialisation().getDateFin(), elementSuivant.getDateFin())) {
				elementSuivant.setDateDebut(DateCtrl.jourSuivant(getCurrentEmploiSpecialisation().getDateFin()));
			} else {
				throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_FIN_HISTO_KO, 
						DateCtrl.dateToString(elementSuivant.getDateFin())));
			}
		}
	}

	@Override
	protected void traitementsPourSuppression() {
		getEdc().deleteObject((EOEmploiSpecialisation) getCurrentEmploiSpecialisation());
	}

	@Override
	protected void traitementsApresSuppression() {
	}

	private NSTimestamp getDateDebut()	{
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}

	private NSTimestamp getDateFin()	{
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}

	public IEmploi getCurrentEmploi() {
		return currentEmploi;
	}

	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
	}

	public IEmploiSpecialisation getCurrentEmploiSpecialisation() {
		return currentEmploiSpecialisation;
	}

	/**
	 * Met à jour la spécialisation d'emploi courante
	 * @param currentEmploiSpecialisation : la spécialisation courante de l'emploi 
	 */
	public void setCurrentEmploiSpecialisation(IEmploiSpecialisation currentEmploiSpecialisation) {
		this.currentEmploiSpecialisation = currentEmploiSpecialisation;
		updateDatas();
	}

	@Override
	protected void setSaisieEnabled(boolean yn) {
		super.setSaisieEnabled(yn);
		myCtrlSpecialisation.setSaisieEnabled(yn);
		updateInterface();
	}

	/** 
	 * Classe d'ecoute sur les spécialisations d'emploi
	 * 	=> Permet de mettre à jour l'interface
	 */
	private class ListenerEmploiSpecialisation implements ZEOTableListener {

		public void onDbClick() {
		}

		public void onSelectionChanged() {
			setCurrentEmploiSpecialisation((IEmploiSpecialisation) eod.selectedObject());
		}
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		myCtrlSpecialisation.setDateReference(getDateDebut());
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		
	}

}

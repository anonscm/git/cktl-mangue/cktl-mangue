package org.cocktail.mangue.client.gpeec;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.budget.BudgetCtrl;
import org.cocktail.mangue.client.gui.gpeec.EmploisView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.CategorieEmploiSelectCtrl;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageConsultation;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiFactory;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiCategorieFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiLocalisationFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiNatureBudgetFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiSpecialisationFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.OccupationFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiCategorie;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiLocalisation;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiNatureBudget;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EONatureBudget;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe du controleur des emplois
 * 
 * @author Chama LAATIK
 */
public class EmploisCtrl extends ModelePageConsultation {

	private static EmploisCtrl sharedInstance;

	private EODisplayGroup 	eod;
	private EmploisView myView;

	private ListenerEmploi 	listenerEmploi = new ListenerEmploi();

	private IEmploi currentEmploi;
	private IEmploi nouvelEmploi;
	private EOCategorieEmploi currentCategorie;
	private EOStructure currentStructure;
	private IEmploiSpecialisation currentSpecialisation;
	private List<IEmploiLocalisation> currentListeAffectations;
	private IEmploiCategorie currentEmploiCategorie;
	private IEmploiNatureBudget currentEmploiNatureBudget;
	private List<EOOccupation> currentListeOccupations;

	private BudgetCtrl myCtrlBudget;

	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	@SuppressWarnings("unchecked")
	public EmploisCtrl(EOEditingContext edc) {
		super(edc);

		// Initialisation du displayGroup et de l'interface
		eod = new EODisplayGroup();
		//On passe à la vue un boolean pour l'utilisation ou non du numéro d'emploi formatté à partir du numéro d'emploi national
		myView = new EmploisView(null, true, eod, EOGrhumParametres.isNoEmploiFormatte());

		myCtrlBudget = new BudgetCtrl(getEdc());
		myCtrlBudget.setTypeGestion(BudgetCtrl.TYPE_GESTION_EMPLOIS);

		//Gestion des différents boutons
		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());

		myView.getBtnRechercher().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { rechercher(); } }
				);
		myView.getBtnCategories().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherCategories(); } }
				);
		myView.getBtnAffectations().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherAffectations(); } }
				);
		myView.getBtnOccupations().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherOccupations(); } }
				);
		myView.getBtnSpecialisations().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherSpecialisations(); } }
				);
		myView.getBtnNatureBudget().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherNatureBudgets(); } }
				);
		myView.getBtnBudget().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { afficherBudget(); } }
				);
		myView.getBtnNettoyer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { nettoyerFiltres(); } }
				);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { exporterListeEmplois(); } }
				);
		myView.getBtnEditionFicheEmploi().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { imprimerFicheEmploi(); } }
				);
		myView.getBtnEditionFicheEmploiHistorique().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { imprimerFicheEmploiHistorique(); } }
				);

		//Filtre de recherche
		myView.getBtnGetCategorie().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { selectListeCategories(); } }
				);
		myView.getBtnDelCategorie().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { viderFiltreCategorie(); } }
				);
		myView.getBtnGetAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { selectListeStructures(); } }
				);
		myView.getBtnDelAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { viderFiltreStructure(); } }
				);

		myView.getMyEOTable().addListener(listenerEmploi);

		// Ajout d'un listener sur les champs dates pour les completions automatiques
		setDateListeners(myView.getTfFiltrePeriodeDebut());
		setDateListeners(myView.getTfFiltrePeriodeFin());

		myView.setBudgets(EONatureBudget.fetchAll(edc));
		myView.setListeFiltreTemoinEnseignant();
		myView.setListeUAI(EOStructure.rechercherListeServicesAvecRneNonNull(edc));
		myView.setListeEtatEmploi(EmploiFinder.sharedInstance().getListeEtats());
		myView.setListeTypePopulation(EOTypePopulation.rechercherListeTypePopulationVisible(getEdc()));
		myView.setListeCategFonctionPublique(EOCategorie.rechercherListeCategorieAvecCorrespondance(getEdc()));

		//Champs non éditables ou vidés
		myView.getTfFiltreAffectation().setEditable(false);
		myView.getTfFiltreCategorie().setEditable(false);
		CocktailUtilities.initTextField(myView.getTfAffectation(), true, false);
		CocktailUtilities.initTextField(myView.getTfBudget(), true, false);
		CocktailUtilities.initTextField(myView.getTfCategorie(), true, false);
		CocktailUtilities.initTextField(myView.getTfOccupation(), true, false);
		CocktailUtilities.initTextField(myView.getTfSpecialisation(), true, false);

		myView.getTfFiltreNoEmploi().addActionListener(new MyActionFiltreListener());
	}

	/**
	 * Permet d'acceder a cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploisCtrl.sharedInstance(edc).methode
	 * 
	 * @param edc : editingContext
	 * @return une instance de la classe
	 */
	public static EmploisCtrl sharedInstance(EOEditingContext edc)	{
		if (sharedInstance == null) {
			sharedInstance = new EmploisCtrl(edc);
		}
		return sharedInstance;
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecté
	 * @param utilisateur : utilsateur
	 */
	public void setDroitsGestion(EOAgentPersonnel utilisateur) {
		setPeutGererModule(utilisateur.peutGererEmplois());

		// Activation / Desactivation d'actions selon les droits
	}

	/**
	 * Ouverture de la fenetre
	 */
	public void open()	{
		CRICursor.setWaitCursor(myView);
		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(true);  //Grise l'écran parent
		// Actualisation des données
		actualiser();

		CRICursor.setDefaultCursor(myView);
		// Affichage de l'ecran
		myView.setVisible(true);
		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(false); 
	}

	/**
	 * Actualisation des donnes
	 */
	protected void actualiser()	{
		CRICursor.setWaitCursor(myView);
		rechercher();
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * Qualifier de filtre automatique
	 * @return
	 */
	protected EOQualifier filterQualifier() {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		//Recherche sur le no emploi local
		if (!StringCtrl.chaineVide(myView.getTfFiltreNoEmploi().getText())) {
			qualifiers.addObject(EmploiFinder.sharedInstance().qualifierEmploiParNoEmploi(myView.getTfFiltreNoEmploi().getText(),
					EOGrhumParametres.isNoEmploiFormatte()));
		} 

		//Recherche sur le témoin enseignant
		if (myView.getPopupEnseignant().getSelectedIndex() > 0) {
			qualifiers.addObject(EmploiFinder.sharedInstance().qualifierEmploiParTemoinEnseignant((String)
					myView.getPopupEnseignant().getSelectedItem()));
		}

		//Recherche sur le type de budget
		if (myView.getPopupBudget().getSelectedIndex() > 0) {
			qualifiers.addObject(EmploiFinder.sharedInstance().qualifierEmploiParNatureBudget((EONatureBudget) 
					myView.getPopupBudget().getSelectedItem()));
		}

		//Période : date de début et de fin
		if (!StringCtrl.chaineVide(myView.getTfFiltrePeriodeDebut().getText())) {
			qualifiers.addObject(EmploiFinder.sharedInstance().qualifierEmploiParPeriode(getFiltreDebutPeriode(), getFiltreFinPeriode()));
		} 

		//Implantation
		if (myView.getPopupImplantation().getSelectedIndex() > 0) {
			qualifiers.addObject(EmploiFinder.sharedInstance().qualifierEmploiParImplantationUAI((EORne) 
					myView.getPopupImplantation().getSelectedItem()));
		}

		//Categorie emploi
		if (!StringCtrl.chaineVide(myView.getTfFiltreCategorie().getText())) {
			qualifiers.addObject(EmploiFinder.sharedInstance().qualifierEmploiParCategorieEmploi(getCurrentCategorie()));
		}

		//Affectation
		if (!StringCtrl.chaineVide(myView.getTfFiltreAffectation().getText())) {
			qualifiers.addObject(EmploiFinder.sharedInstance().qualifierEmploiParLocalisation(getCurrentStructure()));
		}

		LogManager.logDetail("  Filtre de recherche : " + new EOAndQualifier(qualifiers));
		return new EOAndQualifier(qualifiers);
	}


	/** 
	 * Lancement de la recherche - Mise a jour du displaygroup par un Finder specifique
	 */
	private void rechercher() {
		CRICursor.setWaitCursor(myView);
		//Gérer la liste déroulante des états en premier 
		NSArray<IEmploi> listeEmplois = null;

		if (getFiltreDebutPeriode() == null) {
			listeEmplois = EmploiFinder.sharedInstance().rechercherListeEmploisAvecEtat(getEdc(), (String) myView.getPopupEtat().getSelectedItem(), DateCtrl.today(), null);
		} else {
			listeEmplois = EmploiFinder.sharedInstance().rechercherListeEmploisAvecEtat(getEdc(), (String) myView.getPopupEtat().getSelectedItem(), 
					getFiltreDebutPeriode(), getFiltreFinPeriode());
		}

		eod.setObjectArray(listeEmplois);
		eod.setQualifier(filterQualifier());	//Puis on applique les filtres

		eod.updateDisplayedObjects();		//Maj des données après qualifier

		//Appliquer les filtres de la catégorie de la fonction publique et le type de population
		filtrerParCategFonctionEtTypePop();

		myView.getMyEOTable().updateData();

		CocktailUtilities.setTextToLabel(myView.getLblNbEmplois(), eod.displayedObjects().count() + " emplois");		//Maj du nombre d'emplois
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * On applique les filtres type de population et catégorie de la fonction publique après pour optimiser le temps de réponse
	 */
	@SuppressWarnings("unchecked")
	private void filtrerParCategFonctionEtTypePop() {
		//Type de population
		if (myView.getPopupTypePopulation().getSelectedIndex() > 0) {
			eod.setObjectArray(EmploiFinder.sharedInstance().filtrerParTypePopulation(eod.displayedObjects(), 
					(EOTypePopulation) myView.getPopupTypePopulation().getSelectedItem()));
		}

		//Catégorie de la fonction publique
		if ((myView.getPopupTypePopulation().getSelectedIndex() > 0)
				&& (myView.getPopupCategFonctionPublique().getSelectedIndex() > 0)) {
			eod.setObjectArray(EmploiFinder.sharedInstance().filtrerParCategorieFonctionPublique(eod.displayedObjects(), 
					(EOCategorie) myView.getPopupCategFonctionPublique().getSelectedItem()));
		}

		//Catégorie de la fonction publique pour type de population Non enseignant
		if ((myView.getPopupTypePopulation().getSelectedIndex() == 0) 
				&& (myView.getPopupCategFonctionPublique().getSelectedIndex() > 0)) {
			eod.setObjectArray(EmploiFinder.sharedInstance().filtrerParCategFonctionPubliquePourNonEnseignant(eod.displayedObjects(), 
					(EOCategorie) myView.getPopupCategFonctionPublique().getSelectedItem()));	
		}
	}

	protected void updateDatas() {
		//MAJ des données reliées à un emploi selectionné
		setCurrentSpecialisation(EmploiSpecialisationFinder.sharedInstance().findEmploiSpecialisationCourante(getEdc(), getCurrentEmploi()));
		setCurrentListeAffectations(EmploiLocalisationFinder.sharedInstance().findListeAffectationsCourantes(getEdc(), getCurrentEmploi()));
		setCurrentEmploiCategorie(EmploiCategorieFinder.sharedInstance().findEmploiCategorieCourant(getEdc(), getCurrentEmploi()));
		setCurrentListeOccupations(OccupationFinder.sharedInstance().findListeOccupations(getEdc(), getCurrentEmploi()));
		setCurrentEmploiNatureBudget(EmploiNatureBudgetFinder.sharedInstance().findEmploiNatureBudgetCourant(getEdc(), getCurrentEmploi()));

		updateInterface();
	}

	/**
	 * Gestion de l'accessibilite de tous les objets graphiques de l'ecran
	 */
	protected void updateInterface() {
		myView.getBtnAjouter().setEnabled(true);
		myView.getBtnModifier().setEnabled(getCurrentEmploi() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentEmploi() != null);

		//On vide les filtres que si la localisation ou la catégorie est selectionnée 
		myView.getBtnDelAffectation().setEnabled(getCurrentStructure() != null);
		myView.getBtnDelCategorie().setEnabled(getCurrentCategorie() != null);

		//On ne peut accéder au détail d'un emploi que s'il est selectionné
		myView.getBtnAffectations().setEnabled(getCurrentEmploi() != null);
		myView.getBtnNatureBudget().setEnabled(getCurrentEmploi() != null);
		myView.getBtnCategories().setEnabled(getCurrentEmploi() != null);
		myView.getBtnOccupations().setEnabled(getCurrentEmploi() != null);
		myView.getBtnSpecialisations().setEnabled(getCurrentEmploi() != null && getCurrentEmploiCategorie() != null);

		myView.getBtnEditionFicheEmploi().setEnabled(getCurrentEmploi() != null);
		myView.getBtnEditionFicheEmploiHistorique().setEnabled(getCurrentEmploi() != null);

		myView.toFront();
	}

	@Override
	protected void traitementsPourCreation() {
		setNouvelEmploi(EmploiSaisieCtrl.sharedInstance(getEdc()).ajouter());
	}

	@Override
	protected void traitementsPourModification() {
		EmploiSaisieCtrl.sharedInstance(getEdc()).modifier(getCurrentEmploi());
		myView.getMyEOTable().updateUI();
	}

	@Override
	protected void traitementsPourSuppression() {
		if (EOOccupation.rechercherOccupationsPourEmploi(getEdc(), getCurrentEmploi(), DateCtrl.today(), false).size() > 0) {
			EODialogs.runInformationDialog("ERREUR", MangueMessagesErreur.ERREUR_EMPLOI_OCCUPATION_SUPPRESSION_KO);
			return;
		}

		getCurrentEmploi().invalider();
	}

	@Override
	protected void traitementsApresSuppression() {
	}

	@Override
	protected void traitementsApresCreation() {
		if (getNouvelEmploi() != null) {
			myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray<IEmploi>(getNouvelEmploi()));
		}
	}

	/**
	 * afficher la liste des catégories de l'emploi
	 */
	private void afficherCategories() {
		EmploiCategorieGestionCtrl.sharedInstance(getEdc()).open(getCurrentEmploi());
		listenerEmploi.onSelectionChanged();
		myView.getMyEOTable().updateUI();
	}

	/**
	 * afficher la liste des affectations de l'emploi
	 */
	private void afficherAffectations() {
		EmploiLocalisationGestionCtrl.sharedInstance(getEdc()).open(getCurrentEmploi());
		listenerEmploi.onSelectionChanged();
	}

	/**
	 * afficher la liste des occupations de l'emploi
	 */
	private void afficherOccupations() {
		OccupationGestionCtrl.sharedInstance(getEdc()).open(getCurrentEmploi());
		listenerEmploi.onSelectionChanged();
	}

	/**
	 * afficher la liste des spécialisations de l'emploi
	 */
	private void afficherSpecialisations() {
		EmploiSpecialisationGestionCtrl.sharedInstance(getEdc()).open(getCurrentEmploi());
		listenerEmploi.onSelectionChanged();
	}


	/**
	 * afficher la liste des budgets de l'emploi
	 */
	private void afficherBudget() {

		CRICursor.setWaitCursor(myView);
		myCtrlBudget.open(getCurrentEmploi());
		listenerEmploi.onSelectionChanged();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * afficher la liste des budgets de l'emploi
	 */
	private void afficherNatureBudgets() {
		EmploiNatureBudgetGestionCtrl.sharedInstance(getEdc()).open(getCurrentEmploi());
		listenerEmploi.onSelectionChanged();
	}

	/**
	 * Nettoyage de tous les filtres saisis
	 */
	private void nettoyerFiltres() {
		setCurrentStructure(null);
		setCurrentCategorie(null);

		CocktailUtilities.viderTextField(myView.getTfFiltreNoEmploi());
		CocktailUtilities.viderTextField(myView.getTfFiltrePeriodeDebut());
		CocktailUtilities.viderTextField(myView.getTfFiltrePeriodeFin());

		myView.getPopupBudget().setSelectedIndex(0);
		myView.getPopupImplantation().setSelectedIndex(0);
		myView.getPopupEnseignant().setSelectedIndex(0);
		myView.getPopupEtat().setSelectedIndex(0);
		myView.getPopupTypePopulation().setSelectedIndex(0);
		myView.getPopupCategFonctionPublique().setSelectedIndex(0);

		updateInterface();
	}

	/**
	 * afficher la liste des budgets de l'emploi
	 */
	@SuppressWarnings("unchecked")
	private void exporterListeEmplois() {

		try {
			JFileChooser saveDialog = new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "EMPLOIS_" + DateCtrl.dateToString(new NSTimestamp(), "%Y%m%d");
			saveDialog.setSelectedFile(new File(nomFichierExport + CocktailConstantes.EXTENSION_CSV));

			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {
				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = EmploiFactory.sharedInstance().getEnteteExport();
				List<IEmploi> listeEmplois = new ArrayList<IEmploi>(eod.displayedObjects());

				for (IEmploi myRecord : listeEmplois) {
					texte += EmploiFactory.sharedInstance().getTexteExportPourRecord(getEdc(), myRecord);
				}

				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);
				CRICursor.setDefaultCursor(myView);
				UtilitairesFichier.openFile(file.getPath());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Impression de la fiche d'emploi
	 */
	public void imprimerFicheEmploi() {
		LogManager.logDetail("Impression de la fiche d'emploi ");

		try {
			Object[] parametres = new Object[]{getEdc().globalIDForObject((EOEmploi) getCurrentEmploi())};
			UtilitairesImpression.imprimerSansDialogue(
					getEdc(), "clientSideRequestImprimerEmploi", new Class[] {EOGlobalID.class}, 
					parametres, "Emploi" + getCurrentEmploi().getNoEmploi(), "Impression de la fiche d'emploi");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur", e.getMessage());
		}
	}

	/**
	 * Impression de l'historique de la fiche d'emploi
	 */
	public void imprimerFicheEmploiHistorique() {
		LogManager.logDetail("Impression de l'historique de la fiche d'emploi ");

		try {
			Object[] parametres = new Object[]{getEdc().globalIDForObject((EOEmploi) getCurrentEmploi())};
			UtilitairesImpression.imprimerSansDialogue(
					getEdc(), "clientSideRequestImprimerEmploiHistorique", new Class[] {EOGlobalID.class}, 
					parametres, "Emploi" + getCurrentEmploi().getNoEmploi(), "Impression de l'historique de l'emploi");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur", e.getMessage());
		}
	}

	/**
	 * Affichage de la nomenclature des catégories d'emploi
	 */
	private void selectListeCategories() {
		EOCategorieEmploi categorie =  CategorieEmploiSelectCtrl.sharedInstance(getEdc()).getCategorie();
		if (categorie != null) {
			setCurrentCategorie(categorie);
		}
	}

	/**
	 * On vide le champ de saisie pour la catégorie d'emploi
	 */
	private void viderFiltreCategorie() {
		setCurrentCategorie(null);
	}

	/**
	 * Affichage de la liste des structures de l'établissement sous forme d'arbre
	 */
	private void selectListeStructures() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(getEdc());

		if (structure != null) {
			setCurrentStructure(structure);
		}
	}

	/**
	 * On vide le champ de saisie pour la catégorie d'emploi
	 */
	private void viderFiltreStructure() {
		setCurrentStructure(null);
	}

	/**
	 * @return la date du début de période saisie dans le filtre
	 */
	private NSTimestamp getFiltreDebutPeriode() {
		return CocktailUtilities.getDateFromField(myView.getTfFiltrePeriodeDebut());
	}

	/**
	 * @return la date de fin de période saisie dans le filtre
	 */
	private NSTimestamp getFiltreFinPeriode() {
		return CocktailUtilities.getDateFromField(myView.getTfFiltrePeriodeFin());
	}

	public IEmploi getNouvelEmploi() {
		return nouvelEmploi;
	}

	public void setNouvelEmploi(IEmploi nouvelEmploi) {
		this.nouvelEmploi = nouvelEmploi;
	}

	public IEmploi getCurrentEmploi() {
		return currentEmploi;
	}

	/**
	 * Met à jour l'emploi courant + les autres données reliées à cet emploi
	 * @param currentEmploi : l'emploi courant
	 */
	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
		updateDatas();
	}

	public EOCategorieEmploi getCurrentCategorie() {
		return currentCategorie;
	}

	/**
	 * Met à jour la catégorie d'emploi
	 * @param currentCategorie : la catégorie d'emploi
	 */
	public void setCurrentCategorie(EOCategorieEmploi currentCategorie) {
		this.currentCategorie = currentCategorie;
		CocktailUtilities.viderTextField(myView.getTfFiltreCategorie());

		if (currentCategorie != null) {
			CocktailUtilities.setTextToField(myView.getTfFiltreCategorie(), currentCategorie.codeEtLibelle());
		}
		updateInterface();
	}

	public EOStructure getCurrentStructure() {
		return currentStructure;
	}

	/**
	 * Met à jour la structure
	 * 
	 * @param currentStructure : la structure d'affectation
	 */
	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
		CocktailUtilities.viderTextField(myView.getTfFiltreAffectation());

		if (currentStructure != null) {
			CocktailUtilities.setTextToField(myView.getTfFiltreAffectation(), currentStructure.llStructure());
		}

		updateInterface();

	}

	public IEmploiSpecialisation getCurrentSpecialisation() {
		return currentSpecialisation;
	}

	/**
	 * Met à jour la spécialisation d'emploi
	 * 
	 * @param currentSpecialisation : la spécialisation d'emploi
	 */
	public void setCurrentSpecialisation(IEmploiSpecialisation currentSpecialisation) {
		this.currentSpecialisation = currentSpecialisation;
		CocktailUtilities.viderTextField(myView.getTfSpecialisation());

		if (currentSpecialisation != null) {
			CocktailUtilities.setTextToField(myView.getTfSpecialisation(), currentSpecialisation.libelleSpecialisationAffiche());
		}
	}

	public List<IEmploiLocalisation> getCurrentListeAffectations() {
		return currentListeAffectations;
	}

	/**
	 * Met à jour la liste des affectations de l'emploi
	 * 
	 * @param currentListeAffectations : la liste des affectations
	 */
	public void setCurrentListeAffectations(List<IEmploiLocalisation> currentListeAffectations) {
		this.currentListeAffectations = currentListeAffectations;
		CocktailUtilities.viderTextField(myView.getTfAffectation());

		if (currentListeAffectations != null) {
			String libelleAffectation = "";
			for (IEmploiLocalisation uneAffectation : currentListeAffectations) {
				if (uneAffectation == currentListeAffectations.get(0)) {
					libelleAffectation += uneAffectation.toString(); 
				} else {
					libelleAffectation += ", " + uneAffectation.toString();
				}
			}
			CocktailUtilities.setTextToField(myView.getTfAffectation(), libelleAffectation);
		}
	}

	public IEmploiCategorie getCurrentEmploiCategorie() {
		return currentEmploiCategorie;
	}

	/**
	 * Met à jour la catégorie d'emploi
	 * 
	 * @param currentEmploiCategorie : l'emploi catégorie
	 */
	public void setCurrentEmploiCategorie(IEmploiCategorie currentEmploiCategorie) {
		this.currentEmploiCategorie = currentEmploiCategorie;
		CocktailUtilities.viderTextField(myView.getTfCategorie());

		if (currentEmploiCategorie != null) {
			CocktailUtilities.setTextToField(myView.getTfCategorie(), currentEmploiCategorie.toString());
		}
	}

	public IEmploiNatureBudget getCurrentEmploiNatureBudget() {
		return currentEmploiNatureBudget;
	}

	/**
	 * Met à jour le budget de l'emploi
	 * 
	 * @param currentEmploiNatureBudget : le budget de l'emploi
	 */
	public void setCurrentEmploiNatureBudget(IEmploiNatureBudget currentEmploiNatureBudget) {
		this.currentEmploiNatureBudget = currentEmploiNatureBudget;
		CocktailUtilities.viderTextField(myView.getTfBudget());

		if (currentEmploiNatureBudget != null) {
			CocktailUtilities.setTextToField(myView.getTfBudget(), currentEmploiNatureBudget.toString());
		}
	}

	public List<EOOccupation> getCurrentListeOccupations() {
		return currentListeOccupations;
	}

	/**
	 * Met à jour l'occupation de l'emploi
	 * 
	 * @param currentListeOccupations : : la liste des occupations
	 */
	public void setCurrentListeOccupations(List<EOOccupation> currentListeOccupations) {
		this.currentListeOccupations = currentListeOccupations;
		CocktailUtilities.viderTextField(myView.getTfOccupation());

		if (currentListeOccupations != null) {
			String libelleOccupation = "";
			for (EOOccupation uneOccupation : currentListeOccupations) {
				if (uneOccupation == currentListeOccupations.get(0)) {
					libelleOccupation += uneOccupation.libellePourEmploi(); 
				} else {
					libelleOccupation += ", " + uneOccupation.libellePourEmploi();
				}
			}

			CocktailUtilities.setTextToField(myView.getTfOccupation(), libelleOccupation);
		}
	}

	public EmploisView getMyView() {
		return myView;
	}

	/**
	 * Classe d'écoute pour lancer la recherche
	 * @author cpinsard
	 */
	private class MyActionFiltreListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			rechercher();
		}
	}

	/**
	 * Classe d'ecoute sur le display group.
	 * Permet de lancer des actions lors d'un clic ou double clic sur une ligne de donnees
	 * @author cpinsard
	 *
	 */
	private class ListenerEmploi implements ZEOTableListener {

		public void onDbClick() {
			modifier();
		}

		public void onSelectionChanged() {
			CRICursor.setWaitCursor(myView);
			// Mise a jour de l'objet courant
			setCurrentEmploi((EOEmploi) eod.selectedObject());
			//maj des 4 textfield du bas
			updateDatas();
			updateInterface();
			CRICursor.setDefaultCursor(myView);
		}
	}

}

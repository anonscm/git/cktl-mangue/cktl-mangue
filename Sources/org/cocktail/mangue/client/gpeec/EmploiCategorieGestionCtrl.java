package org.cocktail.mangue.client.gpeec;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.gpeec.EmploiCategorieGestionView;
import org.cocktail.mangue.client.select.CategorieEmploiSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiCategorieFactory;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiCategorieFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Controleur de la gestion des catégories par emploi
 * 
 * @author Chama LAATIK
 */
public class EmploiCategorieGestionCtrl extends ModelePageGestion {

	private static EmploiCategorieGestionCtrl sharedInstance;

	private EmploiCategorieGestionView myView;
	private EODisplayGroup eod;
	private ListenerEmploiCategorie listenerEmploiCategorie;
	
	// Objet principal géré dans la fenetre
	private IEmploi currentEmploi;
	private IEmploiCategorie currentEmploiCategorie;
	private EOCategorieEmploi currentCategorie;
	private EOCategorieEmploi oldCategorie;
	
	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	public EmploiCategorieGestionCtrl(EOEditingContext edc) {
		super(edc);
		eod = new EODisplayGroup();
		listenerEmploiCategorie = new ListenerEmploiCategorie();

		// Generation / Initialisation de l'interface
		myView = new EmploiCategorieGestionView(null, true, eod);

		// Affectation d'une methode a chaque bouton de l'interface
		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		myView.getBtnGetCategorieEmploi().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { selectListeCategorie(); } }
		);
		
		//Controle de saisie et completion d'une date
		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());
		
		// Desactivation d'un text field correspondant a une nomenclature - Pas de saisie libre
		CocktailUtilities.initTextField(myView.getTfCategorieEmploi(), false, false);

		// Ajout d'un listener sur la table pour surveiller les clics / double clics
		myView.getMyEOTable().addListener(listenerEmploiCategorie);

		// Mode saisie desactive par defaut
		setSaisieEnabled(false);
		updateInterface();
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiCategorieGestionCtrl.sharedInstance(edc).methode
	 * 
	 * @param edc : editingContext
	 * @return une instance de la classe
	 */
	public static EmploiCategorieGestionCtrl sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null) {
			sharedInstance = new EmploiCategorieGestionCtrl(edc);
		}
		return sharedInstance;
	}
	
	/**
	 * Ouverture de l'interface de gestion
	 * 
	 * @param emploi : emploi géré
	 */
	public void open(IEmploi emploi) {
		setCurrentEmploi(emploi);
		myView.setTitle("Gestion des catégories d'emploi - Emploi " + emploi.getNoEmploi());
		setSaisieEnabled(false);
		actualiser();
		// Affichage de la fenetre en mode modal ou pas selon le parametrage
		myView.setVisible(true);
	}

	@Override
	protected void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		setCurrentCategorie(null);
	}

	@Override
	protected void updateDatas() {
		clearDatas();
		
		if (getCurrentEmploiCategorie() != null) {
			setCurrentCategorie(getCurrentEmploiCategorie().getToCategorieEmploi());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentEmploiCategorie().getDateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentEmploiCategorie().getDateFin());
		}

		updateInterface();
	}
	
	@Override
	protected void updateInterface() {
		myView.getBtnAjouter().setEnabled(getCurrentEmploi() != null && !isSaisieEnabled());
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentEmploiCategorie() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentEmploiCategorie() != null);		
		
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());

		myView.getBtnGetCategorieEmploi().setEnabled(isSaisieEnabled());
	}

	@Override
	protected void actualiser() {
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EmploiCategorieFinder.sharedInstance().findForEmploi(getEdc(), getCurrentEmploi()));
		myView.getMyEOTable().updateData();
		CRICursor.setDefaultCursor(myView);
	}
	
	@Override
	protected void refresh() {
		if (isModeCreation()) {
			IEmploiCategorie newSelection = getCurrentEmploiCategorie();
			actualiser();
			CocktailUtilities.forceSelection(myView.getMyEOTable(), new NSArray<IEmploiCategorie>(newSelection));
		} else {
			myView.getMyEOTable().updateUI();
		}
	}

	@Override
	protected void traitementsAvantValidation() {
		//MAJ objet
		getCurrentEmploiCategorie().setDateDebut(getDateDebut());
		getCurrentEmploiCategorie().setDateFin(getDateFin());
		setOldCategorie(getCurrentEmploiCategorie().getToCategorieEmploi());
		getCurrentEmploiCategorie().setToCategorieEmploiRelationship(getCurrentCategorie());
		
		// Controle de saisie
		if (getDateDebut() == null) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE);
		} else if (getDateFin() != null && getDateDebut().after(getDateFin())) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT);
		}
		
		if (DateCtrl.isBefore(getDateDebut(), getCurrentEmploi().getDateDebutEmploi())) {
			String date = DateCtrl.dateToString(getCurrentEmploi().getDateDebutEmploi());
			throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_DEBUT_AVANT_DEBUT_EMPLOI, date));
		}
		
		if ((getCurrentEmploi().getDateFermetureEmploi() != null) && (getDateFin() == null)) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_ELEMENTS_DATE_FIN_NON_RENSEIGNE);
		}
		
		if ((getCurrentEmploi().getDateFermetureEmploi() != null) && (getDateFin() != null)
				&& DateCtrl.isBefore(getCurrentEmploi().getDateFermetureEmploi(), getDateFin())) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_DATE_FIN_APRES_FERMETURE);
		}

		//Vérifier que la catégorie d'emploi est renseignée
		if (getCurrentCategorie() == null) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_CATEGORIE_EMPLOI_NON_RENSEIGNE);
		}	
		
		traitementsPourHistorisationDate();
	}

	@Override
	protected void traitementsApresValidation() {
		//Si on change de type de population, on maj le témoin enseignant et alerte l'utilisateur pour maj la spécialisation
		if ((getCurrentCategorie().typePopulation() != null) && !getCurrentEmploi().getTemEnseignant().equals(getCurrentCategorie().typePopulation().temEnseignant())) {
			//MAJ du témoin enseignant de l'emploi
			getCurrentEmploi().setTemEnseignant(getCurrentCategorie().typePopulation().temEnseignant());
			EODialogs.runInformationDialog(CocktailConstantes.ATTENTION, MangueMessagesErreur.ATTENTION_EMPLOI_ENSEIGNANT_MODIF_SPECIALISATION);
		}
		
		//Si on change la catégorie d'emploi, on recalcule le no d'emploi formatté
		if (getCurrentCategorie() != getOldCategorie()) {
			String noEmploiFormatte = null;
			
			//Met à jour le no d'emploi formatté
			if (currentCategorie.typePopulation() != null) {
				noEmploiFormatte = getCurrentEmploi().formatterNoEmploi(getCurrentEmploi().getNoEmploi(), getCurrentCategorie(), null, 
						getCurrentCategorie().typePopulation().est2Degre(), getCurrentCategorie().typePopulation().estEnseignantSuperieur());
			} else {
				EODialogs.runInformationDialog(CocktailConstantes.ATTENTION, MangueMessagesErreur.ATTENTION_CATEGORIE_EMPLOI_SANS_CORPS);
				noEmploiFormatte = getCurrentEmploi().formatterNoEmploi(getCurrentEmploi().getNoEmploi(), getCurrentCategorie(), null, 
						false, false);
			}
		}
	}

	@Override
	protected void traitementsPourAnnulation() {
		// Réactualisation des données de l'objet selectionné
		listenerEmploiCategorie.onSelectionChanged();
	}

	@Override
	protected void traitementsPourCreation() {
		setCurrentEmploiCategorie(EmploiCategorieFactory.sharedInstance().creer(getEdc(), getCurrentEmploi(), 
				((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel()));
		
		// S'il n'y a pas de catégorie d'emploi, on initialise avec la date de publication de l'emploi
		// sinon avec la date d'effet
		if (getCurrentEmploi().getListeEmploiCategories().size() == 1) {
			if (getCurrentEmploi().getDatePublicationEmploi() != null) {
				getCurrentEmploiCategorie().setDateDebut(getCurrentEmploi().getDatePublicationEmploi());
			} else if (getCurrentEmploi().getDateEffetEmploi() != null) {
				getCurrentEmploiCategorie().setDateDebut(getCurrentEmploi().getDateEffetEmploi());
			}
		} else {
			//S'il existe des catégories d'emploi, on initialise la date de début avec la date de fin + 1j du dernier élément
			getCurrentEmploiCategorie().setDateDebut(DateCtrl.jourSuivant(EmploiCategorieFinder.sharedInstance()
					.findForEmploi(new EOEditingContext(), getCurrentEmploi()).get(0).getDateFin()));
		}
		
		updateDatas();
	}
	
	/**
	 * Permet de ne pas avoir de trous dans l'historisation des catégories d'emploi
	 *  => Met à jour la date de début et de fin des éléments précédents et suivants
	 * de l'élément modifié/créé 
	 */
	protected void traitementsPourHistorisationDate() {
		IEmploiCategorie elementPrecedent = null;
		IEmploiCategorie elementSuivant = null;
		NSArray<IEmploiCategorie> liste = null;
		EOEditingContext nouveauEdc = new EOEditingContext();
		
		if (isModeCreation()) {
			//A la création, on initialise avec un nouveau EditingContext, sinon erreur EOCheapMutableArray
			liste = EmploiCategorieFinder.sharedInstance().findForEmploi(nouveauEdc, getCurrentEmploi());
		} else {
			liste = EmploiCategorieFinder.sharedInstance().findForEmploi(getEdc(), getCurrentEmploi());
		}
		
		//Uniquement en modification
		if (liste.indexOf(getCurrentEmploiCategorie()) >= 0) {
			int index = liste.indexOf(getCurrentEmploiCategorie());
			LogManager.logDetail("date de début : " + DateCtrl.dateToString(getCurrentEmploiCategorie().getDateDebut()));
			LogManager.logDetail("date de fin : " + DateCtrl.dateToString(getCurrentEmploiCategorie().getDateFin()));
			
			//Si l'élément est le dernier de la liste => on n'a pas d'élément précédent
			if (getCurrentEmploiCategorie() != liste.get(liste.size() - 1)) {	
				elementPrecedent = liste.get(index + 1);
				LogManager.logDetail("la catégorie précédente : " + elementPrecedent.getToCategorieEmploi().cCategorieEmploi());
				LogManager.logDetail("  > date de début : " + DateCtrl.dateToString(elementPrecedent.getDateDebut()));
				LogManager.logDetail("  > date de fin : " + DateCtrl.dateToString(elementPrecedent.getDateFin()));
			}
			
			//Si l'élément est le premier de la liste => on n'a pas d'élément suivant
			if (getCurrentEmploiCategorie() != liste.get(0)) {	
				elementSuivant = liste.get(index - 1);
				LogManager.logDetail("la catégorie suivante : " + elementSuivant.getToCategorieEmploi().cCategorieEmploi());
				LogManager.logDetail("  > date de début : " + DateCtrl.dateToString(elementSuivant.getDateDebut()));
				LogManager.logDetail("  > date de fin : " + DateCtrl.dateToString(elementSuivant.getDateFin()));
			}
		
			//elementPrecedent.dateDebut < element.dateDebut (=> permet de ne pas avoir de chevauchement sur d'autres éléments)
			if (elementPrecedent != null) {
				if (DateCtrl.isBefore(elementPrecedent.getDateDebut(), getCurrentEmploiCategorie().getDateDebut())) {
				elementPrecedent.setDateFin(DateCtrl.jourPrecedent(getCurrentEmploiCategorie().getDateDebut()));
				} else {
					throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_DEBUT_HISTO_KO, 
							DateCtrl.dateToString(elementPrecedent.getDateDebut())));
				}
			}
			
			//element.dateFin < elementSuivant.dateFin (=> permet de ne pas avoir de chevauchement sur d'autres éléments)
			if (elementSuivant != null) {
				traitementDateElementSuivant(elementSuivant);
			}
		} else {
			//En création, si la date de fin de l'élement précédent est null, on la met à jour avec la date de début
			//de la catégorie d'emploi - 1j
			if (liste.size() > 0 && liste.get(0) != null && liste.get(0).getDateFin() == null) {
				liste.get(0).setDateFin(DateCtrl.jourPrecedent(getCurrentEmploiCategorie().getDateDebut()));
				nouveauEdc.saveChanges();
			}
		}
	}
	
	/**
	 * Permet de gérer les dates de l'élement qui suit la catégorie modifiée
	 * element.dateFin < elementSuivant.dateFin (=> permet de ne pas avoir de chevauchement sur d'autres éléments)
	 * @param elementSuivant : élement suivant
	 */
	private void traitementDateElementSuivant(IEmploiCategorie elementSuivant) {
		if (elementSuivant.getDateFin() == null) {
			elementSuivant.setDateDebut(DateCtrl.jourSuivant(getCurrentEmploiCategorie().getDateFin()));
		} else {
			if (DateCtrl.isBefore(getCurrentEmploiCategorie().getDateFin(), elementSuivant.getDateFin())) {
				elementSuivant.setDateDebut(DateCtrl.jourSuivant(getCurrentEmploiCategorie().getDateFin()));
			} else {
				throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_FIN_HISTO_KO, 
						DateCtrl.dateToString(elementSuivant.getDateFin())));
			}
		}
	}

	@Override
	protected void traitementsPourSuppression() {
		getEdc().deleteObject((EOEmploiCategorie) getCurrentEmploiCategorie());
	}

	@Override
	protected void traitementsApresSuppression() {
		
	}
	
	/**
	 * Affichage de la nomenclature des catégories d'emploi
	 */
	private void selectListeCategorie() {
		EOCategorieEmploi categorie =  CategorieEmploiSelectCtrl.sharedInstance(getEdc()).getCategorie();
		if (categorie != null) {
			setCurrentCategorie(categorie);
		}
	}
	
	public IEmploi getCurrentEmploi() {
		return currentEmploi;
	}

	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
	}

	public IEmploiCategorie getCurrentEmploiCategorie() {
		return currentEmploiCategorie;
	}

	/**
	 * Met à jour la catégorie d'emploi courante + les autres données reliées à cette catégorie
	 * @param currentEmploiCategorie : la catégorie d'emploi courante
	 */
	public void setCurrentEmploiCategorie(IEmploiCategorie currentEmploiCategorie) {
		this.currentEmploiCategorie = currentEmploiCategorie;
		updateDatas();
	}

	public EOCategorieEmploi getCurrentCategorie() {
		return currentCategorie;
	}

	/**
	 * Met à jour la catégorie d'emploi (nomenclature) courante 
	 * @param currentCategorie : la catégorie d'emploi (nomenclature)
	 */
	public void setCurrentCategorie(EOCategorieEmploi currentCategorie) {
		this.currentCategorie = currentCategorie;
		CocktailUtilities.viderTextField(myView.getTfCategorieEmploi());
		
		if (currentCategorie != null) {
			CocktailUtilities.setTextToField(myView.getTfCategorieEmploi(), currentCategorie.codeEtLibelle());
		}
	}
	
	private NSTimestamp getDateDebut()	{
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	
	private NSTimestamp getDateFin()	{
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}

	public EOCategorieEmploi getOldCategorie() {
		return oldCategorie;
	}

	public void setOldCategorie(EOCategorieEmploi oldCategorie) {
		this.oldCategorie = oldCategorie;
	}

	/** 
	 * Classe d'ecoute sur les catégories d'emploi
	 * Permet de mettre à jour l'interface
	 */
	private class ListenerEmploiCategorie implements ZEOTableListener {

		public void onDbClick() {
			modifier();
		}

		public void onSelectionChanged() {
			setCurrentEmploiCategorie((IEmploiCategorie) eod.selectedObject());
			updateInterface();
		}
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		
	}

}

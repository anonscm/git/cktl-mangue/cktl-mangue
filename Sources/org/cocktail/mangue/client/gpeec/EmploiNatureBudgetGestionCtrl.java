package org.cocktail.mangue.client.gpeec;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.gpeec.EmploiNatureBudgetGestionView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.metier.factory.gpeec.EmploiNatureBudgetFactory;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiNatureBudgetFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiNatureBudget;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EONatureBudget;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Controleur de la gestion des nature budget par emploi
 * 
 * @author Chama LAATIK
 */
public class EmploiNatureBudgetGestionCtrl extends ModelePageGestion {

	private static EmploiNatureBudgetGestionCtrl sharedInstance;

	private EmploiNatureBudgetGestionView myView;
	private EODisplayGroup eod;
	private ListenerEmploiNatureBudget listenerEmploiNatureBudget;
	
	// Objet principal géré dans la fenetre
	private IEmploi currentEmploi;
	private IEmploiNatureBudget currentEmploiNatureBudget;
	
	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	@SuppressWarnings("unchecked")
	public EmploiNatureBudgetGestionCtrl(EOEditingContext edc) {
		super(edc);
		eod = new EODisplayGroup();
		listenerEmploiNatureBudget = new ListenerEmploiNatureBudget();

		// Generation / Initialisation de l'interface
		myView = new EmploiNatureBudgetGestionView(null, true, eod);


		// Affectation d'une methode a chaque bouton de l'interface
		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		
		//Controle de saisie et completion d'une date
		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());
		
		//Initialisation des popups
		myView.setListeBudgets(EONatureBudget.fetchAll(edc));

		// Ajout d'un listener sur la table pour surveiller les clics / double clics
		myView.getMyEOTable().addListener(listenerEmploiNatureBudget);

		// Mode saisie desactive par defaut
		setSaisieEnabled(false);
		updateInterface();
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiNatureBudgetGestionCtrl.sharedInstance(edc).methode
	 * 
	 * @param edc : editingContext
	 * @return une instance de la classe
	 */
	public static EmploiNatureBudgetGestionCtrl sharedInstance(EOEditingContext edc) {
		if (sharedInstance == null) {
			sharedInstance = new EmploiNatureBudgetGestionCtrl(edc);
		}
		return sharedInstance;
	}
	
	/**
	 * Ouverture de l'interface de gestion
	 * 
	 * @param emploi : emploi géré
	 */
	public void open(IEmploi emploi) {
		setCurrentEmploi(emploi);
		myView.setTitle("Gestion des budgets d'emploi - Emploi " + emploi.getNoEmploi());
		setSaisieEnabled(false);
		actualiser();
		// Affichage de la fenetre en mode modal ou pas selon le parametrage
		myView.setVisible(true);
	}

	@Override
	protected void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		setCurrentNatureBudget(null);
	}

	@Override
	protected void updateDatas() {
		clearDatas();

		if (getCurrentEmploiNatureBudget() != null) {
			setCurrentNatureBudget(getCurrentEmploiNatureBudget().getToNatureBudget());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentEmploiNatureBudget().getDateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentEmploiNatureBudget().getDateFin());
		}

		updateInterface();
	}
	
	@Override
	protected void updateInterface() {
		myView.getBtnAjouter().setEnabled(getCurrentEmploi() != null && !isSaisieEnabled());
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentEmploiNatureBudget() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentEmploiNatureBudget() != null);

		myView.getPopupNatureBudget().setEnabled(isSaisieEnabled());
		
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());
	}

	@Override
	protected void actualiser() {
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EmploiNatureBudgetFinder.sharedInstance().findForEmploi(getEdc(), getCurrentEmploi()));
		myView.getMyEOTable().updateData();
		
		CRICursor.setDefaultCursor(myView);
	}
	
	@Override
	protected void refresh() {
		if (isModeCreation()) {
			IEmploiNatureBudget newSelection = getCurrentEmploiNatureBudget();
			actualiser();
			CocktailUtilities.forceSelection(myView.getMyEOTable(), new NSArray(newSelection));
		}
		else
			myView.getMyEOTable().updateUI();
	}

	@Override
	protected void traitementsAvantValidation() {
		//MAJ objet
		getCurrentEmploiNatureBudget().setDateDebut(getDateDebut());
		getCurrentEmploiNatureBudget().setDateFin(getDateFin());
		getCurrentEmploiNatureBudget().setToNatureBudgetRelationship(getCurrentNatureBudget());
				
		// Controle de saisie
		if (getDateDebut() == null) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE);
		} else if (getDateFin() != null && getDateDebut().after(getDateFin())) {
			throw new ValidationException(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT);
		}
		
		if (DateCtrl.isBefore(getDateDebut(), getCurrentEmploi().getDateDebutEmploi())) {
			String date = DateCtrl.dateToString(getCurrentEmploi().getDateDebutEmploi());
			throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_DEBUT_AVANT_DEBUT_EMPLOI, date));
		}
		
		if ((getCurrentEmploi().getDateFermetureEmploi() != null) && (getDateFin() == null)) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_ELEMENTS_DATE_FIN_NON_RENSEIGNE);
		}
		
		if ((getCurrentEmploi().getDateFermetureEmploi() != null) && (getDateFin() != null)
				&& DateCtrl.isBefore(getCurrentEmploi().getDateFermetureEmploi(), getDateFin())) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_EMPLOI_DATE_FIN_APRES_FERMETURE);
		}	
		
		//Vérifier que le budget est renseigné
		if (getCurrentNatureBudget() == null) {
			throw new ValidationException(MangueMessagesErreur.ERREUR_BUDGET_NON_RENSEIGNE);
		}
		
		traitementsPourHistorisationDate();
	}

	@Override
	protected void traitementsApresValidation() {
	}

	@Override
	protected void traitementsPourAnnulation() {
		// Réactualisation des données de l'objet selectionné
		listenerEmploiNatureBudget.onSelectionChanged();
	}

	@Override
	protected void traitementsPourCreation() {
		setCurrentEmploiNatureBudget(EmploiNatureBudgetFactory.sharedInstance().creer(getEdc(), getCurrentEmploi(), 
				((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel()));
		
		// S'il n'y a pas de budget, on initialise avec la date de publication de l'emploi
		// sinon avec la date d'effet
		if (getCurrentEmploi().getListeEmploiNatureBudgets().size() == 1) {
			if (getCurrentEmploi().getDatePublicationEmploi() != null) {
				getCurrentEmploiNatureBudget().setDateDebut(getCurrentEmploi().getDatePublicationEmploi());
			} else if (getCurrentEmploi().getDateEffetEmploi() != null) {
				getCurrentEmploiNatureBudget().setDateDebut(getCurrentEmploi().getDateEffetEmploi());
			}
		} else {
			//S'il existe des budgets, on initialise la date de début avec la date de fin + 1j du dernier élément
			getCurrentEmploiNatureBudget().setDateDebut(DateCtrl.jourSuivant(EmploiNatureBudgetFinder.sharedInstance()
					.findForEmploi(new EOEditingContext(), getCurrentEmploi()).get(0).getDateFin()));
		}
		
		updateDatas();
	}
	
	/**
	 * Permet de ne pas avoir de trous dans l'historisation des budgets de l'emploi
	 *  => Met à jour la date de début et de fin des éléments précédents et suivants
	 * de l'élement modifié/créé 
	 */
	protected void traitementsPourHistorisationDate() {
		IEmploiNatureBudget elementPrecedent = null;
		IEmploiNatureBudget elementSuivant = null;
		NSArray<IEmploiNatureBudget> liste = null;
		EOEditingContext nouveauEdc = new EOEditingContext();
		
		if (isModeCreation()) {
			//A la création, on initialise avec un nouveau EditingContext, sinon erreur EOCheapMutableArray
			liste = EmploiNatureBudgetFinder.sharedInstance().findForEmploi(nouveauEdc, getCurrentEmploi());
		} else {
			liste = EmploiNatureBudgetFinder.sharedInstance().findForEmploi(getEdc(), getCurrentEmploi());
		}
		
		if (liste.indexOf(getCurrentEmploiNatureBudget()) >= 0) {
			int index = liste.indexOf(getCurrentEmploiNatureBudget());
			LogManager.logDetail("date de début : " + DateCtrl.dateToString(getCurrentEmploiNatureBudget().getDateDebut()));
			LogManager.logDetail("date de fin : " + DateCtrl.dateToString(getCurrentEmploiNatureBudget().getDateFin()));
			
			//Si l'élément est le dernier de la liste => on n'a pas d'élément précédent
			if (getCurrentEmploiNatureBudget() != liste.get(liste.size() - 1)) {	
				elementPrecedent = liste.get(index + 1);
				LogManager.logDetail("le budget précédent : " + elementPrecedent.getToNatureBudget().libelle());
				LogManager.logDetail("  > date de début : " + DateCtrl.dateToString(elementPrecedent.getDateDebut()));
				LogManager.logDetail("  > date de fin : " + DateCtrl.dateToString(elementPrecedent.getDateFin()));
			}
			
			//Si l'élément est le premier de la liste => on n'a pas d'élément suivant
			if (getCurrentEmploiNatureBudget() != liste.get(0)) {	
				elementSuivant = liste.get(index - 1);
				LogManager.logDetail("le budget suivant : " + elementSuivant.getToNatureBudget().libelle());
				LogManager.logDetail("  > date de début : " + DateCtrl.dateToString(elementSuivant.getDateDebut()));
				LogManager.logDetail("  > date de fin : " + DateCtrl.dateToString(elementSuivant.getDateFin()));
			}
		
			//elementPrecedent.dateDebut < element.dateDebut (=> permet de ne pas avoir de chevauchement sur d'autres éléments)
			if (elementPrecedent != null) {
				if (DateCtrl.isBefore(elementPrecedent.getDateDebut(), getCurrentEmploiNatureBudget().getDateDebut())) {
				elementPrecedent.setDateFin(DateCtrl.jourPrecedent(getCurrentEmploiNatureBudget().getDateDebut()));
				} else {
					throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_DEBUT_HISTO_KO, 
							DateCtrl.dateToString(elementPrecedent.getDateDebut())));
				}
			}
			
			//element.dateFin < elementSuivant.dateFin (=> permet de ne pas avoir de chevauchement sur d'autres éléments)
			if (elementSuivant != null) {
				traitementDateElementSuivant(elementSuivant);
			}
		} else {
			//En création, si la date de fin de l'élement précédent est null, on la met à jour avec la date de début
			//de la catégorie d'emploi - 1j
			if (liste.size() > 0 && liste.get(0) != null && liste.get(0).getDateFin() == null) {
				liste.get(0).setDateFin(DateCtrl.jourPrecedent(getCurrentEmploiNatureBudget().getDateDebut()));
				nouveauEdc.saveChanges();
			}
		}
		
	}
	
	/**
	 * Permet de gérer les dates de l'élement qui suit le budget modifié
	 * element.dateFin < elementSuivant.dateFin (=> permet de ne pas avoir de chevauchement sur d'autres éléments)
	 * @param elementSuivant : élement suivant
	 */
	private void traitementDateElementSuivant(IEmploiNatureBudget elementSuivant) {
		if (elementSuivant.getDateFin() == null) {
			elementSuivant.setDateDebut(DateCtrl.jourSuivant(getCurrentEmploiNatureBudget().getDateFin()));
		} else {
			if (DateCtrl.isBefore(getCurrentEmploiNatureBudget().getDateFin(), elementSuivant.getDateFin())) {
				elementSuivant.setDateDebut(DateCtrl.jourSuivant(getCurrentEmploiNatureBudget().getDateFin()));
			} else {
				throw new ValidationException(String.format(MangueMessagesErreur.ERREUR_EMPLOI_DATE_FIN_HISTO_KO, 
						DateCtrl.dateToString(elementSuivant.getDateFin())));
			}
		}
	}

	@Override
	protected void traitementsPourSuppression() {
		getEdc().deleteObject((EOEmploiNatureBudget) getCurrentEmploiNatureBudget());
	}

	@Override
	protected void traitementsApresSuppression() {
		
	}
	
	public IEmploi getCurrentEmploi() {
		return currentEmploi;
	}

	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
	}


	public IEmploiNatureBudget getCurrentEmploiNatureBudget() {
		return currentEmploiNatureBudget;
	}

	/**
	 * Met à jour la nature budget d'emploi courante
	 * @param currentEmploiNatureBudget : la nature budget d'emploi courante
	 */
	public void setCurrentEmploiNatureBudget(IEmploiNatureBudget currentEmploiNatureBudget) {
		this.currentEmploiNatureBudget = currentEmploiNatureBudget;
		updateDatas();
	}

	/**
	 * @return le budget selectionné
	 */
	public EONatureBudget getCurrentNatureBudget() {
		return (EONatureBudget) myView.getPopupNatureBudget().getSelectedItem();
	}

	/**
	 * Met à jour le budget
	 * @param currentBudget : la nature de budget
	 */
	public void setCurrentNatureBudget(EONatureBudget currentBudget) {
		myView.getPopupNatureBudget().setSelectedIndex(0);
		
		if (currentBudget != null) {
			myView.getPopupNatureBudget().setSelectedItem(currentBudget);
		} 
	}
	
	private NSTimestamp getDateDebut()	{
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	
	private NSTimestamp getDateFin()	{
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}

	/** 
	 * Classe d'ecoute sur les catégories d'emploi
	 * Permet de mettre à jour l'interface
	 */
	private class ListenerEmploiNatureBudget implements ZEOTableListener {

		public void onDbClick() {
			modifier();
		}

		public void onSelectionChanged() {
			setCurrentEmploiNatureBudget((IEmploiNatureBudget) eod.selectedObject());
			updateInterface();
		}
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		
	}

}

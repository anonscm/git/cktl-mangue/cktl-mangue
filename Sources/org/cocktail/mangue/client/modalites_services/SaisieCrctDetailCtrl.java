// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.modalites_services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.modalites.SaisieCrctDetailView;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.modele.mangue.modalites.EOCrctDetail;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieCrctDetailCtrl extends ModelePageSaisie
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieCrctDetailCtrl sharedInstance;
	private EOEditingContext edc;
	private SaisieCrctDetailView myView;
	
	private EOCrct currentCrct;
	private EOCrctDetail currentDetail;

	public SaisieCrctDetailCtrl(EOEditingContext edc) {

		super(edc);
		this.edc = edc;

		myView = new SaisieCrctDetailView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

	}

	public static SaisieCrctDetailCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieCrctDetailCtrl(editingContext);
		return sharedInstance;
	}

	public EOCrct getCurrentCrct() {
		return currentCrct;
	}

	public void setCurrentCrct(EOCrct currentCrct) {
		this.currentCrct = currentCrct;
	}

	public EOCrctDetail getCurrentDetail() {
		return currentDetail;
	}

	public void setCurrentDetail(EOCrctDetail currentDetail) {
		this.currentDetail = currentDetail;
	}

	/**
	 * 
	 * @param crct
	 * @return
	 */
	public EOCrctDetail ajouter(EOCrct crct)	{

		clearDatas();
		setCurrentCrct(crct);
		
		myView.setTitle("Fractionnement CRCT ( " + getCurrentCrct().individu().identitePrenomFirst() + ")");

		setCurrentDetail(EOCrctDetail.creer(edc, getCurrentCrct(), false));

		updateDatas();
		myView.setVisible(true);
		return getCurrentDetail();
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
	}

	@Override
	protected void updateDatas() {
		
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentDetail() != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentDetail().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentDetail().dateFin());
		}
		updateInterface();
		
	}
	
	protected void valider() {
		
		try {
			traitementsAvantValidation();
			edc.insertObject(getCurrentDetail());
			traitementsApresValidation();
		}
		catch (NSValidation.ValidationException e) {
			EODialogs.runErrorDialog("ERREUR", e.getMessage());
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", e.getMessage());			
		}

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() throws ValidationException {
		// TODO Auto-generated method stub
		getCurrentDetail().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentDetail().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
		
		if (getCurrentDetail().dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début de la période de fractionnement doit être fournie");
		} else if (DateCtrl.isBefore(getCurrentDetail().dateDebut(), getCurrentCrct().dateDebut()) || DateCtrl.isAfter(getCurrentDetail().dateDebut(), getCurrentCrct().dateFin())) {
			throw new NSValidation.ValidationException("Le début de la période de fractionnement doit se trouver dans la période du CRCT");
		}
		if (getCurrentDetail().dateFin() == null) {
			throw new NSValidation.ValidationException("La date de fin de la période de fractionnement doit être fournie");
		} else if (DateCtrl.isBefore(getCurrentDetail().dateFin(), getCurrentCrct().dateDebut()) || DateCtrl.isAfter(getCurrentDetail().dateFin(),getCurrentCrct().dateFin())) {
			throw new NSValidation.ValidationException("La fin de la période de fractionnement doit se trouver dans la période du CRCT");
		}
		if (DateCtrl.isBefore(getCurrentDetail().dateFin(),getCurrentDetail().dateDebut())) {
			throw new NSValidation.ValidationException("La date de fin  de la période de fractionnement ne peut être postérieure à celle de début");
		}

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
			myView.setVisible(false);		
	}

	@Override
	protected void traitementsPourAnnulation() {
		setCurrentDetail(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}

}

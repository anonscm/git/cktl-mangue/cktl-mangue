/*
 * Created on 22 nov. 2005
 *
 * Gère les reprises à temps plein
 * Window - Preferences - Java - Code Style - Code Templates
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.modalites_services;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.outils_interface.GestionEvenementAvecArrete;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.Arrete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.modalites.EORepriseTempsPlein;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;

/** Gere les reprises a temps plein
 * @author christine
 *
 */
// 13/10/2010 - on prend la même gestion des arrêtés que le temps partiel
// 03/11/2010 - Adaptation Netbeans
public class GestionRepriseTempsPlein extends GestionEvenementAvecArrete {
	private EOGlobalID tempsPartielID;
	private EOTempsPartiel currentTempsPartiel;
	public GestionRepriseTempsPlein(EOGlobalID tempsPartielID) {
        super("RepriseTempsPlein");
        this.tempsPartielID = tempsPartielID;
    }
	// actions
	public boolean peutValider() {
		return (currentReprise() != null && currentReprise().dRepriseTempsPlein() != null);
	}
	// méthodes protégées
	protected void preparerFenetre() {
		prepareComponent();	// 03/11/2010 Cet appel est nécessaire pour que l'archive soit chargée
		currentTempsPartiel = (EOTempsPartiel)editingContext().faultForGlobalID(tempsPartielID,editingContext());
		super.preparerFenetre();
		if (displayGroup().displayedObjects().count() > 0) {
			// il y a un seul élément de RepriseTempsPlein
			displayGroup().setSelectedObject(displayGroup().displayedObjects().objectAtIndex(0));
			super.displayGroupDidChangeSelection(displayGroup());
		} else {
			super.ajouter();
		}
	}
	 /** methode a surcharger pour retourner les objets affiches par le display group */
    protected NSArray fetcherObjets() {
	    	if (currentIndividu() != null && currentTempsPartiel != null) {
	    		return  EORepriseTempsPlein.findForTempsPartiel(editingContext(), currentTempsPartiel);
		} else {
			return null;
		}
    }
	/** traitements a ex&eacute;cuter suite a la creation d'un objet */
	protected void traitementsPourCreation() {
		if (currentReprise() != null) {
			currentReprise().initAvecIndividuEtTempsPartiel(currentIndividu(),currentTempsPartiel);
		}
	}
	protected boolean traitementsAvantValidation() {
		if (currentReprise() != null) {
			Arrete arrete = recupererArrete();
			if (arrete != null) {
				currentReprise().setDateArrete(arrete.date());
				currentReprise().setNoArrete(arrete.numero());
				if (arrete.temSignature() == null) {
					currentReprise().setTemConfirme(CocktailConstantes.FAUX);
				} else {
					currentReprise().setTemConfirme(arrete.temSignature());
				}
			}
			if (currentTempsPartiel.aReprisTempsPlein() == false) {
				currentTempsPartiel.setAReprisTempsPlein(true);
			}
		}
		return true;
	}

	/** traitements a executer avant la destruction d'un objet */
	protected boolean traitementsPourSuppression() {
		super.traitementsPourSuppression();
		currentTempsPartiel.setAReprisTempsPlein(false);
		return true;
	} 
	/** true si l'agent peut gerer les carrieres ou les contrats */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutGererCarrieres() || agent.peutGererContrats();
	}
	
	/**
	 * 
	 */
	protected EOTypeGestionArrete typeGestionArrete() {
		if (currentReprise() == null || currentReprise().dRepriseTempsPlein() == null) {
			return null;
		} else {
			// 13/10/2010 - on prend la même gestion des arrêtés que le temps partiel
			String nomTable = EOTempsPartiel.ENTITY_NAME;
			NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),currentEvenement().individu(),currentReprise().dRepriseTempsPlein(),currentReprise().dRepriseTempsPlein());
			// vérifie si fonctionnaire
			if (carrieres == null || carrieres.size() == 0) {
				// Vérifier si contractuel
				NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),currentEvenement().individu(), currentReprise().dRepriseTempsPlein(),currentReprise().dRepriseTempsPlein(),false);
				if (contrats == null || contrats.size() == 0) {
					return null;
				} else {
					EOTypeGestionArrete gestion = currentIndividu().rechercherTypeGestionArretePourPeriode(nomTable, nomTable, currentReprise().dRepriseTempsPlein(), currentReprise().dRepriseTempsPlein());
					return gestion;
				}
			} else {
				EOTypePopulation type = carrieres.get(0).toTypePopulation();
				return EOTypeGestionArrete.rechercherTypeGestionArretePourTypePopulation(editingContext(),nomTable,type);
			}
		}			
	}
	 protected Arrete arreteCourant() {
	 	if (currentReprise() == null) {
	 		return null;
	 	} else {
	 		return new Arrete(currentReprise().dateArrete(),currentReprise().noArrete(),currentReprise().temConfirme());
	 	}
	 }
	 protected Arrete arreteAnnulationCourant() {
	 	return null;
	 }
	 protected boolean supporteSignature() {
	 	return false;
	 }
	
	// méthodes privées
	private EORepriseTempsPlein currentReprise() {
		return (EORepriseTempsPlein)displayGroup().selectedObject();
	}

}

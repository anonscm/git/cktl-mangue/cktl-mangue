// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.modalites_services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.GregorianCalendar;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;
import org.cocktail.mangue.client.gui.modalites.TempsPartielView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.select.EnfantSelectCtrl;
import org.cocktail.mangue.client.select.MotifTempsPartielSelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.modele.nomenclatures.modalites.EOMotifTempsPartiel;
import org.cocktail.mangue.common.utilities.CocktailIcones;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOQuotite;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;
import org.cocktail.mangue.modele.mangue.modalites.EORepriseTempsPlein;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class TempsPartielCtrl {

	private EOEditingContext ec;
	public static String IMPRIMER_ARRETE = "ImpressionArrete";

	private ModalitesServicesCtrl ctrlParent;

	private TempsPartielView myView;
	private EOEnfant currentEnfant;
	private EOMotifTempsPartiel currentMotif;
	private EOTempsPartiel currentObject;
	private EORepriseTempsPlein currentReprise;
	private EOIndividu currentIndividu;

	public TempsPartielCtrl(ModalitesServicesCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new TempsPartielView();

		myView.getBtnGetMotif().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectMotif();}}
				);
		myView.getBtnDelMotif().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delMotif();}}
				);
		myView.getBtnGetEnfant().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectEnfant();}}
				);
		myView.getBtnDelEnfant().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delEnfant();}}
				);
		myView.getBtnImprimerReprise().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerArreteReprise();}}
				);
		myView.getBtnAddRepriseTempsPlein().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterReprise();}}
				);
		myView.getBtnDelRepriseTempsPlein().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerReprise();}}
				);

		CocktailUtilities.initPopupAvecListe(myView.getPopupQuotites(), EOQuotite.quotitesTempsPartiel(ec), false);
		setSaisieEnabled(false);

		myView.getTfDateFinReelle().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFinReelle()));
		myView.getTfDateFinReelle().addActionListener(new ActionListenerDateTextField(myView.getTfDateFinReelle()));
		myView.getTfDateArreteAnnulation().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArreteAnnulation()));
		myView.getTfDateArreteAnnulation().addActionListener(new ActionListenerDateTextField(myView.getTfDateArreteAnnulation()));

		CocktailUtilities.initTextField(myView.getTfDateReprise(), false, false);
		CocktailUtilities.initTextField(myView.getTfArreteReprise(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleMotif(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleEnfant(), false, false);

	}

	public EOEnfant getCurrentEnfant() {
		return currentEnfant;
	}

	public void setCurrentEnfant(EOEnfant currentEnfant) {
		this.currentEnfant = currentEnfant;
		myView.getTfLibelleEnfant().setText("");
		if (currentEnfant != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleEnfant(), currentEnfant.prenom()+" "+currentEnfant.nom());
		}
		updateInterface();
	}

	public EOMotifTempsPartiel getCurrentMotif() {
		return currentMotif;
	}

	public void setCurrentMotif(EOMotifTempsPartiel currentMotif) {
		this.currentMotif = currentMotif;
		myView.getTfLibelleMotif().setText("");
		if (currentMotif != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleMotif(), currentMotif.libelleCourt());
		}
		updateInterface();
	}

	public EOTempsPartiel getCurrentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOTempsPartiel currentObject) {
		this.currentObject = currentObject;
	}

	public EORepriseTempsPlein getCurrentReprise() {
		return currentReprise;
	}

	public void setCurrentReprise(EORepriseTempsPlein currentReprise) {
		this.currentReprise = currentReprise;
	}

	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}

	public JPanel getView() {
		return myView;
	}	
	public NSTimestamp getDateFin() {
		return getCurrentObject().dateFin();
	}

	/**
	 * 
	 */
	public void clearDatas() {

		setCurrentObject(null);
		setCurrentMotif(null);
		setCurrentEnfant(null);

		CocktailUtilities.viderTextField(myView.getTfLibelleMotif());
		CocktailUtilities.viderTextField(myView.getTfLibelleEnfant());
		CocktailUtilities.viderTextArea(myView.getTfCommentaires());
		CocktailUtilities.viderTextField(myView.getTfDateArreteAnnulation());
		CocktailUtilities.viderTextField(myView.getTfNoArreteAnnulation());
		CocktailUtilities.viderTextField(myView.getTfPeriodicite());
		CocktailUtilities.viderTextField(myView.getTfDateFinReelle());
		CocktailUtilities.viderTextField(myView.getTfQuotite());
		CocktailUtilities.viderTextField(myView.getTfDateReprise());
		CocktailUtilities.viderTextField(myView.getTfArreteReprise());

		myView.getCheckSurcotisation().setSelected(false);

	}

	/**
	 * 
	 */
	private void selectMotif() {
		EOMotifTempsPartiel motif = MotifTempsPartielSelectCtrl.sharedInstance(ec).getMotifTempsPartiel();
		if (motif != null) {
			currentMotif = motif;
			CocktailUtilities.setTextToField(myView.getTfLibelleMotif(), getCurrentMotif().libelle());

			if (!getCurrentMotif().estPourEleverEnfant() 
					&& ! getCurrentMotif().estPourEleverEnfantAdopte()
					&& ! getCurrentMotif().estPourDonnerSoins()) {
				setCurrentEnfant(null);
			}

			updateInterface();
		}

	}

	/**
	 * 
	 * @param dateDebut
	 * @param dateFin
	 */
	public void preparePeriodicite(NSTimestamp dateDebut, NSTimestamp dateFin) {		
		myView.getTfPeriodicite().setText(DateCtrl.calculerDureeEnMois(dateDebut, dateFin, true).toString());
	}

	private void delMotif() {
		setCurrentMotif(null);
	}

	/**
	 * 
	 */
	private void selectEnfant() {

		EOEnfant enfant = null;

		// Selection de l'enfant en fonction du motif
		if (getCurrentMotif().estPourEleverEnfant() || getCurrentMotif().estPourDonnerSoins()) {
			NSTimestamp dateReference = ctrlParent.getDateFin();
			if (dateReference == null) {
				EODialogs.runInformationDialog("ERREUR", "Veuillez renseigner la date de fin du temps partiel !");
				return;
			}
			enfant  = EnfantSelectCtrl.sharedInstance(ec).getEnfant(currentIndividu(), dateReference , new Integer(3));
		}
		else
			if (getCurrentMotif().estPourEleverEnfantAdopte())
				enfant  = EnfantSelectCtrl.sharedInstance(ec).getEnfantAdopte(currentIndividu);

		if (enfant != null) {
			setCurrentEnfant(enfant);
			CocktailUtilities.setTextToField(myView.getTfLibelleEnfant(), getCurrentEnfant().prenom()+" "+getCurrentEnfant().nom());
		}
	}
	/**
	 * 
	 */
	private void delEnfant() {
		setCurrentEnfant(null);
	}


	/**
	 * 
	 * @param newObject
	 */
	public void ajouter(EOTempsPartiel newObject) {		

		clearDatas();
		setCurrentObject(newObject);
		setCurrentIndividu(newObject.individu());
		updateInterface();

	}

	/**
	 * 
	 */
	public void renouveler() {

		setCurrentObject(EOTempsPartiel.renouveler(ec, getCurrentObject()));
		setCurrentIndividu(getCurrentObject().individu());
		updateDatas();
		updateInterface();

	}

	/**
	 * 
	 * @return
	 */
	public boolean valider() {

		try {
			getCurrentObject().setDateDebut(ctrlParent.getDateDebut());
			getCurrentObject().setDateFin(ctrlParent.getDateFin());
			getCurrentObject().setDateArrete(ctrlParent.getDateArrete());
			getCurrentObject().setNoArrete(ctrlParent.getNumeroArrete());

			getCurrentObject().setQuotite(CocktailUtilities.getBigDecimalFromFieldWithScale(myView.getTfQuotite(), 2));
			getCurrentObject().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));
			
			if (myView.getTfDateFinReelle().getText().length() > 0)
				getCurrentObject().setDFinExecution( CocktailUtilities.getDateFromField(myView.getTfDateFinReelle()));
			else
				getCurrentObject().setDFinExecution(getCurrentObject().dateFin());

			getCurrentObject().setPeriodicite(CocktailUtilities.getIntegerFromField(myView.getTfPeriodicite()));

			getCurrentObject().setDAnnulation(CocktailUtilities.getDateFromField(myView.getTfDateArreteAnnulation()));

			getCurrentObject().setNoArreteAnnulation(CocktailUtilities.getTextFromField(myView.getTfNoArreteAnnulation()));

			getCurrentObject().setMotifRelationship(getCurrentMotif());
			getCurrentObject().setEnfantRelationship(getCurrentEnfant());

			getCurrentObject().setTemSurcotisation((myView.getCheckSurcotisation().isSelected())?"O":"N");

			if (getCurrentObject().absence() != null) {
				if (getCurrentObject().dateDebut() != null) {
					getCurrentObject().absence().setDateDebut(getCurrentObject().dateDebut());
					getCurrentObject().absence().setDateFin(getCurrentObject().dateFin());
					getCurrentObject().absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(getCurrentObject().dateDebut(), getCurrentObject().dateFin(), true)));
				}
			}

			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 
	 */
	public void imprimerArrete() {

		NSArray<EOGlobalID>destinatairesGids = DestinatairesSelectCtrl.sharedInstance(ec).getDestinatairesGlobalIds();
		if (destinatairesGids  != null) {
			try {				
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class};
				Object[] parametres = new Object[]{ec.globalIDForObject(getCurrentObject()), destinatairesGids};
				UtilitairesImpression.imprimerSansDialogue(ec,"clientSideRequestImprimerArreteTempsPartiel",classeParametres,parametres,"ArreteTempsPartiel" + getCurrentObject().individu().noIndividu() ,"Impression de l'arrêté de temps Partiel");
			} catch (Exception e) {
				LogManager.logException(e);
				e.printStackTrace();
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	/**
	 * 
	 */
	private void ajouterReprise() {

		if (getCurrentObject() != null && currentReprise == null) {

			if ( getCurrentObject().dateArrete() == null || getCurrentObject().noArrete() == null ) {
				EODialogs.runInformationDialog("ATTENTION", "Pour une reprise à temps plein, la date et le numéro d'arrêté du temps partiel doivent être renseignés !");
				return;
			}

			if (SaisieRepriseTempsPleinCtrl.sharedInstance(ec).ajouter(getCurrentObject()) != null) {
				updateDatas();
			}
		}
		else {
			if (SaisieRepriseTempsPleinCtrl.sharedInstance(ec).modifier(currentReprise)) {
				updateDatas();
			}			
		}

		updateInterface();
	}

	/**
	 * 
	 */
	private void updateDatas() {
		
		setCurrentEnfant(getCurrentObject().enfant());
		setCurrentMotif(getCurrentObject().motif());

		CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentObject().quotite());			
		CocktailUtilities.setDateToField(myView.getTfDateFinReelle(), getCurrentObject().dFinExecution());
		CocktailUtilities.setDateToField(myView.getTfDateArreteAnnulation(), getCurrentObject().dAnnulation());
		CocktailUtilities.setTextToField(myView.getTfNoArreteAnnulation(), getCurrentObject().noArreteAnnulation());
		CocktailUtilities.setTextToArea(myView.getTfCommentaires(), getCurrentObject().commentaire());

		CocktailUtilities.setNumberToField(myView.getTfPeriodicite(), getCurrentObject().periodicite());

		myView.getCheckSurcotisation().setSelected(getCurrentObject().temSurcotisation().equals("O"));

		setCurrentReprise(EORepriseTempsPlein.rechercherReprisePourTempsPartiel(ec, getCurrentObject()));
		if (getCurrentReprise() != null) {
			if (getCurrentReprise().dRepriseTempsPlein() != null)
				myView.getTfDateReprise().setText(DateCtrl.dateToString(getCurrentReprise().dRepriseTempsPlein()));

			String libelleArrete = "";
			if (getCurrentReprise().noArrete() != null)
				libelleArrete = getCurrentReprise().noArrete();
			if (getCurrentReprise().dateArrete() != null)
				libelleArrete = libelleArrete + " du " + DateCtrl.dateToString(getCurrentReprise().dateArrete());
			myView.getTfArreteReprise().setText(libelleArrete);
		}		
		else {
			myView.getTfDateReprise().setText("");
			myView.getTfArreteReprise().setText("");			
		}
	}

	/**
	 * 
	 */
	private void supprimerReprise() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette reprise Temps Plein ?", "Oui", "Non"))		
			return;			

		try {
			getCurrentObject().setTemRepriseTempsPlein(CocktailConstantes.FAUX);
			getCurrentReprise().setTemValide(CocktailConstantes.FAUX);
			ec.saveChanges();
			setCurrentReprise(null);
			updateDatas();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	public void imprimerArreteReprise() {

		NSArray<EOGlobalID>destinatairesGids = DestinatairesSelectCtrl.sharedInstance(ec).getDestinatairesGlobalIds();
		if (destinatairesGids  != null) {
			try {				
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class};
				Object[] parametres = new Object[]{ec.globalIDForObject(currentReprise), destinatairesGids};
				UtilitairesImpression.imprimerSansDialogue(ec,"clientSideRequestImprimerArreteRepriseTempsPlein", classeParametres, parametres, "ArreteTempsPartiel" + getCurrentObject().individu().noIndividu() ,"Impression de l'arrêté de temps Partiel");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void supprimer() throws Exception {
		try {
			EOAbsences absence = EOAbsences.rechercherAbsencePourIndividuDateEtTypeAbsence(
					ec, currentIndividu(), getCurrentObject().dateDebut(), getCurrentObject().dateFin(), (EOTypeAbsence)NomenclatureFinder.findForCode(ec, EOTypeAbsence.ENTITY_NAME ,EOTypeAbsence.TYPE_TEMPS_PARTIEL));

			if (absence != null)
				absence.setEstValide(false);

			currentObject.setTemValide(CocktailConstantes.FAUX);
		}
		catch (Exception ex) {
			throw ex;
		}		
	}

	/**
	 * 
	 * @param yn
	 */
	public void setSaisieEnabled(boolean yn) {

		myView.getBtnGetMotif().setEnabled(yn);
		myView.getBtnDelMotif().setEnabled(yn);
		myView.getBtnGetEnfant().setEnabled(yn);
		myView.getBtnDelEnfant().setEnabled(yn);

		CocktailUtilities.initTextArea(myView.getTfCommentaires(), false, yn);
		myView.getPopupQuotites().setEnabled(yn);
		CocktailUtilities.initTextField(myView.getTfQuotite(), false, yn);
		CocktailUtilities.initTextField(myView.getTfDateFinReelle(), false, yn);
		CocktailUtilities.initTextField(myView.getTfPeriodicite(), false, yn);		
		CocktailUtilities.initTextField(myView.getTfDateArreteAnnulation(), false, yn);
		CocktailUtilities.initTextField(myView.getTfNoArreteAnnulation(), false, yn);

		myView.getCheckSurcotisation().setEnabled(yn && currentObject != null);// && !currentObject.estAncienRegime());
		myView.getBtnImprimerReprise().setEnabled(yn && currentReprise != null);

		updateInterface();
	}

	/**
	 * 
	 * @param modalite
	 */
	public void actualiser(EOModalitesService modalite) {

		LogManager.logDetail("\t TEMPS PARTIEL - Actualiser - DEBUT : " + (new GregorianCalendar()).getTimeInMillis()  + " Ms");

		clearDatas();
		setCurrentIndividu(modalite.toIndividu());
		setCurrentReprise(null);

		if (modalite != null) {

			setCurrentObject(EOTempsPartiel.findForKey(ec, modalite.modId()));

			if (getCurrentObject() != null) {

				setCurrentMotif(currentObject.motif());
				setCurrentEnfant(currentObject.enfant());

				updateDatas();
			}			
		}

		updateInterface();

		LogManager.logDetail("\t TEMPS PARTIEL - Actualiser - FIN : " + (new GregorianCalendar()).getTimeInMillis()  + " Ms");

	}

	/**
	 * 
	 * @return
	 */
	public boolean saisieEnabled() {
		return myView.getBtnGetMotif().isEnabled();
	}

	/**
	 * 
	 */
	public void updateInterface() {

		myView.getBtnGetEnfant().setEnabled(
				saisieEnabled()
				&& getCurrentMotif() != null 
				&& (getCurrentMotif().estPourEleverEnfant() 
						|| getCurrentMotif().estPourEleverEnfantAdopte()
						|| getCurrentMotif().estPourDonnerSoins() )
				);

		myView.getCheckSurcotisation().setEnabled(saisieEnabled());

		myView.getBtnDelEnfant().setEnabled(saisieEnabled() && getCurrentEnfant() != null);

		myView.getPanelRepriseTempsPlein().setVisible(!saisieEnabled());

		if (getCurrentReprise() != null)
			myView.getBtnAddRepriseTempsPlein().setIcon(CocktailIcones.ICON_UPDATE);
		else
			myView.getBtnAddRepriseTempsPlein().setIcon(CocktailIcones.ICON_ADD);

		myView.getBtnAddRepriseTempsPlein().setEnabled(getCurrentObject() != null && ctrlParent.peutGererModule());
		myView.getBtnDelRepriseTempsPlein().setEnabled(getCurrentObject() != null && getCurrentReprise() != null  && ctrlParent.peutGererModule());
		myView.getBtnImprimerReprise().setEnabled(getCurrentReprise() != null  && ctrlParent.peutGererModule());

		myView.getPopupQuotites().setVisible(false);
		myView.getTfQuotite().setVisible(true);

	}

	/**
	 * 
	 * @param myTextField
	 */
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateInterface();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}
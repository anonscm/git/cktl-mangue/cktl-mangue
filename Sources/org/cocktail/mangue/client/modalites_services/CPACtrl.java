// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.modalites_services;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.modalites.CPAView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.modalites.EOCessProgActivite;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eocontrol.EOEditingContext;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class CPACtrl {

	private EOEditingContext ec;
	private ModalitesServicesCtrl ctrlParent;
	private CPAView myView;
	private EOCessProgActivite currentObject;

	public CPACtrl(ModalitesServicesCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;
		myView = new CPAView();

		setSaisieEnabled(false);
	}

	public JPanel getView() {
		return myView;
	}	

	public EOCessProgActivite currentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOCessProgActivite currentObject) {
		this.currentObject = currentObject;
	}

	public void clearTextFields() {

		myView.getCheckCessation().setSelected(false);
		myView.getCheckQuotiteDegressive().setSelected(false);
		myView.getCheckSurcotisation().setSelected(false);

		myView.getTfCommentaires().setText("");

	}

	public void ajouter(EOCessProgActivite newObject) {		
		currentObject = newObject;
	}

	public void valider() {

		currentObject.setDateDebut(ctrlParent.getDateDebut());
		currentObject.setDateFin(ctrlParent.getDateFin());
		currentObject.setDateArrete(ctrlParent.getDateArrete());
		currentObject.setNoArrete(ctrlParent.getNumeroArrete());
		currentObject.setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));
		currentObject.setTemCta((myView.getCheckCessation().isSelected())?"O":"N");
		currentObject.setTemQuotDegressive((myView.getCheckQuotiteDegressive().isSelected())?"O":"N");
		currentObject.setTemSurcotisation((myView.getCheckSurcotisation().isSelected())?"O":"N");

		if (currentObject().dateDebut() != null) {
			currentObject.absence().setDateDebut(currentObject.dateDebut());
			currentObject.absence().setDateFin(currentObject.dateFin());
			currentObject.absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(currentObject.dateDebut(), currentObject.dateFin(), true)));
		}

	}

	public void supprimer() throws Exception {
		try {
			currentObject.setTemValide("N");
		}
		catch (Exception ex) {
			throw ex;
		}		
	}

	public void setSaisieEnabled(boolean yn) {

		myView.getCheckCessation().setEnabled(yn);
		myView.getCheckSurcotisation().setEnabled(yn);
		myView.getCheckQuotiteDegressive().setEnabled(yn);

		myView.getTfCommentaires().setEnabled(yn);
	}

	public void actualiser(EOModalitesService modalite) {

		clearTextFields();

		if (modalite != null) {

			setCurrentObject(EOCessProgActivite.findForKey(ec, modalite.modId()));

			if (currentObject() != null) {	

				CocktailUtilities.setTextToArea(myView.getTfCommentaires(), currentObject.commentaire());
				myView.getCheckSurcotisation().setSelected(currentObject().temSurcotisation().equals("O"));
				myView.getCheckCessation().setSelected(currentObject().temCta().equals("O"));
				myView.getCheckQuotiteDegressive().setSelected(currentObject().temQuotDegressive().equals("O"));

			}			
		}
	}

}

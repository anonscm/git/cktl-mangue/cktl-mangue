// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.modalites_services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.modalites.MiTempsTherapeutiqueView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOQuotite;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.modalites.EOMiTpsTherap;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class TempsPartielTherapeutiqueCtrl {

	private EOEditingContext ec;
	private ModalitesServicesCtrl ctrlParent;

	private MiTempsTherapeutiqueView myView;

	private EOMiTpsTherap currentObject;
	private NSArray destinatairesArrete;

	public TempsPartielTherapeutiqueCtrl(ModalitesServicesCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new MiTempsTherapeutiqueView();

		myView.setQuotites(EOQuotite.quotitesTempsPartiel(ec));
		
		setSaisieEnabled(false);
		
		myView.getTfDateComiteMedical().addFocusListener(new FocusListenerDateTextField(myView.getTfDateComiteMedical()));
		myView.getTfDateComiteMedical().addActionListener(new ActionListenerDateTextField(myView.getTfDateComiteMedical()));
		myView.getTfDateComiteMedicalSup().addFocusListener(new FocusListenerDateTextField(myView.getTfDateComiteMedicalSup()));
		myView.getTfDateComiteMedicalSup().addActionListener(new ActionListenerDateTextField(myView.getTfDateComiteMedicalSup()));
		myView.getTfDateComiteReforme().addFocusListener(new FocusListenerDateTextField(myView.getTfDateComiteReforme()));
		myView.getTfDateComiteReforme().addActionListener(new ActionListenerDateTextField(myView.getTfDateComiteReforme()));

	}

	protected EOMiTpsTherap getCurrentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOMiTpsTherap currentObject) {
		this.currentObject = currentObject;
	}

	public JPanel getView() {
		return myView;
	}	

	public void clearTextFields() {

		myView.getTfDateComiteMedical().setText("");
		myView.getTfDateComiteMedicalSup().setText("");
		myView.getTfDateComiteReforme().setText("");
		myView.getTfCommentaires().setText("");

	}
	
	public void ajouter(EOMiTpsTherap newObject) {
		currentObject = newObject;		
	}
	
	public void valider() {
								
		getCurrentObject().setQuotite(((EOQuotite)myView.getPopupQuotites().getSelectedItem()).numQuotite());

		getCurrentObject().setDateDebut(ctrlParent.getDateDebut());
		getCurrentObject().setDateFin(ctrlParent.getDateFin());
		getCurrentObject().setDateArrete(ctrlParent.getDateArrete());
		getCurrentObject().setNoArrete(ctrlParent.getNumeroArrete());

		getCurrentObject().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));
		getCurrentObject().setDComMedMtth(CocktailUtilities.getDateFromField(myView.getTfDateComiteMedical()));
		getCurrentObject().setDComMedSupMtth(CocktailUtilities.getDateFromField(myView.getTfDateComiteMedicalSup()));
		getCurrentObject().setDComReforme(CocktailUtilities.getDateFromField(myView.getTfDateComiteReforme()));
	
		if (getCurrentObject().absence() == null) {
			INomenclature typeAbsence = NomenclatureFinder.findForCode(ec, EOTypeAbsence.ENTITY_NAME ,EOTypeAbsence.TYPE_TEMPS_PARTIEL_THERAP);
			EOAbsences absence = EOAbsences.creer(ec, getCurrentObject().individu(), (EOTypeAbsence)typeAbsence);		
			getCurrentObject().setAbsenceRelationship(absence);
		}

		if (getCurrentObject().dateDebut() != null && getCurrentObject().dateFin() != null) {
			getCurrentObject().absence().setDateDebut(getCurrentObject().dateDebut());
			getCurrentObject().absence().setDateFin(getCurrentObject().dateFin());
			getCurrentObject().absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(getCurrentObject().dateDebut(), getCurrentObject().dateFin(), true)));
		}

	}
	
	
	public void imprimerArrete() {
		
		destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(ec).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
					NSMutableArray destinatairesGlobalIds = new NSMutableArray();
					for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(ec.globalIDForObject(e.nextElement())));
					Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class};
					Object[] parametres = new Object[]{ec.globalIDForObject(getCurrentObject()),destinatairesGlobalIds};
					UtilitairesImpression.imprimerSansDialogue(ec,"clientSideRequestImprimerArreteMiTempsTherapeutique",classeParametres,parametres,"ArreteTempsPartiel" + getCurrentObject().individu().noIndividu() ,"Impression de l'arrêté de TPT");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	public void supprimer() throws Exception {
		try {
			EOAbsences absence = EOAbsences.rechercherAbsencePourIndividuDateEtTypeAbsence(
					ec, getCurrentObject().individu(), getCurrentObject().dateDebut(), getCurrentObject().dateFin(), (EOTypeAbsence)NomenclatureFinder.findForCode(ec, EOTypeAbsence.ENTITY_NAME ,EOTypeAbsence.TYPE_TEMPS_PARTIEL_THERAP));

			if (absence != null)
				absence.setEstValide(false);
			getCurrentObject().setTemValide("N");
		}
		catch (Exception ex) {
			throw ex;
		}		
	}
	
	public void setSaisieEnabled(boolean yn) {

		myView.getPopupQuotites().setEnabled(yn);

		myView.getTfDateComiteMedical().setEnabled(yn);
		myView.getTfDateComiteMedicalSup().setEnabled(yn);
		myView.getTfDateComiteReforme().setEnabled(yn);

		myView.getTfCommentaires().setEnabled(yn);

	}

	/**
	 * 
	 * @param modalite
	 */
	public void actualiser(EOModalitesService modalite) {

		clearTextFields();

		if (modalite != null) {
			
			setCurrentObject(EOMiTpsTherap.findForKey(ec, modalite.modId()));
			
			if (getCurrentObject() != null) {	
				
				if (getCurrentObject().quotite() != null)
					myView.getPopupQuotites().setSelectedItem(EOQuotite.findForQuotite(ec, getCurrentObject().quotite()));
				
				CocktailUtilities.setDateToField(myView.getTfDateComiteMedical(), getCurrentObject().dComMedMtth());
				CocktailUtilities.setDateToField(myView.getTfDateComiteMedicalSup(), getCurrentObject().dComMedSupMtth());
				CocktailUtilities.setDateToField(myView.getTfDateComiteReforme(), getCurrentObject().dComReforme());
				CocktailUtilities.setTextToArea(myView.getTfCommentaires(), getCurrentObject().commentaire());
				
			}			
		}
	}
	
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}


}

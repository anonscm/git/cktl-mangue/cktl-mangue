package org.cocktail.mangue.client.modalites_services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.modalites.CRCTView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.select.NomenclatureSelectCodeLibelleCtrl;
import org.cocktail.mangue.client.select.OrigineDemandeSelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.modalites.EOOrigineDemande;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.modele.mangue.modalites.EOCrctDetail;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CRCTCtrl {

	private EOEditingContext edc;
	private CRCTView myView;
	private EOCnu currentCnu;
	private EODisplayGroup eodDetail;
	private EOOrigineDemande currentOrigine;
	private ModalitesServicesCtrl ctrlParent;
	private boolean saisieEnabled;
	private EOCrct currentObject;
	private EOCrctDetail currentDetail;
	private NSArray destinatairesArrete;

	public CRCTCtrl(ModalitesServicesCtrl ctrlParent, EOEditingContext edc) {

		setEdc(edc);
		
		this.ctrlParent = ctrlParent;
		this.eodDetail = new EODisplayGroup();

		myView = new CRCTView(eodDetail);

		myView.getBtnGetCnu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	selectCnu();}
		});
		myView.getBtnDelCnu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	setCurrentCnu(null);}
		});
		myView.getBtnGetOrigine().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	selectOrigine();}
		});
		myView.getBtnAddDetail().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	ajouterDetail();}
		});
		myView.getBtnDelDetail().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerDetail();}
		});

		myView.getTfDateArreteAnnulation().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArreteAnnulation()));
		myView.getTfDateArreteAnnulation().addActionListener(new ActionListenerDateTextField(myView.getTfDateArreteAnnulation()));

		myView.getCheckCongeFractionne().addActionListener(new ActionListenerCheckFractionne());
		myView.getCheckCongeFractionne().setSelected(false);
		// myView.getCheckCongeFractionne().setVisible(false);

		myView.getMyEOTable().addListener(new ListenerDetail());

		setSaisieEnabled(false);

	}



	public EOEditingContext getEdc() {
		return edc;
	}



	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}



	public EOCrctDetail getCurrentDetail() {
		return currentDetail;
	}
	public void setCurrentDetail(EOCrctDetail currentDetail) {
		this.currentDetail = currentDetail;
	}



	public boolean isSaisieEnabled() {
		return saisieEnabled;
	}

	public JPanel getView() {
		return myView;
	}

	public EOCnu currentCnu() {
		return currentCnu;
	}

	public void setCurrentCnu(EOCnu currentCnu) {
		this.currentCnu = currentCnu;
		CocktailUtilities.setTextToField(myView.getTfLibelleCnu(), "");
		if (currentCnu != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleCnu(),
					currentCnu.libelleLong());
		}
	}
	
	/**
	 * 
	 */
	public void imprimerArrete() {
		
		destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
					NSMutableArray destinatairesGlobalIds = new NSMutableArray();
					for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(getEdc().globalIDForObject(e.nextElement())));
					Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class};
					Object[] parametres = new Object[]{getEdc().globalIDForObject(getCurrentObject()),destinatairesGlobalIds};
					UtilitairesImpression.imprimerSansDialogue(getEdc(),"clientSideRequestImprimerArreteCrct",classeParametres,parametres,"ArreteCrct" + getCurrentObject().individu().noIndividu() ,"Impression de l'arrêté de CRCT");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}


	public EOOrigineDemande currentOrigine() {
		return currentOrigine;
	}

	/**
	 * 
	 * @param currentOrigine
	 */
	public void setCurrentOrigine(EOOrigineDemande currentOrigine) {
		this.currentOrigine = currentOrigine;
		CocktailUtilities.setTextToField(myView.getTfLibelleOrigine(), "");
		if (currentOrigine != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleOrigine(),
					currentOrigine.libelleLong());
			if (currentOrigine.estEtablissement())
				setCurrentCnu(null);
		}
	}

	/**
	 * 
	 * @return
	 */
	public EOCrct getCurrentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOCrct currentObject) {
		this.currentObject = currentObject;
	}

	public void clearTextFields() {

		setCurrentCnu(null);
		setCurrentOrigine(null);
		eodDetail.setObjectArray(new NSArray<EOCrctDetail>());
		myView.getMyEOTable().updateData();

		myView.getTfDateArreteAnnulation().setText("");
		myView.getTfCommentaires().setText("");
		myView.getTfNoArreteAnnulation().setText("");
		myView.getCheckCongeFractionne().setSelected(false);

	}

	private void selectOrigine() {
		EOOrigineDemande origine = OrigineDemandeSelectCtrl.sharedInstance(getEdc()).getOrigine();
		if (origine != null)
			setCurrentOrigine(origine);
	}

	private void selectCnu() {
		EOCnu cnu = (EOCnu)NomenclatureSelectCodeLibelleCtrl.sharedInstance(getEdc()).getObject(NomenclatureFinder.find(getEdc(), EOCnu.ENTITY_NAME, Nomenclature.SORT_ARRAY_CODE));
		if (cnu != null)
			setCurrentCnu(cnu);
	}

	public void ajouter(EOCrct newObject) {
		setCurrentObject(newObject);
	}

	/**
	 * 
	 * @param detail
	 */
	public void ajouterDetail() {
		EOCrctDetail myDetail = SaisieCrctDetailCtrl.sharedInstance(getEdc()).ajouter(getCurrentObject());
		if (myDetail != null) {
			eodDetail.insertObjectAtIndex(myDetail, 0);
			myView.getMyEOTable().updateData();
		}
		updateInteface();
	}
	public void supprimerDetail() {

		try {
			getEdc().deleteObject(getCurrentDetail());
			getEdc().saveChanges();
			updateDatas();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	public void valider() {

		getCurrentObject().setToCnuRelationship(currentCnu());
		getCurrentObject().setOrigineDemandeRelationship(currentOrigine());

		getCurrentObject().setDateDebut(ctrlParent.getDateDebut());
		getCurrentObject().setDateFin(ctrlParent.getDateFin());
		getCurrentObject().setDateArrete(ctrlParent.getDateArrete());
		getCurrentObject().setNoArrete(ctrlParent.getNumeroArrete());

		getCurrentObject().setDAnnulation(
				CocktailUtilities.getDateFromField(myView
						.getTfDateArreteAnnulation()));
		getCurrentObject().setNoArreteAnnulation(CocktailUtilities.getTextFromField(myView	.getTfNoArreteAnnulation()));
		getCurrentObject().setCommentaire(
				CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));

		getCurrentObject().setTemFractionnement(
				(myView.getCheckCongeFractionne().isSelected()) ? "O" : "N");

		currentObject.absence().setDateDebut(currentObject.dateDebut());
		currentObject.absence().setDateFin(currentObject.dateFin());
		currentObject.absence().setAbsDureeTotale(
				String.valueOf(DateCtrl.nbJoursEntre(currentObject.dateDebut(),
						currentObject.dateFin(), true)));

	}

	/**
	 * 
	 * @throws Exception
	 */
	public void supprimer() throws Exception {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer ce fractionnement ?", "Oui", "Non"))		
			return;			

		try {

			if (getCurrentObject().absence() != null) {
				getCurrentObject().absence().setTemValide(CocktailConstantes.FAUX);
			}

			getCurrentObject().setTemValide(CocktailConstantes.FAUX);
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * 
	 * @param yn
	 */
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInteface();

	}

	/**
	 * 
	 * @param modalite
	 */
	public void actualiser(EOModalitesService modalite) {

		clearTextFields();

		if (modalite != null) {

			setCurrentObject(EOCrct.findForKey(getEdc(), modalite.modId()));
			updateDatas();
		}
	}

	/**
	 * 
	 */
	protected void updateDatas() {


		if (getCurrentObject() != null) {

			setCurrentCnu(getCurrentObject().toCnu());
			setCurrentOrigine(getCurrentObject().origineDemande());

			CocktailUtilities.setDateToField(myView.getTfDateArreteAnnulation(),getCurrentObject().dAnnulation());
			CocktailUtilities.setTextToField(myView.getTfNoArreteAnnulation(),	getCurrentObject().noArreteAnnulation());
			CocktailUtilities.setTextToArea(myView.getTfCommentaires(),	getCurrentObject().commentaire());

			myView.getCheckCongeFractionne().setSelected(getCurrentObject().estFractionne());

			eodDetail.setObjectArray(EOCrctDetail.findForCrct(getEdc(), getCurrentObject()));
			myView.getMyEOTable().updateData();
		}
		updateInteface();
	}

	/**
	 * 
	 */
	protected void updateInteface() {

		myView.getBtnGetCnu().setEnabled(isSaisieEnabled());
		myView.getBtnDelCnu().setEnabled(isSaisieEnabled() && currentCnu != null);
		myView.getBtnGetOrigine().setEnabled(isSaisieEnabled());
		myView.getTfCommentaires().setEnabled(isSaisieEnabled());

		myView.getCheckCongeFractionne().setEnabled(isSaisieEnabled());
		myView.getTfDateArreteAnnulation().setEnabled(isSaisieEnabled());
		myView.getTfNoArreteAnnulation().setEnabled(isSaisieEnabled());

		myView.getBtnAddDetail().setEnabled(isSaisieEnabled() && getCurrentObject() != null && myView.getCheckCongeFractionne().isSelected());
		myView.getBtnDelDetail().setEnabled(isSaisieEnabled() && getCurrentDetail() != null);
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerDetail implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentDetail((EOCrctDetail)eodDetail.selectedObject());
		}
	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))
			return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate)) {
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide",
					"La date saisie n'est pas valide !");
		} else {
			myTextField.setText(myDate);
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ActionListenerCheckFractionne implements ActionListener {
		private ActionListenerCheckFractionne() {
		}
		public void actionPerformed(ActionEvent e) {

			if (myView.getCheckCongeFractionne().isSelected() == false) {

				if (eodDetail.displayedObjects().size() > 0) {

					if(!EODialogs.runConfirmOperationDialog("Attention", 
							"Les périodes de fractionnement vont être supprimées.\n Continuer ?", "Oui", "Non"))		
						return;			

					for (EOCrctDetail myDetail : (NSArray<EOCrctDetail>)eodDetail.allObjects()) {

						getEdc().deleteObject(myDetail);

					}
				}
			}

			updateInteface();
		}
	}

	private class ActionListenerDateTextField implements ActionListener {
		private JTextField myTextField;

		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}

		public void actionPerformed(ActionEvent e) {
			dateHasChanged(myTextField);
		}
	}

	private class FocusListenerDateTextField implements FocusListener {
		private JTextField myTextField;

		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}

		public void focusGained(FocusEvent e) {
		}

		public void focusLost(FocusEvent e) {
			dateHasChanged(myTextField);
		}
	}

}

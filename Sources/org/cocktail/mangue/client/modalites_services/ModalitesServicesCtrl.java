package org.cocktail.mangue.client.modalites_services;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.modalites.ModalitesServiceView;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;
import org.cocktail.mangue.modele.mangue.modalites.EOCessProgActivite;
import org.cocktail.mangue.modele.mangue.modalites.EOCongeSsTraitement;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.modele.mangue.modalites.EODelegation;
import org.cocktail.mangue.modele.mangue.modalites.EOMiTpsTherap;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class ModalitesServicesCtrl {

	private static ModalitesServicesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.TRUE;
	private EOEditingContext edc;
	private ModalitesServiceView myView;

	private ListenerModalites listenerModalites = new ListenerModalites();

	private boolean modeCreation, saisieEnabled;
	private EODisplayGroup eod;
	private EOModalitesService currentModalite;
	private EOIndividu currentIndividu;
	private EOAgentPersonnel currentUtilisateur;
	private boolean peutGererModule;
	private TempsPartielCtrl ctrlTempsPartiel;
	private TempsPartielTherapeutiqueCtrl ctrlTpt;
	private CRCTCtrl ctrlCrct;
	private DelegationCtrl ctrlDelegation;
	private CongeSansTraitementCtrl ctrlCst;
	private CPACtrl ctrlCpa;

	private PopupTypeModaliteListener listenerTypeModalite = new PopupTypeModaliteListener();

	public ModalitesServicesCtrl(EOEditingContext editingContext) {

		edc = editingContext;

		eod = new EODisplayGroup();
		myView = new ModalitesServiceView(null, MODE_MODAL.booleanValue(),eod);

		myView.getMyEOTable().addListener(listenerModalites);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnDupliquer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {renouveler();}}
				);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
				);
		myView.getBtnImprimerArrete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerArrete();}}
				);
		myView.getBtnClose().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {myView.setVisible(false);}}
				);

		ctrlTempsPartiel = new TempsPartielCtrl(this, edc);
		ctrlTpt = new TempsPartielTherapeutiqueCtrl(this, edc);
		ctrlCrct = new CRCTCtrl(this, edc);
		ctrlDelegation = new DelegationCtrl(this, edc);
		ctrlCst = new CongeSansTraitementCtrl(this, edc);
		ctrlCpa = new CPACtrl(this, edc);

		// Initialisation des layouts
		myView.getSwapViewModalites().add("VIDE",new JPanel(new BorderLayout()));
		myView.getSwapViewModalites().add(EOTypeAbsence.TYPE_TEMPS_PARTIEL, ctrlTempsPartiel.getView());
		myView.getSwapViewModalites().add(EOTypeAbsence.TYPE_TEMPS_PARTIEL_THERAP, ctrlTpt.getView());
		myView.getSwapViewModalites().add(EOTypeAbsence.TYPE_CRCT, ctrlCrct.getView());
		myView.getSwapViewModalites().add(EOTypeAbsence.TYPE_DELEGATION,ctrlDelegation.getView());
		myView.getSwapViewModalites().add(EOTypeAbsence.TYPE_CST,ctrlCst.getView());
		myView.getSwapViewModalites().add(EOTypeAbsence.TYPE_CPA,ctrlCpa.getView());

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateArrete().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArrete()));
		myView.getTfDateArrete().addActionListener(new ActionListenerDateTextField(myView.getTfDateArrete()));

		setSaisieEnabled(false);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		myView.getBtnImprimerArrete().setEnabled(false);
		myView.getPopupTypes().addActionListener(listenerTypeModalite);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		myView.getCheckSigne().setVisible(false);
		setSaisieEnabled(false);

	}

	public static ModalitesServicesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ModalitesServicesCtrl(editingContext);
		return sharedInstance;
	}

	public void open(EOIndividu individu)	{
		clearDatas();
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}


	public EOModalitesService getCurrentModalite() {
		return currentModalite;
	}

	public void setCurrentModalite(EOModalitesService currentModalite) {
		this.currentModalite = currentModalite;
		updateDatas();
	}
	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public EOAgentPersonnel currentUtilisateur() {
		return currentUtilisateur;
	}

	public boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;

		setPeutGererModule(currentUtilisateur.peutGererCarrieres());

		// Gestion des droits
		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());
		myView.getBtnDupliquer().setVisible(peutGererModule());
		myView.getBtnImprimerArrete().setVisible(peutGererModule());

	}

	public void employeHasChanged(NSNotification  notification) {
		clearDatas();
		if (notification != null && notification.object() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
	}


	public void actualiser() {

		eod.setObjectArray(EOModalitesService.findForIndividu(edc, currentIndividu));
		myView.getMyEOTable().updateData();
		updateUI();

	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewModalites() {
		return myView.getViewModalites();
	}

	public NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	public void setDateFin(NSTimestamp dateFin) {
		CocktailUtilities.setDateToField(myView.getTfDateFin(), dateFin);
	}
	public NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}
	public NSTimestamp getDateArrete() {
		return CocktailUtilities.getDateFromField(myView.getTfDateArrete());
	}
	public String getNumeroArrete() {
		return CocktailUtilities.getTextFromField(myView.getTfNoArrete());
	}

	public void toFront() {
		myView.toFront();
	}

	private void ajouter() {

		modeCreation = true;

		clearDatas();

		myView.getBtnAjouter().setEnabled(false);
		myView.getBtnModifier().setEnabled(false);
		myView.getBtnSupprimer().setEnabled(false);
		myView.getMyEOTable().setEnabled(false);
		myView.getLblTypeModalite().setForeground(Color.RED);
		myView.getPopupTypes().setEnabled(true);

		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);

		myView.getBtnAnnuler().setEnabled(true);
		((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), "VIDE");

		myView.getBtnImprimerArrete().setEnabled(false);

	}

	private void modifier() {

		modeCreation = false;
		setSaisieEnabled(true);
		updateUI();
	}


	private void renouveler() {

		modeCreation = true;

		CocktailUtilities.setDateToField(myView.getTfDateDebut(), DateCtrl.jourSuivant(currentModalite.dateFin()));
		CocktailUtilities.setTextToField(myView.getTfDateFin(), "");
		CocktailUtilities.setTextToField(myView.getTfNoArrete(), "");
		CocktailUtilities.setTextToField(myView.getTfDateArrete(), "");

		ctrlTempsPartiel.renouveler();

		setSaisieEnabled(true);
		updateUI();

	}


	/**
	 * 
	 */
	private void imprimerArrete() {

		try {
			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlTempsPartiel.imprimerArrete();break;
			case 2 : ctrlTpt.imprimerArrete();break;
			case 3 : ctrlCrct.imprimerArrete();break;
			}			
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			edc.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
		}

	}


	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cet enregistrement ?", "Oui", "Non"))		
			return;			

		try {

			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlTempsPartiel.supprimer();break;
			case 2 : ctrlTpt.supprimer();break;
			case 3 : ctrlCrct.supprimer();break;
			case 4 : ctrlDelegation.supprimer();break;
			case 5 : ctrlCst.supprimer();break;
			case 6 : ctrlCpa.supprimer();break;
			}

			edc.saveChanges();
			actualiser();
			toFront();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			edc.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
		}
	}


	/**
	 * 
	 * @throws ValidationException
	 */
	private void traitementsAvantValidation() throws ValidationException {

		if (getDateDebut() != null) {
			NSArray<EOChangementPosition> positions = EOChangementPosition.rechercherChangementsPourPeriode(edc, getCurrentIndividu(), getDateDebut(), getDateFin());
			NSArray<EOPasse> passesEAS = EOPasse.rechercherPassesEASPourPeriode(edc, getCurrentIndividu(), getDateDebut(), getDateFin());
			for (EOChangementPosition myPosition : positions) {
				if (passesEAS.size() == 0 && myPosition.toPosition().estEnActivite() == false)
					throw new ValidationException ("L'agent doit être en activité sur la période allant du " + DateCtrl.dateToString(getDateDebut()) + " au " + DateCtrl.dateToString(getDateFin()) + " !");

			}
		}

	}

	private void valider() {

		try {

			traitementsAvantValidation();

			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlTempsPartiel.valider();break;
			case 2 : ctrlTpt.valider();break;
			case 3 : ctrlCrct.valider();break;
			case 4 : ctrlDelegation.valider();break;
			case 5 : ctrlCst.valider();break;
			case 6 : ctrlCpa.valider();break;
			}
			edc.saveChanges();

			setSaisieEnabled(false);

			if (modeCreation) {
				actualiser();
			}
			else {
				edc.invalidateObjectsWithGlobalIDs(new NSArray(edc.globalIDForObject(getCurrentModalite())));
				myView.getMyEOTable().updateUI();
			}

			NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);
			updateUI();

		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			myView.toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {

		edc.revert();
		ServerProxy.clientSideRequestRevert(edc);
		setSaisieEnabled(false);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);
		listenerModalites.onSelectionChanged();

		updateUI();

	}

	public void nettoyerChamps(NSNotification notification) {

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
		setCurrentIndividu(null);
		updateUI();
	}

	private void clearDatas() {

		myView.getPopupTypes().setSelectedIndex(0);

		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfDateArrete());
		CocktailUtilities.viderTextField(myView.getTfNoArrete());

		ctrlTempsPartiel.clearDatas();
		ctrlTpt.clearTextFields();
		ctrlCrct.clearTextFields();
		ctrlDelegation.clearDatas();
		ctrlCst.clearTextFields();
		ctrlCpa.clearTextFields();

	}

	private void updateDatas() {

		clearDatas();

		if (getCurrentModalite() != null) {

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentModalite().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentModalite().dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentModalite().dateArrete());
			CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentModalite().noArrete());

			// Swap view
			myView.getPopupTypes().removeActionListener(listenerTypeModalite);
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), getCurrentModalite().modType());				

			myView.getBtnImprimerArrete().setEnabled(false);

			if (getCurrentModalite().estTempsPartiel()) {
				myView.getPopupTypes().setSelectedIndex(1);		
				ctrlTempsPartiel.actualiser(getCurrentModalite());
				setDateFin(ctrlTempsPartiel.getDateFin());
				myView.getBtnImprimerArrete().setEnabled(true);
			}
			if (currentModalite.estTempsPartielTherapeutique()) {
				myView.getPopupTypes().setSelectedIndex(2);			
				ctrlTpt.actualiser(getCurrentModalite());
				myView.getBtnImprimerArrete().setEnabled(true);
			}
			if (EOTypeAbsence.TYPE_CRCT.equals(currentModalite.modType())) {
				myView.getPopupTypes().setSelectedIndex(3);			
				ctrlCrct.actualiser(currentModalite);
			}
			if (EOTypeAbsence.TYPE_DELEGATION.equals(currentModalite.modType())) {
				myView.getPopupTypes().setSelectedIndex(4);			
				ctrlDelegation.actualiser(currentModalite);
			}
			if (EOTypeAbsence.TYPE_CST.equals(currentModalite.modType())) {
				myView.getPopupTypes().setSelectedIndex(5);			
				ctrlCst.actualiser(currentModalite);
			}
			if (EOTypeAbsence.TYPE_CPA.equals(currentModalite.modType())) {
				myView.getPopupTypes().setSelectedIndex(6);			
				ctrlCpa.actualiser(currentModalite);
			}

			myView.getPopupTypes().addActionListener(listenerTypeModalite);
		}

		updateUI();

	}

	private class ListenerModalites implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentModalite() != null && peutGererModule())
				modifier();
		}
		public void onSelectionChanged() {

			clearDatas();
			setCurrentModalite((EOModalitesService)eod.selectedObject());

		}
	}

	private class PopupTypeModaliteListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			if (myView.getPopupTypes().getSelectedIndex() > 0) {

				setSaisieEnabled(true);
				myView.getLblTypeModalite().setForeground(Color.BLACK);

				switch (myView.getPopupTypes().getSelectedIndex()) {
				case 1 : gererTempsPartiel();break;
				case 2 : gererMiTempsTherapeutique();break;
				case 3 : gererCrct();break;
				case 4 : gererDelegation();break;
				case 5 : gererCongeSansTraitement();break;
				case 6 : gererCpa();break;
				}

				myView.getPopupTypes().setEnabled(false);
			}
		}
	}

	private void setSaisieEnabled(boolean yn) {

		saisieEnabled = yn;

		switch (myView.getPopupTypes().getSelectedIndex()) {
		case 1 : ctrlTempsPartiel.setSaisieEnabled(yn);break;
		case 2 : ctrlTpt.setSaisieEnabled(yn);break;
		case 3 : ctrlCrct.setSaisieEnabled(yn);break;
		case 4 : ctrlDelegation.setSaisieEnabled(yn);break;
		case 5 : ctrlCst.setSaisieEnabled(yn);break;
		case 6 : ctrlCpa.setSaisieEnabled(yn);break;
		}

		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		else
			NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);

		updateUI();

	}

	private void gererTempsPartiel() {
		if (modeCreation) {
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), EOTypeAbsence.TYPE_TEMPS_PARTIEL);				
			EOTempsPartiel newConge= EOTempsPartiel.creer(edc, currentIndividu);
			ctrlTempsPartiel.ajouter(newConge);			
		}		
	}
	private void gererMiTempsTherapeutique() {
		if (modeCreation) {
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), EOTypeAbsence.TYPE_TEMPS_PARTIEL_THERAP);				
			EOMiTpsTherap newConge= EOMiTpsTherap.creer(edc, currentIndividu);
			ctrlTpt.ajouter(newConge);			
		}		
	}
	private void gererCrct() {
		if (modeCreation) {
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), EOTypeAbsence.TYPE_CRCT);				
			EOCrct newConge= EOCrct.creer(edc, currentIndividu);

			ctrlCrct.ajouter(newConge);			
		}		
	}
	private void gererDelegation() {
		if (modeCreation) {
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), EOTypeAbsence.TYPE_DELEGATION);				
			EODelegation newConge= EODelegation.creer(edc, currentIndividu);
			ctrlDelegation.ajouter(newConge);			
		}		
	}
	private void gererCongeSansTraitement() {
		if (modeCreation) {
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), EOTypeAbsence.TYPE_CST);				
			EOCongeSsTraitement newConge= EOCongeSsTraitement.creer(edc, currentIndividu);
			ctrlCst.ajouter(newConge);			
		}		
	}
	private void gererCpa() {
		if (modeCreation) {			
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), EOTypeAbsence.TYPE_CPA);				
			EOCessProgActivite newConge= EOCessProgActivite.creer(edc, currentIndividu);
			ctrlCpa.ajouter(newConge);
		}		
	}


	private boolean modeCreation() {
		return modeCreation;
	}
	private boolean isSaisieEnabled() {
		return saisieEnabled;
	}

	private void updateUI() {

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null);
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null && getCurrentModalite() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null && getCurrentModalite() != null);
		myView.getBtnDupliquer().setEnabled(!isSaisieEnabled() && getCurrentIndividu() != null && getCurrentModalite() != null && getCurrentModalite().estTempsPartiel());

		myView.getMyEOTable().setEnabled(!isSaisieEnabled());		

		// On ne peut imprimer un arrete que pour les temps partiels ou les mi temps therapeutiques
		myView.getBtnImprimerArrete().setEnabled(
				(
						getCurrentModalite() != null &&
						(getCurrentModalite().estTempsPartiel()
								||
								getCurrentModalite().estTempsPartielTherapeutique()
								||
								getCurrentModalite().estCrct())
						)
						&& !isSaisieEnabled() 
						&& myView.getTfDateArrete().getText().length() > 0);


		ctrlTempsPartiel.updateInterface();
		myView.getLblTypeModalite().setForeground(Color.BLACK);

		myView.getPopupTypes().setEnabled(modeCreation() && isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateArrete(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfNoArrete(), false, isSaisieEnabled());

	}

	/**
	 * 
	 * @param myTextField
	 */
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.modalites_services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.modalites.SaisieRepriseTempsPleinView;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.modalites.EORepriseTempsPlein;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisieRepriseTempsPleinCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieRepriseTempsPleinCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieRepriseTempsPleinView myView;
	
	private EOTempsPartiel currentTempsPartiel;
	private EORepriseTempsPlein currentReprise;

	public SaisieRepriseTempsPleinCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new SaisieRepriseTempsPleinView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);

		myView.getTfDateReprise().addFocusListener(new FocusListenerDateReprise());
		myView.getTfDateReprise().addActionListener(new ActionListenerDateReprise());
		myView.getTfDateArrete().addFocusListener(new FocusListenerDateArreteReprise());
		myView.getTfDateArrete().addActionListener(new ActionListenerDateArreteReprise());

	}

	public static SaisieRepriseTempsPleinCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieRepriseTempsPleinCtrl(editingContext);
		return sharedInstance;
	}

	private void clearTextFields()	{

		myView.getTfDateReprise().setText("");
		myView.getTfDateArrete().setText("");
		myView.getTfNoArrete().setText("");

	}

	public EORepriseTempsPlein ajouter(EOTempsPartiel tempsPartiel)	{

		clearTextFields();

		currentTempsPartiel = tempsPartiel;
		
		myView.setTitle("REPRISE TEMPS PLEIN ( " + currentTempsPartiel.individu().identitePrenomFirst() + ")");

		currentReprise = EORepriseTempsPlein.creer(ec, tempsPartiel, true);

		updateData();
		myView.setVisible(true);
		return currentReprise;
	}

	public boolean modifier(EORepriseTempsPlein reprise) {

		currentReprise = reprise;
		currentTempsPartiel = currentReprise.tempsPartiel();

		myView.setTitle("REPRISE TEMPS PLEIN ( " + currentTempsPartiel.individu().identitePrenomFirst() + ")");

		updateData();
		myView.setVisible(true);
		return currentReprise != null;
	}

	private void updateData() {
		
		clearTextFields();

		if (currentReprise.dRepriseTempsPlein() != null)
			myView.getTfDateReprise().setText(DateCtrl.dateToString(currentReprise.dRepriseTempsPlein()));

		if (currentReprise.dateArrete() != null)
			myView.getTfDateArrete().setText(DateCtrl.dateToString(currentReprise.dateArrete()));

		if (currentReprise.noArrete() != null)
			myView.getTfNoArrete().setText(currentReprise.noArrete());

		if (currentReprise.commentaire() != null)
			myView.getTfCommentaires().setText(currentReprise.commentaire());

	}
	
	private void valider()  {

		try {			

			if (myView.getTfDateReprise().getText().length() > 0)
				currentReprise.setDRepriseTempsPlein(DateCtrl.stringToDate(myView.getTfDateReprise().getText()));

			if (myView.getTfDateArrete().getText().length() > 0)
				currentReprise.setDateArrete(DateCtrl.stringToDate(myView.getTfDateArrete().getText()));

			if (myView.getTfNoArrete().getText().length() > 0)
				currentReprise.setNoArrete(myView.getTfNoArrete().getText());

			if (myView.getTfCommentaires().getText().length() > 0)
				currentReprise.setCommentaire(myView.getTfCommentaires().getText());
			else
				currentReprise.setCommentaire(null);
				
			currentTempsPartiel.setTemRepriseTempsPlein("O");
					
			
			
			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		ec.revert();
		currentReprise = null;
		myView.setVisible(false);
	}


	private class ActionListenerDateReprise implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateReprise().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateReprise().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateReprise().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDateReprise().setText(myDate);
			}
		}
	}
	private class FocusListenerDateReprise implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateReprise().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateReprise().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateReprise().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDateReprise().setText(myDate);
			}
		}
	}



	private class ActionListenerDateArreteReprise implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateArrete().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateArrete().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateArrete().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfDateArrete().setText(myDate);
		}
	}
	private class FocusListenerDateArreteReprise implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateArrete().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateArrete().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateArrete().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfDateArrete().setText(myDate);
		}
	}

}

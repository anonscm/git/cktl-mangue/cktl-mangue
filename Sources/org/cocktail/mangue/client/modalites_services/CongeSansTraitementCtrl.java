// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.modalites_services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.modalites.CongeSansTraitementView;
import org.cocktail.mangue.client.select.MotifCgStagiaireSelectCtrl;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOMotifCgStagiaire;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.modalites.EOCongeSsTraitement;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eocontrol.EOEditingContext;

public class CongeSansTraitementCtrl {

	private EOEditingContext ec;
	private ModalitesServicesCtrl ctrlParent;
	private CongeSansTraitementView myView;
	private EOMotifCgStagiaire currentMotif;
	private EOCongeSsTraitement currentObject;
	private EOIndividu currentIndividu;

	public CongeSansTraitementCtrl(ModalitesServicesCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new CongeSansTraitementView();

		myView.getBtnGetMotif().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectMotif();}}
		);

		setSaisieEnabled(false);
	}

	public JPanel getView() {
		return myView;
	}	
	public EOMotifCgStagiaire currentMotif() {
		return currentMotif;
	}

	public void setCurrentMotif(EOMotifCgStagiaire currentMotif) {
		this.currentMotif = currentMotif;
		myView.getTfLibelleMotif().setText("");
		if (currentMotif != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleMotif(), currentMotif.llMotifCgStagiaire());			
		}
	}

	public EOCongeSsTraitement currentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOCongeSsTraitement currentObject) {
		this.currentObject = currentObject;
	}

	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}

	public void clearTextFields() {
		setCurrentMotif(null);
		myView.getTfCommentaires().setText("");
	}

	/**
	 * 
	 */
	private void selectMotif() {
		EOMotifCgStagiaire motif = MotifCgStagiaireSelectCtrl.sharedInstance(ec).getMotif();
		if (motif != null)
			setCurrentMotif(motif);
	}

	public void ajouter(EOCongeSsTraitement newObject) {		
		currentObject = newObject;
		currentIndividu = newObject.individu();
	}
	
	public void valider() {
						
		currentObject().setDateDebut(ctrlParent.getDateDebut());
		currentObject().setDateFin(ctrlParent.getDateFin());
		currentObject().setDateArrete(ctrlParent.getDateArrete());
		currentObject().setNoArrete(ctrlParent.getNumeroArrete());
		currentObject().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));
		currentObject().setMotifRelationship(currentMotif());

		currentObject.absence().setDateDebut(currentObject.dateDebut());
		currentObject.absence().setDateFin(currentObject.dateFin());
		currentObject.absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(currentObject.dateDebut(), currentObject.dateFin(), true)));

	}
	
	public void supprimer() throws Exception {
		try {
			currentObject().setTemValide(CocktailConstantes.FAUX);
		}
		catch (Exception ex) {
			throw ex;
		}		
	}

	public void setSaisieEnabled(boolean yn) {

		myView.getBtnGetMotif().setEnabled(yn);
		myView.getTfCommentaires().setEnabled(yn);
		myView.getTfLibelleMotif().setEnabled(false);

	}

	public void actualiser(EOModalitesService modalite) {

		setCurrentIndividu(modalite.toIndividu());
		clearTextFields();

		if (modalite != null) {
			
			setCurrentObject(EOCongeSsTraitement.findForKey(ec, modalite.modId()));			
			if (currentObject() != null) {		
				setCurrentMotif(currentObject().motif());
				CocktailUtilities.setTextToArea(myView.getTfCommentaires(), currentObject.commentaire());
			}			
		}
	}

}

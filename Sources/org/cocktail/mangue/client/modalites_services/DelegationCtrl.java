// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.modalites_services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.modalites.DelegationView;
import org.cocktail.mangue.client.select.TypeModDelegationSelectCtrl;
import org.cocktail.mangue.client.select.TypeMotDelegationSelectCtrl;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.modalites.EOTypeModDelegation;
import org.cocktail.mangue.common.modele.nomenclatures.modalites.EOTypeMotDelegation;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.modalites.EODelegation;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class DelegationCtrl {

	private EOEditingContext ec;
	private ModalitesServicesCtrl ctrlParent;
	
	private DelegationView myView;
	private EODelegation currentObject;
	private EOTypeModDelegation currentMode; 
	private EOTypeMotDelegation currentMotif;
	private EORne currentUai;
	
	private boolean saisieEnabled;

	/**
	 * 
	 * @param ctrlParent
	 * @param editingContext
	 */
	public DelegationCtrl(ModalitesServicesCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;
		
		myView = new DelegationView();

		myView.getBtnGetMotif().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectMotif();}}
		);
		myView.getBtnGetMode().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectMode();}}
		);
		myView.getBtnGetUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectUai();}}
		);
		myView.getBtnDelUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delUai();}}
		);

		setSaisieEnabled(false);
	
		myView.getTfDateConvention().addFocusListener(new FocusListenerDateTextField(myView.getTfDateConvention()));
		myView.getTfDateConvention().addActionListener(new ActionListenerDateTextField(myView.getTfDateConvention()));
		CocktailUtilities.initTextField(myView.getTfLibelleMotif(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleUai(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleMode(), false, false);
	}	
	
	public EODelegation getCurrentObject() {
		return currentObject;
	}

	public void setCurrentObject(EODelegation currentObject) {
		this.currentObject = currentObject;
	}

	public EOTypeModDelegation getCurrentMode() {
		return currentMode;
	}

	public void setCurrentMode(EOTypeModDelegation currentMode) {
		this.currentMode = currentMode;
		myView.getTfLibelleMode().setText("");
		if (currentMode != null)
			myView.getTfLibelleMode().setText(currentMode.libelleLong());
	}

	public EOTypeMotDelegation getCurrentMotif() {
		return currentMotif;
	}

	public void setCurrentMotif(EOTypeMotDelegation currentMotif) {
		this.currentMotif = currentMotif;
		myView.getTfLibelleMotif().setText("");
		if (currentMotif != null)
			myView.getTfLibelleMotif().setText(currentMotif.libelleLong());
	}

	public EORne getCurrentUai() {
		return currentUai;
	}

	public void setCurrentUai(EORne currentUai) {
		this.currentUai = currentUai;
		myView.getTfLibelleUai().setText("");
		if (currentUai != null) {
			myView.getTfLibelleUai().setText(currentUai.libelleLong());
		}
	}

	public JPanel getView() {
		return myView;
	}	

	public void clearDatas() {

		currentMotif = null;
		currentMode = null;
		currentUai = null;
		
		CocktailUtilities.viderTextField(myView.getTfLibelleMotif());
		CocktailUtilities.viderTextField(myView.getTfLibelleMode());
		CocktailUtilities.viderTextField(myView.getTfLibelleUai());

		CocktailUtilities.viderTextArea(myView.getTfCommentaires());
		CocktailUtilities.viderTextArea(myView.getTfLieu());

		myView.getCheckMontantAnnuel().setSelected(false);
		CocktailUtilities.viderTextField(myView.getTfQuotite());
		CocktailUtilities.viderTextField(myView.getTfDateConvention());
		CocktailUtilities.viderTextField(myView.getTfMontantConvention());

	}
	
	private void selectMotif() {
		EOTypeMotDelegation motif = TypeMotDelegationSelectCtrl.sharedInstance(ec).getType();
		if (motif != null) {
			setCurrentMotif(motif);
		}
	}
	
	private void selectUai() {
		EORne uai = (EORne)UAISelectCtrl.sharedInstance(ec).getObject();
		if (uai != null) {
			setCurrentUai(uai);
		}
	}
	
	private void delUai() {
		setCurrentUai(null);
	}

	private void selectMode() {
		EOTypeModDelegation mode = TypeModDelegationSelectCtrl.sharedInstance(ec).getType();
		if (mode != null) {
			setCurrentMode(mode);
			if (mode.quotite() != null) {
				myView.getjPanelQuotite().setVisible(true);
				CocktailUtilities.setTextToField(myView.getTfQuotite(), currentMode.quotite().toBigInteger().toString());
				if (mode.isTypeModeAllegementEnseignement()) {
					myView.getjPanelQuotite().setVisible(false);
				}
			} else {
				CocktailUtilities.setTextToField(myView.getTfQuotite(), "100");
				myView.getjPanelQuotite().setVisible(true);
			}
		}
	}
	
	public void ajouter(EODelegation newObject) {		
		setCurrentObject(newObject);
		updateDatas();
	}
	
	/**
	 * 
	 */
	public void valider() {
						
		getCurrentObject().setMotifDelegationRelationship(getCurrentMotif());
		getCurrentObject().setToTypeModDelegationRelationship(getCurrentMode());
		getCurrentObject().setToRneRelationship(getCurrentUai());
		
		getCurrentObject().setDateDebut(ctrlParent.getDateDebut());
		getCurrentObject().setDateFin(ctrlParent.getDateFin());
		getCurrentObject().setDateArrete(ctrlParent.getDateArrete());
		getCurrentObject().setNoArrete(ctrlParent.getNumeroArrete());

		getCurrentObject().setDConvDelegation(CocktailUtilities.getDateFromField(myView.getTfDateConvention()));

		getCurrentObject().setQuotite(CocktailUtilities.getIntegerFromField(myView.getTfQuotite()));
		getCurrentObject().setMontantDelegation(CocktailUtilities.getBigDecimalFromField(myView.getTfMontantConvention()));
		getCurrentObject().setLieuDelegation(CocktailUtilities.getTextFromArea(myView.getTfLieu()));
		getCurrentObject().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));
				
		getCurrentObject().setTemAnnuelDelegation((myView.getCheckMontantAnnuel().isSelected())?"O":"N");
				
		getCurrentObject().absence().setDateDebut(getCurrentObject().dateDebut());
		getCurrentObject().absence().setDateFin(getCurrentObject().dateFin());
		getCurrentObject().absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(getCurrentObject().dateDebut(), getCurrentObject().dateFin(), true)));
	}
	
	public void supprimer() throws Exception {
		try {
			getCurrentObject().setEstValide(false);
		}
		catch (Exception ex) {
			throw ex;
		}		
	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}
	
	private void updateUI() {
		myView.getBtnGetMotif().setEnabled(saisieEnabled());
		myView.getBtnGetMode().setEnabled(saisieEnabled());
		myView.getBtnGetUai().setEnabled(saisieEnabled());
		myView.getBtnDelUai().setEnabled(saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateConvention(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfMontantConvention(), false, saisieEnabled());		
		CocktailUtilities.initTextField(myView.getTfQuotite(), false, saisieEnabled());
		CocktailUtilities.initTextArea(myView.getTfLieu(), false, saisieEnabled());
		CocktailUtilities.initTextArea(myView.getTfCommentaires(), false, saisieEnabled());
		
		myView.getCheckMontantAnnuel().setEnabled(saisieEnabled());
	}

	public void actualiser(EOModalitesService modalite) {

		clearDatas();

		if (modalite != null) {
			
			currentObject = EODelegation.findForKey(ec, modalite.modId());
			
			if (currentObject != null) {	
				
				updateDatas();
				
			}			
		}
		
		updateUI();
	}

	/**
	 * 
	 */
	private void updateDatas() {
		
		setCurrentMotif(getCurrentObject().motifDelegation());
		setCurrentMode(getCurrentObject().toTypeModDelegation());
		setCurrentUai(getCurrentObject().toRne());
		
		CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentObject().quotite());
		CocktailUtilities.setTextToArea(myView.getTfCommentaires(), getCurrentObject().commentaire());
		CocktailUtilities.setTextToArea(myView.getTfLieu(), getCurrentObject().lieuDelegation());
		CocktailUtilities.setDateToField(myView.getTfDateConvention(), getCurrentObject().dConvDelegation());
		CocktailUtilities.setNumberToField(myView.getTfMontantConvention(), getCurrentObject().montantDelegation());
		
		myView.getCheckMontantAnnuel().setSelected(getCurrentObject().temAnnuelDelegation() != null && "O".equals(getCurrentObject().temAnnuelDelegation()));
		if (getCurrentObject().toTypeModDelegation() != null) {
			myView.getjPanelQuotite().setVisible(!getCurrentObject().toTypeModDelegation().isTypeModeAllegementEnseignement());
		}
	}
	
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}

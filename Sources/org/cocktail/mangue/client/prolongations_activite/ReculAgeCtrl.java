// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.prolongations_activite;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.prolongations.ReculAgeView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.select.EnfantSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeMotProlongation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartEnfant;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationsActivite;
import org.cocktail.mangue.modele.mangue.prolongations.EOReculAge;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class ReculAgeCtrl {

	private EOEditingContext ec;
	public static String IMPRIMER_ARRETE = "ImpressionArrete";

	private PopupMotifListener listenerMotif = new PopupMotifListener();
	private ProlongationsActivitesCtrl ctrlParent;

	private ReculAgeView myView;
	private EOEnfant currentEnfant, currentEnfant2, currentEnfant3;
	private EOTypeMotProlongation currentMotif;
	private EOReculAge currentObject;
	private EOIndividu currentIndividu;
	private NSArray destinatairesArrete;
	private boolean saisieEnabled;

	public ReculAgeCtrl(ProlongationsActivitesCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new ReculAgeView();

		myView.getBtnGetEnfant().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getEnfant();}}
		);
		myView.getBtnGetEnfant2().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getEnfant2();}}
		);
		myView.getBtnGetEnfant3().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getEnfant3();}}
		);
		myView.getBtnDelEnfant2().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delEnfant2();}}
		);
		myView.getBtnDelEnfant3().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delEnfant3();}}
		);

		myView.setMotifs(EOTypeMotProlongation.rechercherTypesProlongation(ec, EOTypeMotProlongation.TYPE_MOTIF_RECUL_AGE));
		setSaisieEnabled(false);

		myView.getTfDateFinReelle().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFinReelle()));
		myView.getTfDateFinReelle().addActionListener(new ActionListenerDateTextField(myView.getTfDateFinReelle()));

		CocktailUtilities.initTextField(myView.getTfLibelleEnfant(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleEnfant1(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleEnfant2(), false, false);

		myView.getPopupMotif().addActionListener(listenerMotif);

	}

	public JPanel getView() {
		return myView;
	}	




	public EOEnfant currentEnfant() {
		return currentEnfant;
	}

	public void setCurrentEnfant(EOEnfant currentEnfant) {
		this.currentEnfant = currentEnfant;
		myView.getTfLibelleEnfant2().setText("");
		if (currentEnfant != null)
			CocktailUtilities.setTextToField(myView.getTfLibelleEnfant(), currentEnfant.identite());
	}

	public EOEnfant currentEnfant2() {
		return currentEnfant2;
	}

	public void setCurrentEnfant2(EOEnfant currentEnfant2) {
		this.currentEnfant2 = currentEnfant2;
		myView.getTfLibelleEnfant1().setText("");
		if (currentEnfant2 != null)
			CocktailUtilities.setTextToField(myView.getTfLibelleEnfant1(), currentEnfant2.identite());
	}

	public EOEnfant currentEnfant3() {
		return currentEnfant3;
	}

	public void setCurrentEnfant3(EOEnfant currentEnfant3) {
		this.currentEnfant3 = currentEnfant3;
		myView.getTfLibelleEnfant2().setText("");
		if (currentEnfant3 != null)
			CocktailUtilities.setTextToField(myView.getTfLibelleEnfant2(), currentEnfant3.identite());
	}

	public EOTypeMotProlongation currentMotif() {
		return currentMotif;
	}

	public void setCurrentMotif(EOTypeMotProlongation currentMotif) {
		this.currentMotif = currentMotif;
		myView.getPopupMotif().setSelectedItem(currentMotif);
		preparerDateFin();
		updateUI();
	}

	public EOReculAge getCurrentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOReculAge currentObject) {
		this.currentObject = currentObject;
		updateData();
	}

	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}

	public void clearTextFields() {

		myView.getTfLibelleEnfant().setText("");
		myView.getTfLibelleEnfant1().setText("");
		myView.getTfLibelleEnfant2().setText("");
		myView.getTfCommentaires().setText("");
		myView.getTfDateFinReelle().setText("");

	}

	private void delEnfant2() {
		setCurrentEnfant2(null);
	}
	private void delEnfant3() {
		setCurrentEnfant3(null);
	}

	// 10/06/2010
	private EOQualifier qualifierPourEnfants() {

		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.PARENT_KEY + " = %@", new NSArray(currentIndividu())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+EOEnfant.TEM_VALIDE_KEY + "=%@",new NSArray (CocktailConstantes.VRAI)));

		if (currentMotif().estEnfantMortPourFrance()) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("enfant.dDeces != null AND enfant.mortPourLaFrance = 'O'",null));
		} else if (currentMotif().estEnfantAChargeA65Ans()) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.TEM_A_CHARGE_KEY + "=%@",new NSArray(CocktailConstantes.VRAI)));
			NSTimestamp date65ans = DateCtrl.dateAvecAjoutAnnees(currentIndividu().dNaissance(), 65);
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+".dDeces = null OR enfant.dDeces > %@",new NSArray(date65ans)));

		} else {
			// Il faut identifier les enfants vivant à l'âge de 50 ans
			NSTimestamp date50ans = DateCtrl.dateAvecAjoutAnnees(currentIndividu().dNaissance(), 50);
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY + "." + EOEnfant.D_DECES_KEY + " = null OR enfant.dDeces > %@",new NSArray(date50ans)));
		}
		if (currentEnfant() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY + " != %@",new NSArray(currentEnfant())));
		}
		if (currentEnfant2() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY + " != %@",new NSArray(currentEnfant2())));
		}
		if (currentEnfant3() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY + " != %@",new NSArray(currentEnfant3())));
		}

		return new EOAndQualifier(qualifiers);
	}

	private void getEnfant() {
		EOEnfant enfant = EnfantSelectCtrl.sharedInstance(ec).getEnfantForQualifier(qualifierPourEnfants());		
		if (enfant != null)
			setCurrentEnfant(enfant);
	}
	private void getEnfant2() {
		EOEnfant enfant = EnfantSelectCtrl.sharedInstance(ec).getEnfantForQualifier(qualifierPourEnfants());		
		if (enfant != null)
			setCurrentEnfant2(enfant);
	}
	private void getEnfant3() {
		EOEnfant enfant = EnfantSelectCtrl.sharedInstance(ec).getEnfantForQualifier(qualifierPourEnfants());		
		if (enfant != null)
			setCurrentEnfant3(enfant);
	}


	private void preparerDateFin() {

		NSTimestamp dateDebut = ctrlParent.getDateDebut();
		NSTimestamp dateFin = null;

//		if (dateDebut != null) {
//			if (currentObject().motif().estEnfantAChargeA65Ans()) {
//				int nbAnnees = nbEnfantsAChargeA65Ans();
//				if (nbAnnees > EOReculAge.NB_ANNEES_MAXIMUM_POUR_ENFANT_A_CHARGE_A_65) {
//					nbAnnees = EOReculAge.NB_ANNEES_MAXIMUM_POUR_ENFANT_A_CHARGE_A_65;
//				}
//				dateFin = DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutAnnees(dateDebut, nbAnnees));
//			} else	if (currentObject().motif().est3EnfantsA50Ans()) {
//				currentObject().setDateFin(DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutAnnees(dateDebut, EOReculAge.NB_ANNEES_POUR_ENFANT_A_CHARGE_A_50)));
//			} else if (currentObject().motif().estEnfantMortPourFrance()) {
//				currentObject().setDateFin(DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutAnnees(dateDebut, nbEnfantsDecede)));
//			}
//
//			if (dateFin != null)
//				ctrlParent.setDateFin(dateFin);
//		}

	}

	public void ajouter(EOReculAge newObject) {
		currentObject = newObject;
		setCurrentIndividu(newObject.individu());
		updateData();
		updateUI();		
	}

	public boolean valider() {

		currentObject.setDateDebut(ctrlParent.getDateDebut());
		currentObject.setDateFin(ctrlParent.getDateFin());
		currentObject.setDateArrete(ctrlParent.getDateArrete());
		currentObject.setNoArrete(ctrlParent.getNumeroArrete());

		currentObject.setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));
		currentObject.setDFinExecution(CocktailUtilities.getDateFromField(myView.getTfDateFinReelle()));

		getCurrentObject().setMotifRelationship((EOTypeMotProlongation)myView.getPopupMotif().getSelectedItem());
		getCurrentObject().setEnfantRelationship(currentEnfant());
		getCurrentObject().setEnfant2Relationship(currentEnfant2());
		getCurrentObject().setEnfant3Relationship(currentEnfant3());

		return true;
	}

	public void imprimerArrete() {

		destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(ec).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
				NSMutableArray destinatairesGlobalIds = new NSMutableArray();
				for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(ec.globalIDForObject(e.nextElement())));
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class};
				Object[] parametres = new Object[]{ec.globalIDForObject(getCurrentObject()),destinatairesGlobalIds};
				UtilitairesImpression.imprimerSansDialogue(ec,"clientSideRequestImprimerArreteTempsPartiel",classeParametres,parametres,"ArreteTempsPartiel" + getCurrentObject().individu().noIndividu() ,"Impression de l'arrêté de temps Partiel");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	private void updateData() {

		clearTextFields();
		if (getCurrentObject() != null) {

			setCurrentMotif(getCurrentObject().motif());
			setCurrentEnfant(getCurrentObject().enfant());
			setCurrentEnfant2(getCurrentObject().enfant2());
			setCurrentEnfant3(getCurrentObject().enfant3());

			CocktailUtilities.setDateToField(myView.getTfDateFinReelle(), currentObject.dFinExecution());
			CocktailUtilities.setTextToArea(myView.getTfCommentaires(), currentObject.commentaire());

		}

		updateUI();
	}

	public void supprimer() throws Exception {
		try {
			ec.deleteObject(getCurrentObject());
		}
		catch (Exception ex) {
			throw ex;
		}		
	}



	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}


	public void actualiser(EOProlongationsActivite prolongation) {

		clearTextFields();
		setCurrentIndividu(prolongation.toIndividu());
		setCurrentObject(EOReculAge.findForKey(ec, prolongation.praId()));

	}

	private class PopupMotifListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){

			try {
				setCurrentMotif((EOTypeMotProlongation)myView.getPopupMotif().getSelectedItem());
			}
			catch (Exception e) {
				setCurrentMotif(null);
			}
		}
	}


	public void updateUI() {

		myView.getPopupMotif().setEnabled(saisieEnabled());

		myView.getBtnGetEnfant().setEnabled(
				saisieEnabled()
				&& currentMotif() != null 
				&& (currentMotif().est3EnfantsA50Ans() || currentMotif().estEnfantAChargeA65Ans()
						|| currentMotif().estEnfantMortPourFrance())
		);

		myView.getBtnGetEnfant2().setEnabled(
				saisieEnabled()
				&& currentMotif() != null 
				&& ( currentMotif().est3EnfantsA50Ans() || currentMotif().estEnfantAChargeA65Ans() )
		);

		myView.getBtnGetEnfant3().setEnabled(
				saisieEnabled()
				&& currentMotif() != null 
				&& ( currentMotif().est3EnfantsA50Ans() || currentMotif().estEnfantAChargeA65Ans() )
		);

		myView.getBtnDelEnfant2().setEnabled(saisieEnabled() && currentEnfant2() != null);
		myView.getBtnDelEnfant3().setEnabled(saisieEnabled() && currentEnfant3() != null);
		CocktailUtilities.initTextField(myView.getTfDateFinReelle(), false, saisieEnabled());
		CocktailUtilities.initTextArea(myView.getTfCommentaires(), false, saisieEnabled());

	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}
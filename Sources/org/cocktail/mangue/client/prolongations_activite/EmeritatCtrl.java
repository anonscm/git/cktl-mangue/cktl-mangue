// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.prolongations_activite;

import java.util.Enumeration;

import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.prolongations.EmeritatView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.prolongations.EOEmeritat;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationsActivite;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class EmeritatCtrl {

	private EOEditingContext ec;
	public static String IMPRIMER_ARRETE = "ImpressionArrete";

	private EmeritatView myView;
	private EOEmeritat currentObject;
	private EOIndividu currentIndividu;
	private NSArray destinatairesArrete;
	private boolean saisieEnabled;
	private ProlongationsActivitesCtrl ctrlParent;
	
	public EmeritatCtrl(ProlongationsActivitesCtrl ctrlParent, EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new EmeritatView();
		setSaisieEnabled(false);
		
	}

	public JPanel getView() {
		return myView;
	}	

	public EOEmeritat currentObject() {
		return currentObject;
	}
	public void setCurrentObject(EOEmeritat currentObject) {
		this.currentObject = currentObject;
	}

	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}

	public void clearTextFields() {
		myView.getTfCommentaires().setText("");
		myView.getTfDateDecision().setText("");
	}
	
	public void ajouter(EOEmeritat newObject) {
		clearTextFields();
		setCurrentObject(newObject);
		setCurrentIndividu(newObject.individu());
		updateUI();		
	}

	public boolean valider() {

		currentObject().setDateDebut(ctrlParent.getDateDebut());
		currentObject().setDateFin(ctrlParent.getDateFin());
		currentObject().setDateArrete(ctrlParent.getDateArrete());
		currentObject().setNoArrete(ctrlParent.getNumeroArrete());

		currentObject().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));
		currentObject().setDDecision(CocktailUtilities.getDateFromField(myView.getTfDateDecision()));

		return true;
	}

	public void imprimerArrete() {
		
		destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(ec).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
					NSMutableArray destinatairesGlobalIds = new NSMutableArray();
					for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(ec.globalIDForObject(e.nextElement())));
					Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class};
					Object[] parametres = new Object[]{ec.globalIDForObject(currentObject()),destinatairesGlobalIds};
					UtilitairesImpression.imprimerSansDialogue(ec,"clientSideRequestImprimerArreteEmeritat",classeParametres,parametres,"ArreteEmeritat" + currentObject().individu().noIndividu() ,"Impression de l'arrêté d'éméritat");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	
	private void updateData() {		
		CocktailUtilities.setDateToField(myView.getTfDateDecision(), currentObject().dDecision());
		CocktailUtilities.setTextToArea(myView.getTfCommentaires(), currentObject().commentaire());
	}
	
	public void supprimer() throws Exception {
		try {
			ec.deleteObject(currentObject());
		}
		catch (Exception ex) {
			throw ex;
		}		
	}

	public boolean saisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}

	public void actualiser(EOProlongationsActivite prolongation) {

		clearTextFields();
		setCurrentIndividu(prolongation.toIndividu());

		if (prolongation != null) {
			setCurrentObject(EOEmeritat.findForKey(ec, prolongation.praId()));
			if (currentObject() != null) {
				updateData();
			}			
		}

		updateUI();
	}

	public void updateUI() {	
		
		CocktailUtilities.initTextField(myView.getTfDateDecision(), false, saisieEnabled());
		CocktailUtilities.initTextArea(myView.getTfCommentaires(), false, saisieEnabled());
		
	}

}
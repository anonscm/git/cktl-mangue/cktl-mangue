package org.cocktail.mangue.client.prolongations_activite;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.prolongations.ProlongationsView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.prolongations.EOEmeritat;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationActivite;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationsActivite;
import org.cocktail.mangue.modele.mangue.prolongations.EOReculAge;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class ProlongationsActivitesCtrl {

	private static ProlongationsActivitesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;
	private ProlongationsView myView;

	private ListenerProlongations listenerProlongations = new ListenerProlongations();

	private ReculAgeCtrl ctrlReculAge;
	private ProlongationCtrl ctrlProlongation;
	private EmeritatCtrl ctrlEmeritat;
	private boolean peutGererModule;

	private boolean modeCreation, peutAjouter;
	private EODisplayGroup eod;
	private EOProlongationsActivite currentProlongation;
	private EOIndividu currentIndividu;
	private EOAgentPersonnel currentUtilisateur;

	private boolean saisieEnabled;

	private PopupTypeProlongationListener listenerTypeProlongation = new PopupTypeProlongationListener();

	public ProlongationsActivitesCtrl(EOEditingContext editingContext) {

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new ProlongationsView(null, MODE_MODAL.booleanValue(),eod);

		myView.getMyEOTable().addListener(listenerProlongations);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		myView.getBtnImprimerArrete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerArrete();}}
		);

		ctrlProlongation = new ProlongationCtrl(this, ec);
		ctrlReculAge = new ReculAgeCtrl(this, ec);
		ctrlEmeritat = new EmeritatCtrl(this, ec);

		// Initialisation des layouts
		myView.getSwapViewModalites().add("VIDE",new JPanel(new BorderLayout()));
		myView.getSwapViewModalites().add(EOProlongationsActivite.TYPE_RECUL_AGE,ctrlReculAge.getView());
		myView.getSwapViewModalites().add(EOProlongationsActivite.TYPE_PROLONGATION,ctrlProlongation.getView());
		myView.getSwapViewModalites().add(EOProlongationsActivite.TYPE_EMERITAT, ctrlEmeritat.getView());

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateArrete().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArrete()));
		myView.getTfDateArrete().addActionListener(new ActionListenerDateTextField(myView.getTfDateArrete()));

		setSaisieEnabled(false);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		myView.getBtnImprimerArrete().setEnabled(false);
		myView.getPopupTypes().addActionListener(listenerTypeProlongation);

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(ec, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		myView.getCheckSigne().setVisible(false);
		setSaisieEnabled(false);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

	}
	
	
	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {

		this.currentUtilisateur = currentUtilisateur;

		setPeutGererModule(currentUtilisateur.peutGererCarrieres());

		// Gestion des droits
		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());
		myView.getBtnImprimerArrete().setVisible(peutGererModule());
	}

	public static ProlongationsActivitesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ProlongationsActivitesCtrl(editingContext);
		return sharedInstance;
	}

	public EOProlongationsActivite currentProlongation() {
		return currentProlongation;
	}
	public void setCurrentProlongation(EOProlongationsActivite currentProlongation) {
		this.currentProlongation = currentProlongation;
		updateData();
	}
	public EOIndividu currentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	private boolean peutAjouter() {
		return peutAjouter;
	}
	private void setPeutAjouter(boolean yn) {
		peutAjouter = yn;
	}

	private void verifierConditionsFonctionnaire() {

		NSTimestamp date65Ans = DateCtrl.dateAvecAjoutAnnees(currentIndividu().dNaissance(), 65);
		setPeutAjouter(false);
		NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(ec,currentIndividu(),date65Ans,null);
		for (java.util.Enumeration<EOCarriere> e = carrieres.objectEnumerator();e.hasMoreElements();) {
			EOCarriere carriere = e.nextElement();
			if (carriere.toTypePopulation().estFonctionnaire()) {
				setPeutAjouter(true);
			}
		}
	}

	public void employeHasChanged(NSNotification  notification) {
		clearTextFields();
		if (notification != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(ec,(Number)notification.object(), false));
		}
	}

	public void actualiser() {

		eod.setObjectArray(EOProlongationsActivite.fetchForIndividu(ec, currentIndividu()));
		myView.getMyEOTable().updateData();
		updateUI();

	}
	public JFrame getView() {
		return myView;
	}
	public JPanel getViewProlongations() {
		return myView.getViewProlongations();
	}

	public NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	public void setDateDebut(NSTimestamp myDateDebut) {
		CocktailUtilities.setDateToField(myView.getTfDateDebut(), myDateDebut);
	}
	public NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}
	public void setDateFin(NSTimestamp myDateFin) {
		CocktailUtilities.setDateToField(myView.getTfDateFin(), myDateFin);
	}
	public NSTimestamp getDateArrete() {
		return CocktailUtilities.getDateFromField(myView.getTfDateArrete());
	}
	public String getNumeroArrete() {
		return CocktailUtilities.getTextFromField(myView.getTfNoArrete());
	}

	public void toFront() {
		myView.toFront();
	}

	private void ajouter() {

		modeCreation = true;

		clearTextFields();

		myView.getBtnAjouter().setEnabled(false);
		myView.getBtnModifier().setEnabled(false);
		myView.getBtnSupprimer().setEnabled(false);
		myView.getMyEOTable().setEnabled(false);
		myView.getLblTypeModalite().setForeground(Color.RED);
		myView.getPopupTypes().setEnabled(true);

		myView.getBtnAnnuler().setEnabled(true);
		((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), "VIDE");

		myView.getBtnImprimerArrete().setEnabled(false);

	}

	private void modifier() {

		modeCreation = false;
		setSaisieEnabled(true);
		updateUI();
	}


	private void imprimerArrete() {

		try {

			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlReculAge.imprimerArrete();break;
			case 2 : ctrlProlongation.imprimerArrete();break;
			case 3 : ctrlEmeritat.imprimerArrete();break;
			}

			ec.saveChanges();
			actualiser();
			toFront();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			ec.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			ec.revert();
		}

	}


	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cet enregistrement ?", "Oui", "Non"))		
			return;			

		try {

			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlReculAge.supprimer();break;
			case 2 : ctrlProlongation.supprimer();break;
			case 3 : ctrlEmeritat.supprimer();break;
			}

			ec.saveChanges();
			actualiser();
			toFront();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			ec.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			ec.revert();
		}
	}

	private void valider() {

		try {
			switch (myView.getPopupTypes().getSelectedIndex()) {
			case 1 : ctrlReculAge.valider();break;
			case 2 : ctrlProlongation.valider();break;
			case 3 : ctrlEmeritat.valider();break;
			}

			ec.saveChanges();
			setSaisieEnabled(false);

			if (modeCreation) {
				EOProlongationsActivite prolong = currentProlongation();
				actualiser();
				if (prolong != null)
					myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(prolong));
			}
			else {
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(currentProlongation())));
				myView.getMyEOTable().updateUI();
				listenerProlongations.onSelectionChanged();
			}

			updateUI();

		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			myView.toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void annuler() {

		ec.revert();
		setSaisieEnabled(false);
		listenerProlongations.onSelectionChanged();

		updateUI();

	}

	public void nettoyerChamps(NSNotification notification) {

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
		currentIndividu = null;
		updateUI();
	}

	private void clearTextFields() {

		myView.getPopupTypes().setSelectedIndex(0);

		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
		myView.getTfDateArrete().setText("");
		myView.getTfNoArrete().setText("");

		ctrlReculAge.clearTextFields();
		ctrlProlongation.clearTextFields();
		ctrlEmeritat.clearTextFields();

	}

	private void updateData() {

		clearTextFields();

		if (currentProlongation() != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), currentProlongation().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), currentProlongation().dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateArrete(), currentProlongation().dateArrete());
			CocktailUtilities.setTextToField(myView.getTfNoArrete(), currentProlongation().noArrete());

			// Swap view
			myView.getPopupTypes().removeActionListener(listenerTypeProlongation);
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), currentProlongation().praType());				

			myView.getBtnImprimerArrete().setEnabled(false);

			if (EOProlongationsActivite.TYPE_RECUL_AGE.equals(currentProlongation().praType())) {
				myView.getPopupTypes().setSelectedIndex(1);		
				ctrlReculAge.actualiser(currentProlongation());
				myView.getBtnImprimerArrete().setEnabled(true);
			}
			if (EOProlongationsActivite.TYPE_PROLONGATION.equals(currentProlongation().praType())) {
				myView.getPopupTypes().setSelectedIndex(2);			
				ctrlProlongation.actualiser(currentProlongation());
				myView.getBtnImprimerArrete().setEnabled(true);
			}
			if (EOProlongationsActivite.TYPE_EMERITAT.equals(currentProlongation().praType())) {
				myView.getPopupTypes().setSelectedIndex(3);			
				ctrlEmeritat.actualiser(currentProlongation());
			}

			myView.getPopupTypes().addActionListener(listenerTypeProlongation);
		}
		updateUI();

	}

	private class ListenerProlongations implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if ( currentProlongation() != null && peutGererModule() )
			modifier();
		}
		public void onSelectionChanged() {
			setCurrentProlongation((EOProlongationsActivite)eod.selectedObject());
		}
	}

	private class PopupTypeProlongationListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			if (myView.getPopupTypes().getSelectedIndex() > 0) {

				if (modeCreation) {

					boolean saisiePossible = true;

					int index = myView.getPopupTypes().getSelectedIndex();
					verifierConditionsFonctionnaire();
					if (index == 1 && !peutAjouter()) {
						EODialogs.runErrorDialog("ERREUR", "Pas de recul d'âge possible pour un non titulaire !");
						saisiePossible = false;
					}
					if (index == 2 && !peutAjouter()) {
						EODialogs.runErrorDialog("ERREUR", "Pas de prolongation d'activité possible pour un non titulaire !");
						saisiePossible = false;
					}
					if (index == 3) {
						NSArray elements = EOElementCarriere.findForEmeritat(ec, currentIndividu());
						if (elements.count() == 0) {
							EODialogs.runErrorDialog("ERREUR", "Les éléments de carrière de cet agent ne reposent pas sur des corps émérites !");							
							saisiePossible = false;
						}						
					}
					if( !saisiePossible) {
						myView.getPopupTypes().setSelectedIndex(0);
						setSaisieEnabled(false);
						return;
					}
				}

				setSaisieEnabled(true);
				myView.getLblTypeModalite().setForeground(Color.BLACK);

				switch (myView.getPopupTypes().getSelectedIndex()) {
				case 1 : gererReculAge();break;
				case 2 : gererProlongation();break;
				case 3 : gererEmeritat();break;
				}

				myView.getPopupTypes().setEnabled(false);
			}
		}
	}


	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	private void setSaisieEnabled(boolean yn) {

		saisieEnabled = yn;

		if (yn)
			NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		else
			NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);

		switch (myView.getPopupTypes().getSelectedIndex()) {
		case 1 : ctrlReculAge.setSaisieEnabled(yn);break;
		case 2 : ctrlProlongation.setSaisieEnabled(yn);break;
		case 3 : ctrlEmeritat.setSaisieEnabled(yn);break;
		}

		updateUI();

	}

	private void traitementsPourCreationEmeritat() {

		NSArray reculs = EOQualifier.filteredArrayWithQualifier(eod.displayedObjects(), 
				EOQualifier.qualifierWithQualifierFormat(EOProlongationsActivite.PRA_TYPE_KEY+"=%@", new NSArray(EOProlongationsActivite.TYPE_RECUL_AGE)));
		if( reculs.count() > 0) {
			reculs = EOSortOrdering.sortedArrayUsingKeyOrderArray(reculs, EOProlongationsActivite.SORT_ARRAY_DATE_DEBUT_DESC);
			setDateDebut(DateCtrl.jourSuivant(((EOProlongationsActivite)reculs.objectAtIndex(0)).dateFin()));
		} else {
			// première période, calculer le jour du 65ème anniversaire
			NSTimestamp dateAnniversaire = DateCtrl.dateAvecAjoutAnnees(currentIndividu().dNaissance(),65);
			setDateDebut(DateCtrl.jourSuivant(dateAnniversaire));
		}
	}

	private void traitementsPourCreationReculAge() {

		NSArray reculs = EOQualifier.filteredArrayWithQualifier(eod.displayedObjects(), 
				EOQualifier.qualifierWithQualifierFormat(EOProlongationsActivite.PRA_TYPE_KEY+"=%@", new NSArray(EOProlongationsActivite.TYPE_RECUL_AGE)));
		if( reculs.count() > 0) {
			reculs = EOSortOrdering.sortedArrayUsingKeyOrderArray(reculs, EOProlongationsActivite.SORT_ARRAY_DATE_DEBUT_DESC);
			setDateDebut(DateCtrl.jourSuivant(((EOProlongationsActivite)reculs.objectAtIndex(0)).dateFin()));
		} else {
			// première période, calculer le jour du 65ème anniversaire
			NSTimestamp dateAnniversaire = DateCtrl.dateAvecAjoutAnnees(currentIndividu().dNaissance(),65);
			setDateDebut(DateCtrl.jourSuivant(dateAnniversaire));
			//setDateFin(DateCtrl.dateAvecAjoutAnnees(dateAnniversaire, 1));
		}
	}

	private void traitementsPourCreationProlongation() {

		NSArray prolongations = EOQualifier.filteredArrayWithQualifier(eod.displayedObjects(), 
				EOQualifier.qualifierWithQualifierFormat(EOProlongationsActivite.PRA_TYPE_KEY+"=%@", new NSArray(EOProlongationsActivite.TYPE_PROLONGATION)));
		if( prolongations.count() > 0) {
			prolongations = EOSortOrdering.sortedArrayUsingKeyOrderArray(prolongations, EOProlongationsActivite.SORT_ARRAY_DATE_DEBUT_DESC);
			setDateDebut(DateCtrl.jourSuivant(((EOProlongationsActivite)prolongations.objectAtIndex(0)).dateFin()));
		} else {
			NSArray reculs = EOQualifier.filteredArrayWithQualifier(eod.displayedObjects(), 
					EOQualifier.qualifierWithQualifierFormat(EOProlongationsActivite.PRA_TYPE_KEY+"=%@", new NSArray(EOProlongationsActivite.TYPE_RECUL_AGE)));
			if (reculs.count() > 0) {
				reculs = EOSortOrdering.sortedArrayUsingKeyOrderArray(reculs, EOReculAge.SORT_ARRAY_DATE_DEBUT_DESC);
				setDateDebut(DateCtrl.jourSuivant(((EOProlongationsActivite)reculs.objectAtIndex(0)).dateFin()));				
			}
			else {
				NSTimestamp dateAnniversaire = DateCtrl.dateAvecAjoutAnnees(currentIndividu().dNaissance(),65);
				setDateDebut(DateCtrl.jourSuivant(dateAnniversaire));
			}
		}
	}


	private void gererReculAge() {
		if (modeCreation) {
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), EOProlongationsActivite.TYPE_RECUL_AGE);				
			traitementsPourCreationReculAge();
			ctrlReculAge.ajouter(EOReculAge.creer(ec, currentIndividu()));			
		}
	}
	private void gererProlongation() {
		if (modeCreation) {
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), EOProlongationsActivite.TYPE_PROLONGATION);				
			traitementsPourCreationProlongation();
			ctrlProlongation.ajouter(EOProlongationActivite.creer(ec, currentIndividu));			
		}		
	}
	private void gererEmeritat() {
		if (modeCreation) {
			((CardLayout)myView.getSwapViewModalites().getLayout()).show(myView.getSwapViewModalites(), EOProlongationsActivite.TYPE_EMERITAT);				
			traitementsPourCreationEmeritat();
			ctrlEmeritat.ajouter(EOEmeritat.creer(ec, currentIndividu));			
		}		
	}

	private void updateUI() {

		myView.getLblTypeModalite().setForeground(Color.BLACK);
		myView.getPopupTypes().setEnabled(modeCreation && saisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateArrete(), false, saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfNoArrete(), false, saisieEnabled());

		myView.getBtnAjouter().setEnabled(currentIndividu != null && !saisieEnabled());
		myView.getBtnModifier().setEnabled(currentIndividu != null && currentProlongation() != null && !saisieEnabled());
		myView.getBtnSupprimer().setEnabled(currentIndividu != null && currentProlongation() != null && !saisieEnabled());
		myView.getBtnValider().setEnabled(saisieEnabled());
		myView.getBtnAnnuler().setEnabled(saisieEnabled());

		myView.getMyEOTable().setEnabled(!saisieEnabled());

		myView.getBtnImprimerArrete().setVisible(
				!saisieEnabled() 
				&& ( currentProlongation() != null) 
				&& ( currentProlongation().estEmeritat())
				&& myView.getTfDateArrete().getText().length() > 0);

	}



	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}

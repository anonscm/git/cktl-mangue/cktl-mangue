// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.prolongations_activite;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.prolongations.ProlongationView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeMotProlongation;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.EOStage;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationActivite;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationsActivite;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class ProlongationCtrl {

	private EOEditingContext ec;
	public static String IMPRIMER_ARRETE = "ImpressionArrete";
	private PopupMotifListener listenerMotif = new PopupMotifListener();

	private ProlongationsActivitesCtrl ctrlParent;

	private ProlongationView myView;
	private EOTypeMotProlongation currentMotif;
	private EOProlongationActivite currentObject;
	private EOIndividu currentIndividu;
	private NSArray destinatairesArrete;
	private boolean saisieEnabled;

	public ProlongationCtrl(ProlongationsActivitesCtrl ctrlParent,  EOEditingContext editingContext)	{

		ec = editingContext;
		this.ctrlParent = ctrlParent;

		myView = new ProlongationView();
		myView.setMotifs(EOTypeMotProlongation.rechercherTypesProlongation(ec, EOTypeMotProlongation.TYPE_MOTIF_PROLONGATION));
		setSaisieEnabled(false);

		myView.getTfDateFinReelle().addFocusListener(new FocusListenerDateFinReelle());
		myView.getTfDateFinReelle().addActionListener(new ActionListenerDateFinReelle());
		myView.getPopupMotif().addActionListener(listenerMotif);

	}

	public JPanel getView() {
		return myView;
	}	

	public EOTypeMotProlongation currentMotif() {
		return currentMotif;
	}

	public void setCurrentMotif(EOTypeMotProlongation currentMotif) {
		this.currentMotif = currentMotif;
	}

	public EOProlongationActivite currentObject() {
		return currentObject;
	}

	public void setCurrentObject(EOProlongationActivite currentObject) {
		this.currentObject = currentObject;
	}

	public EOIndividu currentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}

	public void clearTextFields() {
		myView.getTfCommentaires().setText("");
		myView.getTfDateFinReelle().setText("");
	}

	public void ajouter(EOProlongationActivite newObject) {

		clearTextFields();
		setCurrentObject(newObject);
		setCurrentIndividu(newObject.individu());

		updateUI();
	}


	/**
	 * Fermeture des carrieres / occupations / affectation a la date de fin de la prolongation
	 * @return
	 */
	public void traitementAvantValidation() throws ValidationException {

		if (currentObject.motif().estSurnombre()) {

//			EODepart depart = EODepart.dernierDepartPourIndividu(ec, currentIndividu());
//			if (depart == null || depart.dEffetRadiation() == null)
//				throw new NSValidation.ValidationException("Veuillez saisir un départ avec une date de radiation des cadres avant la prolongation pour surnombre !");

			NSArray stages = EOStage.findForIndividuEtPeriode(ec,currentIndividu(),currentObject().dateDebut(),currentObject().dateFin(),false);
			if (stages != null && stages.count() > 0) {
				throw new NSValidation.ValidationException("Un agent stagiaire ne peut bénéficier d'une période de surnombre !");
			}

			EOElementCarriere element = EOElementCarriere.lastElementCarriere(ec, currentIndividu());
			if (element != null && element.toCorps().beneficieSurnombre() == false)
				throw new NSValidation.ValidationException("Le corps de " + element.toCorps().llCorps() + " ne supporte pas de périodes de surnombre !");

		}

	//	fermerSituationAgentADate(currentObject().dateFin());
	}

	//FIXME - Controler la fermeture des carrieres 
	public void fermerSituationAgentADate(NSTimestamp dateRef) {

		NSArray carrieres = EOCarriere.rechercherCarrieresADate(ec, currentIndividu(), DateCtrl.jourPrecedent(currentObject().dateDebut()));
		for (Enumeration<EOCarriere> e = carrieres.objectEnumerator();e.hasMoreElements();) {
			EOCarriere carriere = e.nextElement();
			if (DateCtrl.isBeforeEq(carriere.dateDebut(), currentObject().dateFin())) {
				carriere.fermerCarriereADate(currentObject().dateFin());
			}
		}

		// Occupations
		NSArray occupations = EOOccupation.findForIndividuAndDate(ec,currentIndividu(),DateCtrl.jourPrecedent(currentObject().dateDebut()));
		for (Enumeration<EOOccupation> e = occupations.objectEnumerator();e.hasMoreElements();) {
			EOOccupation occupation = e.nextElement();
			occupation.setDateFin(currentObject().dateFin());
		}

		// Affectations
		NSArray affectations = EOAffectation.rechercherAffectationsADate(ec,currentIndividu(),DateCtrl.jourPrecedent(currentObject().dateDebut()));
		for (Enumeration<EOAffectation> e = affectations.objectEnumerator();e.hasMoreElements();) {
			EOAffectation affectation = e.nextElement();
			affectation.setDateFin(currentObject().dateFin());
		}

	}


	/**
	 * 
	 * Lors d'une suppression de periode de prolongation, fermer la situation de l'agent au dernier depart connu
	 * 
	 * @throws Exception
	 */
	public void traitementPourSuppression() throws Exception {
		try {
			EODepart depart = EODepart.rechercherDernierDepartPourIndividu(ec, currentIndividu());
			if (depart != null && depart.motifDepart().estDefinitif()) {
//				fermerSituationAgentADate(depart.dateFin());
			}
		}
		catch (Exception e) {
			throw e;
		}

	}


	public void valider() throws ValidationException {

		try {
			currentObject().setDateDebut(ctrlParent.getDateDebut());
			currentObject().setDateFin(ctrlParent.getDateFin());
			currentObject().setDateArrete(ctrlParent.getDateArrete());
			currentObject().setNoArrete(ctrlParent.getNumeroArrete());

			currentObject().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));
			currentObject().setDFinExecution(CocktailUtilities.getDateFromField(myView.getTfDateFinReelle()));

			currentObject().setMotifRelationship((EOTypeMotProlongation)myView.getPopupMotif().getSelectedItem());

			traitementAvantValidation();
		}
		catch (ValidationException e) {
			throw e;
		}
	}

	public void imprimerArrete() {

		destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(ec).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
				NSMutableArray destinatairesGlobalIds = new NSMutableArray();
				for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(ec.globalIDForObject(e.nextElement())));
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class};
				Object[] parametres = new Object[]{ec.globalIDForObject(currentObject()),destinatairesGlobalIds};
				UtilitairesImpression.imprimerSansDialogue(ec,"clientSideRequestImprimerArreteTempsPartiel",classeParametres,parametres,"ArreteTempsPartiel" + currentObject().individu().noIndividu() ,"Impression de l'arrêté de temps Partiel");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	private void updateData() {

		myView.getPopupMotif().setSelectedItem(currentMotif());
		CocktailUtilities.setDateToField(myView.getTfDateFinReelle(), currentObject.dFinExecution());
		CocktailUtilities.setTextToArea(myView.getTfCommentaires(), currentObject.commentaire());

	}

	public void supprimer() throws Exception {
		try {

			traitementPourSuppression();

			ec.deleteObject(currentObject());
		}
		catch (Exception ex) {
			throw ex;
		}		
	}

	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateUI();
	}

	private class PopupMotifListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			setCurrentMotif((EOTypeMotProlongation)myView.getPopupMotif().getSelectedItem());
			updateUI();

		}
	}


	public void actualiser(EOProlongationsActivite prolongation) {

		clearTextFields();
		setCurrentIndividu(prolongation.toIndividu());

		if (prolongation != null) {

			setCurrentObject(EOProlongationActivite.findForKey(ec, prolongation.praId()));

			if (currentObject != null) {
				setCurrentMotif(currentObject.motif());
				updateData();
			}			
		}

		updateUI();
	}


	public boolean saisieEnabled() {
		return saisieEnabled;
	}

	public void updateUI() {	

		myView.getPopupMotif().setEnabled(saisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFinReelle(), false, saisieEnabled());
		CocktailUtilities.initTextArea(myView.getTfCommentaires(), false, saisieEnabled());

	}

	private class ActionListenerDateFinReelle implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateFinReelle().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateFinReelle().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin réelle n'est pas valide !");
				myView.getTfDateFinReelle().selectAll();
			}
			else
				myView.getTfDateFinReelle().setText(myDate);
		}
	}
	private class FocusListenerDateFinReelle implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateFinReelle().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateFinReelle().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin réelle n'est pas valide !");
				myView.getTfDateFinReelle().selectAll();
			}
			else
				myView.getTfDateFinReelle().setText(myDate);
		}
	}

}
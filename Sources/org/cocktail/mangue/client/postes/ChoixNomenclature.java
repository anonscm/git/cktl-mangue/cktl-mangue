/*
 * Created on 11 avr. 2006
 *
 * Gestion des nomenclatures Lolf
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.postes;

import org.cocktail.client.components.DialogueModalAvecArbre;
import org.cocktail.mangue.modele.mangue.budget.EOLolfNomenclatureDepense;
import org.cocktail.mangue.modele.mangue.lolf.EOFctSilland;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/** Gestion des nomenclatures Lolf. Pr&eacute;sentation sous une forme arborescente<BR>
 * @author christine
 *

 */
public class ChoixNomenclature extends DialogueModalAvecArbre {
	private EOFctSilland fonctionSilland;
	
	private static ChoixNomenclature sharedInstance = null;
	
	/** Méthodes statiques */
	public static ChoixNomenclature sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new ChoixNomenclature();
		}
		return sharedInstance;
	}

	/**
	* Selection d'une nomenclature
	*
	*/
	public EOLolfNomenclatureDepense selectionnerNomenclature(EOEditingContext editingContext,EOFctSilland fonctionSilland) {
		this.fonctionSilland = fonctionSilland;
		return (EOLolfNomenclatureDepense)super.selectionnerObjet(editingContext);
	}
	
	// méthodes protégées
	protected String titreFenetre() {
		return "Sélection d'une nomenclature Lolf";
	}
	protected String titreArbre() {
		return "Nomeclatures";
	}
	protected String nomEntiteAffichee() {
		return "LolfNomenclature";
	}
	protected EOQualifier qualifierForColumn() {
		return EOQualifier.qualifierWithQualifierFormat("tosLolfNomenclatureFils <> nil", null);
	}
	protected EOQualifier restrictionQualifier() {
		NSArray args = new NSArray(fonctionSilland);
		return EOQualifier.qualifierWithQualifierFormat("tosRepartSilLolf.toFctSilland = %@",args);
	}
	protected String parentRelationship() {
		return "toLolfNomenclaturePere";
	}
	protected String attributeOfParent() {
		return null;
	}
	protected String parentAttributeOfChild() {
		return null;
	}
	protected String attributeForDisplay() {
		return "lolfLibelle";
	}
	/** On peut toujours s&eacute;lectionner une nomenclature lolf */
	protected boolean peutSelectionnerObjet(EOGenericRecord objet) {	
		return true;
	}

}

/*
 * Created on 7 avr. 2006
 *
 * Gestion des postes
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.postes;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.MatrixUtilities;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOLoge;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.lolf.EOPoste;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOMatrix;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;


/** Gestion des postes
 * @author christine
 *
 */
// 26/11/09 - ajout de la gestion du type de logement associé au poste et de la gestion des coefficients de PFR
// 18/01/2011 - Adaptation Netbeans
public class GestionPostes extends ModelePageComplete implements ChangeListener {
	// Pour la recherche
	public EODisplayGroup displayGroupStructure;
	public EOMatrix matriceRecherchePoste;
	public EOView vueAgent;
	public JTabbedPane vueOnglets;
	private String codePourRecherche,structurePourRecherche;
	private InspecteurPoste inspecteur;
	private FicheDePoste controleurFicheDePoste;
	private FicheLolf controleurFicheLolf;
	private GestionPostePFR controleurPostePFR;
	private boolean preparationFenetre;
	private EOAgentPersonnel currentUtilisateur;

	/** Notification envoyee pour synchroniser l'affichage dans l'inspecteur */
	public static String CHANGER_POSTE = "ChangerPoste";
	private final static int FICHE_POSTE = 0;
	private final static int FICHE_LOLF = 1;
	private final static int POSTE_PFR = 2;
	
	public GestionPostes() {
		super();
		initialiser(false,false);
		currentUtilisateur = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
	}	
	public String codePourRecherche() {
		return codePourRecherche;
	}
	public void setCodePourRecherche(String uneString) {
		codePourRecherche = uneString;
	}
	public String structurePourRecherche() {
		return structurePourRecherche;
	}
	public void setStructurePourRecherche(String uneString) {
		structurePourRecherche = uneString;
	}
	/** retourne l'occupant du poste et "" en mode creation */
	public String occupantDuPoste() {
		if (modeCreation() || currentPoste() == null) {
			return "";
		} else {
			return currentPoste().occupantDuPoste();
		}
	}
	public String cTypeLogement() {
		if (currentPoste() == null || currentPoste().typeLogement() == null) {
			return null;
		} else {
			return currentPoste().typeLogement().code();
		}
	}
	public void setCTypeLogement(String aType) {
		if (currentPoste().typeLogement() != null) {
			if (aType == null ||  currentPoste().typeLogement().code().equals(aType) == false) {
				currentPoste().setTypeLogementRelationship(null);
			} 
		}
		EOLoge typeLogement = (EOLoge)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), EOLoge.ENTITY_NAME, INomenclature.CODE_KEY, aType);
		if (typeLogement != null) {
			currentPoste().setTypeLogementRelationship(typeLogement);
		}
		updaterDisplayGroups();
	}
	public String libelleTypeLogement() {
		if (currentPoste() == null || currentPoste().typeLogement() == null) {
			return null;
		} else {
			return currentPoste().typeLogement().code();
		}
	}
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aDisplayGroup) {
		super.displayGroupDidChangeSelection(aDisplayGroup);
		vueAgent.setVisible(!modeCreation());
		NSNotificationCenter.defaultCenter().postNotification(CHANGER_POSTE,currentPoste());
		if (controleurFicheDePoste != null) {
			if (currentPoste() != null && !modeCreation()) {
				controleurFicheDePoste.setPosteID(editingContext().globalIDForObject(currentPoste()));
			} else {
				controleurFicheDePoste.setPosteID(null);
			}
		}
		if (controleurFicheLolf != null) {
			if (currentPoste() != null && !modeCreation()) {
				// si il y a un accès sur les fiches Lolf autorisés en lecture ou en écriture
				controleurFicheLolf.setPosteID(editingContext().globalIDForObject(currentPoste()));
			} else {
				controleurFicheLolf.setPosteID(null);
			}
		}
		if (controleurPostePFR != null) {
			if (currentPoste() != null && !modeCreation()) {
				// si il y a un accès sur les fiches Lolf autorisés en lecture ou en écriture
				controleurPostePFR.setPosteID(editingContext().globalIDForObject(currentPoste()));
			} else {
				controleurPostePFR.setPosteID(null);
			}
		}
		updaterDisplayGroups();
	}	
	// actions
	public void supprimerStructure() {
		currentPoste().setToStructureRelationship(null);
	}
	public void afficherTypeLogement() {
		UtilitairesDialogue.afficherDialogue(this,"Loge","getLogement",false,null,true);
	}
	public void supprimerTypeLogement() {
		currentPoste().setTypeLogementRelationship(null);
	}
	public void ouvrirInspecteur() {
		if (inspecteur == null) {
			inspecteur = new InspecteurPoste(editingContext());
			inspecteur.init();
		} 
		inspecteur.afficher();
		NSNotificationCenter.defaultCenter().postNotification(CHANGER_POSTE,currentPoste());
	}
	public void afficherStructures() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(editingContext());
		if (structure != null) {
			currentPoste().setToStructureRelationship(structure);
		}
	}
	//IMPRIMER : Imprime la liste des postes affiches
	public void imprimer() {
		try {
			if (displayGroup().selectedObjects().count() == 1) {
				boolean estFichePoste = (vueOnglets.getSelectedIndex() == FICHE_POSTE);
				String titre = "Impression de la Fiche de Poste";
				if (!estFichePoste) {
					titre = "Impression de la Fiche Lolf";
				}
				Class[] classeParametres =  new Class[] {EOGlobalID.class, Boolean.class};
				Object[] parametres = new Object[]{editingContext().globalIDForObject(currentPoste()),new Boolean(estFichePoste)};
				UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerPoste",classeParametres,parametres,"Fiche_" + currentPoste().posCode() ,titre);
			} else {
				Class[] classeParametres =  new Class[] {NSArray.class};
				Object[] parametres = new Object[]{Utilitaires.tableauDeGlobalIDs(displayGroup().selectedObjects(),editingContext())};
				UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerListePostes",classeParametres,parametres,"Postes","Impression de la liste des Postes");
			}
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}

	public void supprimer() {
		
		try {
			currentPoste().setTemValide("N");
			editingContext().saveChanges();
		}
		catch (Exception e) {
			editingContext().revert();
			e.printStackTrace();
		}
		
	}

	public void rechercher() {
		if (!preparationFenetre) {
			component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			modifierDisplayGroupAvecObjets(fetcherObjets());
			component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}
	/** Affiche un dialogue d'aide pour la saisie du type de logement */
	public void afficherAideLogement() {
		EODialogs.runInformationDialog("", "Cette information est utilisée pour le calcul de la PFR");
	}
	// Notifications
	public void getLogement(NSNotification aNotif) {
		ajouterRelation(currentPoste(), aNotif.object(), "typeLogement");
		updaterDisplayGroups();
	}
	public void lockSaisie(NSNotification aNotif) {
		super.lockSaisie(aNotif);
		if (aNotif.object() instanceof FicheDePoste) {
			vueOnglets.getComponentAt(FICHE_LOLF).setEnabled(false);
			vueOnglets.getComponentAt(POSTE_PFR).setEnabled(false);
		} else if (aNotif.object() instanceof FicheLolf) {
			vueOnglets.getComponentAt(FICHE_POSTE).setEnabled(false);
			vueOnglets.getComponentAt(POSTE_PFR).setEnabled(false);
		} else if (aNotif.object() instanceof GestionPostePFR) {
			vueOnglets.getComponentAt(FICHE_POSTE).setEnabled(false);
			vueOnglets.getComponentAt(FICHE_LOLF).setEnabled(false);
		} else {
			for (int i = 0; i < vueOnglets.getComponentCount(); i++) {
				vueOnglets.getComponentAt(i).setEnabled(false);
			}
		}
	}
	public void unlockSaisie(NSNotification aNotif) {
		super.unlockSaisie(aNotif);
		if (aNotif.object() instanceof FicheDePoste) {
			vueOnglets.getComponentAt(FICHE_LOLF).setEnabled(true);
			vueOnglets.getComponentAt(POSTE_PFR).setEnabled(true);

		} else if (aNotif.object() instanceof FicheLolf) {
			vueOnglets.getComponentAt(FICHE_POSTE).setEnabled(true);
			vueOnglets.getComponentAt(POSTE_PFR).setEnabled(true);
		} else if (aNotif.object() instanceof GestionPostePFR) {
			vueOnglets.getComponentAt(FICHE_POSTE).setEnabled(true);
			vueOnglets.getComponentAt(FICHE_LOLF).setEnabled(true);
		} else {
			for (int i = 0; i < vueOnglets.getComponentCount(); i++) {
				vueOnglets.getComponentAt(i).setEnabled(true);
			}
		}
	}
	// méthodes du controller DG
	public boolean peutRechercher() {
		return modificationEnCours() == false && estLocke() == false;
	}
	public boolean peutSupprimerTypeLogement() {
		return modificationEnCours() && currentPoste().typeLogement() != null;
	}
	public boolean peutValider() {
		return currentPoste() != null && currentPoste().posCode() != null && currentPoste().posLibelle() != null &&
		currentPoste().posDDebut() != null && currentPoste().toStructure() != null;

	}
	public boolean peutSupprimerStructure() {
		return modificationEnCours() && currentPoste().toStructure() != null;
	}

	public boolean peutAjouter() {
		return modeSaisiePossible() && currentUtilisateur.peutGererPostes();
	}
	public boolean peutModifier() {
		return boutonModificationAutorise() && currentUtilisateur.peutGererPostes();
	}
	public boolean peutSupprimer() {
		return boutonModificationAutorise() && currentPoste().estOccupe() == false && currentUtilisateur.peutGererPostes();
	}

	public boolean peutImprimer() {
		int selectedIndex = vueOnglets.getSelectedIndex();
		return boutonModificationAutorise() && (displayGroup().displayedObjects().count() > 1 || (selectedIndex == FICHE_POSTE || selectedIndex == FICHE_LOLF));
	}
	// interface ChangeListener
	//	Change Listener
	public void stateChanged(ChangeEvent e) {
		int selectedIndex = vueOnglets.getSelectedIndex();
		controleurFicheLolf.updaterPourAffichage(selectedIndex == FICHE_LOLF);
		controleurFicheDePoste.updaterPourAffichage(selectedIndex == FICHE_POSTE);
		controleurPostePFR.updaterPourAffichage(selectedIndex == POSTE_PFR);
	}
	// méthodes protégées
	protected void preparerFenetre() {
		preparationFenetre = true;
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());

		// préparer les vues à onglets
		preparerVues();
		vueOnglets.addChangeListener(this);
		listeAffichage.table().addMouseListener(new DoubleClickListener());
		// préparer le popup de recherche des implantations
		// Limiter structures que peut gérer l'agent.
		// Si pas de limitation, prendre toutes les structures de l'établissement
		NSArray structures = agent.structuresGerees();
		if (structures == null || structures.count() == 0) {
			// ajouter une structure fictive pour avoir le libellé "toutes"
			EOStructure structure = new EOStructure();
			structure.setLlStructure("*");
			NSMutableArray temp = new NSMutableArray(EOStructure.rechercherStructuresEtablissements(editingContext()));
			temp.insertObjectAtIndex(structure,0);
			structures = new NSArray(temp);
		}
		vueAgent.setVisible(false);
		displayGroupStructure.setObjectArray(structures);
		// afficher les postes
		MatrixUtilities.selectionnerBouton(matriceRecherchePoste,((ApplicationClient)EOApplication.sharedApplication()).typePosteAuDemarrage());
		super.preparerFenetre();
		displayGroupStructure.updateDisplayedObjects();
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		preparationFenetre = false;
	}
	/** methode a surcharger pour terminer avant fermeture de la fenetre en invoquant
	 * un appel a la methode de la super classe */
	protected void terminer() {
		vueOnglets.removeChangeListener(this);
		if (inspecteur != null) {
			inspecteur.terminer();
		}
	}
	protected void traitementsPourCreation() {
		currentPoste().setTemValide("O");
	}
	protected boolean traitementsPourSuppression() {
		currentPoste().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce poste ?";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherAvecQualifier(editingContext(),EOPoste.ENTITY_NAME,construireQualifierRecherche(),true);
	}

	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey( EOPoste.POS_CODE_KEY,EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return ((ApplicationClient)EOApplication.sharedApplication()).afficherPostesAuDemarrage();
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}

	// méthodes privées
	private EOPoste currentPoste() {
		return (EOPoste)displayGroup().selectedObject();
	}
	private void preparerVues() {
		controleurFicheDePoste = new FicheDePoste();
		controleurFicheDePoste.init();
		GraphicUtilities.swaperViewEtCentrer((JComponent)vueOnglets.getComponentAt(FICHE_POSTE),controleurFicheDePoste.component());
		controleurFicheDePoste.activer();
		controleurFicheDePoste.component().setVisible(true);
		controleurFicheLolf = new FicheLolf();
		controleurFicheLolf.init();
		GraphicUtilities.swaperViewEtCentrer((JComponent)vueOnglets.getComponentAt(FICHE_LOLF),controleurFicheLolf.component());
		controleurFicheLolf.activer();
		controleurFicheLolf.component().setVisible(false);
		controleurPostePFR = new GestionPostePFR();
		GraphicUtilities.swaperViewEtCentrer((JComponent)vueOnglets.getComponentAt(POSTE_PFR),controleurPostePFR.component());
		controleurPostePFR.activer();
		controleurPostePFR.component().setVisible(false);
		vueOnglets.setSelectedIndex(FICHE_POSTE);
	}
	//	 CONSTRUIRE QUALIFIER RECHERCHE
	private EOQualifier construireQualifierRecherche() {
		EOQualifier myQualif1 = null;
		NSMutableArray	args = new NSMutableArray();
		NSMutableArray	mesQualifiers = new NSMutableArray();
		// pour ramener à une date sans prendre en compte les heures
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));

		// Type d'emploi (0 : En cours, 1 : Occupes, 2 : Vacants, 3 :  Tous)
		switch (MatrixUtilities.colonneSelectionnee(matriceRecherchePoste)) {
		case 0 : 	myQualif1 = SuperFinder.qualifierPourPeriode(EOPoste.POS_D_DEBUT_KEY,today,EOPoste.POS_D_FIN_KEY, today);break;		
		case 1 : 	myQualif1 = SuperFinder.qualifierPourPeriode("tosAffectationDetail.adeDDebut",today,"tosAffectationDetail.adeDFin",today);break;			
		case 2 : 	myQualif1 = qualifierPostesVacants();break;			
		case 3 : 	myQualif1 = EOQualifier.qualifierWithQualifierFormat("",null);break;
		}
		if (myQualif1 != null) {
			mesQualifiers.addObject(myQualif1);
		}
		String stringQualifier = "";
		if (codePourRecherche() != null && !"".equals(codePourRecherche())) {
			stringQualifier = "posCode like '*" + codePourRecherche().toUpperCase() + "*'";
			myQualif1 = EOQualifier.qualifierWithQualifierFormat(stringQualifier,null);	
			mesQualifiers.addObject(myQualif1);
		}

		// Qualification de la structure
		if (structurePourRecherche() != null && !"".equals(structurePourRecherche()) && structurePourRecherche().equals("*") == false) {
			args.removeAllObjects();args.addObject(structurePourRecherche());
			myQualif1 = EOQualifier.qualifierWithQualifierFormat("toStructure.llStructure = %@",args);
			mesQualifiers.addObject(myQualif1);
		}
		EOQualifier qualifierRecherche = (EOQualifier)new EOAndQualifier(mesQualifiers);
		LogManager.logDetail("GestionPostes - rechercher qualifier : " + qualifierRecherche);
		return qualifierRecherche;
	}
	private EOQualifier qualifierPostesVacants() {
		// pour ramener à une date sans prendre en compte les heures
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("posDFin = nil OR posDFin > %@",new NSArray(today));
		NSMutableArray qualifiers = new NSMutableArray(qualifier);
		qualifier = EOPoste.qualifierPourPostesVacants(editingContext(),today);
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}
		return new EOAndQualifier(qualifiers);
	}
	// Classe d'ecoute pour les doubleclics dans la table view  */
	public class DoubleClickListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				if (!modificationEnCours()) {
					ouvrirInspecteur();
				}
			}
		}
	}
}

/*
 * Created on 7 avr. 2006
 *
 * Gestion de la fiche Lolf
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.postes;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.budget.EOLolfNomenclatureDepense;
import org.cocktail.mangue.modele.mangue.lolf.EOFctSilland;
import org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf;
import org.cocktail.mangue.modele.mangue.lolf.EORepartLolfNomen;
import org.cocktail.mangue.modele.mangue.lolf.EORepartSilland;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;


/** Gestion de la fiche Lolf
 * @author christine
 *
 */
// 18/01/2011 - Adaptation Netbeans
public class FicheLolf extends GestionFiche {
	public EODisplayGroup displayGroupSilland, displayGroupLolf;
	public EOTable listeFonctions,listeNomenclatures;
	public EOView vueBoutonsModificationSilland,vueBoutonsValidationSilland,vueBoutonsModificationLolf,vueBoutonsValidationLolf;
	private boolean modificationSillandEnCours,modificationNomenclatureEnCours;
	
	// 18/01/2011
	public void init() {
		EOArchive.loadArchiveNamed("FicheLolf", this, "org.cocktail.mangue.client.postes.interfaces", this.disposableRegistry());
	}
	// Accesseurs
	public boolean modificationNomenclatureEnCours() {
		return modificationNomenclatureEnCours;
	}
	public void setModificationNomenclatureEnCours(boolean modificationNomenclatureEnCours) {
		this.modificationNomenclatureEnCours = modificationNomenclatureEnCours;
	}
	public boolean modificationSillandEnCours() {
		return modificationSillandEnCours;
	}
	public void setModificationSillandEnCours(boolean modificationSillandEnCours) {
		this.modificationSillandEnCours = modificationSillandEnCours;
	}
	// actions
	public void ajouter() {
		super.ajouter();
		masquerBoutons(0);
	}
	public void modifier() {
		super.modifier();
		masquerBoutons(0);
	}
	public void annuler() {
		super.annuler();
		afficherBoutons();
	}
	public void imprimer() {
		try {
			Class[] classeParametres =  new Class[] {EOGlobalID.class};
			Object[] parametres = new Object[]{editingContext().globalIDForObject(currentFiche())};
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerFicheLolf",classeParametres,parametres,"FicheDePoste_" + currentFiche().toPoste().posCode() ,"Impression de la Fiche Lolf");
		
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	public void ajouterFonctionSilland() {
		LogManager.logDetail("FicheLolf - ajouterFonctionSilland");
		displayGroupSilland.insert();
		currentRepartSilland().initAvecFicheLolf(currentFiche());
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		masquerBoutons(1);
	}
	public void modifierFonctionSilland() {
		LogManager.logDetail("FicheLolf - modifierFonctionSilland");
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		masquerBoutons(1);
	}
	public void supprimerFonctionSilland() {
		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer cette fonction Silland ?","Oui","Non")) {
			currentRepartSilland().supprimerRelations();
			editingContext().deleteObject(currentRepartSilland());
			try {
				editingContext().saveChanges();
				LogManager.logDetail("FicheLolf - supprimerFonctionSilland");
			} catch (Exception e) {
				editingContext().revert();
			} 
			updaterDisplayGroups();
         }
	}
	public void afficherFonctionSilland() {
		UtilitairesDialogue.afficherDialogue(this,"FctSilland","getFonctionSilland",false,null,true);
	}
	public void selectionnerNomenclature() {
		LogManager.logDetail("FicheLolf - selectionnerNomenclature");
		EOLolfNomenclatureDepense nomenclature = ChoixNomenclature.sharedInstance().selectionnerNomenclature(editingContext(),currentRepartSilland().toFctSilland());
		if (nomenclature != null) {
			currentRepartLolf().addObjectToBothSidesOfRelationshipWithKey(nomenclature,"toLolfNomenclature");
		}
	}
	public void ajouterNomenclature() {
		LogManager.logDetail("FicheLolf - ajouterNomenclature");
		displayGroupLolf.insert();
		currentRepartLolf().initAvecRepartSilland(currentRepartSilland());
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		masquerBoutons(2);
	}
	public void modifierNomenclature() {
		LogManager.logDetail("FicheLolf - modifierNomenclature");
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		masquerBoutons(2);
	}
	public void supprimerNomenclature() {
		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer cette Nomenclature ?","Oui","Non")) {
			currentRepartLolf().supprimerRelations();
			editingContext().deleteObject(currentRepartLolf());
			try {
				editingContext().saveChanges();
				LogManager.logDetail("FicheLolf - supprimerNomenclature");
			} catch (Exception e) {
				editingContext().revert();
			} 
			updaterDisplayGroups();
         }
	}
	// Notifications
	public void getFonctionSilland(NSNotification aNotif){
		EOGlobalID globalID = (EOGlobalID)aNotif.object();
		EOFctSilland fctSilland = null;
		if (globalID != null) {
			fctSilland = (EOFctSilland)SuperFinder.objetForGlobalIDDansEditingContext(globalID,editingContext());
		}
		
		NSArray lolfNomens = EORepartLolfNomen.findForRepartSilland(editingContext(), currentRepartSilland());
		
		if (fctSilland != currentRepartSilland().toFctSilland() && lolfNomens.count() > 0) {
			int total = lolfNomens.count();
	        for (int i = 0; i < total; i++) {
	          EORepartLolfNomen uneRepart = (EORepartLolfNomen) lolfNomens.objectAtIndex(0);
	          currentRepartSilland().removeObjectFromBothSidesOfRelationshipWithKey(uneRepart,"tosRepartLolfNomen");
	          editingContext().deleteObject(uneRepart);
	        }
		}
		ajouterRelation(currentRepartSilland(),aNotif.object(),"toFctSilland");
	}
	// divers
	public void updaterPourAffichage(boolean estActif) {
		super.updaterPourAffichage(estActif);
		if (estFicheActive()) {
			raffraichirAssociations()	;	// pour forcer le refresh
		}
	}
	// méthodes du controller DG
	public boolean modeSaisiePossible() {
		//return super.modeSaisiePossible() && modificationSillandEnCours == false && modificationNomenclatureEnCours == false;
		return false;	// géré par Feve pour l'instant
	}
	public boolean boutonModificationAutorise() {
		return super.boutonModificationAutorise() && modificationSillandEnCours == false && modificationNomenclatureEnCours == false;
	}
	public boolean peutAjouterFonctionSilland() {
		//return boutonModificationAutorise();
		return false;	// géré par Feve pour l'instant
	}
	public boolean peutAjouterNomenclature() {
		//return boutonModificationAutorise() && currentRepartSilland() != null;
		return false;	// géré par Feve pour l'instant
	}
	public boolean peutModifierFonctionSilland() {
		//return boutonModificationAutorise() && currentRepartSilland() != null;
		return false;	// géré par Feve pour l'instant
	}
	public boolean peutModifierNomenclature() {
		//return boutonModificationAutorise() && currentRepartLolf() != null;
		return false;	// géré par Feve pour l'instant
	}
	public boolean peutValiderFonctionSilland() {
		return modificationSillandEnCours && currentRepartSilland() != null && currentRepartSilland().rfsQuotite() != null && currentRepartSilland().toFctSilland() != null;
	}
	public boolean peutValiderNomenclature() {
		return modificationNomenclatureEnCours && currentRepartLolf() != null && currentRepartLolf().rrfQuotite() != null && currentRepartLolf().toLolfNomenclature() != null;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		GraphicUtilities.changerTaillePolice(listeFonctions,11);
		GraphicUtilities.rendreNonEditable(listeFonctions);
		GraphicUtilities.changerTaillePolice(listeNomenclatures,11);
		GraphicUtilities.rendreNonEditable(listeNomenclatures);
		afficherBoutons();
	}
	
	protected boolean traitementsAvantValidation() {
		double quotiteTotale = 0.00;
		for (java.util.Enumeration<EORepartSilland> e = displayGroupSilland.displayedObjects().objectEnumerator();e.hasMoreElements();) {
			EORepartSilland repart = e.nextElement();
			quotiteTotale += repart.rfsQuotite().doubleValue();
		}
		if (quotiteTotale > 100.00) {
			EODialogs.runErrorDialog("Erreur","La quotité totale des fonctions Silland ne peut dépasser 100%");
			return false;
		}
		quotiteTotale = 0;
		for (java.util.Enumeration<EORepartLolfNomen> e1 = displayGroupLolf.displayedObjects().objectEnumerator();e1.hasMoreElements();) {
			EORepartLolfNomen repart = e1.nextElement();
			quotiteTotale += repart.rrfQuotite().doubleValue();
		}
		if (quotiteTotale > 100.00) {
			EODialogs.runErrorDialog("Erreur","La quotité totale des Nomenclatures Lolf ne peut dépasser 100%");
			return false;
		}
		return true;
	}
	protected void traitementsApresValidation() {
		super.traitementsApresValidation();
		afficherBoutons();
	}
	protected NSArray fetcherObjets() {
		if (currentPoste() != null) {
			return currentPoste().tosFicheLolf();
		} else {
			return null;
		}
	}
	protected void updaterDisplayGroups() {
		if (estFicheActive()) {
			super.updaterDisplayGroups();
			displayGroupLolf.updateDisplayedObjects();
			displayGroupSilland.updateDisplayedObjects();
		}
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOFicheLolf currentFiche() {
		return (EOFicheLolf)displayGroup().selectedObject();
	}
	private EORepartSilland currentRepartSilland() {
		return (EORepartSilland)displayGroupSilland.selectedObject();
	}
	private EORepartLolfNomen currentRepartLolf() {
		return (EORepartLolfNomen)displayGroupLolf.selectedObject();
	}
	private void afficherBoutons() {
		vueBoutonsModificationSilland.setVisible(true);
		vueBoutonsModificationLolf.setVisible(true);
		vueBoutonsValidationSilland.setVisible(false);
		vueBoutonsValidationLolf.setVisible(false);
		modificationSillandEnCours = false;
		modificationNomenclatureEnCours = false;
		updaterDisplayGroups();
	}
	private void masquerBoutons(int num) {
		switch(num) {
			case 0 : 
				vueBoutonsModificationSilland.setVisible(false);
				vueBoutonsModificationLolf.setVisible(false);
				vueBoutonsValidationSilland.setVisible(false);
				vueBoutonsValidationLolf.setVisible(false);
				break;
			case 1 : 
				vueBoutonsModification.setVisible(false);
				vueBoutonsModificationLolf.setVisible(false);
				modificationSillandEnCours = true;
				vueBoutonsValidationSilland.setVisible(true);
				vueBoutonsValidationLolf.setVisible(false);
				break;
			case 2 : 
				vueBoutonsModification.setVisible(false);
				vueBoutonsModificationSilland.setVisible(false);
				vueBoutonsValidationSilland.setVisible(false);
				vueBoutonsValidationLolf.setVisible(true);
				modificationNomenclatureEnCours = true;
		}
		updaterDisplayGroups();
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.postes;

import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EOPostePFR;
import org.cocktail.mangue.modele.mangue.lolf.EOPoste;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;


// 18/01/2011 - Adaptation Netbeans (suppression de preparerFenetre)
public class GestionPostePFR extends ModelePageComplete {
	private EOPoste currentPoste;
	
	public GestionPostePFR() {
		super();
		EOArchive.loadArchiveNamed("GestionPostePFR", this, "org.cocktail.mangue.client.postes.interfaces", this.disposableRegistry()); // 18/01/2011
		initialiser(false,true);
	}
	public void setPosteID(EOGlobalID globalID) {
		if (editingContext() != null) {
			if (globalID != null) {
				currentPoste = (EOPoste)SuperFinder.objetForGlobalIDDansEditingContext(globalID,editingContext());
			} else {
				currentPoste = null;
			}
			displayGroup().setObjectArray(fetcherObjets());
			updaterDisplayGroups();
		}
	}
	// divers
	public void updaterPourAffichage(boolean estActif) {
		if (estActif) {
			component().setVisible(true);
			updaterDisplayGroups();
		} else {
			component().setVisible(false);
		}
	}
	// méthodes du controller DG
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && currentPoste() != null;
	}
	public boolean peutValider() {
		return modificationEnCours() && currentPostePFR().ppfrCoef() != null && currentPostePFR().ppfrDDebut() != null;
	}
	// Méthodes protégées
	protected void traitementsPourCreation() {
		currentPostePFR().initAvecPoste(currentPoste());
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR", message);

	}
	protected boolean conditionsOKPourFetch() {
		return currentPoste() != null;

	}
	protected NSArray fetcherObjets() {
		if (currentPoste() == null) {
			return null;
		} else {
			return EOPostePFR.postesPFRPourPoste(editingContext(), currentPoste());
		}
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous supprimer ce coefficient de PFR ?";
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("ppfrDDebut", EOSortOrdering.CompareDescending)));
	}
	/** Pas de traitement sp&eacute;cifique */
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected boolean traitementsPourSuppression() {
		currentPostePFR().removeObjectFromBothSidesOfRelationshipWithKey(currentPoste(), "poste");
		deleteSelectedObjects();
		return true;
	}
	protected EOPoste currentPoste() {
		return currentPoste;
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOPostePFR currentPostePFR() {
		return (EOPostePFR)displayGroup().selectedObject();
	}

}

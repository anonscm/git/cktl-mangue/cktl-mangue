/* Gestion des fiches de poste
 * Created on 7 avr. 2006
 *
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.postes;

import java.awt.Font;
import java.awt.Window;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.administration.AffichageLibelle;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste;
import org.cocktail.mangue.modele.mangue.lolf.EOReferensActivite;
import org.cocktail.mangue.modele.mangue.lolf.EOReferensCompetences;
import org.cocktail.mangue.modele.mangue.lolf.EORepartFdpActi;
import org.cocktail.mangue.modele.mangue.lolf.EORepartFdpAutre;
import org.cocktail.mangue.modele.mangue.lolf.EORepartFdpComp;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOTextArea;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;


/** Gestion des fiches de poste
 * @author christine
 *
 */
// 18/01/2011 - Adaptation Netbeans
public class FicheDePoste extends GestionFiche implements ChangeListener {
	public JTabbedPane vueOnglets;
	public EOTable listeActivites,listeAutresActivites,listeCompetences;
	public EOTextArea champTexteAutre;
	public EODisplayGroup displayGroupActivites,displayGroupAutresActivites,displayGroupCompetences;
	private AffichageLibelle controleurPourInfo;
	private static int INDEX_VUE_ACTIVITES = 3;
	private static int INDEX_VUE_COMPETENCES = 4;
	
	// 18/01/2011
	public void init() {
		EOArchive.loadArchiveNamed("FicheDePoste", this, "org.cocktail.mangue.client.postes.interfaces", this.disposableRegistry());
	}
	public void connectionWasBroken() {
		super.connectionWasBroken();
		if (controleurPourInfo != null) {
			controleurPourInfo.fermerFenetre();
		}
	}
	// méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroupActivites) {
			modifierLibelle(true);
		} else if (aGroup == displayGroupCompetences) {
			modifierLibelle(false);
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
	// actions
	/** Surcharge du parent pour se positionner sur l'onglet info */
	// 18/01/2011
	public void ajouter() {
		vueOnglets.setSelectedIndex(0);
		super.ajouter();
	}
	public void ajouterActivite() {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("code = %@", new NSArray(currentFiche().referensEmplois().code()));
		UtilitairesDialogue.afficherDialogue(this,"ReferensActivites","getActivite",true,qualifier,true);
	}
	public void supprimerActivite() {
		LogManager.logDetail("FicheDePoste - supprimerActivite");
		currentRepartActivite().supprimerRelations();
		editingContext().deleteObject(currentRepartActivite());
		displayGroupActivites.updateDisplayedObjects();
	}
	public void afficherActivite() {
		afficherLibelle(true);
	}
	public void ajouterAutreActivite() {
		LogManager.logDetail("FicheDePoste - ajouterAutreActivite");
		// les éléments du DG sont classés par ordre croissant
		int position = 1;
		if (displayGroupAutresActivites.displayedObjects().count() > 0) {
			position = ((EORepartFdpAutre)displayGroupAutresActivites.displayedObjects().lastObject()).fauPosition().intValue() + 1;
		}
		displayGroupAutresActivites.insert();
		currentRepartAutreActivite().initAvecFicheDePoste(currentFiche(),position);
		champTexteAutre.textArea().setCaretPosition(0);
	}
	public void supprimerAutreActivite() {
		currentRepartAutreActivite().supprimerRelations();
		editingContext().deleteObject(currentRepartAutreActivite());
		displayGroupAutresActivites.updateDisplayedObjects();
	}
	public void ajouterCompetence() {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("code = %@", new NSArray(currentFiche().referensEmplois().code()));
		UtilitairesDialogue.afficherDialogue(this,"ReferensCompetences","getCompetence",true,qualifier,true);
	}
	public void supprimerCompetence() {
		LogManager.logDetail("FicheDePoste - supprimerCompetence");
		currentRepartCompetence().supprimerRelations();
		editingContext().deleteObject(currentRepartCompetence());
		displayGroupCompetences.updateDisplayedObjects();
	}
	public void afficherCompetence() {
		afficherLibelle(false);
	}
	public void supprimerReferensEmploi() {
		LogManager.logDetail("FicheDePoste - supprimerEmploiType");
		currentFiche().removeObjectFromBothSidesOfRelationshipWithKey(currentFiche().referensEmplois(),"referensEmplois");
	}
	public void afficherReferensEmploi() {
		UtilitairesDialogue.afficherDialogue(this,"ReferensEmplois","getReferensEmploi",true,null,false);
	}
	public void imprimerFicheDePoste() {
		try {
			Class[] classeParametres =  new Class[] {EOGlobalID.class};
			Object[] parametres = new Object[]{editingContext().globalIDForObject(currentFiche())};
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerFicheDePoste",classeParametres,parametres,"FicheDePoste_" + currentFiche().toPoste().posCode() ,"Impression de la Fiche de Poste");
		
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	// Notifications
	public void getReferensEmploi(NSNotification aNotif) {
		ajouterRelation(currentFiche(),aNotif.object(),"referensEmplois");
	}
	public void getActivite(NSNotification aNotif) {
		if (aNotif.object() != null) {
			EOReferensActivite activite = (EOReferensActivite)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			if (((NSArray)displayGroupActivites.displayedObjects().valueForKey("referensActivite")).containsObject(activite) == false) {
				// les éléments du DG sont classés par ordre croissant
				int position = 1;
				if (displayGroupActivites.displayedObjects().count() > 0) {
					position = ((EORepartFdpActi)displayGroupActivites.displayedObjects().lastObject()).rfaPosition().intValue() + 1;
				}
				displayGroupActivites.insert();	
				currentRepartActivite().initAvecReferensActiviteEtFicheDePoste(activite,currentFiche(),position);
				LogManager.logDetail("FicheDePoste - getActivite pour repart :" + currentRepartActivite());
				modifierLibelle(true);
			}
		}
	}
	public void getCompetence(NSNotification aNotif) {
		if (aNotif.object() != null) {
			EOReferensCompetences competence = (EOReferensCompetences)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			if (((NSArray)displayGroupCompetences.displayedObjects().valueForKey("referensCompetence")).containsObject(competence) == false) {
				//les éléments du DG sont classés par ordre croissant
				int position = 1;
				if (displayGroupCompetences.displayedObjects().count() > 0) {
					position = ((EORepartFdpComp)displayGroupCompetences.displayedObjects().lastObject()).rfcPosition().intValue() + 1;
				}
				displayGroupCompetences.insert();
				currentRepartCompetence().initAvecReferensCompetenceEtFicheDePoste(competence,currentFiche(),position);
				LogManager.logDetail("FicheDePoste - getCompetence pour repart :" + currentRepartCompetence());
				modifierLibelle(false);
			}
		}
	}
	// méthodes du controller DG
	public String longueurAutreActivite() {
		if (currentRepartAutreActivite() != null && currentRepartAutreActivite().fauActiviteAutre() != null) {
			return "" + currentRepartAutreActivite().fauActiviteAutre().length() + "/" + EORepartFdpAutre.LONGUEUR_TEXTE;
		} else {
			return "";
		}
	}
	public String longueurContexte() {
		if (currentFiche() != null && currentFiche().fdpContexteTravail() != null) {
			return "" + currentFiche().fdpContexteTravail().length() + "/" + EOFicheDePoste.LONGUEUR_CONTEXTE;
		} else {
			return "";
		}
	}
	public String longueurMission() {
		if (currentFiche() != null && currentFiche().fdpMissionPoste() != null) {
			return "" + currentFiche().fdpMissionPoste().length() + "/" + EOFicheDePoste.LONGUEUR_MISSION;
		} else {
			return "";
		}
	}
	public boolean peutValider() {
		return currentFiche() != null && currentFiche().fdpDDebut() != null;
	
	}
	public boolean peutSupprimerActivite() {
		return modificationEnCours() && currentRepartActivite() != null;
	}
	public boolean peutSupprimerAutreActivite() {
		return false;//modificationEnCours() && currentRepartAutreActivite() != null;
	}
	public boolean peutSupprimerCompetence() {
		return false;//modificationEnCours() && currentRepartCompetence() != null;
	}
	public boolean peutSupprimerReferensEmploi() {
		return false;//modificationEnCours() && currentFiche().referensEmplois() != null;
	}
	
	// interface ChangeListener
	// pour gérer correctement les données affichées dans la palette de libelleé
	public void stateChanged(ChangeEvent e) {
		int index = vueOnglets.getSelectedIndex();
		if (index == INDEX_VUE_ACTIVITES) {
			modifierLibelle(true);
		} else if (index == INDEX_VUE_COMPETENCES) {
			modifierLibelle(false);
		}
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		listeAffichage.table().addMouseListener(new DoubleClickListener());
		vueOnglets.setSelectedIndex(0);
		vueOnglets.setFont(new Font("Helvetica",Font.BOLD,11));
		GraphicUtilities.changerTaillePolice(listeActivites,11);
		GraphicUtilities.rendreNonEditable(listeActivites);
		GraphicUtilities.changerTaillePolice(listeAutresActivites,11);
		GraphicUtilities.rendreNonEditable(listeAutresActivites);
		GraphicUtilities.changerTaillePolice(listeCompetences,11);
		GraphicUtilities.rendreNonEditable(listeCompetences);
		vueOnglets.addChangeListener(this);
	}
	protected NSArray fetcherObjets() {
		if (currentPoste() != null) {
			return currentPoste().tosFicheDePoste();
		} else {
			return null;
		}
	}
	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		displayGroupActivites.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("rfaPosition", EOSortOrdering.CompareAscending)));
		displayGroupAutresActivites.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("fauPosition", EOSortOrdering.CompareAscending)));
		displayGroupCompetences.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("rfcPosition", EOSortOrdering.CompareAscending)));
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOFicheDePoste currentFiche() {
		return (EOFicheDePoste)displayGroup().selectedObject();
	}
	private EORepartFdpActi currentRepartActivite() {
		return (EORepartFdpActi)displayGroupActivites.selectedObject();
	}
	private EORepartFdpComp currentRepartCompetence() {
		return (EORepartFdpComp)displayGroupCompetences.selectedObject();
	}
	private EORepartFdpAutre currentRepartAutreActivite() {
		return (EORepartFdpAutre)displayGroupAutresActivites.selectedObject();
	}
	private void afficherLibelle(boolean estActivite) {
		LogManager.logDetail("FicheDePoste - afficherLibelle pour activite : " + estActivite);
		if (controleurPourInfo == null) {
			Window window = EOApplication.sharedApplication().windowObserver().activeWindow();
			int Y = window.getHeight() + window.getY() + 5;
			controleurPourInfo = new AffichageLibelle(window.getX(),Y,"","");
			controleurPourInfo.afficherFenetre();
		} else {
			controleurPourInfo.changerEtat();
		}
		modifierLibelle(estActivite);
	}
	private void modifierLibelle(boolean estActivite) {
		if (controleurPourInfo != null) {
			String titre = estActivite ? "Activité" : "Compétence";
			String message = "";
			if (estActivite) {
				if (currentRepartActivite() != null && currentRepartActivite().referensActivite() != null) {
					message = currentRepartActivite().referensActivite().intitulactivite();
				}
			} else {
				if (currentRepartCompetence() != null && currentRepartCompetence().referensCompetence() != null) {
					message = currentRepartCompetence().referensCompetence().intitulcomp();
				}
			}
			controleurPourInfo.setLabelText(titre);
			controleurPourInfo.setText(message);
			LogManager.logDetail("FicheDePoste - modifierLibelle : " + titre);
		}
	}
}

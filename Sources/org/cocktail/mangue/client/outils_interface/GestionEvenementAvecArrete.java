
//GestionEvenementAvecArrete.java
//Mangue

//Created by Christine Buttin on Thu Sep 01 2005.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.


/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import java.awt.Font;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.GestionnaireEvenementIndividu;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Arrete;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.DureeAvecArrete;
import org.cocktail.mangue.modele.IValidite;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTextArea;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
/** Modelise la gestion des evenements avec arrete<BR>
 * Les objets metiers doivent h&eacute;riter de la classe DureeAvecArrete<BR>
 * Les controles de validation sont faits dans les objets metiers
 * @author christine
 *
 * 
 */
// 26/01/2011 - Adaptation Netbeans
public abstract class GestionEvenementAvecArrete extends ModelePageAvecIndividu {
	public EOView vueArretes,vueArrete,vueArreteAnnulation,contenuVueArrete,contenuVueArreteAnnulation;
	public EOTextArea vueTexte;
	private GestionArrete gestionArrete,gestionArreteAnnulation;
	private String entite;
	private EOAbsences absenceCourante;	// l'absence est initialisée lors de l'accès à une des méthodes de création/sélection
	// elle n'est modifiée qu'au moment de la validation

	public GestionEvenementAvecArrete(String entite) {
		super();
		System.out
				.println("GestionEvenementAvecArrete.GestionEvenementAvecArrete()");
		this.entite = entite;
	}
	public void setModificationEnCours(boolean aBool) {
		super.setModificationEnCours(aBool);
		if (gestionArrete != null) {
			gestionArrete.setModificationEnCours(aBool);
			gestionArrete.setModeCreation(modeCreation());
		}
		if (gestionArreteAnnulation != null) {
			gestionArreteAnnulation.setModificationEnCours(aBool);
			gestionArreteAnnulation.setModeCreation(modeCreation());
		}
	}
	//  méthodes du délégué du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		super.displayGroupDidChangeSelection(aGroup);
		if (aGroup == displayGroup()) {
			if (currentEvenement() == null || modeCreation()) {
				absenceCourante = null;
			} else {
				try {
					absenceCourante = GestionnaireEvenementIndividu.rechercherEvenementPourDuree(currentEvenement());

				} catch (Exception e) {
					// on ne fait rien i.e on ne gère pas l'absence courante
				}
			}
			preparerVuesArrete(false);
		}
	}
	// Accesseurs
	public String compteurCommentaire() {
		if (currentEvenement() != null && currentEvenement().commentaire() != null) {
			return "" + currentEvenement().commentaire().length() + "/2000";
		} else {
			return "";
		}
	}
	// Notifications
	public void gererModificationArrete(NSNotification aNotif) {
		GestionArrete gestionnaire = (GestionArrete)aNotif.object();
		setEdited(true);
		if (gestionnaire != null && gestionnaire == gestionArrete) {
			if (vueArreteAnnulation != null && contenuVueArreteAnnulation != null) {
				afficherVueAnnulation();
			}
		}
		controllerDisplayGroup().redisplay();
	}
	public void imprimerArrete(NSNotification aNotif) {
		GestionArrete gestionnaire = (GestionArrete)aNotif.object();
		if (gestionnaire != null) {
			if (gestionnaire == gestionArrete && modificationEnCours()) {
				if (isEdited()) {	// si l'utilisateur a fait des modifications
					// vérifier auparavant si on peut valider, sinon afficher un message à l'utilisateur
					if (!peutValider()) {
						EODialogs.runInformationDialog("Erreur", "Impossible d'imprimer l'arrêté.\nToutes les données nécessaires pour ce congé ne sont pas saisies");
						return;
					}
					currentEvenement().setNoArrete(gestionArrete.arreteCourant().numero());
					currentEvenement().setDateArrete(gestionArrete.arreteCourant().date());
					if (modificationEnCours()) {
						valider();
					}
					if (modificationEnCours()){	// problème lors de la validation
						LogManager.logDetail("Impression de l'arrêté - Erreur lors de l'enregistrement des données");
						EODialogs.runInformationDialog("Erreur", "Impossible d'imprimer l'arrêté.\nProblème lors de l'enregistrement des données");
						return;
					}
				}
				// Sélectionner les destinataires
				UtilitairesDialogue.afficherDialogue(this,"Destinataire", "getDestinataires", false,true,null,true,true);

			} else if (gestionnaire == gestionArreteAnnulation) {
				LogManager.logDetail("Impression de l'arrêté " + gestionArreteAnnulation.arreteCourant());
			}
		}
	}
	/** recupere les destinataires et lance l'impression de l'arrete */
	public void getDestinataires(NSNotification aNotif) {
		Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class,Boolean.class};
		Object[] parametres = new Object[]{editingContext().globalIDForObject(currentEvenement()),(NSArray)aNotif.object(),new Boolean(false)};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerArretePourEvenement",classeParametres,parametres,"ArreteEvenement_" + currentEvenement().noArrete(),"Impression de l'arrêté");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}	
	}
	// méthodes du controller DG
	public boolean peutValider() {
		if (currentEvenement() == null || currentEvenement().dateDebut() == null) {
			return false;
		}
		if (currentEvenement().dateFin() != null && DateCtrl.isBefore(currentEvenement().dateFin(),currentEvenement().dateDebut())) {
			return false;
		}
		return super.peutValider();

	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		preparerVuesArrete(true);
		if (vueTexte != null) {
			Font font = vueTexte.getFont();
			vueTexte.setFont(new Font(font.getName(),font.getStyle(),11));
		}
	}
	protected void terminer() { 
		gestionArrete.terminer();
		if (gestionArreteAnnulation != null) {
			gestionArreteAnnulation.terminer();
		}
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("gererModificationArrete", new Class[] {NSNotification.class}),
				GestionArrete.ARRETE_MODIFIE,null);
		// s'enregistrer pour recevoir les notifications d'impression d'arrêté
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("imprimerArrete",new Class[] {NSNotification.class}),GestionArrete.IMPRIMER_ARRETE,null);
	}
	/** m&eacute;thode &agrave; surcharger pour retourner les objets affich&eacute;s par le display group */
	protected NSArray fetcherObjets() {
		if (currentIndividu() != null) {
			return Duree.rechercherDureesPourIndividu(editingContext(),entite,currentIndividu());
		} else {
			return null;
		}
	}
	/** methode a surcharger pour parametrer le display group (tri ou filtre) */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareDescending)));
	}
	/** traitements a executer suite a la creation d'un objet */
	protected void traitementsPourCreation() {
		currentEvenement().initAvecIndividu(currentIndividu());
		try {
			absenceCourante = GestionnaireEvenementIndividu.creerEvenementPourIndividu(currentEvenement(),true);
		} catch (Exception e) {
			LogManager.logException(e);
		}
	}
	/** traitements a executer avant la destruction d'un objet */
	protected boolean traitementsPourSuppression() {
		if (absenceCourante != null) {	// dans certains cas, on ne gère pas d'absence courante
			try {
				GestionnaireEvenementIndividu.supprimerEvenement(absenceCourante,(absenceCourante instanceof IValidite) == false);
			} catch (Exception e) {
				LogManager.logException(e);
			}
		}
		if (currentEvenement() instanceof IValidite) {
			((IValidite)currentEvenement()).setEstValide(false);
		} else {
			currentEvenement().supprimerRelations();
			deleteSelectedObjects();
		}
		preparerVuesArrete(false);
		return true;
	}
	/** retourne le message de confirmation aupr&egrave;s de l'utilisateur avant d'effectuer une suppression  */
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cet élément ?";
	}
	/** affiche un message suite &agrave; une exception de validation */
	protected  void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	/** m&eacute;thode &agrave; surcharger pour faire des traitements avant l'enregistrement des donn&eacute;es dans la base
	 * retourne true si on peut enregistrer */
	protected boolean traitementsAvantValidation() {
		if (currentEvenement() != null) {
			Arrete arrete = recupererArrete();
			if (arrete != null) {
				currentEvenement().setDateArrete(arrete.date());
				currentEvenement().setNoArrete(arrete.numero());
			}
		}
		if (absenceCourante != null) {
			try {
				GestionnaireEvenementIndividu.modifierEvenementAvecDuree(absenceCourante,currentEvenement(),DateCtrl.nbJoursEntre(currentEvenement().dateDebut(),currentEvenement().dateFin(),true));
			} catch (Exception e) {
				LogManager.logException(e);
			}
		}
		return true;
	}
	/** m&eacute;thode &agrave; surcharger pour faire des traitements apr&egrave;s un revert des donn&eacute;es dans la base
	 * Dans la m&eacute;thode surcharg&eacute;e il est imp&eacute;ratif d'appeler super */
	protected  void traitementsApresRevert() {
		super.traitementsApresRevert();
		preparerVuesArrete(false);
	}
	protected void traitementsApresValidation() {
		// 04/06/09 -Pour corriger le problème des arrêtés de temps partiel qui n'étaient pas imprimables après une création
		if (modeCreation()) {
			preparerVuesArrete(false);
		}
		super.traitementsApresValidation();
		if (absenceCourante != null && absenceCourante.isFault()) {		// Il se peut qu'au cours d'un validateForSave, l'absence ait été transformée en fautHandler
			absenceCourante.willRead();
		}
	}
	/** m&eacute;thode &agrave; surcharger pour faire des traitements apr&egrave;s une suppression de donn&eacute;es dans la base
	 * Dans la m&eacute;thode surcharg&eacute;e il est imp&eacute;ratif d'appeler super */
	protected void traitementsApresSuppression() {
		super.traitementsApresSuppression();
		absenceCourante = null;
		preparerVuesArrete(false);
	}
	protected DureeAvecArrete currentEvenement() {
		return (DureeAvecArrete)displayGroup().selectedObject();
	}

	/** retourne l'arrete */
	protected Arrete recupererArrete() {
		return gestionArrete.arreteCourant();
	}
	/** retourne l'arrete d'annulation si il existe ou null */
	protected Arrete recupererArreteAnnulation() {
		if (gestionArreteAnnulation != null) {
			return gestionArreteAnnulation.arreteCourant();
		} else {
			return null;
		}
	}
	/** retourne le type de gestion de l'arrete (etablissement, ...) */
	protected abstract EOTypeGestionArrete typeGestionArrete();
	/** retourne true si la signature de l'arrete doit etre geree */
	protected abstract boolean supporteSignature();
	/** retourne l'arrete courant : la surclasse doit l'allouer et l'initialiser si necessaire */
	protected abstract Arrete arreteCourant();
	/** retourne l'arrete d'annulation courant : la surclasse doit l'allouer et l'initialiser si necessaire */
	protected abstract Arrete  arreteAnnulationCourant();

	// méthodes privées
	// n'affiche les vues d'arrêtés que si les conditions sont remplies
	private void preparerVuesArrete(boolean preparerComposants) {
		if (preparerComposants) {
			gestionArrete = new GestionArrete(editingContext());
			gestionArrete.setModificationEnCours(false);
			contenuVueArrete.add(gestionArrete.component());	
			if (vueArreteAnnulation != null && contenuVueArreteAnnulation != null) {
				gestionArreteAnnulation = new GestionArrete(editingContext());
				gestionArreteAnnulation.setModificationEnCours(false);
				contenuVueArreteAnnulation.add(gestionArreteAnnulation.component());	
			}
		}
		gestionArrete.afficherArrete(arreteCourant(), typeGestionArrete(), supporteSignature(),true);
		if (vueArreteAnnulation != null) {
			afficherVueAnnulation();
		}
	}
	private void afficherVueAnnulation() {
		Arrete arrete = gestionArrete.arreteCourant();
		//	n'afficher la vue que si l'arrêté principal est signé ou comporte un numéro ou une date d'arrêté
		boolean visible = arrete != null && (arrete.numero() != null || arrete.date() != null);
		vueArreteAnnulation.setVisible(visible);
		if (visible) {
			gestionArreteAnnulation.afficherArrete(arreteAnnulationCourant(),null,false,true);
		}
	}
}

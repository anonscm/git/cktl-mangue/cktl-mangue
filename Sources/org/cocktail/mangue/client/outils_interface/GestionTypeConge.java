/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import javax.swing.JComboBox;

import org.cocktail.client.components.ArchiveWithDisplayGroup;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.conges.EOCgntRaisonFamPerso;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;

/** Gere la selection d'yn type de conge dans un popup. Permet de preciser si on souhaite les conges
 * pour titulaires, contractuels, tous. Travaille dans le meme editing context que l'appelant. Peut recevoir la liste des types de conges
 * depuis le composant parent. Communique avec les composants qui l'incorporent par une notification dont l'objet est le nom de l'entite selectionnee
 * ou "" quand tout conge est selectionn&eacute;
 * @author christine
 *
 */
// 12/12/2010 - Adaptation Netbeans
// 04/02/2011 - Ajout du congé de solidarité familiale
public class GestionTypeConge extends ArchiveWithDisplayGroup {
	/** Notification envoyee pour changer de type de conge. */
	public static String TYPE_CONGE_HAS_CHANGED = "ChangerTypeConge";
	/** affichage de tous les conges */
	public static String TOUS_CONGES = "TOUS";
	public JComboBox popup;
	// variables statiques permettant de faire correspondre un type congé et une entité à afficher. Dans certains cas, un même contrôleur d'interface
	// peut gérer différents types de congés : par exemple CongeMaladie et CgntMaladie
	// en tête, mettre les types de congés qui supportent le détail de traitement
	// Cette liste est à synchroniser avec celle de org.cocktail.mangue.conges.GestionAbsences
	private static String TYPES_CONGE[] = {"TOUS","MAL","MALNT","CLM","CGM","CGRFP","CLD","AHCUAO3","AHCUAO4","AHCUAO5","AHCUAO6","MALSSTRT","CMATER","CPATER","CFP","ENFANT","ACCSERV","ACCTRAV","ADOPTION","CRCT","CSF","CCP","CIR","RDT"};
	private static String NOM_ENTITES[] = {"","CongeMaladie","CgntMaladie","Clm","CgntCgm",EOCgntRaisonFamPerso.ENTITY_NAME,"Cld","CongeAl3","CongeAl4","CongeAl5","CongeAl6","CongeMaladieSsTrt","CongeMaternite","CongePaternite","CongeFormation","CongeGardeEnfant","CongeAccidentServ","CgntAccidentTrav","CongeAdoption","Crct","CongeSolidariteFamiliale","Ccp","CongeInsufRes","Rdt"};
	private static String libelles[] = {""," de maladie"," de maladie non titulaire"," de longue maladie"," de grave maladie", "pour raisons personnelles"," de longue durée"," hospitalo universitaire art26-7 alinéa 3"," hospitalo universitaire art26-7 alinéa 4"," hospitalo universitaire art26-7 alinéa 5"," hospitalo universitaire art26-7 alinéa 6"," de maladie sans traitement"," de maternité"," de paternité"," de formation"," de garde des enfants"," d'accident de service"," d'accident du travail"," d'adoption"," de conversion et de recherche thématique"," de solidarité familiale", " pour convenance personnelle", " pour insuffisance de résultat", " pour redoublement avec traitement"};
	private static String LIBELLE_TOUS = "Tous les congés";
	private String typeConge;

	// constructeurs
	/** constructeur
	 * @param editingContext editing context dans lequel travailler
	 * @param typeConge type de conge a afficher au demarrage
	 */
	public GestionTypeConge(EOEditingContext editingContext,String typeConge) {
		super(editingContext,"GestionTypeConge","org.cocktail.mangue.client.outils_interface.interfaces");	// 12/12/2010
		this.typeConge = typeConge;
	}
	/** m&eacute;thode d'initialisation */
	public void init() {
		super.init();
		if (typeConge() != null && typeConge.equals(TOUS_CONGES) == false) {
			NSNotificationCenter.defaultCenter().postNotification(TYPE_CONGE_HAS_CHANGED,nomEntiteSelectionnee());
		}
		if (popup != null) {
			popup.setMaximumRowCount(TYPES_CONGE.length);
		}
	}
	// Accesseurs
	public String libelleTypeConge() {
		if (typeConge == null) {
			return null;
		} else if (typeConge.equals(TOUS_CONGES)) {
			return LIBELLE_TOUS;
		} else {
			return ((EOTypeAbsence)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), EOTypeAbsence.ENTITY_NAME, INomenclature.CODE_KEY,typeConge)).libelleLong();
		}
	}
	public void setLibelleTypeConge(String aString) {
		if (aString == null) {	
			typeConge = null;
			return;
		}
		if (aString.equals(LIBELLE_TOUS)) {
			typeConge = TOUS_CONGES;
		} else {
			EOTypeAbsence typeAbsence = (EOTypeAbsence)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), EOTypeAbsence.ENTITY_NAME,INomenclature.LIBELLE_LONG_KEY, aString);
			typeConge = typeAbsence.code();
		}
		LogManager.logDetail("GestionTypeConge - changementConge, typeConge - " + typeConge);
		NSNotificationCenter.defaultCenter().postNotification(TYPE_CONGE_HAS_CHANGED,nomEntiteSelectionnee());
	}
	public String typeConge() {
		return typeConge;
	}
	public String nomEntiteSelectionnee() {
		return NOM_ENTITES[indexSelection()];
	}
	public String nomEntitePour(String typeConge) {
		return NOM_ENTITES[indexPour(typeConge)];
	}
	public int indexSelection() {
		return indexPour(typeConge);
	}
	public int indexPour(String typeConge) {
		for (int i = 0; i < TYPES_CONGE.length; i++) {
			if (TYPES_CONGE[i].equals(typeConge)) {
				return i;
			} 
		}
		return -1;
	}
	public String libelleSelectionne() {
		int i = indexSelection();
		if (i == 0) {
			return "de " + LIBELLE_TOUS;
		} else {
			return "des congés " + libelles[i];
		}
	}
	public boolean estSelectionTousConges() {
		return typeConge != null && typeConge.equals("TOUS");
	}
	public void  setTypesConges(NSArray anArray) {
		NSMutableArray types = new NSMutableArray(anArray);
		// on trie avant pour pouvoir rajouter un type en tête du tableau
		EOSortOrdering.sortArrayUsingKeyOrderArray(types, Nomenclature.SORT_ARRAY_LIBELLE);
		// créer un type absence bidon
		EOTypeAbsence typeTous = new EOTypeAbsence();
		typeTous.setCode(TOUS_CONGES);
		typeTous.setLibelleCourt(LIBELLE_TOUS);
		typeTous.setLibelleLong(LIBELLE_TOUS);
		typeTous.setCongeLegal(CocktailConstantes.VRAI);
		types.insertObjectAtIndex(typeTous,0);
	
		displayGroup().setObjectArray(types);
		displayGroup().updateDisplayedObjects();
	}
	/** retourne la liste de tous les types de conges affiches dans le popup */
	public NSArray tousTypes() {
		NSMutableArray types = new NSMutableArray();
		for (int i = 1; i < displayGroup().displayedObjects().count();i++) {	// on commence à 1, car le 1er item est "tous"
			EOTypeAbsence typeAbsence = (EOTypeAbsence)displayGroup().displayedObjects().objectAtIndex(i);
			types.addObject(typeAbsence.code());
		}
		return types;
	}
}

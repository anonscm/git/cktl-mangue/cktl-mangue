/*
 * Created on 1 juin 2005
 *
 * Permet la gestion de congés où les absences peuvent être à la demi-journée
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import org.cocktail.client.components.utilities.MatrixUtilities;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOMatrix;

/**
 * @author christine<BR>
 *
 * Permet la gestion de conges ou les absences peuvent etre a la demi-journee
 */
public abstract class GestionCongeAvecDemiJournee extends GestionConge {
	public EOMatrix matriceDebut,matriceFin;
	private String amDebut,amFin;
	
	   /** Constructeur
     * @param nomEntite	nom entite de conge
     * @param titreFenetre titre du dialogue affiche
     */
    public GestionCongeAvecDemiJournee(String nomEntite,String titreFenetre) {
        super(nomEntite,titreFenetre); 
    }
    // méthodes de délégation du display group
//  Méthodes de délégation du display group
    public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
	 	updateDisplayGroups();
    }
 
    // actions
    public void debutHasChanged() {
    		if (matriceDebut != null) {
			amDebut = MatrixUtilities.labelSelectionne(matriceDebut);	
    		}
    }
    public void finHasChanged() {
    		if (matriceFin != null) {
			amFin = MatrixUtilities.labelSelectionne(matriceFin);
    		}
    }
  
    public void afficherFenetre() {
    		amDebut = absenceCourante().absAmpmDebut();
    		amFin = absenceCourante().absAmpmFin();
    		if (matriceDebut != null) {
	    		if  (absenceCourante().absAmpmDebut() != null && absenceCourante().absAmpmDebut().equals(EOAbsences.MATIN)) {
	    			MatrixUtilities.selectionnerBouton(matriceDebut,0);
	    		} else {
	    			MatrixUtilities.selectionnerBouton(matriceDebut,1);
	    		}
    		}
    		if (matriceFin != null) {
	    		if (absenceCourante().absAmpmFin() != null && absenceCourante().absAmpmFin().equals(EOAbsences.MATIN)) {
	    			MatrixUtilities.selectionnerBouton(matriceFin,0);
	    		} else {
	    			MatrixUtilities.selectionnerBouton(matriceFin,1);
	    		}
    		}
    		super.afficherFenetre();
    }
	protected void traitementsApreErreurValidation() {
	}
    protected boolean traitementsAvantValidation() {
    	    	
    	if (amDebut != null && amDebut.equals(absenceCourante().absAmpmDebut()) == false) {
   			absenceCourante().setAbsAmpmDebut(amDebut);
   		}
   		if (amFin != null && amFin.equals(absenceCourante().absAmpmFin()) == false) {
   			absenceCourante().setAbsAmpmFin(amFin);
   		}
   		return super.traitementsAvantValidation();
   }
    /** retourne le nombre de jours de conges en incluant les demi-journees */
    protected float nbJoursConge() {
    		float nbJoursConge = super.nbJoursConge();
    		if (absenceCourante().absAmpmDebut() != null) {
    			if (absenceCourante().absAmpmDebut().equals("am")) {
    				if (absenceCourante().absAmpmFin() != null && absenceCourante().absAmpmFin().equals("am")) {
    					nbJoursConge -= 0.5;
    				}
    			} else if  (absenceCourante().absAmpmDebut().equals("pm")) {
    				if (absenceCourante().absAmpmFin() != null) {
    					if (absenceCourante().absAmpmFin().equals("am")) {
    						nbJoursConge -= 1;
    					} else {
    						nbJoursConge -= 0.5;
    					}
    				}
    			}
    		}
		/*if (absenceCourante().absAmpmFin() != null && absenceCourante().absAmpmFin().equals("pm")) {
			nbJoursConge += 0.5;
		}*/
		return nbJoursConge;
    }
}


//GestionConge.java
//Mangue

//Created by Christine Buttin on Tue May 31 2005.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.

//Modèle général de gestion d'un congé
//Utilise deux display groups un maître sur les absences et un detail sur les congés
//Comporte les 3 méthodes utilisées lors de la gestion des congés : ajouter, modifier, supprimer
//Toutes les classes de gestion de congé doivent hériter de cette classe et implémenter les méthodes abstraites

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import java.awt.Cursor;
import java.awt.Font;

import org.cocktail.client.components.DialogueSimple;
import org.cocktail.client.components.DialogueWithDisplayGroup;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.GestionnaireEvenementIndividu;
import org.cocktail.mangue.client.conges.GestionAbsences;
import org.cocktail.mangue.client.conges.GestionCongeGardeEnfant;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOParamAnciennete;
import org.cocktail.mangue.modele.grhum.EOParamAncienneteMaladie;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.EOCgntMaladie;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTextArea;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSValidation.ValidationException;

/**  Modele general de gestion d'un conge
 * Utilise deux display groups un sur les absences et un sur les conges : displayGroup() correspond au DG des absences<BR>
 * Comporte les 3 methodes utilisees lors de la gestion des conges : ajouter, modifier, supprimer ainsi que les methodes
 * de validation et d'annulation<BR>

 * Toutes les classes de gestion de conge doivent heriter de cette classe et implementer les methodes abstraites<BR>
 * */
public abstract class GestionConge extends DialogueWithDisplayGroup {
	// l'absence est initialisee lors de l'acces a une des methodes ajouter/modifier/supprimer
	// elle n'est modifiee qu'au moment de la validation
	public EOTextArea vueTexte;
	private EOAbsences absenceCourante;
	private int typeTraitement;
	/** mode de traitement : creation */
	public static  int MODE_CREATION = 0;
	/**	mode de traitement : modification */
	public static int MODE_MODIFICATION = 1;
	/** mode de traitement : suppression */
	public static int MODE_SUPPRESSION = 2;

	/** Constructeur
	 * @param nomEntite	nom entite de conge
	 * @param titreFenetre titre du dialogue affiche
	 */
	public GestionConge(String nomEntite,String titreFenetre) {
		super(nomEntite,null,null,titreFenetre,true);	// 05/05/2010 - On ne passe pas de nom de notification au parent car on la gère pour pouvoir envoyer un autre type de données
		chargerArchive();
	}
	public void connectionWasBroken() {
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,null);
	}
	public void connectionWasEstablished() {
		super.connectionWasEstablished();
		displayGroup().setSelectsFirstObjectAfterFetch(true);
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
		if (vueTexte != null) {
			Font font = vueTexte.getFont();
			vueTexte.setFont(new Font(font.getFontName(),font.getStyle(),11));
		}
	}
	// accesseurs
	public String compteurCommentaire() {
		if (congeCourant() != null && congeCourant().commentaire() != null) {
			return "" + congeCourant().commentaire().length() + "/2000";
		} else {
			return "";
		}
	}
	/** Retourne le type de traitement en cours : creation/modification/suppression */
	public int typeTraitement() {
		return typeTraitement;
	}
	public EOAbsences absenceCourante() {
		return absenceCourante;
	}
	/** Ajoute un nouveau conge a un employe
	 * @param individuGlobalID global id de l'individu
	 */
	public void ajouter(EOGlobalID individuGlobalID) {

		typeTraitement = MODE_CREATION;
		preparerAvantChargement();
		displayGroup().insert();
		EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuGlobalID,editingContext());
		congeCourant().initAvecIndividu(individu);
		setAnciennete();

		try {
			absenceCourante = GestionnaireEvenementIndividu.creerEvenementPourIndividu(congeCourant(),true);
			//selectionnerObjets();	// à faire ici pour que absenceCourante soit initialisee et conge courant soit selectionne ensuite
			congeCourant().addObjectToBothSidesOfRelationshipWithKey(absenceCourante(),"absence");
			congeCourant().setTemValide("O");
			// poster une notification pour locker les écrans
			NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
			afficherFenetre();
		} catch (Exception  e) {
			EODialogs.runErrorDialog("Erreur",e.getMessage());
			LogManager.logException(e);
		}
	}

	public void setAnciennete() {
		if (congeCourant() instanceof EOCgntMaladie) {
			EOParamAncienneteMaladie anciennete = null;
			if (congeCourant().dateDebut() != null)
				anciennete = EOParamAncienneteMaladie.getAncienneteForIndividu(editingContext(), congeCourant().individu(), congeCourant().dateDebut());
			else
				anciennete = EOParamAncienneteMaladie.getAncienneteForIndividu(editingContext(), congeCourant().individu(), DateCtrl.today());

			((EOCgntMaladie)congeCourant()).setAnciennete(anciennete);		
		}

	}

	/**
	 * 
	 * @param absenceID
	 */
	public void modifier(EOGlobalID absenceID) {
		try {
			typeTraitement = MODE_MODIFICATION;
			preparerAvantChargement();
			absenceCourante = (EOAbsences)SuperFinder.objetForGlobalIDDansEditingContext(absenceID,editingContext());
			Conge conge = Conge.rechercherCongeAvecAbsence(editingContext,entityName(),absenceCourante());
			if (conge == null)  {
				EODialogs.runErrorDialog("Erreur","Pas de congé associé à cette absence");
			} else {
				// pour gérer les anciennes données
				if (conge.absence() == null) {
					conge.setAbsenceRelationship(absenceCourante());
				}
				displayGroup().setObjectArray(new NSArray(conge));
				selectionnerConge();
				// poster une notification pour locker les écrans
				NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
				afficherFenetre();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param absenceID
	 */
	public void imprimer(EOGlobalID absenceID) {

		preparerAvantChargement();
		absenceCourante = (EOAbsences)SuperFinder.objetForGlobalIDDansEditingContext(absenceID,editingContext());
		Conge conge = Conge.rechercherCongeAvecAbsence(editingContext,entityName(),absenceCourante());
		if (conge == null)  {
			EODialogs.runErrorDialog("Erreur","Pas de congé associé à cette absence");
		} else {
			// on ne s'occupe pas ici des anciens congés car les impressions déclenchées par cette méthode
			// sont faites sur le serveur
			displayGroup().setObjectArray(new NSArray<Conge>(conge));
			selectionnerConge();
			if (this instanceof GestionCongeAvecArrete) {
				((GestionCongeAvecArrete)this).imprimer();
			} else 	if (this instanceof GestionCongeGardeEnfant) {
				((GestionCongeGardeEnfant)this).imprimerArrete();
			}
		}
	}

	/**
	 * 
	 * @param absenceID
	 * @return
	 */
	public boolean supprimer(EOGlobalID absenceID) {
		typeTraitement = MODE_SUPPRESSION;
		absenceCourante = (EOAbsences)SuperFinder.objetForGlobalIDDansEditingContext(absenceID,editingContext());
		Conge conge = Conge.rechercherCongeAvecAbsence(editingContext,entityName(),absenceCourante);
		if (conge == null)  {
			EODialogs.runErrorDialog("Erreur","Pas de congé associé à cette absence");
			return false;
		}
		// pour gerer les anciennes donnees
		if (conge.absence() == null) {
			conge.setAbsenceRelationship(absenceCourante());
		}
		displayGroup().setObjectArray(new NSArray(conge));
		selectionnerConge();		// on travaille avec les DG pour avoir acces aux objets dans traitementsAvantSuppression
		if (traitementsAvantSuppression()) {	// pour faire les traitements specifiques a chaque classe
			congeCourant().setTemValide("N");
			try {
				absenceCourante().setTemValide("N");
				conge.setTemValide("N");
				//				GestionnaireEvenementIndividu.supprimerEvenement(absenceCourante(),true);
				editingContext().saveChanges();
				traitementsApresSuppression();
				return true;
			} catch (Exception e) {
				editingContext().revert();
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur","Erreur lors de la suppression du congé");
				return false;
			}
		} else {
			return false;
		}
	} 

	// Actions
	public void valider() {
		if (traitementsAvantValidation()) {
			try {
				editingContext().saveChanges(); 
				super.validate();		// pour fermer la fenêtre
				// poster une notification pour unlocker les écrans
				NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);
				NSNotificationCenter.defaultCenter().postNotification(ModelePage.SYNCHRONISER,absenceCourante().getClass().getName());	// pour synchronisation
				traitementsApresValidation();
			}
			catch (ValidationException ex) {
				EODialogs.runErrorDialog("Erreur",ex.getMessage());
				traitementsApreErreurValidation();				
			}
			catch (Exception e) {
				if (e.getClass().getName().equals("com.webobjects.foundation.NSValidation$ValidationException")) {
					EODialogs.runErrorDialog("Erreur",e.getMessage());
					traitementsApreErreurValidation();
				} else {
					LogManager.logException(e);
					EODialogs.runErrorDialog("Erreur","Erreur d'enregistrement d'un Conge !\nMESSAGE : " + e.getMessage());
					editingContext().revert();
				}
			}
		}
	}
	public boolean validerSansFermerFenetre() {
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		boolean validationOK = traitementsAvantValidation();
		if (validationOK) {
			try {
				editingContext().saveChanges(); 
				NSNotificationCenter.defaultCenter().postNotification(GestionAbsences.SYNCHRONISATION_DATES_AFFICHAGE,absenceCourante().dateDebut()); // pour forcer l'affichage de cette absence si les dates ne correspondent pas aux dates affichées
				NSNotificationCenter.defaultCenter().postNotification(ModelePage.SYNCHRONISER,absenceCourante().getClass().getName());	// pour synchronisation
				traitementsApresValidation();
				return true;
			}
			catch (Exception e) {
				if (e.getClass().getName().equals("com.webobjects.foundation.NSValidation$ValidationException")) {
					EODialogs.runErrorDialog("Erreur",e.getMessage());
					traitementsApreErreurValidation();
					return false;
				} else {
					LogManager.logException(e);
					EODialogs.runErrorDialog("Erreur","Erreur d'enregistrement d'un Conge !\nMESSAGE : " + e.getMessage());
					editingContext().revert();
					return false;
				}
			}
		}
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		return validationOK;
	}
	public void annuler() {
		if (editingContext().hasChanges()) {
			editingContext().revert();
		}
		// poster une notification pour unlocker les écrans
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);
		super.cancel();
	}
	// méthodes du controllerDG
	/** on peut modifier les champs d'un conge que si il est correctement initialise */
	public boolean peutModifier() {
		return congeCourant() != null && congeCourant().individu() != null;
	}
	/** on ne peut saisir la date de debut que si que si le conge est initialise */
	public boolean peutModifierDateDebut() {
		return peutModifier();
	}
	/** on ne peut saisir la date de fin que si la date de debut est fournie */
	public boolean peutModifierDateFin() {
		return peutModifier() && congeCourant().dateDebut() != null;
	}
	/** on ne peut valider que si la date de debut est fournie */
	public boolean peutValider() {
		return congeCourant() != null && congeCourant().individu() != null && congeCourant().dateDebut() != null;
	}
	// méthodes protégées
	/** Preparation avant le chargement de l'archive */
	protected void preparerAvantChargement() {
		preparerDataSourcePourDisplayGroupEtEntite(displayGroup(),entityName());
	}
	/** Pas de traitement specifique de l'interface apres chargement de l'archive, a surcharger par les sous-classes si necessaire
	 */
	protected void prepareInterface() {
	}
	/** methode abstraite surchargee par les sous-classes pour charger l'archive ad-hoc */
	protected abstract void chargerArchive();
	/** a surcharger pour faire des traitements specifiques avant validation d'un conge et de l'absence associee */
	protected boolean traitementsAvantValidation() {
		if (congeCourant().dateFin() == null) {
			// Si la date de fin est nulle, on la met par defaut a la date de debut (on a pris une demi journee ou une journee entiere);
			congeCourant().setDateFin(congeCourant().dateDebut());
		}
		try {
			GestionnaireEvenementIndividu.modifierEvenementAvecDuree(absenceCourante(),congeCourant(),nbJoursConge());
			return true;
		} catch (Exception e) {
			// Problème sur les types d'exclusion
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
			return false;
		}

	}
	/** a surcharger pour faire des traitements specifiques avant suppression d'un conge et de l'absence associee */
	protected abstract boolean traitementsAvantSuppression();
	/** a surcharger pour faire des traitements apres validation des donnees */
	protected abstract void traitementsApresValidation();
	/** a surcharger pour faire des traitements apres suppression des donnees dans la base */
	protected abstract void traitementsApresSuppression();
	/** a surcharger pour faire des traitements apres erreur de validation */
	protected abstract void traitementsApreErreurValidation();
	protected Conge congeCourant() {
		return (Conge)displayGroup().selectedObject();
	}
	protected void selectionnerConge() {
		displayGroup().setSelectionIndexes(new NSArray(new Integer(0)));
	} 

	/** a surcharger si le nombre de jours de conge n'est pas le nombre de jours entre le debut et la fin du conge */ 
	protected float nbJoursConge() {
		return (float)DateCtrl.nbJoursEntre(congeCourant().dateDebut(),congeCourant().dateFin(),true);
	}
	// autres méthodes
	protected void afficherDialogue(String nomEntite,String nomMethodeNotification,EOQualifier qualifier) {
		afficherDialogue(nomEntite,nomMethodeNotification,qualifier,false,null);
	}
	protected void afficherDialogue(String nomEntite,String nomMethodeNotification,EOQualifier qualifier,boolean estSelectionMultiple) {
		afficherDialogue(nomEntite, nomMethodeNotification, qualifier,estSelectionMultiple,null);
	}
	protected void afficherDialogue(String nomEntite,String nomMethodeNotification,EOQualifier qualifier,boolean estSelectionMultiple,NSArray sorts) {
		String nomNotification = getClass().getName() + "_Selection" + nomEntite;
		//String nomNotification = "Selection" + nomEntite; remplacer le 09/02/09 pour éviter que d'autres composants reçoivent la notification
		String titreFenetre = "Sélection " + nomEntite;
		DialogueSimple controleur = new DialogueSimple(nomEntite,nomNotification,titreFenetre,false,estSelectionMultiple,true);
		controleur.init();
		if (qualifier != null) {
			controleur.setQualifier(qualifier);
		}
		if (sorts != null) {
			controleur.displayGroup().setSortOrderings(sorts);
		}
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector(nomMethodeNotification,new Class[] {NSNotification.class}),nomNotification,null);
		controleur.afficherFenetre();
	}
	/** ajoute une relation a un objet source
	 * @param source	source de la relation	
	 * @param destin	globalID de l'objet destinataire de la relation
	 * @param nomRelation	 cle utilisee pour definir cette relation
	 */
	protected boolean ajouterRelation(EOGenericRecord source, Object destin,String nomRelation) {
		if (destin != null) {
			EOGenericRecord recordDestin = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)destin,editingContext());
			if (source != null && recordDestin != null) {
				LogManager.logDetail(this.getClass().getName() + " - ajouterRelation " + nomRelation + ", objet :" + recordDestin);
				source.addObjectToBothSidesOfRelationshipWithKey(recordDestin,nomRelation);
				return true;
			}
		}
		return false;
	}
	/** Retourne le conge courant */
	protected Object selectedObject() {
		return congeCourant();
	}
	// methodes privees
	private void preparerDataSourcePourDisplayGroupEtEntite(EODisplayGroup displayGroup,String nomEntite) {
		EODistributedDataSource dataSource = new EODistributedDataSource(editingContext(),nomEntite);
		displayGroup.setDataSource(dataSource);		// pour etre sur d'avoir des objets du bon type
	}

}

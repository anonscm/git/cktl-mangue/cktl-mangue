/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.cocktail.client.components.DialogueWithDisplayGroup;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EOTypeGroupe;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;

// 10/12/2010- Adaptation Netbeans
public class ChoixOrganisme extends DialogueWithDisplayGroup {
	public EOTable listeAffichage;
	public EODisplayGroup displayGroupTypes;
	private String typeChoisi = null;
	private String libelleRecherche;
	private final static String TYPE_PAR_DEFAUT = "Service";
	public final static String NOTIFICATION_SELECTION_ORGANISME = "NotifChoixOrganisme";
	
	public ChoixOrganisme(String nomNotification) {
		super("StructureUlr",nomNotification,null,"Sélection d'un organisme de rattachement",true);
	}
	/** Accesseurs */
	public String typeChoisi() {
		return typeChoisi;
	}
	public void setTypeChoisi(String aStr) {
		this.typeChoisi = aStr;
		if (aStr == null) {
			typeChoisi = TYPE_PAR_DEFAUT;
		} 
		modifierQualifierEtFetcher();
	}
	  // accesseurs
	public String libelleRecherche() {
		return libelleRecherche;
	}
	public void setLibelleRecherche(String libelleRecherche) {
		this.libelleRecherche = libelleRecherche;
	}
	/** Initialiser le contr&eocirc;leur
	 * @param typesliste des types de groupe &agrave; afficher
	 */
	public void init(String[] types) {
		chargerArchive();
		preparerTypesGroupe(types);
		setTypeChoisi(TYPE_PAR_DEFAUT);
	}
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		if (controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
	} 
	// actions
	public void rechercher() {
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		modifierQualifierEtFetcher();
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public void valider() {
		LogManager.logDetail("ChoixOrganisme - valider");
		if (displayGroup().selectedObject() != null) {
			// la notification renvoie des globals IDs
			NSNotificationCenter.defaultCenter().postNotification(validationNotification(),editingContext().globalIDForObject((EOGenericRecord)displayGroup().selectedObject()));
			terminate();
		}
	}
	// Méthodes du controller DG
	public boolean peutValider() {
		return displayGroup().selectedObject() != null;
	}

	// méthodes protégées
	protected void chargerArchive() {
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
		EOArchive.loadArchiveNamed("ChoixOrganisme",this,"org.cocktail.mangue.client.outils_interface.interfaces",this.disposableRegistry());
	}
	protected void prepareInterface() {
		GraphicUtilities.rendreNonEditable(listeAffichage);
		GraphicUtilities.changerTaillePolice(listeAffichage,11);
		listeAffichage.table().addMouseListener(new DoubleClickListener());
	}
	protected Object selectedObject() {
		return displayGroup().selectedObject();
	}

	private void preparerTypesGroupe(String[] types) {
		NSMutableArray qualifiers = new NSMutableArray();
		for (int i = 0; i < types.length;i++) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeGroupe.CODE_KEY + " =%@", new NSArray(types[i])));
		}
		EOQualifier qualifier = new EOOrQualifier(qualifiers);
		displayGroupTypes.setObjectArray(SuperFinder.rechercherAvecQualifier(editingContext(), EOTypeGroupe.ENTITY_NAME, qualifier, true));
	}

	private void modifierQualifierEtFetcher() {
		String typeCode = null;
		java.util.Enumeration e = displayGroupTypes.displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOTypeGroupe type = (EOTypeGroupe)e.nextElement();
			if (type.libelleLong().equals(typeChoisi)) {
				typeCode = type.code();
				break;
			}
		}
		// En cas de valeur nulle (au démarrage), on prend le type par défaut
		if (typeCode == null) {
			typeCode = TYPE_PAR_DEFAUT.substring(0,1);
		}
		fetcherObjets(typeCode);
		updateDisplayGroups();
	}
	private EOQualifier qualifierRecherche() {
		String libelle = "*";
		if (libelleRecherche != null) {
			libelle = "*" + libelleRecherche + libelle + "*";
		}
		NSArray args = new NSArray(libelle);
		return EOQualifier.qualifierWithQualifierFormat("llStructure caseinsensitivelike %@" ,args);
	}
	private void fetcherObjets(String typeCode) {
		displayGroup().setObjectArray(EOStructure.findForTypeGroupe(editingContext(),typeCode,qualifierRecherche()));
	}
	//	Classe d'ecoute pour les doubleclics dans la table view  */
	public class DoubleClickListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				valider();
			}
		}
	}

}

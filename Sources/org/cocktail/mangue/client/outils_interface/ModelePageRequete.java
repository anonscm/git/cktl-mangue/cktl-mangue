/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/** Mod&egrave;le de page pour une vue permettant de saisir des informations utilis&eacute;es pour des requ&egrave;tes */
public abstract class ModelePageRequete extends ModelePage {
	/** Notification &eacute;nvoy&eacute;e pour signaler que les informations saisies ont &eacute;t&eacute;s modifi&eacute;es */
	public static String RAFFRAICHIR = "RaffraichirDonnees";

	// accesseurs
	/** methode d'initialisation responsable du chargement de l'archive */
	public abstract void init(NSTimestamp debutPeriode,NSTimestamp finPeriode);
	
	/** &agrave; surcharger par la sous-classe pour retourner le qualifierlier li&eacute; &agrave; la page 
	 * @param dateReference date pour laquelle on recherche des donn&eacute;es */
	public abstract EOQualifier qualifierRecherche();
	public void terminerControleur() {
		terminer();
	}
	// méthodes de délégation du DG
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
   		updaterDisplayGroups();
	} 
	// actions
	public void deselectionner() {
		if (displayGroup() != null) {
			  displayGroup().setSelectedObject(null);
		}
		updaterDisplayGroups();
	}
	/** &agrave; surcharger pour effacer les champs */
	public void effacer() {
		effacerChamps();
		deselectionner();
	}


	// méthodes du controller DG
	public boolean peutDeselectionner() {
		return displayGroup() != null && displayGroup().selectedObjects().count() > 0;
	}
	// méthodes protégées
	protected abstract void effacerChamps();
	
	protected boolean traitementsPourSuppression() {
		// pas de suppression
		return true;
	}
	protected String messageConfirmationDestruction() {
		// pas de suppression
		return null;
	}
	
	protected boolean traitementsAvantValidation() {
		// pas de validation
		return true;
	}
	// pas de validation
	protected void traitementsApresValidation() {}
	// pas de validation
	protected void afficherExceptionValidation(String message) {}
	
	/** raffraichissement des display groups */
	protected void updaterDisplayGroups() {
		if (displayGroup() != null) {
			displayGroup().updateDisplayedObjects();
		}
		if (controllerDisplayGroup() != null) {
			controllerDisplayGroup().redisplay();
		}
		NSNotificationCenter.defaultCenter().postNotification(RAFFRAICHIR,null);
	}
	/** stocke une valeur dans un attribut de la page
	 * @param valeur valeur a stocker
	 * @param cleDestin	nom de l'attribut
	 */
	protected boolean stockerValeur(Object valeur, String cleDestin) {
		if (cleDestin != null) {
			takeValueForKey(valeur,cleDestin);
			updaterDisplayGroups();
			return true;
		}
		return false;
	}
	/** Extrait d'une notification la valeur a stocker et la stocke dans un attribut de la page (on attend un globalID comme valeur)
	 * @param aNotif notification contenant la valeur a stocker
	 * @param cleDestin	nom de l'attribut
	 */
	protected boolean stockerValeur(NSNotification aNotif, String cleDestin) {
		if (cleDestin != null) {
			EOGlobalID gid = (EOGlobalID)aNotif.object();
			if (gid != null) {
				EOGenericRecord record = SuperFinder.objetForGlobalIDDansEditingContext(gid,editingContext());
				takeValueForKey(record,cleDestin);
				updaterDisplayGroups();
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

}

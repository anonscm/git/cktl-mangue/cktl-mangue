/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.GestionnaireEvenementIndividu;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.IDureePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;

/** Modelise une page qui gere des donnees lees a un segment de carriere et
 * qui supportent l'interface InterfaceDureePourIndividu. Gere les evenements (EOAbsences) liees a ces donnees
 * Ces donnees sont gerees dans le display group. 
 */
public abstract class GestionEvenementAvecCarriere extends ModelePageComplete {
	private EOGlobalID carriereID;
	private EOCarriere currentCarriere;
	private EOAbsences absenceCourante;	// l'absence est initialisée lors de l'accès à une des méthodes ajouter/modifier/supprimer
	// elle n'est modifiée qu'au moment de la validation
	
	public GestionEvenementAvecCarriere(EOGlobalID carriereID) {
		super();
		this.carriereID = carriereID;
	}
	//	 méthodes du délégué du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		super.displayGroupDidChangeSelection(aGroup);
		if (aGroup == displayGroup()) {
			if (currentEvenement() == null || modeCreation()) {
				absenceCourante = null;
			} else {
				try {
					absenceCourante = GestionnaireEvenementIndividu.rechercherEvenementPourDuree(currentEvenement());
				} catch (Exception e) {
					// on ne fait rien i.e on ne gère pas l'absence courante
				}
			}
		}
	}

	protected void preparerFenetre() {
		currentCarriere = (EOCarriere)SuperFinder.objetForGlobalIDDansEditingContext(carriereID,editingContext());
		super.preparerFenetre();
	}
	
	/** cree une absence si elle n'est pas deja creee */
	protected EOAbsences creerAbsenceSiNecessaire() {
		if (absenceCourante == null) {
			try {
				absenceCourante = GestionnaireEvenementIndividu.creerEvenementPourIndividu(currentEvenement(),true);
			} catch (Exception e) {
				afficherException(e);
				return null;
			}
		}
		return absenceCourante;
	}
	protected EOAbsences creerAbsence() {
		try {
			absenceCourante = GestionnaireEvenementIndividu.creerEvenementPourIndividu(currentEvenement(),true);
		} catch (Exception e) {
			absenceCourante = null;
			afficherException(e);
		}
		return absenceCourante;
	}

	protected boolean traitementsPourSuppression() {
		if (absenceCourante != null) {	// dans certains cas, on ne gère peut-être pas d'absence courante
			try {
				GestionnaireEvenementIndividu.supprimerEvenement(absenceCourante,false);	// false => Ne pas supprimer mais invalider
			} catch (Exception e) {
				afficherException(e);
			}
		}
		return true;
	}
	/** true si l'agent peut gerer les carrieres */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutGererCarrieres();
	}
	protected boolean conditionsOKPourFetch() {
		return currentCarriere() != null;
	}
	protected boolean traitementsAvantValidation() {
		if (absenceCourante != null) {
			try {
				GestionnaireEvenementIndividu.modifierEvenementAvecDuree(absenceCourante,currentEvenement(),DateCtrl.nbJoursEntre(currentEvenement().dateDebut(),currentEvenement().dateFin(),true));
			} catch (Exception e) {
				afficherException(e);
			}
		}
		return true;
	}
	/** affiche un message suite a une exception de validation */
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR","MANGUE -" + message);
		controllerDisplayGroup().redisplay();
	}
	/** Cree un evenement ou le modifie si il existe deja et que son type ne correspond pas au type passe en parametre */
	protected EOAbsences modifierOuCreerEvenement() {
		if (absenceCourante == null) {
			absenceCourante = creerAbsence();
		} else if (absenceCourante.typeEvenement().equals(currentEvenement().typeEvenement()) == false) {
			try {
				EOTypeAbsence typeAbsence = GestionnaireEvenementIndividu.rechercherTypeEvenement(editingContext(), currentEvenement());
				if (typeAbsence == null) {
					return null;
				}
				absenceCourante.setToTypeAbsenceRelationship(typeAbsence);
				return absenceCourante;
			} catch (Exception e) {
				afficherException(e);
				return null;
			}
		}

		return absenceCourante;

	}
	protected EOCarriere currentCarriere() {
		return currentCarriere;
	}
	// méthodes protégées
	private IDureePourIndividu currentEvenement() {
		return (IDureePourIndividu)displayGroup().selectedObject();
	}
	private void afficherException(Exception e) {
		LogManager.logException(e);
		EODialogs.runErrorDialog("Erreur",e.getMessage());
	}
}

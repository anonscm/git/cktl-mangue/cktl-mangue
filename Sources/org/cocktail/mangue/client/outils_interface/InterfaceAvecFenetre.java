/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import org.cocktail.client.composants.ModelePage;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotificationCenter;

/** Mod&eacute;lise une fen&ecirc;tre d'interface secondaire qui ne sera pas affich&eacute;e dans une vue &grave; onglets */
// 13/10/2010 - Modifications pour adapter à Netbeans
// 14/10/2010 - Pour éviter le chargement par défaut de l'archive
public class InterfaceAvecFenetre extends EOInterfaceController {
	private boolean fermetureExterieure;
	private String titreFenetre;
	private boolean archiveLoaded;

	/** constructeur */
	public InterfaceAvecFenetre(String titreFenetre) {
		this.titreFenetre = titreFenetre;
		fermetureExterieure = false;
	}
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void prepareComponent() {
		// On vérifie isConnected car cette méthode est appelée après establishConnection dans le cas où on charge
		// une interface via EOFrameController.runControllerInNewFrame
		if (archiveLoaded == false) {
			try {
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());

			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				super.prepareComponent();
			}
			archiveLoaded = true;
		}
	}
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void establishConnection() {
		if (archiveLoaded == false) {
			try {
				controllerWillLoadArchive();
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				NSDictionary namedObjects = EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
				controllerDidLoadArchive(namedObjects);
				setConnected(true);
				connectionWasEstablished();
			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				super.establishConnection();
			}
			archiveLoaded = true;
		}
	}
	public void connectionWasBroken() {
		if (!fermetureExterieure) {
			NSNotificationCenter.defaultCenter().postNotification(ModelePage.ARRET_CONTROLEUR,titreFenetre);
		}
	}
	public void afficherFenetre() {
		((EOFrameController)supercontroller()).activateWindow();
	}
	public void arreter() {
		if (supercontroller() instanceof EOFrameController) {
			((EOFrameController)supercontroller()).closeWindow();
			fermetureExterieure = true;
		} else if (supercontroller() instanceof EODialogController) {
			((EODialogController)supercontroller()).closeWindow();
			fermetureExterieure = true;
		} 
	}
	// Méthodes protégées
	// 14/10/2010 - Pour éviter le chargement par défaut
	protected boolean loadArchive() {
		if (archiveLoaded == false) {
			try {
				controllerWillLoadArchive();
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				NSDictionary namedObjects = EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
				controllerDidLoadArchive(namedObjects);
			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				return super.loadArchive();
			}
			archiveLoaded = true;
		}	
		return archiveLoaded;  
	}


}

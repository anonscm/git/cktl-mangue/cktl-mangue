package org.cocktail.mangue.client.outils_interface.utilitaires;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import org.cocktail.common.LogManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;

public class TimerForAsynchronousRemoteCalls extends Timer {
	private EODistributedObjectStore store;
	private EOEditingContext editingContext;
	private boolean termine;
	private ActionListener listener;
	public final static int ID_MESSAGE = 0;
	public final static int ID_RESULTAT = 1;
	public final static String COMMANDE_ERREUR = "erreurArrivee";
	public final static String COMMANDE_RESULTAT = "resultatObtenu";
	public final static String FIN_TRAITEMENT = "threadTermine";
	public final static String MESSAGE_INFORMATION = "messageInformation";

	public TimerForAsynchronousRemoteCalls(int delay, ActionListener listener) {
		super(delay, listener);
		editingContext = new EOEditingContext();
		store = (EODistributedObjectStore)editingContext.parentObjectStore();
		termine = false;
		this.listener = listener;
	}
	
	protected void fireActionPerformed(ActionEvent e) {
		try {
			if (!termine) {
				String message = (String)store.invokeRemoteMethodWithKeyPath(editingContext,"session","clientSideRequestMessage", null,null,false);
				if (message != null) {
					if (message.equals("termine")) {
						NSDictionary resultatAction = (NSDictionary)store.invokeRemoteMethodWithKeyPath(editingContext,"session","clientSideRequestResultatAction", null,null,false);
						// vérifier si une exception c'est produite
						message = extraireInformationDiagnostic(resultatAction,"exception");
						if (message != null) {
							super.fireActionPerformed(new RemoteActionEvent(this,ID_MESSAGE, COMMANDE_ERREUR, message));				
							} else {
							// poster le résultat obtenu via une notification si il existe un résultat
							if (resultatAction.objectForKey("resultat") != null) {
								super.fireActionPerformed(new RemoteActionEvent(this, ID_RESULTAT, COMMANDE_RESULTAT, resultatAction.objectForKey("resultat")));				
							}
						}
						super.fireActionPerformed(new RemoteActionEvent(this, ID_MESSAGE, FIN_TRAITEMENT, null));				
						termine = true;
					} else {
						super.fireActionPerformed(new RemoteActionEvent(this, ID_MESSAGE, MESSAGE_INFORMATION, message));				
					}
				}
			} else {
				stop();
				removeActionListener(listener);
			}
		} catch (Exception exc) {
			LogManager.logException(exc);
		}
	}
	
	// méthodes privées
	private String extraireInformationDiagnostic(NSDictionary dictionnaire,String cle) {
		NSDictionary diagnostic = (NSDictionary)dictionnaire.objectForKey("diagnostic");
		if (diagnostic != null) {
			return (String)diagnostic.objectForKey(cle);
		} else {
			return null;
		}
	}
	// Classes pour gérer les événements
	public class RemoteActionEvent extends ActionEvent {
		private Object resultat;
		public RemoteActionEvent(Object source, int id, String commande,Object resultat) {
			super(source,id,commande);
			this.resultat = resultat;
		}
		public String message() {
			if (resultat instanceof String) {
				return (String)resultat;
			} else {
				return null;
			}
		}
		public NSDictionary resultat() {
			return (NSDictionary)resultat;

		}
	}


}

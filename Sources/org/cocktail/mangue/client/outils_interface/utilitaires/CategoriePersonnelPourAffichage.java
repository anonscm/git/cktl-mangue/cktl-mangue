/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface.utilitaires;

import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** Gere les categories de personnel que l'agent peut acceder en fonction de son profil
 * Utilisee pour la gestion des preferences et la liste des agents.<BR>
 * Permet de faire le lien entre l'index d'une categorie de personnel et son nom */
public class CategoriePersonnelPourAffichage {
	private final static String[] CATEGORIES_PERSONNELS = {"Enseignants","Non Enseignants","Vacataires","Hébergés","Tous"};

	/** retourne -1 si categorie non trouvee */
	public static int categoriePersonnelPour(String nomCategorie) {
		if (nomCategorie == null || nomCategorie.length() == 0) {
			return -1;
		}
		for (int i = 0; i < CATEGORIES_PERSONNELS.length;i++) {
			if (nomCategorie.equals(CATEGORIES_PERSONNELS[i])) {
				return i;
			}
		}
		return -1;
	}
	/** retourne le nom de la categorie en fonction de l'index fourni (a partir de O)
	 *  ou null si index invalide */
	public static String nomCategoriePersonnel(int index) {
		if (index >= CATEGORIES_PERSONNELS.length) {
			return null;
		} else {
			return CATEGORIES_PERSONNELS[index];
		}
	}
	/** retourne la liste des categories gerees par un agent (tableau de strings) */
	public static NSArray nomCategoriesPourAgent(EOAgentPersonnel agent) {
		NSMutableArray noms = new NSMutableArray();
		if (agent.gereEnseignants()) {
			noms.addObject(CategoriePersonnelPourAffichage.nomCategoriePersonnel(ManGUEConstantes.ENSEIGNANT));
		}
		if (agent.gereNonEnseignants()) {
			noms.addObject(CategoriePersonnelPourAffichage.nomCategoriePersonnel(ManGUEConstantes.NON_ENSEIGNANT));
		} 
		if (agent.gereVacataires()) {
			noms.addObject(CategoriePersonnelPourAffichage.nomCategoriePersonnel(ManGUEConstantes.VACATAIRE));
		}
		if (agent.gereHeberges()) {
			noms.addObject(CategoriePersonnelPourAffichage.nomCategoriePersonnel(ManGUEConstantes.HEBERGE));
		}
		noms.addObject(CategoriePersonnelPourAffichage.nomCategoriePersonnel(ManGUEConstantes.TOUT_PERSONNEL));
		return new NSArray(noms);
	}
}

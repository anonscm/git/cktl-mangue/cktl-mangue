/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.cocktail.client.components.DialogueWithDisplayGroup;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.component.COTable;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** Pour pouvoir rechercher les UAI sur le code, le libell&eacute; et la ville */
public class ChoixRne extends DialogueWithDisplayGroup {
	private String code,libelle,ville;
	public COTable listeAffichage;
	
	/** Constructeur : la classe du parent est utilis&eacute;e pour diff&eacute;rencier les notifications */
	public ChoixRne(String nomNotification) {
		super("Rne", nomNotification, null, "Sélection UAI",true);
	}
	// Accesseurs
	public String code() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String libelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String ville() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}
	// Méthodes de délégation
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		updateDisplayGroups();
	}
	// Actions
	public void rechercher() {
		NSMutableArray qualifiers = new NSMutableArray();
		if (code() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + " caseinsensitivelike %@", new NSArray("*" + code() + "*")));
		}
		if (libelle() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_LONG_KEY + " caseinsensitivelike %@", new NSArray("*" + libelle() + "*")));
		}
		if (ville() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("ville caseinsensitivelike %@", new NSArray("*" + ville() + "*")));
		}
		EOFetchSpecification fs = new EOFetchSpecification(EORne.ENTITY_NAME, new EOAndQualifier(qualifiers), Nomenclature.SORT_ARRAY_LIBELLE);
		displayGroup().setObjectArray(editingContext().objectsWithFetchSpecification(fs));
	}
	// Méthodes du controller DG
	public boolean peutRechercher() {
		return (code() != null && code().length() > 0) || (libelle() != null && libelle().length() > 0) || (ville() != null && ville().length() > 0);
	}
	public boolean peutValider() {
		return displayGroup().selectedObject() != null;
	}
	public void chargerArchive() {
		EOArchive.loadArchiveNamed("ChoixRne",this,"org.cocktail.mangue.client.outils_interface.interfaces",this.disposableRegistry());
	}
	// Méthodes protégées
	protected void prepareInterface() {
        GraphicUtilities.rendreNonEditable(listeAffichage);
		listeAffichage.table().addMouseListener(new DoubleClickListener());
	}
	protected Object selectedObject() {
		return editingContext().globalIDForObject((EORne)displayGroup().selectedObject());
	}
	
	//	Classe DoubleClick
	public class DoubleClickListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				if (displayGroup().selectedObject() != null) {
					validate();
				}
			}
		}
	}
}

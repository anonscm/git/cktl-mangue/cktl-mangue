/*
 * Created on 19 juil. 2005
 *
 * Modélise toutes les pages qui gèrent des données de l'individu
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/**
 * Modelise toutes les pages qui gerent des donnees de l'individu
 */

public abstract class ModelePageAvecIndividu extends org.cocktail.client.composants.ModelePageAvecIndividu {
	private EOIndividu currentIndividu;

	public ModelePageAvecIndividu() {
		super();
	}
	public ModelePageAvecIndividu(EOEditingContext edc) {
		super(edc);
	}


	// méthodes protégées
	protected void preparerFenetre() {
		if (individuCourantID() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(editingContext(),individuCourantID(),false));
		}
		super.preparerFenetre();
		raffraichirAssociations();
		updaterDisplayGroups();
	}
	protected EOIndividu currentIndividu() {
		return currentIndividu;
	}
	protected void setCurrentIndividu(EOIndividu unIndividu) {
		currentIndividu = unIndividu;
	}

	//    /**
	//     * 
	//     */
	//    public EOEditingContext editingContext() {
	//    	return ((ApplicationClient)EOApplication.sharedApplication()).getEditingContext();
	//    }

	/**
	 * 
	 */
	protected void changerIndividu() {
		if (individuCourantID() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(editingContext(),individuCourantID(),false));
		} else {
			setCurrentIndividu(null);
		}
	}
	protected String identiteIndividu() {
		if (currentIndividu() == null) {
			return "";
		} else {
			return currentIndividu().identite();
		}
	}

	/**
	 * 
	 */
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("employeHasChanged", new Class[] { NSNotification.class }), ListeAgents.CHANGER_EMPLOYE, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("nettoyer", new Class[] { NSNotification.class }), ListeAgents.NETTOYER_CHAMPS, null);
	}

}

/*
 * Created on 9 févr. 2006
 *
 * Gère les congés qui peuvent avoir des arrêtés d'annulation
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import java.lang.reflect.Constructor;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Arrete;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Gere les conges qui peuvent avoir des arretes d'annulation
 * Un arrete peut etre soit annule, soit remplace par un autre arrete qui l'annule<BR>
 * Les superclasses doivent avoir un constructeur sans parametre qui est appele lors de l'annulation d'un conge<BR>
 * @author christine
 */
public abstract class GestionCongeAvecArreteAnnulation extends GestionCongeAvecArrete {
	public EOView vueArreteAnnulation,contenuVueArreteAnnulation;
	private GestionArrete gestionArreteAnnulation;
	private CongeAvecArreteAnnulation congeRemplace;
	private NSArray congesRemplaces;
	/** Notification envoyee pour demander la fermeture de tous les dialogues ouverts : utilise quand on ouvre un
	 * de meme type */
	public static String TERMINER_AFFICHAGE = "Terminer";

	/** Constructeur
	 * @param nomEntite	nom entite de conge
	 * @param titreFenetre titre du dialogue affiche
	 */
	public GestionCongeAvecArreteAnnulation(String nomEntite,String titreFenetre) {
		super(nomEntite,titreFenetre);
	}

	public void terminer() { 
		super.terminer();
		if (gestionArreteAnnulation != null) {
			gestionArreteAnnulation.terminer();
		}
	}
	public void afficherFenetre() {
		preparerVueArrete();	// on ne le fait que là pour que tout soit initialisé
		preparerCongesRemplaces();
		if (congesRemplaces != null && congesRemplaces.count() > 0 && congeRemplace == null) {
			// nécessaire pour le cas où on a besoin de revenir sur les congés remplacés lors d'une modif
			congeRemplace = (CongeAvecArreteAnnulation)congesRemplaces.objectAtIndex(0);
		}
		super.afficherFenetre();
	}
	// Actions
	// affiche le congé qui annule ce congé
	public void afficherArreteAnnulation() {
		// on instancie un contrôleur de la même classe de congé 
		try {
			Class classe = Class.forName(this.getClass().getName());
			try {
				// on recherche un constructeur du type nomClasse() ou nomClasse(String nomEntite)
				GestionCongeAvecArreteAnnulation controleur = null;
				try {
					Constructor constructor = classe.getConstructor(null);
					controleur = (GestionCongeAvecArreteAnnulation)constructor.newInstance(null);
				} catch (Exception e) {
					try {
						// essayer avec un constructeur qui a comme paramètre le nom d'entité (cas des congés qui gèrent plusieurs types de congés)
						Constructor constructor = classe.getConstructor(new Class[] {java.lang.String.class});
						controleur = (GestionCongeAvecArreteAnnulation)constructor.newInstance(new Object[] {entityName()});
					} catch (Exception e1) {
						throw e1;
					}
				}
				controleur.init(editingContext(),currentConge());
				controleur.modifier(editingContext().globalIDForObject(currentConge().congeDeRemplacement().absence()));
			} catch (Exception e) {
				throw e;
			}
		} catch (Exception e) {
			LogManager.logException(e);
		}
	}
	//	annulation des arrêtés
	public void annulerArrete() {
		if (!validerSansFermerFenetre()) {
			LogManager.logDetail("Annulation de l'arrêté - Erreur lors de l'enregistrement des données");
			EODialogs.runInformationDialog("Erreur", "Impossible d'annuler l'arrêté.\nProblème lors de l'enregistrement des données du congé courant");
			return;
		}
		try {
			Class classe = Class.forName(this.getClass().getName());
			GestionCongeAvecArreteAnnulation controleur = null;
			try {		
				Constructor constructor = classe.getConstructor(null);
				controleur = (GestionCongeAvecArreteAnnulation)constructor.newInstance(null);

			} catch (Exception e) {
				try {
					// essayer avec un constructeur qui a comme paramètre le nom d'entité (cas des congés qui gèrent plusieurs types de congés)
					Constructor constructor = classe.getConstructor(new Class[] {java.lang.String.class});
					controleur = (GestionCongeAvecArreteAnnulation)constructor.newInstance(new Object[] {entityName()});
				} catch (Exception e1) {
					throw e1;
				}
			}
			controleur.init(editingContext(),currentConge());
			controleur.ajouter(editingContext().globalIDForObject(currentConge().individu()));
			currentConge().addObjectToBothSidesOfRelationshipWithKey(controleur.currentConge(),"congeDeRemplacement");
			controleur.preparerPourRemplacement();
			currentConge().absence().setEstValide(false);
			controleur.updateDisplayGroups();
			// s'enregistrer pour recevoir les notifications lors de la fermeture de la fenêtre de saisie du nouveau congé
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("terminer",new Class[] {NSNotification.class}),TERMINER_AFFICHAGE,null);
		} catch (Exception e) {
			LogManager.logException(e);
		}
	}
	// Notifications
	//	 déclenchée suite à l'ajout d'un congé de remplacement et la fermeture de la fenêtre du congé de remplacement
	public void terminer(NSNotification aNotif) {
		terminer();
	}
	public  void imprimerArrete(NSNotification aNotif) {
		GestionArrete gestionnaire = (GestionArrete)aNotif.object();
		if (gestionnaire != null) {
			if (gestionnaire == gestionArreteAnnulation) {
				LogManager.logDetail(this.getClass().getName() + " Impression de l'arrêté d'annulation " + gestionArreteAnnulation.arreteCourant());
			} else {
				super.imprimerArrete(aNotif);
			}
		}
	}
	public void gererModificationArrete(NSNotification aNotif) {
		GestionArrete gestionnaire = (GestionArrete)aNotif.object();
		if (gestionnaire != gestionArreteAnnulation) {	
			// une modification a été faite dans le gestionnaire d'arrêté
			Arrete arretePrincipal = gestionnaire.arreteCourant();
			afficherVueArrete(arretePrincipal);
			updateDisplayGroups();
			controllerDisplayGroup().redisplay();
		} else {
			controllerDisplayGroup().redisplay();
		}
	}
	public void gererSignatureArrete(NSNotification aNotif) {
		if (aNotif.object() == gestionArrete() || aNotif.object() == gestionArreteAnnulation) {
			controllerDisplayGroup().redisplay();
		}
	}

	// méthodes du controller DG
	/** on ne peut pas modifier la declaration d'un conge remplace */
	public boolean peutModifierDeclaration() {
		if (currentConge() == null || currentConge().estAnnule()) {
			return false;
		} else if (gestionArreteAnnulation != null) {
			//	pour les cas où on vient juste de saisir les données pour l'arrêté d'annulation
			return gestionArreteAnnulation.arreteCourant().date() == null && gestionArreteAnnulation.arreteCourant().numero() == null;
		} else {
			return true;
		}
	}
	public boolean peutModifierConge() {
		return peutModifierDeclaration() && peutModifier();
	}
	public boolean peutModifierDateFin() {
		if (super.peutModifierDateFin()) {
			return peutModifierConge();
		} else {
			return false;
		}
	}
	public boolean peutAfficherArreteAnnulation() {
		return currentConge() != null && currentConge().congeDeRemplacement() != null;
	}
	/** on ne peut annuler un arrete que si l'arrete a ete signe et qu'il n'existe pas de
	 * conge de remplacement
	 */
	public boolean peutAnnulerArrete() {
		return !peutModifier() && currentConge() != null && currentConge().congeDeRemplacement() == null && typeTraitement() == MODE_MODIFICATION;
	}
	// méthodes protégées
	protected void init(EOEditingContext editingContext,CongeAvecArreteAnnulation conge) {
		setEditingContext(editingContext);
		this.congeRemplace = conge;
	}
	protected void prepareInterface() {
		super.prepareInterface();
		// s'enregistrer pour recevoir les notifications de modification dans un arrêté	
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("gererModificationArrete", new Class[] {NSNotification.class}),GestionArrete.ARRETE_MODIFIE,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("gererSignatureArrete", new Class[] {NSNotification.class}),GestionArrete.ARRETE_SIGNE,null);
	}
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) {
			if (gestionArreteAnnulation != null) {
				Arrete arreteAnnulation = gestionArreteAnnulation.arreteCourant();
				if (arreteAnnulation != null) {
					modifierAvecArreteAnnulation(currentConge(),arreteAnnulation.numero(),arreteAnnulation.date());
				}
			}
			if (congeRemplace() != null) {
				modifierCongeRemplace();
			}	
			return true;	
		} else {
			return false;
		}
	}
	protected void traitementsApresValidation() {
		// Pour fermer toutes les fenêtres si un ajout de congé a été fait
		NSNotificationCenter.defaultCenter().postNotification(TERMINER_AFFICHAGE,null);
	}
	protected void modifierValiditeAbsencePourConge(CongeAvecArreteAnnulation conge,boolean estValide) {
		EOAbsences absence = conge.absence();
		absence.setEstValide(estValide);
	}
	// Gestion des arrêtés d'annulation
	/** un arr&ecirc;t&eacute qui supporte la signature peut etre annule ou remplace.
	 * Les sous-classes peuvent surcharger si elles ont des comportements variables */
	protected abstract boolean supporteSignature();

	/** surcharger pour faire des traitements specifiques pour le conge de remplacement suite a l'annulation d'un conge */
	protected abstract void preparerPourRemplacement();
	/** return true si supporte les cong&eacute;s de remplacement */
	protected abstract boolean supporteCongesRemplaces();
	protected CongeAvecArreteAnnulation congeRemplace() {
		return congeRemplace;
	}
	protected void modifierCongeRemplace() {
		modifierAvecArreteAnnulation(congeRemplace(),currentConge().noArrete(),currentConge().dateArrete());
	}
	// Gestion des arrêtés d'annulation
	/** modifie les numeros et dates d'annulation d'un conge */
	protected void modifierAvecArreteAnnulation(CongeAvecArreteAnnulation conge,String numero,NSTimestamp date) {
		boolean memesDates = memesDatesArreteAnnulation(conge,date);
		boolean memesNumeros = memesNumeroArreteAnnulation(conge,numero);
		if (!memesDates || !memesNumeros) {
			// modifier  ce congé
			if (!memesDates) {
				conge.setDAnnulation(date);
				modifierValiditeAbsencePourConge(conge,!conge.estAnnule());
			}
			if (!memesNumeros) {
				conge.setNoArreteAnnulation(numero);
				modifierValiditeAbsencePourConge(conge,!conge.estAnnule());
			}
		}
	}

	protected boolean peutModifierArrete() {
		return currentConge() != null && currentConge().estAnnule() == false &&
		(congesRemplaces == null || congesRemplaces.count() == 0 || (currentConge().noArrete() == null && currentConge().dateArrete() == null)) ;
	}
	/** on ne peut modifier un arrete que si il n'est pas remplace */   
	protected boolean peutModifierArreteAnnulation() {
		return currentConge() != null && currentConge().congeDeRemplacement() == null;
	}
	protected boolean memesDatesArreteAnnulation(CongeAvecArreteAnnulation conge,NSTimestamp dateAnnulation) {
		return (dateAnnulation == null && conge.dAnnulation() == null) || 
		(dateAnnulation != null && conge.dAnnulation() != null && DateCtrl.isSameDay(conge.dAnnulation(),dateAnnulation));
	}
	protected boolean memesNumeroArreteAnnulation(CongeAvecArreteAnnulation conge,String numeroAnnulation) {
		return (numeroAnnulation == null && conge.noArreteAnnulation() == null) || 
		(numeroAnnulation != null && conge.noArreteAnnulation() != null && conge.noArreteAnnulation().equals(numeroAnnulation));
	}
	// méthodes privées
	private CongeAvecArreteAnnulation currentConge() {
		return (CongeAvecArreteAnnulation)displayGroup().selectedObject();
	}
	private void preparerVueArrete() {
		if (supporteSignature() && vueArreteAnnulation != null && contenuVueArreteAnnulation != null) {
			// il y a une gestion des arrêtés d'annulation
			gestionArreteAnnulation = new GestionArrete(editingContext());
			gestionArreteAnnulation.setModificationEnCours(true);
			gestionArreteAnnulation.setModeCreation(typeTraitement() == GestionConge.MODE_CREATION);
			contenuVueArreteAnnulation.add(gestionArreteAnnulation.component());
			afficherVueArrete(null);
		} 
	}
	private void afficherVueArrete(Arrete arreteAnnule) {
		if (gestionArreteAnnulation == null) {
			return;
		}
		if (vueArreteAnnulation != null && contenuVueArreteAnnulation != null) {
			boolean visible = true;
			if (arreteAnnule == null)	{	// au démarrage
				Arrete arreteAnnulation = arreteAnnulationInitial();
				//	n'afficher la vue d'annulation que que si l'arrêté d'annulation comporte un numéro ou une date d'arrêté
				visible = currentConge().noArrete() != null || currentConge().dateArrete() != null || (supporteSignature() && currentConge().estSigne()) ||
				currentConge().noArreteAnnulation() != null || currentConge().dAnnulation() != null;	// cas où le congé est remplacé
				gestionArreteAnnulation.afficherArrete(arreteAnnulation,null,false,true);

			} else {
				visible = (supporteSignature() && arreteAnnule.estSigne()) || arreteAnnule.numero() != null || arreteAnnule.date() != null;
			}
			vueArreteAnnulation.setVisible(visible);
		}
	}
	private Arrete arreteAnnulationInitial() {
		if (currentConge() == null) {
			return null;
		} else {
			// l'arrêté d'annulation n'est modifiable pour un congé annulé que si il n'y a pas de congé de remplacement
			return new Arrete(currentConge().dAnnulation(),currentConge().noArreteAnnulation(),null,peutModifierArreteAnnulation());
		}
	}
	private void preparerCongesRemplaces() {
		if (typeTraitement() == MODE_CREATION || supporteCongesRemplaces() == false) {
			congesRemplaces = null;
		} else {
			NSArray args = new NSArray(currentConge());
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("congeDeRemplacement = %@",args);
			EOFetchSpecification fs = new EOFetchSpecification(entityName(),qualifier,null);
			congesRemplaces = editingContext().objectsWithFetchSpecification(fs);
		}
	}
}

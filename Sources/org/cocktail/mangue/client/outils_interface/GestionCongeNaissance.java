/*
 * Created on 26 janv. 2006
 *

 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.conges.CongeNaissance;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOStage;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe de base pour mod&eacute;liser le comportement des cong&eacute;s de maternit&eacute;, paternit&eacute; et adoption.
 * G&egrave;re les modifications des dates de stage si l'individu est en stage pendant le cong&eacute; de naissance<BR>
 * @author christine
 */
public abstract class GestionCongeNaissance extends GestionCongeAvecArreteAnnulation {
	private static int NB_MOIS_MINIMUM = 6;
	private NSTimestamp datePremiereAffectation = null;
	private int nbMoisDepuisPremiereAffectation;
	private boolean peutAvoirCongeSansTraitement;

	public GestionCongeNaissance(String nomEntite,String titreFenetre) {
		super(nomEntite,titreFenetre);
		nbMoisDepuisPremiereAffectation = -1;
	}
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		super.displayGroupDidSetValueForObject(group, value, eo, key);
		if (key.equals("dateDebutFormatee")) {
			preparerPourCongeSansTraitement();
		}
	}
	// méthodes du controller DG
	/** le conge sans traitement ne s'applique qu'aux contractuels */
	public boolean peutAvoirCongeSansTraitement() {
		return peutAvoirCongeSansTraitement;
	}
	// On surcharge cette méthode pour être sûr que le congé est dans le display group et sélectionné
	public void afficherFenetre() {
		preparerDatePremiereAffectation();
		preparerPourCongeSansTraitement();
		super.afficherFenetre();
	}

	// méthodes protégées
	/** gere la prolongation des durees de stage pour les conges de naissance */
	protected boolean traitementsAvantValidation() {	
		if (super.traitementsAvantValidation()) { 	
			NSArray stages = EOStage.findForIndividuEtPeriode(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin(),false);
			if (stages != null && stages.count() > 0) {
				EODialogs.runInformationDialog("Attention","L'agent est en stage pendant cette période, pensez à modifier la date de la fin de stage");	        		
			}
			if (nbMoisDepuisPremiereAffectation >= 0 && nbMoisDepuisPremiereAffectation < NB_MOIS_MINIMUM) {
				EODialogs.runInformationDialog("Attention", "Ce contractuel est embauché depuis moins de 6 mois, il bénéficie donc d'un congé sans traitement.\nN'oubliez pas de lui signaler");
			}
			return true;
		} else {
			return false;
		}
	}
	
	protected void traitementsApresSuppression() {}
	
	//	 Gestion des arrêtés
	protected EOTypeGestionArrete typeGestionArrete() {
		if (currentConge() != null && currentConge().individu() != null) {
			return currentConge().individu().rechercherTypeGestionArretePourPeriode(nomTableCongeContractuel(),nomTableCongeFonctionnaire(),currentConge().dateDebut(),currentConge().dateFin());
		} else {
			return null;
		}
	}
	protected boolean supporteSignature() {
		return true;
	}
	protected boolean supporteCongesRemplaces() {
		return true;
	}
	/** retourne le nom de la table de cong&eeacute; pour le fonctionnaire */
	protected abstract String nomTableCongeFonctionnaire();
	/** retourne le nom de la table de cong&eeacute; pour le contractuel */
	protected abstract String nomTableCongeContractuel();
	
	
	/**
	 * 
	 */
	protected void preparerPourCongeSansTraitement() {
		peutAvoirCongeSansTraitement = true;
		nbMoisDepuisPremiereAffectation = -1;
		if (currentConge() != null && currentConge().dateDebut() != null && datePremiereAffectation != null) {

			boolean estFonctionnaire = EOCarriere.aCarriereEnCoursSurPeriode(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin()) && (EOChangementPosition.peutAvoirContratSurPeriode(editingContext(), currentConge().individu(), currentConge().dateDebut(), currentConge().dateFin()) == false);
			peutAvoirCongeSansTraitement = !estFonctionnaire && EOContrat.aContratEnCoursSurPeriode(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin());
			if (peutAvoirCongeSansTraitement) {
				if (datePremiereAffectation != null) {
					nbMoisDepuisPremiereAffectation = DateCtrl.calculerDureeEnMois(datePremiereAffectation, currentConge().dateDebut(), true).intValue();
					// On vérifie si la date depuis la première durée d'affectation est inférieure à 6 mois, 
					//en quel cas on force le témoin sur le congé sans traitement
					if (nbMoisDepuisPremiereAffectation < NB_MOIS_MINIMUM) {
//						peutAvoirCongeSansTraitement = false;
						if (currentConge().estCongeSansTraitement() == false) {
							currentConge().setEstCongeSansTraitement(true);
						}
					} else {
						if (currentConge().estCongeSansTraitement()) {
							currentConge().setEstCongeSansTraitement(false);
						}
					}
				} else {
					EODialogs.runInformationDialog("Attention", "Pas d'affectation pour cet individu, vous ne pourrez pas régler le témoin de congé sans traitement");
					nbMoisDepuisPremiereAffectation = -1;
				}
			} else {	// Il s'agit d'un fonctionnaire ou d'un individu qui n'a pas de contrat en cours
				if (estFonctionnaire && currentConge().estCongeSansTraitement()) {
					currentConge().setEstCongeSansTraitement(false);
				}
			}
		}
		updateDisplayGroups();
	}
	
	/**
	 * 
	 * @return
	 */
	private CongeNaissance currentConge() {
		return (CongeNaissance)displayGroup().selectedObject();
	}
	
	// Prépare la date du premier contrat de rémunération principale ou du premier segment de carrière
	private void preparerDatePremiereAffectation() {
		datePremiereAffectation = null;
		NSArray<EOContrat> contrats = EOContrat.rechercherContratsSupportantCongePourIndividu(editingContext(), currentConge().individu());
		if (contrats.count() > 0) {
			contrats = EOSortOrdering.sortedArrayUsingKeyOrderArray(contrats, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
			datePremiereAffectation = ((EOContrat)contrats.get(0)).dateDebut();
		}
		LogManager.logDetail(this.getClass().getName() + " - datePremiereAffectation : " + datePremiereAffectation);
		NSArray<EOCarriere> carrieres = EOCarriere.findForIndividu(editingContext(), currentConge().individu());
		if (carrieres.count() > 0) {
			carrieres = EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
			EOCarriere carriere = (EOCarriere)carrieres.objectAtIndex(0);
			if (datePremiereAffectation == null || DateCtrl.isAfter(datePremiereAffectation, carriere.dateDebut())) {
				datePremiereAffectation = carriere.dateDebut();
			}
		}
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import org.cocktail.client.components.DialogueModalAvecArbre;
import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/** Permet de s&eacute;lectionner des associations en fournissant le type du noeud d'entr&eacute;e dans la table Association
 * &agrave; partir du r&eacute;seau d'associations. Fournir le libell&eacute; du type (voir TypeAssociation)<BR>
 * @author christine
 *
 */

public class ChoixAssociation extends DialogueModalAvecArbre {
	private static ChoixAssociation sharedInstance = null;
	private String typeAssociation;
	
	/** Méthodes statiques */
	public static ChoixAssociation sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new ChoixAssociation();
		}
		return sharedInstance;
	}
	/**
	 * Selection d'une association &agrave; partir d'une association racine
	 * @param typeAssociation type de l'association recherch&eacute;e
	 * @return l'association s&eacute;lectionn&eacute;e ou null si le libell&eacute; racine est nul ou que l'utilisateur a annul&eacute;
	 *
	 */
	public EOAssociation selectionnerAssociation(EOEditingContext editingContext,String libelleTypeAssociation) {
		if (libelleTypeAssociation == null) {
			return null;
		}
		this.typeAssociation = libelleTypeAssociation;
		return (EOAssociation)super.selectionnerObjet(editingContext,false,true);
	}
	protected String attributeForDisplay() {
		return EOAssociation.LIBELLE_LONG_KEY;
	}
	protected String nomEntiteAffichee() {	
		return EOAssociation.ENTITY_NAME;
	}
	protected String parentRelationship() {
		return null;
	}
	protected String attributeOfParent() {
		return null;
	}
	protected String parentAttributeOfChild() {
		return null;
	}
	protected EOQualifier qualifierForColumn() {
		return EOQualifier.qualifierWithQualifierFormat("typeAssociation.tasLibelle = %@ AND assRacine = 'O'", new NSArray(typeAssociation));
	}
	protected EOQualifier restrictionQualifier() {
		return null;
	}

	protected String titreArbre() {
		return "Roles";
	}

	protected String titreFenetre() {
		return "Sélection d'un rôle";
	}
	/** On peut toujours s&eacute;lectionner une association sauf la racine */
	protected boolean peutSelectionnerObjet(EOGenericRecord objet) {	
		if (objet != null) {
			return ((EOAssociation)objet).libelleLong().equals(typeAssociation) == false;
		} else {
			return false;
		}
	}
}

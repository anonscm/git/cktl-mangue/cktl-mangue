/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import org.cocktail.mangue.modele.mangue.conges.CongeAvecRemiseEnCause;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Gere les conges qui peuvent remettre en cause d'autres conges :<BR>
 * Dans le cas ou un conge est insere avant oun conge de meme type, les arretes
 * de conge comportent des details de traitement qui peuvent etre erronnes. 
 * Ces conges doivent etre annules si les arretes ont etes signes 
 * ou bien l'arrete doit etre reimprimer ou bien le temoin est mis a jour manuellement
 * @author christine
 *
 */
public abstract class GestionCongeAvecRemiseEnCause extends	GestionCongeAvecArreteAnnulation {
	private NSArray congesRemisEnCause;	// pour pouvoir revenir en arrière dans le cas d'une erreur dans le validateForSave ou d'une annulation de l'utilisateur
	private boolean remettreEnCause;	// on remet en cause si les dates changent où qu'il y a une annulation
	private boolean avaitCongeDeRemplacement;	// utiliser pour les remises en cause
	/** Constructeur
     * @param nomEntite	nom entite de conge
     * @param titreFenetre titre du dialogue affiche
     */
	
    public GestionCongeAvecRemiseEnCause(String nomEntite,String titreFenetre) {
        super(nomEntite,titreFenetre);
    }
    
    //  Méthodes de délégation du display group
    public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
    		super.displayGroupDidSetValueForObject(group,value,eo,key);
    		if (key.equals("dateDebutFormatee") || key.equals("dateFinFormatee")) {
    			remettreEnCause = true;
    		}
    }
    public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
    		if (typeTraitement() == MODE_MODIFICATION) {
    			// on charge maintenant les congés remis en cause, de manière à pouvoir invalider leur état dans le cas où les dates
    			// sont changées
    			congesRemisEnCause = currentConge().congesARemettreEnCause();
    		}
    		remettreEnCause = false;
    		avaitCongeDeRemplacement = (currentConge().congeDeRemplacement() != null);
    }
    // Méthodes du controller DG
    public boolean peutChangerRemiseEnCause() {
    		return currentConge().estRemisEnCause();
    }
    // méthodes protégées
    protected boolean supporteCongesRemplaces() {
		 return true;
	 }
	/** Annule la remise en cause ou au contraire force la remise en cause selon que le conge courant est annule ou non */
	protected boolean traitementsAvantValidation() {
		// Récupérer les anciennes valeurs des arrêtés d'annulation avant les traitements de super
		NSTimestamp dateAnnulation = currentConge().dAnnulation();
		String noAnnulation = currentConge().noArreteAnnulation();
		if (super.traitementsAvantValidation()) {
			if ((currentConge().estAnnule() || arreteImprime()) && currentConge().estRemisEnCause()) {
				// le congé a été annulé ou l'arrêté a été regénéré, il n'est donc plus remis en cause
				currentConge().setEstRemisEnCause(false);
			}
			if (remiseEnCause(dateAnnulation,noAnnulation)) {
				if (congesRemisEnCause != null && congesRemisEnCause.count() > 0) {
					// remettre dans l'état précédent les congés mis en cause, on passe pour la deuxième fois dans traitementAvantValidation ou bien on est en mode modification
					for (java.util.Enumeration<CongeAvecRemiseEnCause> e = congesRemisEnCause.objectEnumerator();e.hasMoreElements();) {
						CongeAvecRemiseEnCause conge = e.nextElement();
						if (conge != currentConge() && conge.estRemisEnCause()) {	// les qualifiers de période ont pu aussi sélectionner ce congé
							conge.setEstRemisEnCause(false);
						}
					}
				}
				return remiseEnCauseEffectuee();
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	protected boolean traitementsAvantSuppression() {
		if (currentConge().estRemisEnCause()) {
			// ce congé est lui-même remis en cause par d'autres congés
			return true;
		}
		return remiseEnCauseEffectuee();
	}

	// méthodes privées
	private CongeAvecRemiseEnCause currentConge() {
		return (CongeAvecRemiseEnCause)displayGroup().selectedObject();
	}
	// on remet en cause si les dates ont changé ou une annulation a eu lieu
	private boolean remiseEnCause(NSTimestamp dateAnnulation,String noAnnulation) {
		if (remettreEnCause) {
			return true;
		} else if ((avaitCongeDeRemplacement && currentConge().congeDeRemplacement() == null) || (!avaitCongeDeRemplacement && currentConge().congeDeRemplacement() != null)) {
			return true;
		} else if (memesDatesArreteAnnulation(currentConge(),dateAnnulation) == false || memesNumeroArreteAnnulation(currentConge(),noAnnulation) == false) {
			return true;
		}
		return false;
	}
	private boolean remiseEnCauseEffectuee() {
		boolean utilisateurInforme = false;
		NSArray congesEnCause = currentConge().congesARemettreEnCause();
		for (java.util.Enumeration<CongeAvecRemiseEnCause> e = congesEnCause.objectEnumerator();e.hasMoreElements();) {
			CongeAvecRemiseEnCause conge = e.nextElement();
			if (conge != currentConge()) {	// les qualifiers de période ont pu aussi sélectionner ce congé
				if (utilisateurInforme == false) {
					if (EODialogs.runConfirmOperationDialog("Attention","Certains congés vont être remis en cause, il se peut que leurs arrêtés deviennent invalides. Souhaitez-vous continuer ?","Oui","Non")) {
						utilisateurInforme = true;
						EODialogs.runInformationDialog("Attention","Pensez à vérifier les congés remis en cause");
					} else {
						return false;
					}
				}
				conge.setEstRemisEnCause(true);
			}
		}
		// annuler la remise en cause sur le congé qui a été remplacé
		if (congeRemplace() != null && ((CongeAvecRemiseEnCause)congeRemplace()).estRemisEnCause()) {
			((CongeAvecRemiseEnCause)congeRemplace()).setEstRemisEnCause(false);
		}
		congesRemisEnCause = congesEnCause;
		
		return true;
	}
}

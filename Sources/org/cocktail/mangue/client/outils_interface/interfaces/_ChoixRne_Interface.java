// _ChoixRne_Interface.java
// Created on 13 janv. 2011 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _ChoixRne_Interface extends COFrame {

    /** Creates new form _ChoixRne_Interface */
    public _ChoixRne_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cODisplayGroup1 = new org.cocktail.component.CODisplayGroup();
        listeAffichage = new org.cocktail.component.COTable();
        cOButton10 = new org.cocktail.component.COButton();
        boutonValider = new org.cocktail.component.COButton();
        jLabel1 = new javax.swing.JLabel();
        cOTextField1 = new org.cocktail.component.COTextField();
        cOTextField2 = new org.cocktail.component.COTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cOTextField3 = new org.cocktail.component.COTextField();
        boutonRechercher = new org.cocktail.component.COButton();

        cODisplayGroup1.setEntityName("Rne");
        cODisplayGroup1.setHasDelegate(true);
        cODisplayGroup1.setIsMainDisplayGroupForController(true);

        setControllerClassName("org.cocktail.mangue.client.outils_interface.ChoixRne");

        listeAffichage.setColumns(new Object[][] {{null,"code",new Integer(0),"Code",new Integer(0),new Integer(100),new Integer(1000),new Integer(10)},{null,"libelleLong",new Integer(2),"Libellé",new Integer(0),new Integer(220),new Integer(1000),new Integer(10)},{null,"ville",new Integer(2),"Ville",new Integer(0),new Integer(140),new Integer(1000),new Integer(10)}});
        listeAffichage.setDisplayGroupForTable(cODisplayGroup1);
        listeAffichage.setResizingAllowed(true);

        cOButton10.setActionName("cancel");
        cOButton10.setBorderPainted(false);
        cOButton10.setIconName("annuler16.gif");

        boutonValider.setActionName("validate");
        boutonValider.setBorderPainted(false);
        boutonValider.setEnabledMethod("peutValider");
        boutonValider.setIconName("valider16.gif");

        jLabel1.setFont(new java.awt.Font("Helvetica", 0, 11));
        jLabel1.setText("Code");

        cOTextField1.setValueName("code");

        cOTextField2.setValueName("libelle");

        jLabel2.setFont(new java.awt.Font("Helvetica", 0, 11));
        jLabel2.setText("Libellé");

        jLabel3.setFont(new java.awt.Font("Helvetica", 0, 11));
        jLabel3.setText("Ville");

        cOTextField3.setValueName("ville");

        boutonRechercher.setActionName("rechercher");
        boutonRechercher.setBorderPainted(false);
        boutonRechercher.setEnabledMethod("peutRechercher");
        boutonRechercher.setIconName("loupe16.gif");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 479, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(cOButton10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(boutonValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(layout.createSequentialGroup()
                        .add(jLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jLabel2)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 158, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jLabel3)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 115, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(boutonRechercher, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(20, 20, 20)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .add(cOButton10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(boutonValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 397, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel2)
                    .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel3)
                    .add(boutonRechercher, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COButton boutonRechercher;
    public org.cocktail.component.COButton boutonValider;
    public org.cocktail.component.COButton cOButton10;
    public org.cocktail.component.CODisplayGroup cODisplayGroup1;
    public org.cocktail.component.COTextField cOTextField1;
    public org.cocktail.component.COTextField cOTextField2;
    public org.cocktail.component.COTextField cOTextField3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    public org.cocktail.component.COTable listeAffichage;
    // End of variables declaration//GEN-END:variables
                  

}

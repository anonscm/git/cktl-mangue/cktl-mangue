// _GestionArrete_Interface.java
// Created on 29 sept. 2010 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _GestionArrete_Interface extends COFrame {

    /** Creates new form _GestionArrete_Interface */
    public _GestionArrete_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new org.cocktail.component.CODisplayGroup();
        vuePrincipale = new org.cocktail.component.COView();
        jLabel1 = new javax.swing.JLabel();
        cOTextField1 = new org.cocktail.component.COTextField();
        jLabel2 = new javax.swing.JLabel();
        cOTextField2 = new org.cocktail.component.COTextField();
        boiteSignature = new org.cocktail.component.COCheckbox();
        cOButton1 = new org.cocktail.component.COButton();

        displayGroup.setHasDelegate(true);
        displayGroup.setIsMainDisplayGroupForController(true);
        displayGroup.setKeys(new Object[] {"dateFormatee","estSigne","numero"});

        setControllerClassName("org.cocktail.mangue.client.outils_interface.GestionArrete");
        setSize(new java.awt.Dimension(478, 48));

        vuePrincipale.setIsBox(true);

        jLabel1.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel1.setText("Numéro");

        cOTextField1.setDisplayGroupForValue(displayGroup);
        cOTextField1.setEnabledMethod("peutModifierArrete");
        cOTextField1.setSupportsBackgroundColor(true);
        cOTextField1.setValueName("numero");

        jLabel2.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel2.setText("Date");

        cOTextField2.setDisplayGroupForValue(displayGroup);
        cOTextField2.setEnabledMethod("peutModifierArrete");
        cOTextField2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cOTextField2.setSupportsBackgroundColor(true);
        cOTextField2.setValueName("dateFormatee");

        boiteSignature.setDisplayGroupForValue(displayGroup);
        boiteSignature.setEnabledMethod("peutSigner");
        boiteSignature.setText("Signé");
        boiteSignature.setValueName("estSigne");

        cOButton1.setActionName("imprimer");
        cOButton1.setBorderPainted(false);
        cOButton1.setEnabledMethod("peutImprimer");
        cOButton1.setIconName("Imprimante.gif");

        org.jdesktop.layout.GroupLayout vuePrincipaleLayout = new org.jdesktop.layout.GroupLayout(vuePrincipale);
        vuePrincipale.setLayout(vuePrincipaleLayout);
        vuePrincipaleLayout.setHorizontalGroup(
            vuePrincipaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vuePrincipaleLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 166, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 83, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(boiteSignature, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 7, Short.MAX_VALUE)
                .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 46, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        vuePrincipaleLayout.setVerticalGroup(
            vuePrincipaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, vuePrincipaleLayout.createSequentialGroup()
                .add(5, 5, 5)
                .add(vuePrincipaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 32, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(vuePrincipaleLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel1)
                        .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jLabel2)
                        .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(boiteSignature, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vuePrincipale, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vuePrincipale, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COCheckbox boiteSignature;
    public org.cocktail.component.COButton cOButton1;
    public org.cocktail.component.COTextField cOTextField1;
    public org.cocktail.component.COTextField cOTextField2;
    public org.cocktail.component.CODisplayGroup displayGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    public org.cocktail.component.COView vuePrincipale;
    // End of variables declaration//GEN-END:variables
                  

}

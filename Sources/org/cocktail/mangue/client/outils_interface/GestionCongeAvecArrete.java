/*
 * Created on 10 févr. 2006
 *
 * Modèle pour la gestion des congés avec arrêté
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.impression.GestionImpression;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.Arrete;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArrete;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Mod&egrave;le pour la gestion des conges avec arrete<BR>
 * G&egrave;re l'affichage des vues arr&ecirc;t&eacute;s : uniquement une fois que les dates de d&eacute;but et fin du cong&eacute; sont saisies<BR>
 * 
 * * @author christine
 * */
// 02/11/2010 - affichage des destinataires par ordre de priorité
public abstract class GestionCongeAvecArrete extends GestionConge {
	public EOView vueArretes,vueArrete,contenuVueArrete;
	private GestionArrete gestionArrete;
	private boolean arreteImprime;

	private DestinatairesSelectCtrl ctrlDestinataires;

	/** Constructeur
	 * @param nomEntite	nom entit&eacute; de cong&eacute;
	 * @param titreFenetre titre du dialogue affich&eacute;
	 */
	public GestionCongeAvecArrete(String nomEntite,String titreFenetre) {
		super(nomEntite,titreFenetre);
	}
	// Méthodes de délégation du display group
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("dateDebutFormatee") || key.equals("dateFinFormatee")) {
			changerAffichageArrete();
		} 
		updateDisplayGroups();
	}
	// autres
	public void afficherFenetre() {
		preparerVueArrete();	// on ne le fait que là pour que tout soit initialisé
		super.afficherFenetre();
	}
	
	/**
	 * 
	 */
	public void imprimer() {

		if (typeGestionArrete() != null && typeGestionArrete().peutImprimerArrete()) {
			if (currentConge() instanceof CongeAvecArreteAnnulation) {
				if (((CongeAvecArreteAnnulation)congeCourant()).estSigne()) {
					EODialogs.runInformationDialog("Attention", "L'arrêté de ce congé est signé, il ne peut pas être imprimé.");
					return;
				}
				if (((CongeAvecArreteAnnulation)congeCourant()).estAnnule()) {
					EODialogs.runInformationDialog("Attention", "Ce congé est annulé, l'arrêté ne peut pas être imprimé.");
					return;
				}
			}

			preparerVueArrete();
			imprimer(true);
			// On veut recevoir les notifications de fin d'impression pour pouvoir terminer
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("terminer",new Class [] {NSNotification.class}),GestionImpression.FIN_IMPRESSION,null);
		} else {
			EODialogs.runInformationDialog("Attention", "Pas d'arrêté produit localement pour ce type de personnel");
		}
	}
	// Notifications
	public void terminer(NSNotification aNotif) {
		terminer();
		NSNotificationCenter.defaultCenter().removeObserver(this,GestionImpression.FIN_IMPRESSION,null);
	}
	public  void imprimerArrete(NSNotification aNotif) {
		GestionArrete gestionnaire = (GestionArrete)aNotif.object();
		if (gestionnaire != null && gestionnaire == gestionArrete) {
			imprimer(false);
		}
	}
	/** recupere les destinataires et lance l'impression de l'arrete */
	public void getDestinataires(NSNotification aNotif) {
		// Récupérer les destinataires (c'est un tableau de globalIDs de EODestinataire)
		// Lancer l'impression
		Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class,Boolean.class};
		Object[] parametres = new Object[]{editingContext().globalIDForObject(currentConge()),(NSArray)aNotif.object(),new Boolean(false)};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerArreteConge",classeParametres,parametres,"Arrete_" + entityName() + "_" + currentConge().noArrete() + "_" + currentConge().individu().noIndividu(),"Impression de l'arrêté");
			arreteImprime = true;
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	// méthodes du controllerDG
	/** on ne peut valider que si la date de debut et la date de fin sont fournies */
	public boolean peutValider() {
		if (super.peutValider()) {
			return currentConge().dateFin() != null;
		}  else {
			return false;
		}
	}
	/** on peut modifier les champs d'un conge que si il n'y a pas d'arrete signe */
	public boolean peutModifier() {
		if (super.peutModifier()) {
			if (gestionArrete != null && gestionArrete.arreteCourant() != null && gestionArrete.arreteCourant().estSigne()) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	// méthodes protégées
	protected void preparerAvantChargement() {
		super.preparerAvantChargement();
		// s'enregistrer pour recevoir les notifications d'impression d'arrêté
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("imprimerArrete",new Class[] {NSNotification.class}),GestionArrete.IMPRIMER_ARRETE,null);
	}
	protected void terminer() { 
		if (gestionArrete != null) {
			gestionArrete.terminer();
		}
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) {
			try {
				// arrêté géré par établissement
				if (typeTraitement() == MODE_CREATION && typeGestionArrete() != null && (typeGestionArrete().peutImprimerArrete() || typeGestionArrete().gereSignature())) {
					congeCourant().takeValueForKey(CocktailConstantes.VRAI,"temGestEtab");
				}
			} catch (Exception e) {
				// au cas où pas de gestion de signature etablissement (ne devrait pas se produire), donc pas de champ temGestEtab
				LogManager.logException(e);
			}
			if (congeCourant() != null && congeCourant() instanceof CongeAvecArrete && gestionArrete != null) {
				Arrete arrete = gestionArrete.arreteCourant();
				if (arrete != null) {
					((CongeAvecArrete)congeCourant()).setDateArrete(arrete.date());
					((CongeAvecArrete)congeCourant()).setNoArrete(arrete.numero());
					if (supporteSignature() && congeCourant() instanceof CongeAvecArreteAnnulation) {
						((CongeAvecArreteAnnulation)congeCourant()).setTemConfirme(arrete.temSignature());
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}
	/** Lors d'une erreur de validation, si on est en mode creation et que l'arree vient d'etre signe,
	 * on enleve la signature pour permettre la modification des donnees
	 */
	protected void traitementsApreErreurValidation() {
		if (typeTraitement() == MODE_CREATION && supporteSignature() && congeCourant() instanceof CongeAvecArreteAnnulation) {
			((CongeAvecArreteAnnulation)congeCourant()).setTemConfirme(CocktailConstantes.FAUX);
			gestionArrete.arreteCourant().setEstSigne(false);
			controllerDisplayGroup().redisplay();
			gestionArrete.controllerDisplayGroup().redisplay();
		}
	}
	// Gestion des arrêtés
	//	
	protected boolean arreteImprime() {
		return arreteImprime;
	}
	/** retourne le gestionnaire d'arrete */
	protected GestionArrete gestionArrete() {
		return gestionArrete;
	}
	protected void changerAffichageArrete(boolean estVisible) {
		if (estVisible) {
			changerAffichageArrete();
		} else {
			vueArretes.setVisible(false);
		}
	}
	/** affiche la vue arrete */
	protected void changerAffichageArrete() {
		if (gestionArrete == null) {
			return;
		}
		// on ne peut voir les arrêtés que si les dates de début et fin sont définies
		if (congeCourant() == null || congeCourant().dateDebut() == null || congeCourant().dateFin() == null) {
			vueArretes.setVisible(false);
			return;
		} else {
			vueArretes.setVisible(true);
			vueArrete.setVisible(true);
		}
	}
	/** retourne le type de gestion de l'arrete (etablissement,...) */
	protected abstract EOTypeGestionArrete typeGestionArrete();
	/** retourne true si la signature de l'arrete doit etre geree */
	protected abstract boolean supporteSignature();
	/** retourne true si l'arrete est modifiable */
	protected boolean peutModifierArrete() {
		return true;
	}
	/** Affichage d'un dialogue de type SelectionSimple
	 * @param nomEntite	nom entite a afficher
	 * @param nomMethodeNotification nom de la methode de notification invoquee pour retourner la globalID de la valeur selectionnee
	 * @param qualifier	qualifier de la liste d'objets a afficher
	 */

	// méthodes privées
	private CongeAvecArrete currentConge() {
		return (CongeAvecArrete)displayGroup().selectedObject();
	}
	private void preparerVueArrete() {
		if (vueArretes != null && vueArrete != null && contenuVueArrete != null) {
			gestionArrete = new GestionArrete(editingContext());
			gestionArrete.setModificationEnCours(true);
			gestionArrete.setModeCreation(typeTraitement() == GestionConge.MODE_CREATION);
			contenuVueArrete.add(gestionArrete.component());
			Arrete arrete = arreteInitial();
			if (arrete != null) {
				gestionArrete.afficherArrete(arrete,typeGestionArrete(),supporteSignature(),true);
			}
			changerAffichageArrete();
		} 
	}
	private Arrete arreteInitial() {
		if (currentConge() == null) {
			return null;
		} else if (currentConge() instanceof CongeAvecArreteAnnulation) {
			CongeAvecArreteAnnulation conge = (CongeAvecArreteAnnulation)currentConge();
			return new Arrete(conge.dateArrete(),conge.noArrete(),conge.temConfirme(),peutModifierArrete());
		} else  {
			return new Arrete(currentConge().dateArrete(),currentConge().noArrete());
		}
	}

	/**
	 * 
	 * @param fournirDateArrete
	 */
	private void imprimer(boolean fournirDateArrete) {	// on doit fournir la date d'arrêté si elle est nulle lors d'une impression depuis les absences

		//	vérifier auparavant si on peut valider, sinon afficher un message à l'utilisateur

		if (!peutValider()) {
			EODialogs.runInformationDialog("Erreur", "Impossible d'imprimer l'arrêté.\nToutes les données nécessaires pour ce congé ne sont pas saisies");
			return;
		}
		// Si le numéro d'arrêté n'est pas fourni et qu'il s'agit d'un arrêté géré de façon déconcentrée
		// et qu'il est spécifié dans les paramètres de Mangue que le numéro d'arrêté doit être géré
		// de façon automatique alors générer le numéro d'arrêter
		String numeroArrete = gestionArrete.arreteCourant().numero();
		if (typeGestionArrete().peutImprimerArrete() && numeroArrete == null) {

			if (EOGrhumParametres.isNoArreteAuto()) {
				if (!EODialogs.runConfirmOperationDialog("Attention", "Le numéro d'arrêté va être généré automatiquement. Est-ce ce que vous voulez ?", "Oui", "Non")) {
					return;
				}
				// Rechercher tous les congés pour l'année afin de déterminer le dernier numéro d'arrêté automatique
				int annee = DateCtrl.getYear(currentConge().dateDebut());
				String numeroAuto = "" + annee + "/" ;
				numeroAuto = numeroAuto + StringCtrl.get0Int(numeroArretIncremente(numeroAuto,annee), 2);
				gestionArrete.arreteCourant().setNumero(numeroAuto);	// on force le numéro dans la gestion d'arrêté, lors de la validation il sera récupéré dans le congé
			} else {	// le numéro d'arrêté est obligatoire
				String message = "Vous devez fournir un numéro d'arrêté";
				if (fournirDateArrete) {
					message = message + ". Veuillez modifier ce congé";
				}
				EODialogs.runErrorDialog("Erreur",message);
				return;
			}
		}

		if (currentConge().dateArrete() == null && fournirDateArrete) {
			gestionArrete.arreteCourant().setDate(new NSTimestamp());
		}
		if (!fournirDateArrete) {
			gestionArrete.redisplay();
		}
		if (!validerSansFermerFenetre()) {
			LogManager.logDetail("Impression de l'arrêté - Erreur lors de l'enregistrement des données");
			EODialogs.runInformationDialog("Erreur", "Impossible d'imprimer l'arrêté.\nProblème lors de l'enregistrement des données");
			return;
		}

		try {
			if (ctrlDestinataires == null)
				ctrlDestinataires = new DestinatairesSelectCtrl(editingContext());
			NSArray<EOGlobalID> destinatairesIds = ctrlDestinataires.getDestinatairesGlobalIds();

			if (destinatairesIds != null) {
				// Récupérer les destinataires (c'est un tableau de globalIDs de EODestinataire)
				// Lancer l'impression
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class,Boolean.class};
				Object[] parametres = new Object[]{editingContext().globalIDForObject(currentConge()), destinatairesIds,new Boolean(false)};
				// avec Thread
				UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerArreteConge",classeParametres,parametres,"Arrete_" + entityName() + "_" + currentConge().noArrete() + "_" + currentConge().individu().noIndividu(),"Impression de l'arrêté");
				arreteImprime = true;
			}
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}

	}
	
	/**
	 * 
	 * @param prefixNumeroAuto
	 * @param annee
	 * @return
	 */
	private int numeroArretIncremente(String prefixNumeroAuto,int annee) {
		// Rechercher tous les congés pour déterminer le dernier numéro automatique attribué
		NSArray conges = Conge.rechercherDureesPourIndividuEtPeriode(editingContext(), entityName(), currentConge().individu(),
				DateCtrl.debutAnnee(annee), DateCtrl.finAnnee(annee));
		int valeurIncrementee = 0;
		if (conges != null && conges.count() > 0) {
			conges = EOSortOrdering.sortedArrayUsingKeyOrderArray(conges,new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareDescending)));
			for (java.util.Enumeration<CongeAvecArrete> e = conges.objectEnumerator();e.hasMoreElements();) {
				CongeAvecArrete conge = e.nextElement();
				String noArrete = conge.noArrete();
				if (noArrete != null && noArrete.indexOf(prefixNumeroAuto) >= 0) {	// Vérifier si cela correspond à un numéro automatique
					String num = noArrete.substring(prefixNumeroAuto.length());
					try {
						int numAuto = new Integer(num).intValue();
						if (numAuto > valeurIncrementee) {
							valeurIncrementee = numAuto;
						}
					} catch (Exception exc) {
						// Le numéro d'arrêté n'est pas automatique
					}
				}
			}
		}
		return valeurIncrementee + 1;
	}
}

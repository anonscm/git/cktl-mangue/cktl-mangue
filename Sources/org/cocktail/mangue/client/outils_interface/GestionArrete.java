/*
 * Created on 4 nov. 2005
 *
 *Composant de gestion des arrêtés
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.outils_interface;

import javax.swing.JCheckBox;

import org.cocktail.client.components.ArchiveWithDisplayGroup;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.modele.Arrete;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

/** Gestion des arr&ecirc;t&eacute;s<BR>
 *	Travaille dans l'editing context du composant qui le contient<BR>
 *	Retourne un objet de type Arrete<BR>
 * @author christine
 *
 */
// 10/12/2010 - Adaptation Netbeans
public class GestionArrete extends ArchiveWithDisplayGroup {
	public EOView vuePrincipale;
	public JCheckBox boiteSignature;
	private EOTypeGestionArrete typeGestion;
	private boolean modificationEnCours,modeCreation;	
	/** Notification envoy&eacute;e en cas de modification d'un des champs d'un arrete */
	public static String ARRETE_MODIFIE = "ModificationArrete";
	/** Notification envoyee lors de la signature d'un arrete */
	public static String ARRETE_SIGNE = "ArreteSigne";
	/** Notification envoyee pour demander l'impression d'un arrete; */
	public static String IMPRIMER_ARRETE = "ImpressionArrete";

	/** Constructeur :
	 * @param editingContext editing context de travail
	 */
	public GestionArrete(EOEditingContext editingContext) {
		super(editingContext,"GestionArrete","org.cocktail.mangue.client.outils_interface.interfaces"); // 10/12/2010 - Ajout du package
		super.init();
		modificationEnCours = false;
		modeCreation = false;
		displayGroup().setObjectArray(null);
		displayGroup().setSelectedObject(null);
		displayGroup().setDelegate(this);
		redisplay();
	}
	public void terminer() {
		displayGroup().setDelegate(null);
	}
	// Actions
	public void imprimer() {
		NSNotificationCenter.defaultCenter().postNotification(IMPRIMER_ARRETE,this);
	}
	// méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("estSigne")) {
			LogManager.logDetail("GestionArrete arrete signe");
			// poster une notification pour signaler le changement dans la signature
			NSNotificationCenter.defaultCenter().postNotification(ARRETE_SIGNE,this);
		} else {
			LogManager.logDetail("GestionArrete arrete modifie");
			NSNotificationCenter.defaultCenter().postNotification(ARRETE_MODIFIE,this); 
		}
		controllerDisplayGroup().redisplay();
	}
	// Autres
	public void redisplay() {
		updaterDisplayGroups();
	}
	public void afficherArrete(Arrete arrete,EOTypeGestionArrete typeGestion,boolean afficherSignature,boolean afficher) {
		this.typeGestion = typeGestion;
		// afficherSignature permet de différentier les arrêtés et les arrêtés d'annulation : ces derniers ne requièrent pas de signature
		vuePrincipale.setVisible(afficher);
		// vérifier si gestion de la signature
		if (afficherSignature) {
			if (typeGestion != null && typeGestion.gereSignature()) {
				boiteSignature.setVisible(true);
			} else {
				boiteSignature.setVisible(false);
			}
		} else {
			boiteSignature.setVisible(false);
		}
		if (afficher) {
			Arrete arreteCourant;
			if (arrete != null) {
				arreteCourant = arrete;
			} else {
				arreteCourant = new Arrete();
			} 
			// sélectionner l'objet
			displayGroup().setObjectArray(new NSArray(arreteCourant));
			displayGroup().setSelectedObject(arreteCourant);
		} else {
			displayGroup().setObjectArray(null);
			displayGroup().setSelectedObject(null);
		}
		controllerDisplayGroup().redisplay();
		if (typeGestion != null) {
			LogManager.logDetail("GestionArrete - afficherArrete  : typeGestion " + typeGestion.lGestionArrete() + 
							 " peut signer : " + afficherSignature + " arrete : " + arreteCourant());
		} else {
			LogManager.logDetail("GestionArrete - afficherArrete  : pas de gestion d'arrete");
		}
	}
	// accesseurs
	public boolean modificationEnCours() {
		return modificationEnCours;
	}
	public void setModificationEnCours(boolean aBool) {
		modificationEnCours = aBool;
		controllerDisplayGroup().redisplay();
	}
	public boolean modeCreation() {
		return modeCreation;
	}
	public void setModeCreation(boolean aBool) {
		modeCreation = aBool;
	}
	// méthodes du controller DG
	/** retourne true si l'arr&ecirc;t&eacute; est g&eacuter&eacute; par l'&eacute;tablissement */
	public boolean peutImprimer() {
		return arreteCourant() != null && arreteCourant().estImprimable() && typeGestion != null && typeGestion.peutImprimerArrete() && modeCreation() == false && modificationEnCours() == true;
	}
	/** on ne peut signer un arr&ecirc;t&eacute; que si il a &eacute;t&eacute; d&eacute;fini */
	public boolean peutSigner() {
		// on peut signer un arr&eacute;t&eacute; si celui-ci est d&eacute;fini et qu'il n'a pas encore été signé
		return modificationEnCours && arreteCourant() != null && arreteCourant().numero() != null && !arreteCourant().estSigne();
	}
	/** on ne peut modifier un arr&ecirc;t&eacute; qui a &eacute;t&eacute; sign&eacute; */
	public boolean peutModifierArrete() {
		return modificationEnCours && (arreteCourant() == null || (arreteCourant() != null && !arreteCourant().estSigne() && arreteCourant().estModifiable()));
	}
	/** retourne l'arrete; */
	public Arrete arreteCourant() {
		return (Arrete)displayGroup.selectedObject();
	}

}

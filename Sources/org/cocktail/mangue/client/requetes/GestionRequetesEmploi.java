/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiCategorieFinder;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiFinder;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.EmploiPourEdition;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

// 03/01/2011 - Adaptation Netbeans
// 03/02/2011 - On restreint les emplois aux types de population visibles par l'utilisateur
public class GestionRequetesEmploi extends PageEditionRequete {
	private EOStructure currentStructure;
	private int typeEmploi,typePersonnel;
	private static int TOUS = 0;
	private static int BIATSS = 1, ENSEIGNANT = 2;
	private static String[] titres = {"de tous les emplois","des emplois budgétaires","des emplois sur ressources propres","des rompus de temps partiel","des emplois vacants"};
	private boolean inclureFils = true, peutModifier = false;
	private NSArray populationsGerees; // 03/02/2011 - pour filtrer les emplois

	// constructeur
	public GestionRequetesEmploi() {	
		setInclureFils(true);
	}
	public boolean peutModifier() {
		return peutModifier;
	}
	public void setPeutModifier(boolean modifier) {
		peutModifier = modifier;
	}


	// accesseurs
	public boolean inclureFils() {
		return inclureFils;
	}
	public void setInclureFils(boolean inclureFils) {
		this.inclureFils = inclureFils;
	}
	public String libelleStructure() {
		if (currentStructure != null) {
			return currentStructure.llStructure();
		} else {
			return null;
		}
	}
	public float quotiteTotale() {
		float total = 0;
		for (java.util.Enumeration<EmploiPourEdition> e = displayGroup().displayedObjects().objectEnumerator();e.hasMoreElements();) {
			EmploiPourEdition info = e.nextElement();
			Number quotite = info.quotiteFinanciere();
			if (quotite != null) {
				total += quotite.floatValue();
			}
		}
		return total;
	}
	public float quotiteService() {
		float total = 0;
		for (java.util.Enumeration<EmploiPourEdition> e = displayGroup().displayedObjects().objectEnumerator();e.hasMoreElements();) {
			EmploiPourEdition info = e.nextElement();
			Number quotite = info.quotite();
			if (quotite != null) {
				total += quotite.floatValue();
			}
		}
		return total;
	}
	public int typeEmploi() {
		return typeEmploi;
	}
	public void setTypeEmploi(int type) {
		this.typeEmploi = type;
		if (typeEmploi == EmploiPourEdition.TOUT_TYPE || typeEmploi == EmploiPourEdition.VACANT) {
			// pas de structure sélectionnée, car la recherche est globale sur tous les emplois (y compris les vacants) 
			supprimerStructure();
			setInclureFils(false);
		}
		if (typeEmploi == EmploiPourEdition.VACANT) {
			setTypePersonnel(TOUS);
		}
		controllerDisplayGroup().redisplay();
	}
	public int typePersonnel() {
		return typePersonnel;
	}
	public void setTypePersonnel(int typePersonnel) {
		this.typePersonnel = typePersonnel;
	}
	// actions
	public void afficherStructure() {
		currentStructure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(editingContext());
		controllerDisplayGroup().redisplay();
	}
	public void supprimerStructure() {
		currentStructure = null;
		controllerDisplayGroup().redisplay();
	}
	// méthodes du controller DG
	public boolean peutRechercher() {
		return super.peutRechercher() && 
		((typeEmploi != EmploiPourEdition.TOUT_TYPE && typeEmploi != EmploiPourEdition.VACANT && currentStructure != null) || 
				typeEmploi == EmploiPourEdition.VACANT || typeEmploi == EmploiPourEdition.TOUT_TYPE);
	}
	/** on ne peut selectionner une structure que si le type d'emploi n'est pas vacant ou tous */
	public boolean peutSelectionnerStructure() {
		return /*typeEmploi != EmploiPourEdition.TOUT_TYPE && */typeEmploi != EmploiPourEdition.VACANT;
	}
	public boolean peutSupprimerStructure() {
		return currentStructure != null;
	}
	
	public boolean libelleStructureEnabled() {
		return false;
	}
	
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		populationsGerees = ((ApplicationClient)EOApplication.sharedApplication()).populationsGerees(editingContext());
		typeEmploi = TOUS;
		typePersonnel = TOUS;
	}
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("typeEmploi",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("numeroEmploi",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("nom",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("prenom",EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
	}
	protected  NSMutableArray<EmploiPourEdition> preparerInformations() {
				
		if (typeEmploi == EmploiPourEdition.TOUT_TYPE && currentStructure == null) {
			if (EODialogs.runConfirmOperationDialog("Attention","Vous recherchez tous les emplois, le traitement va être long.\nVoulez-vous continuer ?","OK","Annuler")) {
				return preparerTousEmplois();
			} else {
				return new NSMutableArray();
			}
		} else {
			return preparerEmploisPourType();
		}


	}
	protected String textePourExport() {
		String texte = "Support\tEmploi\tImp\tCategorie\tSpec\tChapitre\tGroupe\tCiv\tNom\tPrenom\tNom JF\tStatut\tPosition\tCorps\tGrade\tEchelon\tDebut\tFin\tQuot Occ\tQuot Srv\tNiv 1\tNiv2\n";

		int typeCourant = -1;
		float sousTotalQuotiteFinanciere = 0,sousTotalQuotiteService = 0;
		NumberFormat formatter = NumberFormat.getInstance(Locale.FRENCH);
		if (formatter instanceof DecimalFormat) {
			((DecimalFormat) formatter).applyPattern("0.00");
		}

		// les éléments sont déjà classés par type d'emploi
		for (java.util.Enumeration<EmploiPourEdition> e = displayGroup().displayedObjects().objectEnumerator();e.hasMoreElements();) {
			EmploiPourEdition info = e.nextElement();
			Number type = info.typeEmploi();
			if (type != null && type.intValue() != typeCourant) {
				if (typeCourant > 0) {
					// ie on change de type d'emploi
					if (sousTotalQuotiteFinanciere != 0 || sousTotalQuotiteService != 0) {
						texte = texte + "\nSOUS TOTAL " + typeCourant + " : Quot Occupation : " + formatter.format(new Float(sousTotalQuotiteFinanciere)) + 
						"		Quot Service : " + formatter.format(new Float(sousTotalQuotiteService)) + "\n\n\n";
					}
					sousTotalQuotiteFinanciere = 0;sousTotalQuotiteService = 0;
				}
				typeCourant = type.intValue();
			}
			texte =  texte + info.type() + "\t" + info.numeroEmploi() + "\t" + info.implantation() + "\t"  + info.categorieEmploi() + "\t"
					+ info.codeSpecEmploi() + "\t" + info.chapitre() + "\t" + info.typePopulation() + "\t"+ info.cCivilite() + "\t" 
					+ info.nom() + "\t" + StringCtrl.recupererChaine(info.prenom()) + "\t" 
					+ StringCtrl.recupererChaine(info.nomPatronymique()) + "\t" + info.statut() + "\t" 
					+ info.lcTypeContratTrav() + "\t" + info.lcCorps() + "\t" + info.lcGrade() + "\t" + info.cEchelon() + "\t" 
					+ DateCtrl.dateToString(info.debut(),"%Y/%m/%d") + "\t" + DateCtrl.dateToString(info.fin(),"%Y/%m/%d");
			if (info.quotiteFinanciere() != null) {	// pour les vacants, elle vaut null
				texte = texte + "\t" + formatter.format(info.quotiteFinanciere());
			} else {
				texte = texte + "\t";
			}
			if (info.quotite() != null) {
				texte = texte + "\t" + formatter.format(info.quotite());
			} else {
				texte = texte + "\t";
			}
			texte = texte + "\t" + info.llComposante() + "\t" + info.llStructure() + "\n";
			if (info.quotiteFinanciere() != null) {
				sousTotalQuotiteFinanciere += info.quotiteFinanciere().floatValue();
			}
			if (info.quotite() != null) {
				sousTotalQuotiteService += info.quotite().floatValue();
			}
		}
		// écrire le dernier sous-total
		if (sousTotalQuotiteFinanciere != 0 || sousTotalQuotiteService != 0) {
			texte = texte + "\nSOUS TOTAL " + typeCourant + " : Quot Occupation : " + formatter.format(new Float(sousTotalQuotiteFinanciere)) + 
			"		Quot Service : " + formatter.format(new Float(sousTotalQuotiteService));
		}
		return texte;
	}
	protected String methodeImpression() {
		return "clientSideRequestImprimerResultatRequeteEmploi";
	}
	protected  String nomFichierImpression() {
		return "ImpressionRequeteEmploi";
	}
	protected String nomFichierJasper() {
		return "EditionRequeteEmploi";
	}
	/** methode a definir dans les sous-classes. Retourne le titre par defaut de l'impression */
	protected String titreImpressionParDefaut() {
		return "Edition " + titres[typeEmploi()];
	}
	protected void terminer() {
	}
	// méthodes privées
	private NSMutableArray preparerTousEmplois() {
		
		CRICursor.setWaitCursor(listeAffichage);

		// Récupération des emplois (Enseignants, BIatoss ou tous)
		String type = null;
		if (typePersonnel() == BIATSS) {
			type = "N";
		} else if (typePersonnel() == ENSEIGNANT) {
			type = "O";
		}
		LogManager.logDetail("GestionRequetesEmploi preparerTousEmplois - typePersonnel " + typePersonnel());
		//NSArray<IEmploi> emplois = EmploiFinderOLD.fetchEmplois(editingContext(),type,dateDebut(),dateFin(),populationsGerees);
		//FIXME : Ajouter filtre?
		NSArray<IEmploi> emplois = EmploiFinder.sharedInstance().rechercherTousLesEmploisAvecFiltre(editingContext(), null);
		
		NSMutableArray infos = new NSMutableArray();
		NSMutableDictionary emploisPourIndividu = new NSMutableDictionary();
		for (IEmploi unEmploi : emplois) {
			NSArray occupations = EOOccupation.rechercherOccupationsPourEmploi(editingContext(), unEmploi, dateDebut(), false);
			switch (occupations.count()) {
			case 0 :
				infos.addObject(new EmploiPourEdition(unEmploi, EmploiPourEdition.VACANT));
				break;
			case 1 : // emploi budgétaire ou rompu si quotité < 100
				EOOccupation occupation = (EOOccupation)occupations.objectAtIndex(0);
				int typeEmploi = EmploiPourEdition.EMPLOI_BUDGETAIRE;
				if (typePersonnel() == BIATSS && occupation.quotite().floatValue() < 100.00) {
					typeEmploi = EmploiPourEdition.ROMPUS;
				}
				if (infoAAjouter(emploisPourIndividu,occupation.individu(),occupation.toEmploi())) {
					ajouterInfos(infos,EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext(),occupation.individu(),dateDebut(),dateFin()),occupation,typeEmploi);
				}
				break;
			default : // Rompus (plusieurs emplois)
				for (java.util.Enumeration<EOOccupation> e1 = occupations.objectEnumerator();e1.hasMoreElements();) {
					occupation = e1.nextElement();
					typeEmploi = EmploiPourEdition.ROMPUS;
					if (occupation.quotite().floatValue() == 100.00) {
						typeEmploi = EmploiPourEdition.EMPLOI_BUDGETAIRE;
					}
					if (infoAAjouter(emploisPourIndividu,occupation.individu(),occupation.toEmploi())) {
						ajouterInfos(infos,EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext(),occupation.individu(),dateDebut(),dateFin()),occupation,typeEmploi);
					}
				}
			}
		}

		CRICursor.setDefaultCursor(listeAffichage);

		return infos;
	}
	private NSMutableArray<EmploiPourEdition> preparerEmploisPourType() {

		LogManager.logDetail("GestionRequetesEmploi - preparerEmploisPourType ");
		CRICursor.setWaitCursor(listeAffichage);

		NSMutableArray<EmploiPourEdition> infos = new NSMutableArray();
		if (currentStructure == null) {
			if (typeEmploi() == EmploiPourEdition.VACANT) {
				// 04/02/2011 - filtrage sur les types de population
				//FIXME : ajouter type de population?
				//NSArray emplois = EOEmploiOLD.rechercherEmploisVacantsPourPeriode(editingContext(), dateDebut(), dateFin(),populationsGerees);
				NSArray<IEmploi> emplois = EmploiFinder.sharedInstance().rechercherListeEmploisVacants(editingContext(), dateDebut(), dateFin());
				for (IEmploi emploi : emplois) {
					infos.addObject(new EmploiPourEdition(emploi,EmploiPourEdition.VACANT));
				}
			}
		} else {
			NSMutableArray myQualifiers = new NSMutableArray();
			NSMutableArray structures = new NSMutableArray(currentStructure.cStructure());
			if (inclureFils()) {
				structures.addObjectsFromArray((NSArray)currentStructure.toutesStructuresFils().valueForKey(EOStructure.C_STRUCTURE_KEY));
			}
			EOQualifier qualifier = SuperFinder.construireORQualifier(EOAffectation.TO_STRUCTURE_ULR_KEY + ".cStructure",structures);
			myQualifiers.addObject(qualifier);

			// Rechercher toutes les affectations pour la période
			myQualifiers.addObject(SuperFinder.qualifierPourPeriode(EOAffectation.DATE_DEBUT_KEY, dateDebut(), EOAffectation.DATE_FIN_KEY, dateFin()));
			EOQualifier qualifierRecherche = new EOAndQualifier(myQualifiers);
			LogManager.logDetail("GestionRequetesEmploi - preparerEmploisPourType qualifier : " + qualifierRecherche);
			NSArray<EOAffectation> affectations = EOAffectation.rechercherAffectationsAvecCriteres(editingContext(),qualifierRecherche);

			LogManager.logDetail("GestionRequetesEmploi - preparerEmploisPourType AFFECTIONS COUNT : " + affectations.count());

			NSMutableDictionary emploisPourIndividu = new NSMutableDictionary();
			for (EOAffectation myAffectation : affectations) {

				// vérifie le type de personnel. Si ce n'est pas tous, vérifie dans les carrières ou les contrats
				// si le type de personnel correspond au type demandé
				boolean garderAffectation = (typePersonnel() == TOUS);
				if (typePersonnel() != TOUS) {
					if (!garderAffectation) {
						// Rechercher dans les carrières si elles correspondent au type de personnel
						NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),myAffectation.individu(),dateDebut(),dateFin());
						for (java.util.Enumeration<EOCarriere> e1 = carrieres.objectEnumerator();e1.hasMoreElements();) {
							EOCarriere carriere = e1.nextElement();
							garderAffectation = (typePersonnel() == BIATSS && carriere.toTypePopulation().estEnseignant() == false) ||
							(typePersonnel() == ENSEIGNANT && carriere.toTypePopulation().estEnseignant());
							// Dès qu'on en a trouvé un, on s'arrête
							if (garderAffectation) {
								break;
							}
						}
					}
					if (!garderAffectation) {
						// rechercher parmi les contrats
						NSArray contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),myAffectation.individu(),dateDebut(),dateFin(),true);
						for (java.util.Enumeration<EOContrat> e1 = contrats.objectEnumerator();e1.hasMoreElements();) {
							EOContrat contrat = e1.nextElement();
							garderAffectation = (typePersonnel() == BIATSS && contrat.toTypeContratTravail().estEnseignant() == false) ||
							(typePersonnel() == ENSEIGNANT && contrat.toTypeContratTravail().estEnseignant());
							// Dès qu'on en a trouvé un, on s'arrête
							if (garderAffectation) {
								break;
							}
						}
					}
				}

				LogManager.logDetail("GestionRequetesEmploi - preparerEmploisPourType GARDER AFF : " + garderAffectation);

				if (garderAffectation) {
					// rechercher les occupations pour cette période pour savoir si on retient l'occupation
					NSArray occupations = EOOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContext(),myAffectation.individu(),dateDebut(),dateFin());

					LogManager.logDetail("GestionRequetesEmploi - preparerEmploisPourType OCCUPATIONS COUNT : " + occupations.count());

					switch (occupations.count()) {
					case 0 : // Emploi sur Ressource Propre
						if (typeEmploi() == EmploiPourEdition.RESSOURCE_PROPRE || typeEmploi() == EmploiPourEdition.TOUT_TYPE) {
							ajouterInfos(infos,new NSArray(myAffectation),null,typeEmploi());
						}
						break;
					case 1 : // emploi budgétaire si quotite = 100 ou rompus si quotité < 100
						EOOccupation occupation = (EOOccupation)occupations.objectAtIndex(0);

						if (typeEmploi() == EmploiPourEdition.EMPLOI_BUDGETAIRE 
								|| typeEmploi() == EmploiPourEdition.TOUT_TYPE
								|| typeEmploi() == EmploiPourEdition.ROMPUS) {
							boolean ajouter = (typeEmploi() == EmploiPourEdition.EMPLOI_BUDGETAIRE && occupation.quotite().floatValue() == 100.00) ||
							(typeEmploi() == EmploiPourEdition.ROMPUS && occupation.quotite().floatValue() < 100.00)
							|| typeEmploi() == EmploiPourEdition.TOUT_TYPE;

							if (ajouter && infoAAjouter(emploisPourIndividu,myAffectation.individu(),occupation.toEmploi()) &&
									estEmploiValidePourPopulation(occupation.toEmploi())) {	// 03/02/2011 - rajout d'un filtage su  les populations
								ajouterInfos(infos,new NSArray(myAffectation),occupation,typeEmploi());
							}
						}
						break;
					default : // Rompus (plusieurs emplois)
						if (typeEmploi() == EmploiPourEdition.ROMPUS || typeEmploi() == EmploiPourEdition.TOUT_TYPE) {
							for (java.util.Enumeration<EOOccupation> e1 = occupations.objectEnumerator();e1.hasMoreElements();) {
								occupation = e1.nextElement();
								if (occupation.quotite().floatValue() < 100.00 && infoAAjouter(emploisPourIndividu,myAffectation.individu(),occupation.toEmploi()) &&
										estEmploiValidePourPopulation(occupation.toEmploi())) {	// 03/02/2011 - rajout d'un filtage su  les populations
									ajouterInfos(infos,new NSArray(myAffectation),occupation,typeEmploi());
								}
							}
						}
					}
				}
			}
		}
		CRICursor.setDefaultCursor(listeAffichage);
		return infos;
	}

	//	vérifie si une info pour cet emploi et cet individu
	// sinon on stocke dans le dictionnaire pour chaque individu (clé) un tableau des emplois occupés
	private boolean infoAAjouter(NSMutableDictionary emploisPourIndividu,EOIndividu individu, IEmploi emploi) {
		// vérifier si l'emploi est fermé (cas de certaines occupations mal définies)
		if (emploi.getDateFermetureEmploi() != null && DateCtrl.isBefore(emploi.getDateFermetureEmploi(),dateDebut())) {
			return false;
		}		
		NSMutableArray emplois = (NSMutableArray)emploisPourIndividu.objectForKey(individu);
		if (emplois != null) {
			if (emplois.containsObject(emploi)) {
				return false;
			} else {
				emplois.addObject(emploi);
				return true;
			}
		} else {
			emplois = new NSMutableArray(emploi);
			emploisPourIndividu.setObjectForKey(emplois,individu);
			return true;
		}
	}
	// 03/02/2011 - vérifie si un emploi rentre dans le filtre
	private boolean estEmploiValidePourPopulation(IEmploi emploi) {
		if (populationsGerees == null || populationsGerees.count() == 0) {
			return true;
		} else {
			EOTypePopulation typePopulation = EmploiCategorieFinder.sharedInstance().rechercherTypePopulationPourEmploi(editingContext(), emploi, dateDebut());
			if (typePopulation != null) {
				return populationsGerees.containsObject(typePopulation);
			} else {
				return false;
			}
		}
	}
	private void ajouterInfos(NSMutableArray<EmploiPourEdition> infos,NSArray<EOAffectation> affectations,EOOccupation occupation,int type) {
		for (EOAffectation myAffectation : affectations) {
			infos.addObject(new EmploiPourEdition(myAffectation,occupation,dateDebut(),dateFin(),type));
		}
	}

}

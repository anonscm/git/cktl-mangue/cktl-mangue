/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import javax.swing.ListSelectionModel;

import org.cocktail.mangue.client.outils_interface.ChoixRne;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

public class InterfaceRequeteContrat extends InterfaceRequeteAvecPeriode {

	private EORne currentRne;
	private Number quotite;
	private String fonctionAvenant,referenceContrat,composanteBudgetaire;
	private boolean estContratRecherche;

	// accesseurs
	public String composanteBudgetaire() {
		return composanteBudgetaire;
	}
	public void setComposanteBudgetaire(String composanteBudgetaire) {
		this.composanteBudgetaire = composanteBudgetaire;
	}
	public EORne currentRne() {
		return currentRne;
	}
	public void setCurrentRne(EORne uneRne) {
		currentRne = uneRne;
	}
	public String fonctionAvenant() {
		return fonctionAvenant;
	}
	public void setFonctionAvenant(String fonctionAvenant) {
		this.fonctionAvenant = fonctionAvenant;
	}
	public Number quotite() {
		return quotite;
	}
	public void setQuotite(Number quotite) {
		this.quotite = quotite;
	}
	public String referenceContrat() {
		return referenceContrat;
	}
	public void setReferenceContrat(String referenceContrat) {
		this.referenceContrat = referenceContrat;
	}
	public boolean estContratRecherche() {
		return estContratRecherche;
	}
	public void setEstContratRecherche(boolean estContratRecherche) {
		this.estContratRecherche = estContratRecherche;
	}
	public void init(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		super.init(debutPeriode,finPeriode);
		EOArchive.loadArchiveNamed("InterfaceRequeteContrat",this,"org.cocktail.mangue.client.requetes.interfaces",this.disposableRegistry());

	}

	/**
	 * 
	 */
	public EOQualifier qualifierRecherche() {
				
		String nomRelation = EOContratAvenant.CONTRAT_KEY + ".";

		NSMutableArray args = new NSMutableArray();

		String stringQualifier = nomRelation + EOContrat.TEM_ANNULATION_KEY + " = 'N'";

		stringQualifier  = stringQualifier + " AND " + nomRelation + EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY+ "."+EOTypeContratTravail.TEM_VACATAIRE_KEY + " = 'N'";

		if (referenceContrat() != null && referenceContrat().length() > 0) {
			args.addObject(referenceContrat() + "*");
			stringQualifier = stringQualifier +   " AND " + EOContratAvenant.REFERENCE_CONTRAT_KEY + " caseinsensitivelike  %@ ";
		}
		if (fonctionAvenant() != null && fonctionAvenant().length() > 0) {
			args.addObject(fonctionAvenant() + "*");
			stringQualifier = stringQualifier +  " AND " + EOContratAvenant.FONCTION_CTR_AVENANT_KEY + " caseinsensitivelike  %@ ";
		}
		if (quotite() != null) {
			args.addObject(quotite);
			stringQualifier = stringQualifier +  " AND " + EOContratAvenant.QUOTITE_KEY + " =  %@ ";
		}

		if (currentRne() != null) {
			args.addObject(currentRne().code());
			stringQualifier = stringQualifier +  " AND " + nomRelation + EOContrat.TO_RNE_KEY + "." + EORne.CODE_KEY + " =  %@ ";
		}
		if (estContratRecherche()) {
			stringQualifier = stringQualifier + nomRelation + EOContrat.TO_ORIGINE_FINANCEMENT_KEY + "." + INomenclature.CODE_KEY + " = 'CR'";
		}
		if (displayGroup().selectedObjects().count() > 0) {
			String typeQualifier = "";
			for (java.util.Enumeration<EOTypeContratTravail> e = displayGroup().selectedObjects().objectEnumerator();e.hasMoreElements();) {
				EOTypeContratTravail typeContrat = e.nextElement();
				typeQualifier = typeQualifier + nomRelation + EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY + "." + EOTypeContratTravail.CODE_KEY + " = '" + typeContrat.code() + "' OR ";
			}
			typeQualifier = SuperFinder.nettoyerQualifier(typeQualifier," OR ");
			stringQualifier = stringQualifier +  " AND " + "(" + typeQualifier + ")";
		}
		
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		NSMutableArray myQualifiers = new NSMutableArray();
		if (qualifier != null) {
			myQualifiers.addObject(qualifier);
		}
		// 25/09/09 - DT 1843 gestion des dates anticipées
		if (finPeriode() == null) {
			myQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(nomRelation + EOContrat.DATE_FIN_ANTICIPEE_KEY  +" = NIL",null));
		} else {
			myQualifiers.addObject(
					EOQualifier.qualifierWithQualifierFormat(nomRelation + EOContrat.DATE_FIN_ANTICIPEE_KEY + " = NIL OR " + nomRelation + EOContrat.DATE_FIN_ANTICIPEE_KEY + " >= %@",new NSArray(debutPeriode())));
		}

		qualifier = qualifierDates(EOContratAvenant.DATE_DEBUT_KEY,EOContratAvenant.DATE_FIN_KEY);

		myQualifiers.addObject(qualifier);

		return new EOAndQualifier(myQualifiers);
	}


	/**
	 * 
	 * @param typeHeberge
	 * @return
	 */
	public EOQualifier qualifierRecherchePourHeberge(int typeHeberge) {
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContratHeberges.TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		if (currentRne() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContratHeberges.TO_UAI_KEY+"."+EORne.CODE_KEY + "=%@", new NSArray(currentRne().code())));
		}
		if (displayGroup().selectedObjects().size() > 0) {	
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			
			String typeQualifier = "";
			for (EOTypeContratTravail typeContrat : (NSArray<EOTypeContratTravail>)displayGroup().selectedObjects()) {
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOContratHeberges.TYPE_CONTRAT_TRAVAIL_KEY +  "." + EOTypeContratTravail.CODE_KEY + " =%@", new NSArray(typeContrat.code())));
			}
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		qualifiers.addObject(qualifierDates( "dateDebut", "dateFin"));
		
		return new EOAndQualifier(qualifiers);
	}
	
	// Actions
	public void afficherRne() {
		String nomNotification = "RnePourRequeteContratNotification";
		ChoixRne controleur = new ChoixRne(nomNotification);
		controleur.chargerArchive();
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getRne",new Class[] {NSNotification.class}),nomNotification,null);
		controleur.afficherFenetre();	}
	public void supprimerRne() {
		currentRne = null;
		updaterDisplayGroups();
	}

	// Notifications
	// GET Rne
	public void getRne(NSNotification aNotif) {
		stockerValeur(aNotif,"currentRne");
	}

	public boolean peutSupprimerRne() {
		return currentRne() != null;
	}

	protected void preparerFenetre() {

		super.preparerFenetre();
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

	}
	protected void terminer() {
	}
	protected void effacerChamps() {
		quotite = null;
		currentRne = null;
		fonctionAvenant = null;
		referenceContrat = null;
		composanteBudgetaire = null;
		estContratRecherche = false;
	}

	/**
	 * 
	 */
	protected NSArray<EOTypeContratTravail> fetcherObjets() {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		if (!EOGrhumParametres.isGestionHu()) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeContratTravail.TEM_HOSPITALIER_KEY  + " = %@", new NSArray(CocktailConstantes.FAUX)));
		}
		return EOTypeContratTravail.fetchAll(editingContext(), new EOAndQualifier(qualifiers), null);
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(Nomenclature.SORT_ARRAY_LIBELLE);
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}

}

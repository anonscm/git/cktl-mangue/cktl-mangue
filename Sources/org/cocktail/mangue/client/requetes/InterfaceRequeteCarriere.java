/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** gere les donnees de requete sur les carrieres  et sur les modelites de services ou autres types d'information.
 * On ne peut a la fois selectionner des positions et d'autres types d'infos. Le qualifier retourne est un qualifier sur les positions
 * 
 */
// 04/01/2011 - Adaptation Netbeans
public class InterfaceRequeteCarriere extends InterfaceRequeteAvecPeriode {
	public EODisplayGroup displayGroupAutresInfos;
	public EOTable listeAutresInfos;
	
    public void init(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	super.init(debutPeriode,finPeriode);
		EOArchive.loadArchiveNamed("InterfaceRequeteCarriere",this,"org.cocktail.mangue.client.requetes.interfaces",this.disposableRegistry());
    }
    // accesseurs
    public boolean autreInfosSelectionnees() {
    		return peutDeselectionnerAutresInfos();
    }
    public boolean infoSansAffectionSelectionnee() {
    	return peutDeselectionnerAutresInfos() || !doitSelectionnerAffectation();
    }
    // méthode de délégation du DG
    public void displayGroupDidChangeSelection(EODisplayGroup group) {
    		if (group == displayGroup()) {
    			listeAutresInfos.table().setEnabled(group.selectedObject() == null);
    		} else {
    			listeAffichage.table().setEnabled(group.selectedObject() == null);
    		}
		super.displayGroupDidChangeSelection(group);
    }
    /** retourne le qualifier sur les positions */
	public EOQualifier qualifierRecherche() {
		if (displayGroup().selectedObjects().count() > 0) {
			EOQualifier qualifier = SuperFinder.construireORQualifier(INomenclature.CODE_KEY,(NSArray)displayGroup().selectedObjects().valueForKey(INomenclature.CODE_KEY));
			return qualifier;
		} else {
			return null;
		}
	}
	 /** retourne le tableau des autres infos selectionnees */
	public NSArray autresInfosModalites() {
		return displayGroupAutresInfos.selectedObjects();	
	}
	// actions
	public void deselectionnerAutresInfos() {
		if (displayGroupAutresInfos != null) {
			displayGroupAutresInfos.setSelectedObject(null);
		}
		updaterDisplayGroups();
	}
	// méthodes du controller DG
	public boolean peutDeselectionnerAutresInfos() {
		return displayGroupAutresInfos.selectedObjects().count() > 0;
	}
	/** retourne le tableau des modalites ou autres selectionnees */
	public String messageAffectation() {
		if (doitSelectionnerAffectation()) {
			return "Vous devez sélectionner une structure d'affectation";
		} else {
			return null;
		}
	}
	/** retourne true si au moins une position active est selectionnee */
	public boolean doitSelectionnerAffectation() {
		if (displayGroupAutresInfos.selectedObjects().count() > 0) {
			return false;
		}
		if (displayGroup().selectedObjects().count() > 0) {
			for (EOPosition position : (NSArray<EOPosition>)displayGroup().selectedObjects()) {
				if (position.estEnActivite()) {
					return true;
				}
			}
			return false;	// on n'a pas trouvé de position "en activité"
		} else {
			return true;
		}
	}
	public boolean doitRecupererElementsCarriere() {
		return doitSelectionnerAffectation();
	}
	public boolean doitRecupererCarriere() {
		if (displayGroup().selectedObjects().count() > 0) {
			for (EOPosition position : (NSArray<EOPosition>)displayGroup().selectedObjects()) {
				if (!position.estEnActivite()) {
					return true;
				}
			}
			return false;	// on n'a pas trouvé de position non "en activité"
		} else {
			return true;
		}
	}
	
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		displayGroupAutresInfos.setSelectsFirstObjectAfterFetch(false);
		GraphicUtilities.changerTaillePolice(listeAutresInfos,11);
		GraphicUtilities.rendreNonEditable(listeAutresInfos);
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		listeAutresInfos.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}
	protected void terminer() {
	}
	protected void effacerChamps() {
		displayGroupAutresInfos.setSelectedObject(null);
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),EOPosition.ENTITY_NAME);
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(Nomenclature.SORT_ARRAY_LIBELLE);
		displayGroupAutresInfos.setSelectsFirstObjectAfterFetch(false);
		displayGroupAutresInfos.setObjectArray(InformationPourModalite.informations());
		displayGroupAutresInfos.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey(INomenclature.LIBELLE_KEY,EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}


}

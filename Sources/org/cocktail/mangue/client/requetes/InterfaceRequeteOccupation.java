/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import org.cocktail.mangue.client.select.EmploiSelectCtrl;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSTimestamp;

// 04/01/2011 - Adaptation Netbeans
public class InterfaceRequeteOccupation extends InterfaceRequeteAvecPeriode {
	private EOStructure currentStructure;
	private String operateurComparaisonQuotiteAffectation,operateurComparaisonQuotiteOccupation;
	private String dateDebut,dateFin;
	private Number quotiteAffectation,quotiteOccupation;
	private IEmploi currentEmploi;
	private boolean inclureFils, peutModifier;
	private boolean estTitulaireEmploi,prendreEnCompteTitularite;

	// accesseurs
	public EOStructure currentStructure() {
		return currentStructure;
	}
	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
	}
	
	public IEmploi currentEmploi() {
		return currentEmploi;
	}
	
	public boolean peutModifier() {
		return peutModifier;
	}
	public void setPeutModifier(boolean modifier) {
		peutModifier = modifier;
	}

	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
	}
	public boolean inclureFils() {
		return inclureFils;
	}
	public void setInclureFils(boolean inclureFils) {
		this.inclureFils = inclureFils;
	}
	public boolean prendreEnCompteTitularite() {
		return prendreEnCompteTitularite;
	}
	public void setPrendreEnCompteTitularite(boolean prendreEnCompteTitularite) {
		this.prendreEnCompteTitularite = prendreEnCompteTitularite;
	}
	public boolean estTitulaireEmploi() {
		return estTitulaireEmploi;
	}
	public void setEstTitulaireEmploi(boolean estTitulaireEmploi) {
		this.estTitulaireEmploi = estTitulaireEmploi;
	}
	public String dateDebut() {
		return dateDebut;
	}
	public void setDateDebut(String uneDate) {
		if (uneDate != null && uneDate.length() > 0) {
			String date = DateCtrl.dateCompletion(uneDate);
			if (date != null && date.length() > 0) {
				this.dateDebut = date;
			} else {
				this.dateDebut = null;
			}
			controllerDisplayGroup().redisplay();
		} else {
			this.dateDebut = uneDate;
		}
	}
	public String dateFin() {
		return dateFin;
	}
	public void setDateFin(String uneDate) {
		if (uneDate != null && uneDate.length() > 0) {
			String date = DateCtrl.dateCompletion(uneDate);
			if (date != null && date.length() > 0) {
				this.dateFin = date;
			} else {
				this.dateFin = null;
			}
			controllerDisplayGroup().redisplay();
		} else {
			this.dateFin = uneDate;
		}
	}
	public String operateurComparaisonQuotiteAffectation() {
		return operateurComparaisonQuotiteAffectation;
	}
	public void setOperateurComparaisonQuotiteAffectation(String operateurComparaisonQuotiteAffectation) {
		this.operateurComparaisonQuotiteAffectation = operateurComparaisonQuotiteAffectation;
	}
	public String operateurComparaisonQuotiteOccupation() {
		return operateurComparaisonQuotiteOccupation;
	}
	public void setOperateurComparaisonQuotiteOccupation(String operateurComparaisonQuotiteOccupation) {
		this.operateurComparaisonQuotiteOccupation = operateurComparaisonQuotiteOccupation;
	}
	public Number quotiteAffectation() {
		return quotiteAffectation;
	}
	public void setQuotiteAffectation(Number quotiteAffectation) {
		this.quotiteAffectation = quotiteAffectation;
	}
	public Number quotiteOccupation() {
		return quotiteOccupation;
	}
	public void setQuotiteOccupation(Number quotiteOccupation) {
		this.quotiteOccupation = quotiteOccupation;
	}
	public boolean structureSelectionnee() {
		return currentStructure() != null;
	}
	public void init(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
				
		EOArchive.loadArchiveNamed("InterfaceRequeteOccupation",this,"org.cocktail.mangue.client.requetes.interfaces",this.disposableRegistry());

		super.init(debutPeriode,finPeriode);
		setInclureFils(true);
		setPeutModifier(false);

		setCurrentStructure(EOStructure.rechercherEtablissement(editingContext()));

		component().repaint();
		component().validate();
		
	}
	
	public EOQualifier qualifierRecherche() {
		NSMutableArray myQualifiers = new NSMutableArray();
		if (currentStructure() != null) {
			NSMutableArray structures = new NSMutableArray();
			if (inclureFils()) {
				structures.addObjectsFromArray((NSArray)currentStructure().toutesStructuresFils().valueForKey(EOStructure.C_STRUCTURE_KEY));
			}
			structures.addObject(currentStructure().cStructure());
			EOQualifier qualifier = SuperFinder.construireORQualifier(EOAffectation.TO_STRUCTURE_ULR_KEY + "." + EOStructure.C_STRUCTURE_KEY,structures);
			myQualifiers.addObject(qualifier);
		}	

		if (quotiteAffectation() != null) {
			NSArray args = new NSArray(quotiteAffectation());
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOAffectation.QUOTITE_KEY + operateurComparaisonQuotiteAffectation() + "  %@",args);
			myQualifiers.addObject(qualifier);
		}
		if (myQualifiers.count() > 0) {
			EOQualifier qualifier =  qualifierDates(EOAffectation.DATE_DEBUT_KEY,EOAffectation.DATE_FIN_KEY);
			if (qualifier != null) {
				myQualifiers.addObject(qualifier);
			}
			return new EOAndQualifier(myQualifiers);
		} else {	// pas de qualifier sur les affectations
			return null;
		}
	}
	public EOQualifier qualifierOccupation() {
		if (quotiteOccupation() == null && currentEmploi() == null && !prendreEnCompteTitularite()) {
			return null;	// pas de qualifier sur les occupations
		}

		NSMutableArray args = new NSMutableArray();
		String stringQualifier = "";
		if (quotiteOccupation() != null) {
			args.addObject(quotiteOccupation());
			stringQualifier = stringQualifier + EOOccupation.QUOTITE_KEY + " " + operateurComparaisonQuotiteOccupation() + "  %@ AND ";
		}
		if (currentEmploi() != null) {
			args.addObject(currentEmploi().getNoEmploi());
			stringQualifier = stringQualifier +  EOOccupation.TO_EMPLOI_KEY + "." + EOEmploi.NO_EMPLOI_KEY + " =  %@ AND ";
		}
		if (prendreEnCompteTitularite()) {
			if (estTitulaireEmploi()) {
				stringQualifier = stringQualifier + EOOccupation.TEM_TITULAIRE_KEY + " =  'O' AND ";
			} else {
				stringQualifier = stringQualifier + EOOccupation.TEM_TITULAIRE_KEY + " =  'N' AND ";
			}
		}
		stringQualifier = SuperFinder.nettoyerQualifier(stringQualifier," AND ");
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(stringQualifier,args));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOccupation.TEM_VALIDE_KEY + " = 'O'",null));
		if (qualifiers.count() > 0) {
			EOQualifier qualifier =  qualifierDates(EOOccupation.DATE_DEBUT_KEY,EOOccupation.DATE_FIN_KEY);
			if (qualifier != null) {
				qualifiers.addObject(qualifier);
			}
		}
		return new EOAndQualifier(qualifiers);
	}
	// Actions
	public void afficherStructure() {
		currentStructure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(editingContext());
		updaterDisplayGroups();
	}
	/**
	 * Afficher la liste des sélections des emplois valides pendant la période
	 */
	public void afficherEmploi() {
		currentEmploi = EmploiSelectCtrl.sharedInstance(editingContext()).getEmploiTous(debutPeriode(), finPeriode());
		updaterDisplayGroups();
	}
	
	public void supprimerStructure() {
		currentStructure = null;
		updaterDisplayGroups();
	}
	public void supprimerEmploi() {
		currentEmploi = null;
		updaterDisplayGroups();
	}
	// Notifications
	// GET Emploi
	public void getEmploi(NSNotification aNotif) {
		stockerValeur(aNotif,"currentEmploi");
		updaterDisplayGroups();
	}
	// Controller DG
	public boolean peutSupprimerStructure() {
		return currentStructure() != null;
	}
	public boolean peutSupprimerEmploi() {
		return currentEmploi() != null;
	}
	public boolean peutAffecterTitularite() {
		return prendreEnCompteTitularite();
	}
	// Méthodes protégées
	protected void effacerChamps() {
		currentStructure = null;
		currentEmploi = null;
		dateDebut = null;
		dateFin = null;
		operateurComparaisonQuotiteAffectation = "=";
		operateurComparaisonQuotiteOccupation = "=";
		quotiteAffectation = null;
		quotiteOccupation = null;
		prendreEnCompteTitularite = false;
		estTitulaireEmploi = false;
	}

	protected NSArray fetcherObjets() {
		// pas de liste gérée
		return null;
	}
	protected void parametrerDisplayGroup() {}
	protected boolean conditionsOKPourFetch() {
		return true;
	}

	protected void terminer() {
	}

}

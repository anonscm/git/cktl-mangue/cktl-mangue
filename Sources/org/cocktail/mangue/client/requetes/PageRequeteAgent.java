/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import org.cocktail.client.composants.AideUtilisateur;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** page commune a l'editeur de requetes et l'affichage des resultats des requetes */
public abstract class PageRequeteAgent extends ModelePage {
	private NSMutableDictionary donneesATraiter;	// dictionnaire contenant des booléens pour indiquer les données à traiter lors de l'affichage des résultats de requête
	private NSTimestamp debutPeriode,finPeriode;

	public PageRequeteAgent() {
		super();
		donneesATraiter = new NSMutableDictionary();
		setRecupererAdresse(false);
		setRecupererEnfants(false);
		setRecupererReduction(false);
		setRecupererAnciennete(false);
		setRecupererAffectation(false);
		setRecupererCarriere(false);
		setRecupererEmploi(false);
		setRecupererEtatCivil(true);
		setRecupererSpecialisation(false);
	}
	public PageRequeteAgent(NSDictionary donneesATraiter) {
		super();
		this.donneesATraiter = new NSMutableDictionary(donneesATraiter);
	}
	// Accesseurs
	public String dateDebut() {
		if (debutPeriode == null) {
			return null;
		}
		return DateCtrl.dateToString(debutPeriode);
	}
	public void setDateDebut(String uneDate) {
		setDate(true,uneDate);
	}
	public NSTimestamp debutPeriode() {
		return debutPeriode;
	}
	public String dateFin() {
		if (finPeriode == null) {
			return null;
		}
		return DateCtrl.dateToString(finPeriode);
	}
	public void setDateFin(String uneDate) {
		setDate(false,uneDate);
	}
	public NSTimestamp finPeriode() {
		return finPeriode;
	}
	
	public NSDictionary donneesATraiter() {
		return donneesATraiter;
	}
	public boolean recupererAdresse() {
		return ((Boolean)donneesATraiter.objectForKey("recupererAdresse")).booleanValue();
	}
	public void setRecupererAdresse(boolean recupererAdresse) {
		donneesATraiter.setObjectForKey(new Boolean(recupererAdresse),"recupererAdresse");
	}
	public boolean recupererEnfants() {
		return ((Boolean)donneesATraiter.objectForKey("recupererEnfants")).booleanValue();
	}
	public void setRecupererEnfants(boolean recupererEnfants) {
		donneesATraiter.setObjectForKey(new Boolean(recupererEnfants),"recupererEnfants");
	}
	public boolean recupererAffectation() {
		return ((Boolean)donneesATraiter.objectForKey("recupererAffectation")).booleanValue();
	}
	public void setRecupererAffectation(boolean recupererAffectation) {
		donneesATraiter.setObjectForKey(new Boolean(recupererAffectation),"recupererAffectation");
	}
	public boolean recupererReduction() {
		return ((Boolean)donneesATraiter.objectForKey("recupererReduction")).booleanValue();
	}
	public void setRecupererReduction(boolean recupererAnciennete) {
		donneesATraiter.setObjectForKey(new Boolean(recupererAnciennete),"recupererReduction");
	}
	public boolean recupererAnciennete() {
		return ((Boolean)donneesATraiter.objectForKey("recupererAnciennete")).booleanValue();
	}
	public void setRecupererAnciennete(boolean recupererAnciennete) {
		donneesATraiter.setObjectForKey(new Boolean(recupererAnciennete),"recupererAnciennete");
	}
	public boolean recupererCarriere() {
		return ((Boolean)donneesATraiter.objectForKey("recupererCarriere")).booleanValue();
	}
	public void setRecupererCarriere(boolean recupererCarriere) {
		donneesATraiter.setObjectForKey(new Boolean(recupererCarriere),"recupererCarriere");
	}
	public boolean recupererEmploi() {
		return ((Boolean)donneesATraiter.objectForKey("recupererEmploi")).booleanValue();
	}
	public void setRecupererEmploi(boolean recupererEmploi) {
		donneesATraiter.setObjectForKey(new Boolean(recupererEmploi),"recupererEmploi");
	}
	public boolean recupererEtatCivil() {
		return ((Boolean)donneesATraiter.objectForKey("recupererEtatCivil")).booleanValue();
	}
	public void setRecupererEtatCivil(boolean recupererEtatCivil) {
		donneesATraiter.setObjectForKey(new Boolean(recupererEtatCivil),"recupererEtatCivil");
	}
	public boolean recupererSpecialisation() {
		return ((Boolean)donneesATraiter.objectForKey("recupererSpecialisation")).booleanValue();
	}
	public void setRecupererSpecialisation(boolean recupererSpecialisation) {
		donneesATraiter.setObjectForKey(new Boolean(recupererSpecialisation),"recupererSpecialisation");
	}
	// Actions
	public void afficherAide() {
		AideUtilisateur controleur = new AideUtilisateur("Aide_EditeurRequete",null);
		controleur.afficherFenetre();
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		debutPeriode = null;
		finPeriode = null;
	}
	protected boolean traitementsPourSuppression() {
		return false;
	}
	protected String messageConfirmationDestruction() {
		return null;
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		return true;	// pas de validation
	}
	protected void traitementsApresValidation() {}
	protected void recupererExceptionValidation(String message) {}
	protected void afficherExceptionValidation(String message) {}
	
	// méthodes privées
	public void setDate(boolean estDebut,String valeur) {
		NSTimestamp dateASetter = null;
		if (valeur != null && valeur.length() > 0) {
			String date = DateCtrl.dateCompletion(valeur);
			if (date != null && date.length() > 0) {
			dateASetter = DateCtrl.stringToDate(date);
			} 
		}
		if (estDebut) {
			debutPeriode = dateASetter;
		} else {
			finPeriode = dateASetter;
		}
		if (debutPeriode() != null && finPeriode() != null && DateCtrl.isAfter(debutPeriode(),finPeriode())) {
			EODialogs.runInformationDialog("Attention","La date de début ne peut être postérieure à la date de fin");
			dateASetter = null;
		}
		if (estDebut) {
			debutPeriode = dateASetter;
		} else {
			finPeriode = dateASetter;
		}
		controllerDisplayGroup().redisplay();
	}
}

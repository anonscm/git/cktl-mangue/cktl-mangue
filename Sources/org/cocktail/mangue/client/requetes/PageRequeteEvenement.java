/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import java.text.NumberFormat;
import java.util.Locale;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.EvenementPourEdition;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.modele.mangue.modalites.EODelegation;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationActivite;
import org.cocktail.mangue.modele.mangue.prolongations.EOReculAge;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class PageRequeteEvenement extends PageEditionRequete {

	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		preparerTypesEvenement();

	}

	/**
	 * 
	 */
	protected NSMutableArray<EvenementPourEdition> preparerInformations() {

		setWaitCursor(true);

		NSMutableArray<EvenementPourEdition> infosEvenement = new NSMutableArray<EvenementPourEdition>();

		NSArray<EOAbsences> evenements = rechercherEvenements();

		String motifEvenement = "";
		String typeEvenement = "";

		boolean agentExclus = false;

		for (EOAbsences evenement : evenements) {

			motifEvenement = "";

			float dureeEvenement = 0,dureePeriodeEvenement = 0;

			if (evenement.toTypeAbsence() != null) {

				typeEvenement = evenement.toTypeAbsence().code();

				if (evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_DEPART) ) {
					EODepart depart = EODepart.rechercherPourAbsence(editingContext(), evenement);
					if (depart != null)
						motifEvenement = depart.motifDepart().libelleCourt();
				}
				if (evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_CRCT) ) {
					EOCrct crct = EOCrct.rechercherPourAbsence(editingContext(), evenement);
					if (crct != null)
						motifEvenement = crct.origineDemande().libelleLong();
				}
				if (evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_DETACHEMENT)
						|| evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_DISPONIBILITE) ) {
					EOChangementPosition changement = EOChangementPosition.rechercherPourAbsence(editingContext(), evenement);
					if (changement != null && changement.toMotifPosition() != null) {
						if (evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_DETACHEMENT)) {							
							if (changement.estDetachementEntrant()) {
								motifEvenement="Entrant - ";
								motifEvenement += changement.toMotifPosition().libelleCourt();
							}
							else {
								motifEvenement="Sortant - ";
								motifEvenement += changement.lieuDestin();
						}
						}
						else
							motifEvenement = changement.toMotifPosition().libelleCourt();
					}

				}
				if (evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_TEMPS_PARTIEL) ) {
					EOTempsPartiel tempsPartiel = EOTempsPartiel.findForAbsence(editingContext(), evenement);
					if (tempsPartiel != null)
						motifEvenement = tempsPartiel.motif().libelleCourt();
				}

				if (evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_DELEGATION) ) {
					EODelegation delegation = EODelegation.findForAbsence(editingContext(), evenement);
					if (delegation != null)
						motifEvenement = delegation.motifDelegation().libelleLong();
				}
				if (evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_PROLONGATION) ) {
					EOProlongationActivite prolongation = EOProlongationActivite.rechercherPourAbsence(editingContext(), evenement);
					if (prolongation != null)
						motifEvenement = prolongation.motif().libelleLong();
				}
				if (evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_RECUL_AGE) ) {
					EOReculAge recul = EOReculAge.findForAbsence(editingContext(), evenement);
					if (recul != null)
						motifEvenement = recul.motif().libelleLong();
				}

			}

			// Cumul des durees (Totale et sur la periode)
			agentExclus = false;
			if (!evenement.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_DEPART)) {
				float duree = 0;
				if (evenement.absDureeTotale() == null || evenement.absDureeTotale().length() == 0 || evenement.absDureeTotale().indexOf("(") > 0) {
					// pour gérer les anciennes données
					duree = evenement.calculerDureeAbsence();
				} else {
					// avec Mangue la durée totale est calculée lors de la gestion de l'absence
					duree = new Float(evenement.absDureeTotale()).floatValue();
				}
				dureePeriodeEvenement += evenement.calculerDureeAbsencePourPeriode(dateDebut(), dateFin());
				if (duree != -1) {		// cas des événements sans date fin
					dureeEvenement += duree;
				}
			}
			else {

				// Verifier que la date de debut du depart soit dans la periode
				agentExclus = DateCtrl.isBetween(evenement.dateDebut(), dateDebut(), dateFin()) == false;

			}

			if (! agentExclus && estIndividuValide(evenement.toIndividu())) {

				// Recuperation du grade de l'agent

				EvenementPourEdition eve = new EvenementPourEdition(editingContext(), evenement.toIndividu(), 
						typeEvenement, motifEvenement, dureeEvenement, dureePeriodeEvenement, evenement.dateDebut(), evenement.dateFin());
				infosEvenement.addObject(eve);
			}
			else
				dureePeriodeEvenement = 0;
			dureeEvenement = 0;

		}

		setWaitCursor(false);

		return infosEvenement;
	}


	/**
	 * 
	 */
	protected String textePourExport() {

		String texte =  "Nom\tPrénom\tType\tMotif\tDurée période\tDurée totale\tDébut\tFin\tGrade\tInsee\tPers Id\tNo Ind\n";

		for (EvenementPourEdition evenement : (NSArray<EvenementPourEdition>) displayGroup().displayedObjects() ) {

			// formatter avec , comme séparateur décimal pour excel
			String duree = "",dureePeriode ="";
			if (evenement.duree() != null) {
				duree = NumberFormat.getInstance(Locale.FRENCH).format(evenement.duree());
				duree = duree.replaceAll(" ","");
			}
			if (evenement.dureePeriode() != null) {
				dureePeriode = NumberFormat.getInstance(Locale.FRENCH).format(evenement.dureePeriode());
				dureePeriode = dureePeriode.replaceAll(" ","");
			}

			texte = texte + evenement.nom() + "\t" + evenement.prenom() + "\t" + evenement.type() + "\t" + 
					evenement.motif() + "\t" + 
					dureePeriode + "\t" + duree + "\t";
			if (evenement.dateDebut() != null) {
				texte = texte + evenement.dateDebut();
			}
			texte = texte + "\t";
			if (evenement.dateFin() != null) {
				texte = texte + evenement.dateFin();
			}

			// Grade
			texte = texte + "\t";
			EOElementCarriere element = EOElementCarriere.rechercherElementADate(editingContext(), evenement.toIndividu(), DateCtrl.stringToDate(evenement.dateDebut()));
			if (element != null)
				texte = texte + element.toGrade().llGrade();
			else {
				EOContratAvenant avenant = EOContratAvenant.rechercherAvenantADate(editingContext(), evenement.toIndividu(), DateCtrl.stringToDate(evenement.dateDebut()));
				if (avenant != null && avenant.toGrade() != null)
					texte = texte + avenant.toGrade().llGrade();
			}

			// Identifiants
			texte = texte + "\t";
			if (evenement.toIndividu().indNoInsee() != null) {
				texte = texte + evenement.toIndividu().indNoInsee()+ " " + evenement.toIndividu().indCleInsee().toString();				
			}
			else {
				if (evenement.toIndividu().indNoInseeProv() != null) {
					texte = texte + evenement.toIndividu().indNoInseeProv();
					if (evenement.toIndividu().indCleInseeProv() != null) {
						texte += " " + evenement.toIndividu().indCleInseeProv().toString();
					}
				}
				else {
					texte = texte + "\t";					
				}
			}
			texte = texte + "\t" + evenement.toIndividu().persId() + "\t" + evenement.toIndividu().noIndividu();

			texte = texte + "\n";
		}
		return texte;
	}

	protected String methodeImpression() {
		return "clientSideRequestImprimerResultatRequeteEvenement";
	}
	protected String nomFichierJasper() {
		return "EditionRequeteEvenement";
	}
	protected String nomFichierImpression() {
		return "ImpressionRequeteEvenement";
	}

	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("type",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("nom",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("prenom",EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
	}
	protected abstract void preparerTypesEvenement();


	/** retourne un tableau de codes selectionnes */
	protected abstract void setWaitCursor(Boolean value);
	protected abstract NSArray typesEvenementSelectionnes();
	protected abstract boolean estIndividuValide(EOIndividu individu);


	/**
	 * 
	 * @return
	 */
	private NSArray rechercherEvenements() {

		String dateDebut = "", dateFin = "";
		if (dateDebut() != null) {
			dateDebut = DateCtrl.dateToString(dateDebut());
		}
		if (dateFin() != null) {
			dateFin = DateCtrl.dateToString(dateFin());
		}
		LogManager.logDetail(this.getClass().getName() + " - rechercherEvenements pour periode du " + dateDebut + " au " + dateFin + " de types " + typesEvenementSelectionnes());
		NSArray<EOAbsences> evenements = EOAbsences.findForPeriodeEtTypes(editingContext(), dateDebut(), dateFin(), typesEvenementSelectionnes());

		// trier par type et numéro individu
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOAbsences.TO_TYPE_ABSENCE_KEY+"."+EOTypeAbsence.CODE_KEY,EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOAbsences.TO_INDIVIDU_KEY+"."+EOIndividu.NO_INDIVIDU_KEY,EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOAbsences.DATE_DEBUT_KEY,EOSortOrdering.CompareAscending));

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(evenements,sorts);
	}
}

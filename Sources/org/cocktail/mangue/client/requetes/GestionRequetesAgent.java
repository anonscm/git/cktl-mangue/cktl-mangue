/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import java.awt.Cursor;
import java.awt.Window;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.outils_interface.ModelePageRequete;
import org.cocktail.mangue.client.preferences.GestionPreferences;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.QualifierPourRequete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOArrivee;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodeHandicap;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogController;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EOAssociation;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Editeur de requetes<BR>
 * Les donnees de la vue a sont gerees par les composants specifiques. Chaque composant est responsable 
 * du qualifier des donnees qu'il gere. Par exemple, le composant li&eacute; aux donnees de selection d'un contrat gere le
 * qualifier sur les avenants de contrat. Voir la description des qualifiers dans chaque composant<BR>
 * Les composants Carriere et Contrat gerent des periodes de temps. Il faut donc s'assurer de la compatibilite de ces intervalles
 * avec la date de reference.Gestion des intervalles de temps et de la date de reference :<BR>
 * Lors de l'initialisation d'un composant, la date de reference et l'operateur courant sont fournis<BR>
 * Les changements de la date de reference ou de l'operateur de comparaison sont notifies par des notifications<BR>
 * Pour chaque changement, on verifie si les periodes sont compatibles avec la date de reference et un message est affiche
 * &agrave; l'utilisateur en cas d'incompatibilite. L'incompatibilite signifie que le resultat de la requete ne rapporte aucune donnee. 
 * On interdit donc de lancer la requete.Voir la classe InterfaceRequeteAvecPeriode pour la description des regles de gestion<BR>
 * Les intervalles de temps dans les qualifiers sont generes dans les composants concernes<BR>
 *
 */

public class GestionRequetesAgent extends PageRequeteAgent implements ChangeListener {
	public EOView vuePeriode;
	private static String controleursVueOnglet[] = {"InterfaceRequeteIndividu","InterfaceRequeteContrat","InterfaceRequeteCarriere","InterfaceRequeteOccupation","InterfaceRequeteSpecialisation"};
	public static int TOUS = 0;
	private final static int TITULAIRES = 1,CONTRACTUELS = 2,NON_AFFECTES = 3, HEBERGES = 4;
	public final static int ENSEIGNANT = 1,NON_ENSEIGNANT = 2;
	public final static int TYPE_HEBERGE_VALIDE = 0, TYPE_TOUT_HEBERGE = 1;
	private final static int ONGLET_CONTRAT = 1,ONGLET_CARRIERE = 2,ONGLET_AFFECTATION = 3,ONGLET_SPECIALISATION = 4;
	private final static String TYPE_INDICE_MAJORE = "Majoré",TYPE_INDICE_EFFECTIF = "Effectif";
	public static final String RAZ_CHAMPS = "RaZeroChamps";
	public EODisplayGroup displayGroupCorps,displayGroupGrade,displayGroupEchelon;
	public EOTable listeAffichageCorps,listeAffichageGrade,listeAffichageEchelon;
	public JComboBox popupTypeIndice;
	public JTabbedPane vueOnglets;
	NSMutableDictionary dictionnaireControleurs;	// contient les références sur les contrôleurs (la clé étant le numéro d'index de l'onglet associé);
	private GestionPeriode controleurPeriode;
	private int typePersonnel,typeEnseignant;
	private String indice;
	private String typeIndice;
	private boolean exclureHeberges,deselectionEnCours,actionEnCours;

	// accesseurs
	public int typeEnseignant() {
		return typeEnseignant;
	}
	public void setTypeEnseignant(int typeEnseignant) {
		if (typeEnseignant != this.typeEnseignant) {
			this.typeEnseignant = typeEnseignant;
			if (typePersonnel == TOUS) {
				exclureHeberges = EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous exclure les hébergés valides ?","Oui","Non");
			}
			restreindreDisplayGroup();
		}
	}
	public int typePersonnel() {
		return typePersonnel;
	}
	public void setTypePersonnel(int typePersonnel) {
		if (typePersonnel != this.typePersonnel) {
			this.typePersonnel = typePersonnel;
			if (typePersonnel == NON_AFFECTES) {
				exclureHeberges = EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous exclure les hébergés valides ?","Oui","Non");
				setTypeEnseignant(TOUS);
				setIndice(null);
				setTypeIndice(null);
			} else {
				if (typePersonnel == TOUS) {
					exclureHeberges = EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous exclure les hébergés valides ?","Oui","Non");
				} else if ((typePersonnel == CONTRACTUELS || typePersonnel == HEBERGES) && typeIndice() != null && typeIndice().equals(TYPE_INDICE_EFFECTIF)) {
					// le type effectif n'est valable que pour les titulaires
					setTypeIndice(null);
				}
			}
			popupTypeIndice.setEnabled(typePersonnel != NON_AFFECTES && peutModifierTypeIndice());
			autoriserOngletsEtListes();
		}
	}
	public String indice() {
		return indice;
	}
	public void setIndice(String aStr) {
		this.indice = aStr;
	}
	public String typeIndice() {
		return typeIndice;
	}
	public void setTypeIndice(String value) {
		if (value != null && value.equals(TYPE_INDICE_EFFECTIF) && (typePersonnel() == CONTRACTUELS || typePersonnel() == HEBERGES)) {
			EODialogs.runInformationDialog("Attention","La notion d'indice effectif ne s'applique qu'aux titulaires");
			this.typeIndice = null;
			popupTypeIndice.setSelectedItem(null);
		} else {
			this.typeIndice = value;
			if (value == null) {
				popupTypeIndice.setSelectedItem(null);
			}
		}
	}
	// méthodes de délégation du display group
	public boolean displayGroupShouldChangeSelection(EODisplayGroup group,NSArray newIndexes) {
		return (indice == null) && (typePersonnel() != NON_AFFECTES || deselectionEnCours);
	}
	
	/**
	 * 
	 */
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		String attributRecherche = null,attributSource = null,nomEntite = null;
		EOQualifier restrictionQualifier = null;
		EODisplayGroup displayGroupToFetch = null;
		if (group == displayGroup()) {
			attributRecherche = EOCorps.C_TYPE_CORPS_KEY;
			attributSource = EOTypePopulation.CODE_KEY;
			NSMutableArray args = new NSMutableArray();
			String stringQualifier = EOCorps.LL_CORPS_KEY + " != ' ' AND " + EOCorps.LL_CORPS_KEY + " != nil";
			if (debutPeriode() != null) {
				args.addObject(debutPeriode());
				stringQualifier = stringQualifier + " AND (" + EOCorps.D_OUVERTURE_CORPS_KEY  + " = nil OR dOuvertureCorps <= %@)";
			} else {
				stringQualifier = stringQualifier + " AND " + EOCorps.D_OUVERTURE_CORPS_KEY + " = nil";
			}
			if (finPeriode() != null) {
				args.addObject(finPeriode());
				stringQualifier = stringQualifier + " AND (" + EOCorps.D_FERMETURE_CORPS_KEY  + " = nil  OR dFermetureCorps >= %@)";
			} else {
				stringQualifier = stringQualifier + " AND " + EOCorps.D_FERMETURE_CORPS_KEY + " = nil";
			}
			restrictionQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
			nomEntite = EOCorps.ENTITY_NAME;
			displayGroupToFetch = displayGroupCorps;
			// invalider les données dans grade et échelon
			displayGroupGrade.setObjectArray(null);
			displayGroupEchelon.setObjectArray(null);
		} else if (group == displayGroupCorps) {
			attributRecherche = EOGrade.C_CORPS_KEY;
			attributSource = EOCorps.C_CORPS_KEY;
			restrictionQualifier = SuperFinder.qualifierPourPeriode(EOGrade.D_OUVERTURE_KEY, debutPeriode(), EOGrade.D_FERMETURE_KEY, finPeriode());
			nomEntite = EOGrade.ENTITY_NAME;
			displayGroupToFetch = displayGroupGrade;
			// invalider les données dans échelon
		} else if (group == displayGroupGrade) {
			attributRecherche = EOPassageEchelon.C_GRADE_KEY;
			attributSource = EOGrade.C_GRADE_KEY;
			restrictionQualifier = SuperFinder.qualifierPourPeriode(EOPassageEchelon.D_OUVERTURE_KEY, debutPeriode(), EOPassageEchelon.D_FERMETURE_KEY, finPeriode());
			nomEntite = EOPassageEchelon.ENTITY_NAME;
			displayGroupToFetch = displayGroupEchelon;
		}
		if (nomEntite != null) {
			if (group.selectedObjects() == null || group.selectedObjects().count() == 0 || group.selectedObjects().count() > 1) {
				// si sélection unique, on autorise l'affichage des sous infos, sinon non
				// exemple : si plusieurs types de population choisis, on n'affiche pas les corps
				displayGroupToFetch.setObjectArray(null);
			} else {
				EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(attributRecherche + " = %@",new NSArray((((EOGenericRecord)group.selectedObject()).valueForKey(attributSource))));
				if (restrictionQualifier != null) {
					NSMutableArray qualifiers = new NSMutableArray(2);
					qualifiers.addObject(qualifier);
					qualifiers.addObject(restrictionQualifier);
					qualifier = new EOAndQualifier(qualifiers);
				}
				EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier,null);
				displayGroupToFetch.setObjectArray(editingContext().objectsWithFetchSpecification(fs));
			}
			displayGroupToFetch.updateDisplayedObjects();
		}
		controllerDisplayGroup().redisplay();
	} 
	// actions
	public void popupTypeIndiceHasChanged() {
		if (popupTypeIndice == null || actionEnCours) {		// au démarrage  ou pour éviter les appels récursifs
			return;
		}
		actionEnCours = true;
		setTypeIndice((String)popupTypeIndice.getSelectedItem());
		controllerDisplayGroup().redisplay();
		actionEnCours = false;
	}
	public void deselectionnerTypePopulation() {
		LogManager.logDetail("GestionRequetesAgent - deselectionnerTypePopulation");
		displayGroup().setSelectedObject(null);
		displayGroup().updateDisplayedObjects();
	}
	public void deselectionnerCorps() {
		LogManager.logDetail("GestionRequetesAgent - deselectionnerCorps");
		displayGroupCorps.setSelectedObject(null);
		displayGroupCorps.updateDisplayedObjects();
	}
	public void deselectionnerGrade() {
		LogManager.logDetail("GestionRequetesAgent - deselectionnerGrade");
		displayGroupGrade.setSelectedObject(null);
		displayGroupGrade.updateDisplayedObjects();
	}
	public void deselectionnerEchelon() {
		LogManager.logDetail("GestionRequetesAgent - deselectionnerEchelon");
		displayGroupEchelon.setSelectedObject(null);
		displayGroupEchelon.updateDisplayedObjects();
	}
	public void remettreAZero() {
		LogManager.logDetail("GestionRequetesAgent - remettreAZero");
		NSNotificationCenter.defaultCenter().postNotification(RAZ_CHAMPS,null);
		setTypePersonnel(TOUS);
		setTypeEnseignant(TOUS);
		indice = null;
		setTypeIndice(null);
		deselectionnerTypePopulation();
		controllerDisplayGroup().redisplay();
	}

	/**
	 * 
	 */
	public void rechercher() {

		Window activeWindow = EOApplication.sharedApplication().windowObserver().activeWindow();
		if (activeWindow != null) {
			activeWindow.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		} 

		try {
			
			// préparer la liste des requêtes qui seront transmises au controleur d'affichage des données
			NSMutableArray requetes = new NSMutableArray();
			EOQualifier autreQualifierContrat = null;
			if (typePersonnel() == NON_AFFECTES) {
				requetes.addObject(new QualifierPourRequete(EOPersonnel.ENTITY_NAME,recupererQualifierNonAffectes()));
				// Dans le cas des non-affectés, les requêtes suivantes sont gérées comme des personnels à exclure

				if (exclureHeberges) {
					requetes.addObject(new QualifierPourRequete(EOContratHeberges.ENTITY_NAME,new EOAndQualifier(qualifiersPourContratsHeberges(TYPE_TOUT_HEBERGE))));
				}
			} else {
				if (typePersonnel() == TOUS || typePersonnel() == TITULAIRES) {
					InterfaceRequeteCarriere controleur = (InterfaceRequeteCarriere)controleur("InterfaceRequeteCarriere");
					if (controleur == null || controleur.doitRecupererElementsCarriere()) {	// controleur == null => on veut toutes les positions
						EOQualifier qualifier = recupererQualifierTitulaires(true);
						if (qualifier != null) {	// Null si l'indice ne correspond à aucun passage échelon
							requetes.addObject(new QualifierPourRequete(EOElementCarriere.ENTITY_NAME,qualifier));
						}	
					} else {		// on veut récupérer les autres infos
						NSArray autresInfos = controleur.autresInfosModalites();
						for (java.util.Enumeration<InformationPourModalite> e = autresInfos.objectEnumerator();e.hasMoreElements();) {
							InformationPourModalite info = e.nextElement();
							requetes.addObject(new QualifierPourRequete(info.nomEntite(),info.qualifierRecherche(debutPeriode(),finPeriode())));
						}
					}
					if ((controleur == null || controleur.doitRecupererCarriere()) && indice() == null) {
						// On doit egalement recuperer les agents qui ont une carriere correspondant a une position ne 
						// necessitant pas d'éléments de carriere
						EOQualifier qualif = recupererQualifierTitulaires(false);
						if (qualif != null) {
							requetes.addObject(new QualifierPourRequete(EOCarriere.ENTITY_NAME,qualif));
						}
					}
					// récupérer le qualifier pour les positions
					EOQualifier qualif = recupererQualifier("InterfaceRequeteCarriere");
					if (qualif != null) {
						requetes.addObject(new QualifierPourRequete(EOPosition.ENTITY_NAME,qualif));
					}
				}

				if (typePersonnel() == TOUS || typePersonnel() == CONTRACTUELS) {
					// on ne peut sélectionner que les contractuels, les hébergés
					InterfaceRequeteContrat controleurContrat = (InterfaceRequeteContrat)controleur("InterfaceRequeteContrat");
					if (controleurContrat != null) {

						NSArray qualifiers = qualifiersPourContrat();

						requetes.addObject(new QualifierPourRequete(EOContratAvenant.ENTITY_NAME,recupererQualifierContrats(qualifiers)));

						autreQualifierContrat = recupererAutreQualifier(qualifiers);

						if (controleurContrat.estContratRecherche()) {
							requetes.addObject(new QualifierPourRequete(EOContrat.ENTITY_NAME,EOQualifier.qualifierWithQualifierFormat("temRecherche = 'O'", null)));
						}
					} 
				}

				if ((typePersonnel() == TOUS && exclureHeberges == false) || typePersonnel() == HEBERGES) {
					requetes.addObject(new QualifierPourRequete(EOContratHeberges.ENTITY_NAME,new EOAndQualifier(qualifiersPourContratsHeberges(TYPE_HEBERGE_VALIDE))));
				}
				
				//  Récuperer le qualifier sur les affectations et les occupations
				InterfaceRequeteOccupation controleur = (InterfaceRequeteOccupation)controleur("InterfaceRequeteOccupation");
				if (controleur != null) { 	// ie on a réglé une affectation ou occupation
					EOQualifier qualif = controleur.qualifierRecherche();
					if (qualif != null) {
						requetes.addObject(new QualifierPourRequete(EOAffectation.ENTITY_NAME,qualif));
					}
					qualif = controleur.qualifierOccupation();
					if (qualif != null) {
						requetes.addObject(new QualifierPourRequete(EOOccupation.ENTITY_NAME,qualif));
					}
				}
			}
			// Récupérer le qualifier éventuel pour les agents handicapés
			InterfaceRequeteIndividu controleur = (InterfaceRequeteIndividu)controleur("InterfaceRequeteIndividu");
			if (controleur != null) {
				EOQualifier qualif = controleur.qualifierHandicap();
				if (qualif != null) {
					requetes.addObject(new QualifierPourRequete(EOPeriodeHandicap.ENTITY_NAME,qualif));
				}
				qualif = controleur.qualifierPremiereAffectation();
				if (qualif != null) {
					requetes.addObject(new QualifierPourRequete(EOArrivee.ENTITY_NAME,qualif));
				}
			}

			EditionRequeteAgent controleurEdition = new EditionRequeteAgent(requetes,autreQualifierContrat,donneesATraiter(),debutPeriode(),finPeriode(),typeEnseignant);
			EODialogController.runControllerInNewDialog(controleurEdition,"Résultat de la recherche");
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (activeWindow != null) {
				activeWindow.setCursor(Cursor.getDefaultCursor());
			} 
		}
	}
	// Notifications
	public void synchroniserDates(NSNotification aNotif) {
		LogManager.logDetail("GestionRequetesAgent - synchroniserDates");
		setDateDebut(controleurPeriode.debutPeriode());
		setDateFin(controleurPeriode.finPeriode());
		// 10/10/07 - pour refléter les corps et grades actifs pendant la période
		EOCorps currentCorps = (EOCorps)displayGroupCorps.selectedObject();
		if (displayGroupCorps.displayedObjects().count() > 0) {
			displayGroupDidChangeSelection(displayGroup());
		}
		if (currentCorps != null && displayGroupCorps.displayedObjects().containsObject(currentCorps)) {
			displayGroupDidChangeSelection(displayGroupCorps);
		}
	}
	public void modifierTypePopulation(NSNotification aNotif) {
		LogManager.logDetail("GestionRequetesAgent - modifierTypePopulation");
		initialiserDisplayGroup();
	}
	public void raffraichirDonnees(NSNotification aNotif) {
		LogManager.logDetail("GestionRequetesAgent - raffraichirDonnees");
		controllerDisplayGroup().redisplay();
	}
	//	Interface Change Listener
	public void stateChanged(ChangeEvent e) {
		
		Object source = e.getSource();
		if (source instanceof JTabbedPane) {
			JTabbedPane onglet = (JTabbedPane)source;
			int index = onglet.getSelectedIndex();
			if (dictionnaireControleurs.objectForKey(new Integer(index)) == null) {
				ajouterControleur(index);
				
			}
			
		}
	}
	// méthodes du controller DG
	public boolean peutModifierTypeIndice() {
		return peutSelectionnerTypeEnseignant() && displayGroup().selectedObject() == null && 
		displayGroupCorps.selectedObject() == null && displayGroupGrade.selectedObject() == null && 
		displayGroupEchelon.selectedObject() == null;
	}
	public boolean peutModifierIndice() {
		return peutModifierTypeIndice() && typeIndice() != null;
	}
	/** On ne peut demander l'&eacute;valuation de l'anciennet&eacute; que si la date de fin de p&eacute;riode est fournie */
	public boolean peutRecupererAnciennete() {
		return finPeriode() != null && typePersonnel() != HEBERGES;
	}
	public boolean peutSelectionnerTypeEnseignant() {
		return typePersonnel() != NON_AFFECTES;
	}
	public boolean peutRechercher() {
		if (typePersonnel() == NON_AFFECTES) {
			return true;
		}
		if (debutPeriode() == null) {
			return false;
		}
		if (typePersonnel() == HEBERGES) {
			return true;
		}
		InterfaceRequeteCarriere controleur1 = (InterfaceRequeteCarriere)controleur("InterfaceRequeteCarriere");
		InterfaceRequeteOccupation controleur2 = (InterfaceRequeteOccupation)controleur("InterfaceRequeteOccupation");

		if (controleur1 != null && controleur1.infoSansAffectionSelectionnee()) {
			return true;
		}
		if (controleur2 == null) {	// l'utilisateur n'a pas encore activé ce contrôleur
			return false;
		}
		if (controleur1 == null) {
			return controleur2.structureSelectionnee();
		} else {
			return !controleur1.doitSelectionnerAffectation() || (controleur1.doitSelectionnerAffectation() && controleur2.structureSelectionnee());
		}
	}
	// méthodes protégées
	protected void preparerFenetre() {

		typePersonnel = TOUS;
		typeEnseignant = TOUS;
		
		super.preparerFenetre();

		loadNotifications();
		dictionnaireControleurs = new NSMutableDictionary();
		displayGroupCorps.setSelectsFirstObjectAfterFetch(false);
		setTypeIndice(null);
		// Autoriser la sélection multiple
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		preparerAffichage(listeAffichageCorps);
		displayGroupGrade.setSelectsFirstObjectAfterFetch(false);
		preparerAffichage(listeAffichageGrade);
		displayGroupEchelon.setSelectsFirstObjectAfterFetch(false);
		preparerAffichage(listeAffichageEchelon);
		vueOnglets.addChangeListener(this);

		vueOnglets.setSelectedIndex(4);
		ajouterControleur(4);
		vueOnglets.setSelectedIndex(3);
		ajouterControleur(3);
		vueOnglets.setSelectedIndex(2);
		ajouterControleur(2);
		vueOnglets.setSelectedIndex(1);
		ajouterControleur(1);
		vueOnglets.setSelectedIndex(0);
		ajouterControleur(0);

		
		String today = DateCtrl.dateToString(new NSTimestamp());
		controleurPeriode = new GestionPeriode(today,today,GestionPeriode.PERIODE_SAISIE);
		controleurPeriode.init();
		
		GraphicUtilities.swaperView(vuePeriode,controleurPeriode.component());
		
	}

	protected void terminer() {
		vueOnglets.removeChangeListener(this);
		for (java.util.Enumeration<ModelePageRequete> e = dictionnaireControleurs.allValues().objectEnumerator();e.hasMoreElements();) {
			ModelePageRequete controleur = e.nextElement();
			controleur.terminerControleur();
		}
	}
	
	/**
	 * 
	 */
	protected NSArray<EOTypePopulation> fetcherObjets() {
		// à restreindre selon le type de population géré
		NSArray typesPopulations = ((ApplicationClient)EOApplication.sharedApplication()).populationsGerees(editingContext());
		if (typesPopulations != null) {
			return typesPopulations;
		} else {		// toutes populations gérées

			// Vérifier si il faut inclure les normaliens
			NSMutableArray qualifiers = new NSMutableArray();

			if (EOGrhumParametres.isGestionHu() == false) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.TEM_HOSPITALIER_KEY + " = 'N'", null));
			}
			if (EOGrhumParametres.isGestionEns() == false) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.CODE_KEY + " != 'N'",null));
			}				

			return EOTypePopulation.fetchAll(editingContext(), new EOAndQualifier(qualifiers), Nomenclature.SORT_ARRAY_LIBELLE_COURT);
		}
	}
	
	/**
	 * 
	 */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(Nomenclature.SORT_ARRAY_LIBELLE_COURT);
		displayGroupCorps.setSortOrderings(new NSArray(new EOSortOrdering(EOCorps.LL_CORPS_KEY,EOSortOrdering.CompareAscending)));
		displayGroupGrade.setSortOrderings(new NSArray(new EOSortOrdering(EOGrade.LC_GRADE_KEY,EOSortOrdering.CompareAscending)));
		displayGroupEchelon.setSortOrderings(new NSArray(new EOSortOrdering(EOPassageEchelon.C_ECHELON_KEY,EOSortOrdering.CompareAscending)));
	}
	// méthodes privées
	private void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("modifierTypePopulation", new Class[] { NSNotification.class }), GestionPreferences.PREFERENCES_CHANGEES, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("raffraichirDonnees", new Class[] { NSNotification.class }), ModelePageRequete.RAFFRAICHIR, null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserDates", new Class[] {NSNotification.class}),
				GestionPeriode.NOTIFICATION_PERIODE_HAS_CHANGED,null);
	}
	private void preparerAffichage(EOTable aTable) {
		GraphicUtilities.changerTaillePolice(aTable,11);
		GraphicUtilities.rendreNonEditable(aTable);
		// Autoriser la sélection multiple
		aTable.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

	}
	private void ajouterControleur(int index) {
		if (index + 1 > controleursVueOnglet.length) {
			return;
		}
		String nomArchive = controleursVueOnglet[index];
		if (nomArchive != null) {
			try {
				Class classe = Class.forName("org.cocktail.mangue.client.requetes." + nomArchive);
				ModelePageRequete controleur = (ModelePageRequete)classe.newInstance();
				controleur.init(debutPeriode(),finPeriode());
				JPanel vue = (JPanel)vueOnglets.getComponentAt(index);
				GraphicUtilities.swaperViewEtCentrer(vue,controleur.component());
				controleur.activer();
				
				dictionnaireControleurs.setObjectForKey(controleur,new Integer(index));
				
			} catch (Exception e) {
				LogManager.logException(e);
			}
		}
	}
	private EOQualifier construireOrQualifier(String attributRecherche,NSArray valeurs) {
		String stringQualifier = "";
		for (java.util.Enumeration<String> e = valeurs.objectEnumerator();e.hasMoreElements();) {
			String valeur = e.nextElement();
			stringQualifier = stringQualifier + attributRecherche + " = '" + valeur + "' OR ";
		}
		int lastIndex = stringQualifier.lastIndexOf(" OR ");
		if (lastIndex > 0) {
			stringQualifier = stringQualifier.substring(0,lastIndex);
		}
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier,null);
	}
	private void autoriserOngletsEtListes() {
		vueOnglets.setEnabledAt(ONGLET_CONTRAT,(typePersonnel() == TOUS || typePersonnel() == CONTRACTUELS || typePersonnel() == HEBERGES));
		vueOnglets.setEnabledAt(ONGLET_CARRIERE,(typePersonnel() == TOUS || typePersonnel() == TITULAIRES));
		vueOnglets.setEnabledAt(ONGLET_AFFECTATION, typePersonnel() != NON_AFFECTES);
		vueOnglets.setEnabledAt(ONGLET_SPECIALISATION, typePersonnel() != NON_AFFECTES && typePersonnel() != HEBERGES);
		if (typePersonnel() == CONTRACTUELS || typePersonnel() == NON_AFFECTES || typePersonnel() == HEBERGES) {
			// Effacer les données de carrière
			ModelePageRequete controleur = (ModelePageRequete)dictionnaireControleurs.objectForKey(new Integer(ONGLET_CARRIERE));
			if (controleur != null) {	// null  quand pas encore chargé
				controleur.effacer();
			}
		}
		if (typePersonnel() == TITULAIRES || typePersonnel() == NON_AFFECTES) {
			// Effacer les données de contrat
			ModelePageRequete controleur = (ModelePageRequete)dictionnaireControleurs.objectForKey(new Integer(ONGLET_CONTRAT));
			if (controleur != null) {		// null  quand pas encore chargé
				controleur.effacer();
			}
		}
		if (typePersonnel() == NON_AFFECTES) {
			// Effacer les données d'affectation
			ModelePageRequete controleur = (ModelePageRequete)dictionnaireControleurs.objectForKey(new Integer(ONGLET_AFFECTATION));
			if (controleur != null) {		// null  quand pas encore chargé
				controleur.effacer();
			}
		}
		if (typePersonnel() == NON_AFFECTES || typePersonnel() == HEBERGES) {
			// Effacer les données de spécialisation
			ModelePageRequete controleur = (ModelePageRequete)dictionnaireControleurs.objectForKey(new Integer(ONGLET_SPECIALISATION));
			if (controleur != null) {		// null  quand pas encore chargé
				controleur.effacer();
			}
		}
		if (typePersonnel() == NON_AFFECTES || typePersonnel() == HEBERGES) {
			vueOnglets.setSelectedIndex(0);
		} else {
			int selectedIndex = vueOnglets.getSelectedIndex();
			if ((selectedIndex == ONGLET_CONTRAT && vueOnglets.isEnabledAt(ONGLET_CONTRAT) == false) ||
					(selectedIndex == ONGLET_CARRIERE && vueOnglets.isEnabledAt(ONGLET_CARRIERE) == false)) {
				vueOnglets.setSelectedIndex(0);
			}
		}
		deselectionEnCours = true;
		if (!peutSelectionnerTypeEnseignant()) {
			deselectionnerTypePopulation();
			deselectionnerCorps();
			deselectionnerGrade();
			deselectionnerEchelon();
		}
		deselectionEnCours = false;
	}
	private void restreindreDisplayGroup() {
		if (typeEnseignant() == TOUS) {
			displayGroup().setQualifier(null);
		} else if (typeEnseignant() == ENSEIGNANT) {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.TEM_ENSEIGNANT_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		} else {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.TEM_ENSEIGNANT_KEY + "=%@", new NSArray(CocktailConstantes.FAUX)));
		}
		displayGroup().updateDisplayedObjects();
		displayGroup().setSelectedObject(null);
	}

	// Gestion des qualifiers
	private ModelePageRequete controleur(String nomInterface) {
		for (int i = 0; i < controleursVueOnglet.length;i++) {
			if (nomInterface.equals(controleursVueOnglet[i])) {
				return (ModelePageRequete)dictionnaireControleurs.objectForKey(new Integer(i));
			}
		}
		return null;
	}
	private EOQualifier recupererQualifier(String nomInterface) {
		ModelePageRequete controleur = controleur(nomInterface);
		if (controleur != null) {
			return controleur.qualifierRecherche();
		} else {
			return null;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private EOQualifier recupererQualifierNonAffectes() {
		// Qualifiers sur l'identite
		InterfaceRequeteIndividu controleurIndividu = (InterfaceRequeteIndividu)controleur("InterfaceRequeteIndividu");
		if (controleurIndividu != null) {
			return controleurIndividu.qualifierRecherche("toIndividu");
		} else {
			return EOQualifier.qualifierWithQualifierFormat("toIndividu.temValide = 'O' AND toIndividu.nomUsuel like '*'", null);
		}
	}
	
	/**
	 * 
	 * @param pourElementCarriere
	 * @return
	 */
	private EOQualifier recupererQualifierTitulaires(boolean pourElementCarriere) {
		NSMutableArray	mesQualifiers = new NSMutableArray();
		// Qualifiers sur l'individu
		// 08/12/09 - Bug quand recherche des caractéristiques de l'individu, la relation sur l'individu n'était pas fournie
		InterfaceRequeteIndividu controleurIndividu = (InterfaceRequeteIndividu)controleur("InterfaceRequeteIndividu");
		EOQualifier qualif = null;
		if (controleurIndividu != null) {
			qualif = controleurIndividu.qualifierRecherche(EOElementCarriere.TO_INDIVIDU_KEY);
			if (qualif != null) {
				mesQualifiers.addObject(qualif);
			}
		}
		// On s'intéresse aux éléments de carrière ou aux segments valides
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCarriere.TEM_VALIDE_KEY + " = 'O'", null));
		if (pourElementCarriere) {
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TEM_PROVISOIRE_KEY + " = %@", new NSArray("N")));
		}
		// Qualifier sur les specialisations
		InterfaceRequeteSpecialisation controleur = (InterfaceRequeteSpecialisation)controleur("InterfaceRequeteSpecialisation");
		if (controleur != null) {
			if (pourElementCarriere) {
				qualif = controleur.qualifierElementCarriere();
			} else {
				qualif = controleur.qualifierCarriere();
			}
			if (qualif != null) {
				mesQualifiers.addObject(qualif);
			}
		}
		// Qualifier sur le type Enseignant / Non Enseignant
		if (typeEnseignant() != TOUS) {
			String attributRecherche = EOCarriere.TO_TYPE_POPULATION_KEY + "." + EOTypePopulation.TEM_ENSEIGNANT_KEY;
			if (pourElementCarriere) {
				attributRecherche = EOElementCarriere.TO_CARRIERE_KEY + "." + attributRecherche;
			}
			if (typeEnseignant() == ENSEIGNANT) {
				qualif = EOQualifier.qualifierWithQualifierFormat(attributRecherche + " = 'O'",null);
			} else {
				qualif = EOQualifier.qualifierWithQualifierFormat(attributRecherche + " = 'N'",null);
			}
			mesQualifiers.addObject(qualif);
		}
		// Qualifier sur la date de reference
		if (pourElementCarriere) {
			qualif = SuperFinder.qualifierPourPeriode(EOElementCarriere.DATE_DEBUT_KEY,debutPeriode(), EOElementCarriere.DATE_FIN_KEY,finPeriode());
		} else {
			qualif = SuperFinder.qualifierPourPeriode(EOCarriere.DATE_DEBUT_KEY,debutPeriode(), EOCarriere.DATE_FIN_KEY, finPeriode());
		}
		mesQualifiers.addObject(qualif);
		if (pourElementCarriere) {
			// Qualifier sur les grades
			if (displayGroupGrade.selectedObjects().count() > 0) {
				qualif = construireOrQualifier(EOElementCarriere.TO_GRADE_KEY + ".cGrade",(NSArray)displayGroupGrade.selectedObjects().valueForKey("cGrade"));
				mesQualifiers.addObject(qualif);
			}
			// Qualifier sur les corps
			if (displayGroupCorps.selectedObjects().count() > 0) {
				qualif = construireOrQualifier(EOElementCarriere.TO_CORPS_KEY + ".cCorps",(NSArray)displayGroupCorps.selectedObjects().valueForKey("cCorps"));
				mesQualifiers.addObject(qualif);
			} else if (displayGroup().selectedObjects().count() > 0) {
				qualif = construireOrQualifier(EOElementCarriere.TO_CARRIERE_KEY + ".cTypePopulation",(NSArray)displayGroup().selectedObjects().valueForKey(INomenclature.CODE_KEY));
				mesQualifiers.addObject(qualif);
			}
			// Qualifier sur les echelons
			if (displayGroupEchelon.selectedObjects().count() > 0) {
				qualif = construireOrQualifier(EOElementCarriere.C_ECHELON_KEY,(NSArray)displayGroupEchelon.selectedObjects().valueForKey(EOPassageEchelon.C_ECHELON_KEY));
				mesQualifiers.addObject(qualif);
			}
			if (indice() != null && indice().length() > 0) {
				qualif = recupererQualifierIndice();
				if (qualif != null) {
					mesQualifiers.addObject(qualif);
				} else {
					// On interrompt le traitement car on ne trouvera pas d'agent avec cet échelon
					return null;
				}
			}
		}

		qualif = new EOAndQualifier(mesQualifiers);
		return qualif;
	}


	/**
	 * 
	 * @param qualifiers
	 * @return
	 */
	private EOQualifier recupererQualifierContrats(NSArray qualifiers) {
		NSMutableArray allQualifiers = new NSMutableArray(qualifiers);

		if (typeEnseignant() != TOUS) {
			EOQualifier qualif;
			if (typeEnseignant() == ENSEIGNANT) {
				qualif = EOQualifier.qualifierWithQualifierFormat("toGrade.toCorps.toTypePopulation.temEnseignant = 'O'",null);	// DT 1253 - 09/01/09 - corrigé le 04/05/09 par la méthode complémentaire autreQualifier
			} else {
				qualif = EOQualifier.qualifierWithQualifierFormat("toGrade.toCorps.toTypePopulation.temEnseignant = 'N'",null);	// DT 1253 - 09/01/09 - corrigé le 04/05/09 par la méthode complémentaire autreQualifier
			}
			allQualifiers.addObject(qualif);
		}
		return new EOAndQualifier(allQualifiers);
	}
	// DT 1253 - modification du 04/05/09 pour récupérer les agents correctement qualifiés du point de vue enseignant
	private EOQualifier recupererAutreQualifier(NSArray qualifiers) {
		String nomRelation = EOContratAvenant.CONTRAT_KEY + "." + EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY;
		// Autre qualifier sur le type Enseignant / Non Enseignant
		if (typeEnseignant() != TOUS) {
			NSMutableArray allQualifiers = new NSMutableArray(qualifiers);
			if (typeEnseignant() == ENSEIGNANT) {
				allQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(nomRelation + "." + EOTypeContratTravail.TEM_ENSEIGNANT_KEY + " = 'O'",null));	// DT 1253 - 09/01/09 - correction du 04/05/09
			} else {
				allQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(nomRelation + "." + EOTypeContratTravail.TEM_ENSEIGNANT_KEY + " = 'N'",null));	// DT 1253 - 09/01/09 - correction du 04/05/09
			}
			return new EOAndQualifier(allQualifiers);
		} else {
			return null;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private NSMutableArray qualifiersPourContrat() {
		NSMutableArray mesQualifiers = new NSMutableArray();
		// 03/06/2010 - qualifier sur la validité des avenants
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContrat.TEM_ANNULATION_KEY + "='N'", null));
		// Qualifiers sur l'identite
		InterfaceRequeteIndividu controleurIndividu = (InterfaceRequeteIndividu)controleur("InterfaceRequeteIndividu");
		if (controleurIndividu != null) {
			EOQualifier qualif = controleurIndividu.qualifierRecherche(EOContratAvenant.CONTRAT_KEY+"."+EOContrat.TO_INDIVIDU_KEY);
			if (qualif != null) {
				mesQualifiers.addObject(qualif);
			}
		}
		// Qualifier sur les specialisations
		InterfaceRequeteSpecialisation controleur = (InterfaceRequeteSpecialisation)controleur("InterfaceRequeteSpecialisation");
		if (controleur != null) {		// spécialisation choisie par l'utilisateur
			EOQualifier qualif = controleur.qualifierContrat();
			if (qualif != null) {
				mesQualifiers.addObject(qualif);
			}
		}
		// Qualifiers sur le contrat
		EOQualifier qualif = recupererQualifier("InterfaceRequeteContrat");
		if (qualif == null) {
			// qualifier sur la date de référence à ajouter, il n'a pas été ajouté par Interface contrat
			qualif = SuperFinder.qualifierPourPeriode(EOContratAvenant.DATE_DEBUT_KEY,debutPeriode(),EOContratAvenant.DATE_FIN_KEY,finPeriode());
			mesQualifiers.addObject(qualif);
			// 25/09/09 - ajouter un qualifier sur la date de fin anticipéee (DT 1843)
			if (finPeriode() == null) {
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOContratAvenant.CONTRAT_KEY+"."+EOContrat.DATE_FIN_ANTICIPEE_KEY + "=NIL",null));
			} else {
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOContratAvenant.CONTRAT_KEY+"."+EOContrat.DATE_FIN_ANTICIPEE_KEY+" = NIL OR contrat.dateFinAnticipee >= %@",new NSArray(debutPeriode())));
			}
		}
		mesQualifiers.addObject(qualif);

		// Qualifier sur les grades
		if (displayGroupGrade.selectedObjects().count() > 0) {
			qualif = construireOrQualifier(EOContratAvenant.TO_GRADE_KEY+"."+EOGrade.C_GRADE_KEY, (NSArray)displayGroupGrade.selectedObjects().valueForKey(EOGrade.C_GRADE_KEY));
			mesQualifiers.addObject(qualif);
		}
		// Qualifier sur les corps
		if (displayGroupCorps.selectedObjects().count() > 0) {
			qualif = construireOrQualifier(EOContratAvenant.TO_GRADE_KEY+".toCorps.cCorps",(NSArray)displayGroupCorps.selectedObjects().valueForKey(EOCorps.C_CORPS_KEY));
			mesQualifiers.addObject(qualif);
		} else if (displayGroup().selectedObjects().count() > 0){
			qualif = construireOrQualifier(EOContratAvenant.TO_GRADE_KEY+".toCorps.cTypeCorps",(NSArray)displayGroup().selectedObjects().valueForKey(INomenclature.CODE_KEY));
			mesQualifiers.addObject(qualif);
		}
		// Qualifier sur les echelons
		if (displayGroupEchelon.selectedObjects().count() > 0) {
			qualif = construireOrQualifier("cEchelon",(NSArray)displayGroupEchelon.selectedObjects().valueForKey("cEchelon"));
			mesQualifiers.addObject(qualif);
		}
		if (indice() != null && indice().length() > 0) {
			if (typeIndice() != null && typeIndice().equals(TYPE_INDICE_MAJORE)) {
				try {
					Integer indiceMajore = new Integer(indice());
					NSArray indices = EOIndice.indicesPourIndiceMajoreEtDate(editingContext(), indiceMajore, debutPeriode());
					if (indices != null &&indices.count() > 0) {
						mesQualifiers.addObject(SuperFinder.construireORQualifier("indiceContrat", (NSArray)indices.valueForKey("cIndiceBrut")));
					}
				} catch (Exception e) {
					EODialogs.runErrorDialog("Erreur", "Un indice majoré est une valeur numérique, l'indice ne sera pas pris en compte");
				}	
			} else {
				NSArray args = new NSArray(indice());
				qualif = EOQualifier.qualifierWithQualifierFormat("indiceContrat = %@", args);
				mesQualifiers.addObject(qualif);
			}
		}

		return mesQualifiers;

	}
	
	/**
	 * 
	 * @param typeHeberge
	 * @return
	 */
	private NSMutableArray qualifiersPourContratsHeberges(int typeHeberge) {
		NSMutableArray mesQualifiers = new NSMutableArray();
		// Qualifiers sur l'identite
		InterfaceRequeteIndividu controleurIndividu = (InterfaceRequeteIndividu)controleur("InterfaceRequeteIndividu");
		if (controleurIndividu != null) {
			EOQualifier qualif = controleurIndividu.qualifierRecherche("individu");
			if (qualif != null) {
				mesQualifiers.addObject(qualif);
			}
		}
		// Qualifiers sur le contrat
		EOQualifier qualif = null;
		InterfaceRequeteContrat controleurContrat = (InterfaceRequeteContrat)controleur("InterfaceRequeteContrat");
		if (controleurContrat != null) {
			qualif = controleurContrat.qualifierRecherchePourHeberge(typeHeberge);
		} else {
			// 31/03/2011 - selon type hébergé on ajoutera les différents types
			if (typeHeberge == TYPE_HEBERGE_VALIDE) {
				qualif = EOQualifier.qualifierWithQualifierFormat("temValide = 'O'", null);
			}
		}
		if (qualif == null) {
			// qualifier sur la date de référence à ajouter, il n'a pas été ajouté par Interface contrat
			qualif = SuperFinder.qualifierPourPeriode("dateDebut",debutPeriode(),"dateFin",finPeriode());
			mesQualifiers.addObject(qualif);
		}
		mesQualifiers.addObject(qualif);

		// Qualifier sur les grades
		if (displayGroupGrade.selectedObjects().count() > 0) {
			qualif = construireOrQualifier(EOContratHeberges.TO_GRADE_KEY+"."+EOGrade.C_GRADE_KEY,(NSArray)displayGroupGrade.selectedObjects().valueForKey("cGrade"));
			mesQualifiers.addObject(qualif);
		}
		// Qualifier sur les corps
		if (displayGroupCorps.selectedObjects().count() > 0) {
			qualif = construireOrQualifier(EOContratHeberges.TO_CORPS_KEY+"."+EOCorps.C_CORPS_KEY,(NSArray)displayGroupCorps.selectedObjects().valueForKey(EOCorps.C_CORPS_KEY));
			mesQualifiers.addObject(qualif);
		} else if (displayGroup().selectedObjects().count() > 0){
			qualif = construireOrQualifier(EOContratHeberges.TO_CORPS_KEY+"."+EOCorps.C_TYPE_CORPS_KEY,(NSArray)displayGroup().selectedObjects().valueForKey(INomenclature.CODE_KEY));
			mesQualifiers.addObject(qualif);
		}

		return mesQualifiers;
		
	}

	private EOQualifier recupererQualifierIndice() {
		if (typeIndice() != null && typeIndice().equals(TYPE_INDICE_EFFECTIF)) {
			return EOQualifier.qualifierWithQualifierFormat("inmEffectif = %@", new NSArray(new Integer(indice())));
		} else {
			NSArray passagesEchelon = EOPassageEchelon.rechercherPassageEchelonPourIndice(editingContext(),debutPeriode(), indice(),typeIndice().equals(TYPE_INDICE_MAJORE));
			if (passagesEchelon.count() == 0) {
				return null;
			}
			java.util.Enumeration e = passagesEchelon.objectEnumerator();
			NSMutableArray qualifiers = new NSMutableArray();
			while (e.hasMoreElements()) {
				EOPassageEchelon passage = (EOPassageEchelon)e.nextElement();
				NSMutableArray args = new NSMutableArray(passage.cGrade());
				args.addObject(passage.cEchelon());
				EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("toGrade.cGrade = %@ AND cEchelon = %@", args);
				qualifiers.addObject(qualifier);
			}	
			return new EOOrQualifier(qualifiers);
		}
	}
	protected void refreshAssociations() {
		java.util.Enumeration e = controllerDisplayGroup().observingAssociations().objectEnumerator();
		while (e.hasMoreElements()) {
			EOAssociation association = (EOAssociation)e.nextElement();
			association.subjectChanged();
		}
	}
}

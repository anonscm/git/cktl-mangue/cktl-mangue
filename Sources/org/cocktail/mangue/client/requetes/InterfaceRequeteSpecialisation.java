/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.client.outils_interface.ModelePageRequete;
import org.cocktail.mangue.client.select.NomenclatureSelectCodeLibelleCtrl;
import org.cocktail.mangue.client.select.specialisations.ReferensEmploisSelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

public class InterfaceRequeteSpecialisation extends ModelePageRequete {
	private EOCnu currentCnu;
	private EODiscSecondDegre currentDiscSecondDegre;
	private EOSpecialiteAtos currentSpecAtos;
	private EOSpecialiteItarf currentSpecItarf;
	private EOBap currentBap;
	private EOReferensEmplois currentReferensEmploi;
	
	public EOBap currentBap() {
		return currentBap;
	}
	public void setCurrentBap(EOBap currentBap) {
		this.currentBap = currentBap;
	}
	public EOReferensEmplois currentReferensEmploi() {
		return currentReferensEmploi;
	}
	public void setCurrentReferensEmploi(EOReferensEmplois currentReferensEmploi) {
		this.currentReferensEmploi = currentReferensEmploi;
	}
	public EOCnu currentCnu() {
		return currentCnu;
	}
	public void setCurrentCnu(EOCnu currentCnu) {
		this.currentCnu = currentCnu;
	}
	public EODiscSecondDegre currentDiscSecondDegre() {
		return currentDiscSecondDegre;
	}
	public void setCurrentDiscSecondDegre(EODiscSecondDegre currentDiscSeconDeg) {
		this.currentDiscSecondDegre = currentDiscSeconDeg;
	}
	public EOSpecialiteAtos currentSpecAtos() {
		return currentSpecAtos;
	}
	public void setCurrentSpecAtos(EOSpecialiteAtos currentSpecAtos) {
		this.currentSpecAtos = currentSpecAtos;
	}
	public EOSpecialiteItarf currentSpecItarf() {
		return currentSpecItarf;
	}
	public void setCurrentSpecItarf(EOSpecialiteItarf currentSpecItarf) {
		this.currentSpecItarf = currentSpecItarf;
	}
	public void init(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		EOArchive.loadArchiveNamed("InterfaceRequeteSpecialisation",this,"org.cocktail.mangue.client.requetes.interfaces",this.disposableRegistry());
	}	
	public EOQualifier qualifierRecherche() {
		return qualifierElementCarriere();
	}
	public EOQualifier qualifierElementCarriere() {
		return qualifierCarriere();
	}
	public EOQualifier qualifierContrat() {
		NSMutableArray qualifiers = new NSMutableArray();
		EOQualifier qualifier = qualifierCommun(false);
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}
		if (currentReferensEmploi() != null) {
            qualifier = EOQualifier.qualifierWithQualifierFormat(EOContratAvenant.TO_REFERENS_EMPLOI_KEY + "=%@",new NSArray(currentReferensEmploi()));
			qualifiers.addObject(qualifier);
		}
		return new EOAndQualifier(qualifiers);
	}
	public EOQualifier qualifierCarriere() {
		return qualifierCommun(true);
	}
	// actions
	public void afficherCnu() {
		EOCnu spec = (EOCnu)NomenclatureSelectCodeLibelleCtrl.sharedInstance(editingContext()).getObject(
				NomenclatureFinder.find(editingContext(), EOCnu.ENTITY_NAME, Nomenclature.SORT_ARRAY_CODE));
		if (spec != null)
			stockerValeur(spec,"currentCnu");
	}
	public void supprimerCnu() {
		currentCnu = null;
		updaterDisplayGroups();
	}
	public void afficherDiscSecondDegre() {
		EODiscSecondDegre spec = (EODiscSecondDegre)NomenclatureSelectCodeLibelleCtrl.sharedInstance(editingContext()).getObject(NomenclatureFinder.findStatic(editingContext(), EODiscSecondDegre.ENTITY_NAME));
		if (spec  != null)
			stockerValeur(spec,"currentDiscSecondDegre");
	}
	public void supprimerDiscSecondDegre() {
		currentDiscSecondDegre = null;
		updaterDisplayGroups();
	}
	public void afficherSpecialiteAtos() {
		UtilitairesDialogue.afficherDialogue(this,EOSpecialiteAtos.ENTITY_NAME,"getSpecialiteAtos",false,null,false);
	}
	public void supprimerSpecialiteAtos() {
		currentSpecAtos = null;
		updaterDisplayGroups();
	}
	public void afficherBap() {
		UtilitairesDialogue.afficherDialogue(this,"Bap","getBap",false,null,false);
	}
	public void supprimerBap() {
		currentBap = null;
		currentSpecItarf = null;
		updaterDisplayGroups();
	}
	public void afficherReferensEmploi() {
		EOReferensEmplois spec = (EOReferensEmplois)ReferensEmploisSelectCtrl.sharedInstance(editingContext()).getObject(DateCtrl.today());
		if (spec != null) 
			stockerValeur(spec, "currentReferensEmploi");
	}
	public void supprimerReferensEmploi() {
		currentReferensEmploi = null;
		updaterDisplayGroups();
	}	
	public void afficherSpecialiteItarf() {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOSpecialiteItarf.TO_BAP_KEY+"."+ INomenclature.CODE_KEY + " = %@", new NSArray(currentBap().code()));
		UtilitairesDialogue.afficherDialogue(this,EOSpecialiteItarf.ENTITY_NAME,"getSpecialiteItarf",false,qualifier,false);
	}
	public void supprimerSpecialiteItarf() {
		currentSpecItarf = null;
		updaterDisplayGroups();
	}
	public void getDiscSecondDegre(NSNotification aNotif) {
		stockerValeur(aNotif,"currentDiscSecondDegre");
	}
	public void getSpecialiteAtos(NSNotification aNotif) {
		stockerValeur(aNotif,"currentSpecAtos");
	}
	public void getBap(NSNotification aNotif) {
		stockerValeur(aNotif,"currentBap");
	}
	public void getSpecialiteItarf(NSNotification aNotif) {
		stockerValeur(aNotif,"currentSpecItarf");
	}
	public void remettreAZero(NSNotification aNotif) {
		effacer();
	}
	// Controller DG
	public boolean peutSupprimerCnu() {
		return currentCnu() != null;
	}
	public boolean peutSupprimerDiscSecondDegre() {
		return currentDiscSecondDegre() != null;
	}
	public boolean peutSupprimerSpecialiteAtos() {
		return currentSpecAtos() != null;
	}
	public boolean peutSupprimerBap() {
		return currentBap() != null;
	}
	public boolean peutSupprimerReferensEmploi() {
		return currentReferensEmploi() != null;
	}
	public boolean peutSupprimerSpecialiteItarf() {
		return currentSpecItarf() != null;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("remettreAZero", new Class[] {NSNotification.class}),
				GestionRequetesAgent.RAZ_CHAMPS,null);
	}
	protected void effacerChamps() {
		currentCnu = null;
		currentDiscSecondDegre = null;
		currentSpecAtos = null;
		currentSpecItarf = null;
		currentBap = null;
		currentReferensEmploi = null;
		updaterDisplayGroups();
	}
	protected NSArray fetcherObjets() {
		// pas de liste gérée
		return null;
	}
	protected void parametrerDisplayGroup() {}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOQualifier qualifierCommun(boolean estCarriere) {
		NSMutableArray args = new NSMutableArray();
		String stringQualifier = "";
		String prefixe = "";
		if (estCarriere) {
            prefixe = EOElementCarriere.TO_CARRIERE_KEY + "." + EOCarriere.SPECIALISATIONS_KEY+".";
		}
		if (currentCnu() != null) {
			args.addObject(currentCnu().code());
            stringQualifier = stringQualifier + prefixe + EOCarriereSpecialisations.TO_CNU_KEY + "." + INomenclature.CODE_KEY + " =  %@ OR ";
		}
		if (currentDiscSecondDegre() != null) {
			args.addObject(currentDiscSecondDegre().code());
            stringQualifier = stringQualifier + prefixe + EOCarriereSpecialisations.TO_DISC_SECOND_DEGRE_KEY + "." + INomenclature.CODE_KEY  + " = %@ OR ";
		}
		if (currentSpecAtos() != null) {
			args.addObject(currentSpecAtos().code());
            stringQualifier = stringQualifier + prefixe + EOCarriereSpecialisations.TO_SPECIALITE_ATOS_KEY + "." + INomenclature.CODE_KEY + " =  %@ OR ";
		}
		if (currentBap() != null) {
			args.addObject(currentBap().code());
            stringQualifier = stringQualifier + prefixe + EOCarriereSpecialisations.TO_BAP_KEY + "." + INomenclature.CODE_KEY  + " =  %@ OR ";
		}
		if (currentSpecItarf() != null) {
			args.addObject(currentSpecItarf().code());
            stringQualifier = stringQualifier + prefixe + EOCarriereSpecialisations.TO_SPECIALITE_ITARF_KEY + "." + INomenclature.CODE_KEY + " like %@ OR ";
		}
		if (currentReferensEmploi() != null) {
			args.addObject(currentReferensEmploi());
            stringQualifier = stringQualifier + prefixe + EOCarriereSpecialisations.TO_REFERENS_EMPLOI_KEY + " = %@ OR ";
		}
		stringQualifier = SuperFinder.nettoyerQualifier(stringQualifier," OR ");
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
	}


}

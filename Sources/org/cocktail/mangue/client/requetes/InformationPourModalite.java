/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** D&eacute;crit les informations affich&eacute;es dans l'onglet "Carri&egrave;re" de l'&eacute;diteur de requ&ecirc;tes pour les autres infos */
public class InformationPourModalite {
	private String nomEntite;
	private String libelle;
	private static String libelles[] = {"CFA","Congé Bonifié","MAD","MTT","Prolongation d'activité","Recul d'âge","Stagiaire"};
	private static String codes[] = {"CFA","CNBI","MAD","MTT","PROLONG","RECAG","STAG"};
	private static String entites[] = {"CgFinActivite","CongeBonifie","Mad","MiTpsTherap","ProlongationActivite","ReculAge","Stage"};
	
	// constructeur
	public InformationPourModalite(String libelle,String nomEntite) {
		this.libelle = libelle;
		this.nomEntite = nomEntite;
	}
	// accesseurs
	public String libelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String nomEntite() {
		return nomEntite;
	}
	public void setNomEntite(String nomEntite) {
		this.nomEntite = nomEntite;
	}
	/** retourne le qualifier pour ce type d'info */
	public EOQualifier qualifierRecherche(NSTimestamp dateDebut,NSTimestamp dateFin) {
		return SuperFinder.qualifierPourPeriode("dateDebut",dateDebut,"dateFin",dateFin);
	}
	// méthode d'initialisation
	public static NSArray informations() {
		NSMutableArray infos = new NSMutableArray(libelles.length);
		for (int i = 0; i < libelles.length;i++) {
			InformationPourModalite info = new InformationPourModalite(libelles[i],entites[i]);
			infos.addObject(info);
		}
		return infos;
	}
	/** retourne la liste des entit&eacute;s trait&eacute;es */
	public static NSArray entites() {
		return new NSArray(entites);
	}
	/** retourne le code associ&eacute; &agrave, une entit&eacute; trait&eacute;e */
	public static String codePourEntite(String nomEntite) {
		for (int i = 0; i < entites.length;i++) {
			if (entites[i].equals(nomEntite)) {
				return codes[i];
			}
		}
		return null;
	}
}

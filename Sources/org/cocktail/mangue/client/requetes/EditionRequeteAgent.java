/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.math.BigDecimal;

import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.individu.GestionEtatCivil;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.FicheAnciennete;
import org.cocktail.mangue.modele.IndividuPourEdition;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.QualifierPourRequete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.mangue.impression.EOModuleImpressionIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOArrivee;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Affiche le resultat des requetes sur les individus et permet l'export/impression des resultats.<BR>
 * Les noms des fichiers Jasper utilises pour la generation du pdf sont nommes de la façon suivante :
 * 		EditionRequeteIndividu1, EditionRequeteIndividu2, le chiffre indiquant le numero d'item dans le popup de selection des editions
 **/ 

public class EditionRequeteAgent extends PageRequeteAgent {
	public EODisplayGroup displayGroupModulesImpression;
	private NSArray tableauRequetes, individusHandicapes, individusPourPremiereAffectation;
	private EOQualifier autreQualifierContrat;
	private NSMutableArray individusEdition;
	private NSTimestamp debutPeriode,finPeriode;
	private boolean selectionModifiee;
	private String titreEdition;
	private int nbAgents;
	private int typeEnseignant;
	private static int numRequeteImpression = 0;
	private final static String FICHE_IDENTITE = "FicheIdentiteIndividu";

	// constructeur
	public EditionRequeteAgent(NSArray tableauRequetes,EOQualifier autreQualifierContrat,NSDictionary donneesATraiter,NSTimestamp debutPeriode,NSTimestamp finPeriode,int typeEnseignant) {

		super(donneesATraiter);
		this.tableauRequetes = tableauRequetes;
		this.autreQualifierContrat = autreQualifierContrat;
		this.debutPeriode = debutPeriode;
		this.finPeriode = finPeriode;
		this.typeEnseignant = typeEnseignant;
	}
	// accesseurs
	public String selectedTitle() {
		if (currentModule() == null) {
			return null;
		} else {
			return currentModule().intitulePourMenu();
		}
	}
	public void setSelectedTitle(String titre) {
		if (titre == null) {
			displayGroupModulesImpression.selectObject(null);
		} else {

			for (java.util.Enumeration<EOModuleImpressionIndividu> e = displayGroupModulesImpression.displayedObjects().objectEnumerator();e.hasMoreElements();) {
				EOModuleImpressionIndividu module = e.nextElement();
				if (module.intitulePourMenu().equals(titre)) {
					displayGroupModulesImpression.selectObject(module);
					break;
				}
			}
		}
	}
	public String titreEdition() {
		return titreEdition;
	}
	public void setTitreEdition(String titreEdition) {
		if (titreEdition == null) {
			this.titreEdition = "";		// pour ne pas avoir de problème lors de l'impression
		} else {
			this.titreEdition = titreEdition;
		}
	}
	public NSTimestamp debutPeriode() {
		return debutPeriode;
	}
	public NSTimestamp finPeriode() {
		return finPeriode;
	}
	public int nbAgents() {
		return nbAgents;
	}
	// actions
	public void annuler() {
		LogManager.logDetail("GestionRequetesAgent - annuler");
		displayGroup().setObjectArray(individusEdition);
		displayGroup().updateDisplayedObjects();
		selectionModifiee = false;
		controllerDisplayGroup().redisplay();
	}
	public void supprimer() {
		LogManager.logDetail("GestionRequetesAgent - supprimer");
		NSMutableArray agents = new NSMutableArray(displayGroup().displayedObjects());
		agents.removeObjectsInArray(displayGroup().selectedObjects());
		displayGroup().setObjectArray(agents);
		displayGroup().updateDisplayedObjects();
		selectionModifiee = true;
		controllerDisplayGroup().redisplay();
	}
	public void restreindre() {
		LogManager.logDetail("GestionRequetesAgent - restreindre");
		displayGroup().setObjectArray(displayGroup().selectedObjects());
		displayGroup().updateDisplayedObjects();
		selectionModifiee = true;
		controllerDisplayGroup().redisplay();
	}
	public void exporter() {
		LogManager.logDetail("GestionRequetesAgent - exporter");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			String texte = headerExport();
			for (java.util.Enumeration<IndividuPourEdition> e = displayGroup().displayedObjects().objectEnumerator();e.hasMoreElements();) {
				IndividuPourEdition agent = e.nextElement();
				texte = texte + texteExport(agent);
			}
			UtilitairesImpression.afficherFichierExport(texte,file.getParent(),file.getName());
		}
	}
	public void imprimer() {
		LogManager.logDetail("GestionRequetesAgent - imprimer");
		numRequeteImpression++;
		String postfixe = DateCtrl.dateToStringSansDelim(new NSTimestamp()) + "_" + numRequeteImpression;
		InfoPourEditionRequete infoEdition = new InfoPourEditionRequete(currentModule().nom(),titreEdition(),debutPeriode(),finPeriode());
		Class[] classeParametres = new Class[] {NSArray.class,InfoPourEditionRequete.class};
		Object[] parametres = new Object[]{displayGroup().displayedObjects(),infoEdition};

		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerResultatRequeteIndividu",classeParametres,parametres,"ImpressionRequete" + postfixe,"Impression des résultats de la requête");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}

	}
	// méthodes du controller DG
	public boolean selectionModifiee() {
		return selectionModifiee;
	}
	public boolean peutImprimer() {
		return displayGroup().displayedObjects().count() > 0 && currentModule() != null;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		listeAffichage.table().addMouseListener(new DoubleClickListener());
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		titreEdition = "Edition des agents - Période du " + DateCtrl.dateToString(debutPeriode());
		if (finPeriode() != null) {
			titreEdition += " au " + DateCtrl.dateToString(finPeriode());
		}
	}
	protected void terminer() {
	}

	/** Retourne la liste des IndividusPourEdition pour la periode en fonction des criteres de recherche demandes :
	 * <UL>
	 * <LI> les personnels non affectes,</LI>
	 * <LI> les individus ayant des affectations liees pour les positions de carrieres en activite ou les contrats,</LI>
	 * <LI> les individus ayant les positions d'inactivite ou les modalites de services</LI>
	 * </UL>
	 * On retourne toutes les informations demandees avec les regles suivantes:<BR>
	 * On retourne une entree pour chaque element de carriere ou avenant pendant la periode<BR>
	 * Si uniquement l'affichage des affectations (resp. occupations) est demande, on retourne la liste des affectations (resp. occupations)
	 * sur la periode.
	 **/
	protected NSArray fetcherObjets() {
		NSMutableArray modulesImpression = new NSMutableArray(SuperFinder.rechercherEntiteAvecRefresh(editingContext(),"ModuleImpressionIndividu"));
		EOSortOrdering.sortArrayUsingKeyOrderArray(modulesImpression, new NSArray(EOSortOrdering.sortOrderingWithKey("noOrdreMenu",EOSortOrdering.CompareAscending)));
		EOModuleImpressionIndividu lastModule = (EOModuleImpressionIndividu)modulesImpression.lastObject();
		EOModuleImpressionIndividu module = new EOModuleImpressionIndividu();
		if (lastModule == null) {
			module.initAvecPosition(1);
		} else {
			module.initAvecPosition(lastModule.noOrdreMenu().intValue() + 1);
		}
		module.setNom(FICHE_IDENTITE);
		module.setTexteMenu("Fiche Identité de l'Individu");
		modulesImpression.addObject(module);
		displayGroupModulesImpression.setObjectArray(modulesImpression);
		preparerIndividusHandicapes();
		preparerPourPremieresAffectations();
		NSMutableArray individusTraites = new NSMutableArray();
		individusEdition = new NSMutableArray();
		// vérifier si il y a un qualifier pour les personnels i.e on recherche les non affectés
		if (qualifierPourEntite("Personnel") != null) {
			individusEdition = preparerIndividusNonAffectes();
		} else {
			// on recherche les affectations
			// on ne traitera que les agents qui sont lies à une affectation pour les positions de carriere en activite et les contrats
			// pour les positions qui ne sont pas en activite ou les modalites de service, on traite tous les agents 
			// On prendra aussi eventuellement les occupations si ce critere est demande

			NSArray<EOAffectation> affectations = fetcher(EOAffectation.ENTITY_NAME);
			NSArray<EOOccupation> occupations = fetcher(EOOccupation.ENTITY_NAME);

			if (affectations != null && affectations.count() > 0) {
				if (qualifierPourEntite(EOContratAvenant.ENTITY_NAME) != null) {
					individusEdition.addObjectsFromArray(preparerContrats(individusTraites,affectations,occupations));
				}
				if (qualifierPourEntite(EOContratHeberges.ENTITY_NAME) != null) {
					individusEdition.addObjectsFromArray(preparerContratsHeberges(individusTraites,affectations));
				}
			} else if (qualifierPourEntite(EOContratHeberges.ENTITY_NAME) != null) {	// 23/09/09
				individusEdition.addObjectsFromArray(preparerContratsHeberges(individusTraites,null));
			}
			if (qualifierPourEntite(EOElementCarriere.ENTITY_NAME) == null) { 	// autres infos demandées ou uniquement contractuels
				if (qualifierPourEntite(EOPosition.ENTITY_NAME) != null) {
					individusEdition.addObjectsFromArray(preparerCarrieresNonActives(individusTraites));
				} else {
					// Il s'agit des autres positions
					individusEdition.addObjectsFromArray(preparerAutresInfosCarriere(individusTraites));
				}
			} else {
				if (affectations != null && affectations.count() > 0) {
					individusEdition.addObjectsFromArray(preparerCarrieresActives(individusTraites,affectations,occupations));
				} else if (qualifierPourEntite("Affectation") == null) {	
					// on ne préparer les informations sur les carrières inactives que si on n'a pas choisi d'affectation
					individusEdition.addObjectsFromArray(preparerCarrieresNonActives(individusTraites));
				}
			}
		}
		calculerNbAgents(individusEdition);
		return individusEdition;
	}
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("libelleCourt",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("nom",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("prenom",EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
		displayGroupModulesImpression.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("noOrdreMenu",EOSortOrdering.CompareAscending)));
	}

	// méthodes privées
	private EOModuleImpressionIndividu currentModule() {
		return (EOModuleImpressionIndividu)displayGroupModulesImpression.selectedObject();
	}
	private EOQualifier qualifierPourEntite(String nomEntite) {
		for (java.util.Enumeration<QualifierPourRequete> e = tableauRequetes.objectEnumerator();e.hasMoreElements();) {
			QualifierPourRequete qualif = e.nextElement();
			if (qualif.nomEntite().equals(nomEntite)) {
				return qualif.qualifier();

			}
		}
		return null;
	}
	private NSArray fetcher(String nomEntite,String clePourOrdre,NSSelector ordreTri) {
		EOQualifier qualifier = qualifierPourEntite(nomEntite);
		LogManager.logDetail("Fetch de " + nomEntite + " avec le qualifier " + qualifier);
		if (qualifier != null) {
			NSArray sorts = null;
			if (clePourOrdre != null) {
				sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(clePourOrdre,ordreTri));
			}
			EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier,sorts);
			fs.setRefreshesRefetchedObjects(true);
			return editingContext().objectsWithFetchSpecification(fs);
		} else {
			return null;
		}
	}
	private NSArray fetcher(String nomEntite) {
		return fetcher(nomEntite,null,null);
	}

	private NSArray rechercherAvenants() {

		// dans le cas ou il s'agit des enseignants (ou non-enseignants), qualifie sur le grade 
		NSMutableArray<EOContratAvenant> avenants = new NSMutableArray(fetcher(EOContratAvenant.ENTITY_NAME,EOContratAvenant.DATE_DEBUT_KEY,EOSortOrdering.CompareDescending));		
		// Si l'autre qualifier n'est pas nul, ajouter aussi les agents concernés (pour trouver tous les enseignants ou non-enseignants)
		if ((typeEnseignant == GestionRequetesAgent.ENSEIGNANT || typeEnseignant == GestionRequetesAgent.NON_ENSEIGNANT) && autreQualifierContrat != null) {

			// dans le cas où il s'agit des enseignants (ou non-enseignants), qualifie sur le type de contrat de travail
			LogManager.logDetail("Fetch des avenants avec le qualifier " + autreQualifierContrat);			
			EOFetchSpecification fs = new EOFetchSpecification(EOContratAvenant.ENTITY_NAME,autreQualifierContrat,null);
			fs.setRefreshesRefetchedObjects(true);
			NSArray<EOContratAvenant> result = editingContext().objectsWithFetchSpecification(fs);
			for (EOContratAvenant myAvenant : result) {

				if (avenants.containsObject(myAvenant) == false) {

					boolean peutAjouter = myAvenant.toGrade() == null || myAvenant.toGrade().toCorps().toTypePopulation() == null;

					if (!peutAjouter) {
						if (typeEnseignant == GestionRequetesAgent.ENSEIGNANT && myAvenant.toGrade().toCorps().toTypePopulation().estEnseignant()) {
							peutAjouter = true;
						}
						if (typeEnseignant == GestionRequetesAgent.NON_ENSEIGNANT && myAvenant.toGrade().toCorps().toTypePopulation().estEnseignant() == false) {
							peutAjouter = true;
						}
					}
					if (peutAjouter) {
						avenants.addObject(myAvenant);
					}
				}
			}
			EOSortOrdering.sortArrayUsingKeyOrderArray(avenants,new NSArray(EOSortOrdering.sortOrderingWithKey(EOContratAvenant.DATE_DEBUT_KEY,EOSortOrdering.CompareDescending)));
		} 

		return avenants;
	}

	private NSMutableArray preparerIndividusNonAffectes() {
		EOQualifier qualifier = qualifierPourEntite(EOPersonnel.ENTITY_NAME);
		// Rechercher les personnels
		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(EOPersonnel.NO_DOSSIER_PERS_KEY,EOSortOrdering.CompareAscending));
		EOFetchSpecification fs = new EOFetchSpecification(EOPersonnel.ENTITY_NAME,qualifier,sorts);
		fs.setRefreshesRefetchedObjects(true);
		NSArray personnels = editingContext().objectsWithFetchSpecification(fs);
		NSArray identites = (NSArray)personnels.valueForKey(EOPersonnel.NO_DOSSIER_PERS_KEY);
		// pour optimiser on fait directement un fetch des individus trouvés
		if (identites.count() > 0) {
			NSMutableArray qualifiers = new NSMutableArray();
			for (java.util.Enumeration e = identites.objectEnumerator();e.hasMoreElements();) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividu.NO_INDIVIDU_KEY + "= %@", new NSArray(e.nextElement())));
			}
			fs = new EOFetchSpecification(EOIndividu.ENTITY_NAME, new EOOrQualifier(qualifiers),null);
			NSArray individus = editingContext().objectsWithFetchSpecification(fs);
			// restreindre aux personnels non affectés
			individus = EOIndividu.restreindrePersonnelsNonAffectes(editingContext(),individus);
			qualifier = qualifierPourEntite(EOContrat.ENTITY_NAME);
			// présence d'un qualifier de contrat
			if (qualifier != null) {
				sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(EOPersonnel.NO_DOSSIER_PERS_KEY,EOSortOrdering.CompareAscending));
				fs = new EOFetchSpecification(EOContrat.ENTITY_NAME,qualifier,sorts);
				NSArray individusVacataires = (NSArray)editingContext().objectsWithFetchSpecification(fs).valueForKey(EOVacataires.TO_INDIVIDU_KEY);
				individus = EOIndividu.restreindreNonVacataires(editingContext(), individus, individusVacataires);
			}
			qualifier = qualifierPourEntite(EOContratHeberges.ENTITY_NAME);
			if (qualifier != null) {
				sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(EOPersonnel.NO_DOSSIER_PERS_KEY,EOSortOrdering.CompareAscending));
				fs = new EOFetchSpecification(EOContratHeberges.ENTITY_NAME,qualifier,sorts);
				NSArray individusHeberges = (NSArray)editingContext().objectsWithFetchSpecification(fs).valueForKey(EOContratHeberges.INDIVIDU_KEY);
				individus = restreindreNonHeberges(individus,individusHeberges);
			}
			NSMutableArray agents = new NSMutableArray(),individusTraites = new NSMutableArray();
			for (java.util.Enumeration<EOIndividu> e1 = individus.objectEnumerator();e1.hasMoreElements();) {
				EOIndividu individu = e1.nextElement();
				IndividuPourEdition agent = ajouterAgent(agents,individusTraites,individu,null,null);
				if (agent != null)  {
					preparerAutresDonneesPourIndividu(agents,agent,individu,null,null);
				}
			}
			return agents;
		} else {
			return new NSMutableArray();
		}

	}

	
	private NSArray preparerContrats(NSMutableArray individusTraites,NSArray affectations,NSArray occupations) {
		// Il y a des contrats ou des vacations ou rien si on ne veut que les titulaires
		NSMutableArray agents = new NSMutableArray();

		NSArray avenants = rechercherAvenants();

		if (avenants != null && avenants.count() > 0) {

			for (java.util.Enumeration<EOContratAvenant> e = avenants.objectEnumerator();e.hasMoreElements();) {
				EOContratAvenant avenant = e.nextElement();
				IndividuPourEdition agent = ajouterAgent(agents,individusTraites,avenant.contrat().toIndividu(),affectations,occupations);
				if (agent != null) {
					if (avenant.toGrade() != null) {
						agent.setLlGrade(avenant.toGrade().llGrade());
					}
					// 28/03/2011 
					if (avenant.cEchelon() != null) {
						agent.setCEchelon(avenant.cEchelon());
					}
					if (recupererCarriere()) {
						agent.setContratAvenant(avenant,recupererSpecialisation());
					}
					preparerAutresDonneesPourIndividu(agents,agent,avenant.contrat().toIndividu(),avenant.contrat().dateDebut(),avenant.contrat().dateFin());
				}
			}
		}

		// Ajouter les contrats sans avenants
		NSArray contrats = fetcher(EOContrat.ENTITY_NAME, EOContrat.DATE_DEBUT_KEY,EOSortOrdering.CompareDescending);
		if (contrats != null && contrats.count() > 0) {
			for (java.util.Enumeration<EOContrat> e = contrats.objectEnumerator();e.hasMoreElements();) {
				EOContrat contrat = e.nextElement();
				IndividuPourEdition agent = ajouterAgent(agents,individusTraites,contrat.toIndividu(),affectations,occupations);
				if (agent != null) {
					preparerAutresDonneesPourIndividu(agents,agent,contrat.toIndividu(),contrat.dateDebut(),contrat.dateFin());
				}
			}
		}
		return agents;
	}


	// 23/09/02 - Gestion des heberges
	private NSArray preparerContratsHeberges(NSMutableArray individusTraites,NSArray affectations) {
		NSMutableArray agents = new NSMutableArray();
		NSArray contratsHeberges = fetcher(EOContratHeberges.ENTITY_NAME,"dateDebut",EOSortOrdering.CompareDescending);
		if (contratsHeberges != null && contratsHeberges.count() > 0) {

			for (java.util.Enumeration<EOContratHeberges> e = contratsHeberges.objectEnumerator();e.hasMoreElements();) {
				EOContratHeberges contratHeberge = e.nextElement();
				boolean peutAjouter = (typeEnseignant == GestionRequetesAgent.TOUS);

				if (!peutAjouter) {
					if (contratHeberge.typeContratTravail() != null) {
						peutAjouter = ((typeEnseignant == GestionRequetesAgent.ENSEIGNANT && contratHeberge.typeContratTravail().estEnseignant()) ||
								(typeEnseignant == GestionRequetesAgent.NON_ENSEIGNANT && contratHeberge.typeContratTravail().estEnseignant() == false));
					}
					if (contratHeberge.toCorps() != null && contratHeberge.toCorps().toTypePopulation() != null) {
						peutAjouter = ((typeEnseignant == GestionRequetesAgent.ENSEIGNANT && contratHeberge.toCorps().toTypePopulation().estEnseignant()) ||
								(typeEnseignant == GestionRequetesAgent.NON_ENSEIGNANT && contratHeberge.toCorps().toTypePopulation().estEnseignant() == false));
					}
				}
				if (peutAjouter) {
					IndividuPourEdition agent = ajouterAgent(agents,individusTraites,contratHeberge.individu(),affectations,null);
					if (agent != null) {
						if (contratHeberge.toGrade() != null) {
							agent.setLlGrade(contratHeberge.toGrade().llGrade());
						}
						preparerAutresDonneesPourIndividu(agents,agent,contratHeberge.individu(),contratHeberge.dateDebut(),contratHeberge.dateFin());
					}
				}
			}
		}

		return agents;


	}
	private NSArray preparerAutresInfosCarriere(NSMutableArray individusTraites) {
		NSMutableArray agents = new NSMutableArray();

		for (java.util.Enumeration<String> e = InformationPourModalite.entites().objectEnumerator();e.hasMoreElements();) {
			String nomEntite = e.nextElement();
			NSArray infos = fetcher(nomEntite,"dateDebut",EOSortOrdering.CompareDescending);
			if (infos != null && infos.count() > 0) {

				for (java.util.Enumeration<Duree> e1 = infos.objectEnumerator();e1.hasMoreElements();) {
					Duree info = e1.nextElement();
					IndividuPourEdition agent = ajouterAgent(agents,individusTraites,info.individu(),null,null);
					if (agent != null) {
						agent.setDuree(info,InformationPourModalite.codePourEntite(nomEntite));
						preparerAutresDonneesPourIndividu(agents,agent,info.individu(),info.dateDebut(),info.dateFin());
					}
				}
			}
		}
		return agents;
	}
	private NSArray preparerCarrieresActives(NSMutableArray individusTraites,NSArray affectations,NSArray occupations) {
		NSMutableArray agents = new NSMutableArray();
		NSArray<EOElementCarriere> elementsCarriere = fetcher(EOElementCarriere.ENTITY_NAME, EOElementCarriere.DATE_DEBUT_KEY,EOSortOrdering.CompareDescending);
		if (elementsCarriere != null && elementsCarriere.count() > 0) {
			// on ne traite que les positions qui sont demandées
			NSArray positionsActives = positions(true);

			for (EOElementCarriere element : elementsCarriere) {

				if (aPositionPourPeriode(element.toCarriere(),positionsActives)) {
					
					IndividuPourEdition agent = ajouterAgent(agents,individusTraites,element.toIndividu(),affectations,occupations);
					if (agent != null) {

						agent.setElementCarriere(element,recupererSpecialisation());
						
						preparerAutresDonneesPourIndividu(agents,agent,element.toIndividu(),element.dateDebut(),element.dateFin());
						if (recupererReduction()) {
							NSArray agentsAjoutes = agent.setAnciennetePourElement(element);
							if (agentsAjoutes.count() > 0) {
								agents.addObjectsFromArray(agentsAjoutes);
							}
						}
					}
				}
			}
		}
		return agents;
	}
	
	private NSArray preparerCarrieresNonActives(NSMutableArray individusTraites) {
		NSMutableArray agents = new NSMutableArray();
		NSArray carrieres = fetcher(EOCarriere.ENTITY_NAME,EOCarriere.DATE_DEBUT_KEY,EOSortOrdering.CompareDescending);
		if (carrieres != null && carrieres.count() > 0) {
			// on ne traite que les positions qui sont demandées
			NSArray positionsInactives = positions(false);
			for (java.util.Enumeration<EOCarriere> e = carrieres.objectEnumerator();e.hasMoreElements();) {
				EOCarriere carriere = e.nextElement();
				if (aPositionPourPeriode(carriere,positionsInactives)) {
					IndividuPourEdition agent = ajouterAgent(agents,individusTraites,carriere.toIndividu(),null,null);
					if (agent != null) {
						if (recupererCarriere()) {
							agent.setCarriere(carriere,true,recupererSpecialisation());
						}
						preparerAutresDonneesPourIndividu(agents,agent,carriere.toIndividu(),carriere.dateDebut(),carriere.dateFin());	
					}
				}
			}
		}
		return agents;
	}
	// On passe le tableau d'agents pour le cas où il faudrait en ajouter parce qu'il y a plusieurs occupations ou affectations
	private void preparerAutresDonneesPourIndividu(NSMutableArray agents,IndividuPourEdition agent,EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (recupererEtatCivil()) {
			agent.setIndividu(individu);
		}
		// Vérifier si la date de début est bien dans la période requise pour limiter à la période demandée
		if (debutPeriode() != null) {
			if (dateDebut == null || (DateCtrl.isBefore(dateDebut, debutPeriode()))) {
				dateDebut = debutPeriode();
			}
		}
		// Vérifier si la date de fin est bien dans la période requise pour limiter à la période demandée
		if (finPeriode() != null) {
			if (dateFin == null || DateCtrl.isAfter(dateFin, finPeriode())) {
				dateFin = finPeriode();
			}
		}
		// 04/11/2009 - DT 1924
		if (recupererAnciennete()) {
			NSArray fichesAnciennete = FicheAnciennete.calculerAnciennete(editingContext(), individu, finPeriode());
			agent.setAncienneteGenerale(FicheAnciennete.calculerTotal(fichesAnciennete, FicheAnciennete.TOUTE_ANCIENNETE));
			agent.setAncienneteService(FicheAnciennete.calculerTotal(fichesAnciennete, FicheAnciennete.ANCIENNETE_TITULAIRE));
		}
		if (recupererAdresse()) {
			NSArray reparts = EORepartPersonneAdresse.adressesPersoValides(editingContext(),individu);
			try {
				EOAdresse adresse = ((EORepartPersonneAdresse)reparts.objectAtIndex(0)).toAdresse();
				agent.setAdresse(adresse);
			} catch (Exception exc) {
				// pas d'adresse personnelle
			}
		}
		if (recupererEnfants()) {
			NSArray enfants = individu.enfants();
			if (enfants != null && enfants.count() > 0) {
				// Creer de nouvelles entrees pour les autres enfants
				for (int i = 1; i < enfants.count();i++) {
					IndividuPourEdition nouvelAgent = (IndividuPourEdition)agent.duplicate();
					nouvelAgent.setEnfant((EOEnfant)enfants.objectAtIndex(i));
					agents.addObject(nouvelAgent);
				}
				// Appelee apres pour ne pas dupliquer les informations d'enfants
				agent.setEnfant((EOEnfant)enfants.objectAtIndex(0));
			}
		}
		NSArray<EOOccupation> occupations = null;
		NSArray<EOAffectation> affectations = null;
		// EMPLOI
		if (recupererEmploi()) {
			occupations = EOOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContext(),individu,dateDebut,dateFin);
			occupations = EOSortOrdering.sortedArrayUsingKeyOrderArray(occupations, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		}
		if (recupererAffectation()) {
			affectations = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext(),individu,dateDebut,dateFin);
			affectations = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		}
		
		// Les temps de travail sont enregistres dans les modalites de service
		// Si pas de modalite ==> 100%
		EOModalitesService modalite = EOModalitesService.modalitePourDate(editingContext(), individu, dateFin);
		if (modalite != null)
			agent.setQuotiteTravail(modalite.quotite());
		else
			agent.setQuotiteTravail(ManGUEConstantes.QUOTITE_100);
			
		// Si on veut récupérer les infos de carrière ou les affectations et occupations alors 
		// on récupère la dernière affectation et le dernier emploi
		if (recupererCarriere() || (recupererAffectation() && recupererEmploi())) {

			try {
				agent.setOccupation(occupations.lastObject());
			} catch (Exception exc) {
				// pas d'occupation
			}

			try {
				for (EOAffectation myAffectation : affectations) {
					agent.setAffectation(myAffectation);
					if (myAffectation.estPrincipale())
						break;
				}
			}
			catch (Exception exc) {
				// pas d'affectation
			}

		} else {
			// On veut exclusivement l'un ou l'autre
			if (recupererAffectation()) {
				if (affectations == null || affectations.count() == 0) {
					// pas d'affectation à ajouter
					return;
				} else {
					// Créer de nouvelles entrées pour les autres affectations
					for (int i = 1; i < affectations.count();i++) {
						IndividuPourEdition nouvelAgent = (IndividuPourEdition)agent.duplicate();
						nouvelAgent.setAffectation((EOAffectation)affectations.objectAtIndex(i));
						agents.addObject(nouvelAgent);
					}
					// Appelée après pour ne pas dupliquer les informations d'affectation
					agent.setAffectation((EOAffectation)affectations.objectAtIndex(0));
				}
			} else {	// On veut les occupations
				if (occupations == null || occupations.count() == 0) {
					// pas d'occupation à ajouter
					return;
				} else {
					// Créer de nouvelles entrées pour les autres occupations
					for (int i = 1; i < occupations.count();i++) {
						IndividuPourEdition nouvelAgent = (IndividuPourEdition)agent.duplicate();
						nouvelAgent.setOccupation((EOOccupation)occupations.objectAtIndex(i));
						agents.addObject(nouvelAgent);
					}
					// Appelée après pour ne pas dupliquer les informations d'occupation
					agent.setOccupation((EOOccupation)occupations.objectAtIndex(0));
				}
			}
		}
	}
	private  NSArray restreindreNonHeberges(NSArray individus,NSArray individusHeberges) {
		NSMutableArray individusRetenus = new NSMutableArray();
		for (java.util.Enumeration<EOIndividu> e = individus.objectEnumerator();e.hasMoreElements();) {
			// pour chaque personne de la liste initiale
			// et qu'on ne l'a pas encore gardée, alors l'ajouter au résultat
			EOIndividu individu = e.nextElement();
			if (individusRetenus.containsObject(individu) == false && individusHeberges.containsObject(individu) == false) {
				// n'est pas hébergé, on l'ajoute
				individusRetenus.addObject(individu);	
			}
		}
		return new NSArray(individusRetenus);
	}
	private void preparerIndividusHandicapes() {
		EOQualifier qualifier = qualifierPourEntite("PeriodeHandicap");
		if (qualifier == null) {
			individusHandicapes =  null;
		} else {
			individusHandicapes = (NSArray)fetcher("PeriodeHandicap","individu.noIndividu",EOSortOrdering.CompareAscending).valueForKey("individu");
		}
	}
	private void preparerPourPremieresAffectations() {
		EOQualifier qualifier = qualifierPourEntite(EOArrivee.ENTITY_NAME);
		if (qualifier == null) {
			individusPourPremiereAffectation = null;
		} else {
			individusPourPremiereAffectation = (NSArray)fetcher(EOArrivee.ENTITY_NAME,"individu.noIndividu",EOSortOrdering.CompareAscending).valueForKey("individu");
		}
	}
	//	dans le cas où on veut ajouter l'agent en dehors de toute affectation, on passe null dans les affectations
	private IndividuPourEdition ajouterAgent(NSMutableArray agents,NSMutableArray individusTraites,EOIndividu individu,NSArray affectations,NSArray occupations) {
		// Si la date de 1ère affectation est fournie
		boolean ajouterIndividu = true;
		if (affectations != null && affectations.count() > 0) {
			// on ne crée un agent pour édition que si on recherche les affectations et que l'individu a une affectation
			NSArray individus = (NSArray)affectations.valueForKey("individu");
			ajouterIndividu = individus.containsObject(individu);
			if (ajouterIndividu) {
				// Regarder si on demande les occupations
				if (occupations != null) {
					// on a recherché des occupations mais il n'y en a pas
					if (occupations.count() > 0) {
						NSArray individusOccupes = (NSArray)occupations.valueForKey("individu");
						ajouterIndividu = individusOccupes.containsObject(individu);
					} else {
						ajouterIndividu = false;
					}
				}
			}
		}
		if (ajouterIndividu) {
			// on n'ajoute un individu que si on ne recherche pas les handicapés ou que l'individu en fait partie
			ajouterIndividu = individusHandicapes == null || individusHandicapes.containsObject(individu);
		}
		if (ajouterIndividu) {
			// on n'ajoute un individu que si on ne recherche pas sur la date de 1ère affectation ou que l'individu en fait partie
			ajouterIndividu = individusPourPremiereAffectation == null || individusPourPremiereAffectation.containsObject(individu);
		}
		if (ajouterIndividu && (recupererCarriere() || !individusTraites.containsObject(individu))) {
			// lorsqu'on récupère les informations de carrière, on ajoute autant de fois l'individu qu'il a d'éléments
			// de carrière ou de contrat pendant la période
			IndividuPourEdition agent = new IndividuPourEdition();
			agents.addObject(agent);
			individusTraites.addObject(individu);
			return agent;
		} else {
			return null;
		}
	}
	private NSArray positions(boolean positionsActives) {
		NSArray positions = fetcher(EOPosition.ENTITY_NAME);
		if (positions != null && positions.count() > 0) {
			NSMutableArray result = new NSMutableArray();
			for (java.util.Enumeration<EOPosition> e = positions.objectEnumerator();e.hasMoreElements();) {
				EOPosition position = e.nextElement();
				if ((positionsActives && position.estEnActivite()) || (!positionsActives && !position.estEnActivite())){
					result.addObject(position);
				}
			}
			return result;
		} else {
			return null;
		}
	}
	private boolean aPositionPourPeriode(EOCarriere carriere,NSArray positions) {
		if (positions == null) {
			return true;
		}
		NSArray changementsPosition = carriere.changementsPositionPourPeriode(debutPeriode(),finPeriode());
		if (changementsPosition == null || changementsPosition.count() == 0) {
			return false;
		}
		for (java.util.Enumeration<EOChangementPosition> e = changementsPosition.objectEnumerator();e.hasMoreElements();) {
			EOChangementPosition changement = e.nextElement();
			if (positions.containsObject(changement.toPosition())) {
				return true;
			}
		}
		return false;
	}

	//	Classe d'ecoute pour les doubleclics dans la table view  */
	public class DoubleClickListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,"Etat Civil");
				IndividuPourEdition agent = (IndividuPourEdition)displayGroup().selectedObject();
				NSMutableDictionary userInfo = new NSMutableDictionary();
				userInfo.setObjectForKey(agent.nom(),"nom");
				userInfo.setObjectForKey(agent.prenom(),"prenom");
				NSNotificationCenter.defaultCenter().postNotification(GestionEtatCivil.NOUVEL_EMPLOYE, null,userInfo);

			}
		}
	}
	
	
	/**
	 * 
	 * @return
	 */
	private String headerExport() {
		String entete = "";
		if (recupererEtatCivil()) {
			entete = CocktailExports.getEntete(new String[]{"Sexe", "Civ", "Nom", "Prenom", "Nom Patronymique", "Date Naissance", "Lieu Naissance", "Dept Naissance", "Pays Naissance", "Pays Nationalite"
					, "No INSEE", "No Individu", "Pers Id"},
					CocktailExports.SEPARATEUR_TABULATION);
		}
		if (recupererCarriere()) {
			entete = entete + "\t" + CocktailExports.getEntete(new String[]{"Date Début", "Date Fin", "Corps", "Grade", "Echelon", "Position", "Quotité", "Indice", "Statut", "Type contrat"}, 
					CocktailExports.SEPARATEUR_TABULATION);
		}
		if (recupererReduction()) {
			entete = entete + "\tReliquat Annee\tReliquat Annees\tReliquat Mois\tReliquat Jours\tConserv. Annee\tConserv. Annees\tConserv. Mois\tConserv. Jours";
		}
		if (recupererAffectation()) {
			entete = entete + "\tDebut Aff.\tFin Aff.\tAff. Niv 1\tAff. Niv 2\tAff. globale\tQuot Aff.";
		}
		if (recupererAnciennete()) {
			entete = entete + "\tAnc. Gen.\tAnc. Svc";
		}
		if (recupererEmploi()) {
			entete = entete + "\tDebut Occ.\tFin Occ.\tQuot Occ.\tQuot Fi\tEmploi\tImplantation\tChapitre\tFonction";
		}
		if (recupererAdresse()) {
			entete = entete + "\tAdresse\tCode Postal\tVille";
		}
		if (recupererEnfants()) {
			entete = entete + "\tEnfant\tDate Naissance";
		}
		if (recupererSpecialisation()) {
			entete = entete + "\tCode Spec\tSpécialisation";
		}
		entete += CocktailExports.SAUT_DE_LIGNE;
		return entete;

	}
	
	/**
	 * 
	 * @param agent
	 * @return
	 */
	private String texteExport(IndividuPourEdition agent) {
		
		String texte = "";
		if (recupererEtatCivil()) {
			if (agent.sexe() != null) {
				texte = agent.sexe();
			}
			// Rem Philou : il faut convertir la date en string au format %Y/%m/%d pour l'afficher sous Excel au format %d/%m/%Y
			texte = texte + "\t" + agent.cCivilite() + "\t" + agent.nom() + "\t" + agent.prenom() + 
			"\t" + agent.nomPatronymique() + "\t" + DateCtrl.dateToString(agent.dNaissance(),"%Y/%m/%d") +
			"\t" + agent.lieuNaissance() + "\t" + agent.dptNaissance() + "\t" + agent.paysNaissance() +
			"\t" + agent.paysNationalite() +
			"\t" + agent.inseeComplet() + "\t" + agent.noIndividu() + "\t" + agent.persId();
		}
		if (recupererCarriere()) {
			texte = texte + "\t" + DateCtrl.dateToString(agent.debut(),"%Y/%m/%d") + "\t";
			if (agent.fin() != null) {
				texte += DateCtrl.dateToString(agent.fin(),"%Y/%m/%d");
			}
			
			texte += "\t" + agent.lcCorps() + "\t" + agent.lcGrade() + "\t" + agent.cEchelon() + "\t" + agent.cPosition() + "\t" + agent.quotiteTravail() + "\t";
			
			if (agent.indiceMajore() != null) {
				texte += agent.indiceMajore()+ "\t";
			}
			texte += agent.statut() + "\t" + agent.lcTypeContratTrav();
		}
		if (recupererReduction()) {
			texte += "\t" + agent.ancienneteAnnee() + "\t" + agent.ancienneteNbAnnees() + "\t" + agent.ancienneteNbMois() + "\t" + agent.ancienneteNBJours() + "\t" + 
			agent.conservationAnnee() + "\t" + agent.conservationNbAnnees() + "\t" + agent.conservationNbMois() + "\t" + agent.conservationNbJours()+ "\t";
		}
		if (recupererAffectation()) {
			texte += "\t" + DateCtrl.dateToString(agent.debutAffectation(),"%Y/%m/%d");
			texte += "\t";
			if (agent.finAffectation() != null) {
				texte = texte + DateCtrl.dateToString(agent.finAffectation(),"%Y/%m/%d");
			} 
			texte = texte + "\t" + agent.llComposante() + "\t" + agent.llStructure() + "\t" + agent.llStructureComplet() +"\t" + agent.quotite() ;
		}

		if (recupererAnciennete()) {
			texte = texte + "\t" + agent.ancienneteGenerale() + "\t" + agent.ancienneteService();
		}
		if (recupererEmploi()) {
			texte = texte + "\t" + DateCtrl.dateToString(agent.debutOccupation(),"%Y/%m/%d");
			texte = texte +  "\t";
			if (agent.finOccupation() != null) {
				texte = texte + DateCtrl.dateToString(agent.finOccupation(),"%Y/%m/%d");
			} 
			if (agent.quotiteFinanciere() != null) {
				texte = texte + "\t" + agent.quotiteOccupation()  + "\t" + agent.quotiteFinanciere() + "\t" + agent.numeroEmploi() + "\t" + agent.implantation() + "\t" + agent.chapitre();
			} else {
				texte = texte + "\t\t" + agent.numeroEmploi() + "\t" + agent.implantation() + "\t" + agent.chapitre();
			}
			texte += "\t";
			if (agent.fonction() != null) {
				texte += agent.fonction();
			}
		}
		if (recupererAdresse()) {
			texte = texte + "\t" + agent.adressePerso() + "\t" + agent.codePostal() + "\t" + agent.ville() ;
		}
		if (recupererEnfants()) {
			texte = texte + "\t" + agent.nomEnfant() + "\t";
			if (agent.dateNaissanceEnfant() != null) {
				texte = texte + DateCtrl.dateToString(agent.dateNaissanceEnfant(),"%Y/%m/%d") ;
			}
		}
		if (recupererSpecialisation()) {
			texte = texte + "\t" + agent.codeSpec() + "\t" + agent.specialisation();
		}
		texte = texte + "\n";
		return texte;
	}
	private void calculerNbAgents(NSArray individus) {
		nbAgents = 0;
		if (individus.count() == 0) {
			return;
		}
		NSMutableArray individusVus = new NSMutableArray();

		for (java.util.Enumeration<IndividuPourEdition> e = individus.objectEnumerator();e.hasMoreElements();) {
			IndividuPourEdition individu = e.nextElement();
			if (individusVus.containsObject(individu.noIndividu()) == false) {
				nbAgents++;
				individusVus.addObject(individu.noIndividu());
			}
		}
	}

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.individu.GestionEtatCivil;
import org.cocktail.mangue.client.outils_interface.GestionTypeConge;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.EvenementPourEdition;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

// 03/01/2011 - Adaptation Netbeans
public class GestionRequetesConge extends PageRequeteEvenement {
	public EOView vueTypeConge;
	private GestionTypeConge controleurTypeConge;
	private int typeStatut,typePersonnel;
	private static int TOUS = 0;
	private static int TITULAIRE = 1, CONTRACTUEL = 2;
	private static int ENSEIGNANT = 1, NON_ENSEIGNANT = 2;

	// accesseurs
	public int typeStatut() {
		return typeStatut;
	}
	public void setTypeStatut(int typeStatut) {
		this.typeStatut = typeStatut;
		preparerTypesEvenement();
	}
	public int typePersonnel() {
		return typePersonnel;
	}
	public void setTypePersonnel(int typePersonnel) {
		this.typePersonnel = typePersonnel;
	}

	// méthodes du controller DG
	public boolean peutRechercher() {
		return super.peutRechercher() && controleurTypeConge != null && controleurTypeConge.indexSelection() >= 0;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		//   préparer le contrôleur de type congé et l'afficher dans la vue des type de congés
		String typeConge = ((ApplicationClient)EOApplication.sharedApplication()).typeAbsenceParDefaut();
		if (typeConge == null) {
			typeConge = GestionTypeConge.TOUS_CONGES;
		}

		listeAffichage.table().addMouseListener(new DoubleClickListener());

		typeStatut = TOUS;
		typePersonnel = TOUS;
		controleurTypeConge = new GestionTypeConge(editingContext(),typeConge);
		controleurTypeConge.init();
		super.preparerFenetre();
		GraphicUtilities.swaperView(vueTypeConge,controleurTypeConge.component());
	}
	
	protected void terminer() {
	}
	
	protected void preparerTypesEvenement() {
		// changer les types de congé affichés
		NSArray typesConges;
		if (typeStatut == TOUS) {
			typesConges = EOTypeAbsence.rechercherTypesAbsencesGeres(editingContext());
		} else if (typeStatut == TITULAIRE) {
			NSMutableArray types = new NSMutableArray(EOTypeAbsence.rechercherTypesAbsencesFontionnaire(editingContext()));
			if (EOGrhumParametres.isGestionEns()) {
				types.addObjectsFromArray(EOTypeAbsence.rechercherTypesAbsencesNormalien(editingContext()));
			}
			typesConges = new NSArray(types);
		} else {
			typesConges = EOTypeAbsence.rechercherTypesAbsencesContractuels(editingContext());
		}

		controleurTypeConge.setTypesConges(typesConges);
	}
	protected  NSArray typesEvenementSelectionnes() {
		if (!controleurTypeConge.estSelectionTousConges()) {
			// un type de congé a été sélectionné
			return new NSArray(controleurTypeConge.typeConge());
		} else {
			return controleurTypeConge.tousTypes();
		}
	}
	
	/**
	 * Verifie la validite de l'agent en fonction des criteres TITULAIRE / NON TITULAIRE, ENSEIGNANT / NON ENSEIGNANT
	 */
	protected boolean estIndividuValide(EOIndividu individu) {

		if (typeStatut == TOUS && typePersonnel == TOUS) {
			return true;
		}

		NSTimestamp dateDebut = dateDebut(), dateFin = dateFin();
		boolean estValide = false;

		if (typeStatut == TOUS || typeStatut == TITULAIRE) {

			NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),individu,dateDebut,dateFin);
			if (carrieres != null && carrieres.count() > 0) {
				EOCarriere carriere = carrieres.get(0);
				estValide = (typePersonnel  == TOUS) ||
				(
						(typePersonnel == ENSEIGNANT && carriere.toTypePopulation().estEnseignant()) || 
						(typePersonnel == NON_ENSEIGNANT && !carriere.toTypePopulation().estEnseignant())
				);
			}
		}

		if (!estValide && (typeStatut == TOUS || typeStatut == CONTRACTUEL) ) {
			NSArray<EOContratAvenant> avenants = EOContratAvenant.finForIndividuAndPeriode(editingContext(), individu, dateDebut, dateFin);

			if (avenants.count() > 0) {
				EOContratAvenant avenant = avenants.get(0);					
				if (avenant.toGrade() != null && !avenant.toGrade().estNonRenseigne() 
						&& avenant.toGrade().toCorps().toTypePopulation() != null) {
					estValide = (typePersonnel  == TOUS) ||
					(
							(typePersonnel == ENSEIGNANT && avenant.toGrade().toCorps().toTypePopulation().estEnseignant()) || 
							(typePersonnel == NON_ENSEIGNANT && !avenant.toGrade().toCorps().toTypePopulation().estEnseignant())
					);
				}
				else {
					estValide = (typePersonnel  == TOUS) ||
					(
							(typePersonnel == ENSEIGNANT && avenant.contrat().toTypeContratTravail().estEnseignant()) || 
							(typePersonnel == NON_ENSEIGNANT && !avenant.contrat().toTypeContratTravail().estEnseignant())
					);					
				}
			}
		}

		return estValide;

	}

	protected String titreImpressionParDefaut() {
		return "Edition " + controleurTypeConge.libelleSelectionne();
	}
	//	Classe d'ecoute pour les doubleclics dans la table view  */
	public class DoubleClickListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {

				NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,"Congés");

				EvenementPourEdition evenement = (EvenementPourEdition)displayGroup().selectedObject();
				NSMutableDictionary userInfo = new NSMutableDictionary();
				userInfo.setObjectForKey(evenement.nom(),"nom");
				userInfo.setObjectForKey(evenement.prenom(),"prenom");

				NSNotificationCenter.defaultCenter().postNotification(GestionEtatCivil.NOUVEL_EMPLOYE, null,userInfo);

			}
		}
	}
	@Override
	protected void setWaitCursor(Boolean value) {
		// TODO Auto-generated method stub
		if (value)
			CRICursor.setWaitCursor(((ApplicationClient)EOApplication.sharedApplication()).activeFrame());
		else
			CRICursor.setDefaultCursor(((ApplicationClient)EOApplication.sharedApplication()).activeFrame());
	}

}

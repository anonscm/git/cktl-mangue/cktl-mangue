/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import java.awt.Cursor;
import java.awt.Window;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.InfoPourEditionRequete;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class PageEditionRequete extends ModelePage {
	public EOView vuePeriode;
	private GestionPeriode controleurPeriode;
	private NSMutableArray infosTrouvees;
	private String titreEdition;
	private boolean selectionModifiee;
	private static int numRequeteImpression = 0;

	// accesseurs
	public String titreEdition() {
		return titreEdition;
	}
	public void setTitreEdition(String titreEdition) {
		if (titreEdition == null) {
			this.titreEdition = "";		// pour ne pas avoir de problème lors de l'impression
		} else {
			this.titreEdition = titreEdition;
		}
	}
	/** retourne le nombre total d'elements affiches */
	public int nbTotalElements() {
		return displayGroup().displayedObjects().count();
	}
	// actions
	/** methode a definir dans les sous-classes */
	public void rechercher() {
		Window activeWindow = EOApplication.sharedApplication().windowObserver().activeWindow();
		if (activeWindow != null) {
			activeWindow.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		} 
		String titre = titreImpressionParDefaut();
		if (controleurPeriode != null && controleurPeriode.debutPeriode() != null) {
			titre += " - Période du " + controleurPeriode.debutPeriode();
			if (controleurPeriode.finPeriode() != null) {
				titre += " au " + controleurPeriode.finPeriode();
			}
		}
		setTitreEdition(titre);

		infosTrouvees = new NSMutableArray();
		infosTrouvees = preparerInformations();
				
		displayGroup().setObjectArray(infosTrouvees);
		displayGroup().updateDisplayedObjects();
		controllerDisplayGroup().redisplay();
		if (activeWindow != null) {
			activeWindow.setCursor(Cursor.getDefaultCursor());
		} 
	}
	public void annuler() {
		LogManager.logDetail(this.getClass().getName() + "annuler");
		displayGroup().setObjectArray(infosTrouvees);
		displayGroup().updateDisplayedObjects();
		selectionModifiee = false;
		controllerDisplayGroup().redisplay();
	}
	public void supprimer() {
		LogManager.logDetail(this.getClass().getName() + "supprimer");
		NSMutableArray infos = new NSMutableArray(displayGroup().displayedObjects());
		infos.removeObjectsInArray(displayGroup().selectedObjects());
		displayGroup().setObjectArray(infos);
		displayGroup().updateDisplayedObjects();
		selectionModifiee = true;
		controllerDisplayGroup().redisplay();
	}
	public void restreindre() {
		LogManager.logDetail(this.getClass().getName() + "restreindre");
		displayGroup().setObjectArray(displayGroup().selectedObjects());
		displayGroup().updateDisplayedObjects();
		selectionModifiee = true;
		controllerDisplayGroup().redisplay();
	}
	public void exporter() {
		LogManager.logDetail(this.getClass().getName() + "exporter");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourExport(),file.getParent(),file.getName());
		}
	}
	public void imprimer() {
		LogManager.logDetail(this.getClass().getName() + "imprimer");
		numRequeteImpression++;
		String postfixe = DateCtrl.dateToStringSansDelim(new NSTimestamp()) + "_" + numRequeteImpression;
		InfoPourEditionRequete infoEdition = new InfoPourEditionRequete(nomFichierJasper(),titreEdition(),controleurPeriode.dateDebut(),controleurPeriode.dateFin());
		Class[] classeParametres = new Class[] {NSArray.class,InfoPourEditionRequete.class};
		Object[] parametres = new Object[]{displayGroup().displayedObjects(),infoEdition};

		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(), methodeImpression(), classeParametres, parametres, nomFichierImpression() + postfixe,"Impression des résultats de la requête");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	// méthodes du controller DG
	public boolean peutRechercher() {
		return controleurPeriode != null && controleurPeriode.debutPeriode() != null;
	}
	public boolean selectionModifiee() {
		return selectionModifiee;
	}
	public boolean peutImprimer() {
		return displayGroup().displayedObjects().count() > 0;
	}
	public boolean elementsSelectionnes() {
		return displayGroup().selectedObjects().count() > 0;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		//   préparer le contrôleur de période et l'afficher dans la vue des périodes en sélectionnant l'affichage des périodes par défaut
		controleurPeriode = new GestionPeriode(null,null,((ApplicationClient)EOApplication.sharedApplication()).periodeAbsenceParDefaut());
		controleurPeriode.init();
		GraphicUtilities.swaperView(vuePeriode,controleurPeriode.component());
	}
	protected boolean traitementsPourSuppression() {
		// pas de suppression
		return false;
	}
	protected String messageConfirmationDestruction() {
		// // pas de suppression
		return null;
	}

	protected NSArray fetcherObjets() {
		// pas d'affichage au démarrage
		return null;
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		// pas de validation
		return true;
	}
	protected NSTimestamp dateDebut() {
		if (controleurPeriode != null && controleurPeriode.dateDebut() != null) {
			return controleurPeriode.dateDebut();
		} else {
			return null;
		}
	}
	protected NSTimestamp dateFin() {
		if (controleurPeriode != null && controleurPeriode.dateFin() != null) {
			return controleurPeriode.dateFin();
		} else {
			return null;
		}
	}
	protected void traitementsApresValidation() {}
	protected void afficherExceptionValidation(String message) {}
	/** methode a definir dans les sous-classes. Retourne un NSMutableArray contenant les resultats de la recherche */
	protected abstract NSMutableArray preparerInformations();
	/** methode a definir dans les sous-classes. Retourne le texte a exporter dans un fichier */
	protected abstract String textePourExport();
	/** methode a definir dans les sous-classes. Retourne le nom de la methode d'impression sur le serveur */
	protected abstract String methodeImpression();
	/** methode a d&eacute;finir dans les sous-classes. Retourne le nom du fichier d'impression */
	protected abstract String nomFichierImpression();
	/** methode a d&eacute;finir dans les sous-classes. Retourne le nom du fichier Jasper a utiliser */
	protected abstract String nomFichierJasper();
	/** methode a d&eacute;finir dans les sous-classes. Retourne le titre par defaut de l'impression */
	protected abstract String titreImpressionParDefaut();
}

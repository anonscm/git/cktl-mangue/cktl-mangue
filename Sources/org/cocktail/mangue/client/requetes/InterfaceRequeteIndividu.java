/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.requetes;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.client.select.PaysSelectCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EODepartement;
import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale;
import org.cocktail.mangue.common.modele.nomenclatures.EOSituationMilitaire;
import org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeHandicap;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

// 07/05/2010 - correction d'un bug lors de la réception de notification du pays de nationalité + suppression des notifications
// sur les pays avant d'afficher le dialogue de sélection
// 04/01/2011 - Adaptation Netbeans
public class InterfaceRequeteIndividu extends InterfaceRequeteAvecPeriode {
	private String civilite;
	private String  nom;
	private String prenom;
	private String dateNaissance;
	private String lieuNaissance;
	private Integer nbEnfants;
	private EOSituationFamiliale situationFamiliale;
	private EOPays paysNaissance;
	private EODepartement departement;
	private EOPays paysNationalite;
	private EOSituationMilitaire situationMilitaire;
	private EOTypeHandicap typeHandicap;
	private boolean estHandicape;
	private String operateurComparaisonDateNaissance;
	private String operateurComparaisonEnfants;
	private NSTimestamp date1Affectation;
	private PaysSelectCtrl mySelectorPaysNaissance;
	private PaysSelectCtrl mySelectorNationalite;

	// accesseurs
	public String civilite() {
		return civilite;
	}
	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}
	public String dateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(String dateNaissance) {
		if (dateNaissance != null && dateNaissance.length() > 0) {
			String date = DateCtrl.dateCompletion(dateNaissance);
			if (date != null && date.length() > 0) {
				this.dateNaissance = date;
			} else {
				this.dateNaissance = null;
			}
			controllerDisplayGroup().redisplay();
		} else {
			this.dateNaissance = dateNaissance;
		}
	}
	public String cDepartement() {
		if (departement == null) {
			return null;
		} else {
			return departement.code();
		}
	}
	public void setCDepartement(String departement) {
		stockerValeurPourEntiteChampEtDestination(departement, EODepartement.ENTITY_NAME, INomenclature.CODE_KEY, "departement");
	}
	public String cPaysNaissance() {
		if (paysNaissance == null) {
			return null;
		} else {
			return paysNaissance.code();
		}
	}
	public void setCPaysNaissance(String paysNaissance) {
		stockerValeurPourEntiteChampEtDestination(paysNaissance, EOPays.ENTITY_NAME, INomenclature.CODE_KEY, "paysNaissance");
	}
	public String cPaysNationalite() {
		if (paysNationalite == null) {
			return null;
		} else {
			return paysNationalite.code();
		}
	}
	public void setCPaysNationalite(String paysNationalite) {
		stockerValeurPourEntiteChampEtDestination(paysNationalite, EOPays.ENTITY_NAME, INomenclature.CODE_KEY, "paysNationalite");
	}
	public String cSituationFamiliale() {
		if (situationFamiliale == null) {
			return null;
		} else {
			return situationFamiliale.code();
		}
	}
	public void setCSituationFamiliale(String situationFamiliale) {
		stockerValeurPourEntiteChampEtDestination(situationFamiliale, EOSituationFamiliale.ENTITY_NAME, INomenclature.CODE_KEY, "situationFamiliale");
	}
	public boolean estHandicape() {
		return estHandicape;
	}
	public void setEstHandicape(boolean aBool) {
		estHandicape = aBool;
		if (aBool == false && typeHandicap() != null) {
			supprimerTypeHandicap();
		}
	}
	public String cSituationMilitaire() {
		if (situationMilitaire == null) {
			return null;
		} else {
			return situationMilitaire.code();
		}
	}
	public void setCSituationMilitaire(String situationMilitaire) {
		stockerValeurPourEntiteChampEtDestination(situationMilitaire, EOSituationMilitaire.ENTITY_NAME, INomenclature.CODE_KEY,"situationMilitaire");
	}
	public String datePremiereAffectation() {
		if (date1Affectation == null) {
			return "";
		} else {	
			NSTimestampFormatter formatter=new NSTimestampFormatter("%d/%m/%Y");
			return formatter.format(date1Affectation);
		}
	}
	public void setDatePremiereAffectation(String aDate) {
		if (aDate == null) {
			date1Affectation = null;
		} else {
			String myDate = DateCtrl.dateCompletion((String)aDate);
			if (myDate.length() == 0) {
				date1Affectation = null;
			} else {
				date1Affectation = DateCtrl.stringToDate(myDate);
			}
		}
	}
	public EODepartement departement() {
		return departement;
	}
	public void setDepartement(EODepartement departement) {
		this.departement = departement;
	}
	public String lieuNaissance() {
		return lieuNaissance;
	}
	public void setLieuNaissance(String lieuNaissance) {
		this.lieuNaissance = lieuNaissance;
	}
	public EOPays paysNationalite() {
		return paysNationalite;
	}
	public void setPaysNationalite(EOPays nationalite) {
		this.paysNationalite = nationalite;
	}
	public Integer nbEnfants() {
		return nbEnfants;
	}
	public void setNbEnfants(Integer nbEnfants) {
		this.nbEnfants = nbEnfants;
	}
	public String nom() {
		return nom;
	}
	public void setNom(String nom) {
		if (nom != null) {
			this.nom = nom.toUpperCase();
		} else {
			this.nom = null;
		}
	}
	public String operateurComparaisonDateNaissance() {
		return operateurComparaisonDateNaissance;
	}
	public void setOperateurComparaisonDateNaissance(String operateurComparaisonDate) {
		this.operateurComparaisonDateNaissance = operateurComparaisonDate;
	}
	public String operateurComparaisonEnfants() {
		return operateurComparaisonEnfants;
	}
	public void setOperateurComparaisonEnfants(String operateurComparaisonEnfants) {
		this.operateurComparaisonEnfants = operateurComparaisonEnfants;
	}
	public EOPays paysNaissance() {
		return paysNaissance;
	}
	public void setPaysNaissance(EOPays paysNaissance) {
		this.paysNaissance = paysNaissance;
	}
	public String prenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		if (prenom != null) {
			this.prenom = StringCtrl.capitalizedString((String)prenom);
		} else {
			this.prenom = null;
		}
	}
	public EOSituationFamiliale situationFamiliale() {
		return situationFamiliale;
	}
	public void setSituationFamiliale(EOSituationFamiliale situationFamiliale) {
		this.situationFamiliale = situationFamiliale;
	}
	public EOSituationMilitaire situationMilitaire() {
		return situationMilitaire;
	}
	public void setSituationMilitaire(EOSituationMilitaire situationMilitaire) {
		this.situationMilitaire = situationMilitaire;
	}
	public EOTypeHandicap typeHandicap() {
		return typeHandicap;
	}
	public void setTypeHandicap(EOTypeHandicap typeHandicap) {
		this.typeHandicap = typeHandicap;
	}
	// Méthodes d'initialisation
	public void init(NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		EOArchive.loadArchiveNamed("InterfaceRequeteIndividu",this,"org.cocktail.mangue.client.requetes.interfaces",this.disposableRegistry());
		super.init(debutPeriode,finPeriode);
	}
	public EOQualifier qualifierRecherche() {
		return qualifierRecherche("");
	}
	public EOQualifier qualifierRecherche(String relation) {
		String nomRelation = "";
		if (relation.length() > 0) {
			nomRelation = relation + "." + nomRelation;
		}
		NSMutableArray args = new NSMutableArray();
		String stringQualifier = nomRelation + "temValide = 'O' AND ";
		if (civilite() != null && civilite().equals("*") == false) {
			args.addObject(civilite());
			stringQualifier = nomRelation + "cCivilite = %@ AND ";
		}
		if (nom() != null && nom().length() > 0) {
			args.addObject("*" + nom() + "*");
			stringQualifier = stringQualifier + nomRelation + "nomUsuel caseinsensitivelike  %@ AND ";
		}
		if (prenom() != null && prenom().length() > 0) {
			args.addObject("*" + prenom() + "*");
			stringQualifier = stringQualifier + nomRelation + "prenom caseinsensitivelike  %@ AND ";
		}
		if (dateNaissance() != null && dateNaissance().length() > 0) {
			args.addObject(DateCtrl.stringToDate(dateNaissance()));
			stringQualifier = stringQualifier + nomRelation + "dNaissance " + operateurComparaisonDateNaissance() + " %@ AND ";
		}
		if (lieuNaissance() != null && lieuNaissance().length() > 0) {
			args.addObject("*" + lieuNaissance() + "*");
			stringQualifier = stringQualifier  + nomRelation + "villeDeNaissance caseinsensitivelike  %@ AND ";
		}
		if (nbEnfants() != null) {
			args.addObject(nbEnfants());
			stringQualifier = stringQualifier  + nomRelation + "personnels.nbEnfants " + operateurComparaisonEnfants + "  %@ AND ";
		}
		if (situationFamiliale() != null) {
			args.addObject(situationFamiliale().code());
			stringQualifier = stringQualifier  + nomRelation + "toSituationFamiliale.code = %@ AND ";
		}
		if (paysNaissance() != null) {
			args.addObject(paysNaissance().code());
			stringQualifier = stringQualifier  + nomRelation + "toPaysNaissance.code = %@ AND ";
		}
		if (departement() != null) {
			args.addObject(departement().code());
			stringQualifier = stringQualifier  + nomRelation + "toDepartement.code = %@ AND ";
		}
		if (paysNationalite() != null) {
			args.addObject(paysNationalite().code());
			stringQualifier = stringQualifier  + nomRelation + "toPaysNationalite.code = %@ AND ";
		}
		if (situationMilitaire() != null) {
			args.addObject(situationMilitaire().code());
			stringQualifier = stringQualifier  + nomRelation + "toSituationMilitaire.code = %@ AND ";
		}
		stringQualifier = SuperFinder.nettoyerQualifier(stringQualifier," AND ");
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);	

		if (displayGroup().selectedObjects().count() > 0) {
			NSMutableArray myQualifiers = new NSMutableArray();
			if (qualifier != null) {
				myQualifiers.addObject(qualifier);
			}
			qualifier = SuperFinder.construireORQualifier(nomRelation + "diplomes.diplome.code",(NSArray)displayGroup().selectedObjects().valueForKey(EODiplomes.CODE_KEY));
			myQualifiers.addObject(qualifier);
			return new EOAndQualifier(myQualifiers);
		} else {
			return qualifier;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public EOQualifier qualifierHandicap() {
		if (estHandicape() == false) {
			return null;
		} else {
			NSMutableArray qualifiers = new NSMutableArray();
			EOQualifier qualifier = qualifierDates("dateDebut","dateFin");
			if (qualifier != null) {
				qualifiers.addObject(qualifier);
			} else {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeHandicap <> NIL",null));
			}
			if (typeHandicap != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeHandicap.thanCode = %@", new NSArray(typeHandicap().code())));
			} 
			return new EOAndQualifier(qualifiers);
		}
	}
	// DT 1675 - recherche sur la première affectation (entité Arrivée)
	public EOQualifier qualifierPremiereAffectation() {
		if (date1Affectation == null) {
			return null;
		} else {
			NSMutableArray args = new NSMutableArray(DateCtrl.jourPrecedent(date1Affectation));
			args.addObject(DateCtrl.jourSuivant(date1Affectation));
			return EOQualifier.qualifierWithQualifierFormat("d1Affectation > %@ and d1Affectation < %@",args);
		}
	}
//	actions

	public void afficherPaysNaissance() {
		if (mySelectorPaysNaissance == null)
			mySelectorPaysNaissance = new PaysSelectCtrl(editingContext());
		
		EOPays pays = mySelectorPaysNaissance.getPays(null, null);
		if (pays != null) {
			stockerValeur(pays,"paysNaissance");
			if (cPaysNaissance() != null && cPaysNaissance().equals(EOPays.CODE_PAYS_FRANCE) == false && departement() != null) {
				departement = null;
				updaterDisplayGroups();
			}
		}
	}
	
	/** Affiche tous les pays valides &grave; la date */
	public void afficherPaysNationalite() {

		if (mySelectorNationalite == null)
			mySelectorNationalite = new PaysSelectCtrl(editingContext());
		
		EOPays pays = mySelectorNationalite.getPays(null, null);
		if (pays != null) {
			stockerValeur(pays,"paysNationalite");
		}
		
	}
	public void afficherTypeHandicap() {
		UtilitairesDialogue.afficherDialogue(this,"TypeHandicap","getTypeHandicap",false,null,true);
	}
	public void afficherSituationMilitaire() {
		UtilitairesDialogue.afficherDialogue(this,EOSituationMilitaire.ENTITY_NAME,"getSituationMilitaire",false,null,true);
	}
	public void afficherSituationFamiliale() {
		UtilitairesDialogue.afficherDialogue(this,EOSituationFamiliale.ENTITY_NAME,"getSituationFamiliale",false,null,true);
	}
	public void afficherDepartement() {
		UtilitairesDialogue.afficherDialogue(this,EODepartement.ENTITY_NAME,"getDepartementNaissance",true,null,true);
	}
	public void supprimerPaysNaissance() {
		paysNaissance = null;
		updaterDisplayGroups();
	}
	public void supprimerPaysNationalite() {
		paysNationalite = null;
		updaterDisplayGroups();
	}
	public void supprimerSituationMilitaire() {
		situationMilitaire = null;
		updaterDisplayGroups();
	}
	public void supprimerTypeHandicap() {
		typeHandicap = null;
		updaterDisplayGroups();
	}
	public void supprimerSituationFamiliale() {
		situationFamiliale = null;
		updaterDisplayGroups();
	}
	public void supprimerDepartement() {
		departement = null;
		updaterDisplayGroups();
	}
//	Notifications
//	GET DEPARTEMENT
	public void getDepartementNaissance(NSNotification aNotif) {
		stockerValeur(aNotif,"departement");
	}
//	GET NATIONALITE
	public void getPaysNationalite(NSNotification aNotif) {
		stockerValeur(aNotif,"paysNationalite");
	}
//	GET SITUATION MILITAIRE
	public void getSituationMilitaire(NSNotification aNotif) {
		stockerValeur(aNotif,"situationMilitaire");
	}
//	GET TYPE HANDICAP
	public void getTypeHandicap(NSNotification aNotif) {
		stockerValeur(aNotif,"typeHandicap");
	}
//	GET SITUTATION FAMILIALE
	public void getSituationFamiliale(NSNotification aNotif) {
		stockerValeur(aNotif,"situationFamiliale");
	}
//	méthodes du controller DG
	public boolean peutAfficherTypeHandicap() {
		return estHandicape();
	}
	public boolean peutModifierDepartement() {
		return paysNaissance == null || paysNaissance().code().equals(EOPays.CODE_PAYS_FRANCE);
	}
	public boolean peutSupprimerDepartement() {
		return departement() != null;
	}
	public boolean peutSupprimerPaysNaissance() {
		return paysNaissance() != null;
	}
	public boolean peutSupprimerPaysNationalite() {
		return paysNationalite() != null;
	}
	public boolean peutSupprimerSituationFamiliale() {
		return situationFamiliale() != null;
	}
	public boolean peutSupprimerSituationMilitaire() {
		return situationMilitaire() != null;
	}
	public boolean peutSupprimerTypeHandicap() {
		return typeHandicap() != null;
	}
	//	méthodes protégées
	protected void terminer() {
	}
	protected void effacerChamps() {
		civilite = null;
		nom = null;
		prenom = null;
		dateNaissance = null;
		lieuNaissance = null;
		nbEnfants = null;
		situationFamiliale = null;
		paysNaissance = null;
		paysNationalite = null;
		departement = null;
		situationMilitaire = null;
		typeHandicap = null;
		estHandicape = false;
	}
	/** retourne la liste des dipl&ocirc;mes */
	protected NSArray<EODiplomes> fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(), EODiplomes.ENTITY_NAME);
	}

	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey(EODiplomes.LIBELLE_COURT_KEY, EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}

//	méthodes privées
	private void stockerValeurPourEntiteChampEtDestination(String valeur,String nomEntite,String nomAttribut,String cleDestin) {
		NSArray results = SuperFinder.rechercherAvecAttributEtValeurEgale(editingContext(),nomEntite,nomAttribut,valeur);
		if (results.count() > 0) {
			stockerValeur(results.objectAtIndex(0),cleDestin);
		}
	}	
}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFicheIdentiteCtrl.java

package org.cocktail.mangue.client.promouvabilites;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.promotions.PromoEcDetailView;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.EOSupInfoData;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class PromoEcDetailCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static PromoEcDetailCtrl sharedInstance;
	private EOEditingContext ec;
	private PromoEcDetailView myView;

	private EOSupInfoData currentSupInfo;

	public PromoEcDetailCtrl(EOEditingContext globalEc) {
		ec = globalEc;
		myView = new PromoEcDetailView(new JFrame(), true);

		myView.getBtnFermer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){fermer();}}
		);
		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){valider();}}
		);

		myView.getTfObservations().setEnabled(false);		
		myView.getPopupEtats().addActionListener(new PopupEtatListener());
		
	}

	public static PromoEcDetailCtrl sharedInstance(EOEditingContext editingContext)
	{
		if(sharedInstance == null)
			sharedInstance = new PromoEcDetailCtrl(editingContext);
		return sharedInstance;
	}

	private void clearTextFields() {
				
		CocktailUtilities.viderTextField(myView.getTfNom());
		CocktailUtilities.viderTextField(myView.getTfPrenom());
		CocktailUtilities.viderTextField(myView.getTfNumen());
		CocktailUtilities.viderTextField(myView.getTfDateNaissance());
		CocktailUtilities.viderTextField(myView.getTfCodePosition());
		CocktailUtilities.viderTextField(myView.getTfDebutPosition());
		CocktailUtilities.viderTextField(myView.getTfFinPosition());
		CocktailUtilities.viderTextField(myView.getTfQuotite());
		CocktailUtilities.viderTextField(myView.getTfLieuPosition());
		CocktailUtilities.viderTextField(myView.getTfTitularisation());
		CocktailUtilities.viderTextField(myView.getTfLibelleGrade());
		CocktailUtilities.viderTextField(myView.getTfDateNaissance());
		CocktailUtilities.viderTextField(myView.getTfDateGrade());
		CocktailUtilities.viderTextField(myView.getTfEchelon());
		CocktailUtilities.viderTextField(myView.getTfDateEchelon());
		CocktailUtilities.viderTextField(myView.getTfChevron());
		CocktailUtilities.viderTextArea(myView.getTfObservations());
		CocktailUtilities.viderTextField(myView.getTfUai());
		CocktailUtilities.viderTextField(myView.getTfStatut());

	}

	public void open(EOSupInfoData supInfo) {
		
		clearTextFields();
		
		currentSupInfo = supInfo;
		
		myView.getTfTitre().setText("DETAIL PROMOTION E.C. - " + currentSupInfo.toIndividu().identitePrenomFirst());

		updateData();
		
		myView.setVisible(true);

	}

	private class PopupEtatListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {			
			
			try {
				
				switch (myView.getPopupEtats().getSelectedIndex()) {
				
				case 0:currentSupInfo.setEficEtat(null);break;
				case 1:currentSupInfo.setEficEtat(EOSupInfoData.STATUT_PROVISOIRE);break;
				case 2:currentSupInfo.setEficEtat(EOSupInfoData.STATUT_TRANSMIS_MINISTERE);break;
				case 3:currentSupInfo.setEficEtat(EOSupInfoData.STATUT_PROMU);break;
				case 4:currentSupInfo.setEficEtat(EOSupInfoData.STATUT_NEGATIF);break;
				
				}
				
				ec.saveChanges();
				
			}
			catch (Exception ex) {				
			}
		}
	}

	
	private void updateData() {

		if(currentSupInfo.eficEtat() != null) {
			if (currentSupInfo.eficEtat().equals(EOSupInfoData.STATUT_PROVISOIRE))
				myView.getPopupEtats().setSelectedIndex(1);
			if (currentSupInfo.eficEtat().equals(EOSupInfoData.STATUT_TRANSMIS_MINISTERE))
				myView.getPopupEtats().setSelectedIndex(2);
			if (currentSupInfo.eficEtat().equals(EOSupInfoData.STATUT_PROMU))
				myView.getPopupEtats().setSelectedIndex(3);
			if (currentSupInfo.eficEtat().equals(EOSupInfoData.STATUT_NEGATIF))
				myView.getPopupEtats().setSelectedIndex(4);
		}
		else
			myView.getPopupEtats().setSelectedIndex(0);
		
		CocktailUtilities.setTextToArea(myView.getTfObservations(), currentSupInfo.eficObservations());
		CocktailUtilities.setTextToField(myView.getTfNom(), currentSupInfo.eficNomUsuel());
		CocktailUtilities.setTextToField(myView.getTfPrenom(), currentSupInfo.eficPrenom());
		CocktailUtilities.setTextToField(myView.getTfNumen(), currentSupInfo.eficNumen());
		CocktailUtilities.setDateToField(myView.getTfDateNaissance(), currentSupInfo.eficDNaissance());
		CocktailUtilities.setTextToField(myView.getTfCodePosition(), currentSupInfo.eficCPosition());
		CocktailUtilities.setDateToField(myView.getTfDebutPosition(), currentSupInfo.eficDDebPostion());
		CocktailUtilities.setDateToField(myView.getTfFinPosition(), currentSupInfo.eficDFinPostion());
		CocktailUtilities.setNumberToField(myView.getTfQuotite(), currentSupInfo.eficQuotite());
		CocktailUtilities.setTextToField(myView.getTfLieuPosition(), currentSupInfo.eficLieuPosition());
		CocktailUtilities.setDateToField(myView.getTfTitularisation(), currentSupInfo.eficDTitularisation());
		CocktailUtilities.setTextToField(myView.getTfUai(), currentSupInfo.eficUai());
		CocktailUtilities.setTextToField(myView.getTfStatut(), currentSupInfo.eficStatut());

		if (currentSupInfo.eficCTypeAccesCorps() != null) {
			EOTypeAcces acces = EOTypeAcces.findForCode(ec, currentSupInfo.eficCTypeAccesCorps());
			CocktailUtilities.setTextToField(myView.getTfAccesCorps(), acces.code() + " - " + acces.libelleLong());
		}
		
		if (currentSupInfo.eficCTypeAccesGrade() != null) {
			EOTypeAcces acces = EOTypeAcces.findForCode(ec, currentSupInfo.eficCTypeAccesGrade());
			CocktailUtilities.setTextToField(myView.getTfAccesGrade(), acces.code() + " - " + acces.code());
		}

		if(currentSupInfo.toGrade() != null)						
			CocktailUtilities.setTextToField(myView.getTfLibelleGrade(), currentSupInfo.toGrade().llGrade());
		
		CocktailUtilities.setDateToField(myView.getTfDateGrade(), currentSupInfo.eficDGrade());
		CocktailUtilities.setDateToField(myView.getTfDateNominationCorps(), currentSupInfo.eficDNominationCorps());
		CocktailUtilities.setTextToField(myView.getTfEchelon(), currentSupInfo.eficCEchelon());
		CocktailUtilities.setDateToField(myView.getTfDateEchelon(), currentSupInfo.eficDEchelon());
		CocktailUtilities.setTextToField(myView.getTfChevron(), currentSupInfo.eficCChevron());

		CocktailUtilities.setTextToField(myView.getTfSpecialisation(), currentSupInfo.eficCSectionCnu());
		
	}
	
	private void fermer() {

		ec.revert();
		myView.setVisible(false);
		
	}
	
	private void valider() {
		try {
		
			if (myView.getTfCodePosition().getText().length() > 0)
				currentSupInfo.setEficCPosition(myView.getTfCodePosition().getText());
			else
				currentSupInfo.setEficCPosition(null);
				
			if (myView.getTfUai().getText().length() > 0)
				currentSupInfo.setEficUai(myView.getTfUai().getText());
			else
				currentSupInfo.setEficUai(null);

			ec.saveChanges();
			myView.setVisible(false);
		}
		catch (ValidationException ex) {
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}
		catch (Exception e) {
			e.printStackTrace();
		}		
	}



}

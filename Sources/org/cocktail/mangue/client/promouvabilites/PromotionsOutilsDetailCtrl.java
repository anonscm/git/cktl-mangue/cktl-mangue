// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFicheIdentiteCtrl.java

package org.cocktail.mangue.client.promouvabilites;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.promotions.PromotionsOutilsDetailView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.individu.EOHistoPromotion;

import com.webobjects.eocontrol.EOEditingContext;

public class PromotionsOutilsDetailCtrl {
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static PromotionsOutilsDetailCtrl sharedInstance;
	private EOEditingContext edc;
	private PromotionsOutilsDetailView myView;

	private EOHistoPromotion currentPromotion;

	public PromotionsOutilsDetailCtrl(EOEditingContext edc) {
		
		setEdc(edc);
		myView = new PromotionsOutilsDetailView(new JFrame(), true);

		myView.getBtnFermer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){myView.setVisible(false);}}
		);
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public static PromotionsOutilsDetailCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new PromotionsOutilsDetailCtrl(editingContext);
		return sharedInstance;
	}
	
	public EOHistoPromotion getCurrentPromotion() {
		return currentPromotion;
	}

	public void setCurrentPromotion(EOHistoPromotion currentPromotion) {
		this.currentPromotion = currentPromotion;
		updateDatas();
	}

	/**
	 * 
	 * @param promotion
	 */
	public void open(EOHistoPromotion promotion) {
		setCurrentPromotion(promotion);		
		myView.setVisible(true);
	}

	/**
	 * 
	 */
	private void updateDatas() {

		if (getCurrentPromotion() != null) {

			CocktailUtilities.setTextToField(myView.getTfAncCorps(), getCurrentPromotion().hproDureeCorps());
			CocktailUtilities.setTextToField(myView.getTfAncGrade(), getCurrentPromotion().hproDureeGrade());
			CocktailUtilities.setTextToField(myView.getTfAncEchelon(), getCurrentPromotion().hproDureeEchelon());
			CocktailUtilities.setTextToField(myView.getTfAncPublic(), getCurrentPromotion().hproDureeServPublic());
			CocktailUtilities.setTextToField(myView.getTfAncCategorie(), getCurrentPromotion().hproDureeCategServPublics());
			CocktailUtilities.setTextToField(myView.getTfAncEffectifs(), getCurrentPromotion().hproDureeServEffectifs());
			CocktailUtilities.setTextToArea(myView.getTfAncConditions(), getCurrentPromotion().paramPromotion().description());		

		}
	}

}

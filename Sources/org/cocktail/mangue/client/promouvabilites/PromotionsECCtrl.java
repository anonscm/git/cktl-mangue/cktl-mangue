
package org.cocktail.mangue.client.promouvabilites;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Enumeration;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ConsoleTraitementCtrl;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.promotions.PromotionsECView;
import org.cocktail.mangue.client.select.IndividuSelectCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOParamPromotion;
import org.cocktail.mangue.modele.mangue.EOSupInfoData;
import org.cocktail.mangue.modele.mangue.EOSupInfoFichier;
import org.cocktail.mangue.modele.mangue.individu.EOHistoPromotion;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class PromotionsECCtrl {

	private static PromotionsECCtrl sharedInstance;
	private EOEditingContext edc;
	private PromotionsECView myView;
	private EODisplayGroup eod;
	private ListenerPromotion listenerPromotion = new ListenerPromotion();
	private String nomFichierExport;
	private Integer currentExercice;

	private EOSupInfoData currentPromotion;
	private boolean traitementServeurEnCours, preparationEnCours = false;

	private EOParamPromotion currentParamPromotion;
	private NSMutableArray<EOParamPromotion> paramsPromotion= new NSMutableArray();
	private ConsoleTraitementCtrl 		myConsole;

	public PromotionsECCtrl(EOEditingContext edc) {

		this.edc = edc;
		eod = new EODisplayGroup();

		myView = new PromotionsECView(eod);
		myConsole = new ConsoleTraitementCtrl(edc);
		myConsole.setCtrlParent(this, "Traitement des promotions E.C. ...");

		myView.getBtnCalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){
				preparer();
			}
		});
		myView.getBtnFichier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){
				exporterFichier();
			}
		});

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ajouter();
			}
		});
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				supprimer();
			}
		});
		myView.getBtnDetail().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				afficherDetail();
			}
		});		myView.getBtnImprimerFiches().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				imprimerFichesIndividuelles();
			}
		});

		myView.setListeExercices(CocktailConstantes.LISTE_ANNEES_DESC);

		myView.getPopupEtats().addActionListener(new PopupEtatListener());

		myView.getPopupAnnees().addActionListener(new PopupExerciceListener());
		myView.getPopupValidite().addActionListener(new PopupValiditeListener());

		myView.getPopupGrades().removeAllItems();
		myView.getPopupGrades().addItem("*");
		myView.getPopupGrades().addActionListener(new PopupGradeListener());

		myView.getTfFiltreNom().getDocument().addDocumentListener(new ADocumentListener());
		myView.getMyEOTable().addListener(listenerPromotion);

		setLabelInfos();
		updateInterface();
	}

	public Integer getCurrentExercice() {
		return currentExercice;
	}
	public void setCurrentExercice(Integer currentExercice) {
		this.currentExercice = currentExercice;
	}
	public EOSupInfoData getCurrentPromotion() {
		return currentPromotion;
	}

	public boolean isTraitementServeurEnCours() {
		return traitementServeurEnCours;
	}

	public void setTraitementServeurEnCours(boolean traitementServeurEnCours) {
		this.traitementServeurEnCours = traitementServeurEnCours;
	}

	public boolean isPreparationEnCours() {
		return preparationEnCours;
	}

	public void setPreparationEnCours(boolean preparationEnCours) {
		this.preparationEnCours = preparationEnCours;
	}

	public void setCurrentPromotion(EOSupInfoData currentPromotion) {
		this.currentPromotion = currentPromotion;
	}

	public EOParamPromotion getCurrentParamPromotion() {
		return currentParamPromotion;
	}

	public void setCurrentParamPromotion(EOParamPromotion currentParamPromotion) {
		this.currentParamPromotion = currentParamPromotion;
	}

	private void afficherDetail() {
		PromoEcDetailCtrl.sharedInstance(edc).open(getCurrentPromotion());
		myView.getMyEOTable().updateUI();
	}

	private void setLabelInfos() {
		myView.getLabelInfos().setText("");

		if (getCurrentExercice() != null)
			myView.getLabelInfos().setText("** Prise en charge de la situation des agents au 31/12/" + String.valueOf(getCurrentExercice().intValue()));
	}

	// L'impression des fiches individuelles est faite individu par individu et le nom  du fichier est uai_numen. 
	// Si pas de numen alors uai_noIndividu
	// Mmodification de la fiche individuelle imprimée et de l'uai incluse dans le nom du fichier imprimé
	// si pas d'uai pour le nom du fichier alors uai établissement
	public void imprimerFichesIndividuelles() {

		CRICursor.setWaitCursor(myView);

		boolean avertirPourNomFichier = false;
		NSMutableArray fichiersPdf = new NSMutableArray();
		String dirImpression = pathFichier();//((ApplicationClient)EOApplication.sharedApplication()).directoryImpression();

		String uaiEtablissement = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE);

		for (EOSupInfoData record : (NSArray<EOSupInfoData>)eod.selectedObjects()) {

			EOIndividu individu = record.toIndividu();
			String nomFichier = uaiEtablissement + "_";
			if (individu.personnel().numen() != null)
				nomFichier += individu.personnel().numen();
			else {
				nomFichier = individu.nomUsuel() + "_" + individu.prenom();
				nomFichier = nomFichier.replaceAll(" ", "");
				avertirPourNomFichier = true;
			}

			try {
				Class[] classeParametres = new Class[] {EOGlobalID.class};
				Object[] parametres = new Object[]{edc.globalIDForObject(individu)};
				EODistributedObjectStore store = (EODistributedObjectStore)edc.parentObjectStore();
				NSDictionary dictionary = (NSDictionary)store.invokeRemoteMethodWithKeyPath(edc,
						"session",
						"clientSideRequestImprimerFicheElectraSansThread",classeParametres,parametres,false);
				NSData datas = (NSData)dictionary.objectForKey("data");
				if (datas == null)
					EODialogs.runErrorDialog("Erreur", "Impossible de générer la fiche d'identité de " + individu.identite());
				else {
					String filePath = UtilitairesFichier.enregistrerFichier(datas,dirImpression,nomFichier,"pdf",false);
					if (filePath != null)
						fichiersPdf.addObject(filePath);

				}	
			} catch (Exception exc) {
				exc.printStackTrace();
				EODialogs.runErrorDialog("Erreur", "Impossible d'afficher la fiche d'identité de " + individu.identite());
			}
		}
		// Créer le zip et supprimer les fichiers
		if (fichiersPdf.count() > 0) {
			if (UtilitairesFichier.creerArchiveZip(pathFichier(),"FichesElectra", fichiersPdf,true) == false)
				EODialogs.runErrorDialog("Erreur", "Impossible de créer le fichier ZIP");
			else {
				EODialogs.runInformationDialog("", "L'archive FichesElectra.zip est prête dans le répertoire " + pathFichier());
			}
		}

		if (avertirPourNomFichier)
			EODialogs.runInformationDialog("Attention", "Certains individus n'ayant pas de numen, le nom de leur fiche sera invalide dans l'archive");

		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	public void preparer() 	{

		CRICursor.setWaitCursor(myView);

		try {

			Class classeParametres[] = {
					Integer.class, NSArray.class
			};


			NSMutableArray<EOGlobalID> globalIds = new NSMutableArray<EOGlobalID>();
			for (int i=1;i<myView.getPopupGrades().getItemCount();i++) {
				globalIds.addObject(edc.globalIDForObject(((EOParamPromotion)myView.getPopupGrades().getItemAt(i))));
			}

			Object parametres[] = {
					getCurrentExercice(),
					globalIds
			};
			
			lancerTraitementServeur("clientSideRequestPreparerRecordsPromEC", classeParametres, parametres);
			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		actualiser();
		CRICursor.setDefaultCursor(myView);
	}

	public String pathFichier()
	{
		String pathDir = (new StringBuilder(String.valueOf(((ApplicationClient)EOApplication.sharedApplication()).directoryExports()))).append(System.getProperty("file.separator")).append("PROMO_EC").append(System.getProperty("file.separator")).toString();
		UtilitairesFichier.verifierPathEtCreer(pathDir);
		return pathDir;
	}

	/**
	 * 
	 */
	private void exporterFichier() 	{
		JFileChooser saveDialog = new JFileChooser(pathFichier());
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(1);
		String rne = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE);
		nomFichierExport = (new StringBuilder(String.valueOf(rne))).append("E").toString();
		saveDialog.setSelectedFile(new File((new StringBuilder(String.valueOf(nomFichierExport))).append(".csv").toString()));

		if(saveDialog.showSaveDialog(myView) == 0) {

			File file = saveDialog.getSelectedFile();
			CRICursor.setWaitCursor(myView);
			String texte = "";
			NSArray sortedDatas = EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(), new NSArray(new EOSortOrdering("eficNomPatronymique", EOSortOrdering.CompareAscending)));
			for(Enumeration<EOSupInfoData> e = sortedDatas.objectEnumerator(); e.hasMoreElements();) {
				EOSupInfoData record = e.nextElement();
				texte =  texte + texteExportPourRecord(record) + "\n";
			}

			UtilitairesFichier.afficherFichier(texte, file.getParent(), nomFichierExport, "csv", false);
			CRICursor.setDefaultCursor(myView);
		}
	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EOSupInfoData record) {

		String texte = "";

		texte += CocktailExports.ajouterChamp(record.eficUai());

		texte += CocktailExports.ajouterChamp(record.eficNumen());

		texte += CocktailExports.ajouterChamp(record.eficCivilite());

		texte += CocktailExports.ajouterChamp(record.eficNomPatronymique());

		texte += CocktailExports.ajouterChamp(record.eficPrenom());

		texte += CocktailExports.ajouterChamp(record.eficNomUsuel());

		texte += CocktailExports.ajouterChampDate(record.eficDNaissance());

		texte += CocktailExports.ajouterChamp(record.eficCPaysNationalite());

		texte += CocktailExports.ajouterChamp(record.eficCPosition());

		texte += CocktailExports.ajouterChampDate(record.eficDDebPostion());

		if (record.eficDFinPostion() != null)
			texte += CocktailExports.ajouterChampDate(record.eficDFinPostion());
		else
			texte += CocktailExports.ajouterChampVide();

		texte += CocktailExports.ajouterChamp(record.eficCSectionCnu());

		texte += CocktailExports.ajouterChamp(record.eficCCorps());

		texte += CocktailExports.ajouterChamp(record.eficStatut());

		texte += CocktailExports.ajouterChampDate(record.eficDNominationCorps());

		texte += CocktailExports.ajouterChampDate(record.eficDTitularisation());

		texte += CocktailExports.ajouterChamp(record.eficCGrade());

		texte += CocktailExports.ajouterChampDate(record.eficDGrade());

		texte += CocktailExports.ajouterChamp(record.eficCEchelon());

		texte += CocktailExports.ajouterChamp(record.eficCChevron());

		texte += CocktailExports.ajouterChampDate(record.eficDEchelon());

		texte += CocktailExports.ajouterChamp(record.eficAncConservee());

		return texte;
	}

	public static PromotionsECCtrl sharedInstance(EOEditingContext editingContext)
	{
		if(sharedInstance == null)
			sharedInstance = new PromotionsECCtrl(editingContext);
		return sharedInstance;
	}

	private void initListeGrades() {

		EOParamPromotion lastParamPromotion = null;

		if (myView.getPopupGrades().getSelectedIndex() > 0)
			lastParamPromotion = (EOParamPromotion)myView.getPopupGrades().getSelectedItem();

		myView.getPopupGrades().removeAllItems();
		myView.getPopupGrades().addItem("**");

		if (getCurrentExercice() == null) {
			return;
		} else {

			NSArray<EOParamPromotion> parametresPromotion = EOParamPromotion.rechercherParametresPourDateEtType(edc, EOHistoPromotion.datePromotionPourParametreEtAnnee("EC", currentExercice), "EC");

			NSMutableArray<EOParamPromotion> codesAjoutes = new NSMutableArray<EOParamPromotion>();
			for( EOParamPromotion parametre : parametresPromotion) {
				if (((NSArray)codesAjoutes.valueForKey(EOParamPromotion.PARP_CODE_KEY)).containsObject(parametre.parpCode()) == false)
					codesAjoutes.addObject(parametre);
			}

			if (codesAjoutes.count() > 0) {
				EOSortOrdering.sortArrayUsingKeyOrderArray(codesAjoutes, new NSArray(EOSortOrdering.sortOrderingWithKey(EOParamPromotion.PARP_CODE_KEY, EOSortOrdering.CompareAscending)));
				for (EOParamPromotion code : codesAjoutes) {
					myView.getPopupGrades().addItem(code);
				}

				myView.getPopupGrades().setSelectedIndex(0);
				if (lastParamPromotion != null && ((NSArray)codesAjoutes.valueForKey(EOParamPromotion.PARP_CODE_KEY)).containsObject(lastParamPromotion.parpCode()))
					myView.getPopupGrades().setSelectedItem(lastParamPromotion);

			}
		}
	}


	public JPanel getView() {
		return myView;
	}	

	private void clean() {
		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
		myView.getLblMessage().setText("");
	}

	public void actualiser()	{

		clean();

		initListeGrades();

		if(getCurrentExercice() != null) {
			eod.setObjectArray(new NSArray());
			CRICursor.setWaitCursor(myView);
			EOSupInfoFichier fichier = EOSupInfoFichier.fetchForAnneeAndType(edc, getCurrentExercice(), EOSupInfoFichier.OBJET_PROM_EC);
			if (fichier != null) {
				eod.setObjectArray(EOSupInfoData.findForFichier(edc, fichier));
				filter();
			}
			CRICursor.setDefaultCursor(myView);
		}
	}

	/**
	 * 
	 */
	private void ajouter() {
		try {
			EOIndividu personnel = IndividuSelectCtrl.sharedInstance(edc).getIndividu();

			if (personnel != null) {

				EOSupInfoFichier fichier = EOSupInfoFichier.fetchForAnneeAndType(edc, getCurrentExercice(), EOSupInfoFichier.OBJET_PROM_EC);

				if (fichier == null) {
					fichier = EOSupInfoFichier.creer(edc, EOSupInfoFichier.OBJET_PROM_EC);
					fichier.setSupfAnnee(getCurrentExercice());
					fichier.setSupfDateObs(DateCtrl.finAnnee(getCurrentExercice()));

					edc.saveChanges();				
				}

				NSArray<EOParamPromotion> params = EOParamPromotion.rechercherParametresPourCode(edc, ((EOParamPromotion)myView.getPopupGrades().getSelectedItem()).parpCode());
				NSMutableArray<EOGlobalID> globalIds = new NSMutableArray<EOGlobalID>();
				for (EOParamPromotion myParam : params)
					globalIds.addObject(edc.globalIDForObject(myParam));
				String retour = ServerProxy.clientSideRequestPreparerPromoEcManuelle(edc, edc.globalIDForObject(fichier), globalIds, edc.globalIDForObject(personnel));

				if(retour != null)
					EODialogs.runInformationDialog("OK", retour);

				actualiser();

			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer les donnés des agents sélectionnés ?", "Oui", "Non"))		
			return;			

		try {
			for(Enumeration<EOEnterpriseObject> e = eod.selectedObjects().objectEnumerator(); e.hasMoreElements(); edc.deleteObject(e.nextElement()));
			edc.deleteObject(getCurrentPromotion());
			edc.saveChanges();
			actualiser();
		}
		catch(Exception e) {
			edc.revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();

		if (myView.getPopupGrades().getSelectedIndex() > 0) {

			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();			
			NSArray<EOParamPromotion> params = EOParamPromotion.rechercherParametresPourCode(edc, ((EOParamPromotion)myView.getPopupGrades().getSelectedItem()).parpCode());
			for (EOParamPromotion myParam : params)
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOSupInfoData.TO_PARAM_PROMOTION_KEY + " = %@", new NSArray(myParam)));

			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));			
		}		

		if(myView.getPopupValidite().getSelectedIndex() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOSupInfoData.EFIC_TEM_VALIDE_KEY + " = %@", new NSArray(myView.getPopupValidite().getSelectedIndex() == 1 ? "O" : "N")));

		if(myView.getPopupEtats().getSelectedIndex() > 0) {
			String etat = "";
			switch (myView.getPopupEtats().getSelectedIndex()) {
			case 1 : etat = EOSupInfoData.STATUT_PROVISOIRE;break;
			case 2 : etat = EOSupInfoData.STATUT_TRANSMIS_MINISTERE;break;
			case 3 : etat = EOSupInfoData.STATUT_PROMU;break;
			case 4 : etat = EOSupInfoData.STATUT_NEGATIF;break;
			}
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOSupInfoData.EFIC_ETAT_KEY + " = %@", new NSArray(etat)));
		}

		if(myView.getTfFiltreNom().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOSupInfoData.EFIC_NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray((new StringBuilder("*")).append(myView.getTfFiltreNom().getText()).append("*").toString())));

		return new EOAndQualifier(mesQualifiers);
	}

	/**
	 * 
	 */
	private void filter() 	{
		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();
		myView.getLblMessage().setText( eod.displayedObjects().count() + " Agents");
		updateInterface();
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnAjouter().setEnabled(getCurrentExercice() != null && myView.getPopupGrades().getSelectedIndex() > 0);
		myView.getBtnModifier().setEnabled(false);
		myView.getBtnSupprimer().setEnabled(getCurrentPromotion() != null);
		myView.getBtnCalculer().setEnabled(getCurrentExercice() != null);
		myView.getBtnFichier().setEnabled(getCurrentExercice() != null && eod.displayedObjects().count() > 0);

		myView.getBtnImprimerFiches().setEnabled(eod.selectedObjects().count() > 0);
		myView.getBtnDetail().setEnabled(getCurrentPromotion() != null);

	}

	private class ADocumentListener
	implements DocumentListener
	{

		public void changedUpdate(DocumentEvent e)
		{
			filter();
		}

		public void insertUpdate(DocumentEvent e)
		{
			filter();
		}

		public void removeUpdate(DocumentEvent e)
		{
			filter();
		}

	}

	private class ListenerPromotion
	implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			afficherDetail();
		}

		public void onSelectionChanged()
		{
			setCurrentPromotion((EOSupInfoData)eod.selectedObject());
			updateInterface();
		}

	}

	private class PopupEtatListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {			
			filter();
		}
	}


	private class PopupGradeListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {

			if (myView.getPopupGrades().getSelectedIndex() == 0)
				setCurrentParamPromotion(null);
			else
				setCurrentParamPromotion((EOParamPromotion)myView.getPopupGrades().getSelectedItem());

			filter();
		}
	}

	private class PopupExerciceListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {
			setCurrentExercice(null);
			if(myView.getPopupAnnees().getSelectedIndex() > 0)
				setCurrentExercice((Integer)myView.getPopupAnnees().getSelectedItem());
			setLabelInfos();
			actualiser();
		}
	}

	private class PopupValiditeListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction) {
			filter();
		}
	}

	/**
	 * 
	 * @param nomMethode
	 * @param individus
	 * @param estPreparation
	 */
	private void lancerTraitementServeur(String nomMethode, Class[] classeParametres, Object[] parametres) {

		setTraitementServeurEnCours(true);

		myConsole.lancerTraitement(nomMethode, classeParametres, parametres);

		myConsole.toFront();
		myView.setVisible(false);

	}

	/**
	 * 
	 */
	public void terminerTraitementServeur() {

		try {

			setTraitementServeurEnCours(false);	
			setPreparationEnCours(false);

			myConsole.addToMessage("\n >> TRAITEMENT TERMINE !");
			actualiser();

			myView.setVisible(true);

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}

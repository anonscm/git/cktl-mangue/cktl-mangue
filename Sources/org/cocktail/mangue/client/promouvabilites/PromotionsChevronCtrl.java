// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.promouvabilites;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.util.Enumeration;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.promotions.PromotionsChevronsView;
import org.cocktail.mangue.client.impression.GestionImpression;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.common.modele.manager.Manager;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.IndividuPromouvable;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOPassageChevron;
import org.cocktail.mangue.modele.mangue.EOCorpsPromouvable;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class PromotionsChevronCtrl
{

	private static PromotionsChevronCtrl sharedInstance;

	private EODisplayGroup eodCorps, eodGrade, eodPassageChevron, eodPromouvables;
	private ListenerCorps listenerCorps = new ListenerCorps();
	private ListenerGrade listenerGrade = new ListenerGrade();
	private ListenerPromouvables listenerPromouvables = new ListenerPromouvables();

	private PromotionsChevronsView myView;
	private int numImpressionCourante;
	private NSArray destinatairesArrete;

	private EOCorps currentCorps;
	private EOGrade currentGrade;
	private IndividuPromouvable currentPromouvable;
	private Manager manager;

	public PromotionsChevronCtrl(Manager manager)	{

		this.manager = manager;
		
		eodCorps = new EODisplayGroup();
		eodGrade = new EODisplayGroup();
		eodPassageChevron = new EODisplayGroup();
		eodPromouvables = new EODisplayGroup();

		myView = new PromotionsChevronsView(eodCorps, eodGrade, eodPassageChevron, eodPromouvables);

		myView.getBtnListe().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				calculer();
			}
		}
		);

		myView.getBtnPromouvoir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				promouvoir();
			}
		}
		);
		myView.getBtnImprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				imprimer();
			}
		}
		);
		myView.getBtnImprimerArrete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				imprimerArrete();
			}
		}
		);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				exporter();
			}
		}
		);


		myView.getMyEOTablePassage().setEnabled(false);

		myView.getMyEOTableCorps().addListener(listenerCorps);
		myView.getMyEOTableGrade().addListener(listenerGrade);
		myView.getMyEOTableProm().addListener(listenerPromouvables);

		eodCorps.setSortOrderings(new NSArray(new EOSortOrdering(EOCorpsPromouvable.CORPS_KEY+"."+EOCorps.LL_CORPS_KEY, EOSortOrdering.CompareAscending)));
		eodGrade.setSortOrderings(new NSArray(new EOSortOrdering(EOGrade.LL_GRADE_KEY, EOSortOrdering.CompareAscending)));

		NSMutableArray sortChevrons = new NSMutableArray();
		sortChevrons.addObject(new EOSortOrdering(EOPassageChevron.C_ECHELON_KEY, EOSortOrdering.CompareAscending));
		sortChevrons.addObject(new EOSortOrdering(EOPassageChevron.C_CHEVRON_KEY, EOSortOrdering.CompareAscending));
		eodPassageChevron.setSortOrderings(sortChevrons);
		eodPromouvables.setSortOrderings(new NSArray(new EOSortOrdering("datePromotion", EOSortOrdering.CompareAscending)));

		myView.getTypesPeriode().addActionListener(new PopupPeriodeListener());
		myView.getTypesPeriode().setSelectedIndex(2);

		myView.getTfPeriodeDebut().addFocusListener(new FocusListenerDateDebut());
		myView.getTfPeriodeFin().addActionListener(new ActionListenerDateDebut());

		myView.getTfPeriodeFin().addFocusListener(new FocusListenerDateFin());
		myView.getTfPeriodeFin().addActionListener(new ActionListenerDateFin());

	}
	
	private EOEditingContext getEdc() {
		return manager.getEdc();
	}

	public static PromotionsChevronCtrl sharedInstance() {
		if(sharedInstance == null)
			sharedInstance = new PromotionsChevronCtrl(((ApplicationClient)ApplicationClient.sharedApplication()).getManager());
		return sharedInstance;
	}

	public EOCorps getCurrentCorps() {
		return currentCorps;
	}

	public void setCurrentCorps(EOCorps currentCorps) {
		this.currentCorps = currentCorps;
	}

	public EOGrade getCurrentGrade() {
		return currentGrade;
	}

	public void setCurrentGrade(EOGrade currentGrade) {
		this.currentGrade = currentGrade;
	}

	public IndividuPromouvable getCurrentPromouvable() {
		return currentPromouvable;
	}

	public void setCurrentPromouvable(IndividuPromouvable currentPromouvable) {
		this.currentPromouvable = currentPromouvable;
	}

	public JPanel getView() {
		return myView;
	}	

	/**
	 * 
	 */
	private void calculer() {

		if (CocktailUtilities.getTextFromField(myView.getTfPeriodeDebut()) == null) {			
			EODialogs.runErrorDialog("ERREUR","Merci de renseigner une date de début ET une date de fin de période");
			PromouvabilitesCtrl.sharedInstance().toFront();
			return;
		}

		CRICursor.setWaitCursor(myView);
		NSArray<IndividuPromouvable> promouvables = IndividuPromouvable.evaluerIndividusPromouvablesChevron(getEdc(), 
				qualifierRecherche(),
				DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText()),
				DateCtrl.stringToDate(myView.getTfPeriodeFin().getText()),
				myView.getCheckNonPromus().isSelected());

		eodPromouvables.setObjectArray(promouvables);
		myView.getMyEOTableProm().updateData();

		updateUI();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier qualifierRecherche() {
		
		NSMutableArray<EOQualifier>	andQualifiers = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOQualifier>	orQualifiers = new NSMutableArray<EOQualifier>();
		
		NSTimestamp dateDebut = CocktailUtilities.getDateFromField(myView.getTfPeriodeDebut());
		NSTimestamp dateFin = CocktailUtilities.getDateFromField(myView.getTfPeriodeFin());

		// On recherche les éléments de carrière valides sur la période
		andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(EOElementCarriere.DATE_DEBUT_KEY, dateDebut, EOElementCarriere.DATE_FIN_KEY, dateFin));

		// Qualifier sur les grades
		if (eodGrade.selectedObjects().size() > 0) {
			orQualifiers = new NSMutableArray<EOQualifier>();
			for (EOGrade grade : (NSArray<EOGrade>)eodGrade.selectedObjects()) {
				orQualifiers.addObject(CocktailFinder.getQualifierEqual(EOGrade.C_GRADE_KEY, grade.cGrade()));
			}
			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		// Qualifier sur les corps
		if (eodCorps.selectedObjects().size() > 0) {
			orQualifiers = new NSMutableArray<EOQualifier>();
			for (EOCorpsPromouvable corpsPromouvable: (NSArray<EOCorpsPromouvable>)eodCorps.selectedObjects()) {
				EOCorps corps = corpsPromouvable.corps();
				orQualifiers.addObject(CocktailFinder.getQualifierEqual(EOGrade.C_CORPS_KEY,corps.cCorps()));			}
			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(andQualifiers);
	}


	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		eodCorps.setObjectArray(EOCorpsPromouvable.rechercherCorpsPromouvablesPourPeriodeEtType(getEdc(), new NSTimestamp(), null, EOCorpsPromouvable.PROMOTION_CHEVRON));
		myView.getMyEOTableCorps().updateData();

		updateUI();
		CRICursor.setDefaultCursor(myView);
	}

	public void imprimer() {

		CRICursor.setWaitCursor(myView);
		NSTimestamp dateDebut = DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText());
		NSTimestamp dateFin = DateCtrl.stringToDate(myView.getTfPeriodeFin().getText());

		Class[] classeParametres =  new Class[] {NSTimestamp.class,NSTimestamp.class,NSArray.class};
		Object[] parametres = new Object[]{dateDebut,dateFin,eodPromouvables.selectedObjects()};
		try {
			// avec Thread
			UtilitairesImpression.imprimerSansDialogue(getEdc(), "clientSideRequestImprimerPromouvables",classeParametres,parametres,"Promouvabilites","Impression des promouvabilités");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		CRICursor.setDefaultCursor(myView);
	}
	public void exporter() {
		CRICursor.setWaitCursor(myView);
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourExport(),file.getParent(),file.getName());
		}
		CRICursor.setDefaultCursor(myView);
	}
	private String textePourExport() {
		String texte =  "Civilité\tNom\tPrénom\tEchelon\tIndice\tDate d'effet\tEchelon Suivant\tIndice\tDate d'effet\tGrade\n";
		for (Enumeration<IndividuPromouvable> e = eodPromouvables.displayedObjects().objectEnumerator();e.hasMoreElements();) {
			IndividuPromouvable individu = e.nextElement();
			// formatter avec , comme séparateur décimal pour excel
			texte = texte + individu.individu().cCivilite() + "\t" + individu.individu().nomUsuel() + "\t" + individu.individu().prenom() + "\t" + individu.echelon() + "\t";
			// 24/12/2010 - ajout des indices majorés
			if (individu.indiceMajore() != null) {
				texte += individu.indiceMajore();
			}
			texte += "\t"+ individu.dateEffetFormatee() + "\t" + individu.echelonSuivant() + "\t";
			if (individu.indiceMajoreSuivant() != null) {
				texte += individu.indiceMajoreSuivant();
			}
			texte += "\t" + individu.datePromotionFormatee() + "\t" + individu.grade().lcGrade() + "\n";
		}
		return texte;
	}
	public void continuerImpression(NSNotification aNotif) {
		if (++numImpressionCourante < eodPromouvables.selectedObjects().count())
			imprimerArretePromouvable((IndividuPromouvable)eodPromouvables.selectedObjects().objectAtIndex(numImpressionCourante));
		else
			NSNotificationCenter.defaultCenter().removeObserver(this, GestionImpression.FIN_IMPRESSION,null);
	}
	
	
	private void imprimerArrete() {
		
		destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
				numImpressionCourante = 0;
				NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("continuerImpression",new Class [] {NSNotification.class}),GestionImpression.FIN_IMPRESSION,null);
				imprimerArretePromouvable((IndividuPromouvable)eodPromouvables.selectedObjects().objectAtIndex(numImpressionCourante));			
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
		CRICursor.setDefaultCursor(myView);
	}
	

	private void imprimerArretePromouvable(IndividuPromouvable promouvable) {
		try {
			NSMutableArray destinatairesGlobalIds = new NSMutableArray();
			for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(getEdc().globalIDForObject(e.nextElement())));
			Class[] classeParametres =  new Class[] {IndividuPromouvable.class,NSArray.class};
			LogManager.logDetail(" Individu Promouvable : " + promouvable.chevron());
			LogManager.logDetail(" Individu Promouvable : " + promouvable.chevronSuivant());
			Object[] parametres = new Object[]{promouvable,destinatairesGlobalIds};
			UtilitairesImpression.imprimerSansDialogue(getEdc(), "clientSideRequestImprimerArretePromouvabiliteChevron",classeParametres,parametres,"ArretePromouvabilite" + promouvable.individu().noIndividu() ,"Impression de l'arrêté de promouvabilité");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}

	/**
	 * 
	 */
	private void promouvoir() {

		try {

			CRICursor.setWaitCursor(myView);
			for (IndividuPromouvable promouvable : (NSArray<IndividuPromouvable>) eodPromouvables.selectedObjects()) {

				// Fermer l'élément de carrière au jour précédent de la promouvabilité
				promouvable.elementCarriere().setDateFin(DateCtrl.jourPrecedent(promouvable.datePromotion()));

				// Créer un nouvel élément de carrière à partir du précédent
				EOElementCarriere nouvelElement = new EOElementCarriere();
				nouvelElement.initAvecElement(promouvable.elementCarriere());
				nouvelElement.setCEchelon(promouvable.echelonSuivant());
				nouvelElement.setCChevron(promouvable.chevronSuivant());
				getEdc().insertObject(nouvelElement);
			}
			getEdc().saveChanges();
			EODialogs.runInformationDialog("", "" + eodPromouvables.selectedObjects().count() + " élément(s) de carrière créé(s)");
		} catch (Exception exc) {
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite lors de l'enregistrement des éléments de carrière.\n" + exc.getMessage());
			getEdc().revert();
		}

		PromouvabilitesCtrl.sharedInstance().toFront();
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void updateUI(){

		myView.getBtnPromouvoir().setEnabled(eodPromouvables.selectedObjects().count() > 0);
		myView.getTfMessage().setText(eodPromouvables.displayedObjects().count() + " Agents");
		myView.getBtnListe().setEnabled(eodCorps.selectedObjects().count() > 0 );

		myView.getBtnImprimer().setEnabled(eodPromouvables.displayedObjects().count() > 0);
		myView.getBtnImprimerArrete().setEnabled(eodPromouvables.selectedObjects().count() > 0);
		myView.getBtnExporter().setEnabled(eodPromouvables.displayedObjects().count() > 0);

	}

	private class PopupPeriodeListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			switch (myView.getTypesPeriode().getSelectedIndex()) {

			case 1:myView.getTfPeriodeDebut().setText(DateCtrl.dateToString(CocktailUtilities.debutAnneeCivile(new NSTimestamp())));
			myView.getTfPeriodeFin().setText(DateCtrl.dateToString(CocktailUtilities.finAnneeCivile(new NSTimestamp())));break;
			case 2:myView.getTfPeriodeDebut().setText(DateCtrl.dateToString(CocktailUtilities.debutAnneeUniversitaire(new NSTimestamp())));
			myView.getTfPeriodeFin().setText(DateCtrl.dateToString(CocktailUtilities.finAnneeUniversitaire(new NSTimestamp())));break;

			}
		}
	}

	private class ListenerCorps implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
		}

		public void onSelectionChanged() {

			setCurrentCorps(null);
			eodGrade.setObjectArray(new NSArray());
			eodPromouvables.setObjectArray(new NSArray());

			if (eodCorps.selectedObject() != null) {

				currentCorps = ((EOCorpsPromouvable)eodCorps.selectedObject()).corps();

				if (currentCorps != null && eodCorps.selectedObjects().count() == 1) {
					eodGrade.setObjectArray(EOGrade.rechercherGradesPourCorpsValidesEtPeriode(getEdc(), currentCorps, 
							DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText()),
							DateCtrl.stringToDate(myView.getTfPeriodeFin().getText())));
				}
			}

			myView.getMyEOTableGrade().updateData();
			myView.getMyEOTableProm().updateData();
			updateUI();

		}
	}
	private class ListenerGrade implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
		}

		public void onSelectionChanged() {

			setCurrentGrade((EOGrade)eodGrade.selectedObject());
			eodPassageChevron.setObjectArray(new NSArray());
			eodPromouvables.setObjectArray(new NSArray());

			if (getCurrentGrade() != null && eodGrade.selectedObjects().count() == 1) {
				eodPassageChevron.setObjectArray(
						EOPassageChevron.rechercherPassagesChevronPourGradesValidesPourPeriode(getEdc(), currentGrade, 
								DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText()),
								DateCtrl.stringToDate(myView.getTfPeriodeFin().getText())));
			}
			myView.getMyEOTablePassage().updateData();
			myView.getMyTableModelPassage().fireTableDataChanged();
			myView.getMyEOTableProm().updateData();

			updateUI();
		}
	}
	private class ListenerPromouvables implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
		}

		public void onSelectionChanged() {

			currentPromouvable = (IndividuPromouvable)eodPromouvables.selectedObject();			
			updateUI();
		}
	}


	private class ActionListenerDateDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfPeriodeDebut().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfPeriodeDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de début de période saisie n'est pas valide !");
				myView.getTfPeriodeDebut().selectAll();
			}
			else	{
				myView.getTfPeriodeDebut().setText(myDate);
			}
		}
	}
	private class FocusListenerDateDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfPeriodeDebut().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfPeriodeDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de début de période saisie n'est pas valide !");
				myView.getTfPeriodeDebut().selectAll();
			}
			else
				myView.getTfPeriodeDebut().setText(myDate);
		}
	}
	private class ActionListenerDateFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfPeriodeFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfPeriodeFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin de période saisie n'est pas valide !");
				myView.getTfPeriodeFin().selectAll();
			}
			else	{
				myView.getTfPeriodeFin().setText(myDate);
			}
		}
	}
	private class FocusListenerDateFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfPeriodeFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfPeriodeFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin de période saisie n'est pas valide !");
				myView.getTfPeriodeFin().selectAll();
			}
			else
				myView.getTfPeriodeFin().setText(myDate);
		}
	}

}

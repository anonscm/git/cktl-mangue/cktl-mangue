package org.cocktail.mangue.client.promouvabilites;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.promotions.PromouvabilitesView;
import org.cocktail.mangue.common.modele.manager.Manager;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class PromouvabilitesCtrl {

	private static PromouvabilitesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private PromouvabilitesView myView;
	private OngletChangeListener listenerOnglets;
	private EOAgentPersonnel currentUtilisateur;
	
	private Manager manager;

	public PromouvabilitesCtrl(Manager manager) {
		
		this.manager = manager;

		listenerOnglets = new OngletChangeListener();
		myView = new PromouvabilitesView(null, MODE_MODAL.booleanValue());

		myView.getOnglets().addTab("Promotions ECHELON ", null, PromotionsEchelonCtrl.sharedInstance(getEdc()).getView());
		myView.getOnglets().addTab("Promotions CHEVRON ", null, PromotionsChevronCtrl.sharedInstance().getView());
		myView.getOnglets().addTab("Promotions EC ", null, PromotionsECCtrl.sharedInstance(getEdc()).getView());
		myView.getOnglets().addTab("Promotions LA / TA ", null, PromotionsOutilsCtrl.sharedInstance(getEdc()).getView());
		myView.getOnglets().addChangeListener(listenerOnglets);

		currentUtilisateur = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();

	}

	public static PromouvabilitesCtrl sharedInstance()	{
		if(sharedInstance == null)
			sharedInstance = new PromouvabilitesCtrl(((ApplicationClient)ApplicationClient.sharedApplication()).getManager());
		return sharedInstance;
	}
	
	public EOEditingContext getEdc() {
		return manager.getEdc();
	}

	public void open()	{
		
		// Controle ds droits
		if (currentUtilisateur.peutGererPromouvabilites() == false) {
			EODialogs.runInformationDialog("ERREUR", "Vous n'avez pas les droits nécessaires pour gérer les promouvabilités !");
			return;
		}
		
		listenerOnglets.stateChanged(null);
		myView.setVisible(true);
	}

	public JFrame getView() {
		return myView;
	}
	public void toFront() {
		myView.toFront();
	}
	private void actualiser()	{
		CRICursor.setWaitCursor(myView);
		switch(myView.getOnglets().getSelectedIndex())		{
			case 0:	PromotionsEchelonCtrl.sharedInstance(getEdc()).actualiser();break;
			case 1:	PromotionsChevronCtrl.sharedInstance().actualiser();break;
			case 2:	PromotionsOutilsCtrl.sharedInstance(getEdc()).actualiser();break;
		}
		CRICursor.setDefaultCursor(myView);
	}

	private class OngletChangeListener implements ChangeListener	{
		public void stateChanged(ChangeEvent e) {
			CRICursor.setWaitCursor(myView);
			switch(myView.getOnglets().getSelectedIndex())	{
				case 0:	PromotionsEchelonCtrl.sharedInstance(getEdc()).actualiser();break;
				case 1:	PromotionsChevronCtrl.sharedInstance().actualiser();break;
				case 3:	PromotionsECCtrl.sharedInstance(getEdc()).actualiser();break;
			}
			CRICursor.setDefaultCursor(myView);
		}
	}

}

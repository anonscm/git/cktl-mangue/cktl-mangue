// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.promouvabilites;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.promotions.PromotionsEchelonView;
import org.cocktail.mangue.client.impression.GestionImpression;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.IndividuPromouvable;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOCorpsPromouvable;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.individu.EOConservationAnciennete;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOReliquatsAnciennete;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

public class PromotionsEchelonCtrl {

	private static PromotionsEchelonCtrl sharedInstance;
	private EOEditingContext edc;
	private EODisplayGroup eodCorps, eodGrade, eodPassageEchelon, eodPromouvables;
	private ListenerCorps listenerCorps = new ListenerCorps();
	private ListenerGrade listenerGrade = new ListenerGrade();
	private ListenerPromouvables listenerPromouvables = new ListenerPromouvables();

	private PromotionsEchelonView myView;
	private NSArray<EODestinataire> destinatairesArrete;
	private int numImpressionCourante;
	private EOCorps currentCorps;
	private EOGrade currentGrade;
	private IndividuPromouvable currentPromouvable;

	public PromotionsEchelonCtrl(EOEditingContext edc)	{

		setEdc(edc);
		
		eodCorps = new EODisplayGroup();
		eodGrade = new EODisplayGroup();
		eodPassageEchelon = new EODisplayGroup();
		eodPromouvables = new EODisplayGroup();

		myView = new PromotionsEchelonView(eodCorps, eodGrade, eodPassageEchelon, eodPromouvables);

		myView.getBtnListe().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {calculer();}}
				);
		myView.getBtnPromouvoir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {promouvoir();}}
				);
		myView.getBtnImprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
				);
		myView.getBtnImprimerArrete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerArrete();}}
				);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporter();}}
				);
		myView.getBtnSelectAll().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectAll();}}
				);

		myView.getMyEOTablePassage().setEnabled(false);

		myView.getMyEOTableCorps().addListener(listenerCorps);
		myView.getMyEOTableGrade().addListener(listenerGrade);
		myView.getMyEOTableProm().addListener(listenerPromouvables);

		eodCorps.setSortOrderings(EOCorps.SORT_ARRAY_LIBELLE_ASC);
		eodGrade.setSortOrderings(EOGrade.SORT_ARRAY_LIBELLE_ASC);
		eodPassageEchelon.setSortOrderings(EOPassageEchelon.SORT_ARRAY_C_ECHELON_ASC);
		eodPromouvables.setSortOrderings(new NSArray(new EOSortOrdering("datePromotion", EOSortOrdering.CompareAscending)));

		myView.getTypesPeriode().addActionListener(new PopupPeriodeListener());
		myView.getTypesPeriode().setSelectedIndex(2);

		myView.getTfPeriodeDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfPeriodeDebut()));
		myView.getTfPeriodeDebut().addActionListener(new ActionListenerDateTextField(myView.getTfPeriodeDebut()));

		myView.getTfPeriodeFin().addFocusListener(new FocusListenerDateTextField(myView.getTfPeriodeFin()));
		myView.getTfPeriodeFin().addActionListener(new ActionListenerDateTextField(myView.getTfPeriodeFin()));

	}

	public static PromotionsEchelonCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new PromotionsEchelonCtrl(editingContext);
		return sharedInstance;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOCorps getCurrentCorps() {
		return currentCorps;
	}

	public void setCurrentCorps(EOCorps currentCorps) {
		this.currentCorps = currentCorps;
	}

	public EOGrade getCurrentGrade() {
		return currentGrade;
	}

	public void setCurrentGrade(EOGrade currentGrade) {
		this.currentGrade = currentGrade;
	}

	public IndividuPromouvable getCurrentPromouvable() {
		return currentPromouvable;
	}

	public void setCurrentPromouvable(IndividuPromouvable currentPromouvable) {
		this.currentPromouvable = currentPromouvable;
	}

	public JPanel getView() {
		return myView;
	}	

	/**
	 * 
	 */
	private void calculerNew() {

		NSMutableArray<IndividuPromouvable> listePromouvables = new NSMutableArray<IndividuPromouvable>();

		for (EOGrade myGrade : (NSArray<EOGrade>)eodGrade.selectedObjects()) {

			// On recherche les passages valides pour le grade en cours de traitement
			NSArray<EOPassageEchelon> passages = 	EOPassageEchelon.rechercherPassagesEchelonPourGradesValidesPourPeriode(getEdc(), currentGrade, 
					DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText()),
					DateCtrl.stringToDate(myView.getTfPeriodeFin().getText()));

			int indexPassage = 0;
			for (EOPassageEchelon myPassage : passages) {

				// On ne prend en compte que les passages ayant une duree de renseignee
				if (myPassage.dureePassageAnnees() != null || myPassage.dureePassageMois() != null && indexPassage < passages.count()) {

					// On recherche les individus correspondant aux criteres
					NSArray<EOElementCarriere> elements = EOElementCarriere.rechercherElementsAvecCriteres(getEdc(), qualifierRechercheEchelon(null, myGrade, myPassage), true, true);

					NSMutableArray<EOIndividu> individusTraites = new NSMutableArray<EOIndividu>();
					for (EOIndividu myIndividu : ((NSArray<EOIndividu>)elements.valueForKey(EOElementCarriere.TO_INDIVIDU_KEY))) {

						if (!individusTraites.containsObject(myIndividu)) {

							EOPassageEchelon passageSuivant = (EOPassageEchelon)passages.objectAtIndex(indexPassage+1);
							NSArray<EOElementCarriere> elementsSuivant = EOElementCarriere.rechercherElementsAvecCriteres(getEdc(), qualifierRechercheEchelon(myIndividu, myGrade, passageSuivant),true,true);

							if (!myView.getCheckNonPromus().isSelected() || elementsSuivant.count() == 0) {

								IndividuPromouvable promu = IndividuPromouvable.evaluerIndividuPromouvableEchelon(getEdc(),
										myIndividu,
										myPassage,
										passageSuivant,
										qualifierRechercheEchelon(myIndividu, myGrade, myPassage),
										DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText()),
										DateCtrl.stringToDate(myView.getTfPeriodeFin().getText()),
										myView.getCheckNonPromus().isSelected());

								if (promu != null)
									listePromouvables.addObject(promu);			

								individusTraites.addObject(myIndividu);
							}
						}					
					}

					indexPassage ++;
				}			
			}
		}

		eodPromouvables.setObjectArray(listePromouvables.immutableClone());
		myView.getMyEOTableProm().updateData();

		updateUI();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void calculer() {

		if (myView.getTfPeriodeDebut().getText().length() == 0 && myView.getTfPeriodeFin().getText().length() == 0) {			
			EODialogs.runErrorDialog("ERREUR","Merci de renseigner une date de début ET une date de fin de période");
			PromouvabilitesCtrl.sharedInstance().toFront();
			return;
		}

		CRICursor.setWaitCursor(myView);
		NSArray<IndividuPromouvable> promouvables = IndividuPromouvable.evaluerIndividusPromouvables(getEdc(), 
				qualifierRecherche(),
				DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText()),
				DateCtrl.stringToDate(myView.getTfPeriodeFin().getText()),
				myView.getCheckNonPromus().isSelected());

		eodPromouvables.setObjectArray(promouvables);
		myView.getMyEOTableProm().updateData();

		updateUI();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier qualifierRecherche() {

		NSMutableArray<EOQualifier>	qualifiers = new NSMutableArray<EOQualifier>(),qualifiersOR = new NSMutableArray<EOQualifier>();

		NSTimestamp dateDebut = DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText());
		NSTimestamp dateFin = DateCtrl.stringToDate(myView.getTfPeriodeFin().getText());

		// On recherche les éléments de carrière valides sur la période
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOElementCarriere.DATE_DEBUT_KEY, dateDebut, EOElementCarriere.DATE_FIN_KEY, dateFin));

		// Qualifier sur les grades
		if (eodGrade.selectedObjects().size() > 0) {
			qualifiersOR = new NSMutableArray<EOQualifier>();
			for (EOGrade grade : (NSArray<EOGrade>)eodGrade.selectedObjects()) {
				qualifiersOR.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.C_GRADE_KEY + " = %@", new NSArray<String>(grade.cGrade())));
			}
			qualifiers.addObject(new EOOrQualifier(qualifiersOR));
		}
		// Qualifier sur les corps
		if (eodCorps.selectedObjects().size() > 0) {
			qualifiersOR = new NSMutableArray<EOQualifier>();
			for (EOCorps corps : (NSArray<EOCorps>)eodCorps.selectedObjects()) {
				qualifiersOR.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.C_CORPS_KEY + " = %@", new NSArray<String>(corps.cCorps())));
			}
			qualifiers.addObject(new EOOrQualifier(qualifiersOR));
		}
		EOQualifier qualifier = new EOAndQualifier(qualifiers);
		LogManager.logDetail("qualifier pour promouvable " + qualifier);
		return qualifier;
	}

	/**
	 * 
	 */
	private EOQualifier qualifierRechercheEchelon(EOIndividu individu, EOGrade grade, EOPassageEchelon passage) {

		NSMutableArray	qualifiers = new NSMutableArray();

		if (individu != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_INDIVIDU_KEY  + "=%@", new NSArray(individu)));

		NSTimestamp dateDebut = DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText());
		NSTimestamp dateFin = DateCtrl.stringToDate(myView.getTfPeriodeFin().getText());

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_GRADE_KEY + "=%@", new NSArray(grade)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.C_ECHELON_KEY + "=%@", new NSArray(passage.cEchelon())));

		qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOElementCarriere.DATE_DEBUT_KEY, dateDebut, EOElementCarriere.DATE_FIN_KEY, dateFin));

		EOQualifier qualifier = new EOAndQualifier(qualifiers);
		LogManager.logDetail("qualifier pour promouvable " + qualifier);
		return qualifier;
	}

	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		// Liste des corps
		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorpsPromouvable.COPR_PROMOTION_KEY + "=%@", new NSArray(EOCorpsPromouvable.PROMOTION_ECHELON)));

		if (!EOGrhumParametres.isGestionHu())
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorpsPromouvable.CORPS_KEY + ".toTypePopulation.temHospitalier = 'N'", null));

		if (!EOGrhumParametres.isGestionEns())
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorpsPromouvable.CORPS_KEY + ".toTypePopulation.code != 'N'",null));

		eodCorps.setObjectArray((NSArray)EOCorpsPromouvable.rechercherCorpsPromouvablesValidesPourPeriodeAvecQualifier(getEdc(), DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText()),DateCtrl.stringToDate(myView.getTfPeriodeFin().getText()), new EOAndQualifier(qualifiers)).valueForKey("corps"));
		myView.getMyEOTableCorps().updateData();

		updateUI();
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	public void imprimer() {

		CRICursor.setWaitCursor(myView);
		NSTimestamp dateDebut = DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText());
		NSTimestamp dateFin = DateCtrl.stringToDate(myView.getTfPeriodeFin().getText());

		Class[] classeParametres =  new Class[] {NSTimestamp.class,NSTimestamp.class,NSArray.class};
		Object[] parametres = new Object[]{dateDebut,dateFin,eodPromouvables.selectedObjects()};
		try {
			// avec Thread
			UtilitairesImpression.imprimerSansDialogue(getEdc(), "clientSideRequestImprimerPromouvables",
					classeParametres,parametres,
					"Promouvabilites","Impression des promouvabilités");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		CRICursor.setDefaultCursor(myView);
	}

	public void exporter() {
		CRICursor.setWaitCursor(myView);
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourExport(),file.getParent(),file.getName());
		}
		CRICursor.setDefaultCursor(myView);
	}

	private String textePourExport() {

		String texte =  "Civilité\tNom\tPrénom\tEchelon\tIndice\tDate d'effet\tEchelon Suivant\tIndice\tDate d'effet\tGrade\tComposante\tStructure\n";

		for (IndividuPromouvable myIndividu : (NSArray<IndividuPromouvable>) eodPromouvables.displayedObjects()) {

			texte = texte + myIndividu.individu().cCivilite() + "\t" + myIndividu.individu().nomUsuel() + "\t" + myIndividu.individu().prenom() + "\t" + myIndividu.echelon() + "\t";

			if (myIndividu.indiceMajore() != null) {
				texte += myIndividu.indiceMajore();
			}
			texte += "\t"+ myIndividu.dateEffetFormatee() + "\t" + myIndividu.echelonSuivant() + "\t";
			if (myIndividu.indiceMajoreSuivant() != null) {
				texte += myIndividu.indiceMajoreSuivant();
			}
			texte += "\t" + myIndividu.datePromotionFormatee() + "\t" + myIndividu.grade().lcGrade() + "\t" + myIndividu.affectationComp()+ "\t" + myIndividu.affectationStructure()+"\n";
		}
		return texte;
	}



	private void imprimerArrete() {

		destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.size() > 0) {
			try {				
				numImpressionCourante = 0;
				NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("continuerImpression",new Class [] {NSNotification.class}),GestionImpression.FIN_IMPRESSION,null);
				imprimerArretePromouvable((IndividuPromouvable)eodPromouvables.selectedObjects().get(numImpressionCourante));			
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
		CRICursor.setDefaultCursor(myView);
	}

	
	/**
	 * 
	 * @param aNotif
	 */
	public void continuerImpression(NSNotification aNotif) {
		if (++numImpressionCourante < eodPromouvables.selectedObjects().size())
			imprimerArretePromouvable((IndividuPromouvable)eodPromouvables.selectedObjects().get(numImpressionCourante));
		else
			NSNotificationCenter.defaultCenter().removeObserver(this, GestionImpression.FIN_IMPRESSION,null);
	}

	/**
	 * 
	 * @param promouvable
	 */
	private void imprimerArretePromouvable(IndividuPromouvable promouvable) {
		try {
			NSMutableArray<EOGlobalID> destinatairesGlobalIds = new NSMutableArray<EOGlobalID>();
			for (EODestinataire myDestinataire : destinatairesArrete) destinatairesGlobalIds.addObject(getEdc().globalIDForObject(myDestinataire));
			Class[] classeParametres =  new Class[] {IndividuPromouvable.class,NSArray.class};
			Object[] parametres = new Object[]{promouvable,destinatairesGlobalIds};
			UtilitairesImpression.imprimerSansDialogue(getEdc(), "clientSideRequestImprimerArretePromouvabilite",classeParametres,parametres,"ArretePromouvabilite" + promouvable.individu().noIndividu() ,"Impression de l'arrêté de promouvabilité");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}

	/**
	 * 
	 */
	private void promouvoir() {

		try {

			CRICursor.setWaitCursor(myView);

			for (IndividuPromouvable myPromouvable : (NSArray<IndividuPromouvable>)eodPromouvables.selectedObjects()) {

				// Retrouver le reliquat d'anciennete de l'element selectionne
				NSArray<EOReliquatsAnciennete> reliquats = EOReliquatsAnciennete.rechercherReliquatsNonUtilisesPourElementCarriere(getEdc(), myPromouvable.elementCarriere());
				for (EOReliquatsAnciennete reliquat : reliquats) {
					reliquat.setEstUtilisee(true);
				}

				// Retrouver la conservation d'anciennete de l'element selectionne
				NSArray<EOConservationAnciennete> conservations = EOConservationAnciennete.rechercherAnciennetesNonUtiliseesPourElementCarriere(getEdc(), myPromouvable.elementCarriere());
				for (EOConservationAnciennete conservation : conservations) {
					conservation.setEstUtilisee(true);
				}

				myPromouvable.elementCarriere().setDateFin(DateCtrl.jourPrecedent(myPromouvable.datePromotion()));
				// Crée un nouvel élément de carrière à partir du précédent
				EOElementCarriere nouvelElement = new EOElementCarriere();
				nouvelElement.initAvecElement(myPromouvable.elementCarriere());
				nouvelElement.setToTypeAccesRelationship(EOTypeAcces.findTypeAnciennete(getEdc()));
				nouvelElement.setCEchelon(myPromouvable.echelonSuivant());
					
				if (EOGrhumParametres.isNoArreteAuto()) {
					int annee = DateCtrl.getYear(nouvelElement.dateDebut());
					nouvelElement.setNoArrete(EOElementCarriere.getNumeroArreteAutomatique(getEdc(), annee, myPromouvable.individu()));
				}
				
				getEdc().insertObject(nouvelElement);
			}
			getEdc().saveChanges();
			EODialogs.runInformationDialog("", "" + eodPromouvables.selectedObjects().size() + " élément(s) de carrière créé(s)");
		} catch (Exception exc) {
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite lors de l'enregistrement des éléments de carrière.\n" + exc.getMessage());
			getEdc().revert();
		}

		PromouvabilitesCtrl.sharedInstance().toFront();
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void selectAll() {

		CRICursor.setWaitCursor(myView);
		//		eodPromouvables.selectObjectsIdenticalTo(eodPromouvables.displayedObjects());
		eodPromouvables.setSelectedObjects(eodPromouvables.displayedObjects());
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void updateUI(){

		myView.getBtnPromouvoir().setEnabled(eodPromouvables.selectedObjects().count() > 0);
		myView.getTfMessage().setText(eodPromouvables.displayedObjects().count() + " Agents");
		myView.getBtnListe().setEnabled(eodCorps.selectedObjects().count() > 0 );

		myView.getBtnImprimer().setEnabled(eodPromouvables.displayedObjects().count() > 0);
		myView.getBtnImprimerArrete().setEnabled(eodPromouvables.selectedObjects().count() > 0);
		myView.getBtnExporter().setEnabled(eodPromouvables.displayedObjects().count() > 0);

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupPeriodeListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			switch (myView.getTypesPeriode().getSelectedIndex()) {

			case 1:myView.getTfPeriodeDebut().setText(DateCtrl.dateToString(CocktailUtilities.debutAnneeCivile(new NSTimestamp())));
			myView.getTfPeriodeFin().setText(DateCtrl.dateToString(CocktailUtilities.finAnneeCivile(new NSTimestamp())));break;
			case 2:myView.getTfPeriodeDebut().setText(DateCtrl.dateToString(CocktailUtilities.debutAnneeUniversitaire(new NSTimestamp())));
			myView.getTfPeriodeFin().setText(DateCtrl.dateToString(CocktailUtilities.finAnneeUniversitaire(new NSTimestamp())));break;

			}
			listenerGrade.onSelectionChanged();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerCorps implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}

		public void onSelectionChanged() {

			setCurrentCorps((EOCorps)eodCorps.selectedObject());
			eodGrade.setObjectArray(new NSArray());
			eodPromouvables.setObjectArray(new NSArray());

			if (getCurrentCorps() != null && eodCorps.selectedObjects().count() == 1) {
				eodGrade.setObjectArray(EOGrade.rechercherGradesPourCorpsValidesEtPeriode(getEdc(), getCurrentCorps(), 
						DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText()),
						DateCtrl.stringToDate(myView.getTfPeriodeFin().getText())));
			}

			myView.getMyEOTableGrade().updateData();
			myView.getMyEOTableProm().updateData();
			updateUI();

		}
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerGrade implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}

		public void onSelectionChanged() {

			setCurrentGrade((EOGrade)eodGrade.selectedObject());
			eodPassageEchelon.setObjectArray(new NSArray());
			eodPromouvables.setObjectArray(new NSArray());

			if (getCurrentGrade() != null && eodGrade.selectedObjects().count() == 1) {
				eodPassageEchelon.setObjectArray(
						EOPassageEchelon.rechercherPassagesEchelonPourGradesValidesPourPeriode(getEdc(), getCurrentGrade(), 
								DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText()),
								DateCtrl.stringToDate(myView.getTfPeriodeFin().getText())));
			}
			myView.getMyEOTablePassage().updateData();
			myView.getMyTableModelPassage().fireTableDataChanged();
			myView.getMyEOTableProm().updateData();

			updateUI();
		}
	}
	private class ListenerPromouvables implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}

		public void onSelectionChanged() {
			setCurrentPromouvable((IndividuPromouvable)eodPromouvables.selectedObject());			
			updateUI();
		}
	}


	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			listenerGrade.onSelectionChanged();
			updateUI();
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}


}

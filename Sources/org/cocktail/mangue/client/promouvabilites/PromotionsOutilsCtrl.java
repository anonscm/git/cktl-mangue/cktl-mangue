// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.promouvabilites;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Enumeration;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.promotions.PromotionsOutilsView;
import org.cocktail.mangue.client.impression.GestionImpression;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOParamPromotion;
import org.cocktail.mangue.modele.mangue.individu.EOHistoPromotion;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class PromotionsOutilsCtrl
{
	private final static int TYPE_TOUS = 0;
	private final static int TYPE_DOSSIER_REMPLIS = 1;
	private final static int TYPE_DOSSIER_TRANSMIS_MINISTERE = 2;
	private final static int TYPE_PROMUS = 3;
	private final static int TYPE_REFUSES = 4;

	private static PromotionsOutilsCtrl sharedInstance;
	private EOEditingContext edc;
	private EODisplayGroup eodPromouvables, eodPotentiels;
	private ListenerPromouvables listenerPromouvables = new ListenerPromouvables();
	private ListenerPotentiels listenerPotentiels = new ListenerPotentiels();
	private NSArray<EOParamPromotion> parametresPromotion;
	private NSMutableArray<EOParamPromotion> parametresCourants;
	private boolean recherchePromotionEchelonPossible;
	private PromotionsOutilsView myView;
	private int numImpressionCourante;

	private Integer currentExercice;
	private EOHistoPromotion currentPromotion, currentPromotionPotentielle;

	public PromotionsOutilsCtrl(EOEditingContext edc) {

		setEdc(edc);

		eodPromouvables = new EODisplayGroup();
		eodPotentiels = new EODisplayGroup();

		myView = new PromotionsOutilsView(eodPromouvables, eodPotentiels);

		myView.getBtnExporterPromouvables().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporterPromouvables();}}
		);
		myView.getBtnExporterPotentiels().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporterPotentiels();}}
				);
		myView.getBtnImprimerPromouvables().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}
		}
				);
		myView.getBtnImprimerPotentiels().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerPotentiels();}}
				);
		myView.getBtnCalculer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {calculer();}}
				);
		myView.getBtnPromouvoir().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {promouvoir();}}
				);
		myView.getBtnRefuserPromotion().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {refuserPromotion();}}
				);
		myView.getBtnDelPromouvable().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerPromouvables();}}
		);
		myView.getBtnDetailPromouvable().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherDetailPromouvable();}}
				);
		myView.getBtnDetailPotentiel().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherDetailPotentiel();}}
				);
		myView.getBtnDelPotentiel().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	supprimerPotentiels();}}
				);

		myView.getMyEOTablePromouvables().addListener(listenerPromouvables);
		myView.getMyEOTablePotentiels().addListener(listenerPotentiels);

		myView.setListeExercices(CocktailConstantes.LISTE_ANNEES_DESC);
		myView.getPopupAnnees().addActionListener(new PopupAnneeListener());
		myView.setListeTypes(new NSArray(EOParamPromotion.typesPromotionLong));
		myView.getPopupTypes().addActionListener(new PopupTypeListener());
		myView.getPopupGrades().addActionListener(new PopupGradesListener());
		myView.getPopupFiltre().addActionListener(new PopupFiltreListener());

//		NSMutableArray mySort = new NSMutableArray();
//		mySort.addObject(new EOSortOrdering(EOHistoPromotion.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
//		mySort.addObject(new EOSortOrdering(EOHistoPromotion.INDIVIDU_KEY+"."+EOIndividu.PRENOM_KEY, EOSortOrdering.CompareAscending));
//		eodPromouvables.setSortOrderings(mySort);
//		eodPotentiels.setSortOrderings(mySort);
	}

	public static PromotionsOutilsCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new PromotionsOutilsCtrl(editingContext);
		return sharedInstance;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public Integer getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(Integer currentExercice) {
		this.currentExercice = currentExercice;
	}

	public EOHistoPromotion getCurrentPromotion() {
		return currentPromotion;
	}

	public void setCurrentPromotion(EOHistoPromotion currentPromotion) {
		this.currentPromotion = currentPromotion;
	}

	public EOHistoPromotion getCurrentPromotionPotentielle() {
		return currentPromotionPotentielle;
	}

	public void setCurrentPromotionPotentielle(
			EOHistoPromotion currentPromotionPotentielle) {
		this.currentPromotionPotentielle = currentPromotionPotentielle;
	}

	public JPanel getView() {
		return myView;
	}	

	private class PopupAnneeListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			currentExercice = null;
			if(myView.getPopupAnnees().getSelectedIndex() > 0)
				currentExercice = (Integer)myView.getPopupAnnees().getSelectedItem();
			preparerPopupLibelle();
		}
	}
	private class PopupTypeListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			preparerPopupLibelle();
		}
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupFiltreListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			int type = myView.getPopupFiltre().getSelectedIndex();

			NSMutableArray qualifiers = new NSMutableArray();
			if (type == TYPE_TOUS || type == TYPE_DOSSIER_REMPLIS)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHistoPromotion.HPRO_STATUT_KEY +"<> %@",new NSArray(EOHistoPromotion.STATUT_SUPPRIME)));
			else if (type == TYPE_DOSSIER_TRANSMIS_MINISTERE)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHistoPromotion.HPRO_STATUT_KEY +"= %@",new NSArray(EOHistoPromotion.STATUT_TRANSMIS_MINISTERE)));
			else if (type == TYPE_PROMUS)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHistoPromotion.HPRO_STATUT_KEY +"= %@",new NSArray(EOHistoPromotion.STATUT_PROMU)));
			else if (type == TYPE_REFUSES)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHistoPromotion.HPRO_STATUT_KEY +"= %@",new NSArray(EOHistoPromotion.STATUT_NEGATIF)));

			if (type == TYPE_DOSSIER_REMPLIS || type == TYPE_DOSSIER_TRANSMIS_MINISTERE || type == TYPE_PROMUS || type == TYPE_REFUSES)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHistoPromotion.HPRO_REMPLI_KEY +" = %@",new NSArray(CocktailConstantes.VRAI)));

			EOQualifier qualifier = new EOAndQualifier(qualifiers);
			eodPromouvables.setQualifier(qualifier);
			eodPromouvables.updateDisplayedObjects();
			myView.getMyEOTablePromouvables().updateData();

		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupGradesListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			// Preparation des parametres courants
			parametresCourants = new NSMutableArray();
			String libelle = (String)myView.getPopupGrades().getSelectedItem();

			for (EOParamPromotion parametre : parametresPromotion) {
				if ((parametre.parpCode().equals(libelle)))
					parametresCourants.addObject(parametre);
			}
			CocktailUtilities.viderLabel(myView.getLblCondition());
			if (parametresCourants.size() > 0) {
				CocktailUtilities.setTextToLabel(myView.getLblCondition(), parametresCourants.get(0).description());
			}

			preparerRecherchePromotionEchelon();

			actualiser();

		}
	}

	/**
	 * 
	 */
	private void preparerRecherchePromotionEchelon() {
		myView.getCheckPromotionEchelon().setEnabled(true);
		if (!EOParamPromotion.typesPromotion[myView.getPopupTypes().getSelectedIndex()].equals(EOParamPromotion.LISTE_APTITUDE)) {
			for (EOParamPromotion param : parametresPromotion) {
				if (param.gradeDepart() != null && param.cEchelon() != null && (param.parpDureeEchelon() == null || param.parpDureeEchelon().intValue() < 1))
					recherchePromotionEchelonPossible = true;
			}
			if (recherchePromotionEchelonPossible == false)
				myView.getCheckPromotionEchelon().setEnabled(false);
		} else
			myView.getCheckPromotionEchelon().setEnabled(false);
	}

	/**
	 * 
	 */
	private void preparerPopupLibelle() {

		myView.getPopupGrades().removeAllItems();
		String type = EOParamPromotion.typesPromotion[myView.getPopupTypes().getSelectedIndex()];			
		String lastLibelle = (String)myView.getPopupGrades().getSelectedItem();
		parametresCourants = null;
		if (currentExercice == null)
			return;
		else {
			parametresPromotion = EOParamPromotion.rechercherParametresPourDateEtType(getEdc(), EOHistoPromotion.datePromotionPourParametreEtAnnee(type, getCurrentExercice()), type);
			NSMutableArray codesAjoutes = new NSMutableArray();
			for (EOParamPromotion parametre : parametresPromotion) {
				if (((NSArray)codesAjoutes.valueForKey(EOParamPromotion.PARP_CODE_KEY)).containsObject(parametre.parpCode()) == false) {
					// 28/02/2011 - on masque les paramètres de promotion relevant de corps qui ne concernent pas l'établissement (HU)
					boolean estPourHU = (parametre.corpsDepart() != null && parametre.corpsDepart().toTypePopulation().estHospitalier()) ||
							(parametre.corpsArrivee() != null && parametre.corpsArrivee().toTypePopulation().estHospitalier()) ||
							(parametre.gradeDepart() != null && parametre.gradeDepart().toCorps().toTypePopulation().estHospitalier()) ||
							(parametre.gradeArrivee() != null && parametre.gradeArrivee().toCorps().toTypePopulation().estHospitalier());
					if (EOGrhumParametres.isGestionHu() || !estPourHU) {
						codesAjoutes.addObject(parametre);
					}
				}
			}
			if (codesAjoutes.count() > 0) {
				EOSortOrdering.sortArrayUsingKeyOrderArray(codesAjoutes, new NSArray(new EOSortOrdering(EOParamPromotion.PARP_CODE_KEY, EOSortOrdering.CompareAscending)));
				for (Enumeration<EOParamPromotion> e1 = codesAjoutes.objectEnumerator();e1.hasMoreElements();) {
					EOParamPromotion parametre = e1.nextElement();
					myView.getPopupGrades().addItem(parametre.parpCode());
				}
				if (lastLibelle != null && ((NSArray)codesAjoutes.valueForKey(EOParamPromotion.PARP_CODE_KEY)).containsObject(lastLibelle))
					myView.getPopupGrades().setSelectedItem(lastLibelle);
				else
					myView.getPopupGrades().setSelectedIndex(0);
			}
		}
	}

	/**
	 * 
	 */
	private void calculer() {
		
		CRICursor.setWaitCursor(myView);
		
		Class[] classeParametres = new Class[] {Integer.class, NSArray.class, Boolean.class};
		Object[] parametres = new Object[] {getCurrentExercice(),Utilitaires.tableauDeGlobalIDs(parametresCourants, getEdc()), myView.getCheckPromotionEchelon().isSelected()};
		try {
			NSTimestamp debut = new NSTimestamp();
			LogManager.logDetail("GestionPromotions - Lancement du traitement sur le serveur");
			EODistributedObjectStore store = (EODistributedObjectStore)getEdc().parentObjectStore();
			String result = (String)store.invokeRemoteMethodWithKeyPath(getEdc(),"session","clientSideRequestPreparerPromotions", classeParametres, parametres, true);
			NSTimestamp fin = new NSTimestamp();
			long duree = (fin.getTime() - debut.getTime()) / 1000;
			LogManager.logDetail("GestionPrimes - Fin du traitement sur le serveur, duree " + duree + " secondes");
			if (result == null) {
				actualiser();
				//				notifierSynchronisationPromotions();
			} else {

				eodPromouvables.setObjectArray(null);
				eodPotentiels.setObjectArray(null);
				myView.getMyEOTablePromouvables().updateData();
				myView.getMyEOTablePotentiels().updateData();

				if (result.equals("Pas d'individu promouvable !"))
					EODialogs.runInformationDialog("",result);
				else
					EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite pendant l'évaluation des promotions.\n" + result);
			}
			actualiser();

			PromouvabilitesCtrl.sharedInstance().toFront();

		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		CRICursor.setDefaultCursor(myView);
	}

	public void afficherDetailPotentiel() {
		PromotionsOutilsDetailCtrl.sharedInstance(getEdc()).open(getCurrentPromotionPotentielle());
	}
	public void afficherDetailPromouvable() {
		PromotionsOutilsDetailCtrl.sharedInstance(getEdc()).open(getCurrentPromotion());
	}

	/**
	 * 
	 */
	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		eodPromouvables.setObjectArray(EOHistoPromotion.rechercherPromotionsReellesPourParametresEtAnnee(getEdc(), parametresCourants, getCurrentExercice()));
		myView.getMyEOTablePromouvables().updateData();
		myView.getTfMessagePromouvables().setText(eodPromouvables.displayedObjects().count() + " Agents");
		eodPotentiels.setObjectArray(EOHistoPromotion.rechercherPromotionsPotentiellesPourParametreEtAnnee(getEdc(), parametresCourants, getCurrentExercice()));
		myView.getMyEOTablePotentiels().updateData();
		myView.getTfMessagePotentiels().setText(eodPotentiels.displayedObjects().count() + " Agents");

		updateUI();
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void supprimerPromouvables() {
		supprimer(eodPromouvables);
		PromouvabilitesCtrl.sharedInstance().toFront();
	}
	private void supprimerPotentiels() {
		supprimer(eodPotentiels);
		PromouvabilitesCtrl.sharedInstance().toFront();
	}

	/**
	 * 
	 * @param eod
	 */
	private void supprimer(EODisplayGroup eod) {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez vous supprimer les individus potentiellement promouvables sélectionnée ?","Oui","Non")) {
			try {
				for (java.util.Enumeration<EOHistoPromotion> e = eod.selectedObjects().objectEnumerator();e.hasMoreElements();)
					e.nextElement().invalider();
				getEdc().saveChanges();
				actualiser();
			}
			catch (Exception ex) {
				ex.printStackTrace();
				getEdc().revert();
			}
		}
	}

	/**
	 * 
	 */
	private void imprimer() {
		if (EODialogs.runConfirmOperationDialog("", "Voulez-vous imprimer la fiche individuelle ?", "Oui", "Non")) {
			// on attend qu'une impression soit terminée pour lancer la suivante
			numImpressionCourante = 0;
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("continuerImpression",new Class [] {NSNotification.class}),GestionImpression.FIN_IMPRESSION,null);
			imprimerFicheIndividuelle((EOHistoPromotion)eodPromouvables.selectedObjects().objectAtIndex(numImpressionCourante));
		} else 
			imprimerPromouvables(eodPromouvables.displayedObjects(),true);
	}
	public void continuerImpression(NSNotification aNotif) {
		if (++numImpressionCourante < eodPromouvables.selectedObjects().count()) {
			imprimerFicheIndividuelle((EOHistoPromotion)eodPromouvables.selectedObjects().objectAtIndex(numImpressionCourante));
		} else {
			NSNotificationCenter.defaultCenter().removeObserver(this, GestionImpression.FIN_IMPRESSION,null);
		}
	}

	private void exporterPromouvables() {
		exporter(eodPromouvables.displayedObjects(),true);
	}
	private void exporterPotentiels() {
		exporter(eodPotentiels.displayedObjects(),true);
	}

	private void exporter(NSArray promotions, boolean individusPromouvables) {
		CRICursor.setWaitCursor(myView);
		boolean exportComplet = EODialogs.runConfirmOperationDialog("", "Souhaitez-vous un export comportant toutes les durées d'ancienneté ?", "Oui", "Non");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourExport(promotions,individusPromouvables,exportComplet),file.getParent(),file.getName());
		}
		CRICursor.setDefaultCursor(myView);
	}
	private String textePourExport(NSArray promotions,boolean individusPromouvables,boolean estExportComplet) {
		String texte =  EOParamPromotion.typesPromotionLong[myView.getPopupTypes().getSelectedIndex()] + " pour l'année " + currentExercice + " pour la promotion : " + (String)myView.getPopupGrades().getSelectedItem() + "\n";
		texte += preparerSousTitre(promotions.count(),individusPromouvables) + "\n";
		if (estExportComplet) {
			texte += "\t\t\tAncienneté\tAncienneté\tAncienneté\tAncienneté Générale\tAncienneté\tAncienneté\t\t\t\t\t\t\n";
			texte += "Civilité\tNom\tPrénom\t dans le Corps\tdans le Grade\tdans l'Echelon\tde Service\tde Service Effectif\tCatégorie Svc Public\tCorps Origine\tGrade Origine\tEchelon\tAge\tClassement\tBarême\n";
		} else {
			texte += "\t\t\tAncienneté\tAncienneté Générale\t\t\t\t\t\t\t\n";
			texte += "Civilité\tNom\tPrénom\tdans le Grade\tde Service\tCorps Origine\tGrade Origine\tEchelon\tAge\tClassement\tBarême";
			if (parametresCourants.count() > 1) {
				texte += "\tCondition\n";
			} else {
				texte += "\n";
			}
		}

		for (java.util.Enumeration<EOHistoPromotion> e = promotions.objectEnumerator();e.hasMoreElements();) {
			EOHistoPromotion promotion = e.nextElement();
			// formatter avec , comme séparateur décimal pour excel
			texte = texte + promotion.individu().cCivilite() + "\t" + promotion.individu().nomUsuel() + "\t" + promotion.individu().prenom() + "\t";
			if (estExportComplet) {
				if (promotion.hproDureeCorps() != null) {
					texte += promotion.hproDureeCorps();
				}
				texte += "\t";
			}
			texte += promotion.hproDureeGrade() + "\t";
			if (estExportComplet) {
				if (promotion.hproDureeEchelon() != null) {
					texte += promotion.hproDureeEchelon();
				}
				texte += "\t";
			}
			texte += promotion.hproDureeServPublic() + "\t";
			if (estExportComplet) {
				if (promotion.hproDureeServEffectifs() != null) {
					texte += promotion.hproDureeServEffectifs();
				}
				texte += "\t";
				if (promotion.hproDureeCategServPublics() != null) {
					texte += promotion.hproDureeCategServPublics();
				}
				texte += "\t";
			}
			if (promotion.paramPromotion().corpsDepart() != null) {
				texte += promotion.paramPromotion().corpsDepart().llCorps();
			}
			texte += "\t" ;
			if (promotion.paramPromotion().gradeDepart() != null) {
				texte += promotion.paramPromotion().gradeDepart().llGrade();
			}
			texte += "\t" ;
			if (promotion.hproEchelon() != null) {
				texte += promotion.hproEchelon();
			}
			texte += "\t" + promotion.ageADatePromotion() + "\t";
			if (promotion.hproClassement() != null) {
				texte += promotion.hproClassement();
			}
			texte += "\t";
			if (promotion.hproBareme() != null) {
				texte += promotion.hproBareme();
			}
			if (estExportComplet && parametresCourants.count() > 1) {
				texte += "\t" + promotion.paramPromotion().description();
			}
			texte += "\n";

		}
		return texte;
	}
	private String preparerSousTitre(int nbIndividus,boolean individusPromouvables) {
		String sousTitre = "Conditions de promouvabilité :\n";

		for (java.util.Enumeration<EOParamPromotion> e = parametresCourants.objectEnumerator();e.hasMoreElements();) {
			EOParamPromotion parametre = e.nextElement();
			sousTitre += parametre.description() + "\n";
		}
		if (!individusPromouvables) {
			sousTitre += "\nNombre de Candidats Potentiels : ";
		} else {
			int type = myView.getPopupFiltre().getSelectedIndex();
			switch(type) {
			case TYPE_TOUS :
				sousTitre += "\nNombre de Promouvables : ";
				break;
			case TYPE_DOSSIER_REMPLIS :
				sousTitre += "\nNombre de Promouvables ayant remis leur dossier : ";
				break;
			case TYPE_DOSSIER_TRANSMIS_MINISTERE :
				sousTitre += "\nNombre de Promouvables dont le dossier a été transmis au Ministère : ";
				break;
			case TYPE_PROMUS :
				sousTitre += "\nNombre de Promus : ";
				break;
			case TYPE_REFUSES :
				sousTitre += "\nNombre de Promotions refusées : ";
				break;
			}
		}
		sousTitre +=  nbIndividus + "\n";
		return sousTitre;
	}

	public void imprimerPotentiels() {
		LogManager.logDetail(this.getClass().getName() + "imprimerPotentiels");
		imprimerPromouvables(eodPotentiels.displayedObjects(),false);
	}
	private void imprimerPromouvables(NSArray promouvables, boolean individusPromouvables) {
		Class[] classeParametres =  new Class[] {Integer.class,NSArray.class, String.class,String.class};
		String titre = EOParamPromotion.typesPromotionLong[myView.getPopupTypes().getSelectedIndex()] + 
				" pour les " + ((String)myView.getPopupGrades().getSelectedItem());

		Object[] parametres = new Object[]{currentExercice,Utilitaires.tableauDeGlobalIDs(promouvables, getEdc()),titre,preparerSousTitre(promouvables.count(),individusPromouvables)};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(getEdc(),"clientSideRequestImprimerPromotions",classeParametres,parametres,"Promouvabilites","Impression des promouvabilités");
		} catch (Exception exc) {
			LogManager.logException(exc);
			EODialogs.runErrorDialog("Erreur",exc.getMessage());
		}
	}
	
	/**
	 * 
	 * @param promotion
	 */
	private void imprimerFicheIndividuelle(EOHistoPromotion promotion) {
		Class[] classeParametres =  new Class[] {EOGlobalID.class,NSTimestamp.class,};
		NSTimestamp dateReference = EOHistoPromotion.datePromotionPourParametreEtAnnee(EOParamPromotion.typesPromotion[myView.getPopupTypes().getSelectedIndex()], currentExercice);
		Object[] parametres = new Object[]{getEdc().globalIDForObject(promotion),dateReference};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(getEdc(),"clientSideRequestimprimerFicheLATA",classeParametres,parametres,"Promotions","Impression des fiches individuelles de promotions");
		} catch (Exception exc) {
			LogManager.logException(exc);
			EODialogs.runErrorDialog("Erreur",exc.getMessage());
		}
	}

	/**
	 * 
	 */
	public void promouvoir() {
		try {
			NSArray<EOHistoPromotion> promotionsSelectionnees = eodPotentiels.selectedObjects();
			NSMutableArray promotions = new NSMutableArray(eodPromouvables.displayedObjects());
			for (EOHistoPromotion promotion : promotionsSelectionnees) {
				promotion.indiquerManuelle();
				promotion.setDModification(new NSTimestamp());
				promotions.addObject(promotion);
			}
			eodPromouvables.setObjectArray(promotions);
			promotions = new NSMutableArray(eodPotentiels.displayedObjects());
			promotions.removeObjectsInArray(promotionsSelectionnees);
			eodPotentiels.setObjectArray(promotions);

			myView.getMyEOTablePromouvables().updateData();
			myView.getMyEOTablePotentiels().updateData();

			getEdc().saveChanges();

			actualiser();

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void refuserPromotion() {

		try {
			NSArray<EOHistoPromotion> promotionsSelectionnees = eodPromouvables.selectedObjects();
			NSMutableArray promotions = new NSMutableArray(eodPotentiels.displayedObjects());
			for (EOHistoPromotion promotion : promotionsSelectionnees) {
				if (!promotion.aRempliDossier()) {
					promotion.rendreProvisoire();
					promotion.setDModification(new NSTimestamp());
					promotions.addObject(promotion);
				}
			}
			eodPotentiels.setObjectArray(promotions);
			promotions = new NSMutableArray(eodPromouvables.displayedObjects());
			promotions.removeObjectsInArray(promotionsSelectionnees);
			eodPromouvables.setObjectArray(promotions);
			getEdc().saveChanges();
			actualiser();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateUI(){

		myView.getBtnPromouvoir().setEnabled(currentPromotionPotentielle != null);
		myView.getBtnRefuserPromotion().setEnabled(currentPromotion != null && !currentPromotion.aRempliDossier());

		myView.getBtnDelPromouvable().setEnabled(eodPromouvables.selectedObjects().count() > 0);
		myView.getBtnDelPotentiel().setEnabled(eodPotentiels.selectedObjects().count() > 0);

		myView.getBtnExporterPromouvables().setEnabled(eodPromouvables.displayedObjects().count() > 0);
		myView.getBtnExporterPotentiels().setEnabled(eodPotentiels.displayedObjects().count() > 0);

		myView.getBtnImprimerPromouvables().setEnabled(eodPromouvables.selectedObjects().count() > 0);
		myView.getBtnImprimerPotentiels().setEnabled(eodPotentiels.selectedObjects().count() > 0);

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerPromouvables implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
			//			if (!currentPromotion.aRempliDossier())
			//				refuserPromotion();
		}

		public void onSelectionChanged() {
			currentPromotion = (EOHistoPromotion)eodPromouvables.selectedObject();
			updateUI();
		}
	}
	private class ListenerPotentiels implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
		}

		public void onSelectionChanged() {

			currentPromotionPotentielle = (EOHistoPromotion)eodPotentiels.selectedObject();
			updateUI();
		}
	}


}

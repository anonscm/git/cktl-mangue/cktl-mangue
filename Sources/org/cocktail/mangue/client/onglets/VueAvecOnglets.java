/*
 * Created on 9 mars 2006
 *
 * Gère une interface sous forme de vues à onglets, éventuellement imbriquées les unes dans les autres.
 * Un path correspond au nom de la fenêtre concaténé à l'ensemble des clics sur les onglets pour afficher la vue voulue.
 * Cette interface repose sur le fait que les Strings des items de menu et ceux des onglets sont identiques et que 
 * la hiérarchie des menus correspondent à celles des onglets dans les fenêtre : un nom de menu correspond à un nom de 
 * fenêtre, un sous-menu correspond à une
 * vue à onglets et un item terminal de menu à une tab d'onglets
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.onglets;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.AgentsCtrl;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EOAssociation;
import com.webobjects.eointerface.swing.EOSwingUtilities;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/**  G&egrave;re une interface sous forme de vues &agrave onglets, &eacute;ventuellement imbriqu&eacute;es les unes dans les autres.
 * Un path correspond &agrave; l'ensemble des clics sur les onglets pour afficher la vue voulue.<BR>
 * Cette interface utilise le fait que les Strings des items de menu et ceux des onglets sont identiques<BR>
 * Exemple : <BR>
 * Menu Employ&eacute;/Infos/Etat Civil => onglet "Etat Civil" de l'onglet "Infos" de la fen&ecirc;tre "Employ&eacute;"<BR>
 * EOPoste des notifications pour signaler l'onglet s&eacute;lectionn&eacute;<BR>
 * Supporte les notifications utilis&eacute;es pour le locking en cas de modification
 */

public class VueAvecOnglets extends EOInterfaceController implements ChangeListener {
	private int positionHoriz,positionVertic;
	private String nomFenetre;
	private boolean activationOngletEnCours;
	private boolean fermetureExterieure;
	private NSMutableDictionary dictionnaireControleurs;	// dictionnaire des contrôleurs : clé nom de la classe du contrôleur

	public VueAvecOnglets() {
		super();
		dictionnaireControleurs = new NSMutableDictionary();
	}
	public VueAvecOnglets(EOEditingContext substitutionEditingContext) {
		super(substitutionEditingContext);
		
	}
	/** Initialiser le controleur
	 * @param X position horizontale de la fenetre
	 * @param Y position verticale de la fenetre
	 */
	public void initialiser(String nomFenetre,int X,int Y, Number noIndividu) {
		positionHoriz = X;
		positionVertic = Y;
		fermetureExterieure = false;
		this.nomFenetre = nomFenetre;
	}
	public void connectionWasEstablished() {
		preparerFenetre();
	}
	public void connectionWasBroken() {
		if (!fermetureExterieure) {
			terminer();
		}
	}
	public void arreter() { 
		terminer();
		if (supercontroller() instanceof EOFrameController) {
			((EOFrameController)supercontroller()).closeWindow();
		}
		fermetureExterieure = true;
	}
	// actions
	public void afficherFenetre() {

		((EOFrameController)supercontroller()).activateWindow();
	}
	public void fermer() { 
		((ApplicationClient)EOApplication.sharedApplication()).supprimerControleurPourAction(this);
		((EOFrameController)supercontroller()).closeWindow();
		fermetureExterieure = true;
	}
	// Notifications
	// LOCK ECRAN
	public void lockSaisie(NSNotification aNotif) {
		if (aNotif.object() != this) {
			lockerFenetre(true);
		}
	} 
	// DELOCK ECRAN
	public void unlockSaisie(NSNotification aNotif) {
		if (aNotif.object() != this) {
			lockerFenetre(false);
		}
	} 
	// Autres méthodes
	public void decalerFenetre(int X,int Y) {
		if (supercontroller() != null) {
			((EOFrameController)supercontroller()).window().setLocation(X,Y);
		}
	}
	public boolean aChargeControleurPourClasse(String nomClasse) {
		return dictionnaireControleurs.objectForKey(nomClasse) != null;
	}
	/** ajoute le controleur a l'onglet lie au path passe; en parametre puis affiche l'onglet */
	public void ajouterControleurPourOngletAvecPath(ModelePage controleur,String path,String pathDelimiteur) {
		NSMutableArray titres = new NSMutableArray(NSArray.componentsSeparatedByString(path,pathDelimiteur));
		
		// vérifier si le premier item du path correspond au titre de la fenêtre, on démarrre alors au suivant
		String titreFenetre = ((JFrame)((EOFrameController)supercontroller()).window()).getTitle();

		if (titres.objectAtIndex(0).equals(titreFenetre)) {
			titres.removeObjectAtIndex(0);
		}
		
		JTabbedPane pane = panePourPath(titres.componentsJoinedByString(pathDelimiteur),pathDelimiteur);
		
		if (pane != null && controleur.component() != null) {

			int index = pane.indexOfTab((String)titres.lastObject());	// titre de l'onglet
			JPanel vue = (JPanel)pane.getComponentAt(index);

			 if (EOSwingUtilities._isRunningOnMacPlatform()) {
				GraphicUtilities.swaperViewEtCentrerDansEOView(vue, controleur.component());
			 } else {
			
				 GraphicUtilities.swaperViewEtCentrer(vue, controleur.component());
			 }

			controleur.activer();

			if (controleur.controllerDisplayGroup() != null) {	// 16/03/2011 - pour être sûr que les associations sont OK
				for (java.util.Enumeration<EOAssociation> e = controleur.controllerDisplayGroup().observingAssociations().objectEnumerator();
									e.hasMoreElements();) {
					EOAssociation association = e.nextElement();
					association.subjectChanged();
				}
			}
			activerPath(titres.componentsJoinedByString(pathDelimiteur),pathDelimiteur);
			dictionnaireControleurs.setObjectForKey(controleur,controleur.getClass().getName());
		}
	}
	/** active la premiere vue a onglets trouvee en utilisant le path fourni en parametre 
	 * pour descendre dans les vues a onglets */
	public void activerOngletPourPath(String path,String pathDelimiteur) {
		NSMutableArray titres = new NSMutableArray(NSArray.componentsSeparatedByString(path,pathDelimiteur));
		// vérifier si le premier item du path correspond au titre de la fenêtre, on démarrre alors au suivant
		String titreFenetre = ((JFrame)((EOFrameController)supercontroller()).window()).getTitle();
		if (titres.objectAtIndex(0).equals(titreFenetre)) {
			titres.removeObjectAtIndex(0);
		}
		activerPath(titres.componentsJoinedByString(pathDelimiteur),pathDelimiteur);
	}


	/**
	 * 
	 */
	public void stateChanged(ChangeEvent e) {
		Object source = e.getSource();
		if (activationOngletEnCours) {	// le selectIndex a été fait par le programme
			return;
		}
		if (source instanceof JTabbedPane) {
			JTabbedPane onglet = (JTabbedPane)source;
			String nomOnglet = onglet.getTitleAt(onglet.getSelectedIndex());
			AgentsCtrl.sharedInstance().setActivePane(nomOnglet);
			JComponent vuePourOnglet = (JComponent)onglet.getSelectedComponent();
			// vérifier si cet onglet contient lui-même une vue à onglet (récursivement), en quel cas activer le premier onglet de la nouvelle vue à onglets
			boolean found = false;
			while (!found) {
				JTabbedPane nouvelOnglet = ongletPourVue(new NSArray(vuePourOnglet.getComponents()));
				if (nouvelOnglet != null) {
					nomOnglet = nouvelOnglet.getTitleAt(0);
					if (nouvelOnglet.getComponentAt(0) instanceof JComponent) {
						vuePourOnglet = (JComponent)nouvelOnglet.getComponentAt(0);
					} else {
						found = true;
					}
				} else {
					found = true;
				}
			}
			NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,nomOnglet);
		}
	}

	/** methode a surcharger pour faire des initialisations une fois l'archive chargee en invoquant
	 * un appel a la methode de la super classe */
	protected void preparerFenetre() {
		preparerInterface(new NSArray(component().getComponents()));

		NSArray composants = new NSArray(component().getComponents());
		preparerNomOnglets(composants,null,1);
		modifierChangeListener(composants,true);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockSaisie", new Class[] { NSNotification.class }),ModelePage.LOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("unlockSaisie", new Class[] { NSNotification.class }), ModelePage.DELOCKER_ECRAN, null);
	}
	/** permet d'interdire/activer la fermeture de la fenetre en supprimant/ajoutant le window listener<BR>
	 * 	active/desactive la vue a onglets*/
	protected void lockerFenetre(boolean lock) {
		changerVerrou(new NSArray(component().getComponents()),!lock);
	}

	// méthodes privées
	private void activerPath(String path,String pathDelimiteur) {
		activationOngletEnCours = true;
		NSArray components = new NSArray(component().getComponents());
		NSArray titres = NSArray.componentsSeparatedByString(path,pathDelimiteur);
		for (java.util.Enumeration<String> e = titres.objectEnumerator();e.hasMoreElements();) {
			String titre = e.nextElement();
			JTabbedPane currentPane = vueOngletsPourTitre(components,titre);
			if (currentPane != null) {
				currentPane.setSelectedIndex(currentPane.indexOfTab(titre));
				if (currentPane.getSelectedComponent() instanceof JComponent)
					components = new NSArray(((JComponent)currentPane.getSelectedComponent()).getComponents());
			} else {
				break;
			}
		}
		activationOngletEnCours = false;
	}
	// retourne parmi les composants la première vue à onglets trouvée en utilisant le path fourni en paramètre 
	// pour descendre dans les vues &agrave; onglets
	private JTabbedPane panePourPath(String path,String pathDelimiteur) {
		
		NSArray titres = NSArray.componentsSeparatedByString(path,pathDelimiteur);
		NSArray components = new NSArray(component().getComponents());
		JTabbedPane pane = null;
		for (java.util.Enumeration<String> e = titres.objectEnumerator();e.hasMoreElements();) {
			String titre = e.nextElement();
			pane = vueOngletsPourTitre(components,titre);
			if (pane != null && pane.getComponentAt(pane.indexOfTab(titre)) instanceof JComponent) {
				// rechercher dans l'onglet concerné si il y a des sous-composants
				components = new NSArray(((JComponent)pane.getComponentAt(pane.indexOfTab(titre))).getComponents());
			} else
				break;
		}

		return pane;
	}

	// recherche parmi les vues à onglets et recherche dans la liste des menus, le titre de chaque onglet
	// descend récursivement dans les composants de la vue à onglets trouvée
	private void preparerNomOnglets(NSArray components,String nomVueOnglets,int niveau) {
		for (java.util.Enumeration<JComponent> e = components.objectEnumerator();e.hasMoreElements();) {
			JComponent component = e.nextElement();
			if (component instanceof JTabbedPane) {
				JTabbedPane vueOnglet = (JTabbedPane)component;
				NSArray nomsOnglets = NomenclatureOngletMenu.titresPourOnglets(nomFenetre,nomVueOnglets,niveau);

				for (int i = 0; i < vueOnglet.getTabCount();i++) {
					String nom = (String)nomsOnglets.objectAtIndex(i);
					vueOnglet.setTitleAt(i,nom);	// changer le titre de l'onglet
					JComponent sousComposant = (JComponent)vueOnglet.getComponentAt(i);	
					NSArray composants = new NSArray(sousComposant.getComponents());	// récupérer les composants de cet onglet
					if (ongletPourVue(composants) != null) {		// si parmi ces composants, il y a une autre vue à onglet
						preparerNomOnglets(composants,vueOnglet.getTitleAt(i),niveau + 1);
					}
					// interdire l'onglet si les droits ne sont pas autorisés
					String nomMethode = ((ApplicationClient)EOApplication.sharedApplication()).methodePourNomAction(nom);
					if (nomMethode != null) {
						vueOnglet.setEnabledAt(i,((ApplicationClient)EOApplication.sharedApplication()).canPerformActionNamed(nomMethode));
					}
				}
			}
		}
	}
	
	private JTabbedPane vueOngletsPourTitre(NSArray components,String titre) {
		for (java.util.Enumeration<Component> e = components.objectEnumerator();e.hasMoreElements();) {
			Component component = e.nextElement();
			if (component instanceof JTabbedPane) {
				JTabbedPane pane = (JTabbedPane)component;
				for (int i = 0; i < pane.getTabCount();i++) {
				}
				if (pane.indexOfTab(titre) >= 0) {
					return pane;
				}
			}
		}
		return null;
	}
	
	private JTabbedPane ongletPourVue(NSArray components) {
		for (java.util.Enumeration<Component> e = components.objectEnumerator();e.hasMoreElements();) {
			Component component = (Component)e.nextElement();
			if (component instanceof JTabbedPane) {
				return (JTabbedPane)component;
			} 
		}
		return null;
	}
	
	private void terminer() {
		((ApplicationClient)EOApplication.sharedApplication()).supprimerControleurPourAction(this);
		for (java.util.Enumeration<ModelePage> e = dictionnaireControleurs.objectEnumerator();e.hasMoreElements();) {
			ModelePage controleur = e.nextElement();
			controleur.arreter();
		}
	}
	private void modifierChangeListener(NSArray components,boolean ajouter) {
		for (java.util.Enumeration<JComponent> e = components.objectEnumerator();e.hasMoreElements();) {
			JComponent component = e.nextElement();
			if (component instanceof JTabbedPane) {
				JTabbedPane pane = (JTabbedPane)component;
				if (ajouter) {
					pane.addChangeListener(this);
				} else {
					pane.removeChangeListener(this);
				}
				for (int i = 0; i < pane.getTabCount();i++) {
					Component paneComponent = pane.getComponentAt(i);
					if (paneComponent  instanceof JComponent) {
						// rechercher dans l'onglet concerné si il y a d'autres vues à onglets
						components = new NSArray(((JComponent)paneComponent).getComponents());
						modifierChangeListener(components,ajouter);
					}
				}
			}
		}
	}
	private void changerVerrou(JTabbedPane vueOnglets,boolean enabled) {
		for (int i = 0; i < vueOnglets.getTabCount();i++) {
			vueOnglets.setEnabledAt(i,enabled);
		}
	}
	private void changerVerrou(NSArray components,boolean enabled) {
		for (java.util.Enumeration<JComponent> e = components.objectEnumerator();e.hasMoreElements();) {
			JComponent component = e.nextElement();
			if (component instanceof JTabbedPane) {
				changerVerrou((JTabbedPane)component,enabled);
			}
			if (component instanceof JPanel || component instanceof EOView || component instanceof JTabbedPane) {
				changerVerrou(new NSArray(component.getComponents()),enabled);
			}
		}
	}
	public void preparerInterface(NSArray components) {
		for (java.util.Enumeration<Component> e = components.objectEnumerator();e.hasMoreElements();) {
			Component component = e.nextElement();
			if (component instanceof JPanel) {
				NSArray componentsVue = new NSArray(((JPanel)component).getComponents());
				preparerInterface(componentsVue);
			} else {
				if (component instanceof JTabbedPane) {
					JTabbedPane pane = (JTabbedPane)component;
					for(int i = 0; i < pane.getTabCount(); i++) {
						JPanel sousPane = (JPanel)pane.getComponentAt(i);
						NSArray componentsVue = new NSArray(sousPane.getComponents());
						preparerInterface(componentsVue);
					}
				}
			}
		}

	}

}

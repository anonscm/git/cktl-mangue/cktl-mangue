/*
 * Affichage des données Agent sous forme de vue à onglets
 *
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.onglets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.individu.FicheSituationAgentCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailIcones;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOGestionnaires;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;


/**  Affichage des donnees Agent sous forme de vue a onglets
 * @author christine
 *
 */
public class GestionEmploye extends VueAvecOnglets {

	public JTextField tfInfosAgent;
	public JButton btnFicheIdentite;
	private EOIndividu currentIndividu;
	private String texteVersion, texteAgent;
	private EOAgentPersonnel agent;

	// Accesseurs
	public String texteVersion() {
		return texteVersion;
	}
	public void setTexteVersion(String texteVersion) {
		this.texteVersion = texteVersion;
	}

	public String texteAgent() {
		return texteAgent;
	}
	public void setTexteAgent(String texteAgent) {
		this.texteAgent = texteAgent;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}
	/** Initialiser le controleur
	 * @param X position horizontale de la fenetre
	 * @param Y position verticale de la fenetre
	 */
	public void initialiser(String nomFenetre,int X,int Y, Number noIndividu) {
		super.initialiser(nomFenetre, X, Y, noIndividu);
		preparerTextePourIndividu(noIndividu);
	}
	// Notifications
	public void nettoyer(NSNotification aNotif) {
		preparerTextePourIndividu(null);
	}
	public void employeHasChanged(NSNotification aNotif) {
		preparerTextePourIndividu((Number)aNotif.object());
	}

	protected void preparerFenetre() {

		super.preparerFenetre();

		agent = ((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel();

		btnFicheIdentite.setIcon(CocktailIcones.ICON_LOUPE_16);

		btnFicheIdentite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (getCurrentIndividu() != null) {
					CRICursor.setWaitCursor(btnFicheIdentite);
					FicheSituationAgentCtrl.sharedInstance(editingContext()).open(getCurrentIndividu());
					CRICursor.setDefaultCursor(btnFicheIdentite);
				}
			}}
				);

		setTexteVersion(((ApplicationClient)EOApplication.sharedApplication()).infosPourApplication());
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("employeHasChanged", new Class[] { NSNotification.class }), ListeAgents.CHANGER_EMPLOYE, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("nettoyer", new Class[] { NSNotification.class }), ListeAgents.NETTOYER_CHAMPS, null);

	}

	
	/**
	 * 
	 */
	public void imprimerFicheIdentite() {

		CRICursor.setWaitCursor(this);
		try {
			Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class,Boolean.class,Boolean.class};
			NSMutableArray donneesRecherchees = new NSMutableArray();
			Object[] parametres = new Object[]{editingContext().globalIDForObject(getCurrentIndividu()),donneesRecherchees,new Boolean(agent.peutAfficherInfosPerso()), false};
			UtilitairesImpression.imprimerSansDialogue(editingContext(),"clientSideRequestImprimerFicheIdentite",classeParametres,parametres,"FicheIdentite_" + getCurrentIndividu().noIndividu(),"Impression de la fiche d'identité");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		CRICursor.setDefaultCursor(this);
	}


	
	/**
	 * 
	 * @param noIndividu
	 */
	private void preparerTextePourIndividu(Number noIndividu) {

		try {

			setTexteAgent("---");

			if (noIndividu != null) {
				setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(editingContext(),noIndividu,false));
				if (getCurrentIndividu() != null)
					setTexteAgent(getCurrentIndividu().identite());
				setTexteAgent(texteAgent() + " - " + ((ApplicationClient)EOApplication.sharedApplication()).getSituationAgent(getCurrentIndividu()));
			}

			if (EOGrhumParametres.isUseGestionnaire() && noIndividu != null) {

				EOGestionnaires gestionnaire = EOGestionnaires.getPersonneGestionnaire(editingContext(), getCurrentIndividu());
				if (gestionnaire != null && gestionnaire.toStructure() != null) {
					String libelleStructure = gestionnaire.toStructure().lcStructure();
					setTexteAgent(texteAgent() + "   ( Gest : " + libelleStructure + " )");
				}
			}

			tfInfosAgent.setText(texteAgent());			
			controllerDisplayGroup().redisplay();

		}
		catch (Exception e) {
		}
	}
}

/*
 * Created on 14 mars 2006
 *
 *	Gère les titres des onglets qui sont aussi utilisés pour les menus
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.onglets;

import org.cocktail.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOAction;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/** Gere les titres des onglets qui sont aussi utilises comme items de menus<BR>
 * L'ordre des titres dans les tables determine l'ordre des onglets et l'ordre des menus et des items de menu<BR>
 * Les noms des onglets terminaux doivent etre uniques<BR>
 * @author christine
 * 
 */
public class NomenclatureOngletMenu {
	/* Pour ajouter un item de menu :
	 * 	- modifier la table de titres du menu concerné et modifier la table des actions de ce menu. Modifier les fichiers d'interface des vues à onglets
	 * Pour ajouter un menu :
	 * 	- ajouter une table de titre de menu et ajouter une table des actions. Ajouter une vue à onglets (nib et java) au projet
	 * 	- modifier la méthode preparerDictionnaire
	 * 	- modifier la méthode actionsPourMenu.
	 * 	- modifier la méthode tousTitresOnglets
	 * 	- modifier la méthode titrePourMethode
	 * 	- modifier la méthodes droitsPourMethode
	 * 	Les items de menu doivent être rangés dans l'ordre d'affichage
	 */
	// Attention, certains items de menu sont déclenché par une notification ACTIVER_ACTION : Etat Civil, Enfants, Synthèse Carrière
	// Penser à modifier les notifications si les onglets sont renommés (faire une recherche globale sur ACTIVER_ACTION)

	// EMPLOYES
	private static String[] TITRES_MENU_EMPLOYE = {
		"Employé/INFOS",
		"Employé/CONTRATS/Contrats",
		"Employé/CONTRATS/Vacations",
		"Employé/CONTRATS/Hébergé",
		"Employé/CARRIERE/Segments",					
		"Employé/CARRIERE/MAD",
		"Employé/CARRIERE/Prolongations d'activité",
		"Employé/CARRIERE/Promotions",
		"Employé/PASSÉ",
		"Employé/AFF. - OCC.",
		"Employé/MODALITÉS",
		"Employé/CONGÉS/Congés Légaux",
		"Employé/CONGÉS/Congés Bonifiés",
		"Employé/CONGÉS/Congés Fin Activité",
		"Employé/PRIMES",
		"Employé/BUDGET",
		"Employé/SYNTHESES/Ancienneté",
		"Employé/SYNTHESES/Carrière"
	};
	private static String[] ACTIONS_MENU_EMPLOYE = {
		"afficherEtatCivil",
		"afficherSwapViewContrats",
		"afficherSwapViewVacations",
		"afficherSwapViewHeberge",
		"afficherSwapViewCarriere",							
		"afficherMAD",
		"afficherSwapViewProlongations",
		"afficherPromotionsIndividu",
		"afficherSwapViewPasse",
		"afficherSwapViewAffOcc",
		"afficherSwapViewModalites",
		"afficherConges",
		"afficherCongeNbi",
		"afficherCFA",
		"afficherPrimesIndividu",
		"afficherSwapViewBudget",
		"afficherAnciennete",//"afficherAnciennete",						
		"afficherSynthese"
	};	
	private static String[][] DROITS_MENU_EMPLOYE = {
		{"agtConsIdentite","agtUpdIdentite"},		// Etat civil
		{"agtConsContrat","agtUpdContrat"},			// Contrats
		{"agtVacations","agtVacations"},			// Vacations
		{"agtGereHeberge","agtGereHeberge"},		// Heberges
		{"agtConsCarriere","agtUpdCarriere"},		// Carriere
		{"agtConsCarriere","agtUpdCarriere"},		// MAD
		{"agtConsCarriere","agtUpdCarriere"},		// Prolongations
		{"agtConsCarriere","agtUpdCarriere"},		// Promotions
		{"agtConsCarriere","agtUpdCarriere"},		// Passe
		{"agtConsOccupation","agtUpdOccupation"},	// Occupations
		{"agtConsCarriere","agtUpdCarriere"},									// Modalites de service
		{"agtConsConges","agtUpdConges"},										// Conges
		{"agtConsConges","agtUpdConges"},										// NBI
		{"agtConsConges","agtUpdConges"},										// CFA 
		{"agtPrime","agtPrime"},												// primes
		{"agtBudget"},	// Budget
		{"agtConsCarriere","agtUpdCarriere"},	// Anciennete
		{"agtConsCarriere","agtUpdCarriere"}	// Synthese
	};


	// ADMINISTRATION
	private static String[] TITRES_MENU_ADMINISTRATION = {
		"Admin/Agents",
		"Admin/Etablissement",
		"Admin/Paramètres/Paramètres Promotion",
		"Admin/Paramètres/Destinataires",
		"Admin/Impression/Modules Impression",
		"Admin/Impression/Modules pour Requêtes",	
		"Admin/Impression/Modèles pour Contrat",	//10
		"Admin/Elections/Type Election",
		"Admin/Elections/Election",
		"Admin/Elections/Collège",
		"Admin/Elections/Comp. Elective",
		"Admin/Elections/Section Elective",		
		"Admin/Elections/College-Section. Elec.",	
		"Admin/Elections/Secteur",					
		"Admin/Elections/Bureau de vote",
		"Admin/Elections/Exclusion",			
		"Admin/Elections/Inclusion/Corps",			//20
		"Admin/Elections/Inclusion/Type de Contrat",
		"Admin/Elections/Inclusion/Diplôme",
		"Admin/Gestion Visas/Visas ",
		"Admin/Gestion Visas/Arrêtés Titulaires",
		"Admin/Gestion Visas/Arrêtés Contractuels",	
		"Admin/Gestion Visas/Arrêtés Corps",		
		"Admin/Gestion Visas/Arrêtés Contrats"	
	};
	private static String[] ACTIONS_MENU_ADMINISTRATION  = {
		"afficherAgents",
		"afficherServices",
		"afficherParametresPromotion",
		"afficherDestinataires",
		"afficherModulesImpression",
		"afficherModulesImpressionIndividu",		
		"afficherModelesPourContrat",				//10
		"afficherTypeElection",
		"afficherElection",
		"afficherCollege",
		"afficherComposante",
		"afficherSection",						
		"afficherCollegeSection",
		"afficherSecteur",						
		"afficherBureau",
		"afficherExclusion",
		"afficherInclusionCorps",				//20
		"afficherInclusionTypeContrat",
		"afficherInclusionDiplome",
		"afficherVisas",
		"afficherVisasCongesTitulaires",
		"afficherVisasCongesContractuels",		
		"afficherVisasArretesCorps",
		"afficherVisasContrats"		
	};
	private static String[][] DROITS_MENU_ADMINISTRATION = {
		{"agtConsAgents","agtUpdAgents"},
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},	
		{"agtAdministration","agtAdministration"},	//10
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},	
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},		
		{"agtAdministration","agtAdministration"},		
		{"agtAdministration","agtAdministration"},  //20
		{"agtAdministration","agtAdministration"},		
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},	
		{"agtAdministration","agtAdministration"},
		{"agtAdministration","agtAdministration"},	
		{"agtAdministration","agtAdministration"}
	};

	// OUTILS
	private static String[] TITRES_MENU_OUTIL = {
		"Outils/Editions/Personnels",
		"Outils/Editions/Congés",
		"Outils/Editions/Activités",
		"Outils/Editions/Services",
		"Outils/Editions/Divers",
		"Outils/Etat des Personnels",
		"Outils/DIF",
		"Outils/Dossier Médical/Médecins",
		"Outils/Dossier Médical/Responsables Acmo",
		"Outils/Dossier Médical/Edition Examens",
		"Outils/Dossier Médical/Edition Vaccins",
		"Outils/Dossier Médical/Edition Visites",
		"Outils/Extractions/Etat Comparatif",
		"Outils/Elections/Paramétrage",	
	"Outils/Elections/Liste Electorales"};		//20
	private static String[] ACTIONS_MENU_OUTIL = {
		"afficherEditeurRequetes",
		"afficherEditionConges",
		"afficherEditionActivites",
		"afficherEditionServices",
		"afficherEditionDivers",
		"afficherEtatPersonnels",
		"afficherCifs",
		"afficherMedecins",
		"afficherResponsablesAcmo",
		"afficherEditionExamens",
		"afficherEditionVaccins",
		"afficherEditionsVisites",
		"afficherExtractionEtatComparatif",
		"afficherParametrageElection",
		"afficherListesElectorales"
	};
	private static String[][] DROITS_MENU_OUTIL = {
		{"agtEditionRequetes","agtEditionRequetes"},
		{"agtEditionRequetes","agtEditionRequetes"},
		{"agtEditionRequetes","agtEditionRequetes"},
		{"agtEditionRequetes","agtEditionRequetes"}, 
		{"agtEditionRequetes","agtEditionRequetes"},
		{"agtOutils","agtOutils"},
		{"agtOutils","agtOutils"},
		{"agtOutils","agtOutils"},
		{"agtOutils","agtOutils"},
		{"agtOutils","agtOutils"},
		{"agtOutils","agtOutils"},
		{"agtOutils","agtOutils"},
		{"agtOutils","agtOutils"},
		{"agtElection","agtElection"},
		{"agtElection","agtElection"}};


	// POSTES
	private static String[] TITRES_MENU_POSTE = {"Poste/Postes"};
	private static String[] ACTIONS_MENU_POSTE = {"afficherPostes"};
	private static String[][] DROITS_MENU_POSTE = {{"agtConsPostes","agtUpdPostes"}};

	// PRIMES
	private static String[] TITRES_MENU_PRIMES = {
		"Rémun/Administration/Paramétrage des Primes",
		"Rémun/Administration/Exclusions/Prime",
		"Rémun/Administration/Exclusions/Position",
		"Rémun/Administration/Exclusions/Congé",
		"Rémun/Administration/Fonctions",
		"Rémun/Administration/Populations",
		"Rémun/Administration/Paramètres Primes",
		"Rémun/Administration/Programmes",
		"Rémun/Administration/Structures",
		"Rémun/Budget","Rémun/Personnalisation des Indemnités",
	"Rémun/Attribution des Indemnités"};
	private static String[] ACTIONS_MENU_PRIMES = {
		"afficherAdministrationPrimes", 
		"afficherPrimeExclusions",
		"afficherPrimeExclusionsPosition",
		"afficherPrimeExclusionsConge", 
		"afficherPrimeFonctions",
		"afficherPrimePopulations",
		"afficherPrimeParametres",
		"afficherPrimeProgrammes",
		"afficherPrimeStructures",
		"afficherBudget", 
		"afficherArretesPrime",
	"afficherPrimes"};
	private static String[][] DROITS_MENU_PRIMES = {{"agtPrime","agtPrime","agtAdministration","agtAdministration"},
		{"agtPrime","agtPrime","agtAdministration","agtAdministration"},
		{"agtPrime","agtPrime","agtAdministration","agtAdministration"},{"agtPrime","agtPrime","agtAdministration","agtAdministration"},
		{"agtPrime","agtPrime","agtAdministration","agtAdministration"},{"agtPrime","agtPrime","agtAdministration","agtAdministration"},
		{"agtPrime","agtPrime","agtAdministration","agtAdministration"},{"agtPrime","agtPrime","agtAdministration","agtAdministration"},
		{"agtPrime","agtPrime","agtAdministration","agtAdministration"},{"agtPrime","agtPrime"},{"agtPrime","agtPrime"},{"agtPrime","agtPrime"}};

	// EMPLOI
	private static String[] TITRES_MENU_EMPLOI = {"Emploi/Emplois", "Emploi/Stocks"};
	private static String[] ACTIONS_MENU_EMPLOI = { "afficherEmplois","afficherStocks"};		   
	private static String[][] DROITS_MENU_EMPLOI = {{"agtConsEmplois","agtUpdEmplois"}, {"agtConsEmplois","agtUpdEmplois"}};

	// DIVERS
	private static String[] TITRES_MENU_DIVERS = {
		"Divers/Promouvabilités",
		"Divers/Nomenclatures",
		"Divers/Vacations",
		"Divers/Hébergés", 
		"Divers/Décharges", 
		"Divers/NBIs", 
		"Divers/Formations", 
		//"Divers/DIFs"
		};
	private static String[]  ACTIONS_MENU_DIVERS = {
		"afficherPromouvabilites",
		"afficherNomenclatures", 
		"afficherVacations", 
		"afficherHeberges", 
		"afficherDecharges", 
		"afficherNbis", 
		"afficherFormations", 
		//"afficherDifs"
		};
	private static String[][] DROITS_MENU_DIVERS = {
		{"agtPromouvable","agtPromouvable"}, 
		{"agtNomenclatures","agtNomenclatures"}, 
		{"agtConsContrat","agtUpdContrat"}, 
		{"agtGereHeberge","agtGereHeberge"}, 
		{"agtConsIdentite","agtConsIdentite"}, 
		{"agtOutils","agtOutils"}, 
		{"agtConsIdentite","agtConsIdentite"}, 
		//{"agtOutils","agtOutils"}
		};
	//	private static String[] TITRES_MENU_DIVERS = {"Divers/Promouvabilités","Divers/Nomenclatures","Divers/Vacations","Divers/Hébergés", "Divers/Décharges", "Divers/Primes", "Divers/NBIs", "Divers/Formations"};
	//	private static String[]  ACTIONS_MENU_DIVERS = {"afficherPromouvabilites","afficherNomenclatures", "afficherVacations", "afficherHeberges", "afficherDecharges", "afficherListePrimes", "afficherNbis", "afficherFormations"};
	//	private static String[][] DROITS_MENU_DIVERS = {{"agtPromouvable","agtPromouvable"}, {"agtNomenclatures","agtNomenclatures"}, {"agtConsContrat","agtUpdContrat"}, {"agtGereHeberge","agtGereHeberge"}, {"agtConsIdentite","agtConsIdentite"}, {"agtPrimes","agtPrimes"}, {"agtOutils","agtOutils"}, {"agtConsIdentite","agtConsIdentite"}};

	// EXPORTS
	private static String[] TITRES_MENU_EXPORTS = {"Exports/CIR", "Exports/Sup INFO"};
	private static String[] ACTIONS_MENU_EXPORTS = {"afficherCIR", "afficherSupInfo"};
	private static String[][] DROITS_MENU_EXPORTS = {{"agtOutils","agtOutils"},{"agtOutils","agtOutils"}};

	private static int CATEGORIE_INITIALE = 300;
	private static NSMutableDictionary dictionnairePourOnglets;	// contient tous les titres d'onglets avec comme clé le nom du menu et comme objets tous les titres pour ce menu

	// méthodes
	/** retourne les titres des onglets de la fenetre passee en parametre pour le niveau d'onglets recherche 
	 * (commence &agrave 1 pour le premier niveau) et pour le nom de vue a onglets passe en parametre */
	public static NSArray titresPourOnglets(String nomFenetre,String nomVueOnglet,int niveau) {
		if (dictionnairePourOnglets == null) {
			preparerDictionnaire();
		}
		NSMutableArray titresOnglets = new NSMutableArray();
		String[] titres = (String[])dictionnairePourOnglets.objectForKey(nomFenetre);
		for (int i = 0; i < titres.length; i++) {
			if (nomFenetre.equals(extraireTitre(titres,i,0)) == false) {
				break;	// on est passé à une autre menu
			}
			String nomVuePrecedente = extraireTitre(titres,i, niveau-1);
			if (nomVueOnglet == null || nomVuePrecedente != null && nomVuePrecedente.equals(nomVueOnglet)) {
				String titre = extraireTitre(titres,i,niveau);
				if (titre == null) {		// on a dépassé le niveau de sous-menus
					break;
				} else if (titresOnglets.containsObject(titre) == false) {
					titresOnglets.addObject(titre);
				}
			}
		}
		return titresOnglets;
	}

	/** retourne le titre de l'item de menu associe a une methode */
	public static String titrePourMethode(String nomMethode) {
		// on commence par les plus petites listes
		for (int i = 0; i < ACTIONS_MENU_POSTE.length;i++) {
			if (ACTIONS_MENU_POSTE[i].equals(nomMethode)) {
				return extraireTitre(TITRES_MENU_POSTE[i]);
			}
		}
		if (EOGrhumParametres.isUseModulePrimes()) {
			for (int i = 0; i < ACTIONS_MENU_PRIMES.length;i++) {
				if (ACTIONS_MENU_PRIMES[i].equals(nomMethode)) {
					return extraireTitre(TITRES_MENU_PRIMES[i]);
				}
			}
		}
		for (int i = 0; i < ACTIONS_MENU_OUTIL.length;i++) {
			if (ACTIONS_MENU_OUTIL[i].equals(nomMethode)) {
				return extraireTitre(TITRES_MENU_OUTIL[i]);
			}
		}
		for (int i = 0; i < ACTIONS_MENU_ADMINISTRATION.length;i++) {
			if (ACTIONS_MENU_ADMINISTRATION[i].equals(nomMethode)) {
				return extraireTitre(TITRES_MENU_ADMINISTRATION[i]);
			}
		}
		for (int i = 0; i < ACTIONS_MENU_EMPLOI.length;i++) {
			if (ACTIONS_MENU_EMPLOI[i].equals(nomMethode)) {
				return extraireTitre(TITRES_MENU_EMPLOI[i]);
			}
		}

		for (int i = 0; i < ACTIONS_MENU_EMPLOYE.length;i++) {
			if (ACTIONS_MENU_EMPLOYE[i].equals(nomMethode)) {
				return extraireTitre(TITRES_MENU_EMPLOYE[i]);
			}
		}

		for (int i = 0; i < ACTIONS_MENU_DIVERS.length;i++) {
			if (ACTIONS_MENU_DIVERS[i].equals(nomMethode)) {
				return extraireTitre(TITRES_MENU_DIVERS[i]);
			}
		}
		for (int i = 0; i < ACTIONS_MENU_EXPORTS.length;i++) {
			if (ACTIONS_MENU_EXPORTS[i].equals(nomMethode)) {
				return extraireTitre(TITRES_MENU_EXPORTS[i]);
			}
		}

		return null;
	}
	/** retourne une liste comportant les attributs a verifier pour les droits de l'agent */
	public static String[] droitsPourMethode(String nomMethode) {
		
		// on commence par les plus petites listes
		for (int i = 0; i < ACTIONS_MENU_POSTE.length;i++) {
			if (ACTIONS_MENU_POSTE[i].equals(nomMethode)) {
				return DROITS_MENU_POSTE[i];
			}
		}
		for (int i = 0; i < ACTIONS_MENU_EXPORTS.length;i++) {
			if (ACTIONS_MENU_EXPORTS[i].equals(nomMethode)) {
				return null;
			}
		}
		for (int i = 0; i < ACTIONS_MENU_PRIMES.length;i++) {
			if (ACTIONS_MENU_PRIMES[i].equals(nomMethode)) {
				return DROITS_MENU_PRIMES[i];
			}
		}
		for (int i = 0; i < ACTIONS_MENU_OUTIL.length;i++) {
			if (ACTIONS_MENU_OUTIL[i].equals(nomMethode)) {
				return DROITS_MENU_OUTIL[i];
			}
		}

		for (int i = 0; i < ACTIONS_MENU_ADMINISTRATION.length;i++) {
			if (ACTIONS_MENU_ADMINISTRATION[i].equals(nomMethode)) {
				return DROITS_MENU_ADMINISTRATION[i];
			}
		}
		for (int i = 0; i < ACTIONS_MENU_EMPLOI.length;i++) {
			if (ACTIONS_MENU_EMPLOI[i].equals(nomMethode)) {
				return DROITS_MENU_EMPLOI[i];
			}
		}

		for (int i = 0; i < ACTIONS_MENU_EMPLOYE.length;i++) {
			if (ACTIONS_MENU_EMPLOYE[i].equals(nomMethode)) {
				return DROITS_MENU_EMPLOYE[i];
			}
		}
		return null;
	}

	/** retourne un tableau de EOActions contenant les menus visibles selon le profil de l'agent */
	// On crée les menus avec comme catégorie la catégorie initiale et comme priorité 10 puis on incrémente les priorités de 10 et les 
	// catégories de 100 quand on change de sous-menu ou de menu.
	public static NSArray<EOAction> actionsPourMenus(EOGlobalID agentGlobalID, Object ctrlClasse) {

		if (agentGlobalID == null) {
			return new NSArray();
		}
		
		int categorie = CATEGORIE_INITIALE;
		
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(agentGlobalID,new EOEditingContext());
		NSMutableArray<EOAction> actions = new NSMutableArray<EOAction>();

		if (agent.peutAfficherIndividu() || agent.peutAfficherCarrieres() || agent.peutAfficherContrats() || agent.peutAfficherConges() 
				|| agent.peutAfficherOccupation() || agent.peutGererIndividu() || agent.peutGererCarrieres() 
				|| agent.peutGererContrats() || agent.peutGererConges() || agent.peutGererOccupation() ||
				agent.peutAfficherAccidentTravail() || agent.peutGererAccidentTravail() || agent.peutGererPrimes()) {

			actions.addObjectsFromArray(actionsPourMenu(TITRES_MENU_EMPLOYE,ACTIONS_MENU_EMPLOYE,10,categorie, ctrlClasse));

			categorie = ((EOAction)actions.lastObject()).categoryPriority() + 100;

		}
		if (agent.peutAfficherEmplois() || agent.peutGererEmplois()) {
			actions.addObjectsFromArray(actionsPourMenu(TITRES_MENU_EMPLOI,ACTIONS_MENU_EMPLOI,10,categorie, ctrlClasse));
			categorie = ((EOAction)actions.lastObject()).categoryPriority() + 100;
		}
		if (agent.peutUtiliserOutils() || agent.peutGererPromouvabilites() || 
				agent.peutUtiliserRequetes() || agent.peutCreerListesElectorales()) {
			actions.addObjectsFromArray(actionsPourMenu(TITRES_MENU_OUTIL,ACTIONS_MENU_OUTIL,10,categorie, ctrlClasse));
			categorie = ((EOAction)actions.lastObject()).categoryPriority() + 100;
		}
		if (agent.peutAfficherPostes() || agent.peutGererPostes()) {
			actions.addObjectsFromArray(actionsPourMenu(TITRES_MENU_POSTE,ACTIONS_MENU_POSTE,10,categorie, ctrlClasse));
			categorie = ((EOAction)actions.lastObject()).categoryPriority() + 100;
		}
		if (agent.peutAdministrer() || agent.peutAfficherAgents() || agent.peutGererAgents()) {
			actions.addObjectsFromArray(actionsPourMenu(TITRES_MENU_ADMINISTRATION,ACTIONS_MENU_ADMINISTRATION,10,categorie, ctrlClasse));
			categorie = ((EOAction)actions.lastObject()).categoryPriority() + 100;
		}

		actions.addObjectsFromArray(actionsPourMenu(TITRES_MENU_DIVERS,ACTIONS_MENU_DIVERS,10,categorie, ctrlClasse));
		actions.addObjectsFromArray(actionsPourMenu(TITRES_MENU_EXPORTS,ACTIONS_MENU_EXPORTS,10,categorie, ctrlClasse));

		if (agent.peutGererPrimes() && EOGrhumParametres.isUseModulePrimes()) {
			actions.addObjectsFromArray(actionsPourMenu(TITRES_MENU_PRIMES,ACTIONS_MENU_PRIMES,10,categorie, ctrlClasse));
			categorie = ((EOAction)actions.lastObject()).categoryPriority() + 100;
		}

		categorie = ((EOAction)actions.lastObject()).categoryPriority() + 100;

		return actions;
	}
	
	/**
	 */
	public static NSArray titresOngletsPourAgent(EOGlobalID agentID) {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(agentID, new EOEditingContext());
		NSMutableArray titres = new NSMutableArray();
		titres.addObjectsFromArray(extraireTitresAvecDroitsPourAgent(TITRES_MENU_EMPLOYE,DROITS_MENU_EMPLOYE,agent));
		titres.addObjectsFromArray(extraireTitresAvecDroitsPourAgent(TITRES_MENU_EMPLOI,DROITS_MENU_EMPLOI,agent));
		titres.addObjectsFromArray(extraireTitresAvecDroitsPourAgent(TITRES_MENU_OUTIL,DROITS_MENU_OUTIL,agent));
		titres.addObjectsFromArray(extraireTitresAvecDroitsPourAgent(TITRES_MENU_POSTE,DROITS_MENU_POSTE,agent));
		titres.addObjectsFromArray(extraireTitresAvecDroitsPourAgent(TITRES_MENU_ADMINISTRATION,DROITS_MENU_ADMINISTRATION,agent));
		titres.addObjectsFromArray(extraireTitresAvecDroitsPourAgent(TITRES_MENU_DIVERS,DROITS_MENU_DIVERS,agent));
		titres.addObjectsFromArray(extraireTitresAvecDroitsPourAgent(TITRES_MENU_EXPORTS,DROITS_MENU_EXPORTS,agent));
		if (EOGrhumParametres.isUseModulePrimes()) {
			titres.addObjectsFromArray(extraireTitresAvecDroitsPourAgent(TITRES_MENU_PRIMES,DROITS_MENU_PRIMES,agent));
		}
		return titres;
	}

	/**
	 * 
	 */
	private static void preparerDictionnaire() {
		
		if ( EOGrhumParametres.isUseModulePrimes()) {
			TITRES_MENU_DIVERS = new String[]{
					"Divers/Promouvabilités", 
					"Divers/Nomenclatures", 
					"Divers/Vacations", 
					"Divers/Hébergés", 
					"Divers/Décharges", 
					"Divers/Primes", 
					"Divers/NBIs", 
					"Divers/Formations", 
					//"Divers/DIFs"
					};
			ACTIONS_MENU_DIVERS = new String[]{
					"afficherPromouvabilites", 
					"afficherNomenclatures", 
					"afficherVacations", 
					"afficherHeberges", 
					"afficherDecharges", 
					"afficherListePrimes", 
					"afficherNbis", 
					"afficherFormations",
					//"afficherDifs"
					};
			DROITS_MENU_DIVERS = new String[][]{
					{"agtPromouvable","agtPromouvable"}, 
					{"agtNomenclatures","agtNomenclatures"}, 
					{"agtConsContrat","agtUpdContrat"}, 
					{"agtGereHeberge","agtGereHeberge"},
					{"agtConsIdentite","agtConsIdentite"}, 
					{"agtPrimes","agtPrimes"}, 
					{"agtOutils","agtOutils"}, 
					{"agtConsIdentite","agtConsIdentite"},
					//{"agtOutils","agtOutils"}
			};
		}
		
		dictionnairePourOnglets = new NSMutableDictionary();
		dictionnairePourOnglets.setObjectForKey(TITRES_MENU_EMPLOYE, extraireTitre(TITRES_MENU_EMPLOYE,0,0));
		dictionnairePourOnglets.setObjectForKey(TITRES_MENU_EMPLOI, extraireTitre(TITRES_MENU_EMPLOI,0,0));
		dictionnairePourOnglets.setObjectForKey(TITRES_MENU_OUTIL, extraireTitre(TITRES_MENU_OUTIL,0,0));
		dictionnairePourOnglets.setObjectForKey(TITRES_MENU_POSTE, extraireTitre(TITRES_MENU_POSTE,0,0));
		dictionnairePourOnglets.setObjectForKey(TITRES_MENU_ADMINISTRATION, extraireTitre(TITRES_MENU_ADMINISTRATION,0,0));
		dictionnairePourOnglets.setObjectForKey(TITRES_MENU_DIVERS, extraireTitre(TITRES_MENU_DIVERS,0,0));
		dictionnairePourOnglets.setObjectForKey(TITRES_MENU_EXPORTS, extraireTitre(TITRES_MENU_EXPORTS,0,0));
		if (EOGrhumParametres.isUseModulePrimes()) {
			dictionnairePourOnglets.setObjectForKey(TITRES_MENU_PRIMES, extraireTitre(TITRES_MENU_PRIMES,0,0));
		}
	}

	// retourne le titre extrait du titre à l'index index dans le tableau de string et à la position position
	// exemple : extraireTitre(TITRES_MENU_EMPLOYE,0,0) => Infos et extraireTitre(TITRES_MENU_EMPLOYE,0,1) => Etat Civil
	private static String extraireTitre(String[] titres,int index,int position) {
		String titre = titres[index];
		NSArray items = NSArray.componentsSeparatedByString(titre,"/");
		if (position >= items.count()) {
			return null;
		} else {
			return (String)items.objectAtIndex(position);
		}
	}
	// extrait le dernier item de chaque titre
	private static String extraireTitre(String path) {
		NSArray items = NSArray.componentsSeparatedByString(path,"/");
		return (String)items.lastObject();
	}


	/**
	 * 
	 * @param paths
	 * @param droits
	 * @param agent
	 * @return
	 */
	private static NSArray<String> extraireTitresAvecDroitsPourAgent(String[] paths,String[][] droits,EOAgentPersonnel agent) {
		NSMutableArray<String> titres = new NSMutableArray<String>();
		for (int i = 0; i < paths.length; i++) {
			String[] droit = droits[i];
			String titre = extraireTitre(paths[i]);
			boolean peutAfficherTitre = true;
			if (StringCtrl.containsIgnoreCase(titre , "RIB") 
					|| StringCtrl.containsIgnoreCase(titre , "URGENCE") 
					|| StringCtrl.containsIgnoreCase(titre , "MILITAIRE")) {
				peutAfficherTitre = agent.peutAfficherInfosPerso();
			}
			for (int j = 0; j < droit.length; j++) {
				String statut = (String)agent.valueForKey(droit[j]);
				if (statut != null && statut.equals(CocktailConstantes.VRAI) && peutAfficherTitre) {
					titres.addObject(titre);
					break;
				}
			}
		}
		return titres;
	}

	/**
	 *  On crée les menus en incrémentant de 10 la priorité passée en paramètre et la catégorie de 10 quand on change de sous-menu
	 * @param titres
	 * @param nomsActions
	 * @param priorite
	 * @param categorie
	 * @return
	 */
	private static NSArray<EOAction> actionsPourMenu(String[] titres,String[] nomsActions,int priorite,int categorie, Object ctrlClasse) {
		
		String titreMenu = "";
		NSMutableArray<EOAction> actions = new NSMutableArray<EOAction>(titres.length);
		for (int i = 0; i < titres.length; i++) {
			NSArray<String> items = NSArray.componentsSeparatedByString(titres[i],"/");
			String sousTitre =  extraireTitre(titres,i,items.count() - 2);	// les titres ont toujours au moins 2 items (menu/sous-menu)
			if (titreMenu.equals("") || sousTitre.equals(titreMenu) == false) {
				categorie = categorie + 10;
				priorite = 10;
				titreMenu = sousTitre;
			} else {
				priorite = priorite + 10;
			}
			actions.addObject(EOAction.actionForObject(nomsActions[i],titres[i],titres[i],null, null, null, categorie,priorite, ctrlClasse));
		}
		return actions;
	}
}

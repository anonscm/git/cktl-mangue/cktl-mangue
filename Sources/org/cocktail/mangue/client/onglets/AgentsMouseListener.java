/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.mangue.client.onglets;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.individu.FicheSituationAgentCtrl;
import org.cocktail.mangue.client.individu.infoscir.InfosRetraiteCtrl;
import org.cocktail.mangue.client.individu.infoscomp.InfosComplementairesCtrl;
import org.cocktail.mangue.client.individu.infosperso.InfosPersonnellesCtrl;
import org.cocktail.mangue.client.modalites_services.ModalitesServicesCtrl;
import org.cocktail.mangue.client.situation.AffectationOccupationCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.ManGUEIcones;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;

public class AgentsMouseListener implements MouseMotionListener, MouseListener {

	private boolean macosx = false;
	private EOIndividu currentIndividu;

	private EOEditingContext ec;
	private JComponent myView;
	private JPopupMenu menu;
	private JMenu menuSituation, menuInfosPerso, menuInfosComp, menuInfosOnp, menuInfosCir;

	public AgentsMouseListener(EOEditingContext ec, JComponent view) {

		this.myView = view;
		this.ec = ec;

		if ("Mac OS X".equals(System.getProperty("os.name"))) {
			macosx = true;
		}

		menu = new JPopupMenu();
		menu.addSeparator();
		
		JMenuItem menuIdentite = new JMenuItem(new MenuAction("  Fiche identité", "FIC"));
		menuIdentite.setIcon(ManGUEIcones.ICON_DOSSIER_FICHE);

//		menu.add(menuIdentite);
//		menu.addSeparator();

		menuSituation = new JMenu("Situation Agent");
		menuSituation.setIcon(ManGUEIcones.ICON_DOSSIER_SITUATION);
		menuInfosPerso = new JMenu("Infos personnelles");
		menuInfosPerso.setIcon(ManGUEIcones.ICON_DOSSIER_PERSO);
		menuInfosComp = new JMenu("Infos complémentaires");
		menuInfosComp.setIcon(ManGUEIcones.ICON_DOSSIER_COMP);
		menuInfosCir = new JMenu("Infos CIR");
		menuInfosCir.setIcon(ManGUEIcones.ICON_DOSSIER_CIR);
		menuInfosOnp = new JMenu("Infos ONP");
		menuInfosOnp.setIcon(ManGUEIcones.ICON_DOSSIER_ONP);

		menuSituation.add(new MenuAction("  Situation", "SIT"));
		menuSituation.add(new MenuAction("  Modalités de service", "MOD"));
		menuSituation.add(new MenuAction("  Affectations / Occupations", "AFFOCC"));

		JMenuItem menuAdresses = new JMenuItem(new MenuAction(" Adresses", "P"+InfosPersonnellesCtrl.LAYOUT_ADRESSE));
		menuAdresses.setBackground(new Color(204,204,255));
		JMenuItem menuTelephones = new JMenuItem(new MenuAction(" Téléphones", "P"+InfosPersonnellesCtrl.LAYOUT_TELEPHONE));
		menuAdresses.setBackground(new Color(255,204,255));
		JMenuItem menuEnfants = new JMenuItem(new MenuAction(" Enfants", "P"+InfosPersonnellesCtrl.LAYOUT_ENFANT));
		menuAdresses.setBackground(new Color(204,255,204));
		JMenuItem menuSituationFamiliale = new JMenuItem(new MenuAction(" Sit. familiale", "P"+InfosPersonnellesCtrl.LAYOUT_SITUATION));
		menuAdresses.setBackground(new Color(153,204,255));
		JMenuItem menuUrgence = new JMenuItem(new MenuAction(" No Urgence", "P"+InfosPersonnellesCtrl.LAYOUT_URGENCE));
		menuAdresses.setBackground(new Color(255,204,153));
		JMenuItem menuConjoint = new JMenuItem(new MenuAction(" Conjoint", "P"+InfosPersonnellesCtrl.LAYOUT_CONJOINT));
		menuAdresses.setBackground(new Color(204,255,153));
		JMenuItem menuRibs = new JMenuItem(new MenuAction(" Ribs", "P"+InfosPersonnellesCtrl.LAYOUT_RIB));
		menuAdresses.setBackground(new Color(255,153,153));
		JMenuItem menuComptes = new JMenuItem(new MenuAction(" Messagerie", "P"+InfosPersonnellesCtrl.LAYOUT_COMPTE));
		menuAdresses.setBackground(new Color(0,204,204));
		
		menuInfosPerso.add(menuAdresses);
		menuInfosPerso.add(menuTelephones);
		menuInfosPerso.add(menuEnfants);
		menuInfosPerso.add(menuSituationFamiliale);
		menuInfosPerso.add(menuUrgence);
		menuInfosPerso.add(menuConjoint);
		menuInfosPerso.add(menuRibs);
		menuInfosPerso.add(menuComptes);

		menuInfosComp.add(new MenuAction(" Arrivées / Départs", "C"+InfosComplementairesCtrl.LAYOUT_ARRIVEE));
		menuInfosComp.add(new MenuAction(" Service national", "C"+InfosComplementairesCtrl.LAYOUT_SERVICE_NATIONAL));
		menuInfosComp.add(new MenuAction(" Diplômes", "C"+InfosComplementairesCtrl.LAYOUT_DIPLOMES));
		menuInfosComp.add(new MenuAction(" Formations", "C"+InfosComplementairesCtrl.LAYOUT_FORMATIONS));
		menuInfosComp.add(new MenuAction(" Distinctions", "C"+InfosComplementairesCtrl.LAYOUT_DISTINCTIONS));
		menuInfosComp.add(new MenuAction(" Déclaration Accident", "C"+InfosComplementairesCtrl.LAYOUT_ACCIDENT));
		menuInfosComp.add(new MenuAction(" Dossier Médical", "C"+InfosComplementairesCtrl.LAYOUT_MEDICAL));
		menuInfosComp.add(new MenuAction(" Décharges de service", "C"+InfosComplementairesCtrl.LAYOUT_DECHARGES));
		menuInfosComp.add(new MenuAction(" Heures Comp.", "C" + InfosComplementairesCtrl.LAYOUT_HCOMP));
		menuInfosComp.add(new MenuAction(" CDIF", "C" + InfosComplementairesCtrl.LAYOUT_CDIF));
		menuInfosComp.add(new MenuAction(" Fonctions", "C" + InfosComplementairesCtrl.LAYOUT_FONCTIONS));
		menuInfosComp.add(new MenuAction(" Groupes / Rôles", "C" + InfosComplementairesCtrl.LAYOUT_ROLES));

		menuInfosCir.add(new MenuAction(" Synthèse CIR", "R"+InfosRetraiteCtrl.LAYOUT_SYNTHESE));
		
		if (((ApplicationClient)ApplicationClient.sharedApplication()).isUseServicesValides()) {
			menuInfosCir.add(new MenuAction(" Validations", "R"+InfosRetraiteCtrl.LAYOUT_VALIDATION));
		}
		menuInfosCir.add(new MenuAction(" Bonifications", "R"+InfosRetraiteCtrl.LAYOUT_BONIFICATIONS));
		menuInfosCir.add(new MenuAction(" Etudes rachetées", "R"+InfosRetraiteCtrl.LAYOUT_ETUDES));
		menuInfosCir.add(new MenuAction(" Bénéfices études", "R"+InfosRetraiteCtrl.LAYOUT_BENEFICES));

		menu.add(menuInfosPerso);
		menu.addSeparator();

		menu.add(menuInfosComp);
		menu.addSeparator();
		
		menu.add(menuInfosCir);
		menu.addSeparator();
		
	}



	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}
	/** affiche le menu contextuel au clique droit */
	private void afficherMenuRightClick(MouseEvent e) {
		if (currentIndividu != null)
			menu.setToolTipText(currentIndividu.identitePrenomFirst());
		menu.show(myView, e.getX(), e.getY());
	}


	/** evenements du menu contextuel */
	public class MenuAction extends AbstractAction {

		private String id;

		private static final long serialVersionUID = 4368451643255197580L;

		public MenuAction(String name, String anId) {
			putValue(Action.NAME, name);
			id = anId;
		}
		public void actionPerformed(ActionEvent event) {

			CRICursor.setWaitCursor(menu);
			
			if (id.equals("FIC"))
				FicheSituationAgentCtrl.sharedInstance(ec).open(currentIndividu);
			if (id.equals("MOD"))
				ModalitesServicesCtrl.sharedInstance(ec).open(getCurrentIndividu());
			if (id.equals("AFFOCC")) {
				AffectationOccupationCtrl ctrlAff = new AffectationOccupationCtrl(ec);
				ctrlAff.open(getCurrentIndividu());
			}

			if (id.substring(0,1).equals("P"))
				InfosPersonnellesCtrl.sharedInstance(ec).open(getCurrentIndividu(), id.substring(1, id.length()));
			else
				if (id.substring(0,1).equals("C"))
					InfosComplementairesCtrl.sharedInstance(ec).open(getCurrentIndividu(), id.substring(1, id.length()));
				else
					if (id.substring(0,1).equals("R"))
						InfosRetraiteCtrl.sharedInstance().open(getCurrentIndividu(), id.substring(1, id.length()));

			CRICursor.setDefaultCursor(menu);

		}
	}


	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		int modifiers = arg0.getModifiers();
		if (((modifiers & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK)
				|| (((modifiers & InputEvent.BUTTON2_MASK) == InputEvent.BUTTON2_MASK) && (macosx))) {
			//			if (selectedIndex != -1) {
			this.afficherMenuRightClick(arg0);
			return;
			//			}
		}

	}
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
}
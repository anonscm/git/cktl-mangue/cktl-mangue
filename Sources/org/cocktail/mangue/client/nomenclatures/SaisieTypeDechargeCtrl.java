// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieTypeDechargeView;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeDechargeService;
import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieTypeDechargeCtrl extends ModelePageSaisieNomenclature {
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieTypeDechargeCtrl sharedInstance;
	private SaisieTypeDechargeView myView;
	private EOTypeDechargeService currentTypeDecharge;

	public SaisieTypeDechargeCtrl(EOEditingContext edc) {

		super(edc);

		myView = new SaisieTypeDechargeView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setDateListeners(myView.getTfDateOuverture());
		setDateListeners(myView.getTfDateFermeture());
	}

	public static SaisieTypeDechargeCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieTypeDechargeCtrl(editingContext);
		return sharedInstance;
	}

	public EOTypeDechargeService getCurrentTypeDecharge() {
		return currentTypeDecharge;
	}
	public void setCurrentTypeDecharge(EOTypeDechargeService currentTypeDecharge) {
		this.currentTypeDecharge = currentTypeDecharge;
		updateDatas();
	}

	/**
	 * 
	 * @param typeDecharge
	 * @param modeCreation
	 * @return
	 */
	public boolean modifier(EOTypeDechargeService typeDecharge, boolean modeCreation) {
		setCurrentTypeDecharge(typeDecharge);
		setModeCreation(modeCreation);
		myView.setVisible(true);
		return currentTypeDecharge != null;
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfLibelle());
		CocktailUtilities.viderTextField(myView.getTfDateOuverture());
		CocktailUtilities.viderTextField(myView.getTfDateFermeture());
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentTypeDecharge() != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentTypeDecharge().code());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentTypeDecharge().libelleLong());
			CocktailUtilities.setTextToField(myView.getTfLibelleCourt(), getCurrentTypeDecharge().libelleCourt());
			CocktailUtilities.setDateToField(myView.getTfDateOuverture(), getCurrentTypeDecharge().dateOuverture());
			CocktailUtilities.setDateToField(myView.getTfDateFermeture(), getCurrentTypeDecharge().dateFermeture());
			myView.getCheckHcomp().setSelected(getCurrentTypeDecharge().autoriseHcomp());
		}
		
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentTypeDecharge().setDModification(new NSTimestamp());
		getCurrentTypeDecharge().setDateOuverture(CocktailUtilities.getDateFromField(myView.getTfDateOuverture()));
		getCurrentTypeDecharge().setDateFermeture(CocktailUtilities.getDateFromField(myView.getTfDateFermeture()));
		getCurrentTypeDecharge().setCode(CocktailUtilities.getTextFromField(myView.getTfCode()));
		getCurrentTypeDecharge().setLibelleLong(CocktailUtilities.getTextFromField(myView.getTfLibelle()));
		getCurrentTypeDecharge().setLibelleCourt(CocktailUtilities.getTextFromField(myView.getTfLibelleCourt()));
		getCurrentTypeDecharge().setAutoriseHcomp(myView.getCheckHcomp().isSelected());

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentTypeDecharge(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
	}	

}

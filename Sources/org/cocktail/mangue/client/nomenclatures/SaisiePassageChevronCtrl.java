// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisiePassageChevronView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOChevron;
import org.cocktail.mangue.modele.grhum.EOPassageChevron;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisiePassageChevronCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisiePassageChevronCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisiePassageChevronView myView;
	private boolean modeModification;

	private EOPassageChevron currentPassageChevron;
	private EOPassageEchelon currentPassageEchelon;

	public SaisiePassageChevronCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		myView = new SaisiePassageChevronView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		myView.setChevrons(EOChevron.fetchAll(ec, EOChevron.SORT_ARRAY_CODE_ASC));

		CocktailUtilities.initTextField(myView.getTfEchelon(), false, false);		
	}

	public static SaisiePassageChevronCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisiePassageChevronCtrl(editingContext);
		return sharedInstance;
	}



	public EOPassageChevron currentPassageChevron() {
		return currentPassageChevron;
	}
	public void setCurrentPassageChevron(EOPassageChevron currentPassageChevron) {
		this.currentPassageChevron = currentPassageChevron;
	}

	public EOPassageEchelon currentPassageEchelon() {
		return currentPassageEchelon;
	}

	public void setCurrentPassageEchelon(EOPassageEchelon currentPassageEchelon) {
		this.currentPassageEchelon = currentPassageEchelon;
	}

	private void clearTextFields()	{
		myView.getTfEchelon().setText("");
		myView.getPopupPassageAnnees().setSelectedIndex(0);
		myView.getPopupPassageMois().setSelectedIndex(0);
	}

	public EOPassageChevron ajouter(EOPassageEchelon passage)	{

		clearTextFields();

		modeModification = false;

		setCurrentPassageEchelon(passage);
		setCurrentPassageChevron(EOPassageChevron.creer(ec, passage));

		updateData();

		myView.getPopupChevrons().setSelectedIndex(0);

		myView.setVisible(true);
		return currentPassageChevron();
	}

	public boolean modifier(EOPassageChevron passageChevron, EOPassageEchelon passageEchelon) {
		clearTextFields();

		setCurrentPassageEchelon(passageEchelon);
		setCurrentPassageChevron(passageChevron);

		updateData();
		modeModification = true;
		myView.setVisible(true);
		return currentPassageChevron != null;
	}

	private void updateData() {

		CocktailUtilities.setTextToField(myView.getTfEchelon(), currentPassageChevron().cEchelon());
		myView.getPopupChevrons().setSelectedItem(currentPassageChevron().cChevron());
		if(currentPassageChevron().dureeChevronAnnees() != null)
			myView.getPopupPassageAnnees().setSelectedItem(currentPassageChevron().dureeChevronAnnees());
		if(currentPassageChevron().dureeChevronMois() != null)
			myView.getPopupPassageAnnees().setSelectedItem(currentPassageChevron().dureeChevronMois());

	}

	private void valider()  {

		try {

			currentPassageChevron().setCChevron((String)myView.getPopupChevrons().getSelectedItem());
			currentPassageChevron.setCIndiceBrut(currentPassageChevron().cChevron());

			// Passage
			if (myView.getPopupPassageAnnees().getSelectedIndex() > 0)
				currentPassageChevron().setDureeChevronAnnees(new Integer(myView.getPopupPassageAnnees().getSelectedItem().toString()));
			else
				currentPassageChevron().setDureeChevronAnnees(null);

			if (myView.getPopupPassageMois().getSelectedIndex() > 0)
				currentPassageChevron().setDureeChevronMois(new Integer(myView.getPopupPassageMois().getSelectedItem().toString()));
			else
				currentPassageChevron().setDureeChevronMois(null);

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		if(!modeModification)
			ec.deleteObject(currentPassageChevron());
		ec.revert();
		setCurrentPassageChevron(null);
		myView.setVisible(false);
	}

}

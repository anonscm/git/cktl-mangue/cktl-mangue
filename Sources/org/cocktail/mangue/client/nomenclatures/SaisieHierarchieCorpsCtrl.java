// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieHierarchieCorpsView;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOHierarchieCorps;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisieHierarchieCorpsCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieHierarchieCorpsCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieHierarchieCorpsView myView;

	private EOHierarchieCorps currentHierarchie;
	private EOCorps currentCorpsInitial;
	private EOCorps currentCorpsSuivant;

	public SaisieHierarchieCorpsCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		myView = new SaisieHierarchieCorpsView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				valider();
			}
		}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				annuler();
			}
		}
		);
		myView.getBtnGetCorpsInitial().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectCorpsInitial();}}
		);
		myView.getBtnGetCorpsSuivant().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectCorpsSuivant();}}
		);

		myView.getTfDateOuverture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateOuverture().addActionListener(new ActionListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateFermeture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFermeture()));
		myView.getTfDateFermeture().addActionListener(new ActionListenerDateTextField(myView.getTfDateFermeture()));

		CocktailUtilities.initTextField(myView.getTfCorpsInitial(), false, false);
		CocktailUtilities.initTextField(myView.getTfCorpsSuivant(), false, false);

	}

	public static SaisieHierarchieCorpsCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieHierarchieCorpsCtrl(editingContext);
		return sharedInstance;
	}

	public EOHierarchieCorps currentHierarchie() {
		return currentHierarchie;
	}

	public void setCurrentHierarchie(EOHierarchieCorps currentHierarchie) {
		this.currentHierarchie = currentHierarchie;
		updateData();
	}

	public EOCorps currentCorpsInitial() {
		return currentCorpsInitial;
	}

	public void setCurrentCorpsInitial(EOCorps currentCorpsInitial) {
		this.currentCorpsInitial = currentCorpsInitial;
		CocktailUtilities.setTextToField(myView.getTfCorpsInitial(), "");
		if (currentCorpsInitial != null) {
			CocktailUtilities.setTextToField(myView.getTfCorpsInitial(), currentCorpsInitial.cCorps() + " - " + currentCorpsInitial.llCorps());			
		}
	}

	public EOCorps currentCorpsSuivant() {
		return currentCorpsSuivant;
	}

	public void setCurrentCorpsSuivant(EOCorps currentCorpsSuivant) {
		this.currentCorpsSuivant = currentCorpsSuivant;
		CocktailUtilities.setTextToField(myView.getTfCorpsSuivant(), "");
		if (currentCorpsSuivant != null) {
			CocktailUtilities.setTextToField(myView.getTfCorpsSuivant(), currentCorpsSuivant.cCorps() + " - " + currentCorpsSuivant.llCorps());			
		}
	}

	private void clearTextFields()	{

		setCurrentCorpsInitial(null);
		setCurrentCorpsSuivant(null);

		myView.getTfDateOuverture().setText("");
		myView.getTfDateFermeture().setText("");

	}

	public EOHierarchieCorps ajouter()	{

		clearTextFields();

		myView.getBtnGetCorpsInitial().setVisible(true);
		setCurrentHierarchie(EOHierarchieCorps.creer(ec));

		myView.setVisible(true);

		return currentHierarchie();
	}

	public boolean modifier(EOHierarchieCorps hierarchie) {

		setCurrentHierarchie(hierarchie);
		myView.getBtnGetCorpsInitial().setVisible(false);
		myView.setVisible(true);
		return currentHierarchie() != null;
	}

	private void updateData() {

		clearTextFields();

		if (currentHierarchie() != null) {

			setCurrentCorpsInitial(currentHierarchie().toCorpsInitial());
			setCurrentCorpsSuivant(currentHierarchie().toCorpsSuivant());

			CocktailUtilities.setDateToField(myView.getTfDateOuverture(), currentHierarchie().dDebValidite());
			CocktailUtilities.setDateToField(myView.getTfDateFermeture(), currentHierarchie().dFinValidite());

		}

	}

	private void valider()  {

		try {

			currentHierarchie().setToCorpsInitialRelationship(currentCorpsInitial());
			currentHierarchie().setToCorpsSuivantRelationship(currentCorpsSuivant());

			currentHierarchie().setDDebValidite(CocktailUtilities.getDateFromField(myView.getTfDateOuverture()));
			currentHierarchie().setDFinValidite(CocktailUtilities.getDateFromField(myView.getTfDateFermeture()));

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * 
	 */
	public void selectCorpsInitial() {
		CRICursor.setWaitCursor(myView);
		EOCorps corps = CorpsSelectCtrl.sharedInstance(ec).getCorps();
		if (corps != null)
			setCurrentCorpsInitial(corps);

		CRICursor.setDefaultCursor(myView);
	}
	
	/**
	 * 
	 */
	public void selectCorpsSuivant() {
		CRICursor.setWaitCursor(myView);
		EOCorps corps = CorpsSelectCtrl.sharedInstance(ec).getCorps();
		if (corps != null)
			setCurrentCorpsSuivant(corps);

		CRICursor.setDefaultCursor(myView);
	}

	private void annuler() {
		ec.revert();
		setCurrentHierarchie(null);
		myView.setVisible(false);
	}


	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}

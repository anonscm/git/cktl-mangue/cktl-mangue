// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisiePassageEchelonView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisiePassageEchelonCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisiePassageEchelonCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisiePassageEchelonView myView;
	private boolean modeModification;

	private EOPassageEchelon currentPassageEchelon;

	public SaisiePassageEchelonCtrl(EOEditingContext globalEc) {
		ec = globalEc;
		myView = new SaisiePassageEchelonView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		
		myView.getTfDateOuverture().addFocusListener(new FocusListenerDateOuverture());
		myView.getTfDateOuverture().addActionListener(new ActionListenerDateOuverture());
		myView.getTfDateFermeture().addFocusListener(new FocusListenerDateFermeture());
		myView.getTfDateFermeture().addActionListener(new ActionListenerDateFermeture());

	}

	public static SaisiePassageEchelonCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisiePassageEchelonCtrl(editingContext);
		return sharedInstance;
	}

	
	public EOPassageEchelon currentPassageEchelon() {
		return currentPassageEchelon;
	}

	public void setCurrentPassageEchelon(EOPassageEchelon currentPassageEchelon) {
		this.currentPassageEchelon = currentPassageEchelon;
	}

	private NSTimestamp getDateOuverture() {
		if (myView.getTfDateOuverture().getText().length() > 0)
			return DateCtrl.stringToDate(myView.getTfDateOuverture().getText());

		return null;
	}
	private NSTimestamp getDateFermeture() {
		if (myView.getTfDateFermeture().getText().length() > 0)
			return DateCtrl.stringToDate(myView.getTfDateFermeture().getText());

		return null;
	}

	private void clearTextFields()	{

		myView.getTfIndice().setText("");
		myView.getTfEchelon().setText("");
		myView.getTfDateOuverture().setText("");
		myView.getTfDateFermeture().setText("");

		myView.getPopupPassageAnnees().setSelectedIndex(0);
		myView.getPopupPassageMois().setSelectedIndex(0);
		myView.getPopupPtChoixAnnees().setSelectedIndex(0);
		myView.getPopupPtChoixMois().setSelectedIndex(0);
		myView.getPopupGdChoixAnnees().setSelectedIndex(0);
		myView.getPopupGdChoixMois().setSelectedIndex(0);

	}

	public EOPassageEchelon ajouter(EOGrade grade)	{

		clearTextFields();

		modeModification = false;

		currentPassageEchelon = EOPassageEchelon.creer(ec, grade);		
		currentPassageEchelon.setTemLocal("O");

		myView.getTfTitre().setText(currentPassageEchelon().grade().llGrade());

		updateData();
		myView.setVisible(true);
		return currentPassageEchelon;
	}

	public EOPassageEchelon dupliquer(EOPassageEchelon echelon)	{

		clearTextFields();

		modeModification = false;
		setCurrentPassageEchelon(EOPassageEchelon.creer(ec, echelon.grade()));

		currentPassageEchelon.setDureePassageAnnees(echelon.dureePassageAnnees());
		currentPassageEchelon.setDureePassageMois(echelon.dureePassageMois());
		currentPassageEchelon.setDureePtChoixAnnees(echelon.dureePtChoixAnnees());
		currentPassageEchelon.setDureePtChoixMois(echelon.dureePtChoixMois());
		currentPassageEchelon.setDureeGrChoixAnnees(echelon.dureeGrChoixAnnees());
		currentPassageEchelon.setDureeGrChoixMois(echelon.dureeGrChoixMois());

		myView.getTfTitre().setText(currentPassageEchelon().grade().llGrade());

		updateData();
		myView.setVisible(true);
		return currentPassageEchelon;
		
	}

	public boolean modifier(EOPassageEchelon passageEchelon) {
		clearTextFields();

		setCurrentPassageEchelon(passageEchelon);
		myView.getTfTitre().setText(currentPassageEchelon().grade().llGrade());

		currentPassageEchelon.setDModification(new NSTimestamp());
		
		updateData();
		modeModification = true;
		myView.setVisible(true);
		return currentPassageEchelon != null;
	}

	private void updateData() {
		
		CocktailUtilities.setDateToField(myView.getTfDateOuverture(), currentPassageEchelon.dOuverture());
		CocktailUtilities.setDateToField(myView.getTfDateFermeture(), currentPassageEchelon.dFermeture());
		
		CocktailUtilities.setTextToField(myView.getTfEchelon(), currentPassageEchelon.cEchelon());
		CocktailUtilities.setTextToField(myView.getTfIndice(), currentPassageEchelon.cIndiceBrut());

		if(currentPassageEchelon.dureePassageAnnees() != null)
			myView.getPopupPassageAnnees().setSelectedItem(currentPassageEchelon.dureePassageAnnees());
		if(currentPassageEchelon.dureePassageMois() != null)
			myView.getPopupPassageMois().setSelectedItem(currentPassageEchelon.dureePassageMois());

		if(currentPassageEchelon.dureePtChoixAnnees() != null)
			myView.getPopupPtChoixAnnees().setSelectedItem(currentPassageEchelon.dureePtChoixAnnees());
		if(currentPassageEchelon.dureePtChoixMois() != null)
			myView.getPopupPtChoixMois().setSelectedItem(currentPassageEchelon.dureePtChoixMois());

		if(currentPassageEchelon.dureeGrChoixAnnees() != null)
			myView.getPopupGdChoixAnnees().setSelectedItem(currentPassageEchelon.dureeGrChoixAnnees());
		if(currentPassageEchelon.dureeGrChoixMois() != null)
			myView.getPopupGdChoixMois().setSelectedItem(currentPassageEchelon.dureeGrChoixMois());


	}

	private void valider()  {

		try {

				currentPassageEchelon.setDOuverture(getDateOuverture());
				currentPassageEchelon.setDFermeture(getDateFermeture());
			
			// Indice
			if (myView.getTfIndice().getText().length() > 0)
				currentPassageEchelon.setCIndiceBrut(myView.getTfIndice().getText());
			else
				currentPassageEchelon.setCIndiceBrut(null);

			// Echelon
			if (myView.getTfEchelon().getText().length() > 0) {
				String cEchelon = myView.getTfEchelon().getText();
				if (cEchelon.length() == 1)
					cEchelon = "0"+cEchelon;				
				currentPassageEchelon.setCEchelon(cEchelon);
			}
			else
				currentPassageEchelon.setCEchelon(null);

			// Passage
			if (myView.getPopupPassageAnnees().getSelectedIndex() > 0)
				currentPassageEchelon.setDureePassageAnnees(new Integer(myView.getPopupPassageAnnees().getSelectedItem().toString()));
			else
				currentPassageEchelon.setDureePassageAnnees(null);

			if (myView.getPopupPassageMois().getSelectedIndex() > 0)
				currentPassageEchelon.setDureePassageMois(new Integer(myView.getPopupPassageMois().getSelectedItem().toString()));
			else
				currentPassageEchelon.setDureePassageMois(null);

			// Petit Choix
			if (myView.getPopupPtChoixAnnees().getSelectedIndex() > 0)
				currentPassageEchelon.setDureePtChoixAnnees(new Integer(myView.getPopupPtChoixAnnees().getSelectedItem().toString()));
			else
				currentPassageEchelon.setDureePtChoixAnnees(null);

			if (myView.getPopupPtChoixMois().getSelectedIndex() > 0)
				currentPassageEchelon.setDureePtChoixMois(new Integer(myView.getPopupPtChoixMois().getSelectedItem().toString()));
			else
				currentPassageEchelon.setDureePtChoixMois(null);

			// Grand Choix
			if (myView.getPopupGdChoixAnnees().getSelectedIndex() > 0)
				currentPassageEchelon.setDureeGrChoixAnnees(new Integer(myView.getPopupGdChoixAnnees().getSelectedItem().toString()));
			else
				currentPassageEchelon.setDureeGrChoixAnnees(null);

			if (myView.getPopupGdChoixMois().getSelectedIndex() > 0)
				currentPassageEchelon.setDureeGrChoixMois(new Integer(myView.getPopupGdChoixMois().getSelectedItem().toString()));
			else
				currentPassageEchelon.setDureeGrChoixMois(null);

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		if(!modeModification)
			ec.deleteObject(currentPassageEchelon());
		setCurrentPassageEchelon(null);
		myView.setVisible(false);
	}

	private class ActionListenerDateOuverture implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateOuverture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateOuverture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date d'ouverture saisie n'est pas valide !");
				myView.getTfDateOuverture().selectAll();
			}
			else	{
				myView.getTfDateOuverture().setText(myDate);
			}
		}
	}
	private class FocusListenerDateOuverture implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateOuverture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateOuverture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date d'ouverture saisie n'est pas valide !");
				myView.getTfDateOuverture().selectAll();
			}
			else
				myView.getTfDateOuverture().setText(myDate);
		}
	}

	
	private class ActionListenerDateFermeture implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateFermeture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateFermeture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fermeture saisie n'est pas valide !");
				myView.getTfDateFermeture().selectAll();
			}
			else	{
				myView.getTfDateFermeture().setText(myDate);
			}
		}
	}
	private class FocusListenerDateFermeture implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateFermeture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateFermeture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fermeture saisie n'est pas valide !");
				myView.getTfDateFermeture().selectAll();
			}
			else
				myView.getTfDateFermeture().setText(myDate);
		}
	}

	
}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieEquivalenceGradeView;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOEquivAncGrade;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieEquivalenceGradeCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieEquivalenceGradeCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieEquivalenceGradeView myView;
	private boolean modeModification;
	private EOEquivAncGrade currentEquivGrade;
	private EOGrade currentGradeDepart;
	private EOGrade currentGrade;
	private EOTypeAcces currentTypeAcces;

	public SaisieEquivalenceGradeCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		myView = new SaisieEquivalenceGradeView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				valider();
			}
		}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				annuler();
			}
		}
		);
		myView.getBtnGetGradeDepart().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getGradeDepart();}}
		);
		myView.getBtnGetGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getGrade();}}
		);
		myView.getBtnGetTypeAcces().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {}}
		);

		myView.getTfDateOuverture().addFocusListener(new FocusListenerDateOuverture());
		myView.getTfDateOuverture().addActionListener(new ActionListenerDateOuverture());
		myView.getTfDateFermeture().addFocusListener(new FocusListenerDateFermeture());
		myView.getTfDateFermeture().addActionListener(new ActionListenerDateFermeture());

		CocktailUtilities.initTextField(myView.getTfLibelleGradeDepart(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleGrade(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleTypeAcces(), false, false);

	}

	public static SaisieEquivalenceGradeCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieEquivalenceGradeCtrl(editingContext);
		return sharedInstance;
	}

	private void clearTextFields()	{

		myView.getTfLibelleGradeDepart().setText("");
		myView.getTfLibelleGrade().setText("");
		myView.getTfLibelleTypeAcces().setText("");
		myView.getTfDateOuverture().setText("");
		myView.getTfDateFermeture().setText("");
		
		myView.getCheckTemTypeAcces().setSelected(false);
		myView.getCheckPromouvabilite().setSelected(false);

	}

	public EOEquivAncGrade ajouter(EOGrade gradeDepart)	{

		clearTextFields();
		
		if (gradeDepart != null) {
			currentGradeDepart=gradeDepart;
			CocktailUtilities.setTextToField(myView.getTfLibelleGradeDepart(), currentGradeDepart.llGrade());
		}

		myView.getBtnGetGradeDepart().setEnabled(true);
		modeModification = false;

		currentEquivGrade = EOEquivAncGrade.creer(ec);

		myView.getTfDateOuverture().setText(DateCtrl.dateToString(new NSTimestamp()));

		updateData();
		myView.setVisible(true);
		return currentEquivGrade;
	}

	public boolean modifier(EOEquivAncGrade equiv) {

		clearTextFields();
		currentEquivGrade = equiv;
		currentGrade = equiv.gradeEquivalent();

		myView.getBtnGetGradeDepart().setEnabled(false);

		updateData();
		modeModification = true;
		myView.setVisible(true);
		return currentEquivGrade != null;
	}

	private void updateData() {

		CocktailUtilities.setDateToField(myView.getTfDateOuverture(), currentEquivGrade.dDebVal());
		CocktailUtilities.setDateToField(myView.getTfDateFermeture(), currentEquivGrade.dFinVal());

		if (currentEquivGrade.gradeDepart() != null)
			CocktailUtilities.setTextToField(myView.getTfLibelleGradeDepart(),currentEquivGrade.gradeDepart().codeLibelle());		

		currentGrade = currentEquivGrade.gradeEquivalent();
		if(currentGrade != null)
			CocktailUtilities.setTextToField(myView.getTfLibelleGrade(),currentGrade.codeLibelle());		
		
		currentTypeAcces = currentEquivGrade.typeAcces();
		if(currentTypeAcces != null)
			myView.getTfLibelleTypeAcces().setText(currentTypeAcces.libelleLong());

		myView.getCheckPromouvabilite().setSelected(currentEquivGrade.temPromouvabilite()!=null && currentEquivGrade.temPromouvabilite().equals("O"));
		myView.getCheckTemTypeAcces().setSelected(currentEquivGrade.temTtTypeAcces()!=null && currentEquivGrade.temTtTypeAcces().equals("O"));

	}

	private void valider()  {

		try {

			currentEquivGrade.setDModification(new NSTimestamp());
			
			if (myView.getTfDateOuverture().getText().length() > 0)
				currentEquivGrade.setDDebVal(DateCtrl.stringToDate(myView.getTfDateOuverture().getText()));

			if (myView.getTfDateFermeture().getText().length() > 0)
				currentEquivGrade.setDFinVal(DateCtrl.stringToDate(myView.getTfDateFermeture().getText()));
			
			currentEquivGrade.setTemPromouvabilite(myView.getCheckPromouvabilite().isSelected()?"O":"N");
			currentEquivGrade.setTemTtTypeAcces(myView.getCheckTemTypeAcces().isSelected()?"O":"N");

			currentEquivGrade.setGradeDepartRelationship(currentGradeDepart);
			currentEquivGrade.setGradeEquivalentRelationship(currentGrade);
			currentEquivGrade.setTypeAccesRelationship(currentTypeAcces);

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public void getGradeDepart() {

		CRICursor.setWaitCursor(myView);
		EOGrade grade = GradeSelectCtrl.sharedInstance(ec).getGrade(EOGrhumParametres.isGestionHu(), null);

		if (grade != null)	{
			currentGradeDepart = grade;
			myView.getTfLibelleGradeDepart().setText(currentGradeDepart.codeLibelle());
		}
		CRICursor.setDefaultCursor(myView);
	}
	public void getGrade() {

		CRICursor.setWaitCursor(myView);
		
		EOGrade grade = GradeSelectCtrl.sharedInstance(ec).getGrade(EOGrhumParametres.isGestionHu(), null);

		if (grade != null)	{
			currentGrade = grade;
			myView.getTfLibelleGrade().setText(currentGrade.codeLibelle());
		}
		CRICursor.setDefaultCursor(myView);
	}

	private void annuler() {
		if(!modeModification)
			ec.deleteObject(currentEquivGrade);
		ec.revert();
		currentEquivGrade = null;
		myView.setVisible(false);
	}
	
	
	private class ActionListenerDateOuverture implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateOuverture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateOuverture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de début de validité saisie n'est pas valide !");
				myView.getTfDateOuverture().selectAll();
			}
			else	{
				myView.getTfDateOuverture().setText(myDate);
			}
		}
	}
	private class FocusListenerDateOuverture implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateOuverture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateOuverture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de début de validité saisie n'est pas valide !");
				myView.getTfDateOuverture().selectAll();
			}
			else
				myView.getTfDateOuverture().setText(myDate);
		}
	}

	
	private class ActionListenerDateFermeture implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateFermeture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateFermeture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin de validité saisie n'est pas valide !");
				myView.getTfDateFermeture().selectAll();
			}
			else	{
				myView.getTfDateFermeture().setText(myDate);
			}
		}
	}
	private class FocusListenerDateFermeture implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateFermeture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateFermeture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin de validité saisie n'est pas valide !");
				myView.getTfDateFermeture().selectAll();
			}
			else
				myView.getTfDateFermeture().setText(myDate);
		}
	}

}

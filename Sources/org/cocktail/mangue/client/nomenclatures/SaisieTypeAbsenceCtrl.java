// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieTypeAbsenceView;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eocontrol.EOEditingContext;

public class SaisieTypeAbsenceCtrl extends ModelePageSaisieNomenclature
{

	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieTypeAbsenceCtrl sharedInstance;
	private SaisieTypeAbsenceView myView;
	private EOTypeAbsence currentNomenclature;

	public SaisieTypeAbsenceCtrl(EOEditingContext edc) {

		super(edc);
		myView = new SaisieTypeAbsenceView(new JFrame(), true);

        setActionBoutonValiderListener(myView.getBtnValider());
        setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		
		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelle(), false, false);
		CocktailUtilities.initTextField(myView.getTfDecret(), false, false);
		myView.getCheckCir().setEnabled(false);
		myView.getCheckConge().setEnabled(false);
		myView.getCheckEnfant().setEnabled(false);
		myView.getCheckHcomp().setEnabled(true);
		
	}

	public static SaisieTypeAbsenceCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieTypeAbsenceCtrl(editingContext);
		return sharedInstance;
	}

	public EOTypeAbsence getCurrentNomenclature() {
		return currentNomenclature;
	}

	public void setCurrentNomenclature(EOTypeAbsence currentNomenclature) {
		this.currentNomenclature = currentNomenclature;
		updateDatas();
	}

	public boolean modifier(EOTypeAbsence type, boolean modeCreation) {
		setCurrentNomenclature(type);
		setModeCreation(modeCreation);
		myView.setVisible(true);
		return getCurrentNomenclature() != null;
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentNomenclature() != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentNomenclature().code());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentNomenclature().libelleLong());

			myView.getCheckConge().setSelected(getCurrentNomenclature().estCongeLegal() );
			myView.getCheckCir().setSelected(getCurrentNomenclature().estCongeCir() );
			myView.getCheckEnfant().setSelected(getCurrentNomenclature().requiertEnfant() );
			myView.getCheckHcomp().setSelected(getCurrentNomenclature().estHeuresComp() );
		}

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentNomenclature().setTemCir(myView.getCheckCir().isSelected()?"O":"N");
		getCurrentNomenclature().setTemEnfant(myView.getCheckEnfant().isSelected()?"O":"N");
		getCurrentNomenclature().setCongeLegal(myView.getCheckConge().isSelected()?"O":"N");
		getCurrentNomenclature().setEstHeuresComp(myView.getCheckHcomp().isSelected());

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentNomenclature(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		
	}
}
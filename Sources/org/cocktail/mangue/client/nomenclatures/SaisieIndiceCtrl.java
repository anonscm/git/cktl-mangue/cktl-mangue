// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieIndiceView;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOIndice;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieIndiceCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieIndiceCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieIndiceView myView;
	private boolean modeModification;

	private EOIndice currentIndice;

	public SaisieIndiceCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		myView = new SaisieIndiceView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				valider();
			}
		}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				annuler();
			}
		}
		);

		myView.getTfDateOuverture().addFocusListener(new FocusListenerDateOuverture());
		myView.getTfDateOuverture().addActionListener(new ActionListenerDateOuverture());
		myView.getTfDateFermeture().addFocusListener(new FocusListenerDateFermeture());
		myView.getTfDateFermeture().addActionListener(new ActionListenerDateFermeture());
	
	}

	public static SaisieIndiceCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieIndiceCtrl(editingContext);
		return sharedInstance;
	}

	private void clearTextFields()	{

		myView.getTfIndiceBrut().setText("");
		myView.getTfIndiceMajore().setText("");
		myView.getTfDateOuverture().setText("");
		myView.getTfDateFermeture().setText("");

	}

	public EOIndice ajouter()	{

		clearTextFields();

		modeModification = false;

		currentIndice = EOIndice.creer(ec);

		myView.getTfTitre().setText("AJOUT D'UN NOUVEL INDICE ");
		myView.getTfDateOuverture().setText(DateCtrl.dateToString(new NSTimestamp()));

		updateData();
		myView.setVisible(true);
		return currentIndice;
	}

	public boolean modifier(EOIndice indice) {

		clearTextFields();
		currentIndice = indice;

		myView.getTfTitre().setText("MODIFICATION D'UN INDICE ");

		updateData();
		modeModification = true;
		myView.setVisible(true);
		return currentIndice != null;
	}

	private void updateData() {

		if(currentIndice.dMajoration() != null)
			myView.getTfDateOuverture().setText(DateCtrl.dateToString(currentIndice.dMajoration()));

		if(currentIndice.dFermeture() != null)
			myView.getTfDateFermeture().setText(DateCtrl.dateToString(currentIndice.dFermeture()));

		if(currentIndice.cIndiceBrut() != null)
			myView.getTfIndiceBrut().setText(currentIndice.cIndiceBrut());

		if(currentIndice.cIndiceMajore() != null)
			myView.getTfIndiceMajore().setText(currentIndice.cIndiceMajore().toString());

	}

	private void valider()  {

		try {

			currentIndice.setDModification(new NSTimestamp());
			
			if (myView.getTfDateOuverture().getText().length() > 0)
				currentIndice.setDMajoration(DateCtrl.stringToDate(myView.getTfDateOuverture().getText()));

			if (myView.getTfDateFermeture().getText().length() > 0)
				currentIndice.setDFermeture(DateCtrl.stringToDate(myView.getTfDateFermeture().getText()));
			
			if (myView.getTfIndiceBrut().getText().length() > 0)
				currentIndice.setCIndiceBrut(myView.getTfIndiceBrut().getText());

			if (myView.getTfIndiceMajore().getText().length() > 0)
				currentIndice.setCIndiceMajore(new Integer(myView.getTfIndiceMajore().getText()));

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		if(!modeModification)
			ec.deleteObject(currentIndice);
		currentIndice = null;
		myView.setVisible(false);
	}
	
	
	private class ActionListenerDateOuverture implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateOuverture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateOuverture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de majoration saisie n'est pas valide !");
				myView.getTfDateOuverture().selectAll();
			}
			else	{
				myView.getTfDateOuverture().setText(myDate);
			}
		}
	}
	private class FocusListenerDateOuverture implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateOuverture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateOuverture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de majoration saisie n'est pas valide !");
				myView.getTfDateOuverture().selectAll();
			}
			else
				myView.getTfDateOuverture().setText(myDate);
		}
	}

	
	private class ActionListenerDateFermeture implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateFermeture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateFermeture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fermeture saisie n'est pas valide !");
				myView.getTfDateFermeture().selectAll();
			}
			else	{
				myView.getTfDateFermeture().setText(myDate);
			}
		}
	}
	private class FocusListenerDateFermeture implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateFermeture().getText()))	return;
			
			String myDate = DateCtrl.dateCompletion(myView.getTfDateFermeture().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fermeture saisie n'est pas valide !");
				myView.getTfDateFermeture().selectAll();
			}
			else
				myView.getTfDateFermeture().setText(myDate);
		}
	}

}

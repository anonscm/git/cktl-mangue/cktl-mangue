package org.cocktail.mangue.client.nomenclatures;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.NomenclaturesView;
import org.cocktail.mangue.common.utilities.CRICursor;

import com.webobjects.eocontrol.EOEditingContext;

public class NomenclaturesCtrl {

	private static NomenclaturesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private NomenclaturesView myView;

	public static final String LAYOUT_CORPS = " Corps / Grades / Echelons";
	public static final String LAYOUT_INDICES = "Indices";
	public static final String LAYOUT_EQUIV = "Equivalences Corps/Grades";
	public static final String LAYOUT_PROMOUVABLES = "Promouvabilités";
	public static final String LAYOUT_HIERARCHIE_CORPS = "Hiérarchie Corps";
	public static final String LAYOUT_HIERARCHIE_GRADE = "Hiérarchie Grades";
	public static final String LAYOUT_TYPES_CONTRAT = "Types de contrat";
	public static final String LAYOUT_TYPES_POPULATION = "Types de population";
	public static final String LAYOUT_TYPES_ABSENCES = "Types d'absences";
	public static final String LAYOUT_TYPES_DECHARGES = "Types de décharges";
	public static final String LAYOUT_PROFESSION = "Professions";
	public static final String LAYOUT_CAT_EMPLOI = "Catégories emploi";
	public static final String LAYOUT_UAI = "U.A.I.";
	public static final String LAYOUT_CORPS_EMERITES = "Corps Emérites";
	public static final String LAYOUT_MINISTERES = "Ministères";

	private NomenclaturesGradesCtrl gradesCtrl = null;
	private NomenclatureIndicesCtrl indicesCtrl = null;
	private NomenclatureEquivalencesCtrl equivalencesCtrl = null;
	private NomenclatureHierarchieCorpsCtrl hierarchieCorpsCtrl = null;
	private NomenclatureHierarchieGradeCtrl hierarchieGradesCtrl = null;
	private NomenclatureTypesContratTravailCtrl typesContratCtrl = null;
	private NomenclatureTypePopulationCtrl typesPopulationCtrl = null;
	private NomenclatureTypeAbsenceCtrl typesAbsencesCtrl = null;
	private NomenclatureTypeDechargeCtrl typesDechargesCtrl = null;
	private NomenclatureProfessionCtrl professionCtrl = null;
	private NomenclatureCatEmploiCtrl categoriesEmploiCtrl = null;
	private NomenclatureUaiCtrl uaiCtrl = null;
	private NomenclatureCorpsPromouvablesCtrl promouvablesCtrl = null;
	private NomenclatureCorpsEmeritesCtrl emeritatCtrl = null;
	private NomenclatureMinisteresCtrl ministeresCtrl = null;

	public NomenclaturesCtrl(EOEditingContext edc) {
		this.edc = edc;
		myView = new NomenclaturesView(null, MODE_MODAL.booleanValue());

		gradesCtrl = new NomenclaturesGradesCtrl(edc);
		indicesCtrl = new NomenclatureIndicesCtrl(edc);
		equivalencesCtrl = new NomenclatureEquivalencesCtrl(getEdc());
		promouvablesCtrl = new NomenclatureCorpsPromouvablesCtrl(getEdc());
		hierarchieCorpsCtrl = new NomenclatureHierarchieCorpsCtrl(getEdc());
		hierarchieGradesCtrl = new NomenclatureHierarchieGradeCtrl(getEdc());
		typesContratCtrl = new NomenclatureTypesContratTravailCtrl(getEdc());
		typesPopulationCtrl = new NomenclatureTypePopulationCtrl(getEdc());
		typesAbsencesCtrl = new NomenclatureTypeAbsenceCtrl(getEdc());
		typesDechargesCtrl = new NomenclatureTypeDechargeCtrl(getEdc());
		professionCtrl = new NomenclatureProfessionCtrl(getEdc());
		categoriesEmploiCtrl = new NomenclatureCatEmploiCtrl(getEdc());
		uaiCtrl = new NomenclatureUaiCtrl(getEdc());
		emeritatCtrl = new NomenclatureCorpsEmeritesCtrl(getEdc());
		ministeresCtrl = new NomenclatureMinisteresCtrl(getEdc());

		//myView.getSwapView().add(LAYOUT_ETAT_CIVIL,etatCivilCtrl.getViewEtatCivil());
		myView.getSwapView().add(LAYOUT_CORPS,gradesCtrl.getView());
		myView.getSwapView().add(LAYOUT_INDICES,indicesCtrl.getView());
		myView.getSwapView().add(LAYOUT_TYPES_CONTRAT, typesContratCtrl.getView());
		myView.getSwapView().add(LAYOUT_TYPES_POPULATION, typesPopulationCtrl.getView());
		myView.getSwapView().add(LAYOUT_TYPES_ABSENCES, typesAbsencesCtrl.getView());
		myView.getSwapView().add(LAYOUT_TYPES_DECHARGES, typesDechargesCtrl.getView());
		myView.getSwapView().add(LAYOUT_PROFESSION, professionCtrl.getView());
		myView.getSwapView().add(LAYOUT_EQUIV, equivalencesCtrl.getView());
		myView.getSwapView().add(LAYOUT_HIERARCHIE_CORPS, hierarchieCorpsCtrl.getView());
		myView.getSwapView().add(LAYOUT_HIERARCHIE_GRADE, hierarchieGradesCtrl.getView());
		myView.getSwapView().add(LAYOUT_CAT_EMPLOI, categoriesEmploiCtrl.getView());
		myView.getSwapView().add(LAYOUT_UAI, uaiCtrl.getView());
		myView.getSwapView().add(LAYOUT_PROMOUVABLES, promouvablesCtrl.getView());
		myView.getSwapView().add(LAYOUT_CORPS_EMERITES, emeritatCtrl.getView());
		myView.getSwapView().add(LAYOUT_MINISTERES, ministeresCtrl.getView());

		myView.getPopupNomenclature().removeAllItems();
		myView.getPopupNomenclature().addItem(LAYOUT_CORPS);
		myView.getPopupNomenclature().addItem(LAYOUT_INDICES);
		myView.getPopupNomenclature().addItem(LAYOUT_PROMOUVABLES);
		myView.getPopupNomenclature().addItem(LAYOUT_EQUIV);
		myView.getPopupNomenclature().addItem(LAYOUT_HIERARCHIE_CORPS);
		myView.getPopupNomenclature().addItem(LAYOUT_HIERARCHIE_GRADE);
		myView.getPopupNomenclature().addItem(LAYOUT_TYPES_CONTRAT);
		myView.getPopupNomenclature().addItem(LAYOUT_TYPES_POPULATION);
		myView.getPopupNomenclature().addItem(LAYOUT_TYPES_ABSENCES);
		myView.getPopupNomenclature().addItem(LAYOUT_TYPES_DECHARGES);
		myView.getPopupNomenclature().addItem(LAYOUT_PROFESSION);
		myView.getPopupNomenclature().addItem(LAYOUT_CAT_EMPLOI);
		myView.getPopupNomenclature().addItem(LAYOUT_UAI);
		myView.getPopupNomenclature().addItem(LAYOUT_CORPS_EMERITES);
		myView.getPopupNomenclature().addItem(LAYOUT_MINISTERES);

		myView.getPopupNomenclature().addActionListener(new PopupActionListener());
		myView.getPopupNomenclature().setSelectedIndex(0);
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public static NomenclaturesCtrl sharedInstance(EOEditingContext edc)	{
		if(sharedInstance == null)
			sharedInstance = new NomenclaturesCtrl(edc);
		return sharedInstance;
	}
	public void open()	{
		myView.setVisible(true);
	}
	public JFrame getView() {
		return myView;
	}
	public void toFront() {
		myView.toFront();
	}

	/**
	 * 
	 * @param radioButton
	 * @param layout
	 */
	private void popupHasChanged(String layout) {
		((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), layout);				
	}

	public class PopupActionListener implements ActionListener	{
		public void actionPerformed(ActionEvent arg0) {

			CRICursor.setWaitCursor(myView);
			switch(myView.getPopupNomenclature().getSelectedIndex())	{
			case 0:	popupHasChanged(LAYOUT_CORPS);gradesCtrl.actualiser();break;
			case 1:	popupHasChanged(LAYOUT_INDICES);indicesCtrl.actualiser();break;
			case 2:	popupHasChanged(LAYOUT_PROMOUVABLES);promouvablesCtrl.actualiser();break;
			case 3:	popupHasChanged(LAYOUT_EQUIV);equivalencesCtrl.actualiser();break;
			case 4:	popupHasChanged(LAYOUT_HIERARCHIE_CORPS);hierarchieCorpsCtrl.updateDatas();break;
			case 5:	popupHasChanged(LAYOUT_HIERARCHIE_GRADE);hierarchieGradesCtrl.updateDatas();break;
			case 6:	popupHasChanged(LAYOUT_TYPES_CONTRAT);typesContratCtrl.actualiser();break;
			case 7:	popupHasChanged(LAYOUT_TYPES_POPULATION);typesPopulationCtrl.actualiser();break;
			case 8:	popupHasChanged(LAYOUT_TYPES_ABSENCES);typesAbsencesCtrl.actualiser();break;
			case 9:	popupHasChanged(LAYOUT_TYPES_DECHARGES);typesDechargesCtrl.actualiser();break;
			case 10:	popupHasChanged(LAYOUT_PROFESSION);professionCtrl.actualiser();break;
			case 11: popupHasChanged(LAYOUT_CAT_EMPLOI);categoriesEmploiCtrl.actualiser();break;
			case 12: popupHasChanged(LAYOUT_UAI);uaiCtrl.actualiser();break;
			case 13: popupHasChanged(LAYOUT_CORPS_EMERITES);emeritatCtrl.actualiser();break;
			case 14: popupHasChanged(LAYOUT_MINISTERES);ministeresCtrl.actualiser();break;

			}
			CRICursor.setDefaultCursor(myView);
		}
	}
}

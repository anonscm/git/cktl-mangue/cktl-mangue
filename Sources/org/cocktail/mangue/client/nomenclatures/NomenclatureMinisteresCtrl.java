package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureMinisteresView;
import org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

public class NomenclatureMinisteresCtrl {

	private static NomenclatureMinisteresCtrl sharedInstance;
	private EOEditingContext ec;
	private EODisplayGroup eod;

	private ListenerObject myListener = new ListenerObject();

	private NomenclatureMinisteresView myView;
	private EOAgentPersonnel currentUtilisateur;
	private EOMinisteres currentMinistere;

	public NomenclatureMinisteresCtrl(EOEditingContext edc)	{

		ec = edc;

		eod = new EODisplayGroup();

		myView = new NomenclatureMinisteresView(eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		myView.getMyEOTable().addListener(myListener);
	}

	public static NomenclatureMinisteresCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureMinisteresCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}	
	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		myView.getBtnAjouter().setVisible(false);
		myView.getBtnModifier().setVisible(false);
		myView.getBtnSupprimer().setVisible(false);
	}

	public EOMinisteres getCurrentMinistere() {
		return currentMinistere;
	}
	public void setCurrentMinistere(EOMinisteres currentMinistere) {
		this.currentMinistere = currentMinistere;
	}

	private void ajouter() {
	}	
	private void modifier() {
	}	
	private void supprimer() {
	}
	public void actualiser() {

		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EOMinisteres.findAllMinisteres(ec));
		myView.getMyEOTable().updateData();
		updateInterface();	
		CRICursor.setDefaultCursor(myView);
	}

	private void updateInterface(){
	}

	private class ListenerObject implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentMinistere((EOMinisteres)eod.selectedObject());
			updateInterface();
		}
	}


}

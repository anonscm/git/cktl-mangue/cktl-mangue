// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieCorpsView;
import org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOCorps;

import com.webobjects.eocontrol.EOEditingContext;

public class SaisieCorpsCtrl extends ModelePageSaisieNomenclature
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieCorpsCtrl sharedInstance;
	private SaisieCorpsView myView;
	private EOCorps currentCorps;

	public SaisieCorpsCtrl(EOEditingContext edc) {
		
		super(edc);
		
		myView = new SaisieCorpsView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		
		setActionDateListener(myView.getTfDateOuverture());
		setActionDateListener(myView.getTfDateFermeture());

		CocktailUtilities.initTextField(myView.getTfTypePopulation(), false, false);
		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		
		CocktailUtilities.initPopupAvecListe(myView.getPopupMinisteres(), EOMinisteres.findMinisteresValides(edc), true);
		
	}

	public static SaisieCorpsCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieCorpsCtrl(editingContext);
		return sharedInstance;
	}

	
	
	public EOCorps getCurrentCorps() {
		return currentCorps;
	}

	public void setCurrentCorps(EOCorps currentCorps) {
		this.currentCorps = currentCorps;
	}

	/**
	 * 
	 * @param typePopulation
	 * @return
	 */
	public EOCorps ajouter(EOTypePopulation typePopulation)	{

		clearDatas();

		setModeCreation(true);

		setCurrentCorps(EOCorps.creer(getEdc(), typePopulation));
		getCurrentCorps().setTemLocal("O");
		myView.getTfTitre().setText("CREATION D'UN NOUVEAU CORPS");

		updateDatas();
		myView.setVisible(true);
		return currentCorps;
	}

	public boolean modifier(EOCorps corps) {

		clearDatas();
		
		setCurrentCorps(corps);
		myView.getTfTitre().setText(currentCorps.llCorps());

		updateDatas();
		setModeCreation(false);
		myView.setVisible(true);
		return currentCorps != null;
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfLibelle());
		CocktailUtilities.viderTextField(myView.getTfLibelleCourt());
		CocktailUtilities.viderTextField(myView.getTfDateOuverture());
		CocktailUtilities.viderTextField(myView.getTfDateFermeture());
		CocktailUtilities.viderTextField(myView.getTfTypePopulation());

		myView.getPopupCategories().setSelectedIndex(0);
		myView.getPopupMinisteres().setSelectedIndex(0);

	}

	@Override
	protected void updateDatas() {
		
		// TODO Auto-generated method stub
		if (getCurrentCorps().toTypePopulation() != null)
			CocktailUtilities.setTextToField(myView.getTfTypePopulation(), getCurrentCorps().toTypePopulation().libelleLong());

		myView.getPopupMinisteres().setSelectedItem(getCurrentCorps().toMinistere());

		CocktailUtilities.setDateToField(myView.getTfDateOuverture(), getCurrentCorps().dOuvertureCorps());
		CocktailUtilities.setDateToField(myView.getTfDateFermeture(), getCurrentCorps().dFermetureCorps());

		CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentCorps().cCorps());
		CocktailUtilities.setTextToField(myView.getTfLibelleCourt(), getCurrentCorps().lcCorps());
		CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentCorps().llCorps());

		myView.getPopupCategories().setSelectedItem(getCurrentCorps().cCategorie());
		
		myView.getCheckDelegation().setSelected(getCurrentCorps().autoriseDelegation());

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
				
		// TODO Auto-generated method stub		
		getCurrentCorps().setDOuvertureCorps(CocktailUtilities.getDateFromField(myView.getTfDateOuverture()));
		getCurrentCorps().setDFermetureCorps(CocktailUtilities.getDateFromField(myView.getTfDateFermeture()));
		getCurrentCorps().setCCorps(CocktailUtilities.getTextFromField(myView.getTfCode()));
		getCurrentCorps().setLlCorps(CocktailUtilities.getTextFromField(myView.getTfLibelle()));
		getCurrentCorps().setLcCorps(CocktailUtilities.getTextFromField(myView.getTfLibelleCourt()));

		if (myView.getPopupCategories().getSelectedIndex() > 0)
			currentCorps.setCCategorie(myView.getPopupCategories().getSelectedItem().toString());
		else
			currentCorps.setCCategorie(null);
		
		if (myView.getPopupMinisteres().getSelectedIndex() > 0)
			currentCorps.setToMinistereRelationship((EOMinisteres)myView.getPopupMinisteres().getSelectedItem());
		else
			currentCorps.setToMinistereRelationship(null);

		currentCorps.setAutoriseDelegation(myView.getCheckDelegation().isSelected());

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		
	}

}

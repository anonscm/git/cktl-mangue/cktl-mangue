package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureCorpsEmeritesView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOCorpsEmerite;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

public class NomenclatureCorpsEmeritesCtrl {

	private static NomenclatureCorpsEmeritesCtrl sharedInstance;
	private EOEditingContext ec;
	private EODisplayGroup eod;

	private ListenerCorpsEmerite myListener = new ListenerCorpsEmerite();

	private NomenclatureCorpsEmeritesView myView;
	private EOAgentPersonnel currentUtilisateur;
	private EOCorpsEmerite currentCorpsEmerite;

	public NomenclatureCorpsEmeritesCtrl(EOEditingContext editingContext)	{

		ec = editingContext;

		eod = new EODisplayGroup();

		myView = new NomenclatureCorpsEmeritesView(eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		myView.getMyEOTable().addListener(myListener);
	}

	public static NomenclatureCorpsEmeritesCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureCorpsEmeritesCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}	
	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		myView.getBtnAjouter().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifier().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimer().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
	}

	public EOCorpsEmerite getCurrentCorpsEmerite() {
		return currentCorpsEmerite;
	}

	public void setCurrentCorpsEmerite(EOCorpsEmerite currentCorpsEmerite) {
		this.currentCorpsEmerite = currentCorpsEmerite;
	}

	private void ajouter() {
		EOCorpsEmerite newEmeritat = EOCorpsEmerite.creer(ec);
		if (SaisieCorpsEmeriteCtrl.sharedInstance(ec).modifier(newEmeritat, true)) {
			actualiser();
		}
	}	
	private void modifier() {
		if (SaisieCorpsEmeriteCtrl.sharedInstance(ec).modifier(getCurrentCorpsEmerite(), false)) {
			myView.getMyEOTable().updateUI();
		}
	}	
	private void supprimer() {
		try {
			ec.deleteObject(getCurrentCorpsEmerite());
			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void actualiser() {

		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EOCorpsEmerite.fetchAll(ec));
		myView.getMyEOTable().updateData();
		updateInterface();	
		CRICursor.setDefaultCursor(myView);
	}

	private void updateInterface(){
		myView.getBtnAjouter().setEnabled(true);
		myView.getBtnModifier().setEnabled(getCurrentCorpsEmerite() != null);
		myView.getBtnSupprimer().setEnabled(false);
	}

	private class ListenerCorpsEmerite implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentCorpsEmerite((EOCorpsEmerite)eod.selectedObject());
			updateInterface();
		}
	}


}

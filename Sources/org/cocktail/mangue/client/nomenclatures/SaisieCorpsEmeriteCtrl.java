// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieCorpsEmeriteView;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.mangue.EOCorpsEmerite;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieCorpsEmeriteCtrl extends ModelePageSaisieNomenclature {
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieCorpsEmeriteCtrl sharedInstance;
	private SaisieCorpsEmeriteView myView;
	private EOCorpsEmerite currentEmeritat;
	private EOCorps currentCorps;

	/**
	 * 
	 * @param edc
	 */
	public SaisieCorpsEmeriteCtrl(EOEditingContext edc) {

		super(edc);

		myView = new SaisieCorpsEmeriteView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setDateListeners(myView.getTfPeriodeDebut());
		setDateListeners(myView.getTfPeriodeFin());
		
		myView.getBtnGetCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectCorps();}}
		);

		CocktailUtilities.initTextField(myView.getTfCorps(), false, false);
		
	}

	public static SaisieCorpsEmeriteCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieCorpsEmeriteCtrl(editingContext);
		return sharedInstance;
	}

	public EOCorpsEmerite getCurrentEmeritat() {
		return currentEmeritat;
	}
	public void setCurrentEmeritat(EOCorpsEmerite currentEmeritat) {
		this.currentEmeritat = currentEmeritat;
		updateDatas();
	}

	public EOCorps getCurrentCorps() {
		return currentCorps;
	}

	public void setCurrentCorps(EOCorps currentCorps) {
		this.currentCorps = currentCorps;
		CocktailUtilities.viderTextField(myView.getTfCorps());
		if (currentCorps != null) {
			CocktailUtilities.setTextToField(myView.getTfCorps(), getCurrentCorps().llCorps());
		}
	}

	/**
	 * 
	 * @param typeDecharge
	 * @param modeCreation
	 * @return
	 */
	public boolean modifier(EOCorpsEmerite emeritat, boolean modeCreation) {
		setCurrentEmeritat(emeritat);
		setModeCreation(modeCreation);
		myView.setVisible(true);
		return getCurrentEmeritat() != null;
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfCorps());
		CocktailUtilities.viderTextField(myView.getTfPeriodeDebut());
		CocktailUtilities.viderTextField(myView.getTfPeriodeFin());
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentEmeritat() != null) {
			setCurrentCorps(getCurrentEmeritat().corps());
			CocktailUtilities.setDateToField(myView.getTfPeriodeDebut(), getCurrentEmeritat().dDebVal());
			CocktailUtilities.setDateToField(myView.getTfPeriodeFin(), getCurrentEmeritat().dFinVal());
		}
		
		updateInterface();

	}

	/**
	 * 
	 */
	public void selectCorps() {
		CRICursor.setWaitCursor(myView);
		EOCorps corps = CorpsSelectCtrl.sharedInstance(getEdc()).getCorps();
		if (corps != null)
			setCurrentCorps(corps);
		CRICursor.setDefaultCursor(myView);
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentEmeritat().setDModification(new NSTimestamp());
		getCurrentEmeritat().setDDebVal(CocktailUtilities.getDateFromField(myView.getTfPeriodeDebut()));
		getCurrentEmeritat().setDFinVal(CocktailUtilities.getDateFromField(myView.getTfPeriodeFin()));
		getCurrentEmeritat().setCorpsRelationship(getCurrentCorps());

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentEmeritat(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
	}	

}

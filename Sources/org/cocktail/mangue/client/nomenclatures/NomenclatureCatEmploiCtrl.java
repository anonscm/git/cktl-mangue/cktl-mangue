// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureCatEmploiView;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.EOCorps;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclatureCatEmploiCtrl extends ModelePageConsNomenclatures {

	private static NomenclatureCatEmploiCtrl sharedInstance;
	private EODisplayGroup eodCorps, eodCategorie;

	private ListenerCorps listenerCorps = new ListenerCorps();
	private ListenerCategorie listenerCategorie = new ListenerCategorie();
	private NomenclatureCatEmploiView myView;
	private EOCategorieEmploi currentCategorie;
	private EOCorpsCategEmploi currentCorps;

	public NomenclatureCatEmploiCtrl(EOEditingContext edc)	{

		super(edc, ((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());

		eodCorps = new EODisplayGroup();
		eodCategorie = new EODisplayGroup();

		myView = new NomenclatureCatEmploiView(eodCategorie, eodCorps);

		myView.getBtnAjouterCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterCorps();}}
				);
		myView.getBtnSupprimeCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerCorps();}}
				);
		myView.getBtnAjouterCategorie().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterCategorie();}}
				);
		myView.getBtnModifierCategorie().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierCategorie();}}
				);
		myView.getBtnSupprimerCategorie().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerCategorie();}}
				);
		
		setPeutGererModule(getUtilisateur().peutGererNomenclatures());

		myView.getMyEOTableCorps().addListener(listenerCorps);
		myView.getMyEOTableCategorie().addListener(listenerCategorie);
		myView.getTfFiltreLibelleCategorie().getDocument().addDocumentListener(new ADocumentListener());

		myView.getBtnAjouterCorps().setVisible(peutGererModule());
		myView.getBtnSupprimeCorps().setVisible(peutGererModule());
		myView.getBtnAjouterCategorie().setVisible(peutGererModule());
		myView.getBtnModifierCategorie().setVisible(peutGererModule());
		myView.getBtnSupprimerCategorie().setVisible(peutGererModule());

	}

	public static NomenclatureCatEmploiCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureCatEmploiCtrl(editingContext);
		return sharedInstance;
	}



	public EOCategorieEmploi getCurrentCategorie() {
		return currentCategorie;
	}

	public void setCurrentCategorie(EOCategorieEmploi currentCategorie) {
		this.currentCategorie = currentCategorie;
	}

	public EOCorpsCategEmploi getCurrentCorps() {
		return currentCorps;
	}

	public void setCurrentCorps(EOCorpsCategEmploi currentCorps) {
		this.currentCorps = currentCorps;
	}

	public JPanel getView() {
		return myView;
	}	

	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		eodCategorie.setObjectArray(EOCategorieEmploi.fetchAll(getEdc(), EOCategorieEmploi.SORT_ARRAY_LIBELLE_ASC));
		filter();

		CRICursor.setDefaultCursor(myView);
	}


	/**
	 * 
	 * @return
	 */
	private EOQualifier qualifierRecherche() {

		NSMutableArray	qualifiers = new NSMutableArray(),orQualifiers = new NSMutableArray();		

		String filtreLibelleCategorie  = myView.getTfFiltreLibelleCategorie().getText();
		if (filtreLibelleCategorie.length() > 0) {
			orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCategorieEmploi.C_CATEGORIE_EMPLOI_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCategorie+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCategorieEmploi.LL_CATEGORIE_EMPLOI_KEY +" caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCategorie+"*")));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(qualifiers);
	}

	/**
	 * 
	 */
	private void filter() {

		eodCategorie.setQualifier(qualifierRecherche());
		eodCategorie.updateDisplayedObjects();

		myView.getMyEOTableCategorie().updateData();
		updateInterface();
	}

	/**
	 * 
	 */
	private void ajouterCorps() 	{
		NSArray<EOCorps> listeCorps = CorpsSelectCtrl.sharedInstance(getEdc()).getArrayCorps();
		if( listeCorps != null && listeCorps.size() > 0) {
			try {
				for (EOCorps myCorps : listeCorps) {

					EOCorpsCategEmploi record = EOCorpsCategEmploi.findForCategorieAndCorps(getEdc(), getCurrentCategorie(), myCorps);
					if (record == null)
						EOCorpsCategEmploi.creer(getEdc(), getCurrentCategorie(), myCorps);
				}
				getEdc().saveChanges();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}

		actualiser();
	}


	/**
	 * 
	 */
	private void supprimerCorps() {
		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer les corps sélectionnés ?", "Oui", "Non"))		
			return;			

		try {
			for (EOCorpsCategEmploi myCorps : (NSArray<EOCorpsCategEmploi>) eodCorps.selectedObjects()) {
				getEdc().deleteObject(myCorps);
			}
			getEdc().saveChanges();
			listenerCategorie.onSelectionChanged();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void ajouterCategorie() 	{
		EOCategorieEmploi nouvelleCategorie = EOCategorieEmploi.creer(getEdc());
		if (SaisieCategorieEmploiCtrl.sharedInstance(getEdc()).modifier(nouvelleCategorie, true)) {
			actualiser();
		}
	}
	/**
	 * 
	 */
	private void modifierCategorie() {

		SaisieCategorieEmploiCtrl.sharedInstance(getEdc()).modifier(getCurrentCategorie(), false);
		myView.getMyEOTableCategorie().updateUI();

	}	

	/**
	 * 
	 */
	private void supprimerCategorie() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette categorie d'emploi ?", "Oui", "Non"))		
			return;			

		try {

			getEdc().deleteObject(getCurrentCategorie());
			getEdc().saveChanges();
			actualiser();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ListenerCorps implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentCorps((EOCorpsCategEmploi)eodCorps.selectedObject());			
			updateInterface();
		}
	}
	private class ListenerCategorie implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener	{
		public void onDbClick() {
			if (getUtilisateur().peutGererNomenclatures()) {
				modifierCategorie();
			}
		}
		public void onSelectionChanged() {

			setCurrentCategorie((EOCategorieEmploi)eodCategorie.selectedObject());
			eodCorps.setObjectArray(new NSArray());

			if (getCurrentCategorie() != null) {
				eodCorps.setObjectArray(EOCorpsCategEmploi.findForCategorie(getEdc(), getCurrentCategorie()));
				myView.getMyEOTableCorps().updateData();

			}
			updateInterface();
		}
	}
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnAjouterCorps().setEnabled(true);
		myView.getBtnSupprimeCorps().setEnabled(getCurrentCorps() != null );

		myView.getBtnModifierCategorie().setEnabled(getCurrentCategorie() != null);
		myView.getBtnSupprimerCategorie().setEnabled(getCurrentCategorie() != null);

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub

	}

	@Override
	protected EOQualifier filterQualifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void traitementsApresCreation() {
		// TODO Auto-generated method stub
		
	}

}

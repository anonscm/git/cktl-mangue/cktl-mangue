// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieEquivalenceCorpsView;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOEquivAncCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieEquivalenceCorpsCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieEquivalenceCorpsCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieEquivalenceCorpsView myView;
	private boolean modeModification;

	private EOEquivAncCorps currentEquivCorps;
	private EOCorps currentCorps;
	private EOGrade currentGrade;
	private EOTypeAcces currentTypeAcces;

	public SaisieEquivalenceCorpsCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		myView = new SaisieEquivalenceCorpsView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				valider();
			}
		}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				annuler();
			}
		}
		);
		myView.getBtnGetCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getCorps();}}
		);
		myView.getBtnGetGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getGrade();}}
		);
		myView.getBtnGetTypeAcces().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {}}
		);

		myView.getTfDateOuverture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateOuverture().addActionListener(new ActionListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateFermeture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFermeture()));
		myView.getTfDateFermeture().addActionListener(new ActionListenerDateTextField(myView.getTfDateFermeture()));

		CocktailUtilities.initTextField(myView.getTfLibelleCorps(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleGrade(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleTypeAcces(), false, false);

	}

	public static SaisieEquivalenceCorpsCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieEquivalenceCorpsCtrl(editingContext);
		return sharedInstance;
	}

	private void clearTextFields()	{

		myView.getTfLibelleCorps().setText("");
		myView.getTfLibelleGrade().setText("");
		myView.getTfLibelleTypeAcces().setText("");
		myView.getTfDateOuverture().setText("");
		myView.getTfDateFermeture().setText("");
		
		myView.getCheckTemTypeAcces().setSelected(false);
		myView.getCheckPromouvabilite().setSelected(false);

	}

	public EOEquivAncCorps ajouter(EOCorps corps)	{

		clearTextFields();
		
		if (corps != null) {
			currentCorps=corps;
			CocktailUtilities.setTextToField(myView.getTfLibelleCorps(), currentCorps.llCorps());
		}

		myView.getBtnGetCorps().setEnabled(true);
		modeModification = false;

		currentEquivCorps = EOEquivAncCorps.creer(ec);

		myView.getTfDateOuverture().setText(DateCtrl.dateToString(new NSTimestamp()));

		updateData();
		myView.setVisible(true);
		
		return currentEquivCorps;
	}

	public boolean modifier(EOEquivAncCorps equiv) {

		clearTextFields();
		currentEquivCorps = equiv;
		currentCorps = equiv.corpsDepart();

		myView.getBtnGetCorps().setEnabled(false);

		updateData();
		modeModification = true;
		myView.setVisible(true);
		return currentEquivCorps != null;
	}

	private void updateData() {

		if(currentEquivCorps.dDebVal() != null)
			myView.getTfDateOuverture().setText(DateCtrl.dateToString(currentEquivCorps.dDebVal()));

		if(currentEquivCorps.dFinVal() != null)
			myView.getTfDateFermeture().setText(DateCtrl.dateToString(currentEquivCorps.dFinVal()));

		currentCorps = currentEquivCorps.corpsDepart();
		if(currentCorps != null)
			myView.getTfLibelleCorps().setText(currentCorps.llCorps());

		currentGrade = currentEquivCorps.gradeEquivalent();
		if(currentGrade != null)
			myView.getTfLibelleGrade().setText(currentGrade.llGrade());

		currentTypeAcces = currentEquivCorps.typeAcces();
		if(currentTypeAcces != null)
			myView.getTfLibelleTypeAcces().setText(currentTypeAcces.libelleLong());

		myView.getCheckPromouvabilite().setSelected(currentEquivCorps.temPromouvabilite()!=null && currentEquivCorps.temPromouvabilite().equals("O"));
		myView.getCheckTemTypeAcces().setSelected(currentEquivCorps.temTtTypeAcces()!=null && currentEquivCorps.temTtTypeAcces().equals("O"));

	}

	private void valider()  {

		try {

			currentEquivCorps.setDModification(new NSTimestamp());
			
			if (myView.getTfDateOuverture().getText().length() > 0)
				currentEquivCorps.setDDebVal(DateCtrl.stringToDate(myView.getTfDateOuverture().getText()));

			if (myView.getTfDateFermeture().getText().length() > 0)
				currentEquivCorps.setDFinVal(DateCtrl.stringToDate(myView.getTfDateFermeture().getText()));
			
			currentEquivCorps.setTemPromouvabilite(myView.getCheckPromouvabilite().isSelected()?"O":"N");
			currentEquivCorps.setTemTtTypeAcces(myView.getCheckTemTypeAcces().isSelected()?"O":"N");

			currentEquivCorps.setCorpsDepartRelationship(currentCorps);
			currentEquivCorps.setGradeEquivalentRelationship(currentGrade);
			currentEquivCorps.setTypeAccesRelationship(currentTypeAcces);

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public void getCorps() {

		CRICursor.setWaitCursor(myView);
		EOCorps corps = CorpsSelectCtrl.sharedInstance(ec).getCorps();

		if (corps != null)	{
			currentCorps = corps;
			myView.getTfLibelleCorps().setText(currentCorps.llCorps());
		}
		CRICursor.setDefaultCursor(myView);
	}
	public void getGrade() {

		CRICursor.setWaitCursor(myView);
		EOGrade grade = GradeSelectCtrl.sharedInstance(ec).getGrade(true, null);

		if (grade != null)	{
			currentGrade = grade;
			myView.getTfLibelleGrade().setText(currentGrade.llGrade());
		}
		CRICursor.setDefaultCursor(myView);
	}

	private void annuler() {
		if(!modeModification)
			ec.deleteObject(currentEquivCorps);
		ec.revert();
		currentEquivCorps = null;
		myView.setVisible(false);
	}
	
	
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	

}

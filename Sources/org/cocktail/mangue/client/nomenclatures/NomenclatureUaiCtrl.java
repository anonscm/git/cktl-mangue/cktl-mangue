// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureUaiView;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.utilities.CRICursor;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclatureUaiCtrl extends ModelePageConsNomenclatures {

	private static NomenclatureUaiCtrl sharedInstance;
	private EODisplayGroup eod;
	private ListenerNomenclature myListener = new ListenerNomenclature();
	private NomenclatureUaiView myView;
	private EORne 				currentUai;

	/**
	 * 
	 * @param editingContext
	 */
	public NomenclatureUaiCtrl(EOEditingContext edc)	{

		super(edc, ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		
		eod = new EODisplayGroup();

		myView = new NomenclatureUaiView(eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());

		myView.getTfFiltre().getDocument().addDocumentListener(new ADocumentListener());

		myView.getBtnAjouter().setVisible(getUtilisateur().peutGererNomenclatures());
		myView.getBtnModifier().setVisible(getUtilisateur().peutGererNomenclatures());
		myView.getMyEOTable().addListener(myListener);
		
		updateInterface();
	}

	public static NomenclatureUaiCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureUaiCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}	

	public EORne getCurrentUai() {
		return currentUai;
	}

	public void setCurrentUai(EORne currentUai) {
		this.currentUai = currentUai;
	}
	
	/**
	 * 
	 * @return
	 */
	private EOQualifier qualifierRecherche() {
		
		NSMutableArray<EOQualifier>	qualifiers = new NSMutableArray<EOQualifier>();
		
		String filtreLibelleCorps  = myView.getTfFiltre().getText();
		if (filtreLibelleCorps.length() > 0) {
			NSMutableArray orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORne.CODE_KEY + " caseInsensitiveLike %@", new NSArray("*"+ myView.getTfFiltre().getText()+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORne.LIBELLE_LONG_KEY + " caseInsensitiveLike %@", new NSArray("*"+ myView.getTfFiltre().getText()+"*")));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(qualifiers);
	}
	
	/**
	 * 
	 */
	private void filter() {
		eod.setQualifier(qualifierRecherche());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();
		
		myView.getLblMessage().setText(eod.displayedObjects().count() + " Etablissements");
		
		updateInterface();
	}
	

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerNomenclature implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (peutGererModule()) {
				modifier();
			}
		}
		public void onSelectionChanged() {
			setCurrentUai((EORne)eod.selectedObject());
			updateInterface();
		}
	}
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(NomenclatureFinder.find(getEdc(), EORne.ENTITY_NAME, EORne.SORT_ARRAY_LIBELLE));
		filter();
		CRICursor.setDefaultCursor(myView);
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnAjouter().setEnabled(true);
		myView.getBtnModifier().setEnabled(getCurrentUai() != null);

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		SaisieUaiCtrl.sharedInstance(getEdc()).modifier(EORne.creer(getEdc()), true);
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		if (SaisieUaiCtrl.sharedInstance(getEdc()).modifier(getCurrentUai(), false)) {
			myListener.onSelectionChanged();
			myView.getMyEOTable().updateUI();
		}
	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		updateDatas();
	}

	@Override
	protected EOQualifier filterQualifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void traitementsApresCreation() {
		// TODO Auto-generated method stub
		actualiser();
	}

}
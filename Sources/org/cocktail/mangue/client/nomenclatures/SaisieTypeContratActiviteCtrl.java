// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieTypeContratActiviteView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOActivitesTypeContrat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieTypeContratActiviteCtrl extends ModelePageSaisieNomenclature {
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieTypeContratActiviteCtrl sharedInstance;
	private SaisieTypeContratActiviteView myView;
	private EOActivitesTypeContrat currentActivite;

	/**
	 * 
	 * @param edc
	 */
	public SaisieTypeContratActiviteCtrl(EOEditingContext edc) {

		super(edc);

		myView = new SaisieTypeContratActiviteView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
				
	}

	public static SaisieTypeContratActiviteCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieTypeContratActiviteCtrl(editingContext);
		return sharedInstance;
	}

	public EOActivitesTypeContrat getCurrentActivite() {
		return currentActivite;
	}
	public void setCurrentActivite(EOActivitesTypeContrat currentActivite) {
		this.currentActivite = currentActivite;
		updateDatas();
	}


	/**
	 * 
	 * @param typeDecharge
	 * @param modeCreation
	 * @return
	 */
	public boolean modifier(EOActivitesTypeContrat activite, boolean modeCreation) {
		setCurrentActivite(activite);
		setModeCreation(modeCreation);
		myView.setVisible(true);
		return getCurrentActivite() != null;
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfActivite());
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentActivite() != null) {
			CocktailUtilities.setTextToField(myView.getTfActivite(), getCurrentActivite().atcoLibelle());
		}
		
		updateInterface();

	}


	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentActivite().setDModification(new NSTimestamp());
		getCurrentActivite().setAtcoLibelle(CocktailUtilities.getTextFromField(myView.getTfActivite()));

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentActivite(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
	}	

}

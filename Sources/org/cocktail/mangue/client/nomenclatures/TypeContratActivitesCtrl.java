package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.cocktail.mangue.client.gui.nomenclatures.TypeContratActivitesView;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.EOActivitesTypeContrat;
import org.cocktail.mangue.modele.mangue.individu.EORepartCtrActivites;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class TypeContratActivitesCtrl {

	private static TypeContratActivitesCtrl 	sharedInstance;
	private static Boolean 			MODE_MODAL = Boolean.FALSE;
	private EOEditingContext		edc;
	private TypeContratActivitesView 			myView;
	private EODisplayGroup 			eod;
	private ListenerActivite 		listenerActivite = new ListenerActivite();

	private EOActivitesTypeContrat	currentActivite;
	private EOTypeContratTravail	currentTypeContrat;	

	public TypeContratActivitesCtrl(EOEditingContext editingContext) {

		edc = editingContext;

		eod = new EODisplayGroup();
		myView = new TypeContratActivitesView(null, MODE_MODAL.booleanValue(), eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		
		myView.getMyEOTable().addListener(listenerActivite);

		updateInterface();
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TypeContratActivitesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new TypeContratActivitesCtrl(editingContext);
		return sharedInstance;
	}
	

	/**
	 * 
	 */
	public void open(EOTypeContratTravail typeContrat)	{

		CRICursor.setWaitCursor(myView);
		setCurrentTypeContrat(typeContrat);
		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);

	}

	

	public EOActivitesTypeContrat getCurrentActivite() {
		return currentActivite;
	}

	public void setCurrentActivite(EOActivitesTypeContrat currentActivite) {
		this.currentActivite = currentActivite;
	}

	public EOTypeContratTravail getCurrentTypeContrat() {
		return currentTypeContrat;
	}

	public void setCurrentTypeContrat(EOTypeContratTravail currentTypeContrat) {
		this.currentTypeContrat = currentTypeContrat;
	}

	public void toFront() {
		myView.toFront();
	}

	/**
	 * 
	 */
	private void actualiser()	{

		eod.setObjectArray(EOActivitesTypeContrat.rechercherActivitesPourTypeContrat(getCurrentTypeContrat()));
		myView.getMyEOTable().updateData();

	}

	private void ajouter() {
		SaisieTypeContratActiviteCtrl.sharedInstance(edc).modifier(EOActivitesTypeContrat.creer(edc, getCurrentTypeContrat()), true);
		actualiser();
	}
	private void modifier() {
		SaisieTypeContratActiviteCtrl.sharedInstance(edc).modifier(getCurrentActivite(), false);
		myView.getMyEOTable().updateUI();
	}
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette activité ?", "Oui", "Non"))		
			return;

		try {
			
			NSArray<EORepartCtrActivites> activites = EORepartCtrActivites.rechercherRepartsActivitesPourAvenantEtActivite(edc, null, getCurrentActivite());
			if (activites.size() > 0) {
				EODialogs.runErrorDialog("ERREUR", "Cette activité est utilisée dans un contrat, vous ne pouvez pas la supprimer !");
				return;
			}
			
			edc.deleteObject(getCurrentActivite());
			edc.saveChanges();
			actualiser();

		}
		catch(Exception e) {
			edc.revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void updateInterface() {
		myView.getBtnAjouter().setVisible(true);
		myView.getBtnModifier().setEnabled(getCurrentActivite() != null);
		myView.getBtnSupprimer().setVisible(getCurrentActivite() != null);
	}

	private class ListenerActivite implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			modifier();
		}

		public void onSelectionChanged() {
			setCurrentActivite((EOActivitesTypeContrat)eod.selectedObject());
			updateInterface();
		}
	}

}

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe d'un modèle de page de saisie
 * 
 * @author Cyril PINSARD
 * @author Chama LAATIK
 *
 */
public abstract class ModelePageSelectNomenclature {

	private EOEditingContext edc;
	private EODisplayGroup eod;
	private INomenclature currentObject;

	protected abstract String getFilterValue();
	protected abstract void traitementsApresFiltre();
	protected abstract void traitementsApresValidation();
	protected abstract void traitementsPourAnnulation();

	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	public ModelePageSelectNomenclature(EOEditingContext edc) {
		this.edc = edc;
		setEod(new EODisplayGroup());
	}

	/**
	 * Action de valider
	 */
	protected void valider()	{
		try {
			traitementsApresValidation();
		} 
		catch (Exception e)	{
			e.printStackTrace();
			return;
		}
	}

	/**
	 * Action d'annuler
	 */
	protected void annuler()	{
		try {
			setCurrentObject(null);
			eod.setSelectionIndexes(new NSArray<INomenclature>());
			traitementsPourAnnulation();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Action d'écoute sur le bouton valider
	 * @param myButton : bouton
	 */
	public void setActionBoutonValiderListener(JButton myButton) {
		myButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { valider(); } });
	}

	/**
	 * Action d'écoute sur le bouton annuler
	 * @param myButton : bouton
	 */
	public void setActionBoutonAnnulerListener(JButton myButton) {
		myButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { annuler(); } });
	}

	/**
	 * Classe d'ecoute sur la saisie
	 * @param myTextField
	 */
	public void setDocumentFiltreListener(JTextField myTextField) {
		myTextField.getDocument().addDocumentListener(new ADocumentListener());
	}

	/**
	 * 
	 * @return
	 */
	public EOEditingContext getEdc() {
		return edc;
	}

	/**
	 * 
	 * @param edc
	 */
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EODisplayGroup getEod() {
		return eod;
	}
	public void setEod(EODisplayGroup eod) {
		this.eod = eod;
	}

	public INomenclature getCurrentObject() {
		return currentObject;
	}
	public void setCurrentObject(INomenclature currentObject) {
		this.currentObject = currentObject;
	}

	protected void filter() {
		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();
		traitementsApresFiltre();
	}

	protected void setListener(ZEOTable myView) {
		myView.addListener(new ListenerObject());
	}
	
	/**
	 * 
	 * @return
	 */
	protected EOQualifier getFilterQualifier()	{
		
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		if (!StringCtrl.chaineVide(getFilterValue()))	{
			
			NSArray args = new NSArray("*" + getFilterValue() + "*");
			
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + " caseInsensitiveLike %@",args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_LONG_KEY + " caseInsensitiveLike %@",args));
			
			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(andQualifiers);       

	}
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}


	public class ListenerObject implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			valider();
		}
		public void onSelectionChanged() {
			setCurrentObject((INomenclature)getEod().selectedObject());
		}
	}
}
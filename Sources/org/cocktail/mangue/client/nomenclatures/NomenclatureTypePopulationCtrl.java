package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureTypePopulationView;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.NomenclatureAvecDate;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class NomenclatureTypePopulationCtrl {

	private static final int INDEX_OUVERTS = 1;
	private static final int INDEX_FERMES = 2;
	
	private static NomenclatureTypePopulationCtrl sharedInstance;
	private EOEditingContext edc;
	private EODisplayGroup eod;
	private EOAgentPersonnel	currentUtilisateur;

	private ListenerTypePopulation listenerTypePopulation= new ListenerTypePopulation();

	private NomenclatureTypePopulationView myView;
	private EOTypePopulation currentTypePopulation;

	public NomenclatureTypePopulationCtrl(EOEditingContext edc)	{

		this.edc = edc;

		eod = new EODisplayGroup();

		myView = new NomenclatureTypePopulationView(eod);

		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getMyEOTable().addListener(listenerTypePopulation);

		myView.getBtnImprimer().setEnabled(false);
		myView.getBtnExporter().setEnabled(false);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		
		myView.getBtnAjouter().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifier().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimer().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnExporter().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnImprimer().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		
		myView.getPopupEtats().setSelectedIndex(INDEX_OUVERTS);
		myView.getPopupEtats().addActionListener(new PopupEtatListener());

	}

	public static NomenclatureTypePopulationCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureTypePopulationCtrl(editingContext);
		return sharedInstance;
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}

	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	public EOTypePopulation getCurrentTypePopulation() {
		return currentTypePopulation;
	}

	public void setCurrentTypePopulation(EOTypePopulation currentTypePopulation) {
		this.currentTypePopulation = currentTypePopulation;
	}

	public JPanel getView() {
		return myView;
	}	

	public void actualiser() {

		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(NomenclatureFinder.findStatic(edc, EOTypePopulation.ENTITY_NAME));
		filter();		
		CRICursor.setDefaultCursor(myView);
	}
	private void filter() {
		eod.setQualifier(getQualifierRecherche());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();
		updateInterface();
	}
	
	private class PopupEtatListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			filter();
		}
	}
	/**
	 * 
	 * @return
	 */
	private EOQualifier getQualifierRecherche() {
		
		NSMutableArray	qualifiers = new NSMutableArray();
		
		if (myView.getPopupEtats().getSelectedIndex() == INDEX_OUVERTS) {
			qualifiers.addObject(NomenclatureAvecDate.qualifierPourPeriode(DateCtrl.today(), DateCtrl.today()));
		}

		if (myView.getPopupEtats().getSelectedIndex() == INDEX_FERMES) {			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.DATE_FERMETURE_KEY + " != nil", null));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.DATE_FERMETURE_KEY + " <= %@", new NSArray(DateCtrl.today())));			
		}
		
		return new EOAndQualifier(qualifiers);
	}
	private void modifier() {
		if (SaisieTypePopulationCtrl.sharedInstance(edc).modifier(getCurrentTypePopulation(), false)) {
			listenerTypePopulation.onSelectionChanged();
			myView.getMyEOTable().updateUI();
		}		
	}	

	public void imprimer() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}
	public void exporter() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}


	private void updateInterface(){
		myView.getBtnModifier().setEnabled(getCurrentTypePopulation() != null);
		myView.getBtnAjouter().setEnabled(false);
		myView.getBtnSupprimer().setEnabled(false);
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerTypePopulation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererNomenclatures()) {
				modifier();
			}
		}

		public void onSelectionChanged() {
			setCurrentTypePopulation((EOTypePopulation)eod.selectedObject());
			updateInterface();
		}
	}


}

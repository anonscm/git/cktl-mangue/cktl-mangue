// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieHierarchieGradeView;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOHierarchieGrade;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisieHierarchieGradeCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieHierarchieGradeCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieHierarchieGradeView myView;

	private EOHierarchieGrade currentHierarchie;
	private EOGrade currentGradeInitial;
	private EOGrade currentGradeSuivant;

	public SaisieHierarchieGradeCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		myView = new SaisieHierarchieGradeView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				valider();
			}
		}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				annuler();
			}
		}
		);
		myView.getBtnGetGradeInitial().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getGradeInitial();}}
		);
		myView.getBtnGetGradeSuivant().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getGradeSuivant();}}
		);

		myView.getTfDateOuverture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateOuverture().addActionListener(new ActionListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateFermeture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFermeture()));
		myView.getTfDateFermeture().addActionListener(new ActionListenerDateTextField(myView.getTfDateFermeture()));

		CocktailUtilities.initTextField(myView.getTfGradeInitial(), false, false);
		CocktailUtilities.initTextField(myView.getTfGradeSuivant(), false, false);

	}

	public static SaisieHierarchieGradeCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieHierarchieGradeCtrl(editingContext);
		return sharedInstance;
	}

	public EOHierarchieGrade currentHierarchie() {
		return currentHierarchie;
	}

	public void setCurrentHierarchie(EOHierarchieGrade currentHierarchie) {
		this.currentHierarchie = currentHierarchie;
		updateData();
	}

	public EOGrade currentGradeInitial() {
		return currentGradeInitial;
	}

	public void setCurrentGradeInitial(EOGrade currentGradeInitial) {
		this.currentGradeInitial = currentGradeInitial;
		CocktailUtilities.setTextToField(myView.getTfGradeInitial(), "");
		if (currentGradeInitial != null) {
			CocktailUtilities.setTextToField(myView.getTfGradeInitial(), currentGradeInitial.cGrade() + " - " + currentGradeInitial.llGrade());			
		}
	}

	public EOGrade currentGradeSuivant() {
		return currentGradeSuivant;
	}

	public void setCurrentGradeSuivant(EOGrade currentGradeSuivant) {
		this.currentGradeSuivant = currentGradeSuivant;
		CocktailUtilities.setTextToField(myView.getTfGradeSuivant(), "");
		if (currentGradeSuivant != null) {
			CocktailUtilities.setTextToField(myView.getTfGradeSuivant(), currentGradeSuivant.cGrade() + " - " + currentGradeSuivant.llGrade());			
		}
	}

	private void clearTextFields()	{

		setCurrentGradeInitial(null);
		setCurrentGradeInitial(null);

		myView.getTfDateOuverture().setText("");
		myView.getTfDateFermeture().setText("");

	}

	public EOHierarchieGrade ajouter()	{

		clearTextFields();

		myView.getBtnGetGradeInitial().setVisible(true);
		setCurrentHierarchie(EOHierarchieGrade.creer(ec));

		myView.setVisible(true);

		return currentHierarchie();
	}

	public boolean modifier(EOHierarchieGrade hierarchie) {
		setCurrentHierarchie(hierarchie);
		myView.setVisible(true);
		return currentHierarchie() != null;
	}

	private void updateData() {

		clearTextFields();

		if (currentHierarchie() != null) {
			setCurrentGradeInitial(currentHierarchie().toGradeInitial());
			setCurrentGradeSuivant(currentHierarchie().toGradeSuivant());

			CocktailUtilities.setDateToField(myView.getTfDateOuverture(), currentHierarchie().dDebValidite());
			CocktailUtilities.setDateToField(myView.getTfDateFermeture(), currentHierarchie().dFinValidite());
		}		

	}

	private void valider()  {

		try {

			currentHierarchie().setToGradeInitialRelationship(currentGradeInitial());
			currentHierarchie().setToGradeSuivantRelationship(currentGradeSuivant());

			currentHierarchie().setDDebValidite(CocktailUtilities.getDateFromField(myView.getTfDateOuverture()));
			currentHierarchie().setDFinValidite(CocktailUtilities.getDateFromField(myView.getTfDateFermeture()));

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public void getGradeInitial() {
		CRICursor.setWaitCursor(myView);
		EOGrade grade = GradeSelectCtrl.sharedInstance(ec).getGrade(false, null);
		if (grade != null)
			setCurrentGradeInitial(grade);

		CRICursor.setDefaultCursor(myView);
	}
	public void getGradeSuivant() {
		CRICursor.setWaitCursor(myView);
		EOGrade grade = GradeSelectCtrl.sharedInstance(ec).getGrade(false, null);
		if (grade != null)
			setCurrentGradeSuivant(grade);

		CRICursor.setDefaultCursor(myView);
	}

	private void annuler() {
		ec.revert();
		setCurrentHierarchie(null);
		myView.setVisible(false);
	}


	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}

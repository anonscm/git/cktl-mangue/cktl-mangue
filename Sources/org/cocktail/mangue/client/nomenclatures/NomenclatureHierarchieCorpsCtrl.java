// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureHierarchieCorpsView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOHierarchieCorps;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class NomenclatureHierarchieCorpsCtrl extends ModelePageConsNomenclatures{

	private static NomenclatureHierarchieCorpsCtrl sharedInstance;
	private EODisplayGroup eod;

	private MyListener listener = new MyListener();
	private NomenclatureHierarchieCorpsView myView;
	private EOHierarchieCorps currentCorps;

	public NomenclatureHierarchieCorpsCtrl(EOEditingContext edc)	{

		super(edc, ((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());

		eod = new EODisplayGroup();

		myView = new NomenclatureHierarchieCorpsView(eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		
		myView.getMyEOTable().addListener(listener);
		myView.getTfFiltre().getDocument().addDocumentListener(new ADocumentListener());
	}

	public static NomenclatureHierarchieCorpsCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureHierarchieCorpsCtrl(editingContext);
		return sharedInstance;
	}

	public EOHierarchieCorps getCurrentCorps() {
		return currentCorps;
	}

	public void setCurrentCorps(EOHierarchieCorps currentCorps) {
		this.currentCorps = currentCorps;
	}

	public JPanel getView() {
		return myView;
	}	
	
	private void filter() {
		
		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();
		updateInterface();
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class MyListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (peutGererModule()) {
				modifier();
			}
		}
		public void onSelectionChanged() {
			setCurrentCorps((EOHierarchieCorps)eod.selectedObject());
		}
	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnAjouter().setEnabled(true && peutGererModule());
		myView.getBtnModifier().setEnabled(getCurrentCorps() != null && peutGererModule());
		myView.getBtnSupprimer().setEnabled(false );
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EOHierarchieCorps.fetchAll(getEdc(), EOHierarchieCorps.SORT_ARRAY_LIBELLE_ASC));
		filter();
		CRICursor.setDefaultCursor(myView);
	}

	@Override
	protected EOQualifier filterQualifier() {
		// TODO Auto-generated method stub
		NSMutableArray	qualifiers = new NSMutableArray(),orQualifiers = new NSMutableArray();

		String filtreLibelleCorps  = myView.getTfFiltre().getText();
		if (filtreLibelleCorps.length() > 0) {
			orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHierarchieCorps.TO_CORPS_INITIAL_KEY+"."+EOCorps.C_CORPS_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHierarchieCorps.TO_CORPS_INITIAL_KEY+"."+EOCorps.LC_CORPS_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHierarchieCorps.TO_CORPS_INITIAL_KEY+"."+EOCorps.LL_CORPS_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(qualifiers);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		SaisieHierarchieCorpsCtrl.sharedInstance(getEdc()).ajouter();
	}

	@Override
	protected void traitementsApresCreation() {
		// TODO Auto-generated method stub
		actualiser();
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		SaisieHierarchieCorpsCtrl.sharedInstance(getEdc()).modifier(getCurrentCorps());
		myView.getMyEOTable().updateUI();
	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(getCurrentCorps());
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		
	}

}

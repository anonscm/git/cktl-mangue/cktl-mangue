// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.application.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclaturesTypesContratTravailView;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.NomenclatureAvecDate;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.grhum.EOActivitesTypeContrat;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOTypeContratGrades;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionContrat;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclatureTypesContratTravailCtrl {

	private static NomenclatureTypesContratTravailCtrl sharedInstance;
	private EOEditingContext ec;
	private EODisplayGroup eod, eodGrade;

	private ListenerTypeContrat listenerTypeContrat = new ListenerTypeContrat();
	private ListenerGrade listenerGrade = new ListenerGrade();

	private NomenclaturesTypesContratTravailView myView;
	private TypeContratRenderer	monRendererColor = new TypeContratRenderer();
	private TypeContratVisibleRenderer	rendererTypeContratVisible = new TypeContratVisibleRenderer();

	private EOAgentPersonnel 		currentUtilisateur;
	private EOTypeContratTravail 	currentTypeContrat;
	private EOTypeContratGrades 	currentGrade;

	/**
	 * 
	 * @param editingContext
	 */
	public NomenclatureTypesContratTravailCtrl(EOEditingContext editingContext)	{

		ec = editingContext;

		eod = new EODisplayGroup();
		eodGrade = new EODisplayGroup();

		myView = new NomenclaturesTypesContratTravailView(eod, eodGrade, monRendererColor, rendererTypeContratVisible, EOGrhumParametres.isGestionHu());

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnDupliquer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {dupliquer();}}
				);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporter();}}
				);

		myView.getBtnAjouterGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterGrade();}}
				);
		myView.getBtnSupprimerGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerGrade();}}
				);
		myView.getBtnActivites().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherActivites();}}
				);

		myView.getTfFiltreLibelle().getDocument().addDocumentListener(new ADocumentListener());

		CocktailUtilities.initPopupOuiNon(myView.getPopupVisible(), true);
		myView.getPopupVisible().setSelectedItem("O");

		CocktailUtilities.initPopupOuiNon(myView.getPopupLocal(), true);

		myView.getMyEOTableTypeContratTravail().addListener(listenerTypeContrat);
		myView.getMyEOTableGrades().addListener(listenerGrade);

		myView.getPopupEtats().setSelectedIndex(1);
		myView.getPopupEtats().addActionListener(new PopupsListener());

		myView.getPopupNiveaux().addActionListener(new PopupsListener());
		myView.getPopupVisible().addActionListener(new PopupsListener());
		myView.getPopupLocal().addActionListener(new PopupsListener());
		myView.getPopupType().addActionListener(new PopupsListener());

		myView.getBtnImprimer().setEnabled(false);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		myView.getBtnAjouter().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifier().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimer().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnAjouterGrade().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimerGrade().setVisible(currentUtilisateur().peutGererNomenclatures());

		myView.getBtnImprimer().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnExporter().setVisible(currentUtilisateur().peutGererNomenclatures());

	}

	public static NomenclatureTypesContratTravailCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureTypesContratTravailCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}	


	public EOTypeContratTravail getCurrentTypeContrat() {
		return currentTypeContrat;
	}

	/**
	 * 
	 * @param currentTypeContrat
	 */
	public void setCurrentTypeContrat(EOTypeContratTravail currentTypeContrat) {
		this.currentTypeContrat = currentTypeContrat;
		myView.getBtnActivites().setText("Activités (0)");
		if (currentTypeContrat != null) {
			NSArray activites = EOActivitesTypeContrat.rechercherActivitesPourTypeContrat(currentTypeContrat);
			myView.getBtnActivites().setText("Activités (" + activites.size() + ")");			
		}
	}

	public EOAgentPersonnel currentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	public EOTypeContratGrades currentGrade() {
		return currentGrade;
	}

	public void setCurrentGrade(EOTypeContratGrades currentGrade) {
		this.currentGrade = currentGrade;
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray	qualifiers = new NSMutableArray(),orQualifiers = new NSMutableArray();

		String filtreLibelleCorps  = myView.getTfFiltreLibelle().getText();
		if (filtreLibelleCorps.length() > 0) {
			orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeContratTravail.CODE_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeContratTravail.LIBELLE_COURT_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeContratTravail.LIBELLE_LONG_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		if (myView.getPopupNiveaux().getSelectedIndex() > 0 ) {
			qualifiers.addObject(EOTypeContratTravail.getQualifierNiveau(getCurrentNiveau()));
		}
		if (myView.getPopupVisible().getSelectedIndex() > 0 ) {
			qualifiers.addObject(EOTypeContratTravail.getQualifierVisible((String)myView.getPopupVisible().getSelectedItem()));
		}
		if (myView.getPopupLocal().getSelectedIndex() > 0 ) {
			qualifiers.addObject(EOTypeContratTravail.getQualifierLocal((String)myView.getPopupLocal().getSelectedItem()));
		}

		if (myView.getPopupEtats().getSelectedIndex() == 1) {
			qualifiers.addObject(NomenclatureAvecDate.qualifierPourPeriode(DateCtrl.today(), DateCtrl.today()));
		}

		if (myView.getPopupEtats().getSelectedIndex() == 2) {			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.DATE_FERMETURE_KEY + " != nil", null));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.DATE_FERMETURE_KEY + " <= %@", new NSArray(DateCtrl.today())));			
		}
		if (myView.getPopupType().getSelectedIndex() > 0) {
			if (myView.getPopupType().getSelectedIndex() == 1) {
				qualifiers.addObject(EOTypeContratTravail.getQualifierEnseignant(false));
				qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));
			} else
				if (myView.getPopupType().getSelectedIndex() == 2) {
					qualifiers.addObject(EOTypeContratTravail.getQualifierEnseignant(true));
					qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));
				} else
					if (myView.getPopupType().getSelectedIndex() == 3) {
						qualifiers.addObject(EOTypeContratTravail.getQualifierCdi(true));
					} else
						if (myView.getPopupType().getSelectedIndex() == 4) {
							qualifiers.addObject(EOTypeContratTravail.getQualifierDoctorant(true));
						} else
							if (myView.getPopupType().getSelectedIndex() == 5) {
								qualifiers.addObject(EOTypeContratTravail.getQualifierEtudiant(true));
							} else
								if (myView.getPopupType().getSelectedIndex() == 6) {
									qualifiers.addObject(EOTypeContratTravail.getQualifierInvite(true));
									qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));
								} else
									if (myView.getPopupType().getSelectedIndex() == 7) {
										qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(true));
									}
		} else {
			qualifiers.addObject(EOTypeContratTravail.qualifierHU());
		}

		return new EOAndQualifier(qualifiers);
	}

	/**
	 * 
	 */
	private void filter() {

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();

		myView.getMyEOTableTypeContratTravail().updateData();
		updateInterface();
	}

	/**
	 * 
	 */
	private void afficherActivites() {
		TypeContratActivitesCtrl.sharedInstance(ec).open(getCurrentTypeContrat());
	}

	/**
	 * 
	 */
	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		eod.setObjectArray(EOTypeContratTravail.findTypesContrat(ec));
		filter();

		updateInterface();
		CRICursor.setDefaultCursor(myView);

	}


	/**
	 * 
	 * @return
	 */
	private Integer getCurrentNiveau() {
		if (myView.getPopupNiveaux().getSelectedIndex() > 0) {
			return (Integer)myView.getPopupNiveaux().getSelectedItem();
		}

		return null;
	}

	/**
	 * 
	 */
	private void ajouter() 	{

		EOTypeContratTravail typeContratPere = null;
		if (getCurrentTypeContrat().cNiveau().intValue() == 1)
			typeContratPere = getCurrentTypeContrat();
		else
			typeContratPere = getCurrentTypeContrat().toTypeContratPere();

		EOTypeContratTravail myTypeContrat = EOTypeContratTravail.creer(ec, typeContratPere);
		if (SaisieTypeContratTravailCtrl.sharedInstance(ec).modifier(myTypeContrat, true)) {
			actualiser();
			CocktailUtilities.forceSelection(myView.getMyEOTableTypeContratTravail(), new NSArray(myTypeContrat));
		}
	}

	/**
	 * 
	 */
	private void dupliquer() {

		EOTypeContratTravail myTypeContrat = getCurrentTypeContrat().dupliquer(ec);
		if (SaisieTypeContratTravailCtrl.sharedInstance(ec).modifier(myTypeContrat, true)) {
			actualiser();
			CocktailUtilities.forceSelection(myView.getMyEOTableTypeContratTravail(), new NSArray(myTypeContrat));
		}
	}

	/**
	 * 
	 */
	public void ajouterGrade() {

		EOGrade grade = GradeSelectCtrl.sharedInstance(ec).getGrade(EOGrhumParametres.isGestionHu(), null);

		if (grade != null) {

			try {

				CRICursor.setWaitCursor(myView);
				EOTypeContratGrades.creer(ec, getCurrentTypeContrat(), grade);

				ec.saveChanges();
				listenerTypeContrat.onSelectionChanged();

				CRICursor.setDefaultCursor(myView);
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
	public void supprimerGrade() {

		try {
			ec.deleteObject(currentGrade());
			ec.saveChanges();
			listenerTypeContrat.onSelectionChanged();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void modifier() {
		if (SaisieTypeContratTravailCtrl.sharedInstance(ec).modifier(getCurrentTypeContrat(), false)) {
			listenerTypeContrat.onSelectionChanged();
			myView.getMyEOTableTypeContratTravail().updateUI();
		}
	}

	/**
	 * 
	 */
	private void supprimer() {

		if (!EODialogs.runConfirmOperationDialog("Attention", 
				"Confirmez-vous cette suppression de ce type de contrat ?", "Oui", "Non")) {		
			return;			
		}

		try {

			// Ce type de contrat est il utilise dans Mangue ?
			NSMutableArray<EOGenericRecord> contrats = new NSMutableArray<EOGenericRecord>();
			contrats.addObjectsFromArray(EOContrat.findForType(ec, getCurrentTypeContrat()));
			contrats.addObjectsFromArray(EOInclusionContrat.findForType(ec, getCurrentTypeContrat()));
			if (contrats.size() > 0) {
				throw new NSValidation.ValidationException("Ce type de contrat est utilisé , vous ne pouvez pas le supprimer !");			
			}

			ec.deleteObject(getCurrentTypeContrat());
			ec.saveChanges();
			actualiser();

		} catch (ValidationException ex)	{
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void imprimer() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}


	private boolean peutAjouter() {
		return getCurrentTypeContrat() != null;
	}
	private boolean peutDupliquer() {
		return getCurrentTypeContrat() != null && getCurrentTypeContrat().cNiveau().intValue() == 2;
	}
	private boolean peutModifier() {
		return getCurrentTypeContrat() != null;
	}
	private boolean peutSupprimer() {
		return peutModifier() && getCurrentTypeContrat().isLocal();
	}

	/**
	 * 
	 */
	private void updateInterface(){

		myView.getBtnAjouter().setEnabled(peutAjouter());
		myView.getBtnModifier().setEnabled(peutModifier());
		myView.getBtnSupprimer().setEnabled(peutSupprimer());
		myView.getBtnDupliquer().setEnabled(peutDupliquer());
		myView.getBtnActivites().setEnabled(peutModifier());

		myView.getBtnAjouterGrade().setEnabled(peutModifier());
		myView.getBtnSupprimerGrade().setEnabled(peutModifier() && currentGrade() !=null);

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class ListenerTypeContrat implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (peutModifier())
				modifier();
		}

		public void onSelectionChanged() {

			setCurrentTypeContrat((EOTypeContratTravail)eod.selectedObject());
			eodGrade.setObjectArray(EOTypeContratGrades.rechercherPourTypeContrat(ec, getCurrentTypeContrat()));
			myView.getMyEOTableGrades().updateData();

			updateInterface();
		}
	}
	private class ListenerGrade implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
		}

		public void onSelectionChanged() {
			setCurrentGrade((EOTypeContratGrades)eodGrade.selectedObject());
			updateInterface();
		}
	}

	private class PopupsListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			filter();
		}
	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	/**
	 * Classe servant a colorer les cellules selon l'etat de l'engagement
	 */
	private class TypeContratRenderer extends ZEOTableCellRenderer	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2907930349355563787L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOTypeContratTravail obj = (EOTypeContratTravail) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.codeSupinfo() == null)
				leComposant.setBackground(new Color(255,142,134));
			else
				leComposant.setBackground(CocktailConstantes.COLOR_BKG_TABLE_VIEW);

			return leComposant;
		}
	}

	/**
	 * Classe servant a colorer les cellules selon l'etat de l'engagement
	 */
	private class TypeContratVisibleRenderer extends ZEOTableCellRenderer	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2907930349355563787L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOTypeContratTravail obj = (EOTypeContratTravail) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.estVisible() == false)
				leComposant.setForeground(new Color(150, 150, 150));
			else
				leComposant.setForeground(new Color(0,0,0));
			return leComposant;
		}
	}

	/**
	 * 
	 */
	public void exporter() {

		CRICursor.setWaitCursor(myView);
		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "ExportTypesContrats";

			saveDialog.setSelectedFile(new File(nomFichierExport + CocktailConstantes.EXTENSION_CSV));
			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = enteteExport();

				NSMutableArray<EOSortOrdering> mySort = new NSMutableArray<EOSortOrdering>();
				mySort.addObject(new EOSortOrdering(EOTypeContratTravail.C_NIVEAU_KEY, EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering(EOTypeContratTravail.LIBELLE_LONG_KEY, EOSortOrdering.CompareAscending));
				
				for (EOTypeContratTravail myRecord : new NSArray<EOTypeContratTravail>(EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(), mySort) ))
					texte += texteExportPourRecord(myRecord) + CocktailExports.SAUT_DE_LIGNE;

				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, CocktailConstantes.EXTENSION_CSV, false);

				CRICursor.setDefaultCursor(myView);

				UtilitairesFichier.openFile(file.getPath());

			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @return
	 */
	private String enteteExport() {
		return CocktailExports.getEntete(new String[]{"CODE", "LIB COURT", "LIB LONG", "SUPINFO", "CODE PERE", "LIB PERE", "NIV", "CDI", "GRADES", "ENS", "OUVERTURE", "FERMETURE"});
	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EOTypeContratTravail record)    {

		String texte = "";

		// CODE", "LIB COURT", "LIB LONG", "SUPINFO", "CODE PERE", "LIB PERE", "NIV", "CDI", "GRADES", "ENS", "OUVERTURE", "FERMETURE"});

		texte += CocktailExports.ajouterChamp(record.code());
		texte += CocktailExports.ajouterChamp(record.libelleCourt());
		texte += CocktailExports.ajouterChamp(record.libelleLong());
		texte += CocktailExports.ajouterChamp(record.codeSupinfo());
		if (record.toTypeContratPere() != null) {
		texte += CocktailExports.ajouterChamp(record.toTypeContratPere().code());
		texte += CocktailExports.ajouterChamp(record.toTypeContratPere().libelleLong());
		}
		else {
			texte += CocktailExports.ajouterChampVide(2);			
		}
		texte += CocktailExports.ajouterChampNumber(record.cNiveau());
		texte += CocktailExports.ajouterChamp(record.temCdi());
		texte += CocktailExports.ajouterChamp(record.temEquivGrade());
		texte += CocktailExports.ajouterChamp(record.temEnseignant());
		texte += CocktailExports.ajouterChampDate(record.dateOuverture());
		texte += CocktailExports.ajouterChampDate(record.dateFermeture());

		return texte;
	}


}

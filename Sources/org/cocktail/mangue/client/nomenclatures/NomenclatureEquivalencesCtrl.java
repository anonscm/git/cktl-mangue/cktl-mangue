// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureEquivalencesView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOEquivAncCorps;
import org.cocktail.mangue.modele.grhum.EOEquivAncGrade;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclatureEquivalencesCtrl {

	private static NomenclatureEquivalencesCtrl sharedInstance;
	private EOEditingContext edc;
	private EODisplayGroup eodCorps, eodGrade, eodEquivCorps, eodEquivGrade;

	private ListenerCorps listenerCorps = new ListenerCorps();
	private ListenerGrade listenerGrade = new ListenerGrade();
	private ListenerEquivCorps listenerEquivCorps = new ListenerEquivCorps();
	private ListenerEquivGrade listenerEquivGrade = new ListenerEquivGrade();

	private NomenclatureEquivalencesView myView;

	private EOCorps currentCorpsDepart;
	private EOGrade currentGradeDepart;
	private EOEquivAncCorps currentEquivCorps;
	private EOEquivAncGrade currentEquivGrade;
	private EOAgentPersonnel	currentUtilisateur;

	public NomenclatureEquivalencesCtrl(EOEditingContext editingContext)	{

		setEdc(editingContext);

		eodCorps = new EODisplayGroup();
		eodGrade = new EODisplayGroup();
		eodEquivCorps = new EODisplayGroup();
		eodEquivGrade = new EODisplayGroup();

		myView = new NomenclatureEquivalencesView(eodCorps, eodGrade, eodEquivCorps, eodEquivGrade);

		myView.getBtnAjouterEquivCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterEquivalenceCorps();}}
				);
		myView.getBtnModifierEquivCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierEquivalenceCorps();}}
				);
		myView.getBtnSupprimerEquivCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerEquivalenceCorps();}}
				);
		myView.getBtnAjouterEquivGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterEquivalenceGrade();}}
				);
		myView.getBtnModifierEquivGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierEquivalenceGrade();}}
				);
		myView.getBtnSupprimerEquivGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerEquivalenceGrade();}}
				);

		myView.getMyEOTableCorps().addListener(listenerCorps);
		myView.getMyEOTableGrade().addListener(listenerGrade);
		myView.getMyEOTableEquivCorps().addListener(listenerEquivCorps);
		myView.getMyEOTableEquivGrade().addListener(listenerEquivGrade);

		eodCorps.setSortOrderings(EOCorps.SORT_ARRAY_LIBELLE_ASC);
		eodGrade.setSortOrderings(EOGrade.SORT_ARRAY_LIBELLE_ASC);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		myView.getBtnAjouterEquivCorps().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifierEquivCorps().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimerEquivCorps().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnAjouterEquivGrade().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifierEquivGrade().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimerEquivGrade().setVisible(getCurrentUtilisateur().peutGererNomenclatures());

	}

	public static NomenclatureEquivalencesCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureEquivalencesCtrl(editingContext);
		return sharedInstance;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}

	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	public JPanel getView() {
		return myView;
	}	

	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		eodCorps.setObjectArray(EOEquivAncCorps.rechercherCorpsDepart(edc));
		eodGrade.setObjectArray(EOEquivAncGrade.rechercherGradesDepart(edc));

		myView.getMyEOTableCorps().updateData();
		myView.getMyEOTableGrade().updateData();

		updateUI();

		CRICursor.setDefaultCursor(myView);
	}


	private void ajouterEquivalenceCorps() 	{
		if (SaisieEquivalenceCorpsCtrl.sharedInstance(edc).ajouter(currentCorpsDepart) != null);
		actualiser();
	}
	private void modifierEquivalenceCorps() {

		if (SaisieEquivalenceCorpsCtrl.sharedInstance(edc).modifier(currentEquivCorps));
		actualiser();
	}	
	private void supprimerEquivalenceCorps() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer les équivalences sélectionnées ?", "Oui", "Non"))		
			return;			

		try {
			edc.deleteObject(currentEquivCorps);
			edc.saveChanges();
			listenerCorps.onSelectionChanged();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void ajouterEquivalenceGrade() 	{

		if (SaisieEquivalenceGradeCtrl.sharedInstance(edc).ajouter(currentGradeDepart) != null);
		actualiser();
	}
	private void modifierEquivalenceGrade() {

		if (SaisieEquivalenceGradeCtrl.sharedInstance(edc).modifier(currentEquivGrade));
		actualiser();
	}	
	private void supprimerEquivalenceGrade() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer les équivalences sélectionnées ?", "Oui", "Non"))		
			return;			

		try {
			edc.deleteObject(currentEquivGrade);
			edc.saveChanges();
			listenerGrade.onSelectionChanged();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void updateUI(){

		myView.getBtnAjouterEquivCorps().setEnabled(true);
		myView.getBtnModifierEquivCorps().setEnabled(currentCorpsDepart != null );
		myView.getBtnSupprimerEquivCorps().setEnabled(currentCorpsDepart != null );

		myView.getBtnAjouterEquivGrade().setEnabled(true);
		myView.getBtnModifierEquivGrade().setEnabled(currentGradeDepart != null );
		myView.getBtnSupprimerEquivGrade().setEnabled(currentGradeDepart != null );

	}

	private class ListenerCorps implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			currentCorpsDepart = (EOCorps)eodCorps.selectedObject();
			eodEquivCorps.setObjectArray(new NSArray());
			if (currentCorpsDepart != null)
				eodEquivCorps.setObjectArray(EOEquivAncCorps.equivalencesCorpsPourCorps(edc, currentCorpsDepart, new NSTimestamp()));			

			myView.getMyEOTableEquivCorps().updateData();
			updateUI();
		}
	}
	private class ListenerGrade implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener	{
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			currentGradeDepart = (EOGrade)eodGrade.selectedObject();
			eodEquivGrade.setObjectArray(new NSArray());
			if (currentGradeDepart != null)
				eodEquivGrade.setObjectArray(EOEquivAncGrade.equivalencesGradePourGrade(edc, currentGradeDepart, new NSTimestamp()));
			myView.getMyEOTableEquivGrade().updateData();
			updateUI();
		}
	}
	private class ListenerEquivCorps implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererNomenclatures()) {
				modifierEquivalenceCorps();
			}
		}
		public void onSelectionChanged() {
			currentEquivCorps = (EOEquivAncCorps)eodEquivCorps.selectedObject();
			updateUI();
		}
	}
	private class ListenerEquivGrade implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener	{
		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererNomenclatures()) {
				modifierEquivalenceGrade();
			}
		}
		public void onSelectionChanged() {
			currentEquivGrade = (EOEquivAncGrade)eodEquivGrade.selectedObject();
			updateUI();
		}
	}

}

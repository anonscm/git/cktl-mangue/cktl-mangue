// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieGradeView;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieGradeCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieGradeCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieGradeView myView;
	private boolean modeModification;

	private EOGrade currentGrade;
	private EOCorps currentCorps;

	public SaisieGradeCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		myView = new SaisieGradeView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);

		myView.getTfDateOuverture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateOuverture().addActionListener(new ActionListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateFermeture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFermeture()));
		myView.getTfDateFermeture().addActionListener(new ActionListenerDateTextField(myView.getTfDateFermeture()));

	}

	public static SaisieGradeCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieGradeCtrl(editingContext);
		return sharedInstance;
	}

	public EOGrade getCurrentGrade() {
		return currentGrade;
	}
	public void setCurrentGrade(EOGrade currentGrade) {
		this.currentGrade = currentGrade;
		setCurrentCorps(currentGrade.toCorps());
		myView.getTfTitre().setText(currentCorps.llCorps());
		updateData();
	}
	public EOCorps getCurrentCorps() {
		return currentCorps;
	}
	public void setCurrentCorps(EOCorps currentCorps) {
		this.currentCorps = currentCorps;
	}

	private void clearTextFields()	{
		myView.getTfCode().setText("");
		myView.getTfLibelle().setText("");
		myView.getTfLibelleCourt().setText("");
		myView.getTfDateOuverture().setText("");
		myView.getTfDateFermeture().setText("");
		myView.getPopupCategories().setSelectedIndex(0);
	}

	public EOGrade ajouter(EOCorps corps)	{
		clearTextFields();
		modeModification = false;
		setCurrentGrade(EOGrade.createEOGrade(ec, corps, null, new NSTimestamp(), new NSTimestamp()));
		getCurrentGrade().setTemLocal(CocktailConstantes.VRAI);
		myView.setVisible(true);
		return currentGrade;
	}

	public EOGrade dupliquer(EOGrade grade)	{

		clearTextFields();
		currentCorps = grade.toCorps();
		modeModification = false;

		currentGrade = EOGrade.createEOGrade(
				ec, grade.toCorps(), grade.cGrade(), new NSTimestamp(), new NSTimestamp());

		currentGrade.setTemLocal(CocktailConstantes.VRAI);
		currentGrade.setLcGrade(grade.lcGrade());
		currentGrade.setLlGrade(grade.llGrade());
		currentGrade.setCCategorie(grade.cCategorie());
		currentGrade.setToCategorieRelationship(grade.toCategorie());
		currentGrade.setDOuverture(grade.dOuverture());
		currentGrade.setDFermeture(grade.dFermeture());
		
		myView.getTfTitre().setText(currentCorps.llCorps());

		updateData();
		myView.setVisible(true);
		return currentGrade;
	}

	public boolean modifier(EOGrade grade) {
		setCurrentGrade(grade);
		modeModification = true;
		myView.setVisible(true);
		return currentGrade != null;
	}

	private void updateData() {

		clearTextFields();
		CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentGrade().cGrade());
		CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentGrade().llGrade());
		CocktailUtilities.setTextToField(myView.getTfLibelleCourt(), getCurrentGrade().lcGrade());
		CocktailUtilities.setDateToField(myView.getTfDateOuverture(), getCurrentGrade().dOuverture());
		CocktailUtilities.setDateToField(myView.getTfDateFermeture(), getCurrentGrade().dFermeture());

		if (getCurrentGrade().toCategorie() != null)
			myView.getPopupCategories().setSelectedItem(getCurrentGrade().cCategorie());
		
	}

	private void valider()  {

		try {


			getCurrentGrade().setDOuverture(CocktailUtilities.getDateFromField(myView.getTfDateOuverture()));
			getCurrentGrade().setDFermeture(CocktailUtilities.getDateFromField(myView.getTfDateFermeture()));
			getCurrentGrade().setCGrade(CocktailUtilities.getTextFromField(myView.getTfCode()));
			getCurrentGrade().setLlGrade(CocktailUtilities.getTextFromField(myView.getTfLibelle()));
			getCurrentGrade().setLcGrade(CocktailUtilities.getTextFromField(myView.getTfLibelleCourt()));

			if (myView.getPopupCategories().getSelectedIndex() > 0)
				currentGrade.setCCategorie(myView.getPopupCategories().getSelectedItem().toString());
			else
				currentGrade.setCCategorie(null);

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		if(!modeModification)
			ec.deleteObject(currentGrade);
		currentGrade = null;
		myView.setVisible(false);
	}
	
	
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	

}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureHierarchieGradeView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOHierarchieGrade;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclatureHierarchieGradeCtrl extends ModelePageConsNomenclatures{

	private static NomenclatureHierarchieGradeCtrl sharedInstance;
	private EODisplayGroup eod;

	private MyListener listener = new MyListener();
	private NomenclatureHierarchieGradeView myView;
	private EOHierarchieGrade currentGrade;

	public NomenclatureHierarchieGradeCtrl(EOEditingContext edc)	{

		super(edc, ((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());

		eod = new EODisplayGroup();

		myView = new NomenclatureHierarchieGradeView(eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		
		myView.getMyEOTable().addListener(listener);
		myView.getTfFiltre().getDocument().addDocumentListener(new ADocumentListener());
	}

	public static NomenclatureHierarchieGradeCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureHierarchieGradeCtrl(editingContext);
		return sharedInstance;
	}

	public EOHierarchieGrade currentGrade() {
		return currentGrade;
	}

	public void setCurrentGrade(EOHierarchieGrade currentGrade) {
		this.currentGrade = currentGrade;
	}

	public JPanel getView() {
		return myView;
	}	

	public void actualiser() {
		updateDatas();
	}


	private void filter() {

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();
		updateInterface();
	}
	
	private class MyListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener	{
		public void onDbClick() {
			if (peutGererModule()) {
				modifier();
			}
		}
		public void onSelectionChanged() {
			setCurrentGrade((EOHierarchieGrade)eod.selectedObject());
			updateInterface();
		}
	}
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	
	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnAjouter().setEnabled(true && peutGererModule());
		myView.getBtnModifier().setEnabled(currentGrade() != null && peutGererModule());
		myView.getBtnSupprimer().setEnabled(currentGrade() != null && peutGererModule());

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EOHierarchieGrade.fetchAll(getEdc(), EOHierarchieGrade.SORT_ARRAY_LIBELLE_ASC));
		filter();
		CRICursor.setDefaultCursor(myView);
	}

	@Override
	protected EOQualifier filterQualifier() {
		// TODO Auto-generated method stub
		NSMutableArray	qualifiers = new NSMutableArray(),orQualifiers = new NSMutableArray();

		String filtreLibelleCorps  = myView.getTfFiltre().getText();
		if (filtreLibelleCorps.length() > 0) {
			orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHierarchieGrade.TO_GRADE_INITIAL_KEY+"."+EOGrade.C_GRADE_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHierarchieGrade.TO_GRADE_INITIAL_KEY+"."+EOGrade.LC_GRADE_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOHierarchieGrade.TO_GRADE_INITIAL_KEY+"."+EOGrade.LL_GRADE_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(qualifiers);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		SaisieHierarchieGradeCtrl.sharedInstance(getEdc()).ajouter();

	}

	@Override
	protected void traitementsApresCreation() {
		// TODO Auto-generated method stub
		actualiser();
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		SaisieHierarchieGradeCtrl.sharedInstance(getEdc()).modifier(currentGrade());
		myView.getMyEOTable().updateUI();
	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(currentGrade());
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub

	}

}

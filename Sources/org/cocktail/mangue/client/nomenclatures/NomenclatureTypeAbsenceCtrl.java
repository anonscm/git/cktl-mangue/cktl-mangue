// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureTypeAbsenceView;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclatureTypeAbsenceCtrl {

	private static NomenclatureTypeAbsenceCtrl sharedInstance;
	private EOEditingContext ec;
	private EODisplayGroup eod;

	private ListenerTypeAbsence myListener = new ListenerTypeAbsence();
	private NomenclatureTypeAbsenceView myView;

	private EOAgentPersonnel currentUtilisateur;
	private EOTypeAbsence currentTypeAbsence;

	public NomenclatureTypeAbsenceCtrl(EOEditingContext editingContext)	{

		ec = editingContext;

		eod = new EODisplayGroup();

		myView = new NomenclatureTypeAbsenceView(eod);

		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getPopupEtats().setSelectedIndex(1);
		myView.getPopupEtats().addActionListener(new PopupEtatListener());
		myView.getPopupConge().addActionListener(new PopupEtatListener());

		myView.getBtnImprimer().setEnabled(false);
		myView.getBtnExporter().setEnabled(false);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		myView.getBtnModifier().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getMyEOTable().addListener(myListener);
	}

	public static NomenclatureTypeAbsenceCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureTypeAbsenceCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}	


	public EOTypeAbsence currentTypeAbsence() {
		return currentTypeAbsence;
	}
	public void setCurrentTypeAbsence(EOTypeAbsence currentTypeAbsence) {
		this.currentTypeAbsence = currentTypeAbsence;
	}
	public EOAgentPersonnel currentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		myView.getBtnModifier().setVisible(currentUtilisateur.peutGererNomenclatures());
	}


	private EOQualifier qualifierRecherche() {

		NSMutableArray	qualifiers = new NSMutableArray(),orQualifiers = new NSMutableArray();

		if (myView.getPopupConge().getSelectedIndex() == 1) {			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeAbsence.CONGE_LEGAL_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		}
		if (myView.getPopupConge().getSelectedIndex() == 2) {			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeAbsence.CONGE_LEGAL_KEY + "=%@", new NSArray(CocktailConstantes.FAUX)));
		}

		return new EOAndQualifier(qualifiers);
	}
	
	private void filter() {
		eod.setQualifier(qualifierRecherche());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();
		updateUI();
	}

	public void actualiser() {
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EOTypeAbsence.fetchAll(ec, EOTypeAbsence.SORT_ARRAY_LIBELLE));
		filter();
		CRICursor.setDefaultCursor(myView);
	}
	
	private void modifier() {
		if (SaisieTypeAbsenceCtrl.sharedInstance(ec).modifier(currentTypeAbsence(), false)) {
			myListener.onSelectionChanged();
			myView.getMyEOTable().updateUI();
		}
	}	

	public void imprimer() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}
	public void exporter() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}


	private void updateUI(){
		myView.getBtnModifier().setEnabled(currentTypeAbsence() != null);
	}

	private class ListenerTypeAbsence implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentUtilisateur().peutGererNomenclatures())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentTypeAbsence((EOTypeAbsence)eod.selectedObject());
			updateUI();
		}
	}
	private class PopupEtatListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			filter();
		}
	}
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

}

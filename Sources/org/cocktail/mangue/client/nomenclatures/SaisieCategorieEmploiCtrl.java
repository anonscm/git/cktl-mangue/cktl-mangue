// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieCategorieEmploiView;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieCategorieEmploiCtrl extends ModelePageSaisieNomenclature
{
	private static final long 		serialVersionUID = 0x7be9cfa4L;
	private static SaisieCategorieEmploiCtrl 	sharedInstance;
	private SaisieCategorieEmploiView 			myView;

	private EOCategorieEmploi 			currentCategorie;

	public SaisieCategorieEmploiCtrl(EOEditingContext edc) {
		
		super(edc);
		
		myView = new SaisieCategorieEmploiView(new JFrame(), true);

        setActionBoutonValiderListener(myView.getBtnValider());
        setActionBoutonAnnulerListener(myView.getBtnAnnuler());

	}

	public static SaisieCategorieEmploiCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieCategorieEmploiCtrl(editingContext);
		return sharedInstance;
	}

	public EOCategorieEmploi getCurrentCategorie() {
		return currentCategorie;
	}

	public void setCurrentCategorie(EOCategorieEmploi currentCategorie) {
		this.currentCategorie = currentCategorie;
		updateDatas();
	}

	/**
	 * 
	 * @param uai
	 * @return
	 */
	public boolean modifier(EOCategorieEmploi categorie, boolean modeCreation) {
		setModeCreation(modeCreation);
		setCurrentCategorie(categorie);
		myView.setVisible(true);
		return getCurrentCategorie() != null;
	}


	/**
	 * 
	 * @throws ValidationException
	 */
	protected void traitementsAvantValidation() {

			getCurrentCategorie().setCCategorieEmploi(CocktailUtilities.getTextFromField(myView.getTfCode()));
			getCurrentCategorie().setLcCategorieEmploi(CocktailUtilities.getTextFromField(myView.getTfLibelleCourt()));
			getCurrentCategorie().setLlCategorieEmploi(CocktailUtilities.getTextFromField(myView.getTfLibelleLong()));
		
	}

	@Override
	protected void clearDatas() {

		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfLibelleCourt());
		CocktailUtilities.viderTextField(myView.getTfLibelleLong());
		
	}

	@Override
	protected void updateDatas() {
		
		clearDatas();

		if (getCurrentCategorie() != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentCategorie().cCategorieEmploi());
			CocktailUtilities.setTextToField(myView.getTfLibelleCourt(), getCurrentCategorie().lcCategorieEmploi());
			CocktailUtilities.setTextToField(myView.getTfLibelleLong(), getCurrentCategorie().llCategorieEmploi());
		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		CocktailUtilities.initTextField(myView.getTfCode(), false, isModeCreation());
		CocktailUtilities.initTextField(myView.getTfLibelleCourt(), false, isModeCreation());

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		setCurrentCategorie(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}	

}

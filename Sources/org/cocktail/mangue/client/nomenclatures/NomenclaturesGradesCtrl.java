// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclaturesGradesView;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOLienGradeMenTg;
import org.cocktail.mangue.modele.grhum.EOPassageChevron;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclaturesGradesCtrl {

	private static NomenclaturesGradesCtrl sharedInstance;
	private EOEditingContext ec;
	private EODisplayGroup eodCorps, eodGrade, eodEchelon, eodChevron;

	private ListenerCorps 		listenerCorps = new ListenerCorps();
	private ListenerGrade 		listenerGrade = new ListenerGrade();
	private ListenerEchelon 	listenerEchelon = new ListenerEchelon();
	private ListenerChevron 	listenerChevron = new ListenerChevron();
	private EchelonsRenderer	monRendererEchelon = new EchelonsRenderer();

	private NomenclaturesGradesView myView;

	private EOCorps currentCorps;
	private EOGrade currentGrade;
	private EOPassageEchelon currentEchelon;
	private EOPassageChevron currentChevron;
	private EOAgentPersonnel currentUtilisateur;

	public NomenclaturesGradesCtrl(EOEditingContext editingContext)	{

		ec = editingContext;

		eodCorps = new EODisplayGroup();
		eodGrade = new EODisplayGroup();
		eodEchelon = new EODisplayGroup();
		eodChevron = new EODisplayGroup();
		
		myView = new NomenclaturesGradesView(eodCorps, eodGrade, eodEchelon, eodChevron, monRendererEchelon);


		myView.getBtnAjouterCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterCorps();}}
		);
		myView.getBtnModifierCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierCorps();}}
		);
		myView.getBtnAjouterGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterGrade();}}
		);
		myView.getBtnModifierGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierGrade();}}
		);
		myView.getBtnDupliquerGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {dupliquerGrade();}}
		);
		myView.getBtnAjouterEchelon().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterEchelon();}}
		);
		myView.getBtnModifierEchelon().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierEchelon();}}
		);
		myView.getBtnDupliquerEchelon().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {dupliquerEchelon();}}
		);
		myView.getBtnSupprimerEchelon().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerEchelon();}}
		);
		myView.getBtnAjouterChevron().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterChevron();}}
		);
		myView.getBtnModifierChevron().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierChevron();}}
		);
		myView.getBtnSupprimerChevron().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerChevron();}}
		);

		myView.getBtnGetDateCorps().setVisible(false);
		
		myView.getMyEOTableCorps().addListener(listenerCorps);
		myView.getMyEOTableGrade().addListener(listenerGrade);
		myView.getMyEOTableEchelon().addListener(listenerEchelon);
		myView.getMyEOTableChevron().addListener(listenerChevron);

		// Types de population
		CocktailUtilities.initPopupAvecListe(myView.getPopupMinisteres(), EOMinisteres.findMinisteresValides(ec), true);
		CocktailUtilities.initPopupAvecListe(myView.getPopupTypesPopulation(), NomenclatureFinder.findStatic(editingContext, EOTypePopulation.ENTITY_NAME), true);

		myView.getPopupMinisteres().addActionListener(new PopupFiltreListener());
		myView.getPopupTypesPopulation().addActionListener(new PopupFiltreListener());

		myView.getPopupEtats().setSelectedIndex(1);
		myView.getPopupEtats().addActionListener(new PopupFiltreListener());

		myView.getTfFiltreLibelleCorps().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreDateCorps().setText(DateCtrl.dateToString(new NSTimestamp()));
		CocktailUtilities.initTextField(myView.getTfGradeTg(), false, false);
		myView.getBtnGetGradeTg().setEnabled(false);

		myView.getBtnImprimer().setEnabled(false);
		myView.getBtnExporter().setEnabled(false);
		
		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		myView.getBtnAAjouterCorps().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifierCorps().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimerCorps().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnAjouterGrade().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifierGrade().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimerGrade().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnDupliquerGrade().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnAjouterEchelon().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifierEchelon().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimerEchelon().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnDupliquerEchelon().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnAjouterChevron().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifierChevron().setVisible(currentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimerChevron().setVisible(currentUtilisateur().peutGererNomenclatures());

	}

	public static NomenclaturesGradesCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclaturesGradesCtrl(editingContext);
		return sharedInstance;
	}
	
	public EOAgentPersonnel currentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}
	
	public EOCorps currentCorps() {
		return currentCorps;
	}

	public void setCurrentCorps(EOCorps currentCorps) {
		this.currentCorps = currentCorps;
	}

	public EOGrade currentGrade() {
		return currentGrade;
	}

	public void setCurrentGrade(EOGrade currentGrade) {
		this.currentGrade = currentGrade;
	}

	public EOPassageEchelon currentEchelon() {
		return currentEchelon;
	}

	public void setCurrentEchelon(EOPassageEchelon currentEchelon) {
		this.currentEchelon = currentEchelon;
	}

	public EOPassageChevron currentChevron() {
		return currentChevron;
	}

	public void setCurrentChevron(EOPassageChevron currentChevron) {
		this.currentChevron = currentChevron;
	}

	public JPanel getView() {
		return myView;
	}	


	
	private EOQualifier filterQualifier() {

		NSMutableArray	qualifiers = new NSMutableArray(),orQualifiers = new NSMutableArray();

		if (myView.getPopupMinisteres().getSelectedIndex() > 0)		
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.TO_MINISTERE_KEY + " =%@", new NSArray(myView.getPopupMinisteres().getSelectedItem())));

		if (myView.getPopupTypesPopulation().getSelectedIndex() > 0)		
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.TO_TYPE_POPULATION_KEY + " =%@", new NSArray((EOTypePopulation)myView.getPopupTypesPopulation().getSelectedItem())));

		String filtreLibelleCorps  = myView.getTfFiltreLibelleCorps().getText();
		if (filtreLibelleCorps.length() > 0) {
			orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.C_CORPS_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.LC_CORPS_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.LL_CORPS_KEY + " caseInsensitiveLike %@", new NSArray("*"+filtreLibelleCorps+"*")));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		if (myView.getPopupEtats().getSelectedIndex() == 1) {

			NSTimestamp dateReference = new NSTimestamp();

			if (myView.getTfFiltreDateCorps().getText().length() > 0)
				dateReference = DateCtrl.stringToDate(myView.getTfFiltreDateCorps().getText());

			orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.D_OUVERTURE_CORPS_KEY + " = nil", null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.D_OUVERTURE_CORPS_KEY + " <=%@", new NSArray(dateReference)));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));

			orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.D_FERMETURE_CORPS_KEY + " = nil", null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.D_FERMETURE_CORPS_KEY + " >= %@", new NSArray(dateReference)));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));

		}

		if (myView.getPopupEtats().getSelectedIndex() == 2) {			
			NSTimestamp dateReference = new NSTimestamp();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.D_FERMETURE_CORPS_KEY + " != nil", null));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.D_FERMETURE_CORPS_KEY + " <= %@", new NSArray(dateReference)));			
		}

		return new EOAndQualifier(qualifiers);
	}
	private void filter() {

		eodCorps.setQualifier(filterQualifier());
		eodCorps.updateDisplayedObjects();

		myView.getMyEOTableCorps().updateData();
		updateUI();
	}

	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		if (eodCorps.allObjects().count() == 0)
			eodCorps.setObjectArray(EOCorps.fetchAll(ec));

		filter();

		updateUI();
		CRICursor.setDefaultCursor(myView);
	}

	
	private void ajouterCorps() 	{
		if(SaisieCorpsCtrl.sharedInstance(ec).ajouter((EOTypePopulation)myView.getPopupTypesPopulation().getSelectedItem()) != null)
			listenerCorps.onSelectionChanged();
	}
	private void modifierCorps() {
		if (SaisieCorpsCtrl.sharedInstance(ec).modifier(currentCorps)) {
			listenerCorps.onSelectionChanged();
			myView.getMyEOTableCorps().updateUI();
		}
	}	
	private void supprimerCorps() {
		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer le corps sélectionné ?", "Oui", "Non"))		
			return;			
		try {
			ec.deleteObject(currentCorps);
			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	
	private void ajouterEchelon() 	{
		if(SaisiePassageEchelonCtrl.sharedInstance(ec).ajouter(currentGrade()) != null)
			listenerGrade.onSelectionChanged();
	}
	private void modifierEchelon() {
		if (SaisiePassageEchelonCtrl.sharedInstance(ec).modifier(currentEchelon))
			listenerGrade.onSelectionChanged();			
	}	
	private void dupliquerEchelon() {
		if (SaisiePassageEchelonCtrl.sharedInstance(ec).dupliquer(currentEchelon) != null)
		listenerGrade.onSelectionChanged();						
	}
	private void supprimerEchelon() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer l'échelon sélectionné ?", "Oui", "Non"))		
			return;			

		try {
			ec.deleteObject(currentEchelon);
			ec.saveChanges();
			listenerGrade.onSelectionChanged();
			NomenclaturesCtrl.sharedInstance(ec).toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private void ajouterChevron() 	{
		if(SaisiePassageChevronCtrl.sharedInstance(ec).ajouter(currentEchelon()) != null)
			listenerEchelon.onSelectionChanged();
	}
	private void modifierChevron() {
		if (SaisiePassageChevronCtrl.sharedInstance(ec).modifier(currentChevron(), currentEchelon()))
			listenerGrade.onSelectionChanged();			
	}	
	private void supprimerChevron() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer le chevron sélectionné ?", "Oui", "Non"))		
			return;			

		try {
			ec.deleteObject(currentChevron);
			ec.saveChanges();
			listenerEchelon.onSelectionChanged();
			NomenclaturesCtrl.sharedInstance(ec).toFront();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private void ajouterGrade() 	{
		if(SaisieGradeCtrl.sharedInstance(ec).ajouter(currentCorps) != null)
			listenerCorps.onSelectionChanged();
	}
	private void modifierGrade() {
		
		if (SaisieGradeCtrl.sharedInstance(ec).modifier(currentGrade))
			listenerCorps.onSelectionChanged();			
	}	
	private void dupliquerGrade() {
		if (SaisieGradeCtrl.sharedInstance(ec).dupliquer(currentGrade) != null)
			listenerCorps.onSelectionChanged();						
	}
	private void supprimerGrade() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer le grade sélectionné ?", "Oui", "Non"))		
			return;			

		try {
			ec.deleteObject(currentGrade);
			ec.saveChanges();
			listenerCorps.onSelectionChanged();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void imprimer() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}
	public void exporter() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void updateUI() {
	
		myView.getBtnAjouterCorps().setEnabled(false);//myView.getPopupTypesPopulation().getSelectedIndex() > 0);
		myView.getBtnModifierCorps().setEnabled(currentCorps != null && currentCorps.isLocal());
		myView.getBtnSupprimerCorps().setEnabled(false);//currentCorps != null && currentCorps.isLocal());

		myView.getBtnAjouterGrade().setEnabled(currentCorps != null && currentCorps.isLocal());
		myView.getBtnModifierGrade().setEnabled(currentGrade != null && currentGrade.isLocal());
		myView.getBtnDupliquerGrade().setEnabled(currentGrade != null && currentGrade.isLocal());
		myView.getBtnSupprimerGrade().setEnabled(false);//currentGrade != null && currentGrade.isLocal());

		myView.getBtnAjouterEchelon().setEnabled(currentGrade != null);
		myView.getBtnModifierEchelon().setEnabled(currentEchelon != null && currentEchelon.isLocal());
		myView.getBtnDupliquerEchelon().setEnabled(currentEchelon != null && currentEchelon.isLocal());
		myView.getBtnSupprimerEchelon().setEnabled(currentEchelon != null && currentEchelon.isLocal());

		myView.getBtnAjouterChevron().setEnabled(currentGrade != null && currentEchelon != null);
		myView.getBtnModifierChevron().setEnabled(currentChevron != null);
		myView.getBtnSupprimerChevron().setEnabled(currentChevron != null);

	}

	private boolean peutGererCorps() {
		return currentCorps() != null && currentCorps.isLocal() && currentUtilisateur().peutGererNomenclatures();
	}
	
	private class ListenerCorps implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (peutGererCorps())
				modifierCorps();
		}

		public void onSelectionChanged() {

			currentCorps = (EOCorps)eodCorps.selectedObject();
			eodGrade.setObjectArray(new NSArray());
			eodEchelon.setObjectArray(new NSArray());

			if (currentCorps != null) {
				eodGrade.setObjectArray(EOGrade.findForCorps(ec, currentCorps));
			}

			myView.getMyEOTableGrade().updateData();
			myView.getMyEOTableEchelon().updateData();
			updateUI();

		}
	}
	private class ListenerGrade implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
			
			if (currentGrade != null && currentGrade.isLocal() && currentUtilisateur().peutGererNomenclatures())
				modifierGrade();
			
		}

		public void onSelectionChanged() {

			currentGrade = (EOGrade)eodGrade.selectedObject();
			eodEchelon.setObjectArray(new NSArray());

			if (currentGrade != null)
				eodEchelon.setObjectArray(EOPassageEchelon.rechercherPourGrade(ec, currentGrade));

			myView.getMyEOTableEchelon().updateData();

			myView.getTfGradeTg().setText("");
			if (currentGrade != null) {
				EOLienGradeMenTg gradeTg = EOLienGradeMenTg.getGradeTgForGrade(ec, currentGrade);
				if (gradeTg != null)
					myView.getTfGradeTg().setText(gradeTg.cGradeTg() + " - " + gradeTg.libelle());
			}

			updateUI();
		}
	}
	private class ListenerEchelon implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
			if (currentEchelon().isLocal() && currentUtilisateur().peutGererNomenclatures())
				modifierEchelon();
		}

		public void onSelectionChanged() {

			currentEchelon = (EOPassageEchelon)eodEchelon.selectedObject();
			eodChevron.setObjectArray(new NSArray());

			if (currentEchelon != null) {
				eodChevron.setObjectArray(EOPassageChevron.rechercherPourGradeEtEchelon(ec, currentGrade(), currentEchelon().cEchelon()));
			}

			myView.getMyEOTableChevron().updateData();

			updateUI();
		}
	}
	
	
	private class ListenerChevron implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentChevron().isLocal() && currentUtilisateur().peutGererNomenclatures())
				modifierChevron();
		}

		public void onSelectionChanged() {
			setCurrentChevron((EOPassageChevron)eodChevron.selectedObject());
			updateUI();
		}
	}

	
	
	private class EchelonsRenderer extends org.cocktail.application.client.swing.ZEOTableCellRenderer	{

		private static final long serialVersionUID = -2907930349355563787L;

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOPassageEchelon obj = (EOPassageEchelon) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.dFermeture() == null)
				leComposant.setForeground(new Color(0,0,0));
			else
				leComposant.setForeground(new Color(110, 110, 110));

			return leComposant;
		}
	}

	private class PopupFiltreListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			filter();
		}

	}
	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

}

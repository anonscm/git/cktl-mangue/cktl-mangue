// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureProfessionView;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOProfession;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSMutableArray;

public class NomenclatureProfessionCtrl {

	private static NomenclatureProfessionCtrl sharedInstance;
	private EOEditingContext ec;
	private EODisplayGroup eod;

	private ListenerProfession myListener = new ListenerProfession();
	private NomenclatureProfessionView myView;

	private EOAgentPersonnel	currentUtilisateur;
	private EOProfession 		currentProfession;

	public NomenclatureProfessionCtrl(EOEditingContext editingContext)	{

		ec = editingContext;

		eod = new EODisplayGroup();

		myView = new NomenclatureProfessionView(eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnImprimer().setEnabled(false);
		myView.getBtnExporter().setEnabled(false);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		myView.getBtnAjouter().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifier().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimer().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getMyEOTable().addListener(myListener);
		
		updateUI();
	}

	public static NomenclatureProfessionCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureProfessionCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}	

	public EOProfession getCurrentProfession() {
		return currentProfession;
	}

	public void setCurrentProfession(EOProfession currentProfession) {
		this.currentProfession = currentProfession;
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		myView.getBtnModifier().setVisible(currentUtilisateur.peutGererNomenclatures());
	}
	
	/**
	 * 
	 * @return
	 */
	private EOQualifier qualifierRecherche() {
		NSMutableArray	qualifiers = new NSMutableArray();
		return new EOAndQualifier(qualifiers);
	}
	
	/**
	 * 
	 */
	private void filter() {
		eod.setQualifier(qualifierRecherche());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();
		updateUI();
	}
	
	/**
	 * 
	 */
	public void actualiser() {
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(NomenclatureFinder.findStatic(ec, EOProfession.ENTITY_NAME));
		filter();
		CRICursor.setDefaultCursor(myView);
	}
	
	/**
	 * 
	 */
	private void ajouter() {
		SaisieProfessionCtrl.sharedInstance(ec).modifier(EOProfession.creer(ec), true);
		actualiser();
	}
	
	/**
	 * 
	 */
	private void modifier() {
		if (SaisieProfessionCtrl.sharedInstance(ec).modifier(getCurrentProfession(), false)) {
			myListener.onSelectionChanged();
			myView.getMyEOTable().updateUI();
		}
	}	
	
	/**
	 * 
	 */
	private void supprimer() {
		try {
			ec.deleteObject(getCurrentProfession());
			ec.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void imprimer() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}
	public void exporter() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}
	
	/**
	 * 
	 */
	private void updateUI(){
		myView.getBtnAjouter().setEnabled(true);
		myView.getBtnModifier().setEnabled(getCurrentProfession() != null);
		myView.getBtnSupprimer().setEnabled(false);
	}
	
	private class ListenerProfession implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererNomenclatures())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentProfession((EOProfession)eod.selectedObject());
			updateUI();
		}
	}
}
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieUaiView;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieUaiCtrl extends ModelePageSaisieNomenclature {
	
	private static final long 		serialVersionUID = 0x7be9cfa4L;
	private static SaisieUaiCtrl 	sharedInstance;
	private SaisieUaiView 			myView;
	private EORne 			currentNomenclature;

	public SaisieUaiCtrl(EOEditingContext edc) {
		
		super(edc);
		
		myView = new SaisieUaiView(new JFrame(), true);

        setActionBoutonValiderListener(myView.getBtnValider());
        setActionBoutonAnnulerListener(myView.getBtnAnnuler());

        setDateListeners(myView.getTfDateOuverture());
        setDateListeners(myView.getTfDateFermeture());

	}

	public static SaisieUaiCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieUaiCtrl(editingContext);
		return sharedInstance;
	}

	public EORne getCurrentNomenclature() {
		return currentNomenclature;
	}

	public void setCurrentNomenclature(EORne currentNomenclature) {
		this.currentNomenclature = currentNomenclature;
		updateDatas();
	}

	/**
	 * 
	 * @param profession
	 * @return
	 */
	public boolean modifier(EORne uai, boolean modeCreation ) {
		setCurrentNomenclature(uai);
		CocktailUtilities.initTextField(myView.getTfCode(), false, modeCreation);
		setModeCreation(modeCreation);
		myView.setVisible(true);
		return getCurrentNomenclature() != null;
	}

	/**
	 * 
	 * @return
	 */
	private boolean traitementsAvantValidationEnabled() {
		return CocktailUtilities.getTextFromField(myView.getTfCode()) != null
				&& CocktailUtilities.getDateFromField(myView.getTfDateOuverture()) != null
				&& CocktailUtilities.getBigDecimalFromField(myView.getTfLibelle()) != null;
	}

	/**
	 * 
	 * @throws ValidationException
	 */
	protected void traitementsAvantValidation() {

		getCurrentNomenclature().setCode(CocktailUtilities.getTextFromField(myView.getTfCode()));
		getCurrentNomenclature().setLibelleLong(CocktailUtilities.getTextFromField(myView.getTfLibelle()));
		getCurrentNomenclature().setDateOuverture(CocktailUtilities.getDateFromField(myView.getTfDateOuverture()));
		getCurrentNomenclature().setDateFermeture(CocktailUtilities.getDateFromField(myView.getTfDateFermeture()));
		getCurrentNomenclature().setAdresse(CocktailUtilities.getTextFromField(myView.getTfAdresse()));
		getCurrentNomenclature().setVille(CocktailUtilities.getTextFromField(myView.getTfVille()));
		getCurrentNomenclature().setCodePostal(CocktailUtilities.getTextFromField(myView.getTfCodePostal()));
		
		if (myView.getPopupType().getSelectedIndex() == 0)
			getCurrentNomenclature().setEtabStatut(ManGUEConstantes.STATUT_RNE_PUBLIC);
		else
			getCurrentNomenclature().setEtabStatut(ManGUEConstantes.STATUT_RNE_PRIVE);

	}

	@Override
	protected void clearDatas() {

		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfLibelle());
		CocktailUtilities.viderTextField(myView.getTfDateOuverture());
		CocktailUtilities.viderTextField(myView.getTfDateFermeture());

		CocktailUtilities.viderTextField(myView.getTfAdresse());
		CocktailUtilities.viderTextField(myView.getTfVille());
		CocktailUtilities.viderTextField(myView.getTfCodePostal());

	}

	@Override
	protected void updateDatas() {
		
		clearDatas();

		if (getCurrentNomenclature() != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentNomenclature().code());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentNomenclature().libelleLong());
			CocktailUtilities.setDateToField(myView.getTfDateOuverture(), getCurrentNomenclature().dateOuverture());
			CocktailUtilities.setDateToField(myView.getTfDateFermeture(), getCurrentNomenclature().dateFermeture());
			
			if (getCurrentNomenclature().etabStatut().equals(ManGUEConstantes.STATUT_RNE_PUBLIC))
				myView.getPopupType().setSelectedIndex(0);
			else
				myView.getPopupType().setSelectedIndex(1);
		}

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		setCurrentNomenclature(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}	

}

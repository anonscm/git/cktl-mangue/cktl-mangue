// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieTypeContratTravailView;
import org.cocktail.mangue.client.select.TypeContratListeSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class SaisieTypeContratTravailCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieTypeContratTravailCtrl sharedInstance;
	private SaisieTypeContratTravailView myView;
	private EOTypeContratTravail currentTypeContrat;
	private EOTypeContratTravail currentTypeContratPere;

	/**
	 * Constructeur
	 * @param edc : editing context
	 */
	public SaisieTypeContratTravailCtrl(EOEditingContext edc) {
		super(edc);

		myView = new SaisieTypeContratTravailView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setActionBoutonGetCodeSupinfoListener();
		setActionBoutonDelCodeSupinfoListener();
		setActionBoutonGetCodePereListener();

		CocktailUtilities.initPopupAvecListe(myView.getPopupCategories(), NomenclatureFinder.find(getEdc(), EOCategorie.ENTITY_NAME, Nomenclature.SORT_ARRAY_CODE) , true);

		setDateListeners(myView.getTfDateOuverture());
		setDateListeners(myView.getTfDateFermeture());

		CocktailUtilities.initTextField(myView.getTfLibelleContratPere(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleContratSupinfo(), false, false);
		CocktailUtilities.initTextField(myView.getTfDecret(), false, false);
	}

	public static SaisieTypeContratTravailCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieTypeContratTravailCtrl(editingContext);
		return sharedInstance;
	}

	public EOTypeContratTravail getCurrentTypeContrat() {
		return currentTypeContrat;
	}

	public void setCurrentTypeContrat(EOTypeContratTravail currentTypeContrat) {
		this.currentTypeContrat = currentTypeContrat;
		updateDatas();
	}

	public EOTypeContratTravail getCurrentTypeContratPere() {
		return currentTypeContratPere;
	}

	public void setCurrentTypeContratPere(EOTypeContratTravail currentTypeContratPere) {
		this.currentTypeContratPere = currentTypeContratPere;
		CocktailUtilities.viderTextField(myView.getTfLibelleContratPere());
		if (currentTypeContratPere != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleContratPere(), currentTypeContratPere.codeEtLibelle());
		}
	}

	/**
	 * 
	 * @param typeContrat
	 * @return
	 */
	public boolean modifier(EOTypeContratTravail typeContrat, boolean modeCreation) {

		setModeCreation(modeCreation);
		setCurrentTypeContrat(typeContrat);
		myView.setVisible(true);
		return getCurrentTypeContrat() != null;
	}

	/**
	 * Affichage de la liste des types de contrat avec le code supInfo
	 */
	private void selectTypeContratSupinfo() {
		EOTypeContratTravail contratSupinfo = (EOTypeContratTravail) TypeContratListeSelectCtrl.sharedInstance(getEdc()).getObjectSupinfo(DateCtrl.today());
		if (contratSupinfo != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleContratSupinfo(), contratSupinfo.codeSupinfo());
		}
	}
	
	/**
	 * On vide le champ de saisie pour le code SUPINFO
	 */
	private void viderCodeSupInfo() {
		if (getCurrentTypeContrat().isLocal()) {
			getCurrentTypeContrat().setCodeSupinfo(null);
			CocktailUtilities.viderTextField(myView.getTfLibelleContratSupinfo());
		}
	}
	
	/**
	 * Affichage de la liste des types de contrat de niveau 1
	 */
	private void selectTypeContratPere() {
		setCurrentTypeContratPere((EOTypeContratTravail) TypeContratListeSelectCtrl.sharedInstance(getEdc()).getTypeContratNiveauUn(DateCtrl.today()));
		if (getCurrentTypeContratPere() != null) {
			CocktailUtilities.setTextToField(myView.getTfDecret(), getCurrentTypeContratPere().refReglementaire());	
		}
	}

	@Override
	protected void clearDatas() {
		setCurrentTypeContratPere(null);
		CocktailUtilities.viderTextField(myView.getTfDateOuverture());
		CocktailUtilities.viderTextField(myView.getTfDateFermeture());
		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfLibelle());
		CocktailUtilities.viderTextField(myView.getTfLibelleCourt());
		CocktailUtilities.viderTextField(myView.getTfLibelleContratSupinfo());
		CocktailUtilities.viderTextField(myView.getTfDecret());

		CocktailUtilities.viderTextField(myView.getTfDureeInitiale());
		CocktailUtilities.viderTextField(myView.getTfDureeMaximale());

		myView.getPopupCategories().setSelectedIndex(0);

	}

	@Override
	protected void updateDatas() {

		clearDatas();

		if (getCurrentTypeContrat() != null) {

			setCurrentTypeContratPere(getCurrentTypeContrat().toTypeContratPere());

			CocktailUtilities.setTextToField(myView.getTfLibelleContratSupinfo(), getCurrentTypeContrat().codeSupinfo());
			CocktailUtilities.setTextToField(myView.getTfDecret(), getCurrentTypeContrat().refReglementaire());

			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentTypeContrat().code());
			CocktailUtilities.setTextToField(myView.getTfLibelleCourt(), getCurrentTypeContrat().libelleCourt());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentTypeContrat().libelleLong());
			CocktailUtilities.setNumberToField(myView.getTfPotentielBrut(), getCurrentTypeContrat().potentielBrut());

			CocktailUtilities.setDateToField(myView.getTfDateOuverture(), getCurrentTypeContrat().dateOuverture());
			CocktailUtilities.setDateToField(myView.getTfDateFermeture(), getCurrentTypeContrat().dateFermeture());

			CocktailUtilities.setNumberToField(myView.getTfDureeInitiale(), getCurrentTypeContrat().dureeInitContrat());
			CocktailUtilities.setNumberToField(myView.getTfDureeMaximale(), getCurrentTypeContrat().dureeMaxContrat());
			//FIXME : A garder vu que la saisie de la ref n'est pas possible
			if (getCurrentTypeContrat().refReglementaire() != null) {
				CocktailUtilities.setTextToField(myView.getTfDecret(), getCurrentTypeContrat().refReglementaire());
			} else {
				if (getCurrentTypeContratPere() != null) {
					CocktailUtilities.setTextToField(myView.getTfDecret(), getCurrentTypeContratPere().refReglementaire());								
				}
			}
			 
			if (getCurrentTypeContrat().cCategorie() != null) {
				myView.getPopupCategories().setSelectedItem(EOCategorie.rechercherCategoriePourCode(getEdc(), getCurrentTypeContrat().cCategorie()));
			}

			myView.getCheckAutoriseTitulaires().setSelected(getCurrentTypeContrat().estAutorisePourTitulaire());
			myView.getCheckEquivGrade().setSelected(getCurrentTypeContrat().requiertGrade());
			myView.getCheckVisible().setSelected(getCurrentTypeContrat().estVisible());
			myView.getCheckCdi().setSelected(getCurrentTypeContrat().estCdi());
			myView.getCheckEnseignant().setSelected(getCurrentTypeContrat().estEnseignant());
			myView.getCheckHospitalo().setSelected(getCurrentTypeContrat().estHospitalier());
			myView.getCheckInvite().setSelected(getCurrentTypeContrat().estInvite());
			myView.getCheckServicePublic().setSelected(getCurrentTypeContrat().estServicePublic());
			myView.getCheckTempsPartiel().setSelected(getCurrentTypeContrat().estTempsPartiel());
			myView.getCheckVacataire().setSelected(getCurrentTypeContrat().estVacataire());
			myView.getCheckEtudiant().setSelected(getCurrentTypeContrat().estEtudiant());
			myView.getCheckDoctorant().setSelected(getCurrentTypeContrat().estDoctorant());
			myView.getCheckEnseignement().setSelected(getCurrentTypeContrat().estPourEnseignement());
			myView.getCheckCir().setSelected(getCurrentTypeContrat().estPourCir());
		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

		CocktailUtilities.initTextField(myView.getTfCode(), false, getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal() && isModeCreation());

		CocktailUtilities.initTextField(myView.getTfDateOuverture(), false, getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		CocktailUtilities.initTextField(myView.getTfDateFermeture(), false, getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		CocktailUtilities.initTextField(myView.getTfDureeInitiale(), false, getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		CocktailUtilities.initTextField(myView.getTfDureeMaximale(), false, getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		CocktailUtilities.initTextField(myView.getTfPotentielBrut(), false, getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());

		myView.getBtnGetCodeSupinfo().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getBtnDelCodeSupInfo().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getBtnGetCodePere().setEnabled(getCurrentTypeContrat() != null && !getCurrentTypeContrat().isNiveau1());
		myView.getPopupCategories().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());

		myView.getCheckEquivGrade().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckVisible().setEnabled(getCurrentTypeContrat() != null);
		myView.getCheckCdi().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckEnseignant().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckAutoriseTitulaires().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckHospitalo().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckInvite().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckServicePublic().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckTempsPartiel().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckVacataire().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckDelegation().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckEtudiant().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckDoctorant().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckEnseignement().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());
		myView.getCheckCir().setEnabled(getCurrentTypeContrat() != null && getCurrentTypeContrat().isLocal());

	}

	@Override
	protected void traitementsAvantValidation() {

		getCurrentTypeContrat().setCode(CocktailUtilities.getTextFromField(myView.getTfCode()));
		getCurrentTypeContrat().setCodeSupinfo(CocktailUtilities.getTextFromField(myView.getTfLibelleContratSupinfo()));
		getCurrentTypeContrat().setLibelleCourt(CocktailUtilities.getTextFromField(myView.getTfLibelleCourt()));
		getCurrentTypeContrat().setLibelleLong(CocktailUtilities.getTextFromField(myView.getTfLibelle()));

		getCurrentTypeContrat().setDateOuverture(CocktailUtilities.getDateFromField(myView.getTfDateOuverture()));
		getCurrentTypeContrat().setDateFermeture(CocktailUtilities.getDateFromField(myView.getTfDateFermeture()));

		getCurrentTypeContrat().setToTypeContratPereRelationship(getCurrentTypeContratPere());

		getCurrentTypeContrat().setPotentielBrut(CocktailUtilities.getDoubleFromField(myView.getTfPotentielBrut()));
		getCurrentTypeContrat().setDureeInitContrat(CocktailUtilities.getIntegerFromField(myView.getTfDureeInitiale()));
		getCurrentTypeContrat().setDureeMaxContrat(CocktailUtilities.getIntegerFromField(myView.getTfDureeMaximale()));
		getCurrentTypeContrat().setRefReglementaire(CocktailUtilities.getTextFromField(myView.getTfDecret()));
		
		// Gestion des notions de visibilite pour les fils
		if (getCurrentTypeContrat().isNiveau1()) {

			if (!myView.getCheckVisible().isSelected() && getCurrentTypeContrat().estVisible()) {
				if (EODialogs.runConfirmOperationDialog(CocktailConstantes.ATTENTION, 
						String.format(MangueMessagesErreur.ATTENTION_TYPE_CONTRAT_ENFANT_NON_VISIBLE, getCurrentTypeContrat().code()), 
						CocktailConstantes.OUI_LONG, CocktailConstantes.NON_LONG)) {
					NSArray<EOTypeContratTravail> typesContratFils = EOTypeContratTravail.findForPere(getEdc(), getCurrentTypeContrat());
					for (EOTypeContratTravail typeContrat : typesContratFils) {
						typeContrat.setEstVisible(false);
					}
				}				
			}

			if (myView.getCheckVisible().isSelected() && !getCurrentTypeContrat().estVisible()) {
				if (EODialogs.runConfirmOperationDialog(CocktailConstantes.ATTENTION, 
						String.format(MangueMessagesErreur.ATTENTION_TYPE_CONTRAT_ENFANT_VISIBLE, getCurrentTypeContrat().code()), 
						CocktailConstantes.OUI_LONG, CocktailConstantes.NON_LONG)) {
					NSArray<EOTypeContratTravail> typesContratFils = EOTypeContratTravail.findForPere(getEdc(), getCurrentTypeContrat());
					for (EOTypeContratTravail typeContrat : typesContratFils) {
						typeContrat.setEstVisible(true);
					}
				}
			}
		}

		getCurrentTypeContrat().setEstVisible(myView.getCheckVisible().isSelected());

		getCurrentTypeContrat().setCCategorie(null);
		if (myView.getPopupCategories().getSelectedIndex() > 0) {
			getCurrentTypeContrat().setCCategorie(((EOCategorie)myView.getPopupCategories().getSelectedItem()).code());
		}

		getCurrentTypeContrat().setTemCdi((myView.getCheckCdi().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemEnseignant((myView.getCheckEnseignant().isSelected())?"O":"N");			
		getCurrentTypeContrat().setTemEquivGrade((myView.getCheckEquivGrade().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemHospitalier((myView.getCheckHospitalo().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemInviteAssocie((myView.getCheckInvite().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemPartiel((myView.getCheckTempsPartiel().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemDelegation((myView.getCheckDelegation().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemServicePublic((myView.getCheckServicePublic().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemPourTitulaire((myView.getCheckAutoriseTitulaires().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemVacataire((myView.getCheckVacataire().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemEtudiant((myView.getCheckEtudiant().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemDoctorant((myView.getCheckDoctorant().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemEnseignement((myView.getCheckEnseignement().isSelected())?"O":"N");
		getCurrentTypeContrat().setTemCir((myView.getCheckCir().isSelected())?"O":"N");

	}

	@Override
	protected void traitementsApresValidation() {
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		setCurrentTypeContrat(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {
	}

	@Override
	protected void traitementsPourCreation() {
	}
	
	/**
	 * Action d'écoute sur le bouton de sélection du type contrat avec code supinfo
	 */
	public void setActionBoutonGetCodeSupinfoListener() {
		myView.getBtnGetCodeSupinfo().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { selectTypeContratSupinfo(); } }
				);
	}
	
	/**
	 * Action d'écoute sur le bouton de suppression de la sélection du type contrat avec code supinfo
	 */
	public void setActionBoutonDelCodeSupinfoListener() {
		myView.getBtnDelCodeSupInfo().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { viderCodeSupInfo(); } }
				);
	}

	/**
	 * Action d'écoute sur le bouton de sélection du type de contrat père
	 */
	public void setActionBoutonGetCodePereListener() {
		myView.getBtnGetCodePere().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { selectTypeContratPere(); } }
				);
	}

}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureCorpsPromouvablesView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOCorpsPromouvable;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclatureCorpsPromouvablesCtrl {

	private static NomenclatureCorpsPromouvablesCtrl sharedInstance;
	private EOEditingContext edc;
	private EODisplayGroup eodCorpsEchelon, eodCorpsChevron;

	private ListenerCorpsEchelon listenerCorpsEchelon = new ListenerCorpsEchelon();
	private ListenerCorpsChevron listenerCorpsChevron = new ListenerCorpsChevron();

	private NomenclatureCorpsPromouvablesView myView;

	private EOCorpsPromouvable 	currentCorpsEchelon;
	private EOCorpsPromouvable 	currentCorpsChevron;
	private EOAgentPersonnel	currentUtilisateur;

	public NomenclatureCorpsPromouvablesCtrl(EOEditingContext editingContext)	{

		setEdc(editingContext);

		eodCorpsEchelon = new EODisplayGroup();
		eodCorpsChevron = new EODisplayGroup();
	
		myView = new NomenclatureCorpsPromouvablesView(eodCorpsEchelon, eodCorpsChevron);
		myView.getBtnAjouterCorpsEchelon().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterCorpsEchelon();}}
		);
		myView.getBtnModifierCorpsEchelon().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierCorpsEchelon();}}
		);
		myView.getBtnSupprimerCorpsEchelon().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerCorpsEchelon();}}
		);

		myView.getBtnAjouterCorpsChevron().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterCorpsChevron();}}
		);
		myView.getBtnModifierCorpsChevron().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierCorpsChevron();}}
		);
		myView.getBtnSupprimerCorpsChevron().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerCorpsChevron();}}
		);

		myView.getMyEOTableCorpsEchelon().addListener(listenerCorpsEchelon);
		myView.getMyEOTableCorpsChevron().addListener(listenerCorpsChevron);

		eodCorpsEchelon.setSortOrderings(new NSArray(new EOSortOrdering(EOCorpsPromouvable.CORPS_KEY+"."+EOCorps.LL_CORPS_KEY, EOSortOrdering.CompareAscending)));
		eodCorpsChevron.setSortOrderings(new NSArray(new EOSortOrdering(EOCorpsPromouvable.CORPS_KEY+"."+EOCorps.LL_CORPS_KEY, EOSortOrdering.CompareAscending)));

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		myView.getBtnAjouterCorpsEchelon().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifierCorpsEchelon().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimerCorpsEchelon().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnAjouterCorpsChevron().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifierCorpsChevron().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimerCorpsChevron().setVisible(getCurrentUtilisateur().peutGererNomenclatures());

	}

	public static NomenclatureCorpsPromouvablesCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureCorpsPromouvablesCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}	

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}

	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		eodCorpsEchelon.setObjectArray(EOCorpsPromouvable.rechercherCorpsPromouvablesPourPeriodeEtType(getEdc(), new NSTimestamp(), null, EOCorpsPromouvable.PROMOTION_ECHELON));
		eodCorpsChevron.setObjectArray(EOCorpsPromouvable.rechercherCorpsPromouvablesPourPeriodeEtType(getEdc(), new NSTimestamp(), null, EOCorpsPromouvable.PROMOTION_CHEVRON));

		myView.getMyEOTableCorpsChevron().updateData();
		myView.getMyEOTableCorpsEchelon().updateData();

		updateInterface();
		
		CRICursor.setDefaultCursor(myView);
	}

	
	private void ajouterCorpsEchelon() 	{
		if(SaisieCorpsPromouvableCtrl.sharedInstance(getEdc()).ajouter(EOCorpsPromouvable.PROMOTION_ECHELON) != null)
			actualiser();
	}
	private void modifierCorpsEchelon() {
		if(SaisieCorpsPromouvableCtrl.sharedInstance(getEdc()).modifier(currentCorpsEchelon))
			actualiser();
	}	
	
	/**
	 * 
	 */
	private void supprimerCorpsEchelon() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer ce corps de la liste des corps promouvables ?", "Oui", "Non"))		
			return;			

		try {
			getEdc().deleteObject(currentCorpsEchelon);
			getEdc().saveChanges();
			actualiser();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void ajouterCorpsChevron() 	{
		if( SaisieCorpsPromouvableCtrl.sharedInstance(getEdc()).ajouter(EOCorpsPromouvable.PROMOTION_CHEVRON) != null)
			actualiser();
	}
	
	/**
	 * 
	 */
	private void modifierCorpsChevron() {
		if(SaisieCorpsPromouvableCtrl.sharedInstance(getEdc()).modifier(currentCorpsChevron))
			actualiser();
	}	
	
	/**
	 * 
	 */
	private void supprimerCorpsChevron() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer ce corps de la liste des corps promouvables ?", "Oui", "Non"))		
			return;			

		try {
			getEdc().deleteObject(currentCorpsChevron);
			getEdc().saveChanges();
			actualiser();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private void updateInterface(){
	
		myView.getBtnModifierCorpsEchelon().setEnabled(currentCorpsEchelon != null );
		myView.getBtnSupprimerCorpsEchelon().setEnabled(currentCorpsEchelon != null );

		myView.getBtnModifierCorpsChevron().setEnabled(currentCorpsChevron != null );
		myView.getBtnSupprimerCorpsChevron().setEnabled(currentCorpsChevron != null );

	}

	private class ListenerCorpsEchelon implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererNomenclatures()) {
				modifierCorpsEchelon();
			}
		}

		public void onSelectionChanged() {

			currentCorpsEchelon = (EOCorpsPromouvable)eodCorpsEchelon.selectedObject();
			updateInterface();

		}
	}
	private class ListenerCorpsChevron implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener
	{
		public void onDbClick() {
			modifierCorpsChevron();
		}

		public void onSelectionChanged() {

			currentCorpsChevron = (EOCorpsPromouvable)eodCorpsChevron.selectedObject();
			updateInterface();

		}
	}

}

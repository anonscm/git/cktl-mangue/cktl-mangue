// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureTypeDechargeView;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeDechargeService;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclatureTypeDechargeCtrl {

	private static NomenclatureTypeDechargeCtrl sharedInstance;
	private EOEditingContext ec;
	private EODisplayGroup eod;

	private ListenerTypeDecharge myListener = new ListenerTypeDecharge();
	private NomenclatureTypeDechargeView myView;

	private EOAgentPersonnel currentUtilisateur;
	private EOTypeDechargeService currentTypeDecharge;

	public NomenclatureTypeDechargeCtrl(EOEditingContext editingContext)	{

		ec = editingContext;

		eod = new EODisplayGroup();

		myView = new NomenclatureTypeDechargeView(eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getPopupEtats().setSelectedIndex(1);
		myView.getPopupEtats().addActionListener(new PopupEtatListener());
		myView.getBtnImprimer().setEnabled(false);
		myView.getBtnExporter().setEnabled(false);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		myView.getBtnAjouter().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnModifier().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getBtnSupprimer().setVisible(getCurrentUtilisateur().peutGererNomenclatures());
		myView.getMyEOTable().addListener(myListener);
	}

	public static NomenclatureTypeDechargeCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureTypeDechargeCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}	



	public EOTypeDechargeService currentTypeDecharge() {
		return currentTypeDecharge;
	}
	public void setCurrentTypeDecharge(EOTypeDechargeService currentTypeDecharge) {
		this.currentTypeDecharge = currentTypeDecharge;
	}
	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		myView.getBtnModifier().setVisible(currentUtilisateur.peutGererNomenclatures());
	}
	private EOQualifier qualifierRecherche() {
		NSMutableArray	qualifiers = new NSMutableArray();
		
		if (myView.getPopupEtats().getSelectedIndex() > 0) {
			if (myView.getPopupEtats().getSelectedIndex() == 1)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeDechargeService.TEM_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
			else
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeDechargeService.TEM_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.FAUX)));
		}
		
		return new EOAndQualifier(qualifiers);
	}
	private void filter() {
		eod.setQualifier(qualifierRecherche());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();
		updateUI();
	}
	public void actualiser() {
		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EOTypeDechargeService.fetchAll(ec, EOTypeDechargeService.SORT_ARRAY_LIBELLE));
		filter();
		CRICursor.setDefaultCursor(myView);
	}
	private void ajouter() {
		EOTypeDechargeService newDecharge = EOTypeDechargeService.creer(ec);
		SaisieTypeDechargeCtrl.sharedInstance(ec).modifier(newDecharge, true);
		actualiser();
	}	
	private void modifier() {
		if (SaisieTypeDechargeCtrl.sharedInstance(ec).modifier(currentTypeDecharge(), false)) {
			myListener.onSelectionChanged();
			myView.getMyEOTable().updateUI();
		}
	}	
	private void supprimer() {
		try {
			currentTypeDecharge().setEstValide(false);
			ec.saveChanges();
			filter();			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void imprimer() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}
	public void exporter() {
		CRICursor.setWaitCursor(myView);
		CRICursor.setDefaultCursor(myView);
	}
	private void updateUI(){
		myView.getBtnModifier().setEnabled(currentTypeDecharge() != null);
	}
	private class ListenerTypeDecharge implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererNomenclatures())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentTypeDecharge((EOTypeDechargeService)eod.selectedObject());
			updateUI();
		}
	}
	private class PopupEtatListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			filter();
		}
	}
}
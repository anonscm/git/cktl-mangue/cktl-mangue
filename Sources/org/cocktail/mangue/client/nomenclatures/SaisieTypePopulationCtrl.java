// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieTypePopulationView;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;

public class SaisieTypePopulationCtrl extends ModelePageSaisieNomenclature
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieTypePopulationCtrl sharedInstance;
	private SaisieTypePopulationView myView;

	private EOTypePopulation currentNomenclature;

	public SaisieTypePopulationCtrl(EOEditingContext edc)
	{
		super(edc);
		myView = new SaisieTypePopulationView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateOuverture());
		setDateListeners(myView.getTfDateFermeture());

		CocktailUtilities.initTextField(myView.getTfCode(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelle(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleCourt(), false, false);
		CocktailUtilities.initTextField(myView.getTfDateOuverture(), false, false);
		CocktailUtilities.initTextField(myView.getTfDateFermeture(), false, false);

	}

	public static SaisieTypePopulationCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieTypePopulationCtrl(editingContext);
		return sharedInstance;
	}

	public EOTypePopulation getCurrentNomenclature() {
		return currentNomenclature;
	}

	public void setCurrentNomenclature(EOTypePopulation currentNomenclature) {
		this.currentNomenclature = currentNomenclature;
		updateDatas();
	}

	public boolean modifier(EOTypePopulation typePopulation, boolean modeCreation) {

		setCurrentNomenclature(typePopulation);
		myView.getTfTitre().setText(getCurrentNomenclature().libelleLong());
		setModeCreation(modeCreation);
		myView.setVisible(true);
		return getCurrentNomenclature() != null;
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfLibelle());
		CocktailUtilities.viderTextField(myView.getTfLibelleCourt());
		CocktailUtilities.viderTextField(myView.getTfDateOuverture());
		CocktailUtilities.viderTextField(myView.getTfDateFermeture());
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub

		clearDatas();

		if (getCurrentNomenclature() != null) {
			myView.getCheckVisible().setSelected(currentNomenclature.estVisible());

			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentNomenclature().code());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentNomenclature().libelleLong());
			CocktailUtilities.setTextToField(myView.getTfLibelleCourt(), getCurrentNomenclature().libelleCourt());

			CocktailUtilities.setDateToField(myView.getTfDateOuverture(), getCurrentNomenclature().dateOuverture());
			CocktailUtilities.setDateToField(myView.getTfDateFermeture(), getCurrentNomenclature().dateFermeture());
		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentNomenclature().setDateOuverture(CocktailUtilities.getDateFromField(myView.getTfDateOuverture()));
		getCurrentNomenclature().setDateFermeture(CocktailUtilities.getDateFromField(myView.getTfDateFermeture()));
		getCurrentNomenclature().setEstVisible(myView.getCheckVisible().isSelected());

		if (getCurrentNomenclature().dateOuverture() == null) {
			getCurrentNomenclature().setDateOuverture(DateCtrl.debutAnnee(new Integer(1900)));
		}
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentNomenclature(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}
}
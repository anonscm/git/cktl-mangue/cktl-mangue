package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Classe d'un modèle de page de saisie
 * 
 * @author Cyril PINSARD
 * @author Chama LAATIK
 *
 */
public abstract class ModelePageSaisieNomenclature {
	
	private EOEditingContext edc;
	private boolean modeCreation;

	protected abstract void clearDatas();
	protected abstract void updateDatas();
	protected abstract void updateInterface();

	protected abstract void traitementsAvantValidation();
	protected abstract void traitementsApresValidation();
	protected abstract void traitementsPourAnnulation();
	protected abstract void traitementsPourCreation();
	
	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	public ModelePageSaisieNomenclature(EOEditingContext edc) {
		this.edc = edc;
	}

	/**
	 * Action de valider
	 */
	protected void valider()	{
		try {
			traitementsAvantValidation();

			edc.saveChanges();

			traitementsApresValidation();
			
			edc.saveChanges();

		} catch (ValidationException ex)	{
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		} catch (Exception e)	{
			e.printStackTrace();
			return;
		}
	}

	/**
	 * Action d'annuler
	 */
	protected void annuler()	{
		try {
			edc.revert();
			traitementsPourAnnulation();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Action d'écoute sur le bouton valider
	 * @param myButton : bouton
	 */
	public void setActionBoutonValiderListener(JButton myButton) {
		myButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { valider(); } });
	}
	
	/**
	 * Action d'écoute sur le bouton annuler
	 * @param myButton : bouton
	 */
	public void setActionBoutonAnnulerListener(JButton myButton) {
		myButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { annuler(); } });
	}

	/**
	 * Met à jour la date
	 * @param myTextField : date saisie
	 */
	public void setDateListeners(JTextField myTextField) {
		setFocusDateListener(myTextField);
		setActionDateListener(myTextField);
	}
	
	/**
	 * Met à jour la date 
	 * @param myTextField : date saisie
	 */
	public void setActionDateListener(JTextField myTextField) {
		myTextField.addActionListener(new ActionListenerDateTextField(myTextField));
	}
	
	/**
	 * Met à jour la date + controle sur la validité de la date saisie
	 * @param myTextField : date saisie
	 */
	public void setFocusDateListener(JTextField myTextField) {
		myTextField.addFocusListener(new FocusListenerDateTextField(myTextField));
	}
			
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText())) {
			return;
		}

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog(CocktailConstantes.DATE_NON_VALIDE_TITRE, CocktailConstantes.DATE_NON_VALIDE_MESSAGE);
		} else {
			myTextField.setText(myDate);
			updateInterface();
		}
	}

	/** 
	 * Classe d'écoute sur les dates
	 * => Permet de faire de la complétion de dates
	 */
	private final class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}	
		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	
	/** 
	 * Classe d'écoute sur les dates
	 * => Permet de faire de la complétion de dates
	 */
	private final class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		
		public void focusGained(FocusEvent e) { }
		
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

	public EOEditingContext getEdc() {
		return edc;
	}
	
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public boolean isModeCreation() {
		return modeCreation;
	}
	
	public void setModeCreation(boolean modeCreation) {
		this.modeCreation = modeCreation;
	}

}
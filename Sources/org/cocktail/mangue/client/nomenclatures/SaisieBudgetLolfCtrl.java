// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.nomenclatures.SaisieProfessionView;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOProfession;
import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieBudgetLolfCtrl extends ModelePageSaisieNomenclature {
	
	private static final long serialVersionUID = 0x7be9cfa4L;
	
	private static SaisieBudgetLolfCtrl sharedInstance;
	private SaisieProfessionView 		myView;
	private EOProfession 				currentProfession;

	public SaisieBudgetLolfCtrl(EOEditingContext edc) {

		super(edc);
		myView = new SaisieProfessionView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setDateListeners(myView.getTfDateOuverture());
		setDateListeners(myView.getTfDateFermeture());

	}

	public static SaisieBudgetLolfCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieBudgetLolfCtrl(editingContext);
		return sharedInstance;
	}

	public EOProfession getCurrentProfession() {
		return currentProfession;
	}

	public void setCurrentProfession(EOProfession currentProfession) {
		this.currentProfession = currentProfession;
		updateDatas();
	}

	/**
	 * 
	 * @param profession
	 * @return
	 */
	public boolean modifier(EOProfession profession, boolean modeCreation) {
		setCurrentProfession(profession);
		CocktailUtilities.initTextField(myView.getTfCode(), false, modeCreation);
		setModeCreation(modeCreation);
		myView.setVisible(true);
		return getCurrentProfession() != null;
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfCode());
		CocktailUtilities.viderTextField(myView.getTfLibelle());
		CocktailUtilities.viderTextField(myView.getTfDetails());
		CocktailUtilities.viderTextField(myView.getTfDateOuverture());
		CocktailUtilities.viderTextField(myView.getTfDateFermeture());
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentProfession() != null) {
			CocktailUtilities.setTextToField(myView.getTfCode(), getCurrentProfession().code());
			CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentProfession().libelleLong());
			CocktailUtilities.setTextToField(myView.getTfDetails(), getCurrentProfession().proDetails());
			CocktailUtilities.setDateToField(myView.getTfDateOuverture(), getCurrentProfession().dateOuverture());
			CocktailUtilities.setDateToField(myView.getTfDateFermeture(), getCurrentProfession().dateFermeture());
		}
	
		updateInterface();
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentProfession(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		if (isModeCreation()) {
			if (NomenclatureFinder.isNomenclatureExistante(getEdc(), EOProfession.ENTITY_NAME, CocktailUtilities.getTextFromField(myView.getTfCode()))) {
				throw new ValidationException("Ce code est déjà utilisé !");
			}
		}
		
		getCurrentProfession().setCode(CocktailUtilities.getTextFromField(myView.getTfCode()));
		getCurrentProfession().setLibelleLong(CocktailUtilities.getTextFromField(myView.getTfLibelle()));
		getCurrentProfession().setProDetails(CocktailUtilities.getTextFromField(myView.getTfDetails()));
		getCurrentProfession().setDateOuverture(CocktailUtilities.getDateFromField(myView.getTfDateOuverture()));
		getCurrentProfession().setDateFermeture(CocktailUtilities.getDateFromField(myView.getTfDateFermeture()));

	}	
}
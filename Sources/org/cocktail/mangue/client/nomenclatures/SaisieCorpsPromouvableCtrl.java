// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.nomenclatures.SaisieCorpsPromouvableView;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.mangue.EOCorpsPromouvable;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisieCorpsPromouvableCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieCorpsPromouvableCtrl sharedInstance;
	private EOEditingContext ec;
	private SaisieCorpsPromouvableView myView;
	private boolean modeModification;

	private EOCorpsPromouvable currentCorpsPromouvable;
	private EOCorps currentCorps;

	public SaisieCorpsPromouvableCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		myView = new SaisieCorpsPromouvableView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				valider();
			}
		}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				annuler();
			}
		}
		);
		myView.getBtnGetCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getCorps();}}
		);

		myView.getTfDateOuverture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateOuverture().addActionListener(new ActionListenerDateTextField(myView.getTfDateOuverture()));
		myView.getTfDateFermeture().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFermeture()));
		myView.getTfDateFermeture().addActionListener(new ActionListenerDateTextField(myView.getTfDateFermeture()));

		CocktailUtilities.initTextField(myView.getTfLibelleCorps(), false, false);
		
	}

	public static SaisieCorpsPromouvableCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieCorpsPromouvableCtrl(editingContext);
		return sharedInstance;
	}

	private void clearTextFields()	{

		myView.getTfLibelleCorps().setText("");
		myView.getTfDateOuverture().setText("");
		myView.getTfDateFermeture().setText("");

	}

	public EOCorpsPromouvable ajouter(String typePromotion)	{

		clearTextFields();

		modeModification = false;

		currentCorpsPromouvable = EOCorpsPromouvable.creer(ec, typePromotion);

		myView.getTfTitre().setText("AJOUT D'UN CORPS PROMOUVABLE ");
		myView.getTfDateOuverture().setText(DateCtrl.dateToString(new NSTimestamp()));

		updateData();
		myView.setVisible(true);
		return currentCorpsPromouvable;
	}

	public boolean modifier(EOCorpsPromouvable corps) {

		clearTextFields();
		currentCorpsPromouvable = corps;
		currentCorps = corps.corps();

		myView.getTfTitre().setText("MODIFICATION D'UN CORPS PROMOUVABLE ");

		updateData();
		modeModification = true;
		myView.setVisible(true);
		return currentCorpsPromouvable != null;
	}

	private void updateData() {

		if(currentCorpsPromouvable.dDebVal() != null)
			myView.getTfDateOuverture().setText(DateCtrl.dateToString(currentCorpsPromouvable.dDebVal()));

		if(currentCorpsPromouvable.dFinVal() != null)
			myView.getTfDateFermeture().setText(DateCtrl.dateToString(currentCorpsPromouvable.dFinVal()));

		if(currentCorpsPromouvable.corps() != null)
			myView.getTfLibelleCorps().setText(currentCorpsPromouvable.corps().llCorps());
		
	}

	private void valider()  {

		try {

			currentCorpsPromouvable.setDModification(new NSTimestamp());
			
			if (myView.getTfDateOuverture().getText().length() > 0)
				currentCorpsPromouvable.setDDebVal(DateCtrl.stringToDate(myView.getTfDateOuverture().getText()));

			if (myView.getTfDateFermeture().getText().length() > 0)
				currentCorpsPromouvable.setDFinVal(DateCtrl.stringToDate(myView.getTfDateFermeture().getText()));
			
			currentCorpsPromouvable.setCorpsRelationship(currentCorps);

			ec.saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public void getCorps() {

		CRICursor.setWaitCursor(myView);
		EOCorps corps = CorpsSelectCtrl.sharedInstance(ec).getCorps();

		if (corps != null)	{
			currentCorps = corps;
			myView.getTfLibelleCorps().setText(currentCorps.llCorps());
		}
		CRICursor.setDefaultCursor(myView);
	}

	private void annuler() {
		if(!modeModification) {
			ec.deleteObject(currentCorpsPromouvable);
			ServerProxy.clientSideRequestRevert(ec);
		}
		currentCorpsPromouvable = null;
		myView.setVisible(false);
	}
	
	
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}

}

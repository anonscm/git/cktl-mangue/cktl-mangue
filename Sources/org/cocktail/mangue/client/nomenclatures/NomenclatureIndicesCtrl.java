// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.nomenclatures;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.nomenclatures.NomenclatureIndicesView;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.client.cir:
//            CirSaisieFicheIdentiteCtrl

public class NomenclatureIndicesCtrl {

	private static NomenclatureIndicesCtrl sharedInstance;
	private EOEditingContext edc;
	private EODisplayGroup eod;

	private ListenerIndices listenerIndices = new ListenerIndices();

	private NomenclatureIndicesView myView;

	private EOIndice currentIndice;

	public NomenclatureIndicesCtrl(EOEditingContext editingContext)	{

		edc = editingContext;

		eod = new EODisplayGroup();
		
		myView = new NomenclatureIndicesView(eod);


		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnDupliquer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {dupliquer();}}
		);

		myView.getMyEOTable().addListener(listenerIndices);

		myView.getTfFiltreMajore().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfIFiltreBrut().getDocument().addDocumentListener(new ADocumentListener());

		myView.getPopupEtats().setSelectedIndex(1);
		myView.getPopupEtats().addActionListener(new PopupEtatListener());
		
		eod.setSortOrderings(new NSArray(EOIndice.SORT_KEY_BRUT_ASC));
		
		myView.getBtnImprimer().setEnabled(false);
		myView.getBtnExporter().setEnabled(false);

		EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
		myView.getBtnAjouter().setVisible(agent.peutGererNomenclatures());
		myView.getBtnModifier().setVisible(agent.peutGererNomenclatures());
		myView.getBtnSupprimer().setVisible(agent.peutGererNomenclatures());
		myView.getBtnExporter().setVisible(agent.peutGererNomenclatures());
		myView.getBtnDupliquer().setVisible(agent.peutGererNomenclatures());
		myView.getBtnImprimer().setVisible(agent.peutGererNomenclatures());

	}

	public static NomenclatureIndicesCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new NomenclatureIndicesCtrl(editingContext);
		return sharedInstance;
	}

	public JPanel getView() {
		return myView;
	}	


	private EOQualifier qualifierRecherche() {

		NSMutableArray	qualifiers = new NSMutableArray(),orQualifiers = new NSMutableArray();

		if (myView.getTfIFiltreBrut().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndice.C_INDICE_BRUT_KEY + " = %@", new NSArray(myView.getTfIFiltreBrut().getText())));

		if (myView.getTfFiltreMajore().getText().length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndice.C_INDICE_MAJORE_KEY + " = %@", new NSArray(myView.getTfFiltreMajore().getText())));

		if (myView.getPopupEtats().getSelectedIndex() == 1) {

			NSTimestamp dateReference = new NSTimestamp();

			orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndice.D_MAJORATION_KEY + " = nil", null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndice.D_MAJORATION_KEY + " <=%@", new NSArray(dateReference)));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));

			orQualifiers = new NSMutableArray();			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndice.D_FERMETURE_KEY + " = nil", null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndice.D_FERMETURE_KEY + " >= %@", new NSArray(dateReference)));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));

		}

		if (myView.getPopupEtats().getSelectedIndex() == 2) {			
			NSTimestamp dateReference = new NSTimestamp();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndice.D_FERMETURE_KEY + " != nil", null));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndice.D_FERMETURE_KEY + " <= %@", new NSArray(dateReference)));			
		}

		return new EOAndQualifier(qualifiers);
	}
	private void filter() {

		eod.setQualifier(qualifierRecherche());
		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();
		updateUI();
	}

	public void actualiser() {

		CRICursor.setWaitCursor(myView);

		if (eod.allObjects().count() == 0)
			eod.setObjectArray(EOIndice.fetchAll(edc));

		filter();

		updateUI();
		CRICursor.setDefaultCursor(myView);
	}

	
	private void ajouter() 	{
		if(SaisieIndiceCtrl.sharedInstance(edc).ajouter() != null) {

			eod.setObjectArray(EOIndice.fetchAll(edc));
			filter();
			
		}
	}
	
	private void modifier() {
		if (SaisieIndiceCtrl.sharedInstance(edc).modifier(currentIndice))
			myView.getMyEOTable().updateData();			
	}	
	private void supprimer() {
		try {
			edc.deleteObject(currentIndice);
			edc.saveChanges();
			actualiser();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void dupliquer() {
//		if (SaisieIndiceCtrl.sharedInstance(ec).dupliquer(currentIndice) != null)
//			listenerIndices.onSelectionChanged();						
	}

	public void imprimer() {

		CRICursor.setWaitCursor(myView);

		CRICursor.setDefaultCursor(myView);
	}
	public void exporter() {
		CRICursor.setWaitCursor(myView);

		CRICursor.setDefaultCursor(myView);
	}

	private void updateUI(){

		myView.getBtnAjouter().setEnabled(true);
		myView.getBtnModifier().setEnabled(currentIndice != null && currentIndice.isLocal());
		myView.getBtnSupprimer().setEnabled(currentIndice != null && currentIndice.isLocal());
		myView.getBtnDupliquer().setEnabled(currentIndice != null && currentIndice.isLocal());
		
	}

	private class ListenerIndices implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (currentIndice != null && currentIndice.isLocal())
				modifier();
		}

		public void onSelectionChanged() {

			currentIndice = (EOIndice)eod.selectedObject();
			updateUI();
			
		}
	}

	private class PopupEtatListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			filter();
		}
	}
	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}



}

package org.cocktail.mangue.client;

import org.cocktail.application.client.ServerProxyCocktail;
import org.cocktail.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * Rassemble tous les appels aux methodes definies sur le serveur.
 */

public class ServerProxy extends ServerProxyCocktail {
	
	private static final String CLIENT_SIDE_REQUEST_SET_AGENT = "clientSideRequestSetAgent";
	private static final String CLIENT_SIDE_REQUEST_MSG_TO_SERVER = "clientSideRequestMsgToServer";
	private static final String SESSION_STR = "session";

	public static boolean clientSideRequestMsgToServer(EOEditingContext ec, String msg) {
		try {
			((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_MSG_TO_SERVER, new Class[] {
					String.class
			}, new Object[] {
					msg
			}, false);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if ( StringCtrl.containsIgnoreCase(e.getMessage(), "java.io.IOException")
					|| StringCtrl.containsIgnoreCase(e.getClass().toString(), "java.io.IOException")) {
				return true;
			}
			return false;
		}

	}
	
	public static void clientSideRequestSetAgent(EOEditingContext ec, String login) {
		try {
			((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_SET_AGENT, new Class[] {
					String.class
			}, new Object[] {
					login
			}, false);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	

	public static Boolean clientSideRequestLaunchMethode(EOEditingContext ec, String nomMethode, Class[] classesParametres, Object[] parametres) throws Exception {
		return (Boolean)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, "session", nomMethode,
				classesParametres, 
				parametres, 
				true);
	} 
	
	
	public static boolean clientSideRequestGetBooleanParam (EOEditingContext edc, 
			String paramKey) {
		
		return (Boolean)((EODistributedObjectStore)edc.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				edc, 
				"session", 
				"clientSideRequestGetBooleanParam", new Class[] {String.class}, new Object[] {paramKey}, false);
		
	}
	
	public static String clientSideRequestGetParam (EOEditingContext edc, 
			String paramKey) {
		
		return (String)((EODistributedObjectStore)edc.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				edc, 
				"session", 
				"clientSideRequestGetParam", new Class[] {String.class}, new Object[] {paramKey}, false);
		
		
	}
	
	public static String serverBdConnexionName (EOEditingContext ec)  {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestBdConnexionName", new Class[] {}, new Object[] {}, false);
	}

	public static String clientSideRequestAppVersion(EOEditingContext ec) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", 
				"clientSideRequestAppVersion", 
				null,  
				null, 
				false);
	}

	public static final void clientSideRequestRevert(EOEditingContext edc) {
		((EODistributedObjectStore)edc.parentObjectStore()).invokeRemoteMethodWithKeyPath(edc, "session", "clientSideRequestRevert", null, null, true);
	} 

	public static final void clientSideRequestStartProgCir(EOEditingContext edc, EOGlobalID idFichierCir) {
		((EODistributedObjectStore)edc.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				edc, "session", "clientSideRequestStartProgCir",
				new Class[] {EOGlobalID.class}, 
				new Object[] {idFichierCir}, 
				true);
	} 
	public static final void clientSideRequestStopProgCir(EOEditingContext edc) {
		((EODistributedObjectStore)edc.parentObjectStore()).invokeRemoteMethodWithKeyPath(edc, "session", "clientSideRequestStopProgCir", null, null, true);
	}

	public static String clientSideRequestPreparerPromoEcManuelle(EOEditingContext ec, EOGlobalID fichierGID, NSArray paramsPromotion, EOGlobalID gidIndividu) throws Exception {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, "session", "clientSideRequestPreparerPromoEcManuelle",
				new Class[] {EOGlobalID.class, NSArray.class, EOGlobalID.class}, 
				new Object[] {fichierGID, paramsPromotion, gidIndividu}, 
				true);
	} 

	public static NSArray clientSideRequestPreparerFichierCir(EOEditingContext ec, String rubrique, String data) throws Exception {
		return (NSArray)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, "session", "clientSideRequestGetSousRubriques",
				new Class[] {String.class, String.class}, 
				new Object[] {rubrique, data}, 
				true);
	} 

	public static NSArray clientSideRequestSupprimerDonneesCir(EOEditingContext ec, EOGlobalID gidFichier, NSArray individusGids) throws Exception {
		return (NSArray)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, "session", "clientSideRequestSupprimerRecordsCir",
				new Class[] {EOGlobalID.class,NSArray.class}, 
				new Object[] {gidFichier, individusGids}, 
				true);
	} 

	public static NSArray clientSideRequestGetSousRubriques(EOEditingContext ec, String rubrique, String data) throws Exception {
		return (NSArray)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, "session", "clientSideRequestGetSousRubriques",
				new Class[] {String.class, String.class}, 
				new Object[] {rubrique, data}, 
				true);
	} 

}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.metier.finder.gpeec.EmploiFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe du controleur de la sélection des emplois
 */
public class EmploiSelectCtrl  {


	private static final int INDEX_VACANTS = 0;
	private static final int INDEX_ROMPUS = 1;
	
	private static EmploiSelectCtrl sharedInstance;
	private EOEditingContext edc;

	private EmploiSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();
	private PopupTypeListener listenerType = new PopupTypeListener();

	private IEmploi 	currentRecord;
	private NSTimestamp currentDateDebut, currentDateFin;

	private NSArray<IEmploi> listeEmploisVacants = new NSMutableArray<IEmploi>();
	private NSArray<IEmploi> listeEmploisRompus = new NSMutableArray<IEmploi>();

	/**
	 * Constructeur
	 * @param edc : editingContext
	 */
	public EmploiSelectCtrl(EOEditingContext edc)	{
		super();

		this.edc = edc;

		myView = new EmploiSelectView(new JFrame(), true, eod, new ColQuotiteProvider());

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) { annuler(); } }
		);

		myView.getMyEOTable().addListener(new LocalListener());
		myView.getTfFiltreNumero().getDocument().addDocumentListener(new ADocumentListener());

		myView.getPopupType().addActionListener(listenerType);
	}

	/**
	 * Permet d'acceder a cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiSelectCtrl.sharedInstance(edc).methode
	 * 
	 * @param editingContext : editingContext
	 * @return une instance de la classe
	 */
	public static EmploiSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null) {	
			sharedInstance = new EmploiSelectCtrl(editingContext);
		}
		return sharedInstance;
	}

	public IEmploi getCurrentRecord() {
		return currentRecord;
	}

	public void setCurrentRecord(IEmploi currentRecord) {
		this.currentRecord = currentRecord;
	}

	/**
	 * Calcul de la colonne QUOTITE sur laquelle est place le Provider
	 * @author cpinsard
	 *
	 */
	private class ColQuotiteProvider implements org.cocktail.application.client.swing.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {
		public Object getValueAtRow(int row) {
			IEmploi myEmploi = (IEmploi) eod.displayedObjects().objectAtIndex(row);
			return myEmploi.getQuotiteRestanteAOccuper(currentDateDebut, currentDateFin);
		}
	} 

	private void setDateDebut(NSTimestamp debut) {
		currentDateDebut = debut;
	}
	private void setDateFin(NSTimestamp fin) {
		currentDateFin = fin;
	}

	/**
	 * @param debut : date de début
	 * @param fin : date de fin
	 * @return un emploi
	 */
	public IEmploi getEmploi(NSTimestamp debut, NSTimestamp fin)	{
		setDateDebut(debut);
		setDateFin(fin);
		
		listeEmploisVacants = EmploiFinder.sharedInstance().rechercherListeEmploisVacants(edc, debut, fin);
		listeEmploisRompus = EmploiFinder.sharedInstance().rechercherEmploisRompus(edc, debut, fin);
		
		myView.getPanelTypeEmploi().setVisible(true); //On force l'affichage du panel pour le type d'emploi (vacants, rompus)
		myView.getPopupType().setSelectedIndex(0);

		CocktailUtilities.viderTextField(myView.getTfFiltreNumero());
		myView.setVisible(true);
		return getCurrentRecord();
	}	
	
	/**
	 * Permet la sélection de tous les emplois en cours sur une période
	 * 	=> Pas d'affichage du type (Vacants, Rompus)
	 * @param debut : date de début
	 * @param fin : date de fin
	 * @return l'emploi selectionné
	 */
	public IEmploi getEmploiTous(NSTimestamp debut, NSTimestamp fin)	{
		setDateDebut(debut);
		setDateFin(fin);
		
		eod.setObjectArray(EmploiFinder.sharedInstance().rechercherListeTousEmplois(edc, debut, fin));
		
		myView.getPanelTypeEmploi().setVisible(false); // on n'affiche pas le type d'emploi (vacants, rompus)
		filter();

		CocktailUtilities.viderTextField(myView.getTfFiltreNumero());
		myView.setVisible(true);
		return getCurrentRecord();
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();

		if (!StringCtrl.chaineVide(myView.getTfFiltreNumero().getText()))	{
			NSArray<String> args = new NSArray<String>("*" + myView.getTfFiltreNumero().getText() + "*");
			
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploi.NO_EMPLOI_KEY + " caseInsensitiveLike %@", args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploi.NO_EMPLOI_FORMATTE_KEY + " caseInsensitiveLike %@", args));

			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		
		return new EOAndQualifier(mesQualifiers);  
	}

	/** 
	 * filtrer les emplois
	 */
	private void filter()	{
		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();
		
		myView.getLblNbEmplois().setText(eod.displayedObjects().size() + " Emplois");
	}

	/**
	 * 
	 */
	public void annuler() {
		CocktailUtilities.viderTextField(myView.getTfFiltreNumero());
		setCurrentRecord(null);
		
		myView.dispose();
	}  

	/**
	 * Permet de definir un listener sur le type d'emplois. 
	 * Dès que le contenu du champ change, on met a jour la liste des emplois affichés.
	 * 
	 */	
	private class PopupTypeListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction) {
			
			CRICursor.setWaitCursor(myView);
			switch (myView.getPopupType().getSelectedIndex()) {
				case INDEX_VACANTS : eod.setObjectArray(listeEmploisVacants); break;
				case INDEX_ROMPUS : eod.setObjectArray(listeEmploisRompus); break;
			}			
			
			filter();
			CRICursor.setDefaultCursor(myView);
		}
	}

	/**
	 */
	public class LocalListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			setCurrentRecord((EOEmploi) eod.selectedObject());
		}
	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}

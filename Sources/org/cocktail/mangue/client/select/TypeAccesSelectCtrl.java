/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe du controleur des types d'accès
 */
public class TypeAccesSelectCtrl  {


	private static TypeAccesSelectCtrl sharedInstance;

	private EOEditingContext ec;

	private TypeAccesSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();
	private EOTypeAcces currentObject;

	/**
	 * Constructeur
	 * 
	 * @param editingContext : editingContext
	 */
	public TypeAccesSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new TypeAccesSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new Listener());
		myView.getTfFiltreTypeAcces().getDocument().addDocumentListener(new ADocumentListener());
		
	}

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : TypeAccesSelectCtrl.sharedInstance().methode
	 * 
	 * @param editingContext : editingContext
	 * @return une instance de la classe
	 */
	public static TypeAccesSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TypeAccesSelectCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * @param dateReference : date de référence
	 * @return type d'accès valide pour la date donnée
	 */
	public EOTypeAcces getTypeAccesValideADate(NSTimestamp dateReference)	{
		eod.setObjectArray(EOTypeAcces.findForDate(ec, EOTypeAcces.ENTITY_NAME, dateReference, Nomenclature.SORT_ARRAY_LIBELLE));
		myView.getMyEOTable().updateData();
		myView.setVisible(true);

		return currentObject;
	}

	public EOTypeAcces getTypeAccesArrivee()	{

		if (eod.displayedObjects().count() == 0) {
			eod.setObjectArray(EOTypeAcces.findTypesArrivee(ec));
			filter();
		}

		myView.setVisible(true);

		return currentObject;
	}


	public void annuler() {

		currentObject = null;

		eod.setSelectionIndexes(new NSArray());

		myView.dispose();

	}  

	private EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();
		if (!StringCtrl.chaineVide(myView.getTfFiltreTypeAcces().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreTypeAcces().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_LONG_KEY + " caseInsensitiveLike %@",args));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + " caseInsensitiveLike %@",args));
		}
		return new EOOrQualifier(mesQualifiers);       

	}	
	private void filter()	{
		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();
	}
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
	private class Listener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EOTypeAcces)eod.selectedObject();
		}
	}
}

package org.cocktail.mangue.client.select;

import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.modele.finder.GradeFinder;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOTypeContratGrades;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class GradeSelectCtrl 
{
	private static GradeSelectCtrl sharedInstance;

	private 	EOEditingContext	ec;
	private 	EODisplayGroup		eod;

	private GradeSelectView myView;

	private EOGrade currentObject;
	

	/** 
	 *
	 */
	public GradeSelectCtrl (EOEditingContext editingContext)	{

		super();

		ec = editingContext;
		eod = new EODisplayGroup();

		myView = new GradeSelectView(null, true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		eod.setSortOrderings(new NSArray (new EOSortOrdering(EOGrade.LL_GRADE_KEY, EOSortOrdering.CompareAscending)));
		myView.getMyEOTable().addListener(new Listener());

		myView.getTfFiltreCode().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreLbelleCourt().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreLibelleLong().getDocument().addDocumentListener(new ADocumentListener());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static GradeSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new GradeSelectCtrl(editingContext);
		return sharedInstance;
	}

	public EOGrade getGradePourTypeContrat(EOTypeContratTravail typeContrat)	{

		myView.getTfFiltreCode().setText("");
		myView.getTfFiltreLbelleCourt().setText("");
		myView.getTfFiltreLibelleLong().setText("");
		
		eod.setObjectArray( (NSArray) (EOTypeContratGrades.rechercherPourTypeContrat(ec, typeContrat)).valueForKey(EOTypeContratGrades.TO_GRADE_KEY));
		myView.getMyEOTable().updateData();

		myView.setVisible(true);

		if (currentObject == null)	
			return null;

		return currentObject;
	}


	public EOGrade getGradePourCorps(EOCorps corps, NSTimestamp dateReference)	{

		myView.getTfFiltreCode().setText("");
		myView.getTfFiltreLbelleCourt().setText("");
		myView.getTfFiltreLibelleLong().setText("");
		eod.setObjectArray(EOGrade.rechercherGradesPourCorpsValidesEtPeriode(ec, corps, dateReference, dateReference));		
		myView.getMyEOTable().updateData();

		myView.setVisible(true);

		if (currentObject == null)	
			return null;

		return currentObject;
	}

	/**
	 * 
	 * @param gestionHU
	 * @param dateReference
	 * @return
	 */
	public EOGrade getGrade(boolean gestionHU, NSTimestamp dateReference)	{

		eod.setObjectArray(GradeFinder.sharedInstance().findForDate(ec, dateReference, gestionHU));
		filter();

		myView.setVisible(true);	

		if (currentObject == null)	
			return null;

		return currentObject;
	}

	/**
	 * 
	 * @param qualifier
	 * @return
	 */
	public EOGrade getGradePourQualifier(EOQualifier qualifier)	{

		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		eod.setObjectArray(EOGrade.rechercherGradePourQualifier(ec, qualifier));
		filter();

		myView.setVisible(true);	

		if (currentObject == null)	
			return null;

		return currentObject;
	}

	/**
	 * 
	 */
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		currentObject = null;
		myView.dispose();
	}  	


	/**
	 * 
	 * @return
	 */
	public EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFiltreCode().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreCode().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.C_GRADE_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFiltreLbelleCourt().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreLbelleCourt().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.LC_GRADE_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFiltreLibelleLong().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreLibelleLong().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.LL_GRADE_KEY + " caseInsensitiveLike %@",args));
		}

		return new EOAndQualifier(mesQualifiers);        
	}

	private void filter() {

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class Listener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EOGrade)eod.selectedObject();
		}
	}

}
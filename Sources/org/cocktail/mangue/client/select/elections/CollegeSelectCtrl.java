/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select.elections;

import javax.swing.JFrame;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOTypeInstance;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class CollegeSelectCtrl  {


	private static CollegeSelectCtrl sharedInstance;

	private EOEditingContext edc;

	private CollegeSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();
	private ListenerCollege listenerCollege = new ListenerCollege();
	private EOCollege currentObject;

	public CollegeSelectCtrl(EOEditingContext edc)	{

		super();
		this.edc = edc;
		myView = new CollegeSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}
		});
		myView.getMyEOTable().addListener(listenerCollege);

	}
	public static CollegeSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CollegeSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	public EOCollege currentObject() {
		return currentObject;
	}
	public void setCurrentObject(EOCollege currentObject) {
		this.currentObject = currentObject;
	}
	
	public EOCollege getCollege(EOTypeInstance typeInstance)	{

		myView.setTitle("Sélection d'un collège");		
		eod.setObjectArray(EOCollege.findForTypeInstance(edc,typeInstance));
		myView.getMyEOTable().updateData();
		
		myView.setVisible(true);

		return currentObject;
	}
	
	/**
	 * 
	 * @param typeInstance
	 * @return
	 */
	public NSArray<EOCollege> getColleges(EOTypeInstance typeInstance)	{

		myView.setTitle("Sélection de collèges");		
		eod.setObjectArray(EOCollege.findForTypeInstance(edc,typeInstance));
		myView.getMyEOTable().updateData();
		
		myView.setVisible(true);

		return eod.selectedObjects();
	}

	public void annuler() {
		setCurrentObject( null);
		eod.setSelectionIndexes(new NSArray<EOCollege>());
		myView.dispose();
	}  
	private class ListenerCollege implements ZEOTableListener {
		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			setCurrentObject((EOCollege)eod.selectedObject());
		}
	}	   
}

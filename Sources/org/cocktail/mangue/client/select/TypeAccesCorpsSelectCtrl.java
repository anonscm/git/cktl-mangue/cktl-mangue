/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import javax.swing.JFrame;

import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeAccesCorps;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class TypeAccesCorpsSelectCtrl  {


	private static TypeAccesCorpsSelectCtrl sharedInstance;

	private EOEditingContext ec;

	private TypeAccesCorpsSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();
	private EOTypeAccesCorps currentObject;

	/**
	 * 
	 *
	 */
	public TypeAccesCorpsSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new TypeAccesCorpsSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new Listener());
		eod.setSortOrderings(Nomenclature.SORT_ARRAY_LIBELLE);
		
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TypeAccesCorpsSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TypeAccesCorpsSelectCtrl(editingContext);
		return sharedInstance;
	}


	public EOTypeAccesCorps getTypeAcces()	{

		if (eod.displayedObjects().count() == 0) {
			eod.setObjectArray(EOTypeAccesCorps.fetchAll(ec, null, EOTypeAccesCorps.SORT_ARRAY_LIBELLE));
			myView.getMyEOTable().updateData();
		}

		myView.setVisible(true);

		return currentObject;
	}



	public void annuler() {

		currentObject = null;

		eod.setSelectionIndexes(new NSArray());

		myView.dispose();

	}  

	private class Listener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EOTypeAccesCorps)eod.selectedObject();
		}
	}
}

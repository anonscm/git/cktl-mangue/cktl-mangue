/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.modele.nomenclatures.cir.EONatureBonifTerritoire;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class TerritoireSelectCtrl  {


	private static TerritoireSelectCtrl sharedInstance;
	private EOEditingContext ec;

	private TerritoireSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();

	private EONatureBonifTerritoire currentObject;


	public TerritoireSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new TerritoireSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new LocalListener());

		myView.getTfFindLibelle().addActionListener(new MyActionListener());
		myView.getTfFindLibelle().getDocument().addDocumentListener(new ADocumentListener());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TerritoireSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TerritoireSelectCtrl(editingContext);
		return sharedInstance;
	}


	public EONatureBonifTerritoire getCurrentObject() {
		return currentObject;
	}


	public void setCurrentObject(EONatureBonifTerritoire currentObject) {
		this.currentObject = currentObject;
	}


	private void filter() {

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();

	}

	/**
	 * 
	 * @return
	 */
	public EONatureBonifTerritoire getTerritoire()	{

		eod.setObjectArray(EONatureBonifTerritoire.fetchAll(ec, null, new NSArray(EONatureBonifTerritoire.SORT_LIBELLE_ASC)));
		filter();

		myView.setVisible(true);

		return getCurrentObject();
		
	}	


	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
		if (!StringCtrl.chaineVide(myView.getTfFindLibelle().getText()))	{
			String chaineFiltre = "*" + myView.getTfFindLibelle() + "*";			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EONatureBonifTerritoire.NBOT_LIBELLE_KEY + " caseInsensitiveLike %@", new NSArray(chaineFiltre)));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EONatureBonifTerritoire.NBOT_CODE_KEY + " caseInsensitiveLike %@",new NSArray(chaineFiltre)));
		}

		return new EOOrQualifier(orQualifiers);       

	}

	/**
	 * 
	 */
	private void annuler() {

		setCurrentObject(null);
		eod.setSelectionIndexes(new NSArray());
		myView.dispose();

	}  


	private class LocalListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			setCurrentObject((EONatureBonifTerritoire)eod.selectedObject());
		}
	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			filter();
		}
	}
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}

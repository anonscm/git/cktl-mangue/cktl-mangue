/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.modele.nomenclatures.EODepartement;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class DepartementSelectCtrl  {


	private static DepartementSelectCtrl sharedInstance;
	private EOEditingContext ec;

	private DepartementSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();

	private EODepartement currentObject;


	public DepartementSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new DepartementSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new LocalListener());

		myView.getTfFindLibelle().addActionListener(new MyActionListener());
		myView.getTfFindLibelle().getDocument().addDocumentListener(new ADocumentListener());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static DepartementSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new DepartementSelectCtrl(editingContext);
		return sharedInstance;
	}


	public EODepartement currentObject() {
		return currentObject;
	}
	public void setCurrentObject(EODepartement currentObject) {
		this.currentObject = currentObject;
	}


	private void filter() {

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();

	}



	public EODepartement getDepartement(String code, NSTimestamp dateRef)	{

		if (code != null)	{
			eod.setObjectArray(EODepartement.fetch(ec, code, dateRef));
			if (eod.displayedObjects().count() == 1)
				return (EODepartement)eod.displayedObjects().objectAtIndex(0);
			
			return null;
		}

		eod.setObjectArray(EODepartement.fetch(ec, null, dateRef));
		
		filter();

		myView.setVisible(true);

		return currentObject();

	}	

	private EOQualifier getFilterQualifier()	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		NSMutableArray orQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFindLibelle().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindLibelle().getText()+"*");
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODepartement.CODE_KEY + " caseInsensitiveLike %@",args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EODepartement.LIBELLE_LONG_KEY + " caseInsensitiveLike %@",args));

			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(mesQualifiers);       

	}

	/**
	 * 
	 */
	private void annuler() {

		currentObject = null;
		eod.setSelectionIndexes(new NSArray());

		myView.dispose();

	}  

	private class LocalListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			currentObject = (EODepartement)eod.selectedObject();
		}
	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			filter();

		}
	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}

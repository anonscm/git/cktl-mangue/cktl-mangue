/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.cocktail.mangue.client.nomenclatures.ModelePageSelectNomenclature;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class UAISelectCtrl extends ModelePageSelectNomenclature {

	private static UAISelectCtrl sharedInstance;
	private RneSelectView myView;

	public UAISelectCtrl(EOEditingContext edc)	{

		super(edc);
		myView = new RneSelectView(null, true, getEod());

		myView.getBtnFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		setActionBoutonAnnulerListener(myView.getButtonAnnuler());

		setListener(myView.getMyEOTable());

		myView.getTfFindCode().addActionListener(new MyActionListener());
		myView.getTfFindLibelle().addActionListener(new MyActionListener());

		setDocumentFiltreListener(myView.getTfFindCode());
		setDocumentFiltreListener(myView.getTfFindLibelle());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static UAISelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new UAISelectCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 */
	private void rechercher() {

		if (myView.getTfFindCode().getText().length() == 0 && myView.getTfFindLibelle().getText().length() == 0) {
			EODialogs.runInformationDialog("ERREUR","Veuillez renseigner au moins un critère de recherche !");
			return;
		}

		if (myView.getTfFindCode().getText().length() == 0 && myView.getTfFindLibelle().getText().length() <= 2) {
			EODialogs.runInformationDialog("ERREUR","Veuillez renseigner un libellé d'au moins 2 caractères !");
			return;
		}

		if (myView.getTfFindCode().getText().length() <= 2 && myView.getTfFindLibelle().getText().length() ==0) {
			EODialogs.runInformationDialog("ERREUR","Veuillez renseigner un code d'au moins 2 caractères !");
			return;
		}

		getEod().setObjectArray(EORne.rechercherRne(getEdc(), myView.getTfFindCode().getText(), myView.getTfFindLibelle().getText()));

		myView.getMyEOTable().updateData();


	}

	/**
	 * 
	 * Selection unique d'un PlanComptable
	 * 
	 * @param classes		// Filtre sur les types de compte (6, 4, etc ...)
	 * @return EOplanComptable
	 */
	public INomenclature getObject()	{
		myView.setVisible(true);
		return getCurrentObject();
	}	


	protected EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFindCode().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindCode().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFindLibelle().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindLibelle().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_LONG_KEY + " caseInsensitiveLike %@",args));
		}

		return new EOAndQualifier(mesQualifiers);       

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			rechercher();
		}
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}
	@Override
	protected void traitementsPourAnnulation() {
		myView.setVisible(false);
	}

	@Override
	protected String getFilterValue() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	protected void traitementsApresFiltre() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().updateData();
	}

}

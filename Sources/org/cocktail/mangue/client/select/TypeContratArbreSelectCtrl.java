/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.common.utilities.StringCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Affichage des types de contrat sous forme d'arbre
 *
 */
public class TypeContratArbreSelectCtrl  {

	private static TypeContratArbreSelectCtrl sharedInstance;
	private EOEditingContext ec;
	private TypeContratTravailSelectView myView;
	private EODisplayGroup eodNiveau1  = new EODisplayGroup(), eodNiveau2 = new EODisplayGroup();
	private EOTypeContratTravail currentNiveau2;
	private EOTypeContratTravail currentNiveau3;
	private NSTimestamp dateReference;
	private HashMap<EOTypeContratTravail, NSArray<EOTypeContratTravail>> mapTypesContrat;
	private TypeContratRenderer	monRendererColor = new TypeContratRenderer();

	/**
	 * Constructeur
	 * @param editingContext : editing Context
	 */
	public TypeContratArbreSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new TypeContratTravailSelectView(new JFrame(), true, eodNiveau1, eodNiveau2, monRendererColor);

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectionner();}});
		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}});

		myView.getPopupTitulaire().setSelectedIndex(0);

		myView.getMyEOTable1().addListener(new ListenerNiveau1());
		myView.getMyEOTable2().addListener(new ListenerNiveau2());
		myView.getCheckTous().addActionListener(new FiltreTypeContratListener());
		myView.getCheckNonEns().addActionListener(new FiltreTypeContratListener());
		myView.getCheckEns().addActionListener(new FiltreTypeContratListener());
		myView.getCheckHu().addActionListener(new FiltreTypeContratListener());
		myView.getPopupTitulaire().addActionListener(new FiltreTypeContratListener());

		myView.getTfFiltreLibelleNiveau1().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreLibelleNiveau2().getDocument().addDocumentListener(new ADocumentListener());

		myView.getCheckHu().setEnabled(EOGrhumParametres.isGestionHu());
		CocktailUtilities.viderTextField(myView.getTfFiltreLibelleNiveau1());
		CocktailUtilities.viderTextField(myView.getTfFiltreLibelleNiveau2());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TypeContratArbreSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TypeContratArbreSelectCtrl(editingContext);
		return sharedInstance;
	}

	public EOTypeContratTravail getCurrentNiveau1() {
		return currentNiveau2;
	}


	public void setCurrentNiveau1(EOTypeContratTravail currentNiveau2) {
		this.currentNiveau2 = currentNiveau2;
	}


	public EOTypeContratTravail getCurrentNiveau2() {
		return currentNiveau3;
	}


	public void setCurrentNiveau2(EOTypeContratTravail currentNiveau3) {
		this.currentNiveau3 = currentNiveau3;
	}


	public NSTimestamp getDateReference() {
		return dateReference;
	}


	public void setDateReference(NSTimestamp dateReference) {
		this.dateReference = dateReference;
	}


	/**
	 * 
	 * @return
	 */
	private EOQualifier getQualifier() {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		if (CocktailUtilities.getTextFromField(myView.getTfFiltreLibelleNiveau2()) != null) {
			String filtre = "*"+CocktailUtilities.getTextFromField(myView.getTfFiltreLibelleNiveau2())+"*";
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + " caseInsensitiveLike %@", new NSArray(filtre)));			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_LONG_KEY + " caseInsensitiveLike %@", new NSArray(filtre)));			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_COURT_KEY + " caseInsensitiveLike %@", new NSArray(filtre)));			
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		if (myView.getPopupTitulaire().getSelectedIndex() > 0) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeContratTravail.TEM_POUR_TITULAIRE_KEY + "=%@", new NSArray(myView.getPopupTitulaire().getSelectedItem())));
		}

		if (!myView.getCheckTous().isSelected()) {

			if (myView.getCheckNonEns().isSelected()) {
				qualifiers.addObject(EOTypeContratTravail.getQualifierEnseignant(false));
				qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));
			} else
				if (myView.getCheckEns().isSelected()) {
					qualifiers.addObject(EOTypeContratTravail.getQualifierEnseignant(true));
					qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));
				} else
					if (myView.getCheckHu().isSelected()) {
						qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(true));
					}

		} else {
			qualifiers.addObject(EOTypeContratTravail.qualifierHU());
		}

		return new EOAndQualifier(qualifiers);

	}

	/**
	 * Construction d'une HashMap contenant tous les types de contrat valides en fonction du filtre
	 */
	private void setMapTypesContrat() {

		CRICursor.setWaitCursor(myView);
		mapTypesContrat = new HashMap<EOTypeContratTravail, NSArray<EOTypeContratTravail>>();

		NSArray<EOTypeContratTravail> typesContrat = EOTypeContratTravail.findTypesContratValidesForNiveau(ec, EOTypeContratTravail.NIVEAU_1, getDateReference());

		boolean peutAjouter1 = false;

		for (EOTypeContratTravail typeContratNiveau1 : typesContrat) {

			peutAjouter1 = true;

			if (CocktailUtilities.getTextFromField(myView.getTfFiltreLibelleNiveau1()) != null) {

				String filtre = CocktailUtilities.getTextFromField(myView.getTfFiltreLibelleNiveau1());

				peutAjouter1 = StringCtrl.containsIgnoreCase(typeContratNiveau1.code(), filtre)  || 
						StringCtrl.containsIgnoreCase(typeContratNiveau1.libelleCourt(), filtre) ||
						StringCtrl.containsIgnoreCase(typeContratNiveau1.libelleLong(), filtre);

			}

			if (myView.getPopupTitulaire().getSelectedIndex() == 1) {
				peutAjouter1 = peutAjouter1 && typeContratNiveau1.estAutorisePourTitulaire();
			}
			if (myView.getPopupTitulaire().getSelectedIndex() == 2) {
				peutAjouter1 = peutAjouter1 && !typeContratNiveau1.estAutorisePourTitulaire();
			}
			if (peutAjouter1 && !myView.getCheckTous().isSelected()) {

				if (myView.getCheckNonEns().isSelected()) {
					peutAjouter1 = !typeContratNiveau1.estEnseignant();
				} else
					if (myView.getCheckEns().isSelected()) {
						peutAjouter1 = typeContratNiveau1.estEnseignant();
					} else
						if (myView.getCheckHu().isSelected()) {
							peutAjouter1 = typeContratNiveau1.estHospitalier();
						}
			}

			if (peutAjouter1) {
				NSArray<EOTypeContratTravail> fils =  EOTypeContratTravail.findTypesContratValidesForPere(ec, typeContratNiveau1, new Integer(2), getDateReference());
				NSArray<EOTypeContratTravail> typesContratNiveau2 = EOQualifier.filteredArrayWithQualifier(fils, getQualifier());

				if (typesContratNiveau2.size() > 0) {
					mapTypesContrat.put(typeContratNiveau1, typesContratNiveau2.immutableClone());
				}
				else {
					if (CocktailUtilities.getTextFromField(myView.getTfFiltreLibelleNiveau2()) == null)
						mapTypesContrat.put(typeContratNiveau1, new NSArray<EOTypeContratTravail>());
				}	
			}
		}

		CRICursor.setDefaultCursor(myView);
	}


	/**
	 * 
	 */
	private void filter() {

		setMapTypesContrat();

		eodNiveau1.setObjectArray(new NSArray(mapTypesContrat.keySet()));		
		myView.getMyEOTable1().updateData();

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class FiltreTypeContratListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			filter();
		}
	}

	/**
	 * 
	 * @param dateReference
	 * @param filterQualifier
	 * @return
	 */
	public EOTypeContratTravail getTypeContrat(NSTimestamp dateReference, EOQualifier filterQualifier, boolean estTitulaire)	{

		setDateReference(dateReference);

		if (estTitulaire) {
			myView.getPopupTitulaire().setSelectedItem(CocktailConstantes.VRAI);
		} else {
			myView.getPopupTitulaire().setSelectedIndex(0);
		}

		filter();

		myView.setVisible(true);

		return getCurrentObject();
	}


	/**
	 * 
	 * @return
	 */
	private EOTypeContratTravail getCurrentObject() {

		if (getCurrentNiveau2() != null) {
			return getCurrentNiveau2();
		}
		return getCurrentNiveau1();
	}

	public void selectionner() {
		if (getCurrentNiveau1() == null && getCurrentNiveau2() == null) {
			EODialogs.runErrorDialog("ERREUR", "Veuillez sélectionner un type de contrat de niveau 2 ou 3 !");
			return;
		}
		myView.dispose();
	}

	/**
	 * 
	 */
	public void annuler() {

		setCurrentNiveau1(null);
		setCurrentNiveau2(null);
		eodNiveau1.setSelectionIndexes(new NSArray<EOTypeContratTravail>());
		eodNiveau2.setSelectionIndexes(new NSArray<EOTypeContratTravail>());

		myView.dispose();

	}  

	private class ListenerNiveau1 implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			setCurrentNiveau1((EOTypeContratTravail) eodNiveau1.selectedObject());
			eodNiveau2.setObjectArray(new NSArray<EOTypeContratTravail>());
			if (getCurrentNiveau1() != null) {
				eodNiveau2.setObjectArray(mapTypesContrat.get(getCurrentNiveau1()));
			}
			myView.getMyEOTable2().updateData();
		}
	}

	private class ListenerNiveau2 implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			setCurrentNiveau2((EOTypeContratTravail) eodNiveau2.selectedObject());
		}
	}
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	/**
	 * Classe servant a colorer les cellules selon l'etat de l'engagement
	 */
	private class TypeContratRenderer extends ZEOTableCellRenderer	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2907930349355563787L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOTypeContratTravail obj = (EOTypeContratTravail) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.isLocal())
				leComposant.setBackground(new Color(180,180,120));
			else
				if (!obj.isLocal())
					leComposant.setBackground(new Color(102, 180, 120));

			return leComposant;
		}
	}

}

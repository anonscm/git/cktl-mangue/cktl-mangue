/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JDialog;
import javax.swing.JTextField;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCivilite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuSimple;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSTimestamp;

public class SaisieIndividuCtrl {


	private static SaisieIndividuCtrl sharedInstance;

	private 	EOEditingContext		ec;
	private 	boolean nouvelIndividu;

	ActionListenerIdentite actionListenerIdentite = new ActionListenerIdentite();
	FocusListenerIdentite focusListenerIdentite = new FocusListenerIdentite();

	private EODisplayGroup eod;		
	private SaisieIndividuView myView;
	private	EOIndividuSimple selectedIndividu;
	private boolean saisieDateNaissance;


	/**
	 * 
	 * @param globalEc
	 */
	public SaisieIndividuCtrl (EOEditingContext globalEc)	{

		super();

		ec = globalEc;
		eod = new EODisplayGroup();
		myView = new SaisieIndividuView(new JDialog(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}});
		myView.getBtnSelectionner().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectionner();}});
		myView.getBtnVerifIdentite().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {verifierIdentite();}});
		myView.getBtnNouvelAgent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {creerIndividu();}});

		myView.getMyEOTable().addListener(new ListenerIndividu());
		eod.setSortOrderings(EOIndividuSimple.SORT_NOM_ASC);

		myView.getTfNomUsuel().addActionListener(actionListenerIdentite);
		myView.getTfNomUsuel().addFocusListener(focusListenerIdentite);

		myView.getTfPrenom().addActionListener(actionListenerIdentite);
		myView.getTfPrenom().addFocusListener(focusListenerIdentite);

		myView.setListeCivilites(EOCivilite.find(ec));

		myView.getTfDateNaissance().addFocusListener(new FocusListenerDateTextField(myView.getTfDateNaissance()));
		myView.getTfDateNaissance().addActionListener(new ActionListenerDateTextField(myView.getTfDateNaissance()));

		updateUI();
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieIndividuCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieIndividuCtrl(editingContext);
		return sharedInstance;
	}



	public EOIndividuSimple selectedIndividu() {
		return selectedIndividu;
	}
	public void setSelectedIndividu(EOIndividuSimple selectedIndividu) {
		this.selectedIndividu = selectedIndividu;
	}
	public boolean isNouvelIndividu() {
		return nouvelIndividu;
	}
	public void setNouvelIndividu(boolean nouvelIndividu) {
		this.nouvelIndividu = nouvelIndividu;
	}

	private boolean traitementsPourCreation() {

		if (myView.getPopupCivilite().getSelectedIndex() == 0) {
			EODialogs.runInformationDialog("ERREUR", "Veuillez sélectionner une civilité !");
			return false;
		}

		if (myView.getTfNomUsuel().getText().length() == 0) {
			EODialogs.runInformationDialog("ERREUR", "Veuillez saisie un nom usuel !");
			return false;
		}

		if (myView.getPopupCivilite().getSelectedIndex() == 0) {
			EODialogs.runInformationDialog("ERREUR", "Veuillez sélectionner une civilité !");
			return false;
		}

		if (saisieDateNaissance() && myView.getTfDateNaissance().getText().length() == 0) {
			EODialogs.runInformationDialog("ERREUR", "Veuillez saisir une date de naissance !");
			return false;
		}

		return true;
	}


	public void creerIndividu()	{		

		if (!traitementsPourCreation())
			return;

		setNouvelIndividu(true);
		myView.setVisible(false);

	}

	private void selectionner()	{

		setSelectedIndividu((EOIndividuSimple)eod.selectedObject());
		if (saisieDateNaissance()) {
			if (selectedIndividu().dNaissance() == null
					&& CocktailUtilities.getDateFromField(myView.getTfDateNaissance()) == null ) {
				EODialogs.runErrorDialog("ERREUR", "Merci de saisir une date de naissance pour cet agent !");
				return;
			}
			else {
			}
		}

		setNouvelIndividu(false);
		myView.setVisible(false);

	}

	public void clearTextFields() {

		myView.getPopupCivilite().setSelectedIndex(0);
		myView.getTfNomUsuel().setText("");
		myView.getTfNomFamille().setText("");
		myView.getTfPrenom().setText("");

	}

	public EOIndividuSimple getIndividuSimple()	{		

		clearTextFields();
		setSaisieDateNaissance(false);
		updateUI();
		myView.setVisible(true);

		if (isNouvelIndividu()) {			
			setSelectedIndividu(EOIndividuSimple.creer(ec));

			selectedIndividu().setNoIndividu(EOIndividu.construireNoIndividu(ec));
			selectedIndividu().setPersId(SuperFinder.construirePersId(ec));

			selectedIndividu().setCCivilite( ((EOCivilite)myView.getPopupCivilite().getSelectedItem()).cCivilite());
			selectedIndividu().setNomUsuel(myView.getTfNomUsuel().getText());
			if (StringCtrl.chaineVide(myView.getTfNomFamille().getText()))
				selectedIndividu().setNomPatronymique(myView.getTfNomUsuel().getText());
			else
				selectedIndividu().setNomPatronymique(myView.getTfNomFamille().getText());
			selectedIndividu().setPrenom(myView.getTfPrenom().getText());
		}

		return selectedIndividu();

	}



	public boolean saisieDateNaissance() {
		return saisieDateNaissance;
	}
	public void setSaisieDateNaissance(boolean saisieDateNaissance) {
		this.saisieDateNaissance = saisieDateNaissance;
	}


	public NSTimestamp getDateNaissance() {
		if (myView.getTfDateNaissance().getText().length() > 0)
			return DateCtrl.stringToDate(myView.getTfDateNaissance().getText());

		return null;
	}

	/**
	 * 
	 * @param peutAjouterIndividu
	 * @param dateNaissance
	 * @return
	 */
	public EOIndividu getIndividu(boolean peutAjouterIndividu, boolean dateNaissance)	{		

		clearTextFields();
		
		setSaisieDateNaissance(dateNaissance);

		myView.getBtnNouvelAgent().setVisible(peutAjouterIndividu);
		myView.getTfDateNaissance().setVisible(peutAjouterIndividu);		
		myView.getPopupCivilite().setVisible(peutAjouterIndividu);		
		
		updateUI();

		myView.setVisible(true);
		
		// Validation de l'ecran. 
		
		if (selectedIndividu() != null || isNouvelIndividu()) {

			EOIndividu individu = null;
			if (isNouvelIndividu()) {

				individu = new EOIndividu();
				individu.init();
				individu.setIndQualite("NOUVEAU");
				individu.setNoIndividu(EOIndividu.construireNoIndividu(ec));
				individu.setPersId(SuperFinder.construirePersId(ec));

				individu.setCCivilite( ((EOCivilite)myView.getPopupCivilite().getSelectedItem()).cCivilite());
				individu.setNomUsuel(myView.getTfNomUsuel().getText());
				if (StringCtrl.chaineVide(myView.getTfNomFamille().getText()))
					individu.setNomPatronymique(myView.getTfNomUsuel().getText());
				else
					individu.setNomPatronymique(myView.getTfNomFamille().getText());
				
				individu.setPrenom(myView.getTfPrenom().getText());			

				individu.setDNaissance(CocktailUtilities.getDateFromField(myView.getTfDateNaissance()));

				ec.insertObject(individu);
			}
			else {
				
				individu = EOIndividu.rechercherIndividuPersId(ec,selectedIndividu().persId());

				if (saisieDateNaissance() && isNouvelIndividu())
					individu.setDNaissance(CocktailUtilities.getDateFromField(myView.getTfDateNaissance()));
				
			}

			return individu;
		}

		return null;

	}


	public void annuler() {
		setSelectedIndividu(null);
		myView.dispose();
	}  	

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerIndividu implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {selectionner();}
		public void onSelectionChanged() {
			setSelectedIndividu((EOIndividuSimple)eod.selectedObject());
			updateUI();
		}
	}

	/**
	 * 
	 */
	private void verifierIdentite() {		
		if (org.cocktail.mangue.common.utilities.StringCtrl.chaineVide(myView.getTfNomUsuel().getText()) && 
				org.cocktail.mangue.common.utilities.StringCtrl.chaineVide(myView.getTfPrenom().getText()))	{
			return;
		}
		eod.setObjectArray(EOIndividuSimple.rechercherIndividuPourCreation(ec, myView.getTfNomUsuel().getText(), myView.getTfPrenom().getText()));
		myView.getMyEOTable().updateData();

		updateUI();
	}


	/**
	 * 
	 */
	private void updateUI() {		

		myView.getBtnNouvelAgent().setEnabled(
				StringCtrl.chaineVide(myView.getTfNomUsuel().getText()) == false
				&&  StringCtrl.chaineVide(myView.getTfPrenom().getText()) == false );

		myView.getBtnSelectionner().setEnabled(
				selectedIndividu() != null );

		myView.getTfDateNaissance().setVisible(saisieDateNaissance());
		myView.getLblDateNaissance().setVisible(saisieDateNaissance());
	}

	public class ActionListenerIdentite implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			verifierIdentite();
		}
	}
	public class FocusListenerIdentite implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e) {
		}
		public void focusLost(FocusEvent e)	{
			verifierIdentite();
		}
	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}	
}

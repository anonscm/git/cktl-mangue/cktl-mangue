/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.cocktail.mangue.client.nomenclatures.ModelePageSelectNomenclature;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Affichage des types de contrat sous forme de liste
 *	=> Affichage des types de contrat du niveau 2 et les niveaux 1 sans enfants
 */
public class TypeContratListeSelectCtrl extends ModelePageSelectNomenclature {

	private static TypeContratListeSelectCtrl sharedInstance;
	private TypeContratListeSelectView myView;
	private NSTimestamp dateReference;

	/**
	 * Constructeur
	 * @param edc : editing Context
	 */
	public TypeContratListeSelectCtrl(EOEditingContext edc)	{

		super(edc);
		myView = new TypeContratListeSelectView(null, true, getEod(), EOGrhumParametres.isGestionHu());
		setActionBoutonAnnulerListener(myView.getButtonAnnuler());
		setListener(myView.getMyEOTable());
		setDocumentFiltreListener(myView.getTfFiltre());

		myView.getPopupTitulaire().setSelectedIndex(0);
		
		myView.getPopupLocal().addActionListener(new FiltreTypeContratListener());
		myView.getPopupTitulaire().addActionListener(new FiltreTypeContratListener());
		myView.getPopupType().addActionListener(new FiltreTypeContratListener());
		
		CocktailUtilities.viderTextField(myView.getTfFiltre());
		
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static TypeContratListeSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new TypeContratListeSelectCtrl(editingContext);
		return sharedInstance;
	}


	public NSTimestamp getDateReference() {
		return dateReference;
	}


	public void setDateReference(NSTimestamp dateReference) {
		this.dateReference = dateReference;
	}

	/**
	 * 
	 * @param dateReference
	 * @param filterQualifier
	 * @return
	 */
	public INomenclature getObject(NSTimestamp dateReference, EOQualifier filterQualifier, boolean estTitulaire)	{

		setDateReference(dateReference);	
		getEod().setObjectArray(EOTypeContratTravail.findTypesContratValides(getEdc(), getDateReference()));

		if (estTitulaire)
			myView.getPopupTitulaire().setSelectedItem(CocktailConstantes.VRAI);
		else
			myView.getPopupTitulaire().setSelectedItem(0);

		if (filterQualifier != null) {
			getEod().setQualifier(filterQualifier);
			getEod().updateDisplayedObjects();
			getEod().setObjectArray(getEod().displayedObjects());
		}

		filter();

		myView.setVisible(true);

		return getCurrentObject();
	}

	/**
	 * 
	 * @param liste
	 * @return
	 */
	public INomenclature getObject(NSArray<INomenclature> liste)	{
		getEod().setObjectArray(liste);
		myView.getMyEOTable().updateData();
		myView.setVisible(true);
		return getCurrentObject();
	}	

	/**
	 * 
	 * @param dateReference
	 * @return
	 */
	public INomenclature getObjectSupinfo(NSTimestamp dateReference)	{
		setDateReference(dateReference);
		getEod().setObjectArray(EOTypeContratTravail.findTypesContratSupinfo(getEdc(), getDateReference()));
		myView.getMyEOTable().updateData();
		myView.setVisible(true);
		return getCurrentObject();
	}	
	
	/**
	 * @param dateReference : date de référence
	 * @return type de contrat de niveau 1
	 */
	public INomenclature getTypeContratNiveauUn(NSTimestamp dateReference)	{
		setDateReference(dateReference);
		getEod().setObjectArray(EOTypeContratTravail.findTypesContratValidesForNiveauUn(getEdc(), dateReference));
		myView.getMyEOTable().updateData();
		myView.setVisible(true);
		return getCurrentObject();
	}

	/**
	 * @return qualifier pour filtrer la liste
	 */
	protected EOQualifier getFilterQualifier()	{

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		if (!StringCtrl.chaineVide(myView.getTfFiltre().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltre().getText()+"*");
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_LONG_KEY + " caseInsensitiveLike %@", args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + " caseInsensitiveLike %@", args));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		if (myView.getPopupLocal().getSelectedIndex() > 0) {
			qualifiers.addObject(EOTypeContratTravail.getQualifierLocal((String) myView.getPopupLocal().getSelectedItem()));
		}

		if (myView.getPopupType().getSelectedIndex() > 0) {
			if (myView.getPopupType().getSelectedIndex() == 1) {
				qualifiers.addObject(EOTypeContratTravail.getQualifierEnseignant(false));
				qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));
			} else
				if (myView.getPopupType().getSelectedIndex() == 2) {
					qualifiers.addObject(EOTypeContratTravail.getQualifierEnseignant(true));
					qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));
				} else
					if (myView.getPopupType().getSelectedIndex() == 3) {
						qualifiers.addObject(EOTypeContratTravail.getQualifierCdi(true));
					} else
						if (myView.getPopupType().getSelectedIndex() == 4) {
							qualifiers.addObject(EOTypeContratTravail.getQualifierDoctorant(true));
						} else
							if (myView.getPopupType().getSelectedIndex() == 5) {
								qualifiers.addObject(EOTypeContratTravail.getQualifierEtudiant(true));
							} else
								if (myView.getPopupType().getSelectedIndex() == 6) {
									qualifiers.addObject(EOTypeContratTravail.getQualifierInvite(true));
									qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));
								} else
									if (myView.getPopupType().getSelectedIndex() == 7) {
										qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(true));
									}
		} else {
			qualifiers.addObject(EOTypeContratTravail.qualifierHU());
		}
		
		if (myView.getPopupTitulaire().getSelectedIndex() > 0) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeContratTravail.TEM_POUR_TITULAIRE_KEY + " = %@", new NSArray(myView.getPopupTitulaire().getSelectedItem())));
		}

		return new EOAndQualifier(qualifiers);       

	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class FiltreTypeContratListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction) {
			filter();
		}
	}

	@Override
	protected void traitementsApresValidation() {
		myView.setVisible(false);
	}
	
	@Override
	protected void traitementsPourAnnulation() {
		myView.setVisible(false);
	}

	@Override
	protected String getFilterValue() {
		return CocktailUtilities.getTextFromField(myView.getTfFiltre());
	}

	@Override
	protected void traitementsApresFiltre() {
		myView.getMyEOTable().updateData();
	}

}

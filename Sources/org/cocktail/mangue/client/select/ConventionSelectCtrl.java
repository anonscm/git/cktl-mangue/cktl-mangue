/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.mangue.budget.EOConvention;
import org.cocktail.mangue.modele.mangue.budget.EOOrgan;
import org.cocktail.mangue.modele.mangue.budget.EOTypeCredit;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ConventionSelectCtrl  {

	private static ConventionSelectCtrl sharedInstance;
	private EOEditingContext ec;

	private ConventionSelectView myView;

	private EODisplayGroup eod = new EODisplayGroup();

	private EOConvention currentConvention;

	/**
	 * 
	 *
	 */
	public ConventionSelectCtrl(EOEditingContext editingContext)	{

		super();
		ec = editingContext;

		myView = new ConventionSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerConvention());
		myView.getTfFindIndex().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindObjet().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindReference().getDocument().addDocumentListener(new ADocumentListener());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ConventionSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ConventionSelectCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 * Selection et recuperation d une convention en fonction di, exercoce / organ / type de credit
	 * 
	 * @param exercice
	 * @param organ
	 * @param typeCredit
	 * @return
	 */
	public EOConvention getConvention(Integer exercice, EOOrgan organ, EOTypeCredit typeCredit) 	{

		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		NSMutableArray conventions = new NSMutableArray(EOConvention.findConventions(ec,exercice, organ, typeCredit));

		eod.setObjectArray(conventions);

		if (eod.displayedObjects().count() == 0)	{
			EODialogs.runErrorDialog("ERREUR","Aucune convention n'a pu être trouvée pour cette ligne budgétaire et ce type de crédit !");
			return null;
		}

		filter();

		myView.setVisible(true);

		return currentConvention;

	}


	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFindIndex().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindIndex().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CON_INDEX_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFindObjet().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindObjet().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CON_OBJET_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFindReference().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindReference().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CON_REFERENCE_EXTERNE_KEY + " caseInsensitiveLike %@",args));
		}

		return new EOAndQualifier(mesQualifiers);        
	}


	/** 
	 *	Filtre des conventions
	 */
	private void filter()	{

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();

	}	


	/**
	 * 
	 */
	public void annuler() {

		currentConvention = null;

		eod.setSelectionIndexes(new NSArray());

		myView.dispose();

	}    

	private class ListenerConvention implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			currentConvention = (EOConvention)eod.selectedObject();
		}
	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}

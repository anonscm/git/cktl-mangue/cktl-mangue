/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.select;

import javax.swing.JComboBox;

import org.cocktail.client.components.DialogueModalAvecArbre;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EOTypeGroupe;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class GroupeSelectCtrl extends DialogueModalAvecArbre {
	public JComboBox popup;
	private static GroupeSelectCtrl sharedInstance = null;
	private EOTypeGroupe typeGroupe;
	private EOEditingContext editingContext;
	private boolean isStarting;
	
	/** Méthodes statiques */
	public static GroupeSelectCtrl sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new GroupeSelectCtrl();
		}
		return sharedInstance;
	}
	public GroupeSelectCtrl() {
		super();
		loadNotifications();
	}
	// Actions
	public void popupHasChanged() {
		if (isStarting || popup == null) {
			return;
		}
		String libelle = (String)popup.getSelectedItem();
		 if (libelle == null) {
			 typeGroupe = null;
		} else {
			typeGroupe = (EOTypeGroupe)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOTypeGroupe.ENTITY_NAME, EOTypeGroupe.LIBELLE_LONG_KEY, libelle);
		}
		updateTreeView();
	}
	public void connectionWasEstablished() {
		isStarting = true;
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
		typeGroupe = (EOTypeGroupe)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext, EOTypeGroupe.ENTITY_NAME, EOTypeGroupe.CODE_KEY, EOTypeGroupe.TYPE_GROUPE_GROUPE);
		// Préparer le popup
		popup.removeAllItems();
		//NSArray typesGroupe = EORepartTypeGroupe.rechercherTypesGroupesPourEtablissement(editingContext);
		NSArray<EOTypeGroupe> typesGroupe = SuperFinder.rechercherEntite(editingContext, EOTypeGroupe.ENTITY_NAME);
		typesGroupe = EOSortOrdering.sortedArrayUsingKeyOrderArray(typesGroupe, new NSArray(EOSortOrdering.sortOrderingWithKey(EOTypeGroupe.LIBELLE_LONG_KEY, EOSortOrdering.CompareAscending)));
		
		for (EOTypeGroupe type : typesGroupe) {
			popup.addItem(type.libelle());			
		}
			
		// On se positionne par défaut sur les groupes
		popup.setSelectedItem(typeGroupe.libelle());
		super.connectionWasEstablished();
		isStarting = false;
	}
	/**
	* Selection d'un groupe
	*
	*/
	public EOStructure selectionnerGroupe(EOEditingContext edc) {
		this.editingContext = edc;
		return (EOStructure)super.selectionnerObjet(edc, false, false);
	}

	public void lockSaisie(NSNotification aNotif) {
		popup.setEnabled(false);
	} 
	public void unlockSaisie(NSNotification aNotif) {
		popup.setEnabled(true);
	} 

	protected void loadArchive() {
		EOArchive.loadArchiveNamed("ChoixGroupe", this,"org.cocktail.mangue.client.outils_interface.interfaces", this.disposableRegistry());
	}
	protected String titreFenetre() {
		return "Sélection d'un groupe";
	}
	protected String titreArbre() {
		return "Groupes";
	}
	protected String nomEntiteAffichee() {
		return "StructureUlr";
	}
	protected String attributeForDisplay() {
		return "llStructure";
	}
	protected EOQualifier qualifierForColumn() {
		return EOQualifier.qualifierWithQualifierFormat(EOStructure.C_TYPE_STRUCTURE_KEY + " = 'E'", null); 
	}
	/** Pas de qualifier, on veut voir tous les fils, on limitera selon leur type la possibilite de les selectionner */
	protected EOQualifier restrictionQualifier() {
		return EOQualifier.qualifierWithQualifierFormat(EOStructure.TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI));
	}
	protected String parentRelationship() {
		return "toStructurePere";
	}
	protected String attributeOfParent() {
		return null;
	}
	protected String parentAttributeOfChild() {
		return null;
	}
	
	/** On peut selectionner un groupe si il est du type selectionne et que l'agent peut voir ce groupe ou un de ces parents */
	protected boolean peutSelectionnerObjet(EOGenericRecord objet) {
		EOStructure currentStructure = (EOStructure)objet;
		if (currentStructure == null) {
			return false;
		}
		boolean isSelectable = false;
		for (java.util.Enumeration<EORepartTypeGroupe> e = currentStructure.toRepartTypeGroupe().objectEnumerator();e.hasMoreElements();) {
			EORepartTypeGroupe repart = e.nextElement();
			if (typeGroupe != null && repart.tgrpCode().equals(typeGroupe.code())) {
				isSelectable = true;
				if (typeGroupe.code().equals("G") && currentStructure.grpAcces() != null && currentStructure.grpAcces().equals("-")) {
					// un groupe privé n'est pas sélectionnable
					isSelectable = false;
				}
				break;
			}
		}
		return isSelectable;
	}
	// Méthodes privées
	private void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockSaisie", new Class[] { NSNotification.class }), ModelePage.LOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("unlockSaisie", new Class[] { NSNotification.class }), ModelePage.DELOCKER_ECRAN, null);
	}

}

package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class CorpsSelectCtrl 
{
	private static CorpsSelectCtrl sharedInstance;

	private 	EOEditingContext	ec;
	private 	EODisplayGroup		eod;
	
	private CorpsSelectView myView;

	private EOCorps currentObject;

			
	/** 
	 *
	 */
	public CorpsSelectCtrl (EOEditingContext editingContext)	{

		super();

		ec = editingContext;
		eod = new EODisplayGroup();

		myView = new CorpsSelectView(null, true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		eod.setSortOrderings(new NSArray (new EOSortOrdering(EOCorps.LL_CORPS_KEY, EOSortOrdering.CompareAscending)));
		myView.getMyEOTable().addListener(new Listener());

		CocktailUtilities.initPopupAvecListe(myView.getPopupMinisteres(), EOMinisteres.findMinisteresValides(ec), true);
		myView.getPopupMinisteres().addActionListener(new PopupFiltreListener());

        myView.getTfFiltreCode().getDocument().addDocumentListener(new ADocumentListener());
        myView.getTfFiltreLbelleCourt().getDocument().addDocumentListener(new ADocumentListener());
        myView.getTfFiltreLibelleLong().getDocument().addDocumentListener(new ADocumentListener());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CorpsSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CorpsSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	public NSArray<EOCorps> getArrayCorps()	{
		
		myView.getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		if (eod.displayedObjects().count() == 0)	{
			eod.setObjectArray(EOCorps.rechercherCorps(ec, false));
			filter();
		}
		
		myView.setVisible(true);	

		if (eod.selectedObjects().count() == 0)	
			return null;

		return eod.selectedObjects();
	}	

	public EOCorps getCorpsPourTypePopulation(EOTypePopulation typePopulation)	{
		
		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

		eod.setObjectArray(EOCorps.rechercherCorpsPourTypePopulation(ec, typePopulation));
		filter();
	
		myView.setVisible(true);

		if (currentObject == null)	
			return null;

		return currentObject;
	}
	public EOCorps getCorpsPourTypePopulationEtDate(EOTypePopulation typePopulation, NSTimestamp dateRef)	{
		
		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

		eod.setObjectArray(EOCorps.rechercherCorpsPourTypePopulation(ec, typePopulation, dateRef));
		filter();
	
		myView.setVisible(true);

		if (currentObject == null)	
			return null;

		return currentObject;
	}
	
	public EOCorps getCorps()	{
		
		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

		eod.setObjectArray(EOCorps.rechercherCorps(ec, false));
		filter();
		
		myView.setVisible(true);	

		if (currentObject == null)	
			return null;

		return currentObject;
	}
		
	
	/**
	 * 
	 * @param qualifier
	 * @return
	 */
	public EOCorps getCorpsPourQualifier(EOQualifier qualifier)	{
		
		myView.getMyEOTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

		eod.setObjectArray(EOCorps.rechercherCorpsPourQualifier(ec, qualifier));
		filter();
		
		myView.setVisible(true);	

		if (currentObject == null)	
			return null;

		return currentObject;
	}

	/**
	 * 
	 * @param qualifier
	 * @return
	 */
	public NSArray<EOCorps> getArrayCorpsPourQualifier(EOQualifier qualifier)	{
		
		myView.getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		eod.setObjectArray(EOCorps.rechercherCorpsPourQualifier(ec, qualifier));
		filter();
		
		myView.setVisible(true);	

		if (eod.displayedObjects().size() == 0)	
			return null;

		return eod.selectedObjects();
	}

	
	/**
	 * 
	 */
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		currentObject = null;
		myView.dispose();
	}  	
	
	
	/**
	 * 
	 * @return
	 */
    public EOQualifier getFilterQualifier()	{
    	
        NSMutableArray mesQualifiers = new NSMutableArray();
        
		if (myView.getPopupMinisteres().getSelectedIndex() > 0)		
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.TO_MINISTERE_KEY + " =%@", new NSArray(myView.getPopupMinisteres().getSelectedItem())));

        if (!StringCtrl.chaineVide(myView.getTfFiltreCode().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFiltreCode().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.C_CORPS_KEY + " caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(myView.getTfFiltreLbelleCourt().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFiltreLbelleCourt().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.LC_CORPS_KEY + " caseInsensitiveLike %@",args));
        }

        if (!StringCtrl.chaineVide(myView.getTfFiltreLibelleLong().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFiltreLibelleLong().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.LL_CORPS_KEY + " caseInsensitiveLike %@",args));
        }
        
        return new EOAndQualifier(mesQualifiers);        
    }

	private void filter() {

	       eod.setQualifier(getFilterQualifier());
	       eod.updateDisplayedObjects();              
	       myView.getMyEOTable().updateData();

	}

	private class PopupFiltreListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			filter();
		}
	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class Listener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			currentObject = (EOCorps)eod.selectedObject();
		}
	}

}
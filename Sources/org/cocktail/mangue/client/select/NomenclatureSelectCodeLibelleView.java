/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client.select;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.swing.TableSorter;
import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableModel;
import org.cocktail.application.client.swing.ZEOTableModelColumn;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.utilities.CocktailIcones;

import com.webobjects.eointerface.EODisplayGroup;

/**
 *
 * @author  cpinsard
 */
public class NomenclatureSelectCodeLibelleView extends javax.swing.JDialog {

	private static final long serialVersionUID = -978106758741617493L;
	protected EODisplayGroup eod;
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected TableSorter myTableSorter;

	/** Creates new form TemplateJDialog */
	public NomenclatureSelectCodeLibelleView(java.awt.Frame parent, boolean modal, 
			EODisplayGroup displayGroup) {
		super(parent, modal);

		eod = displayGroup;

		initComponents();
		initGui();
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */

	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		viewTable = new javax.swing.JPanel();
		tfFiltre = new javax.swing.JTextField();
		btnSelectionner = new javax.swing.JButton();
		btnAnnuler = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		setTitle("Sélection RNE");

		viewTable.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		viewTable.setLayout(new java.awt.BorderLayout());

		btnSelectionner.setText("Sélectionner");
		btnSelectionner.setIconTextGap(2);
		btnSelectionner.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnSelectionnerActionPerformed(evt);
			}
		});

		btnAnnuler.setText("Annuler");
		btnAnnuler.setIconTextGap(2);

		jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel1.setText("Recherche ?");

		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(layout.createSequentialGroup()
						.addContainerGap()
						.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
								.add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
								.add(layout.createSequentialGroup()
										.add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 94, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
										.add(tfFiltre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
										.add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
												.add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
												.add(btnSelectionner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 110, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
												.addContainerGap())
				);
		layout.setVerticalGroup(
				layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(layout.createSequentialGroup()
						.addContainerGap()
						.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
								.add(jLabel1)
								.add(tfFiltre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
								.add(9, 9, 9)
								.add(viewTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE)
								.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
								.add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
										.add(btnAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
										.add(btnSelectionner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
										.addContainerGap())
				);

		java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((screenSize.width-620)/2, (screenSize.height-706)/2, 620, 706);
	}// </editor-fold>//GEN-END:initComponents

	private void btnSelectionnerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectionnerActionPerformed

		dispose();

	}//GEN-LAST:event_btnSelectionnerActionPerformed

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				NomenclatureSelectCodeLibelleView dialog = new NomenclatureSelectCodeLibelleView(new javax.swing.JFrame(), true, null);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton btnAnnuler;
	private javax.swing.JButton btnSelectionner;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JTextField tfFiltre;
	private javax.swing.JPanel viewTable;
	// End of variables declaration//GEN-END:variables

	private void initGui() {

		setTitle("");

		btnSelectionner.setIcon(CocktailIcones.ICON_COCHE);
		btnAnnuler.setIcon(CocktailIcones.ICON_CANCEL);

		Vector<ZEOTableModelColumn> myCols = new Vector<ZEOTableModelColumn>();

		ZEOTableModelColumn	col = new ZEOTableModelColumn(eod, INomenclature.CODE_KEY, "Code", 50);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);
		col = new ZEOTableModelColumn(eod, INomenclature.LIBELLE_LONG_KEY, "Libellé", 400);
		col.setAlignment(SwingConstants.LEFT);
		myCols.add(col);

		myTableModel = new ZEOTableModel(eod, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		//myEOTable.addListener(myListenerContrat);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(new Color(230, 230, 230));
		myEOTable.setSelectionBackground(new Color(127,155,165));
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

		viewTable.setLayout(new BorderLayout());
		viewTable.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		viewTable.removeAll();
		viewTable.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public void setMyEOTable(ZEOTable myEOTable) {
		this.myEOTable = myEOTable;
	}

	public javax.swing.JButton getBtnAnnuler() {
		return btnAnnuler;
	}

	public void setBtnAnnuler(javax.swing.JButton btnAnnuler) {
		this.btnAnnuler = btnAnnuler;
	}

	public javax.swing.JButton getBtnSelectionner() {
		return btnSelectionner;
	}

	public void setBtnSelectionner(javax.swing.JButton btnSelectionner) {
		this.btnSelectionner = btnSelectionner;
	}

	public javax.swing.JTextField getTfFiltre() {
		return tfFiltre;
	}

	public void setTfFiltre(javax.swing.JTextField tfFiltre) {
		this.tfFiltre = tfFiltre;
	}



}

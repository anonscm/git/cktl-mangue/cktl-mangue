/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class SaisieEnfantCtrl {


	private static SaisieEnfantCtrl sharedInstance;

	private 	EOEditingContext		edc;
	private 	boolean 				nouvelEnfant;

	ActionListenerIdentite actionListenerIdentite = new ActionListenerIdentite();
	FocusListenerIdentite focusListenerIdentite = new FocusListenerIdentite();

	private EODisplayGroup eod;		
	private SaisieEnfantView myView;
	private	EOEnfant selectedEnfant;
	private	EOIndividu currentParent;
	private boolean saisieDateNaissance;


	/**
	 * 
	 * @param globalEc
	 */
	public SaisieEnfantCtrl (EOEditingContext edc)	{

		super();

		this.edc = edc;
		eod = new EODisplayGroup();
		myView = new SaisieEnfantView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}});
		myView.getBtnSelectionner().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectionner();}});
		myView.getBtnVerifIdentite().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {verifierIdentite();}});
		myView.getBtnNouvelEnfant().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {creerEnfant();}});

		myView.getMyEOTable().addListener(new ListenerEnfant());
		eod.setSortOrderings(EOEnfant.SORT_ARRAY_NOM_ASC);

		myView.getTfNomUsuel().addActionListener(actionListenerIdentite);
		myView.getTfNomUsuel().addFocusListener(focusListenerIdentite);
		myView.getTfPrenom().addActionListener(actionListenerIdentite);
		myView.getTfPrenom().addFocusListener(focusListenerIdentite);

		myView.getTfNomUsuel().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfPrenom().getDocument().addDocumentListener(new ADocumentListener());

		myView.getTfDateNaissance().addFocusListener(new FocusListenerDateTextField(myView.getTfDateNaissance()));
		myView.getTfDateNaissance().addActionListener(new ActionListenerDateTextField(myView.getTfDateNaissance()));

		updateUI();
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieEnfantCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieEnfantCtrl(editingContext);
		return sharedInstance;
	}
	public EOEditingContext getEdc() {
		return edc;
	}
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOEnfant selectedEnfant() {
		return selectedEnfant;
	}
	public void setSelectedEnfant(EOEnfant selectedEnfant) {
		this.selectedEnfant = selectedEnfant;
	}

	public EOIndividu currentParent() {
		return currentParent;
	}

	public void setCurrentParent(EOIndividu currentParent) {
		this.currentParent = currentParent;
	}

	public boolean isNouvelEnfant() {
		return nouvelEnfant;
	}
	public void setNouvelEnfant(boolean nouvelEnfant) {
		this.nouvelEnfant = nouvelEnfant;
	}

	protected boolean traitementsPourCreation() {

		if (myView.getTfNomUsuel().getText().length() == 0) {
			EODialogs.runInformationDialog("ERREUR", "Veuillez saisir un nom usuel !");
			return false;
		}

		if (saisieDateNaissance() && myView.getTfDateNaissance().getText().length() == 0) {
			EODialogs.runInformationDialog("ERREUR", "Veuillez saisir une date de naissance !");
			return false;
		}

		return true;
	}


	public void creerEnfant()	{		
		if (!traitementsPourCreation())
			return;

		setNouvelEnfant(true);
		myView.setVisible(false);
	}

	public void selectionner()	{
		setNouvelEnfant(false);
		myView.setVisible(false);

	}

	public void clearTextFields() {
		CocktailUtilities.viderTextField(myView.getTfNomUsuel());
		CocktailUtilities.viderTextField(myView.getTfPrenom());
		CocktailUtilities.viderTextField(myView.getTfDateNaissance());
	}

	/**
	 * 
	 * @param individu
	 * @return
	 */
	public EOEnfant getEnfant(EOIndividu individu)	{		

		setCurrentParent(individu);

		clearTextFields();
		setSaisieDateNaissance(false);
		CocktailUtilities.setTextToField(myView.getTfNomUsuel(), individu.nomUsuel());
		verifierIdentite();
		updateUI();
		CocktailUtilities.setFocusOn(myView.getTfPrenom());
		myView.setVisible(true);
		
		if (isNouvelEnfant()) {
			setSelectedEnfant(EOEnfant.creer(getEdc(), currentParent()));
			selectedEnfant().setNom(myView.getTfNomUsuel().getText().toUpperCase());
			selectedEnfant().setPrenom(myView.getTfPrenom().getText().toUpperCase());
		}

		return selectedEnfant();

	}

	public boolean saisieDateNaissance() {
		return saisieDateNaissance;
	}
	public void setSaisieDateNaissance(boolean saisieDateNaissance) {
		this.saisieDateNaissance = saisieDateNaissance;
	}

	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		myView.dispose();
	}  	

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerEnfant implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {selectionner();}
		public void onSelectionChanged() {			
			try {
				setSelectedEnfant((EOEnfant)eod.selectedObject());
			}
			catch (Exception e){
				setSelectedEnfant(null);
			}
			finally {
				updateUI();
			}

		}
	}

	/**
	 * 
	 */
	private void verifierIdentite() {		
		if (StringCtrl.chaineVide(myView.getTfNomUsuel().getText()))	{
			EODialogs.runErrorDialog("ERREUR","Veuillez au moins entrer tout ou partie du nom du nouvel enfant !");
			return;
		}
		
		eod.setObjectArray(EOEnfant.rechercherEnfantsPourCreation(getEdc(), myView.getTfNomUsuel().getText(), myView.getTfPrenom().getText()));
		myView.getMyEOTable().updateData();
	}

	/**
	 * 
	 */
	private void updateUI() {		

		myView.getBtnNouvelEnfant().setEnabled(
				StringCtrl.chaineVide(myView.getTfNomUsuel().getText()) == false
				&&  StringCtrl.chaineVide(myView.getTfPrenom().getText()) == false );

		myView.getBtnSelectionner().setEnabled(selectedEnfant() != null );

		myView.getTfDateNaissance().setVisible(saisieDateNaissance());
		myView.getLblDateNaissance().setVisible(saisieDateNaissance());

	}

	
	public class ActionListenerIdentite implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			verifierIdentite();
		}
	}
	public class FocusListenerIdentite implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e) {
		}
		public void focusLost(FocusEvent e)	{
			verifierIdentite();
		}
	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			updateUI();
		}

		public void insertUpdate(DocumentEvent e) {
			updateUI();		
		}

		public void removeUpdate(DocumentEvent e) {
			updateUI();		
		}
	}


	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			updateUI();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{

		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{

		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);
		}
	}	
}

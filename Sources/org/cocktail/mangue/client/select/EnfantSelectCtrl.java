/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import javax.swing.JFrame;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EnfantSelectCtrl  {


	private static EnfantSelectCtrl sharedInstance;

	private EOEditingContext ec;

	private EnfantSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();
	private EOEnfant currentObject;

	/**
	 * 
	 *
	 */
	public EnfantSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new EnfantSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new Listener());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EnfantSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new EnfantSelectCtrl(editingContext);
		return sharedInstance;
	}

	public EOEnfant getEnfantForQualifier(EOQualifier qualifier)	{
		
		eod.setObjectArray(EOEnfant.rechercherEnfantsPourQualifier(ec, qualifier));
		myView.getMyEOTable().updateData();

		myView.setVisible(true);

		return currentObject;
	}

	public EOEnfant getEnfant(EOIndividu individu)	{
				
		eod.setObjectArray(EOEnfant.rechercherEnfantsPourIndividu(ec, individu));
		myView.getMyEOTable().updateData();

		myView.setVisible(true);

		return currentObject;
	}
	public EOEnfant getEnfant(EOIndividu individu, NSTimestamp dateReference, Integer limiteAge)	{
		
		myView.setTitle("Enfants de moins de " + limiteAge + " ans au " + DateCtrl.dateToString(dateReference));
		
		eod.setObjectArray(EOEnfant.rechercherEnfantsAvecLimiteAgePourIndividu(ec, individu, dateReference, limiteAge));
		myView.getMyEOTable().updateData();

		myView.setVisible(true);

		return currentObject;
	}
	public EOEnfant getEnfantAdopte(EOIndividu individu)	{
		
		myView.setTitle("Enfants adoptés");

		eod.setObjectArray(EOEnfant.rechercherEnfantsAdoptesPourIndividu(ec, individu));
		myView.getMyEOTable().updateData();

		myView.setVisible(true);

		return currentObject;
	}



	public void annuler() {

		currentObject = null;

		eod.setSelectionIndexes(new NSArray());

		myView.dispose();

	}  

	private class Listener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EOEnfant)eod.selectedObject();
		}
	}
}

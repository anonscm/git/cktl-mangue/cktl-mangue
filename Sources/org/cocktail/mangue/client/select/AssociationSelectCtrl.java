package org.cocktail.mangue.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class AssociationSelectCtrl 
{
	private static AssociationSelectCtrl sharedInstance;

	private 	EOEditingContext	ec;
	private 	EODisplayGroup		eod;
	
	private AssociationSelectView myView;

	private EOAssociation currentObject;

			
	/** 
	 *
	 */
	public AssociationSelectCtrl (EOEditingContext editingContext)	{

		super();

		ec = editingContext;
		eod = new EODisplayGroup();

		myView = new AssociationSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		eod.setSortOrderings(EOAssociation.SORT_ARRAY_LIBELLE);
		myView.getMyEOTable().addListener(new Listener());

        myView.getTfFiltreLibelle().getDocument().addDocumentListener(new ADocumentListener());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AssociationSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new AssociationSelectCtrl(editingContext);
		return sharedInstance;
	}
		

	public EOAssociation getAssociation()	{
		
		if (eod.displayedObjects().count() == 0)	{
			eod.setObjectArray(EOAssociation.fetchAll(ec, null, EOAssociation.SORT_ARRAY_LIBELLE));
			filter();
		}
		
		myView.setVisible(true);	

		if (currentObject == null)	
			return null;

		return currentObject;
	}
		
	/**
	 * 
	 */
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		currentObject = null;
		myView.dispose();
	}  	
	
	
	/**
	 * 
	 * @return
	 */
    public EOQualifier getFilterQualifier()	{
        NSMutableArray mesQualifiers = new NSMutableArray();
        
        if (!StringCtrl.chaineVide(myView.getTfFiltreLibelle().getText()))	{
            NSArray args = new NSArray("*"+myView.getTfFiltreLibelle().getText()+"*");
            mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAssociation.LIBELLE_LONG_KEY + " caseInsensitiveLike %@",args));
        }
        
        return new EOAndQualifier(mesQualifiers);        
    }

	private void filter() {

	       eod.setQualifier(getFilterQualifier());
	       eod.updateDisplayedObjects();              
	       myView.getMyEOTable().updateData();

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class Listener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EOAssociation)eod.selectedObject();
		}
	}

}
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOBanque;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class BanqueSelectCtrl  {


	private static BanqueSelectCtrl sharedInstance;
	private EOEditingContext ec;

	private BanqueSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();

	private EOBanque currentRecord;

	public BanqueSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new BanqueSelectView(new JFrame(), true, eod);

		myView.getBtnFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new LocalListener());

		myView.getTfFindBanque().addActionListener(new MyActionListener());
		myView.getTfFindGuichet().addActionListener(new MyActionListener());
		myView.getTfFindDomiciliation().addActionListener(new MyActionListener());

		myView.getTfFindBanque().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindGuichet().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindDomiciliation().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindBic().getDocument().addDocumentListener(new ADocumentListener());

		CocktailUtilities.initTextField(myView.getTfFindDomiciliation(), false, true);
		CocktailUtilities.initTextField(myView.getTfFindBic(), false, true);

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static BanqueSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new BanqueSelectCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 */
	private void rechercher() {

		if (myView.getTfFindBanque().getText().length() == 0 
				&& myView.getTfFindGuichet().getText().length() == 0
				&& myView.getTfFindDomiciliation().getText().length() == 0
				&& myView.getTfFindBic().getText().length() == 0) {
			EODialogs.runInformationDialog("ERREUR","Veuillez renseigner au moins un critère de recherche !");
			return;
		}

		if (myView.getTfFindBanque().getText().length() == 0 
				&& myView.getTfFindGuichet().getText().length() == 0
				&& myView.getTfFindBic().getText().length() == 0
				&& myView.getTfFindDomiciliation().getText().length() < 4) {
			EODialogs.runInformationDialog("ERREUR","Veuillez renseigner une domiciliation d'au moins 4 caractères !");
			return;
		}

		if (myView.getTfFindBanque().getText().length() == 0 
				&& myView.getTfFindGuichet().getText().length() == 0
				&& myView.getTfFindDomiciliation().getText().length() == 0
				&& myView.getTfFindBic().getText().length() < 4) {
			EODialogs.runInformationDialog("ERREUR","Veuillez renseigner un code BIC d'au moins 4 caractères !");
			return;
		}

		eod.setObjectArray(EOBanque.find(ec, myView.getTfFindBanque().getText(), myView.getTfFindGuichet().getText(), myView.getTfFindDomiciliation().getText(), myView.getTfFindBic().getText()));
		myView.getMyEOTable().updateData();
	}


	/**
	 * 
	 * @return
	 */
	public EOBanque getBanque()	{
		myView.setVisible(true);
		return currentRecord;
	}	


	private EOQualifier getFilterQualifier()	{
		
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFindBanque().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindBanque().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBanque.C_BANQUE_KEY+" caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFindGuichet().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindGuichet().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBanque.C_GUICHET_KEY+" caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFindDomiciliation().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindDomiciliation().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBanque.DOMICILIATION_KEY+" caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFindBic().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindBic().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBanque.BIC_KEY+" caseInsensitiveLike %@",args));
		}
		
		return new EOAndQualifier(mesQualifiers);       

	}



	/** 
	 *
	 */
	private void filter()	{

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();

	}


	/**
	 * 
	 */
	public void annuler() {

		currentRecord = null;

		eod.setSelectionIndexes(new NSArray());

		myView.dispose();

	}  


	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	public class LocalListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			currentRecord = (EOBanque)eod.selectedObject();
		}
	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			rechercher();

		}
	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}

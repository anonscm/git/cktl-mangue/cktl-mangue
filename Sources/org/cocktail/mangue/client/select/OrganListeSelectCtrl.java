/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.mangue.budget.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class OrganListeSelectCtrl  {


	private static OrganListeSelectCtrl sharedInstance;

	private EOEditingContext ec;

	private OrganListeSelectView myView;

	private EODisplayGroup eod;

	private Integer currentExercice;
	private EOOrgan currentOrgan;

	/**
	 * 
	 *
	 */
	public OrganListeSelectCtrl(EOEditingContext editingContext)	{
		super();

		ec = editingContext;

		eod = new EODisplayGroup();
		
		myView = new OrganListeSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});
		
		myView.getMyEOTable().addListener(new ListenerOrgan());

		myView.getTfFiltreEtab().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreUb().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreCr().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreSousCr().getDocument().addDocumentListener(new ADocumentListener());
		
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static OrganListeSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new OrganListeSelectCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 * @return
	 */
	public NSArray<EOOrgan> getOrgans(NSArray organs, String preferredUb, Integer exercice)	{

		if (preferredUb != null) {		
			myView.getTfFiltreUb().setText(preferredUb);
			CocktailUtilities.initTextField(myView.getTfFiltreUb(), false, false);
		}
		else {
			CocktailUtilities.initTextField(myView.getTfFiltreUb(), false, true);			
		}

		if (eod.displayedObjects().count() == 0 || currentExercice == null || 
				(currentExercice.intValue() != exercice)) {
			
			eod.setObjectArray(EOOrgan.findOrgansForExercice(ec, exercice));							
			currentExercice = exercice;
			
		}

		filter();

		myView.setVisible(true);

		return (NSArray<EOOrgan>)eod.selectedObjects();

	}
	/**
	 * 
	 * @return
	 */
	public EOOrgan getOrgan(NSArray organs, String preferredUb, Integer exercice)	{

		if (preferredUb != null) {		
			myView.getTfFiltreUb().setText(preferredUb);
			CocktailUtilities.initTextField(myView.getTfFiltreUb(), false, false);
		}
		else {
			CocktailUtilities.initTextField(myView.getTfFiltreUb(), false, true);			
		}

		if (eod.displayedObjects().count() == 0 || currentExercice == null || 
				(currentExercice.intValue() != exercice)) {
			
			eod.setObjectArray(EOOrgan.findOrgansForExercice(ec, exercice));							
			currentExercice = exercice;
			
		}

		filter();

		myView.setVisible(true);

		return currentOrgan;

	}


	/**
	 * 
	 * @return
	 */
	public EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

//		if (checkNiveau3.isSelected())
//			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = 3", null));
//		else
//			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = 4", null));

		if (!StringCtrl.chaineVide(myView.getTfFiltreEtab().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreEtab().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_ETAB_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFiltreUb().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreUb().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFiltreCr().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreCr().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_CR_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFiltreSousCr().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltreSousCr().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_SOUSCR_KEY + " caseInsensitiveLike %@",args));
		}

		return new EOAndQualifier(mesQualifiers);        
	}

	/** 
	 *
	 */
	public void filter()	{

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();

	}


	public void annuler() {
		
		currentOrgan = null;
		myView.dispose();
		
	}  


	private class ListenerOrgan implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {	
			currentOrgan =(EOOrgan)eod.selectedObject();
		}
	}


	/**
	 * Permet de définir un listener sur le contenu du champ texte qui sert  a filtrer la table. 
	 * Dés que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.mangue.budget.EOLolfNomenclatureDepense;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class LolfSelectCtrl  {


	private static LolfSelectCtrl sharedInstance;

	private EOEditingContext edc;

	private LolfSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();

	private EOLolfNomenclatureDepense currentLolf;
	private Integer currentExercice;


	/**
	 * 
	 *
	 */
	public LolfSelectCtrl(EOEditingContext edc)	{

		setEdc(edc);
		myView = new LolfSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerLolf());

		myView.getTfFindCode().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFindLibelle().getDocument().addDocumentListener(new ADocumentListener());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static LolfSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new LolfSelectCtrl(editingContext);
		return sharedInstance;
	}


	public EOEditingContext getEdc() {
		return edc;
	}


	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}


	public EOLolfNomenclatureDepense getCurrentLolf() {
		return currentLolf;
	}


	public void setCurrentLolf(EOLolfNomenclatureDepense currentLolf) {
		this.currentLolf = currentLolf;
	}


	public Integer getCurrentExercice() {
		return currentExercice;
	}


	public void setCurrentExercice(Integer currentExercice) {
		this.currentExercice = currentExercice;
	}


	/**
	 * 
	 * @param dateReference
	 * @return
	 */
	public EOLolfNomenclatureDepense getLolf(NSTimestamp dateReference)	{

		myView.setTitle("Sélection d'une action Lolf ("+DateCtrl.dateToString(dateReference)+")");		
			
		eod.setObjectArray(EOLolfNomenclatureDepense.findForDate(edc, dateReference));		

		filter();

		myView.setVisible(true);

		return currentLolf;

	}

	/**
	 * 
	 * @param exercice
	 * @param filtre
	 * @return
	 */
	public EOLolfNomenclatureDepense getTypeAction(Integer exercice, boolean filtre)	{

		// On recharge les donnees seulement si le ctrl est lance pour la premiere fois ou si on a change d'exercice budgetaire
		if (getCurrentExercice() == null || (getCurrentExercice() != exercice) || eod.displayedObjects().count() == 0) {

			setCurrentExercice(exercice);
			myView.setTitle("Sélection d'une action Lolf (" + getCurrentExercice() + ")");		

			eod.setObjectArray(EOLolfNomenclatureDepense.findForDate(edc, DateCtrl.debutAnnee(exercice)));	

			filter();
		}

		myView.setVisible(true);

		return currentLolf;

	}


	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFindCode().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindCode().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.LOLF_CODE_KEY + " caseInsensitiveLike %@",args));
		}

		if (!StringCtrl.chaineVide(myView.getTfFindLibelle().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindLibelle().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureDepense.LOLF_LIBELLE_KEY + " caseInsensitiveLike %@",args));
		}

		return new EOAndQualifier(mesQualifiers);        
	}

	/** 
	 *
	 */
	private void filter()	{

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();

	}



	/**
	 * 
	 */
	private void annuler() {

		setCurrentLolf(null);
		eod.setSelectionIndexes(new NSArray());
		myView.dispose();
	}  


	private class ListenerLolf implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			setCurrentLolf((EOLolfNomenclatureDepense)eod.selectedObject());
		}
	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}

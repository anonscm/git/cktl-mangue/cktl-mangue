
//ChoixStructure.java
//Mangue

//Created by Christine Buttin on Thu May 26 2005.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.
//Permet le choix d'une structure via un dialogue modal


/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.select;

// 25/12/2010 - Modifications pour prendre en compte les modifications apportées à DialogModalAvecArbre
import org.cocktail.client.components.DialogueModalAvecArbre;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** Permet le choix d'une structure via un dialogue modal */
public class StructureArbreSelectCtrl extends DialogueModalAvecArbre {
	
	private static StructureArbreSelectCtrl sharedInstance = null;
	private NSArray structuresGereesParUtilisateur;
	private NSTimestamp dateRestriction = null;
	
	/** Méthodes statiques */
	public static StructureArbreSelectCtrl sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new StructureArbreSelectCtrl();
		}
		return sharedInstance;
	}

	
	
	public NSTimestamp getDateRestriction() {
		return dateRestriction;
	}



	public void setDateRestriction(NSTimestamp dateRestriction) {
		this.dateRestriction = dateRestriction;
	}



	/**
	 * Selection d'une structure
	 *
	 */
	public EOStructure selectionnerStructure(EOEditingContext editingContext) {
		return selectionnerStructure(editingContext, null);
	}
	public EOStructure selectionnerStructure(EOEditingContext editingContext, NSTimestamp dateReference) {

		setDateRestriction(dateReference);
		
		EOAgentPersonnel utilisateurCourant = (((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
		if (utilisateurCourant.gereTouteStructure()) {
			structuresGereesParUtilisateur = null;
		} else {
			structuresGereesParUtilisateur = utilisateurCourant.structuresGerees();
		}
		return (EOStructure)super.selectionnerObjet(editingContext);
	}

	// méthodes protégées
	protected String titreFenetre() {
		return "Sélection d'une structure";
	}
	protected String titreArbre() {
		return "Structures";
	}
	protected String nomEntiteAffichee() {
		return EOStructure.ENTITY_NAME;
	}
	protected EOQualifier qualifierForColumn() {
		return EOQualifier.qualifierWithQualifierFormat("temValide = 'O' and cStructure = cStructurePere", null);
	}
	protected EOQualifier restrictionQualifier() {
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.TEM_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.TO_REPART_TYPE_GROUPE_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY + " = %@", new NSArray("S")));

		if (getDateRestriction() != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.DATE_FERMETURE_KEY + " = nil or " + EOStructure.DATE_FERMETURE_KEY + " >= %@", new NSArray(getDateRestriction())));

		return new EOAndQualifier(qualifiers);
	}
	
	protected String parentRelationship() {
		return EOStructure.TO_STRUCTURE_PERE_KEY;
	}
	protected String attributeForDisplay() {
		return EOStructure.LL_STRUCTURE_KEY;
	}
	protected String attributeOfParent() {
		return null;
	}
	protected String parentAttributeOfChild() {
		return null;
	}
	/** On peut selectionner une structure si l'agent peut voir cette structure ou un de ces parents */
	protected boolean peutSelectionnerObjet(EOGenericRecord objet) {
		if (structuresGereesParUtilisateur == null) {
			// l'utilisateur gère tout
			return true;
		}
		boolean found = false,finished = false;
		EOStructure currentStructure = (EOStructure)objet;
		do {
			if (currentStructure.estEtablissement()) {
				finished = true;
			}
			if (structuresGereesParUtilisateur.containsObject(currentStructure)) {
				found = true;
			} else if (!finished) {
				currentStructure = currentStructure.toStructurePere();
			}
		} while (!found && !finished);
		return found;
	}
}


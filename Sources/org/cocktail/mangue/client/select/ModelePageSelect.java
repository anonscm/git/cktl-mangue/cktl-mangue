package org.cocktail.mangue.client.select;

import javax.swing.JDialog;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.mangue.modele.grhum.elections.EOBureauVote;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * Classe d'un modèle de page de saisie
 * 
 * @author Cyril PINSARD
 * @author Chama LAATIK
 *
 */
public abstract class ModelePageSelect {
	
	private EOEditingContext edc;
	private EODisplayGroup eod = new EODisplayGroup();
	private EOGenericRecord currentObject;
	private JDialog myView;
	private MyListener listener = new MyListener();
	
	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	public ModelePageSelect(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOEditingContext getEdc() {
		return edc;
	}
	public void setEod(EODisplayGroup eod) {
		this.eod = eod;
	}
	public EODisplayGroup getEod() {
		return eod;
	}
	public void setView(JDialog view) {
		myView = view;
	}
	public JDialog getView() {
		return myView;
	}
	public EOGenericRecord getCurrentObject() {
		return currentObject;
	}
	public void setCurrentObject(EOGenericRecord currentObject) {
		this.currentObject = currentObject;
	}
	public MyListener getListener() {
		return listener;
	}
	public void setListener(MyListener listener) {
		this.listener = listener;
	}

	public void annuler() {
		setCurrentObject(null);
		eod.setSelectionIndexes(new NSArray<EOBureauVote>());
		myView.dispose();
	}  
	private class MyListener implements ZEOTableListener {
		public void onDbClick() {
			myView.dispose();
		}
		public void onSelectionChanged() {
			setCurrentObject((EOGenericRecord)eod.selectedObject());
		}
	}	   

}
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import javax.swing.JFrame;

import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class IndividuSelectCtrl {

	
	private static IndividuSelectCtrl sharedInstance;
	
	private 	EOEditingContext		ec;
	
	private EODisplayGroup eod;
		
	IndividuSelectView myView;
	
	/**
	 * 
	 * @param globalEc
	 */
	public IndividuSelectCtrl (EOEditingContext globalEc)	{
		super();

		ec = globalEc;

		eod = new EODisplayGroup();

		myView = new IndividuSelectView(new JFrame(), true, eod);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getButtonFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});

		myView.getTfNom().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				rechercher();
			}
		});
		
		myView.getMyEOTable().addListener(new ListenerPersonnel());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static IndividuSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new IndividuSelectCtrl(editingContext);
		return sharedInstance;
	}
		
	/**
	 * Selection d'un engagement
	 */
	public EOIndividu getIndividu()	{
		
		myView.setVisible(true);
		
		if (eod.selectedObjects().count() > 0)
			return (EOIndividu) eod.selectedObject();			
		
		return null;
	}

	
	/** 
	 * Recherche de tous les individus correspondant aux criteres entres
	 */
	public void rechercher()	{
		
		if (StringCtrl.chaineVide(myView.getTfNom().getText()))	{
			EODialogs.runInformationDialog("ERREUR","Veuillez saisir au moins une chaine de caractères pour la recherche");
			return;
		}
		
		NSMutableArray mesQualifiers = new NSMutableArray();
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividu.TEM_VALIDE_KEY + " = 'O'", null));
		
		if (!StringCtrl.chaineVide(myView.getTfNom().getText()))
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike '*" + myView.getTfNom().getText() + "*' and prenom != nil", null));

		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOIndividu.PRENOM_KEY, EOSortOrdering.CompareAscending));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOIndividu.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
				
		eod.setObjectArray(ec.objectsWithFetchSpecification(fs));	
		
		myView.getMyEOTable().updateData();
		
	}
		
	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		myView.dispose();
	}  	
	
	private class ListenerPersonnel implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
		}
	}

	
	
}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.util.Enumeration;

import javax.swing.JFrame;

import org.cocktail.mangue.modele.mangue.EODestinataire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class DestinatairesSelectCtrl  {


	private static DestinatairesSelectCtrl sharedInstance;

	private EOEditingContext edc;

	private DestinatairesSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();

	private EODestinataire currentObject;
	private boolean isCancelled;


	/**
	 * 
	 *
	 */
	public DestinatairesSelectCtrl(EOEditingContext editingContext)	{

		super();

		edc = editingContext;

		myView = new DestinatairesSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerSelection());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static DestinatairesSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new DestinatairesSelectCtrl(editingContext);
		return sharedInstance;
	}


	public EODestinataire getCurrentObject() {
		return currentObject;
	}


	public void setCurrentObject(EODestinataire currentObject) {
		this.currentObject = currentObject;
	}


	public boolean isCancelled() {
		return isCancelled;
	}


	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}


	/**
	 * 
	 * @return
	 */
	public NSArray<EODestinataire> getDestinataires()	{

		myView.setTitle("Sélection d'un ou plusieurs destinataires");		
		eod.setObjectArray(EODestinataire.find(edc));
		myView.getMyEOTable().updateData();

		setCancelled(false);
		myView.setVisible(true);

		if (isCancelled())
			return null;

		return eod.selectedObjects();
	}
	public NSArray<EOGlobalID> getDestinatairesGlobalIds()	{

		myView.setTitle("Sélection d'un ou plusieurs destinataires");		
		eod.setObjectArray(EODestinataire.find(edc));
		myView.getMyEOTable().updateData();

		setCancelled(false);
		myView.setVisible(true);

		if (isCancelled())
			return null;
		
		NSMutableArray<EOGlobalID> destinatairesGlobalIds = new NSMutableArray();
		for (Enumeration<EODestinataire> e=eod.selectedObjects().objectEnumerator();e.hasMoreElements();
			destinatairesGlobalIds.addObject(edc.globalIDForObject(e.nextElement())));

		
		return destinatairesGlobalIds.immutableClone();
	}


	/**
	 * 
	 */
	public void annuler() {

		setCurrentObject(null);
		setCancelled(true);
		eod.setSelectionIndexes(new NSArray());

		myView.dispose();

	}  

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerSelection implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentObject = (EODestinataire)eod.selectedObject();

		}

	}	   
}

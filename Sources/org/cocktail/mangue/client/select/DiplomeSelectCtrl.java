/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class DiplomeSelectCtrl  {

	private static DiplomeSelectCtrl sharedInstance;
	private EOEditingContext ec;

	private DiplomeSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();

	private EODiplomes currentRecord;


	public DiplomeSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new DiplomeSelectView(new JFrame(), true, eod);
		setSize(0);
		
		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new LocalListener());

		myView.getTfFindLibelle().addActionListener(new MyActionListener());
		myView.getTfFindLibelle().getDocument().addDocumentListener(new ADocumentListener());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static DiplomeSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new DiplomeSelectCtrl(editingContext);
		return sharedInstance;
	}

	public void setSize(int value) {
		myView.setSize(1000, 600);
	}
	
	private void filter() {

		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();

	}



	public EODiplomes getDiplome()	{

		eod.setObjectArray(EODiplomes.findDiplomesValides(ec));		
		filter();

		myView.setVisible(true);

		return currentRecord;

	}	

	public NSArray<EODiplomes> getDiplomes()	{

		eod.setObjectArray(EODiplomes.findDiplomesValides(ec));		
		filter();

		myView.setVisible(true);

		return eod.selectedObjects();

	}	


	private EOQualifier getFilterQualifier()	{

		NSMutableArray mesQualifiers = new NSMutableArray();
		NSMutableArray orQualifiers = new NSMutableArray();

		if (!StringCtrl.chaineVide(myView.getTfFindLibelle().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFindLibelle().getText()+"*");
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + " caseInsensitiveLike %@",args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_COURT_KEY + " caseInsensitiveLike %@",args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_LONG_KEY + " caseInsensitiveLike %@",args));

			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(mesQualifiers);       

	}

	/**
	 * 
	 */
	private void annuler() {

		currentRecord = null;
		eod.setSelectionIndexes(new NSArray<EODiplomes>());

		myView.dispose();

	}  


	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	private class LocalListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentRecord = (EODiplomes)eod.selectedObject();
		}
	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			filter();

		}
	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}

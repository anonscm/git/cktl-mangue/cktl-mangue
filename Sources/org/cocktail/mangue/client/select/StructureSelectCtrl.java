/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EOTypeGroupe;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class StructureSelectCtrl  {


	private static final int INDEX_TYPE_SERVICE = 4;
	private static StructureSelectCtrl sharedInstance;
	private EOEditingContext ec;

	private StructureSelectView myView;

	private EODisplayGroup eod  = new EODisplayGroup();

	private EOStructure currentRecord;


	public StructureSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		myView = new StructureSelectView(new JFrame(), true, eod);

		myView.getBtnFind().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {rechercher();}
		});

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}
		});

		myView.getMyEOTable().addListener(new LocalListener());

		CocktailUtilities.initPopupAvecListe(myView.getPopupTypes(), NomenclatureFinder.findForCodes(ec, EOTypeGroupe.ENTITY_NAME, EOTypeGroupe.ARRAY_FILTRES), true);

		myView.getTfFiltreStructure().addActionListener(new MyActionListener());
		myView.getTfFiltrePere().addActionListener(new MyActionListener());
		myView.getPopupTypes().addActionListener(new MyActionListener());

		myView.getTfFiltreStructure().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltrePere().getDocument().addDocumentListener(new ADocumentListener());

	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static StructureSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new StructureSelectCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 */
	private void rechercher() {

		if (myView.getPopupTypes().getSelectedIndex() == 0)  {
			if (myView.getTfFiltreStructure().getText().length() == 0 
					&& myView.getTfFiltrePere().getText().length() == 0) { 
				EODialogs.runInformationDialog("ERREUR","Veuillez renseigner au moins un critère de recherche !");
				return;
			}
			if (myView.getTfFiltreStructure().getText().length() == 0 && myView.getTfFiltrePere().getText().length() <= 2) {
				EODialogs.runInformationDialog("ERREUR","Veuillez renseigner un libellé d'au moins 2 caractères !");
				return;
			}
			if (myView.getTfFiltreStructure().getText().length() <= 2 && myView.getTfFiltrePere().getText().length() ==0) {
				EODialogs.runInformationDialog("ERREUR","Veuillez renseigner un code d'au moins 2 caractères !");
				return;
			}
		}
		
		CRICursor.setWaitCursor(myView);
		
		EOTypeGroupe typeGroupe = null;
		if (myView.getPopupTypes().getSelectedIndex() > 0)
			typeGroupe = (EOTypeGroupe)myView.getPopupTypes().getSelectedItem();
		eod.setObjectArray(EOStructure.rechercherStructuresAvecLibelle(ec, myView.getTfFiltreStructure().getText(), myView.getTfFiltrePere().getText(), typeGroupe));
		filter();
		
		CRICursor.setDefaultCursor(myView);
		
	}


	public EOStructure getStructure()	{
		myView.setVisible(true);
		return currentRecord;
	}	
	public EOStructure getStructureService()	{
		myView.getPopupTypes().setSelectedIndex(INDEX_TYPE_SERVICE);
		myView.setVisible(true);
		return currentRecord;
	}	


	private EOQualifier getFilterQualifier()	{
		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.TEM_VALIDE_KEY + "=%@",new NSArray(CocktailConstantes.VRAI)));

		if (!StringCtrl.chaineVide(myView.getTfFiltreStructure().getText()))	{
			
			NSArray args = new NSArray("*"+myView.getTfFiltreStructure().getText()+"*");
			
			NSMutableArray orQualifiers = new NSMutableArray();
			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.LL_STRUCTURE_KEY + " caseInsensitiveLike %@",args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.LC_STRUCTURE_KEY + " caseInsensitiveLike %@",args));
		
			mesQualifiers.addObject(new EOOrQualifier(orQualifiers));
			
		}

		if (!StringCtrl.chaineVide(myView.getTfFiltrePere().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltrePere().getText()+"*");
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.TO_STRUCTURE_PERE_KEY + "." + EOStructure.LL_STRUCTURE_KEY + " caseInsensitiveLike %@",args));
		}

//		if (myView.getPopupTypes().getSelectedIndex() > 0)
//			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructure.TO_REPART_TYPE_GROUPE_KEY + "." + EORepartTypeGroupe.TYPE_GROUPE_KEY + "=%@", new NSArray(myView.getPopupTypes().getSelectedItem())));
	
		return new EOAndQualifier(mesQualifiers);       
	}



	/** 
	 *
	 */
	private void filter()	{
		eod.setQualifier(getFilterQualifier());
		eod.updateDisplayedObjects();
		myView.getMyEOTable().updateData();
	}


	/**
	 * 
	 */
	public void annuler() {

		currentRecord = null;

		eod.setSelectionIndexes(new NSArray());

		myView.dispose();

	}  


	/**
	 * Listener sur le premier niveau de l'arborescence budgetaire
	 * Mise a jour du deuxieme niveau si premier niveau selectionne
	 */
	public class LocalListener implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			myView.dispose();
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			currentRecord = (EOStructure)eod.selectedObject();
		}
	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			rechercher();
		}
	}


	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}

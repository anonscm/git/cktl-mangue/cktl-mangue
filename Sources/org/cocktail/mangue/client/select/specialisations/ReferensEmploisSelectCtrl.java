/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select.specialisations;

import javax.swing.JFrame;

import org.cocktail.mangue.client.nomenclatures.ModelePageSelectNomenclature;
import org.cocktail.mangue.client.select.NomenclatureSelectCodeLibelleView;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.NomenclatureAvecDate;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class ReferensEmploisSelectCtrl extends ModelePageSelectNomenclature {

	private static ReferensEmploisSelectCtrl sharedInstance;
	private NomenclatureSelectCodeLibelleView myView;

	/**
	 * 
	 * @param editingContext
	 */
	public ReferensEmploisSelectCtrl(EOEditingContext edc)	{

		super(edc);

		myView = new NomenclatureSelectCodeLibelleView(new JFrame(), true, getEod());
		myView.setTitle("Referens");

		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setDocumentFiltreListener(myView.getTfFiltre());
		setListener(myView.getMyEOTable());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ReferensEmploisSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ReferensEmploisSelectCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 * @return
	 */
	public INomenclature getObject(NSTimestamp dateReference)	{

		getEod().setObjectArray(NomenclatureAvecDate.findForDate(getEdc(), EOReferensEmplois.ENTITY_NAME, dateReference, EOReferensEmplois.SORT_ARRAY_CODE));
		myView.getMyEOTable().updateData();

		myView.setVisible(true);
		return getCurrentObject();
	}	

	@Override
	protected void traitementsApresFiltre() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().updateData();
	}
	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}
	@Override
	protected String getFilterValue() {
		// TODO Auto-generated method stub
		return CocktailUtilities.getTextFromField(myView.getTfFiltre());
	}
}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.application.client.tools.CocktailUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CategorieEmploiSelectCtrl  {

	private static CategorieEmploiSelectCtrl sharedInstance;

	private EOEditingContext edc;

	private CategorieEmploiSelectView myView;

	private EODisplayGroup 		eod  = new EODisplayGroup();
	private EOCategorieEmploi 	currentObject;

	public CategorieEmploiSelectCtrl(EOEditingContext editingContext)	{

		super();

		edc = editingContext;

		myView = new CategorieEmploiSelectView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerObject());
		myView.getTfFiltreLibelle().getDocument().addDocumentListener(new ADocumentListener());

		CocktailUtilities.viderTextField(myView.getTfFiltreLibelle());
		
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CategorieEmploiSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CategorieEmploiSelectCtrl(editingContext);
		return sharedInstance;
	}

	public EOCategorieEmploi getCurrentObject() {
		return currentObject;
	}
	public void setCurrentObject(EOCategorieEmploi currentObject) {
		this.currentObject = currentObject;
	}


	private EOQualifier getQualifier() {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		if (CocktailUtilities.getTextFromField(myView.getTfFiltreLibelle()) != null) {
			String filtre = "*"+CocktailUtilities.getTextFromField(myView.getTfFiltreLibelle())+"*";
			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCategorieEmploi.C_CATEGORIE_EMPLOI_KEY + " caseInsensitiveLike %@", new NSArray(filtre)));			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCategorieEmploi.LC_CATEGORIE_EMPLOI_KEY + " caseInsensitiveLike %@", new NSArray(filtre)));			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCategorieEmploi.LL_CATEGORIE_EMPLOI_KEY + " caseInsensitiveLike %@", new NSArray(filtre)));			
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
				
		return new EOAndQualifier(qualifiers);
		
	}
	
	/**
	 * 
	 */
	private void filter() {
		
		eod.setQualifier(getQualifier());
		eod.updateDisplayedObjects();
		
		myView.getMyEOTable().updateData();
		
	}
	

	/**
	 * 
	 * @return
	 */
	public EOCategorieEmploi getCategorie()	{
	
		eod.setObjectArray(EOCategorieEmploi.fetchAll(edc, EOCategorieEmploi.SORT_ARRAY_LIBELLE_ASC));		
		filter();
		
		myView.setVisible(true);

		return getCurrentObject();
	}

	public void annuler() {

		setCurrentObject(null);
		myView.dispose();

	}  

	private class ListenerObject implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentObject = (EOCategorieEmploi)eod.selectedObject();
		}
	}
	
	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.nomenclatures.ModelePageSelectNomenclature;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOMotifDepart;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class MotifDepartSelectCtrl  extends ModelePageSelectNomenclature {

	private static MotifDepartSelectCtrl sharedInstance;
	private MotifDepartSelectView myView;

	public MotifDepartSelectCtrl(EOEditingContext edc)	{

		super(edc);

		myView = new MotifDepartSelectView(new JFrame(), true, getEod());
		myView.setTitle("Motifs de départ");

		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		CocktailUtilities.initPopupOuiNon(myView.getPopupDepartDefinitif(), true);		
		setDocumentFiltreListener(myView.getTfFiltre());
		setListener(myView.getMyEOTable());

		myView.getPopupDepartDefinitif().addActionListener(new TypeDefinitifListener());
	}
	
	public static MotifDepartSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new MotifDepartSelectCtrl(editingContext);
		return sharedInstance;
	}
	
	/**
	 * 
	 * @param dateReference
	 * @return
	 */
	public INomenclature getObject(NSTimestamp dateReference)	{
		if (getEod().displayedObjects().count() == 0) {
			getEod().setObjectArray(EOMotifDepart.findMotifsValides(getEdc(), dateReference));
			myView.getMyEOTable().updateData();
		}
		myView.setVisible(true);
		return getCurrentObject();
	}

	protected class TypeDefinitifListener implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			filter();
		}
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}
	@Override
	protected void traitementsPourAnnulation() {
		myView.setVisible(false);
	}

	@Override
	protected EOQualifier getFilterQualifier() {
		// TODO Auto-generated method stub
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		if (myView.getPopupDepartDefinitif().getSelectedIndex() > 0) {
			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOMotifDepart.TEM_FIN_CARRIERE_KEY + " = %@", new NSArray(myView.getPopupDepartDefinitif().getSelectedItem())));			
		}
			
		if (!StringCtrl.chaineVide(getFilterValue()))	{
			
			NSArray<String> args = new NSArray<String>("*" + getFilterValue() + "*");
			
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + " caseInsensitiveLike %@",args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.LIBELLE_LONG_KEY + " caseInsensitiveLike %@",args));
			
			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(andQualifiers);       
	}

	@Override
	protected String getFilterValue() {
		// TODO Auto-generated method stub
		return CocktailUtilities.getTextFromField(myView.getTfFiltre());
	}

	@Override
	protected void traitementsApresFiltre() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().updateData();
	}


}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.select;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class SaisieStructureCtrl {


	private static SaisieStructureCtrl sharedInstance;
	private 	EOEditingContext		ec;
	private 	boolean 				nouvelleStructure;

	private EODisplayGroup eod;		
	private SaisieStructureView myView;
	private	EOStructure selectedStructure;
	private boolean saisieSiret;


	/**
	 * 
	 * @param globalEc
	 */
	public SaisieStructureCtrl (EOEditingContext globalEc)	{

		super();

		ec = globalEc;
		eod = new EODisplayGroup();
		myView = new SaisieStructureView(new JFrame(), true, eod);

		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}});
		myView.getBtnSelectionner().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectionner();}});
		myView.getBtnVerifIer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {verifierStructure();}});
		myView.getBtnCreer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {creer();}});

		myView.getMyEOTable().addListener(new ListenerStructure());
		eod.setSortOrderings(EOStructure.SORT_ARRAY_LIBELLE_ASC);

		//		myView.getTfLibelleLong().addActionListener(new ActionListenerLibelle());
		//		myView.getTfSiret().addActionListener(new ActionListenerSiret());

		myView.getTfLibelleCourt().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfLibelleLong().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfSiret().getDocument().addDocumentListener(new ADocumentListener());

		updateInterface();
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieStructureCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieStructureCtrl(editingContext);
		return sharedInstance;
	}
	public EOStructure selectedStructure() {
		return selectedStructure;
	}

	public void setSelectedStructure(EOStructure selectedStructure) {
		this.selectedStructure = selectedStructure;
	}

	public boolean isNouvelleStructure() {
		return nouvelleStructure;
	}
	public void setNouvelleStructure(boolean nouvelleStructure) {
		this.nouvelleStructure = nouvelleStructure;
	}

	/**
	 * 
	 * @return
	 */
	private boolean traitementsPourCreation() {

		if (myView.getTfLibelleLong().getText().length() == 0) {
			EODialogs.runInformationDialog("ERREUR", "Veuillez saisie un libellé long !");
			return false;
		}
		if (myView.getTfLibelleCourt().getText().length() == 0) {
			EODialogs.runInformationDialog("ERREUR", "Veuillez saisie un libellé court !");
			return false;
		}

		return true;
	}

	/**
	 * 
	 */
	public void creer()	{		

		// Contrôle de l'existence de la structure
		if (CocktailUtilities.getTextFromField(myView.getTfSiret()) != null) {
			EOStructure structureLibelle = EOStructure.rechercherStructureAvecLibelleEtType(ec, CocktailUtilities.getTextFromField(myView.getTfLibelleLong()), null);
			if (structureLibelle != null) {
				if(!EODialogs.runConfirmOperationDialog("Attention", "Cette structure existe déjà dans le référentiel.\nVoulez-vous continuer ?", "Oui", "Non")) {
					return;			
				}
			}
		}
		if (CocktailUtilities.getTextFromField(myView.getTfSiret()) != null) {
			NSArray<EOStructure> structuresSiret = EOStructure.rechercherStructuresAvecSiret(ec, CocktailUtilities.getTextFromField(myView.getTfSiret()));
			if (structuresSiret != null && structuresSiret.size() > 0) {
				if(!EODialogs.runConfirmOperationDialog("Attention", "Ce numéro SIRET est déjà utilisé.\nVoulez-vous continuer ?", "Oui", "Non")) {
					return;			
				}
			}
		}

		if (!traitementsPourCreation())
			return;
		setNouvelleStructure(true);


		myView.setVisible(false);

	}

	public void selectionner()	{

		setSelectedStructure((EOStructure)eod.selectedObject());		
		setNouvelleStructure(false);
		myView.setVisible(false);

	}

	/**
	 * 
	 */
	public void clearDatas() {
		CocktailUtilities.viderTextField(myView.getTfLibelleCourt());
		CocktailUtilities.viderTextField(myView.getTfLibelleLong());
		CocktailUtilities.viderTextField(myView.getTfSiret());
	}

	/**
	 * 
	 * @param agent
	 * @return
	 */
	public EOStructure getStructure(EOAgentPersonnel agent)	{		

		clearDatas();
		setSaisieSiret(false);
		myView.getBtnCreer().setEnabled(false);
		updateInterface();

		myView.setVisible(true);

		if (isNouvelleStructure()) {
			setSelectedStructure(EOStructure.creer(ec, null, agent, true));
			selectedStructure().setLlStructure(myView.getTfLibelleLong().getText());
			selectedStructure().setLcStructure(myView.getTfLibelleCourt().getText());
			selectedStructure().setSiret(myView.getTfSiret().getText());
		}

		return selectedStructure();

	}

	public boolean saisieSiret() {
		return saisieSiret;
	}
	public void setSaisieSiret(boolean saisieSiret) {
		this.saisieSiret = saisieSiret;
	}

	public void annuler() {
		setSelectedStructure(null);
		myView.dispose();
	}  	

	private class ListenerStructure implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {selectionner();}
		public void onSelectionChanged() {
			setSelectedStructure((EOStructure)eod.selectedObject());
			updateInterface();
		}
	}

	/**
	 * 
	 */
	private void verifierStructure() {		

		if (StringCtrl.chaineVide(myView.getTfLibelleLong().getText()) && CocktailUtilities.getTextFromField(myView.getTfSiret()) == null)	{
			EODialogs.runErrorDialog("ERREUR","Veuillez entrer tout ou partie du libellé ou le numéro SIRET !");
			return;
		}
		eod.setObjectArray(EOStructure.rechercherPourCreation(ec, myView.getTfLibelleLong().getText(), myView.getTfSiret().getText()));
		myView.getMyEOTable().updateData();
		myView.getBtnCreer().setEnabled(true);

	}



	/**
	 *
	 */
	private void updateInterface() {

		myView.getBtnSelectionner().setEnabled(selectedStructure() != null && myView.getBtnCreer().isEnabled());

	}

	public class ActionListenerLibelle implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			verifierStructure();
		}
	}
	public class ActionListenerSiret implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			verifierStructure();
		}
	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			updateInterface();
		}

		public void insertUpdate(DocumentEvent e) {
			updateInterface();		
		}

		public void removeUpdate(DocumentEvent e) {
			updateInterface();		
		}
	}

}

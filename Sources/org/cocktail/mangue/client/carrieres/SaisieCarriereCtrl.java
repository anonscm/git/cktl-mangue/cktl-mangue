// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.carrieres;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.carriere.SaisieCarriereView;
import org.cocktail.mangue.client.select.TypePopulationSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOStage;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class SaisieCarriereCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieCarriereCtrl sharedInstance;
	private SaisieCarriereView myView;

	private EOCarriere 			currentCarriere;
	private EOTypePopulation 	currentTypePopulation;

	private TypePopulationSelectCtrl myTypePopulationSelectCtrl;
	private boolean modeCreation;

	public SaisieCarriereCtrl(EOEditingContext edc) {

		super(edc);

		myView = new SaisieCarriereView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		myView.getBtnGetTypePopulation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectTypePopulation();}}
				);

		myView.setTypesRecrutement(EOTypeRecrutementHu.fetchAll(getEdc()));

		CocktailUtilities.initTextField(myView.getTfTypePopulation(), false, false);

	}

	public static SaisieCarriereCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieCarriereCtrl(editingContext);
		return sharedInstance;
	}


	private EOCarriere getCurrentCarriere() {
		return currentCarriere;
	}
	private void setCurrentCarriere(EOCarriere carriere) {
		currentCarriere = carriere;
		updateDatas();		
	}

	private EOTypePopulation getCurrentTypePopulation() {
		return currentTypePopulation;
	}
	private void setCurrentTypePopulation(EOTypePopulation typePopulation) {
		currentTypePopulation = typePopulation;
		myView.getTfTypePopulation().setText("");
		if (currentTypePopulation != null )
			CocktailUtilities.setTextToField(myView.getTfTypePopulation(), currentTypePopulation.libelleLong());
	}

	/**
	 * 
	 * @param individu
	 * @return
	 */
	public EOCarriere ajouter(EOIndividu individu)	{

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		setModeCreation(true);
		myView.setTitle("CARRIERE / AJOUT ( " + individu.identitePrenomFirst() + ")");

		setCurrentCarriere(EOCarriere.creer(getEdc(), individu));
		myView.getBtnGetTypePopulation().setVisible(true);

		myView.setVisible(true);

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentCarriere();
	}

	/**
	 * 
	 * @param carriere
	 * @return
	 */
	public boolean modifier(EOCarriere carriere) {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		setModeCreation(false);
		setCurrentCarriere(carriere);

		myView.setTitle("CARRIERE ( " + getCurrentCarriere().toIndividu().identitePrenomFirst() + ")");

		NSArray<EOElementCarriere> elements = EOElementCarriere.findForCarriere(getEdc(), carriere);
		myView.getBtnGetTypePopulation().setVisible(elements.count() == 0);

		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentCarriere() != null;
	}

	/**
	 * 
	 */
	public void selectTypePopulation() {

		if (myTypePopulationSelectCtrl == null)
			myTypePopulationSelectCtrl = new TypePopulationSelectCtrl(getEdc());

		EOTypePopulation typePopulation = myTypePopulationSelectCtrl.getTypePopulation();

		if (typePopulation != null) {

			setCurrentTypePopulation(typePopulation);
			if (currentTypePopulation.estHospitalier() == false) {
				getCurrentCarriere().setTypeRecrutementRelationship(null);
			}
		}

		updateInterface();

	}

	private boolean isDateFinModifiee() {
		return ! isModeCreation() && !DateCtrl.isSameDay(getCurrentCarriere().dateFin(), CocktailUtilities.getDateFromField(myView.getTfDateFin()));
	}
	private boolean isTypePopulationModifie() {
		return ! isModeCreation() &&  ! getCurrentTypePopulation().code().equals(getCurrentCarriere().toTypePopulation().code()); 
	}

	/**
	 * 
	 * Traitements a effectuer avant la validation de la carriere. 
	 * A de moment, les enregistrements ne sont pas effectues dans l'object carriere.
	 * 
	 * @return
	 */
	protected void traitementsAvantValidation() {

		// Les dates de debut et le type de population sont obligatoires

		if (CocktailUtilities.getDateFromField(myView.getTfDateDebut()) != null) {

			getCurrentCarriere().setDateDebut( CocktailUtilities.getDateFromField(myView.getTfDateDebut()));


			if (getCurrentTypePopulation() != null) {

				// Controle d'un contrat existant
				NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(getEdc(), getCurrentCarriere().toIndividu(), 
						CocktailUtilities.getDateFromField(myView.getTfDateDebut()), 
						CocktailUtilities.getDateFromField(myView.getTfDateFin()), false);
				for (EOContrat contrat : contrats) {
					if (contrat.toTypeContratTravail().estAutorisePourTitulaire() == false) {
						throw new NSValidation.ValidationException("Cet agent a déjà un contrat de travail du " + contrat.dateDebutFormatee() + " au " + contrat.dateFinFormatee() + " !");
					}
				}
				
				// 
				if (isTypePopulationModifie()) {
					if (EODialogs.runConfirmOperationDialog("ATTENTION","Le changement de type de population entraîne l'invalidation des éléments de carrière ?\nVoulez-vous continuer ?",  "Oui","Annuler")) {
						EOCarriere.invaliderElements(getEdc(), getCurrentCarriere());
					}
				}
				// faire correspondre les dates de fin des elements de carrière et des changements de position
				// si la date de fin a été modifiée et qu'elle était la même que celle du segment de carrière
				// pour  et les stages uniquement si la date de fin est antérieure
				if (isDateFinModifiee()) {
					if (EODialogs.runConfirmOperationDialog("ATTENTION","La date de fin a été modifiée.\nLes dates de fin des éléments de carrière/changements de position et stages seront peut-être modifiées ?\nVoulez-vous continuer ?","Oui","Non")) {
						getCurrentCarriere().modifierDateFinAutresInfos(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
					}
				}

				getCurrentCarriere().setToTypePopulationRelationship(getCurrentTypePopulation());
				getCurrentCarriere().setDateFin( CocktailUtilities.getDateFromField(myView.getTfDateFin()));

				getCurrentCarriere().setTypeRecrutementRelationship(getTypeRecrutement());
				getCurrentCarriere().setEstPriseEnChargeCir(myView.getCheckCir().isSelected());

				if (getCurrentTypePopulation().estHospitalier()) 
					getCurrentCarriere().setTypeRecrutementRelationship(getTypeRecrutement());

				NSArray<EOCarriere> carrieres = EOSortOrdering.sortedArrayUsingKeyOrderArray(CarrieresCtrl.sharedInstance(getEdc()).getCarrieres(), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
				EOCarriere segmentPrecedent = null;
				EOCarriere segmentSuivant = null;

				for (EOCarriere carriere : carrieres) {
					if (!carriere.equals(getCurrentCarriere())) {
						if (DateCtrl.isBefore(carriere.dateDebut(), getCurrentCarriere().dateDebut()))
							segmentPrecedent = carriere;
						else {
							segmentSuivant = carriere;
							break;
						}
					}
				}

				if (segmentPrecedent != null) {
					if ( segmentPrecedent.toTypePopulation().code().equals(getCurrentTypePopulation().code()))
						EODialogs.runInformationDialog("Attention", "Il n'est pas recommandé de définir un nouveau segment de carrière avec le même type de population !");

					//	vérifier si la date de fin est nulle que la date de début est bien la dernière (i.e postérieure à la date de début du dernier segment)
					if (CocktailUtilities.getDateFromField(myView.getTfDateFin()) == null && DateCtrl.isBefore(getCurrentCarriere().dateDebut(),segmentPrecedent.dateDebut()))
						throw new NSValidation.ValidationException("Vous n'avez pas fourni de date de fin, or il existe un autre segment de carrière commençant après cette date");

					// clore le segment précédent si nécessaire
					if (segmentPrecedent.dateFin() == null || DateCtrl.isAfterEq(segmentPrecedent.dateFin(), getCurrentCarriere().dateDebut())) {
						String message = "Le segment de carrière commençant le " + segmentPrecedent.dateDebut();
						if (segmentPrecedent.dateFin() == null) {
							message = message + " n'a pas de date de fin.";
						} else {
							message = message + " se termine le " + segmentPrecedent.dateFin() + ".";
						}
						message = message + " Sa date de fin va être modifiée ainsi éventuellement que celle des éléments de carrière, changements de position et stages.\nEst-ce bien ce que vous voulez ?";
						if (EODialogs.runConfirmOperationDialog("Attention",message,"OK","Annuler")) {
							if (segmentPrecedent.peutFermerADate(getCurrentCarriere().dateDebut()) == false) {
								throw new NSValidation.ValidationException("Le segment précédent commence après ou a des éléments/changements de position ou stages commençant après le " + getCurrentCarriere().dateDebut() + ", la modification est impossible");
							}
							segmentPrecedent.fermerCarriereADate(DateCtrl.jourPrecedent(getCurrentCarriere().dateDebut()));
						}
					}
				}
				else {
					if (segmentSuivant != null) {
						if (segmentSuivant.toTypePopulation().code().equals(getCurrentTypePopulation().code()))
							EODialogs.runInformationDialog("Attention", "Il n'est pas recommandé de définir un nouveau segment de carrière avec le même type de population !");				
					}
				}


				// La date de debut de la premiere periode de stage doit correspondre à la date de debut de la carriere
				NSArray<EOStage> stages = EOStage.findForCarriere(getEdc(), getCurrentCarriere());
				if (stages.size() > 0) {
					EOStage premierStage = stages.get(0);
					premierStage.setDateDebut(getCurrentCarriere().dateDebut());
				}
			}
		}
	}

	private EOTypeRecrutementHu getTypeRecrutement() {
		if (myView.getPopupTypeRecrutement().getSelectedIndex()==0)
			return null;
		return (EOTypeRecrutementHu)myView.getPopupTypeRecrutement().getSelectedItem();
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		setCurrentTypePopulation(null);
		myView.getPopupTypeRecrutement().setSelectedIndex(0);
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentCarriere() != null) {
			myView.getPopupTypeRecrutement().setSelectedItem(getCurrentCarriere().typeRecrutement());
			setCurrentTypePopulation(getCurrentCarriere().toTypePopulation());
			myView.getCheckCir().setSelected(getCurrentCarriere().estPriseEnChargeCir());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentCarriere().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentCarriere().dateFin());
		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnGetTypePopulation().setEnabled(CocktailUtilities.getDateFromField(myView.getTfDateDebut()) != null);
		myView.getViewTypeRecrutement().setVisible(getCurrentTypePopulation() != null && getCurrentTypePopulation().estHospitalier());

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		getEdc().revert();
		myView.setVisible(false);

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}


}

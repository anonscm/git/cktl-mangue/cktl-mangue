package org.cocktail.mangue.client.carrieres;

import java.awt.CardLayout;

import org.cocktail.mangue.client.gui.carriere.AncienneteView;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;

public class AncienneteCtrl {

	private static final String VIEW_CONSERVATION = "CONSERVATION";
	private static final String VIEW_REDUCTION = "REDUCTION";
	private static AncienneteCtrl sharedInstance;
	private EOEditingContext edc;
	private AncienneteView myView;
	
	public AncienneteCtrl(EOEditingContext editingContext) {

		setEdc(editingContext);
				
		myView = new AncienneteView(null, true);
		myView.getViewReduction().add(VIEW_REDUCTION,AncienneteReductionCtrl.sharedInstance(edc).getView());
		myView.getViewConservation().add(VIEW_CONSERVATION,AncienneteConservationCtrl.sharedInstance(edc).getView());

		((CardLayout)myView.getViewReduction().getLayout()).show(myView.getViewReduction(), VIEW_REDUCTION);				
		((CardLayout)myView.getViewConservation().getLayout()).show(myView.getViewConservation(), VIEW_CONSERVATION);				
	}

	public static AncienneteCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new AncienneteCtrl(editingContext);
		return sharedInstance;
	}
	
	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public void open(EOElementCarriere element)	{
		AncienneteReductionCtrl.sharedInstance(getEdc()).actualiser(element);
		AncienneteConservationCtrl.sharedInstance(getEdc()).actualiser(element);		
		myView.setVisible(true);
	}
	
	public void lock(boolean yn) {
		if (yn)
			myView.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		else
			myView.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
			
		myView.getjButton1().setEnabled(!yn);
	}
	
	public void toFront() {
		myView.toFront();
	}
}

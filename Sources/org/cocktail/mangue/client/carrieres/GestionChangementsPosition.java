/*
 * Created on 19 oct. 2005
 *
 * Gestion des changements de position
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.carrieres;

import java.awt.Font;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.client.outils_interface.GestionEvenementAvecCarriere;
import org.cocktail.mangue.client.select.PaysSelectCtrl;
import org.cocktail.mangue.client.select.PositionSelectCtrl;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOMotifPosition;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartEnfant;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodesMilitaires;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/** Gestion des changements de position pour un segment de carriere
 *	Les changements de position, en dehors de la position "En activite dans l'Etablissement ont une absence associee dans la
 *	table des Absences
 *	On ne peut pas supprimer un changement de position si il existe des occupations ou affectations pour cette periode<BR>
 *	On ne peut supprimer que le dernier changement de position<BR>
 *	Pour les changements de position qui ne sont pas en activite, on ferme les affectations et les occupations au jour
 *  precedent le changement de position et on le signale a l'utilisateur.<BR>
 */
public class GestionChangementsPosition extends GestionEvenementAvecCarriere {

	public EOView swapViewRneOrigine,vueRneOrig;
	public JLabel labelEnfant;

	public	JCheckBox checkPcAcquitee;

	private UAISelectCtrl myUAISelectCtrl;
	private UAISelectCtrl myUAIOrigineSelectCtrl;
	private PaysSelectCtrl myPaysSelectCtrl;
	private PositionSelectCtrl myPositionSelectCtrl;

	//public JCheckBox checkPcAcquitee;

	/** Notification envoyee pour signaler que le changement de position est modifie */
	public static String CHANGER_POSITION = "NotifChangementPositionChanged";

	// Constructeur
	public GestionChangementsPosition(EOGlobalID carriereID) {
		super(carriereID);
	}
	// Accesseurs
	public String codePays() {
		if (currentChangement() == null || currentChangement().pays() == null) {
			return null;
		} else {
			return currentChangement().pays().code();
		}
	}
	public void setCodePays(String codePays) {
		if (codePays == null) {
			if (currentChangement().pays() != null) {
				currentChangement().removeObjectFromBothSidesOfRelationshipWithKey(currentChangement().pays(),EOChangementPosition.PAYS_KEY);
			}
		} else {
			EOPays pays = (EOPays)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"Pays","code",codePays);
			if (pays != null && EOPays.estPaysValideADate(editingContext(), pays.code(), currentChangement().dateDebut()) == false) {
				pays = null;
			}
			currentChangement().setPaysRelationship(pays);
		}
		updaterDisplayGroups();
	}


	//	 méthodes du délégué du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {

		super.displayGroupDidChangeSelection(aGroup);
		if (aGroup == displayGroup()) {
			gererAffichageVueRneOrig();
			changerFontLabelEnfant();
		}

		checkPcAcquitee.setSelected(currentChangement() != null && currentChangement().estPcAcquitee());

	}

	// Actions

	/** affiche les motifs correspondant a la position */
	public void afficherMotif() {
		NSArray args = new NSArray(currentChangement().toPosition().code());	// ne peut être nul sinon action non déclenchable
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOMotifPosition.TO_POSITION_KEY + "." + EOPosition.CODE_KEY + " = %@",args);
		UtilitairesDialogue.afficherDialogue(this,EOMotifPosition.ENTITY_NAME,"getMotif",false,qualifier,true);
	}

	/**
	 * 
	 */
	public void afficherPosition() {

		EOQualifier qualifier = null;
		if (currentCarriere().toTypePopulation().estFonctionnaire() == false)
			qualifier = EOQualifier.qualifierWithQualifierFormat(EOPosition.TEM_TITULAIRE_KEY + " = 'N'",null);

		if (myPositionSelectCtrl == null)
			myPositionSelectCtrl = new PositionSelectCtrl(editingContext());
		EOPosition position = myPositionSelectCtrl.getPosition(qualifier);

		if (position != null) {

			currentChangement().setToPositionRelationship(position);

			if (position.estUneDispo() || position.estUnDetachement())
				currentChangement().setEstPcAcquitee(false);
			else
				currentChangement().setEstPcAcquitee(true);

			checkPcAcquitee.setSelected(currentChangement().estPcAcquitee());

			gererAffichageVueRneOrig();

			if (currentChangement().toPosition().estUnDetachement() == false) {
				currentChangement().setToRneOrigineRelationship(null);
				currentChangement().setLieuPositionOrig(null);
				currentChangement().setTxtPositionOrig(null);
			} 
			currentChangement().setToMotifPositionRelationship(null);

			if (currentChangement().toPosition().requiertEnfant() == false) {
				currentChangement().setEnfantRelationship(null);
			}

			changerFontLabelEnfant();
			updaterDisplayGroups();
		}
	}

	public void afficherRne() {

		try {
			if (myUAISelectCtrl == null)
				myUAISelectCtrl = new UAISelectCtrl(editingContext());
			EORne rne = (EORne)myUAISelectCtrl.getObject();
			if (rne != null) {
				currentChangement().setToRneRelationship(rne);
				ajouterRelation(currentChangement(),editingContext().globalIDForObject(rne),"toRne");
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public void afficherRneOrig() {

		if (myUAIOrigineSelectCtrl == null)
			myUAIOrigineSelectCtrl = new UAISelectCtrl(editingContext());

		EORne rneOrigine = (EORne)myUAIOrigineSelectCtrl.getObject();
		if (rneOrigine != null) {
			currentChangement().setToRneOrigineRelationship(rneOrigine);
			ajouterRelation(currentChangement(),editingContext().globalIDForObject(rneOrigine),EOChangementPosition.TO_RNE_ORIGINE_KEY);
		}
	}

	/**
	 * 
	 */
	public void afficherEnfant() {
		NSMutableArray args = new NSMutableArray(currentCarriere().toIndividu());
		String stringQualifier = EORepartEnfant.PARENT_KEY + " = %@";
		stringQualifier = stringQualifier + " AND enfant.temValide = 'O'";

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		UtilitairesDialogue.afficherDialogue(this,EORepartEnfant.ENTITY_NAME,"getEnfant",false,qualifier,true);
	}
	public void afficherPays() {

		if (myPaysSelectCtrl == null)
			myPaysSelectCtrl = new PaysSelectCtrl(editingContext());

		EOPays pays = myPaysSelectCtrl.getPays(null, currentChangement().dateDebut());
		if (pays != null) {
			currentChangement().setPaysRelationship(pays);
		}
	}

	// SUPPRIMER RNE
	public void supprimerRne() {
		currentChangement().setToRneRelationship(null);
	}
	// SUPPRIMER RNE ORIG
	public void supprimerRneOrig() {
		currentChangement().setToRneOrigineRelationship(null);
	}
	// SUPPRIMER PAYS
	public void supprimerPays() {
		currentChangement().setPaysRelationship(null);
	}
	// SUPPRIMER MOTIF
	public void supprimerMotif() {
		currentChangement().setToMotifPositionRelationship(null);
		// 01/03/2011 - supprimer l'enfant si la position ne justifie pas d'avoir un enfant
		if (currentChangement().enfant() != null && currentChangement().toPosition().requiertEnfant() == false) {
			currentChangement().removeObjectFromBothSidesOfRelationshipWithKey(currentChangement().enfant(),EOChangementPosition.ENFANT_KEY);
		}
		updaterDisplayGroups();
	}

	/**
	 * Selection du motif de position
	 * @param aNotif
	 */
	public void getMotif(NSNotification aNotif) {	
		ajouterRelation(currentChangement(), aNotif.object(),EOChangementPosition.TO_MOTIF_POSITION_KEY);
		if (currentChangement() != null && currentChangement().toMotifPosition() != null 
				&& !currentChangement().toMotifPosition().requiertEnfant() )
			currentChangement().setEnfantRelationship(null);
	}

	/**
	 * Selection de l'enfant associe a la position
	 * @param aNotif
	 */
	public void getEnfant(NSNotification aNotif) {
		if (aNotif.object() != null) {
			EORepartEnfant repart = (EORepartEnfant)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(), editingContext());
			if (currentChangement() != null && repart != null) {
				currentChangement().setEnfantRelationship(repart.enfant());
			}
		}
	}

	public boolean peutValider() {
		return modificationEnCours();
	}
	/** true si derniere position */
	public boolean peutSupprimer() {
		return boutonModificationAutorise();
	}
	public boolean peutSelectionnerMotif() {
		return modificationEnCours() && currentChangement().toPosition() != null;
	}
	public boolean peutSupprimerMotif() {
		return modificationEnCours() && currentChangement().toMotifPosition() != null;
	}
	/** On ne peut saisir un pays que si la date de debut de position est saisie */
	public boolean peutSelectionnerPays() {
		return modificationEnCours() && currentChangement().dateDebut() != null && currentChangement().toRne() == null;
	}
	public boolean peutSupprimerRne() {
		return modificationEnCours() && currentChangement().toRne() != null;
	}
	public boolean peutSupprimerRneOrig() {
		return modificationEnCours() && currentChangement().toRneOrigine() != null;
	}
	public boolean peutSupprimerPays() {
		return modificationEnCours() && currentChangement().pays() != null;
	}
	public boolean peutSelectionnerEnfant() {
		return modificationEnCours() && 
				((currentChangement().toPosition() != null && currentChangement().toPosition().requiertEnfant()) ||
						(currentChangement().toMotifPosition() != null && currentChangement().toMotifPosition().requiertEnfant()));
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		if(listeAffichage != null) {
			GraphicUtilities.changerTaillePolice(listeAffichage, 11);
			listeAffichage.table().getSelectionModel().setSelectionMode(0);
			GraphicUtilities.rendreNonEditable(listeAffichage);
		}
		GraphicUtilities.preparerInterface(new NSArray(vueRneOrig));
	}

	/**
	 * 
	 */
	protected void traitementsPourCreation() {
		currentChangement().initAvecCarriere(currentCarriere());
		if (displayGroup().allObjects().count() > 0) {
			currentChangement().setTemoinPositionPrev(CocktailConstantes.VRAI);
		}

		String rneParDefaut = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE);
		currentChangement().setToRneRelationship((EORne)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),EORne.ENTITY_NAME, EORne.CODE_KEY,rneParDefaut));

		gererAffichageVueRneOrig();
	}

	protected NSArray fetcherObjets() {
		try {
			return currentCarriere().changementsPosition();
		}
		catch (Exception e ){
			return new NSArray();
		}
	}
	/** Affichage des changements de position valides tries par ordre de date debut decroissant */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(EOChangementPosition.SORT_ARRAY_DATE_DEBUT_DESC);
		displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat(EOChangementPosition.TEM_VALIDE_KEY + " = 'O'", null));
	}

	/**
	 * 
	 */
	protected boolean traitementsAvantValidation() {

		currentChangement().setEstPcAcquitee(checkPcAcquitee.isSelected());			

		if (currentChangement().dateDebut() != null) {

			// trouver le changement de position précédent
			EOChangementPosition lastChangementPosition = null;
			boolean takeNext = false;
			for (java.util.Enumeration <EOChangementPosition>e = displayGroup().displayedObjects().objectEnumerator();e.hasMoreElements();) {
				EOChangementPosition position = e.nextElement();
				if (!takeNext) {
					if (position == currentChangement()) {
						takeNext = true;
					}
				} else {
					lastChangementPosition = position;
					break;
				}
			}
			if (lastChangementPosition != null) {
				//	vérifier si la date de fin est nulle que la date de début est bien la dernière
				if (currentChangement().dateFin() == null && DateCtrl.isBefore(currentChangement().dateDebut(),lastChangementPosition.dateDebut())) {
					EODialogs.runErrorDialog("ERREUR","Vous n'avez pas fourni de date de fin, or il existe une autre position commençant après cette position");
					return false;
				}
				// clore la position précédente si nécessaire et si sa quotité est 100%
				if (lastChangementPosition.dateFin() == null && lastChangementPosition.quotite().floatValue() == 100) {
					lastChangementPosition.setDateFin(DateCtrl.jourPrecedent(currentChangement().dateDebut()));	// pas nul car sinon pas de validation
				}
			}

			if (modeCreation()) {
				if (currentChangement().toPosition().estEnActiviteDansEtablissement() == false) {
					EOAbsences absence = creerAbsenceSiNecessaire();
					currentChangement().setAbsenceRelationship(absence);
				} 
			} else {
				// En mode modification, vérifier si un événement associé est nécessaire
				if (currentChangement().toPosition().estEnActivite() && !currentChangement().estDetachement()) {
					super.traitementsPourSuppression();
				} else {
					// C'est le cas, le créer si il n'existe pas ou modifier son type si ce n'est pas le même type
					if (currentChangement().toPosition().doitGenererEvenement()) {
						EOAbsences absence = modifierOuCreerEvenement();
						if (absence == null)
							return false;
						currentChangement().setAbsenceRelationship(absence);
					}
				}
			}

			boolean estEnActivite = 
					currentChangement().toPosition().estEnActiviteDansEtablissement()
					|| (currentChangement().toPosition().estUnDetachement() && currentChangement().estDetachementSortant() == false);

			if (!estEnActivite) {

				NSTimestamp jourFin = DateCtrl.jourPrecedent(currentChangement().dateDebut());

				// L agent a t il des periodes d activite par la suite
				NSArray positions = EOChangementPosition.rechercherChangementsActivitePourPeriode(editingContext(), currentChangement().individu(), jourFin, null);
				if (positions.count() == 0) {

					// Rechercher les occupations et les affectations valides au jour précédent
					NSArray affectations = EOAffectation.findForIndividu(editingContext(), currentChangement().individu(), jourFin);
					NSArray occupations = EOOccupation.findForIndividuAndDate(editingContext(), currentChangement().individu(), jourFin);
					String message = "";
					if (affectations.count() > 0) {
						message += "Les affectations ";
					}
					if (occupations.count() > 0) {
						if (message.length() == 0) {
							message += "Les occupations";
						} else {
							message += "et les occupations";
						}
					}
					if (message.length() > 0) {
						message += " seront fermées. Souhaitez-vous vraiment les fermer ?";
						if (EODialogs.runConfirmOperationDialog("Attention", "Cette position n'est pas en activité.\n" + message, "NON", "OUI")) {
							if (affectations.count() > 0) {
								try {
									EOAffectation.fermerAffectations(editingContext(), currentChangement().individu(), jourFin);
								} catch (Exception exc) {}
							}
							if (occupations.count() > 0) {
								EOOccupation.fermerOccupations(editingContext(), currentChangement().individu(), jourFin);
							}
						}
					}
				}
				else {
					//EODialogs.runInformationDialog("ATTENTION", "Pensez éventuellement à revoir les occupations et affectations pour la période !");
				}
			}


			if (currentChangement().toPosition().estServiceNational()) {
				// Vérifier qu'il y a cohérence avec les périodes de service national
				NSArray periodesMil = EOPeriodesMilitaires.findForIndividu(editingContext(), currentCarriere().toIndividu());
				boolean periodeTrouvee = false;
				if (periodesMil != null && periodesMil.count() > 0) {
					for (java.util.Enumeration<EOPeriodesMilitaires> e1= periodesMil.objectEnumerator();e1.hasMoreElements();) {
						EOPeriodesMilitaires periode = e1.nextElement();
						if (DateCtrl.isSameDay(currentChangement().dateDebut(), periode.dateDebut()) &&
								(currentChangement().dateFin() == null ||
								(currentChangement().dateFin() != null && DateCtrl.isSameDay(currentChangement().dateFin(), periode.dateFin())))) {
							periodeTrouvee = true;
							break;
						}
					}
				}
				if (!periodeTrouvee) {
					EODialogs.runInformationDialog("Attention", "Cette période n'est pas déclarée dans les périodes militaires");
				}
			}
		}
		super.traitementsAvantValidation();
		return true;
	}


	public boolean peutModifierPcAcquitee() {
		return modificationEnCours() && currentChangement() != null 
				&& ( currentChangement().toPosition().estUnDetachement() || currentChangement().toPosition().estUneDispo()) ;
	}
	public boolean peutModifierQuotite() {
		return modificationEnCours();
	}


	/**
	 * Gestion des absences et du temoin de pension civile
	 */
	protected void traitementsApresValidation() {

		super.traitementsApresValidation();

		try {

			INomenclature typeAbsence = null;

			if (currentChangement().toPosition().estUnDetachement()) {
				typeAbsence = NomenclatureFinder.findForCode(editingContext(), EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_DETACHEMENT);
			}
			if (currentChangement().toPosition().estUneDispo()) {
				typeAbsence = NomenclatureFinder.findForCode(editingContext(),  EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_DISPONIBILITE);
				currentChangement().setEstPcAcquitee(false);
			}
			if (currentChangement().toPosition().estServiceNational()) {
				typeAbsence = NomenclatureFinder.findForCode(editingContext(),  EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_SERVICE_NATIONAL);
				currentChangement().setEstPcAcquitee(false);
			}
			if (currentChangement().toPosition().estCongeParental()) {
				typeAbsence = NomenclatureFinder.findForCode(editingContext(),  EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_PARENTAL);
				currentChangement().setEstPcAcquitee(false);
			}
			if (typeAbsence != null) {

				if (currentChangement().absence() == null)
					currentChangement().setAbsenceRelationship(EOAbsences.creer(editingContext(), currentChangement().carriere().toIndividu(), (EOTypeAbsence)typeAbsence));

				currentChangement().absence().setToTypeAbsenceRelationship((EOTypeAbsence)typeAbsence);
				currentChangement().absence().setDateDebut(currentChangement().dateDebut());
				currentChangement().absence().setDateFin(currentChangement().dateFin());
				currentChangement().absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(currentChangement().dateDebut(), currentChangement().dateFin(), true)));

			}

			editingContext().saveChanges();
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		NSNotificationCenter.defaultCenter().postNotification(CHANGER_POSITION,null);		
	}


	/**
	 * 
	 */
	protected boolean traitementsPourSuppression() {
		if (super.traitementsPourSuppression()) {
			if (currentChangement().sansOccupationOuAffectation() == false) {
				// DT 60438 - on permet la modification de l'historique même si il y a des occupations ou des 
				// affectations
				EODialogs.runInformationDialog("Attention", "Pensez à revoir les affectations et occupations");
			}
			currentChangement().invalider();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 */
	protected void traitementsApresSuppression() {

		super.traitementsApresSuppression();
		fetcherObjets();
		NSNotificationCenter.defaultCenter().postNotification(CHANGER_POSITION,null);

	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce changement de position ?";
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOChangementPosition currentChangement() {
		return (EOChangementPosition)displayGroup().selectedObject();
	}
	private void gererAffichageVueRneOrig() {

		if (currentCarriere() != null && currentChangement() != null && currentChangement().toPosition() != null) {
			if (currentChangement().toPosition().estUnDetachement()) {
				GraphicUtilities.swaperView(swapViewRneOrigine,vueRneOrig);
			} else {
				GraphicUtilities.swaperView(swapViewRneOrigine,null);
			}
		}
	}
	private void changerFontLabelEnfant() {
		int fontStyle = Font.PLAIN;
		if (currentChangement() != null && currentChangement().toPosition() != null && currentChangement().toPosition().requiertEnfant()) {
			fontStyle = Font.BOLD;
		}
		Font font = labelEnfant.getFont();
		labelEnfant.setFont(new Font(font.getName(),fontStyle,font.getSize()));

	}
}

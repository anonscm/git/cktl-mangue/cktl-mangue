
//GestionStages.java
//Mangue

//Created by Christine Buttin on Fri Sep 02 2005.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.


/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.carrieres;

import org.cocktail.mangue.client.outils_interface.GestionEvenementAvecCarriere;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOStage;
import org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/** Gestion des stages d'un segment de carriere<BR>
 *
 * Un stage peut etre cree ou renouvelle<BR>
 * En cas de creation, l'utilisateur commence par saisir la date de debut de stage. 
 * Le corps est automatiquement ajoute si il existe un element de carrie valide 
 * a cette date, sinon un message d'erreur est affiche.<BR>
 * Si la date de debut ne correspond pas a la date d'effet du plus premier element de carriere pour
 * ce stage, celle-ci est automatiquement modifiee.<BR>
 * Pour le renouvellement, l'utilisateur clique sur un bouton qui met a jour automatiquement la date de debut, le corps et le temoin
 * de renouvellement. L'utilisateur n'a plus qu'a saisir la date de fin.<BR>
 * On ne peut renouveler un stage que si sa date fin est definie.<BR>
 * On ne peut definir un stage que si un changement de position est defini pour le segment de carriere.<BR>
 * On affiche un avertissement, si pendant la periode de stage, l'agent a une bonification indiciaire.<BR>
 * On affiche un avertissement, si l'agent suit plus de deux stages pour un meme corps.<BR>
 * On ne peut pas supprimer un stage si il est suivi par une periode de renouvellement de stage<BR>
 * On ne peut pas supprimer une periode de stage si cette suppression remet en cause l'arrete lie a ce stage 
 * dans le cas ou l'arrete de l'element de carriere a ete signe par l'etablissement
 * Les autres regles de validation des stages sont decrites dans la classe EOStage<BR>
 * @author christine
 */
// 13/01/2011 - Adaptation Netbeans (suppression de la méthode preparerFenetre)
public class GestionStages extends GestionEvenementAvecCarriere {

	private boolean existeArrete;	// true si un des éléments de carrière pour ce corps a un arrêté

	public GestionStages(EOGlobalID carriereID) {
		super(carriereID);
	}
	// méthodes de délégation du display group
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("dateDebutFormatee") && currentStage() != null && currentStage().estUnRenouvellement() == false) {
			evaluerCorpsEtDateDebut();
		}
		controllerDisplayGroup().redisplay();
	}
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		if (currentStage() == null || currentStage().estUnRenouvellement()) {	// || currentStage().corps() != null 
			//peutModifierDateDebut = false;
			controllerDisplayGroup().redisplay();
		}
		preparerPourArrete();
		super.displayGroupDidChangeSelection(group);
	} 
	
	// actions
	public void renouveler() {
		EOStage stage = currentStage();
		insertObject();
		currentStage().initAvecCarriereCorpsEtDate(stage.carriere(),stage.corps(),DateCtrl.jourSuivant(stage.dateFin()));
		creerAbsence();
		super.modifier();
	}
	// méthodes du controller DG
	/** retourne true si on peut modifier la date de debut de stage */
	public boolean peutModifierDateDebut() {
		return peutModifierDate();// && peutModifierDateDebut;
	}
	/** retourne true si le stage n'est pas renouvelle */
	public boolean peutModifierDate() {

		return modificationEnCours() ;//&& !currentStage().estRenouvele();
		
	}
	/** return true si un stage est selectionne, qu'il a une date fin et pas de date de titularisation */
	public boolean peutRenouveler() {
		return boutonModificationAutorise() && modificationEnCours() == false && currentStage().estRenouvele() == false &&
		currentStage().dateFin() != null && currentStage().dateTitularisation() == null;
	}
	public boolean peutValider() {
		if (currentStage() == null || currentStage().dateDebut() == null || currentStage().corps() == null)
			return false;

		if (currentStage().dateFin() != null && DateCtrl.isBefore(currentStage().dateFin(),currentStage().dateDebut()))
			return false;

		return super.peutValider();

	}
	public boolean peutSupprimer() {
		return currentStage() != null && !currentStage().estRenouvele() && !existeArrete;
	}

	// méthodes protegees
	/** methode a surcharger pour retourner les objets affiches par le display group */
	protected NSArray fetcherObjets() {		
		return EOStage.findForCarriere(editingContext(), currentCarriere());
	}
	
	/** methode a surcharger pour parametrer le display group (tri ou filtre)
	 * stages valides tries par date debut decroissante */
	protected void parametrerDisplayGroup() {
		displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat( EOStage.TEM_VALIDE_KEY + " = 'O'", null));
		displayGroup().setSortOrderings(EOStage.SORT_ARRAY_DATE_DEBUT_ASC);
	}

	/** traitements a executer suite a la creation d'un objet */
	protected void traitementsPourCreation() {
		
		currentStage().initAvecCarriere(currentCarriere());
		currentStage().setAbsenceRelationship(creerAbsence());

		if (displayGroup().displayedObjects().count() == 1) {
			currentStage().setDateDebut(currentCarriere().dateDebut());
			evaluerCorpsEtDateDebut();
		}
		
		currentStage().setTemRenouvellement(CocktailConstantes.FAUX);

		existeArrete = false;
		
	}
	
	/** 05/03/08 - Lors d'une suppression les stages sont invalides */
	protected boolean traitementsPourSuppression() {

		if (super.traitementsPourSuppression()) {
			currentStage().setEstValide(false);
			return true;
		}
		return false;

	}
	/** retourne le message de confirmation aupr&egrave;s de l'utilisateur avant d'effectuer une suppression  */
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce stage ?";
	}
	
	/** methode a surcharger pour faire des traitements avant l'enregistrement des donnees dans la base
	 * retourne true si on peut enregistrer */
	protected boolean traitementsAvantValidation() {

		if (displayGroup().displayedObjects().count() == 1)
			currentStage().setDateDebut(currentStage().carriere().dateDebut());
		
		NSArray nbis = EONbiOccupation.rechercherNbiOccupationsPourIndividuEtPeriode(editingContext(),currentCarriere().toIndividu(),currentStage().dateDebut(),currentStage().dateFin());
		if (nbis.count() > 0) {
			if (EODialogs.runConfirmOperationDialog("Attention","L'agent bénéficie d'une nbi pendant la période de stage.\nVoulez-vous vraiment enregistrer les données ?","Oui","Non"))
				return super.traitementsAvantValidation();
			else
				return false;
		} else
			return super.traitementsAvantValidation();
	}
	/** affiche un message si il y a plus de 2 stages pour un meme corps */
	protected void traitementsApresValidation() {
		
		super.traitementsApresValidation();
				
		NSArray args = new NSArray(currentStage().corps());
		NSArray stages = EOQualifier.filteredArrayWithQualifier(displayGroup().displayedObjects(),EOQualifier.qualifierWithQualifierFormat("corps = %@",args));
		if (stages.count() > 2)
			EODialogs.runInformationDialog("Attention","Normalement un agent peut suivre au maximum deux stages pour un même corps");

	}
	
	protected void terminer() {
	}
	
	// methodes privees
	private EOStage currentStage() {
		return (EOStage)displayGroup().selectedObject();
	}

	
	/**
	 * 
	 */
	private void evaluerCorpsEtDateDebut() {

		if (currentStage() == null || currentCarriere() == null)
			return;

		EOElementCarriere elementTrouve = null;
		NSArray elements = currentCarriere().elementsPourPeriode(currentStage().dateDebut(),currentStage().dateFin());
		if (elements == null || elements.count() == 0)
			EODialogs.runErrorDialog("Alerte","Pas de corps valable pour cette date !");
		else {
			// on range les éléments du plus petit au plus grand pour pouvoir trouver la date d'effet minimum
			elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
			for (java.util.Enumeration<EOElementCarriere> e = elements.objectEnumerator();e.hasMoreElements();) {
				
				EOElementCarriere element = e.nextElement();
				if (element.estProvisoire() == false && element.estAnnule() == false) {
					elementTrouve = element;
					break;
				}
			}
			if (currentStage().corps() == null) {
				// si pas de corps défini et un seul corps l'ajouter automatiquement au stage
				if (elementTrouve == null) {
					EODialogs.runErrorDialog("Alerte","Pas de corps trouvé pour cette date (les éléments de carrière sont provisoires ou annulés)");
				} else {					
					currentStage().setCorpsRelationship(elementTrouve.toCorps());
				}
			}
		}
	}
	private void preparerPourArrete() {
		existeArrete = false;
		if (currentStage() != null && currentStage().corps() != null) {
			NSArray elements = currentStage().carriere().elementsValidesPourCorps(currentStage().corps());
			java.util.Enumeration<EOElementCarriere> e = elements.objectEnumerator();
			while (e.hasMoreElements()) {
				EOElementCarriere element = e.nextElement();
				if (element.estGereParEtablissement()) {
					existeArrete = true;
					break;
				}
			}
		}
	}
	
}

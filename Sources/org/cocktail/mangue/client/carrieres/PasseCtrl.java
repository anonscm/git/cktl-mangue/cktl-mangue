package org.cocktail.mangue.client.carrieres;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.carriere.PasseView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeService;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOMotifPosition;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class PasseCtrl extends ModelePageGestion {

	private static PasseCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private PasseView myView;

	private ListenerPasse listenerPasse = new ListenerPasse();

	private EODisplayGroup 			eod;
	private EOPasse 				currentPasse;
	private EOChangementPosition 	currentChangementPosition;
	private EOIndividu 				currentIndividu;

	private ListenerPopupTypeFonctionPublique 	listenerFonctionPublique = new ListenerPopupTypeFonctionPublique();
	private ListenerPopupTypeService 			listenerTypeService = new ListenerPopupTypeService();
	private ListenerPopupPosition 				listenerPosition = new ListenerPopupPosition();
	private ListenerTempsTravail 				listenerTempsTravail = new ListenerTempsTravail();
	private ListenerTypeSecteur 				listenerTypeSecteur = new ListenerTypeSecteur();

	public PasseCtrl(EOEditingContext edc) {

		super(edc);
		eod = new EODisplayGroup();
		myView = new PasseView(null, MODE_MODAL.booleanValue(),eod);

		myView.getMyEOTable().addListener(listenerPasse);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());
		setDateListeners(myView.getTfDateValidation());

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		NSMutableArray<INomenclature> typesServices = new NSMutableArray<INomenclature>(NomenclatureFinder.find(getEdc(), EOTypeService.ENTITY_NAME, null));
		if (((ApplicationClient)ApplicationClient.sharedApplication()).isUseServicesValides()) {
			typesServices.removeIdenticalObject(NomenclatureFinder.findForCode(getEdc(), EOTypeService.ENTITY_NAME, EOTypeService.TYPE_SERVICE_VALIDES));
		}
		
		// TRI
		NSArray<EOSortOrdering> sort = new NSArray(new EOSortOrdering(EOTypeService.CODE_KEY, EOSortOrdering.CompareDescending));
		// on trie avant pour pouvoir rajouter un type en tête du tableau
		EOSortOrdering.sortArrayUsingKeyOrderArray(typesServices, sort);

		CocktailUtilities.initPopupAvecListe(myView.getPopupTypesPopulation(), EOTypePopulation.fetchAll(getEdc()), true);
		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeService(), typesServices.immutableClone(), true);
		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeFonctionPublique(), EOTypeFonctionPublique.fetchAll(getEdc()), true);
		CocktailUtilities.initPopupAvecListe(myView.getPopupPosition(), EOPosition.getPositionsForPasseEAS(getEdc()), true);
		CocktailUtilities.initPopupAvecListe(myView.getPopupMinistere(), EOMinisteres.findMinisteresValides(getEdc()), true);

		addActionListeners();

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(getEdc(), ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));
		}

		setSaisieEnabled(false);
		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

	}

	/**
	 * 
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {

		// Gestion des droits
		myView.getBtnAjouter().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnModifier().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnSupprimer().setVisible(currentUtilisateur.peutGererCarrieres());
	}

	public static PasseCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new PasseCtrl(editingContext);
		return sharedInstance;
	}

	public EOPasse getCurrentPasse() {
		return currentPasse;
	}

	public void setCurrentPasse(EOPasse currentPasse) {
		this.currentPasse = currentPasse;
		updateDatas();
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EOChangementPosition getCurrentChangementPosition() {
		return currentChangementPosition;
	}

	public void setCurrentChangementPosition(
			EOChangementPosition currentChangementPosition) {
		this.currentChangementPosition = currentChangementPosition;
	}

	public EOPosition getCurrentPosition () {

		if( myView.getPopupPosition().getSelectedIndex() > 0) {
			return (EOPosition)myView.getPopupPosition().getSelectedItem();
		}

		return null;
	}
	public EOMotifPosition getSelectedMotifPosition () {

		if( myView.getPopupMotif().getSelectedIndex() > 0) {
			return (EOMotifPosition)myView.getPopupMotif().getSelectedItem();
		}

		return null;
	}
	public EOEnfant getSelectedEnfant () {
		if( myView.getPopupEnfant().getSelectedIndex() > 0) {
			return (EOEnfant)myView.getPopupEnfant().getSelectedItem();
		}
		return null;
	}

	/**
	 * 
	 * @param notification
	 */
	public void employeHasChanged(NSNotification  notification) {
		clearDatas();
		if (notification != null && notification.object() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(getEdc(),(Number)notification.object(), false));
		}
	}

	private void addActionListeners() {
		myView.getPopupTypeFonctionPublique().addActionListener(listenerFonctionPublique);
		myView.getPopupTypeService().addActionListener(listenerTypeService);
		myView.getPopupPosition().addActionListener(listenerPosition);
		
		myView.getCheckPublic().addActionListener(listenerTypeSecteur);
		myView.getCheckPrive().addActionListener(listenerTypeSecteur);		
		myView.getCheckTempsComplet().addActionListener(listenerTempsTravail);
		myView.getCheckTempsIncomplet().addActionListener(listenerTempsTravail);
		myView.getCheckTempsPartiel().addActionListener(listenerTempsTravail);

	}
	
	/**
	 * 
	 */
	private void removeActionListeners() {
		CocktailUtilities.removeActionListeners(new JComboBox[] {
				myView.getPopupTypeService(), myView.getPopupTypeFonctionPublique(), myView.getPopupPosition()});
		
		CocktailUtilities.removeActionListeners(new JRadioButton[] {
				myView.getCheckPublic(), myView.getCheckPrive()});
		
		CocktailUtilities.removeActionListeners(new JCheckBox[] {
				myView.getCheckTempsComplet(), myView.getCheckTempsIncomplet(), myView.getCheckTempsPartiel()});
	}

	/**
	 * 
	 */
	public void actualiser() {
		removeActionListeners();
		eod.setObjectArray(EOPasse.findForIndividu(getEdc(), getCurrentIndividu()));		
		myView.getMyEOTable().updateData();
		
		if (getCurrentPasse() == null) {
			updateInterface();
		}
		addActionListeners();
	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewPasse() {
		return myView.getViewPasse();
	}

	/**
	 * 
	 * @return
	 */
	private EOTypePopulation getTypePopulation() {
		if (myView.getPopupTypesPopulation().getSelectedIndex() > 0)
			return (EOTypePopulation)myView.getPopupTypesPopulation().getSelectedItem();
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public EOTypeService getSelectedService() {

		if (myView.getPopupTypeService().getSelectedIndex() == 0)
			return null;

		return (EOTypeService)myView.getPopupTypeService().getSelectedItem();
	}
	/**
	 * 
	 * @return
	 */
	public EOMinisteres getSelectedMinistere() {

		if (myView.getPopupMinistere().getSelectedIndex() == 0)
			return null;

		return (EOMinisteres)myView.getPopupMinistere().getSelectedItem();
	}
	/**
	 * 
	 * @return
	 */
	public EOTypeFonctionPublique getSelectedFonctionPublique() {

		if (myView.getPopupTypeFonctionPublique().getSelectedIndex() == 0)
			return null;

		return (EOTypeFonctionPublique)myView.getPopupTypeFonctionPublique().getSelectedItem();

	}

	/**
	 * 
	 * @param notification
	 */
	public void nettoyerChamps(NSNotification notification) {
		eod.setObjectArray(new NSArray<EOPasse>());
		myView.getMyEOTable().updateData();
		setCurrentIndividu(null);
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerPasse implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentPasse().contratAvenant() == null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentPasse((EOPasse)eod.selectedObject());
		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerTempsTravail implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			CocktailUtilities.initTextField(myView.getTfQuotiteService(), false , myView.getCheckTempsPartiel().isSelected());

			if (myView.getCheckTempsComplet().isSelected() || myView.getCheckTempsIncomplet().isSelected())
				myView.getTfQuotiteService().setText("100");
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerTypeSecteur implements ActionListener {
		public void actionPerformed(ActionEvent anAction){

			updateInterface();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerPopupTypeFonctionPublique implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			updateInterface();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerPopupTypeService implements ActionListener {
		public void actionPerformed(ActionEvent anAction){

			updateInterface();

			if (getSelectedService() != null && !getSelectedService().estTypeServiceValide()) {
				CocktailUtilities.viderTextField(myView.getTfAnnees());
				CocktailUtilities.viderTextField(myView.getTfMois());
				CocktailUtilities.viderTextField(myView.getTfJours());
				CocktailUtilities.viderTextField(myView.getTfDateValidation());
			}
		}
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerPopupPosition implements ActionListener {
		public void actionPerformed(ActionEvent anAction){
			updateMotifsPosition();
			updateInterface();
		}
	}

	/**
	 * 
	 */
	private void updateMotifsPosition() {
		CocktailUtilities.initPopupAvecListe(myView.getPopupMotif(), EOMotifPosition.findForPosition(getEdc(), getCurrentPosition()), true);
	}
	
	/**
	 * 
	 * @return
	 */
	private boolean estSecteurPublic() {
		return myView.getCheckPublic().isSelected();
	}
	private boolean estServicesValides() {
		return getSelectedService() != null && getSelectedService().estTypeServiceValide();
	}
	private boolean estEAS() {
		return getSelectedService() != null && getSelectedService().estTypeServiceEAS();
	}
	private boolean estEleve() {
		return getSelectedService() != null && getSelectedService().estTypeServiceEleve();
	}
	private boolean peutSaisirDuree() {
		return  getCurrentPasse() != null &&  estServicesValides();
	}
	private boolean peutSaisirPensionCivile() {
		return  isSaisieEnabled() && getCurrentPasse() != null && (estServicesValides() || estEAS() || estEleve());
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		
		setCurrentChangementPosition(null);
		
		myView.getPopupTypesPopulation().setSelectedIndex(0);
		myView.getPopupTypeFonctionPublique().setSelectedIndex(0);
		myView.getPopupTypeService().setSelectedIndex(0);
		myView.getPopupPosition().setSelectedIndex(0);
		myView.getPopupMinistere().setSelectedIndex(0);

		CocktailUtilities.viderPopup(myView.getPopupMotif());
		CocktailUtilities.viderPopup(myView.getPopupEnfant());
		
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfDateValidation());
		CocktailUtilities.viderTextField(myView.getTfAnnees());
		CocktailUtilities.viderTextField(myView.getTfMois());
		CocktailUtilities.viderTextField(myView.getTfJours());
		CocktailUtilities.viderTextField(myView.getTfQuotiteService());
		CocktailUtilities.viderTextField(myView.getTfEtablissement());

	}

	@Override
	protected void updateDatas() {
				
		// TODO Auto-generated method stub
		removeActionListeners();
		clearDatas();
		
		if (getCurrentPasse() != null) {

			CocktailUtilities.initPopupAvecListe(myView.getPopupEnfant(), EOEnfant.rechercherEnfantsPourIndividu(getEdc(), getCurrentIndividu()), true);

			EOChangementPosition changement = EOChangementPosition.findForPasse(getEdc(), getCurrentPasse());
			if (changement != null) {
				setCurrentChangementPosition(changement);
				myView.getPopupPosition().setSelectedItem(getCurrentChangementPosition().toPosition());

				updateMotifsPosition();
				myView.getPopupMotif().setSelectedItem(getCurrentChangementPosition().toMotifPosition());
				
				myView.getPopupEnfant().setSelectedItem(getCurrentChangementPosition().enfant());
			}


			myView.getPopupMinistere().setSelectedItem(getCurrentPasse().toMinistere());
			if (getCurrentPasse().pasMinistere() != null)	
				myView.getPopupMinistere().setToolTipText(getCurrentPasse().pasMinistere());
			else
				myView.getPopupMinistere().setToolTipText("");

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentPasse().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentPasse().dateFin());
			CocktailUtilities.setNumberToField(myView.getTfQuotiteService(), getCurrentPasse().pasQuotiteCotisation());
			CocktailUtilities.setTextToField(myView.getTfEtablissement(), getCurrentPasse().etablissementPasse());

			myView.getCheckTempsComplet().setSelected(getCurrentPasse().estTempsComplet());
			myView.getCheckTempsIncomplet().setSelected(getCurrentPasse().estTempsIncomplet());
			myView.getCheckTempsPartiel().setSelected(getCurrentPasse().estTempsPartiel());
			myView.getCheckPcAcquitee().setSelected(getCurrentPasse().estPcAquitees());

			myView.getPopupTypesPopulation().setSelectedItem(getCurrentPasse().typePopulation());
			myView.getPopupTypeService().setSelectedItem(getCurrentPasse().toTypeService());
			myView.getPopupTypeFonctionPublique().setSelectedItem(getCurrentPasse().toTypeFonctionPublique());

			myView.getCheckPublic().setSelected(getCurrentPasse().estSecteurPublic());
			myView.getCheckPrive().setSelected(!getCurrentPasse().estSecteurPublic());

			CocktailUtilities.setDateToField(myView.getTfDateValidation(), getCurrentPasse().dValidationService());
			CocktailUtilities.setNumberToField(myView.getTfAnnees(), getCurrentPasse().dureeValideeAnnees());
			CocktailUtilities.setNumberToField(myView.getTfMois(), getCurrentPasse().dureeValideeMois());
			CocktailUtilities.setNumberToField(myView.getTfJours(), getCurrentPasse().dureeValideeJours());

		}

		addActionListeners();

		updateInterface();
	}


	@Override
	protected void updateInterface() {
			
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null && !isSaisieEnabled());
		myView.getBtnModifier().setEnabled(getCurrentPasse() != null && !isSaisieEnabled());
		myView.getBtnSupprimer().setEnabled(getCurrentPasse() != null && !isSaisieEnabled());

		myView.getBtnValider().setVisible(isSaisieEnabled());
		myView.getBtnAnnuler().setVisible(isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled() && getCurrentPasse() != null && getCurrentPasse().contratAvenant() == null);
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled() && getCurrentPasse() != null && getCurrentPasse().contratAvenant() == null);

		myView.getPopupTypesPopulation().setEnabled(isSaisieEnabled() && estSecteurPublic());
		myView.getPopupTypeFonctionPublique().setEnabled(isSaisieEnabled() && estSecteurPublic());
		myView.getPopupTypeService().setEnabled(isSaisieEnabled() && getCurrentPasse() != null && getCurrentPasse().contratAvenant() == null);

		myView.getCheckPublic().setEnabled(isSaisieEnabled());
		myView.getCheckPrive().setEnabled(isSaisieEnabled());

		myView.getPopupMinistere().setEnabled(isSaisieEnabled() && estSecteurPublic());
		CocktailUtilities.initTextField(myView.getTfEtablissement(), false, isSaisieEnabled());

		myView.getCheckPcAcquitee().setVisible(peutSaisirPensionCivile());
		myView.getViewDureeValidee().setVisible(peutSaisirDuree());

		CocktailUtilities.initTextField(myView.getTfAnnees(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfMois(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfJours(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateValidation(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfQuotiteService(), false, isSaisieEnabled() &&  myView.getCheckTempsPartiel().isSelected());

		myView.getCheckTempsComplet().setEnabled(isSaisieEnabled());
		myView.getCheckTempsIncomplet().setEnabled(isSaisieEnabled());
		myView.getCheckTempsPartiel().setEnabled(isSaisieEnabled());

		// gestion des changements de position
		myView.getViewPosition().setVisible(estEAS());
		myView.getPopupPosition().setEnabled(isSaisieEnabled());
		myView.getPopupMotif().setEnabled(isSaisieEnabled() && getCurrentPosition() != null 
				&& getCurrentPosition().requiertMotif());
		
		myView.getPopupEnfant().setEnabled(isSaisieEnabled() && 
				(( getSelectedMotifPosition() != null && getSelectedMotifPosition().requiertEnfant())
				|| 
				( getCurrentPosition() != null	&& getCurrentPosition().requiertEnfant())
				));

	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		if (isModeCreation()) {
			EOPasse newPasse = getCurrentPasse();
			actualiser();
			CocktailUtilities.forceSelection(myView.getMyEOTable(), new NSArray(newPasse));
		}
		else {
			getEdc().invalidateObjectsWithGlobalIDs(new NSArray(getEdc().globalIDForObject(getCurrentPasse())));
			myView.getMyEOTable().updateUI();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	private NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}
	@Override
	protected void traitementsAvantValidation() {

		// TODO Auto-generated method stub
		getCurrentPasse().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentPasse().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));

		getCurrentPasse().setTypePopulationRelationship(getTypePopulation());
		getCurrentPasse().setEstSecteurPublic(myView.getCheckPublic().isSelected());
		getCurrentPasse().setToTypeServiceRelationship(getSelectedService());
		getCurrentPasse().setToTypeFonctionPubliqueRelationship(getSelectedFonctionPublique());

		getCurrentPasse().setDValidationService(CocktailUtilities.getDateFromField(myView.getTfDateValidation()));

		getCurrentPasse().setEtablissementPasse(CocktailUtilities.getTextFromField(myView.getTfEtablissement()));

		getCurrentPasse().setToMinistereRelationship(getSelectedMinistere());

		if (myView.getCheckTempsComplet().isSelected())
			getCurrentPasse().setPasTypeTemps(EOPasse.TYPE_TEMPS_COMPLET);
		if (myView.getCheckTempsIncomplet().isSelected())
			getCurrentPasse().setPasTypeTemps(EOPasse.TYPE_TEMPS_INCOMPLET);				
		if (myView.getCheckTempsPartiel().isSelected())
			getCurrentPasse().setPasTypeTemps(EOPasse.TYPE_TEMPS_PARTIEL);

		getCurrentPasse().setPasQuotiteCotisation(CocktailUtilities.getBigDecimalFromField(myView.getTfQuotiteService()));
		getCurrentPasse().setEstPcAquitees(myView.getCheckPcAcquitee().isSelected());			

		// Enregistrement de la duree validee - Seulement pour les type 'Service de non titulaires validés'
		getCurrentPasse().setDureeValideeAnnees(CocktailUtilities.getIntegerFromField(myView.getTfAnnees()));
		getCurrentPasse().setDureeValideeMois(CocktailUtilities.getIntegerFromField(myView.getTfMois()));
		getCurrentPasse().setDureeValideeJours(CocktailUtilities.getIntegerFromField(myView.getTfJours()));

		getCurrentPasse().validateForSave();

		if (estEAS()) {
			if (getCurrentChangementPosition() == null) {
				setCurrentChangementPosition(EOChangementPosition.creer(getEdc(), getCurrentPasse()));
			}
			if (getCurrentPasse().toMinistere() != null)
				getCurrentChangementPosition().setLieuPosition(getCurrentPasse().toMinistere().libelle());
			else
				if (getCurrentPasse().pasMinistere() != null)
					getCurrentChangementPosition().setLieuPosition(getCurrentPasse().pasMinistere());
				else
					getCurrentChangementPosition().setLieuPosition(getCurrentPasse().etablissementPasse());
			getCurrentChangementPosition().setDateDebut(getCurrentPasse().dateDebut());
			getCurrentChangementPosition().setDateFin(getCurrentPasse().dateFin());
			getCurrentChangementPosition().setToPositionRelationship(getCurrentPosition());
			getCurrentChangementPosition().setToMotifPositionRelationship(getSelectedMotifPosition());
			getCurrentChangementPosition().setEnfantRelationship(getSelectedEnfant());
			
		}

		verifierChevauchements();

	}

	/**
	 * Controle des chevauchements
	 * La quotite totale ne doit pas depasser 100%
	 */
	private void verifierChevauchements() {
		NSMutableArray<EOPasse> passes = new NSMutableArray<EOPasse>(eod.displayedObjects());
		if (passes.containsObject(getCurrentPasse()) == false)
			passes.addObject(getCurrentPasse());
		passes = new NSMutableArray<EOPasse>(EOSortOrdering.sortedArrayUsingKeyOrderArray(passes, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT));

		for (EOPasse myPasse : passes) {

			BigDecimal quotiteTotale = getCurrentPasse().pasQuotiteCotisation();
			if (myPasse != getCurrentPasse()) {

				if (DateCtrl.chevauchementPeriode(myPasse.dateDebut(), myPasse.dateFin(), getDateDebut() , getDateFin()) ) {

					quotiteTotale = quotiteTotale.add(myPasse.pasQuotiteCotisation());

					if ( quotiteTotale.doubleValue() > 100.0) {
						throw new NSValidation.ValidationException("La quotité totale des périodes de passé ne peut dépasser 100% pour cette période !");
					}
				}
			}
		}
	}

	@Override
	protected void traitementsApresValidation() {
		refresh();
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		getEdc().revert();
		setSaisieEnabled(false);
		listenerPasse.onSelectionChanged();

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		CRICursor.setWaitCursor(myView);
		setCurrentPasse(EOPasse.creer(getEdc(), getCurrentIndividu()));
		myView.getCheckTempsComplet().setSelected(true);
		myView.getTfQuotiteService().setText("100");
		CRICursor.setDefaultCursor(myView);

	}

	@Override
	protected void traitementsPourSuppression() {
		getCurrentPasse().setEstValide(false);
		if (getCurrentChangementPosition() != null)	 {
			getCurrentChangementPosition().setEstValide(false);
		}
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub

	}	

}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.carrieres;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.specialisations.CarriereSpecialisationsView;
import org.cocktail.mangue.client.specialisations.SpecialisationCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class CarriereSpecialisationsCtrl extends ModelePageGestion {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6490623926473207900L;

	private static CarriereSpecialisationsCtrl sharedInstance;

	private EODisplayGroup eod;
	private CarriereSpecialisationsView myView;

	private ListenerSpecialisation listenerSpecialisation = new ListenerSpecialisation();

	private EOCarriere 					currentCarriere;
	private EOCarriereSpecialisations 	currentSpec;

	private SpecialisationCtrl myCtrlSpecialisation;

	/** 
	 *
	 */
	public CarriereSpecialisationsCtrl (EOEditingContext edc) {

		super(edc);
		myCtrlSpecialisation = new SpecialisationCtrl(getEdc());

		eod = new EODisplayGroup();
		myView = new CarriereSpecialisationsView(new JFrame(), true, eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		myView.getSwapView().add("VIDE", new JPanel(new BorderLayout()));
		myView.getSwapView().add("SPEC", myCtrlSpecialisation.getView());
		((CardLayout)myView.getSwapView().getLayout()).show(myView.getSwapView(), "SPEC");

		myView.getMyEOTable().addListener(listenerSpecialisation);

		updateInterface();

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CarriereSpecialisationsCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CarriereSpecialisationsCtrl(editingContext);
		return sharedInstance;
	}

	public EOCarriereSpecialisations getCurrentSpec() {
		return currentSpec;
	}
	public void setCurrentSpec(EOCarriereSpecialisations currentSpec) {
		this.currentSpec = currentSpec;
		updateDatas();
	}
	public EOCarriere getCurrentCarriere() {
		return currentCarriere;
	}
	public void setCurrentCarriere(EOCarriere currentCarriere) {
		this.currentCarriere = currentCarriere;
	}

	/**
	 * 
	 * @param carriere
	 */
	public void open(EOCarriere carriere)	{

		setCurrentCarriere(carriere);
		myView.setTitle(getCurrentCarriere().toIndividu().identitePrenomFirst() + " - SPECIALISATIONS");

		setSaisieEnabled(false);
		myCtrlSpecialisation.setSaisieEnabled(false);
		actualiser();

		myView.setVisible(true);
	}

	private NSTimestamp getDateDebut()	{
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	private NSTimestamp getDateFin()	{
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}


	/**
	 * 
	 * @throws ValidationException
	 */
	protected void traitementsAvantValidation() throws ValidationException {

		getCurrentSpec().setSpecDebut(getDateDebut());
		getCurrentSpec().setSpecFin(getDateFin());

		if (getDateDebut() != null) {

			// Controle des chevauchements
			NSMutableArray<EOCarriereSpecialisations> specialisations = new NSMutableArray<EOCarriereSpecialisations>(eod.displayedObjects());
			if (specialisations.containsObject(getCurrentSpec()) == false)
				specialisations.addObject(getCurrentSpec());
			specialisations = new NSMutableArray<EOCarriereSpecialisations>(EOSortOrdering.sortedArrayUsingKeyOrderArray(specialisations, EOCarriereSpecialisations.SORT_ARRAY_DATE_DEBUT_ASC));

			for (EOCarriereSpecialisations mySpec : specialisations) {
				if (mySpec != getCurrentSpec()) {
					if (DateCtrl.chevauchementPeriode(mySpec.specDebut(), mySpec.specFin(), getDateDebut() , getDateFin()) ) {
						throw new ValidationException("L'agent a déjà des spécialisations pendant cette période !");
					}
				}
			}
		}

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerSpecialisation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentSpec((EOCarriereSpecialisations)eod.selectedObject());
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		myCtrlSpecialisation.clean();

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentSpec() != null) {

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentSpec().specDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentSpec().specFin());

			myCtrlSpecialisation.showSpecForCarriereSpecialisation(getCurrentSpec());
			myCtrlSpecialisation.setDateReference(getDateDebut());

		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled());
		myView.getBtnModifier().setEnabled(getCurrentSpec() != null && !isSaisieEnabled());
		myView.getBtnSupprimer().setEnabled(getCurrentSpec() != null && !isSaisieEnabled());

		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());
		myView.getBtnValider().setEnabled(isSaisieEnabled());

	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		eod.setObjectArray(EOCarriereSpecialisations.findForCarriere(getEdc(), getCurrentCarriere()));
		myView.getMyEOTable().updateData();
	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		if (isModeCreation()) {
			EOCarriereSpecialisations newSpec = getCurrentSpec();
			actualiser();
			CocktailUtilities.forceSelection(myView.getMyEOTable(), new NSArray(newSpec));
		}
		else {
			myView.getMyEOTable().updateUI();
		}
	}

	@Override
	protected void traitementsApresValidation() {
		myCtrlSpecialisation.setSaisieEnabled(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		getEdc().revert();
		setCurrentSpec(null);
		myCtrlSpecialisation.setSaisieEnabled(false);
		listenerSpecialisation.onSelectionChanged();

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentSpec(EOCarriereSpecialisations.creer(getEdc(), getCurrentCarriere(), ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel()));
		myCtrlSpecialisation.setSaisieEnabled(true);

	}

	@Override
	protected void traitementsPourSuppression() {
		for (EOCarriereSpecialisations mySpec : (NSArray<EOCarriereSpecialisations>)eod.selectedObjects()) {
			getEdc().deleteObject(mySpec);
		}
	}

	@Override
	protected void traitementsApresSuppression() {

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		myCtrlSpecialisation.setDateReference(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		myCtrlSpecialisation.setSaisieEnabled(true);
	}

}

package org.cocktail.mangue.client.carrieres;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.carriere.AncienneteConservationView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.modele.mangue.individu.EOConservationAnciennete;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

public class AncienneteConservationCtrl extends ModelePageGestion {
	
	private static AncienneteConservationCtrl sharedInstance;
	private AncienneteConservationView myView;
	private EODisplayGroup eod;
	private ListenerConservation listenerConservation = new ListenerConservation();

	private EOElementCarriere currentElement;
	private EOConservationAnciennete currentConservation;

	public AncienneteConservationCtrl(EOEditingContext edc) {

		super(edc);
		
		eod = new EODisplayGroup();
		myView = new AncienneteConservationView(eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		
		myView.getMyEOTable().addListener(listenerConservation);
		
		setSaisieEnabled(false);

	}

	public void lock(boolean yn) {

		if (yn) {//lock 
			myView.getBtnAjouter().setEnabled(false);
			myView.getBtnModifier().setEnabled(false);
			myView.getBtnSupprimer().setEnabled(false);
			myView.getMyEOTable().setEnabled(false);			
		}else {	// unlock
			myView.getMyEOTable().setEnabled(true);			
			updateInterface();
		}

	}

	public static AncienneteConservationCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new AncienneteConservationCtrl(editingContext);
		return sharedInstance;
	}

	

	public EOElementCarriere getCurrentElement() {
		return currentElement;
	}

	public void setCurrentElement(EOElementCarriere currentElement) {
		this.currentElement = currentElement;
	}

	public EOConservationAnciennete getCurrentConservation() {
		return currentConservation;
	}

	public void setCurrentConservation(EOConservationAnciennete currentConservation) {
		this.currentConservation = currentConservation;
		updateDatas();
	}

	public JPanel getView() {
		return myView;
	}	

	/**
	 * 
	 * @param element
	 */
	public void actualiser(EOElementCarriere element)	{
		setCurrentElement(element);
		actualiser();
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerConservation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()		{
			if (myView.getMyEOTable().isEnabled())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentConservation((EOConservationAnciennete)eod.selectedObject());
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getPopupNbAnnees().setSelectedIndex(0);
		myView.getPopupNbMois().setSelectedIndex(0);
		myView.getPopupNbJours().setSelectedIndex(0);
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentConservation() != null) {
			if (getCurrentConservation().ancNbAnnees() != null)
				myView.getPopupNbAnnees().setSelectedItem(getCurrentConservation().ancNbAnnees());
			if (getCurrentConservation().ancNbMois() != null)
				myView.getPopupNbMois().setSelectedItem(getCurrentConservation().ancNbMois());
			if (getCurrentConservation().ancNbJours() != null)
				myView.getPopupNbJours().setSelectedItem(getCurrentConservation().ancNbJours());
			myView.getCheckUtilise().setSelected(getCurrentConservation().estUtilisee());
		}
		updateInterface();
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnAjouter().setEnabled(!isSaisieEnabled() && getCurrentElement() != null);
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentElement() != null && getCurrentConservation() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentElement() != null && getCurrentConservation() != null);

		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getPopupNbAnnees().setEnabled(isSaisieEnabled());
		myView.getPopupNbMois().setEnabled(isSaisieEnabled());
		myView.getPopupNbJours().setEnabled(isSaisieEnabled());
		myView.getCheckUtilise().setEnabled(isSaisieEnabled());

	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		eod.setObjectArray(EOConservationAnciennete.findForElementCarriere(getEdc(), getCurrentElement()));
		myView.getMyEOTable().updateData();

		updateInterface();
	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
	}

	private Integer getSelectedAnnees() {
		if (myView.getPopupNbAnnees().getSelectedIndex() > 0) {
			return (Integer)myView.getPopupNbAnnees().getSelectedItem();
		}
		return null;
	}
	private Integer getSelectedMois() {
		if (myView.getPopupNbMois().getSelectedIndex() > 0) {
			return (Integer)myView.getPopupNbMois().getSelectedItem();
		}
		return null;
	}
	private Integer getSelectedJours() {
		if (myView.getPopupNbJours().getSelectedIndex() > 0) {
			return (Integer)myView.getPopupNbJours().getSelectedItem();
		}
		return null;
	}
	
	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentConservation().setEstUtilisee(myView.getCheckUtilise().isSelected());
		getCurrentConservation().setAncNbAnnees(getSelectedAnnees());
		getCurrentConservation().setAncNbMois(getSelectedMois());
		getCurrentConservation().setAncNbJours(getSelectedJours());
		
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		AncienneteReductionCtrl.sharedInstance(getEdc()).lock(false);
		AncienneteCtrl.sharedInstance(getEdc()).lock(false);
		
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		listenerConservation.onSelectionChanged();
		AncienneteReductionCtrl.sharedInstance(getEdc()).lock(false);
		AncienneteCtrl.sharedInstance(getEdc()).lock(false);

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		AncienneteCtrl.sharedInstance(getEdc()).lock(true);
		AncienneteReductionCtrl.sharedInstance(getEdc()).lock(true);

		setCurrentConservation(EOConservationAnciennete.creer(getEdc(), getCurrentElement()));

	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		AncienneteCtrl.sharedInstance(getEdc()).lock(true);
		AncienneteReductionCtrl.sharedInstance(getEdc()).lock(true);

	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(currentConservation);

	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}

}

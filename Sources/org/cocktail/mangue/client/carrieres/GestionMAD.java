
//GestionMAD.java
//Mangue

//Created by Christine Buttin on Fri Sep 02 2005.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.


/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.carrieres;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.outils_interface.GestionEvenementAvecArrete;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.Arrete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOMad;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
/** Gestion des mises a disposition<BR>
 * La date de debut, de fin, le type et la rne ou le lieu d'accueil doivent etre saisis.<BR>
 * On ne peut modifier un mi-temps therapeuthique lorsqu'un arrete a ete signe.<BR>
 * Pour saisir une date d'avis de comite medical superieur il faut saisir une date d'avis de comite medical local.<BR>
 * On affiche un avertissement si la duree de Mad est superieurea a 5 ans pour les astronomes, les enseignants-chercheurs, les physiciens et les agreges
 * du 2nd degre et a 3 ans sinon<BR>
 * Les regles de validation sont definies dans la classe EOMad
 * 
 * @author christine
 */
// 25/03/2011 - il faut fournir la quotité de MAD
public class GestionMAD extends GestionEvenementAvecArrete {

	public GestionMAD() {
		super("Mad");
	}

	// actions
	public void afficherRne() {
		EORne rne = (EORne)UAISelectCtrl.sharedInstance(editingContext()).getObject();
		if (rne != null)
			currentMad().setToRneRelationship(rne);
	}

	public void afficherTypeMad() {
		UtilitairesDialogue.afficherDialogue(this,"TypeMad","getTypeMad",false,null,true);
	}
	//	 SUPPRIMER RNE
	public void supprimerRne() {
		currentMad().setToRneRelationship(null);
	}

	// NOTIFICATIONS
	//	 GET RNE
	public void getRne(NSNotification aNotif) {
		ajouterRelation(currentMad(),aNotif.object(),EOMad.TO_RNE_KEY);
	}
	// Get mode delegation
	public void getTypeMad(NSNotification aNotif) {
		ajouterRelation(currentMad(),aNotif.object(),EOMad.TO_TYPE_MAD_KEY);
	}
	// méthodes du controleur DG
	// 25/03/2011 - vérification de la quotité
	public boolean peutValider() {
		if (currentMad() == null || currentMad().toTypeMad() == null || currentMad().dateDebut() == null || 
				currentMad().dateFin() == null || (currentMad().toRne() == null && currentMad().lieuMad() == null) ||
				currentMad().madQuotite() == null) {
			return false;
		} else {
			return super.peutValider();
		}
	}
	public boolean peutSupprimerRne() {
		return modificationEnCours() && currentMad().toRne() != null;
	}

	// méthodes protégées
	/** true si l'agent peut gerer les carrieres */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutGererCarrieres();
	}
	protected boolean traitementsAvantValidation() {
		verifierDureeMad();
		return super.traitementsAvantValidation();
	}
	protected EOTypeGestionArrete typeGestionArrete() {
		return null;
	}
	protected Arrete arreteCourant() {
		if (currentMad() == null) {
			return null;
		} else {
			return new Arrete(currentMad().dateArrete(),currentMad().noArrete(),CocktailConstantes.FAUX);
		}
	}
	protected Arrete arreteAnnulationCourant() {
		return null;
	} 
	protected boolean supporteSignature() {
		return false;
	}
	// méthodes privées
	private EOMad currentMad() {
		return (EOMad)displayGroup().selectedObject();
	}
	// on détermine le segment de carrière concerné pour pouvoir afficher des messages d'information
	private void verifierDureeMad() {
		NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),currentIndividu(),currentMad().dateDebut(),currentMad().dateFin());
		// vérifie si fonctionnaire titulaire, non en stage avec un corps qui supporte la délégation
		if (carrieres != null && carrieres.count() > 0) {
			EOCarriere carriereTrouvee = null;
			boolean enActivite = false;
			for (java.util.Enumeration<EOCarriere> e = carrieres.objectEnumerator();e.hasMoreElements();) {
				EOCarriere carriere = e.nextElement();
				NSArray stages = carriere.stagesPourPeriode(currentMad().dateDebut(),currentMad().dateFin());
				if (carriere.toTypePopulation().estFonctionnaire() && currentIndividu().estTitulaire() && (stages == null || stages.count() == 0)) {
					// vérifier si le corps dans les segments de carrière supporte la délégation
					NSArray result = carriere.elementsPourPeriode(currentMad().dateDebut(),currentMad().dateFin());
					if (result != null && result.count() > 0) {

						for (java.util.Enumeration<EOElementCarriere> e1 = result.objectEnumerator();e1.hasMoreElements();) {
							if (e1.nextElement().toCorps().beneficieDelegation()) {
								NSArray changements = carriere.changementsPositionPourPeriode(currentMad().dateDebut(),currentMad().dateFin());
								if (changements != null && changements.count() > 0) {
									
									for (java.util.Enumeration<EOChangementPosition> e2 = changements.objectEnumerator();e2.hasMoreElements();) {
										EOChangementPosition changement = (EOChangementPosition)e2.nextElement();
										// La période de maintien en fonction doit reposer sur une position d'activité ou de détachement avec carrière d'accueil
										if (!changement.toPosition().estEnActiviteDansEtablissement() && (!changement.toPosition().estUnDetachement() || (changement.estDetachementSortant()))) {
											enActivite = false;
										} else {
											enActivite = true;
										}
									}
									if (enActivite) {
										carriereTrouvee = carriere;
										break;
									}
								}
							}
						}
					}
					if (carriereTrouvee != null && enActivite) {
						break;
					}
				}	
			}
			if (carriereTrouvee != null && enActivite) {
				int nbMois = DateCtrl.calculerDureeEnMois(currentMad().dateDebut(),currentMad().dateFin()).intValue();
				EOCorps corpsAgrege = null;
				if (carriereTrouvee.toTypePopulation().est2Degre()) {
					NSArray elementsCarriere = carriereTrouvee.elementsPourPeriode(currentMad().dateDebut(),currentMad().dateFin());
					for (java.util.Enumeration<EOElementCarriere> e3= elementsCarriere.objectEnumerator();e3.hasMoreElements();) {
						EOElementCarriere element = e3.nextElement();
						if (element.toCorps().cCorps().equals(EOCorps.CORPS_POUR_AGREFGE_2DEGRE)) {
							corpsAgrege = element.toCorps();
							break;
						}
					}
				}
				if (carriereTrouvee.toTypePopulation().code().equals(EOTypePopulation.ENSEIGNANT_CHERCHEUR) || corpsAgrege != null) {
					if (nbMois > EOMad.DUREE_MAX_CHERCHEUR) {
						EODialogs.runInformationDialog("ATTENTION","Pour les enseignants-chercheurs, les astronomes, les physiciens et les agrégés du 2nd degré, la durée maximum d'une période de mise à disposition est de 5 ans");
					}
				} else {
					int anneesMax = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_DUREE_MIN);
					if (nbMois > (anneesMax * 12)) {
						EODialogs.runInformationDialog("ATTENTION","Seuls les enseignants-chercheurs, les astronomes, les physiciens et les agrégés du 2nd degré, peuvent avoir une durée  de mise à disposition supérieure à 3 ans");
					}
				}
			}
		}
	}

}
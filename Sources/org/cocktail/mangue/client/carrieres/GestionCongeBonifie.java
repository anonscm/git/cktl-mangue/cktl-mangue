/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.carrieres;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.outils_interface.GestionEvenementAvecArrete;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Arrete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOCongeBonifie;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
/** Gestion des mises a disposition<BR>
 * La date de debut et de de fin,doivent etre saisies.<BR>
 * La duree maximale d'un conge bonifie est de 65 jours samedi et dimanche compris.<BR>
 * La duree entre deux conges bonifies doit etre d'au moins 36 mois, duree du precedent conge incluse.<BR>
 * Les autres regles de validation sont definies dans la classe EOCongeBonifie
 * 
 * @author christine
*/

public class GestionCongeBonifie extends GestionEvenementAvecArrete {
	 public GestionCongeBonifie() {
	    super("CongeBonifie");
    }
	// méthodes protégées
	/** true si l'agent peut gerer les carrieres */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutGererCarrieres();
	}
	public boolean peutValider() {
		if (currentConge() == null || currentConge().dateDebut() == null || currentConge().dateFin() == null) {
			return false;
		} else {
			return super.peutValider();
		}
	}
	protected boolean traitementsAvantValidation() {
		if (datesCongeOK()) {
			return super.traitementsAvantValidation();
		} else {
			return false;
		}
	}
	 protected EOTypeGestionArrete typeGestionArrete() {
	 	return null;
	 }
	 protected Arrete arreteCourant() {
	 	if (currentEvenement() == null) {
	 		return null;
	 	} else {
	 		return new Arrete(currentEvenement().dateArrete(),currentEvenement().noArrete(),CocktailConstantes.FAUX);
	 	}
	 }
	 protected Arrete arreteAnnulationCourant() {
		 	return null;
	 } 
	 protected boolean supporteSignature() {
	 	return false;
	 }
	 //	 méthodes privées
	private EOCongeBonifie currentConge() {
		return (EOCongeBonifie)displayGroup().selectedObject();
	}
	private boolean datesCongeOK() {
		// * La duree maximale d'un conge bonifie est de 65 jours samedi et dimanche compris.<BR>
		// * La duree entre deux conges bonifies doit etre d'au moins 36 mois, duree du precedent conge incluse.<BR>
		float nbJours = DateCtrl.nbJoursEntre(currentConge().dateDebut(),currentConge().dateFin(),true);
		if ((int)nbJours > EOCongeBonifie.DUREE_MAX) {
			EODialogs.runErrorDialog("Erreur","Un congé bonifié dure au maximum " + EOCongeBonifie.DUREE_MAX + " jours");
			return false;
		}
		int nbConges = displayGroup().displayedObjects().count();
		if (nbConges > 1) {
			NSArray congesTries = EOSortOrdering.sortedArrayUsingKeyOrderArray(displayGroup().displayedObjects(),new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));
			EOCongeBonifie congeSuivant = null, congePrecedent = null;
			for (int i = 0; i < nbConges ;i++) {
				EOCongeBonifie conge = (EOCongeBonifie)congesTries.get(i);
				if (conge == currentConge()) {
					if (i > 0) {
						congePrecedent = (EOCongeBonifie)congesTries.get(i - 1);
					}
					if (i < congesTries.count() - 1) {
						congeSuivant = (EOCongeBonifie)congesTries.get(i + 1);
					}
					break;
				}
			}
			if (congeSuivant != null) {
				// vérifier avec le congé 
				if (DateCtrl.calculerDureeEnMois(currentConge().dateDebut(),congeSuivant.dateFin()).intValue() < EOCongeBonifie.DUREE_ENTRE_DEUX_CONGES) {
					EODialogs.runErrorDialog("Erreur","La durée entre deux congés bonifiés doit être au moins de " + EOCongeBonifie.DUREE_ENTRE_DEUX_CONGES + " mois (durée du congé incluse)");
					return false;
				}
			}
			if (congePrecedent != null) {
				if (DateCtrl.calculerDureeEnMois(congePrecedent.dateDebut(),currentConge().dateFin()).intValue() < EOCongeBonifie.DUREE_ENTRE_DEUX_CONGES) {
					EODialogs.runErrorDialog("Erreur","La durée entre deux congés bonifiés doit être au moins de " + EOCongeBonifie.DUREE_ENTRE_DEUX_CONGES + " mois (durée du congé précédent incluse)");
					return false;
				}
			}
		}
		return true;

	}
}

package org.cocktail.mangue.client.carrieres;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.carriere.DetailsIndicesView;
import org.cocktail.mangue.client.gui.contrats.ContratDetailPaieView;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeRemunContrat;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class DetailsIndicesCtrl {

	private static final long serialVersionUID = 0x7be9cfa4L;
	private static DetailsIndicesCtrl sharedInstance;

	private EOEditingContext	edc;
	private EODisplayGroup eod;	
	private DetailsIndicesView myView;
	private EOElementCarriere currentElement;

	public DetailsIndicesCtrl(EOEditingContext edc) {

		setEdc(edc);

		eod = new EODisplayGroup();
		myView = new DetailsIndicesView(new JFrame(), true, eod);
	}

	public static DetailsIndicesCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new DetailsIndicesCtrl(editingContext);
		return sharedInstance;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOElementCarriere getCurrentElement() {
		return currentElement;
	}

	public void setCurrentElement(EOElementCarriere currentElement) {
		this.currentElement = currentElement;
	}

	/**
	 * 
	 * @param contratAvenant
	 */
	public void open(EOElementCarriere element) {

		CRICursor.setWaitCursor(myView);
		setCurrentElement(element);

		// Recuperation de l'indice		
		NSArray<EOPassageEchelon> passagesEchelons = EOPassageEchelon.findForGradeEchelonAndPeriode(edc, 
				getCurrentElement().toGrade(), getCurrentElement().cEchelon(), getCurrentElement().dateDebut(), getCurrentElement().dateFin());

		NSMutableArray<NSDictionary> indices = new NSMutableArray<NSDictionary>();
		NSTimestamp dateDebut, dateFin;
		for (EOPassageEchelon passage : passagesEchelons) {
						
			NSMutableDictionary dico = new NSMutableDictionary();
			
			if ( DateCtrl.isBefore(getCurrentElement().dateDebut(), passage.dOuverture()) ) {
				dateDebut = passage.dOuverture();				
			}
			else {
				dateDebut = getCurrentElement().dateDebut();
			}
			
			if (passage.dFermeture() == null) {
				dateFin = getCurrentElement().dateFin();								
			}
			else {
				if (getCurrentElement().dateFin() != null && DateCtrl.isAfter(currentElement.dateFin(), passage.dFermeture()) )  {
					dateFin = passage.dFermeture();				
				}
				else {
					dateFin = getCurrentElement().dateFin();				
				}				
			}
			
			dico.setObjectForKey(dateDebut, EOPassageEchelon.D_OUVERTURE_KEY);
			if (dateFin != null)
				dico.setObjectForKey(dateFin, 	EOPassageEchelon.D_FERMETURE_KEY);
			EOIndice indice = EOIndice.indiceMajorePourIndiceBrutEtDate(getEdc(), passage.cIndiceBrut(), dateDebut);
			if (indice != null)
				dico.setObjectForKey(indice.cIndiceMajore(), EOPassageEchelon.INDICE_MAJORE_KEY);
			if (passage.cIndiceBrut() != null)
				dico.setObjectForKey(passage.cIndiceBrut(), EOPassageEchelon.C_INDICE_BRUT_KEY);

			indices.addObject(dico.immutableClone());
		}
		
		eod.setObjectArray(indices);
		myView.getMyEOTable().updateData();
		
		if (getCurrentElement().dateFin() == null) {
			myView.setTitle("Détail des indices depuis le " + DateCtrl.dateToString(getCurrentElement().dateDebut()));
		}
		else {
			myView.setTitle("Détail des indices du " + DateCtrl.dateToString(getCurrentElement().dateDebut()) + " au " 
					+ DateCtrl.dateToString(getCurrentElement().dateFin()));
		}

		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);

	}

}
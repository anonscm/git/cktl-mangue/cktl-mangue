/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.carrieres;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.AideUtilisateur;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.outils_interface.ModelePageAvecIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EOParamPromotion;
import org.cocktail.mangue.modele.mangue.individu.EOHistoPromotion;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/** Gere les promotions d'un individu. Permet de creer/modifier/supprimer une promotion et de regler les détails de la promotion.
 * Pour les regles concernant le statut de la promotion, voir la classe EOHistoPromotion.
 *
 */

public class GestionPromotionIndividu extends ModelePageAvecIndividu {
	public final static String SYNCHRONISER_PROMOTION = "SynchroniserPromotion";
	private Integer anneePromotionSupprimee;
	private boolean rechercherPromotionsEchelon;	// 09/02/2010 positionner par l'utilisateur pour indiquer qu'on recherche les promotions d'échelon de l'individu
	// Accesseurs
	public String descriptionParametre() {
		if (currentPromotion() == null || currentPromotion().paramPromotion() == null) {
			return null;
		} else {
			return currentPromotion().paramPromotion().description();
		}
	}
	/** Retourne le libelle du label pour type de population, corps ou grade d'origine */
	public String libelleLabel() {
		if (currentPromotion() == null || currentPromotion().paramPromotion() == null) {
			return "";
		} else {
			if (currentPromotion().paramPromotion().gradeDepart() != null) {
				return "Grade d'origine";
			} else if (currentPromotion().paramPromotion().corpsDepart() != null) {
				return "Corps d'origine";
			} else if (currentPromotion().paramPromotion().typePopulationDepart() != null) {
				return "Type Population d'origine";
			} else {
				return "";
			}
		}
	}
	public boolean rechercherPromotionsEchelon() {
		return rechercherPromotionsEchelon;
	}
	public void setRechercherPromotionsEchelon(boolean rechercherPromotionsEchelon) {
		this.rechercherPromotionsEchelon = rechercherPromotionsEchelon;
		if (currentPromotion().paramPromotion() != null) {
			// On veut repréparer la promotion, on a juste changer le statut
			if (currentPromotion().paramPromotion().estListeAptitude() || currentPromotion().paramPromotion() == null || currentPromotion().paramPromotion().cEchelon() == null || 
					(currentPromotion().paramPromotion().parpDureeEchelon() != null && currentPromotion().paramPromotion().parpDureeEchelon().intValue() > 1)) {
				rechercherPromotionsEchelon = false;
				EODialogs.runInformationDialog("Attention", "La recherche de promotion d'échelon n'a pas de sens pour ce paramètre, elle ne sera pas prise en compte");
			} else {
				EODialogs.runInformationDialog("", "La promotion est réévaluée");
				preparerPromotion();
			}
		}
	}
	public boolean estProvisoire() {
		return currentPromotion() != null && currentPromotion().estProvisoire();
	}
	public void setEstProvisoire(boolean aBool) {
		if (aBool) {
			currentPromotion().rendreProvisoire();
		} else {
			currentPromotion().indiquerManuelle();
		}
	}
	public boolean estTransmiseMinistere() {
		return currentPromotion() != null && currentPromotion().estTransmiseMinistere();
	}
	public void setEstTransmiseMinistere(boolean aBool) {
		if (aBool) {
			currentPromotion().transmettreMinistere();
		} else {
			currentPromotion().indiquerManuelle();
		}
	}
	public boolean individuPromu() {
		return currentPromotion() != null && currentPromotion().individuPromu();
	}
	public void setIndividuPromu(boolean aBool) {
		if (aBool) {
			currentPromotion().promouvoir();
		} else {
			currentPromotion().transmettreMinistere();	// L'interface empêche qu'on revienne en arrière i.e aBool = false
		}
	}
	public boolean promotionRefusee() {
		return currentPromotion() != null && currentPromotion().promotionRefusee();
	}
	public void setPromotionRefusee(boolean aBool) {
		if (aBool) {
			currentPromotion().refuserPromotion();
		} else {
			currentPromotion().transmettreMinistere();	// L'interface empêche qu'on revienne en arrière i.e aBool = false
		}
	}
	// Actions
	public void afficherAide() {
		AideUtilisateur controleur = new AideUtilisateur("Aide_GestionPromotions",null);
		controleur.afficherFenetre();
	}
	public void afficherParametres() {
		UtilitairesDialogue.afficherDialogue(this, EOParamPromotion.ENTITY_NAME, "getParamPromotion", true, null,false);
	}
	public void imprimerFicheIndividuelle() {
		EOHistoPromotion promotion = (EOHistoPromotion)displayGroup().selectedObject();
		Class[] classeParametres =  new Class[] {EOGlobalID.class,NSTimestamp.class};
		NSTimestamp dateReference = EOHistoPromotion.datePromotionPourParametreEtAnnee(promotion.paramPromotion().parpType(), promotion.hproAnnee());
		Object[] parametres = new Object[]{editingContext().globalIDForObject(promotion),dateReference};
		try {
			// avec Thread
			UtilitairesImpression.imprimerSansDialogue(editingContext(), "clientSideRequestimprimerFicheLATA", classeParametres,parametres, "Promouvabilites","Impression des promouvabilités");
		} catch (Exception exc) {
			LogManager.logException(exc);
			EODialogs.runErrorDialog("Erreur",exc.getMessage());
		}
	}
	// Notifications
	public void getParamPromotion(NSNotification aNotif) {
		EOGlobalID globalID = (EOGlobalID)aNotif.object();
		EOParamPromotion paramPromotion = (EOParamPromotion)SuperFinder.objetForGlobalIDDansEditingContext(globalID, editingContext());
		// Vérifier si il existe déjà une promotion pour ce paramètre
		for (java.util.Enumeration<EOHistoPromotion> e = displayGroup().displayedObjects().objectEnumerator();e.hasMoreElements();) {
			EOHistoPromotion promotion = e.nextElement();
			if (promotion != currentPromotion() && promotion.paramPromotion() == 
				paramPromotion && promotion.hproAnnee().intValue() == currentPromotion().hproAnnee().intValue()) {
				EODialogs.runErrorDialog("Erreur", "Il existe déjà une promotion de ce type");
				return;
			}
		}
		// 09/02/2010 - Vérifier si la promotion d'échelon est possible
		if (rechercherPromotionsEchelon) {
			// Vérifier si le paramètre justifie qu'on regarde les échelons
			// Ce ne doit pas être une liste d'aptitude, le grade de départ doit être fourni ainsi que l'échelon et la durée à cet échelon doit être inférieure à 1 an (car elle sera faite dans l'année)
			if (paramPromotion.estListeAptitude() || paramPromotion.gradeDepart() == null || paramPromotion.cEchelon() == null || 
					(paramPromotion.parpDureeEchelon() != null && paramPromotion.parpDureeEchelon().intValue() > 1)) {
				rechercherPromotionsEchelon = false;
			}
		}
		currentPromotion().setParamPromotionRelationship(paramPromotion);
		preparerPromotion();
	}
	public void synchroniserPromotions(NSNotification aNotif) {
		displayGroup().setObjectArray(fetcherObjets());
		updaterDisplayGroups();
	}
	// Méthodes du controller DG
	/** On ne peut saisir des informations (annee ou parametre associe) que si
	 * on est en mode creation **/
	public boolean peutSaisir() {
		return super.modificationEnCours() && modeCreation();
	}
	/** On ne peut selectionner le parametre que si l'annee a ete saisie et qu'on est en mode creation */
	public boolean peutSelectionnerParametre() {
		return peutSaisir() && currentPromotion().hproAnnee() != null && currentPromotion().hproAnnee().intValue() >= 2000;
	}
	/** On peut promouvoir un individu qui a rempli son dossier et dont le dossier a ete transmis au Ministere ou retourner 
	 * a l'etat transmis au minist&grave;re. 
	 * On est en mode modification */
	public boolean peutPromouvoir() {
		return modificationEnCours() && currentPromotion().paramPromotion() != null && currentPromotion().aRempliDossier() &&
		currentPromotion().estTransmiseMinistere();
	}
	/** On ne peut imprimer la fiche individuelle que si la promotion est selectionnee et qu'elle a le statut
	 * manuel ou automatique
	 */
	public boolean peutImprimer() {
		return boutonModificationAutorise() && (currentPromotion().estAjouteeManuellement() || currentPromotion().estEvalueeAutomatiquement());
	}
	/** On peut changer le statut de provisoire d'une promotion si elle est provisoire/manuelle, que le dossier n'est pas rempli 
	 * et qu'on est en mode modification */
	public boolean peutModifierProvisoire() {
		return modificationEnCours() && currentPromotion().paramPromotion() != null && 
		(currentPromotion().estProvisoire() || currentPromotion().estAjouteeManuellement() || currentPromotion().estEvalueeAutomatiquement()) &&
		currentPromotion().aRempliDossier() == false;
	}
	/** On ne peut indiquer que le dossier est rempli que si la promotion est automatique ou manuelle.On est en mode modification */
	public boolean peutModifierDossierTransmis() {
		return modificationEnCours() && currentPromotion().paramPromotion() != null &&
				(currentPromotion().estEvalueeAutomatiquement() || currentPromotion().estAjouteeManuellement());
	}
	/** On ne peut transmettre au ministere que si le dossier est rempli. On est en mode modification */
	public boolean peutTransmettreMinistere() {
		return modificationEnCours() && currentPromotion().paramPromotion() != null && currentPromotion().aRempliDossier();
	}
	public boolean peutSupprimer() {
		return boutonModificationAutorise() && currentPromotion().aRempliDossier() == false && currentPromotion().estTransmiseMinistere() == false
		&& currentPromotion().individuPromu() == false && currentPromotion().promotionRefusee() == false;
	}
	public boolean peutValider() {
		return super.peutValider() && currentPromotion().hproAnnee() != null && currentPromotion().paramPromotion() != null;
	}
	// Méthodes protégées
	protected void loadNotifications() {
		super.loadNotifications();
	}
	protected NSArray fetcherObjets() {
		return EOHistoPromotion.rechercherPromotionsPourIndividu(editingContext(), currentIndividu());
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette promotion ?";
	}
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOHistoPromotion.HPRO_ANNEE_KEY, EOSortOrdering.CompareDescending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOHistoPromotion.HPRO_STATUT_KEY, EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
		displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat(EOHistoPromotion.HPRO_STATUT_KEY + " <> '" + EOHistoPromotion.STATUT_SUPPRIME + "'", null));
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void traitementsApresValidation() {
		NSNotificationCenter.defaultCenter().postNotification(SYNCHRONISER_PROMOTION,currentPromotion().hproAnnee(),null);
		super.traitementsApresValidation();
	}
	protected void traitementsPourCreation() {
		currentPromotion().initAvecIndividu(currentIndividu());
	}
	protected boolean traitementsPourSuppression() {
		anneePromotionSupprimee = (Integer)currentPromotion().hproAnnee();
		currentPromotion().setHproStatut(EOHistoPromotion.STATUT_SUPPRIME);
		return true;
	}
	protected void traitementsApresSuppression() {
		NSNotificationCenter.defaultCenter().postNotification(SYNCHRONISER_PROMOTION,anneePromotionSupprimee,null);
		super.traitementsApresSuppression();
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOHistoPromotion currentPromotion() {
		return (EOHistoPromotion)displayGroup().selectedObject();
	}
	// 26/02/2010 - si l'individu ne peut pas bénéficier de la promotion parce qu'il ne respecte pas toutes les conditions
	// on le signale à l'utilisateur et on lui permet de recommencer
	private void preparerPromotion() {
		EOHistoPromotion.ResultatEvaluationAnciennete resultat = EOHistoPromotion.verifierPromotionPourIndividuAnneeEtParametre(currentIndividu(),currentPromotion().hproAnnee(),currentPromotion().paramPromotion(),rechercherPromotionsEchelon);
		if (resultat == null) {	// Le resultat est null si un individu n'est pas promouvable (disponibilité, hors cadre ou congé parental)
			EODialogs.runErrorDialog("Attention","L'individu a une position qui empêche les promotions");
		} else if (resultat.statutPromotion().equals(EOHistoPromotion.STATUT_SUPPRIME)) {
			EODialogs.runErrorDialog("Erreur", "L'individu ne peut pas bénéficier de cette promotion");
		} else if (resultat.statutPromotion().equals(EOHistoPromotion.STATUT_PROVISOIRE)) {
			if (EODialogs.runConfirmOperationDialog("Attention", "Cet individu ne remplit pas toutes les conditions pour bénéficier de la promotion. Voulez-vous la maintenir ?", "Oui", "Non")) {
				currentPromotion().modifierAvecResultatPromotion(resultat);
			} else if (currentPromotion().hproStatut() != null && (currentPromotion().hproStatut().equals(EOHistoPromotion.STATUT_MANUEL) || currentPromotion().hproStatut().equals(EOHistoPromotion.STATUT_PROVISOIRE))) {
				// Suite à un recalcul, la promotion est devenue provisoire et n'est pas acceptée par l'utilisateur
				currentPromotion().setHproStatut(EOHistoPromotion.STATUT_SUPPRIME);
			}
		} else {
			currentPromotion().modifierAvecResultatPromotion(resultat);
		}
	}
}

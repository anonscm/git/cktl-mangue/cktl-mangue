package org.cocktail.mangue.client.carrieres;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.client.composants.ModalDialogController;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.carriere.CarrieresView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.modalites_services.ModalitesServicesCtrl;
import org.cocktail.mangue.client.outils_interface.DialogueAvecSaisie;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.situation.AffectationOccupationCtrl;
import org.cocktail.mangue.client.specialisations.SpecialisationCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.Promotion;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOConservationAnciennete;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOReliquatsAnciennete;
import org.cocktail.mangue.modele.mangue.individu.EOStage;
import org.cocktail.mangue.modele.mangue.visa.EOVisaTypeArreteCorps;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class CarrieresCtrl {

	private static CarrieresCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private CarrieresView myView;

	private ListenerCarriere listenerCarriere = new ListenerCarriere();
	private ListenerPosition listenerPosition = new ListenerPosition();
	private ListenerElements listenerElement = new ListenerElements();

	private EODisplayGroup 		eod, eodPositions, eodElements;
	private ElementRenderer		elementRenderer = new ElementRenderer();

	private ModalitesServicesCtrl 	modalitesCtrl = null;
	private SpecialisationCtrl 		myCtrlSpec;
		
	private AffectationOccupationCtrl ctrlAff;

	private EOCarriere 				currentCarriere;
	private EOChangementPosition	currentPosition;
	private EOElementCarriere 		currentElement;
	private EOIndividu 				currentIndividu;
	private EOAgentPersonnel		currentUtilisateur;

	public CarrieresCtrl(EOEditingContext editingContext) {

		setEdc(editingContext);
		myCtrlSpec = new SpecialisationCtrl(getEdc());
		ctrlAff = new AffectationOccupationCtrl(getEdc());
		
		eod = new EODisplayGroup();
		eodElements = new EODisplayGroup();
		eodPositions = new EODisplayGroup();
		myView = new CarrieresView(null, MODE_MODAL.booleanValue(),eod, eodPositions, eodElements, elementRenderer);

		myView.getMyEOTable().addListener(listenerCarriere);
		myView.getMyEOTableElement().addListener(listenerElement);
		myView.getMyEOTablePosition().addListener(listenerPosition);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerCarriere();}}
		);

		myView.getBtnAjouterElement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterElement();}}
		);
		myView.getBtnModifierElement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierElement();}}
		);
		myView.getBtnSupprimerElement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerElement();}}
		);
		myView.getBtnRenouveller().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {renouvelerElement();}}
		);
		myView.getBtnImprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
		);
		myView.getBtnAnciennete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherAnciennete();}}
		);
		myView.getBtnPromoCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {promouvoirCorps();}}
		);
		myView.getBtnPromoGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {promouvoirGrade();}}
		);
		myView.getBtnPromoEchelon().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {promouvoirEchelon();}}
		);
		myView.getBtnPromoChevron().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {promouvoirChevron();}}
		);
		myView.getBtnAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherAffectations();}}
		);
		myView.getBtnSpecialisations().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherSpecialisations();}}
		);
		myView.getBtnFicheSynthese().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherFicheSynthese();}}
		);
		myView.getBtnStage().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherStages();}}
		);
		myView.getBtnModalites().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherModalites();}}
		);
		myView.getBtnAjouterPosition().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherPositions();}}
		);
		myView.getBtnModifierPosition().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherPositions();}}
		);
		myView.getBtnSupprimerPosition().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherPositions();}}
		);
		myView.getBtnDetailIndices().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherDetailIndices();}}
		);
				
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		myView.getMyEOTablePosition().setEnabled(false);
		myView.getBtnModifierPosition().setVisible(false);
		myView.getBtnSupprimerPosition().setVisible(false);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		eod.setSortOrderings(PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		myView.getTfSpecialisation().setText("");

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(getEdc(), ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		myView.getBtnAffectation().setText("Aff. / Occ.");

		updateInterface();
	}


	public EOEditingContext getEdc() {
		return edc;
	}


	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}


	public static CarrieresCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new CarrieresCtrl(editingContext);
		return sharedInstance;
	}

	public void employeHasChanged(NSNotification  notification) {
		clearTextFields();
		if (notification != null && notification.object() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(getEdc(),(Number)notification.object(), false));
	}

	public void actualiser() {		
		eod.setObjectArray(EOCarriere.findForIndividu(getEdc(), currentIndividu));
		myView.getMyEOTable().updateData();
		updateInterface();
	
	}

	public NSArray<EOCarriere> getCarrieres() {
		return eod.displayedObjects();
	}

	private void afficherFicheSynthese() {
		NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION, "Carrière");
	}

	/**
	 * 
	 */
	public void afficherStages() {
		//StagesCtrl.sharedInstance(editingContext()).open(currentCarriere());
		GestionStages controleur = new GestionStages(getEdc().globalIDForObject(getCurrentCarriere()));
		controleur.initialiser(false,true);
		ModalDialogController modalDialogController = new ModalDialogController(controleur,"Gestion des Stages");
		controleur.activer(); // 13/01/2011 - déplacer pour un affichage correct des stages en Netbeans
		modalDialogController.activateWindow();
	}
	
	/**
	 * 
	 */
	public void afficherAnciennete() {
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		CRICursor.setWaitCursor(myView);
		AncienneteCtrl.sharedInstance(getEdc()).open(getCurrentElement());
		if (getCurrentElement() != null) {
			NSMutableArray anciennetes = new NSMutableArray();
			anciennetes.addObjectsFromArray(EOReliquatsAnciennete.rechercherReliquatsPourElementCarriere(getEdc(), getCurrentElement()));
			anciennetes.addObjectsFromArray(EOConservationAnciennete.findForElementCarriere(getEdc(), getCurrentElement()));
			myView.getBtnAnciennete().setText("Ancienneté (" + anciennetes.count() + ")" );
		}
		CRICursor.setDefaultCursor(myView);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}


	private void afficherAffectations() {
		ctrlAff.open(getCurrentIndividu());
	}
	private void afficherDetailIndices() {
		DetailsIndicesCtrl.sharedInstance(getEdc()).open(getCurrentElement());
	}
	
	private void afficherSpecialisations() {
		CarriereSpecialisationsCtrl.sharedInstance(getEdc()).open(getCurrentCarriere());
		// Remise a jour de la specialisation
		String spec = myCtrlSpec.getStringSpecForCarriereSpec(getCurrentCarriere().derniereSpecialisation());
		myView.getTfSpecialisation().setVisible(spec != null && spec.length() > 0);
        myView.getTfSpecialisation().setText(spec);                
	}

	
	/**
	 * 
	 */
	public void afficherPositions() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		GestionChangementsPosition controleur = new GestionChangementsPosition(getEdc().globalIDForObject(getCurrentCarriere()));
		controleur.initialiser(false,true);
		ModalDialogController modalDialogController = new ModalDialogController(controleur,"Gestion des Changements de Position");
		controleur.activer();
		modalDialogController.activateWindow();

		eodPositions.setObjectArray(EOChangementPosition.rechercherChangementsPourCarriere(getEdc(), getCurrentCarriere()));
		myView.getMyEOTablePosition().updateData();

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	/**
	 * 
	 */
	public void afficherModalites() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		if (modalitesCtrl == null)
			modalitesCtrl = new ModalitesServicesCtrl(getEdc());
		modalitesCtrl.open(getCurrentIndividu());
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	/**
	 * 
	 * @return
	 */
	private EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	
	/**
	 * 
	 * @param currentIndividu
	 */
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	private EOCarriere getCurrentCarriere() {
		return currentCarriere;
	}
	private void setCurrentCarriere(EOCarriere carriere) {
		currentCarriere = carriere;
	}
	private EOElementCarriere getCurrentElement() {
		return currentElement;
	}
	private void setCurrentElement(EOElementCarriere element) {
		currentElement = element;
	}
	private EOChangementPosition getCurrentPosition() {
		return currentPosition;
	}
	private void setCurrentPosition(EOChangementPosition position) {
		currentPosition = position;
	}
	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		
		this.currentUtilisateur = currentUtilisateur;
		
		// Gestion des droits
		myView.getBtnAjouter().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnModifier().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnSupprimer().setVisible(currentUtilisateur.peutGererCarrieres());

		myView.getBtnStage().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnAffectation().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnAjouterPosition().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnRenouveler().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnAnciennete().setVisible(currentUtilisateur.peutGererCarrieres());

		myView.getBtnPromoCorps().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnPromoGrade().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnPromoEchelon().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnPromoChevron().setVisible(currentUtilisateur.peutGererCarrieres());

		myView.getBtnAjouterElement().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnModifierElement().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnSupprimerElement().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnSupprimer().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnSupprimer().setVisible(currentUtilisateur.peutGererCarrieres());

	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewCarrieres() {
		return myView.getViewCarriere();
	}

	private void ajouter() {
		EOCarriere carriere = SaisieCarriereCtrl.sharedInstance(getEdc()).ajouter(getCurrentIndividu());
		if (carriere != null) {
			// On se replace sur le nouveau contrat
			setCurrentCarriere(carriere);
			EOCarriere carriereEnCours = getCurrentCarriere();
			actualiser();			
			myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(carriereEnCours));
		}
	}

	private void modifier() {		

		SaisieCarriereCtrl.sharedInstance(getEdc()).modifier(currentCarriere);

		myView.getMyEOTable().updateUI();
		listenerCarriere.onSelectionChanged();

	}

	private void ajouterElement() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		SaisieElementCarriereCtrl.sharedInstance(getEdc()).ajouter(getCurrentCarriere());
		listenerCarriere.onSelectionChanged();
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	private void modifierElement() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		SaisieElementCarriereCtrl.sharedInstance(getEdc()).modifier(getCurrentElement());
		myView.getMyEOTableElement().updateUI();
		listenerElement.onSelectionChanged();
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	private void renouvelerElement() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		SaisieElementCarriereCtrl.sharedInstance(getEdc()).renouveler(getCurrentElement());
		listenerCarriere.onSelectionChanged();
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	/**
	 * 
	 */
	private void supprimerElement() {

		if (EODialogs.runConfirmOperationDialog("Attention","Voulez-vous supprimer cet élément","Oui","Non")) {

			try {
				EOElementCarriere.invalider(getEdc(), getCurrentElement());
				getEdc().saveChanges();
				eodElements.deleteSelection();
				myView.getMyEOTableElement().updateData();
//				listenerCarriere.onSelectionChanged();
			}
			catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur","Erreur de suppression d'un élément!\nMESSAGE : " + e.getMessage());
			}

		}
	}

	/**
	 * 
	 */
	private void imprimer() {

		// Vérifier que l'element de carriere a un numéro d'arrete et une date
		boolean shouldSave = false;
		if (getCurrentElement().noArrete() == null) {

			if (EOGrhumParametres.isNoArreteAuto()) {
				if (!EODialogs.runConfirmOperationDialog("Attention", "Le numéro d'arrêté va être généré automatiquement. Est-ce ce que vous voulez ?", "Oui", "Non")) {
					return;
				}
				int annee = DateCtrl.getYear(getCurrentElement().dateDebut());
				getCurrentElement().setNoArrete(EOElementCarriere.getNumeroArreteAutomatique(edc, annee, getCurrentIndividu()));
				shouldSave = true;
			} else {
				DialogueAvecSaisie controleur = new DialogueAvecSaisie("Numéro d'arrêté", "Saisir le numéro d'arrêté");
				controleur.init();
				controleur.afficherFenetre();
				NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("getNoArrete", new Class[] {NSNotification.class}), DialogueAvecSaisie.NOTIFICATION_SAISIE, null);
				return;
			} 
		} 
		if (getCurrentElement().dateArrete() == null) {
			getCurrentElement().setDateArrete(new NSTimestamp());
			shouldSave = true;
		}
		if (shouldSave) {
			try {
				getEdc().saveChanges();
			} catch (Exception e) {
				EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite lors de l'enregistrement des données de l'arrêté. Pour imprimer l'arrêté, veuillez saisir dasn l'élément de carrière le numéro d'arrêté et la date");
				return;
			}
		}

		NSArray destinataires = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinatairesGlobalIds();
		if (destinataires != null) {
			Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class,Boolean.class};
			Object[] parametres = new Object[]{getEdc().globalIDForObject(getCurrentElement()),destinataires,new Boolean(false)};
			try {
				// avec Thread
				UtilitairesImpression.imprimerAvecDialogue(getEdc(),"clientSideRequestImprimerArretePourCarriere",classeParametres,parametres,"ArreteCarriere_" + getCurrentElement().noArrete(),"Impression de l'arrêté");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}		
		}

	}

	// GESTION DES PROMOTIONS
	// on détermine le nouveau corps, grade, echelon puis on ouvre la fenetre de modification avec les nouvelles valeurs
	public void promouvoirCorps() {
		Promotion promotion = Promotion.promotionCorpsPourElementCarriere(getEdc(), getCurrentElement());
		if (promotion != null) {
			if (SaisieElementCarriereCtrl.sharedInstance(getEdc()).promouvoir(getCurrentElement(), promotion) != null )
				listenerCarriere.onSelectionChanged();
		}			
		else
			EODialogs.runInformationDialog("","Pas de promotion de corps possible !");
	}
	public void promouvoirGrade() {
		Promotion promotion = Promotion.promotionGradePourElementCarriere(getEdc(), getCurrentElement());
		if (promotion != null) {
			if (SaisieElementCarriereCtrl.sharedInstance(getEdc()).promouvoir(getCurrentElement(), promotion) != null)
				listenerCarriere.onSelectionChanged();
		}
		else
			EODialogs.runInformationDialog("","Pas de promotion de grade possible !");
	}
	public void promouvoirEchelon() {

		Promotion promotion = Promotion.promotionEchelonPourElementCarriere(getEdc(), getCurrentElement());
		if (promotion != null) {
			if (SaisieElementCarriereCtrl.sharedInstance(getEdc()).promouvoir(getCurrentElement(), promotion) != null)
				listenerCarriere.onSelectionChanged();
		} else {
			EODialogs.runInformationDialog("","Pas de promotion d'échelon possible !");
		}
	}
	public void promouvoirChevron() {
		Promotion promotion = Promotion.promotionChevronPourElementCarriere(getEdc(), getCurrentElement());
		if (promotion != null) {
			if (SaisieElementCarriereCtrl.sharedInstance(getEdc()).promouvoir(getCurrentElement(), promotion) != null)			
				listenerCarriere.onSelectionChanged();
		}
		else
			EODialogs.runInformationDialog("","Pas de promotion de chevron possible !");
	}


	private void supprimerCarriere() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer la carriere sélectionnée et ses élements, positions associés ?", "Oui", "Non"))		
			return;			

		try {
			EOCarriere.invaliderCarriere(getEdc(), getCurrentCarriere());
			getEdc().saveChanges();
			actualiser();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			getEdc().revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			getEdc().revert();
		}
	}

	public void refreshElements() {
		listenerCarriere.onSelectionChanged();
	}

	public void nettoyerChamps(NSNotification notification) {

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();

		eodPositions.setObjectArray(new NSArray());
		myView.getMyEOTablePosition().updateData();

		eodElements.setObjectArray(new NSArray());
		myView.getMyEOTableElement().updateData();

		currentIndividu = null;
		setCurrentCarriere(null);
		setCurrentElement(null);

		updateInterface();

	}

	private void clearTextFields() {
		myView.getTfSpecialisation().setText("");
	}

	/**
	 * 
	 */
	private void updateDatas() {
		
		clearTextFields();
		
		String specialisation = "";
		if (getCurrentCarriere() != null) {
			
			eodPositions.setObjectArray(EOChangementPosition.rechercherChangementsPourCarriere(getEdc(), getCurrentCarriere()));
			eodElements.setObjectArray(EOElementCarriere.findForCarriere(getEdc(), getCurrentCarriere()));

			myView.getBtnStage().setText("Stages (0)");
			if (getCurrentCarriere() != null && getCurrentCarriere().toIndividu() != null)
				myView.getBtnStage().setText("Stages (" + EOStage.findForCarriere(getEdc(), getCurrentCarriere()).size() + ")");

			specialisation = myCtrlSpec.getStringSpecForCarriereSpec(getCurrentCarriere().derniereSpecialisation());
	        CocktailUtilities.setTextToField(myView.getTfSpecialisation(), specialisation);                
	        
		}
		myView.getTfSpecialisation().setVisible(specialisation != null && specialisation.length() > 0);
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerCarriere implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererCarrieres())
				modifier();
		}

		public void onSelectionChanged() {
			
			eodPositions.setObjectArray(new NSArray());
			eodElements.setObjectArray(new NSArray());
			setCurrentCarriere((EOCarriere)eod.selectedObject());

			updateDatas();

			myView.getMyEOTablePosition().updateData();
			myView.getMyTableModelPosition().fireTableDataChanged();
			myView.getMyEOTableElement().updateData();

			updateInterface();
		}
	}

	private class ListenerElements implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererCarrieres()) {
				((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
				SaisieElementCarriereCtrl.sharedInstance(getEdc()).modifier(getCurrentElement());
				myView.getMyEOTableElement().updateUI();
				((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
			}
		}

		/**
		 * 
		 */
		public void onSelectionChanged() {

			setCurrentElement((EOElementCarriere)eodElements.selectedObject());		

			if (getCurrentElement() != null) {
				
				myView.getBtnAnciennete().setText("Ancienneté (0)" );
				if (getCurrentElement() != null) {
					NSMutableArray anciennetes = new NSMutableArray();
					anciennetes.addObjectsFromArray(EOReliquatsAnciennete.rechercherReliquatsPourElementCarriere(getEdc(), getCurrentElement()));
					anciennetes.addObjectsFromArray(EOConservationAnciennete.findForElementCarriere(getEdc(), getCurrentElement()));
					myView.getBtnAnciennete().setText("Ancienneté (" + anciennetes.count() + ")" );
				}
			}

			updateInterface();
		}
	}

	private class ListenerPosition implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
		}

		public void onSelectionChanged() {
			setCurrentPosition((EOChangementPosition)eodPositions.selectedObject());			
			updateInterface();
		}
	}

	/** 
	 * Retourne true si un element est selectionne, qu'on n'est pas en modification, que l'arrete
	 * est gere par l'etablissement, que l'arrete n'est pas annule
	 * @return
	 */
	public boolean peutImprimerArrete() {
		if ( getCurrentElement().dateArreteAnnulation() == null && getCurrentElement().noArreteAnnulation() == null) {
			NSArray<EOVisaTypeArreteCorps> types = EOVisaTypeArreteCorps.findForTypePopulation(getEdc(), getCurrentElement().toCorps().toTypePopulation());
			return types != null && types.size() > 0;
		}

		return false;
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null);
		myView.getBtnModifier().setEnabled(getCurrentCarriere() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentCarriere() != null);

		myView.getBtnStage().setEnabled(getCurrentIndividu() != null && getCurrentCarriere() != null);
		myView.getBtnModalites().setEnabled(getCurrentIndividu() != null && getCurrentCarriere() != null);
		myView.getBtnSpecialisations().setEnabled(getCurrentCarriere() != null);
		myView.getBtnDetailIndices().setEnabled(getCurrentCarriere() != null && getCurrentElement() != null);

		myView.getBtnAjouterPosition().setEnabled(getCurrentCarriere() != null);
		myView.getBtnModifierPosition().setEnabled(getCurrentCarriere() != null && getCurrentPosition() != null);
		myView.getBtnSupprimerPosition().setEnabled(getCurrentCarriere() != null && getCurrentPosition() != null);

		myView.getBtnAjouterElement().setEnabled(getCurrentCarriere() != null);
		myView.getBtnModifierElement().setEnabled(getCurrentElement() != null);
		myView.getBtnSupprimerElement().setEnabled(getCurrentElement() != null);
		myView.getBtnRenouveller().setEnabled(getCurrentElement() != null);
		myView.getBtnImprimer().setEnabled(currentElement != null && peutImprimerArrete());

		myView.getBtnAnciennete().setEnabled(getCurrentElement() != null);
		myView.getBtnPromoEchelon().setEnabled(getCurrentElement() != null);
		myView.getBtnPromoGrade().setEnabled(getCurrentElement() != null);
		myView.getBtnPromoCorps().setEnabled(getCurrentElement() != null);
		myView.getBtnPromoChevron().setEnabled(getCurrentElement() != null);

		myView.getBtnAffectation().setEnabled(getCurrentCarriere() != null);

		myView.getBtnFicheSynthese().setEnabled(getCurrentIndividu() != null);

	}


	private class ElementRenderer extends org.cocktail.application.client.swing.ZEOTableCellRenderer	{

		private static final long serialVersionUID = -2907930349355563787L;

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOElementCarriere obj = (EOElementCarriere) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().get(mdlRow);           

			if (obj.estAnnule())
				leComposant.setForeground(new Color(180,0,0));
			else {
				if (obj.estProvisoire())
					leComposant.setForeground(new Color(100,100,100));
				else
					leComposant.setForeground(new Color(0,0,0));
			}

			return leComposant;
		}
	}


}


package org.cocktail.mangue.client.carrieres;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.carriere.AncienneteReductionView;
import org.cocktail.mangue.client.select.MotifReductionSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOMotifReduction;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOReliquatsAnciennete;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSTimestamp;

public class AncienneteReductionCtrl extends ModelePageGestion {

	private static AncienneteReductionCtrl sharedInstance;
	private AncienneteReductionView myView;
	private EODisplayGroup eod;
	private ListenerReduction listenerReduction = new ListenerReduction();

	private EOElementCarriere currentElement;
	private EOReliquatsAnciennete currentReduction;

	public AncienneteReductionCtrl(EOEditingContext edc) {

		super(edc);
		eod = new EODisplayGroup();

		myView = new AncienneteReductionView(eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getMyEOTable().addListener(listenerReduction);
		setDateListeners(myView.getTfDateArrete());
		setSaisieEnabled(false);
		myView.getCheckReduction().setSelected(true);
		myView.getCheckMajoration().setEnabled(false);
	}


	public static AncienneteReductionCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new AncienneteReductionCtrl(editingContext);
		return sharedInstance;
	}

	public EOElementCarriere getCurrentElement() {
		return currentElement;
	}
	public void setCurrentElement(EOElementCarriere currentElement) {
		this.currentElement = currentElement;
	}
	public EOReliquatsAnciennete getCurrentReduction() {
		return currentReduction;
	}
	public void setCurrentReduction(EOReliquatsAnciennete currentReduction) {
		this.currentReduction = currentReduction;
		updateDatas();
	}

	public JPanel getView() {
		return myView;
	}	

	public void actualiser(EOElementCarriere element)	{

		setCurrentElement(element);
		actualiser();

	}
	public void lock(boolean yn) {

		if (yn) {//lock 
			myView.getBtnAjouter().setEnabled(false);
			myView.getBtnModifier().setEnabled(false);
			myView.getBtnSupprimer().setEnabled(false);
			myView.getMyEOTable().setEnabled(false);			
		}else {	// unlock
			setSaisieEnabled(false);
		}

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerReduction implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()		{
			if (myView.getMyEOTable().isEnabled())
				modifier();
		}
		public void onSelectionChanged() {

			setCurrentReduction((EOReliquatsAnciennete)eod.selectedObject());

		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub

		CocktailUtilities.viderTextField(myView.getTfDateArrete());
		CocktailUtilities.viderTextField(myView.getTfNoArrete());
		CocktailUtilities.viderTextField(myView.getTfAnnee());

		myView.getPopupNbAnnees().setSelectedIndex(0);
		myView.getPopupNbMois().setSelectedIndex(0);
		myView.getPopupNbJours().setSelectedIndex(0);

	}


	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentReduction() != null) {

			CocktailUtilities.setNumberToField(myView.getTfAnnee(), getCurrentReduction().ancAnnee());

			myView.getCheckUtilise().setSelected(getCurrentReduction().estUtilisee());

			myView.getPopupNbAnnees().setSelectedItem(getCurrentReduction().ancNbAnnees());
			myView.getPopupNbMois().setSelectedItem(getCurrentReduction().ancNbMois());
			myView.getPopupNbJours().setSelectedItem(getCurrentReduction().ancNbJours());

			CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentReduction().dArrete());

		}	

		updateInterface();

	}


	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().setEnabled(!isSaisieEnabled());

		myView.getBtnAjouter().setEnabled(!isSaisieEnabled());
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentElement() != null && getCurrentReduction() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentElement() != null && getCurrentReduction() != null);
		myView.getBtnValider().setEnabled(isSaisieEnabled());
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled());

		myView.getPopupNbAnnees().setEnabled(isSaisieEnabled());
		myView.getPopupNbMois().setEnabled(isSaisieEnabled());
		myView.getPopupNbJours().setEnabled(isSaisieEnabled());
		myView.getCheckUtilise().setEnabled(isSaisieEnabled());
		
		myView.getCheckReduction().setEnabled(isSaisieEnabled());

		CocktailUtilities.initTextField(myView.getTfAnnee(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateArrete(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfNoArrete(), false, isSaisieEnabled());

	}


	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub

		eod.setObjectArray(EOReliquatsAnciennete.rechercherReliquatsPourElementCarriere(getEdc(), getCurrentElement()));
		myView.getMyEOTable().updateData();		
		updateInterface();

	}


	@Override
	protected void refresh() {
		// TODO Auto-generated method stub

	}

	private Integer getSelectedAnnees() {
		if (myView.getPopupNbAnnees().getSelectedIndex() > 0) {
			return (Integer)myView.getPopupNbAnnees().getSelectedItem();
		}
		return null;
	}
	private Integer getSelectedMois() {
		if (myView.getPopupNbMois().getSelectedIndex() > 0) {
			return (Integer)myView.getPopupNbMois().getSelectedItem();
		}
		return null;
	}
	private Integer getSelectedJours() {
		if (myView.getPopupNbJours().getSelectedIndex() > 0) {
			return (Integer)myView.getPopupNbJours().getSelectedItem();
		}
		return null;
	}


	@Override
	protected void traitementsAvantValidation() {

			getCurrentReduction().setDArrete(CocktailUtilities.getDateFromField(myView.getTfDateArrete()));
			getCurrentReduction().setNoArrete(CocktailUtilities.getTextFromField(myView.getTfNoArrete()));
			getCurrentReduction().setAncAnnee(CocktailUtilities.getIntegerFromField(myView.getTfAnnee()));
			getCurrentReduction().setEstUtilisee(myView.getCheckUtilise().isSelected());

			getCurrentReduction().setAncNbAnnees(getSelectedAnnees());
			getCurrentReduction().setAncNbMois(getSelectedMois());
			getCurrentReduction().setAncNbJours(getSelectedJours());

	}


	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		AncienneteReductionCtrl.sharedInstance(getEdc()).lock(false);
		AncienneteCtrl.sharedInstance(getEdc()).lock(false);
		AncienneteConservationCtrl.sharedInstance(getEdc()).lock(false);

	}


	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		AncienneteCtrl.sharedInstance(getEdc()).lock(false);
		AncienneteConservationCtrl.sharedInstance(getEdc()).lock(false);

	}


	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}


	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		AncienneteCtrl.sharedInstance(getEdc()).lock(true);
		AncienneteConservationCtrl.sharedInstance(getEdc()).lock(true);
		setCurrentReduction(EOReliquatsAnciennete.creer(getEdc(), getCurrentElement()));

	}


	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		AncienneteCtrl.sharedInstance(getEdc()).lock(true);
		AncienneteConservationCtrl.sharedInstance(getEdc()).lock(true);

	}


	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(currentReduction);

	}


	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub

	}


}

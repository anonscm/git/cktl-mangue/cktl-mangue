/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.carrieres;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.carriere.StagesView;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOStage;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class StagesCtrl extends ModelePageGestion {

	private static final long serialVersionUID = -6490623926473207900L;
	private static StagesCtrl sharedInstance;
	
	private EODisplayGroup eod;
	private StagesView myView;
	private ListenerStage listenerStage = new ListenerStage();
	
	private EOCarriere currentCarriere;
	private EOStage currentStage;
	
	boolean peutModifierDateDebut = true;
	
	/** 
	 *
	 */
	public StagesCtrl (EOEditingContext edc) {

		super(edc);

		eod = new EODisplayGroup();
		myView = new StagesView(new JFrame(), true, eod);
		
		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		CocktailUtilities.initTextField(myView.getTfCorps(), false, false);
		myView.getMyEOTable().addListener(listenerStage);

		setSaisieEnabled(false);
	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static StagesCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new StagesCtrl(editingContext);
		return sharedInstance;
	}
	
				
	private EOStage getCurrentStage() {
		return currentStage;
	}
	
	
	public EOCarriere getCurrentCarriere() {
		return currentCarriere;
	}

	public void setCurrentCarriere(EOCarriere currentCarriere) {
		this.currentCarriere = currentCarriere;
	}

	public void setCurrentStage(EOStage currentStage) {
		this.currentStage = currentStage;
	}
	
	/**
	 * Selection d'un statut parmi une liste de valeurs
	 *
	 */
	public void open(EOCarriere carriere)	{
				
		setCurrentCarriere(carriere);
		clearDatas();
	
		myView.getTfTitre().setText(getCurrentCarriere().toIndividu().nomUsuel() + " " + getCurrentCarriere().toIndividu().prenom() + " - PERIODES DE STAGE");
				
		actualiser();
		
		myView.setVisible(true);
	}

	
	private class ListenerStage implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			if (currentStage != null)
				modifier();
		}
		public void onSelectionChanged() {

			setCurrentStage((EOStage)eod.selectedObject());

		}
	}

	private void updateUI() {
		
		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateValidation(), false, false);

		myView.getBtnGetDateDebut().setEnabled(peutModifierDateDebut && isSaisieEnabled());
		myView.getBtnGetDateFin().setEnabled(isSaisieEnabled());
		myView.getBtnGetDateTitularisation().setEnabled(false);
		
		myView.getBtnAjouter().setEnabled(!isSaisieEnabled());
		myView.getBtnModifier().setEnabled(currentStage != null && !isSaisieEnabled());
		myView.getBtnSupprimer().setEnabled(currentStage != null && !isSaisieEnabled());

		myView.getBtnAnnuler().setEnabled(currentStage != null && isSaisieEnabled());
		myView.getBtnValider().setEnabled(currentStage != null && isSaisieEnabled());
				
	}
	
	/**
	 * Recherche du corps correspondant a la date de debut de stage saisie.
	 * Modification possible de la date de début de stage en fonction des elements de carriere trouves.
	 */
	private void evaluerCorpsEtDateDebut() {

		EOElementCarriere elementTrouve = null;
		NSArray<EOElementCarriere> elements = getCurrentStage().carriere().elementsPourPeriode(getCurrentStage().dateDebut(),getCurrentStage().dateFin());
		if (elements == null || elements.count() == 0) {
			EODialogs.runErrorDialog("Alerte","Pas de corps valable pour cette date !");
		} else {
			// on range les elements du plus petit au plus grand pour pouvoir trouver la date d'effet minimum
			elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
			
			for (EOElementCarriere element : elements) {
				if (element.estProvisoire() == false && element.estAnnule() == false) {
					elementTrouve = element;
					break;
				}
			}
			if (getCurrentStage().corps() == null) {
				// si pas de corps défini et un seul corps l'ajouter automatiquement au stage
				if (elementTrouve == null) {
					EODialogs.runErrorDialog("Alerte","Pas de corps trouvé pour cette date (les éléments de carrière sont provisoires ou annulés)");
				} else {
					//currentStage.setCorpsRelationship(elementTrouve.toCorps());		
					CocktailUtilities.setTextToField(myView.getTfCorps(), elementTrouve.toCorps().llCorps());
					if (DateCtrl.isSameDay(elementTrouve.dateDebut(),getCurrentStage().dateDebut()) == false) {
						getCurrentStage().setDateDebut(elementTrouve.dateDebut());
						myView.getTfDateDebut().setText(DateCtrl.dateToString(currentStage.dateDebut()));
						peutModifierDateDebut = false;
						updateUI();
					}

				}
			}
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getTfCorps().setText("");
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
		myView.getTfDateValidation().setText("");

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentStage() != null) {

			CocktailUtilities.setTextToField(myView.getTfCorps(), getCurrentStage().corps().llCorps());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentStage().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentStage().dateFin());

		}

		updateUI();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		eod.setObjectArray(EOStage.findForCarriere(getEdc(), getCurrentCarriere()));
		myView.getMyEOTable().updateData();

	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		
		getCurrentStage().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentStage().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
		
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentStage(null);

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		evaluerCorpsEtDateDebut();

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		//setCurrentStage(EOStage.creer(getEdc(), getCurrentCarriere()));

	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getCurrentStage().setEstValide(false);

	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}


}
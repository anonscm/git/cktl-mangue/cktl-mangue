// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.carrieres;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.carriere.SaisieElementCarriereView;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.client.select.TypeAccesCorpsSelectCtrl;
import org.cocktail.mangue.client.select.TypeAccesGradeSelectCtrl;
import org.cocktail.mangue.client.select.TypeAccesSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeAccesCorps;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeAccesGrade;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Promotion;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOPassageChevron;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe du controleur des éléments de carrière
 */
public class SaisieElementCarriereCtrl {
	
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieElementCarriereCtrl sharedInstance;
	private EOEditingContext edc;
	private SaisieElementCarriereView myView;

	private PopupEchelonListener listenerEchelons = new PopupEchelonListener();
	private PopupChevronListener listenerChevrons = new PopupChevronListener();
	
	private EOTypePopulation currentTypePopulation;
	private EOElementCarriere currentElement;
	private EOElementCarriere elementAvantPromotion;
	private EOCorps currentCorps;
	private EOGrade currentGrade;

	private EOTypeAcces 		currentAcces;
	private EOTypeAccesCorps 	currentAccesCorps;
	private EOTypeAccesGrade 	currentAccesGrade;
	
	/**
	 * Constructeur
	 * 
	 * @param globalEc : editingContext
	 */
	public SaisieElementCarriereCtrl(EOEditingContext edc) {
		
		setEdc(edc);
		
		myView = new SaisieElementCarriereView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		myView.getBtnGetCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {getCorps();}}
		);
		myView.getBtnGetGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectGrade();}}
		);
		
		myView.getBtnGetAcces().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectTypeAcces();}}
		);
		myView.getBtnDelAcces().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delTypeAcces();}}
		);
		myView.getBtnGetAccesCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectTypeAccesCorps();}}
		);
		myView.getBtnDelAccesCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delTypeAccesCorps();}}
		);
		myView.getBtnGetAccesGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectTypeAccesGrade();}}
		);
		myView.getBtnDelAccesGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delTypeAccesGrade();}}
		);

		myView.getPopupEchelons().addActionListener(listenerEchelons);
		myView.getPopupChevrons().addActionListener(listenerChevrons);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateDebut().addActionListener(new ActionListenerDateTextField(myView.getTfDateDebut()));
		myView.getTfDateFin().addFocusListener(new FocusListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateFin().addActionListener(new ActionListenerDateTextField(myView.getTfDateFin()));
		myView.getTfDateArrete().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArrete()));
		myView.getTfDateArrete().addActionListener(new ActionListenerDateTextField(myView.getTfDateArrete()));
		myView.getTfDateArreteAnnulation().addFocusListener(new FocusListenerDateTextField(myView.getTfDateArreteAnnulation()));
		myView.getTfDateArreteAnnulation().addActionListener(new ActionListenerDateTextField(myView.getTfDateArreteAnnulation()));

		CocktailUtilities.initTextField(myView.getTfLibelleCorps(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleGrade(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeAccesCorps(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeAccesGrade(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeAcces(), false, false);
		CocktailUtilities.initTextField(myView.getTfIndiceBrut(), false, false);
		CocktailUtilities.initTextField(myView.getTfIndiceMajore(), false, false);
		
		//Masque les type d'accès aux corps et grade
		myView.getPanelNouveauTypeAcces().setVisible(false);

	}

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : SaisieElementCarriereCtrl.sharedInstance().methode
	 * 
	 * @param editingContext : editingContext
	 * @return une instance de la classe
	 */
	public static SaisieElementCarriereCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieElementCarriereCtrl(editingContext);
		return sharedInstance;
	}

	
	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	private EOElementCarriere getCurrentElement() {
		return currentElement;
	}
	private void setCurrentElement(EOElementCarriere element) {
		currentElement = element;
	}
	
	public EOElementCarriere elementAvantPromotion() {
		return elementAvantPromotion;
	}

	public void setElementAvantPromotion(EOElementCarriere elementAvantPromotion) {
		this.elementAvantPromotion = elementAvantPromotion;
	}

	private EOCorps getCurrentCorps() {
		return currentCorps;
	}

	private void setCurrentCorps(EOCorps corps) {
		currentCorps = corps;
		myView.getTfLibelleCorps().setText("");
		if (currentCorps != null) {
			getCurrentElement().setToCorpsRelationship(currentCorps);
			CocktailUtilities.setTextToField(myView.getTfLibelleCorps(), currentCorps.llCorps());
		}
	}
	
	private EOGrade getCurrentGrade() {
		return currentGrade;
	}
	private void setCurrentGrade(EOGrade grade) {
		currentGrade = grade;
		myView.getTfLibelleGrade().setText("");
		if (currentGrade != null) {
			getCurrentElement().setToGradeRelationship(getCurrentGrade());
			CocktailUtilities.setTextToField(myView.getTfLibelleGrade(), currentGrade.llGrade());
			majEchelons();
		}
	}
	public EOTypeAccesCorps currentAccesCorps() {
		return currentAccesCorps;
	}
	public void setCurrentAccesCorps(EOTypeAccesCorps currentAccesCorps) {
		this.currentAccesCorps = currentAccesCorps;
		myView.getTfTypeAccesCorps().setText("");
		if (currentAccesCorps != null)
			CocktailUtilities.setTextToField(myView.getTfTypeAccesCorps(), currentAccesCorps.toString());
	}
	public EOTypeAccesGrade currentAccesGrade() {
		return currentAccesGrade;
	}
	public void setCurrentAccesGrade(EOTypeAccesGrade currentAccesGrade) {
		this.currentAccesGrade = currentAccesGrade;
		myView.getTfTypeAccesGrade().setText("");
		if (currentAccesGrade != null)
			CocktailUtilities.setTextToField(myView.getTfTypeAccesGrade(), currentAccesGrade.toString());
	}

	
	public EOTypeAcces currentAcces() {
		return currentAcces;
	}

	public void setCurrentAcces(EOTypeAcces currentAcces) {
		this.currentAcces = currentAcces;
		myView.getTfTypeAcces().setText("");
		if (currentAcces != null) {
			CocktailUtilities.setTextToField(myView.getTfTypeAcces(), currentAcces.toString());
		}
		updateInterface();
	}

	public EOTypePopulation currentTypePopulation() {
		return currentTypePopulation;
	}

	public void setCurrentTypePopulation(EOTypePopulation currentTypePopulation) {
		this.currentTypePopulation = currentTypePopulation;
	}

	
	public String getCurrentEchelon() {
		if (myView.getPopupEchelons().getItemCount() > 0 && myView.getPopupEchelons().getSelectedIndex() > 0) 
			return (String)myView.getPopupEchelons().getSelectedItem();
		
		return null;
	}
	private void setCurrentEchelon(String currentEchelon) {
		myView.getPopupEchelons().setSelectedItem(currentEchelon);
	}
	public String currentChevron() {
		if (myView.getPopupChevrons().getItemCount() > 0 && myView.getPopupChevrons().getSelectedIndex() > 0) 
			return (String)myView.getPopupChevrons().getSelectedItem();
		
		return null;
	}
	private void setCurrentChevron(String currentChevron) {
		myView.getPopupChevrons().setSelectedItem(currentChevron);
	}

	/**
	 * 
	 */
	private void clearTextFields()	{

		myView.getTfLibelleCorps().setText("");
		myView.getTfLibelleGrade().setText("");
		myView.getTfTypeAccesCorps().setText("");
		myView.getTfTypeAccesGrade().setText("");
		myView.getTfTypeAcces().setText("");

		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");

		myView.getTfIndiceBrut().setText("");
		myView.getTfIndiceMajore().setText("");
		myView.getTfIndiceEffectif().setText("");

		myView.getTfDateArrete().setText("");
		myView.getTfDateArreteAnnulation().setText("");
		myView.getTfNoArrete().setText("");
		myView.getTfNoArreteAnnulation().setText("");

		myView.getPopupEchelons().removeAllItems();
		myView.getPopupChevrons().removeAllItems();

	}

	public EOElementCarriere ajouter(EOCarriere carriere)	{

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		myView.setTitle("Eléments de carrière / AJOUT");
		
		clearTextFields();

		NSArray elementsValides = carriere.elementsValides();
		
		setCurrentTypePopulation(carriere.toTypePopulation());
		setCurrentElement(EOElementCarriere.creer(getEdc(), carriere));

		// S'il n'y a pas d'elements on initialise avec la date de la carriere
		if (elementsValides.count() == 0)
			getCurrentElement().setDateDebut(carriere.dateDebut());
		setElementAvantPromotion(null);

		updateDatas();
		updateInterface();
		
		myView.setVisible(true);

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentElement();
	}

	public boolean modifier(EOElementCarriere element) {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		myView.setTitle("Eléments de carrière / MODIFICATION");
		clearTextFields();

		setCurrentTypePopulation(element.toCorps().toTypePopulation());
		setCurrentElement(element);
		setElementAvantPromotion(null);

		updateDatas();
		updateInterface();
		
		myView.setVisible(true);

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentElement() != null;
	}

	public EOElementCarriere renouveler(EOElementCarriere element) {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		clearTextFields();

		setCurrentElement(EOElementCarriere.renouveler(getEdc(), element));
		setCurrentTypePopulation(element.toCorps().toTypePopulation());
		setElementAvantPromotion(null);
		updateDatas();

		updateInterface();
		myView.setVisible(true);		

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentElement();
	}

	/**
	 * 
	 * Promotion de Corps, Grade, Echelon ou Chevron
	 * 
	 * @param element
	 * @param promotion
	 * @return
	 */
	public EOElementCarriere promouvoir(EOElementCarriere element, Promotion promotion)	{

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		myView.setTitle("Eléments de carrière / PROMOTION");

		clearTextFields();

		setElementAvantPromotion(element);
				
		setCurrentTypePopulation(element.toCorps().toTypePopulation());
		setCurrentElement(EOElementCarriere.renouveler(getEdc(), element));
		getCurrentElement().setDateDebut(DateCtrl.jourSuivant(element.dateFin()));
		getCurrentElement().setToCorpsRelationship(promotion.corps());
		getCurrentElement().setToGradeRelationship(promotion.grade());
		getCurrentElement().setCEchelon(promotion.echelon());
		getCurrentElement().setCChevron(promotion.chevron());

		updateDatas();
		updateInterface();
		
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentElement();
	}


	/**
	 * 
	 */
	private void updateDatas() {

		CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentElement().dateDebut());
		CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentElement().dateFin());
		CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentElement().dateArrete());
		CocktailUtilities.setDateToField(myView.getTfDateArreteAnnulation(), getCurrentElement().dateArreteAnnulation());
	
		setCurrentCorps(getCurrentElement().toCorps());
		setCurrentAcces(getCurrentElement().toTypeAcces());
		setCurrentAccesCorps(getCurrentElement().toTypeAccesCorps());
		setCurrentAccesGrade(getCurrentElement().toTypeAccesGrade());
			
		CocktailUtilities.setNumberToField(myView.getTfIndiceEffectif(), getCurrentElement().inmEffectif());
		CocktailUtilities.setTextToField(myView.getTfIndiceBrut(), getCurrentElement().indiceBrut());
		CocktailUtilities.setNumberToField(myView.getTfIndiceMajore(), getCurrentElement().indiceMajore());

		String echelon = getCurrentElement().cEchelon();
		setCurrentGrade(getCurrentElement().toGrade());

		setCurrentEchelon(echelon);
		majChevrons();
		setCurrentChevron(getCurrentElement().cChevron());

		myView.getCheckProvisoire().setSelected(getCurrentElement().estProvisoire());
		
		CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentElement().noArrete());
		CocktailUtilities.setTextToField(myView.getTfNoArreteAnnulation(), getCurrentElement().noArreteAnnulation());
		
	}

	private class PopupEchelonListener implements ActionListener {		
		public void actionPerformed(ActionEvent anAction){
			if (getCurrentElement() != null && getCurrentEchelon() != null) {
				getCurrentElement().setCEchelon(getCurrentEchelon());
				myView.getTfIndiceBrut().setText(getCurrentElement().indiceBrut());
				CocktailUtilities.setNumberToField(myView.getTfIndiceMajore(), getCurrentElement().indiceMajore());
			}
			majChevrons();
			updateInterface();
		}
	}
	private class PopupChevronListener implements ActionListener {		
		public void actionPerformed(ActionEvent anAction){
			if (getCurrentElement() != null && currentChevron() != null) {
				getCurrentElement().setCChevron(currentChevron());
				myView.getTfIndiceBrut().setText(getCurrentElement().indiceBrut());
				CocktailUtilities.setNumberToField(myView.getTfIndiceMajore(), getCurrentElement().indiceMajore());
			}
		}
	}


	private NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	private NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}

	
	protected boolean traitementsAvantValidation() {

		if (getCurrentElement().dateDebut() == null) {
			EODialogs.runErrorDialog("Erreur","Veuillez rentrer une date d'effet !");
			return false;
		}
		if (getCurrentElement().toCorps() == null || currentElement.toGrade() == null) {
			EODialogs.runErrorDialog("Erreur","Veuillez rentrer un corps et un grade !");
			return false;
		}
		if (myView.getPopupEchelons().getItemCount() > 1 && getCurrentEchelon() == null) {
			EODialogs.runErrorDialog("ERREUR", "L'échelon est obligatoire !");
			return false;
		}

		
		// Verifier si le corps saisi est valide pendant toute la periode
		EOCorps corps = getCurrentElement().toCorps();
		if ((getCurrentElement().dateFin() != null && corps.dFermetureCorps() != null && DateCtrl.isAfter(getCurrentElement().dateFin(), corps.dFermetureCorps())) ||
				(getCurrentElement().dateFin() == null && corps.dFermetureCorps() != null) ||
				(corps.dOuvertureCorps()  != null && DateCtrl.isBefore(getCurrentElement().dateDebut(), corps.dOuvertureCorps()))) {
			EODialogs.runErrorDialog("Erreur","Le corps choisi n'est pas valide pendant toute la période");
			return false;
		}
		
		// Verifier si le grade saisi est valide pendant toute la période
		EOGrade grade = getCurrentElement().toGrade();
		if ((getCurrentElement().dateFin() != null && grade.dFermeture() != null && DateCtrl.isAfter(getCurrentElement().dateFin(), grade.dFermeture())) ||
				(getCurrentElement().dateFin() == null && grade.dFermeture() != null) ||
				(grade.dOuverture()  != null && DateCtrl.isBefore(getCurrentElement().dateDebut(), grade.dOuverture()))) {
			EODialogs.runErrorDialog("Erreur","Le grade choisi n'est pas valide pendant toute la période");
			return false;
		}

		if (elementAvantPromotion != null) {
			if (currentElement.cEchelon() != null && getCurrentElement().estAPrendreEnCompte()) {
				// Il s'agit d'une promotion d'echelon, verifier que la date de debut de promotion est valide i.e
				// qu'elle est dans les bornes de l'element de carriere precedent
				// sinon afficher un warning				
				NSTimestamp finPeriode = elementAvantPromotion.dateFin();
				if (finPeriode == null)
					finPeriode = DateCtrl.jourPrecedent(getCurrentElement().dateDebut());
				
				NSTimestamp datePromotion = elementAvantPromotion.datePromotionEchelon(getCurrentElement().dateDebut(), elementAvantPromotion.dateDebut(), finPeriode);
				if (datePromotion == null) {
					EODialogs.runInformationDialog("Attention", "L'individu ne peut avoir de promotion d'échelon à cette date car il n'est pas promouvable");
				} else if (DateCtrl.isBefore(getCurrentElement().dateDebut(), datePromotion)) {
					EODialogs.runInformationDialog("Attention", "L'individu ne peut avoir de promotion d'échelon à cette date.\nLa date de promotion est " + DateCtrl.dateToString(datePromotion));
				}
			}
			
			// verifier les dates de fin de l'element precedant la promotion, les modifier si necessaire
			if (elementAvantPromotion.dateFin() == null || DateCtrl.isAfter(elementAvantPromotion.dateFin(),getCurrentElement().dateDebut())) {
				elementAvantPromotion.setDateFin(DateCtrl.jourPrecedent(getCurrentElement().dateDebut()));
				// vérifier les anciennetes pour signaler eventuellement à l'utilisateur qu'il y a des dates à revoir
				//verifierAnciennete(elementAvantPromotion);
			}
		}

		return true;
	}
	
	
	private void valider()  {

		try {

			getCurrentElement().setDModification(new NSTimestamp());

			getCurrentElement().setDateDebut(getDateDebut());
			getCurrentElement().setDateFin(getDateFin());
			getCurrentElement().setDateArrete(CocktailUtilities.getDateFromField(myView.getTfDateArrete()));
			getCurrentElement().setDateArreteAnnulation(CocktailUtilities.getDateFromField(myView.getTfDateArreteAnnulation()));

			getCurrentElement().setToCorpsRelationship(getCurrentCorps());
			getCurrentElement().setToGradeRelationship(getCurrentGrade());
			getCurrentElement().setToTypeAccesRelationship(currentAcces());
			getCurrentElement().setToTypeAccesCorpsRelationship(currentAccesCorps());
			getCurrentElement().setToTypeAccesGradeRelationship(currentAccesGrade());

			getCurrentElement().setNoArrete(CocktailUtilities.getTextFromField(myView.getTfNoArrete()));
			getCurrentElement().setNoArreteAnnulation(CocktailUtilities.getTextFromField(myView.getTfNoArreteAnnulation()));

			// ECHELONS / CHEVRONS
			getCurrentElement().setCEchelon(getCurrentEchelon());
			getCurrentElement().setCChevron(currentChevron());

			// Notion d'element provisoire
			getCurrentElement().setEstProvisoire(myView.getCheckProvisoire().isSelected());
			
			// INDICES
			getCurrentElement().setInmEffectif(CocktailUtilities.getIntegerFromField(myView.getTfIndiceEffectif()));

			if (!traitementsAvantValidation())
				return;
			
			getEdc().saveChanges();
			myView.setVisible(false);

		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex)   {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	
	private void getCorps() {
		CRICursor.setWaitCursor(myView);
		EOCorps corps = CorpsSelectCtrl.sharedInstance(getEdc()).getCorpsPourTypePopulationEtDate(currentElement.toCarriere().toTypePopulation(), getDateDebut());
		if (corps != null)	{
			setCurrentGrade(null);
			setCurrentCorps(corps);
			updateInterface();
		}
		CRICursor.setDefaultCursor(myView);
	}
	private void selectGrade() {
		EOGrade grade = GradeSelectCtrl.sharedInstance(getEdc()).getGradePourCorps(getCurrentCorps(), getDateDebut());
		if (grade != null) {
			setCurrentGrade(grade);
			updateInterface();
		}
	}
	
	private void selectTypeAcces() {
		EOTypeAcces typeAcces = TypeAccesSelectCtrl.sharedInstance(getEdc()).getTypeAccesValideADate(getDateDebut());
		if (typeAcces != null) {
			currentElement.setToTypeAccesRelationship(typeAcces);
			setCurrentAcces(typeAcces);
		}
	}
	private void delTypeAcces() {
		setCurrentAcces(null);
	}


	private void selectTypeAccesCorps() {
		EOTypeAccesCorps typeAcces = TypeAccesCorpsSelectCtrl.sharedInstance(getEdc()).getTypeAcces();
		if (typeAcces != null) {
			currentElement.setToTypeAccesCorpsRelationship(typeAcces);
			setCurrentAccesCorps(typeAcces);
		}
	}
	private void delTypeAccesCorps() {
		setCurrentAccesCorps(null);
	}
	private void delTypeAccesGrade() {
		setCurrentAccesGrade(null);
	}


	private void selectTypeAccesGrade() {
		EOTypeAccesGrade typeAcces = TypeAccesGradeSelectCtrl.sharedInstance(getEdc()).getTypeAcces();
		if (typeAcces != null) {
			currentElement.setToTypeAccesGradeRelationship(typeAcces);
			setCurrentAccesGrade(typeAcces);
		}
	}

	/**
	 * 
	 */
	private void majEchelons() {

		NSArray<EOPassageEchelon> listePassages = EOPassageEchelon.rechercherPassagesEchelonPourGradesValidesPourPeriode(getEdc(), getCurrentGrade(), getDateDebut(), getDateFin());
		NSMutableArray<String> listeEchelons = new NSMutableArray<String>();
		
		myView.getPopupEchelons().removeActionListener(listenerEchelons);
		myView.getPopupEchelons().removeAllItems();
		myView.getPopupEchelons().addItem("");
		
		for (EOPassageEchelon passage : listePassages) {
			if (!listeEchelons.contains(passage.cEchelon())) {
				myView.getPopupEchelons().addItem(passage.cEchelon());
			}
			listeEchelons.addObject(passage.cEchelon());
		}
		myView.getPopupEchelons().addActionListener(listenerEchelons);
		listenerEchelons.actionPerformed(null);
	}

	private void majChevrons() {

		NSArray<EOPassageChevron> listeChevrons = new NSArray<EOPassageChevron>();
		if (getCurrentGrade() != null && getCurrentEchelon() != null)
			listeChevrons = EOPassageChevron.rechercherPassageChevronOuvertPourGradeEtEchelon(getEdc(), getCurrentGrade().cGrade(), getCurrentEchelon(), getDateDebut(), false);
		myView.getPopupChevrons().removeAllItems();
		myView.getPopupChevrons().addItem("");
		
		for (EOPassageChevron passage : listeChevrons) {
			myView.getPopupChevrons().addItem(passage.cChevron());			
		}
		
	}
	
	private void annuler() {
		currentElement = null;
		getEdc().revert();
		myView.setVisible(false);
	}

	private void updateInterface() {
		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, true);
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, true);

		//Champ désactivé si la date de début n'est pas renseignée
		myView.getBtnGetAcces().setEnabled(getDateDebut() != null);
		myView.getBtnGetCorps().setEnabled(getDateDebut() != null);
		
		myView.getBtnGetGrade().setEnabled(getCurrentCorps() != null);
		myView.getBtnDelAcces().setEnabled(currentAcces() != null);
		
		myView.getBtnGetAccesCorps().setEnabled(false);//currentCorps() != null);
		myView.getBtnGetAccesGrade().setEnabled(false);//currentGrade() != null);

		myView.getBtnDelAccesCorps().setEnabled(currentAccesCorps() != null);
		myView.getBtnDelAccesGrade().setEnabled(currentAccesGrade() != null);

		myView.getPopupEchelons().setEnabled(getCurrentGrade() != null);
		myView.getPopupChevrons().setEnabled(getCurrentGrade() != null && myView.getPopupEchelons().getSelectedIndex() > 0);

	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		} else {
			myTextField.setText(myDate);
			updateInterface();
		}
	}

	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}


}

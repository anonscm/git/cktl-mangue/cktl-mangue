package org.cocktail.mangue.client.contrats;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.contrats.ContratDetailPaieView;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeRemunContrat;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

public class ContratDetailPaieCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;
	private static ContratDetailPaieCtrl sharedInstance;

	private EODisplayGroup eod;	
	private ContratDetailPaieView myView;
	private EOContratAvenant currentAvenant;

	public ContratDetailPaieCtrl(EOEditingContext edc) {

		super(edc);

		eod = new EODisplayGroup();
		myView = new ContratDetailPaieView(new JFrame(), true, eod);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnFermer());

		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeMontant(), EOTypeRemunContrat.fetchAll(getEdc()), true);		
		CocktailUtilities.initTextField(myView.getTfMontant(), false, true);
		CocktailUtilities.initTextField(myView.getTfTauxHoraire(), false, true);
		CocktailUtilities.initTextField(myView.getTfNbUnites(), false, true);

	}

	public static ContratDetailPaieCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new ContratDetailPaieCtrl(editingContext);
		return sharedInstance;
	}

	public EOContratAvenant getCurrentAvenant() {
		return currentAvenant;
	}

	public void setCurrentAvenant(EOContratAvenant currentAvenant) {
		this.currentAvenant = currentAvenant;
		updateDatas();
	}

	/**
	 * 
	 * @param contratAvenant
	 */
	public void open(EOContratAvenant contratAvenant) {

		setCurrentAvenant(contratAvenant);
		myView.setTitle("DONNEES PAIE - Contrat de " + getCurrentAvenant().contrat().individu().identitePrenomFirst());
		myView.setVisible(true);

	}

	@Override
	protected void clearDatas() {

		// TODO Auto-generated method stub
		myView.getPopupTypeMontant().setSelectedIndex(0);

		CocktailUtilities.viderTextField(myView.getTfMontant());
		CocktailUtilities.viderTextField(myView.getTfTauxHoraire());
		CocktailUtilities.viderTextField(myView.getTfNbUnites());

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub

		CocktailUtilities.setNumberToField(myView.getTfMontant(), getCurrentAvenant().montant());
		CocktailUtilities.setNumberToField(myView.getTfNbUnites(), getCurrentAvenant().nbrUnite());
		CocktailUtilities.setNumberToField(myView.getTfTauxHoraire(), getCurrentAvenant().tauxHoraire());
		CocktailUtilities.setNumberToField(myView.getTfMontant(), getCurrentAvenant().montant());
		CocktailUtilities.setNumberToField(myView.getTfMontant(), getCurrentAvenant().montant());
		
		myView.getPopupTypeMontant().setSelectedItem(getCurrentAvenant().toTypeMontant());
		
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 * @return
	 */
	private String getTypeMontant() {
		if (myView.getPopupTypeMontant().getSelectedIndex() > 0) {
			return ((EOTypeRemunContrat)myView.getPopupTypeMontant().getSelectedItem()).code();
		}
		return null;
	}

	@Override
	protected void traitementsAvantValidation() {

		getCurrentAvenant().setMontant(CocktailUtilities.getBigDecimalFromField(myView.getTfMontant()));
		getCurrentAvenant().setTauxHoraire(CocktailUtilities.getBigDecimalFromField(myView.getTfTauxHoraire()));
		getCurrentAvenant().setNbrUnite(CocktailUtilities.getBigDecimalFromField(myView.getTfNbUnites()));
		getCurrentAvenant().setTypeMontant(getTypeMontant());

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
	}
}
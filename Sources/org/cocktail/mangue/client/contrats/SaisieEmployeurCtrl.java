// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.contrats;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.SaisieEmployeurView;
import org.cocktail.mangue.client.select.SaisieStructureCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOCommune;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EOTypeGroupe;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartStructure;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * 
 * Gere la saisie d'un employeur (Nouvel employeur ou selection parmi une liste d'employeurs).
 *  
 * @author cpinsard
 *
 */

public class SaisieEmployeurCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieEmployeurCtrl sharedInstance;
	private SaisieEmployeurView myView;

	private EOStructure 	currentStructure;
	private EOAdresse 		currentAdresse;

	private boolean isNouvelleStructure = false;

	public SaisieEmployeurCtrl(EOEditingContext edc) {

		super(edc);
		myView = new SaisieEmployeurView(new JFrame(), null, true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getTfCodePostal().addFocusListener(new ListenerTextFieldCodePostal());
		myView.getTfCodePostal().addActionListener(new ActionListenerCodePostal());

		CocktailUtilities.initTextField(myView.getTfStructurePere(), false, false);

	}

	public static SaisieEmployeurCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieEmployeurCtrl(editingContext);
		return sharedInstance;
	}

	public EOStructure getCurrentStructure() {
		return currentStructure;
	}

	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
	}

	public EOAdresse getCurrentAdresse() {
		return currentAdresse;
	}

	/**
	 * 
	 * @param currentAdresse
	 */
	public void setCurrentAdresse(EOAdresse currentAdresse) {
		this.currentAdresse = currentAdresse;
		CocktailUtilities.viderTextField(myView.getTfAdresse());
		CocktailUtilities.viderTextField(myView.getTfComplement());
		CocktailUtilities.viderTextField(myView.getTfCodePostal());
		if (currentAdresse != null) {
			CocktailUtilities.setTextToField(myView.getTfAdresse(), currentAdresse.adrAdresse1());
			CocktailUtilities.setTextToField(myView.getTfComplement(), currentAdresse.adrAdresse2());
			CocktailUtilities.setTextToField(myView.getTfCodePostal(), currentAdresse.codePostal());

			getCommune();

			if (currentAdresse.ville() != null)
				myView.getPopupVilles().setSelectedItem(currentAdresse.ville());
			LogManager.logDetail(" - SAISIE EMPLOYEURS - Sélection de la ville : " + currentAdresse.ville());
		}
	}

	/**
	 * Action de valider
	 */
	protected void valider()	{

		try {
			traitementsAvantValidation();
			traitementsApresValidation();
		} catch (ValidationException ex)	{
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		} catch (Exception e)	{
			e.printStackTrace();
			return;
		}
	}

	/**
	 * 
	 * @return
	 */
	public EOStructure ajouter()	{

		clearDatas();

		setModeCreation(true);

		EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
		setCurrentStructure(SaisieStructureCtrl.sharedInstance(getEdc()).getStructure(agent));
		isNouvelleStructure = SaisieStructureCtrl.sharedInstance(getEdc()).isNouvelleStructure(); 

		if (getCurrentStructure() != null) {

			if (!isNouvelleStructure)
				return currentStructure;

			NSMutableArray<EORepartTypeGroupe> typesGroupe = new NSMutableArray();
			EORepartTypeGroupe typeGroupeGroupe = EORepartTypeGroupe.creer(getEdc(), getCurrentStructure(), (EOTypeGroupe)NomenclatureFinder.findForCode(getEdc(), EOTypeGroupe.ENTITY_NAME, EOTypeGroupe.TYPE_GROUPE_GROUPE), agent);
			if (typeGroupeGroupe != null)
				typesGroupe.addObject(typeGroupeGroupe);
			EORepartTypeGroupe typeGroupeEntreprises = EORepartTypeGroupe.creer(getEdc(), getCurrentStructure(), (EOTypeGroupe)NomenclatureFinder.findForCode(getEdc(), EOTypeGroupe.ENTITY_NAME, EOTypeGroupe.TYPE_GROUPE_ENTREPRISE), agent);
			if (typeGroupeEntreprises != null)
				typesGroupe.addObject(typeGroupeEntreprises);

			setCurrentAdresse(EORepartPersonneAdresse.rechercherAdresse(getEdc(), getCurrentStructure(), EOTypeAdresse.TYPE_PROFESSIONNELLE));
			if (getCurrentAdresse() == null)
				setCurrentAdresse(EORepartPersonneAdresse.rechercherAdresse(getEdc(), getCurrentStructure(), EOTypeAdresse.TYPE_FACTURATION));

			setSaisieAdresseEnabled(isNouvelleStructure);
			updateDatas();

			myView.setVisible(true);

			return getCurrentStructure();

		}

		return null;
	}

	/**
	 * Modification d'un employeur
	 * 
	 * @param structure
	 * @param adresse
	 * @return
	 */
	public boolean modifier(EOStructure structure, EOAdresse adresse) {

		clearDatas();
		setCurrentStructure(structure);
		setCurrentAdresse(adresse);

		EOStructure structureEmployeurs = EOStructure.structurePourCode(getEdc(), EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_EMPLOYEURS));
		EORepartStructure repartEmployeur = EORepartStructure.rechercherPourPersIdEtStructure(getEdc(), getCurrentStructure().persId(), structureEmployeurs);

		setSaisieAdresseEnabled(repartEmployeur != null);

		updateDatas();
		setModeCreation(false);
		myView.setVisible(true);
		return getCurrentStructure() != null;
	}

	/**
	 * 
	 * @param yn
	 */
	private void setSaisieAdresseEnabled(boolean yn) {

		CocktailUtilities.initTextField(myView.getTfAdresse(), false, yn);
		CocktailUtilities.initTextField(myView.getTfComplement(), false, yn);
		CocktailUtilities.initTextField(myView.getTfCodePostal(), false, yn);
		myView.getPopupVilles().setEnabled(yn);

	}

	/**
	 * 
	 */
	public void getCommune()	{

		NSArray<EOCommune> communes = EOCommune.rechercherCommunes(getEdc(), myView.getTfCodePostal().getText());

		myView.getPopupVilles().removeAllItems();
		for (EOCommune commune : communes)
			myView.getPopupVilles().addItem(commune.libelle());
	}

	public class ActionListenerCodePostal implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			getCommune();
		}
	}
	public class ListenerTextFieldCodePostal implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e)		{
		}
		public void focusLost(FocusEvent e)	{
			getCommune();
		}
	}
	@Override
	protected void clearDatas() {

		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfLibelle());
		CocktailUtilities.viderTextField(myView.getTfLibelleCourt());
		CocktailUtilities.viderTextField(myView.getTfAdresse());
		CocktailUtilities.viderTextField(myView.getTfComplement());
		CocktailUtilities.viderTextField(myView.getTfCodePostal());

		myView.getPopupVilles().removeAllItems();

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.setTextToField(myView.getTfLibelle(), getCurrentStructure().llStructure());
		CocktailUtilities.setTextToField(myView.getTfLibelleCourt(), getCurrentStructure().lcStructure());
		CocktailUtilities.setTextToField(myView.getTfSiret(), getCurrentStructure().siret());

		if (getCurrentStructure().toStructurePere() != null)
			CocktailUtilities.setTextToField(myView.getTfStructurePere(), getCurrentStructure().toStructurePere().llStructure());

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsAvantValidation() {

		String siret = CocktailUtilities.getTextFromField(myView.getTfSiret());

		if (CocktailUtilities.getTextFromField(myView.getTfSiret()) == null && EOGrhumParametres.isSiretObligatoire()) {
			throw new NSValidation.ValidationException("Le numéro de SIRET est obligatoire !");
		}

		if (siret != null) {
			// Controle de l'existence du numero SIRET
			if (EOGrhumParametres.findParametre(getEdc(), ManGUEConstantes.GRHUM_PARAM_KEY_CONTROLE_SIRET).isParametreVrai()) {
				if (EOStructure.estSiretValide(CocktailUtilities.getTextFromField(myView.getTfSiret())) == false) {
					throw new NSValidation.ValidationException( "Le numéro de Siret est invalide !");
				}
			}
			// Vérifier qu'il n'y ait pas d'autres structures avec le même numéro de Siret
			NSArray<EOStructure> structures = EOStructure.rechercherStructuresAvecSiret(getEdc(), siret);
			if (structures.count() > 1 || (structures.count() == 1 && structures.get(0) != getCurrentStructure())) {
				throw new NSValidation.ValidationException("Une autre structure a déjà ce numéro de siret.\n" + structures.get(0).llStructure());
			}
		}

		// TODO Auto-generated method stub
		getCurrentStructure().setLlStructure(CocktailUtilities.getTextFromField(myView.getTfLibelle()));
		getCurrentStructure().setLcStructure(CocktailUtilities.getTextFromField(myView.getTfLibelleCourt()));
		getCurrentStructure().setSiret(CocktailUtilities.getTextFromField(myView.getTfSiret()));

		// Gestion des adresses
		if (isModeCreation() && getCurrentAdresse() == null) {

			if (myView.getTfCodePostal().getText().length() > 0 && myView.getPopupVilles().getComponentCount() > 0) {

				// Creation de l'adresse
				EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
				EOAdresse newAdresse = new EOAdresse();
				newAdresse.setAdrOrdre(new Integer(EOAdresse.construireAdresseOrdre(getEdc()).intValue()));
				newAdresse.setPersIdCreation(agent.toIndividu().persId());
				newAdresse.setPersIdModification(agent.toIndividu().persId());
				newAdresse.init();
				getEdc().insertObject(newAdresse);

				EORepartPersonneAdresse rpa = new EORepartPersonneAdresse();
				rpa.initAdresseEntreprise(getCurrentStructure().persId(),newAdresse);
				getEdc().insertObject(rpa);

				newAdresse.setAdrAdresse1(CocktailUtilities.getTextFromField(myView.getTfAdresse()));
				newAdresse.setAdrAdresse2(CocktailUtilities.getTextFromField(myView.getTfComplement()));
				newAdresse.setCodePostal(CocktailUtilities.getTextFromField(myView.getTfCodePostal()));
				newAdresse.setVille((String)myView.getPopupVilles().getSelectedItem());
				newAdresse.setToPaysRelationship(EOPays.getDefault(getEdc()));

				currentAdresse = newAdresse;
			}
		}
		else {

			// Modification de l'adresse. Seulement pour les structures du groupe EMPLOYEURS
			EOStructure structureEmployeurs = EOStructure.structurePourCode(getEdc(), EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_EMPLOYEURS));
			EORepartStructure repartEmployeur = EORepartStructure.rechercherPourPersIdEtStructure(getEdc(), getCurrentStructure().persId(), structureEmployeurs);
			if (repartEmployeur != null) {

				EOAdresse myAdresse = getCurrentAdresse();

				if (myAdresse == null) {
					// Creation de l'adresse
					EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
					myAdresse= EOAdresse.creer(getEdc(), agent);
					EORepartPersonneAdresse.creer(getEdc(), getCurrentStructure().persId(), myAdresse, EOTypeAdresse.TYPE_PROFESSIONNELLE);
				}

				myAdresse.setAdrAdresse1(CocktailUtilities.getTextFromField(myView.getTfAdresse()));
				myAdresse.setAdrAdresse2(CocktailUtilities.getTextFromField(myView.getTfComplement()));
				myAdresse.setCodePostal(CocktailUtilities.getTextFromField(myView.getTfCodePostal()));
				if (myView.getPopupVilles().getComponentCount() > 0)
					myAdresse.setVille((String)myView.getPopupVilles().getSelectedItem());

				setCurrentAdresse(myAdresse);

			}
		}

		myView.setVisible(false);
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub

		// Ajout du nouvel employeur dans le groupe EMPLOYEURS
		try {
			EOStructure structureEmployeurs = EOStructure.structurePourCode(getEdc(), EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_EMPLOYEURS));
			EORepartStructure repartEmployeur = EORepartStructure.rechercherPourPersIdEtStructure(getEdc(), getCurrentStructure().persId(), structureEmployeurs);

			if (repartEmployeur == null) {
				EORepartStructure.creer(getEdc(), getCurrentStructure().persId(), structureEmployeurs);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}


	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentStructure(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

}

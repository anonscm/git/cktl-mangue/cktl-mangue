package org.cocktail.mangue.client.contrats;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.mangue.client.gui.EmployeursView;
import org.cocktail.mangue.client.individu.infosperso.IndividuConjointCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartStructure;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class EmployeursCtrl {
	private static final long serialVersionUID = 0x7be9cfa4L;

	private EOEditingContext edc;
	private EmployeursView myView;

	private String classeParent;
	private boolean saisieEnabled;
	private EOStructure currentStructure;
	private EOAdresse currentAdresse;
	
	private boolean membreEmployeurs;
	private EOStructure structureEmployeurs;
	
	private IndividuConjointCtrl ctrlParentConjoint;

	public EmployeursCtrl(EOEditingContext edc, String title) {

		this.edc = edc;

		myView = new EmployeursView(new JFrame(), true);
		myView.getTfTitre().setText(title);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
		);
		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		CocktailUtilities.initTextField(myView.getTfLibelleStructure(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleCourtStructure(), false, false);
		CocktailUtilities.initTextField(myView.getTfSiret(), false, false);

		structureEmployeurs = EOStructure.structurePourCode(edc, EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_EMPLOYEURS));
		setSaisieEnabled(false);

	}

	public boolean isMembreEmployeurs() {
		return membreEmployeurs;
	}

	public void setMembreEmployeurs(boolean membreEmployeurs) {
		this.membreEmployeurs = membreEmployeurs;
	}

	public EOStructure getStructureEmployeurs() {
		return structureEmployeurs;
	}

	public void setStructureEmployeurs(EOStructure structureEmployeurs) {
		this.structureEmployeurs = structureEmployeurs;
	}

	private String classeParent(){
		return classeParent;
	}
	public void setClasseParent(String classeParent) {
		this.classeParent = classeParent;
	}

	public void setCtrlConjoint(IndividuConjointCtrl ctrlParentConjoint) {
		this.ctrlParentConjoint = ctrlParentConjoint;
	}

	public EOStructure getCurrentStructure() {
		return currentStructure;
	}
	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;

		setMembreEmployeurs(false);
		if (currentStructure != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleStructure(), currentStructure.llStructure());
			CocktailUtilities.setTextToField(myView.getTfLibelleCourtStructure(), currentStructure.lcStructure());
			CocktailUtilities.setTextToField(myView.getTfSiret(), currentStructure.siret());
			
			setMembreEmployeurs(EORepartStructure.rechercherPourPersIdEtStructure(edc, currentStructure.persId(), structureEmployeurs) != null);
		}

	}
	public EOAdresse getCurrentAdresse() {
		return currentAdresse;
	}
	public void setCurrentAdresse(EOAdresse currentAdresse) {
		this.currentAdresse = currentAdresse;
	}

	/**
	 * 
	 */
	public void clearDatas()	{
		CocktailUtilities.viderLabel(myView.getLblAdresse());
		CocktailUtilities.viderTextField(myView.getTfLibelleCourtStructure());
		CocktailUtilities.viderTextField(myView.getTfLibelleStructure());
		CocktailUtilities.viderTextField(myView.getTfSiret());
	}

	private boolean saisieEnabled() {
		return saisieEnabled;
	}
	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	public JPanel getView() {
		return myView.getViewEmployeurs();
	}

	public EOStructure getSelectedStructure() {
		return currentStructure;
	}

	public EOStructure getEmployeur(String title, EOStructure structure, boolean peutAjouter)	{

		myView.getTfTitre().setText(title);

		clearDatas();

		setCurrentStructure(structure);
		updateData();
		myView.setVisible(true);

		return getSelectedStructure();
	}

	/**
	 * 
	 */
	public void valider() {
		myView.dispose();
	}  

	public void annuler() {
		currentStructure = null;
		myView.dispose();
	}  

	private void ajouter() {
		
		EOStructure newStructure = SaisieEmployeurCtrl.sharedInstance(edc).ajouter();
		
		if (newStructure != null) {				
			setCurrentStructure(newStructure);
			setCurrentAdresse(SaisieEmployeurCtrl.sharedInstance(edc).getCurrentAdresse());
			updateData();
			updateInterface();
		}
	}
	private void modifier() {
		if (SaisieEmployeurCtrl.sharedInstance(edc).modifier(currentStructure, currentAdresse)) {
			currentAdresse = SaisieEmployeurCtrl.sharedInstance(edc).getCurrentAdresse();
			updateData();
		}
	}
	
	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cet employeur ?", "Oui", "Non"))		
			return;			

		if (classeParent().equals(IndividuConjointCtrl.class.getName()))
			ctrlParentConjoint.supprimerEmployeur();

		clearDatas();
		setCurrentStructure(null);
		setCurrentAdresse(null);
		updateInterface();
	}

	/**
	 * 
	 * @param structure
	 * @param adresse
	 */
	public void update (EOStructure structure, EOAdresse adresse) {

		setCurrentStructure(null);
		setCurrentAdresse(null);
		clearDatas();

		if (structure != null) {
			setCurrentStructure(structure);
						
			updateData();
		}

		updateInterface();
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnAjouter().setEnabled(saisieEnabled() && getCurrentStructure() == null);
		myView.getBtnModifier().setEnabled(saisieEnabled() && getCurrentStructure() != null && isMembreEmployeurs());
		myView.getBtnSupprimer().setEnabled(saisieEnabled() && getCurrentStructure() != null);
		
	}

	/**
	 * 
	 */
	private void updateData() {

		if (getCurrentStructure() != null) {
			
			// Adresse Professionnelle ou de facturation
			if (getCurrentAdresse() == null) {
				setCurrentAdresse(EORepartPersonneAdresse.rechercherAdresse(edc, getCurrentStructure(), EOTypeAdresse.TYPE_PROFESSIONNELLE));
				if (getCurrentAdresse() == null) {
					setCurrentAdresse(EORepartPersonneAdresse.rechercherAdresse(edc, getCurrentStructure(), EOTypeAdresse.TYPE_FACTURATION));
				}
			}

			if (getCurrentAdresse() != null) {
				CocktailUtilities.setTextToLabel(myView.getLblAdresse(), getCurrentAdresse().adresseComplete());
			}
		}
	}

}
package org.cocktail.mangue.client.contrats;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.contrats.ContratsView;
import org.cocktail.mangue.client.situation.AffectationOccupationCtrl;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOContratPeriodesEssai;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSValidation.ValidationException;

/** Gestion des contrats
 * Regles de gestion des contrats<BR>
 * Les types de contrat proposes varient selon le statut de l'agent durant la periode saisie pour le contrat :<BR>
 * Si il a des segments de carriere, on propose uniquement les types pour titulaires valables aux dates de debut et fin de contrat. Sinon
 * on propose tous les types de contrat valables a ces dates.<BR>
 * Si un agent a un contrat de type "Titulaire" => il est forcement fonctionnaire.<BR> Dans les autres cas, si le type de contrat est ouvert aux
 * fonctionnaires, alors on laisse la possibilite de signaler dans le contrat que l'agent est fonctionnaire (ATER par exemple)
 * Un contrat ne peut etre modifie que si il ne comporte pas de details de contrat.Dans le cas contraire, seule la date de fin de peut
 * etre modifiee.<BR>
 * Un contrat ne peut etre renouvele que si son type de contrat le permet<BR>
 * On peut supprimer un detail de contrat qui n'a pas d'arrete signe, sinon il faut l'annuler<BR>
 * Fermeture des dates du contrat precedent lors de l'enregistrement des donnees:<BR>
 * Lorsqu'on ajoute un nouveau contrat ou qu'on modifie ses dates :<BR>
 * si il existe un contrat precedent qui se termine apres la date de debut
 * du contrat courant, on verifie si la quotite du dernier detail de contrat du contrat precedent 
 * est 100%. Dans ce cas, on modifie apres avoir demande son accord a l'utilisateur 
 * la date de fin du detail de contrat du contrat precedent et du contrat<BR>
 * On ne peut ajouter des affectations et occupations que lorsqu'au moins un detail de contrat ou une vacation est definie<R>
 * Les regles de validation des contrats sont decrites dans la classe EOContrat<BR>
 * Impressions : on ne peut imprimer le contrat de travail que si l'individu occupe un emploi vacant ou de type rompu, qu'un detail de contrat
 * correspondant au type de contrat a imprimer est selectionne et que cet detail de contrat a un numero d'arrete.<BR>
 * Gestion des numeros d'arrete : pour les vacations, il n'y a pas de numerotation automatique, l'utilisateur doit fournir le numero d'arrete.
 * Pour les avenants, l'utilisateur est consulte si la numerotation n'est pas automatique, sinon le numero d'arrete est
 * calcule en fonction des numeros deja fournis dans les avenants de l'annee : le format est AAAA/numero ou AAAA represente l'annee.
 *
 */

public class ContratsCtrl {

	private static ContratsCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private ContratsView myView;

	private ListenerContrat listenerContrat = new ListenerContrat();
	private EODisplayGroup eod;
	private AffectationOccupationCtrl ctrlAff;

	private EOContrat 			currentContrat;
	private EOIndividu 			currentIndividu;
	private EOAgentPersonnel	currentUtilisateur;

	/**
	 * 
	 * @param editingContext
	 */
	public ContratsCtrl(EOEditingContext editingContext) {

		edc = editingContext;

		eod = new EODisplayGroup();
		myView = new ContratsView(null, MODE_MODAL.booleanValue(),eod);
		ctrlAff = new AffectationOccupationCtrl(edc);

		myView.getMyEOTable().addListener(listenerContrat);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnDupliquer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {renouveler();}}
				);
		myView.getBtnAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherAffectations();}}
				);
		myView.getBtnFicheSynthese().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherFicheSynthese();}}
				);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		CocktailUtilities.initTextField(myView.getTfUai(), false, false);
		CocktailUtilities.initTextArea(myView.getTaCommentaires(), false, false);
		CocktailUtilities.initTextField(myView.getTfFinancement(), false, false);

		myView.getSwapViewDetail().add("VIDE",new JPanel(new BorderLayout()));
		myView.getSwapViewDetail().add("AVENANTS",ContratsAvenantsCtrl.sharedInstance(edc).getView());
		myView.getTfTitreDetailContrat().setText("DETAILS DU CONTRAT");

		((CardLayout)myView.getSwapViewDetail().getLayout()).show(myView.getSwapViewDetail(), "VIDE");				

		eod.setSortOrderings(PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		updateInterface();
	}


	public static ContratsCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ContratsCtrl(editingContext);
		return sharedInstance;
	}


	public void open(EOIndividu individu)	{

		clearDatas();
		setCurrentIndividu(individu);
		if (getCurrentIndividu() != null) {
			actualiser();
		}

		myView.setVisible(true);		
	}


	public void employeHasChanged(NSNotification  notification) {
		clearDatas();
		if (notification != null && notification.object() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
			actualiser();
		}
	}

	/**
	 * 
	 */
	public void actualiser() {
		eod.setObjectArray(EOContrat.rechercherContratsPourIndividu(edc, getCurrentIndividu(), false));
		myView.getMyEOTable().updateData();
		updateInterface();
	}

	private void afficherFicheSynthese() {
		NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,"Carrière");
	}
	private void afficherAffectations() {
		ctrlAff.open(getCurrentIndividu());
	}

	private EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	private EOContrat getCurrentContrat() {
		return currentContrat;
	}
	private void setCurrentContrat(EOContrat contrat) {
		currentContrat = contrat;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public EOAgentPersonnel currentUtilisateur() {
		return currentUtilisateur;
	}

	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;

		// Gestion des droits
		myView.getBtnAjouter().setVisible(currentUtilisateur.peutGererContrats());
		myView.getBtnModifier().setVisible(currentUtilisateur.peutGererContrats());
		myView.getBtnSupprimer().setVisible(currentUtilisateur.peutGererContrats());
		myView.getBtnAffectation().setVisible(currentUtilisateur.peutGererContrats());
		myView.getBtnDupliquer().setVisible(currentUtilisateur.peutGererContrats());

		ContratsAvenantsCtrl.sharedInstance(edc).setCurrentUtilisateur(currentUtilisateur);
	}


	public JFrame getView() {
		return myView;
	}
	public JPanel getViewContrats() {
		return myView.getViewContrats();
	}
	public NSArray<EOContrat> getContrats() {
		return eod.displayedObjects();
	}


	private void ajouter() {		

		EOContrat contrat = SaisieContratCtrl.sharedInstance(edc).ajouter(getCurrentIndividu());
		if (contrat != null) {
			// On se replace sur le nouveau contrat
			setCurrentContrat(contrat);
			EOContrat contratEnCours = getCurrentContrat();
			actualiser();			
			myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(contratEnCours));
		}

	}

	/**
	 * 
	 */
	private void modifier() {		

		SaisieContratCtrl.sharedInstance(edc).modifier(currentContrat);

		myView.getMyEOTable().updateUI();
		listenerContrat.onSelectionChanged();

	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer le contrat sélectionné et ses détails ?", "Oui", "Non"))		
			return;			

		try {

			for (Enumeration<EOContratAvenant> e = EOContratAvenant.findForContrat(edc, currentContrat).objectEnumerator();e.hasMoreElements();) {
				e.nextElement().setTemAnnulation("O");
			}

			currentContrat.setTemAnnulation("O");

			edc.saveChanges();
			actualiser();

		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			edc.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
		}
	}

	public void refreshAvenants() {
		listenerContrat.onSelectionChanged();
	}

	/**
	 * 
	 */
	private void renouveler() {

		try {			
			EOContrat contrat = SaisieContratCtrl.sharedInstance(edc).renouveler(getCurrentContrat());
			if (contrat  != null)
				contrat.setToIndividuRelationship(currentIndividu);
			if (contrat != null) {
				setCurrentContrat(contrat);
				EOContrat contratEnCours = getCurrentContrat();
				actualiser();
				myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(contratEnCours));
			}
		}
		catch (Exception e) {
		}
	}

	/**
	 * 
	 * @param notification
	 */
	public void nettoyerChamps(NSNotification notification) {

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();

		currentIndividu = null;
		updateInterface();

	}

	private void clearDatas() {

		CocktailUtilities.viderTextField(myView.getTfUai());
		CocktailUtilities.viderLabel(myView.getLblPeriodeEssai());
		CocktailUtilities.viderTextField(myView.getTfFinancement());

		((CardLayout)myView.getSwapViewDetail().getLayout()).show(myView.getSwapViewDetail(), "VIDE");				

	}


	/**
	 * 
	 */
	private void updateDatas() {

		clearDatas();

		if (getCurrentContrat().toRne() != null)
			CocktailUtilities.setTextToField(myView.getTfUai(), getCurrentContrat().toRne().libelleLong());

		CocktailUtilities.setTextToArea(myView.getTaCommentaires(), getCurrentContrat().commentaire());
		if (getCurrentContrat().toOrigineFinancement() != null)
			CocktailUtilities.setTextToField(myView.getTfFinancement(), getCurrentContrat().toOrigineFinancement().libelleLong());

		EOContratPeriodesEssai periodeEssai = EOContratPeriodesEssai.getLastPeriodeEssai(edc, getCurrentContrat());
		if (periodeEssai != null) {
			CocktailUtilities.setTextToLabel(myView.getLblPeriodeEssai(), 
					"Période d'essai jusqu'au " + DateCtrl.dateToString(periodeEssai.dateFin()));
		}
		
	}

	private class ListenerContrat implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			if (currentUtilisateur().peutGererContrats())
				modifier();
		}

		public void onSelectionChanged() {

			clearDatas();
			setCurrentContrat((EOContrat)eod.selectedObject());

			if (getCurrentContrat() != null) {
				updateDatas();

				myView.getTfTitreDetailContrat().setText("DETAILS DU CONTRAT");
				((CardLayout)myView.getSwapViewDetail().getLayout()).show(myView.getSwapViewDetail(), "AVENANTS");				
				ContratsAvenantsCtrl.sharedInstance(edc).setCurrentContrat(getCurrentContrat());
			}
			else {
				CocktailUtilities.viderTextField(myView.getTfTitreDetailContrat());
				((CardLayout)myView.getSwapViewDetail().getLayout()).show(myView.getSwapViewDetail(), "VIDE");				
				ContratsAvenantsCtrl.sharedInstance(edc).setCurrentContrat(null);
			}

			updateInterface();
		}
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null);
		myView.getBtnModifier().setEnabled(getCurrentContrat() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentContrat() != null);
		myView.getBtnDupliquer().setEnabled(getCurrentContrat() != null);

		myView.getBtnAffectation().setEnabled(getCurrentContrat() != null);
		myView.getBtnFicheSynthese().setEnabled(currentIndividu != null);

	}
}

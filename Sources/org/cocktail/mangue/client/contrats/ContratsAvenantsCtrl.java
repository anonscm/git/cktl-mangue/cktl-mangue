// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirIdentiteCtrl.java

package org.cocktail.mangue.client.contrats;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.contrats.ContratsAvenantsView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.specialisations.SpecialisationCtrl;
import org.cocktail.mangue.client.templates.ModelePageConsultation;
import org.cocktail.mangue.common.utilities.AskForString;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOActivitesTypeContrat;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;
import org.cocktail.mangue.modele.mangue.individu.EORepartCtrActivites;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class ContratsAvenantsCtrl extends ModelePageConsultation {

	private static ContratsAvenantsCtrl sharedInstance;

	private ContratsAvenantsView myView;
	private EODisplayGroup eod;
	private String noArrete;
	private SpecialisationCtrl myCtrlSpec;
	private EOContrat currentContrat;
	private EOContratAvenant currentAvenant;
	private ListenerAvenant listenerAvenant = new ListenerAvenant();
	private NSArray activitesPourTypeContrat;

	/**
	 * 
	 * @param editingContext
	 */
	public ContratsAvenantsCtrl(EOEditingContext edc)	{

		super(edc);
		myCtrlSpec = new SpecialisationCtrl(edc);

		eod = new EODisplayGroup();
		myView = new ContratsAvenantsView(eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());

		myView.getBtnDetailPaie().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherDetailPaie();}}
				);
		myView.getBtnRenouveler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {renouveler();}}
				);
		myView.getBtnImprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
				);
		myView.getBtnActivites().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherActivites();}}
				);

		myView.getMyEOTable().addListener(listenerAvenant);
		CocktailUtilities.viderLabel(myView.getLblSpecialite());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ContratsAvenantsCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new ContratsAvenantsCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 * @return
	 */
	public EOContrat getCurrentContrat() {
		return currentContrat;
	}
	public void setCurrentContrat(EOContrat currentContrat) {
		this.currentContrat = currentContrat;
		updateDatas();
	}

	public EOContratAvenant getCurrentAvenant() {
		return currentAvenant;
	}
	public void setCurrentAvenant(EOContratAvenant avenant) {
		currentAvenant = avenant;
	}

	/**
	 * 
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		myView.getBtnAjouter().setVisible(currentUtilisateur.peutGererContrats());
		myView.getBtnModifier().setVisible(currentUtilisateur.peutGererContrats());
		myView.getBtnSupprimer().setVisible(currentUtilisateur.peutGererContrats());
		myView.getBtnRenouveler().setVisible(currentUtilisateur.peutGererContrats());
		myView.getBtnImprimer().setVisible(currentUtilisateur.peutGererContrats());
		myView.getBtnDetailPaie().setVisible(currentUtilisateur.peutGererContrats());
	}

	public JPanel getView() {
		return myView;
	}	

	/**
	 * 
	 */
	private void afficherDetailPaie() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		ContratDetailPaieCtrl.sharedInstance(getEdc()).open(getCurrentAvenant());
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}

	/**
	 * 
	 */
	private void afficherActivites() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		ContratActivitesCtrl.sharedInstance(getEdc()).open(getCurrentAvenant());

		NSArray<EORepartCtrActivites> activites = EORepartCtrActivites.rechercherRepartsActivitesPourAvenant(getCurrentAvenant(), true);
		myView.getBtnActivites().setText("Activités (" + activites.count() +")");

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}

	/**
	 * 
	 */
	private void imprimer() {

		CRICursor.setWaitCursor(myView);
		boolean shouldAskUser = true;

		if (getCurrentAvenant().noArrete() != null) {
			noArrete = getCurrentAvenant().noArrete();
			shouldAskUser = false;
		} else {
			if (EOGrhumParametres.isNoArreteAuto()) {
				shouldAskUser = !EODialogs.runConfirmOperationDialog("Attention", "Le numéro d'arrêté va être généré automatiquement. Est-ce ce que vous voulez ?", "Oui", "Non");
				if (!shouldAskUser) {
					noArrete = EOContratAvenant.getNumeroArreteAutomatique(getEdc(), DateCtrl.getYear(getCurrentAvenant().dateDebut()));
					enregistrerNoArrete();
				}
			}
		}

		if (shouldAskUser)
			noArrete = AskForString.sharedInstance().getString("Numéro d'Arrêté", "Saisir le numéro d'arrêté :", "");

		NSArray<EOGlobalID> destinataires = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinatairesGlobalIds();

		EOGlobalID globalIDDetail = getEdc().globalIDForObject(getCurrentAvenant());
		Number noIndividu = getCurrentAvenant().contrat().individu().noIndividu();

		Class[] classeParametres =  new Class[] {String.class,EOGlobalID.class,NSArray.class, Boolean.class};
		Object[] parametres = new Object[]{noArrete,globalIDDetail, destinataires, new Boolean(false)};
		try {
			UtilitairesImpression.imprimerAvecDialogue(getEdc(),"clientSideRequestImprimerContratTravail",classeParametres,parametres,"Contrat_Travail" +  "_" + noArrete + "_" + noIndividu,"Impression du contrat de travail");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}

		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 * @param avenant
	 */
	private void enregistrerNoArrete() {
		// pour les avenants, on garde le noArrete dans l'avenant et on enregistre les données
		try {
			getCurrentAvenant().setNoArrete(noArrete);
			if (getCurrentAvenant().dateArrete() == null)
				getCurrentAvenant().setDateArrete(new NSTimestamp());
			getEdc().saveChanges();
		} catch (Exception e) {
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite lors de l'enregistrement des données de l'arrêté. Pour imprimer l'arrêté, veuillez saisir dasn l'élément de carrière le numéro d'arrêté et la date");
			return;
		}
	}

	/**
	 * 
	 */
	private void renouveler() {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);		
		EOContratAvenant avenant = SaisieContratAvenantCtrl.sharedInstance(getEdc()).renouveler(getCurrentAvenant());
		if (avenant != null) {
			setCurrentAvenant(avenant);
			// On se replace sur le nouvel avenant
			EOContratAvenant avenantEnCours = getCurrentAvenant();
			actualiser();
			myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(avenantEnCours));

		}
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerAvenant implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}
		public void onSelectionChanged() {

			setCurrentAvenant((EOContratAvenant)eod.selectedObject());
			myView.getLblSpecialite().setText("");
			myView.getBtnActivites().setText("Activités (0)");
			if (getCurrentAvenant() != null) {
				NSArray<EORepartCtrActivites> activites = EORepartCtrActivites.rechercherRepartsActivitesPourAvenant(getCurrentAvenant(), true);
				myView.getBtnActivites().setText("Activités (" + activites.count() +")");
				myView.getLblSpecialite().setText(myCtrlSpec.getStringSpecForAvenant(getCurrentAvenant()));				
			}

			updateInterface();

		}
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnAjouter().setEnabled(getCurrentContrat() != null);
		myView.getBtnModifier().setEnabled(getCurrentAvenant() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentAvenant() != null);
		myView.getBtnRenouveler().setEnabled(getCurrentAvenant() != null);
		myView.getBtnImprimer().setEnabled(getCurrentAvenant() != null);
		myView.getBtnDetailPaie().setEnabled(getCurrentAvenant() != null);
		myView.getBtnActivites().setEnabled(getCurrentAvenant() != null && activitesPourTypeContrat != null && activitesPourTypeContrat.count() > 0);
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		if (getCurrentContrat() != null)
			activitesPourTypeContrat = EOActivitesTypeContrat.rechercherActivitesPourTypeContrat(getCurrentContrat().toTypeContratTravail());

		eod.setObjectArray(EOContratAvenant.findForContrat(getEdc(), getCurrentContrat()));
		myView.getMyEOTable().updateData();

		updateInterface();
	}

	@Override
	protected EOQualifier filterQualifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		EOContratAvenant avenant = SaisieContratAvenantCtrl.sharedInstance(getEdc()).ajouter(getCurrentContrat());
		if (avenant != null) {
			setCurrentAvenant(avenant);
			// On se replace sur le nouvel avenant
			EOContratAvenant avenantEnCours = getCurrentAvenant();
			updateDatas();
			myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(avenantEnCours));
		}

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	@Override
	protected void traitementsApresCreation() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		CRICursor.setWaitCursor(myView);

		SaisieContratAvenantCtrl.sharedInstance(getEdc()).modifier(getCurrentAvenant());
		
		getEdc().invalidateObjectsWithGlobalIDs(new NSArray(getEdc().globalIDForObject(getCurrentAvenant())));
		myView.getMyEOTable().updateUI();
		listenerAvenant.onSelectionChanged();

		CRICursor.setDefaultCursor(myView);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getCurrentAvenant().setEstAnnule(true);

		EOPasse passe = EOPasse.rechercherPassePourAvenant(getEdc(), getCurrentAvenant());
		if (passe != null)
			passe.setEstValide(false);
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		updateDatas();
	}
}
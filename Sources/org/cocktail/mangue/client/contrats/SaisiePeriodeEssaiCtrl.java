// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.contrats;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.modalites.SaisieCrctDetailView;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratPeriodesEssai;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.modele.mangue.modalites.EOCrctDetail;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisiePeriodeEssaiCtrl extends ModelePageSaisie
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisiePeriodeEssaiCtrl sharedInstance;
	private EOEditingContext edc;
	private SaisieCrctDetailView myView;
	
	private EOContrat currentContrat;
	private EOContratPeriodesEssai currentPeriode;

	public SaisiePeriodeEssaiCtrl(EOEditingContext edc) {

		super(edc);
		this.edc = edc;

		myView = new SaisieCrctDetailView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	valider();}	}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	annuler();}	}
		);

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

	}

	public static SaisiePeriodeEssaiCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisiePeriodeEssaiCtrl(editingContext);
		return sharedInstance;
	}

	public EOContrat getCurrentContrat() {
		return currentContrat;
	}

	public void setCurrentContrat(EOContrat currentContrat) {
		this.currentContrat = currentContrat;
	}

	public EOContratPeriodesEssai getCurrentPeriode() {
		return currentPeriode;
	}

	public void setCurrentPeriode(EOContratPeriodesEssai currentPeriode) {
		this.currentPeriode = currentPeriode;
	}

	/**
	 * 
	 * @param crct
	 * @return
	 */
	public EOContratPeriodesEssai ajouter(EOContrat contrat)	{

		clearDatas();
		setCurrentContrat(contrat);
		
		myView.setTitle("Périodes d'essai ( " + getCurrentContrat().individu().identitePrenomFirst() + ")");

		setCurrentPeriode(EOContratPeriodesEssai.creer(edc, getCurrentContrat(), false));
		evaluerDatesPeriodes();

		updateDatas();
		myView.setVisible(true);
		return getCurrentPeriode();
	}

	/**
	 * 
	 * @param crct
	 * @return
	 */
	public EOContratPeriodesEssai modifier(EOContratPeriodesEssai periodeEssai)	{

		clearDatas();
		setCurrentPeriode(periodeEssai);
		
		myView.setTitle("Périodes d'essai ( " + getCurrentContrat().individu().identitePrenomFirst() + ")");

		setCurrentPeriode(EOContratPeriodesEssai.creer(edc, getCurrentContrat(), false));
		
		updateDatas();
		myView.setVisible(true);
		return getCurrentPeriode();
	}

	/**
	 * 
	 */
	private void evaluerDatesPeriodes() {
		
		getCurrentPeriode().setDateDebut(getCurrentContrat().dateDebut());
		
		NSTimestamp dateFinPeriode = null;
		
		if (getCurrentContrat().dateFin() == null) {
			dateFinPeriode = DateCtrl.dateAvecAjoutJours(getCurrentContrat().dateDebut(), 120);
		}
		else {
			int jours = DateCtrl.nbJoursEntre(getCurrentContrat().dateDebut(), getCurrentContrat().dateFin() , true);
			int nbMois = jours / 30;
			
			if (nbMois <= 6) {
				dateFinPeriode = DateCtrl.dateAvecAjoutJours(getCurrentContrat().dateDebut(), 21);
			}
			else
				if (nbMois <= 12) {
					dateFinPeriode = DateCtrl.dateAvecAjoutMois(getCurrentContrat().dateDebut(), 1);
				}
				else
					if (nbMois < 24) {
						dateFinPeriode = DateCtrl.dateAvecAjoutMois(getCurrentContrat().dateDebut(), 2);
					}
					else
						if (nbMois >= 24) {
							dateFinPeriode = DateCtrl.dateAvecAjoutMois(getCurrentContrat().dateDebut(), 3);
						}
		}

		getCurrentPeriode().setDateFin(dateFinPeriode);

	}
	
	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
	}

	@Override
	protected void updateDatas() {
		
		// TODO Auto-generated method stub
		clearDatas();
		if (getCurrentPeriode() != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentPeriode().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentPeriode().dateFin());
		}
		updateInterface();
		
	}
	
	protected void valider() {
		
		try {
			traitementsAvantValidation();
			edc.insertObject(getCurrentPeriode());
			traitementsApresValidation();
		}
		catch (NSValidation.ValidationException e) {
			EODialogs.runErrorDialog("ERREUR", e.getMessage());
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", e.getMessage());			
		}

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void traitementsAvantValidation() throws ValidationException {
		// TODO Auto-generated method stub
		getCurrentPeriode().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentPeriode().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
			myView.setVisible(false);		
	}

	@Override
	protected void traitementsPourAnnulation() {
		setCurrentPeriode(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}

}

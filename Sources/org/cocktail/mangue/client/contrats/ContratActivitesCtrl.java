package org.cocktail.mangue.client.contrats;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.contrats.ContratActivitesView;
import org.cocktail.mangue.client.select.ActiviteTypeContratSelectCtrl;
import org.cocktail.mangue.modele.grhum.EOActivitesTypeContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EORepartCtrActivites;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class ContratActivitesCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static ContratActivitesCtrl sharedInstance;

	private EODisplayGroup eod;
	private EOEditingContext ec;

	private ContratActivitesView myView;
	private ListenerActivite listenerActivite = new ListenerActivite();

	private EOContratAvenant currentAvenant;
	private EORepartCtrActivites currentRepartActivite;

	public ContratActivitesCtrl(EOEditingContext globalEc) {
		ec = globalEc;

		eod = new EODisplayGroup();
		myView = new ContratActivitesView(new JFrame(), true, eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnFermer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){fermer();}}
		);

		myView.getMyEOTable().addListener(listenerActivite);


	}

	public static ContratActivitesCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null)
			sharedInstance = new ContratActivitesCtrl(editingContext);
		return sharedInstance;
	}

	private void clearTextFields(){

	}

	public void open(EOContratAvenant contratAvenant) {

		clearTextFields();

		currentAvenant = contratAvenant;

		myView.getTfTitre().setText("ACTIVITES - Contrat de " + currentAvenant.contrat().individu().identitePrenomFirst());

		updateData();

		myView.setVisible(true);

	}

	private void updateData() {

		eod.setObjectArray(EORepartCtrActivites.rechercherRepartsActivitesPourAvenant(currentAvenant, true));
		myView.getMyEOTable().updateData();

	}

	/**
	 * 
	 */
	private void ajouter() {

		EOActivitesTypeContrat activite = ActiviteTypeContratSelectCtrl.sharedInstance(ec).getActivite(currentAvenant.contrat().toTypeContratTravail());
		if (activite != null) {
			try {

				NSArray activites = EORepartCtrActivites.rechercherRepartsActivitesPourAvenantEtActivite(ec, currentAvenant, activite);
				if (activites.count() == 0) {	

					EORepartCtrActivites.creer(ec, currentAvenant, activite);
					ec.saveChanges();
					updateData();
				}
				else
					EODialogs.runInformationDialog("","Cette activité est déjà associée à ce contrat !");
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		updateData();

	}

	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer l'activité sélectionnée ?", "Oui", "Non"))		
			return;			

		try {

			ec.deleteObject(currentRepartActivite);
			ec.saveChanges();

			updateData();

		}
		catch (Exception ex) {
			ec.revert();
			ex.printStackTrace();
		}

	}

	private void fermer() {

		ec.revert();
		myView.setVisible(false);

	}

	private void updateUI() {
		myView.getBtnSupprimer().setEnabled(currentRepartActivite != null);
	}


	private class ListenerActivite implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			currentRepartActivite = (EORepartCtrActivites)eod.selectedObject();
			updateUI();
		}
	}
}

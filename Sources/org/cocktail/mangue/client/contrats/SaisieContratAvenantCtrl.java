package org.cocktail.mangue.client.contrats;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.contrats.SaisieContratAvenantView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.ConditionRecrutementSelectCtrl;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.client.specialisations.SpecialisationCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeUniteTemps;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOConditionRecrutement;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.grhum.EOTypeContratGrades;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;
import org.cocktail.mangue.modele.mangue.individu.EOValidationServices;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class SaisieContratAvenantCtrl extends ModelePageSaisie
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieContratAvenantCtrl sharedInstance;
	private SaisieContratAvenantView myView;
	private SpecialisationCtrl myCtrlSpec;
	private EOContrat currentContrat;
	private EOContratAvenant currentAvenant;
	private EOPasse currentPasse;
	private EOConditionRecrutement currentRecrutement;
	private EOGrade currentGrade;
	private ListenerTempsTravail listenerTempsTravail = new ListenerTempsTravail();

	/**
	 * Initialisation du composant : Vue, Boutons, Listeners, Popups
	 * 
	 * @param globalEc	Un editing context global a l'application est passe en parametres
	 */
	public SaisieContratAvenantCtrl(EOEditingContext edc) {

		super(edc);

		myCtrlSpec = new SpecialisationCtrl(getEdc());
		myCtrlSpec.setSaisieEnabled(true);

		myView = new SaisieContratAvenantView(new JFrame(), true);

		setActionBoutonValiderListener(myView .getBtnValider());
		setActionBoutonAnnulerListener(myView .getBtnAnnuler());

		myView.getBtnGetGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectGrade();}}
				);
		myView.getBtnDelGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delGrade();}}
				);
		myView.getBtnGetRecrutement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectConditionRecrutement();}}
				);
		myView.getBtnDelRecrutement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delConditionRecrutement();}}
				);
		myView.getBtnPrintArrete().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimerArrete();}}
				);

		myView.getCheckTempsComplet().addActionListener(listenerTempsTravail);
		myView.getCheckTempsIncomplet().addActionListener(listenerTempsTravail);
		myView.getCheckTempsPartiel().addActionListener(listenerTempsTravail);

		myView.getSwapViewSpecialisation().add("VIDE",new JPanel(new BorderLayout()));
		myView.getSwapViewSpecialisation().add("SPEC",myCtrlSpec.getView());
		((CardLayout)myView.getSwapViewSpecialisation().getLayout()).show(myView.getSwapViewSpecialisation(), "SPEC");

		myView.getPopupEchelons().addActionListener(new PopupEchelonListener());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());
		setDateListeners(myView.getTfDateValidation());
		setDateListeners(myView.getTfDateArrete());

		myView.getTfIndiceMajore().addFocusListener(new FocusListenerIndiceMajore());
		myView.getTfIndiceMajore().addActionListener(new ActionListenerIndiceMajore());

		CocktailUtilities.initTextField(myView.getTfRecrutement(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleGrade(), false, false);
		CocktailUtilities.initTextField(myView.getTfIndiceBrut(), false, false);
		CocktailUtilities.initTextField(myView.getTfIndiceMajoreCalcule(), false, false);

		myView.getCheckDea().setEnabled(false);

		CocktailUtilities.initPopupAvecListe(myView.getPopupCategories(), NomenclatureFinder.find(getEdc(), EOCategorie.ENTITY_NAME, Nomenclature.SORT_ARRAY_CODE) , true);
		CocktailUtilities.initPopupAvecListe(myView.getPopupTypeDuree(), EOTypeUniteTemps.fetchAll(getEdc()) , true);

		myView.getCheckPcAcquitee().setSelected(false);
		myView.getViewArrete().setVisible(false);

	}

	public static SaisieContratAvenantCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieContratAvenantCtrl(editingContext);
		return sharedInstance;
	}

	private EOContratAvenant getCurrentAvenant() {
		return currentAvenant;
	}

	public EOContrat getCurrentContrat() {
		return currentContrat;
	}

	public void setCurrentContrat(EOContrat currentContrat) {

		this.currentContrat = currentContrat;

		// Vue a afficher seulement pour les types contrats renseignes dans la table CONDITION_RECRUTEMENT
		myView.getViewConditionRecrutement().setVisible(currentContrat != null && EOConditionRecrutement.existePourTypeContrat(getEdc(), currentContrat.toTypeContratTravail()));

	}
	public void setCurrentAvenant(EOContratAvenant currentAvenant) {
		this.currentAvenant = currentAvenant;
		if (currentAvenant != null) {
			setCurrentContrat(getCurrentAvenant().contrat());
			updateDatas();
		}
		else
			setCurrentContrat(null);
	}

	public EOPasse getCurrentPasse() {
		return currentPasse;
	}

	public void setCurrentPasse(EOPasse currentPasse) {
		this.currentPasse = currentPasse;
	}

	public EOConditionRecrutement getCurrentRecrutement() {
		return currentRecrutement;
	}

	public void setCurrentRecrutement(EOConditionRecrutement recrutement) {
		currentRecrutement = recrutement;
		myView.getTfRecrutement().setText("");
		if (currentRecrutement != null) {
			CocktailUtilities.setTextToField(myView.getTfRecrutement(), currentRecrutement.libelleLong());
		}
		updateInterface();
	}

	public EOGrade getCurrentGrade() {
		return currentGrade;
	}

	/**
	 * 
	 * @param grade
	 */
	public void setCurrentGrade(EOGrade grade) {

		currentGrade = grade;
		myView.getTfLibelleGrade().setText("");
		if (getCurrentGrade() != null) {
			getCurrentAvenant().setToGradeRelationship(currentGrade);
			CocktailUtilities.setTextToField(myView.getTfLibelleGrade(), getCurrentGrade().cGrade() + " - " + getCurrentGrade().llGrade());

			myCtrlSpec.showSpecForAvenant(getCurrentAvenant());

			if (currentGrade.toCategorie() != null)
				myView.getPopupCategories().setSelectedItem(currentGrade.toCategorie());

			// Si c'est un grade d'enseignant  on bloque la saisie en heures
			myView.getPopupTypeDuree().setEnabled(true);
			if ( getCurrentGrade().toCorps() != null && currentGrade.toCorps() != null
					&& getCurrentGrade().toCorps().toTypePopulation() != null
					&& getCurrentGrade().toCorps().toTypePopulation().estEnseignant()) {

				myView.getPopupTypeDuree().setEnabled(false);
				myView.getPopupTypeDuree().setSelectedItem((EOTypeUniteTemps)NomenclatureFinder.findForCode(getEdc(), EOTypeUniteTemps.ENTITY_NAME, EOTypeUniteTemps.TYPE_CODE_HEURES));				

			}

		}
	}

	/**
	 * 
	 * @return
	 */
	private EOTypeUniteTemps getCurrentTypeUniteTemps() {

		if (myView.getPopupTypeDuree().getSelectedIndex() > 0)
			return (EOTypeUniteTemps)myView.getPopupTypeDuree().getSelectedItem();

		return null;
	}
	/**
	 * 
	 * @return
	 */
	private EOCategorie getCurrentCategorie() {

		if (myView.getPopupCategories().getSelectedIndex() > 0)
			return (EOCategorie)myView.getPopupCategories().getSelectedItem();

		return null;
	}

	/**
	 * 
	 * @return
	 */
	private String getCurrentEchelon() {

		if (myView.getPopupEchelons().getSelectedIndex() > 0)
			return (String)myView.getPopupEchelons().getSelectedItem();

		return null;
	}

	/**
	 * 
	 * @return
	 */
	private String titre() {
		return currentAvenant.contrat().individu().identitePrenomFirst() + " - Détail Contrat du " + DateCtrl.dateToString(currentAvenant.contrat().dateDebut()) + " au " + DateCtrl.dateToString(currentAvenant.contrat().dateFin()) + ")";
	}

	/**
	 * 
	 * @param contrat
	 * @return
	 */
	public EOContratAvenant ajouter(EOContrat contrat)	{

		setCurrentContrat(contrat);
		setCurrentAvenant(EOContratAvenant.creer(getEdc(), getCurrentContrat()));
		setCurrentPasse(null);
		myView.setTitle(titre());

		updateDatas();
		updateInterface();
		myView.setVisible(true);

		return getCurrentAvenant();
	}

	/**
	 * 
	 * @param avenant
	 * @return
	 */
	public boolean modifier(EOContratAvenant avenant) {

		setCurrentContrat(avenant.contrat());
		setCurrentAvenant(avenant);
		setCurrentPasse(EOPasse.rechercherPassePourAvenant(getEdc(), getCurrentAvenant()));

		myView.setTitle(titre());

		updateDatas();
		updateInterface();
		myView.setVisible(true);

		return getCurrentAvenant() != null;
	}

	/**
	 * 
	 * @param contratAvenant
	 * @return
	 */
	public EOContratAvenant renouveler(EOContratAvenant contratAvenant) {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		setCurrentAvenant(EOContratAvenant.renouveler(getEdc(), contratAvenant));

		myView.getTfDateFin().setText("");
		myView.setVisible(true);		

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentAvenant();
	}


	/**
	 * 
	 */
	private void majIndiceCalcule() {

		if (getCurrentGrade() != null && getCurrentEchelon() != null) {

		}

	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerTempsTravail implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			CocktailUtilities.initTextField(myView.getTfQuotiteCotisation(), false , myView.getCheckTempsPartiel().isSelected());

			if (myView.getCheckTempsComplet().isSelected() || myView.getCheckTempsIncomplet().isSelected())
				myView.getTfQuotiteCotisation().setText("100");
		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupEchelonListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			if (getCurrentAvenant() != null 
					&& getCurrentAvenant().toGrade() != null 
					&& getDateDebut() != null && myView.getPopupEchelons().getItemCount() > 0) {

				NSArray<EOPassageEchelon> passages = EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(getEdc(), 
						getCurrentGrade().cGrade() , 
						(String)myView.getPopupEchelons().getSelectedItem(), 
						getDateDebut() , true);
				if (passages.count() >0 ) {

					EOPassageEchelon passage = passages.get(0);					
					myView.getTfIndiceBrut().setText(passage.cIndiceBrut());

					EOIndice indiceMajore = EOIndice.indiceMajorePourIndiceBrutEtDate(getEdc(), passage.cIndiceBrut(), getDateDebut());
					myView.getTfIndiceMajore().setText("");
					if (indiceMajore != null) {
						CocktailUtilities.setNumberToField(myView.getTfIndiceMajore(), indiceMajore.cIndiceMajore());
						CocktailUtilities.setNumberToField(myView.getTfIndiceMajoreCalcule(), indiceMajore.cIndiceMajore());
					}
				}
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	protected void traitementsAvantValidation() {

		if (currentAvenant.contrat() == null) {			
			throw new NSValidation.ValidationException("L'avenant n'a pas pu être associé au contrat. Veuillez annuler cette saisie et essayer de nouveau.");
		}

		getCurrentAvenant().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentAvenant().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
		getCurrentAvenant().setDateArrete(CocktailUtilities.getDateFromField(myView.getTfDateArrete()));

		getCurrentAvenant().setIndiceContrat(CocktailUtilities.getTextFromField(myView.getTfIndiceMajore()));
		getCurrentAvenant().setCtraDuree(CocktailUtilities.getDoubleFromField(myView.getTfDuree()));
		getCurrentAvenant().setQuotite(CocktailUtilities.getDoubleFromField(myView.getTfQuotite()));
		getCurrentAvenant().setNoArrete(CocktailUtilities.getTextFromField(myView.getTfNoArrete()));
		getCurrentAvenant().setFonctionCtrAvenant(CocktailUtilities.getTextFromField(myView.getTfFonction()));
		getCurrentAvenant().setReferenceContrat(CocktailUtilities.getTextFromField(myView.getTfReference()));
		getCurrentAvenant().setTauxHoraire(CocktailUtilities.getBigDecimalFromField(myView.getTfTauxHoraire()));

		getCurrentAvenant().setADispenseDea(myView.getCheckDea().isSelected());
		
		getCurrentAvenant().setToGradeRelationship(getCurrentGrade());
		getCurrentAvenant().setCEchelon(getCurrentEchelon());
		getCurrentAvenant().setConditionRecrutementRelationship(getCurrentRecrutement());
		getCurrentAvenant().setToCategorieRelationship(getCurrentCategorie());
		getCurrentAvenant().setTypeDureeRelationship(getCurrentTypeUniteTemps());

		if ( ! ((ApplicationClient)ApplicationClient.sharedApplication()).isUseServicesValides()) {

			if (myView.getCheckTempsComplet().isSelected())
				getCurrentAvenant().setCtraTypeTemps(EOPasse.TYPE_TEMPS_COMPLET);
			if (myView.getCheckTempsIncomplet().isSelected())
				getCurrentAvenant().setCtraTypeTemps(EOPasse.TYPE_TEMPS_INCOMPLET);				
			if (myView.getCheckTempsPartiel().isSelected())
				getCurrentAvenant().setCtraTypeTemps(EOPasse.TYPE_TEMPS_PARTIEL);
			getCurrentAvenant().setEstPcAcquitees(myView.getCheckPcAcquitee().isSelected());			
			getCurrentAvenant().setCtraQuotiteCotisation(CocktailUtilities.getDoubleFromField(myView.getTfQuotiteCotisation()));

			getCurrentAvenant().setDValContratAv(CocktailUtilities.getDateFromField(myView.getTfDateValidation()));
			getCurrentAvenant().setCtraDureeValideeAnnees(null);
			getCurrentAvenant().setCtraDureeValideeMois(null);
			getCurrentAvenant().setCtraDureeValideeJours(null);

			getCurrentAvenant().setCtraDureeValideeAnnees(CocktailUtilities.getIntegerFromField(myView.getTfAnnees()));
			getCurrentAvenant().setCtraDureeValideeMois(CocktailUtilities.getIntegerFromField(myView.getTfMois()));
			getCurrentAvenant().setCtraDureeValideeJours(CocktailUtilities.getIntegerFromField(myView.getTfJours()));

			// Si on a une duree de services valides, on enregistre une periode de passe.
			if (getCurrentAvenant().dValContratAv() != null) {

				if (getCurrentPasse() == null)
					setCurrentPasse(EOPasse.creerFromContratAvenant(getEdc(), getCurrentAvenant()));

				getCurrentPasse().setDateDebut(getCurrentAvenant().dateDebut());
				getCurrentPasse().setDateFin(getCurrentAvenant().dateFin());

				getCurrentPasse().setDureeValideeAnnees(getCurrentAvenant().ctraDureeValideeAnnees());
				getCurrentPasse().setDureeValideeMois(getCurrentAvenant().ctraDureeValideeMois());
				getCurrentPasse().setDureeValideeJours(getCurrentAvenant().ctraDureeValideeJours());
				getCurrentPasse().setPasPcAcquitee(getCurrentAvenant().ctraPcAcquitees());
				getCurrentPasse().setPasTypeTemps(getCurrentAvenant().ctraTypeTemps());
				getCurrentPasse().setPasQuotiteCotisation(new BigDecimal(getCurrentAvenant().ctraQuotiteCotisation()));
				getCurrentPasse().setDValidationService(getCurrentAvenant().dValContratAv());

			}
			else {
				if (getCurrentPasse() != null)
					getCurrentPasse().setTemValide(CocktailConstantes.FAUX);
			}
		}
		else {	// Utilisation des services valides dans CIR / Validations

			EOValidationServices validation = EOValidationServices.findForAvenant(getEdc(), getCurrentAvenant());

			if (CocktailUtilities.getDateFromField(myView.getTfDateValidation()) != null) {

				if (validation == null) {
					validation = EOValidationServices.creer(getEdc(), getCurrentAvenant().individu());
					validation.setToContratAvenantRelationship(getCurrentAvenant());
				}

				validation.setDateDebut(getCurrentAvenant().dateDebut());
				validation.setDateFin(getCurrentAvenant().dateFin());

				if (myView.getCheckTempsComplet().isSelected())
					validation.setTempsComplet();
				if (myView.getCheckTempsIncomplet().isSelected())
					validation.setTempsIncomplet();
				if (myView.getCheckTempsPartiel().isSelected())
					validation.setTempsPartiel();

				validation.setPcAquitees(myView.getCheckPcAcquitee().isSelected());			

				validation.setValAnnees(null);
				validation.setValMois(null);
				validation.setValJours(null);
				validation.setValMinistere("MESR");

				validation.setValAnnees(CocktailUtilities.getIntegerFromField(myView.getTfAnnees()));
				validation.setValMois(CocktailUtilities.getIntegerFromField(myView.getTfMois()));
				validation.setValJours(CocktailUtilities.getIntegerFromField(myView.getTfJours()));
				validation.setValQuotite(CocktailUtilities.getBigDecimalFromField(myView.getTfQuotiteCotisation()));
				validation.setDateValidation(CocktailUtilities.getDateFromField(myView.getTfDateValidation()));

			}
			else {

			}

		}

		getCurrentAvenant().validateObjectForSave();

		// Mise a jour de la date de fin du contrat ?
		if (getCurrentAvenant().dateFin() != null && getCurrentAvenant().contrat().dateFin() != null) {
			if ((DateCtrl.isBefore(getCurrentAvenant().dateFin(),getCurrentAvenant().contrat().dateDebut()) || 
					(DateCtrl.isAfter(getCurrentAvenant().dateFin(),getCurrentAvenant().contrat().dateFin())))) {

				if (getCurrentAvenant().contrat().dateFin() != null && DateCtrl.isAfter(getCurrentAvenant().dateFin(),getCurrentAvenant().contrat().dateFin())) {

					if(EODialogs.runConfirmOperationDialog("Attention", 
							"La date de fin de l'avenant est postérieure à celle du contrat, voulez-vous modifier la date de fin de contrat ?", "Oui", "Non"))	{
						getCurrentAvenant().contrat().setDateFin(getCurrentAvenant().dateFin());
					}
				}
			}
		}

		//	vérifier si le type de contrat requiert un DEA
		if (getCurrentAvenant().contrat().toTypeContratTravail().estAllocataireRecherche()) {
			if (getCurrentAvenant().aDispenseDea() == false) {
				if (!getCurrentAvenant().contrat().toIndividu().aDea()) {
					if (EODialogs.runConfirmOperationDialog("Attention","Le type de contrat de travail requiert que l'individu ait un DEA. Or, ce n'est pas le cas. A-t-il une dispense de DEA ?","Oui","Non")) {
						getCurrentAvenant().setADispenseDea(true);
					} else {
						throw new NSValidation.ValidationException("Veuillez ajouter un DEA à cet individu ou changer de type de contrat de travail dans le contrat");
					}
				}
			}	
		}

		// Vérifier si le grade saisi est valide pendant toute la période
		if (getCurrentAvenant().toGrade() != null && getDateDebut() != null) {
			if ((getCurrentAvenant().dateFin() != null 
					&& getCurrentAvenant().toGrade().dFermeture() != null 
					&& DateCtrl.isAfter(getCurrentAvenant().dateFin(), getCurrentAvenant().toGrade().dFermeture())) ||
					(getCurrentAvenant().dateFin() == null && getCurrentAvenant().toGrade().dFermeture() != null) ||
					(getCurrentAvenant().toGrade().dOuverture() != null 
					&& DateCtrl.isBefore(getCurrentAvenant().dateDebut(), getCurrentAvenant().toGrade().dOuverture()))) {
				throw new NSValidation.ValidationException("Le grade choisi n'est pas valide pendant toute la période !");
			}
		}

		if (getCurrentAvenant().quotite() != null && getCurrentAvenant().quotite().intValue() == 100) {
			NSArray<EOContratAvenant> avenants = currentAvenant.contrat().avenants();
			avenants = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenants, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
			EOContratAvenant lastAvenant = null; // dernier avenant
			if (avenants != null) {
				// trouver l'avenant précédent
				boolean takeNext = false;
				for (java.util.Enumeration<EOContratAvenant> e = avenants.objectEnumerator();e.hasMoreElements();) {
					EOContratAvenant avenant = e.nextElement();
					if (!takeNext) {
						if (avenant == getCurrentAvenant()) {
							takeNext = true;
						}
					} else {
						if (avenant.estAnnule() == false) {
							lastAvenant = avenant;
							break;
						}
					}
				}
				if (lastAvenant != null) {
					//	vérifier si la date de fin est nulle que la date de début est bien la plus récente
					if (getCurrentAvenant().dateFin() == null && DateCtrl.isBefore(getCurrentAvenant().dateDebut(),lastAvenant.dateDebut())) {
						throw new NSValidation.ValidationException("Vous n'avez pas fourni de date de fin, or il existe un autre détail de contrat commençant après cette date");
					}
				}

				// Vérifier que les avenants ne peuvent pas se chevaucher
				if (getCurrentAvenant().estAnnule() == false) {
					for (EOContratAvenant avenant : avenants) {
						if (avenant != getCurrentAvenant() && avenant.estAnnule() == false) {
							if (Utilitaires.IntervalleTemps.intersectionPeriodes(avenant.dateDebut(), avenant.dateFin(), getCurrentAvenant().dateDebut(), getCurrentAvenant().dateFin()) != null) {
								throw new NSValidation.ValidationException("Ce contrat a déjà un détail pendant cette période !");
							}
						}
					}
				}
			}

			// clore l'avenant precedent si nécessaire 
			if (lastAvenant != null && lastAvenant != getCurrentAvenant() && 
					DateCtrl.isAfter(getCurrentAvenant().dateDebut(), lastAvenant.dateDebut()) && lastAvenant.dateFin() == null) {
				lastAvenant.setDateFin(DateCtrl.jourPrecedent(getCurrentAvenant().dateDebut()));
			}
		}

	}

	/**
	 *
	 */
	private void selectGrade() {

		boolean gestionHU = EOGrhumParametres.isGestionHu() && getCurrentAvenant().contrat().toTypeContratTravail().estHospitalier();

		NSArray<EOTypeContratGrades> grades = EOTypeContratGrades.rechercherPourTypeContrat(getEdc(), getCurrentAvenant().contrat().toTypeContratTravail());

		EOGrade grade = null;
		if (grades.size() == 0) {
			grade = GradeSelectCtrl.sharedInstance(getEdc()).getGrade(gestionHU, getDateDebut());						
		} else 
			if (grades.count() == 1) {
				grade = grades.get(0).toGrade();
			} else {
				grade = GradeSelectCtrl.sharedInstance(getEdc()).getGradePourTypeContrat(getCurrentAvenant().contrat().toTypeContratTravail());
			}

		if (grade != null) {

			setCurrentGrade(grade);
			majEchelons();

			myCtrlSpec.showSpecForAvenant(getCurrentAvenant());

			updateInterface();
		}		
	}

	/**
	 * 
	 */
	public void delGrade() {
		setCurrentGrade(null);
		updateInterface();
	}


	/**
	 * 
	 */
	private void selectConditionRecrutement() {

		INomenclature condition = ConditionRecrutementSelectCtrl.sharedInstance(getEdc()).getCondition(getCurrentAvenant().contrat().toTypeContratTravail());
		if (condition != null) {
			setCurrentRecrutement((EOConditionRecrutement)condition);
		}
	}
	private void delConditionRecrutement() {
		setCurrentRecrutement(null);
	}

	private NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	private NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}

	private void majEchelons() {
		NSArray<EOPassageEchelon> listeEchelons = EOPassageEchelon.rechercherPassagesEchelonPourGradesValidesPourPeriode(getEdc(), currentGrade, getDateDebut(), getDateFin());
		myView.getPopupEchelons().removeAllItems();
		myView.getPopupEchelons().addItem("");
		for (int i = 0;i< listeEchelons.count();i++)	{
			myView.getPopupEchelons().addItem(((EOPassageEchelon) listeEchelons.objectAtIndex(i)).cEchelon());
		}
	}

	/**
	 * 
	 * @return
	 */
	public boolean peutImprimer() {

		if (getCurrentAvenant().quotite() == null || getCurrentAvenant().quotite().doubleValue() == 0 || 
				getCurrentAvenant().dateDebut() == null ||
				(getCurrentAvenant().contrat().dateFin() != null && getCurrentAvenant().dateFin() == null) ||
				(getCurrentAvenant().dValContratAv() != null && (getCurrentAvenant().ctraPcAcquitees() == null || getCurrentAvenant().ctraQuotiteCotisation() == null))) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 */
	public void imprimerArrete() {

		getCurrentAvenant().setNoArrete(myView.getTfNoArrete().getText());			
		getCurrentAvenant().setDateArrete(DateCtrl.stringToDate(myView.getTfNoArrete().getText()));

		NSArray<EOGlobalID> destinataires = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinatairesGlobalIds();
		if (destinataires != null) {

			Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class};
			Object[] parametres = new Object[]{getEdc().globalIDForObject(getCurrentAvenant()), destinataires};
			try {
				// avec Thread
				UtilitairesImpression.imprimerSansDialogue(getEdc(),"clientSideRequestImprimerArretePourAvenant",classeParametres,parametres,"ArreteAvenant_" + getCurrentAvenant().noArrete(),"Impression de l'arrêté");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}		

		}				
	}	

	// Focus sur l'INDICE MAJORE
	private class ActionListenerIndiceMajore implements ActionListener	{
		private ActionListenerIndiceMajore() {}		
		public void actionPerformed(ActionEvent e)	{indiceMajoreHasChanged();}
	}
	private class FocusListenerIndiceMajore implements FocusListener	{
		private FocusListenerIndiceMajore() {}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{indiceMajoreHasChanged();}
	}	
	private void indiceMajoreHasChanged() {

		myView.getTfIndiceBrut().setText("");
		if ("".equals(myView.getTfIndiceMajore().getText()))
			return;

		NSArray<EOIndice> indices = EOIndice.indicesPourIndiceMajoreEtDate(getEdc(), new Integer(myView.getTfIndiceMajore().getText()),getDateDebut());
		if (indices.count() > 0) {
			indices = EOSortOrdering.sortedArrayUsingKeyOrderArray(indices, new NSArray(EOIndice.SORT_KEY_BRUT_DESC));
			myView.getTfIndiceBrut().setText(indices.get(0).cIndiceBrut());
		}
	}


	@Override
	protected void clearDatas() {

		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfLibelleGrade());
		CocktailUtilities.viderTextField(myView.getTfRecrutement());
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfQuotite());
		CocktailUtilities.viderTextField(myView.getTfQuotiteCotisation());
		CocktailUtilities.viderTextField(myView.getTfFonction());
		CocktailUtilities.viderTextField(myView.getTfReference());
		CocktailUtilities.viderTextField(myView.getTfDuree());

		CocktailUtilities.viderTextField(myView.getTfAnnees());
		CocktailUtilities.viderTextField(myView.getTfMois());
		CocktailUtilities.viderTextField(myView.getTfJours());

		CocktailUtilities.viderTextField(myView.getTfIndiceBrut());
		CocktailUtilities.viderTextField(myView.getTfIndiceMajore());
		CocktailUtilities.viderTextField(myView.getTfIndiceMajoreCalcule());

		myView.getCheckPcAcquitee().setSelected(false);
		myView.getPopupCategories().setSelectedIndex(0);
		myView.getPopupEchelons().removeAllItems();

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentContrat() != null) {
			NSArray<EOTypeContratGrades> grades = EOTypeContratGrades.rechercherPourTypeContrat(getEdc(), getCurrentContrat().toTypeContratTravail());

			myView.getPopupCategories().setSelectedItem(getCurrentAvenant().toCategorie());
			myView.getPopupTypeDuree().setSelectedItem(getCurrentAvenant().typeDuree());

			if (getCurrentAvenant().toGrade() == null && grades.count() == 1)
				setCurrentGrade(grades.get(0).toGrade());
			else
				setCurrentGrade(getCurrentAvenant().toGrade());

			setCurrentRecrutement(getCurrentAvenant().conditionRecrutement());

			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentAvenant().dateDebut()); 
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentAvenant().dateFin()); 

			majEchelons();

			majIndiceCalcule();

			if (getCurrentAvenant().toCategorie() != null) {
				myView.getPopupCategories().setSelectedItem(getCurrentAvenant().toCategorie());
			}

			if (getCurrentAvenant().cEchelon() != null)
				myView.getPopupEchelons().setSelectedItem(getCurrentAvenant().cEchelon());

			if (getCurrentAvenant().indiceContrat() != null) {
				CocktailUtilities.setTextToField(myView.getTfIndiceMajore(), getCurrentAvenant().indiceContrat()); 
				CocktailUtilities.setTextToField(myView.getTfIndiceBrut(), getCurrentAvenant().indiceBrut()); 
			}

			CocktailUtilities.setNumberToField(myView.getTfTauxHoraire(), getCurrentAvenant().tauxHoraire()); 

			CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentAvenant().noArrete()); 
			CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentAvenant().dateArrete()); 

			CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentAvenant().quotite()); 
			CocktailUtilities.setTextToField(myView.getTfFonction(), getCurrentAvenant().fonctionCtrAvenant()); 
			CocktailUtilities.setTextToField(myView.getTfReference(), getCurrentAvenant().referenceContrat()); 
			CocktailUtilities.setNumberToField(myView.getTfDuree(), getCurrentAvenant().ctraDuree()); 

			CocktailUtilities.setNumberToField(myView.getTfAnnees(), getCurrentAvenant().ctraDureeValideeAnnees()); 
			CocktailUtilities.setNumberToField(myView.getTfMois(), getCurrentAvenant().ctraDureeValideeMois()); 
			CocktailUtilities.setNumberToField(myView.getTfJours(), getCurrentAvenant().ctraDureeValideeJours()); 

			// DUREES VALIDEES

			if ( ((ApplicationClient)ApplicationClient.sharedApplication()).isUseServicesValides() == false) {

				CocktailUtilities.setDateToField(myView.getTfDateValidation(), getCurrentAvenant().dValContratAv()); 

				if (currentAvenant.ctraQuotiteCotisation() != null)
					CocktailUtilities.setNumberToField(myView.getTfQuotiteCotisation(), getCurrentAvenant().ctraQuotiteCotisation()); 
				else {
					if (getCurrentAvenant().estTempsComplet() || getCurrentAvenant().estTempsIncomplet())
						myView.getTfQuotiteCotisation().setText("100");
				}

				CocktailUtilities.setNumberToField(myView.getTfAnnees(), getCurrentAvenant().ctraDureeValideeAnnees());
				CocktailUtilities.setNumberToField(myView.getTfMois(), getCurrentAvenant().ctraDureeValideeMois());
				CocktailUtilities.setNumberToField(myView.getTfJours(), getCurrentAvenant().ctraDureeValideeJours());

				myView.getCheckTempsComplet().setSelected(getCurrentAvenant().estTempsComplet());
				myView.getCheckTempsIncomplet().setSelected(getCurrentAvenant().estTempsIncomplet());
				myView.getCheckTempsPartiel().setSelected(getCurrentAvenant().estTempsPartiel());
				myView.getCheckPcAcquitee().setSelected(getCurrentAvenant().pcAcquitees());
			}
			else {
				
				EOValidationServices validation = EOValidationServices.findForAvenant(getEdc(), getCurrentAvenant());
				
				if (validation != null) {
					CocktailUtilities.setDateToField(myView.getTfDateValidation(), validation.dateValidation()); 

					if (validation.valQuotite() != null)
						CocktailUtilities.setNumberToField(myView.getTfQuotiteCotisation(), validation.valQuotite()); 
					else {
						if (validation.estTempsComplet() || validation.estTempsIncomplet())
							myView.getTfQuotiteCotisation().setText("100");
					}

					CocktailUtilities.setNumberToField(myView.getTfAnnees(), validation.valAnnees());
					CocktailUtilities.setNumberToField(myView.getTfMois(), validation.valMois());
					CocktailUtilities.setNumberToField(myView.getTfJours(), validation.valJours());

					myView.getCheckTempsComplet().setSelected(validation.estTempsComplet());
					myView.getCheckTempsIncomplet().setSelected(validation.estTempsIncomplet());
					myView.getCheckTempsPartiel().setSelected(validation.estTempsPartiel());
					myView.getCheckPcAcquitee().setSelected(validation.isPcAcquitees());					
				}

			}
		}

		updateInterface();

	}

	@Override
	protected void updateInterface() {

		// TODO Auto-generated method stub
		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, true);
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, true);
		CocktailUtilities.initTextField(myView.getTfQuotiteCotisation(), false, myView.getCheckTempsPartiel().isSelected());

		myView.getCheckDea().setEnabled(getCurrentAvenant() != null 
				&& getCurrentAvenant().contrat() != null 
				&& getCurrentAvenant().contrat().toTypeContratTravail().estAllocataireRecherche());

		myView.getBtnGetGrade().setEnabled(getCurrentAvenant() != null);

		myView.getBtnDelGrade().setEnabled(getCurrentAvenant() != null  
				&& getCurrentGrade() != null
				&& !getCurrentAvenant().contrat().toTypeContratTravail().requiertGrade());

		myView.getBtnGetRecrutement().setEnabled(getCurrentAvenant() != null);
		myView.getBtnDelRecrutement().setEnabled(getCurrentAvenant() != null && getCurrentRecrutement() != null);
		myView.getPopupEchelons().setEnabled(getCurrentGrade() != null);

		EOTypeGestionArrete typeGestion = getCurrentAvenant().contrat().toTypeContratTravail().typeGestionArrete();
		boolean arreteVisible = typeGestion != null || getCurrentAvenant().noArrete() != null || getCurrentAvenant().dateArrete() != null;
		myView.getViewArrete().setVisible(arreteVisible);

		myView.getBtnPrintArrete().setEnabled(getCurrentAvenant() != null && peutImprimer() && myView.getTfDateArrete().getText().length() > 0);

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		getEdc().revert();	
		setCurrentAvenant(null);
		myView.setVisible(false);		

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		myCtrlSpec.setDateReference(getDateDebut());
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		getCurrentAvenant().setContratRelationship(getCurrentContrat());
		myView.setVisible(false);
	}

}

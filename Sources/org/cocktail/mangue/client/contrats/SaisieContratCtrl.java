// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.contrats;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.contrats.SaisieContratView;
import org.cocktail.mangue.client.select.TypeContratArbreSelectCtrl;
import org.cocktail.mangue.client.select.TypeContratListeSelectCtrl;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeOrigineFinancement;
import org.cocktail.mangue.common.modele.nomenclatures.NomenclatureAvecDate;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EOContratPeriodesEssai;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Controleur de la saisie d'un contrat
 */
public class SaisieContratCtrl extends ModelePageSaisie {

	private static SaisieContratCtrl sharedInstance;
	private SaisieContratView myView;

	private EOContrat 					currentContrat;
	private EORne 						currentUai;
	private EOTypeContratTravail 		currentTypeContrat;
	private EOTypeRecrutementHu 		currentTypeRecrutement;
	private EODisplayGroup				eodPeriodesEssai;
	private UAISelectCtrl 				 myUAISelectCtrl;
	private EOIndividu 						currentIndividu;
	private EOContratPeriodesEssai			currentPeriodeEssai;

	private TypeContratArbreSelectCtrl 	myTypeContratArbreSelectCtrl;
	private TypeContratListeSelectCtrl	 	myTypeContratListeSelectCtrl;
	
	private boolean agentTitulaire;

	/**
	 * Constructeur
	 * 
	 * @param edc : editingContext
	 */
	public SaisieContratCtrl(EOEditingContext edc) {

		super(edc);

		eodPeriodesEssai = new EODisplayGroup();
		myView = new SaisieContratView(new JFrame(), true, eodPeriodesEssai);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());
		setDateListeners(myView.getTfDateFinAnticipee());

		myView.getBtnAddPeriodeEssai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterPeriodeEssai();}}
				);
		myView.getBtnDelPetiodeEssai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerPeriodeEssai();}}
				);

		myView.getBtnGetUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectUai();}}
				);
		myView.getBtnDelUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delUai();}}
				);
		myView.getBtnGetTypeContrat().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectTypeContrat();}}
				);

		CocktailUtilities.initTextField(myView.getTfUai(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeContrat(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeRecrutement(), false, false);
		CocktailUtilities.initTextField(myView.getTfReferenceReglementaire(), false, false);
		myView.getCheckCIR().setVisible(false);
		

		myView.getLblMessage().setText("");
		myView.setOriginesFinancement(EOTypeOrigineFinancement.fetchAll(edc));
		myView.getMyEOTable().addListener(new ListenerPeriodes());

	}

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : SaisieContratCtrl.sharedInstance(edc).methode
	 * 
	 * @param edc : editingContext
	 * @return une instance de la classe
	 */
	public static SaisieContratCtrl sharedInstance(EOEditingContext edc)	{
		if (sharedInstance == null) {
			sharedInstance = new SaisieContratCtrl(edc);
		}
		return sharedInstance;
	}

	private EOContrat getCurrentContrat() {
		return currentContrat;
	}
	public void setCurrentContrat(EOContrat currentContrat) {
		this.currentContrat = currentContrat;
	}
	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}	

	public EOContratPeriodesEssai getCurrentPeriodeEssai() {
		return currentPeriodeEssai;
	}

	public void setCurrentPeriodeEssai(EOContratPeriodesEssai currentPeriodeEssai) {
		this.currentPeriodeEssai = currentPeriodeEssai;
	}

	public EORne getCurrentUai() {
		return currentUai;
	}
	public void setCurrentUai(EORne currentUai) {
		this.currentUai = currentUai;
		myView.getTfUai().setText("");
		if (currentUai != null)
			CocktailUtilities.setTextToField(myView.getTfUai(), currentUai.codeEtLibelle());
	}

	public EOTypeContratTravail getCurrentTypeContrat() {
		return currentTypeContrat;
	}

	public void setCurrentTypeContrat(EOTypeContratTravail currentTypeContrat) {
		this.currentTypeContrat = currentTypeContrat;
		myView.getTfTypeContrat().setText("");
		myView.getCheckCIR().setSelected(false);
		if (currentTypeContrat != null) {
			CocktailUtilities.setTextToField(myView.getTfTypeContrat(), currentTypeContrat.codeEtLibelle());
			myView.getCheckCIR().setSelected(currentTypeContrat.estPourCir());
		}
	}

	public EOTypeRecrutementHu getCurrentTypeRecrutement() {
		return currentTypeRecrutement;
	}

	public void setCurrentTypeRecrutement(EOTypeRecrutementHu currentTypeRecrutement) {
		this.currentTypeRecrutement = currentTypeRecrutement;
		myView.getTfTypeRecrutement().setText("");
		if (currentTypeRecrutement != null)
			CocktailUtilities.setTextToField(myView.getTfTypeRecrutement(), currentTypeRecrutement.libelle());
	}

	/**
	 * 
	 * @return
	 */
	public EOTypeOrigineFinancement getCurrentOrigineFinancement() {
		if (myView.getPopupOrigineFinancement().getSelectedIndex()  > 0)
			return (EOTypeOrigineFinancement)myView.getPopupOrigineFinancement().getSelectedItem();
		return null;
	}

	public void setCurrentOrigineFinancement(
			EOTypeOrigineFinancement currentOrigineFinancement) {
		myView.getPopupOrigineFinancement().setSelectedItem(currentOrigineFinancement);
	}

	/**
	 * Action d'ajouter
	 * @param individu : individu
	 * @return un contrat
	 */
	public EOContrat ajouter(EOIndividu individu)	{
		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(true);
		setModeCreation(true);
		clearDatas();

		myView.setTitle("CONTRATS ( " + individu.identitePrenomFirst() + ")");
		setCurrentIndividu(individu);
		traitementsPourCreation();

		myView.setVisible(true);

		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentContrat();
	}


	public EOContrat renouveler(EOContrat contrat) {
		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(true);
		clearDatas();
		setModeCreation(true);

		setCurrentContrat(EOContrat.renouveler(getEdc(), contrat));
		setCurrentIndividu(contrat.individu());
		updateDatas();

		myView.setVisible(true);		

		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(false);

		return currentContrat;
	}


	/**
	 * Action de modifier
	 * @param contrat : le contrat à modifier
	 * @return le contrat
	 */
	public boolean modifier(EOContrat contrat) {
		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(true);
		clearDatas();
		setModeCreation(false);

		setCurrentContrat(contrat);
		setCurrentIndividu(getCurrentContrat().individu());
		myView.setTitle("CONTRATS ( " + getCurrentContrat().individu().identitePrenomFirst() + ")");
		updateDatas();
		myView.setVisible(true);
		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentContrat() != null;
	}

	/**
	 * 
	 * @param detail
	 */
	public void ajouterPeriodeEssai() {

		getCurrentContrat().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentContrat().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));

		EOContratPeriodesEssai myDetail = SaisiePeriodeEssaiCtrl.sharedInstance(getEdc()).ajouter(getCurrentContrat());
		if (myDetail != null) {
			eodPeriodesEssai.insertObjectAtIndex(myDetail, 0);
			myView.getMyEOTable().updateData();
		}
		updateInterface();
	}

	/**
	 *
	 */
	public void supprimerPeriodeEssai() {

		try {
			getEdc().deleteObject(getCurrentPeriodeEssai());
			eodPeriodesEssai.deleteSelection();
			myView.getMyEOTable().updateData();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * 
	 */
	private void selectTypeContrat() {

		CRICursor.setWaitCursor(myView);

		EOTypeContratTravail typeContrat = null;

		NSTimestamp myDateFin = CocktailUtilities.getDateFromField(myView.getTfDateFin());
		
		setAgentTitulaire(!EOChangementPosition.peutAvoirContratSurPeriode(getEdc(), getCurrentContrat().individu(),
				myDateFin, myDateFin));
		EOQualifier qualifier = EOTypeContratTravail.getQualifierVacataires(false);

		// Est t on dans le cadre de la nouvelle nomenclature des contrats
		if (myDateFin == null || DateCtrl.isAfterEq(myDateFin, DateCtrl.stringToDate(ManGUEConstantes.NOMENCLATURE_CONTRATS))) {

			if (myTypeContratArbreSelectCtrl == null) {
				myTypeContratArbreSelectCtrl = new TypeContratArbreSelectCtrl(getEdc());
			}
			
			if (myTypeContratListeSelectCtrl == null) {
				myTypeContratListeSelectCtrl = new TypeContratListeSelectCtrl(getEdc());
			}

			String typeAffichage = EOGrhumParametres.getValueParam(ManGUEConstantes.PARAM_KEY_AFF_TYPE_CONTRAT);

			if (typeAffichage.equals("A")) {
				typeContrat = myTypeContratArbreSelectCtrl.getTypeContrat(myDateFin, qualifier, isAgentTitulaire());
			} else {
				typeContrat = (EOTypeContratTravail) myTypeContratListeSelectCtrl.getObject(EOTypeContratTravail.findForSelection(getEdc(), myDateFin));			
			}

		} else {
			
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			
			qualifiers.addObject(EOTypeContratTravail.getQualifierVisible(true));
			qualifiers.addObject(EOTypeContratTravail.getQualifierVacataires(false));
			qualifiers.addObject(NomenclatureAvecDate.qualifierPourPeriode(myDateFin, myDateFin));
			NSArray<INomenclature> typesContrat = NomenclatureFinder.findWithQualifier(getEdc(), EOTypeContratTravail.ENTITY_NAME, new EOAndQualifier(qualifiers));
			
			typeContrat = (EOTypeContratTravail) TypeContratListeSelectCtrl.sharedInstance(getEdc()).getObject(typesContrat);
			
		}

		if (typeContrat != null) {

			setCurrentTypeContrat(typeContrat);
			CocktailUtilities.setTextToField(myView.getTfReferenceReglementaire(), getCurrentTypeContrat().refReglementaire());

			if (!getCurrentTypeContrat().estHospitalier()) {
				setCurrentTypeRecrutement(null);
				getCurrentContrat().setTypeRecrutementRelationship(null);
			}
		}

		updateInterface();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void selectUai() {

		if (myUAISelectCtrl == null)
			myUAISelectCtrl = new UAISelectCtrl(getEdc());
		EORne rne = (EORne)myUAISelectCtrl.getObject();
		if (rne != null)
			setCurrentUai(rne);
		updateInterface();
	}
	private void delUai() {
		setCurrentUai(null);
		updateInterface();
	}


	/**
	 * 
	 */
	protected void traitementsAvantValidation() {
		//Prend en compte les dates saisies pour voir si la période du contrat couvre une carrière
		setAgentTitulaire(!EOChangementPosition.peutAvoirContratSurPeriode(getEdc(), getCurrentContrat().individu(),
				CocktailUtilities.getDateFromField(myView.getTfDateDebut()), CocktailUtilities.getDateFromField(myView.getTfDateFin())));


		EOContrat lastContrat = null;
		boolean takeNext = false;
		NSArray<EOContrat> contrats = EOSortOrdering.sortedArrayUsingKeyOrderArray(ContratsCtrl.sharedInstance(getEdc()).getContrats(), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		for (EOContrat myContrat : contrats) {
			if (!takeNext) {
				if (myContrat == getCurrentContrat()) {
					takeNext = true;
				}
			}
		}
		if (lastContrat != null) {
			//	vérifier si la date de fin est nulle que la date de début est bien la dernière
			if (getCurrentContrat().dateFin() == null && DateCtrl.isBefore(getCurrentContrat().dateDebut(),lastContrat.dateDebut())) {
				throw new NSValidation.ValidationException("Vous n'avez pas fourni de date de fin, or il existe un autre contrat commençant après cette date");
			}
			// clôre le contrat précédent si nécessaire
			if (lastContrat.dateFin() == null || DateCtrl.isAfterEq(lastContrat.dateFin(), getCurrentContrat().dateDebut())) {
				// on vérifie la quotité de l'avenant précédent	
				EOContratAvenant lastAvenant = lastContrat.avenantCourant();
				if (lastAvenant != null && lastAvenant.quotite() != null && lastAvenant.quotite().intValue() == 100)  {
					// Si la quotité précédent
					String message = "Le contrat de " + lastContrat.toTypeContratTravail().libelleLong();
					if (lastContrat.dateFin() == null) {
						message = message + " n'a pas de date de fin ";
					} else {
						message = message + " se termine le " + lastContrat.dateFinFormatee() + " ";
					}
					message = message + "et a une quotité de 100%.\nLa date de fin va être modifiée ainsi éventuellement celle des détails.\nEst-ce bien ce que vous voulez ?";
					if (EODialogs.runConfirmOperationDialog("Attention",message,"OK","Annuler")) {
						if (lastContrat.peutFermerADate(getCurrentContrat().dateDebut()) == false) {
							throw new NSValidation.ValidationException("Le contrat précédent commence après ou a un avenant commençant après le " + getCurrentContrat().dateDebutFormatee() + ", la modification est impossible");
						}
						lastContrat.fermerContratADate(DateCtrl.jourPrecedent(getCurrentContrat().dateDebut()));	// pas nul car sinon pas de validation
					}
				}
			}
		}

		getCurrentContrat().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentContrat().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));
		getCurrentContrat().setDateFinAnticipee(CocktailUtilities.getDateFromField(myView.getTfDateFinAnticipee()));

		getCurrentContrat().setToTypeContratTravailRelationship(getCurrentTypeContrat());
		getCurrentContrat().setToRneRelationship(getCurrentUai());
		getCurrentContrat().setTypeRecrutementRelationship(getCurrentTypeRecrutement());
		getCurrentContrat().setToOrigineFinancementRelationship(getCurrentOrigineFinancement());
		getCurrentContrat().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTaObservations()));

		getCurrentContrat().setEstFonctionnaire(myView.getCheckFonctionnaire().isSelected());
		getCurrentContrat().setEstCIR(myView.getCheckCIR().isSelected());

		// Modification automatique de la date de fin d'un détail lors de la saisie d'une date de fin anticipee
		if (getCurrentContrat().dateFinAnticipee() != null) {
			NSArray<EOContratAvenant> details = EOContratAvenant.findForContrat(getEdc(), getCurrentContrat());
			if (details.size() > 0) {
				EOContratAvenant dernierDetail = details.lastObject();
				dernierDetail.setDateFin(getCurrentContrat().dateFinAnticipee());
			}
		}

		getCurrentContrat().validateObjectForSave();
		
		// Si le type de contrat n'est pas autorise pour les titulaires ==> ERREUR
		if (!getCurrentTypeContrat().estAutorisePourTitulaire()) {
			
			// Controle d'une carriere existante
			NSArray<EOCarriere> carrieres = EOCarriere.findForPeriode(getEdc(), getCurrentIndividu(), 
					CocktailUtilities.getDateFromField(myView.getTfDateDebut()), 
					CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			if (carrieres.size() > 0) {
				throw new NSValidation.ValidationException("Cet agent a déjà une carriere sur cette période !");
			}
			// Controle d'un passe existant (Hors services valides)
			NSArray<EOPasse> passes = EOPasse.findForPeriode(getEdc(), getCurrentIndividu(), 
					CocktailUtilities.getDateFromField(myView.getTfDateDebut()), 
					CocktailUtilities.getDateFromField(myView.getTfDateFin()));
			for (EOPasse passe : passes) {
				if (!passe.toTypeService().estTypeServiceValide()) {
					throw new NSValidation.ValidationException("Cet agent a déjà un PASSE (" + passe.toTypeService().libelleLong() + ") sur cette période !");
				}
			}
		}

		if (isAgentTitulaire() && !getCurrentTypeContrat().estAutorisePourTitulaire()) {
			throw new NSValidation.ValidationException(MangueMessagesErreur.ERREUR_CONTRAT_NON_AUTORISE_POUR_TITULAIRES);
		}

		if (!peutSaisirPeriodeEssai()) {
			for (EOContratPeriodesEssai periode : (NSArray<EOContratPeriodesEssai>) eodPeriodesEssai.allObjects()) {
				getEdc().deleteObject(periode);				
			}
		}

		// verifier la quotite des autres contrats en cours pour voir si on autorise un contrat entre ces dates dans le cas
		// ou le contrat ne vient pas d'etre annule
		if (!isModeCreation() && getCurrentTypeContrat() != null) {
			double quotiteTotale = EOContratAvenant.calculerQuotiteTotaleAvecContratOuAvenant(getEdc(), getCurrentContrat(), null);
			if (quotiteTotale > 100) {
				throw new NSValidation.ValidationException("Cet agent a déjà d'autres contrats en cours pour une quotité totale de 100%.\nVeuillez modifier les dates du contrat ou annuler");
			}
			if (EOContratHeberges.aContratHeberge(getEdc(),getCurrentIndividu(), getCurrentContrat().dateDebut(),getCurrentContrat().dateFin())) {
				throw new NSValidation.ValidationException("EOContrat - L'individu a un contrat d'hébergé pendant cette période");
			}
		}


	}

	/**
	 * 
	 */
	protected  void traitementsApresValidation() {

		if (getCurrentContrat().toTypeContratTravail() != null
				&& getCurrentContrat().toTypeContratTravail().dateFermeture() != null 
				&& (getCurrentContrat().dateFin() == null ||
				DateCtrl.isAfter(getCurrentContrat().dateFin(), getCurrentContrat().toTypeContratTravail().dateFermeture()))) {

			EODialogs.runInformationDialog(CocktailConstantes.ATTENTION, "Le contrat de travail se termine après la date de validité de ce type de contrat");
		}

		if (isModeCreation()) {

			getCurrentContrat().creationAvenantContrat();

			//Message d'alerte si le type de contrat exige une saisie de grade
			if (getCurrentContrat().avenantCourant() != null
					&& getCurrentContrat().avenantCourant().dateDebut() != null
					&& getCurrentContrat().toTypeContratTravail() != null 
					&& getCurrentContrat().toTypeContratTravail().requiertGrade()
					&& getCurrentContrat().avenantCourant().toGrade() == null) {
				EODialogs.runInformationDialog(CocktailConstantes.ATTENTION, MangueMessagesErreur.ATTENTION_CONTRAT_GRADE_A_RENSEIGNER);
			}
		}

		myView.setVisible(false);

	}

	/**
	 * 
	 * @return
	 */
	private void testPeriodeChevauchement() {

		CocktailUtilities.viderLabel(myView.getLblMessage());				

		NSTimestamp myDateFin = CocktailUtilities.getDateFromField(myView.getTfDateFin());
		if (myDateFin != null && CocktailUtilities.getDateFromField(myView.getTfDateFinAnticipee()) != null)
			myDateFin = CocktailUtilities.getDateFromField(myView.getTfDateFinAnticipee());

		NSTimestamp myDateDebut = CocktailUtilities.getDateFromField(myView.getTfDateDebut());
		if (myDateDebut == null)
			return;

		for (EOContrat myContrat : ContratsCtrl.sharedInstance(getEdc()).getContrats()) {

			NSTimestamp myContratFin = myContrat.dateFin();
			if (myContrat.dateFinAnticipee() != null)
				myContratFin = myContrat.dateFin();

			if (myContrat != getCurrentContrat()) {
				if (DateCtrl.isBetween(myDateDebut, myContrat.dateDebut(), myContratFin) ) {
					CocktailUtilities.setTextToLabel(myView.getLblMessage(), "ATTENTION : Chevauchement de période avec un contrat existant du " + DateCtrl.dateToString(myContrat.dateDebut()) + " au " + DateCtrl.dateToString(myContrat.dateFin()) + " !");				
					break;
				}

				if (myDateFin != null) {
					if (DateCtrl.isBetween(myDateFin, myContrat.dateDebut(), myContratFin)) {
						CocktailUtilities.setTextToLabel(myView.getLblMessage(), "ATTENTION : Chevauchement de période avec un contrat existant du " + DateCtrl.dateToString(myContrat.dateDebut()) + " au " + DateCtrl.dateToString(myContrat.dateFin()) + " !");				
						break;
					}
				}

			}
		}

	}


	@Override
	protected void clearDatas() {

		eodPeriodesEssai.setObjectArray(new NSArray<EOContratPeriodesEssai>());
		myView.getMyEOTable().updateData();

		CocktailUtilities.viderTextField(myView.getTfTypeContrat());
		CocktailUtilities.viderTextField(myView.getTfUai());
		CocktailUtilities.viderTextArea(myView.getTaObservations());
		CocktailUtilities.viderTextField(myView.getTfTypeRecrutement());
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfDateFinAnticipee());
		CocktailUtilities.viderTextField(myView.getTfReferenceReglementaire());

		setCurrentTypeContrat(null);
		setCurrentUai(null);

	}

	@Override
	protected void updateDatas() {
		clearDatas();

		if (getCurrentContrat() != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentContrat().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentContrat().dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateFinAnticipee(), getCurrentContrat().dateFinAnticipee());
			CocktailUtilities.setTextToArea(myView.getTaObservations(), getCurrentContrat().commentaire());

			myView.getCheckFonctionnaire().setSelected(getCurrentContrat().estFonctionnaire());
			myView.getCheckCIR().setSelected(getCurrentContrat().estCIR());
			setCurrentTypeContrat(getCurrentContrat().toTypeContratTravail());
			setCurrentUai(getCurrentContrat().toRne());
			setCurrentOrigineFinancement(getCurrentContrat().toOrigineFinancement());
			
			if (getCurrentTypeContrat() != null) {
				CocktailUtilities.setTextToField(myView.getTfReferenceReglementaire(), getCurrentTypeContrat().refReglementaire());
			}

			testPeriodeChevauchement();

			eodPeriodesEssai.setObjectArray(EOContratPeriodesEssai.findForContrat(getEdc(), getCurrentContrat()));
			myView.getMyEOTable().updateData();

		}
		updateInterface();

	}

	private NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	private NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}

	/**
	 * 
	 * @return
	 */
	private boolean peutSaisirPeriodeEssai() {
		NSTimestamp dateReference = DateCtrl.stringToDate(ManGUEConstantes.DEBUT_PERIODES_ESSAI);
		return
				getDateDebut() != null 
				&& (getDateFin() == null || DateCtrl.isAfterEq(getDateFin(), getDateDebut()) )
				&& DateCtrl.isAfterEq(getDateDebut(), dateReference);
	}

	@Override
	protected void updateInterface() {
		myView.getBtnGetTypeContrat().setEnabled(!StringCtrl.chaineVide(myView.getTfDateDebut().getText()));

		CocktailUtilities.initTextField(myView.getTfDateFinAnticipee(), false, !StringCtrl.chaineVide(myView.getTfDateFin().getText()));

		myView.getBtnDelUai().setEnabled(getCurrentUai() != null);
		myView.getViewTypeRecrutement().setVisible(getCurrentTypeContrat() != null && getCurrentTypeContrat().estHospitalier());
		myView.getCheckCIR().setVisible(getCurrentTypeContrat() != null && getCurrentTypeContrat().estPourCir());

		myView.getBtnAddPeriodeEssai().setEnabled(getCurrentContrat() != null && CocktailUtilities.getDateFromField(myView.getTfDateDebut()) != null);
		myView.getBtnDelPetiodeEssai().setEnabled(getCurrentPeriodeEssai() != null);		

		myView.getPanelPeriodesEssai().setVisible(peutSaisirPeriodeEssai());
	}

	@Override
	protected void traitementsPourAnnulation() {
		setCurrentContrat(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
		setCurrentContrat(EOContrat.creer(getEdc(), getCurrentIndividu()));

		getCurrentContrat().setToRneRelationship((EORne)NomenclatureFinder.findForCode(getEdc(), EORne.ENTITY_NAME, EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE)));
		updateDatas();	
	}

	@Override
	protected void traitementsChangementDate() {
		testPeriodeChevauchement();
		updateInterface();
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerPeriodes implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentPeriodeEssai((EOContratPeriodesEssai)eodPeriodesEssai.selectedObject());
		}
	}
	
	private void majAgentTitulaire() {
		setAgentTitulaire(!EOChangementPosition.peutAvoirContratSurPeriode(getEdc(), getCurrentContrat().individu(),
				CocktailUtilities.getDateFromField(myView.getTfDateDebut()), CocktailUtilities.getDateFromField(myView.getTfDateFin())));
	}

	public boolean isAgentTitulaire() {
		return agentTitulaire;
	}

	public void setAgentTitulaire(boolean agentTitulaire) {
		this.agentTitulaire = agentTitulaire;
	}
}

package org.cocktail.mangue.client.formations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.formations.ListeFormationsView;
import org.cocktail.mangue.client.individu.infoscomp.FormationsCtrl;
import org.cocktail.mangue.client.select.IndividuSelectCtrl;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuFormations;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class ListeFormationsCtrl {

	private static ListeFormationsCtrl 	sharedInstance;
	private static Boolean 			MODE_MODAL = Boolean.FALSE;
	private EOEditingContext		edc;
	private ListeFormationsView 	myView;
	private EODisplayGroup 			eod;
	private boolean 				saisieEnabled, isLocked;

	private ListenerFormation 		listenerFormation = new ListenerFormation();
	private EOIndividuFormations	currentFormation;

	private EOAgentPersonnel		currentUtilisateur;
	private boolean 				gereModule;

	public ListeFormationsCtrl(EOEditingContext edc) {

		setEdc(edc);

		eod = new EODisplayGroup();
		myView = new ListeFormationsView(null, MODE_MODAL.booleanValue(), eod);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);
		myView.getBtnRechercher().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {rechercher();}}
		);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporter();}}
		);
		
		CocktailUtilities.initPopupAvecListe(myView.getPopupAnnees(), new NSArray(CocktailConstantes.LISTE_ANNEES_DESC), true);
		Integer exerciceCir = DateCtrl.getYear(new NSTimestamp());
		
		if (DateCtrl.getCurrentMonth() == 11)
			exerciceCir = new Integer(exerciceCir.intValue()+1);
				
		myView.getPopupAnnees().setSelectedItem(DateCtrl.getYear(new NSTimestamp()));

		myView.getMyEOTable().addListener(listenerFormation);

		myView.getTfFiltreNom().getDocument().addDocumentListener(new ADocumentListener());

		setCurrentUtilisateur(((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());

		setSaisieEnabled(false);
		updateInterface();
		
	}
	
	public EOEditingContext getEdc() {
		return edc;
	}
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
		setGereModule(currentUtilisateur.peutGererCarrieres());
	}
	public boolean isGereModule() {
		return gereModule;
	}
	public void setGereModule(boolean gereModule) {
		this.gereModule = gereModule;
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ListeFormationsCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ListeFormationsCtrl(editingContext);
		return sharedInstance;
	}
	

	/**
	 * 
	 */
	public void open()	{

		if (!isGereModule()) {
			EODialogs.runInformationDialog("ERREUR", "Vous n'avez pas les droits nécessaires pour la gestion des formations !");
			return;
		}

		CRICursor.setWaitCursor(myView);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	

	public EOIndividuFormations getCurrentFormation() {
		return currentFormation;
	}

	public void setCurrentFormation(EOIndividuFormations currentFormation) {
		this.currentFormation = currentFormation;
	}

	public void toFront() {
		myView.toFront();
	}
	public boolean saisieEnabled() {
		return saisieEnabled;
	}
	public boolean isLocked() {
		return isLocked;
	}
	public void setIsLocked(boolean yn) {
		isLocked = yn;
		updateInterface();
	}

	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	/**
	 * 
	 */
	private void actualiser()	{

		CRICursor.setWaitCursor(myView);
		rechercher();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		if (!StringCtrl.chaineVide(myView.getTfFiltreNom().getText()))
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuFormations.TO_INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreNom().getText()+"*")));
		
		if (myView.getPopupAnnees().getSelectedIndex() > 0) {

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuFormations.DATE_DEBUT_KEY + " >= %@", new NSArray(CocktailUtilities.debutAnneeCivile((Integer)myView.getPopupAnnees().getSelectedItem()))));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuFormations.DATE_DEBUT_KEY + " <= %@", new NSArray(CocktailUtilities.finAnneeCivile((Integer)myView.getPopupAnnees().getSelectedItem()))));
			
		}
		
		return new EOAndQualifier(qualifiers);
	}

	/**
	 * 
	 */
	private void rechercher() {

		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EOIndividuFormations.findForQualifier(getEdc(), filterQualifier()));
		filter();
		CRICursor.setDefaultCursor(myView);

	}
	
	/**
	 * 
	 */
	private void exporter() {

		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "FORMATIONS";
			
			saveDialog.setSelectedFile(new File(nomFichierExport + CocktailConstantes.EXTENSION_CSV));
			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = enteteExport();

				for (EOIndividuFormations myRecord : new NSArray<EOIndividuFormations>(EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(), EOIndividuFormations.SORT_ARRAY_NOM_ASC) ))
					texte += texteExportPourRecord(myRecord) + CocktailExports.SAUT_DE_LIGNE;
				
				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

				CRICursor.setDefaultCursor(myView);

				UtilitairesFichier.openFile(file.getPath());
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @return
	 */
	private String enteteExport() {

		String entete = "";

		entete += "NOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PRENOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "DUREE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "DUREE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "LIBELLE";
		entete += CocktailExports.SAUT_DE_LIGNE;

		return entete;
	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EOIndividuFormations record)    {

		String texte = "";

		// NOM
		texte += CocktailExports.ajouterChamp(record.toIndividu().nomUsuel());
		// PRENOM
		texte += CocktailExports.ajouterChamp(record.toIndividu().prenom());
		// DEBUT
		texte += CocktailExports.ajouterChampDate(record.dateDebut());
		// FIN
		texte += CocktailExports.ajouterChampDate(record.dateFin());
		// DUREE
		texte += CocktailExports.ajouterChamp(record.duree());
		// DUREE
		texte += CocktailExports.ajouterChamp(record.dureeFormation());
		// LIBELLE
		texte += CocktailExports.ajouterChamp(record.llFormation());

		return texte;
		
	}

	/**
	 * 
	 */
	private void filter() 	{

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();
		CocktailUtilities.setTextToLabel(myView.getLblNBDecharges(), eod.displayedObjects().count() + " Formations");

		updateInterface();

	}

	/**
	 * 
	 */
	private void ajouter() {
		EOIndividu selectedIndividu = IndividuSelectCtrl.sharedInstance(getEdc()).getIndividu();
		if (selectedIndividu != null) {
			FormationsCtrl ctrlFormations = new FormationsCtrl(null, getEdc());
			ctrlFormations.open(selectedIndividu);
		}
	}
	/**
	 * 
	 */
	private void modifier() {

		FormationsCtrl ctrlFormations = new FormationsCtrl(null, getEdc());
		ctrlFormations.open(getCurrentFormation().toIndividu());
		
	}


	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette formation ?", "Oui", "Non"))		
			return;

		try {
			getEdc().deleteObject(getCurrentFormation());
			getEdc().saveChanges();
			actualiser();

		}
		catch(Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnAjouter().setVisible(true && isGereModule());
		myView.getBtnModifier().setEnabled(getCurrentFormation() != null && isGereModule());
		myView.getBtnSupprimer().setVisible(getCurrentFormation() != null && isGereModule());

		if (saisieEnabled() || isLocked())
			myView.getMyEOTable().setForeground(CocktailConstantes.BG_COLOR_GREY);
		else
			myView.getMyEOTable().setForeground(CocktailConstantes.BG_COLOR_BLACK);

	}

	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			actualiser();
		}
	}

	private class ListenerFormation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			modifier();
		}

		public void onSelectionChanged() {
			setCurrentFormation((EOIndividuFormations)eod.selectedObject());
			updateInterface();
		}
	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		public void removeUpdate(DocumentEvent e) {
			filter();		
		}
	}
	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			//actualiser();
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}

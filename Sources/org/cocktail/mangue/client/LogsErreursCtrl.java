package org.cocktail.mangue.client;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Timer;
import java.util.TimerTask;

import org.cocktail.common.utilities.StringCtrl;
import org.cocktail.mangue.common.modele.manager.Manager;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSTimestamp;

public class LogsErreursCtrl {

	public static final String DESTINATAIRES_LOGS_ERREUR = "cyril.pinsard@asso-cocktail.fr, chama.laatik@asso-cocktail.fr";
	private static final long DELAY_TEST_CONNECTION_SERVEUR = 50000;
	
	private static final String PARAM_DELAY_TEST_SERVEUR = "cocktail.delay.serverAlive";
	private static final String PARAM_TEST_SERVEUR = "cocktail.testServerAlive";

	private Manager manager;
	public String destintairesLogs;
	private Timer currentTimer;
	private boolean testServeur;
	private MyByteArrayOutputStream redirectedOutStream, redirectedErrStream;

	public LogsErreursCtrl(Manager manager) {

		System.out.println(">>> Gestion des erreurs et de la connection serveur :");
		
		setManager(manager);

		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));
		String destinataires =DESTINATAIRES_LOGS_ERREUR ;		
		setDestintairesLogs(destinataires);		
		// testServeur
		try {
			setTestServeur(ServerProxy.clientSideRequestGetBooleanParam(manager.getEdc(), PARAM_TEST_SERVEUR));
		}
		catch (Exception e) {
			setTestServeur(false);
		}
		
		// maintenir la connection avec le serveur et verifier les logs
		try {

			String paramDelay = ServerProxy.clientSideRequestGetParam(manager.getEdc(), PARAM_DELAY_TEST_SERVEUR);
			currentTimer = new Timer(true);
			if (paramDelay == null) {
				currentTimer.scheduleAtFixedRate(new TestServerTask(), 5000, DELAY_TEST_CONNECTION_SERVEUR);
			}
			else {
				System.out.println("\t > Delai des tests de connection au serveur : " + paramDelay);
				currentTimer.scheduleAtFixedRate(new TestServerTask(), 5000, new Long(paramDelay));
			}
		}
		catch (Exception e) {
		}
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public boolean isTestServeur() {
		return testServeur;
	}

	public void setTestServeur(boolean testServeur) {
		System.out.println("\t > Test connection serveur : " + testServeur);
		this.testServeur = testServeur;
	}

	public String getDestintairesLogs() {
		return destintairesLogs;
	}
	public void setDestintairesLogs(String destintairesLogs) {
		this.destintairesLogs = destintairesLogs;
		String param = ServerProxy.clientSideRequestGetParam(manager.getEdc(), "MAIL_LOGS_ERREURS");
		if (param != null) {
			this.destintairesLogs += "," + param;
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;

		public MyByteArrayOutputStream(PrintStream out) {
			this.out = out;
		}

		public synchronized void write(int b) {
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			super.write(b, off, len);
			out.write(b, off, len);
		}
	}

	public String outLogs() {
		return redirectedOutStream.toString();
	}

	public String errLogs() {
		return redirectedErrStream.toString();
	}

	public void cleanLogs(String type) {

		if (type.equals("CLIENT")) {
			redirectedOutStream.reset();
			redirectedErrStream.reset();
		}

		if (type.equals("SERVER")) {
			((EODistributedObjectStore) manager.getEdc().parentObjectStore()).invokeRemoteMethodWithKeyPath(
					manager.getEdc(), 
					"session",
					"clientSideRequestCleanLogs", 
					new Class[] {},
					new Object[] {}, 
					false);
			;
		}
	}

	/**
	 * 
	 * @param typeLog
	 */
	public void sendLog(String type, String destinataires) {

		String emetteur = manager.getUserInfo().email();

		try {

			String sujet = "MANGUE LOGS - ";
			if (type != null) {
				sujet = sujet + "(" + type + ") - " ;
			}
			sujet = sujet + ServerProxy.clientSideRequestAppVersion(manager.getEdc());
			sujet = sujet + DateCtrl .dateToString(new NSTimestamp(), "%d/%m/%Y %H:%M")	+ ")";

			String message = "LOGS CLIENT ET SERVEUR.";
			message = message + "\nINDIVIDU CONNECTE : " + manager.getUserInfo().nom()
					+ " " + manager.getUserInfo().prenom();

			message = message
					+ "\n\n************* LOGS CLIENT *****************";
			message = message + "\nOUTPUT log :\n\n"
					+ redirectedOutStream.toString() + "\n\nERROR log :\n\n"
					+ redirectedErrStream.toString();

			message = message
					+ "\n\n************* LOGS SERVER *****************";
			message = message + "\n*****************************************";

			String outLog = (String) ((EODistributedObjectStore) manager.getEdc()
					.parentObjectStore()).invokeRemoteMethodWithKeyPath(
							manager.getEdc(), "session",
							"clientSideRequestOutLog", new Class[] {}, new Object[] {},
							false);
			String errLog = (String) ((EODistributedObjectStore) manager.getEdc()
					.parentObjectStore()).invokeRemoteMethodWithKeyPath(
							manager.getEdc(), "session",
							"clientSideRequestErrLog", new Class[] {}, new Object[] {},
							false);
			message = message + "\nOUTPUT log SERVER :\n\n" + outLog
					+ "\n\nERROR log SERVER :\n\n" + errLog;

			StringBuffer mail = new StringBuffer();
			mail.append(message);

			((EODistributedObjectStore) manager.getEdc()
					.parentObjectStore())
					.invokeRemoteMethodWithKeyPath(manager.getEdc(),
							"session", "clientSideRequestSendMail",
							new Class[] { String.class, String.class,
						String.class, String.class, String.class },
						new Object[] { emetteur, destinataires, null, sujet,
						mail.toString() }, false);

			EODialogs.runInformationDialog("ENVOI MAIL", "Le mail a bien été envoyé.");
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
	}

	/**
	 * 
	 */
	private void forceQuit() {
		((ApplicationClient)ApplicationClient.sharedApplication()).forceQuit();
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	public class TestServerTask extends TimerTask {

		public TestServerTask() {
		}
		public void run() {

			if (isTestServeur() && ServerProxy.clientSideRequestMsgToServer(manager.getEdc(), " ") == false) {
				EODialogs.runInformationDialog("ERREUR", ">>> Le serveur MANGUE a été coupé ou redémarré.\nL'application va se fermer !");
				sendLog("COUPURE", getDestintairesLogs());
				forceQuit();
			}
			
			// Teste la présence d'un NULL pointerException
			if (StringCtrl.containsIgnoreCase(errLogs(), "java.lang.NullPointerException")
					&& !StringCtrl.containsIgnoreCase(errLogs(), "SunToolkit")
					&& !StringCtrl.containsIgnoreCase(errLogs(), "IOException")) {
				EODialogs.runInformationDialog("ERREUR", ">>> Une erreur est survenue !\n Un message a été envoyé aux développeurs pour correction.\nVeuillez relancer l'application !");
				sendLog("EXCEPTION", getDestintairesLogs());
				forceQuit();
			}
		}
	}
}

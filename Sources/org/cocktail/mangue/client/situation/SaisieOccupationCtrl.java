// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.situation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.situation.SaisieOccupationView;
import org.cocktail.mangue.client.select.EmploiSelectCtrl;
import org.cocktail.mangue.client.select.IndividuFonctionSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.InfoCarriereContrat;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOIndividuFonction;
import org.cocktail.mangue.modele.grhum.EOQuotite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudget;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieOccupationCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;
	
	private SaisieOccupationView 	myView;
	private EODisplayGroup			eodCarriereContrat;

	private ListenerContratsCarriere listenerSituation = new ListenerContratsCarriere();
	private AffectationOccupationCtrl ctrlParent;
	private EOOccupation currentOccupation;
	private IEmploi currentEmploi;
	private EOIndividuFonction currentFonction;
	private InfoCarriereContrat	 currentSituation;

	/**
	 * Constructeur
	 * 
	 * @param globalEc           : editingContext
	 */
	public SaisieOccupationCtrl(AffectationOccupationCtrl ctrlParent, EOEditingContext edc) {

		super(edc);

		this.ctrlParent = ctrlParent;

		eodCarriereContrat = new EODisplayGroup();
		myView = new SaisieOccupationView(new JFrame(), true, eodCarriereContrat);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnGetEmploi().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectEmploi();}
		});
		myView.getBtnGetFonction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectFonction();}
		});
		myView.getBtnDelFonction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delFonction();}
		});

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		myView.getTfQuotite().addFocusListener(new FocusListenerQuotite());
		myView.getTfQuotite().addActionListener(new ActionListenerQuotite());
		myView.getMyEOTable().addListener(listenerSituation);

		CocktailUtilities.initTextField(myView.getTfQuotiteFinanciere(), false, false);
		CocktailUtilities.initTextField(myView.getTfEmploi(), false, false);
		CocktailUtilities.initTextField(myView.getTfFonction(), false, false);

	}

	public EOOccupation getCurrentOccupation() {
		return currentOccupation;
	}

	public void setCurrentOccupation(EOOccupation currentOccupation) {
		this.currentOccupation = currentOccupation;
	}

	/**
	 * @return l'emploi
	 */
	public IEmploi getCurrentEmploi() {
		return currentEmploi;
	}

	/**
	 * @param currentEmploi : l'emploi
	 */
	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
		CocktailUtilities.viderTextField(myView.getTfEmploi());
		if (currentEmploi != null) {
			CocktailUtilities.setTextToField(myView.getTfEmploi(), currentEmploi.getNoEmploiAffichage());
		}
	}
	public InfoCarriereContrat getCurrentSituation() {
		return currentSituation;
	}

	public void setCurrentSituation(InfoCarriereContrat currentSituation) {
		this.currentSituation = currentSituation;
	}
	/**
	 * 
	 * @return
	 */
	public EOIndividuFonction getCurrentFonction() {
		return currentFonction;
	}

	/**
	 * 
	 * @param currentFonction
	 */
	public void setCurrentFonction(EOIndividuFonction currentFonction) {
		this.currentFonction = currentFonction;
		CocktailUtilities.setTextToField(myView.getTfFonction(), "");
		if (currentFonction != null)
			CocktailUtilities.setTextToField(myView.getTfFonction(), currentFonction.llIndFnct());
	}

	/**
	 * 
	 */
	private void selectEmploi() {

		CRICursor.setWaitCursor(myView);

		IEmploi emploi = EmploiSelectCtrl.sharedInstance(getEdc()).getEmploi(CocktailUtilities.getDateFromField(myView.getTfDateDebut()),
				CocktailUtilities.getDateFromField(myView.getTfDateFin()));

		if (emploi != null) {
			setCurrentEmploi(emploi);

//			BigDecimal quotiteRestante = EOEmploi.calculerQuotiteRestanteMinimumSurPeriode(getEdc(), new NSArray(),getDateDebut(),getDateFin());
//			CocktailUtilities.setNumberToField(myView.getTfQuotite(), quotiteRestante);

		}

		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void selectFonction() {
		EOIndividuFonction fonction = IndividuFonctionSelectCtrl.sharedInstance(getEdc()).getFonction();
		if (fonction != null)
			setCurrentFonction(fonction);
	}

	private void delFonction() {
		setCurrentFonction(null);
	}

	/**
	 * 
	 * @param individu
	 * @return
	 */
	public EOOccupation ajouter(EOIndividu individu) {
		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(true);

		setModeCreation(true);

		clearDatas();
		myView.setTitle("OCCUPATION / AJOUT ( " + individu.identitePrenomFirst() + ")");

		setCurrentOccupation(EOOccupation.creer(getEdc(), individu));

		updateDatas();
		myView.setVisible(true);

		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentOccupation();
	}


	/**
	 * 
	 * @param occupation
	 * @return
	 */
	public boolean modifier(EOOccupation occupation) {

		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(true);
		setCurrentOccupation(occupation);
		myView.setTitle("OCCUPATION ( " + getCurrentOccupation().individu().identitePrenomFirst() + ")");

		setModeCreation(false);

		updateDatas();
		myView.setVisible(true);
		((ApplicationClient) EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentOccupation() != null;
	}

	/**
	 * @return la date du debut saisie
	 */
	private NSTimestamp getDateDebut() {
		return DateCtrl.stringToDate(myView.getTfDateDebut().getText());
	}

	/**
	 * @return la date du fin saisie
	 */
	private NSTimestamp getDateFin() {
		return DateCtrl.stringToDate(myView.getTfDateFin().getText());
	}

	/**
	 * 
	 * @return
	 */
	private boolean traitementsAvantValidationEnabled() {
		return getCurrentEmploi() != null && getDateDebut() != null && CocktailUtilities.getBigDecimalFromField(myView.getTfQuotite()) != null;
	}

	@Override
	/**
	 * 
	 * Traitements a effectuer avant la validation de la carriere. A de moment,
	 * les enregistrements ne sont pas effectues dans l'object carriere.
	 * 
	 * @return
	 */
	protected void traitementsAvantValidation() {

		if (traitementsAvantValidationEnabled()) {

			BigDecimal quotiteSaisie = CocktailUtilities.getBigDecimalFromField(myView.getTfQuotite());

			// Controle des chevauchements de periode avec quotite > 100 ou meme emploi
			NSMutableArray<EOOccupation> occupations = new NSMutableArray(ctrlParent.getOccupations());
			if (occupations.containsObject(getCurrentOccupation())) {
				occupations.removeObject(getCurrentOccupation());
			}
			occupations = new NSMutableArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(occupations, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT));

			BigDecimal quotiteTotale = quotiteSaisie;
			for (EOOccupation occ : occupations) {

				if (DateCtrl.chevauchementPeriode(occ.dateDebut(), occ.dateFin(), getDateDebut(), getDateFin())) {

					quotiteTotale = quotiteTotale.add(occ.quotite());

					if (quotiteTotale.doubleValue() > 100.0) {
						throw new ValidationException("La quotité totale d'occupation ne peut dépasser 100% pour cette période !");
					}
					if (occ.toEmploi() == getCurrentEmploi()) {
						throw new ValidationException("L'agent ne peut être affecté au même emploi pendant la même période !");
					}
				}
			}
			
			// Controle des rompus
			EOEditingContext ed = new EOEditingContext();		
			NSArray<EOOccupation> occupationsEmploi = EOOccupation.rechercherOccupationsPourEmploiEtPeriode(ed, getCurrentEmploi(), getDateDebut(), getDateFin());
			NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOOccupation.TO_EMPLOI_KEY +"."+ EOEmploi.NO_EMPLOI_AFFICHAGE_KEY , EOSortOrdering.CompareAscending));
			sorts.addObject(PeriodePourIndividu.SORT_DATE_DEBUT);
			occupationsEmploi = EOSortOrdering.sortedArrayUsingKeyOrderArray(occupationsEmploi,sorts);
			BigDecimal quotiteRestante = EOEmploi.calculerQuotiteRestanteMinimumSurPeriode(occupationsEmploi, getDateDebut(), getDateFin());

			if (!isModeCreation()) {
				quotiteRestante = quotiteRestante.add(getCurrentOccupation().quotite());
			}

			if (quotiteSaisie.floatValue() > quotiteRestante.floatValue()) {
				throw new ValidationException("Il ne reste que " + quotiteRestante + " % de quotité d'occupation disponible sur cet emploi.");
			}

		}

		if (EOGrhumParametres.findParametre(getEdc(), ManGUEConstantes.PARAM_KEY_OCCUPATION_CARRIERE).isParametreVrai()) {
			if (getCurrentSituation() == null) {
				throw new NSValidation.ValidationException("L'association d'un contrat ou d'une carrière est obligatoire !");			
			}
		}

		getCurrentOccupation().setToEmploiRelationship(getCurrentEmploi());
		getCurrentOccupation().setDateDebut(getDateDebut());
		getCurrentOccupation().setDateFin(getDateFin());
		getCurrentOccupation().setObservations(CocktailUtilities.getTextFromField(myView.getTfFonction()));
		getCurrentOccupation().setQuotite(CocktailUtilities.getBigDecimalFromField(myView.getTfQuotite()));

		if (getCurrentSituation() != null) {
			getCurrentOccupation().setToContratRelationship(getCurrentSituation().getCurrentContrat());
			getCurrentOccupation().setToCarriereRelationship(getCurrentSituation().getCurrentCarriere());
		}
		else {
			getCurrentOccupation().setToContratRelationship(null);
			getCurrentOccupation().setToCarriereRelationship(null);			
		}


	}

	/**
	 * 
	 */
	private void quotiteHasChanged() {
		myView.getTfQuotiteFinanciere().setText("");
		try {
			String myQuotite = NSArray.componentsSeparatedByString(myView.getTfQuotite().getText(), ",").componentsJoinedByString(".");
			if (!"".equals(myQuotite)) {
				myView.getTfQuotiteFinanciere().setText(EOQuotite.quotiteFinanciere(getEdc(), new BigDecimal(myQuotite)).toString());
			}
		} catch (Exception ex) {
		}
	}

	private class ActionListenerQuotite implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			quotiteHasChanged();
		}
	}

	private class FocusListenerQuotite implements FocusListener {
		public void focusGained(FocusEvent e) {
		}

		public void focusLost(FocusEvent e) {
			quotiteHasChanged();
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfEmploi());
		CocktailUtilities.viderTextField(myView.getTfFonction());
		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderTextField(myView.getTfQuotite());
		CocktailUtilities.viderTextField(myView.getTfQuotiteFinanciere());

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub

		clearDatas();

		if (getCurrentOccupation() != null) {
			
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentOccupation().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentOccupation().dateFin());
			CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentOccupation().quotite());
			CocktailUtilities.setTextToField(myView.getTfFonction(), getCurrentOccupation().observations());
			CocktailUtilities.setTextToArea(myView.getTaMotifFin(), getCurrentOccupation().motifFin());
			CocktailUtilities.setNumberToField(myView.getTfQuotiteFinanciere(), EOQuotite.quotiteFinanciere(getEdc(), getCurrentOccupation().quotite()));

			setCurrentEmploi(getCurrentOccupation().toEmploi());
			
			updateListeCarrieresContrats();

			int index = 0;

			for (InfoCarriereContrat mySituation : (NSArray<InfoCarriereContrat>)eodCarriereContrat.displayedObjects()) {

				if (getCurrentOccupation().toCarriere() != null) {
					if (mySituation.getCurrentCarriere() == getCurrentOccupation().toCarriere() ) {
						eodCarriereContrat.setSelectionIndexes(new NSArray(index));
						myView.getMyEOTable().updateData();
						break;
					}
				}
				else {
					if (getCurrentOccupation().toContrat() != null) {
						if (mySituation.getCurrentContrat() == getCurrentOccupation().toContrat() ) {
							eodCarriereContrat.setSelectionIndexes(new NSArray(index));
							myView.getMyEOTable().updateData();
							break;
						}
					}
				}
				index ++;
			}
		}

		updateInterface();

	}
	
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerContratsCarriere implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentSituation((InfoCarriereContrat)eodCarriereContrat.selectedObject());
		}
	}


	/**
	 * 
	 */
	private void updateListeCarrieresContrats() {

		eodCarriereContrat.setObjectArray(InfoCarriereContrat.carrieresContratsPourPeriode(getEdc(), getCurrentOccupation().individu(), getDateDebut(), getDateFin(), true));
		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();

	}	
	
	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnGetEmploi().setEnabled(getDateDebut() != null);

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentOccupation(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsApresValidation() {
			
		// Alerte par rapport à la quotite de modalité de service
		NSArray<EOModalitesService> modalites = EOModalitesService.rechercherPourIndividuEtPeriode(getEdc(), getCurrentOccupation().individu(), getDateDebut(), getDateFin());
		if (modalites.size() > 0) {
			for (EOModalitesService modalite : modalites) {
				if (modalite.quotite().compareTo(getCurrentOccupation().quotite()) != 0) {
					EODialogs.runInformationDialog("ATTENTION", "La quotité de modalité de service (" + modalite.quotite()+ "%) devrait être égale à la quotité d'occupation pour cette période (" + getCurrentOccupation().quotite() + "%)");
					break;
				}
			}
		}

		// Association de la ligne budgétaire de l'emploi  a l'agent si elle existe
		NSArray<EOPersBudget> budgetsEmploi = EOPersBudget.findForEmploi(getEdc(), getCurrentEmploi()) ;
		if (budgetsEmploi.size() > 0) {
			NSArray<EOPersBudget> budgetsAgent = EOPersBudget.findForIndividu(getEdc(), getCurrentOccupation().individu()) ;
			if (budgetsAgent.size() == 0) {
				EOPersBudget.creerPourEmploi(getEdc(), getCurrentOccupation().individu(), budgetsEmploi, 
						DateCtrl.debutAnnee(DateCtrl.getCurrentYear()), getCurrentOccupation().dateFin());
			}
		}
		
		// Rafraichissement de l'onglet BUDGET
		NSNotificationCenter.defaultCenter().postNotification("NotifRafraichirBudgetAgent", null, null);

		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		updateListeCarrieresContrats();

	}

}

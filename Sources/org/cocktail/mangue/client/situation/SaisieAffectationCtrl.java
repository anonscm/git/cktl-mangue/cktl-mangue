// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.situation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.situation.SaisieAffectationView;
import org.cocktail.mangue.client.select.LocalSelectCtrl;
import org.cocktail.mangue.client.select.LocalSelectView;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.client.select.StructureSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps;
import org.cocktail.mangue.modele.InfoCarriereContrat;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartStructure;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EOSituationGeographique;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class SaisieAffectationCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;

	private SaisieAffectationView myView;
	private NSArray<EOAffectation> affectationsExistantes;

	private ListenerContratsCarriere listenerSituation = new ListenerContratsCarriere();

	private EODisplayGroup		eodCarriereContrat;
	private EOAffectation 		currentAffectation;
	private EOStructure 		currentService;
	private EOStructure 		currentSituationGeo;
	private EOAgentPersonnel	currentAgentPersonnel;

	private InfoCarriereContrat	 currentSituation;

	/**
	 * Constructeur
	 * @param globalEc : editingContext
	 */
	public SaisieAffectationCtrl(EOEditingContext edc) {

		super(edc);

		eodCarriereContrat = new EODisplayGroup();
		myView = new SaisieAffectationView(new JFrame(), true, eodCarriereContrat);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnGetStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectService();}}
				);
		myView.getBtnGetSituationGeo().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectSituationGeographique();}}
				);
		myView.getBtnDelSituationGeo().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delSituationGeographique();}}
				);

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		setCurrentAgentPersonnel(((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());

		myView.getMyEOTable().addListener(listenerSituation);

		CocktailUtilities.initTextField(myView.getTfStructure(), false, false);
		CocktailUtilities.initTextField(myView.getTfSituationGeographique(), false, false);
		CocktailUtilities.initTextField(myView.getTfStructureDetail(), false, false);

	}

	/**
	 * 
	 * @return
	 */
	private EOAffectation getCurrentAffectation() {
		return currentAffectation;
	}

	/**
	 * 
	 * @param affectation
	 */
	public void setCurrentAffectation(EOAffectation affectation) {
		this.currentAffectation = affectation;
		if (currentAffectation != null)
			myView.setTitle("AFFECTATIONS ( " + getCurrentAffectation().individu().identitePrenomFirst() + ")");
		else
			myView.setTitle("AFFECTATIONS");
		updateDatas();
	}

	public InfoCarriereContrat getCurrentSituation() {
		return currentSituation;
	}

	public void setCurrentSituation(InfoCarriereContrat currentSituation) {
		this.currentSituation = currentSituation;
	}

	public EOAgentPersonnel getCurrentAgentPersonnel() {
		return currentAgentPersonnel;
	}

	public void setCurrentAgentPersonnel(EOAgentPersonnel currentAgentPersonnel) {
		this.currentAgentPersonnel = currentAgentPersonnel;
	}

	private EOStructure getCurrentService() {
		return currentService;
	}
	public void setCurrentService(EOStructure currentService) {

		this.currentService = currentService;

		CocktailUtilities.viderTextField(myView.getTfStructure());
		CocktailUtilities.viderTextField(myView.getTfStructureDetail());

		if (currentService != null) {
			CocktailUtilities.setTextToField(myView.getTfStructure(), currentService.llStructure());
			CocktailUtilities.setTextToField(myView.getTfStructureDetail(), currentService.libelleComplet());

			if (EOGrhumParametres.isGestionLocal() && isModeCreation()) {
				EORepartAssociation localPrincipal = EORepartAssociation.findLocalPrincipal(getEdc(), currentService);
				if (localPrincipal != null) {
					setCurrentSituationGeo(localPrincipal.structureAssociee());
				}
			}
		}
		updateInterface();
	}

	public EOStructure getCurrentSituationGeo() {
		return currentSituationGeo;
	}

	public void setCurrentSituationGeo(EOStructure currentSituationGeo) {
		this.currentSituationGeo = currentSituationGeo;
		CocktailUtilities.viderTextField(myView.getTfSituationGeographique());
		if (currentSituationGeo != null) {
			CocktailUtilities.setTextToField(myView.getTfSituationGeographique(), currentSituationGeo.llStructure());
		}		
	}

	/**
	 * 
	 * @param affectations
	 */
	public void setAffectationsExistantes(NSArray<EOAffectation> affectations) {
		affectationsExistantes = affectations;
	}

	/**
	 * 
	 */
	private void selectService() {

		EOStructure service = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(getEdc(), getDateDebut());
		if (service != null) {
			setCurrentService(service);
		}
		
	}
	
	/**
	 * 
	 */
	private void selectSituationGeographique() {

		EOStructure structure = null;
		if (EOGrhumParametres.isGestionLocal()) {
			structure = LocalSelectCtrl.sharedInstance(getEdc()).getSelection(getCurrentService());
		}
		else {
			structure = StructureSelectCtrl.sharedInstance(getEdc()).getStructureService();		
		}
		if (structure != null) {
			setCurrentSituationGeo(structure);
		}
	}
	private void delSituationGeographique() {
		setCurrentSituationGeo(null);
	}

	/**
	 * 
	 * @param individu
	 * @return
	 */
	public EOAffectation ajouter(EOIndividu individu)	{

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		setModeCreation(true);
		setCurrentAffectation(EOAffectation.creer(getEdc(), individu));
				
		myView.getTfDateDebut().selectAll();
		myView.setVisible(true);
		return getCurrentAffectation();

	}

	/**
	 * 
	 * @param contrat
	 * @return
	 */
	public EOAffectation ajouterPourContrat(EOContrat contrat) {

		clearDatas();

		setModeCreation(true);
		setCurrentAffectation(EOAffectation.creer(getEdc(), contrat.individu()));
		getCurrentAffectation().setDateDebut(contrat.dateDebut());
		getCurrentAffectation().setDateFin(contrat.dateFin());

		myView.getTfQuotite().setText("100");

		updateDatas();
		myView.setVisible(true);
		return getCurrentAffectation();

	}

	/**
	 * 
	 * @param affectation
	 * @return
	 */
	public boolean modifier(EOAffectation affectation, boolean modeCreation) {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		setModeCreation(modeCreation);

		setCurrentAffectation(affectation);

		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentAffectation() != null;
	}

	
	/**
	 * 
	 * @param affectation
	 * @return
	 */
	public boolean renouveler(EOAffectation affectation) {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);

		setModeCreation(true);
		setCurrentAffectation(affectation);
		
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentAffectation() != null;
		
	}

	/**
	 * 
	 * @return
	 */
	private NSTimestamp getDateDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDateDebut());
	}
	private NSTimestamp getDateFin() {
		return CocktailUtilities.getDateFromField(myView.getTfDateFin());
	}

	/**
	 * 
	 */
	protected void traitementsAvantValidation() {

		supprimerRepartStructures(affectationsExistantes);

		if (getDateDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "AFFECTATION"));
		}
		if (getCurrentService() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_STRUCTURE_NON_RENSEIGNE, "AFFECTATION"));
		}

		getCurrentAffectation().setDateDebut(getDateDebut());
		getCurrentAffectation().setDateFin(getDateFin());

		getCurrentAffectation().setToStructureUlrRelationship(getCurrentService());

		getCurrentAffectation().setQuotite(CocktailUtilities.getIntegerFromField(myView.getTfQuotite()));
		getCurrentAffectation().setToStructureUlrRelationship(getCurrentService());
		getCurrentAffectation().setEstPrincipale(myView.getCheckPrincipale().isSelected());

		getCurrentAffectation().setPersIdModification(getCurrentAgentPersonnel().toIndividu().persId());
		if (getCurrentAffectation().persIdCreation() == null)
			getCurrentAffectation().setPersIdCreation(getCurrentAgentPersonnel().toIndividu().persId());

		if (getCurrentSituation() != null) {
			getCurrentAffectation().setToContratRelationship(getCurrentSituation().getCurrentContrat());
			getCurrentAffectation().setToCarriereRelationship(getCurrentSituation().getCurrentCarriere());
		}
		else {
			getCurrentAffectation().setToContratRelationship(null);
			getCurrentAffectation().setToCarriereRelationship(null);			
		}

		if (EOGrhumParametres.findParametre(getEdc(), ManGUEConstantes.PARAM_KEY_AFFECTATION_CARRIERE).isParametreVrai()) {
			if (getCurrentSituation() == null ) {
				throw new NSValidation.ValidationException("L'association d'un contrat ou d'une carrière est obligatoire !");			
			}
		}

		// L'agent a t il un contrat ou une carriere sur la periode ?
		if (eodCarriereContrat.displayedObjects().size() == 0) {
			NSArray<EOVacataires> vacations = EOVacataires.findForIndividuAndPeriode(getEdc(), getCurrentAffectation().individu(), getDateDebut(), getDateFin());
			if (vacations.size() == 0) {
				throw new NSValidation.ValidationException("Cet agent n'a ni contrat, ni carrière, ni vacation couvrant toute la période définie !");			
			}
		}

		// Controle des chevauchements de periode avec quotite > 100 ou meme structure 
		NSMutableArray<EOAffectation> affectations = new NSMutableArray<EOAffectation>(affectationsExistantes);
		if (affectations.containsObject(getCurrentAffectation()) == false)
			affectations.addObject(getCurrentAffectation());
		affectations = new NSMutableArray<EOAffectation>(EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT));

		Integer quotiteTotale = getCurrentAffectation().quotite();
		for (EOAffectation myAffectation : affectations) {
			if (myAffectation != getCurrentAffectation()) {

				if (DateCtrl.chevauchementPeriode(myAffectation.dateDebut(), myAffectation.dateFin(), getDateDebut() , getDateFin()) ) {
					// Gestion de l'unicite des affectations principales
					if (getCurrentAffectation().estPrincipale())
						myAffectation.setEstPrincipale(false);

					quotiteTotale = new Integer(quotiteTotale.intValue() + myAffectation.quotite().intValue());
					if ( quotiteTotale.doubleValue() > 100.0) {
						throw new NSValidation.ValidationException("La quotité totale d'affectation ne peut dépasser 100% pour cette période !");
					}
					if ( myAffectation.toStructureUlr() == getCurrentService() ) {
						throw new NSValidation.ValidationException("L'agent ne peut être affecté à la même structure pendant la même période !");
					}
				}
			}
		}

		gererAssociationsHeberges();

	}

	/** methode a surcharger pour faire des traitements apres l'enregistrement des donnees dans la base */
	protected  void traitementsApresValidation() {
		try {
			
			if (isModeCreation() && getCurrentSituationGeo() != null) {
				EOSituationGeographique newSituation = EOSituationGeographique.creer(getEdc(), getCurrentAffectation());
				newSituation.setStructureRelationship(getCurrentSituationGeo());
				newSituation.setToAffectationRelationship(getCurrentAffectation());
			}

			ajouterRepartStructures();
			getEdc().saveChanges();

			//Ajoute le role enseignant si c'est le cas			
			gererAssociationEnseignant();

			myView.setVisible(false);
			((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return
	 */
	private NSArray<EOContratHeberges> contratsHebergesPourAffectation() {
		// Vérifier si il existe un ou plusieurs contrats d'hébergé avec les mêmes dates que l'affectation
		NSMutableArray<EOContratHeberges> contratsHeberges = new NSMutableArray<EOContratHeberges>();

		NSArray carrieres = InfoCarriereContrat.carrieresContratsPourPeriode(getEdc(), getCurrentAffectation().individu(), getDateDebut(), getDateFin(), true);		
		for (java.util.Enumeration<InfoCarriereContrat> e = carrieres.objectEnumerator();e.hasMoreElements();) {
			InfoCarriereContrat info = e.nextElement();
			if (info.estHeberge()) {
				EOContratHeberges contrat = (EOContratHeberges)SuperFinder.objetForGlobalIDDansEditingContext(info.globalIDObjet(), getEdc());
				// Si il y a une intersection commune, on ajoute ce contrat
				if (Utilitaires.IntervalleTemps.intersectionPeriodes(getCurrentAffectation().dateDebut(), getCurrentAffectation().dateFin(), contrat.dateDebut(), contrat.dateFin()) != null)
					contratsHeberges.addObject(contrat);
			}
		}

		return contratsHeberges.immutableClone();
	}


	/**
	 *
	 */
	public void supprimerRepartStructures(NSArray<EOAffectation> affectations) {

		try {
			if (EOGrhumParametres.isAffectationStructure() && affectations != null && affectations.size() > 0) {
				for (EOAffectation myAffectation : affectations) {
					if (DateCtrl.isBetween(DateCtrl.today(), myAffectation.dateDebut(), myAffectation.dateFin()) ) {
						EORepartStructure.supprimerRepartPourIndividuEtStructure(getEdc(), getCurrentAffectation().individu(), myAffectation.toStructureUlr());
					}
				}
			}
		}
		catch (Exception e) {
		}
	}


	/**
	 * 
	 */
	private void ajouterRepartStructures() {

		if (EOGrhumParametres.isAffectationStructure()) {
			NSArray<EOAffectation> affectationsActuelles = EOAffectation.rechercherAffectationsADate(getEdc(), getCurrentAffectation().individu(), DateCtrl.today());

			for (EOAffectation affectation : affectationsActuelles) {
				EORepartStructure repart = EORepartStructure.rechercherPourPersIdEtStructure(getEdc(),affectation.individu().persId(), affectation.toStructureUlr());
				if (repart == null) {
					EORepartStructure.creer(getEdc(), affectation.individu().persId(), affectation.toStructureUlr());
				}
			}
		}
	}


	/**
	 * 
	 */
	private void gererAssociationsHeberges()  {

		// Recuperation des contrats d'heberges pour la periode

		NSArray<EOContratHeberges> contratsHeberges = contratsHebergesPourAffectation();

		// Crée les repartAssociation pour ce repartStructure et pour une date qui est l'intersection des contrats hébergés et de l'affectation

		for (EOContratHeberges contrat : contratsHeberges) {

			IntervalleTemps intervalle = Utilitaires.IntervalleTemps.intersectionPeriodes(getCurrentAffectation().dateDebut(), 
					getCurrentAffectation().dateFin(), contrat.dateDebut(), contrat.dateFin());
			EORepartStructure repartStructure = EORepartStructure.rechercherPourPersIdEtStructure(getEdc(), getCurrentAffectation().individu().persId(), getCurrentAffectation().toStructureUlr());
			if (repartStructure == null) {

				EORepartAssociation repartExistant = EORepartAssociation.rechercherPourIndividuStructureAssociationEtPeriode(
						getEdc(), getCurrentAffectation().individu(), getCurrentAffectation().toStructureUlr(), 
						EOAssociation.LIBELLE_ASSOCIATION_HEBERGE, 
						intervalle.debutPeriode(), intervalle.debutPeriode());

				if (repartExistant == null) {

					EORepartAssociation repart = EORepartAssociation.creerAssociationHeberge(getEdc(), getCurrentAffectation().toStructureUlr(),
							getCurrentAffectation().individu(), getCurrentAgentPersonnel());
					repart.initAvecIndividuStructureEtAgent(getCurrentAffectation().individu(), getCurrentAffectation().toStructureUlr(), getCurrentAgentPersonnel().toIndividu().persId());
					getEdc().insertObject(repart);
					repart.setDateDebut(intervalle.debutPeriode());
					repart.setDateFin(intervalle.finPeriode());

				}
			}
		}
	}


	/**
	 * Creation d une repart_association de type "AFFECTATION".
	 */
	private void gererAssocationAffectation() {

		EORepartAssociation repartAssociation = EORepartAssociation.rechercherPourIndividuStructureAssociationEtPeriode(
				getEdc(), 
				getCurrentAffectation().individu(),
				getCurrentAffectation().toStructureUlr(), 
				EOAssociation.LIBELLE_ASSOCIATION_AFFECTATION, 
				getCurrentAffectation().dateDebut(), getCurrentAffectation().dateFin());

		if (repartAssociation == null)
			repartAssociation = EORepartAssociation.creerAssociationAffectation(getEdc(), getCurrentService(), currentAffectation.individu(), getCurrentAgentPersonnel());

		repartAssociation.setStructureRelationship(getCurrentAffectation().toStructureUlr());
		repartAssociation.setRasQuotite(new BigDecimal(getCurrentAffectation().quotite()));
		repartAssociation.setDateDebut(getCurrentAffectation().dateDebut());
		repartAssociation.setDateFin(getCurrentAffectation().dateFin());

	}

	/**
	 * 
	 */
	private void setQuotiteAffectation() {

		if (getDateDebut() != null) {

			NSMutableArray affectations = new NSMutableArray(EOAffectation.rechercherAffectationsADate(getEdc(), getCurrentAffectation().individu(), getDateDebut()));
			affectations.removeObject(getCurrentAffectation());

			Integer quotiteTotale = CocktailUtilities.computeSumIntegerForKey(affectations, EOAffectation.QUOTITE_KEY);
			myView.getTfQuotite().setText(String.valueOf( 100 - quotiteTotale.intValue()));

		}

	}

	/**
	 * Creation d'une repart Association de type "Enseignant"
	 */
	private void gererAssociationEnseignant() {
		boolean isEnseignant = false;
		NSArray<EOCarriere> listeCarrieres = EOCarriere.rechercherCarrieresSurPeriode(getEdc(), getCurrentAffectation().individu(), getDateDebut(), getDateFin());

		//Si plusieurs carrière possible pr meme période  => forcer la sortie de la boucle
		for (EOCarriere uneCarriere : listeCarrieres) {
			if (uneCarriere.toTypePopulation().estEnseignant()) {
				isEnseignant = true;
				break;
			}
		}

		//Si pas de carrière, on regarde les contrats
		if (listeCarrieres.size() == 0) {
			NSArray<EOContrat> listeContrats = EOContrat.rechercherTousContratsPourIndividuEtPeriode(getEdc(), getCurrentAffectation().individu(), getDateDebut(), getDateFin());

			for (EOContrat unContrat : listeContrats) {
				if (unContrat.temEnseignant().equals(CocktailConstantes.VRAI)) {
					isEnseignant = true;
					break;
				}
			}
		}

		//Si l'affectation concerne un enseignant, on crée une répartition ou la met à jour
		if (isEnseignant) {
			try {
				EORepartAssociation repartAssociation = EORepartAssociation.rechercherPourIndividuStructureAssociationEtPeriode(
						getEdc(), 
						getCurrentAffectation().individu(), 
						getCurrentAffectation().toStructureUlr(), 
						EOAssociation.LIBELLE_ASSOCIATION_ENSEIGNANT, 
						getCurrentAffectation().dateDebut(), getCurrentAffectation().dateFin());

				if (repartAssociation == null) {
					repartAssociation = EORepartAssociation.creerAssociationEnseignant(getEdc(), getCurrentService(), getCurrentAffectation().individu(), ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());
					getEdc().insertObject(repartAssociation);
				}

				repartAssociation.setStructureRelationship(getCurrentAffectation().toStructureUlr());
				repartAssociation.setRasQuotite(new BigDecimal(getCurrentAffectation().quotite()));
				repartAssociation.setDateDebut(getCurrentAffectation().dateDebut());
				repartAssociation.setDateFin(getCurrentAffectation().dateFin());

				getEdc().saveChanges();

			} catch (Exception e) {
			}
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub

		CocktailUtilities.viderTextField(myView.getTfDateDebut());
		CocktailUtilities.viderTextField(myView.getTfDateFin());
		CocktailUtilities.viderLabel(myView.getLblMessage());

		setCurrentService(null);
		setCurrentSituationGeo(null);
		eodCarriereContrat.setObjectArray(new NSArray<InfoCarriereContrat>());
		myView.getMyEOTable().updateData();

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentAffectation() != null) {

			setCurrentService(getCurrentAffectation().toStructureUlr());
			CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentAffectation().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentAffectation().dateFin());
			CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentAffectation().quotite());
			myView.getCheckPrincipale().setSelected(getCurrentAffectation().estPrincipale());

			int index = 0;
			updateListeCarrieresContrats();

			for (InfoCarriereContrat mySituation : (NSArray<InfoCarriereContrat>)eodCarriereContrat.displayedObjects()) {

				if (getCurrentAffectation().toCarriere() != null) {
					if (mySituation.getCurrentCarriere() == getCurrentAffectation().toCarriere() ) {
						eodCarriereContrat.setSelectionIndexes(new NSArray(index));
						myView.getMyEOTable().updateData();
						break;
					}
				}
				else {
					if (getCurrentAffectation().toContrat() != null) {
						if (mySituation.getCurrentContrat() == getCurrentAffectation().toContrat() ) {
							eodCarriereContrat.setSelectionIndexes(new NSArray(index));
							myView.getMyEOTable().updateData();
							break;
						}
					}
				}
				index ++;
			}
		}

		updateInterface();

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerContratsCarriere implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentSituation((InfoCarriereContrat)eodCarriereContrat.selectedObject());
		}
	}

	/**
	 * 
	 */
	private void updateListeCarrieresContrats() {

		eodCarriereContrat.setObjectArray(InfoCarriereContrat.carrieresContratsPourPeriode(getEdc(), getCurrentAffectation().individu(), getDateDebut(), getDateFin(), true));
		myView.getMyEOTable().updateData();
		myView.getMyTableModel().fireTableDataChanged();

	}

	private boolean peutAjouterSituationGeo() {
		
		if (isModeCreation()) {
			if (EOGrhumParametres.isGestionLocal()) {
				return getCurrentService()!= null;
			}
			else
				return true;
		}
		
		
		return false;
	}
	
	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnGetSituationGeo().setEnabled(peutAjouterSituationGeo());
		myView.getBtnDelSituationGeo().setEnabled(isModeCreation() && getCurrentSituationGeo() != null);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		getEdc().revert();
		ServerProxy.clientSideRequestRevert(getEdc());
		setCurrentAffectation(null);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
		myView.setVisible(false);

	}

	/**
	 * 
	 */
	private void updateMessageAffectationPrincipale() {

		NSArray<EOAffectation> affectations = EOAffectation.rechercherAffectationsADate(getEdc(), getCurrentAffectation().individu(), getDateDebut());
		if (affectations != null && affectations.size() > 0) {
			for (EOAffectation affectation : affectations) {
				if (affectation != getCurrentAffectation() 
						&& affectation.estPrincipale() && myView.getCheckPrincipale().isSelected()) {
					CocktailUtilities.setTextToLabel(myView.getLblMessage(), "Attention. Il y a déjà une affectation principale sur cette période !");
				}
			}
		}

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		if (isModeCreation())
			setQuotiteAffectation();

		updateMessageAffectationPrincipale();

		updateListeCarrieresContrats();
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}
}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.situation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Enumeration;

import javax.swing.JFrame;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.situation.SaisieSituationGeographiqueView;
import org.cocktail.mangue.client.select.LocalSelectCtrl;
import org.cocktail.mangue.client.select.StructureSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOSituationGeographique;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class SaisieSituationGeographiqueCtrl extends ModelePageSaisie
{
	private static final long serialVersionUID = 0x7be9cfa4L;

	private SaisieSituationGeographiqueView myView;

	private EOSituationGeographique currentSituation;
	private EOStructure currentStructure;
	private AffectationOccupationCtrl ctrlParent;
	private EOAdresse currentAdresse;

	public SaisieSituationGeographiqueCtrl(AffectationOccupationCtrl ctrlParent, EOEditingContext edc) {

		super(edc);
		this.ctrlParent = ctrlParent;

		myView = new SaisieSituationGeographiqueView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnGetStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {	selectStructure();}	}
				);

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		CocktailUtilities.initTextField(myView.getTfStructure(), false, false);
		CocktailUtilities.initTextField(myView.getTfAdresseLibelle(), false, false);

	}

	private EOSituationGeographique getCurrentSituation() {
		return currentSituation;
	}
	public void setCurrentSituation(EOSituationGeographique situation) {
		currentSituation = situation;
		updateDatas();
	}
	public EOStructure getCurrentStructure() {
		return currentStructure;
	}
	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
		myView.getTfStructure().setText("");
		setCurrentAdresse(null);
		if (currentStructure != null) {
			CocktailUtilities.setTextToField(myView.getTfStructure(), currentStructure.llStructure());
			// Adresse
			if (getCurrentStructure() != null) {
				EOAdresse adrStructure = EORepartPersonneAdresse.rechercherAdresse(getEdc(), getCurrentStructure(), EOTypeAdresse.TYPE_PROFESSIONNELLE);
				setCurrentAdresse(adrStructure);
			}
		}
	}
	public EOAdresse getCurrentAdresse() {
		return currentAdresse;
	}
	public void setCurrentAdresse(EOAdresse currentAdresse) {
		this.currentAdresse = currentAdresse;
		myView.getTfAdresseLibelle().setText("");
		if (currentAdresse != null) {
			CocktailUtilities.setTextToField(myView.getTfAdresseLibelle(), currentAdresse.adresseComplete());
		}
	}

	public void associerEmployeur() {
		getCurrentSituation().setStructureRelationship(null);
	}
	public void supprimerEmployeur() {
		getCurrentSituation().setStructureRelationship(null);
	}

	/**
	 * 
	 * @param situation
	 * @return
	 */
	public boolean modifier(EOSituationGeographique situation, EOAffectation affectation, boolean modeCreation) {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		setModeCreation(modeCreation);		
		
		if (isModeCreation()) {
			situation.setDateDebut(affectation.dateDebut());
			situation.setDateFin(affectation.dateFin());
		}
		
		situation.setToAffectationRelationship(affectation);
		setCurrentSituation(situation);
		myView.setTitle("SITUATION GEOGRAPHIQUE ( " + getCurrentSituation().individu().identitePrenomFirst() + ")");
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentSituation() != null;
	}

	private NSTimestamp getDateDebut() {
		return DateCtrl.stringToDate(myView.getTfDateDebut().getText());
	}
	private NSTimestamp getDateFin() {
		return DateCtrl.stringToDate(myView.getTfDateFin().getText());
	}
	private void selectStructure() {
		EOStructure structure = null;
		if (EOGrhumParametres.isGestionLocal()) {
			structure = LocalSelectCtrl.sharedInstance(getEdc()).getSelection(getCurrentSituation().toAffectation().toStructureUlr());
		}
		else {
			structure = StructureSelectCtrl.sharedInstance(getEdc()).getStructureService();		
		}
		if (structure != null) {
			setCurrentStructure(structure);
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getTaCommentaires().setText("");
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");

		setCurrentStructure( null);

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		setCurrentStructure(getCurrentSituation().structure());
		CocktailUtilities.setDateToField(myView.getTfDateDebut(), getCurrentSituation().dateDebut());
		CocktailUtilities.setDateToField(myView.getTfDateFin(), getCurrentSituation().dateFin());
		CocktailUtilities.setNumberToField(myView.getTfQuotite(), getCurrentSituation().quotite());
		CocktailUtilities.setTextToArea(myView.getTaCommentaires(), getCurrentSituation().situCommentaire());

		if (getCurrentSituation().adresseStructure() != null)
			setCurrentAdresse(getCurrentSituation().adresseStructure());

		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsAvantValidation() {

		getCurrentSituation().setToAffectationRelationship(ctrlParent.getCurrentAffectation());
		getCurrentSituation().setDateDebut(getDateDebut());
		getCurrentSituation().setDateFin(getDateFin());
		getCurrentSituation().setQuotite(CocktailUtilities.getIntegerFromField(myView.getTfQuotite()));
		getCurrentSituation().setStructureRelationship(getCurrentStructure());

		if (getDateDebut() != null && getCurrentStructure() != null) {

			// Controle des chevauchements de periode avec quotite > 100 ou meme structure 
			NSMutableArray<EOSituationGeographique> situations = new NSMutableArray(ctrlParent.getSituations());
			if (situations.containsObject(getCurrentSituation()) == false)
				situations.addObject(getCurrentSituation());
			situations = new NSMutableArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(situations, EOSituationGeographique.SORT_ARRAY_DEBUT_ASC));

			BigDecimal quotiteTotale = new BigDecimal(getCurrentSituation().quotite());
			for (Enumeration<EOSituationGeographique> e = situations.objectEnumerator();e.hasMoreElements();) {

				EOSituationGeographique sit = e.nextElement();
				if (sit != getCurrentSituation()) {

					if (DateCtrl.chevauchementPeriode(sit.dateDebut(), sit.dateFin(), getDateDebut() , getDateFin()) ) {

						quotiteTotale = quotiteTotale.add(new BigDecimal(sit.quotite()));

						if ( sit.structure() == getCurrentStructure() ) {
							throw new NSValidation.ValidationException("L'agent ne peut être situé dans la même structure pendant la même période !");
						}
					}
				}
			}

			if ( quotiteTotale.doubleValue() > 100.0) {
				throw new NSValidation.ValidationException("La quotité totale des situations ne peut dépasser 100% pour une même période !");
			}

		}
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);

	}

}

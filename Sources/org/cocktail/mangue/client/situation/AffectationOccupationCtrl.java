package org.cocktail.mangue.client.situation;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gpeec.EmploisCtrl;
import org.cocktail.mangue.client.gui.situation.AffectationOccupationView;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.EOSituationGeographique;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AffectationOccupationCtrl {

	private static AffectationOccupationCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.TRUE;
	private EOEditingContext edc;
	private AffectationOccupationView myView;

	private ListenerAffectation listenerAffectation = new ListenerAffectation();
	private ListenerOccupation listenerOccupation = new ListenerOccupation();
	private ListenerSituation listenerSituation = new ListenerSituation();

	private SaisieAffectationCtrl ctrlSaisieAff;
	private SaisieOccupationCtrl ctrlSaisieOcc;
	private SaisieSituationGeographiqueCtrl ctrlSaisieSit;
	private RendererSituationGeo rendererSituation = new RendererSituationGeo();

	private boolean peutGererModule;

	private EODisplayGroup eodAffectation, eodOccupation, eodSituationGeo;

	private EOAffectation	currentAffectation;
	private EOOccupation 	currentOccupation;
	private EOSituationGeographique 	currentSituationGeo;
	private EOContrat 		currentContrat;
	private EOCarriere 		currentCarriere;

	private EOIndividu currentIndividu;

	public AffectationOccupationCtrl(EOEditingContext edc) {

		this.edc = edc;

		ctrlSaisieAff = new SaisieAffectationCtrl(edc);
		ctrlSaisieOcc = new SaisieOccupationCtrl(this, edc);
		ctrlSaisieSit = new SaisieSituationGeographiqueCtrl(this, edc);

		eodAffectation = new EODisplayGroup();
		eodOccupation = new EODisplayGroup();
		eodSituationGeo = new EODisplayGroup();

		myView = new AffectationOccupationView(null, MODE_MODAL.booleanValue(), eodAffectation, eodOccupation, eodSituationGeo, rendererSituation);

		myView.getMyEOTableAffectation().addListener(listenerAffectation);
		myView.getMyEOTableOccupation().addListener(listenerOccupation);
		myView.getMyEOTableSituationGeo().addListener(listenerSituation);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterAffectation();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierAffectation();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerAffectation();}}
				);
		myView.getBtnRenouveler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {renouvelerAffectation();}}
				);
		myView.getBtnClose().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {fermer();}}
				);


		myView.getBtnAjouterOccupation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterOccupation();}}
				);
		myView.getBtnModifierOccupation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierOccupation();}}
				);
		myView.getBtnSupprimerOccupation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerOccupation();}}
				);
		myView.getBtnRenouvelerOccupation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {renouvelerOccupation();}}
				);
		myView.getBtnAfficherEmplois().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherEmplois();}}
				);	
		myView.getBtnAjouterSituation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterSituationGeo();}}
				);
		myView.getBtnModifierSituation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierSituationGeo();}}
				);
		myView.getBtnSupprimerSituation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerSituationGeo();}}
				);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));
		}

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		CocktailUtilities.initTextField(myView.getTfStructurePere(), false, false);
		CocktailUtilities.initTextField(myView.getTfDetailOccupation(), false, false);
		CocktailUtilities.initTextField(myView.getTfAdresseSituation(), false, false);

		updateInterface();

	}

	public static AffectationOccupationCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new AffectationOccupationCtrl(editingContext);
		return sharedInstance;
	}

	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}
	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {

		setPeutGererModule(currentUtilisateur.peutGererOccupation());

		myView.getBtnRenouveler().setVisible(peutGererModule());
		myView.getBtnRenouvelerOccupation().setVisible(peutGererModule());
		myView.getBtnAjouter().setVisible(peutGererModule());
		myView.getBtnAjouterOccupation().setVisible(peutGererModule());
		myView.getBtnAjouterSituation().setVisible(peutGererModule());
		myView.getBtnModifier().setVisible(peutGererModule());
		myView.getBtnModifierOccupation().setVisible(peutGererModule());
		myView.getBtnModifierSituation().setVisible(peutGererModule());
		myView.getBtnSupprimer().setVisible(peutGererModule());
		myView.getBtnSupprimerOccupation().setVisible(peutGererModule());
		myView.getBtnSupprimerSituation().setVisible(peutGererModule());

	}


	public EOAffectation getCurrentAffectation() {
		return currentAffectation;
	}
	public void setCurrentAffectation(EOAffectation affectation) {

		currentAffectation = affectation;
		CocktailUtilities.setTextToField(myView.getTfStructurePere(), "");
		if (currentAffectation != null) {
			CocktailUtilities.setTextToField(myView.getTfStructurePere(), currentAffectation.toStructureUlr().libelleComplet());
		}
		actualiserSituationsGeographiques();
		updateInterface();
	}
	public EOOccupation getCurrentOccupation() {
		return currentOccupation;
	}
	public void setCurrentOccupation(EOOccupation currentOccupation) {
		this.currentOccupation = currentOccupation;
		CocktailUtilities.setTextToField(myView.getTfDetailOccupation(), "");

		if (currentOccupation != null) {
			if (currentOccupation.toCarriere() != null) {				
				CocktailUtilities.setTextToField(myView.getTfDetailOccupation(), "CARRIERE du " + DateCtrl.dateToString(currentOccupation.toCarriere().dateDebut()) + " au " + DateCtrl.dateToString(currentOccupation.toCarriere().dateFin()));
			}
			else {
				if (currentOccupation.toContrat() != null) {
					CocktailUtilities.setTextToField(myView.getTfDetailOccupation(), "CONTRAT du " + currentOccupation.toContrat().dateDebutFormatee() + " au " + currentOccupation.toContrat().dateFinFormatee());
				}				
			}
		}

		updateInterface();
	}
	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}
	public EOSituationGeographique getCurrentSituationGeo() {
		return currentSituationGeo;
	}
	public void setCurrentSituationGeo(EOSituationGeographique currentSituationGeo) {
		this.currentSituationGeo = currentSituationGeo;
		CocktailUtilities.setTextToField(myView.getTfAdresseSituation(), "");
		if (currentSituationGeo != null) {

			EOAdresse adrStructure = EORepartPersonneAdresse.rechercherAdresse(edc, currentSituationGeo.structure(), EOTypeAdresse.TYPE_PROFESSIONNELLE);
			if (adrStructure != null) {
				CocktailUtilities.setTextToField(myView.getTfAdresseSituation(), adrStructure.adresseComplete());				
			}
		}
		updateInterface();
	}
	public void setCurrentContrat(EOContrat currentContrat) {
		this.currentContrat = currentContrat;
	}
	public void setCurrentCarriere(EOCarriere currentCarriere) {
		this.currentCarriere = currentCarriere;	
	}

	/**
	 * 
	 * @param individu
	 */
	public void open(EOIndividu individu)	{
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		setCurrentIndividu(individu);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);
	}

	/**
	 * Affiche la fenêtre des emplois en sélectionnant par défaut l'emploi concerné par l'occupation
	 */
	public void afficherEmplois() {
		EmploisCtrl.sharedInstance(edc).open();
		EmploisCtrl.sharedInstance(edc).getMyView().getMyEOTable().forceNewSelectionOfObjects(new NSArray<IEmploi>(((EOOccupation)
				eodOccupation.selectedObject()).toEmploi()));
	}

	public void employeHasChanged(NSNotification  notification) {
		if (notification != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
		}
	}

	/**
	 * 
	 */
	public void actualiser() {
		actualiserAffectations();
		actualiserOccupations();
		actualiserSituationsGeographiques();
		updateInterface();
	}

	/**
	 * 
	 */
	private void actualiserSituationsGeographiques() {

		NSMutableArray<EOSituationGeographique> situations = new NSMutableArray<EOSituationGeographique>();

		situations.addObjectsFromArray(EOSituationGeographique.findForAffectation(edc, getCurrentAffectation()));

		situations.addObjectsFromArray(EOSituationGeographique.findSituationsInvalides(edc, getCurrentIndividu()));		

		eodSituationGeo.setObjectArray(situations.immutableClone());
		myView.getMyEOTableSituationGeo().updateData();
	}

	private void actualiserOccupations() {
		eodOccupation.setObjectArray(EOOccupation.findForIndividuAndDate(edc, getCurrentIndividu(), null));		
		myView.getMyEOTableOccupation().updateData();
	}

	private void actualiserAffectations() {
		eodAffectation.setObjectArray(EOAffectation.findForIndividu(edc, getCurrentIndividu(), null));		
		myView.getMyEOTableAffectation().updateData();
	}

	public JDialog getView() {
		return myView;
	}

	public JPanel getViewAffectation() {
		return myView.getViewAffectation();
	}

	public NSArray<EOAffectation> getAffectations() {
		return eodAffectation.displayedObjects();
	}
	public NSArray<EOOccupation> getOccupations() {
		return eodOccupation.displayedObjects();
	}
	public NSArray<EOSituationGeographique> getSituations() {
		return eodSituationGeo.displayedObjects();
	}

	/**
	 * 
	 */
	private void ajouterAffectation() {

		EOAffectation newAffectation = null;

		ctrlSaisieAff.setAffectationsExistantes(eodAffectation.displayedObjects());
		if (currentContrat != null && eodAffectation.displayedObjects().size() == 0)
			newAffectation = ctrlSaisieAff.ajouterPourContrat(currentContrat);
		else
			newAffectation = ctrlSaisieAff.ajouter(getCurrentIndividu());

		if (newAffectation != null) {
			actualiser();
			myView.getMyEOTableAffectation().forceNewSelectionOfObjects(new NSArray(newAffectation));
		}
		else {
			myView.getMyEOTableAffectation().updateUI();
			actualiserSituationsGeographiques();
		}
	}

	/**
	 * 
	 */
	private void ajouterOccupation() {
		EOOccupation newOccupation = ctrlSaisieOcc.ajouter(getCurrentIndividu());
		if (newOccupation != null) {
			actualiser();
			myView.getMyEOTableOccupation().forceNewSelectionOfObjects(new NSArray(newOccupation));
		}
	}

	/**
	 * 
	 */
	private void ajouterSituationGeo() {
		if (ctrlSaisieSit.modifier(
				EOSituationGeographique.creer(edc, getCurrentIndividu(), getCurrentAffectation()), getCurrentAffectation(), true)) {
			actualiserSituationsGeographiques();
		}
	}

	/***
	 * 
	 */
	private void modifierAffectation() {
		ctrlSaisieAff.setAffectationsExistantes(eodAffectation.displayedObjects());
		ctrlSaisieAff.modifier(getCurrentAffectation(), false);
		myView.getMyEOTableAffectation().updateUI();
	}
	private void modifierOccupation() {
		ctrlSaisieOcc.modifier(getCurrentOccupation());
		myView.getMyEOTableOccupation().updateUI();
		listenerOccupation.onSelectionChanged();
	}

	/**
	 * 
	 */
	private void modifierSituationGeo() {
		ctrlSaisieSit.modifier(getCurrentSituationGeo(), getCurrentAffectation(), false);
		myView.getMyEOTableSituationGeo().updateUI();
		listenerSituation.onSelectionChanged();
	}


	/**
	 * 
	 */
	private void supprimerAffectation() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette période d'affectation ?", "Oui", "Non"))		
			return;			

		try {

			// Suppression de la repart_structure si elle correspond a la date du jour
			ctrlSaisieAff.supprimerRepartStructures(new NSArray(getCurrentAffectation()));

			getCurrentAffectation().setEstValide(false);

			edc.saveChanges();

			eodAffectation.deleteSelection();


			myView.getMyEOTableAffectation().updateData();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			edc.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
		}
	}
	private void supprimerOccupation() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette période d'occupation ?", "Oui", "Non"))		
			return;			

		try {
			getCurrentOccupation().setEstValide(false);
			edc.saveChanges();
			eodOccupation.deleteSelection();
			myView.getMyEOTableOccupation().updateData();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			edc.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
		}
	}
	private void supprimerSituationGeo() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette situation géographique ?", "Oui", "Non"))		
			return;			

		try {
			getCurrentSituationGeo().setEstValide(false);
			edc.saveChanges();
			eodSituationGeo.deleteSelection();
			myView.getMyEOTableSituationGeo().updateData();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			edc.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
		}
	}

	private void renouvelerAffectation() {
		EOAffectation newAffectation = EOAffectation.renouveler(edc, getCurrentAffectation());
		ctrlSaisieAff.setAffectationsExistantes(eodAffectation.displayedObjects());
		ctrlSaisieAff.modifier(newAffectation, true);
		actualiser();
		myView.getMyEOTableAffectation().forceNewSelectionOfObjects(new NSArray(newAffectation));
	}
	private void renouvelerOccupation() {
	}

	private void fermer() {
		myView.setVisible(false);
	}

	public void nettoyerChamps(NSNotification notification) {
		setCurrentIndividu(null);
	}

	private class ListenerAffectation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentAffectation() != null && peutGererModule())
				modifierAffectation();
		}
		public void onSelectionChanged() {
			setCurrentAffectation((EOAffectation)eodAffectation.selectedObject());

		}
	}
	private class ListenerOccupation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentOccupation() != null && peutGererModule())
				modifierOccupation();
		}

		public void onSelectionChanged() {
			setCurrentOccupation((EOOccupation)eodOccupation.selectedObject());
			updateInterface();
		}
	}
	private class ListenerSituation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentSituationGeo() != null && peutGererModule())
				modifierSituationGeo();
		}
		public void onSelectionChanged() {
			setCurrentSituationGeo((EOSituationGeographique)eodSituationGeo.selectedObject());
			updateInterface();
		}
	}

	private void updateInterface() {

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null);
		myView.getBtnModifier().setEnabled(getCurrentAffectation() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentAffectation() != null);
		myView.getBtnRenouveler().setEnabled(getCurrentAffectation() != null);

		myView.getBtnAjouterOccupation().setEnabled(currentIndividu != null);
		myView.getBtnModifierOccupation().setEnabled(getCurrentOccupation() != null);
		myView.getBtnSupprimerOccupation().setEnabled(getCurrentOccupation() != null);
		myView.getBtnRenouvelerOccupation().setEnabled(false);

		myView.getBtnAfficherEmplois().setEnabled(getCurrentIndividu() != null && getCurrentOccupation() != null);
		myView.getBtnAjouterSituation().setEnabled(getCurrentIndividu() != null && getCurrentAffectation() != null);
		myView.getBtnModifierSituation().setEnabled(getCurrentSituationGeo() != null  && getCurrentAffectation() != null);
		myView.getBtnSupprimerSituation().setEnabled(getCurrentSituationGeo() != null);

	}
	
	private class RendererSituationGeo extends org.cocktail.application.client.swing.ZEOTableCellRenderer	{

		private static final long serialVersionUID = -2907930349355563787L;

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOSituationGeographique obj = (EOSituationGeographique) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().get(mdlRow);           

			if (obj.toAffectation() == null)
				leComposant.setForeground(new Color(255,0,0));
			else {
				leComposant.setForeground(new Color(0,0,0));
			}

			return leComposant;
		}
	}


}
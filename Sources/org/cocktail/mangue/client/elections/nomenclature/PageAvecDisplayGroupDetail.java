/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePageComplete;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;

/** G&egrave;re une page comportant un display group principal et un groupe de details. L'ajout/suppression de detail ne peut
 * etre fait qu'en mode modification. La validation est globale a toute la page.
 *
 */
public abstract class PageAvecDisplayGroupDetail extends ModelePageComplete {
	
	public EODisplayGroup 	displayGroupDetail;
	public EOTable 			listeAffichageDetails;
	
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroup()) {
			NSArray details = null;
			if (displayGroup().selectedObject() != null && !modeCreation()) {
				details = fetcherDetails();
			}
			if (displayGroupDetail != null) {
				displayGroupDetail.setObjectArray(details);
				displayGroupDetail.updateDisplayedObjects();
			}
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
	// actions
	public abstract void ajouterDetail();
	
	public void supprimerDetail() { 
		if (EODialogs.runConfirmOperationDialog("Alerte",messageConfirmationDestructionDetail(),"Oui","Non")) {
			if (!traitementsPourSuppressionDetail()) {	
				revertChanges(false);
			}
	     }
	}
	// méthodes du controller DG
	public boolean peutAjouterDetails() {
		return modificationEnCours() && !modeCreation();
	}
	public boolean peutModifierDetail() {
		return peutAjouterDetails() && displayGroupDetail.selectedObject() != null;
	}
	// méthodes protégées
	protected abstract NSArray fetcherDetails();
	protected abstract boolean traitementsPourSuppressionDetail();
	protected abstract void traitementsApresSuppressionDetail();
	protected abstract String messageConfirmationDestructionDetail();
	protected void preparerFenetre() {
		super.preparerFenetre();

	    if (listeAffichageDetails != null) {
	    		GraphicUtilities.changerTaillePolice(listeAffichageDetails,11);
	    		GraphicUtilities.rendreNonEditable(listeAffichageDetails);
	    }
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		if (displayGroupDetail != null) {
			displayGroupDetail.updateDisplayedObjects();
		}
	}
}

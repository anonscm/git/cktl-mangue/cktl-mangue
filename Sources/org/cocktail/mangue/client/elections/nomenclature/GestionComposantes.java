/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOComposanteElective;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementComposante;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamRepartElec;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

//31/03/2010 - suppression en deux temps : d'abord les paramétrages puis les composantes
//09/11/2010 - Adaptation Netbeans
public class GestionComposantes extends PageAvecDisplayGroupDetail {
	// méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("cComposanteElective")) {
			if (modeCreation()) {
				// vérifier si ce code est déjà utilisé et remettre à nul.
				java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
				while (e.hasMoreElements()) {
					EOComposanteElective composante = (EOComposanteElective)e.nextElement();
					if (composante != eo && composante.cComposanteElective().equals(value)) {
						EODialogs.runErrorDialog("Erreur","Ce code est déjà utilisé, veuillez en définir un autre");
						((EOComposanteElective)eo).setCComposanteElective(null);
						updaterDisplayGroups();
						break;
					}
				}
			}
		} 
	}
	// actions
	public void ajouterDetail() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(editingContext());
		if (structure != null) {
			NSArray structures = (NSArray)displayGroupDetail.allObjects().valueForKey("structure");
			if (structures.containsObject(structure) == false) {
				displayGroupDetail.insert();
				currentRegroupement().initAvecComposanteEtStructure(currentComposante(),structure);
				updaterDisplayGroups();
			}
		}
	}
	public void imprimer() {
		try {
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerComposantes",null,null,"ComposantesElectives" ,"Impression des composantes");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}

	//  méthodes du controller DG
	public boolean peutImprimer() {
		return !modificationEnCours() && displayGroup().displayedObjects().count() > 0;
	}
	// méthodes du controller DG
	public boolean peutValider() {
		return super.peutValider() && currentComposante().cComposanteElective() != null && 
		currentComposante().lcComposanteElective() != null && currentComposante().llComposanteElective() != null;
	}
	// méthodes protégées
	protected NSArray fetcherDetails() {
		return EORegroupementComposante.rechercherRegroupementsPourComposanteElective(editingContext(),currentComposante());
	}
	protected boolean traitementsPourSuppressionDetail() {
		currentRegroupement().supprimerRelations();
		displayGroupDetail.deleteSelection();
		return true;
	}
	protected void traitementsApresSuppressionDetail() {}
	protected String messageConfirmationDestructionDetail() {
		return "Voulez-vous vraiment supprimer cette structure ?";
	}
	protected void traitementsPourCreation() {
	}
	protected boolean traitementsPourSuppression() {
		java.util.Enumeration e = displayGroupDetail.displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EORegroupementComposante regroupement = (EORegroupementComposante)e.nextElement();
			regroupement.supprimerRelations();
			editingContext().deleteObject(regroupement);
		}
		currentComposante().supprimerParametragesEtModifierInstance();
		try {	// 31/03/2010
			editingContext().saveChanges();
		} catch (Exception exc) {
			EODialogs.runErrorDialog("Erreur", exc.getMessage());
			LogManager.logException(exc);
			editingContext().revert();
			return false;
		}
		displayGroup().deleteSelection();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette composante ? Toutes les données associées vont être modifiées, le traitement peut être long";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"ComposanteElective");
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cComposanteElective",EOSortOrdering.CompareAscending)));
		displayGroupDetail.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("structure.llStructure",EOSortOrdering.CompareAscending)));
	}
	/**  m&eacute;thode &agrave; surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lock&eacute;e,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutAdministrer();
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		if (!modeCreation()) {
			// Modification - invalider tous les paramétrages finaux
			EOParamRepartElec.invaliderInstances(editingContext(), "composanteElective", currentComposante());
		}
		return true;
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOComposanteElective currentComposante() {
		return (EOComposanteElective)displayGroup().selectedObject();
	}
	private EORegroupementComposante currentRegroupement() {
		return (EORegroupementComposante)displayGroupDetail.selectedObject();
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.client.components.DialogueSimple;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.component.CODisplayGroup;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOCollegeSectionElective;
import org.cocktail.mangue.modele.grhum.elections.EOSectionElective;
import org.cocktail.mangue.modele.grhum.elections.EOTypeInstance;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class GestionCollegeSections extends ModelePageComplete {
	public CODisplayGroup displayGroupCollege;
	private EOCollege currentCollege;

	// Accesseurs
	public String currentCollege() {
		if (currentCollege == null) {
			return null;
		} else {
			return currentCollege.lcCollege();
		}
	}
	public void setCurrentCollege(String libelleCollege) {
		currentCollege = null; 
		if (libelleCollege != null) {
			java.util.Enumeration e = displayGroupCollege.displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOCollege college = (EOCollege)e.nextElement();
				if (college.lcCollege().equals(libelleCollege)) {
					currentCollege = college;
					break;
				}
			}
		}
		preparerDisplayGroup();
	}
	// Actions
	public void ajouter() {
		
		if (currentCollege() == null) {
			EODialogs.runErrorDialog("ERREUR", "Veuillez sélectionner un collège !");
			return;
		}
		
		LogManager.logDetail("GestionCollegeSections ajouter");
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("cancelAjout", new Class[] {
				com.webobjects.foundation.NSNotification.class}), DialogueSimple.CANCEL_NOTIFICATION, null) ;
		NSMutableArray qualifiers = new NSMutableArray();
		NSArray sectionsElectives = ((NSArray)displayGroup().displayedObjects().valueForKey("sectionElective"));
		java.util.Enumeration e = sectionsElectives.objectEnumerator();
		while (e.hasMoreElements()) {
			EOSectionElective section = (EOSectionElective)e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("cSectionElective != %@", new NSArray(section.cSectionElective())));
		}
		// Ne pas afficher les sélections électives qui sont déjà sélectionnées et qui n'ont pas de typeRecrutement associé
		NSArray collegesSection = EOCollegeSectionElective.findSansTypeRecrutement(editingContext(), true);
		e = collegesSection.objectEnumerator();
		while (e.hasMoreElements()) {
			EOCollegeSectionElective collegeSection = (EOCollegeSectionElective)e.nextElement();
			if (sectionsElectives.containsObject(collegeSection.sectionElective()) == false) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("cSectionElective != %@", new NSArray(collegeSection.sectionElective().cSectionElective())));
			}
		}
		UtilitairesDialogue.afficherDialogue(this,"SectionElective","getSectionsElectives",false,true,new EOAndQualifier(qualifiers),true,true);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN, this);	
	}
	public void afficherTypeRecrutement() {
		LogManager.logDetail("GestionCollegeSections afficherTypeRecrutement");
		UtilitairesDialogue.afficherDialogue(this,"TypeRecrutementHu","getTypeRecrutement");
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN, this);	
	}
	public void supprimerTypeRecrutement() {
		LogManager.logDetail("GestionCollegeSections supprimerTypeRecrutement");
		currentRecord().removeObjectFromBothSidesOfRelationshipWithKey(currentRecord().typeRecrutement(), "typeRecrutement");
	}
	public void imprimer() {
		LogManager.logDetail("GestionCollegeSections - imprimer");
		try {
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerCollegesSections",null,null,"Colleges_Sections_Electives","Impression des collèges section");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	// Notifications
	public void getTypeRecrutement(NSNotification aNotif) {
		ajouterRelation(currentRecord(), aNotif.object() , "typeRecrutement");
	}
	public void getSectionsElectives(NSNotification aNotif) {
		if (aNotif.object() != null) {
			java.util.Enumeration e = ((NSArray)aNotif.object()).objectEnumerator();
			while (e.hasMoreElements()) {
				EOSectionElective section = (EOSectionElective)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)e.nextElement(),editingContext());
				EOCollegeSectionElective collegeSection = new EOCollegeSectionElective();
				collegeSection.initAvecCollegeEtSection(currentCollege,section);
				editingContext().insertObject(collegeSection);
				try {
					editingContext().saveChanges();
					// vérifier si cette section est affectée à un autre collège pour signaler à l'utilisateur
					NSArray collegesSections = EOCollegeSectionElective.findForSection(editingContext(), section, true);
					if (collegesSections.count() >= 2) {
						EODialogs.runInformationDialog("Attention", "Cette section élective est déjà affectée à un autre collège, vous devez lui ajouter un type de recrutement");
					}
				} catch (Exception exc) {
					LogManager.logException(exc);
					EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite\n" + exc.getMessage());
				}		
			}
		}
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN, this);	
		preparerDisplayGroup();
	}
	public void cancelAjout(NSNotification aNotif) {
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN, this);	
		updaterDisplayGroups();
	}
	
	// Méthodes du controller DG
	public boolean peutAjouter() {
		return currentCollege() != null && modeSaisiePossible();
	}
	public boolean peutSupprimerTypeRecrutement() {
		return modificationEnCours() && currentRecord() != null && currentRecord().typeRecrutement() != null;
	}
	public boolean peutImprimer() {
		return !modificationEnCours() && currentCollege() != null;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		displayGroupCollege.setSelectsFirstObjectAfterFetch(true);
		displayGroupCollege.setObjectArray(EOCollege.findForTypeInstance(editingContext(), EOTypeInstance.typeInstanceJDNPHU(editingContext())));
		currentCollege = (EOCollege)displayGroupCollege.selectedObject();
		displayGroupCollege.updateDisplayedObjects();
		super.preparerFenetre();
		raffraichirAssociations();
		updaterDisplayGroups();
	}
	/**  m&eacute;thode &agrave; surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lock&eacute;e,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutAdministrer();
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected NSArray fetcherObjets() {
		if (currentCollege == null) {
			return null;
		} else {
			return EOCollegeSectionElective.findForCollege(editingContext(), currentCollege, true);
		}
	}
	protected void traitementsPourCreation() {
		// la méthode n'est pas appelée car ajouter() est surchargée
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette section élective ?";
	}
	protected void parametrerDisplayGroup() {
		displayGroupCollege.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("llCollege", EOSortOrdering.CompareAscending)));
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("sectionElective.llSectionElective", EOSortOrdering.CompareAscending)));
	}
	protected void terminer() {
	}
	protected boolean traitementsAvantValidation() {
		if (currentRecord().typeRecrutement() == null) {
			// vérifier si cette section est affectée à un autre collège pour signaler à l'utilisateur
			NSArray collegesSections = EOCollegeSectionElective.findForSection(editingContext(), currentRecord().sectionElective(), false);
			if (collegesSections.count() >= 2 || (collegesSections.count() == 1 && collegesSections.objectAtIndex(0) != currentRecord())) {
				EODialogs.runErrorDialog("Attention", "Cette section élective est déjà affectée à un autre collège, vous devez lui ajouter un type de recrutement");
				return false;
			}
		}
		return true;
	}
	/** Affiche un message &grave; l'utilisateur si la section &eacute;lective est affect&eacute;e &agrave; un autre coll&egrave;ge
	 * et qu'elle n'a pas de type de recrutment
	 */

	protected boolean traitementsPourSuppression() {
		currentRecord().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur", message);
	}
	// Méthodes privées
	private EOCollegeSectionElective currentRecord() {
		return (EOCollegeSectionElective)displayGroup().selectedObject();
	}
	private void preparerDisplayGroup() {
		displayGroup().setObjectArray(fetcherObjets());
		updaterDisplayGroups();
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.elections.EOTypeInstance;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

/** Gestion des types d'election */

public class GestionTypesElection extends ModelePageComplete {
	/** Notification envoy&eacute;e pour synchronisation des types d'instances suite &agrave; la validation */
	public static String SYNCHRONISER_TYPE_ELECTION = "SynchronisationTypeElection";
	// Méthodes de délégation du display group
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("cTypeInstance")) {
			if (modeCreation()) {
				// vérifier si ce code est déjà utilisé et remettre à nul
				java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
				while (e.hasMoreElements()) {
					EOTypeInstance type = (EOTypeInstance)e.nextElement();
					if (type != eo && type.cTypeInstance().equals(value)) {
						EODialogs.runErrorDialog("Erreur","Ce code est déjà utilisé, veuillez en définir un autre");
						((EOTypeInstance)eo).setCTypeInstance(null);
						updaterDisplayGroups();
						break;
					}
				}
			}
		} 
	}
	// actions
	public void imprimer() {
		try {
			boolean editionDetaillee = EODialogs.runConfirmOperationDialog("", "Souhaitez-vous une impression détaillée ?", "Oui", "Non");
			Class[] classeParametres =  new Class[] {EOGlobalID.class,Boolean.class};
			Object[] parametres = new Object[]{editingContext().globalIDForObject(currentType()),new Boolean(editionDetaillee)};
			String nomFichier = "TypeInstance_" + currentType().cTypeInstance();
			if (editionDetaillee) {
				nomFichier = nomFichier + "_detaillee";
			}
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerTypeInstance",classeParametres,parametres,nomFichier ,"Impression du type d'élection " + currentType().llTypeInstance());

		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	// méthodes du controller DG
	public boolean boutonModificationAutorise() {
		return super.boutonModificationAutorise() && currentType() != null && currentType().estLocal();
	}
	public boolean peutImprimer() {
		return !modificationEnCours() && displayGroup().selectedObjects().count() > 0;
	}
	public boolean peutValider() {
		return super.peutValider() && currentType().cTypeInstance() != null && currentType().lcTypeInstance() != null && currentType().llTypeInstance() != null;
	}

	// méthodes protégées
	protected void terminer() {
	}
	protected void traitementsPourCreation() {
		currentType().init();
	}
	/** Le traitement est fait sur le serveur */
	protected boolean traitementsPourSuppression() {
		Class[] classeParametres =  new Class[] {EOGlobalID.class};
		Object[] parametres = new Object[]{editingContext().globalIDForObject(currentType())};
		try {
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			String result = (String)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestSupprimerTypeInstance",classeParametres,parametres,true);
			if (result == null) {
				//displayGroup().deleteSelection();
				// Raffraîchir les données
				EOApplication.sharedApplication().refreshData();
				return true;
			} else {
				EODialogs.runErrorDialog("Erreur",result);
				return false;
			}
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
			return false;
		}

	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce type d'élection ? Toutes les données associées vont être supprimées, le traitement peut être long";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"TypeInstance");
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cTypeInstance",EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	/**  m&eacute;thode &agrave; surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lock&eacute;e,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutAdministrer();
	}
	protected boolean traitementsAvantValidation() {
		if (!modeCreation()) {
			invaliderInstances();
		}
		return true;
	}
	protected void traitementsApresValidation() {
		NSNotificationCenter.defaultCenter().postNotification(SYNCHRONISER_TYPE_ELECTION,null);
		super.traitementsApresValidation();
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);

	}
	// méthodes privées
	private EOTypeInstance currentType() {
		return (EOTypeInstance)displayGroup().selectedObject();
	}
	private  void invaliderInstances() {
		java.util.Enumeration e = currentType().instances().objectEnumerator();
		while (e.hasMoreElements()) {
			EOInstance instance = (EOInstance)e.nextElement();
			instance.setEstEditionCoherente(false);
		}
	}

}

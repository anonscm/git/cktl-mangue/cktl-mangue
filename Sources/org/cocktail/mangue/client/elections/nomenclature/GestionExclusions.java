/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.select.elections.TypeExclusionSelectCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOExclusion;
import org.cocktail.mangue.modele.grhum.elections.EOGroupeExclusion;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionContrat;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionCorps;
import org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class GestionExclusions extends PageAvecDisplayGroupDetail {
	
	boolean exclusionsSupprimees;
	
	//	 méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("cGroupeExclusion")) {
			if (modeCreation()) {
		 		// vérifier si ce code est déjà utilisé et remettre à nul.
		 		for (EOGroupeExclusion groupe : (NSArray<EOGroupeExclusion>)displayGroup().displayedObjects()) {
		 			if (groupe != eo && groupe.cGroupeExclusion().equals(value)) {
		 				EODialogs.runErrorDialog("Erreur","Ce code est déjà utilisé, veuillez en définir un autre");
		 				((EOGroupeExclusion)eo).setCGroupeExclusion(null);
		 				updaterDisplayGroups();
		 				break;
		 			}
		 		}
	 		}
	 	} else if (key.equals("typeGroupe") && displayGroupDetail.displayedObjects().count() > 0) {
	 		// on change de type Fonctionnaire <-> Contractuel : les exclusions pour le type précédent n'ont plus de sens
	 		EODialogs.runInformationDialog("Attention","Les exclusions ne sont pas valides pour ce type, elles vont être supprimées.");
	 		currentGroupe().supprimerExclusions(displayGroupDetail.displayedObjects());
	 		exclusionsSupprimees = true;
	 		updaterDisplayGroups();
	 	}
	}
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		super.displayGroupDidChangeSelection(aGroup);
		if (aGroup == displayGroup()) {
			exclusionsSupprimees = false;
		}
	}

	/**
	 * 
	 */
	public void ajouterDetail() {
		
		NSArray<EOTypeExclusion> records = TypeExclusionSelectCtrl.sharedInstance(editingContext()).getTypesExclusion();
		if (records != null && records.size() > 0) {
			
			for (EOTypeExclusion myRecord : records) {
				NSArray<EOTypeExclusion> types = (NSArray<EOTypeExclusion>)displayGroupDetail.allObjects().valueForKey(EOExclusion.TYPE_EXCLUSION_KEY);
				if (types.containsObject(myRecord) == false) {
					displayGroupDetail.insert();
					currentExclusion().initAvecGroupeExclusionEtTypeExclusion(currentGroupe(),myRecord);
				}				
			}
			
			updaterDisplayGroups();
			
		}
	}

	public boolean peutModifier() {
		return super.modificationEnCours() && (currentGroupe().estLocal() == true || modeCreation());
	}
	public boolean peutSupprimer() {
		return super.boutonModificationAutorise();
	}
	public boolean peutValider() {
		return super.peutValider() && currentGroupe().cGroupeExclusion() != null && 
		currentGroupe().lcGroupeExclusion() != null && currentGroupe().llGroupeExclusion() != null;
	}
	protected NSArray fetcherDetails() {
		return EOExclusion.findForGroupe(editingContext(),currentGroupe());
	}
	protected boolean traitementsPourSuppressionDetail() {
		currentExclusion().supprimerRelations();
		displayGroupDetail.deleteSelection();
		return true;
	}
	protected void traitementsApresSuppressionDetail() {}
	protected String messageConfirmationDestructionDetail() {
		return "Voulez-vous vraiment supprimer cette exclusion ?";
	}
	protected void traitementsPourCreation() {
		currentGroupe().init();
	}
	
	protected boolean traitementsPourSuppression() {
				
		// INCLUSION CORPS
		NSArray<EOInclusionContrat> inclusionsContrat = EOInclusionContrat.rechercherInclusionsPourGroupeExclusion(editingContext(), EOInclusionContrat.ENTITY_NAME, currentGroupe());
		for (EOInclusionContrat inclusion : inclusionsContrat) {
			inclusion.supprimerRelations();
			editingContext().deleteObject(inclusion);
		}

		// INCLUSION CONTRAT
		NSArray<EOInclusionCorps> inclusionsCorps = EOInclusionContrat.rechercherInclusionsPourGroupeExclusion(editingContext(), EOInclusionCorps.ENTITY_NAME, currentGroupe());
		for (EOInclusionCorps inclusion : inclusionsCorps) {
			inclusion.supprimerRelations();
			editingContext().deleteObject(inclusion);
		}
		
		// EXCLUSIONS
		for (EOExclusion exclusion : (NSArray<EOExclusion>)displayGroupDetail.displayedObjects()) {
			editingContext().deleteObject(exclusion);
		}

		editingContext().deleteObject(currentGroupe());
		
		displayGroup().deleteSelection();
		
		return true;
	}
	
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce groupe d'exclusion ? Toutes les données associées vont être modifiées, le traitement peut être long";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),EOGroupeExclusion.ENTITY_NAME);
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cGroupeExclusion",EOSortOrdering.CompareAscending)));
		displayGroupDetail.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("typeExclusion.cTypeExclusion",EOSortOrdering.CompareAscending)));
	}
	/**  methode a surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lockee,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutAdministrer();
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void traitementsApresValidation() {
		super.traitementsApresValidation();
		exclusionsSupprimees = false;
	}
	protected void traitementsApresRevert() {
		super.traitementsApresRevert();
		if (exclusionsSupprimees && currentGroupe() != null) {
			displayGroupDetail.setObjectArray(fetcherDetails());
			updaterDisplayGroups();
		}
		exclusionsSupprimees = false;
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOGroupeExclusion currentGroupe() {
		return (EOGroupeExclusion)displayGroup().selectedObject();
	}
	private EOExclusion currentExclusion() {
		return (EOExclusion)displayGroupDetail.selectedObject();
	}
}

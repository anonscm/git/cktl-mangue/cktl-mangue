/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.elections.ComposanteElectiveSelectCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOComposanteElective;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementSecteur;
import org.cocktail.mangue.modele.grhum.elections.EOSecteur;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSecteur;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;

public class GestionSecteurs extends PageAvecDisplayGroupDetail {

	/**
	 * 
	 * @param group
	 * @param value
	 * @param eo
	 * @param key
	 */
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals(EOSecteur.C_SECTEUR_KEY)) {
			if (modeCreation()) {
				
		 		// vérifier si ce code est déjà utilisé et remettre à nul.
				for (EOSecteur mySecteur : (NSArray<EOSecteur>) displayGroup().displayedObjects()) {
		 			if (mySecteur != eo && mySecteur.cSecteur().equals(value)) {
		 				EODialogs.runErrorDialog("Erreur","Ce code est déjà utilisé, veuillez en définir un autre");
		 				((EOSecteur)eo).setCSecteur(null);
		 				updaterDisplayGroups();
		 				break;
		 			}
		 		}
	 		}
	 	} 
	}

	/**
	 * 
	 */
	public void ajouterDetail() {

		NSArray<EOComposanteElective> records = ComposanteElectiveSelectCtrl.sharedInstance(editingContext()).getComposantes();
		if (records != null && records.size() >= 0) {

			for (EOComposanteElective myRecord : records) {
				
				NSArray<EOComposanteElective> composantes = (NSArray<EOComposanteElective>)displayGroupDetail.allObjects().valueForKey("composante");
				if (composantes.containsObject(myRecord) == false) {
					displayGroupDetail.insert();
					currentRegroupement().initAvecSecteurEtComposante(currentSecteur(),myRecord);
					updaterDisplayGroups();
				}

			}
		}
	}
	
	/**
	 * 
	 */
    public void imprimer() {
    	try {
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerSecteurs",null,null,"Secteurs" ,"Impression des secteurs");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
    }

    /**
     * 
     * @param aNotif
     */
	public void getComposanteElective(NSNotification aNotif) {
		if (aNotif.object() != null) {
			EOComposanteElective composante = (EOComposanteElective)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			NSArray composantes = (NSArray)displayGroupDetail.allObjects().valueForKey("composante");
			if (composantes.containsObject(composante) == false) {
				displayGroupDetail.insert();
				currentRegroupement().initAvecSecteurEtComposante(currentSecteur(),composante);
				updaterDisplayGroups();
			}
		}
	}
	//  méthodes du controller DG
	public boolean peutImprimer() {
		return !modificationEnCours() && displayGroup().displayedObjects().count() > 0;
	}
	public boolean peutValider() {
		return super.peutValider() && currentSecteur().cSecteur() != null && 
			currentSecteur().lcSecteur() != null && currentSecteur().llSecteur() != null;
	}
	// méthodes protégées
	protected NSArray fetcherDetails() {
		return EORegroupementSecteur.rechercherRegroupementsPourSecteur(editingContext(),currentSecteur());
	}
	protected boolean traitementsPourSuppressionDetail() {
		currentRegroupement().supprimerRelations();
		displayGroupDetail.deleteSelection();
		return true;
	}
	protected void traitementsApresSuppressionDetail() {}
	protected String messageConfirmationDestructionDetail() {
		return "Voulez-vous vraiment supprimer cette composante ?";
	}
	protected void traitementsPourCreation() {
	}
	protected boolean traitementsPourSuppression() {
		java.util.Enumeration e = displayGroupDetail.displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EORegroupementSecteur regroupement = (EORegroupementSecteur)e.nextElement();
			regroupement.supprimerRelations();
			editingContext().deleteObject(regroupement);
		}
		// Supprimer les parametrages et les électeurs
		currentSecteur().supprimerParametragesEtElecteurs();
		try { // 31/03/2010
			editingContext().saveChanges();
		} catch (Exception exc) {
			EODialogs.runErrorDialog("Erreur", exc.getMessage());
			LogManager.logException(exc);
			editingContext().revert();
			return false;
		}
		displayGroup().deleteSelection();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce secteur ? Toutes les données associées vont être modifiées, le traitement peut être long";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"Secteur");
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cSecteur",EOSortOrdering.CompareAscending)));
		displayGroupDetail.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("composante.cComposanteElective",EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		if (!modeCreation()) {
			invaliderInstances();
		}
		return true;
	}
	/**  m&eacute;thode &agrave; surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lock&eacute;e,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutAdministrer();
	}
	protected  void terminer() {
	}
	// méthodes privées
	private EOSecteur currentSecteur() {
		return (EOSecteur)displayGroup().selectedObject();
	}
	private EORegroupementSecteur currentRegroupement() {
		return (EORegroupementSecteur)displayGroupDetail.selectedObject();
	}
	private  void invaliderInstances() {
		NSArray params = EOParamSecteur.rechercherParametragesPourCollegeEtSecteur(editingContext(), null, currentSecteur());
		java.util.Enumeration e= params.objectEnumerator();
		while (e.hasMoreElements()) {
			EOParamSecteur param = (EOParamSecteur)e.nextElement();
			param.instance().setEstEditionCoherente(false);
		}
	}
}

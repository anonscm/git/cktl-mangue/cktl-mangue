/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.modele.InclusionSituation;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOGroupeExclusion;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionCorps;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;

/** Modelise le comportement des pages comportant un display group de detail + 2 displays groups : un pour les groupes d'exclusion,
 * un pour les categories. Sous-class&eacute;e pour la gestion des inclusions de corps ou de type de contrat de travail.<BR>
 * @author christine
 *
 */
public abstract class PageInclusionSituation extends PageInclusion {
	
	public EODisplayGroup displayGroupGroupeExclusion,displayGroupCategorie;
	public EOTable listeAffichageGroupeExclusion,listeAffichageCategorie;
	private NSMutableArray inclusions;
	private boolean constructionEnCours;

	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup != displayGroup() && aGroup != displayGroupCategorie && !constructionEnCours) {
			construireDisplayGroups(aGroup);
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
	// actions
	public void supprimerGroupeExclusion() { 
		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer ce groupe d'exclusion ?","Oui","Non")) {
			supprimerDetails(currentGroupe(),null);
		}
	}
	public void supprimerCategorie() { 
		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer cette catégorie ?","Oui","Non")) {
			supprimerDetails(currentGroupe(),currentCategorie());
		}
	}
	public void ajouterGroupeExclusion() {
		UtilitairesDialogue.afficherDialogue(this,"GroupeExclusion","getGroupeExclusion",false,null,false);
	}
	public void ajouterCategorie() {
		UtilitairesDialogue.afficherDialogue(this,"Categorie","getCategorie",false,null,true);
	}
	// Notifications
	public void getGroupeExclusion(NSNotification aNotif) {
		if (aNotif.object() != null) {
			EOGroupeExclusion groupe =  (EOGroupeExclusion)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			// si ce groupe d'exclusion n'est pas déjà valable
			if (displayGroupGroupeExclusion.allObjects().containsObject(groupe) == false) {
				boolean ajouterPourTous = EODialogs.runConfirmOperationDialog("Attention",messagePourAjoutGroupeExclusion(groupe),"Oui","Non");
				ajouterGroupeExclusion(groupe,ajouterPourTous);
			}
			setEdited(true);
			// stocker l'ancienne sélection avant reconstruction des DG
			constructionEnCours = true;
			EOGenericRecord currentDetail = currentDetail();
			construireDisplayGroups(displayGroupDetail);
			displayGroupDetail.selectObject(currentDetail);
			displayGroupGroupeExclusion.selectObject(groupe);
			updaterDisplayGroups();
			constructionEnCours = false;
		}
	}
	public void getCategorie(NSNotification aNotif) {
		if (aNotif.object() != null) {
			EOCategorie categorie =  (EOCategorie)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			// si cette categorie n'est pas déjà valable
			if (displayGroupCategorie.allObjects().containsObject(categorie) == false) {
				InclusionSituation inclusion = uniqueInclusionPourDetail(currentGroupe());
				if (inclusion == null) {
					creerInclusion(currentDetail(),currentGroupe(),categorie);
				} else {
					inclusion.addObjectToBothSidesOfRelationshipWithKey(categorie,"categorie");
				}
			}
			//	stocker les anciennes sélections avant reconstruction des DG
			Object currentDetail = currentDetail();
			EOGroupeExclusion currentGroupe = currentGroupe();
			constructionEnCours = true;
			construireDisplayGroups(displayGroupGroupeExclusion);
			displayGroupDetail.selectObject(currentDetail);
			displayGroupGroupeExclusion.selectObject(currentGroupe);
			displayGroupCategorie.selectObject(categorie);
			updaterDisplayGroups();
			constructionEnCours = false;
		}
	}
	// méthodes du controller DG
	public boolean peutModifierGroupeExclusion() {
		return super.peutModifierDetail() && currentGroupe() != null;
	}
	public boolean peutModifierCategorie() {
		return peutModifierGroupeExclusion() && currentCategorie() != null;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean peutSupprimerCategorie() {
		return currentCategorie() != null;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		GraphicUtilities.changerTaillePolice(listeAffichageGroupeExclusion,11);
		GraphicUtilities.rendreNonEditable(listeAffichageGroupeExclusion);
		GraphicUtilities.changerTaillePolice(listeAffichageCategorie,11);
		GraphicUtilities.rendreNonEditable(listeAffichageCategorie);
	}
	/** retourne le nom de la relation principale pour une inclusion : a definir dans la sous-classe */
	protected abstract String relationPourEntitePrincipale();
	/** retourne la cl&eacute; sur lequel effectuer le tri du Display group details */
	protected abstract String cleTriDetail();
	/** message affiche a l'utilisateur suite a l'ajout d'un groupe d'exclusion pour proposer d'ajouter ce groupe d'exclusion
	 * a toutes les inclusions
	 * @param groupeExclusion
	 * @return
	 */
	protected abstract String messagePourAjoutGroupeExclusion(EOGroupeExclusion groupeExclusion);
	protected NSArray fetcherDetails() {
		NSMutableArray dejaInclus = new NSMutableArray(), aInclure = new NSMutableArray();
		inclusions = new NSMutableArray(super.fetcherDetails());
		// ne mettre dans le display group que les objets différents de type principal (corps ou typeContratTravail)
		java.util.Enumeration e = inclusions.objectEnumerator();
		while (e.hasMoreElements()) {
			InclusionSituation inclusion = (InclusionSituation)e.nextElement();
			EOGenericRecord detail = (EOGenericRecord)inclusion.valueForKeyPath(relationPourEntitePrincipale());
			if (dejaInclus.containsObject(detail) == false) {
				aInclure.addObject(detail);
				dejaInclus.addObject(detail);
			}
		}
		return aInclure;
	}
	protected boolean traitementsPourSuppressionDetail() {
		supprimerDetails(null,null);
		return true;
	}
	protected void traitementsApresRevert() {
		fetcherDetails();
		// retourner à l'état de la dernière sélection
		if (currentGroupe() != null) {
			construireDisplayGroups(displayGroupDetail);
			construireDisplayGroups(displayGroupGroupeExclusion);
		} else if (currentDetail() != null) {
			construireDisplayGroups(displayGroup());
			construireDisplayGroups(displayGroupDetail);
		} else {
			construireDisplayGroups(displayGroup());
		}
		super.traitementsApresRevert();
	}
	protected void parametrerDisplayGroup() {
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupCategorie.updateDisplayedObjects();
		displayGroupGroupeExclusion.updateDisplayedObjects();
	}
	// cree une nouvelle inclusion
	protected void creerInclusion(EOGenericRecord detail,EOGroupeExclusion groupe,EOCategorie categorie) {
		EOClassDescription classeInclusion = EOClassDescription.classDescriptionForEntityName(nomEntite());
		InclusionSituation inclusion = (InclusionSituation)classeInclusion.createInstanceWithEditingContext(editingContext(),null);
		inclusion.initAvecCollegeGroupeExclusionEtCategorie(currentCollege(),groupe,categorie);
		inclusion.addObjectToBothSidesOfRelationshipWithKey(detail,relationPourEntitePrincipale());
		editingContext().insertObject(inclusion);
		inclusions.addObject(inclusion);
	}

	/**
	 * 
	 * @param aGroup
	 */
	protected void construireDisplayGroups(EODisplayGroup aGroup) {
		if (aGroup != displayGroup() && currentDetail() == null) {
			// on a juste besoin de resetter les autres display group car déselection dans DG de détail
			resetterDisplayGroups(true);
			return;
		}
		NSMutableArray dejaInclus = new NSMutableArray(), aInclure = new NSMutableArray();
		java.util.Enumeration e = inclusions.objectEnumerator();
		while (e.hasMoreElements()) {
			InclusionSituation inclusion = (InclusionSituation)e.nextElement();
			Object detail = inclusion.valueForKeyPath(relationPourEntitePrincipale());
			if (aGroup == displayGroup()) {
				if (dejaInclus.containsObject(detail) == false) {
					aInclure.addObject(detail);
					dejaInclus.addObject(detail);
				}
			} else if (detail == currentDetail()) {
				if (aGroup == displayGroupDetail) {
					EOGroupeExclusion groupe = inclusion.groupeExclusion();
					if (groupe != null && dejaInclus.containsObject(groupe) == false) {
						aInclure.addObject(groupe);
						dejaInclus.addObject(groupe);
					}
				} else if (aGroup == displayGroupGroupeExclusion && currentGroupe() != null && inclusion.groupeExclusion() == currentGroupe()) {
					EOCategorie categorie = inclusion.categorie();
					if (categorie != null && dejaInclus.containsObject(categorie) == false) {
						aInclure.addObject(categorie);
						dejaInclus.addObject(categorie);
					}
				}	
			}
		}
		resetterDisplayGroups(aGroup == displayGroup() || aGroup == displayGroupDetail);
		if (aGroup == displayGroup()) {
			displayGroupDetail.setObjectArray(aInclure);
		} else if (aGroup == displayGroupDetail) {
			displayGroupGroupeExclusion.setObjectArray(aInclure);
		} else if (aGroup == displayGroupGroupeExclusion) {
			displayGroupCategorie.setObjectArray(aInclure);
		}
	}
	// méthodes privées
	protected EOGenericRecord currentDetail() {
		return (EOGenericRecord)displayGroupDetail.selectedObject();
	}
	protected EOGroupeExclusion currentGroupe() {
		if (displayGroupGroupeExclusion.selectedObject() == null)
			return null;
		return ((EOInclusionCorps)displayGroupGroupeExclusion.selectedObject()).groupeExclusion();
	}
	protected EOCategorie currentCategorie() {
		if (displayGroupCategorie.selectedObject() == null)
			return null;
		return ((EOInclusionCorps)displayGroupCategorie.selectedObject()).categorie();
	}
	private void resetterDisplayGroups(boolean tousDG) {
		if (tousDG) {
			displayGroupGroupeExclusion.setObjectArray(null);
		}
		displayGroupCategorie.setObjectArray(null);
	}
	private void supprimerDetails(EOGroupeExclusion groupeExclusion,EOCategorie categorie) {
		// supprimer toutes les inclusions consiste lorsqu'il y a plusieurs inclusions pour un même type (par exemple pour un groupe d'exclusion)
		// à les supprimer toutes sauf une dans laquelle on mettra les relations à nulles. Seules les inclusions correspondant à la relation
		// principale sont à détruires;
		NSArray oldInclusions = new NSArray(inclusions);
		int nbInclusionsPourType = 0;
		if (categorie != null) {
			nbInclusionsPourType = displayGroupCategorie.allObjects().count();
		} else if (groupeExclusion != null) {
			nbInclusionsPourType = displayGroupGroupeExclusion.allObjects().count();
		}
		java.util.Enumeration e = oldInclusions.objectEnumerator();
		while (e.hasMoreElements()) {
			InclusionSituation inclusion = (InclusionSituation)e.nextElement();
			Object detail = inclusion.valueForKeyPath(relationPourEntitePrincipale());
			if (detail == currentDetail()) {
				if (groupeExclusion == null || inclusion.groupeExclusion() == groupeExclusion) {
					if (categorie == null || inclusion.categorie() == categorie) {
						if ((groupeExclusion == null && categorie == null) || nbInclusionsPourType > 1) {
							inclusion.supprimerRelations();
							editingContext().deleteObject(inclusion);
							nbInclusionsPourType--;
							inclusions.removeObject(inclusion);
						} else {	 // nbInclusionsPourType == 1 && (groupeExclusion != null || categorie != null)
							if (categorie != null) {
								// dernière catégorie car nbInclusionsPourType == 1
								// supprimer simplement la catégorie de l'inclusion afin de maintenir celle-ci pour le groupe d'exclusion
								inclusion.removeObjectFromBothSidesOfRelationshipWithKey(categorie,"categorie");
							} else {
								// dernier groupe d'exclusion car nbInclusionsPourType == 1
								// supprimer simplement le grouoe de l'inclusion afin de maintenir celle-ci
								inclusion.removeObjectFromBothSidesOfRelationshipWithKey(groupeExclusion,"groupeExclusion");
								if (inclusion.categorie() != null) {
									inclusion.removeObjectFromBothSidesOfRelationshipWithKey(inclusion.categorie(),"categorie");
								}
							}
							break;
						}
					}
				}
			}
		}
		setEdited(true);
		// reconstruire tous les displays groups
		if (groupeExclusion == null) {
			// suppression d'une relation principale
			construireDisplayGroups(displayGroup());
		} else if (categorie == null) {
			// suppression d'un groupe d'exclusion
			construireDisplayGroups(displayGroupDetail);
		} else {
			// suppression d'une catégorie
			construireDisplayGroups(displayGroupGroupeExclusion);
		} 
	}
	private void ajouterGroupeExclusion(EOGroupeExclusion groupe,boolean pourTous) {
		InclusionSituation inclusion = uniqueInclusionPourDetail(null);
		if (inclusion == null) {
			creerInclusion(currentDetail(),groupe,null);
		} else {
			inclusion.addObjectToBothSidesOfRelationshipWithKey(groupe,"groupeExclusion");
		}
		if (pourTous) {
			// trier les inclusions par detail
			java.util.Enumeration e = displayGroupDetail.allObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOGenericRecord detail = (EOGenericRecord)e.nextElement();
				if (detail != currentDetail()) {
					NSArray inclusionsPourDetail = rechercherInclusionsPourDetail(detail);
					NSArray groupesPourDetail = (NSArray)inclusionsPourDetail.valueForKey("groupeExclusion");
					if (groupesPourDetail.containsObject(groupe) == false) {
						inclusion = (InclusionSituation)inclusionsPourDetail.objectAtIndex(0);	// il y en a au moins forcément une ou ce détail ne serait pas dans le DG
						// ce groupe d'exclusion n'était pas inclus pour ce détail
						boolean aCreer = (inclusionsPourDetail.count() > 1 || inclusion.groupeExclusion() != null);
						if (aCreer) {
							creerInclusion(detail,groupe,null);
						} else {
							inclusion.addObjectToBothSidesOfRelationshipWithKey(groupe,"groupeExclusion");
						}
					}
				}
			}
		}
	}
	// retourne l'inclusion unique qui aurait le groupe d'exclusion à null si le paramètre est nul ou la catégorie à nulle sinon
	private InclusionSituation uniqueInclusionPourDetail(EOGroupeExclusion groupe) {
		java.util.Enumeration e = inclusions.objectEnumerator();
		while (e.hasMoreElements()) {
			InclusionSituation inclusion = (InclusionSituation)e.nextElement();
			Object detail = inclusion.valueForKeyPath(relationPourEntitePrincipale());
			if (detail == currentDetail()) {
				if (inclusion.groupeExclusion() == groupe) {
					if (groupe == null) {
						return inclusion;
					} else if (inclusion.categorie() == null) {
						return inclusion;
					}
				}	
			}
		}
		return null;
	}
	// retourne le tableau des inclusions liés à un certain détail
	private NSArray rechercherInclusionsPourDetail(EOGenericRecord detail) {
		NSMutableArray result = new NSMutableArray();
		java.util.Enumeration e = inclusions.objectEnumerator();
		while (e.hasMoreElements()) {
			InclusionSituation inclusion = (InclusionSituation)e.nextElement();
			Object detailInclusion = inclusion.valueForKeyPath(relationPourEntitePrincipale());
			if (detail == detailInclusion) {
				result.addObject(inclusion);
			}
		}
		return result;
	}
}

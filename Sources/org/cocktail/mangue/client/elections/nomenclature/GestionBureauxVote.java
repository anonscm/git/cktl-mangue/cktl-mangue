/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.NomenclatureAvecDate;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOBureauVote;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementBv;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamRepartElec;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;

// 29/03/2010 - correction de bugs divers : affichage de la commune sélectionnée pour l'adresse. Pas de modification de la
// commune quand sélection de l'adresse
// 31/03/2010 - suppression en deux temps : d'abord les paramétrages puis le bureau de vote
// 08/11/2010 - Adaptation Netbeans
public class GestionBureauxVote extends PageAvecDisplayGroupDetail {
	public EODisplayGroup communeDG;
	public JTextField champVille;
	public JComboBox popupCommune;
	/** Pour notifier la modification/suppression d'un bureau de vote.
	 * Objet transmis : globalID du bureau de vote ou null
	 */
	public final static String MODIFICATION_BUREAU_VOTE = "ModificationBureauVote";
	// méthodes de délégation du display group
	/** 
	 * Methode de delegation des EODisplayGroup
	 *	Selection d'une adresse et mise a jour des donnees correspondantes.
	 *
	 * @param eod DisplayGroup dans lequel la selection a change. 
	 */
	public void displayGroupDidChangeSelection(EODisplayGroup eod)	{
		if (eod == displayGroup()) {
			super.displayGroupDidChangeSelection(eod);
			modifierListeCommunes();
		}
	}
	/** 
	 * Methode de delegation des EODisplayGroup
	 *	Mise &agrave; jour de l'interface en fonction du code postal ou du code pays 
	 */
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("cBureauVote")) {
			if (modeCreation()) {
				// vérifier si ce code est déjà utilisé et remettre à nul.
				java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
				while (e.hasMoreElements()) {
					EOBureauVote bureau = (EOBureauVote)e.nextElement();
					if (bureau != eo && bureau.cBureauVote().equals(value)) {
						EODialogs.runErrorDialog("Erreur","Ce code est déjà utilisé, veuillez en définir un autre");
						((EOBureauVote)eo).setCBureauVote(null);
						updaterDisplayGroups();
						break;
					}
				}
			}
		} else if (key.equals("adresse.codePostal")  && (value == null || ((String)value).length() == 0 || ((String)value).length() == 5)) {
			// afficher la liste des communes liées à ce code postal ou un champ texte pour saisir la commune
			modifierListeCommunes();
			currentAdresse().setCpEtranger(null);
			if (value != null && value.equals("") == false) {
				// afficher la france comme pays par défaut
				EOPays pays = (EOPays)SuperFinder.rechercherAvecAttributEtValeurEgale(editingContext(),"Pays","code",EOPays.CODE_PAYS_FRANCE).objectAtIndex(0);
				currentAdresse().addObjectToBothSidesOfRelationshipWithKey(pays,"toPays");
			} else {
				// pas de code postal donc ville inconnue
				currentAdresse().setVille(null);
			}
			updaterDisplayGroups();
		} else if (key.equals("adresse.cpEtranger")) {
			if (value != null && value.equals("") == false) {
				// ne pas autoriser de code postal
				currentAdresse().setCodePostal(null);
				modifierListeCommunes();
				if (currentAdresse().toPays().code().equals(EOPays.CODE_PAYS_FRANCE)) {
					// supprimer le pays, l'utilisateur devra en sélectionner un nouveau
					currentAdresse().removeObjectFromBothSidesOfRelationshipWithKey(currentAdresse().toPays(),"toPays");
				}
				updaterDisplayGroups();
			}
		}
	}
	// actions
	/** affiche la liste des pays */
	public void afficherPays() {
		UtilitairesDialogue.afficherDialogue(this,"Pays","getPays", false, NomenclatureAvecDate.qualifierPourDate(DateCtrl.today()), false);
	}
	public void imprimer() {
		try {
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerBureauxDeVote",null,null,"BureauxDeVote" ,"Impression des bureaux de vote");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	// Notifications
	/** r&eacute;cup&egrave;re le pays choisi par l'utilisateur */
	public void getPays(NSNotification aNotif) {
		ajouterRelation(currentAdresse(),aNotif.object(),"toPays");
	}
	// Actions
	public void ajouterDetail() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(editingContext());
		if (structure != null) {
			NSArray structures = (NSArray)displayGroupDetail.allObjects().valueForKey("structure");
			if (structures.containsObject(structure) == false) {
				displayGroupDetail.insert();
				setEdited(true);
				currentRegroupement().initAvecBureauVoteEtStructure(currentBureau(),structure);
				updaterDisplayGroups();
			}
		}
	}
	// méthodes du controller DG
	public boolean peutImprimer() {
		return !modificationEnCours() && displayGroup().displayedObjects().count() > 0;
	}
	/** On peut choisir le pays en mode modification si le code postal n'est pas saisi */
	public boolean peutChoisirPays() {
		return modificationEnCours() && currentBureau().adresse() != null && currentBureau().adresse().codePostal() == null;
	}
	public boolean peutValider() {
		return super.peutValider() && currentBureau().cBureauVote() != null && currentBureau().lcBureauVote() != null && currentBureau().llBureauVote() != null
		&& currentBureau().adresse().adrAdresse1() != null && currentBureau().adresse().toPays() != null;
	}
	// méthodes protégées
	protected void traitementsPourCreation() {
		EOAdresse adresse = new EOAdresse();
		adresse.init();
		editingContext().insertObject(adresse);
		currentBureau().initAvecAdresse(adresse);
	}
	protected boolean traitementsPourSuppression() {
		currentBureau().supprimerParametragesEtModifierInstance();
			try {	// 31/03/2010
			editingContext().saveChanges();
		} catch (Exception exc) {
			EODialogs.runErrorDialog("Erreur", exc.getMessage());
			LogManager.logException(exc);
			editingContext().revert();
			return false;
		}
		// 29/03/2010 - suppression des regroupements bv était faite 2 fois dans EOBureauVote et ici
		/*java.util.Enumeration e = displayGroupDetail.displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EORegroupementBv regroupement = (EORegroupementBv)e.nextElement();
			regroupement.supprimerRelations();
			editingContext().deleteObject(regroupement);
		}*/
		EOAdresse adresse = currentAdresse();
		currentBureau().removeObjectFromBothSidesOfRelationshipWithKey(adresse, "adresse");
		editingContext().deleteObject(adresse);

		deleteSelectedObjects();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous supprimer ce bureau de vote ? Toutes les données associées vont être supprimées, le traitement peut être long";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"BureauVote");
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cBureauVote",EOSortOrdering.CompareAscending)));
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	/**  m&eacute;thode &agrave; surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lock&eacute;e,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutAdministrer();
	}
	protected boolean traitementsAvantValidation() {
		if (modeCreation()) {
			// créer la clé primaire
			currentAdresse().setAdrOrdre(new Integer(EOAdresse.construireAdresseOrdre(editingContext()).intValue()));
			currentBureau().setAdrOrdre(currentAdresse().adrOrdre());
		} else {
			// Modification - invalider tous les paramétrages finaux
			EOParamRepartElec.invaliderInstances(editingContext(), "bureauVote", currentBureau());
		}
		return true;
	}
	protected void traitementsApresValidation() {
		//if (!modeCreation())
		super.traitementsApresValidation();
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	protected NSArray fetcherDetails() {
		return EORegroupementBv.rechercherRegroupementsPourBureauDeVote(editingContext(),currentBureau());
	}
	protected boolean traitementsPourSuppressionDetail() {
		currentRegroupement().supprimerRelations();
		
		displayGroupDetail.deleteSelection();
		return true;
	}
	protected void traitementsApresSuppressionDetail() {}
	protected String messageConfirmationDestructionDetail() {
		return "Voulez-vous vraiment supprimer cette structure ?";
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOBureauVote currentBureau() {
		return (EOBureauVote)displayGroup().selectedObject();
	}
	private EORegroupementBv currentRegroupement() {
		return (EORegroupementBv)displayGroupDetail.selectedObject();
	}
	private EOAdresse currentAdresse() {
		if (currentBureau() != null) {
			return currentBureau().adresse();
		} else {
			return null;
		}
	}
	// affiche le popup des communes ou un champ texte si le code postal n'est pas défini
	private void modifierListeCommunes() {
		boolean estCommuneEnFrance,estVilleUnique = false;
		if (currentAdresse() != null && currentAdresse().codePostal() != null) {
			communeDG.setObjectArray((NSArray)SuperFinder.rechercherCommunes(editingContext(),currentAdresse().codePostal()));
			if (communeDG.displayedObjects().count() >= 1) {
				RecordAvecLibelle commune = (RecordAvecLibelle)communeDG.displayedObjects().objectAtIndex(0);
				// 29/03/2010 une seule commune l'associer directement si on est en mode modification
				if (modificationEnCours()) {
					currentAdresse().setVille(commune.libelle());
				} 

				estVilleUnique = communeDG.displayedObjects().count() == 1;
			} 
			estCommuneEnFrance = true;
		} else {
			communeDG.setObjectArray(null);
			estCommuneEnFrance = false;
		}
		champVille.setVisible(!estCommuneEnFrance || estVilleUnique);
		popupCommune.setVisible(estCommuneEnFrance && !estVilleUnique);

	}
}

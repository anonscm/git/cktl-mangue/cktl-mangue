/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.elections.PagePourTypeElection;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

public class GestionElections extends PagePourTypeElection {


	/** Action pour ajouter : invoque traitementsPourCreation pour faire les traitements specifiques */
	public void ajouter() {
		LogManager.logDetail(getClass().getName() + " : ajout d'un objet de type : " + entityName());
		setModeCreation(true);

		displayGroup().insertObjectAtIndex(EOInstance.creer(getEdc(), currentType()), 0);		
		traitementsPourCreation();
		
		// changer cet état après l'insert pour que la méthode de délégation shouldChangeSelection fonctionne
		setModificationEnCours(true);
		// afficher les boutons de validation
		afficherBoutons(true);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		updaterDisplayGroups();
	}


	/**
	 * 
	 * @param group
	 * @param value
	 * @param eo
	 * @param key
	 */
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("cInstance")) {
			if (modeCreation()) {
				// vérifier si ce code est déjà utilisé et remettre à nul. On regarde sur tous les objets car on a un filtre sur le display group
				java.util.Enumeration e = displayGroup().allObjects().objectEnumerator();
				while (e.hasMoreElements()) {
					EOInstance instance = (EOInstance)e.nextElement();
					if (instance != eo && instance.cInstance().equals(value)) {
						EODialogs.runErrorDialog("Erreur","Ce code est déjà utilisé, veuillez en définir un autre");
						((EOInstance)eo).setCInstance(null);
						updaterDisplayGroups();
						break;
					}
				}
			}
		} 
	}

	// méthodes du controller DG
	/** on ne peut ajouter/modifier/supprimer que si on n'est pas en mode modification, que la fenetre n'est pas lockee et que
	 * l'agent est autorise a faire des modifications */
	public boolean modeSaisiePossible() {
		return !modificationEnCours() && conditionSurPageOK() && !estLocke();
	}
	public boolean peutValider() {
		return modificationEnCours() && currentInstance().typeInstance() != null;
	}
	// méthodes protégées
	/**  methode a surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lockee,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
		return agent.peutAdministrer();
	}

	protected void traitementsPourCreation() {
		currentInstance().initAvecTypeInstance(currentType());
	}

	protected boolean traitementsPourSuppression() {
		currentInstance().supprimerParametrages();
		try {
			editingContext().saveChanges();
		} catch (Exception exc) {
			EODialogs.runErrorDialog("Erreur",exc.getMessage());
			LogManager.logException(exc);
			editingContext().revert();
			return false;
		}
		currentInstance().supprimerRelations();
		displayGroup().deleteSelection();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette instance ? Toutes les données associées vont être modifiées, le traitement peut être long";
	}

	/**
	 * 
	 */
	protected NSArray<EOInstance> fetcherObjets() {
		NSArray<EOInstance> instances = SuperFinder.rechercherEntite(getEdc(), EOInstance.ENTITY_NAME);
		return instances;
	}

	/**
	 * 
	 */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cInstance",EOSortOrdering.CompareAscending)));
		super.parametrerDisplayGroup();
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOInstance currentInstance() {
		return (EOInstance)displayGroup().selectedObject();
	}
}

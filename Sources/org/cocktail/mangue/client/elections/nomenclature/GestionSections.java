/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementSection;
import org.cocktail.mangue.modele.grhum.elections.EOSectionElective;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSectionElective;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;


public class GestionSections extends PageAvecDisplayGroupDetail {
	//	 méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals(EOSectionElective.C_SECTION_ELECTIVE_KEY)) {
			if (modeCreation()) {
		 		// vérifier si ce code est déjà utilisé et remettre à nul.
		 		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		 		while (e.hasMoreElements()) {
		 			EOSectionElective section = (EOSectionElective)e.nextElement();
		 			if (section != eo && section.cSectionElective().equals(value)) {
		 				EODialogs.runErrorDialog("Erreur","Ce code est déjà utilisé, veuillez en définir un autre");
		 				((EOSectionElective)eo).setCSectionElective(null);
		 				updaterDisplayGroups();
		 				break;
		 			}
		 		}
	 		}
	 	} 
	}
	// actions
	public void ajouterDetail() {
		UtilitairesDialogue.afficherDialogue(this, EOCnu.ENTITY_NAME, "getCnus", true,true,null,false,false);
	}
	public void imprimer() {
		try {
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerSections",null,null,"SectionsElectives" ,"Impression des sections");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}

	public void getCnus(NSNotification aNotif) {
		if (aNotif.object() != null) {
			NSArray cnus = (NSArray)aNotif.object();
			java.util.Enumeration e = cnus.objectEnumerator();
			while (e.hasMoreElements()) {
				EOCnu cnu = (EOCnu)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)e.nextElement(),editingContext());
				NSArray cnusExistantes = (NSArray)displayGroupDetail.allObjects().valueForKey(EORegroupementSection.CNU_KEY);
				if (cnusExistantes.containsObject(cnu) == false) {
					displayGroupDetail.insert();
					currentRegroupement().initAvecSectionEtCnu(currentSection(),cnu);
					updaterDisplayGroups();
				}
			}
		}
	}
	// méthodes du controller DG
	public boolean peutImprimer() {
		return !modificationEnCours() && displayGroup().displayedObjects().count() > 0;
	}
	public boolean peutValider() {
		return super.peutValider() && currentSection().cSectionElective() != null && 
			currentSection().lcSectionElective() != null && currentSection().llSectionElective() != null;
	}
	// méthodes protégées
	protected NSArray fetcherDetails() {
		return EORegroupementSection.rechercherRegroupementsPourSection(editingContext(),currentSection());
	}
	protected boolean traitementsPourSuppressionDetail() {
		currentRegroupement().supprimerRelations();
		displayGroupDetail.deleteSelection();
		return true;
	}
	protected void traitementsApresSuppressionDetail() {}
	protected String messageConfirmationDestructionDetail() {
		return "Voulez-vous vraiment supprimer cette cnu ?";
	}
	protected void traitementsPourCreation() {
	}
	protected boolean traitementsPourSuppression() {
		java.util.Enumeration e = displayGroupDetail.displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EORegroupementSection regroupement = (EORegroupementSection)e.nextElement();
			regroupement.supprimerRelations();
			editingContext().deleteObject(regroupement);
		}
		currentSection().supprimerParametragesEtElecteurs();
		try { // 31/03/2010
			editingContext().saveChanges();
		} catch (Exception exc) {
			EODialogs.runErrorDialog("Erreur", exc.getMessage());
			LogManager.logException(exc);
			editingContext().revert();
			return false;
		}
		displayGroup().deleteSelection();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette section ? Toutes les données associées vont être modifiées, le traitement peut être long";
	}
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(), EOSectionElective.ENTITY_NAME);
	}
	
	/**
	 * 
	 */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey(EOSectionElective.C_SECTION_ELECTIVE_KEY, EOSortOrdering.CompareAscending)));
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EORegroupementSection.CNU_KEY+"."+EOCnu.CODE_KEY, EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EORegroupementSection.CNU_KEY+"."+EOCnu.CODE_SOUS_SECTION_KEY, EOSortOrdering.CompareAscending));
		displayGroupDetail.setSortOrderings(sorts);
	}
	
	/**  methode a surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lockee,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutAdministrer();
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		if (!modeCreation()) {
			invaliderInstances();
		}
		return true;
	}
	protected  void terminer() {
	}
	// méthodes privées
	private EOSectionElective currentSection() {
		return (EOSectionElective)displayGroup().selectedObject();
	}
	private EORegroupementSection currentRegroupement() {
		return (EORegroupementSection)displayGroupDetail.selectedObject();
	}
	private  void invaliderInstances() {
		NSArray<EOParamSectionElective> params = EOParamSectionElective.rechercherParametragesPourSection(editingContext(), currentSection());
		for (EOParamSectionElective param : params) {
			param.instance().setEstEditionCoherente(false);
		}
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.mangue.client.select.DiplomeSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionDiplome;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class GestionInclusionsDiplome extends PageInclusion {

	/**
	 * Changement de college
	 */
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {

		if (aGroup == displayGroup()) {
			if (currentCollege() == null)
				displayGroupDetail.setObjectArray(new NSArray<EOInclusionDiplome>());
			else
				displayGroupDetail.setObjectArray(EOInclusionDiplome.find(getEdc(), currentCollege()));			
		}
	}

	/**
	 * 
	 */
	public void ajouterDiplome() {

		try {
			NSArray<EODiplomes> diplomes = DiplomeSelectCtrl.sharedInstance(getEdc()).getDiplomes();
			if (diplomes != null && diplomes.size() > 0) {
				for (EODiplomes myDiplome : diplomes) {

					NSArray<EODiplomes> diplomesExistants = (NSArray<EODiplomes>)displayGroupDetail.allObjects().valueForKey(EOInclusionDiplome.DIPLOME_KEY);
					if (diplomesExistants.containsObject(myDiplome) == false) {
						EOInclusionDiplome.creer(getEdc(), currentCollege(), myDiplome);
					}
				}
				
				getEdc().saveChanges();
				displayGroupDetail.setObjectArray(EOInclusionDiplome.find(getEdc(), currentCollege()));			
				updaterDisplayGroups();
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	public void supprimerDiplome() {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer ce diplôme ?","Oui","Non")) {

			try {

				for (EOInclusionDiplome myInclusion : (NSArray<EOInclusionDiplome>)displayGroupDetail.selectedObjects()) {
					getEdc().deleteObject(myInclusion);
				}

				getEdc().saveChanges();
				displayGroupDetail.deleteSelection();
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
	public boolean peutAjouterDiplome() {
		return currentCollege() != null;
	}
	public boolean peutSupprimerDiplome() {
		return currentInclusion() != null;
	}

	protected void terminer() {
	}
	protected String nomEntite() {
		return EOInclusionDiplome.ENTITY_NAME;
	}
	protected boolean traitementsPourSuppressionDetail() {
		currentInclusion().supprimerRelations();
		displayGroupDetail.deleteSelection();
		return true;
	}
	protected void parametrerDisplayGroup() {
		displayGroupDetail.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("diplome.libelleLong",EOSortOrdering.CompareAscending)));
	}
	private EOInclusionDiplome currentInclusion() {
		return (EOInclusionDiplome)displayGroupDetail.selectedObject();
	}

	@Override
	public void ajouterDetail() {
		// TODO Auto-generated method stub

	}
}
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOTypeInstance;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EOAssociation;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Composant comportant un popup pour selectionner le type d'instance et un display group pour afficher la liste des colleges
 * Poste une notification pour signaler qu'un college a ete selectionne (avec comme objet la globalID du college selectionne ou null)
 * Est informe que des modifications ont lieu dans d'autres composants via des notifications
 * Reçoit des notifications pour la synchronisation des colleges et des types d'instance
 *
 */
public class ComposantPourCollege extends ModelePage {
	public static String SELECTION_COLLEGE_INCLUSION = "SelectionCollegeInclusion";
	public EODisplayGroup displayGroupTypeInstance;
	private EOTypeInstance currentType;
	private boolean estLocke;
	
	/** pour le chargement de l'archive */
	public void init() {
		EOArchive.loadArchiveNamed("ComposantPourCollege",this,"org.cocktail.mangue.client.elections.nomenclature.interfaces",this.disposableRegistry());
		preparerFenetre();
	}
	// accesseurs
	public String libelleCurrentType() {
		if (currentType != null) {
			return currentType.lcTypeInstance();
		} else {
			return null;
		}
	}
	public void setLibelleCurrentType(String type) {
		if (type == null) {
			this.currentType = null;
		} else {
			java.util.Enumeration e = displayGroupTypeInstance.displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOTypeInstance typeInstance = (EOTypeInstance)e.nextElement();
				if (typeInstance.lcTypeInstance().equals(type)) {
					this.currentType = typeInstance;
					break;
				}
			}
		}
		displayGroupTypeInstance.selectObject(currentType);
		installerFiltre();
	}
	public EOTypeInstance currentType() {
		return currentType;
	}
	   // délégation du display group   
    public boolean displayGroupShouldChangeSelection(EODisplayGroup group,NSArray newIndexes) {
    		return !estLocke;
    }
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroupTypeInstance) {
			this.currentType = (EOTypeInstance)displayGroupTypeInstance.selectedObject();
			installerFiltre();
		} else if (aGroup == displayGroup()) {
			EOGlobalID globalIDCollege = null;
			if (currentCollege() != null) {
				globalIDCollege = editingContext().globalIDForObject(currentCollege());
			}
			NSNotificationCenter.defaultCenter().postNotification(SELECTION_COLLEGE_INCLUSION,globalIDCollege);
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
    // Notifications
    public void synchroniserTypeInstance(NSNotification aNotif) {
    		EOTypeInstance type = currentType();
    		displayGroupTypeInstance.setObjectArray(SuperFinder.rechercherEntiteAvecRefresh(editingContext(),"TypeInstance"));
		if (displayGroupTypeInstance.allObjects().containsObject(type)) { 
			displayGroupTypeInstance.selectObject(type);
		} else {
			displayGroupTypeInstance.selectObject(null);
		}
		displayGroupTypeInstance.updateDisplayedObjects();
    }
    //  LOCK ECRAN
	public void lockSaisie(NSNotification aNotif) {
		estLocke = true;
		updaterDisplayGroups();
	} 
	// DELOCK ECRAN
	public void unlockSaisie(NSNotification aNotif) {
		estLocke = false;
		updaterDisplayGroups();
	} 
 	// méthodes du controller DG
    /** on ne peut ajouter/modifier/supprimer que les donn&eacute;es li&eacute;es aux types d'instances locaux */
	public boolean peutSelectionner() {
		return !estLocke;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		displayGroupTypeInstance.setSelectsFirstObjectAfterFetch(true);
		super.preparerFenetre();
		loadNotifications();
		estLocke = false;
		displayGroupTypeInstance.setObjectArray(SuperFinder.rechercherEntiteAvecRefresh(editingContext(),"TypeInstance"));
		displayGroupTypeInstance.updateDisplayedObjects();
		// 19/11/2010 - Raffraichissement des associations
		raffraichirAssociations();
		updaterDisplayGroups();
	}
	protected void loadNotifications() {
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniserTypeInstance", new Class[] { NSNotification.class }), GestionTypesElection.SYNCHRONISER_TYPE_ELECTION, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("lockSaisie", new Class[] { NSNotification.class }), ModelePage.LOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("unlockSaisie", new Class[] { NSNotification.class }), ModelePage.DELOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniser", new Class[] {NSNotification.class}), ModelePage.SYNCHRONISER,null);
	}
	protected void traitementsPourCreation() {
		// pas de creation
	}
	protected void parametrerDisplayGroup() {
		displayGroupTypeInstance.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cTypeInstance",EOSortOrdering.CompareAscending)));
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("numElcOrdre",EOSortOrdering.CompareAscending)));
		updaterDisplayGroups();
	}
	
	/**
	 * 
	 */
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(), EOCollege.ENTITY_NAME);
	}
	protected void installerFiltre() {
		displayGroupTypeInstance.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cTypeInstance",EOSortOrdering.CompareAscending)));
		if (currentType() == null) {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("typeInstance = nil",null));
		} else {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("typeInstance = %@",new NSArray(currentType())));
		}
		updaterDisplayGroups();
	
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected void afficherExceptionValidation(String message) {
		// pas de validation
	}
	protected String messageConfirmationDestruction() {
		// pas de destruction
		return "";
	}
	protected boolean traitementsAvantValidation() {
		return true;		// pas de validation
	}
	protected void traitementsApresValidation() {
		// pas de validation
	}
	protected boolean traitementsPourSuppression() {
		// pas de suppression
		return true;
	}
	protected void updaterDisplayGroups() {
		controllerDisplayGroup().redisplay();
		displayGroup().updateDisplayedObjects();
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOCollege currentCollege() {
		return (EOCollege)displayGroup().selectedObject();
	}
	private void raffraichirAssociations() {
		if (controllerDisplayGroup() != null) {
			java.util.Enumeration e = controllerDisplayGroup().observingAssociations().objectEnumerator();
			while (e.hasMoreElements()) {
				EOAssociation association = (EOAssociation)e.nextElement();
				association.subjectChanged();
			}
		}
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.mangue.client.select.CategorieSelectCtrl;
import org.cocktail.mangue.client.select.TypeContratListeSelectCtrl;
import org.cocktail.mangue.client.select.TypeContratArbreSelectCtrl;
import org.cocktail.mangue.client.select.elections.GroupeExclusionSelectCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.elections.EOGroupeExclusion;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionContrat;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** Gestion des inclusions selon les types de contrat de travail. Un type de contrat deja associees a un autre college du meme type d'instance n'est pas ajoute */

public class GestionInclusionsTypeContrat extends PageInclusionSituation {
	
	/**
	 * 
	 */
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {

		if (aGroup == displayGroup()) {
			if (currentCollege() == null)
				displayGroupDetail.setObjectArray(new NSArray<EOInclusionContrat>());
			else
				displayGroupDetail.setObjectArray(EOInclusionContrat.findContrats(getEdc(), currentCollege()));			
		}
		if (aGroup == displayGroupDetail) {
			if (currentDetail() == null)
				displayGroupGroupeExclusion.setObjectArray(new NSArray<EOInclusionContrat>());
			else
				displayGroupGroupeExclusion.setObjectArray(EOInclusionContrat.findExclusions(getEdc(), currentCollege(), ((EOInclusionContrat)currentDetail()).typeContratTravail()));

		}
		if (aGroup == displayGroupGroupeExclusion) {
			if (currentGroupe() == null)
				displayGroupCategorie.setObjectArray(new NSArray<EOInclusionContrat>());
			else
				displayGroupCategorie.setObjectArray(EOInclusionContrat.findCategories(getEdc(), currentCollege(), ((EOInclusionContrat)currentDetail()).typeContratTravail(), currentGroupe()));			
		}


	}

	/**
	 * 
	 */
	public void ajouterTypeContrat() {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		if (EOGrhumParametres.isGestionHu() == false) {
			qualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));
		}		
		qualifiers.addObject(EOTypeContratTravail.getQualifierVacataires(true));
		
		NSArray<INomenclature> typesContrat = new NSArray<INomenclature>(TypeContratListeSelectCtrl.sharedInstance(getEdc()).getObject(DateCtrl.today(), new EOAndQualifier(qualifiers), false));
		if (typesContrat != null && typesContrat.size() > 0) {
			
			for (INomenclature myTypeContrat : typesContrat) {
				
				NSArray<EOInclusionContrat> inclusions = null;

				if (currentCollege().typeInstance().estTypeJuridDiscHu() == false) {
					inclusions = EOInclusionContrat.rechercherInclusionsPourTypeInstanceEtTypeContrat(getEdc(), currentCollege().typeInstance(), (EOTypeContratTravail)myTypeContrat);
				}
				if (inclusions !=null && inclusions.count() > 0) {
					EODialogs.runInformationDialog("Attention","Le type de contrat de travail : " + myTypeContrat.libelleLong() + " est déjà associé à un autre collège relevant du même type d'instance. Il ne sera pas ajouté");
				} else {
					// le display group detail contient directement les corps
					if (displayGroupDetail.allObjects().containsObject(myTypeContrat) == false) {
						EOInclusionContrat.creer(getEdc(), currentCollege(), (EOTypeContratTravail)myTypeContrat, null, null);
					}
				}
			}
			
			getEdc().saveChanges();
			displayGroupDetail.setObjectArray(EOInclusionContrat.findContrats(getEdc(), currentCollege()));
			updaterDisplayGroups();
		}

	}
	
	protected EOInclusionContrat currentDetail() {
		return (EOInclusionContrat)displayGroupDetail.selectedObject();
	}
	protected EOGroupeExclusion currentGroupe() {
		if (displayGroupGroupeExclusion.selectedObject() == null)
			return null;
		return ((EOInclusionContrat)displayGroupGroupeExclusion.selectedObject()).groupeExclusion();
	}
	protected EOCategorie currentCategorie() {
		if (displayGroupCategorie.selectedObject() == null)
			return null;
		return ((EOInclusionContrat)displayGroupCategorie.selectedObject()).categorie();
	}

	/**
	 * 
	 */
	public void ajouterGroupeExclusion() {

		EOGroupeExclusion exclusion = GroupeExclusionSelectCtrl.sharedInstance(getEdc()).getSelection();
		if (exclusion != null) {
			try {

				boolean ajouterPourTous = EODialogs.runConfirmOperationDialog("Attention",messagePourAjoutGroupeExclusion(exclusion),"Oui","Non");

				NSArray<EOInclusionContrat> inclusions = new NSArray<EOInclusionContrat>();
				if (ajouterPourTous)
					inclusions = (NSArray<EOInclusionContrat>) displayGroupDetail.allObjects();
				else
					inclusions = new NSArray(currentDetail());

				for (EOInclusionContrat myInclusion : inclusions) {

					NSArray<EOInclusionContrat> exclusions = EOInclusionContrat.findExclusions(getEdc(), currentCollege(), myInclusion.typeContratTravail());
					if (exclusions == null || exclusions.size() == 0 || !((NSArray)exclusions.valueForKey(EOInclusionContrat.GROUPE_EXCLUSION_KEY)).containsObject(exclusion)) {
						EOInclusionContrat.creer(getEdc(), currentCollege(), myInclusion.typeContratTravail(), exclusion, null);
					}
				}

				getEdc().saveChanges();
				displayGroupGroupeExclusion.setObjectArray(EOInclusionContrat.findExclusions(getEdc(), currentCollege(), ((EOInclusionContrat)currentDetail()).typeContratTravail()));
				updaterDisplayGroups();
				raffraichirAssociations();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public void ajouterCategorie() {

		EOCategorie categorie = CategorieSelectCtrl.sharedInstance(getEdc()).getCategorie();
		if (categorie != null) {
			try {
				NSArray<EOCategorie> listeCategories = (NSArray<EOCategorie>)displayGroupGroupeExclusion.displayedObjects().valueForKey(EOInclusionContrat.CATEGORIE_KEY);
				if (listeCategories.containsObject(categorie) == false) {
					EOInclusionContrat.creer(getEdc(), currentCollege(), ((EOInclusionContrat)currentDetail()).typeContratTravail(), currentGroupe(), categorie);
					getEdc().saveChanges();
					displayGroupCategorie.setObjectArray(EOInclusionContrat.findCategories(getEdc(), currentCollege(), ((EOInclusionContrat)currentDetail()).typeContratTravail(), currentGroupe()));
					updaterDisplayGroups();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public void supprimerTypeContrat() {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer ce type de contrat ?","Oui","Non")) {

			try {

				for (EOInclusionContrat myInclusion : (NSArray<EOInclusionContrat>)displayGroupDetail.selectedObjects()) {

					NSArray<EOInclusionContrat> groupes = EOInclusionContrat.findExclusions(getEdc(), currentCollege(), myInclusion.typeContratTravail());

					for (EOInclusionContrat groupe : groupes) {

						getEdc().deleteObject(groupe);

					}

					getEdc().deleteObject(myInclusion);
					
				}

				getEdc().saveChanges();
				displayGroupDetail.deleteSelection();
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * 
	 */
	public void supprimerGroupeExclusion() {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer ce groupe d'exclusion ?","Oui","Non")) {

			try {

				for (EOInclusionContrat myInclusion : (NSArray<EOInclusionContrat>)displayGroupGroupeExclusion.selectedObjects()) {
					getEdc().deleteObject(myInclusion);
				}

				getEdc().saveChanges();
				displayGroupGroupeExclusion.deleteSelection();
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public void supprimerCategorie() {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer cette catégorie ?","Oui","Non")) {

			try {

				for (EOInclusionContrat myInclusion : (NSArray<EOInclusionContrat>)displayGroupCategorie.selectedObjects()) {
					getEdc().deleteObject(myInclusion);
				}

				getEdc().saveChanges();
				displayGroupCategorie.deleteSelection();
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
	
	public boolean peutAjouterTypeContrat() {
		return currentCollege() != null;
	}
	public boolean peutSupprimerTypeContrat() {
		return currentDetail() != null;
	}
	public boolean peutSupprimerExclusion() {
		return currentGroupe() != null;
	}


	protected void terminer() {
	}
	protected String nomEntite() {
		return EOInclusionContrat.ENTITY_NAME;
	}
	protected String relationPourEntitePrincipale() {
		return EOInclusionContrat.TYPE_CONTRAT_TRAVAIL_KEY;
	}
	protected String cleTriDetail() {
		return EOTypeContratTravail.CODE_KEY;
	}
	protected String messagePourAjoutGroupeExclusion(EOGroupeExclusion groupeExclusion) {
		return "Voulez-vous répéter le groupe d’exclusion " + groupeExclusion.cGroupeExclusion() + " pour tous les types de contrats ? ";
	}

	@Override
	public void ajouterDetail() {
		// TODO Auto-generated method stub
		
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.mangue.client.select.CategorieSelectCtrl;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.client.select.elections.GroupeExclusionSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.elections.EOGroupeExclusion;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionCorps;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** Gestion des inclusions selon les corps. Un corps deja associe a un autre college du meme type d'instance n'est pas ajoute */
public class GestionInclusionsCorps extends PageInclusionSituation {

	//private boolean gereHospitaloUniv, gereENS;
	private CorpsSelectCtrl myCorpsSelectCtrl;

	/**
	 * 
	 */
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {

		if (aGroup == displayGroup()) {
			if (currentCollege() == null)
				displayGroupDetail.setObjectArray(new NSArray<EOInclusionCorps>());
			else
				displayGroupDetail.setObjectArray(EOInclusionCorps.findCorps(getEdc(), currentCollege()));			
		}
		if (aGroup == displayGroupDetail) {
			if (currentDetail() == null)
				displayGroupGroupeExclusion.setObjectArray(new NSArray<EOInclusionCorps>());
			else
				displayGroupGroupeExclusion.setObjectArray(EOInclusionCorps.findExclusions(getEdc(), currentCollege(), ((EOInclusionCorps)currentDetail()).corps()));

		}
		if (aGroup == displayGroupGroupeExclusion) {
			if (currentGroupe() == null)
				displayGroupCategorie.setObjectArray(new NSArray<EOInclusionCorps>());
			else
				displayGroupCategorie.setObjectArray(EOInclusionCorps.findCategories(getEdc(), currentCollege(), ((EOInclusionCorps)currentDetail()).corps(), currentGroupe()));			
		}
	}


	/**
	 * 
	 */
	public void ajouterCorps() {

		try {
			
			if (myCorpsSelectCtrl == null)
				myCorpsSelectCtrl = new CorpsSelectCtrl(getEdc());

			NSMutableArray qualifiers = new NSMutableArray();

			if (!EOGrhumParametres.isGestionHu()) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.TO_TYPE_POPULATION_KEY+"."+EOTypePopulation.TEM_HOSPITALIER_KEY +  "=%@", new NSArray(CocktailConstantes.FAUX)));
			}
			if (!EOGrhumParametres.isGestionEns()) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCorps.TO_TYPE_POPULATION_KEY+"."+EOTypePopulation.CODE_KEY +  "!=%@", new NSArray(CocktailConstantes.FAUX)));
			}

			NSArray<EOCorps> listeCorps = myCorpsSelectCtrl.getArrayCorpsPourQualifier(new EOAndQualifier(qualifiers));

			if (listeCorps != null && listeCorps.size() > 0) {

				for (EOCorps corps : listeCorps) {

					NSArray<EOInclusionCorps> inclusions = null;
					if (currentCollege().typeInstance().estTypeJuridDiscHu() == false) {
						inclusions = EOInclusionCorps.rechercherInclusionsPourTypeInstanceEtCorps(getEdc(), currentCollege().typeInstance(), corps);
					}
					if (inclusions != null && inclusions.count() > 0) {
						EODialogs.runInformationDialog("Attention","Le corps : " + corps.llCorps() + " est déjà associé à un autre collège relevant du même type d'instance. Il ne sera pas ajouté !");
					} else {
						// le display group detail contient directement les corps
						if (displayGroupDetail.allObjects().containsObject(corps) == false) {
							EOInclusionCorps.creer(getEdc(), currentCollege(), corps, null, null);
						}
					}
				}

				getEdc().saveChanges();
				displayGroupDetail.setObjectArray(EOInclusionCorps.findCorps(getEdc(), currentCollege()));
				updaterDisplayGroups();

			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void supprimerCorps() {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer ce corps ?","Oui","Non")) {

			try {

				for (EOInclusionCorps myInclusion : (NSArray<EOInclusionCorps>)displayGroupDetail.selectedObjects()) {

					NSArray<EOInclusionCorps> groupes = EOInclusionCorps.find(getEdc(), currentCollege(), myInclusion.corps(), null, null);

					for (EOInclusionCorps groupe : groupes) {

						getEdc().deleteObject(groupe);

					}

				}

				getEdc().saveChanges();
				displayGroupDetail.deleteSelection();
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * 
	 */
	public void supprimerGroupeExclusion() {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer ce groupe d'exclusion ?","Oui","Non")) {

			try {

				for (EOInclusionCorps myInclusion : (NSArray<EOInclusionCorps>)displayGroupGroupeExclusion.selectedObjects()) {
					getEdc().deleteObject(myInclusion);
				}

				getEdc().saveChanges();
				displayGroupGroupeExclusion.deleteSelection();
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public void supprimerCategorie() {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous supprimer cette catégorie ?","Oui","Non")) {

			try {

				for (EOInclusionCorps myInclusion : (NSArray<EOInclusionCorps>)displayGroupCategorie.selectedObjects()) {
					getEdc().deleteObject(myInclusion);
				}

				getEdc().saveChanges();
				displayGroupCategorie.deleteSelection();
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
	}


	public boolean peutAjouterCorps() {
		return currentCollege() != null;
	}
	public boolean peutAjouterExclusion() {
		return currentDetail() != null;
	}
	public boolean peutAjouterCategorie() {
		return currentGroupe() != null;
	}
	public boolean peutSupprimerCorps() {
		return currentDetail() != null;
	}
	public boolean peutSupprimerExclusion() {
		return currentGroupe() != null;
	}

	/**
	 * 
	 */
	public void ajouterGroupeExclusion() {

		EOGroupeExclusion exclusion = GroupeExclusionSelectCtrl.sharedInstance(getEdc()).getSelection();
		if (exclusion != null) {
			try {

				boolean ajouterPourTous = EODialogs.runConfirmOperationDialog("Attention",messagePourAjoutGroupeExclusion(exclusion),"Oui","Non");

				NSArray<EOInclusionCorps> inclusions = new NSArray<EOInclusionCorps>();
				if (ajouterPourTous)
					inclusions = (NSArray<EOInclusionCorps>) displayGroupDetail.allObjects();
				else
					inclusions = new NSArray(currentDetail());

				for (EOInclusionCorps myInclusion : inclusions) {

					NSArray<EOInclusionCorps> exclusions = EOInclusionCorps.findExclusions(getEdc(), currentCollege(), myInclusion.corps());
					if (exclusions == null || exclusions.size() == 0 || !((NSArray)exclusions.valueForKey(EOInclusionCorps.GROUPE_EXCLUSION_KEY)).containsObject(exclusion))
						EOInclusionCorps.creer(getEdc(), currentCollege(), myInclusion.corps(), exclusion, null);
				}

				getEdc().saveChanges();
				displayGroupGroupeExclusion.setObjectArray(EOInclusionCorps.findExclusions(getEdc(), currentCollege(), ((EOInclusionCorps)currentDetail()).corps()));
				updaterDisplayGroups();
				raffraichirAssociations();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public void ajouterCategorie() {

		EOCategorie categorie = CategorieSelectCtrl.sharedInstance(getEdc()).getCategorie();
		if (categorie != null) {
			try {
				NSArray<EOCategorie> listeCategories = (NSArray<EOCategorie>)displayGroupGroupeExclusion.displayedObjects().valueForKey(EOInclusionCorps.CATEGORIE_KEY);
				if (listeCategories.containsObject(categorie) == false) {
					EOInclusionCorps.creer(getEdc(), currentCollege(), ((EOInclusionCorps)currentDetail()).corps(), currentGroupe(), categorie);
					getEdc().saveChanges();
					displayGroupCategorie.setObjectArray(EOInclusionCorps.findCategories(getEdc(), currentCollege(), ((EOInclusionCorps)currentDetail()).corps(), currentGroupe()));
					updaterDisplayGroups();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	protected void terminer() {
	}
	protected String nomEntite() {
		return EOInclusionCorps.ENTITY_NAME;
	}
	protected String relationPourEntitePrincipale() {
		return EOInclusionCorps.CORPS_KEY;
	}
	protected String cleTriDetail() {
		return EOInclusionCorps.CORPS_KEY+"."+EOCorps.C_CORPS_KEY;
	}
	protected String messagePourAjoutGroupeExclusion(EOGroupeExclusion groupeExclusion) {
		return "Voulez-vous répéter le groupe d’exclusion " + groupeExclusion.cGroupeExclusion() + " pour tous les corps ? ";
	}

	@Override
	public void ajouterDetail() {
		// TODO Auto-generated method stub

	}

}

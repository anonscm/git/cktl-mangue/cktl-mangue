/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.elections.PagePourTypeElection;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCollege;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

public class GestionColleges extends PagePourTypeElection {

	private int oldNumCollege;

	
	/** Action pour ajouter : invoque traitementsPourCreation pour faire les traitements specifiques */
	public void ajouter() {
		LogManager.logDetail(getClass().getName() + " : ajout d'un objet de type : " + entityName());
		setModeCreation(true);

		displayGroup().insertObjectAtIndex(EOCollege.creer(getEdc(), currentType()), 0);		
		traitementsPourCreation();
		
		// changer cet état après l'insert pour que la méthode de délégation shouldChangeSelection fonctionne
		setModificationEnCours(true);
		// afficher les boutons de validation
		afficherBoutons(true);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		updaterDisplayGroups();
	}

	
	/**
	 * 
	 */
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (currentCollege() == null || modeCreation()) {
			oldNumCollege = 0;
		} else {
			oldNumCollege = currentCollege().numElcOrdre().intValue();
		}
		super.displayGroupDidChangeSelection(aGroup);
	}

	/**
	 * 
	 */
	public void imprimer() {
		try {
			Class[] classeParametres =  new Class[] {EOGlobalID.class};
			Object[] parametres = new Object[]{getEdc().globalIDForObject(currentCollege())};
			String nomFichier = "College_" + currentType().cTypeInstance() + "_" + currentCollege().numElcOrdre();
			UtilitairesImpression.imprimerAvecDialogue(getEdc(),"clientSideRequestImprimerCollege",classeParametres,parametres,nomFichier ,"Impression du collège " + currentCollege().llCollege());
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	// méthodes du controller DG
	public boolean peutImprimer() {
		return !modificationEnCours() && currentCollege() != null;
	}
	public boolean peutValider() {
		return modificationEnCours() && currentCollege().llCollege() != null && currentCollege().lcCollege() != null &&
				currentCollege().numElcOrdre() != null && currentCollege().typeInstance() != null; 
	}

	// Méthodes protégées
	protected void traitementsPourCreation() {
		int lastNum = displayGroup().displayedObjects().count();
		currentCollege().initAvecTypeInstance(currentType(),new Integer(lastNum));
	}
	protected boolean traitementsPourSuppression() {
		int positionCourante = currentCollege().numElcOrdre().intValue() - 1;

		currentCollege().supprimerParametrages();
		try {
			getEdc().saveChanges();
		} catch (Exception exc) {
			EODialogs.runErrorDialog("Erreur",exc.getMessage());
			LogManager.logException(exc);
			getEdc().revert();
			return false;
		}
		currentCollege().supprimerRelations();
		displayGroup().deleteSelection();
		LogManager.logDetail("suppression collège");
		if (positionCourante < displayGroup().displayedObjects().count()) {
			renumeroterColleges(positionCourante);
		}
		LogManager.logDetail("fin suppression collège");
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce collège ? Toutes les données associées vont être modifiées, le traitement peut être long";
	}

	/**  methode a surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lockee,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
		return agent.peutAdministrer();
	}

	/**
	 * 
	 */
	protected NSArray<EOCollege> fetcherObjets() {
		return SuperFinder.rechercherEntite(getEdc(), EOCollege.ENTITY_NAME);
	}

	/**
	 * 
	 */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("numElcOrdre",EOSortOrdering.CompareAscending)));
		super.parametrerDisplayGroup();
	}
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) {
			renumeroterColleges(-1);
			if (!modeCreation()) {
				invaliderInstances();
			}
			return true;
		} else {
			return false;
		}
	}
	protected  void terminer() {
	}

	/**
	 * 
	 * @return
	 */
	private EOCollege currentCollege() {
		return (EOCollege)displayGroup().selectedObject();
	}

	/**
	 * 
	 * @param positionCourante
	 */
	private void renumeroterColleges(int positionCourante) {
		for (int i = 0; i < displayGroup().displayedObjects().count();i++) {
			EOCollege college = (EOCollege)displayGroup().displayedObjects().objectAtIndex(i);
			if (positionCourante >= 0) {	// destruction
				if (i >= positionCourante) {	
					// décaler vers la valeur précédente tous les éléments suivants
					college.setNumElcOrdre(new Integer(college.numElcOrdre().intValue() - 1));
				}
			} else {
				int nouvellePosition = -1, anciennePosition = oldNumCollege - 1;
				if (currentCollege() != null) {
					nouvellePosition = currentCollege().numElcOrdre().intValue() - 1;
				}
				if (anciennePosition < 0) { // création
					if (i >= nouvellePosition && college != currentCollege()) {
						// décaler vers la valeur suivante tous les éléments suivants
						college.setNumElcOrdre(new Integer(college.numElcOrdre().intValue() + 1));
					}
				} else if (nouvellePosition > anciennePosition) {
					// décaler vers la valeur précédente tous les éléments compris entre ces deux positions
					if (i >= anciennePosition && i <= nouvellePosition && college != currentCollege()) {
						college.setNumElcOrdre(new Integer(college.numElcOrdre().intValue() - 1));
					}
				} else if (nouvellePosition < anciennePosition) {
					// décaler vers la valeur suivante tous les éléments compris entre ces deux positions
					if (i >= nouvellePosition && i < anciennePosition && college != currentCollege()) {
						college.setNumElcOrdre(new Integer(college.numElcOrdre().intValue() + 1));
					}
				}
			}
		}
	}

	/**
	 * 
	 */
	private  void invaliderInstances() {

		try {
			
			NSArray<EOParamCollege> params = EOParamCollege.rechercherParametragesPourCollege(getEdc(), currentCollege());

			for (EOParamCollege myParam : params) {
				myParam.instance().setEstEditionCoherente(false);
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}

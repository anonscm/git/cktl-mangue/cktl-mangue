/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.modele.Inclusion;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Modelise une page pour les inclusions avec une vue contenant un controleur de type ComposantPourCollege
 * 	Le Display Group principal contient le college selectionne
 * 	On ne peut pas modifier les inclusions pour les types d'instances non locaux
 */ 

public abstract class PageInclusion extends PageAvecDisplayGroupDetail {

	public EOView vueCollege;

	public EOEditingContext getEdc() {
		return ((ApplicationClient)EOApplication.sharedApplication()).getAppEditingContext();
	}
	
	// accesseurs
	public EOCollege currentCollege() {
		return (EOCollege)displayGroup().selectedObject();
	}
	// Notifications
	public void getCollege(NSNotification aNotif) {

		if (aNotif.object() != null) {
			EOCollege currentCollege = (EOCollege)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(), getEdc());
			displayGroup().setObjectArray(new NSArray(currentCollege));
			displayGroup().selectObject(currentCollege);
		} else {
			displayGroup().setObjectArray(null);
			displayGroup().setSelectedObject(null);	
		}
		updaterDisplayGroups();
	}
	// méthodes du controller DG
	public boolean modeSaisiePossible() {
		if (super.modeSaisiePossible()) {
			return currentCollege() != null && currentCollege().typeInstance().estLocal();
		} else {
			return false;
		}
	}
	// méthodes protégées
	/** retourne le nom d'entite geree par cette page */
	protected abstract String nomEntite();
	protected void preparerFenetre() {
		super.preparerFenetre();
		ComposantPourCollege controller = new ComposantPourCollege();
		controller.init();
		GraphicUtilities.swaperViewEtCentrer(vueCollege,controller.component());
		raffraichirAssociations();
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("getCollege", new Class[] { NSNotification.class }), ComposantPourCollege.SELECTION_COLLEGE_INCLUSION, null);
	}
	protected NSArray fetcherDetails() {
		return Inclusion.rechercherInclusionsPourEntiteEtCollege(getEdc(),nomEntite(),currentCollege());
	}
	protected void traitementsPourCreation() {
		// pas de création au niveau du DG principal
	}
	protected boolean traitementsPourSuppression() {
		// pas de suppression au niveau du DG principal
		return true;
	}
	protected String messageConfirmationDestruction() {
		// pas de suppression au niveau du DG principal
		return "";
	}
	protected boolean traitementsAvantValidation() {
		// Invalider toutes les élections liées au collège sélectionné
		if (!modeCreation()) {
			EOListeElecteur.invaliderInstances(getEdc(), "college", (EOGenericRecord)displayGroup().selectedObject());
		}
		return true;
	}
	protected void traitementsApresSuppressionDetail() {}
	protected String messageConfirmationDestructionDetail() {
		return "Voulez-vous vraiment supprimer cette inclusion ?";
	}
	protected NSArray fetcherObjets() {
		// pas de fetch direct
		return null;
	}
	/**  methode a surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lockee,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
		return agent.peutAdministrer();
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	
}

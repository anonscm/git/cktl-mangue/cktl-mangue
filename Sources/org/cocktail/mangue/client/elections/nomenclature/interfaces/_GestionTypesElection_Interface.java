// _GestionTypesElection_Interface.java
// Created on 3 août 2010 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.nomenclature.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _GestionTypesElection_Interface extends COFrame {

    /** Creates new form _GestionTypesElection_Interface */
    public _GestionTypesElection_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new org.cocktail.component.CODisplayGroup();
        listeAffichage = new org.cocktail.component.COTable();
        cOView1 = new org.cocktail.component.COView();
        jLabel3 = new javax.swing.JLabel();
        cOTextField4 = new org.cocktail.component.COTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cOTextField1 = new org.cocktail.component.COTextField();
        cOTextField5 = new org.cocktail.component.COTextField();
        vueBoutonsValidation = new org.cocktail.component.COView();
        cOButton9 = new org.cocktail.component.COButton();
        cOButton10 = new org.cocktail.component.COButton();
        cOCheckbox4 = new org.cocktail.component.COCheckbox();
        cOCheckbox2 = new org.cocktail.component.COCheckbox();
        cOCheckbox1 = new org.cocktail.component.COCheckbox();
        cOCheckbox3 = new org.cocktail.component.COCheckbox();
        cOCheckbox5 = new org.cocktail.component.COCheckbox();
        cOButton4 = new org.cocktail.component.COButton();
        vueBoutonsModification = new org.cocktail.component.COView();
        cOButton1 = new org.cocktail.component.COButton();
        cOButton2 = new org.cocktail.component.COButton();
        cOButton3 = new org.cocktail.component.COButton();

        displayGroup.setEntityName("TypeInstance");
        displayGroup.setHasDelegate(true);
        displayGroup.setIsMainDisplayGroupForController(true);

        setControllerClassName("org.cocktail.mangue.client.elections.nomenclature.GestionTypesElection");
        setSize(new java.awt.Dimension(758, 453));

        listeAffichage.setColumns(new Object[][] {{null,"cTypeInstance",new Integer(2),"Code",new Integer(0),new Integer(80),new Integer(1000),new Integer(38)},{null,"llTypeInstance",new Integer(2),"Libellé",new Integer(0),new Integer(607),new Integer(1000),new Integer(38)}});
        listeAffichage.setDisplayGroupForTable(displayGroup);

        cOView1.setIsBox(true);

        jLabel3.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel3.setText("Code");

        cOTextField4.setDisplayGroupForValue(displayGroup);
        cOTextField4.setEnabledMethod("modificationEnCours");
        cOTextField4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cOTextField4.setSupportsBackgroundColor(true);
        cOTextField4.setValueName("cTypeInstance");

        jLabel6.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel6.setText("Libellé court");

        jLabel5.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel5.setText("Libellé long");

        cOTextField1.setDisplayGroupForValue(displayGroup);
        cOTextField1.setEnabledMethod("modificationEnCours");
        cOTextField1.setSupportsBackgroundColor(true);
        cOTextField1.setValueName("lcTypeInstance");

        cOTextField5.setDisplayGroupForValue(displayGroup);
        cOTextField5.setEnabledMethod("modificationEnCours");
        cOTextField5.setSupportsBackgroundColor(true);
        cOTextField5.setValueName("llTypeInstance");

        cOButton9.setActionName("annuler");
        cOButton9.setBorderPainted(false);
        cOButton9.setEnabledMethod("modificationEnCours");
        cOButton9.setIconName("annuler16.gif");

        cOButton10.setActionName("valider");
        cOButton10.setBorderPainted(false);
        cOButton10.setEnabledMethod("peutValider");
        cOButton10.setIconName("valider16.gif");

        org.jdesktop.layout.GroupLayout vueBoutonsValidationLayout = new org.jdesktop.layout.GroupLayout(vueBoutonsValidation);
        vueBoutonsValidation.setLayout(vueBoutonsValidationLayout);
        vueBoutonsValidationLayout.setHorizontalGroup(
            vueBoutonsValidationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsValidationLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        vueBoutonsValidationLayout.setVerticalGroup(
            vueBoutonsValidationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsValidationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(cOButton10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        cOCheckbox4.setDisplayGroupForValue(displayGroup);
        cOCheckbox4.setEnabledMethod("modificationEnCours");
        cOCheckbox4.setText("Agent peut insérer des individus dans la liste électorale");
        cOCheckbox4.setValueName("peutInsererIndividus");

        cOCheckbox2.setDisplayGroupForValue(displayGroup);
        cOCheckbox2.setEnabledMethod("modificationEnCours");
        cOCheckbox2.setText("Vote autorisé au titre de plusieurs carrières");
        cOCheckbox2.setValueName("estDoubleCarriere");
        cOCheckbox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cOCheckbox2ActionPerformed(evt);
            }
        });

        cOCheckbox1.setDisplayGroupForValue(displayGroup);
        cOCheckbox1.setEnabledMethod("modificationEnCours");
        cOCheckbox1.setText("Rattachement de l'individu à l'affectation Principale");
        cOCheckbox1.setValueName("estRattachementAffectationPrincipale");

        cOCheckbox3.setDisplayGroupForValue(displayGroup);
        cOCheckbox3.setEnabledMethod("modificationEnCours");
        cOCheckbox3.setText("Type local");
        cOCheckbox3.setValueName("estLocal");

        cOCheckbox5.setDisplayGroupForValue(displayGroup);
        cOCheckbox5.setEnabledMethod("modificationEnCours");
        cOCheckbox5.setText("Sectorisé");
        cOCheckbox5.setValueName("estSectorise");
        cOCheckbox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cOCheckbox5ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout cOView1Layout = new org.jdesktop.layout.GroupLayout(cOView1);
        cOView1.setLayout(cOView1Layout);
        cOView1Layout.setHorizontalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView1Layout.createSequentialGroup()
                .addContainerGap()
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel3)
                    .add(jLabel6)
                    .add(jLabel5))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cOView1Layout.createSequentialGroup()
                        .add(cOCheckbox4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 256, Short.MAX_VALUE)
                        .add(vueBoutonsValidation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(cOView1Layout.createSequentialGroup()
                        .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(cOTextField5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 314, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 157, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cOTextField4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 77, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(cOCheckbox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cOCheckbox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cOView1Layout.createSequentialGroup()
                                .add(cOCheckbox3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(cOCheckbox5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(43, 43, 43)))))
                .addContainerGap())
        );
        cOView1Layout.setVerticalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView1Layout.createSequentialGroup()
                .add(15, 15, 15)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel3)
                    .add(cOTextField4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOCheckbox3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOCheckbox5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(8, 8, 8)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel6)
                    .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOCheckbox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(8, 8, 8)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel5)
                    .add(cOTextField5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOCheckbox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cOCheckbox4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(vueBoutonsValidation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cOButton4.setActionName("imprimer");
        cOButton4.setBorderPainted(false);
        cOButton4.setEnabledMethod("peutImprimer");
        cOButton4.setIconName("Imprimante.gif");

        cOButton1.setActionName("ajouter");
        cOButton1.setBorderPainted(false);
        cOButton1.setEnabledMethod("modeSaisiePossible");
        cOButton1.setIconName("ajouter16.gif");

        cOButton2.setActionName("modifier");
        cOButton2.setBorderPainted(false);
        cOButton2.setEnabledMethod("boutonModificationAutorise");
        cOButton2.setIconName("modifier16.gif");

        cOButton3.setActionName("supprimer");
        cOButton3.setBorderPainted(false);
        cOButton3.setEnabledMethod("boutonModificationAutorise");
        cOButton3.setIconName("supprimer16.gif");

        org.jdesktop.layout.GroupLayout vueBoutonsModificationLayout = new org.jdesktop.layout.GroupLayout(vueBoutonsModification);
        vueBoutonsModification.setLayout(vueBoutonsModificationLayout);
        vueBoutonsModificationLayout.setHorizontalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        vueBoutonsModificationLayout.setVerticalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsModificationLayout.createSequentialGroup()
                .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap(37, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cOView1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 693, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cOButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 35, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(20, 20, 20)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 222, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(cOButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(20, 20, 20)
                .add(cOView1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cOCheckbox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cOCheckbox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cOCheckbox2ActionPerformed

    private void cOCheckbox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cOCheckbox5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cOCheckbox5ActionPerformed
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COButton cOButton1;
    public org.cocktail.component.COButton cOButton10;
    public org.cocktail.component.COButton cOButton2;
    public org.cocktail.component.COButton cOButton3;
    public org.cocktail.component.COButton cOButton4;
    public org.cocktail.component.COButton cOButton9;
    public org.cocktail.component.COCheckbox cOCheckbox1;
    public org.cocktail.component.COCheckbox cOCheckbox2;
    public org.cocktail.component.COCheckbox cOCheckbox3;
    public org.cocktail.component.COCheckbox cOCheckbox4;
    public org.cocktail.component.COCheckbox cOCheckbox5;
    public org.cocktail.component.COTextField cOTextField1;
    public org.cocktail.component.COTextField cOTextField4;
    public org.cocktail.component.COTextField cOTextField5;
    private org.cocktail.component.COView cOView1;
    public org.cocktail.component.CODisplayGroup displayGroup;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    public org.cocktail.component.COTable listeAffichage;
    public org.cocktail.component.COView vueBoutonsModification;
    public org.cocktail.component.COView vueBoutonsValidation;
    // End of variables declaration//GEN-END:variables
                  

}

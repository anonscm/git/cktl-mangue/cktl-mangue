/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections;

import java.awt.Cursor;
import java.io.File;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.component.COButton;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

public class GestionElecteurs extends PageFinaleElection {

	public JComboBox popup;
	public COButton btnAjouter, btnReintegrer, btnExclure;

	private static int numImpression = 0;
	private String titreEdition;
	// Accesseurs
	public String titreEdition() {
		return titreEdition;
	}
	public void setTitreEdition(String titreEdition) {
		this.titreEdition = titreEdition;
	}

	public Integer numElecteurs() {
		if (displayGroup() != null) {
			return new Integer(displayGroup().displayedObjects().count());
		} else {
			return new Integer(0);
		}		
	}

	public boolean displayGroupShouldChangeSelection(EODisplayGroup group,NSArray newIndexes) {
		return true;
	}

	// Actions
	public void popupHasChanged() {
		if (popup == null) {
			return;
		}

		btnAjouter.setVisible(false);
		btnReintegrer.setVisible(false);
		btnExclure.setVisible(false);

		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		EOQualifier qualifier = null;
		switch(popup.getSelectedIndex()) {
		case ManGUEConstantes.RETENUS :	
			qualifier = EOQualifier.qualifierWithQualifierFormat(EOListeElecteur.TEM_EXCLU_MAN_KEY + " = 'N'", null);
			titreEdition = "Liste des électeurs";
			btnAjouter.setVisible(true);
			btnExclure.setVisible(true);
			break;
		case ManGUEConstantes.EXCLUS :
			qualifier = EOQualifier.qualifierWithQualifierFormat(EOListeElecteur.TEM_EXCLU_MAN_KEY + " = 'O' OR cTypeExclusion <> nil", null);
			titreEdition =  "Electeurs exclus";
			btnReintegrer.setVisible(true);
			break;
		case ManGUEConstantes.SANS_BUREAU_VOTE :
			qualifier = EOQualifier.qualifierWithQualifierFormat(EOListeElecteur.BUREAU_VOTE_KEY + " = nil",null);
			titreEdition =  "Electeurs sans bureau de vote";
			break;
		case ManGUEConstantes.MULTI_AFFECTATIONS :
			qualifier = EOQualifier.qualifierWithQualifierFormat(EOListeElecteur.TEM_AFFECTATIONS_MULTIPLES_KEY + " = 'O'",null);
			titreEdition =  "Electeurs avec affectations multiples";
			btnExclure.setVisible(true);
			break;
		case ManGUEConstantes.AJOUTES :
			qualifier = EOQualifier.qualifierWithQualifierFormat(EOListeElecteur.TEM_INS_MAN_KEY + " = 'O'",null);
			titreEdition =  "Electeurs ajoutés manuellement";
			btnAjouter.setVisible(true);
			break;
		case ManGUEConstantes.TOUS :
			titreEdition = "Tout type d'électeur";
			btnAjouter.setVisible(true);
			btnExclure.setVisible(true);
			break;
		}
		displayGroup().setQualifier(qualifier);
		updaterDisplayGroups();
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	public void ajouterElecteur() {
		LogManager.logDetail(getClass().getName() + " : ajouterElecteur");
		AjoutElecteurs controleur = new AjoutElecteurs(editingContext().globalIDForObject(currentInstance()));
		EOFrameController.runControllerInNewFrame(controleur, "Ajout d'électeurs");
	}
	public void reajouterElecteur() {
		LogManager.logDetail(getClass().getName() + " : reajouterElecteur");
		preparerPourModification();
		currentElecteur().setEstExclusManuellement(false);
		updaterDisplayGroups();
	}
	public void exclureElecteur() {
		LogManager.logDetail(getClass().getName() + " : exclureElecteur");
		preparerPourModification();

		for (EOListeElecteur electeur : (NSArray<EOListeElecteur>)displayGroup().selectedObjects()) {
			electeur.setEstExclusManuellement(true);
		}

		//		currentElecteur().setEstExclusManuellement(true);
		updaterDisplayGroups();
	}
	public void imprimer() {
		LogManager.logDetail(getClass().getName() + " : imprimer");
		numImpression++;
		String postfixe = DateCtrl.dateToStringSansDelim(new NSTimestamp()) + "_" + numImpression;
		String nomFichierJasper;
		if (popup.getSelectedIndex() == ManGUEConstantes.RETENUS) {
			// 25/02/2011 - pour gérer les élections de type JDNPHU
			if (currentInstance().typeInstance().estTypeJuridDiscHu()) {
				nomFichierJasper = "ListeEmargementJDNPHU";
			} else {
				nomFichierJasper = "ListeEmargement";

			}
		} else {
			nomFichierJasper = "ListeElecteurs";
		}
		InfoPourEditionRequete infoEdition = new InfoPourEditionRequete(nomFichierJasper,titreEdition + "\n" + currentInstance().llInstance(),currentInstance().dScrutin(),null);
		Class[] classeParametres = new Class[] {InfoPourEditionRequete.class,EOGlobalID.class,Integer.class};
		Object[] parametres = new Object[]{infoEdition,editingContext().globalIDForObject(currentInstance()),new Integer(popup.getSelectedIndex())};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerListeElectorale",classeParametres,parametres,"InfoPourEdition" + postfixe,"Impression de la liste électorale");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}

	/**
	 * 
	 */
	public void exporter() {
		LogManager.logDetail(getClass().getName() + " : exporter");
		JFileChooser saveDialog = new JFileChooser();
		saveDialog.setDialogTitle("Enregistrer sous");
		saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);
		if (saveDialog.showSaveDialog(component()) == JFileChooser.APPROVE_OPTION) {
			File file = saveDialog.getSelectedFile();
			UtilitairesImpression.afficherFichierExport(textePourExport(),file.getParent(),file.getName());
		}
	}

	/**
	 * 
	 * @param aNotif
	 */
	public void raffraichirElecteurs(NSNotification aNotif) {
		LogManager.logDetail(getClass().getName() + " : raffraichir électeur");
		popup.setSelectedIndex(ManGUEConstantes.RETENUS);
		NSArray electeurs = EOListeElecteur.rechercherElecteursPourInstance(currentInstance(),true);
		displayGroup().setObjectArray(electeurs);
		updaterDisplayGroups();
	}
	// Méthodes du controller DG
	public boolean estActionPossible() {
		return super.estActionPossible() && displayGroup().allObjects().count() > 0;
	}
	public boolean peutImprimer() {
		return estActionPossible() && !modificationEnCours();
	}
	/** on ne peut ajouter un electeur que si on n'est pas en mode modification et que pour cette election 
	 * on peut ajouter des electeurs
	 */
	public boolean peutAjouterElecteur() {
		return estActionPossible() && currentInstance() != null &&  currentInstance().typeInstance().peutInsererIndividus() && !modificationEnCours();
	}
	/** on ne peut exclure un electeur que si il n'est pas d&eacute;j&agrave; exclus */ 
	public boolean peutExclureElecteur() {
		return estActionPossible() && currentElecteur() != null && currentElecteur().estExclusManuellement() == false;
	}
	/** on ne peut r&eacute;ajouter un electeur que si il est exclus */ 
	public boolean peutReajouter() {
		return estActionPossible() && currentElecteur() != null && currentElecteur().estExclusManuellement();
	}
	// Méthodes protégées
	protected boolean conditionsOKPourFetch() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutUtiliserOutils();
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("raffraichirElecteurs", new Class[] { NSNotification.class }), AjoutElecteurs.RAFFRAICHIR_ELECTEURS, null);
	}

	/**
	 * 
	 */
	protected NSArray fetcherObjets() {

		CRICursor.setWaitCursor(listeAffichage);

		// pas d'instance sélectionnée
		if (currentInstance() == null) {
			popup.setEnabled(false);
			CRICursor.setDefaultCursor(listeAffichage);
			return null;
		} else {
			popup.setSelectedIndex(ManGUEConstantes.RETENUS);
			NSArray<EOListeElecteur> electeurs = EOListeElecteur.rechercherElecteursPourInstance(currentInstance());
			popup.setEnabled(electeurs.count() > 0);
			if (electeurs.count() == 0 && super.estActionPossible()) {	// on veut juste savoir si les actions sont autorisées pour cette instance
				EODialogs.runInformationDialog("Attention","Pas d'électeurs définis pour cette élection. Revenez au paramétrage final");
			}
			CRICursor.setDefaultCursor(listeAffichage);
			return electeurs;
		}
	}

	/**
	 * 
	 */
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.prenom", EOSortOrdering.CompareAscending));

		displayGroup().setSortOrderings(sorts);

		listeAffichage.table().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

	}

	/**
	 * 
	 */
	protected boolean traitementsAvantValidation() {
		java.util.Enumeration e = displayGroup().allObjects().objectEnumerator();
		boolean avertir = false;
		while (e.hasMoreElements()) {
			EOListeElecteur electeur = (EOListeElecteur)e.nextElement();
			if (electeur.estInsereManuellement() && electeur.estExclusManuellement()) {
				// supprimer cet électeur, il est ajouté puis exclus
				electeur.supprimerRelations();
				editingContext().deleteObject(electeur);
				avertir = true;
			}
		}
		if (avertir) {
			EODialogs.runInformationDialog("Attention", "Certains électeurs ont été ajoutés manuellement puis exclus, ils seront supprimés");
			LogManager.logDetail(getClass().getName() + " : suppression de certains électeurs ajoutés manuellement");
		}
		return true;
	}
	protected boolean traitementsPourSuppression() {
		// Pas de suppression directe
		return true;
	}
	// méthodes privées
	private EOListeElecteur currentElecteur() {
		return (EOListeElecteur)displayGroup().selectedObject();
	}
	private void preparerPourModification() {
		setModificationEnCours(true);
		vueBoutonsValidation.setVisible(true);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
	}

	/**
	 * 
	 * @return
	 */
	protected String textePourExport() {

		String texte = "";
		String delimiteur = CocktailExports.SEPARATEUR_TABULATION;
		
		if (currentInstance().typeInstance().estElcCnu()) {
			delimiteur = CocktailExports.SEPARATEUR_EXPORT;
		} else {
			texte = "Nom\tPrénom\tCivilité\tNom de famille\tPrénoms\tUAI\tLibellé UAI\tCollège\tBureau de vote\tComposante Elective\tSection Elective\tSecteur\tCarrière\tContrat\tCorps\tLibellé corps\tGrade\tStructure\tCnu\n";
		}

		EOStructure etablissement = EOStructure.rechercherEtablissement(editingContext());

		for (EOListeElecteur electeur : (NSArray<EOListeElecteur>) displayGroup().displayedObjects()) {

			EORne uai = EOStructure.getUAIEtablissement(editingContext(), electeur.structure());

			if (currentInstance().typeInstance().estElcCnu() == false) {
				texte =  texte + electeur.individu().nomUsuel() + "\t" + electeur.individu().prenom() + delimiteur + electeur.individu().cCivilite();

				if (electeur.individu().nomPatronymique() != null)
					texte += delimiteur + electeur.individu().nomPatronymique();
				else
					texte += "\t";

				if (electeur.individu().prenoms() != null)
					texte += delimiteur + electeur.individu().prenoms();
				else
					texte += "\t";

				if (uai != null) {
					texte += delimiteur + uai.code();
					texte += delimiteur + uai.libelleLong();
				}
				else {
					texte += delimiteur + etablissement.rne().code();
					texte += delimiteur + etablissement.rne().libelleLong();
				}

				texte = ajouterTexteAvecDelimiteur(texte,electeur.college(),"llCollege",delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte,electeur.bureauVote(),"llBureauVote",delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte,electeur.composanteElective(),"llComposanteElective",delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte,electeur.sectionElective(),"llSectionElective",delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte,electeur.secteur(),"llSecteur",delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte,electeur.carriere(),"libelle",delimiteur);
				if (electeur.contrat() != null)
					texte = ajouterTexteAvecDelimiteur(texte,electeur.contrat(),"libelle",delimiteur);
				else
					if (electeur.toContratHeberge() != null)
						texte = ajouterTexteAvecDelimiteur(texte,electeur.toContratHeberge(),"libelle",delimiteur);
					else
						texte += "\t";
				
				if (electeur.contrat() == null)
					texte = ajouterTexteAvecDelimiteur(texte,electeur.corps(),"lcCorps",delimiteur);
				else
					texte += delimiteur + "ANT";
				texte = ajouterTexteAvecDelimiteur(texte,electeur.corps(), EOCorps.LL_CORPS_KEY, delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte,electeur.grade(), EOGrade.LL_GRADE_KEY, delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte,electeur.structure(), EOStructure.LL_STRUCTURE_KEY, delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte,electeur.cnu(), EOCnu.LIBELLE_LONG_KEY, delimiteur);
				texte = texte + "\n";

			} else {
				if (electeur.individu().personnel().numen() != null) {
					texte += electeur.individu().personnel().numen();
				}
				texte += delimiteur;
				String civilite = electeur.individu().cCivilite();
				if (electeur.individu().cCivilite().equals("MME")) {
					civilite = "Mme";
				} else if (electeur.individu().cCivilite().equals("MLLE")) {
					civilite = "Mle";
				}
				texte += civilite;
				texte = ajouterTexteAvecDelimiteur(texte, electeur.individu(), EOIndividu.NOM_USUEL_KEY, delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte, electeur.individu(), EOIndividu.PRENOM_KEY, delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte, electeur.individu(), EOIndividu.NOM_PATRONYMIQUE_KEY, delimiteur);
				texte = ajouterTexteAvecDelimiteur(texte, electeur.individu(), "dateNaissanceFormatee", delimiteur);
				if (electeur.corps() != null) {
					String corps = "PR";
					if (electeur.corps().cCorps().equals(EOCorps.CORPS_MCF)) {
						corps = "MCF";
					}
					texte += delimiteur + corps;
				} else {
					texte += delimiteur;
				}
				if (electeur.college() != null) {
					texte = ajouterTexteAvecDelimiteur(texte, electeur.college(), "lcCollege", delimiteur);
				} else {
					texte += delimiteur;
				}
				if (electeur.cnu() != null) {
					texte = ajouterTexteAvecDelimiteur(texte, electeur.cnu(), EOCnu.CODE_KEY, delimiteur);
				} 
				texte += delimiteur + etablissement.llStructure() + "\n";
			}
		}

		return texte;
	}
	private String ajouterTexteAvecDelimiteur(String texte,EOGenericRecord record,String cle,String delimiteur) {
		texte = texte + delimiteur;
		if (record != null) {
			texte =  texte + record.valueForKey(cle);
		}
		return texte;
	}

}

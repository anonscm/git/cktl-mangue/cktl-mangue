/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections;

import org.cocktail.client.components.DialogueSimpleSansFetch;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamBv;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCollege;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCompElec;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCorps;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSecteur;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Permet d'ajouter des &eacute;lecteurs &agrave; la liste des &eacute;lecteurs d'une &eacute;lection<BR>
 * Il n'y a aucune contrainte dans le choix des &eacute;lecteurs (sauf si il y a des param&eacute;trages de corps),
 * ils doivent faire partie du personnel 
 * et ne pas &ecirc;tre d&eacute:j&agrave; dans la liste des &eacute;lecteurs<BR>
 * Le bureau de vote, le collg&egrave;ge, la composante &eacute;lective et le secteur si l'&eacute;lection
 * est sectoris&eacute;e doivent &ecirc;tre choisis.
 * @author christine
 *
 */
// 12/11/2010 - Adaptation Netbeans
public class AjoutElecteurs extends ModelePageComplete {
	private EOInstance currentInstance;
	private EOGlobalID instanceID;
	public final static String RAFFRAICHIR_ELECTEURS = "RaffraichirElecteurs";

	// Constructeurs
	public AjoutElecteurs(EOGlobalID instanceID) {
		super();
		this.instanceID = instanceID;
	}
	// Accesseurs
	public String nomElection() {
		if (currentInstance == null) {
			return null;
		} else {
			return currentInstance.llInstance();
		}
	}
	// Actions
	public void afficherIndividus() {
		LogManager.logDetail("AjoutElecteurs - Sélection d'un individu");
		DialogueSimpleSansFetch controleur = new DialogueSimpleSansFetch(EOIndividu.ENTITY_NAME,"AjoutElecteur_SelectionIndividu","nomUsuel","Sélection d'un individu",false,true,false,false,false);
		controleur.init();
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("temValide = 'O' AND personnels.noDossierPers <> nil",null);
		controleur.setQualifier(qualifier);
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getIndividu",new Class[] {NSNotification.class}),"AjoutElecteur_SelectionIndividu",null);
		controleur.afficherFenetre();

	}
	public void afficherBureauxVote() {
		LogManager.logDetail("AjoutElecteurs - Sélection d'un bureau de vote");
		UtilitairesDialogue.afficherDialogue(this,"ParamBv", "getBureauVote", false,qualifierPourElement(), true);
	}
	public void afficherColleges() {
		LogManager.logDetail("AjoutElecteurs - Sélection d'un collège");
		UtilitairesDialogue.afficherDialogue(this,"ParamCollege", "getCollege", false,qualifierPourElement(), true);	
	}
	public void afficherComposantes() {
		LogManager.logDetail("AjoutElecteurs - Sélection d'une composante élective");
		UtilitairesDialogue.afficherDialogue(this,"ParamCompElec", "getComposante", false,qualifierPourElement(), true);
	}
	public void afficherSecteurs() {
		LogManager.logDetail("AjoutElecteurs - Sélection d'un secteur");
		UtilitairesDialogue.afficherDialogue(this,"ParamSecteur", "getSecteur", false,qualifierPourSecteur(), true);
	}
	// Notifications
	public void getIndividu(NSNotification aNotif) {
		if (modificationEnCours() && aNotif.object() != null) {
			EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			// Vérifier si cette personne est déjà insérée dans les électeurs : soit ceux qui sont en cours d'ajout
			// soit ceux déjà dans la liste des électeurs
			if (EOListeElecteur.estElecteurSelectionnePourInstance(currentInstance,individu) || ((NSArray)displayGroup().displayedObjects().valueForKey("individu")).containsObject(individu)) {
				EODialogs.runErrorDialog("Attention", "Cet électeur est déjà présent dans la liste des électeurs");
				LogManager.logDetail("AjoutElecteurs - électeur refusé, il est déjà dans la liste");
			} else {
				ajouterElecteur(individu);
				updaterDisplayGroups();
			}
		}
	}
	public void getBureauVote(NSNotification aNotif) {
		ajouterRelation(aNotif.object(), "bureauVote","bureauVote");
	}
	public void getCollege(NSNotification aNotif) {
		ajouterRelation(aNotif.object(), "college","college");
	}
	public void getComposante(NSNotification aNotif) {
		ajouterRelation(aNotif.object(), "composanteElective","composanteElective");
	}
	public void getSecteur(NSNotification aNotif) {
		ajouterRelation(aNotif.object(), "secteur","secteur");
	}
	// Méthodes du controller DG
	public boolean peutAfficherSecteurs() {
		return modificationEnCours() && currentInstance != null && currentInstance.typeInstance().estSectorise() && currentElecteur().college() != null;
	}
	public boolean peutValider() {
		boolean ok = super.peutValider() && currentElecteur().individu() != null && currentElecteur().bureauVote() != null &&
		currentElecteur().college() != null && currentElecteur().composanteElective() != null;
		if (currentInstance.typeInstance().estSectorise()) {
			if (ok) {
				// Pour une élection sectorisée, regarder si le collège choisi est sectorisé
				NSArray colleges = (NSArray)EOParamSecteur.rechercherParametragesActifsPourInstance(editingContext(),"ParamSecteur",currentInstance).valueForKey("college");
				if (colleges.containsObject(currentElecteur().college())) {
					return currentElecteur().secteur() != null;
				} else {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return ok;
		}
	}
	// Autres
	public void connectionWasBroken() {
		super.connectionWasBroken();
		NSNotificationCenter.defaultCenter().postNotification(RAFFRAICHIR_ELECTEURS,null);
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		if (instanceID != null) {
			currentInstance = (EOInstance)SuperFinder.objetForGlobalIDDansEditingContext(instanceID, editingContext());
		}
		super.preparerFenetre();
	}
	protected void traitementsPourCreation() {
		currentElecteur().initAvecInstance(currentInstance);
		currentElecteur().setEstInsereManuellement(true);
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runInformationDialog("Erreur", message);
	}
	protected boolean conditionsOKPourFetch() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutUtiliserOutils();
	}
	protected NSArray fetcherObjets() {
		return EOListeElecteur.rechercherElecteursInclusManuellementPourInstance(currentInstance);
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cet électeur ?";
	}

	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.prenom", EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
	}
	protected boolean traitementsAvantValidation() {
		if (currentElecteur().instance() == null) {
			currentElecteur().addObjectToBothSidesOfRelationshipWithKey(currentInstance, "instance");
		}
		return true;
	}
	protected boolean traitementsPourSuppression() {
		currentElecteur().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOListeElecteur currentElecteur() {
		return (EOListeElecteur)displayGroup().selectedObject();
	}
	// On ne peut ajouter un électeur que si les corps ne sont pas requis ou que l'électeur a un des corps requis
	private void ajouterElecteur(EOIndividu individu) {
		LogManager.logDetail("AjoutElecteurs - Ajout d'un électeur");
		boolean ajouter = true;
		EOCorps corpsElecteur = null;
		EOCarriere carriereElecteur = null;
		EOContrat contratElecteur = null;
		EOGrade gradeElecteur = null;
		// déterminer si il y a une carrière ou un contrat à la date de référence
		carriereElecteur = EOCarriere.carrierePourDate(editingContext(), individu, currentInstance.dReference());
		if (carriereElecteur != null) {
			NSArray elements = carriereElecteur.elementsPourPeriode(currentInstance.dReference(), currentInstance.dReference());
			if (elements != null && elements.count() > 0) {
				EOElementCarriere element = (EOElementCarriere)elements.objectAtIndex(0);	// il n'y en a qu'un seul à une date donnée, pas de chevauchement
				if (element.toCorps() != null) {
					corpsElecteur = element.toCorps();
				}
				if (element.toGrade() != null) {
					gradeElecteur = element.toGrade();
				}
			}
		} else {
			// Vérifier si il y a un contrat
			contratElecteur = EOContrat.contratPrincipalPourDate(editingContext(), individu, currentInstance.dReference());
			if (contratElecteur != null) {
				NSArray avenants = contratElecteur.avenantsPourPeriode(currentInstance.dReference(), currentInstance.dReference());
				if (avenants != null && avenants.count() > 0) {
					EOContratAvenant avenant = (EOContratAvenant)avenants.objectAtIndex(0);	// il n'y en a qu'un seul à une date donnée, pas de chevauchement
					gradeElecteur = avenant.toGrade();
					if (gradeElecteur != null && gradeElecteur.toCorps() != null) {
						corpsElecteur = gradeElecteur.toCorps();

					}
				}
			} else {
				ajouter = false;	// pas de segment de carrière ou de contrat sur la période
			}
		}
		NSArray paramsCorps = EOParamCorps.rechercherParametragesActifsPourInstance(editingContext(), "ParamCorps", currentInstance);
		// Si il n'y a pas de paramétrage de corps ou que le corps est non nul et qu'il est dans les paramétrages, on peut ajouter
		if (paramsCorps.count() == 0 || (corpsElecteur != null && ((NSArray)paramsCorps.valueForKey("corps")).containsObject(corpsElecteur))) {
			ajouter = true;
		}
		if (ajouter) {
			currentElecteur().addObjectToBothSidesOfRelationshipWithKey(individu, "individu");
			if (carriereElecteur != null) {
				currentElecteur().addObjectToBothSidesOfRelationshipWithKey(carriereElecteur,"carriere");
			}
			if (contratElecteur != null) {
				currentElecteur().addObjectToBothSidesOfRelationshipWithKey(contratElecteur,"contrat");
			}
			if (corpsElecteur != null) {
				currentElecteur().addObjectToBothSidesOfRelationshipWithKey(corpsElecteur,"corps");
			}
			if (gradeElecteur != null) {
				currentElecteur().addObjectToBothSidesOfRelationshipWithKey(gradeElecteur,"grade");
			}
			ajouterElementsUniques();
			LogManager.logDetail("AjoutElecteurs - électeur ajouté");
		} else {
			EODialogs.runInformationDialog("Attention", individu.identite() + " ne peut être sélectionné, il n'a pas un des corps requis pour cette élection");
			LogManager.logDetail("AjoutElecteurs - électeur refusé, corps incorrect");
		}
	}
	// Ajoute les bureaux de vote, collège, composante élective et secteur si ils sont uniques
	private void ajouterElementsUniques() {
		EOQualifier qualifier = qualifierPourElement();
		EOFetchSpecification fs = new EOFetchSpecification("ParamBv",qualifier,null);
		NSArray params = editingContext().objectsWithFetchSpecification(fs);
		if (params.count() == 1) {	// Ajouter directement si 1 seul bureau de vote
			currentElecteur().addObjectToBothSidesOfRelationshipWithKey(((EOParamBv)params.objectAtIndex(0)).bureauVote(), "bureauVote");
			LogManager.logDetail("AjoutElecteurs - ajout du bureau de vote");
		}
		fs = new EOFetchSpecification("ParamCollege",qualifier,null);
		params = editingContext().objectsWithFetchSpecification(fs);
		if (params.count() == 1) {		// Ajouter directement si 1 seul collège
			currentElecteur().addObjectToBothSidesOfRelationshipWithKey(((EOParamCollege)params.objectAtIndex(0)).college(), "college");
			LogManager.logDetail("AjoutElecteurs - ajout du collège");
			if (currentInstance.typeInstance().estSectorise()) {
				fs = new EOFetchSpecification("ParamSecteur",qualifierPourSecteur(),null);
				params = editingContext().objectsWithFetchSpecification(fs);
				if (params.count() == 1) {		// Ajouter directement si 1 seul secteur
					currentElecteur().addObjectToBothSidesOfRelationshipWithKey(((EOParamSecteur)params.objectAtIndex(0)).secteur(), "secteur");
					LogManager.logDetail("AjoutElecteurs - ajout du secteur");
				}
			}
		} 
		fs = new EOFetchSpecification("ParamCompElec",qualifierPourElement(),null);
		params = editingContext().objectsWithFetchSpecification(fs);
		if (params.count() == 1) {		// Ajouter directement si 1 seul
			currentElecteur().addObjectToBothSidesOfRelationshipWithKey(((EOParamCompElec)params.objectAtIndex(0)).composanteElective(), "composanteElective");
			LogManager.logDetail("AjoutElecteurs - ajout de la composante élective");
			}			
	}
	// on attend comme objet une global UD
	private void ajouterRelation(Object object, String nomRelationSource,String nomRelationDestin) {
		if (modificationEnCours() && object != null) {
			LogManager.logDetail("AjoutElecteurs - ajout de la relation " + nomRelationSource);
			EOGenericRecord record = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)object, editingContext());
			EOGenericRecord relationRecord = (EOGenericRecord)record.valueForKey(nomRelationSource);
			currentElecteur().addObjectToBothSidesOfRelationshipWithKey(relationRecord,nomRelationDestin);
			updaterDisplayGroups();
		}
	}
	private EOQualifier qualifierPourElement() {
		return EOQualifier.qualifierWithQualifierFormat("instance = %@ AND temSelection = 'O'", new NSArray(currentInstance));
	}
	private EOQualifier qualifierPourSecteur() {
		NSMutableArray qualifiers = new NSMutableArray(qualifierPourElement());
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("college = %@ ",new NSArray(currentElecteur().college())));
		return new EOAndQualifier(qualifiers);
	}
}

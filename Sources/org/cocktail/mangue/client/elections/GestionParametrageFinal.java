/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections;

import java.awt.Cursor;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamRepartElec;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;

/** Affiche et gere la generation du parametrage final et de la liste
 * des electeurs associes a une &eacute;lection en fonction des parametrages 
 * effectues pour cette election.
 *
 */
public class GestionParametrageFinal extends PageFinaleElection {
	public static final String AFFICHER_ELECTEURS = "AfficherElecteur";
	private boolean verificationsBvOK;
	// Actions
	public void preparerParametrage() {
		// Créer les nouvelles reparts et les ajouter dans l'editing context
		try {
			component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			EODistributedObjectStore objectStore = (EODistributedObjectStore)editingContext().parentObjectStore();
			String result = (String)objectStore.invokeRemoteMethodWithKeyPath(editingContext(), "session","clientSideRequestPreparerParametrageFinal", new Class[]{EOInstance.class},
					new Object[]{currentInstance()}, false);
			NSArray params = null;
			if (result == null) {
				params = EOParamRepartElec.rechercherParametragesPourInstance(editingContext(), currentInstance());
			} else {
				EODialogs.runErrorDialog("Erreur", result);
			}
			displayGroup().setObjectArray(params);
			if (params != null) {
				verifierBureauxVote(params);
			}
			updaterDisplayGroups();
			
		} catch (Exception e) {		// Suite à un pré-contrôle
			EODialogs.runErrorDialog("Erreur", e.getMessage());
		}
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	
	/**
	 * 
	 */
	public void preparerListeElectorale() {
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		EODistributedObjectStore objectStore = (EODistributedObjectStore)editingContext().parentObjectStore();
		String result = (String)objectStore.invokeRemoteMethodWithKeyPath(editingContext(), "session","clientSideRequestGenererListeElectorale", new Class[]{EOInstance.class},
				new Object[]{currentInstance()}, false);
		if (result == null) {
			NSNotificationCenter.defaultCenter().postNotification(AFFICHER_ELECTEURS,null);
		} else {
			EODialogs.runErrorDialog("Erreur", result);
		}
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	// Méthodes du controller DG
	/** on peut preparer le parametrage final si l'instance est prete pour l'election
	 * et que le parametrage general est Ok et qu'il a ete modifie
	 */
	public boolean peutPreparerParametrage() {
		return currentInstance() != null;
//		return estActionPossible() && currentInstance() != null &&
//		(currentInstance().estEditionCoherente() == false|| displayGroup().displayedObjects().count() == 0) ;

	}
	/** on peut preparer la liste electorale si l'instance est prete pour l'election et
	 * si le parametrage final est pret et que tous les bureaux de vote sont definis
	 */
	public boolean peutPreparerListeElectorale() {
		return estActionPossible() && displayGroup().displayedObjects().count() > 0 && verificationsBvOK;
	}
	// Méthodes protégées
	protected boolean conditionsOKPourFetch() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutUtiliserOutils();
	}
	
	protected NSArray fetcherObjets() {
		// pas d'instance sélectionnée
		if (currentInstance() == null) {
			//verificationsParametragesOK = false;
			return null;
		} else {
			if (currentInstance().estEditionCoherente()) {
				NSArray<EOParamRepartElec> reparts = EOParamRepartElec.rechercherParametragesPourInstance(editingContext(), currentInstance());
				verifierBureauxVote(reparts);
				return reparts;
			} else {
				// les éditions sont décalées par rapport au paramétrage
				return null;
			}
		}
	}
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("structure.llStructure", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("composanteElective.llComposanteElective", EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
	}
	protected void traitementsApresValidation() {
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected boolean traitementsPourSuppression() {
		return true;
	}
	
	/**
	 * 
	 */
	protected void preparerAffichagePourInstance() {
		super.preparerAffichagePourInstance();
		// les associations ne sont pas systématiquement raffraichies
		raffraichirAssociations();
	}	
	
	/**
	 * 
	 * @param repartsElec
	 */
	private void verifierBureauxVote(NSArray<EOParamRepartElec> repartsElec) {
		verificationsBvOK = true;
		boolean bvOK = true;
		for (EOParamRepartElec repart : repartsElec) {
			if (repart.bureauVote() == null) {
				bvOK = false;
				break;
			}
		}
		if (bvOK == false) {
			EODialogs.runInformationDialog("Attention", "Certaines composantes n'ont pas de bureau de vote associé. Revoyez votre paramétrage !");
			verificationsBvOK = false;
		}
	}

}
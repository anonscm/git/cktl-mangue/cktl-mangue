/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections;

import java.awt.Cursor;

import org.cocktail.client.composants.ModelePage;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.elections.nomenclature.GestionTypesElection;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOTypeInstance;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Modelise une page pour lesquels on doit selectionner dans un pop-up un type d'instance **/
public abstract class PagePourTypeElection extends ModelePageComplete {
	
	public final static String CHANGEMENT_TYPE_INSTANCE = "ChangementTypeInstance";

	public EODisplayGroup displayGroupTypeInstance;
	private EOTypeInstance currentType;
	
	public EOEditingContext getEdc() {
		return ((ApplicationClient)EOApplication.sharedApplication()).getAppEditingContext();
	}
		
	public void valider() {

		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		try {

			if (traitementsAvantValidation()) {

				getEdc().saveChanges();

				traitementsApresValidation();

				updaterDisplaysGroup();

			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}

		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		
	}
	
	/**
	 * 
	 */
	public void annuler() {
		getEdc().revert();
		setEdited(false);
		traitementsApresRevert();
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.DELOCKER_ECRAN,this);
	}

	
	public String libelleCurrentType() {
		if (currentType != null) {
			return currentType.lcTypeInstance();
		} else {
			return null;
		}
	}
	
	/**
	 * 
	 * @param type
	 */
	public void setLibelleCurrentType(String type) {
		if (type == null) {
			this.currentType = null;
		} else {
			
			for (EOTypeInstance myTypeInstance : (NSArray<EOTypeInstance>) displayGroupTypeInstance.displayedObjects()) {
				if (myTypeInstance.lcTypeInstance().equals(type)) {
					this.currentType = myTypeInstance;
					break;
				}
			}
		}
		displayGroupTypeInstance.setSelectedObject(currentType);
		installerFiltre();
	}
	
	public EOTypeInstance currentType() {
		return currentType;
	}
	
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroupTypeInstance) {
			this.currentType = (EOTypeInstance)displayGroupTypeInstance.selectedObject();
			installerFiltre();			
		}
		super.displayGroupDidChangeSelection(aGroup);
	}

	// Notifications
    public void synchroniser(NSNotification aNotif) {
    		EOTypeInstance type = currentType();
    		displayGroupTypeInstance.setObjectArray(SuperFinder.rechercherEntiteAvecRefresh(getEdc(), EOTypeInstance.ENTITY_NAME));
    		if (displayGroupTypeInstance.allObjects().containsObject(type)) { 
    			displayGroupTypeInstance.selectObject(type);
    		} else {
    			displayGroupTypeInstance.selectObject(null);
    		}
    		displayGroupTypeInstance.updateDisplayedObjects();
    }
 	// méthodes du controller DG
    /** on ne peut ajouter/modifier/supprimer que les donnees liees aux types d'instances locaux */
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && currentType() != null && currentType().estLocal() == true;
	}
	public boolean peutChangerTypeInstance() {
		return !modificationEnCours();
	}
	
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		
		displayGroupTypeInstance.setSelectsFirstObjectAfterFetch(true);
		displayGroupTypeInstance.setObjectArray(SuperFinder.rechercherEntite(getEdc(), EOTypeInstance.ENTITY_NAME));
		if (displayGroupTypeInstance.selectedObject() != null) {
			currentType = (EOTypeInstance)displayGroupTypeInstance.selectedObject();
		}

		displayGroupTypeInstance.updateDisplayedObjects();
		raffraichirAssociations();
		updaterDisplayGroups();
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("synchroniser", new Class[] { NSNotification.class }), GestionTypesElection.SYNCHRONISER_TYPE_ELECTION, null);
	}
	protected void parametrerDisplayGroup() {
		displayGroupTypeInstance.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cTypeInstance",EOSortOrdering.CompareAscending)));
		updaterDisplayGroups();
	}
	
	/**
	 * 
	 */
	protected void installerFiltre() {

		displayGroupTypeInstance.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("cTypeInstance",EOSortOrdering.CompareAscending)));
		if (currentType() == null) {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("typeInstance = nil",null));
		} else {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("typeInstance = %@",new NSArray(currentType())));
		}
		
		updaterDisplayGroups();
			
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur",message);
	}
	protected void updaterDisplaysGroup() {
		super.updaterDisplayGroups();
		displayGroupTypeInstance.updateDisplayedObjects();
	}
}

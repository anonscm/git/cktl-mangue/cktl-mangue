/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.modele.ParametrageElection;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOCollegeSectionElective;
import org.cocktail.mangue.modele.grhum.elections.EOSectionElective;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamBv;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCompElec;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

// 19/11/2010 - Adaptation Netbeans
// 23/02/2011 - Correction des vérifications pour les élections de type estElcCses + ajout d'une méthode pour vérification des collèges-sections électives
public class GestionListesElectorales extends PagePourElection implements ChangeListener {
	public JTabbedPane vueOnglets;
	private GestionParametrageFinal gestionParametrageFinal;
	private GestionElecteurs gestionElecteurs;
	private NSArray<EOParamCompElec> paramsComposanteElective;
	private NSArray<EOParamBv> paramsBureauVote;
	private static final int INDEX_ONGLET_PARAMETRAGE = 0,INDEX_ONGLET_ELECTEURS = 1;
	private boolean verificationsParametragesOK;
	
	// Accesseurs
	public String messageInformation() {
		String message = null;
		if (currentInstance() != null) {
			if (currentInstance().dReference() == null) {
			message = "Veuillez définir la date de référence de cette élection dans les paramétrages";
			} else if (currentInstance().dScrutin() == null) {
				message = "Veuillez définir la date de scrutin de cette élection dans les paramétrages";
			} else if (paramsComposanteElective == null || paramsComposanteElective.count() == 0) {
			message = "Veuillez définir des composantes électives pour cette élection dans les paramétrages";
			} else if (paramsBureauVote == null || paramsBureauVote.count() == 0) {
				message = "Veuillez définir des bureaux de vote pour cette élection dans les paramétrages";
			} else if (currentInstance().estEditionCoherente() == false) {
				message = "Veuillez repréparer le paramétrage final";
			} else {
				message = effectuerVerificationsPourInstance();
			}
		}
		if (message != null && vueOnglets.getSelectedIndex() == INDEX_ONGLET_ELECTEURS) {
			vueOnglets.setSelectedIndex(INDEX_ONGLET_PARAMETRAGE);
		}
		return message;
	}
	// Notifications
	public void lockSaisie(NSNotification aNotif) {
		super.lockSaisie(aNotif);
		vueOnglets.setEnabled(false);	// on interdit l'accès à la vue onglets
	} 
	// DELOCK ECRAN
	public void unlockSaisie(NSNotification aNotif) {
		super.unlockSaisie(aNotif);
		vueOnglets.setEnabled(instancePretePourElection());	
	}
	public void afficherElecteurs(NSNotification aNotif) {
		LogManager.logDetail(getClass().getName() + " : afficher électeurs");
		vueOnglets.setEnabledAt(INDEX_ONGLET_ELECTEURS,true);	
		vueOnglets.setSelectedIndex(INDEX_ONGLET_ELECTEURS);
	} 
	// Méthodes du controller DG
	public boolean instancePretePourElection() {
		return currentInstance() != null && currentInstance().dReference() != null && currentInstance().dScrutin() != null
		&& paramsComposanteElective != null && paramsComposanteElective.count() > 0 
		&& paramsBureauVote != null && paramsBureauVote.count() > 0; //&& verificationsParametragesOK;
	}

	// Méthodes protégées
	protected boolean conditionsOKPourFetch() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutUtiliserOutils();
	}
	/**
	 * 
	 */
	protected NSArray fetcherObjets() {
		// pas d'instance sélectionnée
		if (currentInstance() == null) {
			paramsComposanteElective = null;
			paramsBureauVote = null;
		} else {
			// Fetcher la liste des composantes électives de cette instance car si elle est vide,
			// on n'a pas la possibilité d'effectuer le paramétrage final
			paramsComposanteElective = ParametrageElection.rechercherParametragesActifsPourInstance(editingContext(), EOParamCompElec.ENTITY_NAME, currentInstance());
			paramsBureauVote = ParametrageElection.rechercherParametragesActifsPourInstance(editingContext(), "ParamBv", currentInstance());	
		}
		return null;
	}
	protected void preparerFenetre() {
		super.preparerFenetre();
		vueOnglets.addChangeListener(this);
		vueOnglets.setSelectedIndex(INDEX_ONGLET_PARAMETRAGE);
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("afficherElecteurs", new Class[] { NSNotification.class }), GestionParametrageFinal.AFFICHER_ELECTEURS, null);
	}
	protected void preparerAffichagePourInstance() {
		if (vueOnglets == null) {
			return;
		}
		fetcherObjets();	// pour vérifier si le paramétrage de base est correct
		effectuerVerificationsPourInstance();
		preparerControleurs();
		if (currentInstance() == null) {
			vueOnglets.setEnabled(false);
		} else {
			vueOnglets.setEnabled(instancePretePourElection());
			if (gestionParametrageFinal != null) {
				vueOnglets.setEnabledAt(INDEX_ONGLET_ELECTEURS,gestionParametrageFinal.peutPreparerListeElectorale());
			}
		}
		controllerDisplayGroup().redisplay();
	}
	protected boolean instanceModifiable() {
		return false;
	}
	protected void parametrerDisplayGroup() {
	}
	protected void traitementsApresValidation() {
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected boolean traitementsPourSuppression() {
		// Pas de suppression
		return true;
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur", message);	
	}
	protected  void terminer() {
	}
	// Méthodes privées
	private void preparerControleurs() {
		PageFinaleElection controleur = null;
		boolean estInitialisation = false;
		if (vueOnglets.getSelectedIndex() == INDEX_ONGLET_PARAMETRAGE) {
			if (gestionParametrageFinal == null) {
				gestionParametrageFinal = new GestionParametrageFinal();
				estInitialisation = true;
			}
			controleur = gestionParametrageFinal;
		} else {
			if (gestionElecteurs == null) {
				gestionElecteurs = new GestionElecteurs();
				estInitialisation = true;
			}
			controleur =  gestionElecteurs;
		}
		if (estInitialisation) {
			controleur.init();
			GraphicUtilities.swaperViewEtCentrer((JComponent)vueOnglets.getComponentAt(vueOnglets.getSelectedIndex()), (JComponent)controleur.component());
		}
		if (currentInstance() == null) {
			controleur.preparerPourAction(null,false);
		} else {
			controleur.preparerPourAction(currentInstance(),instancePretePourElection());
		}
	}
	private String effectuerVerificationsPourInstance() {
		if (currentInstance() == null) {
			verificationsParametragesOK = false;
			return null;
		}
		verificationsParametragesOK = true;
		if (currentInstance().typeInstance().estElcCap()) {
			if (verifierEntite("ParamCorps") == false) {
				return "Ce type d'élection requiert que vous sélectionnez des corps dans les paramétrages";
			}
		}  
		if (currentInstance().typeInstance().estElcCnu()) {
			if (verifierEntite("ParamCnu")  == false) {
				return "Ce type d'élection requiert que vous sélectionnez des cnus dans les paramétrages";
			}
		}
		if (currentInstance().typeInstance().estElcCses()) {
			if (verifierEntite("ParamSectionElective") == false) {	// 23/02/2011
				return "Ce type d'élection requiert que vous sélectionnez des sections électives dans les paramétrages";
			}
		}
		// 23/03/2011 - vérification des collèges-sections électives (si une même section est utilisée dans plusieurs collèges sections, les types de recrutement doivent êre définis
		if (currentInstance().typeInstance().estTypeJuridDiscHu()) {
			NSArray sections = (NSArray)SuperFinder.rechercherEntite(editingContext(), "ParamSectionElective").valueForKey("sectionElective");
			NSMutableArray collegesSectionsVus = new NSMutableArray();
			java.util.Enumeration e = sections.objectEnumerator();
			// Vérifier que tous les collèges sélectionnés sont bien associés à des collèges-sections
			while (e.hasMoreElements()) {
				EOSectionElective section = (EOSectionElective)e.nextElement();
				// Rechercher tous les collèges-sections électives liés à cette section
				NSArray collegesSections = EOCollegeSectionElective.findForSection(editingContext(), section, true);
				if (collegesSections == null || collegesSections.count() == 0) {
					return "Ce type d'élection requiert que des collèges-sections électives soient définies pour toutes les sections";
				}
				collegesSectionsVus.addObjectsFromArray(collegesSections);
			}
			// Vérifier que les collèges sections sont bien définis i.e que si une même section est affectée plusieurs fois, elle a un type de recrutement
			if (collegesSectionsVus.count() > 1) {
				NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("sectionElective.llSectionElective", EOSortOrdering.CompareAscending));
				sorts.addObject(EOSortOrdering.sortOrderingWithKey("typeRecrutement.trhuCode", EOSortOrdering.CompareAscending));
				EOSortOrdering.sortArrayUsingKeyOrderArray(collegesSectionsVus, sorts);
				EOCollegeSectionElective collegeSectionInitial = (EOCollegeSectionElective)collegesSectionsVus.objectAtIndex(0);
				for (int i = 1; i < collegesSectionsVus.count(); i++) {
					EOCollegeSectionElective collegeSection = (EOCollegeSectionElective)collegesSectionsVus.objectAtIndex(i);
					if (collegeSection.sectionElective() == collegeSectionInitial.sectionElective()) {
						if (collegeSection.typeRecrutement() == null || collegeSectionInitial.typeRecrutement() == null) {
							return "Certains collèges-sections n'ont pas de type recrutement alors qu'il est requis lorsque les sections électives sont associées à plusieurs collèges";
						}
					} else {	// On a changé de section élective
						collegeSectionInitial = collegeSection;
					}
				}
			}
		}
		return null;	// pas d'erreur
		
	}
	private boolean verifierEntite(String nomEntite) {
		NSArray params = ParametrageElection.rechercherParametragesActifsPourInstance(editingContext(),nomEntite, currentInstance());
		if (params == null || params.count() == 0) {
			verificationsParametragesOK = false;
			return false;
		} else {
			return true;
		}
	}
	/*private boolean verifierEntite(String nomEntite,String message) {
		NSArray params = ParametrageElection.rechercherParametragesActifsPourInstance(editingContext(),nomEntite, currentInstance());
		//EODialogs.runInformationDialog("Attention", message);
		if (params == null || params.count() == 0) {
			verificationsParametragesOK = false;
			return false;
		}
	}*/
	// Interface changeListener
	public void stateChanged(ChangeEvent e) {
		Object source = e.getSource();
		if (source instanceof JTabbedPane) {
			preparerControleurs();
			controllerDisplayGroup().redisplay();
		}
	}
}

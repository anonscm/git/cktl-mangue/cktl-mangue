// _AjoutElecteurs_Interface.java
// Created on 2 août 2010 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _AjoutElecteurs_Interface extends COFrame {

    /** Creates new form _AjoutElecteurs_Interface */
    public _AjoutElecteurs_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new org.cocktail.component.CODisplayGroup();
        listeAffichage = new org.cocktail.component.COTable();
        cOView1 = new org.cocktail.component.COView();
        vueBoutonsValidation = new org.cocktail.component.COView();
        cOButton9 = new org.cocktail.component.COButton();
        cOButton10 = new org.cocktail.component.COButton();
        cOTextField5 = new org.cocktail.component.COTextField();
        cOButton4 = new org.cocktail.component.COButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cOTextField2 = new org.cocktail.component.COTextField();
        cOButton5 = new org.cocktail.component.COButton();
        cOButton6 = new org.cocktail.component.COButton();
        jLabel3 = new javax.swing.JLabel();
        cOTextField1 = new org.cocktail.component.COTextField();
        cOButton7 = new org.cocktail.component.COButton();
        cOTextField3 = new org.cocktail.component.COTextField();
        jLabel4 = new javax.swing.JLabel();
        cOButton8 = new org.cocktail.component.COButton();
        cOTextField4 = new org.cocktail.component.COTextField();
        jLabel5 = new javax.swing.JLabel();
        vueBoutonsModification = new org.cocktail.component.COView();
        cOButton12 = new org.cocktail.component.COButton();
        cOButton11 = new org.cocktail.component.COButton();
        cOButton13 = new org.cocktail.component.COButton();
        cOTextField6 = new org.cocktail.component.COTextField();
        jLabel6 = new javax.swing.JLabel();

        displayGroup.setEntityName("ListeElecteur");
        displayGroup.setHasDelegate(true);
        displayGroup.setIsMainDisplayGroupForController(true);

        setControllerClassName("org.cocktail.mangue.client.elections.AjoutElecteurs");
        setSize(new java.awt.Dimension(805, 442));

        listeAffichage.setColumns(new Object[][] {{null,"individu.nomUsuel",new Integer(2),"Nom",new Integer(0),new Integer(174),new Integer(1000),new Integer(38)},{null,"individu.prenom",new Integer(2),"Prénom",new Integer(0),new Integer(156),new Integer(1000),new Integer(38)},{null,"college.llCollege",new Integer(2),"Collège",new Integer(0),new Integer(167),new Integer(1000),new Integer(38)},{null,"bureauVote.llBureauVote",new Integer(2),"Bureau de vote",new Integer(0),new Integer(173),new Integer(1000),new Integer(38)},{null,"composanteElective.llComposanteElective",new Integer(2),"Composante Elective",new Integer(0),new Integer(163),new Integer(1000),new Integer(38)},{null,"secteur.llSecteur",new Integer(2),"Secteur",new Integer(0),new Integer(188),new Integer(1000),new Integer(44)},{null,"carriere.libelle",new Integer(2),"Carrière",new Integer(0),new Integer(170),new Integer(1000),new Integer(38)},{null,"contrat.libelle",new Integer(2),"Contrat",new Integer(0),new Integer(188),new Integer(1000),new Integer(38)},{null,"corps.llCorps",new Integer(2),"Corps",new Integer(0),new Integer(198),new Integer(1000),new Integer(44)},{null,"grade.llGrade",new Integer(2),"Grade",new Integer(0),new Integer(188),new Integer(1000),new Integer(44)}});
        listeAffichage.setDisplayGroupForTable(displayGroup);

        cOView1.setIsBox(true);

        cOButton9.setActionName("annuler");
        cOButton9.setBorderPainted(false);
        cOButton9.setEnabledMethod("modificationEnCours");
        cOButton9.setIconName("annuler16.gif");

        cOButton10.setActionName("valider");
        cOButton10.setBorderPainted(false);
        cOButton10.setEnabledMethod("peutValider");
        cOButton10.setIconName("valider16.gif");

        org.jdesktop.layout.GroupLayout vueBoutonsValidationLayout = new org.jdesktop.layout.GroupLayout(vueBoutonsValidation);
        vueBoutonsValidation.setLayout(vueBoutonsValidationLayout);
        vueBoutonsValidationLayout.setHorizontalGroup(
            vueBoutonsValidationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsValidationLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        vueBoutonsValidationLayout.setVerticalGroup(
            vueBoutonsValidationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsValidationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(cOButton10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        cOTextField5.setDisplayGroupForValue(displayGroup);
        cOTextField5.setEnabledMethod("champNonModifiable");
        cOTextField5.setSupportsBackgroundColor(true);
        cOTextField5.setValueName("individu.identite");

        cOButton4.setActionName("afficherIndividus");
        cOButton4.setBorderPainted(false);
        cOButton4.setEnabledMethod("modeCreation");
        cOButton4.setIconName("loupe16.gif");

        jLabel1.setFont(new java.awt.Font("Helvetica", 1, 12)); // NOI18N
        jLabel1.setText("Personne");

        jLabel2.setFont(new java.awt.Font("Helvetica", 1, 12)); // NOI18N
        jLabel2.setText("Collège");

        cOTextField2.setDisplayGroupForValue(displayGroup);
        cOTextField2.setEnabledMethod("champNonModifiable");
        cOTextField2.setSupportsBackgroundColor(true);
        cOTextField2.setValueName("college.llCollege");

        cOButton5.setActionName("afficherColleges");
        cOButton5.setBorderPainted(false);
        cOButton5.setEnabledMethod("modificationEnCours");
        cOButton5.setIconName("loupe16.gif");

        cOButton6.setActionName("afficherComposantes");
        cOButton6.setBorderPainted(false);
        cOButton6.setEnabledMethod("modificationEnCours");
        cOButton6.setIconName("loupe16.gif");

        jLabel3.setFont(new java.awt.Font("Helvetica", 1, 12)); // NOI18N
        jLabel3.setText("Composante élective");

        cOTextField1.setDisplayGroupForValue(displayGroup);
        cOTextField1.setEnabledMethod("champNonModifiable");
        cOTextField1.setSupportsBackgroundColor(true);
        cOTextField1.setValueName("composanteElective.llComposanteElective");

        cOButton7.setActionName("afficherBureauxVote");
        cOButton7.setBorderPainted(false);
        cOButton7.setEnabledMethod("modificationEnCours");
        cOButton7.setIconName("loupe16.gif");

        cOTextField3.setDisplayGroupForValue(displayGroup);
        cOTextField3.setEnabledMethod("champNonModifiable");
        cOTextField3.setSupportsBackgroundColor(true);
        cOTextField3.setValueName("bureauVote.llBureauVote");

        jLabel4.setFont(new java.awt.Font("Helvetica", 1, 12)); // NOI18N
        jLabel4.setText("Bureau de vote");

        cOButton8.setActionName("afficherSecteurs");
        cOButton8.setBorderPainted(false);
        cOButton8.setEnabledMethod("peutAfficherSecteurs");
        cOButton8.setIconName("loupe16.gif");

        cOTextField4.setDisplayGroupForValue(displayGroup);
        cOTextField4.setEnabledMethod("champNonModifiable");
        cOTextField4.setSupportsBackgroundColor(true);
        cOTextField4.setValueName("secteur.llSecteur");

        jLabel5.setFont(new java.awt.Font("Helvetica", 0, 12)); // NOI18N
        jLabel5.setText("Secteur");

        org.jdesktop.layout.GroupLayout cOView1Layout = new org.jdesktop.layout.GroupLayout(cOView1);
        cOView1.setLayout(cOView1Layout);
        cOView1Layout.setHorizontalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView1Layout.createSequentialGroup()
                .add(122, 122, 122)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(cOView1Layout.createSequentialGroup()
                            .add(jLabel3)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 296, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(cOButton6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(cOView1Layout.createSequentialGroup()
                            .add(jLabel2)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 296, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(cOButton5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(cOView1Layout.createSequentialGroup()
                            .add(jLabel1)
                            .add(18, 18, 18)
                            .add(cOTextField5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 374, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(cOButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(cOView1Layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 56, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOView1Layout.createSequentialGroup()
                                .add(jLabel5)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(cOTextField4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 296, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(cOButton8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOView1Layout.createSequentialGroup()
                                .add(jLabel4)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 296, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(cOButton7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(149, Short.MAX_VALUE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOView1Layout.createSequentialGroup()
                .addContainerGap(690, Short.MAX_VALUE)
                .add(vueBoutonsValidation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        cOView1Layout.setVerticalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOView1Layout.createSequentialGroup()
                .addContainerGap(10, Short.MAX_VALUE)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(cOTextField5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOButton5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOButton6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel3))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cOButton7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel4))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cOTextField4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOButton8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel5))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(vueBoutonsValidation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        cOButton12.setActionName("supprimer");
        cOButton12.setBorderPainted(false);
        cOButton12.setEnabledMethod("boutonModificationAutorise");
        cOButton12.setIconName("supprimer16.gif");

        cOButton11.setActionName("ajouter");
        cOButton11.setBorderPainted(false);
        cOButton11.setIconName("ajouter16.gif");

        cOButton13.setActionName("modifier");
        cOButton13.setBorderPainted(false);
        cOButton13.setEnabledMethod("boutonModificationAutorise");
        cOButton13.setIconName("modifier16.gif");

        org.jdesktop.layout.GroupLayout vueBoutonsModificationLayout = new org.jdesktop.layout.GroupLayout(vueBoutonsModification);
        vueBoutonsModification.setLayout(vueBoutonsModificationLayout);
        vueBoutonsModificationLayout.setHorizontalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton13, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton12, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        vueBoutonsModificationLayout.setVerticalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsModificationLayout.createSequentialGroup()
                .add(cOButton11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton13, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(cOButton12, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        cOTextField6.setEnabledMethod("champNonModifiable");
        cOTextField6.setSupportsBackgroundColor(true);
        cOTextField6.setValueName("nomElection");

        jLabel6.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel6.setText("Election");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(19, 19, 19)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, cOView1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, listeAffichage, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(47, 47, 47))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(142, Short.MAX_VALUE)
                .add(jLabel6)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOTextField6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 449, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(194, 194, 194))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel6)
                    .add(cOTextField6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(15, 15, 15)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 177, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(10, 10, 10)
                        .add(cOView1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COButton cOButton10;
    public org.cocktail.component.COButton cOButton11;
    public org.cocktail.component.COButton cOButton12;
    public org.cocktail.component.COButton cOButton13;
    public org.cocktail.component.COButton cOButton4;
    public org.cocktail.component.COButton cOButton5;
    public org.cocktail.component.COButton cOButton6;
    public org.cocktail.component.COButton cOButton7;
    public org.cocktail.component.COButton cOButton8;
    public org.cocktail.component.COButton cOButton9;
    public org.cocktail.component.COTextField cOTextField1;
    public org.cocktail.component.COTextField cOTextField2;
    public org.cocktail.component.COTextField cOTextField3;
    public org.cocktail.component.COTextField cOTextField4;
    public org.cocktail.component.COTextField cOTextField5;
    public org.cocktail.component.COTextField cOTextField6;
    private org.cocktail.component.COView cOView1;
    public org.cocktail.component.CODisplayGroup displayGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    public org.cocktail.component.COTable listeAffichage;
    public org.cocktail.component.COView vueBoutonsModification;
    public org.cocktail.component.COView vueBoutonsValidation;
    // End of variables declaration//GEN-END:variables
                  

}

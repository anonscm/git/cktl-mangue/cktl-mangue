/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.select.elections.ComposanteElectiveSelectCtrl;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOComposanteElective;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementComposante;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCollege;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCompElec;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;

public class GestionComposantes extends PageParametrageElection {
	public EOTable listeStructure;
	public EODisplayGroup displayGroupRegroupement;
	private ComposanteElectiveSelectCtrl  myCompSelectCtrl;

	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		preparerStructures();
		super.displayGroupDidChangeSelection(aGroup);
	}

	/**
	 * 
	 */
	public void ajouterComposante() {

		NSArray<EOComposanteElective> composantes = myCompSelectCtrl.getComposantes();
		if (composantes != null && composantes.size() >= 0) {
			try {
				for (EOComposanteElective myComposante : composantes) {
					EOParamCompElec.creer(getEdc(), currentInstance(), myComposante);
				}
				getEdc().saveChanges();
				displayGroup().setObjectArray(EOParamCompElec.findForInstance(getEdc(), currentInstance()));
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public void modifier() {
		try {

			currentParametrage().setEstActif(!currentParametrage().estActif());
			getEdc().saveChanges();
			updaterDisplayGroups();

		}
		catch (Exception e) {
			e.printStackTrace();
		}				
	}

	
	/**
	 * 
	 */
	public void supprimer() {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous vraiment supprimer cette composante  ?","Oui","Non")) {
			try {
				getEdc().deleteObject(currentParametrage());
				getEdc().saveChanges();				
				displayGroup().deleteSelection();
				updaterDisplayGroups();
			}
			catch (Exception e) {
			}
		}
	}


	/**
	 * 
	 * @return
	 */
	public boolean peutAjouter() {
		if (modeSaisiePossible()) {
			if (currentInstance().typeInstance().estElcCufr() == false) {
				return true;
			} else {
				// Pour une élection de type Ufr, on ne peut sélectionner qu'une composante
				for (EOParamCompElec param : (NSArray<EOParamCompElec>)displayGroup().displayedObjects()) {
					if (param.estActif()) {
						return false;
					}
				}
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @return
	 */
	public boolean peutModifier() {
		if (boutonModificationAutorise()) {
			if (currentInstance().typeInstance().estElcCufr() == false) {
				return true;
			} else {
				if (currentParametrage().estActif()) {
					return true;		// Si cette composante est sélectionnée, on peut la déselectionner
				} else {
					//	Pour une élection de type Ufr, on ne peut sélectionner qu'une composante
					for (EOParamCompElec param : (NSArray<EOParamCompElec>)displayGroup().displayedObjects()) {
						if (param.estActif()) {
							return false;
						}
					}
					return true;
				}
			}
		} else {
			return false;
		}
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre(); // après pour que lors listeStructure fetch currentInstance soit définie
		GraphicUtilities.changerTaillePolice(listeStructure,11);
		GraphicUtilities.rendreNonEditable(listeStructure);

		myCompSelectCtrl = new ComposanteElectiveSelectCtrl(getEdc());

		preparerStructures();
	}
	protected String nomEntiteAssociee() {
		return EOComposanteElective.ENTITY_NAME;
	}
	protected String nomRelationPourEntiteAssociee() {
		return "composanteElective";
	}
	/** Verifie si on peut ajouter une composante elective : dans le cas d'une election de type cufr, on ne peut
	 * ni ajouter de composante ayant plusieurs structures definies,ni ayant une structure avec des structures filles */
	protected boolean peutAjouterRecord(EOGenericRecord record) {
		if (verifierStructures((EOComposanteElective)record)) {
			return super.peutAjouterRecord(record);
		} else {
			return false;
		}
	}
	protected  void traitementsApresAjout(EOGenericRecord record) {
		preparerStructures();
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous supprimer cette composante élective ?";
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("composanteElective.llComposanteElective", EOSortOrdering.CompareAscending)));
	}
	protected void terminer() {
	}
	// Méthodes privées
	private void preparerStructures() {
		if (currentParametrage() == null) {
			displayGroupRegroupement.setObjectArray(null);
		} else {
			EOParamCompElec param = (EOParamCompElec)currentParametrage();
			if (param.composanteElective() == null) {
				displayGroupRegroupement.setObjectArray(null);
			} else {
				displayGroupRegroupement.setObjectArray(EORegroupementComposante.rechercherRegroupementsPourComposanteElective(getEdc(),param.composanteElective()));
			}
		}
	}
	private boolean verifierStructures(EOComposanteElective composante) {
		// 23/02/2011 - Pour les élections autres que cufr, on ne vérifie pas les composantes
		if (currentInstance().typeInstance().estElcCufr() == false) {
			return true;
		}
		boolean structuresOK = true;
		String message = "";
		NSArray regroupements = composante.regroupements();
		if (regroupements != null) {
			if (regroupements.count() > 1) {
				message = "Pour une élection de type ufr, vous ne pouvez pas sélectionner des composantes avec plusieurs structures";
				structuresOK = false;
			} else if (regroupements.count() == 1) {
				EORegroupementComposante regroupement = (EORegroupementComposante)regroupements.objectAtIndex(0);
				int niveau = regroupement.structure().niveauStructure();
				switch(niveau) {
				case 0:
					message = "Pour une élection de type ufr, vous ne pouvez pas sélectionner ce type de structure dans les composantes électives";
					structuresOK = false;
					break;
				case 1:
					if (regroupement.selectionnerStructuresFilles() == false) {
						message = "Pour une élection de type ufr, vous devez inclure les filles de l'établissement dans les composantes électives";
						structuresOK = false;
					}

					break;
				case 2:
					NSArray structuresFilles = regroupement.structure().structuresFils();
					if (regroupement.selectionnerStructuresFilles() == false && structuresFilles.count() > 0) {
						message = "Pour une élection de type ufr, vous devez sélectionner dans les composantes des structures et inclure les filles";
						structuresOK = false;
					}
					break;
				case 3:
					message = "Pour une élection de type ufr, vous ne pouvez pas sélectionner ce type de structure dans les composantes électives";
					structuresOK = false;
					break;
				}

			} else if (regroupements.count() == 0) {
				message = "Pas de structures associées à cette composante";
				structuresOK = false;
			}
		} else {
			message = "Pas de structures associées à cette composante";
		}

		if (structuresOK == false) {
			EODialogs.runInformationDialog("Attention", message);
		}
		return structuresOK;
	}
	@Override
	protected NSArray sortEntiteAssociee() {
		// TODO Auto-generated method stub
		return new NSArray(new EOSortOrdering(EOParamCompElec.COMPOSANTE_ELECTIVE_KEY + "." + EOComposanteElective.LL_COMPOSANTE_ELECTIVE_KEY, EOSortOrdering.CompareAscending)); 
	}

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.select.elections.SecteurSelectCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOSecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCnu;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCollege;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSecteur;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class GestionSecteurs extends PageParametrageElection {

	public EODisplayGroup displayGroupCollege;
	public EOTable listeCollege;

	/**
	 * 
	 */
	public void ajouter() {

		NSArray<EOSecteur> secteurs = SecteurSelectCtrl.sharedInstance(getEdc()).getSecteurs();
		if (secteurs != null && secteurs.size() > 0) {
			try {
				for (EOSecteur mySecteur : secteurs) {
					EOParamSecteur.creer(getEdc(), currentInstance(), mySecteur, currentCollege());
				}
				getEdc().saveChanges();
				
				displayGroup().setObjectArray(EOParamSecteur.findForInstanceAndCollege(getEdc(), currentInstance() , currentCollege()));
				
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 */
	public void modifier() {
		try {
			
			currentParam().setEstActif(!currentParam().estActif());
			getEdc().saveChanges();
			updaterDisplayGroups();

		}
		catch (Exception e) {
			e.printStackTrace();
		}				
	}

	/**
	 * 
	 */
	public void supprimer() {
		if (EODialogs.runConfirmOperationDialog("Alerte","Confirmez-vous la suppression  ?","Oui","Non")) {
			try {
				for (EOParamSecteur mySecteur : (NSArray<EOParamSecteur>)displayGroup().selectedObjects()) {
					getEdc().deleteObject(mySecteur);
				}
				getEdc().saveChanges();
				displayGroup().deleteSelection();
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}		
		}
	}


	public boolean displayGroupShouldChangeSelection(EODisplayGroup group,NSArray newIndexes) {
		if (group == displayGroupCollege) {
			return modificationEnCours() == false;
		} else {
			return super.displayGroupShouldChangeSelection(group, newIndexes);
		}
	}
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroupCollege) {
			parametrerDisplayGroup();
			updaterDisplayGroups();
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
	// Notifications
	public void changerInstance(NSNotification aNotif) {
		super.changerInstance(aNotif);
		preparerDisplayGroupCollege();
		updaterDisplayGroups();
	}
	/** Pour raffraichir les secteurs affiches */
	public void raffraichir(NSNotification aNotif) {
		preparerDisplayGroupCollege();
		displayGroup().setObjectArray(fetcherObjets());
		updaterDisplayGroups();
	}
	// Méthodes du controller DG
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && currentCollege() != null;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		loadNotifications();
		GraphicUtilities.changerTaillePolice(listeCollege,11);
		GraphicUtilities.rendreNonEditable(listeCollege);
		preparerDisplayGroupCollege();
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("raffraichir", new Class[] { NSNotification.class }), "MAJ_COLLEGE", null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("raffraichir", new Class[] { NSNotification.class }), PageParametrageElection.PARAMETRAGE_ELECTION_MODIFIE, null);
	}
	protected String nomEntiteAssociee() {
		return EOSecteur.ENTITY_NAME;
	}
	protected String nomRelationPourEntiteAssociee() {
		return "secteur";
	}
	protected  void traitementsApresAjout(EOGenericRecord record) {
		currentParam().setCollegeRelationship(currentCollege());
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce secteur ?";
	}
	protected void parametrerDisplayGroup() {
		if (currentCollege() == null) {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat(EOParamSecteur.COLLEGE_KEY + " = nil", null));
		} else {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat(EOParamSecteur.COLLEGE_KEY + " = %@",new NSArray(currentCollege())));
		}
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupCollege.updateDisplayedObjects();
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOCollege currentCollege() {
		return (EOCollege)displayGroupCollege.selectedObject();
	}
	private EOParamSecteur currentParam() {
		return (EOParamSecteur)displayGroup().selectedObject();
	}

	/**
	 * 
	 */
	private void preparerDisplayGroupCollege() {
		if (currentInstance() == null) {
			displayGroupCollege.setObjectArray(null);
		} else {
			displayGroupCollege.setObjectArray((NSArray<EOCollege>)EOParamCollege.findForInstance(getEdc(), currentInstance()).valueForKey(EOParamCollege.COLLEGE_KEY));
		}
	}
	@Override
	protected NSArray sortEntiteAssociee() {
		// TODO Auto-generated method stub
		return new NSArray(new EOSortOrdering(EOParamSecteur.SECTEUR_KEY + "." + EOSecteur.LL_SECTEUR_KEY, EOSortOrdering.CompareAscending)); 
	}

}

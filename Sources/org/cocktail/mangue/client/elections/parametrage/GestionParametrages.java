/*
 * Created on 20 mai 2005
 *
 * Classe Application client
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.elections.PagePourElection;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Gestion des parametrages d'une election<BR>
 * Le display group contient la liste des instances (i.e elections) pour un type d'instance donne
 *
 */

public class GestionParametrages extends PagePourElection implements ChangeListener {
	
	public JTabbedPane vueOnglets;
	private String[] composants = {"GestionBureauxVote","GestionComposantes","GestionColleges","GestionSecteurs","GestionCorps","GestionCnu","GestionSections"};
	private NSMutableDictionary controleurs = new NSMutableDictionary();
	private final static int GESTION_SECTEUR = 3, GESTION_CORPS = 4,GESTION_CNU = 5, GESTION_SECTION = 6;

	// Notifications
	/** pour signaler que l'instance a change */
	public void changementInstance(NSNotification aNotif) {
		String nomClasse = (String)aNotif.userInfo().valueForKey("nomClasse");
		if (nomClasse.equals(this.getClass().getName())) {
			vueOnglets.setSelectedIndex(0);
			preparerAffichagePourInstance();
		}
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		vueOnglets.addChangeListener(this);
		vueOnglets.setSelectedIndex(0);
		afficherParametrage();
		super.preparerFenetre();
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("changementInstance", new Class[] { NSNotification.class }), GestionSelectionInstance.CHANGEMENT_INSTANCE, null);
	}
	/** permet d'interdire/activer la fermeture de la fenetre en supprimant/ajoutant le window listener */
	protected void lockerFenetre(boolean lock) {
		super.lockerFenetre(lock);
		vueOnglets.setEnabled(!lock);
		if (!lock) {
			preparerAffichagePourInstance();
		}
	}
	protected void terminer() {
		vueOnglets.removeChangeListener(this);
	}
	protected boolean conditionsOKPourFetch() {
		// pas de fetch
		return true;
	}
	protected boolean instanceModifiable() {
		return true;
	}
	protected NSArray fetcherObjets() {
		// Pas de fetch
		return null;
	}

	protected void parametrerDisplayGroup() {
		 // pas de DG géré
	}
	protected boolean traitementsAvantValidation() {
		return true;	// pas de validation
	}
	protected void traitementsApresValidation() {
		// pas de validation
	}
	protected void traitementsApresRevert() {
		// pas de validation
	}
	protected  void afficherExceptionValidation(String message) {
		// pas de validation
	}
	protected void preparerAffichagePourInstance() {

		if (currentInstance() == null) {
			vueOnglets.setEnabledAt(GESTION_SECTEUR,false);
			vueOnglets.setEnabledAt(GESTION_CORPS,false);
			vueOnglets.setEnabledAt(GESTION_CNU,false);
			vueOnglets.setEnabledAt(GESTION_SECTION,false);
		} else {
			vueOnglets.setEnabledAt(GESTION_SECTEUR,currentInstance().typeInstance().estSectorise());	// Secteurs autorisés si le type d'instance le supporte
			vueOnglets.setEnabledAt(GESTION_CORPS,currentInstance().typeInstance().estElcCap());	// Corps autorisés si le type d'instance le supporte
			vueOnglets.setEnabledAt(GESTION_CNU,currentInstance().typeInstance().estElcCnu());	// Cnus autorisées si le type d'instance le supporte
			vueOnglets.setEnabledAt(GESTION_SECTION,currentInstance().typeInstance().estElcCses());	// Section électives autorisées si le type d'instance le supporte
		}
		updaterDisplayGroups();
	}
	
	//	 Méthodes privées
	private void afficherParametrage() {
		int index = vueOnglets.getSelectedIndex();
		String composant = composants[index];
		if (controleurs.objectForKey(composant) == null) {
			preparerControleur(composant);
		}
	}
	private void preparerControleur(String composant) {
		try {
			PageParametrageElection controleur = (PageParametrageElection) Class.forName("org.cocktail.mangue.client.elections.parametrage." + composant).newInstance();
			controleur.initialiser(composant,currentInstance());
			controleur.preparerFenetre();
			GraphicUtilities.swaperViewEtCentrer((JComponent)vueOnglets.getSelectedComponent(), controleur.component());
			controleurs.setObjectForKey(controleur, composant);
		} catch (Exception e) {
			e.printStackTrace();
		}		

	}
	//	 interface ChangeListener
	//	Change Listener
	public void stateChanged(ChangeEvent e) {
		afficherParametrage();
	}

}

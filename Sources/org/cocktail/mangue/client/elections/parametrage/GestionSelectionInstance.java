/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage;

import org.cocktail.mangue.client.elections.PagePourTypeElection;
import org.cocktail.mangue.client.select.elections.InstanceSelectCtrl;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class GestionSelectionInstance extends PagePourTypeElection {

	public final static String CHANGEMENT_INSTANCE = "ChangementInstance";
	private boolean estInstanceEditable;
	private String classeParent;

	/**
	 * 
	 * @param classeParent
	 * @param estInstanceEditable
	 */
	public void initialiser(String classeParent,boolean estInstanceEditable) {
		EOArchive.loadArchiveNamed("GestionSelectionInstance", this, "org.cocktail.mangue.client.elections.parametrage.interfaces", this.disposableRegistry());
		super.initialiser(false,false);
		this.classeParent = classeParent;

		this.estInstanceEditable = estInstanceEditable;
		preparerFenetre();
	}

	//  Accesseurs
	// changement de type
	public void setLibelleCurrentType(String aStr) {
		super.setLibelleCurrentType(aStr);
		displayGroup().setObjectArray(null);	// plus d'instance à afficher
		updaterDisplayGroups();
		NSNotificationCenter.defaultCenter().postNotification(CHANGEMENT_INSTANCE,null,new NSDictionary(classeParent,"nomClasse")); 
	}
	// Méthodes de délégation du display group
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		setEdited(true);
		controllerDisplayGroup().redisplay();
	}
	// Actions
	public void afficherInstances() {

		EOInstance instance = InstanceSelectCtrl.sharedInstance(getEdc()).getInstance(currentType());
		if (instance != null) {
			displayGroup().setObjectArray(new NSArray(instance));
			displayGroup().selectObject(instance);
			NSNotificationCenter.defaultCenter().postNotification(CHANGEMENT_INSTANCE, instance, new NSDictionary(classeParent,"nomClasse"));
			updaterDisplayGroups();
			NSNotificationCenter.defaultCenter().removeObserver(this,"SelectionInstance",null);	// pour ne plus recevoir la notification du dialogue			
		}

	}
	// Méthodes du controller DG
	public boolean peutChangerTypeInstance() {
		return !modificationEnCours() && !estLocke() && conditionSurPageOK();
	}
	public boolean peutAfficherInstances() {
		return peutChangerTypeInstance() && currentType() != null;
	}
	public boolean peutValider() {
		return modificationEnCours() && currentInstance().dReference() != null && currentInstance().dScrutin() != null;
	}
	public boolean modeSaisiePossible() {
		return peutChangerTypeInstance() && currentType() != null;
	}

	// Notifications
	public void typeInstanceHasChanged(NSNotification aNotif) {

	}


	protected void preparerFenetre() {
		super.preparerFenetre();
		displayGroup().setSelectsFirstObjectAfterFetch(true);
		if (!estInstanceEditable) {
			vueBoutonsModification.setVisible(false);
		}

		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("typeInstanceHasChanged", new Class[] { NSNotification.class }), PagePourTypeElection.CHANGEMENT_TYPE_INSTANCE, null);

		raffraichirAssociations();
		updaterDisplayGroups();
	}

	protected void traitementsPourCreation() {
		// Pas de création
	}
	protected NSArray fetcherObjets() {
		return null;
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey(EOInstance.LL_INSTANCE_KEY, EOSortOrdering.CompareAscending)));
		super.parametrerDisplayGroup();
	}
	protected String messageConfirmationDestruction() {
		// Pas de destruction
		return null;
	}
	protected boolean traitementsPourSuppression() {
		// Pas de destruction
		return true;
	}
	protected void installerFiltre() {
		// Charger les instances dans le display group
		displayGroup().setObjectArray(null);
		updaterDisplayGroups();
	}
	protected EOInstance currentInstance() {
		return (EOInstance)displayGroup().selectedObject();
	}
	protected void terminer() {
	}
}

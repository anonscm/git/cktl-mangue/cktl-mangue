/*
 * Created on 20 mai 2005
 *
 * Classe Application client
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage;

import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.modele.ParametrageElection;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public abstract class PageParametrageElection extends ModelePageComplete {

	private EOInstance currentInstance;
	public static final String PARAMETRAGE_ELECTION_MODIFIE = "ParametrageElectionModifie";

	/**
	 * 
	 * @return
	 */
	public EOEditingContext getEdc() {
		return ((ApplicationClient)EOApplication.sharedApplication()).getAppEditingContext();
	}

	/** initialise la page
	 * @param nomEntiteParametrage nom de l'entite de parametrage
	 * @param globalIdInstance globalID de l'instance parametree
	 */
	public void initialiser(String composant,EOInstance currentInstance) {
		super.initialiser(false,false);
		EOArchive.loadArchiveNamed(composant,this,"org.cocktail.mangue.client.elections.parametrage.interfaces",null);
		this.currentInstance = currentInstance;
	}
	// Actions
	public void ajouter() {
		UtilitairesDialogue.afficherDialogue(this, nomEntiteAssociee(), "getSelection", true, true, null, affichageDialogueSimple(),true);
	}
	public void modifier() {
		super.modifier();
		// Changer l'état de la sélection en l'état opposé
		java.util.Enumeration e = displayGroup().selectedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			ParametrageElection parametrage = (ParametrageElection)e.nextElement();
			parametrage.changerSelection();
		}
	}
	// Notifications
	// DELOCK ECRAN
	public void unlockSaisie(NSNotification aNotif) {
		// pour raffraîchir les écrans si il y a eu des modifications dans les nomenclatures
		if (aNotif.object() != this) {
			displayGroup().setSortOrderings(sortEntiteAssociee());
			displayGroup().setObjectArray(fetcherObjets());
			displayGroup().setSelectedObject(null);
			updaterDisplayGroups();
		}
		super.unlockSaisie(aNotif);
	} 

	/**
	 * 
	 * @param aNotif
	 */
	public void getSelection(NSNotification aNotif) {
		if (aNotif.object() != null && aNotif.object() instanceof NSArray) {

			boolean ajoute = false;
			for (EOGlobalID myGlobalId : (NSArray<EOGlobalID>)aNotif.object()) {
				EOGenericRecord record = (EOGenericRecord)SuperFinder.objetForGlobalIDDansEditingContext(myGlobalId, getEdc());
				if (peutAjouterRecord(record)) {
					LogManager.logDetail(getClass().getName() + " : ajout d'un objet de type : " + entityName());
					displayGroup().insertNewObjectAtIndex(0);
					displayGroup().setSelectionIndexes(new NSArray(new Integer(0)));
					traitementsPourCreation();
					currentParametrage().addObjectToBothSidesOfRelationshipWithKey(record,nomRelationPourEntiteAssociee());
					traitementsApresAjout(record);
					ajoute = true;
				}
			}
			if (ajoute) {
				setModeCreation(true);
				mettreEnModeModification();
			}
		}
	}

	/**
	 * 
	 * @param aNotif
	 */
	public void changerInstance(NSNotification aNotif) {

		String nomClasse = (String)aNotif.userInfo().valueForKey("nomClasse");
		if (nomClasse.equals("org.cocktail.mangue.client.elections.parametrage.GestionParametrages")) {
			if (aNotif.object() == null) {
				currentInstance = null;
			} else {
				currentInstance = (EOInstance)aNotif.object();
				LogManager.logDetail("Election choisie : " + currentInstance().llInstance());
			}
			displayGroup().setObjectArray(fetcherObjets());
			updaterDisplayGroups();
		}
	}
	// Méthodes du controller DG
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && currentInstance() != null;
	}
	// Méthodes protégée
	protected void preparerFenetre() {
		
		super.preparerFenetre();
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		displayGroup().setSortOrderings(sortEntiteAssociee());
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("changerInstance", new Class[] { NSNotification.class }), GestionSelectionInstance.CHANGEMENT_INSTANCE, null);
	}
	protected ParametrageElection currentParametrage() {
		return (ParametrageElection)displayGroup().selectedObject();
	}
	protected EOInstance currentInstance() {
		return currentInstance;
	}
	/** Retourne true si on affiche un dialogue simple dans la sélection des objets. Valeur par defaut : true */
	protected boolean affichageDialogueSimple() {
		return true;
	}
	/** Retourne le nom de l'entite associee (par exemple College ou Secteur) */
	protected abstract String nomEntiteAssociee();
	/** Retourne le nom de l'entite associee (par exemple College ou Secteur) */
	protected abstract NSArray sortEntiteAssociee();
	/** Retourne le nom de la relation vers l'entite associee (par exemple college ou secteur) */
	protected abstract String nomRelationPourEntiteAssociee();
	/** Verifie si on peut ajouter le record : a surcharger si les conditions sont plus complexes que
	 * le fait qu'un param avec le record comme relation associee existe deja dans le display group */
	protected boolean peutAjouterRecord(EOGenericRecord record) {
		NSArray values = (NSArray)displayGroup().displayedObjects().valueForKey(nomRelationPourEntiteAssociee());
		return (values.containsObject(record) == false);
	}
	protected void traitementsPourCreation() {
		currentParametrage().initAvecInstance(currentInstance());
	}
	protected abstract void traitementsApresAjout(EOGenericRecord record);
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR", message);
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	
	/**
	 * 
	 */
	protected NSArray fetcherObjets() {
		if (currentInstance() == null) {
			return null;
		} else {
			String nomEntiteParametrage = displayGroup().dataSource().classDescriptionForObjects().entityName();
			return ParametrageElection.rechercherParametragesPourInstance(getEdc(), nomEntiteParametrage, currentInstance());
		}
	}
	protected boolean traitementsAvantValidation() {
		currentInstance().setEstEditionCoherente(false);
		// détruire le paramétrage final de cette élection rattaché à l'instance
		currentInstance().supprimerParametrageFinal();
		// détruire les électeurs
		currentInstance().supprimerElecteursPourInstance(); 
		return true;
	}
	protected void traitementsApresValidation() {
		NSNotificationCenter.defaultCenter().postNotification(PARAMETRAGE_ELECTION_MODIFIE,getEdc().globalIDForObject(currentInstance()));
		super.traitementsApresValidation();
	}
	protected boolean traitementsPourSuppression() {
		currentParametrage().supprimerRelations();
		deleteSelectedObjects();
		currentInstance().setEstEditionCoherente(false);
		// détruire le paramétrage final de cette élection rattaché à l'instance
		currentInstance().supprimerParametrageFinal();
		// détruire les électeurs
		currentInstance().supprimerElecteursPourInstance(); 
		return true;
	}
	protected void traitementsApresSuppression() {
		super.traitementsApresSuppression();
		NSNotificationCenter.defaultCenter().postNotification(PARAMETRAGE_ELECTION_MODIFIE,getEdc().globalIDForObject(currentInstance()));
	}
	protected void mettreEnModeModification() {
		setModificationEnCours(true);
		setEdited(true);
		// afficher les boutons de validation
		afficherBoutons(true);
		NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
		updaterDisplayGroups();
	}
}

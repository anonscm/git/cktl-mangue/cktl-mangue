/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage;

import org.cocktail.mangue.client.select.specialisations.CnuSelectCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCnu;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

public class GestionCnu extends PageParametrageElection {

	/**
	 * 
	 */
	public void ajouter() {

		NSArray<EOCnu> cnus = CnuSelectCtrl.sharedInstance(getEdc()).getObjects();
		if (cnus != null && cnus.size() > 0) {
			try {
				for (EOCnu myCnu : cnus) {
					if (peutAjouterRecord(myCnu)) {
						EOParamCnu.creer(getEdc(), currentInstance(), myCnu);
					}
				}
				getEdc().saveChanges();
				displayGroup().setObjectArray(EOParamCnu.findForInstance(getEdc(), currentInstance()));
				updaterDisplayGroups();
				NSNotificationCenter.defaultCenter().postNotification("MAJ_CNU", currentInstance());

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 
	 */
	public void modifier() {
		try {
			currentParametrage().setEstActif(!currentParametrage().estActif());
			getEdc().saveChanges();
			updaterDisplayGroups();

		}
		catch (Exception e) {
			e.printStackTrace();
		}				
	}
	/**
	 * 
	 */
	public void supprimer() {

		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous vraiment supprimer ces sections CNU  ?","Oui","Non")) {
			try {
				for (EOParamCnu param : (NSArray<EOParamCnu>)displayGroup().selectedObjects()) {
					getEdc().deleteObject(param);
				}
				getEdc().saveChanges();				
				displayGroup().deleteSelection();
				updaterDisplayGroups();
				NSNotificationCenter.defaultCenter().postNotification("MAJ_COLLEGE", currentInstance());
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	protected boolean affichageDialogueSimple() {
		return false;
	}
	protected String nomEntiteAssociee() {
		return EOCnu.ENTITY_NAME;
	}
	protected String nomRelationPourEntiteAssociee() {
		return EOParamCnu.CNU_KEY;
	}
	protected  void traitementsApresAjout(EOGenericRecord record) {
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette section Cnu ?";
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey(EOParamCnu.CNU_KEY + "." + EOCnu.LIBELLE_LONG_KEY, EOSortOrdering.CompareAscending)));
	}
	protected void terminer() {
	}
	@Override
	protected NSArray sortEntiteAssociee() {
		// TODO Auto-generated method stub
		return new NSArray(new EOSortOrdering(EOParamCnu.CNU_KEY + "." + INomenclature.CODE_KEY, EOSortOrdering.CompareAscending)); 
	}

}

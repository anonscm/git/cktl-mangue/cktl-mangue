/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.client.select.elections.BureauVoteSelectCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.modele.grhum.elections.EOBureauVote;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementBv;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamBv;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCnu;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamStrBv;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// 12/11/2010 - Adaptation Netbeans
public class GestionBureauxVote extends PageParametrageElection {
	
	public EODisplayGroup displayGroupAutre,displayGroupStructure;
	public EOTable listeStructures,listeAutresStructures;
	
	private final static int PEUT_AJOUTER_STRUCTURE = 1;
	private final static int APPARTIENT_BV = 2;
	private final static int APPARTIENT_AUTRE_BV= 3;

	/**
	 * 
	 */
	protected void preparerFenetre() {
		super.preparerFenetre();
		
		GraphicUtilities.changerTaillePolice(listeStructures,11);
		GraphicUtilities.rendreNonEditable(listeStructures);
		GraphicUtilities.changerTaillePolice(listeAutresStructures,11);
		GraphicUtilities.rendreNonEditable(listeAutresStructures);
		displayGroupStructure.setSelectsFirstObjectAfterFetch(false);
	}

	// Méthode de délégation du display group
	// Pour interdire la sélection dans la liste des structures
	public boolean displayGroupShouldChangeSelection(EODisplayGroup group,NSArray newIndexes) {
		if (group == displayGroupStructure) {
			return false;
		} else {
			return super.displayGroupShouldChangeSelection(group, newIndexes);
		}
	}

	/**
	 * 
	 */
	public void ajouter() {

		NSArray<EOBureauVote> bureaux = BureauVoteSelectCtrl.sharedInstance(getEdc()).getBureauxVote();
		if (bureaux != null && bureaux.size() > 0) {
			try {
				for (EOBureauVote myBureau : bureaux) {
					EOParamBv.creer(getEdc(), currentInstance(), myBureau);
				}
				getEdc().saveChanges();
				displayGroup().setObjectArray(EOParamBv.findForInstance(getEdc(), currentInstance()));
				updaterDisplayGroups();

			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroup()) {
			preparerDisplayGroups();
		}
		super.displayGroupDidChangeSelection(aGroup);
	}
	// Actions
	public void modifier() {
		super.modifier();
		if (currentParamBv().estActif()) {
			// afficher une alerte si il y a une incohérence dans les structures associées au bureau de vote
			verifierCoherenceStructures();
		}
	}

	/**
	 * Ajout d'une autre structure au parametrage des bureaux de vote
	 */
	public void ajouterStructure() {

		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(getEdc());
		if (structure != null) {
			int peutAjouter = peutAjouterAutreStructure(structure,false);
			switch(peutAjouter) {
			case PEUT_AJOUTER_STRUCTURE :
				try {
										
					EOParamStrBv.creer(getEdc(), currentParamBv().bureauVote() , currentInstance(), structure);
					getEdc().saveChanges();
					displayGroupAutre.setObjectArray(EOParamStrBv.findForBureauAndInstance(getEdc(), currentParamBv().bureauVote(), currentInstance()));
					updaterDisplayGroups();					
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case APPARTIENT_AUTRE_BV :
				EODialogs.runInformationDialog("Attention", "Cette structure a déjà été ajoutée à un autre bureau de vote !");
				break;
			case APPARTIENT_BV :
				EODialogs.runInformationDialog("Attention", "Cette structure a déjà été ajoutée à ce bureau de vote !");
				break;
			}
		}
	}

	/**
	 * 
	 */
	public void supprimer() {
		
		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous vraiment supprimer ce bureau  ?","Oui","Non")) {
			try {

				NSArray<EOParamStrBv> params = EOParamStrBv.findForBureauAndInstance(getEdc(), currentParamBv().bureauVote(), currentInstance());
				for (EOParamStrBv myParam : params) {
					getEdc().deleteObject(myParam);
				}

				getEdc().deleteObject(currentParamBv());

				getEdc().saveChanges();
				
				displayGroup().deleteSelection();
				displayGroupAutre.deleteSelection();
				displayGroupStructure.deleteSelection();
				updaterDisplayGroups();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 */
	public void supprimerStructure() {
		
		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous vraiment supprimer les structures séletionnées ?","Oui","Non")) {
			try {
				for (EOParamStrBv myStructure : (NSArray<EOParamStrBv>)displayGroupAutre.selectedObjects()) {
					getEdc().deleteObject(myStructure);
				}
				getEdc().saveChanges();
				displayGroupAutre.deleteSelection();
				updaterDisplayGroups();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean peutAjouterStructure() {
		return currentParamBv() != null && currentParamBv().estActif();
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean peutSupprimerStructure() {
		return peutAjouterStructure() && currentParamStructure() != null;
	}

	protected String nomEntiteAssociee() {
		return EOBureauVote.ENTITY_NAME;
	}
	protected String nomRelationPourEntiteAssociee() {
		return "bureauVote";
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce bureau de vote ?";
	}
	protected  void traitementsApresAjout(EOGenericRecord record) {
		preparerDisplayGroups();
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("bureauVote.llBureauVote", EOSortOrdering.CompareAscending)));
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupAutre.updateDisplayedObjects();
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOParamBv currentParamBv() {
		return (EOParamBv)displayGroup().selectedObject();
	}
	private EOParamStrBv currentParamStructure() {
		return (EOParamStrBv)displayGroupAutre.selectedObject();
	}
	
	/**
	 * 
	 */
	private void preparerDisplayGroups() {
		if (currentParamBv() == null || currentParamBv().bureauVote() == null) {
			displayGroupStructure.setObjectArray(null);
			displayGroupAutre.setObjectArray(null);
		} else {
			NSMutableArray<EOStructure> structures = new NSMutableArray<EOStructure>();
			for (EORegroupementBv myRegroupement : EORegroupementBv.rechercherRegroupementsPourBureauDeVote(getEdc(), currentParamBv().bureauVote())) {
				if (structures.containsObject(myRegroupement.structure()) == false) {
					
					structures.addObject(myRegroupement.structure());
					
					if (myRegroupement.selectionnerStructuresFilles()) {
						for (EOStructure myStructure : myRegroupement.structure().toutesStructuresFils()) {
							if (structures.containsObject(myStructure) == false) {
								structures.addObject(myStructure);
							}
						}	
					}
				}
			}
			displayGroupStructure.setObjectArray(structures);
			displayGroupAutre.setObjectArray(EOParamStrBv.rechercherParamsPourInstanceEtBureau(getEdc(),currentInstance(),currentParamBv().bureauVote()));
		}
	}

	public boolean peutAjouterBureau() {
		return currentInstance() != null;
	}
	public boolean peutModifierBureau() {
		return currentInstance() != null && currentParamBv() != null;
	}
	public boolean peutSupprimerBureau() {
		return currentInstance() != null && currentParamBv() != null;
	}


	private int peutAjouterAutreStructure(EOStructure structure,boolean prendreEnCompteFils) {
		int peutAjouter = PEUT_AJOUTER_STRUCTURE;
		// vérifier que la structure n'est pas déjà sélectionnée pour un bureau de vote actif
		NSArray regroupsBv = EORegroupementBv.rechercherRegroupementsPourStructure(getEdc(),structure);
		java.util.Enumeration e = regroupsBv.objectEnumerator();
		while (e.hasMoreElements()) {
			EORegroupementBv regroupement = (EORegroupementBv)e.nextElement();
			// Si on ne prend pas en compte les fils ou qu'on prend en compte les fils et que le regroupement
			// pour cette structure signale qu'il faut sélectionner les fils => alors on ne peut retenir la structure
			if (!prendreEnCompteFils || (prendreEnCompteFils && regroupement.selectionnerStructuresFilles())) {
				if (regroupement.bureauVote() == currentParamBv().bureauVote()) {
					peutAjouter = APPARTIENT_BV;
					break;
				} else {
					// vérifier que le bureau de vote est associé à cette élection et qu'il est actif
					if (estBureauVoteActif(regroupement.bureauVote())) {
						peutAjouter  = APPARTIENT_AUTRE_BV;
						break;
					}
				}
			}
		}
		if (peutAjouter == PEUT_AJOUTER_STRUCTURE) {
			// Vérifier si les structures parentes n'ont pas été affectées à un bureau de vote 
			// avec sélection des structures filles
			EOStructure parent = structure.toStructurePere();
			if (parent != null && parent != structure) {
				peutAjouter = peutAjouterAutreStructure(parent,true);
			} 
		}
		if (peutAjouter == PEUT_AJOUTER_STRUCTURE && !prendreEnCompteFils) {
			// Vérifier que la structure n'est pas déjà ajoutée comme "Autre Structure" 
			// pour celui-ci et pour les autres bureaux de vote
			e = listeParamsPourStructure(structure).objectEnumerator();
			while (e.hasMoreElements()) {
				// si on en trouve, vérifier à quel bureau de vote elle appartient
				EOParamStrBv param = (EOParamStrBv)e.nextElement();
				if (param.bureauVote() == currentParamBv().bureauVote()) {
					peutAjouter = APPARTIENT_BV;
					break;
				} else if (estBureauVoteActif(param.bureauVote())) {
					peutAjouter  = APPARTIENT_AUTRE_BV;
					break;
				}
			}
		}
		return peutAjouter;
	}


	/**
	 * 
	 * @param bureauVote
	 * @return
	 */
	private boolean estBureauVoteActif(EOBureauVote bureauVote) {
		for (EOParamBv myParam : (NSArray<EOParamBv>)displayGroup().displayedObjects()) {
			if (myParam.bureauVote() == bureauVote) {
				return myParam.estActif();
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param structure
	 * @return
	 */
	private NSArray<EOParamStrBv> listeParamsPourStructure(EOStructure structure) {
		NSMutableArray<EOParamStrBv> params = new NSMutableArray<EOParamStrBv>(EOParamStrBv.rechercherParamsPourInstanceEtStructure(getEdc(), currentInstance(), structure));
		// ajouter ceux qui viennent d'être créés
		for (EOParamStrBv myParam : (NSArray<EOParamStrBv>)displayGroupAutre.displayedObjects()) {
			if (myParam.structure() == structure && params.containsObject(myParam) == false) {
				params.insertObjectAtIndex(myParam,0);
			}
		}
		return params;
	}
	
	// Vérifie qu'une même structure n'est pas attachée à plusieurs bureaux de vote actifs
	// En tant que structure, structure fille ou autre structure
	private void verifierCoherenceStructures() {
		NSMutableArray<EOStructure> structures = new NSMutableArray();
		// Créer une table contenant toutes les structures associées aux bureaux de vote actif
		for (EOParamBv myParamBv : (NSArray<EOParamBv>)displayGroup().displayedObjects()) {
			if (myParamBv.estActif()) {
				// Rechercher toutes les structures pour le bureau de vote
				NSArray<EORegroupementBv> regroupementsBv = EORegroupementBv.rechercherRegroupementsPourBureauDeVote(getEdc(),myParamBv.bureauVote());
				
				for (EORegroupementBv myRegroupement : regroupementsBv) {
					// Ajouter la structure. On s'arrête dès qu'on n'a pas pu ajouté une structure
					if (ajouterStructure(structures,myRegroupement.structure()) == false) {
						return;
					}
					if (myRegroupement.selectionnerStructuresFilles()) {
						
						for (EOStructure myStructure : myRegroupement.structure().toutesStructuresFils()) {
							if (ajouterStructure(structures, myStructure) == false) {
								return;
							}
						}
					}
				}
				// Ajouter aussi les autres structures. On s'arrête dès qu'on n'a pas pu ajouté une structure
				NSArray<EOParamStrBv> paramsStrBv = EOParamStrBv.rechercherParamsPourInstanceEtBureau(getEdc(),currentInstance(), myParamBv.bureauVote());
				for (EOParamStrBv myParam : paramsStrBv) {
					if (ajouterStructure(structures,myParam.structure()) == false) {
						return;
					}
				} 
			}
		}
	}
	
	/**
	 * 
	 * @param structures
	 * @param structure
	 * @return
	 */
	private boolean ajouterStructure(NSMutableArray structures,EOStructure structure) {
		if (structures.containsObject(structure) == false) {
			structures.addObject(structure);
			return true;
		} else {
			EODialogs.runErrorDialog("Erreur", "Pensez à vérifier les structures associées à vos bureaux de vote. Une même structure " +
					"est associée à plusieurs bureaux de vote");
			return false;
		}
	}
	@Override
	protected NSArray sortEntiteAssociee() {
		// TODO Auto-generated method stub
		return new NSArray(new EOSortOrdering(EOParamBv.BUREAU_VOTE_KEY + "." + EOBureauVote.LL_BUREAU_VOTE_KEY, EOSortOrdering.CompareAscending)); 
	}
}

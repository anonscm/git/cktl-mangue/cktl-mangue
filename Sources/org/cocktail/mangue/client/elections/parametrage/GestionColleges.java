/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage;

import org.cocktail.mangue.client.select.elections.CollegeSelectCtrl;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.modele.ParametrageElection;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOCollegeSectionElective;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionContrat;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionCorps;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCnu;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCollege;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSecteur;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

public class GestionColleges extends PageParametrageElection {

	/**
	 * 
	 */
	public void ajouter() {
		
		NSArray<EOCollege> colleges = CollegeSelectCtrl.sharedInstance(getEdc()).getColleges(currentInstance().typeInstance());
		if (colleges != null && colleges.size() > 0) {
			try {
				for (EOCollege myCollege : colleges) {
					if (peutAjouterRecord(myCollege)) {
						EOParamCollege.creer(getEdc(), currentInstance(), myCollege);
					}
				}

				getEdc().saveChanges();
				displayGroup().setObjectArray(EOParamCollege.findForInstance(getEdc(), currentInstance()));
				updaterDisplayGroups();
				NSNotificationCenter.defaultCenter().postNotification("MAJ_COLLEGE", currentInstance());
				
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 */
	public void modifier() {
		try {
			
			currentParametrage().setEstActif(!currentParametrage().estActif());
			getEdc().saveChanges();
			updaterDisplayGroups();

		}
		catch (Exception e) {
			e.printStackTrace();
		}				
	}

	/**
	 * 
	 */
	public void supprimer() {
		
		if (EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous vraiment supprimer ce collège  ?","Oui","Non")) {
			try {
				getEdc().deleteObject(currentParametrage());
				getEdc().saveChanges();				
				displayGroup().deleteSelection();
				updaterDisplayGroups();
				NSNotificationCenter.defaultCenter().postNotification("MAJ_COLLEGE", currentInstance());
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	protected String nomEntiteAssociee() {
		return EOCollege.ENTITY_NAME;
	}
	/**
	 * 
	 */
	protected String nomRelationPourEntiteAssociee() {
		return "college";
	}
	/** Verifie si on peut ajouter college : pour cela les inclusions de corps au minimum doivent etre definies */
	protected boolean peutAjouterRecord(EOGenericRecord record) {
		// vérifier si il existe des inclusions de corps pour ce collège
		NSArray<EOInclusionCorps> inclusions = EOInclusionCorps.rechercherInclusionsPourEntiteEtCollege(getEdc(), EOInclusionCorps.ENTITY_NAME, (EOCollege)record);
		if (inclusions == null || inclusions.count() == 0) {
			// si ce n'est pas le cas, vérifier si il existe des inclusions de contrat pour ce collège
			inclusions = EOInclusionContrat.rechercherInclusionsPourEntiteEtCollege(getEdc(), "InclusionContrat", (EOCollege)record);
			if (inclusions == null || inclusions.count() == 0) {
				EODialogs.runErrorDialog("Attention","Ce collège ne peut pas être sélectionné car les inclusions de corps ou de contrat pour ce collège ne sont pas définies");
				return false;
			} else {
				return super.peutAjouterRecord(record);
			}
		} else if (currentInstance().typeInstance().estTypeJuridDiscHu()) {
			// 23/03/2011 - vérification des collèges-sections électives
			NSArray<EOCollegeSectionElective> collegesSections = EOCollegeSectionElective.findForCollege(getEdc(), (EOCollege)record, false);
			if (collegesSections == null || collegesSections.count() == 0) {
				EODialogs.runErrorDialog("Attention","Ce type d'élection requiert que des collèges-sections électives soient définis pour ce collège");
				return false;
			} else {
				return super.peutAjouterRecord(record);
			}

		} else {
			return super.peutAjouterRecord(record);
		}
	}
	protected  void traitementsApresAjout(EOGenericRecord record) {}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce collège ?";
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("college.llCollege", EOSortOrdering.CompareAscending)));
	}
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) {
			if (!modeCreation() && currentParametrage().estActif() == false) {
				EOCollege college = ((EOParamCollege)currentParametrage()).college();
				// On vient de rendre inactif un collège, il faut rendre inactifs
				// les paramétrages de secteur pour ce collège
				NSArray paramsSecteur = EOParamSecteur.rechercherParametragesPourInstanceEtCollege(getEdc(), currentInstance(), college);
				java.util.Enumeration e = paramsSecteur.objectEnumerator();
				while (e.hasMoreElements()) {
					((ParametrageElection)e.nextElement()).setEstActif(false);
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 */
	protected void traitementsPourCreation() {
		currentParametrage().initAvecInstance(currentInstance());
	}
	protected void terminer() {
	}
	@Override
	protected NSArray sortEntiteAssociee() {
		// TODO Auto-generated method stub
		return new NSArray(new EOSortOrdering(EOParamCollege.COLLEGE_KEY + "." + EOCollege.LC_COLLEGE_KEY, EOSortOrdering.CompareAscending)); 
	}
}

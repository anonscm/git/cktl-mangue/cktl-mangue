// _GestionSections_Interface.java
// Created on 5 août 2010 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _GestionSections_Interface extends COFrame {

    /** Creates new form _GestionSections_Interface */
    public _GestionSections_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new org.cocktail.component.CODisplayGroup();
        displayGroupRegroupement = new org.cocktail.component.CODisplayGroup();
        vueBoutonsModification = new org.cocktail.component.COView();
        cOButton1 = new org.cocktail.component.COButton();
        cOButton2 = new org.cocktail.component.COButton();
        cOButton3 = new org.cocktail.component.COButton();
        listeAffichage = new org.cocktail.component.COTable();
        vueBoutonsValidation = new org.cocktail.component.COView();
        cOButton9 = new org.cocktail.component.COButton();
        cOButton10 = new org.cocktail.component.COButton();
        listeCnu = new org.cocktail.component.COTable();

        displayGroup.setEntityName("ParamSectionElective");
        displayGroup.setHasDelegate(true);
        displayGroup.setIsMainDisplayGroupForController(true);

        displayGroupRegroupement.setEntityName("RegroupementSection");

        setControllerClassName("org.cocktail.mangue.client.elections.parametrage.GestionSections");
        setSize(new java.awt.Dimension(670, 257));

        cOButton1.setActionName("ajouter");
        cOButton1.setBorderPainted(false);
        cOButton1.setEnabledMethod("modeSaisiePossible");
        cOButton1.setIconName("ajouter16.gif");

        cOButton2.setActionName("modifier");
        cOButton2.setBorderPainted(false);
        cOButton2.setEnabledMethod("boutonModificationAutorise");
        cOButton2.setIconName("modifier16.gif");

        cOButton3.setActionName("supprimer");
        cOButton3.setBorderPainted(false);
        cOButton3.setEnabledMethod("boutonModificationAutorise");
        cOButton3.setIconName("supprimer16.gif");

        org.jdesktop.layout.GroupLayout vueBoutonsModificationLayout = new org.jdesktop.layout.GroupLayout(vueBoutonsModification);
        vueBoutonsModification.setLayout(vueBoutonsModificationLayout);
        vueBoutonsModificationLayout.setHorizontalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        vueBoutonsModificationLayout.setVerticalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsModificationLayout.createSequentialGroup()
                .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        listeAffichage.setColumns(new Object[][] {{null,"sectionElective.llSectionElective",new Integer(2),"Sections Electives",new Integer(0),new Integer(469),new Integer(1000),new Integer(38)},{null,"temSelection",new Integer(0),"Sélectionnée",new Integer(0),new Integer(70),new Integer(1000),new Integer(64)}});
        listeAffichage.setDisplayGroupForTable(displayGroup);

        cOButton9.setActionName("annuler");
        cOButton9.setBorderPainted(false);
        cOButton9.setEnabledMethod("modificationEnCours");
        cOButton9.setIconName("annuler16.gif");

        cOButton10.setActionName("valider");
        cOButton10.setBorderPainted(false);
        cOButton10.setEnabledMethod("peutValider");
        cOButton10.setIconName("valider16.gif");

        org.jdesktop.layout.GroupLayout vueBoutonsValidationLayout = new org.jdesktop.layout.GroupLayout(vueBoutonsValidation);
        vueBoutonsValidation.setLayout(vueBoutonsValidationLayout);
        vueBoutonsValidationLayout.setHorizontalGroup(
            vueBoutonsValidationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsValidationLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        vueBoutonsValidationLayout.setVerticalGroup(
            vueBoutonsValidationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsValidationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(cOButton10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        listeCnu.setColumns(new Object[][] {{null,"cnu.llibelleCourt",new Integer(2),"Cnu",new Integer(0),new Integer(399),new Integer(1000),new Integer(28)}});
        listeCnu.setDisplayGroupForTable(displayGroupRegroupement);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(70, 70, 70)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 557, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(listeCnu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 419, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(vueBoutonsValidation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(1619, 1619, 1619))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(10, 10, 10)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 113, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(listeCnu, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 85, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(vueBoutonsValidation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COButton cOButton1;
    public org.cocktail.component.COButton cOButton10;
    public org.cocktail.component.COButton cOButton2;
    public org.cocktail.component.COButton cOButton3;
    public org.cocktail.component.COButton cOButton9;
    public org.cocktail.component.CODisplayGroup displayGroup;
    public org.cocktail.component.CODisplayGroup displayGroupRegroupement;
    public org.cocktail.component.COTable listeAffichage;
    public org.cocktail.component.COTable listeCnu;
    public org.cocktail.component.COView vueBoutonsModification;
    public org.cocktail.component.COView vueBoutonsValidation;
    // End of variables declaration//GEN-END:variables
                  

}

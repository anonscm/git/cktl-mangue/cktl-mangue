/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections.parametrage;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.modele.grhum.elections.EOCollegeSectionElective;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementSection;
import org.cocktail.mangue.modele.grhum.elections.EOSecteur;
import org.cocktail.mangue.modele.grhum.elections.EOSectionElective;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSectionElective;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;

//12/11/2010 - Adaptation Netbeans
//23/02/2011 - Vérification des collèges-sections électives pour les sections ajoutées
public class GestionSections extends PageParametrageElection {
	public EODisplayGroup displayGroupRegroupement;
	public EOTable listeCnu;

	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (aGroup == displayGroup()) {
			EOParamSectionElective currentParam = (EOParamSectionElective)currentParametrage();
			if (currentParam == null || currentParam.sectionElective() == null) {
				displayGroupRegroupement.setObjectArray(null);
			} else {
				displayGroupRegroupement.setObjectArray(EORegroupementSection.rechercherRegroupementsPourSection(editingContext(), currentParam.sectionElective()));
			}
			updaterDisplayGroups();
		}

		super.displayGroupDidChangeSelection(aGroup);
	}
	
	// Méthodes du controller DG
	/** Verification qu'on peut ajouter une section elective pour les &eacute;lections de type JDNPHU */
	public boolean peutAjouterRecord(EOGenericRecord record) {
		if (currentInstance().typeInstance().estTypeJuridDiscHu()) {
			// Rechercher tous les collèges-sections électives liés à cette section
			NSArray collegesSections = EOCollegeSectionElective.findForSection(editingContext(), (EOSectionElective)record, false);
			if (collegesSections == null || collegesSections.count() == 0) {
				EODialogs.runErrorDialog("Erreur","Ce type d'élection requiert que des collèges-sections électives soient définis pour cette section");
				return false;
			}
		}
		return super.peutAjouterRecord(record);
	}
	public boolean peutAjouter() {
		return super.modeSaisiePossible() && displayGroup().displayedObjects().count() == 0;
	}
	// Méthodes protégées
	protected String nomEntiteAssociee() {
		return EOSectionElective.ENTITY_NAME;
	}
	protected String nomRelationPourEntiteAssociee() {
		return EOParamSectionElective.SECTION_ELECTIVE_KEY;
	}
	protected  void traitementsApresAjout(EOGenericRecord record) {
		displayGroupRegroupement.setObjectArray(EORegroupementSection.rechercherRegroupementsPourSection(editingContext(), ((EOParamSectionElective)currentParametrage()).sectionElective()));
		updaterDisplayGroups();
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette section élective ?";
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("sectionElective.llSectionElective", EOSortOrdering.CompareAscending)));
	}
	protected void preparerFenetre() {
		GraphicUtilities.changerTaillePolice(listeCnu,11);
		GraphicUtilities.rendreNonEditable(listeCnu);
		super.preparerFenetre();
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupRegroupement.updateDisplayedObjects();
	}
	protected void terminer() {
	}
	@Override
	protected NSArray sortEntiteAssociee() {
		// TODO Auto-generated method stub
		return new NSArray(new EOSortOrdering(EOParamSectionElective.SECTION_ELECTIVE_KEY + "." + EOSectionElective.LL_SECTION_ELECTIVE_KEY, EOSortOrdering.CompareAscending)); 
	}}

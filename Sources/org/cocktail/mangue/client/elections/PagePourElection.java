/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.elections.parametrage.GestionSelectionInstance;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public abstract class PagePourElection extends ModelePageComplete {
	public EOView vueInstance;
	private EOInstance currentInstance;

	public EOEditingContext editingContext() {
		return ((ApplicationClient)ApplicationClient.sharedApplication()).getEdc();
	}
	
	// Notifications
	public void getInstance(NSNotification aNotif) {

		String nomClasse = (String)aNotif.userInfo().valueForKey("nomClasse");
		if (nomClasse.equals(this.getClass().getName())) {
			if (aNotif.object() != null) {
				currentInstance = (EOInstance)aNotif.object();
			} else {
				currentInstance = null;
			}
			preparerAffichagePourInstance();
		}
	}

	// Méthodes protégées
	protected void preparerFenetre() {
		GestionSelectionInstance controleurSelection = new GestionSelectionInstance();
		controleurSelection.initialiser(this.getClass().getName(),instanceModifiable());
		GraphicUtilities.swaperViewEtCentrer(vueInstance, controleurSelection.component());
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("getInstance", new Class[] { NSNotification.class }), GestionSelectionInstance.CHANGEMENT_INSTANCE, null);
		preparerAffichagePourInstance();

		super.preparerFenetre();
	}
	/** Methode a implementer dans les sous-classes pour signaler si l'instance est modifiable
	 * @return
	 */
	protected abstract boolean instanceModifiable();
	protected void traitementsPourCreation() {
		// Pas de création
	}
	protected EOInstance currentInstance() {
		return currentInstance;
	}
	/** prepare l'affichage apres un changement d'instance */
	protected abstract void preparerAffichagePourInstance();

	protected boolean conditionsOKPourFetch() {
		return true;
	}

	protected String messageConfirmationDestruction() {
		// pas de destrution
		return null;
	}
	protected boolean traitementsAvantValidation() {
		return true;	// pas de validation
	}
	protected boolean traitementsPourSuppression() {
		// pas de suppression
		return true;
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.elections;

import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSNotification;

/** Modelise une page pour le resultat final d'une election (parametrage final, electeurs) */
public abstract class PageFinaleElection extends ModelePageComplete {
	
	private EOInstance currentInstance;
	private boolean estActionPossible;
	
	public EOEditingContext editingContext() {
		return ((ApplicationClient)ApplicationClient.sharedApplication()).getEdc();
	}

	public void init() {
		EOArchive.loadArchiveNamed(nomArchive(), this, "org.cocktail.mangue.client.elections.interfaces", this.disposableRegistry());
		preparerFenetre();
	}
	// Accesseurs
	public boolean estActionPossible() {
		return estActionPossible;
	}
	// méthodes publiques
	public void preparerPourAction(EOInstance instance,boolean estActionPossible) {
		if (instance != null) {
			currentInstance = instance; //(EOInstance)SuperFinder.objetForGlobalIDDansEditingContext(instanceID, editingContext());
			this.estActionPossible = estActionPossible;
		} else {
			currentInstance = null;
			estActionPossible = false;
		}
		preparerAffichagePourInstance();
	}
	
	public void lockSaisie(NSNotification aNotif) {
		super.lockSaisie(aNotif);
		estActionPossible = (aNotif.object() == this);
		controllerDisplayGroup().redisplay();
	} 

	public void unlockSaisie(NSNotification aNotif) {
		super.unlockSaisie(aNotif);
		estActionPossible = true;
		controllerDisplayGroup().redisplay();
	}

	protected boolean conditionsOKPourFetch() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutUtiliserOutils();
	}
	protected void traitementsPourCreation() {
		// pas de création
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected boolean traitementsPourSuppression() {
		// Pas de suppression
		return true;
	}
	protected String messageConfirmationDestruction() {
		// pas de suppression
		return "";
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("Erreur", message);	
	}
	protected EOInstance currentInstance() {
		return currentInstance;
	}
	protected void preparerAffichagePourInstance() {
		displayGroup().setObjectArray(fetcherObjets());
		updaterDisplayGroups();
	}
	protected void terminer() {
	}
	// Méthodes privées
	private String nomArchive() {
		int index = this.getClass().getName().lastIndexOf(".");
		return this.getClass().getName().substring(index+1);
	}


}

package org.cocktail.mangue.client.budget;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.swing.ZEOTable.ZEOTableListener;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.gui.budget.SaisieBudgetView;
import org.cocktail.mangue.client.select.CodeAnalytiqueSelectCtrl;
import org.cocktail.mangue.client.select.ConventionSelectCtrl;
import org.cocktail.mangue.client.select.LolfSelectCtrl;
import org.cocktail.mangue.client.select.TypeCreditSelectCtrl;
import org.cocktail.mangue.client.select.specialisations.ElementPayeSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.paye.EOKxElement;
import org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.budget.EOCodeAnalytique;
import org.cocktail.mangue.modele.mangue.budget.EOConvention;
import org.cocktail.mangue.modele.mangue.budget.EOLolfNomenclatureDepense;
import org.cocktail.mangue.modele.mangue.budget.EOOrgan;
import org.cocktail.mangue.modele.mangue.budget.EOTypeCredit;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudget;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class SaisieBudgetCtrl extends ModelePageSaisie {

	public static SaisieBudgetCtrl sharedInstance;
	private EODisplayGroup eodLbud, eodLolf, eodCodesAnalytiques, eodConventions;

	private ListenerOrgan listenerOrgan = new ListenerOrgan();
	private ListenerLolf listenerLolf = new ListenerLolf();
	private ListenerCodeAnalytique listenerCodeAnalytique = new ListenerCodeAnalytique();
	private ListenerConvention listenerConvention = new ListenerConvention();

	protected SaisieBudgetView myView;
	protected EOPersBudget currentBudget;
	protected Integer currentExercice;
	protected EOOrgan currentOrgan;
	protected EOPersBudgetAction currentLolf;
	protected EOPersBudgetAnalytique currentCodeAnalytique;
	protected EOPersBudgetConvention currentConvention;
	protected EOTypeCredit currentTypeCredit;
	protected	EOKxElement currentElement;	
	protected LolfSelectCtrl 			mySelectorLolf;
	protected CodeAnalytiqueSelectCtrl 	mySelectorAnalytique;
	protected ConventionSelectCtrl 		mySelectorConvention;

	public SaisieBudgetCtrl(EOEditingContext edc) {

		super(edc);

		eodLbud = new EODisplayGroup();
		eodLolf = new EODisplayGroup();
		eodCodesAnalytiques = new EODisplayGroup();
		eodConventions = new EODisplayGroup();

		myView = new SaisieBudgetView(new JFrame(), true, eodLbud, eodLolf, eodCodesAnalytiques, eodConventions, EOGrhumParametres.isUseSifac());

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		mySelectorLolf = new LolfSelectCtrl(getEdc());
		mySelectorAnalytique = new CodeAnalytiqueSelectCtrl(getEdc());
		mySelectorConvention = new ConventionSelectCtrl(getEdc());

		myView.getBtnAddAction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { addAction(); } });
		myView.getBtnDelAction().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { delAction(); } });

		myView.getBtnAddCodeAnalytique().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { addCodeAnalytique(); } });
		myView.getBtnDelCodeAnalytique().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { delCodeAnalytique(); } });
		myView.getBtnAddConvention().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { addConvention(); } });
		myView.getBtnDelConvention().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { delConvention(); } });
		myView.getBtnGetTypeCredit().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { selectTypeCredit(); } });
		myView.getBtnGetElement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { selectElement(); } });
		myView.getBtnDelElement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) { delElement(); } });

		setDateListeners(myView.getTfDebut());
		setDateListeners(myView.getTfFin());

		setCurrentTypeCredit(EOTypeCredit.findDefaultTypeCredit(getEdc()));

		myView.getMyEOTable().addListener(listenerOrgan);
		myView.getMyEOTableLolf().addListener(listenerLolf);
		myView.getMyEOTableAnalytique().addListener(listenerCodeAnalytique);
		myView.getMyEOTableConvention().addListener(listenerConvention);
		myView.getTfFiltre().getDocument().addDocumentListener(new ADocumentListener());
		CocktailUtilities.setFocusOn(myView.getTfDebut());

		CocktailUtilities.initTextField(myView.getTfLibelleElement(), false, false);

		CocktailUtilities.setTextToLabel(myView.getLblLolf(), (EOGrhumParametres.isUseSifac())?"Domaine fonctionnel":"Actions LOLF");
		
		myView.getPanelCodeAnalytique().setVisible(!EOGrhumParametres.isUseSifac());
		myView.getPanelConventions().setVisible(!EOGrhumParametres.isUseSifac());
		
	}
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static SaisieBudgetCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new SaisieBudgetCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 *
	 */
	private void initOrgan()	{

		NSMutableArray<EOSortOrdering> mySort = new NSMutableArray<EOSortOrdering>();

		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_UB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOOrgan.ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending));

		eodLbud.setSortOrderings(mySort);

		NSMutableArray<EOOrgan> organs = new NSMutableArray<EOOrgan>();

		if (getPeriodeDebut() == null)
			eodLbud.setObjectArray(new NSArray<EOOrgan>());
		else {

//			if (eodLbud.displayedObjects().size() == 0) {

				organs.addObjectsFromArray(EOOrgan.rechercherLignesBudgetairesPourPeriodeEtNiveau(getEdc(), getPeriodeDebut(), getPeriodeFin(), EOOrgan.NIVEAU_CR));
				organs.addObjectsFromArray(EOOrgan.rechercherLignesBudgetairesPourPeriodeEtNiveau(getEdc(), getPeriodeDebut(), getPeriodeFin(), EOOrgan.NIVEAU_SOUS_CR));

				eodLbud.setObjectArray(organs);
//			}
		}

		filter();
	}

	public EOPersBudget getCurrentBudget() {
		return currentBudget;
	}



	public void setCurrentBudget(EOPersBudget currentBudget) {
		this.currentBudget = currentBudget;
		updateDatas();
	}

	public Integer getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(Integer currentExercice) {
		this.currentExercice = currentExercice;
	}

	public EOOrgan getCurrentOrgan() {
		return currentOrgan;
	}

	public void setCurrentOrgan(EOOrgan currentOrgan) {
		this.currentOrgan = currentOrgan;
		CocktailUtilities.viderLabel(myView.getLblLbud());
		if (currentOrgan != null) {
			CocktailUtilities.setTextToLabel(myView.getLblLbud(), currentOrgan.orgLib());		
		}
	}

	public EOTypeCredit getCurrentTypeCredit() {
		return currentTypeCredit;
	}
	public void setCurrentTypeCredit(EOTypeCredit currentTypeCredit) {
		this.currentTypeCredit = currentTypeCredit;
		CocktailUtilities.viderTextField(myView.getTfTypeCredit());
		if (currentTypeCredit != null) {
			CocktailUtilities.setTextToField(myView.getTfTypeCredit(), currentTypeCredit.code());		
		}
	}

	public EOPersBudgetAction getCurrentLolf() {
		return currentLolf;
	}
	public void setCurrentLolf(EOPersBudgetAction currentLolf) {
		this.currentLolf = currentLolf;
	}
	public EOPersBudgetAnalytique getCurrentCodeAnalytique() {
		return currentCodeAnalytique;
	}
	public void setCurrentCodeAnalytique(EOPersBudgetAnalytique currentCodeAnalytique) {
		this.currentCodeAnalytique = currentCodeAnalytique;
	}
	public EOPersBudgetConvention getCurrentConvention() {
		return currentConvention;
	}
	public void setCurrentConvention(EOPersBudgetConvention currentConvention) {
		this.currentConvention = currentConvention;
	}
	public NSTimestamp getPeriodeDebut() {
		return CocktailUtilities.getDateFromField(myView.getTfDebut());
	}
	public NSTimestamp getPeriodeFin() {
		return CocktailUtilities.getDateFromField(myView.getTfFin());
	}	
	public EOKxElement getCurrentElement() {
		return currentElement;
	}
	public void setCurrentElement(EOKxElement currentElement) {
		this.currentElement = currentElement;
		CocktailUtilities.viderTextField(myView.getTfLibelleElement());
		if (currentElement != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleElement(), currentElement.codeEtLibelle());		
		}
	}

	private void clearFilters() {
		CocktailUtilities.viderTextField(myView.getTfFiltre());
	}

	/**
	 * 
	 * @param budget
	 */
	public void modifier(EOPersBudget budget, boolean isModeCreation) {

		clearFilters();
		setModeCreation(isModeCreation);

		if (budget.dateDebut() != null) {
			setCurrentExercice(new Integer(DateCtrl.getYear(budget.dateDebut())));
		} else {
			setCurrentExercice(new Integer(DateCtrl.getCurrentYear()));
		}

		setCurrentBudget(budget);

		initOrgan();

		CocktailUtilities.setFocusOn(myView.getTfDebut());
		myView.setVisible(true);
	}

	/**
	 * 
	 */
	private void addAction() {

		CRICursor.setWaitCursor(myView);

		EOLolfNomenclatureDepense action = mySelectorLolf.getLolf(getPeriodeDebut());
		if (action != null) {

			BigDecimal pourcentageActuel = CocktailUtilities.computeSumForKey(eodLolf, EOPersBudgetAction.POURCENTAGE_KEY);
			BigDecimal pourcentage = ManGUEConstantes.QUOTITE_100.subtract(pourcentageActuel);

			eodLolf.insertObjectAtIndex(EOPersBudgetAction.creer(getEdc(), getCurrentBudget(), action, pourcentage), 0);
			myView.getMyEOTableLolf().updateData();				

		}

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void delAction() {
		if(EODialogs.runConfirmOperationDialog("Attention", "Confirmez-vous la suppression de l'action ?", "Oui", "Non"))		 {
			getEdc().deleteObject(getCurrentLolf());
			eodLolf.deleteSelection();
			myView.getMyEOTableLolf().updateData();
		}
	}

	/**
	 * 
	 */
	private void addCodeAnalytique() {

		CRICursor.setWaitCursor(myView);

		EOCodeAnalytique code = mySelectorAnalytique.getCodeAnalytique(getCurrentOrgan(), getPeriodeDebut(), getPeriodeFin());
		if (code != null) {

			BigDecimal pourcentageActuel = CocktailUtilities.computeSumForKey(eodCodesAnalytiques, EOPersBudgetAnalytique.POURCENTAGE_KEY);
			BigDecimal pourcentage = ManGUEConstantes.QUOTITE_100.subtract(pourcentageActuel);

			eodCodesAnalytiques.insertObjectAtIndex(EOPersBudgetAnalytique.creer(getEdc(), getCurrentBudget(), code, pourcentage), 0);
			myView.getMyEOTableAnalytique().updateData();				

		}

		CRICursor.setDefaultCursor(myView);

	}
	private void delCodeAnalytique() {

		if(EODialogs.runConfirmOperationDialog("Attention", 
				"Confirmez-vous la suppression du code analytique ?", "Oui", "Non"))		 {

			getEdc().deleteObject(getCurrentCodeAnalytique());
			eodCodesAnalytiques.deleteSelection();
			myView.getMyEOTableAnalytique().updateData();
		}
	}

	/**
	 * 
	 */
	private void addConvention() {

		CRICursor.setWaitCursor(myView);

		EOConvention convention = mySelectorConvention.getConvention(getCurrentExercice(), getCurrentOrgan(), getCurrentTypeCredit());
		if (convention != null) {

			BigDecimal pourcentageActuel = CocktailUtilities.computeSumForKey(eodConventions, EOPersBudgetConvention.POURCENTAGE_KEY);
			BigDecimal pourcentage = ManGUEConstantes.QUOTITE_100.subtract(pourcentageActuel);

			eodConventions.insertObjectAtIndex(EOPersBudgetConvention.creer(getEdc(), getCurrentBudget(), convention, pourcentage), 0);
			myView.getMyEOTableConvention().updateData();				

		}

		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void delConvention() {

		if(EODialogs.runConfirmOperationDialog("Attention", "Confirmez-vous la suppression de la convention ?", "Oui", "Non"))		 {

			getEdc().deleteObject(getCurrentConvention());
			eodConventions.deleteSelection();
			myView.getMyEOTableConvention().updateData();

		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderLabel(myView.getLblMessage());
		CocktailUtilities.viderLabel(myView.getLblLbud());
		CocktailUtilities.viderTextField(myView.getTfLibelleElement());

	}

	/**
	 * 
	 */
	public void selectTypeCredit() {

		EOTypeCredit typeCredit = TypeCreditSelectCtrl.sharedInstance(getEdc()).getTypeCredit(getCurrentExercice(), false);
		if (typeCredit != null) {
			setCurrentTypeCredit(typeCredit);
		}
	}

	/**
	 * 
	 */
	public void selectElement() {

		INomenclature element = ElementPayeSelectCtrl.sharedInstance(getEdc()).getObject(getPeriodeDebut());
		if (element != null) {
			setCurrentElement((EOKxElement)element);
		}
	}
	public void delElement() {
		setCurrentElement(null);
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub

		clearDatas();

		if (getCurrentBudget() != null) {

			setCurrentElement(getCurrentBudget().toElementPaye());
			CocktailUtilities.setDateToField(myView.getTfDebut(), getCurrentBudget().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfFin(), getCurrentBudget().dateFin());
			CocktailUtilities.setNumberToField(myView.getTfPourcentage(), getCurrentBudget().pbudPourcentage());

			eodLbud.setSelectedObject(getCurrentBudget().toOrgan());

			if (!isModeCreation()) {
				eodLolf.setObjectArray(EOPersBudgetAction.findForBudget(getEdc(), getCurrentBudget()));
				eodCodesAnalytiques.setObjectArray(EOPersBudgetAnalytique.findForBudget(getEdc(), getCurrentBudget()));
				eodConventions.setObjectArray(EOPersBudgetConvention.findForBudget(getEdc(), getCurrentBudget()));
			}
			else {
				eodLolf.setObjectArray(new NSArray<EOPersBudgetAction>());
				eodCodesAnalytiques.setObjectArray(new NSArray<EOPersBudgetAnalytique>());
				eodConventions.setObjectArray(new NSArray<EOPersBudgetConvention>());				
			}

			myView.getMyEOTable().updateData();
			myView.getMyEOTableLolf().updateData();
			myView.getMyEOTableAnalytique().updateData();
			myView.getMyEOTableConvention().updateData();

		}

		updateInterface();
	}

	@Override
	protected void updateInterface() {

		myView.getBtnValider().setEnabled(getPeriodeDebut() != null && getCurrentOrgan() != null);
		myView.getBtnGetTypeCredit().setEnabled(getPeriodeDebut() != null);
		myView.getBtnGetElement().setEnabled(getPeriodeDebut() != null && getCurrentBudget().toEmploi() == null);

		myView.getBtnAddAction().setEnabled(getPeriodeDebut() != null);
		myView.getBtnAddCodeAnalytique().setEnabled(getPeriodeDebut() != null);
		myView.getBtnAddConvention().setEnabled(getPeriodeDebut() != null);

		myView.getBtnDelAction().setEnabled(getCurrentLolf() != null);
		myView.getBtnDelCodeAnalytique().setEnabled(getCurrentCodeAnalytique() != null);
		myView.getBtnDelConvention().setEnabled(getCurrentConvention() != null);
		myView.getBtnDelElement().setEnabled(getCurrentElement() != null);

	}

	@Override
	protected void traitementsAvantValidation() {

		if (!EOGrhumParametres.isUseSifac()) {
			if (getCurrentTypeCredit() == null) {
				throw new NSValidation.ValidationException("Le type de crédit est obligatoire !");
			}
		}

		// TODO Auto-generated method stub
		getCurrentBudget().setDateDebut(getPeriodeDebut());
		getCurrentBudget().setDateFin(getPeriodeFin());
		getCurrentBudget().setToOrganRelationship(getCurrentOrgan());
		getCurrentBudget().setToOrganRelationship(getCurrentOrgan());
		getCurrentBudget().setToElementPayeRelationship(getCurrentElement());
		getCurrentBudget().setToTypeCreditRelationship(getCurrentTypeCredit());
		getCurrentBudget().setPbudPourcentage(CocktailUtilities.getBigDecimalFromField(myView.getTfPourcentage()));

		EOMois moisDebut = EOMois.getMoisForDate(getEdc(), getPeriodeDebut());
		EOMois moisFin = EOMois.getMoisForDate(getEdc(), getPeriodeFin());

		getCurrentBudget().setToMoisDebutRelationship(moisDebut);
		getCurrentBudget().setToMoisFinRelationship(moisFin);

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		ServerProxy.clientSideRequestRevert(getEdc());
		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {

		CocktailUtilities.viderLabel(myView.getLblMessage());
		if (getPeriodeDebut() != null) {

			if ( (currentExercice.intValue() != new Integer(DateCtrl.getYear(getPeriodeDebut())).intValue())
					|| eodLbud.displayedObjects().size() == 0) {
				initOrgan();				
			}
			setCurrentExercice(new Integer(DateCtrl.getYear(getPeriodeDebut())));
		}
		else
			setCurrentExercice(null);

		updateInterface();
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerOrgan implements ZEOTableListener {

		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentOrgan((EOOrgan)eodLbud.selectedObject());
			updateInterface();
		}
	}
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerLolf implements ZEOTableListener {

		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentLolf((EOPersBudgetAction)eodLolf.selectedObject());
			updateInterface();
		}
	}
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerCodeAnalytique implements ZEOTableListener {

		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentCodeAnalytique((EOPersBudgetAnalytique)eodCodesAnalytiques.selectedObject());
			updateInterface();
		}
	}
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerConvention implements ZEOTableListener {

		public void onDbClick() {
		}
		public void onSelectionChanged() {
			setCurrentConvention((EOPersBudgetConvention)eodConventions.selectedObject());
			updateInterface();
		}
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier getFilterQualifier()	{

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		if (!StringCtrl.chaineVide(myView.getTfFiltre().getText()))	{
			NSArray args = new NSArray("*"+myView.getTfFiltre().getText()+"*");

			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY + " caseInsensitiveLike %@",args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_CR_KEY + " caseInsensitiveLike %@",args));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_SOUSCR_KEY + " caseInsensitiveLike %@",args));

			if (EOGrhumParametres.isUseSifac()) {
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_LIB_KEY + " caseInsensitiveLike %@",args));
			}

			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return new EOAndQualifier(andQualifiers);       

	}
	/** 
	 *
	 */
	private void filter()	{

		eodLbud.setQualifier(getFilterQualifier());
		eodLbud.updateDisplayedObjects();              
		myView.getMyEOTable().updateData();

	}

	/**
	 * Permet de definir un listener sur le contenu du champ texte qui sert a filtrer la table. 
	 * Des que le contenu du champ change, on met a jour le filtre.
	 * 
	 */	
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}
}
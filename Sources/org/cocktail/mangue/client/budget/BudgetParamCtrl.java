package org.cocktail.mangue.client.budget;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.budget.BudgetParamView;
import org.cocktail.mangue.client.select.OrganListeSelectCtrl;
import org.cocktail.mangue.modele.mangue.budget.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class BudgetParamCtrl {

	private static BudgetParamCtrl sharedInstance;

	private EOEditingContext ec;
	private BudgetParamView myView;

	private EODisplayGroup eodOrgan;

	public BudgetParamCtrl(EOEditingContext edc)	{

		super();

		this.ec = edc;

		eodOrgan = new EODisplayGroup();
		myView= new BudgetParamView(new JFrame(), true, eodOrgan);

		//		myView.getBtnAddOrgan().addActionListener(new java.awt.event.ActionListener() {
		//			public void actionPerformed(java.awt.event.ActionEvent evt) {
		//				selectOrgan();
		//			}
		//		});
		//
		//		myView.getBtnDelOrgan().addActionListener(new java.awt.event.ActionListener() {
		//			public void actionPerformed(java.awt.event.ActionEvent evt) {
		//				delOrgan();
		//			}
		//		});

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static BudgetParamCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new BudgetParamCtrl(editingContext);
		return sharedInstance;
	}


	/**
	 * 
	 *
	 */
	public void open()	{
		updateDatas();
		myView.setVisible(true);
	}


	/**
	 * 
	 */
	public void updateDatas()	{

		//		 eodOrgan.setObjectArray(EOBudgetParam.findParams(getSelectedExercice(), "ORGAN"));
		//		 myView.getMyEOTableOrgan().updateData();

	}


	/**
	 * 
	 */
	public void selectOrgan()	{

		NSArray<EOOrgan> organs = OrganListeSelectCtrl.sharedInstance(ec).getOrgans(null, null, null);

		if (organs != null && organs.size() > 0) {

			for (EOOrgan organ : organs) {
				//
				//				// EOBudgetParam existingParam = EOBudgetParam.findParamForOrgan(ec, getSelectedExercice(), organ);
				//
				//				 if (existingParam == null)	{
				////					 EOBudgetParam newParam = EOBudgetParam.creer();
				//
				////					 newParam.setToExerciceRelationship(getSelectedExercice());
				////					 newParam.setToOrganRelationship(organ);
				////
				////					 ec.insertObject(newParam);					
			}
		}
	}


	/**
	 * 
	 * @param sender
	 */
	public void delOrgan()	{

		//		 for (EOBudgetParam param : (NSArray<EOBudgetParam()>eodOrgan.displayedObjects())) {
		//			 ec.deleteObject(param);
		//		 }
		//		 if (ec.deletedObjects().count() > 0)	{
		//			 ec.saveChanges();
		//			 eodOrgan.deleteSelection();
		//			 myView.getMyEOTableOrgan().updateData();
		//		 }		
	}
}
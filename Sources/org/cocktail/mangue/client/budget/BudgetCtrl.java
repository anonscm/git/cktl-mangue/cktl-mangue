package org.cocktail.mangue.client.budget;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTable;

import org.cocktail.application.client.swing.ZEOTable;
import org.cocktail.application.client.swing.ZEOTableCellRenderer;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.budget.BudgetView;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudget;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique;
import org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSValidation.ValidationException;

public class BudgetCtrl {

	public final static String TYPE_GESTION_EMPLOIS = "EMPLOI";
	public final static String TYPE_GESTION_AGENT = "AGENT";
	public final static String TYPE_GESTION_PAYE = "PAYE";

	private static Boolean MODE_MODAL = Boolean.TRUE;
	private EOEditingContext edc;
	private BudgetView myView;

	private ListenerBudget 		listenerBudget = new ListenerBudget();
	private ListenerAction 		listenerAction = new ListenerAction();
	private ListenerCanal 		listenerCanal = new ListenerCanal();
	private ListenerConvention 	listenerConvention = new ListenerConvention();

	private boolean peutGererModule;

	private EODisplayGroup eodBudget, eodAction, eodCanal, eodConvention;
	private MyRenderer	monRendererColor = new MyRenderer();

	private EOPersBudget currentBudget;
	private EOPersBudgetAction currentAction;
	private EOPersBudgetAnalytique currentCanal;
	private EOPersBudgetConvention currentConvention;

	private EOIndividu currentIndividu;
	private IEmploi currentEmploi;

	private String typeGestion;
	private SaisieBudgetCtrl myCtrlSaisie;

	/**
	 * 
	 * @param edc
	 */
	public BudgetCtrl(EOEditingContext edc) {

		this.edc = edc;

		eodBudget = new EODisplayGroup();
		eodAction = new EODisplayGroup();
		eodCanal = new EODisplayGroup();
		eodConvention = new EODisplayGroup();

		myView = new BudgetView(null, MODE_MODAL.booleanValue(),eodBudget, eodAction, eodCanal, eodConvention, monRendererColor, EOGrhumParametres.isUseSifac());

		myCtrlSaisie = new SaisieBudgetCtrl(edc);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnRenouveler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {renouveler();}}
				);
		myView.getBtnClose().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {myView.setVisible(false);}}
				);

		myView.getMyEOTableBudget().addListener(listenerBudget);
		myView.getMyEOTableActions().addListener(listenerAction);
		myView.getMyEOTableCanal().addListener(listenerCanal);
		myView.getMyEOTableConventions().addListener(listenerConvention);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		setTypeGestion(TYPE_GESTION_AGENT);

		myView.getPopupEtat().addActionListener(new FiltreBudgetListener());
		myView.getCheckLignePrincipale().addActionListener(new FiltreBudgetListener());
		myView.getCheckElementSpecifique().addActionListener(new FiltreBudgetListener());
		myView.getCheckEmploi().addActionListener(new FiltreBudgetListener());

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		CocktailUtilities.initTextField(myView.getTfLbudDetail(), false, false);

		myView.getPopupEtat().setSelectedIndex(1);
		myView.getCheckLignePrincipale().setSelected(true);
		myView.getCheckElementSpecifique().setSelected(true);

		updateInterface();
		updateMessages();

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));
		}

	}

	/**
	 * 
	 * @param individu
	 */
	public void open(EOIndividu individu){
		myView.setTitle(individu.identitePrenomFirst() + " - INFORMATIONS BUDGETAIRES");
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}
	public void open(IEmploi emploi) {
		myView.setTitle("EMPLOI " + emploi.getNoEmploiAffichage() + " - INFORMATIONS BUDGETAIRES");
		setCurrentEmploi(emploi);
		myView.setVisible(true);
	}
	private boolean peutGererModule() {
		return peutGererModule;
	}
	private void setPeutGererModule(boolean yn) {
		peutGererModule = yn;
	}

	public void employeHasChanged(NSNotification  notification) {

		if (notification != null && notification.object() != null) {
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc,(Number)notification.object(), false));
		}
	}

	/**
	 * 
	 * @param notification
	 */
	public void rafraichirBudgetAgent(NSNotification  notification) {
		updateDatas();
	}
	/**
	 * 
	 * @param notification
	 */
	public void rafraichirBudgetEmploi(NSNotification  notification) {
		updateDatas();
	}

	/**
	 * 
	 * @return
	 */
	public String getTypeGestion() {
		return typeGestion;
	}
	public void setTypeGestion(String typeGestion) {
		this.typeGestion = typeGestion;
		if (isGestionAgent()) {
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("rafraichirBudgetAgent",new Class[] {NSNotification.class}), "NotifRafraichirBudget",null);
		}
		else {
			if (isGestionEmploi()) {
				myView.getCheckEmploi().setEnabled(false);
				myView.getCheckEmploi().setSelected(true);
				NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("rafraichirBudgetEmploi",new Class[] {NSNotification.class}), "NotifRafraichirBudget",null);
			}			
		}
	}

	/**
	 * 
	 * @param notification
	 */
	public void nettoyerChamps(NSNotification notification) {
		setCurrentIndividu(null);
	}

	/**
	 * Gestion des droits en fonction de l'utilisateur connecte
	 * @param currentUtilisateur
	 */
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		setPeutGererModule(currentUtilisateur.peutGererBudget());
	}

	public EOPersBudget getCurrentBudget() {
		return currentBudget;
	}

	public void setCurrentBudget(EOPersBudget currentBudget) {
		this.currentBudget = currentBudget;
		CocktailUtilities.viderTextField(myView.getTfLbudDetail());
		if (currentBudget != null && currentBudget.toOrgan() != null) {
			CocktailUtilities.setTextToField(myView.getTfLbudDetail(), currentBudget.toOrgan().libelleLbudAvecId());
		}
	}

	public EOPersBudgetAction getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(EOPersBudgetAction currentAction) {
		this.currentAction = currentAction;
	}

	public EOPersBudgetAnalytique getCurrentCanal() {
		return currentCanal;
	}

	public void setCurrentCanal(EOPersBudgetAnalytique currentCanal) {
		this.currentCanal = currentCanal;
	}

	public EOPersBudgetConvention getCurrentConvention() {
		return currentConvention;
	}

	public void setCurrentConvention(EOPersBudgetConvention currentConvention) {
		this.currentConvention = currentConvention;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		updateDatas();
	}

	public IEmploi getCurrentEmploi() {
		return currentEmploi;
	}

	public void setCurrentEmploi(IEmploi currentEmploi) {
		this.currentEmploi = currentEmploi;
		updateDatas();
	}

	private boolean isGestionEmploi() {
		return typeGestion!= null && typeGestion.equals(TYPE_GESTION_EMPLOIS);
	}
	private boolean isGestionAgent() {
		return typeGestion!= null && typeGestion.equals(TYPE_GESTION_AGENT);
	}

	/**
	 * 
	 */
	public void updateDatas() {

		NSMutableArray<EOPersBudget> budgets = new NSMutableArray<EOPersBudget>();

		if (myView.getCheckLignePrincipale().isSelected() && isGestionAgent()) {
			budgets.addObjectsFromArray(EOPersBudget.findForIndividu(edc, getCurrentIndividu()));
		}
		if (myView.getCheckElementSpecifique().isSelected() && isGestionAgent()) {
			budgets.addObjectsFromArray(EOPersBudget.findForIndividuEtElements(edc, getCurrentIndividu()));
		}
		if (myView.getCheckEmploi().isSelected()) {
			if (isGestionEmploi()) {
				budgets.addObjectsFromArray(EOPersBudget.findForEmploi(edc, getCurrentEmploi()));
			}
			else {
				NSArray<EOOccupation> occupationsCourante = EOOccupation.findForIndividuAndDate(edc, getCurrentIndividu(), DateCtrl.today());
				if (occupationsCourante.size() > 0) {
					budgets.addObjectsFromArray(EOPersBudget.findForEmploi(edc, occupationsCourante.get(0).toEmploi() ));
				}
			}
		}

		if (myView.getPopupEtat().getSelectedIndex() > 0) {			
			EOQualifier.filterArrayWithQualifier(budgets, EOPersBudget.getQualifierDate(DateCtrl.today()));
		}

		eodBudget.setObjectArray(budgets.immutableClone());
		myView.getMyEOTableBudget().updateData();
		updateInterface();
	}

	public JDialog getView() {
		return myView;
	}

	public JPanel getViewBudget() {
		return myView.getViewBudget();
	}

	/**
	 * 
	 */
	public void ajouter() {

		EOPersBudget newBudget = null;

		if (isGestionAgent()) {
			newBudget = EOPersBudget.creer(edc, getCurrentIndividu(), ManGUEConstantes.QUOTITE_100);
		} else if (isGestionEmploi()) {
			newBudget = EOPersBudget.creer(edc, getCurrentEmploi(), ManGUEConstantes.QUOTITE_100);
		}

		myCtrlSaisie.modifier(newBudget, true);

		updateDatas();

		CocktailUtilities.forceSelection (myView.getMyEOTableBudget(), new NSArray(newBudget));
	}

	/**
	 * 
	 */
	public void modifier() {

		CRICursor.setWaitCursor(myView);

		myCtrlSaisie.modifier(getCurrentBudget(), false);
		myView.getMyEOTableBudget().updateUI();
		listenerBudget.onSelectionChanged();

		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette ligne budgétaire ?", "Oui", "Non"))		
			return;

		try {

			for (EOPersBudgetAction myAction : (NSArray<EOPersBudgetAction>)getCurrentBudget().toListeActions()) {
				edc.deleteObject(myAction);
			}
			for (EOPersBudgetAnalytique myCanal : (NSArray<EOPersBudgetAnalytique>)getCurrentBudget().toListeAnalytiques()) {
				edc.deleteObject(myCanal);
			}
			for (EOPersBudgetConvention myConvention: (NSArray<EOPersBudgetConvention>)getCurrentBudget().toListeConventions()) {
				edc.deleteObject(myConvention);
			}

			edc.deleteObject(getCurrentBudget());

			edc.saveChanges();
			eodBudget.deleteSelection();
			myView.getMyEOTableBudget().updateData();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			edc.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			edc.revert();
		}
	}

	/**
	 * 
	 */
	private void renouveler() {
		EOPersBudget newBudget = null;

		if (isGestionAgent()) {
			newBudget = EOPersBudget.renouveler(edc, getCurrentBudget(), getCurrentIndividu());
		} else if (isGestionEmploi()) {
			newBudget = EOPersBudget.renouveler(edc, getCurrentBudget(), getCurrentEmploi());
		}

		myCtrlSaisie.modifier(newBudget, true);

		updateDatas();

		CocktailUtilities.forceSelection (myView.getMyEOTableBudget(), new NSArray(newBudget));
	}

	/**
	 * 
	 */
	private void updateMessages() {

		String msgLolf = messageActionsLolf();
		String msgAnalytique = messageCodeAnalytiques();
		String msgConvention = messageConvention();

		myView.getLblMsgLolf().setVisible(msgLolf.length() > 0);
		myView.getLblMsgAnalytique().setVisible(msgAnalytique.length() > 0);
		myView.getLblMsgConvention().setVisible(msgConvention.length() > 0);

		myView.getLblMsgLolf().setText(msgLolf);
		myView.getLblMsgAnalytique().setText(msgAnalytique);
		myView.getLblMsgConvention().setText(msgConvention);

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerBudget implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentBudget() != null && peutGererModule())
				modifier();
		}
		public void onSelectionChanged() {

			setCurrentBudget((EOPersBudget)eodBudget.selectedObject());

			eodAction.setObjectArray(new NSArray<EOPersBudgetAction>());
			eodCanal.setObjectArray(new NSArray<EOPersBudgetAnalytique>());
			eodConvention.setObjectArray(new NSArray<EOPersBudgetConvention>());

			if (getCurrentBudget() != null) {

				eodAction.setObjectArray(getCurrentBudget().toListeActions());
				eodCanal.setObjectArray(getCurrentBudget().toListeAnalytiques());
				eodConvention.setObjectArray(getCurrentBudget().toListeConventions());

			}

			myView.getMyEOTableActions().updateData();
			myView.getMyEOTableCanal().updateData();
			myView.getMyEOTableConventions().updateData();

			updateInterface();

		}
	}

	/**
	 * 
	 * @return
	 */
	public String messageActionsLolf() {
		if (getCurrentBudget() != null) {

			if (eodAction.displayedObjects().count() == 0) {
				return "Une ou plusieurs actions LOLF sont nécessaires pour la synchronisation avec la paye ";
			}

			if (eodAction.displayedObjects().count() > 0 
					&& CocktailUtilities.computeSumForKey(eodAction, EOPersBudgetAction.POURCENTAGE_KEY).floatValue() != 100) {
				return "La somme des % des actions LOLF doit être de 100%";
			}
		}

		return "";
	}
	public String messageCodeAnalytiques() {
		if (getCurrentBudget() != null 
				&& eodCanal.displayedObjects().count() > 0 
				&& CocktailUtilities.computeSumForKey(eodCanal, EOPersBudgetAnalytique.POURCENTAGE_KEY).floatValue() != 100)
			return "La somme des % des codes analytiques doit être de 100%";

		return "";
	}
	public String messageConvention() {
		if (getCurrentBudget() != null 
				&& eodConvention.displayedObjects().count() > 0 
				&& CocktailUtilities.computeSumForKey(eodConvention, EOPersBudgetConvention.POURCENTAGE_KEY).floatValue() != 100)
			return "La somme des % des conventions doit être de 100%";

		return "";
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerAction implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentAction() != null && peutGererModule())
				modifier();
		}

		public void onSelectionChanged() {
			setCurrentAction((EOPersBudgetAction)eodAction.selectedObject());
		}
	}
	private class ListenerCanal implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentCanal() != null && peutGererModule())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentCanal((EOPersBudgetAnalytique)eodCanal.selectedObject());
		}
	}
	private class ListenerConvention implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentConvention() != null && peutGererModule())
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentConvention((EOPersBudgetConvention)eodConvention.selectedObject());
		}
	}

	private boolean peutAjouter() {
		return getCurrentIndividu() != null || getCurrentEmploi() != null;
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getPanelCodesAnalytiques().setVisible(eodCanal.displayedObjects().size() > 0);
		myView.getPanelConventions().setVisible(eodConvention.displayedObjects().size() > 0);

		myView.getBtnAjouter().setEnabled(peutAjouter());
		myView.getBtnModifier().setEnabled(getCurrentBudget() != null && (getCurrentBudget().toEmploi() == null || isGestionEmploi() ));
		myView.getBtnSupprimer().setEnabled(getCurrentBudget() != null  && (getCurrentBudget().toEmploi() == null || isGestionEmploi() ));
		myView.getBtnRenouveler().setEnabled(getCurrentBudget() != null
				&& getCurrentBudget().toEmploi() == null
				&& getCurrentBudget().dateFin() != null);

		myView.getCheckLignePrincipale().setVisible(isGestionAgent());
		myView.getCheckElementSpecifique().setVisible(isGestionAgent());

		updateMessages();

	}

	/**
	 * Classe servant a colorer les cellules selon l'etat de l'engagement
	 */
	private class MyRenderer extends ZEOTableCellRenderer	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2907930349355563787L;

		/** 
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)	{
			Component leComposant = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected)
				return leComposant;

			final int mdlRow = ((ZEOTable)table).getRowIndexInModel(row);
			final EOPersBudget obj = (EOPersBudget) ((ZEOTable)table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);           

			if (obj.toElementPaye() != null)
				leComposant.setBackground(myView.getCheckElementSpecifique().getBackground());
			else
				if (obj.toIndividu() != null)
					leComposant.setBackground(myView.getCheckLignePrincipale().getBackground());
				else
					if (obj.toEmploi() != null)
						leComposant.setBackground(myView.getCheckEmploi().getBackground());

			if (obj.dateFin() != null && DateCtrl.isBefore(obj.dateFin(), DateCtrl.today()))
				leComposant.setForeground(new Color(102, 102, 102));
			else
				leComposant.setForeground(new Color(0,0,0));

			return leComposant;
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class FiltreBudgetListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){
			updateDatas();
		}
	}


}
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.ObjetAvecValidite;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSArray;

public abstract class GestionAvecValidite extends ModelePageComplete {
	private String typeValidite;
	protected final static String TYPE_VALIDE = "Valides";
	protected final static String TYPE_EN_COURS = "En cours";		// Utilisé pour l'attribution des primes
	protected final static String TYPE_HISTORIQUE = "Historique";
	private boolean preparationFenetre = true;
	
	// Accesseurs
	public String typeValidite() {
		return typeValidite;
	}
	public void setTypeValidite(String typeValidite) {
		if (preparationFenetre) {
			return;
		}
		this.typeValidite = typeValidite;
		if (typeValidite != null) {
			parametrerDisplayGroup();
			preparerDisplayGroup();
		} else {
			displayGroup().setObjectArray(null);
		}
		updaterDisplayGroups();
	}
	// Méthodes du controller DG
	/** On n'autorise la saisie de primes que si le type de validit&eacute; est l'affichage des primes valides */
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() &&  typeValidite() != null && (typeValidite().equals(TYPE_VALIDE) || typeValidite().equals(TYPE_EN_COURS));
	}
	public boolean popupActif() {
		return super.modeSaisiePossible();
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		preparationFenetre = true;
		super.preparerFenetre();
		preparationFenetre = false;
		setTypeValidite(TYPE_VALIDE);
	}
	protected void traitementsPourCreation() {
		currentObjet().init();
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR", message);
	}
	/** true si l'agent peut g&eacute;rer les primes */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutGererPrimes() && agent.peutAdministrer();
	}
	protected boolean conditionsOKPourFetch() {
		return true;
	}
	protected void parametrerDisplayGroup() {
		if (typeValidite() != null) {
			displayGroup().setQualifier(ObjetAvecValidite.qualifierPourValidite(typeValidite().equals(TYPE_VALIDE)));
		} else {
			displayGroup().setQualifier(null);
		}
	}
	protected NSArray fetcherObjets() {		
		return null;
	}
	/** Retourne les objets conforme &agrave; la validit&eacute; lorsque le popup de validit&eacute; est modifi&eacute;.
	 *  A surcharger par les sous-classes */
	protected  NSArray objetsPourValidite(boolean estValide) {
		if (estValide) {
			return ObjetAvecValidite.rechercherObjetsValides(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName());
		} else {
			return ObjetAvecValidite.rechercherObjetsInvalides(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName());
		}
	}
	protected boolean traitementsAvantValidation() {
		// Pas de traitement spécifique
		return true;
	}
	protected boolean traitementsPourSuppression() {
		currentObjet().invalider();
		return true;
	}
	protected void preparerDisplayGroup() {
		displayGroup().setObjectArray(objetsPourValidite(typeValidite.equals(TYPE_VALIDE)));
	}
	// Méthodes privées
	private ObjetAvecValidite currentObjet() {
		return (ObjetAvecValidite)displayGroup().selectedObject();
	}
}

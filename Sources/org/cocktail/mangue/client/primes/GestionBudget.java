/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import java.math.BigDecimal;

import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPrimeBudget;
import org.cocktail.mangue.modele.goyave.EOPrimeExercice;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSTimestamp;

/** Pour un exercice donn&eacute;, il ne peut y avoir qu'un seul budget global */
// 29/03/2010 - Bug lors de la suppression d'un budget
// 21/12/2010 - Adaptation Netbeans + corrections de bugs dans la suppression des budgets
public class GestionBudget extends ModelePageComplete {
	public EODisplayGroup displayGroupDetail;
	public EOTable listePrimes;
	private int exerciceCourant;
	private BigDecimal oldMontantCredit;
	private boolean choixPrime;

	// Accesseurs
	public int exerciceCourant() {
		return exerciceCourant;
	}
	public void setExerciceCourant(int exercice) {
		if (exercice < 2000) {
			exerciceCourant = 0;
			displayGroup().setObjectArray(null);
		} else {
			displayGroup().setObjectArray(EOPrimeBudget.rechercherBudgetsPourExercice(editingContext(), new Integer(exercice)));
		}
	}
	public BigDecimal creditTotal() {
		return calculerSomme("pbudCredit");
	}
	public BigDecimal debitTotal() {
		return calculerSomme("pbudDebit");
	}
	public BigDecimal engageTotal() {
		return calculerSomme("pbudEngage");
	}
	public BigDecimal soldeTotal() {
		return calculerSomme("pbudSolde");
	}
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		if (group == displayGroup()) {
			if (currentBudget() == null || modeCreation()) {
				oldMontantCredit = null;
				displayGroupDetail.setObjectArray(null);
			} else {
				if (currentBudget().estGlobal()) {
					oldMontantCredit = null;
				} else {
					oldMontantCredit = currentBudget().pbudCredit();
				}
				displayGroupDetail.setObjectArray(EOPrimeExercice.rechercherPrimesExercicesPourBudget(editingContext(),currentBudget()));
			}
		}
		super.displayGroupDidChangeSelection(group);
	}
	/** Pour transformer la saisie d'un cr&eacute;dit n&eacute;gatif en valeur positive */
	public void displayGroupDidSetValueForObject(EODisplayGroup group, Object value, Object eo, String key) {
		if (group == displayGroup() && key.equals("pbudCredit")) {
			if (currentBudget().pbudCredit() != null && currentBudget().pbudCredit().doubleValue() < 0) {
				currentBudget().setPbudCredit(currentBudget().pbudCredit().abs());
				updaterDisplayGroups();
			}			
		}
	}
	// Actions
	/** Calcule le montant total de toutes les attributions li&eacute;es &agrave; ce budget via les primes. Le calcul est fait sur
	 * le serveur pour &eacute;viter de ramener sur le client toutes les attributions. */
	public void calculerEngage() {
		LogManager.logDetail("GestionBudget - calculerEngage");
		Class[] classeParametres = new Class[] {EOGlobalID.class};
		Object[] parametres = new Object[] {editingContext().globalIDForObject(currentBudget())};
		try {
			LogManager.logDetail("GestionBudget - Lancement du traitement sur le serveur");
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			BigDecimal result = (BigDecimal)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestCalculerEngagePourBudget",classeParametres,parametres,true);
			currentBudget().setPbudEngage(result);
			updaterDisplayGroups();
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	/** Calcule le montant total de toutes les attributions r&eacute;ellement d&eacute;bit&eacute;es &agrave; la date du jour li&eacute;es &agrave; ce budget via les primes. Le calcul est fait sur
	 * le serveur pour &eacute;viter de ramener sur le client toutes les p&eacute;riodes li&eacute;es aux attributions. */
	public void calculerDebit() {
		LogManager.logDetail("GestionBudget - calculerDebit");
		Class[] classeParametres = new Class[] {EOGlobalID.class};
		Object[] parametres = new Object[] {editingContext().globalIDForObject(currentBudget())};
		try {
			LogManager.logDetail("GestionBudget - Lancement du traitement sur le serveur");
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			BigDecimal result = (BigDecimal)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestCalculerDebitPourBudget",classeParametres,parametres,true);
			currentBudget().setPbudDebit(result);
			updaterDisplayGroups();
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	public void calculerSolde() {
		LogManager.logDetail("GestionBudget - calculerSolde");
		currentBudget().calculerSolde();
	}
	public void ajouterPrime() {
		LogManager.logDetail("GestionBudget - ajouterPrime");
		EOQualifier qualifier = SuperFinder.qualifierPourPeriode("debutValidite", DateCtrl.stringToDate("01/01/"+ currentBudget().pbudExercice()), "finValidite",  DateCtrl.stringToDate("31/12/"+ currentBudget().pbudExercice()));
		choixPrime = true;
		UtilitairesDialogue.afficherDialogue(this,"Prime", "getPrime", true, qualifier,true);
	}
	/** Supprime un PrimeExercice et enregistre imm&eacute;diatement la modification */
	public void supprimerPrime() {
		LogManager.logDetail("GestionBudget - supprimerPrime");
		String message = "Voulez-vous vraiment supprimer ";
		if (displayGroupDetail.selectedObjects().count() == 1) {
			message += "cette prime ?";
		} else {
			message += "ces primes ?";
		}
		if (EODialogs.runConfirmOperationDialog("Attention", message, "Oui", "Non")) {
			java.util.Enumeration e = displayGroupDetail.selectedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeExercice primeExercice = (EOPrimeExercice)e.nextElement();
				primeExercice.supprimerRelations();
				editingContext().deleteObject(primeExercice);
			}
			enregistrerModificationsPrimeExercice();
		}
	}
	// Notifications
	public void getPrime(NSNotification aNotif) {
		if (choixPrime && aNotif.object() != null) {
			EOGlobalID gid = (EOGlobalID)aNotif.object();
			EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(gid, editingContext());
			// Vérifier si cette prime a déjà été sélectionnée
			if (((NSArray)displayGroupDetail.displayedObjects().valueForKey("prime")).containsObject(prime))  {
				EODialogs.runErrorDialog("Erreur", "Cette prime a déjà été sélectionnée");
			} else {
				displayGroupDetail.insert();
				((EOPrimeExercice)displayGroupDetail.selectedObject()).initAvecBudgetEtPrime(currentBudget(), prime);
				enregistrerModificationsPrimeExercice();
			}
		}
		choixPrime = false;
	}
	// Méthodes du controller DG
	public boolean peutSaisirExercice() {
		return conditionsOKPourFetch() && !modificationEnCours();
	}
	/** On ne peut pas supprimer un budget qui a des primes associ&eacute;es. On ne peut supprimer le budget global que si il n'y a plus
	 * de budget pour des primes */
	public boolean peutSupprimer() {
		if (boutonModificationAutorise()) {
			if (displayGroupDetail.displayedObjects().count() == 0) {
				return (currentBudget().estGlobal() && displayGroup().displayedObjects().count() == 1) || !currentBudget().estGlobal();
			} else {
				return false;
			}	
		} else {
			return false;
		}	
	}
	/** On ne peut valider que si l'exercice est saisi et le cr&eacute;dit aussi */
	public boolean peutValider() {
		return super.peutValider() && currentBudget() != null && currentBudget().pbudExercice() != null && currentBudget().pbudCredit() != null &&
		(currentBudget().estGlobal() || currentBudget().pbudCredit().doubleValue() > 0);
	}
	/** Il n'y a qu'un seul budget global pour l'exercice */
	public boolean peutEtreGlobal() {
		if (modificationEnCours()) {
			java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeBudget budget = (EOPrimeBudget)e.nextElement();
				if (budget.estGlobal()) {
					return budget == currentBudget();
				}
			}
			return true;
		} else {
			return false;
		}
	}
	/** On ne peut ajouter une prime que si le budget est s&eacute;lectionn&eacute; et qu'il n'est pas global */
	public boolean peutAjouterPrime() {
		return super.boutonModificationAutorise() && !currentBudget().estGlobal();
	}
	public boolean peutSupprimerPrime() {
		return boutonModificationAutorise() && displayGroupDetail.selectedObjects().count() > 0;
	}
	/** On effectue les calculs uniquement en mode modification et si des primes ont &eacute;t&eacute; ajout&eacute;es &agrave; ce budget */
	public boolean peutEffectuerCalcul() {
		return peutCalculerSolde() && displayGroupDetail.displayedObjects().count() > 0;
	}
	/** On effectue les calculs uniquement en mode modification */
	public boolean peutCalculerSolde() {
		return modificationEnCours() && !modeCreation();
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		exerciceCourant = DateCtrl.getYear(new NSTimestamp());
		GraphicUtilities.changerTaillePolice(listePrimes,11);
		listePrimes.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		super.preparerFenetre();
	}
	protected void terminer() {
	}
	protected void traitementsPourCreation() {
		currentBudget().initAvecExercice(exerciceCourant());
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR", message);

	}
	protected boolean conditionsOKPourFetch() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutGererPrimes();
	}
	/** les budgets sont affich&eacute;s apr&grave;s s&eacute;lection de l'exercice. Au d&eacute;marrage, on affiche
	 * les budgets de l'ann&eacute;e en cours */
	protected NSArray fetcherObjets() {
		if (exerciceCourant() == 0) {
			return null;
		} else {
			return EOPrimeBudget.rechercherBudgetsPourExercice(editingContext(), new Integer(exerciceCourant()));
		}
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce budget ?";
	}
	protected void parametrerDisplayGroup() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("pbudTemGlobal", EOSortOrdering.CompareDescending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("pbudLibelle", EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
		displayGroupDetail.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("prime.primLibLong", EOSortOrdering.CompareAscending)));
	}
	protected boolean traitementsAvantValidation() {
		EOPrimeBudget budgetGlobal = budgetGlobal();
		if (displayGroup().displayedObjects().count() > 0 && budgetGlobal != null) {
			if (currentBudget().estGlobal()) {
				if (displayGroup().displayedObjects().count() > 1) {
					EODialogs.runInformationDialog("Attention","Le budget global a pour montant le montant saisi diminué du montant des enveloppes pour les primes");
				}
				BigDecimal credit = creditTotal();
				if (currentBudget().pbudCredit().doubleValue() < creditTotal().doubleValue()) {
					EODialogs.runInformationDialog("ERREUR","Le budget global doit au moins être égal au montant total des enveloppes pour les primes");
					return false;
				} else {
					currentBudget().setPbudCredit(currentBudget().pbudCredit().subtract(credit));
				}
			} else {
				if (!modeCreation()) {
					EODialogs.runInformationDialog("Attention","Le budget global va être augmenté de l'ancienne valeur de cette enveloppe et diminué du montant saisi");
				} else {
					EODialogs.runInformationDialog("Attention","Le budget global va être diminué du montant saisi");
				}
				BigDecimal creditGlobal = budgetGlobal.pbudCredit();
				if (oldMontantCredit != null) {
					creditGlobal = creditGlobal.add(oldMontantCredit);
				}
				if (creditGlobal.doubleValue() < currentBudget().pbudCredit().doubleValue()) {
					EODialogs.runInformationDialog("ERREUR","Le nouveau crédit de cette enveloppe dépasse le crédit du budget global");
					return false;
				} else {
					budgetGlobal.setPbudCredit(creditGlobal.subtract(currentBudget().pbudCredit()));
				}
			}
		}
		return true;
	}
	protected void traitementsApresValidation() {
		resetterMontantCredit();
		super.traitementsApresValidation();
	}
	/** Supprime le budget courant et ajoute au budget global le montant du cr&eacute;dit de l'enveloppe supprim&eacute;e si il ne s'agit pas du budget global */
	protected boolean traitementsPourSuppression() {
		if (currentBudget().estGlobal() == false) {
			// réinjecter au budget global, le montant du budget supprimé
			EODialogs.runInformationDialog("Attention","Le montant du budget supprimé va être rajouté au montant global");
			EOPrimeBudget budgetGlobal = budgetGlobal();
			if (budgetGlobal != null) {	// 21/12/2010
				budgetGlobal.setPbudCredit(budgetGlobal.pbudCredit().add(currentBudget().pbudCredit()));
			}
		}
		// 21/12/2010 - suppression des détails si ils existent
		if (displayGroupDetail.displayedObjects().count() > 0) {
			java.util.Enumeration e = displayGroupDetail.displayedObjects().objectEnumerator();
			while (e.hasMoreElements()){
				EOPrimeExercice primeExercice = (EOPrimeExercice)e.nextElement();
				primeExercice.supprimerRelations();
				editingContext().deleteObject(primeExercice);
			}
		}
		deleteSelectedObjects();
		return true;
	}
	protected void traitementsApresSuppression() {
		resetterMontantCredit();
		displayGroupDetail.updateDisplayedObjects();
		super.traitementsApresSuppression();
	}
	protected void traitementsApresRevert() {
		resetterMontantCredit();
		super.traitementsApresRevert();
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupDetail.updateDisplayedObjects();
		choixPrime = false;
	}
	// Méthodes privées
	private EOPrimeBudget currentBudget() {
		return (EOPrimeBudget)displayGroup().selectedObject();
	}
	private EOPrimeBudget budgetGlobal() {
		if (displayGroup().displayedObjects().count() == 0) {
			return null;
		}
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeBudget budget = (EOPrimeBudget)e.nextElement();
			if (budget.estGlobal()) {
				return budget;
			}
		}
		return null;
	}
	private void resetterMontantCredit() {
		// 29/03/2010
		if (currentBudget() == null || currentBudget().estGlobal()) {
			oldMontantCredit = null;
		} else {
			oldMontantCredit = currentBudget().pbudCredit();
		}
	}
	private BigDecimal calculerSomme(String key) {
		if (displayGroup().displayedObjects().count() == 0) {
			return null;
		}
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		BigDecimal result = new BigDecimal(0);
		while (e.hasMoreElements()) {
			EOPrimeBudget budget = (EOPrimeBudget)e.nextElement();
			if (budget.estGlobal() == false) {
				BigDecimal montant = (BigDecimal)budget.valueForKey(key);
				if (montant != null) {
					result = result.add(montant);
				}
			}
		}
		return result;
	}
	private void enregistrerModificationsPrimeExercice() {
		try {
			editingContext().saveChanges();
			updaterDisplayGroups();
		} catch (Exception exception) {
			if (exception.getClass().getName().equals("com.webobjects.foundation.NSValidation$ValidationException")) {
				LogManager.logInformation(this.getClass().getName() + " - erreur lors du validateForSave " + exception.getMessage());
				afficherExceptionValidation(exception.getMessage());
			} else {
				LogManager.logErreur("Erreur lors de l'enregistrement des donnees");
				LogManager.logException(exception);
				EODialogs.runErrorDialog("Erreur","Erreur lors de l'enregistrement des donnees !\nMESSAGE : " + exception.getMessage());
				editingContext().revert();
			}
		}
	}
}

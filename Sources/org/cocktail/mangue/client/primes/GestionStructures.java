/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeStructure;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

// 22/12/2010 - Adaptation Netbeans (Suppression de preparerFenetre)
public class GestionStructures extends GestionPourPrimeAvecDuree {

	// Actions
	public void afficherStructures() {
		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(editingContext());
		if (structure != null) {
			if (((NSArray)displayGroup().displayedObjects().valueForKey("structure")).containsObject(structure) == false) {
				currentObject().addObjectToBothSidesOfRelationshipWithKey(structure,"structure");
			}
		}
		updaterDisplayGroups();
	}
	// Méthodes du controller DG
	public boolean peutValider() {
		return super.peutValider() && currentObject().structure() != null;
	}
	/** On ne peut saisir que si la prime demande des v&eacute;rifications de structure */
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && currentPrime().verifierStructure();
	}

	// Méthodes protégées
	protected String messageConfirmationDestruction() {
		return "Voulez-vous supprimer cette structure ?";
	}
	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		displayGroupPrimes.setQualifier(EOQualifier.qualifierWithQualifierFormat("primVerifStructure = 'O'", null));
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("structure.llStructure", EOSortOrdering.CompareAscending)));
	}
	// Méthodes privées
	private EOPrimeStructure currentObject() {
		return (EOPrimeStructure)displayGroup().selectedObject();
	}

}

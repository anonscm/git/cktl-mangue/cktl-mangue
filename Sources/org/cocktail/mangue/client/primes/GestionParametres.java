/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

// 21/12/2010 - Adaptation Netbeans (Suppression de preparerFenetre) + correction d'un bug sur la vérification des chevauchements
// 16/02/2011 - Restriction du qualifier sur les corps pour les établissements qui ne gèrent pas les populations HU et les ENS
public class GestionParametres extends GestionAvecDureeValidite {
	private GestionCodes controleurCode;
	private String libelleRecherche;

	// Accesseurs
	// Accesseurs
	public String corps() {
		if (currentParametre() == null || currentParametre().corps() == null) {
			return null;
		} else {
			return currentParametre().corps().cCorps();
		}
	}
	public void setCorps(String unCorps) {
		if (unCorps != null) {
			EOCorps corps = (EOCorps)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "Corps", "cCorps", unCorps);
			if (corps != null) {
				currentParametre().addObjectToBothSidesOfRelationshipWithKey(corps, "corps");
			}
		} else {
			supprimerCorps();
		}
		updaterDisplayGroups();
	}
	public String grade() {
		if (currentParametre() == null || currentParametre().grade() == null) {
			return null;
		} else {
			return currentParametre().grade().cGrade();
		}
	}
	public void setGrade(String unGrade) {
		if (unGrade != null) {
			EOGrade grade = (EOGrade)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "Grade", "cGrade", unGrade);
			if (grade != null) {
				currentParametre().addObjectToBothSidesOfRelationshipWithKey(grade, "grade");
			}
		} else {
			supprimerGrade();
		}
		updaterDisplayGroups();
	}
	public String libelleRecherche() {
		return libelleRecherche;
	}
	public void setLibelleRecherche(String libelleRecherche) {
		this.libelleRecherche = libelleRecherche;
	}
	public int nbParametres() {
		if (displayGroup() != null) {
			return displayGroup().displayedObjects().count();
		} else {
			return 0;
		}
	}
	// Actions
	public void rechercher() {
		if (typeValidite() != null) {
			displayGroup().setObjectArray(EOPrimeParam.rechercherParametresPourLibelleEtPeriode(editingContext(), libelleRecherche(), debutPeriode(), finPeriode(), typeValidite().equals(TYPE_VALIDE)));
		} else {
			displayGroup().setObjectArray(null);
		}
		updaterDisplayGroups();
	}
	public void dupliquer() {
		EOPrimeParam paramDuplique = currentParametre();
		ajouter();
		currentParametre().updateAvecParametre(paramDuplique);
		updaterDisplayGroups();
	}
	public void afficherFonctions() {
		LogManager.logDetail("GestionParametres - afficherFonctions");
		UtilitairesDialogue.afficherDialogue(this,"Association", "getFonction", true, null,true);
	}
	public void supprimerFonction() {
		LogManager.logDetail("GestionParametres - supprimerFonction");
		currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().fonction(), "fonction");
	}
	public void afficherCorps() {
		LogManager.logDetail("GestionParametres - afficherCorps");
		NSMutableArray qualifiers = new NSMutableArray();

		if (EOGrhumParametres.isGestionHu() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toTypePopulation.temHospitalier = 'N'", null));
		}
		if (EOGrhumParametres.isGestionEns() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toTypePopulation.code != 'N'",null));
		}
		UtilitairesDialogue.afficherDialogue(this, EOCorps.ENTITY_NAME, "getCorps", true, new EOAndQualifier(qualifiers),false);
	}
	
	public void supprimerCorps() {
		LogManager.logDetail("GestionParametres - supprimerCorps");
		currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().corps(), "corps");
	}
	public void afficherGrades() {
		LogManager.logDetail("GestionParametres - afficherGrades");
		NSMutableArray qualifiers = new NSMutableArray();
		// 16/02/2011
		if (currentParametre().corps() == null) {
			if (EOGrhumParametres.isGestionHu() == false) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps.toTypePopulation.temHospitalier = 'N'", null));
			}
			if (EOGrhumParametres.isGestionEns() == false) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps.toTypePopulation.code != 'N'",null));
			}
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps = %@",new NSArray(currentParametre().corps())));
		}
		UtilitairesDialogue.afficherDialogue(this,"Grade", "getGrade", true, new EOAndQualifier(qualifiers),false);
	}
	public void supprimerGrade() {
		LogManager.logDetail("GestionParametres - supprimerGrade");
		currentParametre().removeObjectFromBothSidesOfRelationshipWithKey(currentParametre().grade(), "grade");
	}
	public void afficherCodes() {
		LogManager.logDetail("GestionParametres - afficherCodes");
		if (controleurCode == null) {
			String nomNotification = "SelectionCodeParam";
			controleurCode = GestionCodes.chargerDialogue(nomNotification, false);
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getCode",new Class[] {NSNotification.class}),nomNotification,null);
		}
		controleurCode.afficherFenetre();
	}

	// Notifications
	public void getCode(NSNotification aNotif) {
		if (modificationEnCours()) {
			ajouterRelation(currentParametre(),aNotif.object(),"code");
		}
	}
	public void getFonction(NSNotification aNotif) {
		if (modificationEnCours()) {
			ajouterRelation(currentParametre(),aNotif.object(),"fonction");
		}
	}
	public void getCorps(NSNotification aNotif) {
		if (modificationEnCours()) {
			ajouterRelation(currentParametre(),aNotif.object(),"corps");
		}
	}
	public void getGrade(NSNotification aNotif) {
		if (modificationEnCours()) {
			ajouterRelation(currentParametre(),aNotif.object(),"grade");
		}
	}
	// Méthodes du controller DG
	public boolean peutSupprimerFonction() {
		return modificationEnCours() && currentParametre().fonction() != null;
	}
	public boolean peutSupprimerCorps() {
		return modificationEnCours() && currentParametre().corps() != null;
	}
	public boolean peutSupprimerGrade() {
		return modificationEnCours() && currentParametre().grade() != null;
	}
	/** Peut valider si on est en mode modification et que :<BR>
	 * 	- le taux ou le montant ou l'indice ou le corps ou le grade ou les &eacute;chelons ou la fonction sont fournis<BR>
	 * 	- le libell&eacute;, le code et la date de d&eacute;but de validit&eacute; sont fournis<BR>
	 * si un &eacute;chelon est fourni, les deux doivent l'&ecirc;tre et le grade aussi
	 */
	public boolean peutValider() {
		if (super.peutValider()) {
			if (currentParametre().pparLibelle() == null || currentParametre().debutValidite() == null ||
					currentParametre().code() == null) {
				return false;
			}
			if (currentParametre().pparTaux() == null && currentParametre().pparMontant() == null && currentParametre().pparIndice() == null &&
					currentParametre().corps() == null && currentParametre().grade() == null && currentParametre().pparEchelonMaxi() == null &&
					currentParametre().pparEchelonMini() == null && currentParametre().fonction() == null && currentParametre().pparEntier() == null) {
				return false;
			}
			if ((currentParametre().pparEchelonMini() != null || currentParametre().pparEchelonMaxi() != null) && currentParametre().grade() == null) {
				return false;
			} 
			return true;
		} else {
			return false;
		}
	}
	// Méthodes protégées
	protected void terminer() {
	}
	/** Ferme la date du param&egrave;tre comportant la valeur pr&eacute;c&eacute;dente si la date de ce param&egrave;tre est nulle */
	protected boolean traitementsAvantValidation() {
		currentParametre().preparerTypeParametre();
		boolean result = true;
		if (currentParametre().finValidite() == null) {
			NSArray parametresValides = EOPrimeParam.rechercherParametresValidesPourCodeEtPeriode(editingContext(), currentParametre().code(), currentParametre().debutValidite(),currentParametre().finValidite());
			// Il y en a un au plus car les valeurs des paramètres ne peuvent se chevaucher
			if (parametresValides.count() > 0) {
				java.util.Enumeration e = parametresValides.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
					if (currentParametre() != parametre && currentParametre().aMemeValeur(parametre)) {
						// 21/12/2010 - test sur currentParametre().finValidite() ajouté pour vérifier le chevauchement 
						if (parametre.finValidite() == null || currentParametre().finValidite() == null || (currentParametre().finValidite() != null && DateCtrl.isAfter(parametre.finValidite(),currentParametre().finValidite()))) {
							if (EODialogs.runConfirmOperationDialog("Attention","Il existe un autre paramètre valide pendant la période avec le même code. Voulez-vous continuer ?","Oui","Non")) {
								parametre.setFinValidite(DateCtrl.jourPrecedent(currentParametre().debutValidite()));
							} else {
								result = false;
							}
						}
						break;
					}
				}

			}
		}
		return result && super.traitementsAvantValidation();
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce paramètre ?";
	}
	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("pparLibelle", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("corps.cCorps", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("grade.cGrade", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("fonction.libelleLong", EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sorts);
	}
	/** La recherche est faite uniquement quand on clique sur le bouton de Recherche */
	protected  NSArray objetsPourValidite(boolean estValide) {
		return null;
	}
	// Méthodes privées
	private EOPrimeParam currentParametre() {
		return (EOPrimeParam)displayGroup().selectedObject();
	}

}

//Created on Wed Jun 11 12:24:00 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import java.awt.Cursor;

import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.AideUtilisateur;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.primes.CriteresEvaluationPrime;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPrimeBudget;
import org.cocktail.mangue.modele.goyave.EOPrimeExercice;
import org.cocktail.mangue.modele.goyave.EOPrimeFonction;
import org.cocktail.mangue.modele.goyave.EOPrimePopulation;
import org.cocktail.mangue.modele.goyave.EOPrimeStructure;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** 

 * @author christine
 *
 */
// 18/11/09 - modification de la suppression des attributions pour supprimer la personnalisation (si elle existe) liée à l'attribution à supprimer
// 22/12/2010 - Adaptation Netbeans
// 16/03/2011 - rajout d'un dialogue dans l'impression pour signaler que la fonctionnalité n'est pas disponible
// 23/03/2011 - impression des primes
public class GestionPrimes extends GestionPourPrimeAvecAnnee {
	public final static String PREFIXE_POUR_ATTRIBUTION = "Globales_";
	public final static String NOTIFICATION_ATTRIBUTIONS_VALIDEES = "NotificationAttributionsValidees";
	public EODisplayGroup displayGroupCorps,displayGroupGrade,displayGroupFonction,displayGroupPopulation,displayGroupStructure,displayGroupTypeContrat;
	public EOTable listeCorps, listeGrade, listeFonction, listePopulation, listeStructure,listeTypeContrat;
	public EOView vuePersonnalisation;
	public JTabbedPane vueOngletsCriteres;
	private GestionAide controleurAide;
	private boolean peutCalculer,attributionsValidees,verifierBudget;
	private GestionAttribution controleurAttribution;

	// Accesseurs
	public int nbAttributions() {
		if (displayGroup() == null) {
			return 0;
		} else {
			return displayGroup().displayedObjects().count();
		}
	}
	/** Surcharge du parent pour pr&eacute;parer le controleur d'attribution */
	public void setAnnee(String annee) {
		super.setAnnee(annee);
		controleurAttribution.setAnnee(annee);
		updaterDisplayGroups();
	}
	/** Surcharge pour informer le controleur d'attribution */
	public void setModificationEnCours(boolean aBool) {
		super.setModificationEnCours(aBool);
		controleurAttribution.setModificationEnCours(aBool);
	}
	/** Surcharge du parent pour pr&eacute;parer les display group de crit&egrave;res */
	public void setLibellePrime(String aStr) {
		super.setLibellePrime(aStr);
		remettreAZero();
		controleurAttribution.preparerCampagnePourPrime(currentPrime());
		peutCalculer = false;
		int indexOngletSelectionne = -1;
		if (currentPrime() != null) {
			String message = "";
			if (currentPrime().verifierCarriere() || currentPrime().verifierContrat()) {
				NSMutableArray corps = new NSMutableArray(), grades = new NSMutableArray(), populations = new NSMutableArray(), typesContrat = new NSMutableArray();
				NSArray primesPopulation = EOPrimePopulation.rechercherPrimesPopulationValidesPourPrime(editingContext(), currentPrime());
				if (primesPopulation.count() > 0) {
					peutCalculer = true;
					java.util.Enumeration e = primesPopulation.objectEnumerator();
					while (e.hasMoreElements()) {
						EOPrimePopulation primePopulation = (EOPrimePopulation)e.nextElement();
						if (primePopulation.typePopulation() != null) {
							populations.addObject(primePopulation.typePopulation());
						}
						if (primePopulation.corps() != null) {
							corps.addObject(primePopulation.corps());
						}
						if (primePopulation.grade() != null) {
							grades.addObject(primePopulation.grade());
						}
						if (primePopulation.typeContratTravail() != null) {
							typesContrat.addObject(primePopulation.typeContratTravail());
						}
					}
					boolean aInformationsCarriere = populations.count() > 0 || corps.count() > 0 || grades.count() > 0;
					vueOngletsCriteres.setEnabledAt(0, aInformationsCarriere);
					if (aInformationsCarriere) {
						indexOngletSelectionne = 0;
					}
					vueOngletsCriteres.setEnabledAt(1, typesContrat.count() > 0);
					if (typesContrat.count() > 0 && indexOngletSelectionne == -1) {
						indexOngletSelectionne = 1;
					}
				} else {
					corps = null; grades = null;populations = null;typesContrat = null;
					message = "Les informations sur les types de population, corps ou grade ou types de contrat de travail";
					vueOngletsCriteres.setEnabledAt(0, false);
					vueOngletsCriteres.setEnabledAt(1, false);
				}
				displayGroupPopulation.setObjectArray(populations);
				displayGroupCorps.setObjectArray(corps);
				displayGroupGrade.setObjectArray(grades);
				displayGroupTypeContrat.setObjectArray(typesContrat);
			}  else {
				displayGroupPopulation.setObjectArray(null);
				displayGroupCorps.setObjectArray(null);
				displayGroupGrade.setObjectArray(null);
				displayGroupTypeContrat.setObjectArray(null);
				vueOngletsCriteres.setEnabledAt(0, false);
				vueOngletsCriteres.setEnabledAt(1, false);
			}
			if (currentPrime().verifierFonction()) {
				NSArray fonctions = EOPrimeFonction.rechercherFonctionsValidesPourPrime(editingContext(), currentPrime());
				if (fonctions.count() > 0) { 
					peutCalculer = true;
				} else {
					message = "Les informations sur les fonctions";

				}
				displayGroupFonction.setObjectArray(fonctions);
				vueOngletsCriteres.setEnabledAt(2, fonctions.count() > 0);
				if (fonctions.count() > 0 && indexOngletSelectionne == -1) {
					indexOngletSelectionne = 2;
				}
			}  else {
				vueOngletsCriteres.setEnabledAt(2, false);
			}
			if (currentPrime().verifierStructure()) {
				NSArray structures = EOPrimeStructure.rechercherStructuresValidesPourPrimeEtPeriode(editingContext(), currentPrime(),debutCampagne(),finCampagne());
				if (structures.count() > 0) { 
					peutCalculer = true;
				}  else {
					message = "Les informations sur les structures";
				}
				displayGroupStructure.setObjectArray(structures);
				vueOngletsCriteres.setEnabledAt(3, structures.count() > 0);
				if (structures.count() > 0 && indexOngletSelectionne == -1) {
					indexOngletSelectionne = 3;
				}
			}  else {
				vueOngletsCriteres.setEnabledAt(3, false);
			}
			vueOngletsCriteres.setSelectedIndex(indexOngletSelectionne);
			if (message.length() > 0) {
				EODialogs.runInformationDialog("Attention", "Veuillez voir votre administrateur car la prime est insuffisamment paramétrée." 
						+ message + " sont indisponibles.\nLes attributions ne peuvent être évaluées");
			}
		}
		// Vérifier si les budgets sont définis si nécessaire
		peutCalculer = verifierValiditeBudgets();
		// Préparation de la vue onglets
		updaterDisplayGroupsCriteres();
		// Préparation du displayGroup pour afficher les attributions
		preparerDisplayGroup();
		updaterDisplayGroups();
	}
	// Méthodes de délégation du displayGroup
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		// Préparer la zone indiquant les paramétrages de calcul
		if (group == displayGroup()) {
			if (currentAttribution() == null) {
				controleurAttribution.preparerPourAttributionsEtSelection(null, null,false);
			} else {
				controleurAttribution.preparerPourAttributionsEtSelection(attributionsPourIndividu(currentAttribution().individu()), currentAttribution(),modeSaisiePossible());
			}
		}
		super.displayGroupDidChangeSelection(group);
	}
	public void afficherAide() {
		if (controleurAide == null) {
			controleurAide = new GestionAide("Aide Utilisateur de la prime","",false);
			controleurAide.init();
		}
		controleurAide.setTexte(currentPrime().primCommentaire());
		controleurAide.afficherFenetre();
	}
	public void afficherAideGlobale() {
		AideUtilisateur controleur = new AideUtilisateur("Aide_Primes","Gestion_Primes");
		controleur.afficherFenetre();
	}
	/** Effectue le calcul d'une prime pour tous les individus &eacute;ligibles selon les crit&egrave;res s&eacute;lectionn&eacute;s.<BR>
	 * On d&eacute;termine tous les individus qui peuvent potentiellement avoir la prime et on cr&eacute;e une attribution pour chacun<BR>
	 * C'est ensuite le r&ocirc;le de l'agent de choisir les individus qui n'auront pas de prime, en les supprimant, de recalculer si n&eacute;cessaire
	 * le montant de chaque attribution et de valider les attributions pour qu'elles deviennent d&eacute;finitives.<BR>
	 * Le calcul est fait sur le serveur
	 */
	public void calculer() {
		LogManager.logDetail("GestionPrimes - calculer");
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		// vérifier si l'individu veut tout recalculer dans le cas où il y a des attributions affichées
		boolean toutRecalculer = true;
		if (displayGroup().displayedObjects().count() > 0) {
			toutRecalculer = EODialogs.runConfirmOperationDialog("", "Voulez vous recalculer toutes les attributions ? Les attributions validées ne seront pas recalculées", "Oui", "Non");
			// Pour éviter les problèmes d'editing context décalés, on invalide toutes les attributions en cours si il faut tout recalculer
			// et on enregistre l'editing context
			if (toutRecalculer) {
				java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
				while (e.hasMoreElements()) {
					EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
					attribution.setEstValide(false);
				}
				try {
					editingContext().saveChanges();
				} catch (Exception exc) {
					LogManager.logException(exc);
					EODialogs.runErrorDialog("Erreur","Une erreur s'est produite en invalidant les attributions en cours, impossible de recalculer toutes les attributions");
				}
			}
		}
		// Récupérer tous les critères de sélection
		CriteresEvaluationPrime criteres = new CriteresEvaluationPrime();
		criteres.setTypesPopulation(objetsDansDisplayGroup(displayGroupPopulation));
		criteres.setCorps(objetsDansDisplayGroup(displayGroupCorps));
		criteres.setGrades(objetsDansDisplayGroup(displayGroupGrade));
		criteres.setTypesContrat(objetsDansDisplayGroup(displayGroupTypeContrat));
		criteres.setFonctions(objetsDansDisplayGroup(displayGroupFonction));
		criteres.setStructures(objetsDansDisplayGroup(displayGroupStructure));
		criteres.transformerEnGlobalIDs(editingContext());
		Class[] classeParametres = new Class[] {EOGlobalID.class,NSTimestamp.class,NSTimestamp.class,CriteresEvaluationPrime.class,Boolean.class};
		Object[] parametres = new Object[] {editingContext().globalIDForObject(currentPrime()),debutCampagne(),finCampagne(),criteres,new Boolean(toutRecalculer)};
		try {
			NSTimestamp debut = new NSTimestamp();
			LogManager.logDetail("GestionPrimes - Lancement du traitement sur le serveur");
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			NSArray attributions = (NSArray)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestCalculerAttributionsPourPrime",classeParametres,parametres,true);
			NSTimestamp fin = new NSTimestamp();
			long duree = (fin.getTime() - debut.getTime()) / 1000;
			LogManager.logDetail("GestionPrimes - Fin du traitement sur le serveur, duree " + duree + " secondes");
			if (attributions != null) {	// Null si exception
				NSMutableArray attribs = new NSMutableArray(Utilitaires.tableauObjetsMetiers(attributions, editingContext()));
				if (!toutRecalculer) {
					// Ajouter toutes les attributions qui sont déjà dans le display group
					attribs.addObjectsFromArray(displayGroup().displayedObjects());
				}
				displayGroup().setObjectArray(attribs);
				// Vérifier si les montants sont disponibles dans le budget
				if (attributions != null && attributions.count() > 0) {
					if (depassementBudgetPourAttributions(attributions)) {
						valider();	// Enregistrer
					}
					EODialogs.runInformationDialog("","" + attributions.count() + " nouvelle(s) attribution(s)");
				}
				updaterDisplayGroups();

			} else {
				if (!toutRecalculer) {
					EODialogs.runInformationDialog("","Pas de nouvelle attribution de la prime");
				} else {
					EODialogs.runInformationDialog("","Pas d'attribution de la prime");
				}
			}
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		component().setCursor(Cursor.getDefaultCursor());
	}
	/** Renouvelle les attributions d'une prime l'ann&eacute;e suivante apres avoir verifie que les individus
	 * peuvent en beneficier */
	public void renouveler() {
		LogManager.logDetail("GestionPrimes - renouveler");
		Class[] classeParametres = new Class[] {NSArray.class};
		Object[] parametres = new Object[] {Utilitaires.tableauDeGlobalIDs(displayGroup().displayedObjects(), editingContext())};
		try {
			NSTimestamp debut = new NSTimestamp();
			LogManager.logDetail("GestionPrimes - Lancement du renouvellement de l'attribution sur le serveur");
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			String resultat = (String)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestRenouvelerAttributions",classeParametres,parametres,true);
			NSTimestamp fin = new NSTimestamp();
			long duree = (fin.getTime() - debut.getTime()) / 1000;
			LogManager.logDetail("GestionAttribution - Fin du traitement sur le serveur, duree " + duree + " secondes");
			if (resultat != null) {
				if (resultat.indexOf("Erreur") >= 0) {
					EODialogs.runErrorDialog("ERREUR", resultat.substring(resultat.indexOf("-") + 2));
				} else {
					EODialogs.runInformationDialog("", resultat);
				}
			}
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		// Changer l'année pour afficher l'année suivante
		setAnnee("" + (new Integer(annee()).intValue() + 1));
		// Positionner aussi sur les primes en cours
		setTypeValidite(TYPE_EN_COURS);
	}
	/** Imprime les arr&circ;t&eacute;s de primes ou la liste des attributions */
	public void imprimer() {
		LogManager.logDetail("GestionPrimes - imprimer");
		// 23/03/2011
		int annee = new Integer(annee()).intValue();
		NSTimestamp dateDebut = currentPrime().debutPeriode(annee),dateFin = currentPrime().finPeriode(annee);
		InfoPourEditionRequete infoEdition = new InfoPourEditionRequete("Primes","Prime " + currentPrime().libelle(),dateDebut,dateFin);
		Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,NSArray.class};
		Object[] parametres = new Object[]{infoEdition,Utilitaires.tableauDeGlobalIDs(displayGroup().displayedObjects(), editingContext())};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerPrimes",classeParametres,parametres,"Primes","Impression des primes");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
	}
	/** Remet &agrave; z&eacute; la s&eacute;lection dans les display groups de crit&egrave;res 
	 * (population/corps/grades/fonctions/structures)
	 */
	public void remettreAZero() {
		displayGroupCorps.setSelectedObjects(null);
		displayGroupGrade.setSelectedObjects(null);
		displayGroupTypeContrat.setSelectedObjects(null);
		displayGroupFonction.setSelectedObjects(null);
		displayGroupPopulation.setSelectedObjects(null);
		displayGroupStructure.setSelectedObjects(null);
		updaterDisplayGroupsCriteres();
	}
	public void terminer() {
		controleurAttribution.terminer();
		super.terminer();
	}
	// Notifications
	/** Notification envoy&eacute;e lorsque les personnalisations sont modifi&eacute;es. L'objet de la notification contient un dictionnaire
	 * avec la prime et l'individu */
	public void reafficherAttributions(NSNotification aNotif) {
		NSDictionary dict = (NSDictionary)aNotif.object();
		EOGlobalID primeID = (EOGlobalID)dict.valueForKey("prime");
		EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext());
		if (currentPrime() == prime) {
			preparerDisplayGroup();
			updaterDisplayGroups();
		}
	}
	/** Suite &agrave; l'annulation de la modification d'une attribution */
	public void annulationAttribution(NSNotification aNotif) {
		if (modificationEnCours()) {
			LogManager.logDetail("GestionPrimes - Notification annulation attribution");
			annuler();
		}
	}
	/** Suite &agrave; la validation de la modification d'une attribution */
	public void validationAttribution(NSNotification aNotif) {
		LogManager.logDetail("GestionPrimes - Notification validation attribution");
		super.traitementsApresValidation();
		preparerDisplayGroup();
	}
	/** Suite &agrave; l'&eacute;dition ou non d'une attribution, l'objet de la notification contient un booleen */
	public void editionAttribution(NSNotification aNotif) {
		if (modificationEnCours()) {
			LogManager.logDetail("GestionPrimes - Notification edition attribution");
			boolean edited = ((Boolean)aNotif.object()).booleanValue();
			setEdited(edited);
		}
	}
	public void raffraichir(NSNotification aNotif) {
		LogManager.logDetail("GestionPrimes - Notification raffraichir attribution");
		preparerDisplayGroup(); 
	}
	// Méthodes du controller DG
	/** on ne peut ajouter une prime que si la saisie est possible et que les c
	 * rit&egrave;res de primes sont correctement d&eacute;finis */
	public boolean peutAjouter() {
		return super.modeSaisiePossible() && peutCalculer();
	}
	/** On ne peut modifier une attribution que si les modifications sont autoris&eacute;es,qu'elle est n'est pas dans l'historique et
	 * que l'individu et la prime de cette attribution sont d&eacute;finis */
	public boolean boutonModificationAutorise() {
		return super.boutonModificationAutorise() && currentAttribution().estInvalide() == false && currentAttribution().prime() != null && 
		currentAttribution().individu() != null;
	}
	/** On ne peut pas supprimer une prime d&eacute;j&agrave; invalid&eacute;e */
	public boolean peutSupprimer() {
		return super.boutonModificationAutorise() && !currentAttribution().estInvalide();
	}
	/** on ne peut calculer que si l'ann&eacute;e de la campagne est saisie, qu'il ne s'agit pas d'une prime individuelle, que la saisie est possible, que les crit&egrave;res de primes sont correctement d&eacute;finis, que
	 * sont affich&eacute;es les attributions en cours */
	public boolean peutCalculer() {
		return modeSaisiePossible() && peutCalculer && typeValidite() != null && typeValidite().equals(TYPE_EN_COURS) && annee() != null && 
		currentPrime() != null && currentPrime().estIndividuelle() == false;
	}
	/** On ne peut renouveler que les attributions valides */
	public boolean peutRenouveler() {
		return modeSaisiePossible() && typeValidite().equals(TYPE_VALIDE) && annee() != null && 
		currentPrime() != null && currentPrime().estAttributionUniqueDansAnnee() == false && displayGroup().displayedObjects().count() > 0;
	}
	/** On ne peut changer de campagne (i.e d'ann&eacute;e) que si on n'est pas en mode modification et que la saisie est possible
	 */
	public boolean peutSaisirAnnee() {
		return !modificationEnCours() && conditionSurPageOK() && !estLocke();
	}
	/** on ne peut imprimer que si toutes les attributions ont &eacute;t&eacute; valid&eacute;es */
	public boolean peutImprimerArrete() {
		return modeSaisiePossible() && typeValidite() != null && typeValidite().equals(TYPE_VALIDE) && attributionsValidees;
	}
	public boolean popupActif() {
		return !modificationEnCours() && conditionSurPageOK() && !estLocke() && currentPrime() != null;
	}
	public boolean peutAfficherAide() {
		return !estLocke() && currentPrime() != null;
	}
	// Méthodes protégées
	protected void preparerFenetre() {

		GraphicUtilities.changerTaillePolice(listeCorps,11);
		GraphicUtilities.rendreNonEditable(listeCorps);
		listeCorps.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		GraphicUtilities.changerTaillePolice(listeGrade,11);
		GraphicUtilities.rendreNonEditable(listeGrade);
		listeGrade.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		GraphicUtilities.changerTaillePolice(listeFonction,11);
		GraphicUtilities.rendreNonEditable(listeFonction);
		listeFonction.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		GraphicUtilities.changerTaillePolice(listePopulation,11);
		GraphicUtilities.rendreNonEditable(listePopulation);
		listePopulation.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		GraphicUtilities.changerTaillePolice(listePopulation,11);
		GraphicUtilities.rendreNonEditable(listePopulation);
		listeTypeContrat.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		GraphicUtilities.changerTaillePolice(listeTypeContrat,11);
		GraphicUtilities.rendreNonEditable(listeTypeContrat);
		GraphicUtilities.changerTaillePolice(listeStructure,11);
		GraphicUtilities.rendreNonEditable(listeStructure);
		listeStructure.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		displayGroupCorps.setSelectsFirstObjectAfterFetch(false);
		displayGroupGrade.setSelectsFirstObjectAfterFetch(false);
		displayGroupTypeContrat.setSelectsFirstObjectAfterFetch(false);
		displayGroupFonction.setSelectsFirstObjectAfterFetch(false);
		displayGroupPopulation.setSelectsFirstObjectAfterFetch(false);
		displayGroupStructure.setSelectsFirstObjectAfterFetch(false);
		controleurAttribution = new GestionAttribution(editingContext(),true);	// Il s'agit d'une gestion globale des primes
		GraphicUtilities.swaperViewEtCentrer(vuePersonnalisation, controleurAttribution.component());
		setTypeValidite(TYPE_EN_COURS);
		super.preparerFenetre(); // à la fin pour que le contrôleur d'attribution soit initialisé
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("reafficherAttributions", new Class[] {NSNotification.class}),
				GestionPersonnalisations.NOTIFICATION_ATTRIBUTION_MODIFIEE,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("annulationAttribution", new Class[] {NSNotification.class}),
				PREFIXE_POUR_ATTRIBUTION + GestionAttribution.NOTIFICATION_ANNULER_ATTRIBUTION,null);	
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("validationAttribution", new Class[] {NSNotification.class}),
				PREFIXE_POUR_ATTRIBUTION + GestionAttribution.NOTIFICATION_VALIDER_ATTRIBUTION,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("editionAttribution", new Class[] {NSNotification.class}),
				PREFIXE_POUR_ATTRIBUTION +GestionAttribution.NOTIFICATION_EDITION_ATTRIBUTION,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("raffraichir", new Class[] {NSNotification.class}),
				PREFIXE_POUR_ATTRIBUTION +GestionAttribution.NOTIFICATION_RAFFRAICHIR_ATTRIBUTION,null);
	}
	protected void traitementsPourCreation() {
		//	controleurAttribution.preparerPourAttributionsEtSelection(new NSArray(currentAttribution()), currentAttribution(),true);
		controleurAttribution.traitementsPourCreation(currentPrime(),null);
	}
	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		// Pour le type en cours, on n'affiche que les attributions en attentes
		if (typeValidite() != null && typeValidite().equals(TYPE_EN_COURS)) {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("temValide = 'C' OR temValide = 'P' or temValide = 'S'", null));
		}
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending));
		displayGroup().setSortOrderings(sorts);
	}
	/** Surcharge du parent pour g&eacute;rer le fetcth des attributions */
	protected void preparerDisplayGroup() {
		EOPrimeAttribution attribution = currentAttribution();
		if (typeValidite() == null || currentPrime() == null || annee() == null) {
			displayGroup().setObjectArray(null);
		} else if (typeValidite() != null) {
			if (typeValidite().equals(TYPE_VALIDE) || typeValidite().equals(TYPE_EN_COURS)) {
				NSArray attributionsValides = EOPrimeAttribution.rechercherAttributionsValidesPourPrimeIndividuEtPeriode(editingContext(), null, currentPrime(), debutCampagne(),finCampagne());
				NSArray attributionsEnCours = EOPrimeAttribution.rechercherAttributionsEnAttentePourPrimeIndividuEtPeriode(editingContext(), null, currentPrime(), debutCampagne(),finCampagne());
				if (typeValidite().equals(TYPE_VALIDE)) {
					displayGroup().setObjectArray(attributionsValides);
				} else if (typeValidite().equals(TYPE_EN_COURS)) {
					displayGroup().setObjectArray(attributionsEnCours);
				}
				attributionsValidees = (attributionsValides.count() > 0 && attributionsEnCours.count() == 0);
			}
			else if (typeValidite().equals(TYPE_HISTORIQUE)) {
				displayGroup().setObjectArray(EOPrimeAttribution.rechercherAttributionsInvalidesPourPrimeIndividuEtPeriode(editingContext(), null, currentPrime(), debutCampagne(),finCampagne()));
			} 
		}
		if (attribution != null && displayGroup().displayedObjects().containsObject(attribution)) {
			displayGroup().selectObject(attribution);
			controleurAttribution.preparerPourAttributionsEtSelection(attributionsPourIndividu(attribution.individu()), attribution,false);
		} else {
			controleurAttribution.preparerPourAttributionsEtSelection(null, null,false);
		}
		updaterDisplayGroups();
	}
	protected String messageConfirmationDestruction() {
		String message = "";
		if (attributionsPourIndividu(currentAttribution().individu()).count() > 1) {
			message = "\nToutes les attributions de cette prime pour cet individu et cette année seront supprimées";
		}
		return "Voulez-vous vraiment supprimer cette attribution ?" + message;
	}
	/** Surcharge du parent pour masquer ou afficher les boutons de validation des attributions */
	protected void afficherBoutons(boolean estValidation) {
		super.afficherBoutons(estValidation);
		controleurAttribution.afficherBoutons(estValidation);
	}
	/** Surcharge du parent pour supprimer du budget le montant de l'attribution si celle-ci est supprim&eacute;e */
	protected boolean traitementsPourSuppression() {
		java.util.Enumeration e = attributionsPourIndividu(currentAttribution().individu()).objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
			if (attribution.estValide() && verifierBudget) {
				VerificateurBudget verificateur = new VerificateurBudget(editingContext(),attribution.prime(),debutCampagne(),finCampagne());
				try {
					verificateur.supprimerMontantAttribution(attribution);
				} catch (Exception exc) {
					EODialogs.runErrorDialog("ERREUR", "Impossible d'invalider l'attribution.\n" + exc.getMessage());
					return false;	// Revert effectué par le parent
				}
			}
			// Si on trouve une personnalisation, l'invalider
			EOPrimePersonnalisation personnalisation = EOPrimePersonnalisation.rechercherPersonnalisationEnCoursPourIndividuPrimeEtPeriode(editingContext(),attribution.individu(),attribution.prime(),attribution.debutValidite(),attribution.finValidite());
			if (personnalisation != null) {
				personnalisation.setEstValide(false);
				NSArray parametresPerso = EOPrimeParamPerso.rechercherParametresPersonnelsValidesPourPersonnalisation(editingContext(), personnalisation);
				java.util.Enumeration e1 = parametresPerso.objectEnumerator();
				while (e1.hasMoreElements()) {
					EOPrimeParamPerso param = (EOPrimeParamPerso)e1.nextElement();
					param.setEstValide(false);
				}
			}
			attribution.setEstInvalide();
		}
		return super.traitementsPourSuppression();
	}
	// Méthodes privées
	private EOPrimeAttribution currentAttribution() {
		return (EOPrimeAttribution)displayGroup().selectedObject();
	}
	private void updaterDisplayGroupsCriteres() {
		displayGroupPopulation.updateDisplayedObjects();
		displayGroupCorps.updateDisplayedObjects();
		displayGroupGrade.updateDisplayedObjects();
		displayGroupTypeContrat.updateDisplayedObjects();
		displayGroupFonction.updateDisplayedObjects();
		displayGroupStructure.updateDisplayedObjects();
	}
	private boolean depassementBudgetPourAttributions(NSArray attributions) {
		if (verifierBudget) {
			VerificateurBudget verificateur = new VerificateurBudget(editingContext(),currentPrime(),debutCampagne(),finCampagne());
			java.util.Enumeration e = attributions.objectEnumerator();
			boolean budgetDepasse = false;
			while (e.hasMoreElements()) {
				EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
				double montant = attribution.pattMontant().doubleValue();
				if (montant != 0) {
					if (!budgetDepasse) {
						try {
							budgetDepasse = verificateur.validerAttribution(attribution);
						} catch (Exception exc) {
							gererExceptionPourAttribution(exc,attribution);
						}
					}
					if (budgetDepasse) {
						attribution.setEstSuspendue();
					}
				}
			}
			if (budgetDepasse) {
				EODialogs.runInformationDialog("Information","Pour des raisons de dépassement de budget, un certain nombre d'attributions sont suspendues");
			}
			return budgetDepasse;
		}
		return false;
	}
	private boolean verifierValiditeBudgets() {
		if (!verifierBudget) {
			return true;
		}
		boolean budgetOK = true;
		int anneeDebut = new Integer(annee()).intValue();
		int anneeFin = DateCtrl.getYear(finCampagne());
		EOPrimeBudget budgetExercice = EOPrimeExercice.budgetPourPrimeEtExercice(editingContext(),currentPrime(), new Integer(anneeDebut));
		if (budgetExercice == null) {
			budgetExercice = EOPrimeBudget.rechercherBudgetGlobalPourExercice(editingContext(), new Integer(anneeDebut));
			if (budgetExercice == null) {
				EODialogs.runErrorDialog("Erreur", "Le budget de la prime " + currentPrime().primLibLong() + " pour l'année " + anneeDebut + " ainsi que le budget global de l'établissement ne sont pas définis");
				budgetOK = false;
			}
		}
		if (anneeFin != anneeDebut) {
			budgetExercice = EOPrimeExercice.budgetPourPrimeEtExercice(editingContext(),currentPrime(), new Integer(anneeFin));
			if (budgetExercice == null) {
				budgetExercice = EOPrimeBudget.rechercherBudgetGlobalPourExercice(editingContext(), new Integer(anneeFin));
				if (budgetExercice == null) {
					EODialogs.runErrorDialog("Erreur", "Le budget de la prime " + currentPrime().primLibLong() + " pour l'année " + anneeFin + " ainsi que le budget global de l'établissement ne sont pas définis");
					budgetOK = false;
				}
			}	
		}
		return budgetOK;
	}
	private void gererExceptionPourAttribution(Exception exc,EOPrimeAttribution attribution) {
		String message = exc.getMessage() + "\nL'attribution pour " + attribution.individu().identitePrenomFirst() + " est remise en mode ";
		if (currentPrime().estCalculee()) {
			attribution.setEstCalculee();
			message +=  "à calculer";
		} else {
			attribution.setEstProvisoire();
			message +=  "provisoire";

		}
		EODialogs.runErrorDialog("Erreur",message);
	}
	private NSArray objetsDansDisplayGroup(EODisplayGroup group) {
		if (group.selectedObject() == null) {
			if (group.displayedObjects().count() > 0) {
				return group.displayedObjects();
			} else {
				return null;
			}
		} else {
			return group.selectedObjects();
		}
	}
	private NSArray attributionsPourIndividu(EOIndividu individu) {
		// Une prime unique dans l'année peut être définie à travers plusieurs attributions, ce qui n'est pas le cas des primes
		// de type horaire
		if (currentPrime() == null || currentPrime().estAttributionUniqueDansAnnee() == false) {
			if (currentAttribution() != null) {
				return new NSArray(currentAttribution());
			} else {
				return new NSArray();
			}
		}
		java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
		NSMutableArray attributions = new NSMutableArray();
		while (e.hasMoreElements()) {
			EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
			if (attribution.individu() == currentAttribution().individu()) {
				attributions.addObject(attribution);
			}
		}

		return attributions;
	}

}

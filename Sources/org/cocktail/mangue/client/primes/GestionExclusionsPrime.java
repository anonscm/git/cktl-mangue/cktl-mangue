/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPrimeExclusion;
import org.cocktail.mangue.modele.goyave.ObjetAvecValidite;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;

// 21/12/2010 - Adaptation Netbeans (Suppression de preparerFenetre)
public class GestionExclusionsPrime extends GestionPourPrime {
	// Actions
	public void afficherPrimes() {
		UtilitairesDialogue.afficherDialogue(this,"Prime", "getPrime", false,ObjetAvecValidite.qualifierPourValidite(true), true);
	}
	// Notifications
	public void getPrime(NSNotification aNotif) {
		if (modificationEnCours()) {
			EOGenericRecord recordDestin = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			if (((NSArray)displayGroup().displayedObjects().valueForKey("primeExclue")).containsObject(recordDestin) == false && recordDestin != currentPrime()) {
				ajouterRelation(currentExclusion(), aNotif.object(), "primeExclue");
			}
		}
	}
	// Méthodes du controller DG
	public boolean peutValider() {
		return super.peutValider() && currentExclusion().primeExclue() != null;
	}
	/** On ne peut saisir que si la prime supporte des exclusions */
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && currentPrime().verifierExclusion();
	}
	// Méthodes protégées
	protected String messageConfirmationDestruction() {
		return "Voulez-vous supprimer cette exclusion ?";
	}

	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		displayGroupPrimes.setQualifier(EOQualifier.qualifierWithQualifierFormat("primVerifExclusion = 'O'", null));
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("primeExclue.primLibLong", EOSortOrdering.CompareAscending)));
	}
	// Méthodes privées
	private EOPrimeExclusion currentExclusion() {
		return (EOPrimeExclusion)displayGroup().selectedObject();
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.DureeAvecValidite;

import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

public abstract class GestionAvecDureeValidite extends GestionAvecValidite {
	public EOView vuePeriode;
	private GestionPeriode controleurPeriode;
	
	// Notifications
	public void synchroniserDates(NSNotification aNotif) {
		LogManager.logDetail("GestionAvecDureeValidite - Synchronisation des dates d'affichage");
		if (typeValidite() != null) {
			displayGroup().setObjectArray(objetsPourValidite(typeValidite().equals(TYPE_VALIDE)));
		} else {
			displayGroup().setObjectArray(null);
		}
		updaterDisplayGroups();
	}
	// Méthodes du controller DG
	public boolean peutValider() {
		return super.peutValider() && currentObjet().debutValidite() != null;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		// préparer le contrôleur de priode et l'afficher dans la vue des périodes en sélectionnant l'affichage des ppériodes par défaut
		String dateDuJour = DateCtrl.dateToString(new NSTimestamp());
		controleurPeriode = new GestionPeriode(dateDuJour,dateDuJour,GestionPeriode.PERIODE_SAISIE);
		controleurPeriode.init();
		GraphicUtilities.swaperView(vuePeriode,controleurPeriode.component());
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserDates", new Class[] {NSNotification.class}),
				GestionPeriode.NOTIFICATION_PERIODE_HAS_CHANGED,null);
	}
	protected DureeAvecValidite currentObjet() {
		return (DureeAvecValidite)displayGroup().selectedObject();
	}
	/** Synchroniser les dates d'affichage avec celles de la prime saisie */
	protected void traitementsApresValidation() {
		// utilise pour synchroniser l'affichage au cas où la date de début n'est pas dans les dates affichées
		boolean periodeChangee = false;
		String debutPeriode = controleurPeriode.debutPeriode();
		if (debutPeriode == null || (debutPeriode != null && DateCtrl.isAfter(DateCtrl.stringToDate(debutPeriode),currentObjet().debutValidite()))) {
			debutPeriode = DateCtrl.dateToString(currentObjet().debutValidite());
			periodeChangee = true;
		}
			String finPeriode = controleurPeriode.finPeriode();
			if (finPeriode == null || (finPeriode != null && DateCtrl.isBefore(DateCtrl.stringToDate(finPeriode),currentObjet().debutValidite()))) {
				finPeriode = DateCtrl.dateToString(currentObjet().debutValidite());
				periodeChangee = true;
			}
				if (periodeChangee) {
					controleurPeriode.changerDates(debutPeriode,finPeriode);
				}
				super.traitementsApresValidation();
	}
	protected  NSArray objetsPourValidite(boolean estValide) {
		if (controleurPeriode != null) {
			if (estValide) {
				return DureeAvecValidite.rechercherObjetsValidesPourPeriode(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),controleurPeriode.dateDebut(),controleurPeriode.dateFin());
			} else {
				return DureeAvecValidite.rechercherObjetsInvalidesPourPeriode(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),controleurPeriode.dateDebut(),controleurPeriode.dateFin());
			}
		} else {
			return null;
		}
	}
	protected NSTimestamp debutPeriode() {
		if (controleurPeriode == null) {
			return null;
		} else {
			return controleurPeriode.dateDebut();
		}
	}
	protected NSTimestamp finPeriode() {
		if (controleurPeriode == null) {
			return null;
		} else {
			return controleurPeriode.dateFin();
		}
	}
}

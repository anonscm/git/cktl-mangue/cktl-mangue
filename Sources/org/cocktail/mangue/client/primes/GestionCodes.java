/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.client.components.DialogueDoubleSansFetch;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPrimeCode;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class GestionCodes extends DialogueDoubleSansFetch {
	public EOView vueBoutonsModification, vueBoutonsValidation;
	private boolean modificationEnCours,estCreation;
	private String currentLibelle,currentLibelleCode;
	
	public GestionCodes(String nomNotification,boolean estSelectionMultiple) {
		super("PrimeCode",nomNotification, "pcodCode","pcodLibelle","Sélection d'un code",estSelectionMultiple,true,true,false,true);
	}
	public void init() {
		EOArchive.loadArchiveNamed("GestionCodes",this,"org.cocktail.mangue.client.primes.interfaces",this.disposableRegistry());
	}
	// Accesseurs
	public boolean modificationEnCours() {
		return modificationEnCours;
	}
	public void setModificationEnCours(boolean modificationEnCours) {
		this.modificationEnCours = modificationEnCours;
	}
	public String currentLibelle() {
		return currentLibelle;
	}
	public void setCurrentLibelle(String currentLibelle) {
		this.currentLibelle = currentLibelle;
	}
	public String currentLibelleCode() {
		return currentLibelleCode;
	}
	public void setCurrentLibelleCode(String currentLibelleCode) {
		this.currentLibelleCode = currentLibelleCode;
	}
	// Méthodes de délégation du display group
	public boolean displayGroupShouldChangeSelection(EODisplayGroup aGroup,NSArray newIndexes) {
		return modificationEnCours() == false;
	}
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		if (currentCode() != null) {
			setCurrentLibelle(currentCode().pcodLibelle());
			setCurrentLibelleCode(currentCode().code());
		} else {
			setCurrentLibelle(null);
			setCurrentLibelleCode(null);
		}
		super.displayGroupDidChangeSelection(group);
	}
	// Actions
	public void ajouter() {
		LogManager.logDetail("Gestion codes - ajout");
		changerModeModification(true);
		estCreation = true;
		setCurrentLibelle(null);
		setCurrentLibelleCode(null);
	}
	public void modifier() {
		estCreation = false;
		LogManager.logDetail("Gestion codes - modifier");
		changerModeModification(true);
	}
	public void dupliquer() {
		LogManager.logDetail("Gestion codes - modifier");
		EOPrimeCode code = currentCode();
		changerModeModification(true);
		estCreation = true;
		// on duplique le code, si il se termine par un chiffre, on prend chiffre + 1, sinon on remplace le dernier caractère par 1
		String debut = code.pcodCode().substring(0,code.pcodCode().length() - 1);
		String lastChar = code.pcodCode().substring(code.pcodCode().length()-1,code.pcodCode().length());
		if (StringCtrl.estNumerique(lastChar,true)) {
			lastChar = new Integer(StringCtrl.stringToInt(lastChar, 0) + 1).toString();
		} else {
			lastChar = "1";
		}
		setCurrentLibelleCode(debut + lastChar);
		debut = code.pcodLibelle();
		if (debut.length() > 196) {
			debut = debut.substring(0,196);	
		}
		setCurrentLibelle(debut + " - " + lastChar);
	}
	public void annulerModification() {
		LogManager.logDetail("Gestion codes - annulation modification");
		changerModeModification(false);
		displayGroupDidChangeSelection(displayGroup());
	}
	public void validerModification() {
		LogManager.logDetail("Gestion codes - validation modification");
		// Vérifier si il existe un code avec le même code
		EOPrimeCode code = (EOPrimeCode)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "PrimeCode", "pcodCode", currentLibelleCode());
		if (code != null && (estCreation || code != currentCode())) {
			EODialogs.runErrorDialog("Erreur","Le code " + currentLibelleCode() + " existe déjà");
			return;
		}
		if (estCreation) {
			code = new EOPrimeCode();
			editingContext().insertObject(code);
		} else {
			code = currentCode();
		}
		code.initAvecCodeEtLibelle(currentLibelleCode(),currentLibelle());
		try {
			editingContext().saveChanges();
			if (estCreation) {
				displayGroup().setObjectArray(displayGroup().displayedObjects().arrayByAddingObject(code));
				displayGroup().selectObject(code);
			}
			changerModeModification(false);
			estCreation = false;
		} catch (Exception e) {
			LogManager.logDetail("Gestion codes - erreur de save " + e.getMessage());
			EODialogs.runErrorDialog("Erreur", e.getMessage());
			editingContext().revert();
			if (e instanceof NSValidation.ValidationException == false) {
				setCurrentLibelle(null);
				setCurrentLibelleCode(null);
				changerModeModification(false);
			}
		}
	
	}
	// Méthodes du controller DG
	public boolean peutAjouter() {
		return peutDeclencherAction();
	}
	public boolean boutonModificationAutorise() {
		return peutDeclencherAction()  && displayGroup().selectedObject() != null;
	}
	public boolean peutDeclencherAction() {
		return modificationEnCours == false;
	}
	public boolean peutValider() {
		 return peutDeclencherAction() && super.canValidate();
	}
	public boolean peutValiderModification() {
		 return currentLibelle() != null && currentLibelleCode() != null;
	}
	// Méthodes protégées
	public void prepareInterface() {
		super.prepareInterface();
		changerModeModification(false);
	}
	// Méthodes privées
	private EOPrimeCode currentCode() {
		return (EOPrimeCode)displayGroup.selectedObject();
	}
	private void changerModeModification(boolean estModification) {
		setModificationEnCours(estModification);
		vueBoutonsModification.setVisible(!estModification);
		vueBoutonsValidation.setVisible(estModification);
		updateDisplayGroups();
	}
	// Méthodes statiques
	public static GestionCodes chargerDialogue(String nomNotification, boolean estSelectionMultiple) {
		GestionCodes controleur = new GestionCodes(nomNotification,estSelectionMultiple);
		controleur.init();
		return controleur;
	}
	
}

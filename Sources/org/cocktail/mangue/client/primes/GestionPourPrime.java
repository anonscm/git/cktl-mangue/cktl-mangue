/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.modele.goyave.ObjetValidePourPrime;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public abstract class GestionPourPrime extends GestionAvecValidite {
	public EODisplayGroup displayGroupPrimes;
	private EOPrime currentPrime;
	private boolean afficherPrimesValides;

	// Accesseurs
	public boolean afficherPrimesValides() {
		return afficherPrimesValides;
	}
	public void setAfficherPrimesValides(boolean afficherPrimesValides) {
		this.afficherPrimesValides = afficherPrimesValides;
		if (afficherPrimesValides) {
			displayGroupPrimes.setObjectArray(EOPrime.rechercherPrimesValidesPourPeriode(editingContext(), null,null));
		} else {
			// Affichages de l'historique
			displayGroupPrimes.setObjectArray(EOPrime.rechercherObjetsInvalides(editingContext(), "Prime"));
			setTypeValidite(GestionAvecValidite.TYPE_HISTORIQUE);
		}
		displayGroup().setSelectedObject(null);
		displayGroup().setObjectArray(null);
		currentPrime = null;
		updaterDisplayGroups();
	}
	public String libellePrime() {
		if (currentPrime != null) {
			return currentPrime.libelleAvecCode();
		} else {
			return null;
		}
	}
	public void setLibellePrime(String aStr) {
		if (aStr == null) {
			currentPrime = null;
			displayGroupPrimes.setSelectedObject(null);
			displayGroup().setSelectedObject(null);
		} else {
			currentPrime = null;
			displayGroup().setSelectedObject(null);
			java.util.Enumeration e = displayGroupPrimes.displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrime prime = (EOPrime)e.nextElement();
				if (prime.libelleAvecCode().equals(aStr)) {
					displayGroupPrimes.setSelectedObject(prime);
					currentPrime = prime;
					break;
				}
			}
		}
		displayGroup().setObjectArray(objetsPourValidite(typeValidite() != null && typeValidite().equals(TYPE_VALIDE)));
		updaterDisplayGroups();
	}
	// Notifications
	public void synchroniserPrimes(NSNotification aNotif) {
		if (afficherPrimesValides()) {
			displayGroupPrimes.setObjectArray(EOPrime.rechercherPrimesValidesPourPeriode(editingContext(), null,null));
		} else {
				// Affichages de l'historique
				displayGroupPrimes.setObjectArray(EOPrime.rechercherObjetsInvalides(editingContext(), "Prime"));
			}
		 if (currentPrime() != null && displayGroupPrimes.displayedObjects().containsObject(currentPrime) == false) {
			displayGroup().setSelectedObject(null);
			displayGroup().setObjectArray(null);
			currentPrime = null;
			updaterDisplayGroups();
		}
	}
	// Méthodes du controller DG
	/** On ne peut choisir une prime dans le popup que si on n'est pas en mode modification */
	public boolean peutChoisirPrime() {
		return !modificationEnCours();
	}
	/** On ne peut choisir la validit&eacute; des &eacute;l&eacute;ments dans le popup que si la saisie est possible et qu'on affiche
	 * les primes valides (dans le cas de primes invalides, tous les &eacute;l&eacute;ments qui leur sont li&eacute;s sont
	 * aussi invalides */
	public boolean popupActif() {
		return super.popupActif() && afficherPrimesValides;
	}
	public boolean peutValider() {
		return super.peutValider() && currentObjet().prime() != null;
	}
	/** On ne peut saisir qu'une fois que la prime a &eacute;t&eacute; choisie */
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && currentPrime() != null && currentPrime().estValide();
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		setAfficherPrimesValides(true);
		raffraichirAssociations();
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserPrimes", new Class[] {NSNotification.class}),
				AdministrationPrimes.NOTIFICATION_RAFFRAICHIR_PRIME,null);
	}
	protected void traitementsPourCreation() {
		currentObjet().initAvecPrime(currentPrime);
	}
	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		displayGroupPrimes.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("primCodeIndemnite", EOSortOrdering.CompareAscending)));
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupPrimes.updateDisplayedObjects();
	}
	protected EOPrime currentPrime() {
		return currentPrime;
	}
	/** Retourne les objets conforme &agrave; la validit&eacute; lorsque le popup de validit&eacute; est modifi&eacute;,
	 * null si pas de prime s&eacute;lectionn&eacute;e.
	 */
	protected  NSArray objetsPourValidite(boolean estValide) {
		if (currentPrime() != null) {
			if (estValide) {
				return ObjetValidePourPrime.rechercherObjetsValidesPourPrime(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),currentPrime());
			} else {
				return ObjetValidePourPrime.rechercherObjetsInvalidesPourPrime(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),currentPrime());
			}
		} else {
			return null;
		}
	}
	protected void terminer() {
	}
	// Méthodes privées
	private ObjetValidePourPrime currentObjet() {
		return (ObjetValidePourPrime)displayGroup().selectedObject();
	}

}

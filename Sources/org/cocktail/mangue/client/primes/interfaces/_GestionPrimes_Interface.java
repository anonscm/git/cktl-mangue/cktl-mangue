// _GestionPrimes_Interface.java
// Created on 11 août 2010 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _GestionPrimes_Interface extends COFrame {

    /** Creates new form _GestionPrimes_Interface */
    public _GestionPrimes_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new org.cocktail.component.CODisplayGroup();
        displayGroupCorps = new org.cocktail.component.CODisplayGroup();
        displayGroupGrade = new org.cocktail.component.CODisplayGroup();
        displayGroupPopulation = new org.cocktail.component.CODisplayGroup();
        displayGroupTypeContrat = new org.cocktail.component.CODisplayGroup();
        displayGroupFonction = new org.cocktail.component.CODisplayGroup();
        displayGroupStructure = new org.cocktail.component.CODisplayGroup();
        displayGroupPrimes = new org.cocktail.component.CODisplayGroup();
        jLabel1 = new javax.swing.JLabel();
        cOTextField3 = new org.cocktail.component.COTextField();
        cOView1 = new org.cocktail.component.COView();
        cOView3 = new org.cocktail.component.COView();
        cOTextField1 = new org.cocktail.component.COTextField();
        cOTextField2 = new org.cocktail.component.COTextField();
        cOCheckbox1 = new org.cocktail.component.COCheckbox();
        cOComboBox1 = new org.cocktail.component.COComboBox();
        cOButton2 = new org.cocktail.component.COButton();
        cOButton1 = new org.cocktail.component.COButton();
        cOView2 = new org.cocktail.component.COView();
        vuePersonnalisation = new org.cocktail.component.COView();
        cOView4 = new org.cocktail.component.COView();
        vueOngletsCriteres = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        listePopulation = new org.cocktail.component.COTable();
        listeCorps = new org.cocktail.component.COTable();
        listeGrade = new org.cocktail.component.COTable();
        jPanel2 = new javax.swing.JPanel();
        listeTypeContrat = new org.cocktail.component.COTable();
        jPanel3 = new javax.swing.JPanel();
        listeFonction = new org.cocktail.component.COTable();
        jPanel4 = new javax.swing.JPanel();
        listeStructure = new org.cocktail.component.COTable();
        cOComboBox2 = new org.cocktail.component.COComboBox();
        cOButton3 = new org.cocktail.component.COButton();
        cOButton4 = new org.cocktail.component.COButton();
        cOButton5 = new org.cocktail.component.COButton();
        cOButton6 = new org.cocktail.component.COButton();
        jLabel2 = new javax.swing.JLabel();
        cOLabel1 = new org.cocktail.component.COLabel();
        vueBoutonsModification = new org.cocktail.component.COView();
        cOButton7 = new org.cocktail.component.COButton();
        cOButton8 = new org.cocktail.component.COButton();
        cOButton9 = new org.cocktail.component.COButton();
        listeAffichage = new org.cocktail.component.COTable();

        displayGroup.setEntityName("PrimeAttribution");
        displayGroup.setHasDelegate(true);
        displayGroup.setIsMainDisplayGroupForController(true);

        displayGroupCorps.setEntityName("Corps");

        displayGroupGrade.setEntityName("Grade");

        displayGroupPopulation.setEntityName("TypePopulation");

        displayGroupTypeContrat.setEntityName("TypeContratTravail");

        displayGroupFonction.setEntityName("Association");

        displayGroupStructure.setEntityName("StructureUlr");

        displayGroupPrimes.setEntityName("Prime");

        setControllerClassName("org.cocktail.mangue.client.primes.GestionPrimes");
        setSize(new java.awt.Dimension(856, 657));

        jLabel1.setFont(new java.awt.Font("Helvetica", 0, 12)); // NOI18N
        jLabel1.setText("Année");

        cOTextField3.setEnabledMethod("peutSaisirAnnee");
        cOTextField3.setSupportsBackgroundColor(true);
        cOTextField3.setValueName("annee");

        cOView1.setFont(new java.awt.Font("Helvetica", 0, 11));
        cOView1.setIsBox(true);
        cOView1.setTitle("Choix Prime");

        cOView3.setFont(new java.awt.Font("Helvetica", 0, 11));
        cOView3.setIsBox(true);
        cOView3.setTitle("Période de référence");

        cOTextField1.setDateFieldFormat("%d/%m/%Y");
        cOTextField1.setEnabledMethod("nonEditable");
        cOTextField1.setSupportsBackgroundColor(true);
        cOTextField1.setValueName("finCampagne");

        cOTextField2.setDateFieldFormat("%d/%m/%Y");
        cOTextField2.setEnabledMethod("nonEditable");
        cOTextField2.setSupportsBackgroundColor(true);
        cOTextField2.setValueName("debutCampagne");

        org.jdesktop.layout.GroupLayout cOView3Layout = new org.jdesktop.layout.GroupLayout(cOView3);
        cOView3.setLayout(cOView3Layout);
        cOView3Layout.setHorizontalGroup(
            cOView3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOView3Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 74, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 74, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        cOView3Layout.setVerticalGroup(
            cOView3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView3Layout.createSequentialGroup()
                .addContainerGap()
                .add(cOView3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cOCheckbox1.setEnabledMethod("peutChoisirPrime");
        cOCheckbox1.setText("Primes valides");
        cOCheckbox1.setValueName("afficherPrimesValides");

        cOComboBox1.setDisplayGroupForTitle(displayGroupPrimes);
        cOComboBox1.setEnabledMethod("peutChoisirPrime");
        cOComboBox1.setTitleAttribute("libelleAvecCode");
        cOComboBox1.setTitleForSelection("libellePrime");

        cOButton2.setActionName("afficherAide");
        cOButton2.setBorderPainted(false);
        cOButton2.setEnabledMethod("peutAfficherAide");
        cOButton2.setIconName("help.gif");

        org.jdesktop.layout.GroupLayout cOView1Layout = new org.jdesktop.layout.GroupLayout(cOView1);
        cOView1.setLayout(cOView1Layout);
        cOView1Layout.setHorizontalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOView1Layout.createSequentialGroup()
                .addContainerGap()
                .add(cOCheckbox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOComboBox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 367, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 16, Short.MAX_VALUE)
                .add(cOButton2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(cOView3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        cOView1Layout.setVerticalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView1Layout.createSequentialGroup()
                .add(5, 5, 5)
                .add(cOView3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(22, 22, 22))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOView1Layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cOCheckbox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOComboBox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOButton2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(32, 32, 32))
        );

        cOButton1.setActionName("afficherAideGlobale");
        cOButton1.setBorderPainted(false);
        cOButton1.setIconName("help.gif");

        cOView2.setFont(new java.awt.Font("Helvetica", 0, 11));
        cOView2.setIsBox(true);
        cOView2.setTitle("Attribution");

        org.jdesktop.layout.GroupLayout vuePersonnalisationLayout = new org.jdesktop.layout.GroupLayout(vuePersonnalisation);
        vuePersonnalisation.setLayout(vuePersonnalisationLayout);
        vuePersonnalisationLayout.setHorizontalGroup(
            vuePersonnalisationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 700, Short.MAX_VALUE)
        );
        vuePersonnalisationLayout.setVerticalGroup(
            vuePersonnalisationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 192, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout cOView2Layout = new org.jdesktop.layout.GroupLayout(cOView2);
        cOView2.setLayout(cOView2Layout);
        cOView2Layout.setHorizontalGroup(
            cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView2Layout.createSequentialGroup()
                .add(23, 23, 23)
                .add(vuePersonnalisation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        cOView2Layout.setVerticalGroup(
            cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView2Layout.createSequentialGroup()
                .add(vuePersonnalisation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cOView4.setFont(new java.awt.Font("Helvetica", 0, 11));
        cOView4.setIsBox(true);
        cOView4.setTitle("Critères de sélection des individus");

        vueOngletsCriteres.setFont(new java.awt.Font("Helvetica", 0, 12));

        jPanel1.setFont(new java.awt.Font("Helvetica", 0, 12));

        listePopulation.setColumns(new Object[][] {{null,"libelleLong",new Integer(2),"Type Population",new Integer(0),new Integer(187),new Integer(1000),new Integer(10)}});
        listePopulation.setDisplayGroupForTable(displayGroupPopulation);

        listeCorps.setColumns(new Object[][] {{null,"lcCorps",new Integer(2),"Corps",new Integer(0),new Integer(187),new Integer(1000),new Integer(10)}});
        listeCorps.setDisplayGroupForTable(displayGroupCorps);

        listeGrade.setColumns(new Object[][] {{null,"lcGrade",new Integer(2),"Grade",new Integer(0),new Integer(187),new Integer(1000),new Integer(10)}});
        listeGrade.setDisplayGroupForTable(displayGroupGrade);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(7, 7, 7)
                .add(listePopulation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 207, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(listeCorps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 207, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(listeGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 207, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(0, 0, 0)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, listeCorps, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, listePopulation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, listeGrade, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        vueOngletsCriteres.addTab("Carrière", jPanel1);

        jPanel2.setFont(new java.awt.Font("Helvetica", 0, 12));

        listeTypeContrat.setColumns(new Object[][] {{null,"libelleLong",new Integer(2),"Type Contrat Travail",new Integer(0),new Integer(393),new Integer(1000),new Integer(116)}});
        listeTypeContrat.setDisplayGroupForTable(displayGroupTypeContrat);

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(156, Short.MAX_VALUE)
                .add(listeTypeContrat, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 413, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(123, 123, 123))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(listeTypeContrat, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        vueOngletsCriteres.addTab("Contrat", jPanel2);

        jPanel3.setFont(new java.awt.Font("Helvetica", 0, 12));

        listeFonction.setColumns(new Object[][] {{null,"libelleLong",new Integer(2),"Fonctions",new Integer(0),new Integer(626),new Integer(1000),new Integer(52)}});
        listeFonction.setDisplayGroupForTable(displayGroupFonction);

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .add(listeFonction, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 646, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(listeFonction, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        vueOngletsCriteres.addTab("Fonction", jPanel3);

        jPanel4.setFont(new java.awt.Font("Helvetica", 0, 12));

        listeStructure.setColumns(new Object[][] {{null,"llStructure",new Integer(2),"Structures",new Integer(0),new Integer(626),new Integer(1000),new Integer(59)}});
        listeStructure.setDisplayGroupForTable(displayGroupStructure);

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .add(listeStructure, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 646, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(listeStructure, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        vueOngletsCriteres.addTab("Structure", jPanel4);

        org.jdesktop.layout.GroupLayout cOView4Layout = new org.jdesktop.layout.GroupLayout(cOView4);
        cOView4.setLayout(cOView4Layout);
        cOView4Layout.setHorizontalGroup(
            cOView4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView4Layout.createSequentialGroup()
                .add(16, 16, 16)
                .add(vueOngletsCriteres, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 697, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        cOView4Layout.setVerticalGroup(
            cOView4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueOngletsCriteres, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
        );

        cOComboBox2.setEnabledMethod("popupActif");
        cOComboBox2.setTitleForSelection("typeValidite");
        cOComboBox2.setTitles("Valides,En cours,Historique");

        cOButton3.setActionName("renouveler");
        cOButton3.setBorderPainted(false);
        cOButton3.setEnabledMethod("peutRenouveler");
        cOButton3.setIconName("renouveler.gif");

        cOButton4.setActionName("calculer");
        cOButton4.setBorderPainted(false);
        cOButton4.setEnabledMethod("peutCalculer");
        cOButton4.setIconName("calculer.gif");

        cOButton5.setActionName("imprimer");
        cOButton5.setBorderPainted(false);
        cOButton5.setEnabledMethod("peutImprimerArrete");
        cOButton5.setIconName("Imprimante_Arrete.gif");

        cOButton6.setActionName("remettreAZero");
        cOButton6.setEnabledMethod("modeSaisiePossible");
        cOButton6.setText("RAZ");

        jLabel2.setFont(new java.awt.Font("Helvetica", 0, 11));
        jLabel2.setText("Attributions");

        cOLabel1.setFont(new java.awt.Font("Helvetica", 0, 11));
        cOLabel1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cOLabel1.setValueName("nbAttributions");

        cOButton7.setActionName("ajouter");
        cOButton7.setBorderPainted(false);
        cOButton7.setEnabledMethod("peutAjouter");
        cOButton7.setIconName("ajouter16.gif");

        cOButton8.setActionName("modifier");
        cOButton8.setBorderPainted(false);
        cOButton8.setEnabledMethod("boutonModificationAutorise");
        cOButton8.setIconName("modifier16.gif");

        cOButton9.setActionName("supprimer");
        cOButton9.setBorderPainted(false);
        cOButton9.setEnabledMethod("peutSupprimer");
        cOButton9.setIconName("supprimer16.gif");

        org.jdesktop.layout.GroupLayout vueBoutonsModificationLayout = new org.jdesktop.layout.GroupLayout(vueBoutonsModification);
        vueBoutonsModification.setLayout(vueBoutonsModificationLayout);
        vueBoutonsModificationLayout.setHorizontalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOButton7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        vueBoutonsModificationLayout.setVerticalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsModificationLayout.createSequentialGroup()
                .add(cOButton7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        listeAffichage.setColumns(new Object[][] {{"","individu.identite",new Integer(2),"Individu",new Integer(0),new Integer(330),new Integer(1000),new Integer(10)},{"%d/%m/%Y","debutValidite",new Integer(0),"Début",new Integer(0),new Integer(80),new Integer(1000),new Integer(10)},{"%d/%m/%Y","finValidite",new Integer(0),"Fin",new Integer(0),new Integer(80),new Integer(1000),new Integer(10)},{"0.00","pattMontant",new Integer(0),"Montant",new Integer(0),new Integer(85),new Integer(1000),new Integer(10)},{"","temValide",new Integer(0),"Statut",new Integer(0),new Integer(50),new Integer(1000),new Integer(10)}});
        listeAffichage.setDisplayGroupForTable(displayGroup);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(12, 12, 12)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(layout.createSequentialGroup()
                                        .add(cOButton6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(15, 15, 15))
                                    .add(layout.createSequentialGroup()
                                        .add(jLabel2)
                                        .add(1, 1, 1)))
                                .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 644, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(4, 4, 4)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(cOButton5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 39, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                            .add(layout.createSequentialGroup()
                                .add(cOView4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(4, 4, 4)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(cOButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(cOLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 33, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(12, 12, 12)
                        .add(cOComboBox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 85, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(cOView2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(10, 10, 10)
                                .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 62, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createSequentialGroup()
                                .add(6, 6, 6)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(layout.createSequentialGroup()
                                        .add(10, 10, 10)
                                        .add(jLabel1))
                                    .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                        .add(18, 18, 18)
                        .add(cOView1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(11, 11, 11)
                        .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(cOView1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cOView4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(13, 13, 13)
                        .add(cOButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(32, 32, 32)
                        .add(cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(cOButton5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(cOButton6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(41, 41, 41)
                        .add(cOComboBox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(43, 43, 43)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(cOLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 160, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOView2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(11, 11, 11))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COButton cOButton1;
    public org.cocktail.component.COButton cOButton2;
    public org.cocktail.component.COButton cOButton3;
    public org.cocktail.component.COButton cOButton4;
    public org.cocktail.component.COButton cOButton5;
    public org.cocktail.component.COButton cOButton6;
    public org.cocktail.component.COButton cOButton7;
    public org.cocktail.component.COButton cOButton8;
    public org.cocktail.component.COButton cOButton9;
    public org.cocktail.component.COCheckbox cOCheckbox1;
    public org.cocktail.component.COComboBox cOComboBox1;
    public org.cocktail.component.COComboBox cOComboBox2;
    public org.cocktail.component.COLabel cOLabel1;
    public org.cocktail.component.COTextField cOTextField1;
    public org.cocktail.component.COTextField cOTextField2;
    public org.cocktail.component.COTextField cOTextField3;
    private org.cocktail.component.COView cOView1;
    private org.cocktail.component.COView cOView2;
    private org.cocktail.component.COView cOView3;
    private org.cocktail.component.COView cOView4;
    public org.cocktail.component.CODisplayGroup displayGroup;
    public org.cocktail.component.CODisplayGroup displayGroupCorps;
    public org.cocktail.component.CODisplayGroup displayGroupFonction;
    public org.cocktail.component.CODisplayGroup displayGroupGrade;
    public org.cocktail.component.CODisplayGroup displayGroupPopulation;
    public org.cocktail.component.CODisplayGroup displayGroupPrimes;
    public org.cocktail.component.CODisplayGroup displayGroupStructure;
    public org.cocktail.component.CODisplayGroup displayGroupTypeContrat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    public org.cocktail.component.COTable listeAffichage;
    public org.cocktail.component.COTable listeCorps;
    public org.cocktail.component.COTable listeFonction;
    public org.cocktail.component.COTable listeGrade;
    public org.cocktail.component.COTable listePopulation;
    public org.cocktail.component.COTable listeStructure;
    public org.cocktail.component.COTable listeTypeContrat;
    public org.cocktail.component.COView vueBoutonsModification;
    public javax.swing.JTabbedPane vueOngletsCriteres;
    public org.cocktail.component.COView vuePersonnalisation;
    // End of variables declaration//GEN-END:variables
                  

}

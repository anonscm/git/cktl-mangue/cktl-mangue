// _GestionPrimesIndividu_Interface.java
// Created on 12 août 2010 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _GestionPrimesIndividu_Interface extends COFrame {

    /** Creates new form _GestionPrimesIndividu_Interface */
    public _GestionPrimesIndividu_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new org.cocktail.component.CODisplayGroup();
        cOTextField3 = new org.cocktail.component.COTextField();
        jLabel1 = new javax.swing.JLabel();
        cOButton1 = new org.cocktail.component.COButton();
        cOComboBox2 = new org.cocktail.component.COComboBox();
        cOView3 = new org.cocktail.component.COView();
        cOTextField1 = new org.cocktail.component.COTextField();
        cOTextField2 = new org.cocktail.component.COTextField();
        listeAffichage = new org.cocktail.component.COTable();
        cOButton3 = new org.cocktail.component.COButton();
        vueBoutonsModification = new org.cocktail.component.COView();
        cOButton7 = new org.cocktail.component.COButton();
        cOButton8 = new org.cocktail.component.COButton();
        cOButton9 = new org.cocktail.component.COButton();
        cOView1 = new org.cocktail.component.COView();
        vuePersonnalisation = new org.cocktail.component.COView();

        displayGroup.setEntityName("PrimeAttribution");
        displayGroup.setHasDelegate(true);
        displayGroup.setIsMainDisplayGroupForController(true);

        setControllerClassName("org.cocktail.mangue.client.primes.GestionPrimesIndividu");
        setSize(new java.awt.Dimension(692, 483));

        cOTextField3.setEnabledMethod("peutSaisirAnnee");
        cOTextField3.setValueName("annee");

        jLabel1.setFont(new java.awt.Font("Helvetica", 0, 12)); // NOI18N
        jLabel1.setText("Année");

        cOButton1.setActionName("afficherAideGlobale");
        cOButton1.setBorderPainted(false);
        cOButton1.setIconName("help.gif");

        cOComboBox2.setEnabledMethod("peutSaisirAnnee");
        cOComboBox2.setTitleForSelection("typeValidite");
        cOComboBox2.setTitles("Valides,En cours,Historique");

        cOView3.setFont(new java.awt.Font("Helvetica", 0, 11)); // NOI18N
        cOView3.setIsBox(true);
        cOView3.setTitle("Période de référence de la prime\n");

        cOTextField1.setDateFieldFormat("%d/%m/%Y");
        cOTextField1.setEnabledMethod("nonEditable\n");
        cOTextField1.setValueName("finCampagne");

        cOTextField2.setDateFieldFormat("%d/%m/%Y");
        cOTextField2.setEnabledMethod("nonEditable");
        cOTextField2.setValueName("debutCampagne");

        org.jdesktop.layout.GroupLayout cOView3Layout = new org.jdesktop.layout.GroupLayout(cOView3);
        cOView3.setLayout(cOView3Layout);
        cOView3Layout.setHorizontalGroup(
            cOView3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOView3Layout.createSequentialGroup()
                .addContainerGap()
                .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 74, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 44, Short.MAX_VALUE)
                .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 74, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        cOView3Layout.setVerticalGroup(
            cOView3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView3Layout.createSequentialGroup()
                .addContainerGap()
                .add(cOView3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        listeAffichage.setColumns(new Object[][] {{"","prime.primLibCourt",new Integer(2),"Prime",new Integer(0),new Integer(328),new Integer(1000),new Integer(10)},{"%d/%m/%Y","debutValidite",new Integer(0),"Début",new Integer(0),new Integer(80),new Integer(1000),new Integer(10)},{"%d/%m/%Y","finValidite",new Integer(0),"Fin",new Integer(0),new Integer(80),new Integer(1000),new Integer(10)},{"0.00","pattMontant",new Integer(0),"Montant",new Integer(0),new Integer(90),new Integer(1000),new Integer(10)},{"","temValide",new Integer(0),"Statut",new Integer(0),new Integer(40),new Integer(1000),new Integer(10)}});
        listeAffichage.setDisplayGroupForTable(displayGroup);

        cOButton3.setActionName("renouveler");
        cOButton3.setBorderPainted(false);
        cOButton3.setEnabledMethod("peutRenouveler");
        cOButton3.setIconName("renouveler.gif");

        cOButton7.setActionName("ajouter");
        cOButton7.setBorderPainted(false);
        cOButton7.setEnabledMethod("peutAjouter");
        cOButton7.setIconName("ajouter16.gif");

        cOButton8.setActionName("modifier");
        cOButton8.setBorderPainted(false);
        cOButton8.setEnabledMethod("boutonModificationAutorise");
        cOButton8.setIconName("modifier16.gif");

        cOButton9.setActionName("supprimer");
        cOButton9.setBorderPainted(false);
        cOButton9.setEnabledMethod("peutSupprimer");
        cOButton9.setIconName("supprimer16.gif");

        org.jdesktop.layout.GroupLayout vueBoutonsModificationLayout = new org.jdesktop.layout.GroupLayout(vueBoutonsModification);
        vueBoutonsModification.setLayout(vueBoutonsModificationLayout);
        vueBoutonsModificationLayout.setHorizontalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOButton7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, cOButton8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        vueBoutonsModificationLayout.setVerticalGroup(
            vueBoutonsModificationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsModificationLayout.createSequentialGroup()
                .add(cOButton7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        cOView1.setFont(new java.awt.Font("Helvetica", 0, 11)); // NOI18N
        cOView1.setIsBox(true);
        cOView1.setTitle("Attribution");

        org.jdesktop.layout.GroupLayout vuePersonnalisationLayout = new org.jdesktop.layout.GroupLayout(vuePersonnalisation);
        vuePersonnalisation.setLayout(vuePersonnalisationLayout);
        vuePersonnalisationLayout.setHorizontalGroup(
            vuePersonnalisationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 650, Short.MAX_VALUE)
        );
        vuePersonnalisationLayout.setVerticalGroup(
            vuePersonnalisationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 192, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout cOView1Layout = new org.jdesktop.layout.GroupLayout(cOView1);
        cOView1.setLayout(cOView1Layout);
        cOView1Layout.setHorizontalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView1Layout.createSequentialGroup()
                .add(2, 2, 2)
                .add(vuePersonnalisation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(5, 5, 5))
        );
        cOView1Layout.setVerticalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView1Layout.createSequentialGroup()
                .add(vuePersonnalisation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(12, 12, 12))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(4, 4, 4)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, cOView1, 0, 659, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                                .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(68, 68, 68)
                                .add(jLabel1)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(cOComboBox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 94, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(67, 67, 67)
                                .add(cOView3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(org.jdesktop.layout.GroupLayout.LEADING, listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 637, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(4, 4, 4)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .addContainerGap(29, Short.MAX_VALUE)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel1)
                            .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(cOComboBox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(17, 17, 17))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(cOView3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 51, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)))
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(vueBoutonsModification, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 146, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOView1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 219, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COButton cOButton1;
    public org.cocktail.component.COButton cOButton3;
    public org.cocktail.component.COButton cOButton7;
    public org.cocktail.component.COButton cOButton8;
    public org.cocktail.component.COButton cOButton9;
    public org.cocktail.component.COComboBox cOComboBox2;
    public org.cocktail.component.COTextField cOTextField1;
    public org.cocktail.component.COTextField cOTextField2;
    public org.cocktail.component.COTextField cOTextField3;
    private org.cocktail.component.COView cOView1;
    private org.cocktail.component.COView cOView3;
    public org.cocktail.component.CODisplayGroup displayGroup;
    private javax.swing.JLabel jLabel1;
    public org.cocktail.component.COTable listeAffichage;
    public org.cocktail.component.COView vueBoutonsModification;
    public org.cocktail.component.COView vuePersonnalisation;
    // End of variables declaration//GEN-END:variables
                  

}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.primes;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.primes.SaisiePrimeAttributionView;
import org.cocktail.mangue.client.select.PrimeSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class SaisiePrimeAttributionCtrl extends ModelePageSaisie
{
	private static SaisiePrimeAttributionCtrl sharedInstance;
	private SaisiePrimeAttributionView myView;

	private EOPrimeAttribution currentAttribution;
	private EOPrime currentPrime;

	public SaisiePrimeAttributionCtrl(EOEditingContext edc) {

		super(edc);
		myView = new SaisiePrimeAttributionView(new JFrame(), true);

		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setActionBoutonValiderListener(myView.getBtnValider());
		
		myView.getBtnSelectPrime().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				selectPrime();
			}
		});

		setDateListeners(myView.getTfDebut());
		setDateListeners(myView.getTfFin());
		
		CocktailUtilities.initPopupAvecListe(myView.getPopupAnnees(), CocktailConstantes.LISTE_ANNEES_DESC, true);
		CocktailUtilities.initTextField(myView.getTfPrime(), false, false);

	}

	public static SaisiePrimeAttributionCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisiePrimeAttributionCtrl(editingContext);
		return sharedInstance;
	}

	public EOPrimeAttribution getCurrentAttribution() {
		return currentAttribution;
	}

	public void setCurrentAttribution(EOPrimeAttribution currentAttribution) {
		this.currentAttribution = currentAttribution;
		updateDatas();
	}
	
	public EOPrime getCurrentPrime() {
		return currentPrime;
	}
	public void setCurrentPrime(EOPrime currentPrime) {
		this.currentPrime = currentPrime;
		CocktailUtilities.viderTextField(myView.getTfPrime());
		if (currentPrime != null) {
			CocktailUtilities.setTextToField(myView.getTfPrime(), currentPrime.codeEtLibelle());
		}
	}

	private Integer getCurrentExercice() {
		if (myView.getPopupAnnees().getSelectedIndex() > 0)
			return (Integer)myView.getPopupAnnees().getSelectedItem();
		
		return null;
	}
	/**
	 * 
	 * @param prime
	 * @param modeCreation
	 * @return
	 */
	public boolean modifier(EOPrimeAttribution attribution, boolean modeCreation) {

		setModeCreation(modeCreation);
		setCurrentAttribution(attribution);
		myView.setVisible(true);
		return getCurrentPrime() != null;
		
	}
	
	private void selectPrime() {
		EOPrime prime = (EOPrime)PrimeSelectCtrl.sharedInstance(getEdc()).getObject();
		if (prime != null) {
			setCurrentPrime(prime);
		}
	}

	@Override
	protected void clearDatas() {

	}

	@Override
	protected void updateDatas() {
		
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentAttribution() != null) {
			
			myView.getPopupAnnees().setSelectedItem(getCurrentAttribution().pattExercice());
			setCurrentPrime(getCurrentAttribution().prime());
			CocktailUtilities.setNumberToField(myView.getTfMontant(), getCurrentAttribution().pattMontant());
			CocktailUtilities.setDateToField(myView.getTfDebut(), getCurrentAttribution().debutValidite());
			CocktailUtilities.setDateToField(myView.getTfFin(), getCurrentAttribution().finValidite());

		}
		
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		getCurrentAttribution().setDModification(new NSTimestamp());
		getCurrentAttribution().setPrimeRelationship(getCurrentPrime());
		getCurrentAttribution().setPattMontant(CocktailUtilities.getBigDecimalFromField(myView.getTfMontant()));
		getCurrentAttribution().setDebutValidite(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		getCurrentAttribution().setFinValiditeFormatee(CocktailUtilities.getTextFromField(myView.getTfFin()));
		getCurrentAttribution().setPattExercice(getCurrentExercice());

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentPrime(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		
	}	

}

//Created on Wed Jun 11 12:24:00 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */package org.cocktail.mangue.client.primes;

 import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.AideUtilisateur;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.outils_interface.ModelePageAvecIndividu;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.PrimeResultatCalcul;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

 //18/11/09 - modification de la suppression des attributions pour supprimer la personnalisation (si elle existe) liée à l'attribution à supprimer
//22/12/2010 - Adaptation Netbeans
public class GestionPrimesIndividu extends ModelePageAvecIndividu {
	 public final static String PREFIXE_POUR_ATTRIBUTION = "Individuelle_";
	 public EOView vuePersonnalisation;
	 private String typeValidite;
	 private final static String TYPE_VALIDE = "Valides";
	 private final static String TYPE_EN_COURS = "En cours";	
	 private final static String TYPE_HISTORIQUE = "Historique";
	 private boolean preparationFenetre = true,verifierBudget;
	 private String annee;
	 private NSTimestamp debutCampagne,finCampagne;
	 private GestionAttribution controleurAttribution;

	 // Accesseurs
	 public String typeValidite() {
		 return typeValidite;
	 }
	 public void setTypeValidite(String typeValidite) {
		 this.typeValidite = typeValidite;
		 if (preparationFenetre) {
			 return;
		 }
		 if (typeValidite != null) {
			 parametrerDisplayGroup();
		 } else {
			 displayGroup().setObjectArray(null);
		 }
		 updaterDisplayGroups();
		 setEditingContext(editingContext());
	 }
	 public String annee() {
		 return annee;
	 }
	 /** V&eacute;rifie que l'ann&eacute;e est sur 4 digits */
	 public void setAnnee(String annee) {
		 this.annee = annee;
		 if (annee != null && annee.length() > 0) {
			 if (annee.length() < 4) {
				 this.annee = null;
			 } else {
				 // Vérifier si il s'agit bien d'un nombre entier
				 try {
					 int numAnnee = new Integer(annee).intValue();
					 if (numAnnee < 1900 || numAnnee > 2100) {
						 this.annee = null;
					 }
				 } catch (Exception e) {
					 this.annee = null;
				 }
			 }
		 }	
		 controleurAttribution.setAnnee(this.annee);
		 preparerDisplayGroup();
		 updaterDisplayGroups();
		 raffraichirAssociations();
	 }
	 public NSTimestamp debutCampagne() {
		 return debutCampagne;
	 }
	 public NSTimestamp finCampagne() {
		 return finCampagne;
	 }
	 /** Surcharge pour informer le controleur d'attribution */
	 public void setModificationEnCours(boolean aBool) {
		 super.setModificationEnCours(aBool);
		 controleurAttribution.setModificationEnCours(aBool);
	 }
	 protected void terminer() {
		 controleurAttribution.terminer();
	 }
	 // Méthodes de délégation du displayGroup
	 public void displayGroupDidChangeSelection(EODisplayGroup group) {
		 // Préparer la zone indiquant les paramétrages de calcul
		 if (!modificationEnCours()) {
			 if (group == displayGroup()) {
				 if (currentAttribution() == null) {
					 controleurAttribution.preparerPourAttributionsEtSelection(null, null,false);
				 } else {
					 controleurAttribution.preparerPourAttributionsEtSelection(attributionsLieesPourPrime(currentAttribution().prime()), currentAttribution(),modeSaisiePossible());
				 }
				 preparerPeriodeCampagne();	// Pour afficher la campagne liée à la prime
			 }
			 super.displayGroupDidChangeSelection(group);
		 }
	 }
	 // Actions
	 public void renouveler() {
		 LogManager.logDetail("GestionPrimesIndividu - renouveler");
		 Class[] classeParametres = new Class[] {EOGlobalID.class};
		 Object[] parametres = new Object[] {editingContext().globalIDForObject(currentAttribution())};
		 try {
			 NSTimestamp debut = new NSTimestamp();
			 LogManager.logDetail("GestionPrimesIndividu - Lancement du renouvellement de l'attribution sur le serveur");
			 EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			 PrimeResultatCalcul resultatCalcul = (PrimeResultatCalcul)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestRenouvelerAttribution",classeParametres,parametres,true);

			 NSTimestamp fin = new NSTimestamp();
			 long duree = (fin.getTime() - debut.getTime()) / 1000;
			 LogManager.logDetail("GestionAttribution - Fin du traitement sur le serveur, duree " + duree + " secondes");
			 if (resultatCalcul != null && resultatCalcul.diagnostic() != null && resultatCalcul.diagnostic().length() > 0) {
				 // Une erreur s'est produite pendant les calculs
				 EODialogs.runErrorDialog("ERREUR", resultatCalcul.diagnostic());
			 }
		 } catch (Exception e) {
			 LogManager.logException(e);
			 EODialogs.runErrorDialog("Erreur",e.getMessage());
		 }
		 // Changer l'année pour afficher l'année suivante
		 setAnnee("" + (new Integer(annee()).intValue() + 1));
		 // Positionner aussi sur les primes en cours
		 setTypeValidite(TYPE_EN_COURS);
	 }
	 public void afficherAideGlobale() {
		 AideUtilisateur controleur = new AideUtilisateur("Aide_Prime_Individu","Gestion_Primes");
		 controleur.afficherFenetre();
	 }
	 // Notifications
	 /** Notification envoy&eacute;e lorsque les personnalisations sont modifi&eacute;es. L'objet de la notification contient un dictionnaire
	  * avec la prime et l'individu */
	 public void reafficherAttributions(NSNotification aNotif) {
		 LogManager.logDetail("GestionPrimesIndividu - Notification réafficher attributions");
		 NSDictionary dict = (NSDictionary)aNotif.object();
		 EOGlobalID individuID = (EOGlobalID)dict.valueForKey("individu");
		 EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuID, editingContext());
		 if (currentIndividu() == individu) {
			 preparerDisplayGroup();
			 updaterDisplayGroups();
		 }
	 }
	 /** Suite &agrave; l'annulation de la modification d'une attribution */
	 public void annulationAttribution(NSNotification aNotif) {
		 if (modificationEnCours()) {
			 LogManager.logDetail("GestionPrimesIndividu - Notification annulation attribution");
			 annuler();
			 if (currentAttribution() != null) {
				 controleurAttribution.preparerPourAttributionsEtSelection(attributionsLieesPourPrime(currentAttribution().prime()), currentAttribution(),modeSaisiePossible());
			 } else {
				 controleurAttribution.preparerPourAttributionsEtSelection(null, null,false);
			 }
		 }
	 }
	 /** Suite &agrave; la validation de la modification d'une attribution */
	 public void validationAttribution(NSNotification aNotif) {
		 if (modificationEnCours()) {
			 LogManager.logDetail("GestionPrimesIndividu - Notification validation attribution");
			 super.traitementsApresValidation();
			 preparerDisplayGroup(); // pour réafficher les objets éventuellement créé
		 }
	 }
	 /** Suite &agrave; l'&eacute;dition ou non d'une attribution, l'objet de la notification contient un booleen */
	 public void editionAttribution(NSNotification aNotif) {
		 if (modificationEnCours()) {
			 LogManager.logDetail("GestionPrimesIndividu - Notification edition attribution");
			 boolean edited = ((Boolean)aNotif.object()).booleanValue();
			 setEdited(edited);
		 }
	 }
	 public void raffraichir(NSNotification aNotif) {
		 LogManager.logDetail("GestionPrimesIndividu - Notification raffraichir attribution");
		 preparerDisplayGroup(); // pour réafficher les objets
	 }
	 /** Suite &agrave; la s&eacute;lection d'une prime dans l'attribution */
	 public void changerDateCampagnes(NSNotification aNotif) {
		 LogManager.logDetail("GestionPrimesIndividu - Notification changer dates campagne");
		 NSDictionary dict = (NSDictionary)aNotif.object();
		 if (dict != null) {
			 debutCampagne = (NSTimestamp)dict.objectForKey("debutCampagne");
			 finCampagne = (NSTimestamp)dict.objectForKey("finCampagne");
		 } else {
			 preparerPeriodeCampagne();
		 }
		 updaterDisplayGroups();
	 }
	 // Méthodes du controller DG
	 /** On ne peut changer d'ann&eacute;e que si on n'est pas en mode modification et que la saisie est possible */
	 public boolean peutSaisirAnnee() {
		 return super.modeSaisiePossible();
	 }
	 /** on ne peut ajouter une prime que si la saisie est possible et que les crit&egrave;res de primes sont correctement d&eacute;finis */
	 public boolean peutAjouter() {
		 return super.modeSaisiePossible() && annee() != null && typeValidite().equals(TYPE_EN_COURS);
	 }
	 /** On ne peut renouveler que les attributions valides apr&egrave;s avoir v&eacute;rifi&eacute; que l'individu
	  * peut en b&acute;n&eacute;ficier */
	 public boolean peutRenouveler() {
		 return super.boutonModificationAutorise() && typeValidite().equals(TYPE_VALIDE) && currentPrime() != null && currentPrime().estAttributionUniqueDansAnnee() == false;
	 }
	 /** On ne peut modifier une attribution que si les modifications sont autoris&eacute;es,qu'elle est n'est pas dans l'historique et
	  * que l'individu et la prime de cette attribution sont d&eacute;finis */
	 public boolean boutonModificationAutorise() {
		 return super.boutonModificationAutorise() && currentAttribution().estInvalide() == false && currentAttribution().prime() != null && 
		 currentAttribution().individu() != null;
	 }
	 /** On ne peut pas supprimer une prime d&eacute;j&agrave; invalid&eacute;e */
	 public boolean peutSupprimer() {
		 return super.boutonModificationAutorise() && !currentAttribution().estInvalide();
	 }
	 /** on ne peut imprimer que si toutes les attributions pour cette prime ont &eacute;t&eacute; valid&eacute;es */
	 public boolean peutImprimerArrete() {
		 return boutonModificationAutorise() && typeValidite() != null && typeValidite().equals(TYPE_VALIDE) && attributionsValidees();
	 }
	 // Méthodes protégées
	 /** true si l'agent peut g&eacute;rer les primes */
	 protected boolean conditionSurPageOK() {
		 EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		 return agent.peutGererPrimes();
	 }
	 protected void preparerFenetre() {
		 preparationFenetre = true;
		 controleurAttribution = new GestionAttribution(editingContext(),false);	// Il s'agit d'une gestion individuelle des primes
		 GraphicUtilities.swaperViewEtCentrer(vuePersonnalisation, controleurAttribution.component());
		 setTypeValidite(TYPE_VALIDE);
		 super.preparerFenetre();	// On le fait après pour que le contrôleur d'attribution soit instancié
		 setAnnee((new Integer(DateCtrl.getYear(new NSTimestamp()))).toString()); // On le fait après pour que le contrôleur d'attribution soit instancié
		 preparationFenetre = false;
	 }
	 protected void loadNotifications() {
		 super.loadNotifications();
		 NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("reafficherAttributions", new Class[] {NSNotification.class}),
				 GestionPersonnalisations.NOTIFICATION_ATTRIBUTION_MODIFIEE,null);
		 NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("annulationAttribution", new Class[] {NSNotification.class}),
				 PREFIXE_POUR_ATTRIBUTION + GestionAttribution.NOTIFICATION_ANNULER_ATTRIBUTION,null);	
		 NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("validationAttribution", new Class[] {NSNotification.class}),
				 PREFIXE_POUR_ATTRIBUTION + GestionAttribution.NOTIFICATION_VALIDER_ATTRIBUTION,null);
		 NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("editionAttribution", new Class[] {NSNotification.class}),
				 PREFIXE_POUR_ATTRIBUTION + GestionAttribution.NOTIFICATION_EDITION_ATTRIBUTION,null);
		 NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("raffraichir", new Class[] {NSNotification.class}),
				 PREFIXE_POUR_ATTRIBUTION + GestionAttribution.NOTIFICATION_RAFFRAICHIR_ATTRIBUTION,null);
		 NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("changerDateCampagnes", new Class[] {NSNotification.class}),
				 PREFIXE_POUR_ATTRIBUTION + GestionAttribution.NOTIFICATION_CAMPAGNE_MODIFIEE,null);
	 }
	 protected NSArray fetcherObjets() {
		 if (annee() == null || currentIndividu() == null) {
			 return null;
		 } else {
			 return EOPrimeAttribution.rechercherAttributionsPourIndividuEtExercice(editingContext(), currentIndividu(),new Integer(annee));
		 }
	 }
	 protected void traitementsPourCreation() {
		 controleurAttribution.preparerPourAttributionsEtSelection(new NSArray(currentAttribution()), currentAttribution(),true);
		 controleurAttribution.traitementsPourCreation(null,currentIndividu());
	 }
	 protected String messageConfirmationDestruction() {	
		 String message = "";
		 if (attributionsLieesPourPrime(currentAttribution().prime()).count() > 1) {
			 message = "\nToutes les attributions de cette prime pour l'année seront supprimées";
		 }
		 return "Voulez-vous vraiment supprimer cette attribution ?" + message;
	 }
	 /** Surcharge du parent pour supprimer du budget le montant de l'attribution si celle-ci est supprim&eacute;e. On supprime
	  * toutes les attributions qui sont li&eacute;es */
	 protected boolean traitementsPourSuppression() {
		 java.util.Enumeration e = attributionsLieesPourPrime(currentAttribution().prime()).objectEnumerator();
		 while (e.hasMoreElements()) {
			 EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
			 if (attribution.estValide() && verifierBudget) {

				 VerificateurBudget verificateur = new VerificateurBudget(editingContext(),attribution.prime(),debutCampagne,finCampagne);
				 try {
					 verificateur.supprimerMontantAttribution(attribution);
				 } catch (Exception exc) {
					 EODialogs.runErrorDialog("ERREUR", "Impossible d'invalider l'attribution.\n" + exc.getMessage());
					 return false;	// Revert effectué par le parent
				 }
			 }
			 EOPrimePersonnalisation personnalisation = EOPrimePersonnalisation.rechercherPersonnalisationEnCoursPourIndividuPrimeEtPeriode(editingContext(),attribution.individu(),attribution.prime(),attribution.debutValidite(),attribution.finValidite());
			 // Si on trouve une personnalisation, l'invalider
			 if (personnalisation != null) {
				 personnalisation.setEstValide(false);
				 NSArray parametresPerso = EOPrimeParamPerso.rechercherParametresPersonnelsValidesPourPersonnalisation(editingContext(), personnalisation);
				 java.util.Enumeration e1 = parametresPerso.objectEnumerator();
				 while (e1.hasMoreElements()) {
					 EOPrimeParamPerso param = (EOPrimeParamPerso)e1.nextElement();
					 param.setEstValide(false);
				 }
			 }
			 attribution.setEstInvalide();
		 }
		 return true;
	 }
	 protected void parametrerDisplayGroup() {
		 if (typeValidite() == null) {
			 displayGroup().setQualifier(null);
		 } else if (typeValidite().equals(TYPE_VALIDE)) {
			 displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("temValide = 'O'", null));
		 } else if (typeValidite().equals(TYPE_EN_COURS)) {
			 displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("temValide = 'C' OR temValide = 'S' or temValide = 'P'", null));
		 } else if (typeValidite().equals(TYPE_HISTORIQUE)) {
			 displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("temValide = 'N'", null));
		 }
		 updaterDisplayGroups();
	 }
	 /** La validation est g&eacute;r&eacute;e par le composant d'attribution */
	 protected boolean traitementsAvantValidation() {
		 return true;
	 }
	 /** Surcharge du parent pour masquer ou afficher les boutons de validation des attributions */
	 protected void afficherBoutons(boolean estValidation) {
		 super.afficherBoutons(estValidation);
		 controleurAttribution.afficherBoutons(estValidation);
	 }
	 // Méthodes privées
	 private EOPrimeAttribution currentAttribution() {
		 return (EOPrimeAttribution)displayGroup().selectedObject();
	 }
	 private EOPrime currentPrime() {
		 if (currentAttribution() == null) {
			 return null;
		 } else {
			 return currentAttribution().prime();
		 }
	 }
	 private void preparerDisplayGroup() {
		 displayGroup().setObjectArray(fetcherObjets());
		 if (currentAttribution() == null) {
			 controleurAttribution.preparerPourAttributionsEtSelection(null, null,false);
		 } else {
			 controleurAttribution.preparerPourAttributionsEtSelection(attributionsLieesPourPrime(currentAttribution().prime()), currentAttribution(),modeSaisiePossible());
		 }
	 }
	 private void preparerPeriodeCampagne() {
		 if (annee() == null || currentPrime() == null) {
			 debutCampagne = null;
			 finCampagne = null;
		 } else {
			 int anneeInt = new Integer(annee()).intValue();
			 debutCampagne = currentPrime().debutPeriode(anneeInt);
			 finCampagne = currentPrime().finPeriode(anneeInt);
		 }
	 }
	 private NSArray attributionsLieesPourPrime(EOPrime prime) {
		 // Une prime unique dans l'année peut être définie à travers plusieurs attributions, ce qui n'est pas le cas des primes
		 // de type horaire
		 if (prime != null && prime.estAttributionUniqueDansAnnee()) {
			 java.util.Enumeration e = displayGroup().displayedObjects().objectEnumerator();
			 NSMutableArray attributions = new NSMutableArray();
			 while (e.hasMoreElements()) {
				 EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
				 if (attribution.prime() == currentAttribution().prime()) {
					 attributions.addObject(attribution);
				 }
			 }
			 return attributions;
		 } else {
			 if (currentAttribution() != null) {
				 return new NSArray(currentAttribution());
			 } else {
				 return new NSArray();
			 }
		 }
	 }
	 // retourne true si toutes les attributions liées ne sont pas en cours d'évaluation
	 private boolean attributionsValidees() {
		 if (currentPrime() != null) {
			 java.util.Enumeration e = displayGroup().allObjects().objectEnumerator();	// Pour voir les objets en cours...
			 while (e.hasMoreElements()) {
				 EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
				 if (attribution.prime() == currentAttribution().prime()) {
					 if (attribution.estCalculee() || attribution.estSuspendue() || attribution.estProvisoire()) {
						 return false;
					 }
				 }
			 }
			 return true;
		 } else {
			 return false;
		 }
	 }
 }

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import java.math.BigDecimal;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeBudget;
import org.cocktail.mangue.modele.goyave.EOPrimeExercice;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class VerificateurBudget {
	private EOPrimeBudget budgetExercice1,budgetExercice2;
	private BigDecimal montantExercice1,montantExercice2;
	private boolean budgetDepasse,exerciceUnique;

	public VerificateurBudget(EOEditingContext editingContext, EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		int anneeDebut = DateCtrl.getYear(debutPeriode);
		int anneeFin = DateCtrl.getYear(finPeriode);
		budgetExercice1 = EOPrimeExercice.budgetPourPrimeEtExercice(editingContext,prime, new Integer(anneeDebut));
		if (budgetExercice1 == null) {	// Il s'agit du budget global
			budgetExercice1 = EOPrimeBudget.rechercherBudgetGlobalPourExercice(editingContext,  new Integer(anneeDebut));
		}
		montantExercice1 = budgetExercice1.pbudCredit().subtract(budgetExercice1.pbudEngage());
		budgetDepasse = false;
		exerciceUnique = true;
		if (anneeFin != anneeDebut) {
			budgetExercice2 = EOPrimeExercice.budgetPourPrimeEtExercice(editingContext,prime, new Integer(anneeFin));
			if (budgetExercice2 == null) {	// Il s'agit du budget global
				budgetExercice2 = EOPrimeBudget.rechercherBudgetGlobalPourExercice(editingContext,  new Integer(anneeFin));
			}
			montantExercice2 = budgetExercice2.pbudCredit().subtract(budgetExercice2.pbudEngage());
			exerciceUnique = false;
		} 
	}
	/** V&eacute;rifie si le montant de l'attribution est disponible dans le budget et si c'est le cas, met le budget &agrave; jour */
	public boolean validerAttribution(EOPrimeAttribution attribution,boolean updaterBudget) throws Exception {
		int anneeDebut = DateCtrl.getYear(attribution.debutValidite());
		int anneeFin = DateCtrl.getYear(attribution.finValidite());
		// Vérifier que les dates de l'attribution sont cohérentes avec les budgets
		if (anneeDebut != anneeFin && exerciceUnique) {
			throw new Exception("L'attribution court sur 2 années alors qu'elle ne devrait être rattachée qu'à un seul exercice");
		}
		if (anneeDebut == anneeFin) {
			montantExercice1 = montantExercice1.subtract(attribution.pattMontant());
			budgetDepasse = montantExercice1.doubleValue() < 0;
			if (!budgetDepasse && updaterBudget) {
				budgetExercice1.setPbudEngage(budgetExercice1.pbudEngage().subtract(attribution.pattMontant()));
			}
		} else {
			// Calculer le prorata sur chaque période
			int nbJoursSurAnnee = DateCtrl.nbJoursEntre(attribution.debutValidite(), DateCtrl.stringToDate("31/12/" + anneeDebut), true);
			int nbJoursTotal = DateCtrl.nbJoursEntre(attribution.debutValidite(),attribution.finValidite(),true);
			double montantPourAnnee = (attribution.pattMontant().doubleValue() * nbJoursSurAnnee) / nbJoursTotal;
			BigDecimal montant = new BigDecimal(montantPourAnnee).setScale(2,BigDecimal.ROUND_HALF_UP);
			montantExercice1  = montantExercice1.subtract(montant);
			budgetDepasse = montantExercice1.doubleValue() < 0;
			if (!budgetDepasse) {
				if (updaterBudget) {
					budgetExercice1.setPbudEngage(budgetExercice1.pbudEngage().subtract(montant));
				}
				nbJoursSurAnnee = DateCtrl.nbJoursEntre(DateCtrl.stringToDate("01/01/" + anneeFin),attribution.finValidite(), true);
				montantPourAnnee = (attribution.pattMontant().doubleValue() * nbJoursSurAnnee) / nbJoursTotal;
				montant = new BigDecimal(montantPourAnnee).setScale(2,BigDecimal.ROUND_HALF_UP);
				montantExercice2  = montantExercice2.subtract(montant);
				budgetDepasse = montantExercice2.doubleValue() < 0;
				if (!budgetDepasse && updaterBudget) {
					budgetExercice2.setPbudEngage(budgetExercice2.pbudEngage().subtract(montant));
				}
			}
		}
		return budgetDepasse;
	}
	public boolean validerAttribution(EOPrimeAttribution attribution) throws Exception {
		return validerAttribution(attribution,false);
	}
	/** Ajoute le montant de l'attribution du(des) budget(s) suite &agrave; l'annulation d'une attribution */
	public void supprimerMontantAttribution(EOPrimeAttribution attribution) throws Exception {
		int anneeDebut = DateCtrl.getYear(attribution.debutValidite());
		int anneeFin = DateCtrl.getYear(attribution.finValidite());
		// Vérifier que les dates de l'attribution sont cohérentes avec les budgets
		if (anneeDebut != anneeFin && exerciceUnique) {
			throw new Exception("L'attribution court sur 2 années alors qu'elle ne devrait être rattachée qu'à un seul exercice");
		}
		if (anneeDebut == anneeFin) {
			montantExercice1  = montantExercice1.add(attribution.pattMontant());	// Pour avoir le montant à jour
			budgetExercice1.setPbudEngage(budgetExercice1.pbudEngage().add(attribution.pattMontant()));
		} else {
			// Calculer le prorata sur chaque période
			int nbJoursSurAnnee = DateCtrl.nbJoursEntre(attribution.debutValidite(), DateCtrl.stringToDate("31/12/" + anneeDebut), true);
			int nbJoursTotal = DateCtrl.nbJoursEntre(attribution.debutValidite(),attribution.finValidite(),true);
			double montantPourAnnee = (attribution.pattMontant().doubleValue() * nbJoursSurAnnee) / nbJoursTotal;
			BigDecimal montant = new BigDecimal(montantPourAnnee).setScale(2,BigDecimal.ROUND_HALF_UP);
			montantExercice1  = montantExercice1.add(montant);
			budgetExercice1.setPbudEngage(budgetExercice1.pbudEngage().add(montant));
			nbJoursSurAnnee = DateCtrl.nbJoursEntre(DateCtrl.stringToDate("01/01/" + anneeFin),attribution.finValidite(), true);
			montantPourAnnee = (attribution.pattMontant().doubleValue() * nbJoursSurAnnee) / nbJoursTotal;
			montant = new BigDecimal(montantPourAnnee).setScale(2,BigDecimal.ROUND_HALF_UP);
			montantExercice2  = montantExercice2.add(montant);
			budgetExercice2.setPbudEngage(budgetExercice2.pbudEngage().add(montant));
		}
	}
}

// Created on Wed Jun 11 12:24:00 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import java.awt.Font;

import org.cocktail.mangue.client.outils_interface.InspecteurAvecDisplayGroup;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.swing.EOTextArea;
import com.webobjects.foundation.NSArray;

/** Affiche les attributions des ann&eacute;es pr&eacute;c&eacute;dentes d'un individu : la notification attend comme objet
 * courant l'attribution s&eacute;lectionn&eacute;e dans la page principale
 * @author christine
 *
 */
// 22/12/2010 - Adaptation Netbeans + correction d'un bug lorsque l'attribution est nulle
public class InspecteurAttributionsPrecedentes extends InspecteurAvecDisplayGroup {
	public EOTextArea vueTexte;
	
	public InspecteurAttributionsPrecedentes(EOEditingContext editingContext,String nomNotification) {
		super(editingContext, nomNotification);
	}
	public void init() {
		EOArchive.loadArchiveNamed("InspecteurAttributionsPrecedentes",this,"org.cocktail.mangue.client.primes.interfaces",this.disposableRegistry());
		displayGroup.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending)));
		Font font = vueTexte.getFont();
		vueTexte.textArea().setFont(new Font(font.getFontName(),font.getStyle(),11));
	}
	public void connectionWasEstablished() {
		super.connectionWasEstablished();
	}
	// Méthodes protégées
	protected NSArray fetcherObjets() {
		if (objetCourant() != null) {
			// on attend une attribution
			EOPrimeAttribution attribution = (EOPrimeAttribution)objetCourant();
			if (attribution == null || attribution.individu() == null || attribution.pattExercice() == null) {	// 21/12/2010
				return null;
			}
			return EOPrimeAttribution.rechercherAttributionsValidesPourIndividuAnterieuresExercice(editingContext, attribution.individu(), (Integer)attribution.pattExercice());
		} else {
			return null;
		}

	}
}

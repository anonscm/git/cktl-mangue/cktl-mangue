/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPrimePopulation;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;

// 22/12/2010 - Adaptation Netbeans (Suppression de preparerFenetre)
// 16/02/2011 - Restriction du qualifier sur les types de population pour les établissements qui ne gèrent pas les populations HU ou les normaliens
public class GestionPopulations extends GestionPourPrime {
	// Accesseurs
	public String corps() {
		if (currentPrimePopulation() == null || currentPrimePopulation().corps() == null) {
			return null;
		} else {
			return currentPrimePopulation().corps().cCorps();
		}
	}
	public void setCorps(String unCorps) {
		if (unCorps != null) {
			EOCorps corps = (EOCorps)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "Corps", "cCorps", unCorps);
			if (corps != null) {
				if (((NSArray)displayGroup().displayedObjects().valueForKey("corps")).containsObject(corps) == false) {
					currentPrimePopulation().addObjectToBothSidesOfRelationshipWithKey(corps, "corps");
				} else {
					EODialogs.runErrorDialog("ERREUR", "Ce corps est déjà saisi");
				}
			}
		} else {
			supprimerCorps();
		}
		updaterDisplayGroups();
	}
	public String grade() {
		if (currentPrimePopulation() == null || currentPrimePopulation().grade() == null) {
			return null;
		} else {
			return currentPrimePopulation().grade().cGrade();
		}
	}
	public void setGrade(String unGrade) {
		if (unGrade != null) {
			EOGrade grade = (EOGrade)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), "Grade", "cGrade", unGrade);
			if (grade != null) {
				if (((NSArray)displayGroup().displayedObjects().valueForKey("grade")).containsObject(grade) == false) {
					currentPrimePopulation().addObjectToBothSidesOfRelationshipWithKey(grade, "grade");
				} else {
					EODialogs.runErrorDialog("ERREUR", "Ce grade est déjà saisi");
				}
			}
		} else {
			supprimerGrade();
		}
		updaterDisplayGroups();
	}
	// Actions
	public void afficherTypePopulation() {
		NSMutableArray qualifiers = new NSMutableArray();

		if (EOGrhumParametres.isGestionHu() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temHospitalier = 'N'", null));
		}
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.DATE_OUVERTURE_KEY+" = nil or " + EOTypePopulation.DATE_OUVERTURE_KEY+"<=%@", new NSArray(currentPrime().debutValidite())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypePopulation.DATE_FERMETURE_KEY+" = nil or " + EOTypePopulation.DATE_FERMETURE_KEY + " >=%@", new NSArray(currentPrime().debutValidite())));

		if (EOGrhumParametres.isGestionEns() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("code != 'N'",null));
		}
		UtilitairesDialogue.afficherDialogue(this,"TypePopulation", "getTypePopulation", false,new EOAndQualifier(qualifiers), false);
	}
	public void afficherCorps() {
		NSMutableArray qualifiers = new NSMutableArray();

		if (currentPrimePopulation().typePopulation() == null) {
			if (EOGrhumParametres.isGestionHu() == false) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toTypePopulation.temHospitalier = 'N'", null));
			}
			if (EOGrhumParametres.isGestionEns() == false) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toTypePopulation.code != 'N'",null));
			}
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toTypePopulation = %@",new NSArray(currentPrimePopulation().typePopulation())));
		}
		UtilitairesDialogue.afficherDialogue(this,"Corps", "getCorps", true,new EOAndQualifier(qualifiers), false);
	}
	public void afficherGrade() {
		NSMutableArray qualifiers = new NSMutableArray();
		// 16/02/2011
		if (currentPrimePopulation().corps() == null) {
			if (currentPrimePopulation().typePopulation() == null) {

				if (EOGrhumParametres.isGestionHu() == false) {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps.toTypePopulation.temHospitalier = 'N'", null));
				}
				if (EOGrhumParametres.isGestionEns() == false) {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps.toTypePopulation.code != 'N'",null));
				}
			} else {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps.toTypePopulation = %@",new NSArray(currentPrimePopulation().typePopulation())));
			}
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps = %@",new NSArray(currentPrimePopulation().corps())));
		}
		UtilitairesDialogue.afficherDialogue(this,"Grade", "getGrade", true, new EOAndQualifier(qualifiers),false);
	}
	
	/**
	 * 
	 */
	public void afficherTypeContrat() {
		EOQualifier qualifier = null;
		if (EOGrhumParametres.isGestionHu() == false) {
			qualifier = EOQualifier.qualifierWithQualifierFormat("temAhCuAo = 'N'", null);
		}		
		UtilitairesDialogue.afficherDialogue(this,"TypeContratTravail", "getTypeContrat", false, qualifier,false);
	}
	public void supprimerTypePopulation() {
		currentPrimePopulation().removeObjectFromBothSidesOfRelationshipWithKey(currentPrimePopulation().typePopulation(),"typePopulation");
	}
	public void supprimerCorps() {
		currentPrimePopulation().removeObjectFromBothSidesOfRelationshipWithKey(currentPrimePopulation().corps(),"corps");
	}
	public void supprimerGrade() {
		currentPrimePopulation().removeObjectFromBothSidesOfRelationshipWithKey(currentPrimePopulation().grade(),"grade");
	}
	public void supprimerTypeContrat() {
		currentPrimePopulation().removeObjectFromBothSidesOfRelationshipWithKey(currentPrimePopulation().typeContratTravail(),"typeContratTravail");
	}

	// Notifications
	public void getTypePopulation(NSNotification aNotif) {
		if (!estLocke()) {
			EOGenericRecord recordDestin = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			if (((NSArray)displayGroup().displayedObjects().valueForKey("typePopulation")).containsObject(recordDestin) == false) {
				ajouterRelation(currentPrimePopulation(), aNotif.object(), "typePopulation");
			} else {
				EODialogs.runErrorDialog("ERREUR","Ce type de population est déjà saisi");
			}
		}
	}
	public void getCorps(NSNotification aNotif) {
		if (!estLocke()) {
			EOGenericRecord recordDestin = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			if (((NSArray)displayGroup().displayedObjects().valueForKey("corps")).containsObject(recordDestin) == false) {
				ajouterRelation(currentPrimePopulation(), aNotif.object(), "corps");
			} else {
				EODialogs.runErrorDialog("ERREUR","Ce corps est déjà saisi");
			}
		}
	}
	public void getGrade(NSNotification aNotif) {
		if (!estLocke()) {
			EOGenericRecord recordDestin = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			if (((NSArray)displayGroup().displayedObjects().valueForKey("grade")).containsObject(recordDestin) == false) {
				ajouterRelation(currentPrimePopulation(), aNotif.object(), "grade");
			} else {
				EODialogs.runErrorDialog("ERREUR","Ce grade est déjà saisi");
			}
		}
	}
	public void getTypeContrat(NSNotification aNotif) {
		if (!estLocke()) {
			EOGenericRecord recordDestin = SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
			if (((NSArray)displayGroup().displayedObjects().valueForKey("typeContratTravail")).containsObject(recordDestin) == false) {
				ajouterRelation(currentPrimePopulation(), aNotif.object(), "typeContratTravail");
			} else {
				EODialogs.runErrorDialog("ERREUR","Ce type de contrat est déjà saisi");
			}
		}
	}
	// Méthodes du controller DG
	public boolean peutSupprimerTypePopulation() {
		return modificationEnCours() && currentPrimePopulation().typePopulation() != null;
	}
	public boolean peutSupprimerCorps() {
		return modificationEnCours() && currentPrimePopulation().corps() != null;
	}
	public boolean peutSupprimerGrade() {
		return modificationEnCours() && currentPrimePopulation().grade() != null;
	}
	public boolean peutSupprimerTypeContrat() {
		return modificationEnCours() && currentPrimePopulation().typeContratTravail() != null;
	}
	public boolean peutValider() {
		return super.peutValider() && (currentPrimePopulation().typePopulation() != null || currentPrimePopulation().corps() != null ||
				currentPrimePopulation().grade() != null || currentPrimePopulation().typeContratTravail() != null);
	}
	/** On ne peut saisir que si la prime demande des v&eacute;rifications de population ou corps ou grade ou type contrat */
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && (currentPrime().verifierPopulation() || currentPrime().verifierCorps() || currentPrime().verifierGrade() ||
				currentPrime().verifierContrat());
	}
	// Méthodes protégées
	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		displayGroupPrimes.setQualifier(EOQualifier.qualifierWithQualifierFormat("primVerifPopulation = 'O' OR primVerifCorps = 'O' OR primVerifGrade = 'O' OR primVerifContrat = 'O'", null));
		NSMutableArray sortOrderings = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("typePopulation.libelleCourt", EOSortOrdering.CompareAscending));
		sortOrderings.addObject(EOSortOrdering.sortOrderingWithKey("corps.cCorps", EOSortOrdering.CompareAscending));
		sortOrderings.addObject(EOSortOrdering.sortOrderingWithKey("grade.cGrade", EOSortOrdering.CompareAscending));
		sortOrderings.addObject(EOSortOrdering.sortOrderingWithKey("typeContratTravail.libelleCourt", EOSortOrdering.CompareAscending));
		displayGroup().setSortOrderings(sortOrderings);
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous supprimer cette population ?";
	}
	// Méthodes privées
	private EOPrimePopulation currentPrimePopulation() {
		return (EOPrimePopulation)displayGroup().selectedObject();
	}
}

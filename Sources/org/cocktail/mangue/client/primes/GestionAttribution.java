//Created on Wed Jun 11 12:24:00 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import java.awt.Cursor;
import java.awt.Window;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.ListSelectionModel;

import org.cocktail.client.components.DialogueSimple;
import org.cocktail.client.components.DialogueSimpleAvecStrings;
import org.cocktail.client.components.DialogueSimpleSansFetch;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.outils_interface.DialogueAvecSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOGoyaveParametres;
import org.cocktail.mangue.modele.goyave.EOPrimeCode;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeParamPerso;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.ParametrePersonnel;
import org.cocktail.mangue.modele.goyave.primes.PrimeResultat;
import org.cocktail.mangue.modele.goyave.primes.PrimeResultatCalcul;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** G&eagrave;re le d&eacute;tail d'une attribution et les actions associ&eacute;es.<BR>
 * Utilis&eacute; pour g&eacute;rer des attributions globales ou individuelles : dans le cas des attributions globales cr&eacute;&eacute;s pr&eacute;c&eacute;demment,
 * l'utilisateur va modifier les dates d'attribution ou saisir les param&grave;tres personnels si la prime est personnalisable.
 * Dans le cas d'une cr&eacute;ation :<BR>
 * pour une attribution "globale", l'utilisateur doit saisir l'individu et les dates d'attribution &agrave; l'int&eacute;rieur de la
 * p&eacute;riode de campagne.<BR>
 * pour une attribution individuelle, l'utilisateur doit saisir la prime et les dates d'attribution &agrave; l'int&eacute;rieur de la
 * p&eacute;riode de campagne.<BR>
 * Dans les deux cas, les r&eacute;sultats sont imm&eacute;diatement calcul&eacute;s pour v&eacute;rifier si l'individu peut b&eacute;n&eacute;ficier
 * de la prime et les attributions correspondantes cr&eacute;es. Puis lors de la validation, ils sont enregistr&eacute;s avec des montants nuls
 * si la prime est personnalisable ou &eacute;valu&eacute;s sinon. On propose ensuite dans le cas des primes personnalisables calcul&eacut;es de
 * saisir les param&egrave;tres personnels. Au moment de la validation des param&egrave;tres personnels, l'attribution sera calcul&eacute;e.
 * Travaille dans le m&circ;me editing context que l'appelant<BR>
 * Gestion des param&grave;tres personnels<BR>
 * Ils sont saisis dans cette interface d'un bloc. Lorsqu'on affiche une attribution personnalisable, on se retrouve dans la situation suivante :<BR>
 * ou tous les param&egrave;tres personnels sont d&eacute;finis ou tous ne le sont pas. La personnalisation et les param&egrave;tres personnels 
 * sont d&eacute;finis sur toute la p&eacute;riode de "campagne" (p&eacute;riode d'attribution de la prime) sauf pour les primes &agrave; versement
 * unique o&ugrave; ils sont d&eacute;finis sur la p&eacute;riode de l'attribution<BR>
 * Lorsque des attributions sont cr&eacute;es ou modifi&eacute;es, on v&eacute;rifie que les dates des attributions sont contig&eacute;es sur la p&eacute;riode
 * Le display group contient toutes les attributions li&eacute;es &agrave; une m&ecirc;me prime pour l'exercice courant sauf pour
 * les primes attribuables plusieurs fois dans l'ann&eacute;e o&ugrave; il contient une seule attribution<BR>
 * @author christine
 *
 */
//Modifications effectuées
// 28/08/09 Gestion de la TAI
// 29/09/09 - correction d'un bug lorsque les paramètres personnels sont saisis et qu'on en modifie un
// 16/11/09 - modification lorsque plusieurs attributions sont créées, elles étaient insérées dans le display group alors que ce
// dernier ne doit contenir que l'attribution courante. Elles sont maintenant insérées dans l'editing context uniquement et transférées
// dans le display group au moment de la validation
// 12/03/2010 - pour signaler que l'attribution a été éditée une fois qu'elle est calculée
// 21/12/2010 - Adaptation Netbeans + correction d'un bug lors de la saisie des paramètres personnels
public class GestionAttribution extends EOInterfaceController {
	public final static String NOTIFICATION_ATTRIBUTIONS_VALIDEES = "NotificationAttributionsValidees";
	public final static String NOTIFICATION_RAFFRAICHIR_ATTRIBUTION = "NotificationRaffraichirAttribution";
	public final static String NOTIFICATION_VALIDER_ATTRIBUTION = "NotificationValiderAttribution";
	public final static String NOTIFICATION_ANNULER_ATTRIBUTION = "NotificationAnnulerAttribution";
	public final static String NOTIFICATION_EDITION_ATTRIBUTION = "NotificationEditionAttribution";
	public final static String NOTIFICATION_CAMPAGNE_MODIFIEE = "NotificationCampagneModifiee";
	private final static String NOTIFICATION_CHANGEMENT_ATTRIBUTION_GLOBALE = "NotificationInspectionAttribPrecGlo";
	private final static String NOTIFICATION_CHANGEMENT_ATTRIBUTION_INDIVIDU = "NotificationInspectionAttribPrecInd";
	public EODisplayGroup displayGroupParametres,displayGroupParametresPerso;
	public EOTable listeParametres,listeParametresPersonnels;
	public EOView vuePersonnalisation,vueBoutonsPourAttribution,vueBoutonsValidation;
	public JButton boutonModificationParametre;
	private int nbParametreCree;
	private NSTimestamp debutCampagne,finCampagne;
	private boolean verifierBudget,creationParametresPersos,doitSaisirParametresPersonnels;
	private boolean datesChanged;				 	// pour recalculer les montants des primes fixes
	private boolean personnalisationChanged;		// pour vérifier si la personnalisation comporte un montant et qu'il est valide
	private boolean parametresPersonnelsChanged;	// pour vérifier si les paramètres personnels saisis sont valides
	private boolean gereAttributionsGlobales;		// Ce composant permet de gérer des attributions globales ou des attributions individuelles
	private boolean actionAutorisee,modificationEnCours,modeCreation,recalculEnCours;
	private String annee;
	private String prefixeNotification;			// pour différencier les notifications entre la gestion globale et la gestion individuelle
	private String nomNotificationAttribPrec;
	private NSMutableArray codesPourParametresPerso; 		// pour permettre la saisie des paramètres personnels
	private EOPrimePersonnalisation personnalisationCourante;
	private InspecteurAttribution controleurPourInfo;
	private InspecteurAttributionsPrecedentes inspecteurAttribPrec;
	private GestionAide controleurAide;
	private NSArray codesCategoriesTAI = null;
	private NSMutableArray autresAttributionsCreees;
	private final static String CODE_FONCTION_POUR_PFI = "FONINPFI";
	private final static String CODE_FONCTION_POUR_TAI = "FONINTAI";
	private final static String	CODE_CATEGORIE_TVX_TAI_IND = "CATINTV";
	private final static String	CODE_CATEGORIE_TVX_TAI = "CATTVX";

	/** Constructeur : charge l'archive et pr&eacute;pare l'interface */
	public GestionAttribution(EOEditingContext editingContext,boolean gereAttributionsGlobales) {
		// le fait de charger l'archive change l'editing context
		// 21/12/2010 - Netbeans + correction d'un bug lors de la saisie des paramètres persos (il faut recréer le dialogue à chaque fois)
		EOArchive.loadArchiveNamed("GestionAttribution", this,"org.cocktail.mangue.client.primes.interfaces",this.disposableRegistry());
		setEditingContext(editingContext);
		((EODistributedDataSource)displayGroup().dataSource()).setEditingContext(editingContext);	// pour travailler dans le bon editing context
		((EODistributedDataSource)displayGroupParametresPerso.dataSource()).setEditingContext(editingContext);	// pour travailler dans le bon editing context
		if (displayGroup() != null) {
			displayGroup().setSelectsFirstObjectAfterFetch(false);
		}
		preparerInterface();
		this.gereAttributionsGlobales = gereAttributionsGlobales;
		if (gereAttributionsGlobales) {
			nomNotificationAttribPrec = NOTIFICATION_CHANGEMENT_ATTRIBUTION_GLOBALE;
			prefixeNotification = GestionPrimes.PREFIXE_POUR_ATTRIBUTION;
		} else {
			nomNotificationAttribPrec = NOTIFICATION_CHANGEMENT_ATTRIBUTION_INDIVIDU;
			prefixeNotification = GestionPrimesIndividu.PREFIXE_POUR_ATTRIBUTION;
		}
	}
	// Accesseurs
	public BigDecimal montantPersonnalise() {
		if (personnalisationCourante != null) {
			return personnalisationCourante.pmpeMontant();
		} else {
			return null;
		}
	}
	public void setMontantPersonnalise(BigDecimal montant) {
		personnalisationChanged = true;
		if (personnalisationCourante == null) {
			// Créer une personnalisation
			personnalisationCourante = new EOPrimePersonnalisation();
			personnalisationCourante.initAvecIndividuPrimeEtDates(currentAttribution().individu(), currentAttribution().prime(), currentAttribution().debutValidite(),currentAttribution().finValidite());
			editingContext().insertObject(personnalisationCourante);
		}
		personnalisationCourante.setPmpeMontant(montant);
		if (currentPrime().estCalculee() == false) {
			modifierMontantAttributionsLieesAvecPersonnalisation(personnalisationCourante);	// Plusieurs attributions peuvent exister (par exemple parce que changement de montant standard en cours d'année
			modifierAffichageInspecteur();
		}
	}
	public String labelMontant() {
		if (currentPrime() == null) {
			return null;
		} else if (currentPrime().estPersonnalisable() && currentPrime().doitSaisirMontantPersonnalise()) {
			return currentPrime().libellePourMontant();
		} else {
			return null;
		}
	}
	public boolean modificationEnCours() {
		return modificationEnCours;
	}
	public void setModificationEnCours(boolean aBool) {
		this.modificationEnCours = aBool;
		afficherBoutons(aBool);
		updaterDisplayGroups();
	}
	public void setAnnee(String annee) {
		this.annee = annee;
		preparerPeriodeCampagne(null,false);
		preparerPourAttributionsEtSelection(null, null,false);
	}
	public void setEdited(boolean aBool) {
		if (aBool != isEdited()) {
			super.setEdited(aBool);
			if (!aBool) {
				NSNotificationCenter.defaultCenter().postNotification(prefixeNotification + NOTIFICATION_EDITION_ATTRIBUTION,new Boolean(aBool));
			}
		}
	}
	// Méthode de délégation du display group
	public void displayGroupDidSetValueForObject(EODisplayGroup group, Object value, Object eo, String key) {
		if (group == displayGroup()) {
			setEdited(true);
			if (key.equals("debutValiditeFormatee") || key.equals("finValiditeFormatee")) {	
				datesChanged = true;
				if (key.equals("debutValiditeFormatee")) {	
					if (currentAttribution().debutValidite() != null) {
						if (DateCtrl.isBefore(currentAttribution().debutValidite(),debutCampagne) || DateCtrl.isAfter(currentAttribution().debutValidite(), finCampagne)) {
							currentAttribution().setDebutValidite(debutCampagne);
						}
						if (currentAttribution().finValidite() != null && DateCtrl.isAfter(currentAttribution().debutValidite(), currentAttribution().finValidite())) {
							currentAttribution().setDebutValidite(finCampagne);
						}
					}
				} else if (key.equals("finValiditeFormatee")) {
					if (currentAttribution().finValidite() != null) {
						if (DateCtrl.isBefore(currentAttribution().finValidite(), debutCampagne) ||  DateCtrl.isAfter(currentAttribution().finValidite(), finCampagne)) {
							currentAttribution().setFinValidite(finCampagne);
						}
						if (currentAttribution().finValidite() != null && DateCtrl.isAfter(currentAttribution().debutValidite(), currentAttribution().finValidite())) {
							currentAttribution().setDebutValidite(debutCampagne);
						}
					}
				}
				updaterDisplayGroups();
				// Probablement notifications pour synchroniser le parent
			}
		}
	}
	// Actions
	/** Action du bouton valider, notifie le parent en fin de traitement */
	public void valider() {
		LogManager.logDetail("GestionAttribution - valider");
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		valider(true);
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}
	/** action du bouton annuler, notifie le parent en fin de traitement */
	public void annuler() {
		LogManager.logDetail("GestionAttribution - Annulation");
		editingContext().revert();
		setEdited(false);
		traitementsApresRevert();
		datesChanged = false;
		personnalisationChanged = false;
		nbParametreCree = 0;
		NSNotificationCenter.defaultCenter().postNotification(prefixeNotification + NOTIFICATION_ANNULER_ATTRIBUTION,null);
	}
	/** Affiche la liste des individus en vue de cr&eacute;er une attribution. Cette m&eacute;thode est &agrave; appeler
	 * dans le cas de la gestion des attributions globales */
	public void afficherIndividus() {
		if (gereAttributionsGlobales) {
			LogManager.logDetail("GestionAttribution - afficherIndividus");
			DialogueSimpleSansFetch controleurSelectionIndividu = new DialogueSimpleSansFetch(EOIndividu.ENTITY_NAME,"AttributionPrime_SelectionIndividu","nomUsuel","Sélection d'un individu",false,true,false,false,false);
			controleurSelectionIndividu.init();
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("temValide = 'O' AND personnels.noDossierPers <> nil",null);
			controleurSelectionIndividu.setQualifier(qualifier);
			// s'enregistrer pour recevoir les notifications
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getIndividu",new Class[] {NSNotification.class}),"AttributionPrime_SelectionIndividu",null);

			controleurSelectionIndividu.afficherFenetre();	
		}
	}
	/** Affiche la liste des primes en vue de cr&eacute;er une attribution. Cette m&eacute;thode est &agrave; appeler
	 * dans le cas de la gestion des attributions individuelles */
	public void afficherPrimes() {
		if (!gereAttributionsGlobales) {
			LogManager.logDetail("GestionAttribution - afficherPrimes");
			DialogueSimple controleurSelectionPrime = new DialogueSimple("Prime","SelectionPrime","Sélection d'une prime",false,false,true);
			controleurSelectionPrime.init();
			// s'enregistrer pour recevoir les notifications
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getPrime",new Class[] {NSNotification.class}),"SelectionPrime",null);
			controleurSelectionPrime.setQualifier(EOPrime.qualifierPourValidite(true, dateDebutCourante(), dateFinCourante()));
			controleurSelectionPrime.afficherFenetre();	
		}
	}
	/** Change le statut d'une attribution provisoire en attribution valide et enregistre le changement. On v&eacute;rifie de nouveau
	 * qu'elle rentre dans le budget et on l'inclut dans le budget. Si il y a plusieurs attributions li&eacute;es suite &grave; un calcul,
	 * on les valide aussi */
	public void validerAttribution() {
		LogManager.logDetail("GestionAttribution - validerAttribution");
		boolean devientValide = true;
		// Le display group contient toutes les attributions liées, on vérifie si on peut toutes les valider
		java.util.Enumeration e = displayGroup().allObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
			if (attribution.estSuspendue() || attribution.estProvisoire()) {
				if (!rentreDansBudget(attribution,true)) {
					devientValide = false;
					break;	// Ce n'est pas la peine de continuer, il va falloir tout invalider
				}
			}
		}
		// On les change toutes en bloc
		e = displayGroup().allObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
			if (devientValide) {
				attribution.setEstValide(true);
			} else {
				attribution.setEstSuspendue();
			}
		}
		if (devientValide) {
			if (personnalisationCourante != null) {
				// Indiquer que la personnalisation est utilisée
				personnalisationCourante.setEstUtilisee();
				personnalisationChanged = true;
			}
		} else {
			// Il y a un dépassement de budget, le signaler
			EODialogs.runInformationDialog("Attention", "Il y a dépassement de budget, l'attribution est suspendue");
		}
		valider(false);
	}
	/** Recalcule le montant d'une prime pour un individu */
	public void recalculer() {
		LogManager.logDetail("GestionAttribution - recalculer");
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		recalculEnCours = true;
		NSArray attributionsACalculer = displayGroup().allObjects();
		NSMutableArray attributionsRecalculees = new NSMutableArray();
		java.util.Enumeration e = attributionsACalculer.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
			NSArray nouvellesAttributions = recalculerAttribution(attribution);
			// Ajouter les nouvelles attributions
			if (nouvellesAttributions != null && nouvellesAttributions.count() > 0) {
				attributionsRecalculees.addObjectsFromArray(nouvellesAttributions);
			}
		}
		NSMutableArray attributionsPourDG = new NSMutableArray(displayGroup().displayedObjects());
		// Supprimer toutes les anciennes attributions de cet individu
		attributionsPourDG.removeObjectsInArray(attributionsACalculer);
		attributionsPourDG.addObjectsFromArray(attributionsRecalculees);
		displayGroup().setSelectedObject(null);
		displayGroup().setObjectArray(attributionsPourDG);
		
		// Vérifier si les montants sont disponibles dans le budget
		if (attributionsRecalculees.count() > 0) {
			displayGroup().selectObject(attributionsRecalculees.objectAtIndex(0));
			boolean shouldSave = depassementBudgetPourAttributions(attributionsRecalculees);
			// Changer le statut des attributions en statut provisoire si elles ne sont pas suspendues
			boolean attributionsChanged = false;
			e = attributionsRecalculees.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
				if (!shouldSave || attribution.estSuspendue() == false) {
					attribution.setEstProvisoire();
					attributionsChanged = true;
				}
			}
			if (shouldSave || attributionsChanged) {
				valider(false);	// Enregistrer les modifications
			}
		} else {
			EODialogs.runInformationDialog("Attention", "Pas d'attribution recalculée pour cet individu et l'attribution courante");
		}
		updaterDisplayGroups();
		component().setCursor(Cursor.getDefaultCursor());
		recalculEnCours = false;
	}
	/** Modifie la valeur d'un param&egrave;tre personnel */
	public void modifierParametrePersonnel() {
		LogManager.logDetail("GestionAttribution - modifierParametrePersonnel");
		nbParametreCree = 0;
		if (displayGroupParametresPerso.selectedObject() != null) {
			// Il s'agit de la modification d'un paramètre existant
			creationParametresPersos = false;
			EOPrimeParamPerso paramPerso = (EOPrimeParamPerso)displayGroupParametresPerso.selectedObject();
			afficherSaisieParametrePerso(paramPerso.code());
		} else {
			creationParametresPersos = true;
			// Commencer par saisir la personnalisation si elle n'existe pas
			if (personnalisationCourante == null) {
				personnalisationCourante = new EOPrimePersonnalisation();
				// La période de la personnalisation est la période de la prime
				personnalisationCourante.initAvecIndividuPrimeEtDates(currentAttribution().individu(), currentAttribution().prime(), currentAttribution().debutValidite(),currentAttribution().finValidite());
				editingContext().insertObject(personnalisationCourante);
				// Sauvegarder tout de suite la personnalisation pour ne pas avoir de souci par la suite lors de la sauvegarde des 
				// paramètres personnels
				editingContext().saveChanges();
			}
			afficherSaisieParametrePerso((EOPrimeCode)codesPourParametresPerso.objectAtIndex(0));
		}
	}
	public void afficherAidePourDates() {
		EODialogs.runInformationDialog("Aide", "On ne peut modifier la date de début et de fin d'une attribution que si il s'agit d'une création ou d'une prime à affectations multiples dans l'année.\nDans les autres cas, supprimez l'attribution et recréez-la");
	}
	public void afficherModeCalcul() {
		afficherInspecteur();
	}
	public void ouvrirInspecteurAttribPrec() {
		LogManager.logDetail(this.getClass().getName() + " - ouvrirInspecteurAttribPrec");
		if (inspecteurAttribPrec == null) {
			inspecteurAttribPrec = new InspecteurAttributionsPrecedentes(editingContext(), nomNotificationAttribPrec);
			inspecteurAttribPrec.init();
		}
		inspecteurAttribPrec.afficher();	
		NSNotificationCenter.defaultCenter().postNotification(nomNotificationAttribPrec,currentAttribution());
	}
	// Autres
	public void preparerPourAttributionsEtSelection(NSArray attributions,EOPrimeAttribution attributionASelectionner,boolean autoriserAction) {
		if (attributions == null || attributions.count() == 0) {
			LogManager.logDetail("GestionAttribution - preparerPourAttributionsEtSelection : pas de selection");
			displayGroup().setObjectArray(null);
		} else {
			LogManager.logDetail("GestionAttribution - preparerPourAttributionsEtSelection, nb Attributions : " + attributions.count() + ", selection : " + attributionASelectionner);
			displayGroup().setObjectArray(attributions);
			displayGroup().selectObject(attributionASelectionner);
			if (attributionASelectionner != null && attributionASelectionner.prime() != null) {
				preparerPeriodeCampagne(attributionASelectionner.prime(),true);
				if (attributionASelectionner.prime().doitAvoirParametresPersonnels()) {
					doitSaisirParametresPersonnels = true;
				}
			}
		}
		this.actionAutorisee = autoriserAction;
		preparerParametrages();
		modifierTitreInspecteur();
		modifierAffichageInspecteur();
		datesChanged = false;
		personnalisationChanged = false;
		// Signaler à l'inspecteur d'attributions précédentes qu'il y a eu une modification
		NSNotificationCenter.defaultCenter().postNotification(nomNotificationAttribPrec,currentAttribution());
		updaterDisplayGroups();
	}
	public void terminer() {
		if (controleurPourInfo != null) {
			controleurPourInfo.fermerFenetre();
		}
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}
	public void afficherAide() {
		if (controleurAide == null) {
			controleurAide = new GestionAide("Aide Utilisateur de la prime","",false);
			Window window = EOApplication.sharedApplication().windowObserver().activeWindow();
			int Y = window.getHeight() + window.getY() + 5;
			controleurAide.initAvecPosition(window.getX(),Y);
			controleurAide.afficherFenetre();
		} else {
			controleurAide.changerEtat();
		}
		controleurAide.setTexte(currentPrime().primCommentaire());
	}
	// Notifications
	public void getIndividu(NSNotification aNotif) {
		if (modificationEnCours() && gereAttributionsGlobales) {
			LogManager.logDetail("GestionAttribution - Notification getIndividu");
			if (aNotif.object() != null) {
				EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
				// Vérifier si l'individu n'a pas déjà une attribution et si il peut bénéficier de la prime
				if (individu != null) {
					// On recherche directement les attributions car en cas de création, il n'y a que l'objet créé dans le display group
					NSArray attributions = EOPrimeAttribution.rechercherAttributionsEnCoursPourPrimeIndividuEtPeriode(editingContext(), individu, currentPrime(), dateDebutCourante(),dateFinCourante());
					if (attributions.count() == 0) {	// Dans la gestion des primes globales, on ne peut ajouter que des primes qui sont attribuées une seule fois par an
						calculerAttributionsPourPrimeIndividuEtPeriode(currentPrime(),individu,dateDebutCourante(),dateFinCourante());
					} else {
						EODialogs.runErrorDialog("Erreur","Cet individu a déjà un montant qui lui est attribué pour cette prime");
					}
				}
			}
		}
	}
	public void getPrime(NSNotification aNotif) {
		if (modificationEnCours() && !gereAttributionsGlobales) {
			LogManager.logDetail("GestionAttribution - Notification getPrime");
			if (aNotif.object() != null) {
				EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
				// Vérifier si l'individu n'a pas déjà une attribution et si il peut bénéficier de la prime
				if (prime != null) {
					// On recherche directement les attributions car en cas de création, il n'y a que l'objet créé dans le display group
					preparerPeriodeCampagne(prime,true);
					NSArray attributions = EOPrimeAttribution.rechercherAttributionsEnCoursPourPrimeIndividuEtPeriode(editingContext(), currentIndividu(), prime, dateDebutCourante(), dateFinCourante());
					if (attributions.count() == 0 || prime.estAttributionUniqueDansAnnee() == false) {
						if (prime.estAttributionUniqueDansAnnee() == false && datesAttributionValides(attributions) == false) {
							return;
						}
						// Si les dates ont déjà été mises par l'utilisateur, ne pas modifier les dates lorsqu'il s'agit
						// de primes qui peuvent avoir plusieurs valeurs
						if (currentAttribution().debutValidite() == null || prime.estAttributionUniqueDansAnnee()) {
							// On sait maintenant exactement les dates à laquelle la prime doit être attribuée
							currentAttribution().setDebutValidite(debutCampagne);
						}
						if (currentAttribution().finValidite() == null || prime.estAttributionUniqueDansAnnee()) {
							currentAttribution().setFinValidite(finCampagne);
						}
						calculerAttributionsPourPrimeIndividuEtPeriode(prime,currentIndividu(),currentAttribution().debutValidite(),currentAttribution().finValidite());
						preparerParametrages();
						updaterDisplayGroups();
					} else {
						EODialogs.runErrorDialog("Erreur","Cet individu a déjà un montant qui lui est attribué pour cette prime");
					}
				}
				// Signaler à l'inspecteur d'attributions précédentes qu'il y a eu une modification
				NSNotificationCenter.defaultCenter().postNotification(nomNotificationAttribPrec,currentAttribution());
			}
		}
	}
	public void getValeurParametrePersonnel(NSNotification aNotif) {
		if (modificationEnCours() && aNotif.object() != null) {
			LogManager.logDetail("GestionAttribution - Notification getValeurParametrePersonnel");
			// Il s'agit de la création de paramètres personnels, on va recommencer la saisie tant qu'il le faut
			String texte = (String)aNotif.object();
			if (texte.length() > 50) {
				texte = texte.substring(0,50);
			}
			preparerParametrePersonnel(texte);
		}
	}
	public void getFonction(NSNotification aNotif) {
		//	28/08/09 - modifié pour que la fonction puisse aussi être saisie en mode global
		if (modificationEnCours() && aNotif.object() != null) {
			LogManager.logDetail("GestionAttribution - Notification getFonction");
			String fonction = (String)aNotif.object();
			preparerParametrePersonnel(fonction);
		}
	}
	public void getCategoriePourTAI(NSNotification aNotif) {
		if (modificationEnCours() && aNotif.object() != null) {
			LogManager.logDetail("GestionAttribution - Notification getCategoriePourTAI");
			String libelleCategorie = (String)aNotif.object();
			String codeValeur = null;
			// Récupérer le code associé à cette catégorie
			java.util.Enumeration e = codesCategoriesTAI.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeCode code = (EOPrimeCode)e.nextElement();
				if (code.libelle().equals(libelleCategorie)) {
					codeValeur = code.code();
					break;
				}
			}
			preparerParametrePersonnel(codeValeur);
		}

	}
	public void annulationSaisieParametrePersonnel(NSNotification aNotif) {
		if (modificationEnCours()) {
			LogManager.logDetail("GestionAttribution - Notification annulationSaisieParametrePersonnel");
			editingContext().revert();
			nbParametreCree = 0;
			if (creationParametresPersos) {
				displayGroupParametresPerso.setObjectArray(null);
			}
			creationParametresPersos = false;
			parametresPersonnelsChanged = false;
			displayGroupParametresPerso.updateDisplayedObjects();
		}
	}
	// Méthodes du controller DG
	/** On ne peut modifier le montant annuel que pour des primes personnalisables dont on doit saisir le montant 
	 *  et lorsqu'on est en mode modification
	 */
	public boolean peutModifierMontant() {
		return modificationEnCours() && vuePersonnalisation.isVisible() && currentPrime() != null && currentPrime().doitSaisirMontantPersonnalise() && 
		(personnalisationCourante == null || (personnalisationCourante != null && personnalisationCourante.estValide()));
	}
	/** on ne peut sélectionner un individu qu'en mode cr&eacute;ation et si il s'agit de la cr&eacute;ation d'une attribution
	 * pour une prime */
	public boolean peutSelectionnerIndividu() {
		return modificationEnCours() && modeCreation && gereAttributionsGlobales;
	}
	/** on ne peut sélectionner une prime qu'en mode cr&eacute;ation et si il s'agit de la cr&eacute;ation d'une attribution
	 * pour un individu */
	public boolean peutSelectionnerPrime() {
		return modificationEnCours() && modeCreation && !gereAttributionsGlobales;
	}
	/** Une action n'est possible que si l'utilisateur a les droits, l'attribution est s&eacute;lectionn&eacute;e et on n'est pas
	 * en mode modification
	 */
	public boolean boutonActionPossible() {
		return actionAutorisee && !modificationEnCours() && vueBoutonsPourAttribution.isVisible() && currentAttribution() != null;
	}
	/** On ne peut modifier la date de d&eacute;but d'une attribution que si l'individu et la prime sont choisis 
	 * et qu'on est en mode cr&eacute;ation ou qu'il s'agit d'une prime &agrave; affectations multiples dans l'ann&eacute;e */
	public boolean peutModifierDateDebut() {
		return modificationEnCours() && currentAttribution().individu() != null && ((modeCreation && !gereAttributionsGlobales) || (currentAttribution().prime() != null && currentAttribution().prime().estAttributionUniqueDansAnnee() == false));
	}
	/** On ne peut modifier la date de fin d'une attribution que si l'individu et la prime sont choisis
	 * et qu'on est en mode cr&eacute;ation ou qu'il s'agit d'une prime &agrave; affectations multiples dans l'ann&eacute;e */
	public boolean peutModifierDateFin() {
		return peutModifierDateDebut();
	}
	/** on ne peut recalculer que les attributions qui sont suspendues ou en mode &agrave; calculer ou provisoires. Pour les primes
	 * personnalisables il faut en plus que la personnalisation soit d&eacute;finie.*/
	public boolean peutRecalculer() {
		if (boutonActionPossible() && currentAttribution() != null) {
			if (currentAttribution().estSuspendue()) {
				return true;
			} else if (currentAttribution().estProvisoire() || currentAttribution().estCalculee()) {
				if (currentPrime() != null && currentPrime().estPersonnalisable()) {
					return (personnalisationCourante != null && (personnalisationCourante.pmpeMontant() != null || displayGroupParametresPerso.displayedObjects().count() > 0));
				} else {
					return true;
				}	
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}
	/** on ne peut valider une attribution en mode modification que si les dates de validit&eacute; sont fournies, 
	 * que l'individu est s&eacute;lectionn&eacute;, la prime s&eacute;lectionn&eacute;e et si la prime est personnalisable que si les donn&eacute;es de personnalisation
	 * sont saisie
	 */
	public boolean peutValider() {
		if (isEdited() && currentAttribution().individu() != null && currentAttribution().prime() != null) {
			if (currentAttribution().debutValidite() == null || currentAttribution().finValidite() == null) {
				return false;
			}
			if (currentPrime().estPersonnalisable() == false) {
				return true;
			} else if (doitSaisirParametresPersonnels) {
				return (currentPrime().doitAvoirParametresPersonnels() && displayGroupParametresPerso.displayedObjects().count() > 0) ||
				(currentPrime().doitSaisirMontantPersonnalise() && currentAttribution().pattMontant() != null);
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	/** on ne peut valider une attribution que si elle est en mode provisoire (i.e elle a d&eacute;j&agrave; &eacute;t&eacute; 
	 * &eacute;valu&eacute;e) */
	public boolean peutValiderAttribution() {
		return boutonActionPossible() &&  currentAttribution().estProvisoire();
	}
	/** On ne peut modifier les param&egrave;tres personnels que si il y a des param&egrave;tres personnels &agrave; d&eacute;finir ou &grave; modifier
	 * et qu'on est en mode modification */
	public boolean peutModifierParametresPerso() {
		return modificationEnCours() && currentAttribution() != null && currentAttribution().prime() != null &&
		currentAttribution().prime().estPersonnalisable() && doitSaisirParametresPersonnels &&
		(displayGroupParametresPerso.displayedObjects().count() != codesPourParametresPerso.count() || displayGroupParametresPerso.selectedObject() != null);
	}
	public boolean peutAfficherAide() {
		return currentPrime() != null && currentPrime().primCommentaire() != null;
	}
	// Autres
	public void afficherBoutons(boolean estValidation) {
		vueBoutonsValidation.setVisible(estValidation);
		if (estValidation) {
			vueBoutonsPourAttribution.setVisible(false);
		} else {
			vueBoutonsPourAttribution.setVisible(currentAttribution() != null);
		}
	}
	public void preparerCampagnePourPrime(EOPrime prime) {
		preparerPeriodeCampagne(prime,false);
		// Signaler à l'inspecteur d'attributions précédentes qu'il y a eu une modification
		NSNotificationCenter.defaultCenter().postNotification(nomNotificationAttribPrec,currentAttribution());
	}
	// Méthodes protégées
	protected void preparerInterface() {
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
		GraphicUtilities.changerTaillePolice(listeParametres,11);
		listeParametres.table().setEnabled(false);	// les paramètres sont là juste pour information
		GraphicUtilities.changerTaillePolice(listeParametresPersonnels,11);
		GraphicUtilities.rendreNonEditable(listeParametresPersonnels);
		listeParametresPersonnels.table().getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// Ranger les attributions par ordre de date croissant
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareAscending)));
		displayGroupParametres.setSelectsFirstObjectAfterFetch(false);
		displayGroupParametres.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("code.pcodCode", EOSortOrdering.CompareAscending)));
		preparerParametrages();
		String valeur = EOGoyaveParametres.valeurParametrePourCle(editingContext(), "VERIFIER_BUDGET");
		verifierBudget = (valeur != null && valeur.equals(CocktailConstantes.VRAI));
	}
	protected void traitementsPourCreation(EOPrime prime,EOIndividu individu) {
		if (gereAttributionsGlobales) {
			currentAttribution().initAvecPrimeEtDates(prime,debutCampagne,finCampagne);
		} else {
			// Fait par le parent
			NSTimestamp debut = DateCtrl.stringToDate("01/01/" + annee), fin = DateCtrl.stringToDate("31/12/" + annee);
			currentAttribution().initAvecIndividuEtDates(individu,debut,fin);
		}
		currentAttribution().setPattExercice(new Integer(annee));
		modeCreation = true;
		actionAutorisee = true;
		nbParametreCree = 0;
	}
	// Méthodes privées
	private EOPrimeAttribution currentAttribution() {
		return (EOPrimeAttribution)displayGroup().selectedObject();
	}
	private EOPrime currentPrime() {
		if (currentAttribution() == null) {
			return null;
		} else {
			return currentAttribution().prime();
		}
	}
	private EOIndividu currentIndividu() {
		if (currentAttribution() == null) {
			return null;
		} else {
			return currentAttribution().individu();
		}
	}
	private void preparerParametrages() {
		if (currentAttribution() == null || currentPrime() == null) {
			vueBoutonsPourAttribution.setVisible(false);
			displayGroupParametresPerso.setObjectArray(null);
			displayGroupParametresPerso.updateDisplayedObjects();
			listeParametresPersonnels.setVisible(false);
			vuePersonnalisation.setVisible(false);
			boutonModificationParametre.setVisible(false);
			personnalisationCourante = null;
			displayGroupParametres.setObjectArray(null);
			displayGroupParametres.updateDisplayedObjects();
			listeParametres.setVisible(false);
		} else {
			vueBoutonsPourAttribution.setVisible(true);
			if (currentAttribution().prime() != null) {
				if (currentAttribution().prime().estPersonnalisable()) {
					// Rechercher la personnalisation pour l'individu et la période valide ou utilisée
					personnalisationCourante = EOPrimePersonnalisation.rechercherPersonnalisationEnCoursPourIndividuPrimeEtPeriode(editingContext(),currentAttribution().individu(),currentAttribution().prime(),currentAttribution().debutValidite(),currentAttribution().finValidite());
					// Il n'y en a pas forcément une car on peut être dans le cas où il s'agit d'un premier calcul de l'attribution
					vuePersonnalisation.setVisible(currentAttribution().prime().doitSaisirMontantPersonnalise()); // Il y aura juste un montant à saisir
					listeParametresPersonnels.setVisible(currentAttribution().prime().doitAvoirParametresPersonnels());
					if (currentAttribution().prime().doitAvoirParametresPersonnels()) {
						displayGroupParametresPerso.setObjectArray(EOPrimeParamPerso.rechercherParametresPersonnelsValidesPourPersonnalisation(editingContext(), personnalisationCourante));
						displayGroupParametresPerso.updateDisplayedObjects();
						preparerParametresStandard(currentAttribution().prime(),currentAttribution().debutValidite(),currentAttribution().finValidite(),true);
						boutonModificationParametre.setVisible(currentAttribution().prime().doitAvoirParametresPersonnels());
					}	
				} else {
					preparerParametresStandard(currentAttribution().prime(),currentAttribution().debutValidite(),currentAttribution().finValidite(),false);
				}
			}
		}
	}
	private void preparerParametresStandard(EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean preparerParametresPerso) {
		NSMutableArray parametres = new NSMutableArray();
		codesPourParametresPerso = new NSMutableArray();
		java.util.Enumeration e = prime.codes().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeCode code = (EOPrimeCode)e.nextElement();
			NSArray params = EOPrimeParam.rechercherParametresValidesPourCodeEtPeriode(editingContext(), code, debutPeriode, finPeriode);
			if (params != null && params.count() > 0) {
				// Ne retenir que les paramètres qui correspondent à des valeurs
				java.util.Enumeration e1 = params.objectEnumerator();
				while (e1.hasMoreElements()) {
					EOPrimeParam param = (EOPrimeParam)e1.nextElement();
					String valeur = param.valeurParametre();
					if (valeur != null) {
						parametres.addObject(param);
					}
				}
			} else if (preparerParametresPerso) {
				codesPourParametresPerso.addObject(code);
			}
		}
		displayGroupParametres.setObjectArray(parametres);
		displayGroupParametres.updateDisplayedObjects();
		listeParametres.setVisible(displayGroupParametres.displayedObjects().count() > 0);
	}
	/* Vérifier les paramètres personnels ou le montant saisi. Vérifie que les dates ne sont pas à cheval avec d'autres attributions */
	private boolean traitementsAvantValidation() {
		if (datesAttributionValides(null) == false) {
			return false;
		}
		if (datesChanged || modeCreation) {
			if (personnalisationCourante != null && currentPrime().estAttributionUniqueDansAnnee() == false) {
				// Forcer les dates de personnalisation à celle de l'attribution
				personnalisationCourante.setDebutValidite(currentAttribution().debutValidite());
				personnalisationCourante.setFinValidite(currentAttribution().finValidite());
				personnalisationChanged = true;
			}
			if (datesChanged && modeCreation) {
				// Comme rien n'a encore été enregistré, on recommence le calcul de l'attribution
				calculerAttributionsPourPrimeIndividuEtPeriode(currentAttribution().prime(), currentAttribution().individu(), currentAttribution().debutValidite(), currentAttribution().finValidite());
				datesChanged = false;
			}
			if (!modeCreation) {
				EODialogs.runInformationDialog("Attention", "Les dates ont changé, l'attribution va être recalculée");
				// Changer le statut de l'attribution
				currentAttribution().setEstCalculee();
			} else if (autresAttributionsCreees != null && autresAttributionsCreees.count() > 0) { 
				// Si des nouvelles attributions ont été créées, les ajouter lors de la création
				java.util.Enumeration e = autresAttributionsCreees.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
					editingContext().insertObject(attribution);
				}
				autresAttributionsCreees = null;
			}
		}
		
		if (currentPrime().estPersonnalisable() && (parametresPersonnelsChanged || personnalisationChanged)) {
			// Vérifier la validité de la personnalisation ou des paramètres personnels
			if (parametresPersonnelsChanged && currentPrime().doitAvoirParametresPersonnels() && displayGroupParametresPerso.displayedObjects().count() > 0) {
				// Vérifier les paramètres personnels
				NSMutableArray paramsPersos = new NSMutableArray();
				java.util.Enumeration e = displayGroupParametresPerso.displayedObjects().objectEnumerator();
				try {
					while (e.hasMoreElements()) {
						EOPrimeParamPerso parametre = (EOPrimeParamPerso)e.nextElement();
						// On crée des ParametrePersonnel plutôt que des param perso au cas où on est en mode création
						paramsPersos.addObject(new ParametrePersonnel(parametre.code().pcodCode(),parametre.pppeValeur()));
					}
					Class[] classeParametres = new Class[] {EOGlobalID.class,EOGlobalID.class,NSArray.class,NSTimestamp.class,NSTimestamp.class};
					Object[] parametres = new Object[] {editingContext().globalIDForObject(currentAttribution().individu()),editingContext().globalIDForObject(currentPrime()),paramsPersos,currentAttribution().debutValidite(),currentAttribution().finValidite()};
					NSTimestamp debut = new NSTimestamp();
					LogManager.logDetail("GestionAttribution - Lancement de la validation des parametres personnels sur le serveur");
					EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
					// Ne pas pusher l'editing context, on peut être en mode création
					String message = (String)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestValiderParametresPersosPourIndividuPrimeEtPeriode",classeParametres,parametres,false);
					NSTimestamp fin = new NSTimestamp();
					long duree = (fin.getTime() - debut.getTime()) / 1000;
					LogManager.logDetail("GestionAttribution - Fin du traitement sur le serveur, duree " + duree + " secondes");
					if (message != null) {
						LogManager.logDetail("Vérification des paramètres personnels : " + message);
						EODialogs.runErrorDialog("ERREUR", message);
					}
					return message == null;
				} catch (Exception exc) {
					EODialogs.runErrorDialog("ERREUR", exc.getMessage());
					return false;
				}
			} else if (personnalisationCourante != null && currentPrime().primNomClasse() != null && personnalisationChanged) {
				String resultat = validationMontant();
				if (resultat != null) {
					LogManager.logDetail("Vérification du montant personnalisé : " + resultat);
					EODialogs.runErrorDialog("ERREUR", resultat);
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
	/** Pour recalculer le montant de l'attribution si :
	 * <UL><LI>les dates sont modifi&eacute;es</LI><LI>ou apr&egrave;s saisie
	 * de param&grave;tres personnels</LI><LI> ou  apr&egrave;es modification du montant d'une personnalisation pour une prime calcul&eacute;e</LI></UL>
	 * Pour notifier que la personnalisation a chang&eacute; si une attribution avec personnalisation a &eacute;t&eacute; valid&eacute;e */
	private void traitementsApresValidation() {
		if (!personnalisationChanged || personnalisationCourante == null || !personnalisationCourante.estUtilisee()) {
			if (!recalculEnCours && (datesChanged || doitSaisirParametresPersonnels || (personnalisationChanged && currentPrime().estCalculee()))) {
				boolean shouldSave = false;
				String dateDebut = currentAttribution().debutValiditeFormatee(), dateFin = currentAttribution().finValiditeFormatee();
				recalculer();
				// Dans le cas où il s'agit d'une attribution unique et que les dates ont été changées, on remet les dates saisies
				// par l'utilisateur
				if (datesChanged && displayGroup().allObjects().count() == 1) {
					if (currentAttribution().debutValiditeFormatee().equals(dateDebut) == false) {
						currentAttribution().setDebutValiditeFormatee(dateDebut);
						shouldSave = true;
					}
					if (currentAttribution().finValiditeFormatee().equals(dateFin) == false) {
						currentAttribution().setDebutValiditeFormatee(dateFin);
						shouldSave = true;
					}
				}
				if (shouldSave) {
					valider();
				}		
			}
		}
		if (personnalisationChanged && personnalisationCourante.estUtilisee()) {
			NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_ATTRIBUTIONS_VALIDEES,editingContext().globalIDForObject(currentPrime()));
		}
		remettreEtatSelection();
	}
	private void traitementsApresRevert() {
		datesChanged = false;
		personnalisationChanged = false;
		doitSaisirParametresPersonnels = false;
		creationParametresPersos = false;
		parametresPersonnelsChanged = false;
		preparerParametrages();
		remettreEtatSelection();
	}
	private void remettreEtatSelection() {
		afficherBoutons(false);
		setEdited(false);
		setModificationEnCours(false);
		modeCreation = false;
		updaterDisplayGroups();
	}
	private NSTimestamp dateDebutCourante() {
		if (currentAttribution() == null || currentAttribution().debutValidite() == null || (currentPrime() != null && currentPrime().estAttributionUniqueDansAnnee())) {
			return debutCampagne;
		} else {
			return currentAttribution().debutValidite();
		}

	}
	private NSTimestamp dateFinCourante() {
		if (currentAttribution() == null || currentAttribution().finValidite() == null || (currentPrime() != null && currentPrime().estAttributionUniqueDansAnnee())) {
			return finCampagne;
		} else {
			return currentAttribution().finValidite();
		}
	}
	private void afficherSaisieParametrePerso(EOPrimeCode code) {
		// On est obligé de saisir à part la fonction pour la Prime de Fonction Informatique ou pour l'indemnité horaire
		if (code.pcodCode().equals(CODE_FONCTION_POUR_PFI) || code.pcodCode().equals(CODE_FONCTION_POUR_TAI)) {
			String fonction = "PRIME INFORMATIQUE";
			if (code.pcodCode().equals(CODE_FONCTION_POUR_TAI)) {
				fonction = "PRIME DE TRAVAUX INFORMATIQUES";
			}
			NSArray fonctions = EOAssociation.rechercherFils(editingContext(), fonction);
			DialogueSimpleAvecStrings controleurSelectionFonction = new DialogueSimpleAvecStrings("SelectionAssociation","Sélection d'une fonction",false,true,(NSArray)fonctions.valueForKey(EOAssociation.LIBELLE_LONG_KEY));
			controleurSelectionFonction.init();
			// s'enregistrer pour recevoir les notifications
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getFonction",new Class[] {NSNotification.class}),"SelectionAssociation",null);
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("annulationSaisieParametrePersonnel", new Class[] {NSNotification.class}), DialogueSimple.CANCEL_NOTIFICATION, null);
			controleurSelectionFonction.afficherFenetre();	
		} else if (code.pcodCode().equals(CODE_CATEGORIE_TVX_TAI_IND)) {
			if (codesCategoriesTAI == null) {
				codesCategoriesTAI = EOPrimeCode.rechercherCodesAvecCode(editingContext(), CODE_CATEGORIE_TVX_TAI);
				codesCategoriesTAI = EOSortOrdering.sortedArrayUsingKeyOrderArray(codesCategoriesTAI, new NSArray(EOSortOrdering.sortOrderingWithKey("pcodCode", EOSortOrdering.CompareAscending)));
			}
			DialogueSimpleAvecStrings controleurSelectionFonction = new DialogueSimpleAvecStrings("SelectionParametreCategorie","Catégorie de travaux informatiques",false,true,(NSArray)codesCategoriesTAI.valueForKey("pcodLibelle"));
			controleurSelectionFonction.init();
			// s'enregistrer pour recevoir les notifications
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getCategoriePourTAI",new Class[] {NSNotification.class}),"SelectionParametreCategorie",null);
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("annulationSaisieParametrePersonnel", new Class[] {NSNotification.class}), DialogueSimple.CANCEL_NOTIFICATION, null);
			controleurSelectionFonction.afficherFenetre();	
		} else {
			// 21/12/2010 - afin que la fenêtre s'affiche, il faut recréer le dialogue à chaque fois
			DialogueAvecSaisie controleurParametrePerso = new DialogueAvecSaisie("Modification Paramètre Personnel",code.pcodLibelle());
			controleurParametrePerso.init();
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("getValeurParametrePersonnel", new Class[] {NSNotification.class}), DialogueAvecSaisie.NOTIFICATION_SAISIE, null);
			NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("annulationSaisieParametrePersonnel", new Class[] {NSNotification.class}), DialogueAvecSaisie.NOTIFICATION_ANNULATION, null);
			controleurParametrePerso.afficherFenetre();

		}
	}
	private void calculerAttributionsPourPrimeIndividuEtPeriode(EOPrime prime, EOIndividu individu,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		try {
			// Déterminer le montant de la prime si c'est possible ie en vérifiant que l'individu est éligible 
			// en demandant le calcul sur le serveur, on ne push pas l'editing context
			Class[] classeParametres = new Class[] {EOGlobalID.class,EOGlobalID.class,NSTimestamp.class,NSTimestamp.class};
			Object[] parametres = new Object[] {editingContext().globalIDForObject(individu),editingContext().globalIDForObject(prime),debutPeriode,finPeriode};
			NSTimestamp debut = new NSTimestamp();
			LogManager.logDetail("GestionAttribution - Lancement du traitement sur le serveur");
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			PrimeResultatCalcul resultatCalcul = (PrimeResultatCalcul)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestCalculerResultatsAttributionPourIndividuEtPrime",classeParametres,parametres,false);
			NSTimestamp fin = new NSTimestamp();
			long duree = (fin.getTime() - debut.getTime()) / 1000;
			LogManager.logDetail("GestionAttribution - Fin du traitement sur le serveur, duree " + duree + " secondes");
			if (resultatCalcul.diagnostic() != null) {
				if (resultatCalcul.diagnostic().equals(PrimeResultatCalcul.SANS_ATTRIBUTION)) {
					EODialogs.runErrorDialog("Erreur","Pas d'attribution pour cet individu");
				} else {
					EODialogs.runErrorDialog("Erreur", resultatCalcul.diagnostic());
				} 
			} else {
				// 12/03/2010 - pour signaler que l'attribution a été éditée une fois qu'elle est calculée
				setEdited(true);
				// Il y a forcément au moins une attribution sans problème
				currentAttribution().addObjectToBothSidesOfRelationshipWithKey(individu,"individu");
				currentAttribution().addObjectToBothSidesOfRelationshipWithKey(prime,"prime");
				preparerAttributionAvecResultat(currentAttribution(), (PrimeResultat)resultatCalcul.resultats().objectAtIndex(0));
				autresAttributionsCreees = new NSMutableArray();
				if (resultatCalcul.resultats().count() > 1) {
					for (int i = 1; i < resultatCalcul.resultats().count();i++) {
						PrimeResultat resultat = (PrimeResultat)resultatCalcul.resultats().objectAtIndex(i);
						EOPrimeAttribution attribution = new EOPrimeAttribution();
						attribution.initAvecPrimeEtIndividu(prime, individu);
						if (!preparerAttributionAvecResultat(attribution, resultat)) {
							annuler();		// On annule la création de tous les objets
							return;
						} else {
							autresAttributionsCreees.addObject(attribution);
						}
					}
					if (prime.estPersonnalisable()) {
						EODialogs.runInformationDialog("Attention", "Plusieurs attributions ont été calculées, il faudra saisir les paramètres personnels pour chaque attribution");
					}
				}
			}	
		} catch (Exception exc) {
			LogManager.logException(exc);
			EODialogs.runErrorDialog("Erreur","Erreur d'évaluation "  + exc.getMessage());
		}
	}
	private boolean preparerAttributionAvecResultat(EOPrimeAttribution attribution,PrimeResultat resultat) {
		if (resultat.messageErreur() == null || resultat.messageErreur().length() == 0) {
			attribution.setPattExercice(new Integer(annee));
			attribution.modifierAvecResultat(resultat);
			if (rentreDansBudget(attribution,false)) {
				attribution.estProvisoire();
			} else {
				attribution.estSuspendue();
			}
			return true;
		} else {
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite pendant le calcul du montant\n" + resultat.messageErreur());
			return false;
		}
	}
	private NSArray recalculerAttribution(EOPrimeAttribution attribution) {
		Class[] classeParametres = new Class[] {EOGlobalID.class};
		Object[] parametres = new Object[] {editingContext().globalIDForObject(attribution)};
		NSArray attributions = null;
		try {
			NSTimestamp debut = new NSTimestamp();
			LogManager.logDetail("GestionAttribution - Lancement du calcul de l'attribution sur le serveur");
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			PrimeResultatCalcul resultatCalcul = (PrimeResultatCalcul)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestRecalculerAttribution",classeParametres,parametres,true);
			if (resultatCalcul != null && resultatCalcul.typeResultat() == PrimeResultatCalcul.TYPE_ATTRIBUTION && resultatCalcul.resultats() != null) {
				attributions = Utilitaires.tableauObjetsMetiers(resultatCalcul.resultats(), editingContext());
			}
			NSTimestamp fin = new NSTimestamp();
			long duree = (fin.getTime() - debut.getTime()) / 1000;
			LogManager.logDetail("GestionAttribution - Fin du traitement sur le serveur, duree " + duree + " secondes");
			if (resultatCalcul != null && resultatCalcul.diagnostic() != null && resultatCalcul.diagnostic().length() > 0) {
				// Une erreur s'est produite pendant les calculs
				EODialogs.runErrorDialog("ERREUR", resultatCalcul.diagnostic());
			}
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}
		return attributions;
	}
	private boolean datesAttributionValides(NSArray attributionsPourPrime) {
		// Vérifie que les dates ne sont pas à cheval avec celles d'autres attributions de l'individu
		// liées à l'attribution courante car le DG ne contient que les attributions courantes
		if (displayGroup().allObjects().count() > 1) {
			java.util.Enumeration e = displayGroup().allObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
				if (attribution != currentAttribution() && Utilitaires.IntervalleTemps.intersectionPeriodes(currentAttribution().debutValidite(), currentAttribution().finValidite(), attribution.debutValidite(), attribution.finValidite()) != null) {
					EODialogs.runErrorDialog("Erreur", "Les dates saisies sont à cheval avec les dates d'une autre attribution, veuillez modifier les dates");
					return false;
				}
			}
		}
		if (attributionsPourPrime == null) {
			// Vérifier qu'elles ne sont pas à cheval avec les autres attributions de cette prime (cas des primes multiples)
			attributionsPourPrime = EOPrimeAttribution.rechercherAttributionsEnCoursPourPrimeIndividuEtPeriode(editingContext(),currentAttribution().individu(), currentAttribution().prime(), currentAttribution().debutValidite(), currentAttribution().finValidite());
		}
		if (attributionsPourPrime != null && attributionsPourPrime.count() > 0) {
			java.util.Enumeration e = attributionsPourPrime.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
				if (displayGroup().allObjects().containsObject(attribution) == false) {
					// Il s'agit d'une attribution déjà effectuée pour ces dates et cette prime
					EODialogs.runErrorDialog("Erreur", "Les dates saisies sont à cheval avec les dates d'une autre attribution, veuillez modifier les dates");
					return false;
				}
			}
		}
		// Vérifier aussi si elles sont dans les dates de campagne
		if (debutCampagne != null && DateCtrl.isBefore(currentAttribution().debutValidite(), debutCampagne)) {
			EODialogs.runErrorDialog("Erreur", "La date de début de validité doit être postérieure à la date de début de la campagne");
			return false;
		}
		if (finCampagne != null && DateCtrl.isAfter(currentAttribution().finValidite(), finCampagne)) {
			EODialogs.runErrorDialog("Erreur", "La date de fin de validité doit être antérieure à la date de fin de la campagne");
		}
		return true;
	}
	private boolean depassementBudgetPourAttributions(NSArray attributions) {
		if (verifierBudget) {
			VerificateurBudget verificateur = new VerificateurBudget(editingContext(),currentPrime(),debutCampagne,finCampagne);
			java.util.Enumeration e = attributions.objectEnumerator();
			boolean budgetDepasse = false;
			while (e.hasMoreElements()) {
				EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
				double montant = attribution.pattMontant().doubleValue();
				if (montant != 0) {
					if (!budgetDepasse) {
						try {
							budgetDepasse = verificateur.validerAttribution(attribution);
						} catch (Exception exc) {
							gererExceptionPourAttribution(exc,attribution);
						}
					}
					if (budgetDepasse) {
						attribution.setEstSuspendue();
					}
				}
			}
			if (budgetDepasse) {
				EODialogs.runInformationDialog("Information","Pour des raisons de dépassement de budget, un certain nombre d'attributions sont suspendues");
			}
			return budgetDepasse;
		}
		return false;
	}
	private boolean rentreDansBudget(EOPrimeAttribution attribution,boolean updaterBudget) {
		if (verifierBudget) {
			VerificateurBudget verificateur = new VerificateurBudget(editingContext(),currentPrime(),attribution.debutValidite(),attribution.finValidite());
			try {
				return verificateur.validerAttribution(attribution,updaterBudget);
			} catch (Exception exc) {
				gererExceptionPourAttribution(exc,attribution);
				return false;
			}
		} else {
			return true;
		}
	}
	private void gererExceptionPourAttribution(Exception exc,EOPrimeAttribution attribution) {
		String message = exc.getMessage() + "\nL'attribution pour " + attribution.individu().identitePrenomFirst() + " est remise en mode ";
		if (currentPrime().estCalculee()) {
			attribution.setEstCalculee();
			message +=  "à calculer";
		} else {
			attribution.setEstProvisoire();
			message +=  "provisoire";

		}
		EODialogs.runErrorDialog("Erreur",message);
	}
	private void modifierMontantAttributionsLieesAvecPersonnalisation(EOPrimePersonnalisation personnalisation) {
		NSArray attributionsACalculer = displayGroup().allObjects();
		if (attributionsACalculer.count() == 1) {
			// Uniquement l'attribution courante
			currentAttribution().setPattMontant(personnalisation.pmpeMontant());
			currentAttribution().setPattCalcul("Montant personnalisé : " + personnalisation.pmpeMontant());
		} else {
			// prendre le ratio du montant de la personnalisation sur la durée de l'attribution
			int nbJoursTotal = DateCtrl.nbJoursEntre(personnalisation.debutValidite(), personnalisation.finValidite(), true);
			java.util.Enumeration e = attributionsACalculer.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
				int nbJours= DateCtrl.nbJoursEntre(attribution.debutValidite(), attribution.finValidite(), true);
				double montant = (personnalisation.pmpeMontant().doubleValue() * nbJours) / nbJoursTotal;
				BigDecimal result = new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
				attribution.setPattMontant(result);
				attribution.setPattCalcul("Montant personnalisé : " + result);
			}
		}
	}
	private void afficherInspecteur() {
		if (controleurPourInfo == null) {
			Window window = EOApplication.sharedApplication().windowObserver().activeWindow();
			int Y = window.getHeight() + window.getY() + 5;
			controleurPourInfo = new InspecteurAttribution(window.getX(),Y,"","");
			controleurPourInfo.afficherFenetre();
		} else {
			controleurPourInfo.changerEtat();
		}
		modifierTitreInspecteur();
		modifierAffichageInspecteur();
	}
	private void modifierTitreInspecteur() {
		if (controleurPourInfo != null) {
			// Le titre affichera le mode de calcul de la prime courante
			String titre = "";
			if (currentPrime() != null) {
				if (currentPrime().estCalculee()) {
					if (gereAttributionsGlobales) {	// On a plus de place dans la zone d'affichage de l'inspecteur
						titre = "Formule de calcul : ";
					}
					// Demander au plugin de calcul, sa formule de calcul
					Class[] classeParametres = new Class[] {EOGlobalID.class,};
					Object[] parametres = new Object[] {editingContext().globalIDForObject(currentPrime())};
					LogManager.logDetail("GestionAttribution - Recherche de la formule de calcul du plugin");
					EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
					// Ne pas pusher l'editing context, on peut être en mode création
					titre += (String)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestModeCalculPourPrime",classeParametres,parametres,false);
				} else if (currentPrime().estPersonnalisable()) {
					titre = "Le montant est la valeur personnalisée saisie pour cet individu";
				} else {
					titre = "Le montant est la valeur de cette prime";
				}
			}
			controleurPourInfo.setLabelText(titre);
		}
	}
	private void modifierAffichageInspecteur() {
		if (controleurPourInfo != null) {
			// Le message affichera le mode de calcul réel de la prime
			String message = "";
			if (currentAttribution() != null) {
				message = currentAttribution().pattCalcul();
			}
			controleurPourInfo.setText(message);
		}
	}
	private void updaterDisplayGroups() {
		displayGroup().updateDisplayedObjects();
		controllerDisplayGroup().redisplay();
	}
	/** M&eacute;thode pour valider un montant en invoquant une m&eacute;thode de validation sur le serveur
	 * @return null ou un message d'erreur.
	 * */
	private String validationMontant() {
		Class[] classeParametres = new Class[] {BigDecimal.class,EOGlobalID.class,EOGlobalID.class,NSTimestamp.class,NSTimestamp.class};
		Object[] parametres = new Object[] {personnalisationCourante.pmpeMontant(),editingContext().globalIDForObject(personnalisationCourante.individu()),editingContext().globalIDForObject(currentPrime()),personnalisationCourante.debutValidite(),personnalisationCourante.finValidite()};
		try {
			NSTimestamp debut = new NSTimestamp();
			LogManager.logDetail("GestionPersonnalisations - Lancement du traitement sur le serveur");
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			// On ne pousse pas l'editing context
			String resultat = (String)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestVerifierValiditeMontantPourIndividuEtPrime",classeParametres,parametres,false);
			NSTimestamp fin = new NSTimestamp();
			long duree = (fin.getTime() - debut.getTime()) / 1000;
			LogManager.logDetail("GestionPersonnalisations - Fin du traitement sur le serveur, duree " + duree + " secondes");
			return resultat;
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	private void valider(boolean validationModification) {
		boolean peutContinuer = !validationModification;
		if (validationModification) {
			peutContinuer = traitementsAvantValidation();
			LogManager.logDetail(getClass().getName() + " - Validation donnees " + peutContinuer);
		}
		if (peutContinuer && save()) {
			if (currentAttribution().prime().doitAvoirParametresPersonnels() && codesPourParametresPerso.count() != displayGroupParametresPerso.displayedObjects().count()) {
				// Les paramètres personnels n'ont pas encore été créés
				EODialogs.runInformationDialog("Information", "Avant de pouvoir calculer le montant de l'attribution, vous devez saisir les paramètres personnels");
				doitSaisirParametresPersonnels = true;
				displayGroupParametresPerso.setSelectedObject(null);
				updaterDisplayGroups();
			}
			else {
				traitementsApresValidation();
				setEdited(false);
				if (validationModification) {
					NSNotificationCenter.defaultCenter().postNotification(prefixeNotification + NOTIFICATION_VALIDER_ATTRIBUTION,null);
				} else {
					NSNotificationCenter.defaultCenter().postNotification(prefixeNotification + NOTIFICATION_RAFFRAICHIR_ATTRIBUTION,null);
				}
			}
		} 
	}
	
	private void preparerPeriodeCampagne(EOPrime prime,boolean notifierParent) {
		if (annee == null || prime == null) {
			debutCampagne = null;
			finCampagne = null;
		} else {
			int anneeInt = new Integer(annee).intValue();
			debutCampagne = prime.debutPeriode(anneeInt);
			finCampagne = prime.finPeriode(anneeInt);
		}
		// Notifier le parent
		if (notifierParent && debutCampagne != null && finCampagne != null) {
			NSMutableDictionary dict = new NSMutableDictionary();
			dict.setObjectForKey(debutCampagne, "debutCampagne");
			dict.setObjectForKey(finCampagne, "finCampagne");
			NSNotificationCenter.defaultCenter().postNotification(prefixeNotification + NOTIFICATION_CAMPAGNE_MODIFIEE,dict);
		} else {
			NSNotificationCenter.defaultCenter().postNotification(prefixeNotification + NOTIFICATION_CAMPAGNE_MODIFIEE,null);
		}
	}
	private void preparerParametrePersonnel(String valeurParametre) {
		if (creationParametresPersos == false) {
			EOPrimeParamPerso paramPerso = (EOPrimeParamPerso)displayGroupParametresPerso.selectedObject();
			if (paramPerso != null) {
				paramPerso.setPppeValeur(valeurParametre);
				setEdited(true);
				parametresPersonnelsChanged = true;
			}
		} else {
			setEdited(true);
			parametresPersonnelsChanged = true;
			EOPrimeCode code = (EOPrimeCode)codesPourParametresPerso.objectAtIndex(nbParametreCree);
			// Vérifier si ce paramètre a déjà été saisi
			boolean shouldInsert = true;
			java.util.Enumeration e = displayGroupParametresPerso.displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParamPerso param = (EOPrimeParamPerso)e.nextElement();
				if (param.code() == code) {
					displayGroupParametresPerso.selectObject(param);
					shouldInsert = false;
					break;
				}
			}
			if (shouldInsert) {
				displayGroupParametresPerso.insert();
			}
			EOPrimeParamPerso paramPerso = (EOPrimeParamPerso)displayGroupParametresPerso.selectedObject();
			paramPerso.initAvecCodePersonnalisationEtValeur(code,personnalisationCourante,valeurParametre);
			if (nbParametreCree < codesPourParametresPerso.count() - 1) {
				// La saisie n'est pas terminée
				afficherSaisieParametrePerso((EOPrimeCode)codesPourParametresPerso.objectAtIndex(++nbParametreCree));
			}
			updaterDisplayGroups();
		}
	}
}

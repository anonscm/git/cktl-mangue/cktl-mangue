/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.ObjetAvecDureeValiditePourPrime;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Ce composant permet de g&eacute;rer des informations en limitant la p&eacute;riode d'affichage &agrave; une ann&eacute;e */
public abstract class GestionPourPrimeAvecAnnee extends GestionPourPrimeAvecDuree {
	public EOView vueBoutonsPourAttribution;
	private String annee;
	private NSTimestamp debutCampagne,finCampagne;
	public GestionPourPrimeAvecAnnee() {
		super(false);
	}
	// Accesseurs
	public String annee() {
		return annee;
	}
	/** V&eacute;rifie que l'ann&eacute;e est sur 4 digits */
	public void setAnnee(String annee) {
		String libellePrime = libellePrime();
		this.annee = annee;
		if (annee != null && annee.length() > 0) {
			if (annee.length() < 4) {
				this.annee = null;
			} else {
				// Vérifier si il s'agit bien d'un nombre entier
				try {
					int numAnnee = new Integer(annee).intValue();
					if (numAnnee < 1900 || numAnnee > 2100) {
						this.annee = null;
					}
				} catch (Exception e) {
					this.annee = null;
				}
			}
		}
		preparerPrimes();
		if (libellePrime != null) {
			setLibellePrime(libellePrime);
		}
		preparerPeriodeCampagne();
		preparerDisplayGroup();
		updaterDisplayGroups();
	}
	/** Surcharge du parent pour pr&eacute;parer les dates la p&eacute;riode de campagne */
	public void setLibellePrime(String aStr) {
		super.setLibellePrime(aStr);
		preparerPeriodeCampagne();
		updaterDisplayGroups();
	}
	// Méthodes de délégation du display group
	/** Pour v&eacute;rifier si les dates saisies sont dans la p&eacute;riode choisie si n&eacute;cessaire */
	public void displayGroupDidSetValueForObject(EODisplayGroup group, Object value, Object eo, String key) {
		if (group == displayGroup()) {
			if (key.equals("debutValiditeFormatee") || key.equals("finValiditeFormatee")) {
				int anneeInt = new Integer(annee()).intValue();
				NSTimestamp dateDebut = currentPrime().debutPeriode(anneeInt);
				NSTimestamp dateFin = currentPrime().finPeriode(anneeInt);
				if (key.equals("debutValiditeFormatee")) {	
					if (currentObjet().debutValidite() != null) {
						if (DateCtrl.isBefore(currentObjet().debutValidite(),dateDebut) || DateCtrl.isAfter(currentObjet().debutValidite(), dateFin)) {
							currentObjet().setDebutValidite(dateDebut);
						}
						if (currentObjet().finValidite() != null && DateCtrl.isAfter(currentObjet().debutValidite(), currentObjet().finValidite())) {
							currentObjet().setDebutValidite(dateDebut);
						}
					}
				} else if (key.equals("finValiditeFormatee")) {
					if (currentObjet().finValidite() != null) {
						if (DateCtrl.isBefore(currentObjet().finValidite(), dateDebut) ||  DateCtrl.isAfter(currentObjet().finValidite(), dateFin)) {
							currentObjet().setFinValidite(dateFin);
						}
						if (currentObjet().finValidite() != null && DateCtrl.isAfter(currentObjet().debutValidite(), currentObjet().finValidite())) {
							currentObjet().setDebutValidite(dateDebut);
						}
					}
				}
			}
			updaterDisplayGroups();
		}
	}
	// Méthodes du controller DG
	/** On ne peut saisir une ann&eacute;e que si on a les droits et qu'on n'est pas en mode modification */
	public boolean peutSaisirAnnee() {
		return super.conditionSurPageOK() && !estLocke() && !modificationEnCours();
	}
	/** On ne peut choisir une prime dans le popup que si on n'est pas en mode modification et que l'ann&eacute;e a &eacute;t&eacute; saisie */
	public boolean peutChoisirPrime() {
		return super.peutChoisirPrime() && annee() != null;
	}
	/** Retourne la date de d&eacute;but de campagne d'attribution */
	public NSTimestamp debutCampagne() {
		return debutCampagne;
	}
	/** Retourne la date de fin de campagne d'attribution */
	public NSTimestamp finCampagne() {
		return finCampagne;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		setAnnee((new Integer(DateCtrl.getYear(new NSTimestamp()))).toString());
	}
	/** Retourne les primes conformes &agrave; la validit&eacute; lorsque les dates sont chang&eacute;es
	 */
	protected NSArray primesPourPeriode(boolean estValide) {
		if (annee() == null) {
			return null;
		} else {
			NSTimestamp dateDebut = DateCtrl.stringToDate("01/01/" + annee), dateFin = DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutAnnees(dateDebut, 1));
			if (estValide) {
				return EOPrime.rechercherPrimesValidesPourPeriode(editingContext(),dateDebut,dateFin);
			} else {
				return EOPrime.rechercherPrimesInvalidesPourPeriode(editingContext(),dateDebut,dateFin);
			}
		}
	}
	/** Retourne les objets conforme &agrave; la validit&eacute; lorsque le popup de validit&eacute; est modifi&eacute;,
	 * null si pas de prime s&eacute;lectionn&eacute;e.
	 */
	protected  NSArray objetsPourValidite(boolean estValide) {
		if (currentPrime() != null && annee() != null) {
			int anneeInt = new Integer(annee()).intValue();
			NSTimestamp dateDebut = currentPrime().debutPeriode(anneeInt);
			NSTimestamp dateFin = currentPrime().finPeriode(anneeInt);
			if (estValide) {
				return ObjetAvecDureeValiditePourPrime.rechercherObjetsValidesPourPrimeEtPeriode(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),currentPrime(),dateDebut,dateFin);
			} else {
				return ObjetAvecDureeValiditePourPrime.rechercherObjetsInvalidesPourPrimeEtPeriode(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),currentPrime(),dateDebut,dateFin);
			}
		} else {
			return null;
		}
	}
	protected void preparerPeriodeCampagne() {
		if (annee() == null || currentPrime() == null) {
			debutCampagne = null;
			finCampagne = null;
		} else {
			int anneeInt = new Integer(annee()).intValue();
			debutCampagne = currentPrime().debutPeriode(anneeInt);
			finCampagne = currentPrime().finPeriode(anneeInt);
		}
	}
}

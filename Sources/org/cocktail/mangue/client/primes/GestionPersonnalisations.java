/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import java.math.BigDecimal;

import org.cocktail.client.components.DialogueSimpleSansFetch;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;
import org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation;
import org.cocktail.mangue.modele.goyave.primes.IndividuPourPrime;
import org.cocktail.mangue.modele.goyave.primes.PrimeResultatEligibilite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Permet de saisir un montant personnalis&eacute; pour un individu qui sera utilis&eacute; dans les attributions de prime.
 * Les primes qui permettent la saisie de montant personnalis&eacute; sont les primes qui sont personnalisables et qui n'ont qu'un
 * seul code utilis&eacute; pour la personnalisation, (i.e il n'existe pas de param&egrave;tre standard pour le code). Le code doit
 * commencer par 'MO'
 * @author christine
 *
 */
// 21/12/2010 - Adaptation Netbeans (Suppression de preparerFenetre)
public class GestionPersonnalisations extends GestionPourPrimeAvecAnnee {
	public final static String NOTIFICATION_ATTRIBUTION_MODIFIEE = "NotificationAttributionModifiee";
	private final static String TYPE_AVEC_ATTRIBUTION_VALIDE = "Avec Attributions valides";
	//SelectionSimpleSansAffichage controleurSelectionIndividu;
	private NSArray attributionsPourIndividu;
	private InspecteurPersonnalisationsPrecedentes inspecteurPersPrec;

	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		if (group == displayGroup()) {
			if (annee() == null || currentPrime() == null || currentPersonnalisation() == null || currentPersonnalisation().individu() == null || typeValidite() == null || typeValidite().equals(TYPE_VALIDE) == false) {
				attributionsPourIndividu = null;
			} else {
				attributionsPourIndividu  = EOPrimeAttribution.rechercherAttributionsEnAttentePourPrimeIndividuEtPeriode(editingContext(), currentPersonnalisation().individu(), currentPrime(),debutCampagne(),finCampagne());
			}
			super.displayGroupDidChangeSelection(group);
			NSNotificationCenter.defaultCenter().postNotification(InspecteurPersonnalisationsPrecedentes.NOTIFICATION_CHANGEMENT_PERSONNALISATION,currentPersonnalisation());
		} 
	}
	// Actions
	public void afficherIndividus() {
		LogManager.logDetail("GestionPersonnalisations - afficherIndividus");
		// afficher la liste des agents pour sélection
		DialogueSimpleSansFetch controleurSelectionIndividu = new DialogueSimpleSansFetch(EOIndividu.ENTITY_NAME,"Personnalisation_SelectionIndividu","nomUsuel","Sélection d'un individu",false,true,false,false,false);
		controleurSelectionIndividu.init();
		controleurSelectionIndividu.setQualifier(EOQualifier.qualifierWithQualifierFormat("temValide = 'O' AND personnels.noDossierPers <> nil",null));
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getIndividu",new Class[] {NSNotification.class}),"Personnalisation_SelectionIndividu",null);
		controleurSelectionIndividu.afficherFenetre();		
	}
	public void renouveler() {
		LogManager.logDetail("GestionPersonnalisations - renouveler");
		// Lancer un traitement sur le serveur pour renouveler les personnalisations et récupérer le mssage du traitement
		Class[] classeParametres = new Class[] {EOGlobalID.class,String.class};
		Object[] parametres = new Object[] {editingContext().globalIDForObject(currentPrime()),annee()};
		LogManager.logDetail("GestionPersonnalisation - Lancement du traitement sur le serveur");
		EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
		String resultat = (String)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestRenouvelerPersonalisations",classeParametres,parametres,false);
		if (resultat != null) {
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite pendant le renouvellement des personnalisations : " + resultat);
		} else {
			//String libellePrime = libellePrime();
			setAnnee("" + (new Integer(annee()).intValue() + 1));
			//setLibellePrime(libellePrime);
			updaterDisplayGroups();
		}
	}
	public void ouvrirInspecteurPersoPrec() {
		LogManager.logDetail(this.getClass().getName() + " - ouvrirInspecteurPersoPrec");
		if (inspecteurPersPrec == null) {
			inspecteurPersPrec = new InspecteurPersonnalisationsPrecedentes(editingContext());
			inspecteurPersPrec.init();
		}
		inspecteurPersPrec.afficher();	
		NSNotificationCenter.defaultCenter().postNotification(InspecteurPersonnalisationsPrecedentes.NOTIFICATION_CHANGEMENT_PERSONNALISATION,currentPersonnalisation());
	}
	// Notifications
	public void getIndividu(NSNotification aNotif) {
		if (modificationEnCours()) {
			LogManager.logDetail("GestionPersonnalisations - Notification getIndividu");
			if (aNotif.object() != null) {
				EOIndividu individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(),editingContext());
				// Vérifier si l'individu n'a pas déjà une personnalisation et si il peut bénéficier de la prime
				if (individu != null) {
					if (((NSArray)displayGroup().displayedObjects().valueForKey("individu")).containsObject(individu) == false) {
						try {
							NSTimestamp dateDebut = currentPersonnalisation().debutValidite();
							if (dateDebut == null) {
								dateDebut = debutCampagne();
							}
							NSTimestamp dateFin = currentPersonnalisation().finValidite();
							if (dateFin == null) {
								dateFin = finCampagne();
							}
							// Ici il faut faire un appel sur le serveur car il se peut qu'on s'adresse au plug-in
							Class[] classeParametres = new Class[] {EOGlobalID.class,EOGlobalID.class,NSTimestamp.class,NSTimestamp.class};
							Object[] parametres = new Object[] {editingContext().globalIDForObject(individu),editingContext().globalIDForObject(currentPrime()),debutCampagne(),finCampagne()};
							NSTimestamp debut = new NSTimestamp();
							LogManager.logDetail("GestionPersonnalisations - Lancement du traitement sur le serveur");
							EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
							// On ne pousse pas l'editing context
							PrimeResultatEligibilite resultat = (PrimeResultatEligibilite)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestResultatEligibilitePourPrime",classeParametres,parametres,false);
							NSTimestamp fin = new NSTimestamp();
							long duree = (fin.getTime() - debut.getTime()) / 1000;
							LogManager.logDetail("GestionPersonnalisations - Fin du traitement sur le serveur, duree " + duree + " secondes");
							if (resultat.diagnosticRefus() != null) {
								EODialogs.runErrorDialog("Erreur","Cet individu ne peut pas beneficier de cette prime sur la période choisie\nDiagnostic :" + resultat.diagnosticRefus());
							} else {
								resultat.preparerDansEditingContext(editingContext());
								IndividuPourPrime individuPourPrime = resultat.individuPourPrimeAtIndex(0);
								currentPersonnalisation().addObjectToBothSidesOfRelationshipWithKey(individu, "individu");
								currentPersonnalisation().setDebutValidite(individuPourPrime.periodeDebut());
								currentPersonnalisation().setFinValidite(individuPourPrime.periodeFin());
								attributionsPourIndividu  = EOPrimeAttribution.rechercherAttributionsEnAttentePourPrimeIndividuEtPeriode(editingContext(), currentPersonnalisation().individu(), currentPrime(),debutCampagne(),finCampagne());
							}
						} catch (Exception e) {
							LogManager.logException(e);
							EODialogs.runErrorDialog("Erreur","Erreur d'évaluation "  + e.getMessage());
						}
					} else {
						EODialogs.runErrorDialog("Erreur","Cet individu a déjà un montant qui lui est attribué pour cette prime");
					}
				}
				NSNotificationCenter.defaultCenter().postNotification(InspecteurPersonnalisationsPrecedentes.NOTIFICATION_CHANGEMENT_PERSONNALISATION,currentPersonnalisation());
			}
		}
	}
	/** Pour r&eacute;afficher les personnalisations si elles ont &eacute;t&eacute; modifi&eacute;es suite &grave; la validation
	 * des attributions
	 */
	public void reafficherPersonnalisations(NSNotification aNotif) {
		LogManager.logDetail("GestionPersonnalisations - Notification reafficher personnalisations");
		EOGlobalID primeID = (EOGlobalID)aNotif.object();
		EOPrime prime = (EOPrime)SuperFinder.objetForGlobalIDDansEditingContext(primeID, editingContext());
		if (currentPrime() == prime) {
			preparerDisplayGroup();
			updaterDisplayGroups();
		}
	}
	// Méthodes du controller DG
	/** On peut modifier une personnalisation si les attributions faites pour cet individu et cette prime ne sont pas encore valid&eacute;es
	 * pendant la p&eacute;riode ie que la personnalisation est valide
	 */
	public boolean boutonModificationAutorise() {
		return super.boutonModificationAutorise() && currentPersonnalisation() != null && currentPersonnalisation().estValide();
	}
	/** On ne peut modifier les dates que si il s'agit d'une prime &agrave; versement unique */
	public boolean peutModifierDates() {
		return modificationEnCours() && currentPrime().periodicite().equals(EOPrime.VERSEMENT_UNIQUE);
	}
	/** L'individu, le montant et les dates doivent &ecirc;tre saisis */
	public boolean peutValider() {
		return super.peutValider() && currentPersonnalisation().debutValidite() != null && currentPersonnalisation().finValidite() != null &&
		currentPersonnalisation().pmpeMontant() != null && currentPersonnalisation().individu() != null && currentPersonnalisation().prime() != null;
	}
	/** On peut demander le renouvellement des personnalisations si l'ann&eacute;e et la prime sont s&eacute;lectionn&eacute;es et
	 * qu'il s'agit d'une prime valide */
	public boolean peutRenouveler() {
		return super.modeSaisiePossible() && annee() != null && currentPrime() != null && currentPrime().estValide();
	}
	// Méthodes protégées
	protected void terminer() {
	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("reafficherPersonnalisations", new Class[] {NSNotification.class}),
				GestionPrimes.NOTIFICATION_ATTRIBUTIONS_VALIDEES,null);
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cet individu  ?";
	}
	protected void traitementsPourCreation() {
		currentPersonnalisation().initAvecPrimeEtDates(currentPrime(),debutCampagne(),finCampagne());
	}
	/** V&eacute;rifier si le montant est correct relativement &agrave; la d&eacute;finition de la prime dans le cas de primes
	 * qui sont associ&eacute;es &agrave; des plugs-in de calcul */
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) {
			if (currentPrime().primNomClasse() != null) {
				String resultat = validationMontantPourIndividuPrimeEtPeriode(currentPersonnalisation().pmpeMontant(),currentPersonnalisation().individu(),currentPrime());
				if (resultat != null) {
					EODialogs.runErrorDialog("ERREUR", resultat);
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	/** Pour modifier le statut des attributions en cours */
	protected void traitementsApresValidation() {
		if (attributionsPourIndividu != null) {
			// Modifier les attributions en attente pour leur donner le statut "à recalculer"
			java.util.Enumeration e = attributionsPourIndividu.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeAttribution attribution = (EOPrimeAttribution)e.nextElement();
				attribution.setEstCalculee();
			}
			try {
				editingContext().saveChanges();
				NSMutableDictionary dict = new NSMutableDictionary();
				dict.setObjectForKey(editingContext().globalIDForObject(currentPrime()), "prime");
				dict.setObjectForKey(editingContext().globalIDForObject(currentPersonnalisation().individu()), "individu");
				NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_ATTRIBUTION_MODIFIEE,dict);
				EODialogs.runInformationDialog("Attention", "N'oubliez pas de recalculer les attributions de cet individu");
			} catch (Exception exc) {
				EODialogs.runErrorDialog("ERREUR", "Impossible de changer le statut des attributions de l'individu, pensez à les recalculer");
			}
		}
		super.traitementsApresValidation();
	}
	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		// Pour le type en cours, on n'affiche que les attributions en attentes
		if (typeValidite() != null && typeValidite().equals(TYPE_AVEC_ATTRIBUTION_VALIDE)) {
			displayGroup().setQualifier(EOQualifier.qualifierWithQualifierFormat("temValide = 'U'", null));
		}
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending));
		displayGroup().setSortOrderings(sorts);
	}
	/** Retourne les primes conformes &agrave; la validit&eacute; lorsque les dates sont chang&eacute;es
	 */
	protected NSArray primesPourPeriode(boolean estValide) {
		if (annee() == null) {
			return null;
		} else {
			NSArray primes = super.primesPourPeriode(estValide);
			return primesGlobalesPersonnalisables(primes);
		}
	}
	/** Surcharge du parent pour g&eacute;rer les personnalisations utilis&eacute;es */
	protected void preparerDisplayGroup() {
		if (typeValidite() == null || currentPrime() == null) {
			displayGroup().setObjectArray(null);
		} else if (typeValidite() != null) {
			if (typeValidite().equals(TYPE_VALIDE)) {
				displayGroup().setObjectArray(EOPrimePersonnalisation.rechercherPersonnalisationsValidesPourPrimeEtPeriode(editingContext(), currentPrime(), debutCampagne(), finCampagne()));
			} else if (typeValidite().equals(TYPE_AVEC_ATTRIBUTION_VALIDE)) {
				displayGroup().setObjectArray(EOPrimePersonnalisation.rechercherPersonnalisationsUtiliseesPourPrimeEtPeriode(editingContext(), currentPrime(), debutCampagne(), finCampagne()));
			} else if (typeValidite().equals(TYPE_HISTORIQUE)) {
				displayGroup().setObjectArray(EOPrimePersonnalisation.rechercherPersonnalisationsInvalidesPourPrimeEtPeriode(editingContext(), currentPrime(), debutCampagne(), finCampagne()));
			} 
		}
		updaterDisplayGroups();
	}
	// Méthodes privées
	private EOPrimePersonnalisation currentPersonnalisation() {
		return (EOPrimePersonnalisation)displayGroup().selectedObject();
	}
	private NSArray primesGlobalesPersonnalisables(NSArray primes) {
		NSMutableArray result = new NSMutableArray();
		java.util.Enumeration e = primes.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrime prime = (EOPrime)e.nextElement();
			if (prime.estIndividuelle() == false && prime.estPersonnalisable() && prime.doitSaisirMontantPersonnalise()) {
				result.addObject(prime);
			}
		}
		return result;
	}
	/* Méthode pour valider un montant en invoquant une méthode de validation sur le serveur
	 * return null ou un message d'erreur.
	 */
	private String validationMontantPourIndividuPrimeEtPeriode(BigDecimal montant,EOIndividu individu,EOPrime prime) {
		Class[] classeParametres = new Class[] {BigDecimal.class,EOGlobalID.class,EOGlobalID.class,NSTimestamp.class,NSTimestamp.class};
		Object[] parametres = new Object[] {montant,editingContext().globalIDForObject(individu),editingContext().globalIDForObject(prime),debutCampagne(),finCampagne()};
		try {
			NSTimestamp debut = new NSTimestamp();
			LogManager.logDetail("GestionPersonnalisations - Lancement du traitement sur le serveur");
			EODistributedObjectStore store = (EODistributedObjectStore)editingContext().parentObjectStore();
			// On ne pousse pas l'editing context
			String resultat = (String)store.invokeRemoteMethodWithKeyPath(editingContext(),"session","clientSideRequestVerifierValiditeMontantPourIndividuEtPrime",classeParametres,parametres,false);
			NSTimestamp fin = new NSTimestamp();
			long duree = (fin.getTime() - debut.getTime()) / 1000;
			LogManager.logDetail("GestionPersonnalisations - Fin du traitement sur le serveur, duree " + duree + " secondes");
			return resultat;
		} catch (Exception e) {
			return e.getMessage();
		}
	}
}

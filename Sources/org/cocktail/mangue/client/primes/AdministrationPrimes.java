/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.outils_interface.DialogueAvecSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPlanComptable;
import org.cocktail.mangue.modele.goyave.EOPrimeCode;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Interface de gestion et de param&eacute;trage des primes */
// 21/12/2010 - Adaptation Netbeans
// 21/12/2010 - Ajout de la gestion du plan comptable
public class AdministrationPrimes extends GestionAvecValidite {
	public EODisplayGroup displayGroupCodes;
	public EOTable listeCodes;
	private GestionCodes controleurCode;
	private GestionAide controleurAide;
	private String typePopulationSelectionnee;
	private int moisSelectionne;
	private boolean preparationMois;
	private final static String TOUTE_POPULATION = "*";
	/** Notification pour le raffraichissement des primes lorsqu'elles sont invalid&eacute;es ou modifi&eacute;es */
	public final static String NOTIFICATION_RAFFRAICHIR_PRIME = "NotificationRaffraichirPrime";
	// Accesseurs
	public String typePopulationSelectionnee() {
		return typePopulationSelectionnee;
	}
	public void setTypePopulationSelectionnee(String typePopulationSelectionnee) {
		this.typePopulationSelectionnee = typePopulationSelectionnee;
		parametrerDisplayGroup();
		updaterDisplayGroups();
	}
	public int moisSelectionne() {
		return moisSelectionne;
	}
	public void setMoisSelectionne(int numMois) {
		if (!preparationMois && modificationEnCours()) {
			currentPrime().setPrimMoisDemarrage(new Integer(numMois + 1));
			moisSelectionne = numMois;
		}
	}
	// Méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup group) {
		preparationMois = true;
		if (group == displayGroup()) {
			preparerMois();
		}
		super.displayGroupDidChangeSelection(group);
		preparationMois = false;
	}
	public void displayGroupDidSetValueForObject(EODisplayGroup group, Object value, Object eo, String key) {
		if (group == displayGroup()) {
			if (key.equals("typePopulation")) {
				// Pour mettre comme mois de démarrage septembre si il s'agit d'une prime pour les enseignants */
				if (currentPrime().estPrimePourEnseignants()) {
					setMoisSelectionne(8);
				} else {
					setMoisSelectionne(0);
				}
			}
			if (key.equals("estAttributionUniqueDansAnnee")) {
				if (currentPrime().estAttributionUniqueDansAnnee() == false) {
					currentPrime().setEstIndividuelle(true);
					currentPrime().setPeriodicite(EOPrime.VERSEMENT_UNIQUE);
				}
			}
			updaterDisplayGroups();
		}
	}
	// Actions
	// 22/12/2010
	public void afficherPlanComptable() {
		UtilitairesDialogue.afficherDialogue(this,"PlanComptable", "getPlanComptable", true, EOQualifier.qualifierWithQualifierFormat("pcoValidite = 'VALIDE'", null),false);
	}
	public void supprimerPlanComptable() {
		currentPrime().setPcoNum(null);
	}
	/** Duplique une prime et les donn&eacute;es associ&eacute;es */
	public void dupliquer() {
		LogManager.logDetail("Administration Prime - dupliquer");
		EOPrime primeDupliquee = currentPrime();
		ajouter();
		currentPrime().updateAvecPrime(primeDupliquee);
		updaterDisplayGroups();
	}
	public void ajouterCodes() {
		LogManager.logDetail("Administration Prime - afficherCodes");
		if (controleurCode == null) {
			String nomNotification = "SelectionCodeParam";
			controleurCode = GestionCodes.chargerDialogue(nomNotification, true);
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getCodes",new Class[] {NSNotification.class}),nomNotification,null);
		}
		controleurCode.afficherFenetre();
	}
	public void supprimerCodes() {
		LogManager.logDetail("Administration Prime - supprimerCodes");
		java.util.Enumeration e = displayGroupCodes.selectedObjects().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeCode code = (EOPrimeCode)e.nextElement();
			currentPrime().removeObjectFromBothSidesOfRelationshipWithKey(code,"codes");
		}
		displayGroupCodes.updateDisplayedObjects();	
	}
	public void afficherAide() {
		if (controleurAide == null) {
			controleurAide = new GestionAide("Aide Utilisateur de la prime","Saisissez un texte",true);
			controleurAide.init();
		}
		if (currentPrime() != null) {
			controleurAide.setTexte(currentPrime().primCommentaire());
		} else {
			controleurAide.setTexte(null);
		}
		controleurAide.afficherFenetre();
	}
	// Notifications
	public void getCodes(NSNotification aNotif) {
		if (modificationEnCours()) {
			// On attend un tableau de globalIDs
			NSArray codesID = (NSArray)aNotif.object();
			if (codesID != null && codesID.count() > 0) {
				java.util.Enumeration e = codesID.objectEnumerator();
				while (e.hasMoreElements()) {
					EOPrimeCode code = (EOPrimeCode)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)e.nextElement(), editingContext());
					if (displayGroupCodes.displayedObjects().containsObject(code) == false) {
						currentPrime().addObjectToBothSidesOfRelationshipWithKey(code, "codes");
					}
				}
				displayGroupCodes.updateDisplayedObjects();
			}
		}
	}
	public void getAide(NSNotification aNotif) {
		if (modificationEnCours()) {
			currentPrime().setPrimCommentaire((String)aNotif.object());
			setEdited(true);
		}
	}
	// 22/12/2010
	public void getPlanComptable(NSNotification aNotif) {
		EOGlobalID gid = (EOGlobalID)aNotif.object();
		EOPlanComptable plan = (EOPlanComptable)SuperFinder.objetForGlobalIDDansEditingContext(gid, editingContext());
		if (plan != null) {
			currentPrime().setPcoNum(plan.pcoNum());
		}
	}
	// Méthodes du controller DG
	// 21/12/2010
	public boolean peutSupprimerPlanComptable() {
		return modificationEnCours() && currentPrime().pcoNum() != null;
	}
	public boolean peutValider() {
		return super.peutValider() && currentPrime().debutValidite() != null && currentPrime().primTypePopulation() != null &&
		currentPrime().primCode() != null && currentPrime().primLibCourt() != null;
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		moisSelectionne = -1;
		setTypePopulationSelectionnee(TOUTE_POPULATION);
		GraphicUtilities.changerTaillePolice(listeCodes,11);
		listeCodes.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		GraphicUtilities.rendreNonEditable(listeCodes);

		super.preparerFenetre();

	}
	protected void loadNotifications() {
		super.loadNotifications();
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("getAide", new Class[] { NSNotification.class }), DialogueAvecSaisie.NOTIFICATION_SAISIE, null);
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette prime ?";
	}
	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		// On récupère le qualifier existant sur la validité et on y ajoute celui sur la population
		NSMutableArray qualifiers = new NSMutableArray();
		if (displayGroup().qualifier() != null) {
			qualifiers.addObject(displayGroup().qualifier());
			if (typePopulationSelectionnee != null && typePopulationSelectionnee.equals(TOUTE_POPULATION) == false) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("primTypePopulation = %@", new NSArray(typePopulationSelectionnee.substring(0,1))));
			}
		} else {
			if (typePopulationSelectionnee != null && typePopulationSelectionnee.equals(TOUTE_POPULATION) == false) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("primTypePopulation = %@", new NSArray(typePopulationSelectionnee.substring(0,1))));
			}
		}
		if (qualifiers.count() > 0) {
			displayGroup().setQualifier(new EOAndQualifier(qualifiers));
		} else {
			displayGroup().setQualifier(null);
		}
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("primCodeIndemnite", EOSortOrdering.CompareAscending)));
	}
	protected void traitementsApresValidation() {
		NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_RAFFRAICHIR_PRIME,null);
		super.traitementsApresValidation();
	}
	protected void traitementsApresSuppression() {
		NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_RAFFRAICHIR_PRIME,null);
		preparerMois();
		super.traitementsApresSuppression();
	}
	protected void traitementsApresRevert() {
		preparerMois();
		super.traitementsApresRevert();
	}
	protected void terminer() {
	}
	// Méthodes privées
	private EOPrime currentPrime() {
		return (EOPrime)displayGroup().selectedObject();
	}
	private void preparerMois() {
		if (currentPrime() == null || currentPrime().primMoisDemarrage() == null) {
			moisSelectionne = -1;
		} else {
			moisSelectionne = currentPrime().primMoisDemarrage().intValue() - 1;
		}
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.primes;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.ObjetAvecDureeValiditePourPrime;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public abstract class GestionPourPrimeAvecDuree extends GestionAvecValidite {
	public EOView vuePeriode;
	public EODisplayGroup displayGroupPrimes;
	private EOPrime currentPrime;
	private boolean afficherPrimesValides;
	protected GestionPeriode controleurPeriode;
	private boolean changerPeriodeApresValidation;

	// Constructeur
	/** changerPeriodeApresValidation true si le contrôleur de période doit afficher les dates saisies dans l'objet courant */
	public GestionPourPrimeAvecDuree(boolean changerPeriodeApresValidation) {
		super();
		this.changerPeriodeApresValidation = changerPeriodeApresValidation;
	}
	public GestionPourPrimeAvecDuree() {
		this(true);
	}
	// Accesseurs
	public boolean afficherPrimesValides() {
		return afficherPrimesValides;
	}
	public void setAfficherPrimesValides(boolean afficherPrimesValides) {
		if (this.afficherPrimesValides != afficherPrimesValides) {
			this.afficherPrimesValides = afficherPrimesValides;
			preparerPrimes();
			if (afficherPrimesValides == false) {
				setTypeValidite(GestionAvecValidite.TYPE_HISTORIQUE);
			}
			displayGroup().setSelectedObject(null);
			displayGroup().setObjectArray(null);
			currentPrime = null;
			updaterDisplayGroups();
		}
	}
	public String libellePrime() {
		if (currentPrime != null) {
			return currentPrime.libelleAvecCode();
		} else {
			return null;
		}
	}
	public void setLibellePrime(String aStr) {
		if (aStr == null) {
			currentPrime = null;
			displayGroupPrimes.setSelectedObject(null);
			displayGroup().setSelectedObject(null);
		} else {
			currentPrime = null;
			displayGroup().setSelectedObject(null);
			java.util.Enumeration e = displayGroupPrimes.displayedObjects().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrime prime = (EOPrime)e.nextElement();
				if (prime.libelleAvecCode().equals(aStr)) {
					displayGroupPrimes.setSelectedObject(prime);
					currentPrime = prime;
					break;
				}
			}
		}
		displayGroup().setObjectArray(objetsPourValidite(typeValidite() != null && typeValidite().equals(TYPE_VALIDE)));
		updaterDisplayGroups();
	}
	// Méthodes de délégation du display group
	/** Pour v&eacute;ifier si les dates saisies sont dans la p&eacute;riode choisie si n&eacute;cessaire */
	public void displayGroupDidSetValueForObject(EODisplayGroup group, Object value, Object eo, String key) {
		if (group == displayGroup()) {
			if (key.equals("debutValiditeFormatee")) {	
				if (!changerPeriodeApresValidation) {
					if (currentObjet().debutValidite() == null || DateCtrl.isBefore(currentObjet().debutValidite(), controleurPeriode.dateDebut())) {
						currentObjet().setDebutValidite(controleurPeriode.dateDebut());
					}
					if (controleurPeriode.dateFin() != null && currentObjet().debutValidite() != null && DateCtrl.isAfter(currentObjet().debutValidite(),controleurPeriode.dateFin())) {
						currentObjet().setDebutValidite(controleurPeriode.dateDebut());
					}
				}
				if (currentObjet().finValidite() != null && DateCtrl.isAfter(currentObjet().debutValidite(), currentObjet().finValidite())) {
					currentObjet().setFinValidite(controleurPeriode.dateFin());
				}
			} else if (key.equals("finValiditeFormatee")) {
				if (!changerPeriodeApresValidation && controleurPeriode.dateFin() != null && 
						(currentObjet().finValidite() == null || DateCtrl.isAfter(currentObjet().finValidite(),controleurPeriode.dateFin()))) {
					currentObjet().setFinValidite(controleurPeriode.dateFin());
				}
				if (currentObjet().finValidite() != null && DateCtrl.isAfter(currentObjet().debutValidite(), currentObjet().finValidite())) {
					currentObjet().setFinValidite(controleurPeriode.dateFin());
				}
			}
			updaterDisplayGroups();
		}
	}
	// Notifications
	public void synchroniserDates(NSNotification aNotif) {
		LogManager.logDetail("GestionPourPrimeAvecDuree - Synchronisation des dates d'affichage");
		if (typeValidite() != null && currentPrime() != null && controleurPeriode != null) {
			if (typeValidite().equals(TYPE_VALIDE)) {
				displayGroup().setObjectArray(ObjetAvecDureeValiditePourPrime.rechercherObjetsValidesPourPrimeEtPeriode(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),currentPrime,controleurPeriode.dateDebut(),controleurPeriode.dateFin()));
			} else {
				displayGroup().setObjectArray(ObjetAvecDureeValiditePourPrime.rechercherObjetsInvalidesPourPrimeEtPeriode(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),currentPrime,controleurPeriode.dateDebut(),controleurPeriode.dateFin()));
			}
		} else {
			displayGroup().setObjectArray(null);
		}
	}
	public void synchroniserPrimes(NSNotification aNotif) {
		LogManager.logDetail("GestionPourPrimeAvecDuree - Synchronisation des primes");
		preparerPrimes();
		if (currentPrime() != null && displayGroupPrimes.displayedObjects().containsObject(currentPrime) == false) {
			displayGroup().setSelectedObject(null);
			displayGroup().setObjectArray(null);
			currentPrime = null;
			updaterDisplayGroups();
		}
	}
	// Méthodes du controller DG
	/** On ne peut choisir une prime dans le popup que si on n'est pas en mode modification */
	public boolean peutChoisirPrime() {
		return !modificationEnCours();
	}
	public boolean peutValider() {
		return super.peutValider() && currentObjet() != null && currentObjet().prime() != null && currentObjet().debutValidite() != null;
	}
	/** On ne peut saisir qu'une fois que la prime a &eacute;t&eacute; choisie */
	public boolean modeSaisiePossible() {
		return super.modeSaisiePossible() && currentPrime() != null && currentPrime().estValide();
	}
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		if (vuePeriode != null) {
			// préparer le contrôleur de période et l'afficher dans la vue des périodes en sélectionnant l'affichage des périodes par défaut
			controleurPeriode = new GestionPeriode(null,null,GestionPeriode.TOUTE_PERIODE);
			controleurPeriode.init();
			GraphicUtilities.swaperView(vuePeriode,controleurPeriode.component());
			displayGroupPrimes.setObjectArray(EOPrime.rechercherPrimesValidesPourPeriode(editingContext(), null,null));
		}
		afficherPrimesValides = true;
	}
	protected void terminer() {
	}
	protected void loadNotifications() {
		super.loadNotifications();
		if (vuePeriode != null) {
			NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserDates", new Class[] {NSNotification.class}),
					GestionPeriode.NOTIFICATION_PERIODE_HAS_CHANGED,null);
		}
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserPrimes", new Class[] {NSNotification.class}),
				AdministrationPrimes.NOTIFICATION_RAFFRAICHIR_PRIME,null);
	}
	protected void traitementsPourCreation() {
		currentObjet().initAvecPrime(currentPrime);
	}
	protected void parametrerDisplayGroup() {
		super.parametrerDisplayGroup();
		displayGroupPrimes.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("primCodeIndemnite", EOSortOrdering.CompareAscending)));
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupPrimes.updateDisplayedObjects();
	}
	/** Pour v&eacute;rifier si les dates saisies sont bien dans la p&eacute;riode affich&eacute;e */

	protected void traitementsApresValidation() {
		if (changerPeriodeApresValidation && controleurPeriode != null) {
			// utilise pour synchroniser l'affichage au cas où la date de début n'est pas dans les dates affichées
			boolean periodeChangee = false;
			String debutPeriode = controleurPeriode.debutPeriode();
			if (debutPeriode == null || (debutPeriode != null && DateCtrl.isAfter(DateCtrl.stringToDate(debutPeriode),currentObjet().debutValidite()))) {
				debutPeriode = DateCtrl.dateToString(currentObjet().debutValidite());
				periodeChangee = true;
			}
			String finPeriode = controleurPeriode.finPeriode();
			if (finPeriode == null || (finPeriode != null && DateCtrl.isBefore(DateCtrl.stringToDate(finPeriode),currentObjet().debutValidite()))) {
				finPeriode = DateCtrl.dateToString(currentObjet().debutValidite());
				periodeChangee = true;
			}
			if (periodeChangee) {
				controleurPeriode.changerDates(debutPeriode,finPeriode);
			}
			super.traitementsApresValidation();
		} else {
			super.traitementsApresValidation();
		}
	}
	protected EOPrime currentPrime() {
		return currentPrime;
	}
	protected void preparerPrimes() {
		LogManager.logDetail("Preparation des primes a afficher");
		displayGroupPrimes.setObjectArray(primesPourPeriode(afficherPrimesValides()));
		displayGroupPrimes.setSelectedObject(null);
		setLibellePrime(null);
	}
	/** Retourne les primes conformes &agrave; la validit&eacute; lorsque les dates sont chang&eacute;es
	 */
	protected NSArray primesPourPeriode(boolean estValide) {
		if (controleurPeriode != null) {
			if (estValide) {
				return EOPrime.rechercherPrimesValidesPourPeriode(editingContext(),controleurPeriode.dateDebut(),controleurPeriode.dateFin());
			} else {
				return EOPrime.rechercherPrimesInvalidesPourPeriode(editingContext(),controleurPeriode.dateDebut(),controleurPeriode.dateFin());
			}
		} else {
			return null;
		}
	}
	/** Retourne les objets conforme &agrave; la validit&eacute; lorsque le popup de validit&eacute; est modifi&eacute;,
	 * null si pas de prime s&eacute;lectionn&eacute;e.
	 */
	protected  NSArray objetsPourValidite(boolean estValide) {
		if (currentPrime() != null && controleurPeriode != null) {
			if (estValide) {
				return ObjetAvecDureeValiditePourPrime.rechercherObjetsValidesPourPrimeEtPeriode(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),currentPrime(),controleurPeriode.dateDebut(),controleurPeriode.dateFin());
			} else {
				return ObjetAvecDureeValiditePourPrime.rechercherObjetsInvalidesPourPrimeEtPeriode(editingContext(), displayGroup().dataSource().classDescriptionForObjects().entityName(),currentPrime(),controleurPeriode.dateDebut(),controleurPeriode.dateFin());
			}
		} else {
			return null;
		}
	}
	protected ObjetAvecDureeValiditePourPrime currentObjet() {
		return (ObjetAvecDureeValiditePourPrime)displayGroup().selectedObject();
	}


}

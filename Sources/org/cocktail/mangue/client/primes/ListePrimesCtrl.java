package org.cocktail.mangue.client.primes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.mangue.client.gui.primes.ListePrimesView;
import org.cocktail.mangue.client.select.IndividuSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageConsultation;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class ListePrimesCtrl extends ModelePageConsultation {

	private static ListePrimesCtrl 	sharedInstance;
	private static Boolean 			MODE_MODAL = Boolean.FALSE;
	private ListePrimesView 		myView;
	private EODisplayGroup 			eod;

	private ListenerPrimes 		listenerDecharge = new ListenerPrimes();

	private EOPrimeAttribution	currentPrime;

	public ListePrimesCtrl(EOEditingContext edc) {

		super(edc);

		eod = new EODisplayGroup();
		myView = new ListePrimesView(null, MODE_MODAL.booleanValue(), eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());

		myView.getBtnRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				actualiser();
			}
		});
		myView.getBtnExporter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exporter();
			}
		});

		CocktailUtilities.initPopupAvecListe(myView.getPopupAnnees(), CocktailConstantes.LISTE_ANNEES_DESC, true);
		myView.getPopupAnnees().setSelectedItem(new Integer(DateCtrl.getYear(new NSTimestamp())));
		
		myView.getMyEOTable().addListener(listenerDecharge);
		myView.getTfFiltreNom().getDocument().addDocumentListener(new ADocumentListener());
		myView.getPopupAnnees().addActionListener(new MyActionListener());

		updateInterface();
		
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ListePrimesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ListePrimesCtrl(editingContext);
		return sharedInstance;
	}

	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			actualiser();
		}
	}
	
	/**
	 * 
	 */
	public void open()	{

		CRICursor.setWaitCursor(myView);
		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);

	}

	

	public EOPrimeAttribution getCurrentPrime() {
		return currentPrime;
	}

	public void setCurrentPrime(EOPrimeAttribution currentPrime) {
		this.currentPrime = currentPrime;
	}

	private Integer getCurrentExercice() {
		if (myView.getPopupAnnees().getSelectedIndex() > 0)
			return (Integer)myView.getPopupAnnees().getSelectedItem();
		
		return null;
	}
	
	public void toFront() {
		myView.toFront();
	}


	/**
	 * 
	 */
	private void rechercher() {

		CRICursor.setWaitCursor(myView);
		eod.setObjectArray(EOPrimeAttribution.findForQualifier(getEdc(), filterQualifier()));
		filter();
		CRICursor.setDefaultCursor(myView);

	}
	
	/**
	 * 
	 */
	private void exporter() {

		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "PRIMES_"+StringCtrl.replace((String)myView.getPopupAnnees().getSelectedItem(), "/","-");
			
			saveDialog.setSelectedFile(new File(nomFichierExport + CocktailConstantes.EXTENSION_CSV));
			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = enteteExport();

				for (EOPrimeAttribution myRecord : new NSArray<EOPrimeAttribution>(EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(), EOPrimeAttribution.SORT_ARRAY_NOM_ASC) ))
					texte += texteExportPourRecord(myRecord) + CocktailExports.SAUT_DE_LIGNE;
				
				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

				CRICursor.setDefaultCursor(myView);

				UtilitairesFichier.openFile(file.getPath());
				
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @return
	 */
	private String enteteExport() {	
		String[] listeEntete = new String[]{"NOM", "PRENOM", "PRIME", "MONTANT"};
		return CocktailExports.getEntete(listeEntete) + CocktailExports.SAUT_DE_LIGNE;
	}

	/**
	 * 
	 * @param record
	 * @return
	 */
	private String texteExportPourRecord(EOPrimeAttribution record)    {

		String texte = "";

		// NOM
		texte += CocktailExports.ajouterChamp(record.individu().nomUsuel());
		// PRENOM
		texte += CocktailExports.ajouterChamp(record.individu().prenom());
		// PRIME
		texte += CocktailExports.ajouterChamp(record.prime().primLibLong());
		// MONTANT
		texte += CocktailExports.ajouterChampNumber(record.pattMontant());

		return texte;
	}

	/**
	 * 
	 */
	private void filter() 	{

		eod.setQualifier(filterQualifier());
		eod.updateDisplayedObjects();

		myView.getMyEOTable().updateData();
		CocktailUtilities.setTextToLabel(myView.getLblNBDecharges(), eod.displayedObjects().count() + " Primes");

		updateInterface();

	}

	private class ListenerPrimes implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			modifier();
		}

		public void onSelectionChanged() {
			setCurrentPrime((EOPrimeAttribution)eod.selectedObject());
			updateInterface();
		}
	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}
		public void insertUpdate(DocumentEvent e) {
			filter();		
		}
		public void removeUpdate(DocumentEvent e) {
			filter();		
		}
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnAjouter().setEnabled(true);
		myView.getBtnModifier().setEnabled(getCurrentPrime() != null);
		myView.getBtnSupprimer().setVisible(getCurrentPrime() != null);
	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		CRICursor.setWaitCursor(myView);
		rechercher();
		CRICursor.setDefaultCursor(myView);

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected EOQualifier filterQualifier() {
		// TODO Auto-generated method stub
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrimeAttribution.PATT_EXERCICE_KEY + " = %@", new NSArray(getCurrentExercice())));

		if (!StringCtrl.chaineVide(myView.getTfFiltreNom().getText()))
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrimeAttribution.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreNom().getText()+"*")));
		
		return new EOAndQualifier(qualifiers);
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		EOIndividu selectedIndividu = IndividuSelectCtrl.sharedInstance(getEdc()).getIndividu();
		if (selectedIndividu != null) {
			EOPrimeAttribution prime = EOPrimeAttribution.creer(getEdc(), selectedIndividu);
			SaisiePrimeAttributionCtrl ctrlPrimes = new SaisiePrimeAttributionCtrl(getEdc());
			ctrlPrimes.modifier(prime, true);
		}
	}

	@Override
	protected void traitementsApresCreation() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub
		SaisiePrimeAttributionCtrl ctrlPrimes = new SaisiePrimeAttributionCtrl(getEdc());
		ctrlPrimes.modifier(getCurrentPrime(), false);
		actualiser();
	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getEdc().deleteObject(getCurrentPrime());
	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		
	}
}

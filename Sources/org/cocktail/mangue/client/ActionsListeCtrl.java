/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.mangue.client;

import javax.swing.JFrame;

import org.cocktail.application.client.tools.ConnectedUsersCtrl;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.administration.GestionAgents;
import org.cocktail.mangue.client.administration.GestionDCP;
import org.cocktail.mangue.client.administration.GestionDestinataires;
import org.cocktail.mangue.client.administration.GestionEmploisType;
import org.cocktail.mangue.client.administration.GestionModelesPourImpressionContrat;
import org.cocktail.mangue.client.administration.GestionModulesImpression;
import org.cocktail.mangue.client.administration.GestionModulesIndividu;
import org.cocktail.mangue.client.administration.GestionServices;
import org.cocktail.mangue.client.administration.ParametresCtrl;
import org.cocktail.mangue.client.administration.visa.GestionVisas;
import org.cocktail.mangue.client.administration.visa.GestionVisasArretesCorps;
import org.cocktail.mangue.client.administration.visa.GestionVisasCongesCarrieres;
import org.cocktail.mangue.client.administration.visa.GestionVisasCongesContractuels;
import org.cocktail.mangue.client.administration.visa.GestionVisasContrats;
import org.cocktail.mangue.client.carrieres.GestionCFA;
import org.cocktail.mangue.client.carrieres.GestionCongeBonifie;
import org.cocktail.mangue.client.carrieres.GestionMAD;
import org.cocktail.mangue.client.cir.CirCtrl;
import org.cocktail.mangue.client.conges.GestionAbsences;
import org.cocktail.mangue.client.decharges.ListeDechargesCtrl;
import org.cocktail.mangue.client.emplois.GestionStocks;
import org.cocktail.mangue.client.extraction.ExtractionEtatComparatif;
import org.cocktail.mangue.client.formations.ListeFormationsCtrl;
import org.cocktail.mangue.client.gpeec.EmploisCtrl;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewAbsences;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewAdresses;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewAnciennete;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewBudget;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewCarrieres;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewContrats;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewEtatCivil;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewFicheSituationAgent;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewHeberge;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewModalites;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewOccupation;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewPasse;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewProlongations;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewSyntheseCarriereCIR;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewVacations;
import org.cocktail.mangue.client.gui.swapviews.GestionSwapViewVisas;
import org.cocktail.mangue.client.heberge.ListeHebergesCtrl;
import org.cocktail.mangue.client.impression.GestionEditionsDivers;
import org.cocktail.mangue.client.individu.GestionEtatCivil;
import org.cocktail.mangue.client.nomenclatures.NomenclaturesCtrl;
import org.cocktail.mangue.client.outils.GestionEtatPersonnels;
import org.cocktail.mangue.client.outils.OutilDif;
import org.cocktail.mangue.client.outils.individu.OutilAnciennete;
import org.cocktail.mangue.client.outils.medical.EditionAcmos;
import org.cocktail.mangue.client.outils.medical.EditionExamens;
import org.cocktail.mangue.client.outils.medical.EditionVaccins;
import org.cocktail.mangue.client.outils.medical.EditionVisites;
import org.cocktail.mangue.client.outils.medical.GestionMedecins;
import org.cocktail.mangue.client.outils.medical.MedicalCtrl;
import org.cocktail.mangue.client.outils.nbi.NbiCtrl;
import org.cocktail.mangue.client.postes.GestionPostes;
import org.cocktail.mangue.client.primes.AdministrationPrimes;
import org.cocktail.mangue.client.primes.GestionExclusionsConge;
import org.cocktail.mangue.client.primes.GestionExclusionsPosition;
import org.cocktail.mangue.client.primes.GestionExclusionsPrime;
import org.cocktail.mangue.client.primes.GestionFonctions;
import org.cocktail.mangue.client.primes.GestionParametres;
import org.cocktail.mangue.client.primes.GestionPersonnalisations;
import org.cocktail.mangue.client.primes.GestionPopulations;
import org.cocktail.mangue.client.primes.GestionPrimes;
import org.cocktail.mangue.client.primes.GestionPrimesIndividu;
import org.cocktail.mangue.client.primes.GestionProgrammes;
import org.cocktail.mangue.client.primes.GestionStructures;
import org.cocktail.mangue.client.primes.ListePrimesCtrl;
import org.cocktail.mangue.client.promouvabilites.PromouvabilitesCtrl;
import org.cocktail.mangue.client.requetes.GestionRequetesActivite;
import org.cocktail.mangue.client.requetes.GestionRequetesAgent;
import org.cocktail.mangue.client.requetes.GestionRequetesEmploi;
import org.cocktail.mangue.client.requetes.InterfaceRequeteCarriere;
import org.cocktail.mangue.client.requetes.InterfaceRequeteContrat;
import org.cocktail.mangue.client.requetes.InterfaceRequeteIndividu;
import org.cocktail.mangue.client.requetes.InterfaceRequeteOccupation;
import org.cocktail.mangue.client.requetes.InterfaceRequeteSpecialisation;
import org.cocktail.mangue.client.supinfo.SupInfoCtrl;
import org.cocktail.mangue.client.vacations.VacationsCtrl;
import org.cocktail.mangue.common.modele.manager.Manager;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class ActionsListeCtrl  {
	
	private Manager manager;
	
	public ActionsListeCtrl (Manager manager) {
		setManager(manager);
	}
	
	public Manager getManager() {
		return manager;
	}
	public void setManager(Manager manager) {
		this.manager = manager;
	}
	private EOEditingContext getEdc() {
		return getManager().getEdc();
	}

	private void afficherInfos(String nomMethode,String nomClasse) {
		((ApplicationClient)ApplicationClient.sharedApplication()).afficherInfos(nomMethode, nomClasse);		
	}
	
	public void modeNormal() {
		preparerLogPourDebug("N");
	}
	public void modeDebug() {
		preparerLogPourDebug("O");
	}
	private void preparerLogPourDebug(String modeDebug) {
		LogManager.changerModeDebug(modeDebug);
	}
	public void afficherParametresMangue() {
		ParametresCtrl.sharedInstance(getManager().getEdc()).open();
	}
	public void afficherConnectedUsers() {
		ConnectedUsersCtrl win = new ConnectedUsersCtrl(getEdc());
		try {
			win.openDialog(new JFrame(), true);
		} catch (Exception e1) {
			EODialogs.runErrorDialog("ERREUR", e1.getMessage());
		}
	}
	public void afficherLogs() {
		new LogsApplication(getManager());
	}
	public void afficherAnciennete() {
		afficherInfos("afficherAnciennete", OutilAnciennete.class.getName());
	}
	public void afficherEtatCivil() {
		afficherInfos("afficherEtatCivil",GestionEtatCivil.class.getName());
	}
	public void afficherConges() {
		afficherInfos("afficherConges",GestionAbsences.class.getName());
	}
	public void afficherPrimesIndividu() {
		afficherInfos("afficherPrimesIndividu",GestionPrimesIndividu.class.getName());
	}
	public void afficherLbudsIndividu() {
		afficherSwapViewBudget();
	}
	public void afficherCFA() {
		afficherInfos("afficherCFA",GestionCFA.class.getName());
	}
	public void afficherCongeNbi() {
		afficherInfos("afficherCongeNbi",GestionCongeBonifie.class.getName());
	}
	public void afficherMAD() {
		afficherInfos("afficherMAD",GestionMAD.class.getName());
	}
	public void afficherPromotionsIndividu() {
		afficherInfos("afficherPromotionsIndividu","org.cocktail.mangue.client.carrieres.GestionPromotionIndividu");
	}
	public void afficherOccupations() {
		afficherInfos("afficherOccupations","org.cocktail.mangue.client.occupations.GestionOccupation");
	}
	public void afficherSituationGeographique() {
		afficherInfos("afficherSituationGeographique","org.cocktail.mangue.client.occupations.GestionSituationGeo");
	}
//	public void afficherAnciennete() {
//		afficherInfos("afficherAnciennete",OutilAnciennete.class.getName());
//	}
	public void afficherSynthese() {
		afficherInfos("afficherSynthese","org.cocktail.mangue.client.outils.individu.SyntheseCarriere");
	}
	public void afficherHeberges() {
		ListeHebergesCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherDecharges() {
		ListeDechargesCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherListePrimes() {
		ListePrimesCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherFormations() {
		ListeFormationsCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherVacations() {
		VacationsCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherEmplois() {
		EmploisCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherStocks() {
		afficherInfos("afficherStocks",GestionStocks.class.getName());
	}
	public void afficherEtatPersonnels() {
		afficherInfos("afficherEtatPersonnels",GestionEtatPersonnels.class.getName());
	}
	public void afficherCifs() {
		afficherInfos("afficherCifs", OutilDif.class.getName());
	}
	public void afficherMedecins() {
		afficherInfos("afficherMedecins", GestionMedecins.class.getName());
	}
	public void afficherResponsablesAcmo() {
		afficherInfos("afficherResponsablesAcmo", EditionAcmos.class.getName());
	}
	public void afficherEditionExamens() {
		afficherInfos("afficherEditionExamens", EditionExamens.class.getName());
	}
	public void afficherEditionVaccins() {
		afficherInfos("afficherEditionVaccins", EditionVaccins.class.getName());
	}
	public void afficherEditionsVisites() {
		afficherInfos("afficherEditionsVisites", EditionVisites.class.getName());
	}
	public void afficherEditeurRequetes() {
		afficherInfos("afficherEditeurRequetes",GestionRequetesAgent.class.getName());
	}
	public void afficherEditionAgentsIdentite() {
		afficherInfos("afficherEditionAgentsIdentite",InterfaceRequeteIndividu.class.getName());
	}
	public void afficherEditionAgentsContrat() {
		afficherInfos("afficherEditionAgentsContrat",InterfaceRequeteContrat.class.getName());
	}
	public void afficherEditionAgentsCarriere() {
		afficherInfos("afficherEditionAgentsCarriere",InterfaceRequeteCarriere.class.getName());
	}
	public void afficherEditionAgentsOccupation() {
		afficherInfos("afficherEditionAgentsOccupation",InterfaceRequeteOccupation.class.getName());
	}
	public void afficherEditionAgentsSpecialisation() {
		afficherInfos("afficherEditionAgentsSpecialisation",InterfaceRequeteSpecialisation.class.getName());
	}
	public void afficherEditionConges() {
		afficherInfos("afficherEditionConges", "org.cocktail.mangue.client.requetes.GestionRequetesConge");
	}
	public void afficherEditionActivites() {
		afficherInfos("afficherEditionActivites", GestionRequetesActivite.class.getName());
	}
	public void afficherEditionServices() {
		afficherInfos("afficherEditionServices", GestionRequetesEmploi.class.getName());
	}
	public void afficherEditionDivers() {
		afficherInfos("afficherEditionDivers", GestionEditionsDivers.class.getName());
	}
	public void afficherPostes() {
		afficherInfos("afficherPostes",GestionPostes.class.getName());
	}
	public void afficherExtractionEtatComparatif() {
		afficherInfos("afficherExtractionEtatComparatif",ExtractionEtatComparatif.class.getName());
	}
	public void afficherAdministrationPrimes() {
		afficherInfos("afficherAdministrationPrimes",AdministrationPrimes.class.getName());
	}
	public void afficherPrimeExclusions() {
		afficherInfos("afficherPrimeExclusions",GestionExclusionsPrime.class.getName());
	}
	public void afficherPrimeExclusionsPosition() {
		afficherInfos("afficherPrimeExclusionsPosition",GestionExclusionsPosition.class.getName());

	}
	public void afficherPrimeExclusionsConge() {
		afficherInfos("afficherPrimeExclusionsConge",GestionExclusionsConge.class.getName());
	}
	public void afficherPrimeFonctions() {
		afficherInfos("afficherPrimeFonctions",GestionFonctions.class.getName());
	}
	public void afficherPrimePopulations() {
		afficherInfos("afficherPrimePopulations",GestionPopulations.class.getName());
	}
	public void afficherPrimeParametres() {
		afficherInfos("afficherPrimeParametres",GestionParametres.class.getName());
	}
	public void afficherPrimeProgrammes() {
		afficherInfos("afficherPrimeProgrammes",GestionProgrammes.class.getName());
	}
	public void afficherPrimeStructures() {
		afficherInfos("afficherPrimeStructures",GestionStructures.class.getName());
	}
	public void afficherPrimes() {
		afficherInfos("afficherPrimes",GestionPrimes.class.getName());
	}

	public void afficherArretesPrime() {
		afficherInfos("afficherArretesPrime",GestionPersonnalisations.class.getName());
	}
	public void afficherAgents() {
		afficherInfos("afficherAgents",GestionAgents.class.getName());
	}
	public void afficherServices() {
		afficherInfos("afficherServices",GestionServices.class.getName());
	}
	public void afficherDestinataires() {
		afficherInfos("afficherDestinataires",GestionDestinataires.class.getName());
	}
	public void afficherModulesImpression() {
		afficherInfos("afficherModulesImpression",GestionModulesImpression.class.getName());
	}
	public void afficherModulesImpressionIndividu() {
		afficherInfos("afficherModulesImpressionIndividu",GestionModulesIndividu.class.getName());
	}
	public void afficherModelesPourContrat() {
		afficherInfos("afficherModelesPourContrat",GestionModelesPourImpressionContrat.class.getName());
	}
	public void afficherDCP() {
		afficherInfos("afficherDCP",GestionDCP.class.getName());
	}
	public void afficherEmploisType() {
		afficherInfos("afficherEmploisType",GestionEmploisType.class.getName());
	}
	public void afficherFamillesProfessionnelles() {
		afficherInfos("afficherFamillesProfessionnelles","org.cocktail.mangue.client.administration.GestionFamillesProfessionnelles");
	}
	public void afficherHierarchie() {
		afficherInfos("afficherHierarchie","org.cocktail.mangue.client.administration.GestionHierarchie");
	}
	public void afficherParametresPromotion() {
		afficherInfos("afficherParametresPromotion","org.cocktail.mangue.client.administration.GestionParametresPromotion");
	}
	public void afficherVisas() {
		afficherInfos("afficherVisas", GestionVisas.class.getName());
	}
	
	/* GESTION DES VISAS */
	
	public void afficherVisasCongesTitulaires() {
		afficherInfos("afficherVisasCongesTitulaires",GestionVisasCongesCarrieres.class.getName());
	}
	public void afficherVisasCongesContractuels() {
		afficherInfos("afficherVisasCongesContractuels",GestionVisasCongesContractuels.class.getName());
	}
	
	public void afficherVisasArretesCorps() {
		afficherInfos("afficherVisasArretesCorps",GestionVisasArretesCorps.class.getName());
	}
	public void afficherVisasContrats() {
		afficherInfos("afficherVisasContrats",GestionVisasContrats.class.getName());
	}
	
	/* GESTION DES ELECTIONS */
	
	public void afficherTypeElection() {
		afficherInfos("afficherTypeElection","org.cocktail.mangue.client.elections.nomenclature.GestionTypesElection");
	}
	public void afficherElection() {
		afficherInfos("afficherElection","org.cocktail.mangue.client.elections.nomenclature.GestionElections");
	}
	public void afficherCollege() {
		afficherInfos("afficherCollege","org.cocktail.mangue.client.elections.nomenclature.GestionColleges");
	}
	public void afficherComposante() {
		afficherInfos("afficherComposante","org.cocktail.mangue.client.elections.nomenclature.GestionComposantes");
	}
	public void afficherSection() {
		afficherInfos("afficherSection","org.cocktail.mangue.client.elections.nomenclature.GestionSections");
	}
	public void afficherCollegeSection() {
		afficherInfos("afficherCollegeSection","org.cocktail.mangue.client.elections.nomenclature.GestionCollegeSections");
	}
	public void afficherSecteur() {
		afficherInfos("afficherSecteur","org.cocktail.mangue.client.elections.nomenclature.GestionSecteurs");
	}
	public void afficherBureau() {
		afficherInfos("afficherBureau","org.cocktail.mangue.client.elections.nomenclature.GestionBureauxVote");
	}
	public void afficherExclusion() {
		afficherInfos("afficherExclusion","org.cocktail.mangue.client.elections.nomenclature.GestionExclusions");
	}
	public void afficherInclusionCorps() {
		afficherInfos("afficherInclusionCorps","org.cocktail.mangue.client.elections.nomenclature.GestionInclusionsCorps");
	}
	public void afficherInclusionTypeContrat() {
		afficherInfos("afficherInclusionTypeContrat","org.cocktail.mangue.client.elections.nomenclature.GestionInclusionsTypeContrat");
	}
	public void afficherInclusionDiplome() {
		afficherInfos("afficherInclusionDiplome","org.cocktail.mangue.client.elections.nomenclature.GestionInclusionsDiplome");
	}
	public void afficherParametrageElection() {
		afficherInfos("afficherParametrageElection","org.cocktail.mangue.client.elections.parametrage.GestionParametrages");
	}
	public void afficherListesElectorales() {
		afficherInfos("afficherListesElectorales","org.cocktail.mangue.client.elections.GestionListesElectorales");
	}


	// Gestion des swapviews
	public void afficherSwapViewContrats() {
		afficherInfos("afficherSwapViewContrats",GestionSwapViewContrats.class.getName());
	}
	public void afficherSwapViewBudget() {
		afficherInfos("afficherSwapViewBudget",GestionSwapViewBudget.class.getName());
	}
	public void afficherSwapViewSyntheses() {
		afficherInfos("afficherSwapViewSyntheses",GestionSwapViewAnciennete.class.getName());
	}
	public void afficherSwapViewHeberge() {
		afficherInfos("afficherSwapViewHeberge",GestionSwapViewHeberge.class.getName());
	}
	public void afficherSwapViewCarriere() {
		afficherInfos("afficherSwapViewCarriere",GestionSwapViewCarrieres.class.getName());
	}
	public void afficherSwapViewModalites() {
		afficherInfos("afficherSwapViewModalites",GestionSwapViewModalites.class.getName());
	}
	public void afficherSwapViewProlongations() {
		afficherInfos("afficherSwapViewProlongations",GestionSwapViewProlongations.class.getName());
	}
	public void afficherSwapViewPasse() {
		afficherInfos("afficherSwapViewPasse",GestionSwapViewPasse.class.getName());
	}
	public void afficherSwapViewOccupations() {
		afficherInfos("afficherSwapViewOccupations",GestionSwapViewOccupation.class.getName());
	}
	public void afficherSwapViewAffOcc() {
		afficherInfos("afficherSwapViewAffOcc",GestionSwapViewOccupation.class.getName());
	}
	public void afficherSwapViewVisas() {
		afficherInfos("afficherSwapViewVisas",GestionSwapViewVisas.class.getName());
	}
	public void afficherSwapViewEtatCivil() {
		afficherInfos("afficherSwapViewEtatCivil",GestionSwapViewEtatCivil.class.getName());
	}
	public void afficherSwapViewAbsences() {
		afficherInfos("afficherSwapViewAbsences",GestionSwapViewAbsences.class.getName());
	}
	public void afficherSwapViewAdresses() {
		afficherInfos("afficherSwapViewAdresses",GestionSwapViewAdresses.class.getName());
	}
	public void afficherSwapViewFicheSituationAgent() {
		afficherInfos("afficherSwapViewFicheSituationAgent",GestionSwapViewFicheSituationAgent.class.getName());
	}

	public void afficherFicheSituationAgent() {
		afficherInfos("afficherFicheSituationAgent",GestionSwapViewFicheSituationAgent.class.getName());
	}

	public void afficherSwapViewSyntheseCarriereCIR() {
		afficherInfos("afficherSwapViewSyntheseCarriereCIR",GestionSwapViewSyntheseCarriereCIR.class.getName());
	}

	public void afficherEditionCIR() {
		afficherInfos("afficherSwapViewRequeteCIR",GestionSwapViewAnciennete.class.getName());
	}
	public void afficherSwapViewVacations() {
		afficherInfos("afficherSwapViewVacations",GestionSwapViewVacations.class.getName());
	}
	public void afficherNomenclatures() {
		NomenclaturesCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherPromouvabilites() {	
		PromouvabilitesCtrl.sharedInstance().open();
	}
	public void afficherNbis() {	
		NbiCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherDonneesMedicales() {	
		MedicalCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherCIR() {	
		CirCtrl.sharedInstance(getEdc()).open();
	}
	public void afficherSupInfo() {	
		SupInfoCtrl.sharedInstance(getEdc()).open();	
	}
	public void afficherEvalEC() {	
		EODialogs.runInformationDialog("INFO","Ce menu sera disponible lors de la diffusion " +
				"de la note officielle concernant les évaluations des enseignants chercheurs.");
		//SupInfoEvalECCtrl.sharedInstance(editingContext).open();
	}
	    
}

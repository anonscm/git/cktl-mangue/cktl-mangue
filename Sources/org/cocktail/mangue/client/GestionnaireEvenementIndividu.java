/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client;

import java.util.Enumeration;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.IDureePourIndividu;
import org.cocktail.mangue.modele.grhum.elections.EOCritereExclusion;
import org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Comporte des methodes de classe pour gerer la creation/modification/suppressions d'evenements lies a un
 * individu (classe EOAbsences) */
public class GestionnaireEvenementIndividu {
	/** Cree un evenement pour une duree.  Si le type d'absence associe a cet evenement n'est pas defini,
	 * l'evenement n'est pas cree et retoure null<BR>
	 *  Genere une exception si la duree passee en parametre n'est pas un
	 * EOGenericRecord, qu'il n'y a pas de table associee a cette duree
	 * @param duree
	 * @param insertDansEditingContext true si l'absence doit etre inseree dans l'editing context
	 * @return absence cree et inseree dans l'editing context
	 * @throws Exception
	 */
	public static EOAbsences creerEvenementPourIndividu(IDureePourIndividu duree, boolean insertDansEditingContext) throws Exception {
		// pour déterminer le type de l'absence, on récupère le nom d'entité de la durée passée en paramètre puis on recherche
		// le nom de la table associée et enfin on peut rechercher le type d'absence
		if (duree instanceof EOGenericRecord) {
			EOEditingContext editingContext = ((EOGenericRecord)duree).editingContext();
			EOTypeAbsence typeAbsence = rechercherTypeEvenement(editingContext,duree);
			if (typeAbsence == null) 
				return null;
			
			EOAbsences absence = new EOAbsences();
			absence.initAvecTypeEtIndividu(typeAbsence,duree.individu());
			absence.setDateDebut(duree.dateDebut());
			absence.setDCreation(new NSTimestamp());
			absence.setDModification(new NSTimestamp());
			absence.setDateFin(duree.dateFin());
			if (insertDansEditingContext)
				editingContext.insertObject(absence);

			return absence;
		} else throw new Exception("Cast Exception, on attend un EOGenericRecord pour la durée");
	}
	/** recherche un evenement associe a une duree. 
	 *	Genere une exception si la duree passee en parametre n'est pas un
	 * EOGenericRecord, qu'il n'y a pas de table associee a cette duree 
	 * @return null si l'absence n'est pas trouvee */
	public static EOAbsences rechercherEvenementPourDuree(IDureePourIndividu duree) throws Exception {
		if (duree instanceof EOGenericRecord) {
			EOEditingContext editingContext = ((EOGenericRecord)duree).editingContext();
			EOTypeAbsence typeAbsence = rechercherTypeEvenement(editingContext,duree);
			if (typeAbsence == null) {
				return null;
			}
			return EOAbsences.rechercherAbsencePourIndividuDateEtTypeAbsence(editingContext,duree.individu(),duree.dateDebut(),duree.dateFin(),typeAbsence);
		} else throw new Exception("Cast Exception, on attend un EOGenericRecord pour la durée");
	}
	
	/** Modifie l'evenement associe a une duree ainsi que sa duree en jours 
	 * (float car on peut avoir une duree comportant des demi-journees 
	 * Evalue si cela s'applique le type d'exclusion. 
	 * Lance une exception si la duree passee en parametre n'est pas un
	 * EOGenericRecord ou qu'il y a un probleme sur les criteres d'exclusion
	 * */
	public static void modifierEvenementAvecDuree(EOAbsences evenement,IDureePourIndividu duree,float nbJours) throws Exception {
		if (evenement == null) {
			return;
		}
		modifierEvenementAvecDuree(evenement,duree);
		evenement.modifierDureeAbsence(nbJours);
		// modifier le type d'exclusion si nécessaire
		NSArray criteresExclusion = EOCritereExclusion.rechercherCriteresPourTypeAbsence(evenement.editingContext(),evenement.toTypeAbsence());
		EOTypeExclusion typeCourant = null,typeFinal = null;
		// les critères d'exclusion sont associés au type d'exclusion. Pour un même type d'exclusion, il peut y avoir plusieurs
		// critères. On fait un ET logique sur l'ensemble des critères pour un type d'exclusion donné
		for (Enumeration<EOCritereExclusion> e = criteresExclusion.objectEnumerator();e.hasMoreElements();) {
			EOCritereExclusion critere = e.nextElement();
			if (typeCourant == null || critere.typeExclusion() != typeCourant) {
				// on change de type d'exclusion
				if (typeFinal != null) {
					break;	// on a rencontré une série de critères pour le type courant qui étaient satisfaits
				}
				typeCourant = critere.typeExclusion();
			}
			if (dureeSupporteCritere(duree,critere)) {
				if (typeFinal != typeCourant) {
					typeFinal = typeCourant;
				}
			} else {		// critère non satisfait, on remet à null
				typeFinal = null;
			}
		}
		if (evenement.typeExclusion() != typeFinal) {
			if (typeFinal != null) {
				evenement.addObjectToBothSidesOfRelationshipWithKey(typeFinal,"typeExclusion");
			} else {
				// invalider le type d'exclusion précédent et remettre à nul
				if (evenement.typeExclusion() != null) {
					evenement.removeObjectFromBothSidesOfRelationshipWithKey(evenement.typeExclusion(),"typeExclusion");
				}
			}
		}
	}
	/** Supprime un evenement
	 * @param duree
	 * @param deleter true si on doit le supprimer de la base */
	public static void supprimerEvenement(EOAbsences evenement,boolean deleter) throws Exception {
		if (evenement != null) {
			evenement.setEstValide(false);
			if (deleter) {
				evenement.supprimerRelations();
				evenement.editingContext().deleteObject(evenement);
			}
		}
	}
	/** Supprime un evenement associee a une duree
	 * @param duree
	 * @param deleter true si on doit le supprimerde la base */
	public static void supprimerEvenementPourDuree(IDureePourIndividu duree,boolean deleter) throws Exception {
		EOAbsences evenement = rechercherEvenementPourDuree(duree);
		if (evenement != null) {
			evenement.setEstValide(false);
			if (deleter) {
				evenement.supprimerRelations();
				evenement.editingContext().deleteObject(evenement);
			}
		}
	}
	/** Recherche le type d'absence lie a un evenement */
	public static EOTypeAbsence rechercherTypeEvenement(EOEditingContext editingContext,IDureePourIndividu duree) throws Exception {
		String nomEntite = ((EOGenericRecord)duree).entityName();
		EODistributedObjectStore store = (EODistributedObjectStore)editingContext.parentObjectStore();
		String nomTable = (String)store.invokeRemoteMethodWithKeyPath(editingContext,"session","clientSideRequestRecupererNomTable",new Class[]{String.class},new Object[] {nomEntite},false);
		if (nomTable == null) {
			throw new Exception("Table inconnue pour la durée de type " + nomEntite);
		}
		int lg = nomTable.indexOf(".");
		if (lg > 0) {
			// Contient le nom du user
			nomTable = nomTable.substring(lg + 1);
		}
		if (duree.supportePlusieursTypesEvenement()) {
			if (duree.typeEvenement() == null) {
				throw new Exception("Plusieurs types d'absences pour cette entité, veuillez fournir le type d'événement");
			}
		}
		return EOTypeAbsence.rechercherTypeAbsencePourTable(editingContext,nomTable,duree.typeEvenement());
	}
	// méthodes privées
	/** Modifie l'evenement associe a une duree */
	private static void modifierEvenementAvecDuree(EOAbsences evenement,IDureePourIndividu duree) {
		if (evenement == null) {
			return;
		}
		
		if (evenement.dateDebut() == null || duree.dateDebut() == null || DateCtrl.dateToString(evenement.dateDebut()).equals(DateCtrl.dateToString(duree.dateDebut())) == false) {
			evenement.setDateDebut(duree.dateDebut());
		}
		if (evenement.dateFin() == null || duree.dateFin() == null || DateCtrl.dateToString(evenement.dateFin()).equals(DateCtrl.dateToString(duree.dateFin())) == false) {
			evenement.setDateFin(duree.dateFin());
		}
	}
	
	private static boolean dureeSupporteCritere(IDureePourIndividu duree,EOCritereExclusion critere) throws Exception {
		if (critere.concerneToutAgent()) {
			return true;
		}
		if (!(duree instanceof EOGenericRecord)) {
			throw new Exception("Cast Exception, on attend un EOGenericRecord pour la durée");
		}
		String attribut = critere.critereAttribut();
		Object valeurAttribut = null;
		if (attribut.indexOf(".") == -1) {
			valeurAttribut = ((EOGenericRecord)duree).valueForKey(attribut);
		} else {
			valeurAttribut = ((EOGenericRecord)duree).valueForKeyPath(attribut);
		}
		if (valeurAttribut == null) {
				throw new Exception("l n'y a pas de valeur pour l'attribut " + attribut);
		}
		if (critere.critereTypeValeur().equals(EOCritereExclusion.TYPE_STRING)) {
			if (!(valeurAttribut instanceof String)) {
				throw new Exception("Pour l'attribut "+ attribut + "on attend une chaîne de caractères");
			}
			return (critere.critereOperateur().equals("=") && valeurAttribut.equals(critere.critereValeur())) ||
				(critere.critereOperateur().equals("!=") && valeurAttribut.equals(critere.critereValeur()) == false);
		} else if (critere.critereTypeValeur().equals(EOCritereExclusion.TYPE_ENTIER)) {
			if (!(valeurAttribut instanceof Number)) {
				throw new Exception("Pour l'attribut "+ attribut + "on attend un entier");
			}
			int valeurReelle = ((Number)valeurAttribut).intValue();
			int valeur = new Integer(critere.critereValeur()).intValue();
			return ((critere.critereOperateur().equals("=") && valeurReelle == valeur) ||
					(critere.critereOperateur().equals("!=") && valeurReelle != valeur) ||
					(critere.critereOperateur().equals(">") && valeurReelle > valeur) ||
					(critere.critereOperateur().equals(">=") && valeurReelle >= valeur) ||
					(critere.critereOperateur().equals("<") && valeurReelle < valeur) ||
					(critere.critereOperateur().equals("<=") && valeurReelle <= valeur));
		} else if (critere.critereTypeValeur().equals(EOCritereExclusion.TYPE_FLOAT)) {
			if (!(valeurAttribut instanceof Number)) {
				throw new Exception("Pour l'attribut "+ attribut + "on attend un float");
			}
			float valeurReelle = ((Number)valeurAttribut).floatValue();
			float valeur = new Float(critere.critereValeur()).floatValue();
			return ((critere.critereOperateur().equals("=") && valeurReelle == valeur) ||
					(critere.critereOperateur().equals("!=") && valeurReelle != valeur) ||
					(critere.critereOperateur().equals(">") && valeurReelle > valeur) ||
					(critere.critereOperateur().equals(">=") && valeurReelle >= valeur) ||
					(critere.critereOperateur().equals("<") && valeurReelle < valeur) ||
					(critere.critereOperateur().equals("<=") && valeurReelle <= valeur));
		}
		return false;
	}
}

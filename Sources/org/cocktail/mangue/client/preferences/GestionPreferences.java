/*
 * Created on 14 mars 2006
 *
 * Gestion des préférences utilisateur
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.preferences;

import java.io.File;
import java.io.FilenameFilter;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import org.cocktail.client.components.DialogueSimpleAvecStrings;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.onglets.NomenclatureOngletMenu;
import org.cocktail.mangue.client.outils_interface.utilitaires.CategoriePersonnelPourAffichage;
import org.cocktail.mangue.client.select.StructureArbreSelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EOAgentsDroitsServices;
import org.cocktail.mangue.modele.mangue.EOPrefsPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EOAssociation;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

/** Gestion des preferences utilisateur */

public class GestionPreferences extends ModelePageComplete  implements FilenameFilter {
	public EODisplayGroup displayGroupTypePopulation;
	public JComboBox popupCategoriePersonnel;
	private EOAgentPersonnel currentAgent;
	private EOGlobalID preferencesID,agentID;
	private String categoriePersonnel;
	private boolean ancienModeAffichage,peutAfficherPhotos,demarrage;
	/** Notification envoyee apres validations si changement de preferences */
	public static String PREFERENCES_CHANGEES = "PreferencesChangees";
	
	public GestionPreferences(EOGlobalID preferencesID,EOGlobalID agentID) {
		super();
		this.preferencesID = preferencesID;
		this.agentID = agentID;
		categoriePersonnel =  null;
		demarrage = true;
	}
	// Accesseurs
	public String categoriePersonnel() {
		return categoriePersonnel;
	}
	public void setCategoriePersonnel(String aStr) {
		if (demarrage) {
			return;
		}
		if (aStr != null && aStr.length() > 0 && currentPreferences() != null) {
			int categorie = CategoriePersonnelPourAffichage.categoriePersonnelPour(aStr);
			if (currentPreferences().typePersonnelAuDemarrage() == null ||
				currentPreferences().typePersonnelAuDemarrage().intValue() != categorie) {
				currentPreferences().setTypePersonnelAuDemarrage(new Integer(categorie));
				categoriePersonnel = aStr;
			}
		}
	}

	/**
	 * 
	 */
	 public void connectionWasBroken() {	
		 for (EOAssociation association : (NSArray<EOAssociation>)displayGroup().observingAssociations()) {
	    	 association.breakConnection();
	     }
	     super.connectionWasBroken();
	     
	}
	// actions
	public void annuler() {
		boolean continuer = true;
		if (isEdited()) {
			continuer = EODialogs.runConfirmOperationDialog("Alerte","Voulez-vous abandonner les modifications en cours  ?","Oui","Non");
		}
		if (continuer)
			super.annuler();
			((EOFrameController)supercontroller()).closeWindow();
	}
	public void afficherStructure() {
		if (currentAgent.gereTouteStructure()) {
			EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(editingContext());
			if (structure != null) {
				currentPreferences().addObjectToBothSidesOfRelationshipWithKey(structure,"structure");
				LogManager.logDetail("GestionPreferences - afficherStructure structure choisie : " + structure.llStructure());
			}
			updaterDisplayGroups();
		} else {
			NSArray structures = currentAgent.structuresGerees();
			if (structures.count() > 1) {
				// permettre la sélection parmi les structures autorisées à l'agent. Autoriser la sélection multiple
			UtilitairesDialogue.afficherDialogue(this,"AgentsDroitsServices", "getStructure",false,true,EOQualifier.qualifierWithQualifierFormat("toIndividu = %@ AND toStructure.toRepartTypeGroupe.tgrpCode = 'S'", new NSArray(currentAgent.toIndividu())),true,true);
			}
			else if (structures.count() == 1) {
				currentPreferences().addObjectToBothSidesOfRelationshipWithKey((EOStructure)structures.objectAtIndex(0), "structure");
				LogManager.logDetail("GestionPreferences - afficherStructure structure obligatoire : " + currentPreferences().structure().llStructure());
				updaterDisplayGroups();
			}
		}
	}
	public void afficherTypeAbsence() {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("congeLegal = 'O'",null);
		UtilitairesDialogue.afficherDialogue(this, EOTypeAbsence.ENTITY_NAME, "getTypeAbsence", false, qualifier,true);	
	}
	public void afficherVueDemarrage() {
		DialogueSimpleAvecStrings controleur = new DialogueSimpleAvecStrings("SelectionVueDemarrage", "Afficher au démarrage",false,false,NomenclatureOngletMenu.titresOngletsPourAgent(agentID));
		controleur.init();
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getVueDemarrage", new Class[] {NSNotification.class}),"SelectionVueDemarrage",null);
		controleur.afficherFenetre();
	}
	public void ajouterTypesPopulation() {
		NSMutableArray qualifiers = new NSMutableArray();

		if (EOGrhumParametres.isGestionEns() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("code != 'N'", null));
		}

		if (EOGrhumParametres.isGestionHu() == false) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temHospitalier = 'N'", null));
		}
		UtilitairesDialogue.afficherDialogue(this,"TypePopulation","getTypePopulation",false,true,new EOAndQualifier(qualifiers),false,false);	 // avec sélection multiple
	}
	public void supprimerStructure() {
		LogManager.logDetail("GestionPreferences - supprimerStructure");
		currentPreferences().removeObjectFromBothSidesOfRelationshipWithKey(currentPreferences().structure(),"structure");
	}
	public void supprimerTypeAbsence() {
		LogManager.logDetail("GestionPreferences - supprimerTypeAbsence");
		currentPreferences().removeObjectFromBothSidesOfRelationshipWithKey(currentPreferences().typeAbsence(),"typeAbsence");
	}
	public void supprimerVueDemarrage() {
		LogManager.logDetail("GestionPreferences - supprimerVueDemarrage");
		currentPreferences().setOngletParDefaut(null);
	}
	public void supprimerTypesPopulation() {
		LogManager.logDetail("GestionPreferences - supprimerTypesPopulation : " + displayGroupTypePopulation.selectedObjects());
		currentPreferences().supprimerTypesPopulations(displayGroupTypePopulation.selectedObjects());
		displayGroupTypePopulation.setObjectArray(currentPreferences().populationsGerees());
		displayGroupTypePopulation.updateDisplayedObjects();
	}
	
	
	public void choisirDirectory() {
		 JFileChooser chooser = new JFileChooser();
		 chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		 chooser.setMultiSelectionEnabled(false);
		 int returnVal = chooser.showOpenDialog(((EOFrameController)supercontroller()).window());
		 if (returnVal == JFileChooser.APPROVE_OPTION)
		      currentPreferences().setDirectoryImpression(chooser.getSelectedFile().getPath());
	}
	public void choisirDirectoryExports() {
		 JFileChooser chooser = new JFileChooser();
		 chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		 chooser.setMultiSelectionEnabled(false);
		 int returnVal = chooser.showOpenDialog(((EOFrameController)supercontroller()).window());
		 if (returnVal == JFileChooser.APPROVE_OPTION)
		      currentPreferences().setDirectoryExports(chooser.getSelectedFile().getPath());
	}
	public void supprimerDirectoryImpression() {
		 currentPreferences().setDirectoryImpression(((ApplicationClient)EOApplication.sharedApplication()).directoryImpressionParDefaut());
	}
	public void supprimerDirectoryExports() {
		 currentPreferences().setDirectoryExports(((ApplicationClient)EOApplication.sharedApplication()).directoryImpressionParDefaut());
	}
	
	
	public void supprimerFichiersImpression() {
		if (EODialogs.runConfirmOperationDialog("Attention","Voulez-vous vraiment supprimer les fichiers du répertoire d'impression ?", "Oui", "Non")) {
			File[] fichiers = fichiersImpression();
			for (int i = 0; i < fichiers.length;i++) {
				File fichier = fichiers[i];
				System.out
				.println("GestionPreferences.supprimerFichiersImpression() SUPPRESSION FICHIER : " +  fichier.getPath());
				fichier.delete();
			}
			controllerDisplayGroup().redisplay();
		}
	}
	// Notifications
	public void getVueDemarrage(NSNotification aNotif) {
		if (aNotif.object() != null) {
			LogManager.logDetail("GestionPreferences - getVueDemarrage : " + aNotif.object());
			currentPreferences().setOngletParDefaut((String)aNotif.object());
		}
	}
	public void getTypeAbsence(NSNotification aNotif) {
		ajouterRelation(currentPreferences(),aNotif.object(),"typeAbsence");
		updaterDisplayGroups();
	}
	public void getTypePopulation(NSNotification aNotif) {
		if (aNotif.object() != null && aNotif.object() instanceof NSArray) {	// tableau de global ids
			NSMutableArray types = new NSMutableArray();
			java.util.Enumeration e = ((NSArray)aNotif.object()).objectEnumerator();
			while (e.hasMoreElements()) {
				EOGlobalID globalID = (EOGlobalID)e.nextElement();
				types.addObject(SuperFinder.objetForGlobalIDDansEditingContext(globalID,editingContext()));
			}
			currentPreferences().ajouterTypesPopulations(types);
			LogManager.logDetail("GestionPreferences - getTypePopulation types : " + types);
			displayGroupTypePopulation.setObjectArray(currentPreferences().populationsGerees());
			updaterDisplayGroups();
		}
	}
	// Récupération de la structure sélectionnée par un agent lorsque celui-ci a accès à un nombre restreint de structure
	public void getStructure(NSNotification aNotif) {
		if (aNotif.object() != null) {
			EOAgentsDroitsServices droitService = (EOAgentsDroitsServices)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(), editingContext());
			currentPreferences().addObjectToBothSidesOfRelationshipWithKey(droitService.toStructure(),"structure");
			LogManager.logDetail("GestionPreferences - getStructure structure : " + droitService.toStructure().llStructure());
			updaterDisplayGroups();
		}
	}
	// Interface FileNameFilter
	public boolean accept(File dir, String name) {
		return name.indexOf(".pdf") > 0 || name.indexOf(".PDF") > 0;
	}
	// Méthodes du controller DG
	public boolean peutSupprimerVueDemarrage() {
		if (modificationEnCours()) {
			return currentPreferences() != null && currentPreferences().ongletParDefaut() != null;
		} else {
			return false;
		}
	}
	public boolean peutSupprimerStructure() {
		if (modificationEnCours()) {
			return currentPreferences() != null && currentPreferences().structure() != null;
		} else {
			return false;
		}
	}
	public boolean peutSelectionnerTypePersonnel() {
		if (modificationEnCours()) {
			if (currentPreferences() != null && currentPreferences().afficherAgentsAuDemarrage() == true) {
				return currentAgent.gerePlusieursCategoriesPersonnel();
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public boolean peutSelectionnerTypeAdresse() {
		if (modificationEnCours()) {
			if (currentPreferences() != null) {
				return currentAgent.peutAfficherInfosPerso();
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public boolean peutSelectionnerAffichagePhoto() {
		return modificationEnCours() && peutAfficherPhotos;
	}
	public boolean peutSupprimerTypeAbsence() {
		if (modificationEnCours()) {
			return currentPreferences() != null && currentPreferences().typeAbsence() != null;
		} else {
			return false;
		}
	}
	public boolean peutExclureHeberges() {
		return modificationEnCours() && currentAgent.gereHeberges();
	}
	public boolean peutSupprimerTypePopulation() {
		return modificationEnCours() && displayGroupTypePopulation.selectedObject() != null;
	}
	public boolean peutAfficherEmplois() {
		return modificationEnCours() && (currentAgent.peutAfficherEmplois() || currentAgent.peutGererEmplois());
	}

	public boolean peutSelectionnerTypeEmploi() {
		return modificationEnCours() && currentPreferences().afficherEmplois();
	}
	public boolean peutAfficherStructures() {
		return modificationEnCours() && (currentAgent.gereTouteStructure() || currentAgent.structuresGerees().count() > 1);
	}
	public boolean peutSupprimerFichiersImpression() {
		File[] fichiers = fichiersImpression();
		return modificationEnCours() && fichiers != null && fichiers.length > 0;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		demarrage = true;
		currentAgent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(agentID,editingContext());
		preparerPopupCategorie();
		super.preparerFenetre();	
		peutAfficherPhotos = EOGrhumParametres.findParametre(editingContext(), ManGUEConstantes.GRHUM_PARAM_KEY_GRHUM_PHOTO).isParametreVrai();

		if (displayGroup().displayedObjects().count() == 0) {
			// il n'y avait pas de préférences
			super.ajouter();
		} else {
			displayGroup().selectObject(displayGroup().displayedObjects().objectAtIndex(0));
			displayGroupTypePopulation.setObjectArray(currentPreferences().populationsGerees());
			ancienModeAffichage = currentPreferences().modeFenetre();
			if (currentPreferences().typePersonnelAuDemarrage() != null) {
				categoriePersonnel = CategoriePersonnelPourAffichage.nomCategoriePersonnel(currentPreferences().typePersonnelAuDemarrage().intValue());
			} 
			super.modifier();	
		}
		demarrage = false;
		updaterDisplayGroups();
	}
	protected void traitementsPourCreation() {
		NSDictionary valeursParDefaut = ((ApplicationClient)EOApplication.sharedApplication()).preferencesParDefaut();
		currentPreferences().initAvecAgent(currentAgent,valeursParDefaut);
	}
	protected boolean traitementsPourSuppression() {
		// pas de suppression
		return true;
	}
	protected String messageConfirmationDestruction() {
		return null;
	}
	protected NSArray fetcherObjets() {
		if (preferencesID == null) {
			return null;
		} else {
			return new NSArray(SuperFinder.objetForGlobalIDDansEditingContext(preferencesID,editingContext()));
		}
	}
	protected void parametrerDisplayGroup() {}
	protected boolean conditionsOKPourFetch() {
		return currentAgent != null;
	}
	protected boolean traitementsAvantValidation() {
		if (currentPreferences().temExcluHeberges() == null) {
			currentPreferences().setExclureHeberges(false);
		}
		return true;
	}
	
	/**
	 * 
	 */
	protected void traitementsApresValidation() {
		if (ancienModeAffichage != currentPreferences().modeFenetre()) {
			((ApplicationClient)EOApplication.sharedApplication()).toutFermer();
		}
		// Si on met l'appel à super avant de fermer les fenêtres, pb car les contrôleurs ne sont plus là 
		// super.traitementsApresValidation post un unlock sur les fenêtres
		// qui a pour conséquences de demander à tous les contrôleurs de raffraîchir 
		// leurs associations ce qui poste des évenements dans la liste des événements.
		// Mais avant qu'ils soient traités, on ferme les fenêtres donc plus de contrôleur !!
		super.traitementsApresValidation();	
		// Il ne faut poster la notification sur les préférences qu'après que l'appel à super
		// soit fait pour que l'action qui affiche le contrôleur par défaut soit déclenchée
		((ApplicationClient)EOApplication.sharedApplication()).changerPreferences();
		NSNotificationCenter.defaultCenter().postNotification(PREFERENCES_CHANGEES,null);
		((EOFrameController)supercontroller()).closeWindow();

		NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,null);

	}
	protected void updaterDisplaysGroups() {
		super.updaterDisplayGroups();
		displayGroupTypePopulation.updateDisplayedObjects();
	}
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR",message);
	}
	protected void terminer() {
	}
	// méthodes privées
	private EOPrefsPersonnel currentPreferences() {
		return (EOPrefsPersonnel)displayGroup().selectedObject();
	}
	private void preparerPopupCategorie() {
		popupCategoriePersonnel.removeAllItems();
		java.util.Enumeration e = CategoriePersonnelPourAffichage.nomCategoriesPourAgent(currentAgent).objectEnumerator();
		while (e.hasMoreElements()) {
			popupCategoriePersonnel.addItem(e.nextElement());
		}
	}
	private File[] fichiersImpression() {
		if (currentPreferences().directoryImpression() != null && currentPreferences().directoryImpression().length() > 0) {
			File directory = new File(currentPreferences().directoryImpression());
			return directory.listFiles(this);
		} else {
			return null;
		}
	}
}

/*
 * Created on 5 déc. 2005
 *
 * Gestion des flux moyens
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.emplois;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePageModification;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EODecisionDelegation;
import org.cocktail.mangue.modele.mangue.EOFluxMoyens;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;

/** Gestion des flux moyens
 * @author christine
 */
// 04/11/2010 - Adaptation Netbeans
public class GestionFlux extends ModelePageModification {
	private EOGlobalID decisionID;
	private EODecisionDelegation currentDecision;

	/** Constructeur principal
	 * @param decisionID
	 * @param fluxID peut &ecirc;tre nulle (cr&eacute;ation)
	 */
	public GestionFlux(EOGlobalID decisionID,EOGlobalID fluxID) {
		super(fluxID);
		this.decisionID = decisionID;
	}
	// méthodes de délégation du display group
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("valFluxMoyens")) {
			if (currentFlux().stockMenAvant() != null && currentFlux().stockMenAvant().floatValue() != 0) {
				currentFlux().setStockMenApres(new Float(currentFlux().stockMenAvant().floatValue() + currentFlux().valFluxMoyens().floatValue()));
			}
		}
		updaterDisplayGroups();
	}
	// actions
	/** affiche les grades d&eacute;finis pour le corps */
	public void afficherCategorieEmploi() {
		UtilitairesDialogue.afficherDialogue(this,"CategorieEmploi","getCategorieEmploi",false,null,true);
	}
	public void afficherProgramme() {
		UtilitairesDialogue.afficherDialogue(this,"Programme","getProgramme",false,null,true);
	}
	public void afficherTitre() {
		UtilitairesDialogue.afficherDialogue(this,"ProgrammeTitre","getTitre",false,null,true);
	}
	/** affiche les corps d&eacute;finis pour le type de population du segment de carri&egrave;e */
	public void afficherChapitre() {
		UtilitairesDialogue.afficherDialogue(this,"Chapitre","getChapitre",false,null,true);

	}
	public void afficherArticle() {
		EOQualifier qualifier = null;
		if (currentFlux() != null && currentFlux().toChapitre() != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat("cChapitre = %@",new NSArray(currentFlux().toChapitre().cChapitre()));
		}
		UtilitairesDialogue.afficherDialogue(this,"ChapitreArticle","getArticle",false,qualifier,true);
	}

	// Notifications
	public void getCategorieEmploi(NSNotification aNotif) {
		LogManager.logDetail("GestionFlux - getCategorieEmploi");
		ajouterRelation(currentFlux(),aNotif.object(),"toCategorieEmploi");
	}
	public void getChapitre(NSNotification aNotif) {
		LogManager.logDetail("GestionFlux - getChapitre");
		if (ajouterRelation(currentFlux(),aNotif.object(),"toChapitre")) {
			currentFlux().removeObjectFromBothSidesOfRelationshipWithKey(currentFlux().toChapitreArticle(),"toChapitreArticle");
		}
	}
	public void getProgramme(NSNotification aNotif) {
		LogManager.logDetail("GestionFlux - getProgramme");
		ajouterRelation(currentFlux(),aNotif.object(),"toProgramme");
	}
	public void getTitre(NSNotification aNotif) {
		LogManager.logDetail("GestionFlux - getTitre");
		ajouterRelation(currentFlux(),aNotif.object(),"toTitre");
	}
	public void getArticle(NSNotification aNotif) {
		LogManager.logDetail("GestionFlux - getArticle");
		ajouterRelation(currentFlux(),aNotif.object(),"toChapitreArticle");
	}
	// méthodes du controller DG
	public int popupSelectedIndex() {
		if (currentFlux() == null) {
			return 0;
		} else {
			if (currentFlux().durabiliteMoyens().equals(EOFluxMoyens.DURABILITE_PERMANENTE)) {
				return 0;
			} else {
				return 1;
			}
		}
	}
	public void setPopupSelectedIndex(int selection) {
		if (currentFlux() != null) {
			switch(selection) {
			case 0 :
				currentFlux().setDurabiliteMoyens(EOFluxMoyens.DURABILITE_PERMANENTE);
				break;
			case 1 :
				currentFlux().setDurabiliteMoyens(EOFluxMoyens.DURABILITE_TEMPORAIRE);
			}
		}
	}
	public boolean peutValider() {
		if (currentFlux() == null) {
			return false;
		}
		if (currentFlux().toCategorieEmploi() == null || (currentFlux().toChapitre() == null && currentFlux().toProgramme() == null) ||
				currentFlux().dDebFluxMoyens() == null || currentFlux().valFluxMoyens() == null) {
			return false;
		}
		return super.peutValider();
	}
	/** on ne peut s&eacute;lectionner un article que si le chapitre est choisi */
	public boolean peutAfficherArticle() {
		return currentFlux() != null && currentFlux().toChapitre() != null && currentFlux().toChapitre().chapitreArticles() != null && 
		currentFlux().toChapitre().chapitreArticles().count() > 0;
	}
	/** on ne peut s&eacute;lectionner un chapitre que si la date du flux est < 01/01/06 */
	public boolean peutAfficherChapitre() {
		return currentFlux() != null && currentFlux().dDebFluxMoyens() != null && DateCtrl.isBefore(currentFlux().dDebFluxMoyens(),DateCtrl.stringToDate("01/01/2006"));
	}
	/** on ne peut s&eacute;lectionner un programme que si la date du flux est >= 01/01/06 */
	public boolean peutAfficherProgramme() {
		return currentFlux() != null && currentFlux().dDebFluxMoyens() != null;
	}
	// méthodes protégées
	/** traitements &agrave; ex&eacute;cuter suite &agrave; la cr&eacute;tion d'un objet */
	protected void traitementsPourCreation() {
		if (currentFlux() != null) {
			currentFlux().initAvecDecisionDelegation(currentDecision);
			updaterDisplayGroups();
		}
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}
	protected void preparerFenetre() {
		prepareComponent();	// 04/11/2010 Cet appel est nécessaire pour que l'archive soit chargée
		currentDecision = (EODecisionDelegation)editingContext().faultForGlobalID(decisionID,editingContext());
		super.preparerFenetre();
	}
	/** true si l'agent peut g&eacute;rer les carri&egrave;res */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutGererEmplois();
	}
	protected  void terminer() {
	}
	// méthodes privées
	private EOFluxMoyens currentFlux() {
		return (EOFluxMoyens)displayGroup().selectedObject();
	}


}

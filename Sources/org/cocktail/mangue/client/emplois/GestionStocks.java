/*
 * Created on 2 déc. 2005
 *
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.emplois;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.MatrixUtilities;
import org.cocktail.client.composants.ModalDialogController;
import org.cocktail.client.composants.ModelePageComplete;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.EODecisionDelegation;
import org.cocktail.mangue.modele.mangue.EOFluxMoyens;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOMatrix;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;

/** Gestion des stocks<BR>
 * @author christine
 *
 */
// 14/01/2011 - appel de choixRne pour sélectionner les uai
public class GestionStocks extends ModelePageComplete {
	public EODisplayGroup displayGroupDetail;
	public EOTable listeFlux;
	public EOMatrix matriceRecherche;
	private String dateRecherche;
	private int selectionPopupType;
	
	// méthodes du délégué du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		super.displayGroupDidChangeSelection(aGroup);
		if (aGroup == displayGroup()) {
			if (currentDecision() == null) {
				selectionPopupType = 0;
				updaterDisplayGroups();
			} else {
				if (currentDecision().cTypeDecision() == null || currentDecision().cTypeDecision().equals(EODecisionDelegation.TYPE_DELEGATION)) {
					selectionPopupType = 0;
				} else {
					selectionPopupType =  1;
				}
				updaterDisplayGroups();
			}
		} else if (aGroup == displayGroupDetail) {
			controllerDisplayGroup().redisplay();
		}
	}
	
	// Actions
	public void rechercher() {
		if (dateRecherche == null) {
			displayGroup().setQualifier(null);
			return;
		}
		setDateRecherche(DateCtrl.dateCompletion(dateRecherche));
		EOQualifier qualifier = null;
		NSArray args = new NSArray(DateCtrl.stringToDate(dateRecherche));
		int typeRecherche = MatrixUtilities.colonneSelectionnee(matriceRecherche);
		switch(typeRecherche) {
			case 0 : 
				qualifier = EOQualifier.qualifierWithQualifierFormat("dDecDelegation >= %@",args);
				break;
			case 1 : 
				qualifier = EOQualifier.qualifierWithQualifierFormat("dDecDelegation <= %@",args);
				break;
			case 2 :
				qualifier = EOQualifier.qualifierWithQualifierFormat("dDecDelegation = %@",args);
				break;
		}
		LogManager.logDetail("GestionStocks qualifier de recherche " + qualifier);
		displayGroup().setQualifier(qualifier);
		updaterDisplayGroups();
	}
	public void afficherRne() {
		
		EORne rne = (EORne)UAISelectCtrl.sharedInstance(editingContext()).getObject();
		if (rne != null)
			currentDecision().addObjectToBothSidesOfRelationshipWithKey(rne, "toRne");
	}	
	/** ajoute un nouvel &eacute;l&eacute;ment de carri&egrave;re en affichant la fen&ecirc;tre de cr&eacute;ation/modification */
	public void ajouterDetail() {
		afficherDetail(null);
	}
	/** modifie un &eacute;l&eacute;ment de carri&egrave;re en affichant la fen&ecirc;tre de cr&eacute;ation/modification */
	public void modifierDetail() {
		afficherDetail(editingContext().globalIDForObject(currentFlux()));
	}
	/** supprime un &eacute;l&eacute;ment de carri&egrave;re
	 * la suppression est enregistr&eacute;e dans la base apr&egrave;s consultation de l'utilisateur
	 *
	 */
	public void supprimerDetail() {
		if (EODialogs.runConfirmOperationDialog("Attention","Voulez-vous supprimer ce flux","Oui","Non")) {
			LogManager.logDetail("GestionStocks : Suppression d'un flux");
			currentFlux().supprimerRelations();
			editingContext().deleteObject(currentFlux());
			// Doit-on faire le save changes ici ??
			try {
				editingContext().saveChanges();
				displayGroupDetail.setSelectedObject(null);
				displayGroupDetail.updateDisplayedObjects();
			}
			catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur","Erreur lors de l'enregistrement des donnees !\nMESSAGE : " + e.getMessage());
				revertChanges(false);
			} finally {
				controllerDisplayGroup().redisplay();
			}
		}
	}
	// Notifications
	 /** me&eacute;thode d&eacute;clench&eacute;e suite &agrave; la s&eacute;ection d'un rne */
	public void getRne(NSNotification aNotif) {	
		ajouterRelation(currentDecision(), aNotif.object(),"toRne");
	}
	// méthodes du controller DG
	public String dateRecherche() {
		return dateRecherche;
	}
	public void setDateRecherche(String dateRecherche) {
		this.dateRecherche = dateRecherche;
	}
	public int selectionPopupType() {
		return selectionPopupType;
	}
	public void setSelectionPopupType(int selection) {
		selectionPopupType = selection;	// on utilise une variable intermédiaire car sinon boucle infinie au démarrage
		if (currentDecision() != null) {
			switch(selection) {
				case 0 :
					currentDecision().setCTypeDecision(EODecisionDelegation.TYPE_DELEGATION);
					break;
				case 1 :
					currentDecision().setCTypeDecision(EODecisionDelegation.TYPE_ROMPU);
			}
		}
	}
	public boolean peutValider() {
		if (currentDecision() == null || currentDecision().dDecDelegation() == null || currentDecision().toRne() == null) {
			return false;
		}
		return super.peutValider();
  	}
	public boolean peutRechercher() {
		return modificationEnCours() == false;
	}
	public boolean modificationFluxAutorisee() {
		return boutonModificationAutorise() && displayGroupDetail.selectedObject() != null;
	}
	// méthodes protégées
	/**  m&eacute;thode &agrave; surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lock&eacute;e,..) */
    protected boolean conditionSurPageOK() {
    		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
    		return agent.peutGererEmplois();
    }
	protected void preparerFenetre() { 
		GraphicUtilities.changerTaillePolice(listeFlux,11);
		GraphicUtilities.rendreNonEditable(listeFlux);
	    listeFlux.table().addMouseListener(new DoubleClickListenerFlux());
	    super.preparerFenetre();
	    updaterDisplayGroups();
	}
	protected void traitementsPourCreation() {
		currentDecision().init();
	}
	protected boolean traitementsPourSuppression() {
		currentDecision().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer cette décision de délégation ?\nTous les flux seront supprimés.";
	}
	 protected boolean conditionsOKPourFetch() {
		return true;
    }
	protected NSArray fetcherObjets() {
		return SuperFinder.rechercherEntite(editingContext(),"DecisionDelegation");
	}

	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("dDecDelegation", EOSortOrdering.CompareDescending)));
		displayGroupDetail.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("toCategorieEmploi.cCategorieEmploi", EOSortOrdering.CompareAscending)));
		displayGroup().setSelectsFirstObjectAfterFetch(false);
		displayGroupDetail.setSelectsFirstObjectAfterFetch(false);
	}
	protected boolean traitementsAvantValidation() {
		return true;
	}

	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR",message);
	}
	protected void updaterDisplayGroups() {
		super.updaterDisplayGroups();
		displayGroupDetail.updateDisplayedObjects();
	}
	protected  void terminer() {
	}
	// méthodes privées
	private EODecisionDelegation currentDecision() {
		return (EODecisionDelegation)displayGroup().selectedObject();
	}
	private EOFluxMoyens currentFlux() {
		return (EOFluxMoyens)displayGroupDetail.selectedObject();
	}
	
	// affiche la page de création/modification d'un flux
	private void afficherDetail(EOGlobalID fluxID) {
		GestionFlux controleur = new GestionFlux(editingContext().globalIDForObject(currentDecision()),fluxID);
		controleur.activer();
		ModalDialogController modalDialogController = new ModalDialogController(controleur,"Gestion des Flux");
		modalDialogController.activateWindow();
		
	}
	public class DoubleClickListenerFlux extends MouseAdapter {
		   public void mouseClicked(MouseEvent e) {
         if (e.getClickCount() == 2) {
         		if (modeSaisiePossible() && !modificationEnCours()) {
         			modifierDetail();
         		}
         }
     }
 }
}

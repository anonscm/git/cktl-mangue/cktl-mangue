
//GestionAbsences.java
//Mangue

//Created by Christine Buttin on Mon May 30 2005.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.



/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Constructor;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.ListSelectionModel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.AideUtilisateur;
import org.cocktail.client.composants.GestionPeriode;
import org.cocktail.client.composants.ModalDialogController;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.component.COButton;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.AgentsCtrl;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.outils_interface.GestionConge;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecArrete;
import org.cocktail.mangue.client.outils_interface.GestionTypeConge;
import org.cocktail.mangue.client.outils_interface.ModelePageAvecIndividu;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeCgMatern;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Outils;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.InfoPourEditionRequete;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.mangue.conges.CongeHospitaloUniversitaire;
import org.cocktail.mangue.modele.mangue.conges.EOCgntAccidentTrav;
import org.cocktail.mangue.modele.mangue.conges.EOCgntCgm;
import org.cocktail.mangue.modele.mangue.conges.EOCgntMaladie;
import org.cocktail.mangue.modele.mangue.conges.EOCgntRaisonFamPerso;
import org.cocktail.mangue.modele.mangue.conges.EOCld;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAccidentServ;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAdoption;
import org.cocktail.mangue.modele.mangue.conges.EOCongeFormation;
import org.cocktail.mangue.modele.mangue.conges.EOCongeGardeEnfant;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaladie;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaladieSsTraitement;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaternite;
import org.cocktail.mangue.modele.mangue.conges.EOCongePaternite;
import org.cocktail.mangue.modele.mangue.conges.EOCongeSolidariteFamiliale;
import org.cocktail.mangue.modele.mangue.conges.EODeclarationMaternite;
import org.cocktail.mangue.modele.mangue.conges.EODroitsGardeEnfant;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Gestion des absences legales
 * Affiche les absences et leur duree. Cette derniere n'est calculee que dans le cas ou elle est inconnue ou avec une ancienne
 * valeur<BR>
 * Affichage des types d'absence:<BR>
 * Par defaut, on debute sur l'annee universitaire.<BR>
 * L'affichage des types de conge est fait selon les regles suivantes:<BR>
 * Si l'agent est fonctionnaire pendant la periode (date debut - date fin saisies), on affiche les types de conges de fonctionnaire 
 * uniquement, meme si il a ces contrats.<BR>
 * Si l'agent est contractuel ou considere comme non-titulaire pendant la periode, on affiche les types de conges de contractuels 
 * uniquement, meme si il a ces contrats.<BR>
 * G&egrave; les regles de creation, modification et suppression d'absences :<BR>
 *  Si une absence comporte un arrete signe, elle n'est plus supprimable. Pour un conge de maternite, si la declaration
 * est annulee ou que le conge est fractionne, l'absence n'est plus supprimable<BR>
 * On ne peut ajouter un nouveau conge de maternite, que si le precedent est complet (i.e la date d'accouchement est fournie)<BR>
 *
 * */

public class GestionAbsences extends ModelePageAvecIndividu {
	//public JComboBox popupTypeAbsence;
	public EOView vueBoutonsGardeEnfant,vuePeriode,vueTypeConge,vueInspecteur;
	public JButton boutonDetailTraitement;
	public COButton btnProlonger;
	private String libelleEntete;
	private String anneeDroits,nbDemiJournees;
	private float dureeTotale,dureePeriode;
	private GestionPeriode controleurPeriode;
	private GestionTypeConge controleurTypeConge;
	private InspecteurAbsence inspecteur;

	private NSMutableDictionary dicoControleurs = new NSMutableDictionary();
	private NSMutableDictionary dicoEntites = new NSMutableDictionary();

	// Cette liste doit être synchronisée avec les listes de org.cocktail.mangue.client.outils_interface.GestionTypeConge
	private static String nomControleurs[] = {"","GestionCongeMaladie","GestionCongeMaladie","GestionCongeLongueMaladie",
		"GestionCongeLongueMaladie","CongeRaisonPersonnelleCtrl","GestionCld","GestionCongeHospitaloUniv",
		"GestionCongeHospitaloUniv","GestionCongeHospitaloUniv","GestionCongeHospitaloUniv",
		"GestionCongeMaladieSansTraitement","GestionCongeMaternite","GestionCongePaternite",
		"GestionCongeFormation","GestionCongeGardeEnfant","GestionCongeAccidentTravail",
		"GestionCongeAccidentTravail","GestionCongeAdoption","GestionCrct","GestionCongeSolidarite",
		"GestionCcp","GestionCongeCir","GestionRdt"};
	// Ces deux variables doivent être synchronisées avec les listes de GestionTypeConge (en tête, mettre les types de congés qui supportent le détail de traitement)
	private static int INDEX_MIN_DETAIL_TRAITEMENT = 1,INDEX_MAX_DETAIL_TRAITEMENT = 9;
	private static String GARDE_ENFANT = "ENFANT";

	/** Notification a envoyee pour signaler la synchronisation des absences */
	public static String SYNCHRONISATION_DATES_AFFICHAGE = "SynchronisationAffichage";
	public static String SYNCHRONISATION_DROITS_GARDE = "SynchronisationAffichageDroitsGarde";
	public static String SYNCHRONISATION_TYPE_CONGE = "SynchronisationAffichageTypeConges";
	/** Notifications envoyees a l'inspecteur */
	public static String CHANGER_ABSENCE = "ChangerAbsence";

	public boolean messageAlerteMaterniteEnvoye = false;

	public GestionAbsences() {
		super();
	}

	//  Accesseurs
	public String libelleEntete() {
		return libelleEntete;
	}

	public void setLibelleEntete(String libelle) {
		this.libelleEntete = "Détail " + libelle;
	}

	public void setEdited(boolean aBool) {
		super.setEdited(false);	// on ne gere pas de modifications dans cette page mais les objets metier sont modifies pour attacher des infos
	}

	public float dureeTotale() {

		if ( controleurTypeConge != null && controleurTypeConge.typeConge() != null && controleurTypeConge.typeConge().equals("ENFANT")  )
			return dureeTotale * 2;

		return dureeTotale;
	}
	public float dureePeriode() {
		return dureePeriode;
	}
	// méthodes du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		preparerPourDetailTraitement();
		calculerDureesTotales(displayGroup().displayedObjects());
		super.displayGroupDidChangeSelection(aGroup);
		if (inspecteur != null && vueInspecteur.isVisible()) {
			NSNotificationCenter.defaultCenter().postNotification(CHANGER_ABSENCE,currentAbsence());
		}
	}
	// Actions
	// action declenchee par une sélection dans le popup de type absence
	public void typeAbsenceHasChanged() {

		CRICursor.setWaitCursor(component());
		messageAlerteMaterniteEnvoye = false;
		afficherAbsences();
		CRICursor.setDefaultCursor(component());

	}
	public void afficherInspecteur() {
		if (inspecteur == null) {
			String nomEntite = null;
			if (controleurTypeConge != null) {
				nomEntite = controleurTypeConge.nomEntiteSelectionnee();
			}
			inspecteur = new InspecteurAbsence(editingContext(),nomEntite);
			inspecteur.init();
			GraphicUtilities.swaperView(vueInspecteur,inspecteur.component());
			vueInspecteur.setVisible(true);
		} else {
			vueInspecteur.setVisible(!vueInspecteur.isVisible());
		}
		if (vueInspecteur.isVisible()) {
			NSNotificationCenter.defaultCenter().postNotification(CHANGER_ABSENCE,currentAbsence());
		}
	}


	/**
	 * 
	 */
	public void ajouter() {

		if (controleurTypeConge.typeConge().equals(EOTypeAbsence.TYPE_CONGE_RFP)) {
			CongeRaisonPersonnelleCtrl.sharedInstance(editingContext()).modifier(EOCgntRaisonFamPerso.creer(editingContext(), currentIndividu()));
			typeAbsenceHasChanged();
		}
		else {
			if (controleurTypeConge.typeConge().equals(EOTypeAbsence.TYPE_CONGE_MALADIE_CGM)) {
				CongeGraveMaladieCtrl.sharedInstance(editingContext()).modifier(EOCgntCgm.creer(editingContext(), currentIndividu()));
				typeAbsenceHasChanged();
			}
//			else {
//				if (controleurTypeConge.typeConge().equals(EOTypeAbsence.TYPE_CLM)) {
//					CongeLongueMaladieCtrl.sharedInstance(editingContext()).modifier(EOClm.creer(editingContext(), currentIndividu()));
//					typeAbsenceHasChanged();
//				}
//				else {
//					if (controleurTypeConge.typeConge().equals(EOTypeAbsence.TYPE_CONGE_GARDE_ENFANT)) {
//						CongeGardeEnfantCtrl.sharedInstance(editingContext()).modifier(EOCongeGardeEnfant.creer(editingContext(), currentIndividu()));
//						typeAbsenceHasChanged();
//					}
//					else {
						GestionConge controleur = controleurCongePopup();
						if (controleur != null) {
							setModeCreation(true);
							controleur.ajouter(editingContext().globalIDForObject(currentIndividu()));
						}
//					}
//				}
//			}
		}
	}

	/**
	 * 
	 */
	public void modifier() {

		setModeCreation(false);

		if (currentAbsence().toTypeAbsence().estCongeRfp()) {
			CongeRaisonPersonnelleCtrl.sharedInstance(editingContext()).modifier((EOCgntRaisonFamPerso)Conge.findForAbsence(editingContext(), EOCgntRaisonFamPerso.ENTITY_NAME, currentAbsence()));
			typeAbsenceHasChanged();
		}
		else {
			if (currentAbsence().toTypeAbsence().estCongeGraveMaladie()) {
				CongeGraveMaladieCtrl.sharedInstance(editingContext()).modifier((EOCgntCgm)Conge.findForAbsence(editingContext(), EOCgntCgm.ENTITY_NAME, currentAbsence()));
				typeAbsenceHasChanged();
			}
			else {
//				if (currentAbsence().toTypeAbsence().estCongeLongueMaladie()) {
//					CongeLongueMaladieCtrl.sharedInstance(editingContext()).modifier((EOClm)Conge.findForAbsence(editingContext(), EOClm.ENTITY_NAME, currentAbsence()));
//					typeAbsenceHasChanged();
//				}
//				else {
//					if (currentAbsence().toTypeAbsence().estCongeGardeEnfant()) {
//						CongeGardeEnfantCtrl.sharedInstance(editingContext()).modifier((EOCongeGardeEnfant)Conge.findForAbsence(editingContext(), EOCongeGardeEnfant.ENTITY_NAME, currentAbsence()));
//						typeAbsenceHasChanged();
//					}
//					else {
						GestionConge controleur = controleurConge();
						if (controleur != null) {
							controleur.modifier(editingContext().globalIDForObject(currentAbsence()));
						}
					}
				}
//			}
//		}
	}


	// les suppressions sont gérés par les contrôleurs de congé pour bénéficier de la gestion des suppressions propres
	// à chaque type de congé
	public void supprimer() {

		String message = "Confirmez-vous la suppression de cette absence ?";
		if (!EODialogs.runConfirmOperationDialog("Attention",message,"Oui","Non"))
			return;

		if (currentAbsence().toTypeAbsence().estCongeRfp()) {
			CongeRaisonPersonnelleCtrl.sharedInstance(editingContext()).supprimer((EOCgntRaisonFamPerso)Conge.findForAbsence(editingContext(), EOCgntRaisonFamPerso.ENTITY_NAME, currentAbsence()));
			typeAbsenceHasChanged();
		}		
		else 
			if (currentAbsence().toTypeAbsence().estCongeGraveMaladie()) {
				CongeGraveMaladieCtrl.sharedInstance(editingContext()).supprimer((EOCgntCgm)Conge.findForAbsence(editingContext(), EOCgntCgm.ENTITY_NAME, currentAbsence()));
				typeAbsenceHasChanged();
			}	
			else 
				if (currentAbsence().toTypeAbsence().estCongeLongueMaladie()) {
					CongeLongueMaladieCtrl.sharedInstance(editingContext()).supprimer((EOClm)Conge.findForAbsence(editingContext(), EOClm.ENTITY_NAME ,currentAbsence()));
					typeAbsenceHasChanged();
				}		
				else {
					GestionConge controleur = controleurConge();
					if (controleur != null) {
						if (controleur.supprimer(editingContext().globalIDForObject(currentAbsence()))) {
							calculerDureesTotales(displayGroup().displayedObjects());
							editingContext().invalidateObjectsWithGlobalIDs(new NSArray(editingContext().globalIDForObject(currentAbsence())));
							typeAbsenceHasChanged();
							boutonDetailTraitement.setEnabled(currentAbsence() != null);
						}
					}
				}
	}

	/**
	 * 
	 */
	public void imprimerArrete() {

		
		//getControleurConge().imprimerArrete();
		
		CRICursor.setWaitCursor(((ApplicationClient)EOApplication.sharedApplication()).activeFrame());

		if (currentAbsence().toTypeAbsence().estCongeAccidentService()) {
			CongeAccidentServiceCtrl.sharedInstance(editingContext()).imprimerArrete((EOCongeAccidentServ)Conge.findForAbsence(editingContext(), EOCongeAccidentServ.ENTITY_NAME, currentAbsence()));
			typeAbsenceHasChanged();
		}
		else {
			GestionConge controleur = controleurConge();
			if (controleur != null && (controleur instanceof GestionCongeAvecArrete || controleur instanceof GestionCongeGardeEnfant)) {
				controleur.imprimer(editingContext().globalIDForObject(currentAbsence()));

			} else {
				EODialogs.runInformationDialog("Attention","Pas d'impression disponible pour ce type de congé");
			}
		}

		CRICursor.setDefaultCursor(((ApplicationClient)EOApplication.sharedApplication()).activeFrame());

	}

	/**
	 * 
	 */
	public void imprimer() {

		String titre = libelleEntete();
		if (libelleEntete() == null) {
			titre = "Détail de tous les congés";
		}

		boolean imprimerDetail = false;
		if (controleurTypeConge.typeConge() != null && controleurTypeConge.typeConge().equals(GestionTypeConge.TOUS_CONGES) == false) {
			imprimerDetail = EODialogs.runConfirmOperationDialog("", "Voulez-vous imprimer les congés détaillés", "Oui", "Non");
		}

		InfoPourEditionRequete infoEdition = new InfoPourEditionRequete("Absences",titre,controleurPeriode.dateDebut(),controleurPeriode.dateFin());
		Class[] classeParametres =  new Class[] {InfoPourEditionRequete.class,EOGlobalID.class,NSArray.class,Boolean.class};
		Object[] parametres = new Object[]{infoEdition,editingContext().globalIDForObject(currentIndividu()),Utilitaires.tableauDeGlobalIDs(displayGroup().displayedObjects(),editingContext()), new Boolean(imprimerDetail)};
		try {
			// avec Thread
			UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerAbsencesPourIndividu",classeParametres,parametres,"Absences_" + currentIndividu().noIndividu(),"Impression des absences");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}

	}

	public void afficherDemiJournees() {
		LogManager.logDetail("Affichage des droits de garde");
		GestionDroitsGarde controleur = new GestionDroitsGarde();
		controleur.initialiser(individuCourantID());
		ModalDialogController modalDialogController = new ModalDialogController(controleur,"Gestion des Droits de garde");
		controleur.activer();	// 28/01/2011 - deplacer après l'initialisation du dialogue
		modalDialogController.activateWindow();
	}


	public void afficherDetailsTraitement() {

		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		if (currentAbsence() != null) {

			String nomEntite = controleurTypeConge.nomEntitePour(currentAbsence().toTypeAbsence().code());

			if (nomEntite.equals(EOCongeMaladie.ENTITY_NAME) == true)
				DetailTraitementsCMOCtrl.sharedInstance(editingContext()).afficherDetails(currentIndividu(), currentAbsence());			
			else
				if (nomEntite.equals(EOCgntMaladie.ENTITY_NAME) == true)
					DetailTraitementsCMNTCtrl.sharedInstance(editingContext()).afficherDetails(currentIndividu(), currentAbsence());
				else 
					if (nomEntite.equals(EOClm.ENTITY_NAME) == true)
						DetailTraitementsCLMCtrl.sharedInstance(editingContext()).afficherDetails(currentIndividu(), currentAbsence());
					else 
					{
						LogManager.logDetail("Affichage des Details de traitement pour l'absence de type " + currentAbsence().toTypeAbsence().code() + 
								" periode du " + DateCtrl.dateToString(currentAbsence().dateDebut()) + " au " + DateCtrl.dateToString(currentAbsence().dateFin()));

						GestionDetailsTraitement controleur = new GestionDetailsTraitement(nomEntite);
						ModalDialogController modalDialogController = new ModalDialogController(controleur,"Détail des Périodes de traitement");
						// 28/01/2011 - déplacé après la création du dialogue
						controleur.initialiser(editingContext().globalIDForObject(currentIndividu()),currentAbsence().dateDebut(),currentAbsence().dateFin());
						modalDialogController.activateWindow();
					}
		}

		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));				

	}

	public void afficherAide() {
		AideUtilisateur controleur = new AideUtilisateur("Aide_GestionConges", null);
		controleur.afficherFenetre();
	}

	// NOTIFICATIONS
	public void employeHasChanged(NSNotification aNotif) {
		super.employeHasChanged(aNotif);
		messageAlerteMaterniteEnvoye = false;
		actualiser();
	}
	public void setCurrentIndividu(NSNotification aNotif) {
		setCurrentIndividu((EOIndividu)aNotif.object());
		actualiser();
	}
	
	public void actualiser() {

		preparerTypesConges();
		NSArray<EOAbsences> absences = rechercherAbsences();
		displayGroup().setObjectArray(absences);
		completerTableAbsences(absences);
		displayGroup().updateDisplayedObjects();

	}

	
	public void synchroniser(NSNotification aNotif) {
		super.synchroniser(aNotif);
		completerTableAbsences(displayGroup().displayedObjects());
		updaterDisplayGroups();
	}
	public void synchroniserAffichageDroitGarde(NSNotification aNotif) {
		preparerInfosGardeEnfant();
		controllerDisplayGroup().redisplay();
	}
	public void synchroniserAffichage(NSNotification aNotif) {
		// utilisée pour synchroniser l'affichage au cas où la date de début de l'absence n'est pas dans les dates affichées
		NSTimestamp debutConge = (NSTimestamp)aNotif.object();
		boolean periodeChangee = false;
		String debutPeriode = controleurPeriode.debutPeriode();
		if (debutPeriode == null || (debutPeriode != null && DateCtrl.isAfter(DateCtrl.stringToDate(debutPeriode),debutConge))) {
			debutPeriode = DateCtrl.dateToString(debutConge);
			periodeChangee = true;
		}
		String finPeriode = controleurPeriode.finPeriode();
		if (finPeriode == null || (finPeriode != null && DateCtrl.isBefore(DateCtrl.stringToDate(finPeriode),debutConge))) {
			finPeriode = DateCtrl.dateToString(debutConge);
			periodeChangee = true;
		}
		if (periodeChangee) {
			controleurPeriode.changerDates(debutPeriode,finPeriode);
		}
	}
	public void synchroniserDates(NSNotification aNotif) {
		LogManager.logDetail("Synchronisation des dates d'affichage");
		preparerTypesConges();
		afficherAbsences();
	}
	public void synchroniserTypesConges(NSNotification aNotif) {
		LogManager.logDetail("Synchronisation des types de congés");
		preparerTypesConges();
	}
	public void changerTypeConge(NSNotification aNotif) {
		LogManager.logDetail("Changement du type de conge");
		preparerInterfaceConge();
		afficherAbsences();
	}
	// Méthodes controller display group
	public String anneeDroits() {
		return anneeDroits;
	}
	public String nbDemiJournees() {
		return nbDemiJournees;
	}
	public boolean modeSaisiePossible() {

		if (controleurTypeConge == null 
				|| individuCourantID() == null
				|| !conditionSurPageOK()		// Pas de droits de modification des conges
				|| controleurTypeConge.typeConge().equals(GestionTypeConge.TOUS_CONGES)
				|| modificationEnCours()
				|| estLocke() 
				) {
			return false;
		}

		if (controleurTypeConge.typeConge().equals(EOTypeAbsence.TYPE_CONGE_GARDE_ENFANT))
			return gardeEnfantPossible();

		return true;

	}


	public boolean peutRequalifier() {
		return  currentAbsence() != null 
				&& ( currentAbsence().toTypeAbsence().estCongeMaladie() || currentAbsence().toTypeAbsence().estCongeLongueMaladie() ) 
				&& displayGroup().selectedObjects().count() > 0;		
	}

	public boolean peutProlonger() {

		return  currentAbsence() != null 
				&& ( currentAbsence().toTypeAbsence().estCongeLongueDuree()) 
				&& displayGroup().selectedObjects().count() > 0;		
	}

	public void requalifier(){
		CongeRequalificationCtrl.sharedInstance(editingContext()).open(currentIndividu(), displayGroup().selectedObjects());
		typeAbsenceHasChanged();
	}

	public void prolonger() {

		GestionConge controleur = controleurConge();
		if (controleur != null) {
			setModeCreation(true);
			controleur.ajouter(editingContext().globalIDForObject(currentIndividu()));
		}

	}

	/**
	 * 
	 * @return
	 */
	public boolean peutAjouter() {

		if (controleurTypeConge == null) {
			return false;
		}

		String typeConge = controleurTypeConge.typeConge();

		// On ne peut plus ajouter de congés pour accident de service ou de travail. 
		// Seulement en consultation. La saisie se fait dans la partie INFOS / DECLARATION AT.
		if (typeConge.equals(EOTypeAbsence.TYPE_CONGE_ACC_SERV) ||
				typeConge.equals(EOTypeAbsence.TYPE_CONGE_ACC_TRAV) ) 
			return false;

		// Test specifique aux conges maternite
		if (typeConge != null && typeConge.equals(EOTypeAbsence.TYPE_CONGE_MATERNITE) && currentIndividu() != null) {

			// vérifier qu'il n'existe pas de déclaration de maternité non annulé sans date d'accouchement
			NSArray<EOCongeMaternite> congesMat = EOCongeMaternite.rechercherCongesValidesPourIndividu(editingContext(), EOCongeMaternite.ENTITY_NAME , currentIndividu());
			for (EOCongeMaternite congeMat : congesMat) {

				// Controle des dates d'accouchement qui ne doivent pas etre nulles.
				if (congeMat.toTypeCgMatern() != null 
						&& congeMat.toTypeCgMatern().code().equals(EOTypeCgMatern.MATERNITE)) {

					// La date d'accouchement des congés maternité doit être saisie
					if (congeMat.declarationMaternite().dAccouchement() == null && 
							DateCtrl.isBefore(congeMat.dateDebut(), new NSTimestamp()) ) {

						if (!messageAlerteMaterniteEnvoye) {
							EODialogs.runErrorDialog("ERREUR", 
									"Veuillez renseigner une date d'accouchement pour le congé maternité " +
											" du " + congeMat.dateDebutFormatee() + " au " + congeMat.dateFinFormatee() + " !");
						}				
						messageAlerteMaterniteEnvoye = true;
						return false;
					}

				}
			}
			// Pas de soucis de saisie
			return true;
		}

		// Autres conges que MATERNITE
		return modeSaisiePossible();

	}

	/**
	 * 
	 * @return
	 */
	public boolean peutModifier() {

		if (	displayGroup() == null 
				|| displayGroup().selectedObject() == null 
				|| displayGroup().selectedObjects().count() > 1
				|| !conditionSurPageOK()
				|| currentAbsence().toTypeAbsence().estCongeAccidentService()
				|| currentAbsence().toTypeAbsence().estCongeAccidentTravail()
				|| estLocke()) 
			return false;

		return true;

	}

	public boolean peutSupprimer() {

		if ( displayGroup() == null 
				|| displayGroup().selectedObject() == null 
				|| displayGroup().selectedObjects().count() > 1 
				|| !conditionSurPageOK()
				|| currentAbsence().toTypeAbsence().estCongeAccidentService()
				|| currentAbsence().toTypeAbsence().estCongeAccidentTravail()
				|| estLocke()) 
			return false;

		Conge conge = trouverConge();
		if (conge == null) {
			return true;
		}
		if (conge instanceof EOCongeMaternite) {
			EOCongeMaternite congeMaternite = (EOCongeMaternite)conge;
			// si la déclaration de maternité ou le congé est annulé ou qu'il y a eu fractionnement du congé, on ne peut pas supprimer
			EODeclarationMaternite declaration = congeMaternite.declarationMaternite();
			if (declaration != null && (declaration.estDeclarationAnnulee() || (congeMaternite.estTypeMaternite() && declaration.estCongeFractionne()))) {
				return false;
			}
		}

		//		if (conge instanceof CongeAvecArreteAnnulation) {
		//			// on ne peut pas supprimer un congé annulé ou un congé qui remplace d'autres congés
		//			CongeAvecArreteAnnulation congeAvecArrete = (CongeAvecArreteAnnulation)conge;
		//			if (congeAvecArrete.congeDeRemplacement() != null || congeAvecArrete.dAnnulation() != null ||
		//					congeAvecArrete.noArreteAnnulation() != null || congeAvecArrete.aCongesRemplaces()) {
		//				return false;
		//			}
		//		}

		//		if (conge instanceof EOCld) {
		//			return ((EOCld)conge).estProlonge() == false;
		//		}
		if (conge instanceof CongeHospitaloUniversitaire) {
			// on ne peut détruire que le dernier d'une série de congés groupés à cause des prolongations. On regarde
			// si il existe des congés associés postérieurs
			NSArray congesAssocies = ((CongeAvecArreteAnnulation)conge).congesAssocies(false);
			return congesAssocies.count() == 0  && ((CongeAvecArreteAnnulation)conge).estSigne() == false;
		}
		//		try {
		//			// vérifier si il y a un arrêté signé
		//			String estSigne = (String)conge.valueForKey("temConfirme");
		//			return estSigne == null || estSigne.equals(CocktailConstantes.FAUX);
		//		} catch (Exception e) {
		//			return true;		// pas de gestion d'arrêté ou pas de signature d'arrêté
		//		}

		return true;
	}

	public boolean peutImprimer() {
		return displayGroup().displayedObjects().count() > 0 && conditionSurPageOK();
	}
	public boolean peutImprimerArrete() {
		return displayGroup().selectedObjects().count() > 0;
	}

	public boolean peutAfficherInspecteur() {
		return controleurTypeConge != null && controleurTypeConge.indexSelection() > 0;
	}
	// méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		preparerInterface();
	}
	protected void loadNotifications() {
		super.loadNotifications();
		
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("setCurrentIndividu", new Class[] {NSNotification.class}),
				AgentsCtrl.NOTIF_SET_INDIVIDU_ABSENCES,null);

		// s'enregistrer pour recevoir les notifications de synchronisation
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniser", new Class[] {NSNotification.class}),
				ModelePage.SYNCHRONISER,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserAffichage", new Class[] {NSNotification.class}),
				SYNCHRONISATION_DATES_AFFICHAGE,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserAffichageDroitGarde", new Class[] {NSNotification.class}),
				SYNCHRONISATION_DROITS_GARDE,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserDates", new Class[] {NSNotification.class}),
				GestionPeriode.NOTIFICATION_PERIODE_HAS_CHANGED,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("changerTypeConge", new Class[] {NSNotification.class}),
				GestionTypeConge.TYPE_CONGE_HAS_CHANGED,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("synchroniserTypesConges", new Class[] {NSNotification.class}),
				SYNCHRONISATION_TYPE_CONGE,null);

		initDicosControleursConges();

	}
	
	
	/** methode a surcharger pour retourner les objets affiches par le display group */
	protected NSArray fetcherObjets() {
		return rechercherAbsences();
	}
	/** methode a surcharger pour parametrer le display group (tri ou filtre) */
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
	}
	/**  methode a surcharger pour indiquer si les conditions de modification de la page sont OK (agent a les droits, page lockee,..) */
	protected boolean conditionSurPageOK() {
		EOAgentPersonnel agent = (EOAgentPersonnel)SuperFinder.objetForGlobalIDDansEditingContext(((ApplicationClient)EOApplication.sharedApplication()).agentGlobalID(),editingContext());
		return agent.peutGererConges();
	}
	/** traitements a executer suite a la cretion d'un objet */
	protected void traitementsPourCreation() {
		// pas de création dans la page
	}
	/** traitements a executer avant la destruction d'un objet 
	 * return true si la suppression s'est bien passee */
	protected boolean traitementsPourSuppression() {
		return true;
	}
	/** methode a surcharger pour faire des traitements avant l'enregistrement des donnees dans la base
	 * retourne true si on peut enregistrer */
	protected boolean traitementsAvantValidation() {
		return true;
	}
	/** retourne le message de confirmation aupres de l'utilisateur avant d'effectuer une suppression  */
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ce congé ?";
	}
	/** affiche un message suite a une exception de validation */
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR",message);
	}
	protected void terminer() {
		if (inspecteur != null)
			inspecteur.terminer();
	}
	// méthodes privees
	private EOAbsences currentAbsence() {
		return (EOAbsences)displayGroup().selectedObject();
	}
	private void preparerInterface() {
		listeAffichage.table().setBackground(new Color((float)1.0,(float)0.619,(float)0.659,(float)1.0));
		listeAffichage.table().setGridColor(Color.BLACK);  
		listeAffichage.table().getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		vueBoutonsGardeEnfant.setVisible(false);
		vueInspecteur.setVisible(false);

		btnProlonger.setVisible(false);

		String typeConge = ((ApplicationClient)EOApplication.sharedApplication()).typeAbsenceParDefaut();
		// on force l'affichage par defaut des conges d'un individu
		if (typeConge == null) {
			typeConge = GestionTypeConge.TOUS_CONGES;
		}
		controleurTypeConge = new GestionTypeConge(editingContext(),typeConge);
		controleurTypeConge.init();
		GraphicUtilities.swaperView(vueTypeConge,controleurTypeConge.component());
		// préparer le contrôleur de période et l'afficher dans la vue des périodes en sélectionnant l'affichage des périodes par défaut
		controleurPeriode = new GestionPeriode(null,null,((ApplicationClient)EOApplication.sharedApplication()).periodeAbsenceParDefaut());
		controleurPeriode.init();
		GraphicUtilities.swaperView(vuePeriode,controleurPeriode.component());
		boutonDetailTraitement.setEnabled(false);
		updaterDisplayGroups();
	}
	private void preparerPourDetailTraitement() {

		if (controleurTypeConge.estSelectionTousConges() 
				|| controleurTypeConge.typeConge().equals(EOTypeAbsence.TYPE_CONGE_MALADIE)
				|| controleurTypeConge.typeConge().equals(EOTypeAbsence.TYPE_CONGE_MALADIE_NT)
				|| controleurTypeConge.typeConge().equals(EOTypeAbsence.TYPE_CONGE_MALADIE_CGM)) {	// affichage de tous les congés
			// gérer l'affichage des boutons de détail de traitement si la sélection correspond à un congé qui a des détails
			int index = 0;
			if (currentAbsence() != null)
				index = controleurTypeConge.indexPour(currentAbsence().toTypeAbsence().code());

			boutonDetailTraitement.setEnabled(
					displayGroup().selectedObjects().count() == 1 
					&& index >= INDEX_MIN_DETAIL_TRAITEMENT 
					&& index <= INDEX_MAX_DETAIL_TRAITEMENT 
					&& currentAbsence().estValide());
		}

		if (boutonDetailTraitement.isEnabled())
			boutonDetailTraitement.setEnabled(currentAbsence() != null && currentAbsence().estValide());

		updaterDisplayGroups();
	}

	// préparer le display group de type congé. On veut rajouter un type absence permettant la sélection de tous les congés
	// restreindre aux congés autorisés selon que la personne est titulaire ou non
	private void preparerTypesConges() {
		if (currentIndividu() == null || controleurTypeConge == null) // cas de la gestion par onglets
			return;
		controleurTypeConge.setTypesConges(EOTypeAbsence.rechercherTypesCongesLegaux(editingContext(),currentIndividu(),controleurPeriode.dateDebut(),controleurPeriode.dateFin()));
	}

	private void preparerInterfaceConge() {

		if (controleurTypeConge.typeConge() != null && controleurTypeConge.typeConge().equals(GARDE_ENFANT)) {
			vueBoutonsGardeEnfant.setVisible(true);
			preparerInfosGardeEnfant();
		} else
			vueBoutonsGardeEnfant.setVisible(false);

		setLibelleEntete(controleurTypeConge.libelleSelectionne());

		int index = controleurTypeConge.indexSelection();
		// ajouter/supprimer dans le popup des types d'absences les "congés remis en cause" si le type de congé choisi supporte cette fonctionnalité
		if (index == 0) {
			if (vueInspecteur.isVisible())
				vueInspecteur.setVisible(false);
		} else {
			EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(controleurTypeConge.nomEntiteSelectionnee());
			EOGenericRecord record = (EOGenericRecord)classDescription.createInstanceWithEditingContext(null,null);
		}

		// afficher le bouton de détail de traitement pour les entités le supportant
		boutonDetailTraitement.setEnabled(index >= INDEX_MIN_DETAIL_TRAITEMENT && index <= INDEX_MAX_DETAIL_TRAITEMENT);
		boutonDetailTraitement.setEnabled(false);

		controllerDisplayGroup().redisplay();
	}

	public void initDicosControleursConges() {

		dicoEntites.setObjectForKey( EOCgntAccidentTrav.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_ACC_TRAV);
		dicoEntites.setObjectForKey( EOCongeAccidentServ.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_ACC_SERV);

		dicoControleurs.setObjectForKey( GestionCongeMaladie.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_MALADIE);
		dicoEntites.setObjectForKey(EOCongeMaladie.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_MALADIE);

		dicoControleurs.setObjectForKey( GestionCongeMaladie.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_MALADIE_NT);
		dicoEntites.setObjectForKey(EOCgntMaladie.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_MALADIE_NT);

		dicoControleurs.setObjectForKey( GestionCongeLongueMaladie.class.getSimpleName(), EOTypeAbsence.TYPE_CLM);
		dicoEntites.setObjectForKey( EOClm.ENTITY_NAME, EOTypeAbsence.TYPE_CLM);

		dicoControleurs.setObjectForKey( GestionCongeLongueMaladie.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_MALADIE_CGM);
		dicoEntites.setObjectForKey( EOCgntCgm.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_MALADIE_CGM);

		dicoControleurs.setObjectForKey( CongeRaisonPersonnelleCtrl.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_RFP);
		dicoEntites.setObjectForKey( EOCgntRaisonFamPerso.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_RFP);

		dicoControleurs.setObjectForKey( GestionCld.class.getSimpleName(), EOTypeAbsence.TYPE_CLD);
		dicoEntites.setObjectForKey( EOCld.ENTITY_NAME, EOTypeAbsence.TYPE_CLD);

		dicoControleurs.setObjectForKey( GestionCongeHospitaloUniv.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_HU_3);
		dicoEntites.setObjectForKey( "CongeAl3", EOTypeAbsence.TYPE_CONGE_HU_3);

		dicoControleurs.setObjectForKey( GestionCongeHospitaloUniv.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_HU_4);
		dicoEntites.setObjectForKey( "CongeAl4", EOTypeAbsence.TYPE_CONGE_HU_4);

		dicoControleurs.setObjectForKey( GestionCongeHospitaloUniv.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_HU_5);
		dicoEntites.setObjectForKey( "CongeAl5", EOTypeAbsence.TYPE_CONGE_HU_5);

		dicoControleurs.setObjectForKey( GestionCongeHospitaloUniv.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_HU_6);
		dicoEntites.setObjectForKey( "CongeAl6", EOTypeAbsence.TYPE_CONGE_HU_6);

		dicoControleurs.setObjectForKey( GestionCongeMaladieSansTraitement.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_MAL_SS_TRAITEMENT);
		dicoEntites.setObjectForKey( EOCongeMaladieSsTraitement.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_MAL_SS_TRAITEMENT);

		dicoControleurs.setObjectForKey( GestionCongeMaternite.class.getSimpleName(), 	EOTypeAbsence.TYPE_CONGE_MATERNITE);
		dicoEntites.setObjectForKey(EOCongeMaternite.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_MATERNITE);

		dicoControleurs.setObjectForKey( GestionCongePaternite.class.getSimpleName(), 	EOTypeAbsence.TYPE_CONGE_PATERNITE);
		dicoEntites.setObjectForKey(EOCongePaternite.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_PATERNITE);

		dicoControleurs.setObjectForKey( GestionCongeFormation.class.getSimpleName(), 	EOTypeAbsence.TYPE_CFP);
		dicoEntites.setObjectForKey( EOCongeFormation.ENTITY_NAME, EOTypeAbsence.TYPE_CFP);

		dicoControleurs.setObjectForKey( GestionCongeGardeEnfant.class.getSimpleName(), EOTypeAbsence.TYPE_CONGE_GARDE_ENFANT);
		dicoEntites.setObjectForKey(EOCongeGardeEnfant.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_GARDE_ENFANT);

		dicoControleurs.setObjectForKey( GestionCongeAdoption.class.getSimpleName(), 	EOTypeAbsence.TYPE_CONGE_ADOPTION);
		dicoEntites.setObjectForKey( EOCongeAdoption.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_ADOPTION);

		dicoControleurs.setObjectForKey( GestionCongeSolidarite.class.getSimpleName(), 	EOTypeAbsence.TYPE_CONGE_SOLIDARITE_FAMILIALE);
		dicoEntites.setObjectForKey( EOCongeSolidariteFamiliale.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_SOLIDARITE_FAMILIALE);

		dicoControleurs.setObjectForKey( GestionCcp.class.getSimpleName(), 	EOTypeAbsence.TYPE_CONGE_CCP);
		dicoEntites.setObjectForKey( "Ccp", EOTypeAbsence.TYPE_CONGE_CCP);

		dicoControleurs.setObjectForKey( GestionCongeCir.class.getSimpleName(), 	EOTypeAbsence.TYPE_CONGE_CIR);
		dicoEntites.setObjectForKey( "CongeInsufRes", EOTypeAbsence.TYPE_CONGE_CIR);

		dicoControleurs.setObjectForKey( GestionRdt.class.getSimpleName(), 	EOTypeAbsence.TYPE_CONGE_RDT);
		dicoEntites.setObjectForKey( "Rdt", EOTypeAbsence.TYPE_CONGE_RDT);

	}

	private GestionConge controleurCongePopup() {

		try {
			String nomControleur = nomControleurs[controleurTypeConge.indexSelection()];

			if (nomControleur.equals(""))
				return null;

			// le contrôleur n'a pas été instancié, on instancie un contrôleur de la bonne classe de congé et on déclenche la méthode ajouter
			try {
				Class classe = Class.forName("org.cocktail.mangue.client.conges." + nomControleur);
				try {
					// rechercher un constructeur avec un parametre le nom de l'entité
					Constructor constructor = classe.getConstructor(new Class[] {java.lang.String.class});
					GestionConge controleur = (GestionConge)constructor.newInstance(new Object[] {controleurTypeConge.nomEntiteSelectionnee()});
					return controleur;
				} catch (Exception e) {
					// on essaie avec le constructeur sans parametre
					Constructor constructor = classe.getConstructor(null);
					GestionConge controleur = (GestionConge)constructor.newInstance(null);
					return controleur;
				}
			} catch (Exception e) {
				LogManager.logException(e);
				return null;
			}
		}
		catch (Exception e) {
			return null;
		}
	}

	private GestionConge controleurConge() {

		try {
			String nomControleur = (String)dicoControleurs.objectForKey(currentAbsence().toTypeAbsence().code());
			String nomEntite = (String)dicoEntites.objectForKey(currentAbsence().toTypeAbsence().code());;

			if (nomControleur.equals(""))
				return null;

			// le contrôleur n'a pas été instancié, on instancie un contrôleur de la bonne classe de congé et on déclenche la méthode ajouter
			try {
				Class classe = Class.forName("org.cocktail.mangue.client.conges." + nomControleur);
				try {
					// rechercher un constructeur avec un parametre le nom de l'entité
					Constructor constructor = classe.getConstructor(new Class[] {java.lang.String.class});
					GestionConge controleur = (GestionConge)constructor.newInstance(new Object[] {nomEntite});
					return controleur;
				} catch (Exception e) {
					// on essaie avec le constructeur sans parametre
					Constructor constructor = classe.getConstructor(null);
					GestionConge controleur = (GestionConge)constructor.newInstance(null);
					return controleur;
				}
			} catch (Exception e) {
				LogManager.logException(e);
				return null;
			}
		}
		catch (Exception e) {
			return null;
		}
	}

	// GESTION DES ABSENCES
	private void afficherAbsences() {
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		displayGroup().setObjectArray(rechercherAbsences());
		displayGroup().setSelectedObject(null);
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	//	COMPLETER TABLE ABSENCES
	private void completerTableAbsences(NSArray absences) {
		if (absences != null) {
			java.util.Enumeration<EOAbsences> e = absences.objectEnumerator();
			while (e.hasMoreElements()) {
				EOAbsences currentAbsence = e.nextElement();
				if (currentAbsence.absDureeTotale() == null || currentAbsence.absDureeTotale().length() == 0 || currentAbsence.absDureeTotale().indexOf("(") > 0) {
					// pour gérer les anciennes données
					currentAbsence.modifierDureeAbsence();
				}
			}
		}
		calculerDureesTotales(absences);
		updaterDisplayGroups();
	}
	// GET ABSENCES
	private NSArray rechercherAbsences()	{	
		if (currentIndividu() == null || controleurTypeConge == null || controleurPeriode == null) {
			// au démarrage
			return null;
		}
		NSTimestamp dateDebut = controleurPeriode.dateDebut(),dateFin = controleurPeriode.dateFin();
		if (dateFin != null && dateDebut != null && DateCtrl.isBefore(dateFin, dateDebut)) {
			return null;
		}
		NSArray absences = null;
		String typeConge = controleurTypeConge.typeConge();
		if (typeConge != null) {
			LogManager.logDetail("Recherche des Absences de " + currentIndividu().identite() + " de type " + typeConge + "pour la periode du " + dateDebut + 
					" au " + dateFin);
			if (typeConge.equals(GestionTypeConge.TOUS_CONGES)) {
				absences = EOAbsences.rechercherAbsencesLegalesPourIndividuEtDates(editingContext(), currentIndividu(),dateDebut,dateFin);
			} else 
				absences = EOAbsences.rechercherAbsencesPourIndividuDatesEtTypeAbsence(editingContext(), currentIndividu(),dateDebut,dateFin,typeConge);

			// Filtrer les absences valides ou invalides
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOAbsences.TEM_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI));
			absences = EOQualifier.filteredArrayWithQualifier(absences, qualifier);

		}
		completerTableAbsences(absences);
		preparerInfosGardeEnfant();
		return absences;

	}

	private boolean gardeEnfantPossible() {
		//		NSArray nbEnfants = EORepartEnfant.rechercherPourParent(editingContext(), currentIndividu());
		return currentIndividu() != null && currentIndividu().personnel() != null &&
				currentIndividu().personnel().nbEnfants() != null && currentIndividu().personnel().nbEnfants().intValue() > 0;
	}

	private void preparerInfosGardeEnfant() {
		if (controleurPeriode == null) {
			return;
		}
		if (vueBoutonsGardeEnfant.isVisible()) {
			String typeAnnee = EOGrhumParametres.getAnneeRefGardeEnfant();
			Integer annee;
			if (controleurPeriode.debutPeriode() != null) {
				if (typeAnnee.equals(EOCongeGardeEnfant.ANNEE_CIVILE)) {
					annee = new Integer(controleurPeriode.debutPeriode().substring(6));
				} else {
					GregorianCalendar date = new GregorianCalendar();
					date.setTime(DateCtrl.stringToDate(controleurPeriode.debutPeriode()));
					int anneeNum = date.get(Calendar.YEAR);
					if (date.get(Calendar.MONTH) >= 8)	// les mois commencent à zéro
						annee = new Integer(anneeNum + 1);
					else
						annee = new Integer(anneeNum);

				}
			} else {
				if (typeAnnee.equals(EOCongeGardeEnfant.ANNEE_CIVILE)) {
					annee = new Integer(Outils.anneeCivile());
				} else {
					annee = new Integer(Outils.anneeUniversitaire());
				}
			}

			EODroitsGardeEnfant droit = EODroitsGardeEnfant.rechercherDroitsGardePourIndividuEtAnnee(editingContext(),currentIndividu(),annee);
			if (droit != null) {
				anneeDroits = droit.annee().toString();
				nbDemiJournees = droit.nbDemiJournees().toString();
			} else {
				anneeDroits = "";
				nbDemiJournees = "?";
			}
		} else {
			anneeDroits = "";
			nbDemiJournees = "";
		}
	}


	private Conge trouverConge() {

		Conge conge = Conge.rechercherCongeAvecAbsence(editingContext(), (String)dicoEntites.objectForKey(currentAbsence().toTypeAbsence().code()), currentAbsence());
		if (conge == null) {
			EODialogs.runErrorDialog("Erreur","Pas de congé associé à cette absence !");	
		}
		return conge;

	}


	// Classe d'ecoute pour les doubleclics dans la table view  */
	public class DoubleClickListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() >= 2)
				afficherInspecteur();
		}
	}

	/**
	 * 
	 * @return
	 */
	public String typeDureeTotale() {
		if ( controleurTypeConge != null && controleurTypeConge.typeConge() != null && controleurTypeConge.typeConge().equals("ENFANT")  )
			return "1/2 Journées";

		return (dureeTotale > 1.0)?"Jours ":"Jour";
	}

	/**
	 * 
	 * @param absences
	 */
	private void calculerDureesTotales(NSArray absences) {

		if (absences == null)
			return;

		dureeTotale = 0;
		dureePeriode = 0;
		for (java.util.Enumeration<EOAbsences> e = absences.objectEnumerator();e.hasMoreElements();) {
			EOAbsences absence = e.nextElement();
			if (absence.absDureeTotale() == null || absence.absDureeTotale().length() == 0 || absence.absDureeTotale().indexOf("(") > 0) {
				// pour gérer les anciennes données
				dureeTotale += absence.calculerDureeAbsence();
			} else
				// avec Mangue la durée totale est calculée lors de la gestion de l'absence
				dureeTotale += new Float(absence.absDureeTotale()).floatValue();

			if (controleurPeriode.dateDebut() == null || controleurPeriode.dateFin() == null)
				dureePeriode = dureeTotale;
			else
				dureePeriode += absence.calculerDureeAbsencePourPeriode(controleurPeriode.dateDebut(), controleurPeriode.dateFin());
		}
	}
}


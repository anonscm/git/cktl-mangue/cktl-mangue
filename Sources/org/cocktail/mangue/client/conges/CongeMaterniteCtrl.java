/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.conges;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.conges.CongeMaterniteView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.modalites_services.ModalitesServicesCtrl;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.select.TypeCgMaternSelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeCgMatern;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaternite;
import org.cocktail.mangue.modele.mangue.conges.EODeclarationMaternite;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

/** Gere les conges de maternite<BR>
 * Le conge de maternite est associe a une declaration de maternite. 
 * Lorsqu'on cree un nouveau conge de maternite, on cree la declaration de maternite.
 * On commence par definir un conge de type Maternite<BR>
 * L'ajout de conges maternite lies a cette meme declaration se fait depuis 
 * la fenetre geree par ce controleur.<BR>
 * Les conges de maternite pouvant etre fractionnes en cas d'hospitalisation (pour une femme). 
 * L'utilisateur doit modifier les dates de debut et de fin du conge principal, puis ajouter un conge avant de valider
 * pour avoir une duree globale sur plusieurs conges correspondant aux regles de gestion<BR>
 * Regles de gestion<BR>
 * Les conges de maternite peuvent etre accordes aux hommes en cas de deces de la mere a l'accouchement<BR>
 * Le type de conge, la date debut et la date fin doivent etre fournis<BR>
 * Une grossesse non menee a terme ne peut etre que de type "conge de type Etat pathologique du a la grossesse"<BR>
 * Un conge de type Etat pathologique du a la grossesse ne peut etre pris qu'avant le debut du conge maternite.<BR>
 * Un conge de type Etat pathologique suite a l'accouchement ne peut etre saisi que si un conge maternite a ete saisi avec la date d'accouchement.<BR>
 * Un conge de maternite prolonge une duree de stage, si l'agent est en stage pendant cette periode.<BR>
 * La declaration d'un conge de maternite ne peut etre annulee que si un arrete a ete produit. Dans ce cas,
 * on ne peut plus associe de conge a cette declaration. Dans le cas contraire (pas d'arrete), on peut supprimer le conge et sa declaration<BR>
 * Verification de dates<BR>
 * <UL>conge de type Etat pathologique du a la grossesse : la date de constat et date de naissance previsionnelle doivent etre fournies</UL>
 * <UL>conge de type Etat pathologique suite a l'accouchement : la date d'accouchement doit etre fournie</UL>
 * <UL>conge de type Maternite : la date de constat et la date de naissance previsionnelle doivent etre fournies (pour une femme) ou la date de constat et la date d'accouchement</UL>
 * Le conge de type Etat pathologique du a la grossesse est de 2 semaines<BR>
 * Le conge de type Etat pathologique suite a l'accouchement est de 4 semaines<BR>
 * Pour la naissance du 1er ou du 2eme enfant, le conge de maternite est de 16 semaines,<BR>
 * Pour la naissance du 3eme enfant et plus, le conge de maternite est de 26 semaines<BR>
 * Pour la naissance de jumeaux, le conge de maternite est de 34 semaines<BR>
 * Pour la naissance de triples ou plus, le conge de maternite est de 46 semaines<BR>
 * En cas d'accouchement apres la date presumee, la periode postnatale n'est pas reduite => la date de fin est repoussee du nombre de jours entre la date d'accouchement et la date presumee.<BR>
 * Les regles de validation sont dans la classe EOCongeMaternite<BR>
 * Ce controleur peut etre instancie depuis GestionAbsences ou depuis lui-meme (quand on ajoute des conges pour une meme
 * declaration de maternite). Dans ce dernier cas, on travaille dans le meme editingContext<BR>
 * @author christine<BR>
 *
 */

public class CongeMaterniteCtrl
{
	private static CongeMaterniteCtrl sharedInstance;

	private EOEditingContext ec;		
	private CongeMaterniteView myView;

	private int reliquatConge = 0;
	private boolean modeCreation;

	private EOCongeMaternite currentConge;
	private EODeclarationMaternite currentDeclaration;

	public CongeMaterniteCtrl (EOEditingContext globalEc) {

		super();

		myView = new CongeMaterniteView(new JFrame(), true);

		ec = globalEc;

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}}
		);
		myView.getBtnAddEtatPathologique().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {ajouterEtatPathologique();}}
		);
		myView.getBtnGetTypeDeclaration().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getTypeCongeMaternite();}}
		);
		myView.getBtnImprimerArrete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerArrete();}}
		);

		CocktailUtilities.initTextField(myView.getTfTypeDeclaration(), false, false);

		myView.getTfNoArrete().addActionListener(new ActionListenerNoArrete());
		myView.getTfNoArrete().addFocusListener(new FocusListenerNoArrete());

		myView.getPopupEnfantsACharge().addActionListener(new ActionListenerEnfantsACharge());
		myView.getCheckJumeaux().addActionListener(new ActionListenerJumeaux());
		myView.getCheckTriples().addActionListener(new ActionListenerTriples());
		myView.getCheckSigne().addActionListener(new ActionListenerTemSigne());

		myView.getTfDateConstatation().addFocusListener(new FocusListenerDateConstatation());
		myView.getTfDateConstatation().addActionListener(new ActionListenerDateConstatation());

		myView.getTfDateAccouchementPrev().addFocusListener(new FocusListenerDatePrevisionnelle());
		myView.getTfDateAccouchementPrev().addActionListener(new ActionListenerDatePrevisionnelle());

		myView.getTfDateAccouchementReelle().addFocusListener(new FocusListenerDateReelle());
		myView.getTfDateAccouchementReelle().addActionListener(new ActionListenerDateReelle());

		myView.getTfDebut().addFocusListener(new FocusListenerDateDebut());
		myView.getTfDebut().addActionListener(new ActionListenerDateDebut());

		myView.getTfFin().addFocusListener(new FocusListenerDateFin());
		myView.getTfFin().addActionListener(new ActionListenerDateFin());

		myView.getTfDateArrete().addFocusListener(new FocusListenerDateArrete());
		myView.getTfDateArrete().addActionListener(new ActionListenerDateArrete());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CongeMaterniteCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CongeMaterniteCtrl(editingContext);
		return sharedInstance;
	}			


	private EOCongeMaternite currentConge() {
		return currentConge;
	}
	private EODeclarationMaternite currentDeclaration() {
		return currentDeclaration;
	}


	/**
	 * 
	 *
	 */
	private void clearTextFields()	{

		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		myView.getTfTypeDeclaration().setText("");

		myView.getTfDateConstatation().setText("");
		myView.getTfDateAccouchementPrev().setText("");
		myView.getTfDateAccouchementReelle().setText("");

		myView.getPopupEnfantsACharge().setSelectedIndex(0);

		myView.getCheckJumeaux().setSelected(false);
		myView.getCheckTriples().setSelected(false);
		myView.getCheckGrossesseInterrompue().setSelected(false);

	}

	public void ajouterEtatPathologique() {

		EOTypeCgMatern type = TypeCgMaternSelectCtrl.sharedInstance(ec).getType();
		if (type != null) {

			currentConge = EOCongeMaternite.creer(ec, currentDeclaration, type);
			CocktailUtilities.setTextToField(myView.getTfTypeDeclaration(), type.libelleLong());

			modeCreation = true;

			updateData();
			updateUI();

		}
	}

	public void ajouter(EOIndividu individu) {

		modeCreation = true;

		currentDeclaration = EODeclarationMaternite.creer(ec, individu);
		currentConge = EOCongeMaternite.creer(ec, currentDeclaration, EOTypeCgMatern.getTypeCongeMaternite(ec));

		updateData();
		updateUI();

		myView.setVisible(true);

	}

	public void modifier(EOCongeMaternite conge)	{

		modeCreation = false;

		currentConge = conge;
		currentDeclaration = currentConge.declarationMaternite();

		actualiser();
		myView.setVisible(true);

	}

	private void actualiser() {
		updateData();
	}

	private void getTypeCongeMaternite() {
		EOTypeCgMatern type = TypeCgMaternSelectCtrl.sharedInstance(ec).getType();
		if (type != null) {
			currentConge.setToTypeCgMaternRelationship(type);
			CocktailUtilities.setTextToField(myView.getTfTypeDeclaration(), type.libelleLong());
			updateUI();
		}
	}


	private void updateData() {

		clearTextFields();

		if (currentConge.dateDebut() != null)
			myView.getTfDebut().setText(DateCtrl.dateToString(currentConge.dateDebut()));

		if (currentConge.dateFin() != null)
			myView.getTfFin().setText(DateCtrl.dateToString(currentConge.dateFin()));

		if (currentConge.toTypeCgMatern() != null)
			myView.getTfTypeDeclaration().setText(currentConge.toTypeCgMatern().libelleLong());

		if (currentDeclaration.dConstatMatern() != null)
			myView.getTfDateConstatation().setText(DateCtrl.dateToString(currentDeclaration.dConstatMatern()));

		if (currentDeclaration.dAccouchement() != null)
			myView.getTfDateAccouchementReelle().setText(DateCtrl.dateToString(currentDeclaration.dAccouchement()));

		if (currentDeclaration.dNaisPrev() != null)
			myView.getTfDateAccouchementPrev().setText(DateCtrl.dateToString(currentDeclaration.dNaisPrev()));

		myView.getPopupEnfantsACharge().setSelectedItem(currentDeclaration.nbEnfantsDecl());

		myView.getCheckSigne().setSelected(currentConge.temConfirme().equals("O"));
		myView.getCheckJumeaux().setSelected(currentDeclaration.temGrossesseGemellaire().equals("O"));
		myView.getCheckTriples().setSelected(currentDeclaration.temGrossesseTriple().equals("O"));
		myView.getCheckSansTraitement().setSelected(currentConge.temCgSansTrait().equals("O"));

		if (currentConge.commentaire() != null)
			myView.getTfCommentaires().setText(currentConge.commentaire());

		updateUI();

	}


	private void valider() {

		try {

			// CONGE
			if (myView.getTfDebut().getText().length() > 0)
				currentConge.setDateDebut(DateCtrl.stringToDate(myView.getTfDebut().getText()));

			if (myView.getTfFin().getText().length() > 0)
				currentConge.setDateFin(DateCtrl.stringToDate(myView.getTfFin().getText()));

			if (myView.getTfDateArrete().getText().length() > 0)
				currentConge.setDateArrete(DateCtrl.stringToDate(myView.getTfDateArrete().getText()));

			if (myView.getTfNoArrete().getText().length() > 0)
				currentConge.setNoArrete(myView.getTfNoArrete().getText());

			if (myView.getTfCommentaires().getText().length() > 0)
				currentConge.setCommentaire(myView.getTfCommentaires().getText());

			currentConge.setTemCgSansTrait((myView.getCheckSansTraitement().isSelected())?"O":"N");

			// DECLARATION
			if (myView.getTfDateConstatation().getText().length() > 0)
				currentDeclaration.setDConstatMatern(DateCtrl.stringToDate(myView.getTfDateConstatation().getText()));

			if (myView.getTfDateAccouchementPrev().getText().length() > 0)
				currentDeclaration.setDNaisPrev(DateCtrl.stringToDate(myView.getTfDateAccouchementPrev().getText()));

			if (myView.getTfDateAccouchementReelle().getText().length() > 0)
				currentDeclaration.setDAccouchement(DateCtrl.stringToDate(myView.getTfDateAccouchementReelle().getText()));

			// CREATION DE L ABSENCE
			EOAbsences absence = EOAbsences.creer(ec, currentConge.individu(), (EOTypeAbsence)NomenclatureFinder.findForCode(ec, EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_MATERNITE));
			absence.setDateDebut(currentConge.dateDebut());
			absence.setDateFin(currentConge.dateFin());

			currentConge.setAbsenceRelationship(absence);

			ec.saveChanges();

			myView.dispose();

		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());

			currentConge.setTemConfirme("O");
			updateUI();

			myView.toFront();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}


	private void annuler()	{

		ec.revert();
		myView.dispose();

	}


	/**
	 * 
	 */
	public void imprimerArrete() {

		NSArray<EOGlobalID >destinatairesGids = DestinatairesSelectCtrl.sharedInstance(ec).getDestinatairesGlobalIds();
		if (destinatairesGids  != null) {
			try {				
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class, Boolean.class};
				Object[] parametres = new Object[]{ec.globalIDForObject(currentConge()), destinatairesGids, false};
				UtilitairesImpression.imprimerSansDialogue(ec,"clientSideRequestImprimerArreteConge",classeParametres,parametres,"ArreteMaternite" + currentConge().individu().noIndividu() ,"Impression de l'arrêté de maternité");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	/**
	 * Calcul des dates du conge maternite en fonction de la date previsionnelle d'accouchement,
	 * du nombre d'enfants à charge et du type de grossesse (Jumeaux ou Triples)
	 */	
	private void preparerDates() {

		if (currentDeclaration() != null && currentDeclaration().dNaisPrev() != null) {

			int nbEnfants = 0;

			if (currentDeclaration().nbEnfantsDecl() != null)
				nbEnfants = currentDeclaration().nbEnfantsDecl().intValue();

			int dureePrenatale = EOCongeMaternite.NB_JOURS_NORMAL_PRENATAL;					// 6 semaines
			if (currentDeclaration().estGrossesseGemellaire()) {
				dureePrenatale = EOCongeMaternite.NB_JOURS_JUMEAUX_PRENATAL;				// 12 semaines
			} else if (currentDeclaration().estGrossesseTriple()) {
				dureePrenatale = EOCongeMaternite.NB_JOURS_TRIPLES_PRENATAL;				// 24 semaines
			} else if (nbEnfants >= 2) {
				dureePrenatale = EOCongeMaternite.NB_JOURS_2ENFANTS_OU_PLUS_PRENATAL;		// 8 semaines
			}

			currentConge().setDateDebut(DateCtrl.dateAvecAjoutJours(currentDeclaration().dNaisPrev(),-dureePrenatale));
			myView.getTfDebut().setText(DateCtrl.dateToString(currentConge().dateDebut()));

			preparerDateFin();

		} 

	}

	private void preparerDateFin() {

		int nbJoursAAjouter = 0;
		if (currentConge().estTypePathologieGrossesse() || currentConge().estTypePathologieAccouchement()) {
			nbJoursAAjouter = currentConge().dureeLegale() - currentConge().dureeTotaleCongeMemeType(false);
		} else {
			if (reliquatConge > 0) {	// congé de maternité fractionné
				nbJoursAAjouter = reliquatConge;
			} else {
				if (currentDeclaration() != null && !currentDeclaration().estGrossesseInterrompue()) {
					if (currentDeclaration().estGrossesseGemellaire()) {
						if (currentConge().individu().estHomme()) {
							nbJoursAAjouter = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;// 22 semaines
						} else {
							nbJoursAAjouter = EOCongeMaternite.NB_JOURS_JUMEAUX_FEMME;				// 34 semaines
						}	
					} else if (currentDeclaration().estGrossesseTriple()) {
						if (currentConge().individu().estHomme()) {
							nbJoursAAjouter = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;	// 22 semaines
						} else {
							nbJoursAAjouter = EOCongeMaternite.NB_JOURS_TRIPLES_FEMME;					// 46 semaines
						}
					} else if ( ((Integer)myView.getPopupEnfantsACharge().getSelectedItem()).intValue() >= 2) {
						if (currentConge().individu().estHomme()) {
							nbJoursAAjouter = EOCongeMaternite.NB_JOURS_2ENFANTS_OU_PLUS_POSTNATAL;	// 18 semaines
						} else {
							nbJoursAAjouter = EOCongeMaternite.NB_JOURS_2ENFANTS_OU_PLUS_FEMME;		// 26 semaines
						}
					} else {
						if (currentConge().individu().estHomme()) {
							nbJoursAAjouter = EOCongeMaternite.NB_JOURS_NORMAL_POSTNATAL;			// 10 semaines
						} else {
							nbJoursAAjouter = EOCongeMaternite.NB_JOURS_NORMAL_FEMME;				// 17 semaines
						}
					}
				}
			}
		}
		if (nbJoursAAjouter > 0) {
			currentConge().setDateFin(DateCtrl.dateAvecAjoutJours(currentConge().dateDebut(),nbJoursAAjouter-1));
			myView.getTfFin().setText(DateCtrl.dateToString(currentConge().dateFin()));
		}

	}


	/**
	 * Mise a jour de l'interface.
	 * Activation (ou non) de tous les elements de l'interface.
	 */
	private void updateUI() {

		myView.getCheckSigne().setEnabled(!modeCreation && !currentConge.estSigne());

		myView.getCheckSigne().setEnabled(
				!currentConge.estSigne()
				&& !StringCtrl.chaineVide(myView.getTfDateAccouchementPrev().getText())
				&& !StringCtrl.chaineVide(myView.getTfNoArrete().getText())
				&& !StringCtrl.chaineVide(myView.getTfDateConstatation().getText())
				&& !StringCtrl.chaineVide(myView.getTfDebut().getText())
				&& !StringCtrl.chaineVide(myView.getTfFin().getText()));

		myView.getBtnImprimerArrete().setEnabled(!modeCreation);
		myView.getBtnGetTypeDeclaration().setEnabled(!currentConge.toTypeCgMatern().estMaternite());//!currentConge.estSigne() && !modeCreation && !currentConge.estSigne());

		// L'etat pathologique ne peut etre ajoute que sur un conge maternite
		myView.getBtnAddEtatPathologique().setEnabled(
				!modeCreation 
				&& currentConge.toTypeCgMatern().estMaternite()
				&& currentConge.toTypeCgMatern().estPathologieGrossesse());

		CocktailUtilities.initTextField(myView.getTfDateArrete(), 
				false,
				!currentConge.estSigne());

		CocktailUtilities.initTextField(myView.getTfNoArrete(), 
				false,
				!currentConge.estSigne());

		CocktailUtilities.initTextField(myView.getTfDateConstatation(), 
				false,
				!currentConge.estSigne());

		CocktailUtilities.initTextField(myView.getTfDateAccouchementPrev(), 
				false,
				!currentConge.estSigne());

		CocktailUtilities.initTextField(myView.getTfDateAccouchementReelle(), 
				!StringCtrl.chaineVide(myView.getTfDateAccouchementPrev().getText()), 
				!currentConge.estSigne() &&  !StringCtrl.chaineVide(myView.getTfDateAccouchementPrev().getText()));

		// La date de debut est fonction de la date previsionnelle d'accouchement
		CocktailUtilities.initTextField(myView.getTfDebut(), 
				StringCtrl.chaineVide(myView.getTfDateAccouchementPrev().getText()), 
				!currentConge.estSigne() &&  !StringCtrl.chaineVide(myView.getTfDateAccouchementPrev().getText()));

		// On ne saisit jamais la date de fin
		CocktailUtilities.initTextField(myView.getTfFin(), 
				StringCtrl.chaineVide(myView.getTfDebut().getText()) || StringCtrl.chaineVide(myView.getTfDateAccouchementPrev().getText()), 
				false);

	}


	private class ActionListenerEnfantsACharge implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{


			currentDeclaration.setNbEnfantsDecl((Integer)myView.getPopupEnfantsACharge().getSelectedItem());
			preparerDates();
		}
	}
	private class ActionListenerJumeaux implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if (myView.getCheckJumeaux().isSelected()) {
				myView.getCheckTriples().setSelected(false);
			}

			currentDeclaration.setTemGrossesseGemellaire((myView.getCheckJumeaux().isSelected())?"O":"N");
			preparerDates();
		}
	}
	private class ActionListenerTriples implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if (myView.getCheckTriples().isSelected()) {
				myView.getCheckJumeaux().setSelected(false);
			}

			currentDeclaration.setTemGrossesseTriple((myView.getCheckTriples().isSelected())?"O":"N");
			preparerDates();

		}
	}


	private class ActionListenerTemSigne implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{

			if (EODialogs.runConfirmOperationDialog("Attention","Confirmez-vous la validation de ce congé ?\n" +
					"Les données ne seront ensuite plus modifiables.","Oui","Non")) {

				currentConge.setTemConfirme("O");
				updateUI();

			}
		}
	}



	private class ActionListenerDateConstatation implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateConstatation().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateConstatation().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateConstatation().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDateConstatation().setText(myDate);
				updateUI();
			}	
		}
	}
	private class FocusListenerDateConstatation implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateConstatation().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateConstatation().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateConstatation().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDateConstatation().setText(myDate);
				updateUI();
			}	
		}
	}

	private class ActionListenerDatePrevisionnelle implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateAccouchementPrev().getText())) {
				currentDeclaration.setDNaisPrev(null);
				updateUI();
				return;
			}

			String myDate = DateCtrl.dateCompletion(myView.getTfDateAccouchementPrev().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateAccouchementPrev().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDateAccouchementPrev().setText(myDate);
				currentDeclaration.setDNaisPrev(DateCtrl.stringToDate(myDate));
				preparerDates();
				updateUI();
			}
		}
	}
	private class FocusListenerDatePrevisionnelle implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateAccouchementPrev().getText())){
				currentDeclaration.setDNaisPrev(null);
				updateUI();
				return;
			}
			String myDate = DateCtrl.dateCompletion(myView.getTfDateAccouchementPrev().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateAccouchementPrev().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDateAccouchementPrev().setText(myDate);
				currentDeclaration.setDNaisPrev(DateCtrl.stringToDate(myDate));
				preparerDates();
				updateUI();
			}
		}
	}

	private class ActionListenerDateReelle implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateAccouchementReelle().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateAccouchementReelle().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateAccouchementReelle().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfDateAccouchementReelle().setText(myDate);
		}
	}
	private class FocusListenerDateReelle implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateAccouchementReelle().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateAccouchementReelle().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateAccouchementReelle().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfDateAccouchementReelle().setText(myDate);
		}
	}


	private class ActionListenerDateDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDebut().getText()))	{
				updateUI();
				return;
			}
			String myDate = DateCtrl.dateCompletion(myView.getTfDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDebut().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDebut().setText(myDate);
				currentConge.setDateDebut(DateCtrl.stringToDate(myDate));
				updateUI();
			}
		}
	}
	private class FocusListenerDateDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDebut().getText()))	{
				updateUI();
				return;
			}
			String myDate = DateCtrl.dateCompletion(myView.getTfDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDebut().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDebut().setText(myDate);
				currentConge.setDateDebut(DateCtrl.stringToDate(myDate));
				updateUI();
			}
		}
	}



	private class ActionListenerDateFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfFin().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfFin().setText(myDate);
		}
	}
	private class FocusListenerDateFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfFin().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfFin().setText(myDate);
		}
	}


	private class ActionListenerDateArrete implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateArrete().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateArrete().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateArrete().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfDateArrete().setText(myDate);
		}
	}
	private class FocusListenerDateArrete implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateArrete().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateArrete().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateArrete().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfDateArrete().setText(myDate);
		}
	}


	private class ActionListenerNoArrete implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			updateUI();
		}
	}
	private class FocusListenerNoArrete implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			updateUI();
		}
	}



}
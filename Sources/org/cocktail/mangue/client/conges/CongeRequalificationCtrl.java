// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFicheIdentiteCtrl.java

package org.cocktail.mangue.client.conges;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.conges.CongeRequalificationView;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.EOCgntCgm;
import org.cocktail.mangue.modele.mangue.conges.EOCld;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class CongeRequalificationCtrl {
	
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static CongeRequalificationCtrl sharedInstance;
	private EOEditingContext edc;

	private EODisplayGroup eod = new EODisplayGroup();
	private CongeRequalificationView myView;
	private EOAbsences currentAbsence;

	public CongeRequalificationCtrl(EOEditingContext edc)
	{
		setEdc(edc);
		myView = new CongeRequalificationView(new JFrame(), eod, true);
		myView.getBtnRequalifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {requalifier();}}
		);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){annuler();}}
		);
		myView.getBtnSupprimerConge().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt){supprimerAbsence();}}
		);

		myView.getTfDateDebut().addFocusListener(new FocusListenerDateDebut());
		myView.getTfDateDebut().addActionListener(new ActionListenerDateDebut());

		myView.getTfDateFin().addFocusListener(new FocusListenerDateFin());
		myView.getTfDateFin().addActionListener(new ActionListenerDateFin());

		myView.getTfDateAvisComiteMedical().addFocusListener(new FocusListenerDateComite());
		myView.getTfDateAvisComiteMedical().addActionListener(new ActionListenerDateComite());

		myView.getMyEOTable().addListener(new ListenerAbsences());

	}

	public static CongeRequalificationCtrl sharedInstance(EOEditingContext edc)
	{
		if(sharedInstance == null)
			sharedInstance = new CongeRequalificationCtrl(edc);
		return sharedInstance;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOAbsences getCurrentAbsence() {
		return currentAbsence;
	}

	public void setCurrentAbsence(EOAbsences currentAbsence) {
		this.currentAbsence = currentAbsence;
	}

	private void clearDatas() {
		myView.getTfDateDebut().setText("");
		myView.getTfDateFin().setText("");
		myView.getTfDateAvisComiteMedical().setText("");
	}

	public void open(EOIndividu individu, NSArray absences) {

		myView.getTfTitre().setText(individu.identitePrenomFirst() + " - Requalification Congés");
		clearDatas();
		
		NSArray absencesTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(absences,new NSArray(EOSortOrdering.sortOrderingWithKey(EOAbsences.DATE_DEBUT_KEY, EOSortOrdering.CompareAscending)));

		eod.setObjectArray(absencesTriees);
		myView.getMyEOTable().updateData();

		myView.getCheckCld().setEnabled(getCurrentAbsence().toTypeAbsence().estCongeMaladie() && getCurrentAbsence().toTypeAbsence().estCongePourFonctionnaire());
		myView.getCheckClm().setEnabled(getCurrentAbsence().toTypeAbsence().estCongeMaladie() && getCurrentAbsence().toTypeAbsence().estCongePourFonctionnaire());
		myView.getCheckCgm().setEnabled(getCurrentAbsence().toTypeAbsence().estCongeMaladieContractuel());
	
		calculerDatesRequalification();

		if (currentAbsence.toTypeAbsence().estCongeMaladieContractuel())
			myView.getCheckCgm().setSelected(true);
							
		updateInterface();

		myView.setVisible(true);

	}

	private void supprimerAbsence() {
		eod.deleteSelection();
		myView.getMyEOTable().updateData();

		calculerDatesRequalification();
		updateInterface();
	}

	/**
	 * Calcul par défaut des dates de requalification
	 */
	private void calculerDatesRequalification() {

		if (eod.displayedObjects().count() > 0) {

			EOAbsences premiereAbsence = (EOAbsences)eod.displayedObjects().objectAtIndex(0);
			EOAbsences derniereAbsence = (EOAbsences)eod.displayedObjects().objectAtIndex(eod.displayedObjects().count() - 1);

			myView.getTfDateDebut().setText(DateCtrl.dateToString(premiereAbsence.dateDebut()));
			myView.getTfDateFin().setText(DateCtrl.dateToString(derniereAbsence.dateFin()));

			if (premiereAbsence.toTypeAbsence().estCongeMaladie()) {
				myView.getCheckClm().setSelected(true);
			}
			else
				if (premiereAbsence.toTypeAbsence().estCongeLongueMaladie())
					myView.getCheckCld().setSelected(true);

		}
	}
	
	/**
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	private boolean validerDurees(NSTimestamp dateDebut , NSTimestamp dateFin) {
		
		if (myView.getCheckClm().isSelected()) {
			int nbMois = DateCtrl.calculerDureeEnMois(dateDebut,dateFin,true).intValue();
			Integer nbMoisMin = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_DUREE_NON_BLOQUANTE);			
			if (nbMoisMin != null && (nbMois < nbMoisMin.intValue()) ) {
				EODialogs.runInformationDialog("ATTENTION","La durée minimum d'un congé de longue maladie est de " + nbMoisMin + " mois !");
			}
		}		
		
		return true;
	}

	/**
	 * 
	 */
	private void requalifier() {

		String message = "Etes-vous sûr de vouloir requalifier ces congés";

		if (myView.getCheckClm().isSelected())
			message.concat(" en CLM ?\n (Tous les congés maladie sélectionnés seront supprimés et transformés en CLM)");
		else
			if (myView.getCheckCld().isSelected())
				message.concat(" en CLD ?\n (Tous les congés maladie sélectionnés seront supprimés et transformés en CLD)");
			else
				if (myView.getCheckCgm().isSelected())
					message.concat(" en CGM ?\n (Tous les congés maladie sélectionnés seront supprimés et transformés en CGM)");
					
		if (!EODialogs.runConfirmOperationDialog("Attention",message,"Oui","Non"))
			return;

		try {

			NSTimestamp dateDebut = DateCtrl.stringToDate(myView.getTfDateDebut().getText());
			NSTimestamp dateFin = DateCtrl.stringToDate(myView.getTfDateFin().getText());
			
			validerDurees(dateDebut, dateFin);
			
			NSTimestamp dateComite = DateCtrl.stringToDate(myView.getTfDateAvisComiteMedical().getText());
			
			myView.getTfMessage().setText("");
			
			if (myView.getCheckClm().isSelected() && StringCtrl.chaineVide(myView.getTfDateAvisComiteMedical().getText())) {
				myView.getTfMessage().setText("Vous devez renseigner la date de l'avis du comité médical !");
				return;
			}
			
			NSArray absencesTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(eod.displayedObjects(),new NSArray(EOSortOrdering.sortOrderingWithKey(EOAbsences.DATE_DEBUT_KEY, EOSortOrdering.CompareAscending)));

			if (myView.getCheckClm().isSelected())
				EOClm.requalifier(getEdc(), absencesTriees, dateDebut, dateFin, dateComite);
			else
				if (myView.getCheckCld().isSelected())
					EOCld.requalifier(getEdc(), absencesTriees, dateDebut, dateFin, dateComite);	
				else
					if (myView.getCheckCgm().isSelected())
						EOCgntCgm.requalifier(getEdc(), absencesTriees, dateDebut, dateFin, dateComite);	

			getEdc().saveChanges();
			
			if (myView.getCheckClm().isSelected())
				EODialogs.runInformationDialog("OK", "Les congés sélectionnés ont bien été requalifiés en CLM !");
			else
				if (myView.getCheckCld().isSelected())
					EODialogs.runInformationDialog("OK", "Les congés sélectionnés ont bien été requalifiés en CLM !");
				else
					EODialogs.runInformationDialog("OK", "Les congés sélectionnés ont bien été requalifiés en CGM !");
				
			myView.setVisible(false);
		}
		catch(com.webobjects.foundation.NSValidation.ValidationException ex) {
			EODialogs.runInformationDialog("ERREUR", ex.getMessage());
			return;
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void annuler() {
		getEdc().revert();
		myView.setVisible(false);
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnRequalifier().setEnabled(eod.displayedObjects().size() > 0);

		CocktailUtilities.initTextField(myView.getTfDateDebut(), false, getCurrentAbsence() != null && !getCurrentAbsence().toTypeAbsence().estCongeLongueDuree());
		CocktailUtilities.initTextField(myView.getTfDateFin(), false, getCurrentAbsence() != null && !getCurrentAbsence().toTypeAbsence().estCongeLongueDuree());
		
		CocktailUtilities.initTextField(myView.getTfDateAvisComiteMedical(), false, getCurrentAbsence() != null);

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerAbsences implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}

		public void onSelectionChanged() {
			currentAbsence = (EOAbsences)eod.selectedObject();
			updateInterface();
		}
	}

	private class ActionListenerDateDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateDebut().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de début n'est pas valide !");
				myView.getTfDateDebut().selectAll();
			}
			else	{
				myView.getTfDateDebut().setText(myDate);
			}
		}
	}

	private class FocusListenerDateDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateDebut().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de certification saisie n'est pas valide !");
				myView.getTfDateDebut().selectAll();
			}
			else
				myView.getTfDateDebut().setText(myDate);
		}
	}



	private class ActionListenerDateFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin saisie n'est pas valide !");
				myView.getTfDateFin().selectAll();
			}
			else	{
				myView.getTfDateFin().setText(myDate);
			}
		}
	}

	private class FocusListenerDateFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin saisie n'est pas valide !");
				myView.getTfDateFin().selectAll();
			}
			else
				myView.getTfDateFin().setText(myDate);
		}
	}

	

	private class ActionListenerDateComite implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateAvisComiteMedical().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateAvisComiteMedical().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateAvisComiteMedical().selectAll();
			}
			else	{
				myView.getTfDateAvisComiteMedical().setText(myDate);
			}
		}
	}

	private class FocusListenerDateComite implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}

		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateAvisComiteMedical().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateAvisComiteMedical().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateAvisComiteMedical().selectAll();
			}
			else
				myView.getTfDateAvisComiteMedical().setText(myDate);
		}
	}


}

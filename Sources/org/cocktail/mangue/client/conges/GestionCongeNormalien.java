/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import java.awt.Font;

import org.cocktail.mangue.client.outils_interface.GestionCongeAvecArrete;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.CongeNormalien;

import com.webobjects.eointerface.swing.EOTextArea;
import com.webobjects.foundation.NSArray;

public abstract class GestionCongeNormalien extends GestionCongeAvecArrete {
	private NSArray autresCongesMemeType;
	public EOTextArea vueMotif;

	/** Constructeur
	 * @param nomEntite	nom entite de conge
	 * @param titreFenetre titre du dialogue affiche
	 */
	public GestionCongeNormalien(String nomEntite,String titreFenetre) {
		super(nomEntite,titreFenetre);
	}
	public void connectionWasEstablished() {
		super.connectionWasEstablished();
		if (vueMotif != null) {
			Font font = vueMotif.getFont();
			vueMotif.setFont(new Font(font.getFontName(),font.getStyle(),11));
		}
	}
	// Méthodes du Controller DG
	public String compteurMotif() {
		if (currentConge() != null && currentConge().motif() != null) {
			return "" + currentConge().motif().length() + "/2000";
		} else {
			return "";
		}
	}
	public int nbCongesMemeType() {
		if (typeTraitement() == MODE_CREATION) {
			return autresCongesMemeType.count() ;
		} else {
			return autresCongesMemeType.count() - 1;	// Ne pas prendre en compte ce congé
		}
	}
	public Integer nbsMois() {
		if (currentConge().dateDebut() == null || currentConge().dateFin() == null) {
			return null;
		} else {
			return DateCtrl.calculerDureeEnMois(currentConge().dateDebut(), currentConge().dateFin(),true);
		}
	}
	public boolean peutValider() {
		return super.peutValider() && currentConge().dateFin() != null;
	}
	/** Pour determiner le nombre de conges */
	public void afficherFenetre() {
		autresCongesMemeType = Conge.rechercherDureesPourIndividu(editingContext(), entityName(), absenceCourante().toIndividu());
		super.afficherFenetre();
	}
	protected void preparerPourRemplacement() {
		// Pas de congé de remplacement
	}
	// Gestion des arrêtés : pas de gestion
	protected boolean supporteSignature() {
		return false;
	}
	protected EOTypeGestionArrete typeGestionArrete() {
		return null;
	}
	protected void traitementsApresSuppression() {
		// pas de traitement spécifique
	}

	protected boolean traitementsAvantSuppression() {
		//pas de traitement spécifique
		return true;
	}
	protected void traitementsApresValidation() {
		// Pas de traitement spécifique
	}
	protected CongeNormalien currentConge() {
		return (CongeNormalien)displayGroup().selectedObject();
	}
}

// _GestionAbsences_Interface.java
// Created on 26 sept. 2010 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _GestionAbsences_Interface extends COFrame {

    /** Creates new form _GestionAbsences_Interface */
    public _GestionAbsences_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new org.cocktail.component.CODisplayGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        listeAffichage = new org.cocktail.component.COTable();
        vuePeriode = new org.cocktail.component.COView();
        vueTypeConge = new org.cocktail.component.COView();
        vueBoutonsGardeEnfant = new org.cocktail.component.COView();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cOLabel2 = new org.cocktail.component.COLabel();
        jLabel5 = new javax.swing.JLabel();
        cOLabel1 = new org.cocktail.component.COLabel();
        cOButton8 = new org.cocktail.component.COButton();
        cOButton7 = new org.cocktail.component.COButton();
        jPanel1 = new javax.swing.JPanel();
        cOButton5 = new org.cocktail.component.COButton();
        vueInspecteur = new org.cocktail.component.COView();
        cOLabel4 = new org.cocktail.component.COLabel();
        jLabel7 = new javax.swing.JLabel();
        cOButton6 = new org.cocktail.component.COButton();
        cOButton10 = new org.cocktail.component.COButton();
        cOLabel5 = new org.cocktail.component.COLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        boutonDetailTraitement = new org.cocktail.component.COButton();
        cOLabel3 = new org.cocktail.component.COLabel();
        jPanel2 = new javax.swing.JPanel();
        cOButton1 = new org.cocktail.component.COButton();
        cOButton9 = new org.cocktail.component.COButton();
        cOButton3 = new org.cocktail.component.COButton();
        cOButton4 = new org.cocktail.component.COButton();
        btnProlonger = new org.cocktail.component.COButton();

        displayGroup.setEntityName("Absences");
        displayGroup.setHasDelegate(true);
        displayGroup.setIsMainDisplayGroupForController(true);

        setControllerClassName("org.cocktail.mangue.client.conges.GestionAbsences");
        setSize(new java.awt.Dimension(656, 631));

        listeAffichage.setColumns(new Object[][] {{"","libelleTypeAbsence",new Integer(2),"Type",new Integer(0),new Integer(290),new Integer(1000),new Integer(10)},{"%d/%m/%Y","dateDebut",new Integer(0),"Début",new Integer(0),new Integer(100),new Integer(1000),new Integer(10)},{"%d/%m/%Y","dateFin",new Integer(0),"Fin",new Integer(0),new Integer(100),new Integer(1000),new Integer(10)},{"","absDureeTotale",new Integer(2),"Durée Absence",new Integer(0),new Integer(90),new Integer(1000),new Integer(10)}});
        listeAffichage.setDisplayGroupForTable(displayGroup);

        org.jdesktop.layout.GroupLayout vuePeriodeLayout = new org.jdesktop.layout.GroupLayout(vuePeriode);
        vuePeriode.setLayout(vuePeriodeLayout);
        vuePeriodeLayout.setHorizontalGroup(
            vuePeriodeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 448, Short.MAX_VALUE)
        );
        vuePeriodeLayout.setVerticalGroup(
            vuePeriodeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 41, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout vueTypeCongeLayout = new org.jdesktop.layout.GroupLayout(vueTypeConge);
        vueTypeConge.setLayout(vueTypeCongeLayout);
        vueTypeCongeLayout.setHorizontalGroup(
            vueTypeCongeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 448, Short.MAX_VALUE)
        );
        vueTypeCongeLayout.setVerticalGroup(
            vueTypeCongeLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 34, Short.MAX_VALUE)
        );

        jLabel3.setFont(new java.awt.Font("Helvetica", 1, 14));
        jLabel3.setText("Année");

        jLabel4.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel4.setText(",");

        cOLabel2.setFont(new java.awt.Font("SansSerif", 0, 14));
        cOLabel2.setSelectable(false);
        cOLabel2.setValueName("nbDemiJournees");

        jLabel5.setFont(new java.awt.Font("Helvetica", 1, 14));
        jLabel5.setText("1/2 Journées");

        cOLabel1.setFont(new java.awt.Font("SansSerif", 0, 14));
        cOLabel1.setSelectable(false);
        cOLabel1.setValueName("anneeDroits");

        cOButton8.setActionName("afficherDemiJournees");
        cOButton8.setBorderPainted(false);
        cOButton8.setEnabledMethod("modeSaisiePossible");
        cOButton8.setIconName("loupe16.gif");

        org.jdesktop.layout.GroupLayout vueBoutonsGardeEnfantLayout = new org.jdesktop.layout.GroupLayout(vueBoutonsGardeEnfant);
        vueBoutonsGardeEnfant.setLayout(vueBoutonsGardeEnfantLayout);
        vueBoutonsGardeEnfantLayout.setHorizontalGroup(
            vueBoutonsGardeEnfantLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsGardeEnfantLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel3)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 37, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel4)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 103, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        vueBoutonsGardeEnfantLayout.setVerticalGroup(
            vueBoutonsGardeEnfantLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueBoutonsGardeEnfantLayout.createSequentialGroup()
                .add(vueBoutonsGardeEnfantLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, cOButton8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, vueBoutonsGardeEnfantLayout.createSequentialGroup()
                        .add(2, 2, 2)
                        .add(vueBoutonsGardeEnfantLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(cOLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel3)
                            .add(jLabel4)
                            .add(cOLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel5))))
                .addContainerGap())
        );

        cOButton7.setActionName("afficherAide");
        cOButton7.setBorderPainted(false);
        cOButton7.setIconName("help.gif");

        cOButton5.setActionName("imprimer");
        cOButton5.setBorderPainted(false);
        cOButton5.setEnabledMethod("peutImprimer");
        cOButton5.setIconName("Imprimante.gif");
        cOButton5.setToolTipText("Impression des absences");

        org.jdesktop.layout.GroupLayout vueInspecteurLayout = new org.jdesktop.layout.GroupLayout(vueInspecteur);
        vueInspecteur.setLayout(vueInspecteurLayout);
        vueInspecteurLayout.setHorizontalGroup(
            vueInspecteurLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 629, Short.MAX_VALUE)
        );
        vueInspecteurLayout.setVerticalGroup(
            vueInspecteurLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 64, Short.MAX_VALUE)
        );

        cOLabel4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cOLabel4.setNumberFieldFormat("0");
        cOLabel4.setValueName("dureePeriode");

        jLabel7.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel7.setText("Durée sur la période :");

        cOButton6.setActionName("imprimerArrete");
        cOButton6.setBorderPainted(false);
        cOButton6.setEnabledMethod("peutImprimerArrete");
        cOButton6.setIconName("Imprimante_Arrete.gif");
        cOButton6.setToolTipText("Edition d'un arrêté");

        cOButton10.setActionName("requalifier");
        cOButton10.setBorderPainted(false);
        cOButton10.setEnabledMethod("peutRequalifier");
        cOButton10.setIconName("cktl_refresh_16.png");
        cOButton10.setText("Requalifier");
        cOButton10.setToolTipText("Requalification des congés sélectionnés");
        cOButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cOButton10ActionPerformed(evt);
            }
        });

        cOLabel5.setSelectable(false);
        cOLabel5.setValueName("typeDureeTotale");

        jLabel6.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Durée totale :");

        jLabel9.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel9.setText("jours");

        boutonDetailTraitement.setActionName("afficherDetailsTraitement");
        boutonDetailTraitement.setIconName("cktl_loupe_16.png");
        boutonDetailTraitement.setToolTipText("Détail de traitements pour les congés maladie");

        cOLabel3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cOLabel3.setNumberFieldFormat("0");
        cOLabel3.setValueName("dureeTotale");

        cOButton1.setActionName("ajouter");
        cOButton1.setBorderPainted(false);
        cOButton1.setEnabledMethod("peutAjouter");
        cOButton1.setIconName("ajouter16.gif");

        cOButton9.setActionName("afficherInspecteur");
        cOButton9.setBorderPainted(false);
        cOButton9.setEnabledMethod("peutAfficherInspecteur");
        cOButton9.setIconName("info_small.gif");
        cOButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cOButton9ActionPerformed(evt);
            }
        });

        cOButton3.setActionName("supprimer");
        cOButton3.setBorderPainted(false);
        cOButton3.setEnabledMethod("peutSupprimer");
        cOButton3.setIconName("supprimer16.gif");

        cOButton4.setActionName("modifier");
        cOButton4.setBorderPainted(false);
        cOButton4.setEnabledMethod("peutModifier");
        cOButton4.setIconName("modifier16.gif");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .add(cOButton9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(cOButton9, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(cOButton3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                    .add(cOButton4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                    .add(cOButton1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        btnProlonger.setActionName("prolonger");
        btnProlonger.setBorderPainted(false);
        btnProlonger.setEnabledMethod("peutProlonger");
        btnProlonger.setIconName("renouveler.gif");
        btnProlonger.setText("Prolonger");
        btnProlonger.setToolTipText("Prolongation du congé sélectionné");
        btnProlonger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProlongerActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(vueInspecteur, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(jLabel7)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(cOLabel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(cOButton5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(cOButton6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(jLabel9)
                                .add(18, 18, 18)
                                .add(jLabel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 93, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(cOLabel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 31, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(cOLabel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(10, 10, 10)
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(btnProlonger, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 162, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(jPanel1Layout.createSequentialGroup()
                                        .add(cOButton10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 162, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                        .add(boutonDetailTraitement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 80, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                        .add(45, 45, 45)
                        .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel7)
                    .add(jLabel9)
                    .add(cOLabel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel6)
                    .add(cOLabel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOLabel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, boutonDetailTraitement, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, cOButton10, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnProlonger, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, cOButton5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, cOButton6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(vueInspecteur, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(vueBoutonsGardeEnfant, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                            .add(95, 95, 95)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(vuePeriode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(layout.createSequentialGroup()
                                    .add(vueTypeConge, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(42, 42, 42)
                                    .add(cOButton7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                            .add(31, 31, 31))
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(layout.createSequentialGroup()
                        .add(10, 10, 10)
                        .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 617, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(49, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(vueTypeConge, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(3, 3, 3)
                        .add(vuePeriode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(cOButton7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(46, 46, 46)
                .add(vueBoutonsGardeEnfant, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 273, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cOButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cOButton9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cOButton9ActionPerformed

    private void cOButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cOButton10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cOButton10ActionPerformed

    private void btnProlongerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProlongerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnProlongerActionPerformed
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COButton boutonDetailTraitement;
    public org.cocktail.component.COButton btnProlonger;
    private javax.swing.ButtonGroup buttonGroup1;
    public org.cocktail.component.COButton cOButton1;
    public org.cocktail.component.COButton cOButton10;
    public org.cocktail.component.COButton cOButton3;
    public org.cocktail.component.COButton cOButton4;
    public org.cocktail.component.COButton cOButton5;
    public org.cocktail.component.COButton cOButton6;
    public org.cocktail.component.COButton cOButton7;
    public org.cocktail.component.COButton cOButton8;
    public org.cocktail.component.COButton cOButton9;
    public org.cocktail.component.COLabel cOLabel1;
    public org.cocktail.component.COLabel cOLabel2;
    public org.cocktail.component.COLabel cOLabel3;
    public org.cocktail.component.COLabel cOLabel4;
    public org.cocktail.component.COLabel cOLabel5;
    public org.cocktail.component.CODisplayGroup displayGroup;
    public javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel4;
    public javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public org.cocktail.component.COTable listeAffichage;
    public org.cocktail.component.COView vueBoutonsGardeEnfant;
    public org.cocktail.component.COView vueInspecteur;
    public org.cocktail.component.COView vuePeriode;
    public org.cocktail.component.COView vueTypeConge;
    // End of variables declaration//GEN-END:variables
                  

}

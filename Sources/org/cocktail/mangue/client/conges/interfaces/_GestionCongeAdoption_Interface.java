// _GestionCongeAdoption_Interface.java
// Created on 14 juil. 2010 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _GestionCongeAdoption_Interface extends COFrame {

    /** Creates new form _GestionCongeAdoption_Interface */
    public _GestionCongeAdoption_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new org.cocktail.component.CODisplayGroup();
        cOView1 = new org.cocktail.component.COView();
        jLabel1 = new javax.swing.JLabel();
        cOTextField1 = new org.cocktail.component.COTextField();
        jLabel3 = new javax.swing.JLabel();
        cOTextField3 = new org.cocktail.component.COTextField();
        cOCheckbox1 = new org.cocktail.component.COCheckbox();
        cOCheckbox2 = new org.cocktail.component.COCheckbox();
        jLabel15 = new javax.swing.JLabel();
        tfEnfantsACharge = new javax.swing.JTextField();
        cOView2 = new org.cocktail.component.COView();
        jLabel4 = new javax.swing.JLabel();
        cOTextField4 = new org.cocktail.component.COTextField();
        jLabel5 = new javax.swing.JLabel();
        cOTextField5 = new org.cocktail.component.COTextField();
        cOCheckbox3 = new org.cocktail.component.COCheckbox();
        labelEnfant = new javax.swing.JLabel();
        cOTextField6 = new org.cocktail.component.COTextField();
        cOButton5 = new org.cocktail.component.COButton();
        jLabel6 = new javax.swing.JLabel();
        vueTexte = new org.cocktail.component.COTextArea();
        cOLabel1 = new org.cocktail.component.COLabel();
        vueArretes = new org.cocktail.component.COView();
        vueArrete = new org.cocktail.component.COView();
        contenuVueArrete = new org.cocktail.component.COView();
        vueArreteAnnulation = new org.cocktail.component.COView();
        cOButton2 = new org.cocktail.component.COButton();
        vueArrete1 = new org.cocktail.component.COView();
        contenuVueArreteAnnulaton = new org.cocktail.component.COView();
        cOButton1 = new org.cocktail.component.COButton();
        cOButton3 = new org.cocktail.component.COButton();
        cOButton4 = new org.cocktail.component.COButton();

        displayGroup.setHasDelegate(true);
        displayGroup.setIsMainDisplayGroupForController(true);
        displayGroup.setKeys(new Object[] {"commentaire","dateArriveeFormatee","dateDebutFormatee","dateDemandeFormatee","dateFinFormatee","enfant.identite","estAdoptionMultiple","estCongeSansTraitement","estPartage","nbEnfantACharge"});

        setControllerClassName("org.cocktail.mangue.client.conges.GestionCongeAdoption");
        setSize(new java.awt.Dimension(754, 580));

        cOView1.setFont(new java.awt.Font("Helvetica", 3, 12)); // NOI18N
        cOView1.setIsBox(true);
        cOView1.setTitle("Informations");

        jLabel1.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel1.setText("Date de la demande");

        cOTextField1.setDisplayGroupForValue(displayGroup);
        cOTextField1.setEnabledMethod("peutModifierDeclaration");
        cOTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cOTextField1.setSupportsBackgroundColor(true);
        cOTextField1.setValueName("dateDemandeFormatee");

        jLabel3.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel3.setText("Date d'arrivée au foyer");

        cOTextField3.setDisplayGroupForValue(displayGroup);
        cOTextField3.setEnabledMethod("peutModifierDeclaration");
        cOTextField3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cOTextField3.setSupportsBackgroundColor(true);
        cOTextField3.setValueName("dateArriveeFormatee");

        cOCheckbox1.setDisplayGroupForValue(displayGroup);
        cOCheckbox1.setEnabledMethod("peutModifierDeclaration");
        cOCheckbox1.setText("Congé partagé avec le conjoint");
        cOCheckbox1.setValueName("estPartage");

        cOCheckbox2.setDisplayGroupForValue(displayGroup);
        cOCheckbox2.setEnabledMethod("peutModifierDeclaration");
        cOCheckbox2.setText("Adoption multiple");
        cOCheckbox2.setValueName("estAdoptionMultiple");

        jLabel15.setFont(new java.awt.Font("Helvetica", 0, 12)); // NOI18N
        jLabel15.setText("Enfants à charge avant l'adoption");

        tfEnfantsACharge.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        org.jdesktop.layout.GroupLayout cOView1Layout = new org.jdesktop.layout.GroupLayout(cOView1);
        cOView1.setLayout(cOView1Layout);
        cOView1Layout.setHorizontalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView1Layout.createSequentialGroup()
                .addContainerGap()
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel3)
                    .add(jLabel1))
                .add(4, 4, 4)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(27, 27, 27)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cOView1Layout.createSequentialGroup()
                        .add(jLabel15)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(tfEnfantsACharge, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 46, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(cOCheckbox1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                        .add(cOCheckbox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 147, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(42, 42, 42))
        );
        cOView1Layout.setVerticalGroup(
            cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView1Layout.createSequentialGroup()
                .addContainerGap()
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(cOTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel15)
                    .add(tfEnfantsACharge, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOView1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel3)
                    .add(cOTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOCheckbox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOCheckbox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cOView2.setIsBox(true);

        jLabel4.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel4.setText("Début");

        cOTextField4.setDisplayGroupForValue(displayGroup);
        cOTextField4.setEnabledMethod("peutModifierDateDebut");
        cOTextField4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cOTextField4.setSupportsBackgroundColor(true);
        cOTextField4.setValueName("dateDebutFormatee");

        jLabel5.setFont(new java.awt.Font("Helvetica", 1, 12));
        jLabel5.setText("Fin");

        cOTextField5.setDisplayGroupForValue(displayGroup);
        cOTextField5.setEnabledMethod("peutModifierDateFin");
        cOTextField5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cOTextField5.setSupportsBackgroundColor(true);
        cOTextField5.setValueName("dateFinFormatee");

        cOCheckbox3.setDisplayGroupForValue(displayGroup);
        cOCheckbox3.setEnabledMethod("peutAvoirCongeSansTraitement");
        cOCheckbox3.setText(" Congé sans traitement");
        cOCheckbox3.setValueName("estCongeSansTraitement");

        labelEnfant.setFont(new java.awt.Font("Helvetica", 0, 12));
        labelEnfant.setText("Enfant");

        cOTextField6.setDisplayGroupForValue(displayGroup);
        cOTextField6.setEnabledMethod("nonEditable");
        cOTextField6.setSupportsBackgroundColor(true);
        cOTextField6.setValueName("enfant.identite");

        cOButton5.setActionName("afficherEnfant");
        cOButton5.setBorderPainted(false);
        cOButton5.setEnabledMethod("peutAfficherEnfant");
        cOButton5.setIconName("loupe16.gif");

        org.jdesktop.layout.GroupLayout cOView2Layout = new org.jdesktop.layout.GroupLayout(cOView2);
        cOView2.setLayout(cOView2Layout);
        cOView2Layout.setHorizontalGroup(
            cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView2Layout.createSequentialGroup()
                .add(29, 29, 29)
                .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(cOView2Layout.createSequentialGroup()
                        .add(jLabel4)
                        .add(4, 4, 4)
                        .add(cOTextField4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(64, 64, 64)
                        .add(jLabel5)
                        .add(4, 4, 4)
                        .add(cOTextField5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 29, Short.MAX_VALUE)
                        .add(cOCheckbox3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(cOView2Layout.createSequentialGroup()
                        .add(labelEnfant, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(cOTextField6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 406, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cOButton5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(2, 2, 2)))
                .add(32, 32, 32))
        );
        cOView2Layout.setVerticalGroup(
            cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(cOView2Layout.createSequentialGroup()
                .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cOCheckbox3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel4)
                    .add(cOTextField4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel5)
                    .add(cOTextField5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(cOView2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(labelEnfant)
                    .add(cOButton5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOTextField6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jLabel6.setFont(new java.awt.Font("Helvetica", 0, 12));
        jLabel6.setText("Commentaire");

        vueTexte.setDisplayGroupForValue(displayGroup);
        vueTexte.setFont(new java.awt.Font("Helvetica", 0, 11));
        vueTexte.setShouldRefreshImmediately(true);
        vueTexte.setSupportsBackgroundColor(true);
        vueTexte.setValueName("commentaire");

        cOLabel1.setFont(new java.awt.Font("Helvetica", 0, 11));
        cOLabel1.setValueName("compteurCommentaire");

        vueArrete.setFont(new java.awt.Font("Helvetica", 3, 11));
        vueArrete.setIsBox(true);
        vueArrete.setTitle("Arrêté");

        org.jdesktop.layout.GroupLayout contenuVueArreteLayout = new org.jdesktop.layout.GroupLayout(contenuVueArrete);
        contenuVueArrete.setLayout(contenuVueArreteLayout);
        contenuVueArreteLayout.setHorizontalGroup(
            contenuVueArreteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 478, Short.MAX_VALUE)
        );
        contenuVueArreteLayout.setVerticalGroup(
            contenuVueArreteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 47, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout vueArreteLayout = new org.jdesktop.layout.GroupLayout(vueArrete);
        vueArrete.setLayout(vueArreteLayout);
        vueArreteLayout.setHorizontalGroup(
            vueArreteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueArreteLayout.createSequentialGroup()
                .addContainerGap()
                .add(contenuVueArrete, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        vueArreteLayout.setVerticalGroup(
            vueArreteLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueArreteLayout.createSequentialGroup()
                .addContainerGap()
                .add(contenuVueArrete, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cOButton2.setActionName("annulerArrete");
        cOButton2.setEnabledMethod("peutAnnulerArrete");
        cOButton2.setFont(new java.awt.Font("Helvetica", 2, 11));
        cOButton2.setText("Annuler cet arrêté");

        vueArrete1.setFont(new java.awt.Font("Helvetica", 3, 11));
        vueArrete1.setIsBox(true);
        vueArrete1.setTitle("Arrêté Annulation");

        org.jdesktop.layout.GroupLayout contenuVueArreteAnnulatonLayout = new org.jdesktop.layout.GroupLayout(contenuVueArreteAnnulaton);
        contenuVueArreteAnnulaton.setLayout(contenuVueArreteAnnulatonLayout);
        contenuVueArreteAnnulatonLayout.setHorizontalGroup(
            contenuVueArreteAnnulatonLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 478, Short.MAX_VALUE)
        );
        contenuVueArreteAnnulatonLayout.setVerticalGroup(
            contenuVueArreteAnnulatonLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 47, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout vueArrete1Layout = new org.jdesktop.layout.GroupLayout(vueArrete1);
        vueArrete1.setLayout(vueArrete1Layout);
        vueArrete1Layout.setHorizontalGroup(
            vueArrete1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueArrete1Layout.createSequentialGroup()
                .addContainerGap()
                .add(contenuVueArreteAnnulaton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        vueArrete1Layout.setVerticalGroup(
            vueArrete1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueArrete1Layout.createSequentialGroup()
                .add(179, 179, 179)
                .add(contenuVueArreteAnnulaton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cOButton1.setActionName("afficherArreteAnnulation");
        cOButton1.setEnabledMethod("peutAfficherArreteAnnulation");
        cOButton1.setFont(new java.awt.Font("Helvetica", 2, 11));
        cOButton1.setText("Afficher Arrêté de remplacement");

        org.jdesktop.layout.GroupLayout vueArreteAnnulationLayout = new org.jdesktop.layout.GroupLayout(vueArreteAnnulation);
        vueArreteAnnulation.setLayout(vueArreteAnnulationLayout);
        vueArreteAnnulationLayout.setHorizontalGroup(
            vueArreteAnnulationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, vueArreteAnnulationLayout.createSequentialGroup()
                .addContainerGap(11, Short.MAX_VALUE)
                .add(vueArreteAnnulationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(vueArrete1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOButton2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 134, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        vueArreteAnnulationLayout.setVerticalGroup(
            vueArreteAnnulationLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueArreteAnnulationLayout.createSequentialGroup()
                .addContainerGap()
                .add(cOButton2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(vueArrete1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 81, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout vueArretesLayout = new org.jdesktop.layout.GroupLayout(vueArretes);
        vueArretes.setLayout(vueArretesLayout);
        vueArretesLayout.setHorizontalGroup(
            vueArretesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(vueArretesLayout.createSequentialGroup()
                .add(vueArretesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, vueArretesLayout.createSequentialGroup()
                        .add(36, 36, 36)
                        .add(vueArrete, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, vueArretesLayout.createSequentialGroup()
                        .add(23, 23, 23)
                        .add(vueArreteAnnulation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        vueArretesLayout.setVerticalGroup(
            vueArretesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, vueArretesLayout.createSequentialGroup()
                .addContainerGap()
                .add(vueArrete, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 81, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(vueArreteAnnulation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        cOButton3.setActionName("annuler");
        cOButton3.setBorderPainted(false);
        cOButton3.setIconName("annuler16.gif");

        cOButton4.setActionName("valider");
        cOButton4.setBorderPainted(false);
        cOButton4.setEnabledMethod("peutValider");
        cOButton4.setIconName("valider16.gif");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(30, 30, 30)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel6)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(layout.createSequentialGroup()
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, cOView1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, vueTexte, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(cOLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 61, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, cOView2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .add(26, 26, 26))
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                            .add(cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(cOButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(layout.createSequentialGroup()
                            .add(vueArretes, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 30, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(16, 16, 16)
                .add(cOView1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(cOView2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel6)
                .add(2, 2, 2)
                .add(vueTexte, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 98, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(9, 9, 9)
                .add(vueArretes, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(cOButton3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cOButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(0, 17, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COButton cOButton1;
    public org.cocktail.component.COButton cOButton2;
    public org.cocktail.component.COButton cOButton3;
    public org.cocktail.component.COButton cOButton4;
    public org.cocktail.component.COButton cOButton5;
    public org.cocktail.component.COCheckbox cOCheckbox1;
    public org.cocktail.component.COCheckbox cOCheckbox2;
    public org.cocktail.component.COCheckbox cOCheckbox3;
    public org.cocktail.component.COLabel cOLabel1;
    public org.cocktail.component.COTextField cOTextField1;
    public org.cocktail.component.COTextField cOTextField3;
    public org.cocktail.component.COTextField cOTextField4;
    public org.cocktail.component.COTextField cOTextField5;
    public org.cocktail.component.COTextField cOTextField6;
    private org.cocktail.component.COView cOView1;
    private org.cocktail.component.COView cOView2;
    public org.cocktail.component.COView contenuVueArrete;
    public org.cocktail.component.COView contenuVueArreteAnnulaton;
    public org.cocktail.component.CODisplayGroup displayGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    public javax.swing.JLabel labelEnfant;
    public javax.swing.JTextField tfEnfantsACharge;
    public org.cocktail.component.COView vueArrete;
    private org.cocktail.component.COView vueArrete1;
    public org.cocktail.component.COView vueArreteAnnulation;
    public org.cocktail.component.COView vueArretes;
    public org.cocktail.component.COTextArea vueTexte;
    // End of variables declaration//GEN-END:variables
                  

}

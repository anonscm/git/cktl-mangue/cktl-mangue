// _GestionDetailsTraitement_Interface.java
// Created on 24 sept. 2010 by JavaClient Wizard 1.0
/*
 * Copyright Consortium Cocktail
 *
 * cocktail@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement,
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges.interfaces;
import org.cocktail.component.COFrame;

/**
 *
 * @author  christine
 */
public class _GestionDetailsTraitement_Interface extends COFrame {

    /** Creates new form _GestionDetailsTraitement_Interface */
    public _GestionDetailsTraitement_Interface() {
        super();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new org.cocktail.component.CODisplayGroup();
        listeAffichage = new org.cocktail.component.COTable();
        cOButton1 = new org.cocktail.component.COButton();

        displayGroup.setHasDelegate(true);
        displayGroup.setIsMainDisplayGroupForController(true);
        displayGroup.setKeys(new Object[] {"dateDebut","dateFin","duree","dureeComptable","numTaux"});

        setControllerClassName("org.cocktail.mangue.client.conges.GestionDetailsTraitement");
        setSize(new java.awt.Dimension(615, 235));

        listeAffichage.setColumns(new Object[][] {{"%d/%m/%Y","dateDebut",new Integer(0),"Début Période",new Integer(0),new Integer(85),new Integer(1000),new Integer(40)},{"%d/%m/%Y","dateFin",new Integer(0),"Fin Période",new Integer(0),new Integer(80),new Integer(1000),new Integer(40)},{"0","duree",new Integer(0),"Durée en jours",new Integer(0),new Integer(85),new Integer(1000),new Integer(10)},{"0","dureeComptable",new Integer(0),"Durée comptable",new Integer(0),new Integer(100),new Integer(1000),new Integer(10)},{"0","numTaux",new Integer(0),"Taux de rémunération",new Integer(0),new Integer(130),new Integer(1000),new Integer(115)}});
        listeAffichage.setDisplayGroupForTable(displayGroup);

        cOButton1.setActionName("fermer");
        cOButton1.setText("Fermer");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 500, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 79, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(cOButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(listeAffichage, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 195, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.component.COButton cOButton1;
    public org.cocktail.component.CODisplayGroup displayGroup;
    public org.cocktail.component.COTable listeAffichage;
    // End of variables declaration//GEN-END:variables
                  

}

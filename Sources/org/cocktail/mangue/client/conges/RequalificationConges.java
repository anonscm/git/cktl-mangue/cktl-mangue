/*
 * Created on 17 févr. 2006
 *
 * Comporte des méthodes statiques pour la requalification des congés
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.mangue.conges.ICongeAvecRequalification;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** Methodes statiques pour la requalification des conges (maladie et longue maladie)
 * * Gestion des requalifications des conges<BR>
 * Lors de la validation, on verifie si le conge recouvre des conges de maladie. Si c'est le cas :<BR>
 * On verifie si il recouvre completement les conge de maladie (i.e un conge maladie doit commencer a la date debut
 * et le eme ou un autre doit se terminer a la date fin). Dans le cas negatif, on signale l'erreur.Dans le cas positif, on verifie 
 * si les conges de maladie sont continus. Dans le cas positif, on consulte l'utilisateur pour savoir si il veut vraiment requalifier les conges maladie, 
 * dans le cas negatif, on signale par une erreur la discontinuite des conge maladie.<BR>
 * Exemples :<BR>
 * 1.COM du 01/01/2004 au 20/01/2004<BR>
 *   COM du 21/01/2004 au 31/01/2004<BR>
 *   CLM du 01/01/2004 au 31/03/2004 ok on requalifie les deux COM<BR>
 * 
 * 2.COM du 01/01/2004 au 20/01/2004<BR>
 *   CLM du 05/01/2004 au 04/03/2004 on ne requalifie pas et affichage d'un message d'erreur<BR>
 *
 * 3.COM du 01/01/2004 au 15/01/2004<BR>
 *   COM du 20/01/2004 au 31/01/2004<BR>
 *   CLM du 01/01/2004 au 31/03/2004 on ne requalifie pas et affichage d'un message d'erreur sur la discontinuite des COM<BR>
 * 
 * Les conges associes doivent implementer l'interface CongeAvecRequalification<BR>
 *
 */
public class RequalificationConges {
	/** verifie si il y a des conges assoces au conge fourni et requalifie si besoin est : (voir description de la classe)
	 * @param congeConcerne conge pour lequel determiner les conges associes
	 * @param nomEntiteCongeAssocie nom d'entite du conge associe
	 * @param nomCongePourMessage nom du conge affiche en cas de message d'erreur
	 * @param nomCongeAssociePourMessage nom du conge associe affiche en cas de message d'erreur
	 * @return false en cas d'erreur
	 */
	public static boolean requalifierCongesAssocies(Conge congeConcerne,String nomEntiteCongeAssocie,String nomCongePourMessage, String nomCongeAssociePourMessage) {
		EOEditingContext editingContext = congeConcerne.editingContext();
		NSArray congesAssociesPourPeriode = CongeAvecArreteAnnulation.rechercherCongesValidesPourIndividuEtPeriode(editingContext, nomEntiteCongeAssocie,congeConcerne.individu(),congeConcerne.dateDebut(),congeConcerne.dateFin());
		if (congesAssociesPourPeriode.count() == 0) {
			return true;		// pas de requalification
		}
		// ils sont triés par date début. Vérifier si il faut requalifier
		boolean congesDiscontinus = false;
		ICongeAvecRequalification conge = (ICongeAvecRequalification)congesAssociesPourPeriode.objectAtIndex(0);
		NSTimestamp dateDebut = conge.dateDebut();
		NSTimestamp dateFin = conge.dateFin();
		NSMutableArray congesPourRequalification = new NSMutableArray();
		// si le premier congé trouvé ne débute pas le même jour, il n'y a pas de recouvrement complet des congés de maladie
		// on n'a pas besoin de vérifier la suite des congés
		if (DateCtrl.isSameDay(dateDebut,congeConcerne.dateDebut())) {
			// si ce congé est déjà requalifié, ne rien faire
			if (conge.estRequalifie()) {
				return true;
			}
			congesPourRequalification.addObject(conge);
			for (int i = 1; i < congesAssociesPourPeriode.count(); i++) {
				ICongeAvecRequalification currentConge = (ICongeAvecRequalification)congesAssociesPourPeriode.objectAtIndex(i);
				if (DateCtrl.isSameDay(DateCtrl.jourSuivant(dateFin),currentConge.dateDebut()) == false) {
					congesDiscontinus = true;
					break;
				} else if (DateCtrl.isAfter(currentConge.dateDebut(),congeConcerne.dateFin())) {
					break;
				} else {
					// normalement la date de fin est nécessairement postérieure à celle du congé précédent car pas de chevauchement des congés
					dateFin = currentConge.dateFin();
					congesPourRequalification.addObject(currentConge);
				}
			}
		}

		if (congesDiscontinus) {
			EODialogs.runErrorDialog("ERREUR","Dates invalides : ce congé de " + nomCongePourMessage + " recouvre des congés de " + nomCongeAssociePourMessage + " discontinus.");
			return false;
		}
		if (DateCtrl.isSameDay(dateDebut,congeConcerne.dateDebut()) && DateCtrl.isSameDay(dateFin,congeConcerne.dateFin())) {
			if (EODialogs.runConfirmOperationDialog("Attention","Ce congé de " + nomCongePourMessage + " requalifie des congés de " + nomCongeAssociePourMessage + ". Est-ce bien ce que vous voulez ?","Oui","Non")) {
				for (java.util.Enumeration<ICongeAvecRequalification> e = congesPourRequalification.objectEnumerator();e.hasMoreElements();) {
					ICongeAvecRequalification currentConge = e.nextElement();
					currentConge.setEstRequalifie(true);
				}
				return true;
			} else {
				return false;
			}
		} else {
			if (DateCtrl.isSameDay(dateDebut,congeConcerne.dateDebut()) == false) {
				EODialogs.runErrorDialog("ERREUR","Dates invalides : ce congé de " + nomCongePourMessage + " recouvre des congés de " + nomCongeAssociePourMessage + " qui ne débutent pas à la même date.");
			} else  {
				EODialogs.runErrorDialog("ERREUR","Dates invalides : ce congé de " + nomCongePourMessage + " recouvre des congés de " + nomCongeAssociePourMessage + " qui ne se terminent pas à la même date.");
			}
			return false;
		}
	}
	
	/* annule la requalification de conges suite a l'annulation ou la suppression du conge concerne */
	public static void annulerQualificationCongesAssocies(Conge congeConcerne,String nomEntiteCongeAssocie) {
		EOEditingContext editingContext = congeConcerne.editingContext();
		// requalifier si nécessaire tous les congés de maladie associés
		NSArray congesAssociesPourPeriode = Duree.rechercherDureesPourIndividuEtPeriode(editingContext,nomEntiteCongeAssocie,congeConcerne.individu(),congeConcerne.dateDebut(),congeConcerne.dateFin());

		for (java.util.Enumeration<ICongeAvecRequalification> e = congesAssociesPourPeriode.objectEnumerator();e.hasMoreElements();) {
			ICongeAvecRequalification conge =(ICongeAvecRequalification)e.nextElement();
			if (conge.estRequalifie()) {
				conge.setEstRequalifie(false);
			}
		}
	}
}

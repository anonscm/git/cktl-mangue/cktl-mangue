/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.conges;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.conges.CongeRaisonPersonnelleView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.select.MotifCgntRfpSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOMotifCgntRfp;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.conges.EOCgntRaisonFamPerso;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** Gere les conges pour raison familiale ou personnelle<BR>
 * 
 *
 */

public class CongeRaisonPersonnelleCtrl extends ModelePageSaisie {

	private static CongeRaisonPersonnelleCtrl sharedInstance;

	private CongeRaisonPersonnelleView myView;
	private EOCgntRaisonFamPerso currentConge;
	private EOMotifCgntRfp currentMotif;

	public CongeRaisonPersonnelleCtrl (EOEditingContext edc) {

		super(edc);

		myView = new CongeRaisonPersonnelleView(new JFrame(), true);

		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setActionBoutonValiderListener(myView.getBtnValider());

		myView.getBtnGetMotif().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectMotifConge();}}
				);
		myView.getBtnImprimerArrete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerArrete();}}
				);

		CocktailUtilities.initTextField(myView.getTfTypeDeclaration(), false, false);

		setDateListeners(myView.getTfDebut());
		setDateListeners(myView.getTfFin());
		setDateListeners(myView.getTfDateArrete());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CongeRaisonPersonnelleCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CongeRaisonPersonnelleCtrl(editingContext);
		return sharedInstance;
	}			
	public EOCgntRaisonFamPerso getCurrentConge() {
		return currentConge;
	}

	public void setCurrentConge(EOCgntRaisonFamPerso currentConge) {
		this.currentConge = currentConge;
		updateDatas();
	}

	public EOMotifCgntRfp getCurrentMotif() {
		return currentMotif;
	}

	public void setCurrentMotif(EOMotifCgntRfp currentMotif) {
		this.currentMotif = currentMotif;
		CocktailUtilities.viderTextField(myView.getTfTypeDeclaration());
		if (currentMotif != null) {
			CocktailUtilities.setTextToField(myView.getTfTypeDeclaration(), currentMotif.llMotifCgRfp());
		}
	}


	/**
	 * 
	 * @param conge
	 */
	public void modifier(EOCgntRaisonFamPerso conge)	{

		setCurrentConge(conge);
		myView.setVisible(true);

	}

	public void supprimer(EOCgntRaisonFamPerso conge) {
		try {
			conge.absence().setEstValide(false);
			conge.setEstValide(false);

			getEdc().saveChanges();
		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}

	private void selectMotifConge() {

		EOMotifCgntRfp motif = MotifCgntRfpSelectCtrl.sharedInstance(getEdc()).getMotif();
		if (motif != null) {
			setCurrentMotif(motif);
			updateInterface();
		}

	}

	/**
	 * 
	 */
	public void imprimerArrete() {

		NSArray<EODestinataire> destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
				NSMutableArray<EOGlobalID> destinatairesGlobalIds = new NSMutableArray<EOGlobalID>();
				for (EODestinataire destinataire : destinatairesArrete) {
					destinatairesGlobalIds.addObject(getEdc().globalIDForObject(destinataire));
				}
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class, Boolean.class};
				Object[] parametres = new Object[]{getEdc().globalIDForObject(getCurrentConge()),destinatairesGlobalIds, false};
				UtilitairesImpression.imprimerSansDialogue(getEdc(),"clientSideRequestImprimerArreteConge", classeParametres, parametres, "ArreteCongeRaisonFamPerso" + getCurrentConge().individu().noIndividu() ,"Impression de l'arrêté");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		setCurrentMotif(null);
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentConge() != null) {
			
			CocktailUtilities.setDateToField(myView.getTfDebut(), getCurrentConge().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfFin(), getCurrentConge().dateFin());

			CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentConge().dateArrete());
			CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentConge().noArrete());

			CocktailUtilities.setTextToArea(myView.getTfCommentaires(), getCurrentConge().commentaire());

			
			setCurrentMotif(getCurrentConge().motif());

		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		getCurrentConge().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		getCurrentConge().setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		getCurrentConge().setDateArrete(CocktailUtilities.getDateFromField(myView.getTfDateArrete()));

		getCurrentConge().setNoArrete(CocktailUtilities.getTextFromField(myView.getTfNoArrete()));
		getCurrentConge().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));

		// CREATION DE L ABSENCE
		EOAbsences absence = getCurrentConge().absence() ;
		if (absence == null) {
			absence = EOAbsences.creer(getEdc(), getCurrentConge().individu(), 
					(EOTypeAbsence)NomenclatureFinder.findForCode(getEdc(), EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_RFP));
			getCurrentConge().setAbsenceRelationship(absence);
		}
		absence.setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		absence.setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		absence.setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(getCurrentConge().dateDebut(), getCurrentConge().dateFin(), true)));

		getCurrentConge().setMotifRelationship(getCurrentMotif());

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}



}
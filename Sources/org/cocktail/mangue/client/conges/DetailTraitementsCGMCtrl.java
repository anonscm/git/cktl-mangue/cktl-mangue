// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.conges;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.conges.DetailTraitementsView;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.mangue.conges.EOCgntCgm;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class DetailTraitementsCGMCtrl
{
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static final String ENTITY_NAME = EOCgntCgm.ENTITY_NAME;
	private static DetailTraitementsCGMCtrl sharedInstance;
	private EOEditingContext ec;
	private EODisplayGroup eod;
	private DetailTraitementsView myView;

	private String messageConsole;
	private EOCgntCgm currentConge;

	public DetailTraitementsCGMCtrl(EOEditingContext globalEc)
	{
		ec = globalEc;
		eod = new EODisplayGroup();
		myView = new DetailTraitementsView(new JFrame(), true,eod);

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {fermer();}}
		);

		myView.getTfTitre().setText("DETAIL DES TRAITEMENTS DE CONGE MALADIE ORDINAIRE");

	}

	public static DetailTraitementsCGMCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new DetailTraitementsCGMCtrl(editingContext);
		return sharedInstance;
	}

	public void enregistrerDetailsTraitement(EOCgntCgm currentConge)	{

		this.currentConge = currentConge;
		if (currentConge != null)
			calculerDetails(currentConge.individu(), currentConge.dateDebut(), currentConge.dateFin());
	}


	public void afficherDetails(EOAbsences absence)	{

		Conge conge = Conge.rechercherCongeAvecAbsence(ec, ENTITY_NAME , absence);
		currentConge = (EOCgntCgm)conge;

		// Calcul des jours calendaires et comptables du conge
		myView.getTfJoursCalendaires().setText(String.valueOf(DateCtrl.nbJoursEntre(currentConge.dateDebut(), currentConge.dateFin(), true, false)));
		myView.getTfJoursComptables().setText(String.valueOf(DateCtrl.nbJoursEntre(currentConge.dateDebut(), currentConge.dateFin(), true, true)));

		if (currentConge != null){
			myView.getLblTitreConge().setText(currentConge.individu().identitePrenomFirst()+" - Congé du " + conge.dateDebutFormatee() + " au " + conge.dateFinFormatee());
			eod.setObjectArray(calculerDetails(currentConge.individu(), currentConge.dateDebut(), currentConge.dateFin()));
			myView.getMyEOTable().updateData();
			myView.getConsole().setText(messageConsole);
		}
		else {
			EODialogs.runInformationDialog("ATTENTION", "Pas de congé associé à cette absence !");
		}

		myView.setVisible(true);
	}

	private NSArray calculerDetails(EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		try {

			int nbAnneesGlissement = 1;
			int nbJoursPleinTraitement = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_PLEIN_TRAITEMENT);
			int nbJoursDemiTraitement = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_DEMI_TRAITEMENT);

			int nbJoursComparaisonTotal = 0; // pour stocker le nombre de jours de comparaison payé à plein traitement et demi-traitement
			int nbJoursTotal = 0;	// pour stocker le nombre de jours total payé à plein traitement et demi-traitement

			NSMutableArray detailsConges = new NSMutableArray();
			NSTimestamp dateATraiter = new NSTimestamp();

			// Ce n'est pas la peine de traiter les conges datant de plus d'un an avant la date de debut du conge traite
			NSTimestamp borne1 = DateCtrl.dateAvecAjoutAnnees(dateDebut,-nbAnneesGlissement);;
			NSTimestamp borne2 = dateFin;

			messageConsole = "Détail du calcul de traitement des congés maladie : \n";

			NSArray conges = CongeAvecArreteAnnulation.rechercherCongesValidesPourIndividuEtPeriode(ec,ENTITY_NAME,individu, borne1, borne2);
			conges = EOSortOrdering.sortedArrayUsingKeyOrderArray(conges, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));

			// On parcourt tous les conges a prendre en compte
			for (java.util.Enumeration<Conge> e = conges.objectEnumerator();e.hasMoreElements();) {

				NSMutableArray<NSMutableDictionary> arrayJours = new NSMutableArray();
				EOCgntCgm congeTraite = (EOCgntCgm)e.nextElement();

				messageConsole = messageConsole.concat("\n*** CONGE DU : " + DateCtrl.dateToString(congeTraite.dateDebut()) + " au " + DateCtrl.dateToString(congeTraite.dateFin()) + ") :\n\n");
				dateATraiter = congeTraite.dateDebut();

				int nbJoursSupPleinTraitement = 1;
				while (DateCtrl.isBeforeEq(dateATraiter,congeTraite.dateFin())) {			

					NSMutableDictionary dicoJours = new NSMutableDictionary();
					dicoJours.setObjectForKey(dateATraiter, "jour");
					nbJoursTotal++;					

					borne1 = DateCtrl.dateAvecAjoutAnnees(dateATraiter,-nbAnneesGlissement);	// Date a traiter moins nb années glissement.
					borne1 = DateCtrl.dateAvecAjoutJours(borne1, 1);							// il faut partir du jour j+1

					borne2 = DateCtrl.jourPrecedent(dateATraiter);
					// Calculer le nombre de jours comptables de plein traitement pendant les congés précédents
					int joursPleinTraitement = nbJoursPleinTraitement(detailsConges,borne1,borne2, false);

					NSTimestamp dateComparaison = DateCtrl.dateAvecAjoutJours(congeTraite.dateDebut(),nbJoursSupPleinTraitement -1); 	// on enlève 1 car nbJoursSupPleinTraitement est initialisé à 1
					int dureeSupplementairePleinTraitement = DateCtrl.nbJoursEntre(congeTraite.dateDebut(), dateComparaison,true, false) ;

					if (dureeSupplementairePleinTraitement <= nbJoursPleinTraitement)
						dicoJours.setObjectForKey(dureeSupplementairePleinTraitement, "joursPt");
					else
						dicoJours.setObjectForKey("0", "joursPt");

					int nbJoursComparaison = joursPleinTraitement + dureeSupplementairePleinTraitement;
					dicoJours.setObjectForKey(new Integer(nbJoursComparaison), "joursComparaison");

					// if (nbJoursComparaison > 0 && nbJoursComparaison <= nbJoursPleinTraitement) { 
					// pose un problème lorsque le congé commence le 31 d'un mois
					if (nbJoursComparaison >= 0 && nbJoursComparaison <= nbJoursPleinTraitement) {
						nbJoursSupPleinTraitement++;	// on vient d'ajouter 1 journée à plein traitement
						dicoJours.setObjectForKey((Number)new Integer(100),"taux");
						if (nbJoursComparaison == nbJoursPleinTraitement) {
							nbJoursTotal = nbJoursComparaison;
							nbJoursComparaisonTotal = nbJoursComparaison;
						}
					} else {

						nbJoursComparaisonTotal += DateCtrl.dureeComptable(dateATraiter, dateATraiter, true);

						if (nbJoursComparaisonTotal <= nbJoursPleinTraitement + nbJoursDemiTraitement) {
							dicoJours.setObjectForKey((Number)new Integer(50),"taux");
						} else { 
							// si on depasse le nombre de jours à plein traitement et le nombre de jours à demi-traitement
							// pas de rémunération => taux nul
							dicoJours.setObjectForKey((Number)new Integer(0),"taux");
						}
					}

					if (dicoJours.objectForKey("carence").equals("O"))
						dicoJours.setObjectForKey((Number)new Integer(0),"taux");

					arrayJours.addObject(dicoJours);
					dateATraiter = DateCtrl.jourSuivant(dateATraiter);
				}

				for (NSDictionary myDico : arrayJours) {

					Integer taux = (Integer)myDico.objectForKey("taux");
					messageConsole = messageConsole.concat("	Jour : " + DateCtrl.dateToString((NSTimestamp)myDico.objectForKey("jour")));
					messageConsole = messageConsole.concat("	Taux : " + myDico.objectForKey("taux"));
					if ( myDico.objectForKey("joursPt") != null && taux.intValue() != 50)
						messageConsole = messageConsole.concat("	Jours PT : " + myDico.objectForKey("joursPt"));
					else
						messageConsole = messageConsole.concat("	             ");

					if (taux.intValue() != 50)
						messageConsole = messageConsole.concat("	TOTAL PT : " + myDico.objectForKey("joursComparaison"));

					messageConsole = messageConsole.concat("\n");

				}

				NSMutableArray<NSDictionary> periodesTraitement = regrouperPeriodesTaux(arrayJours);

				// Créer les détails de conges par taux
				for (NSDictionary myDico : periodesTraitement) {
					NSMutableDictionary dicoDetailConge = new NSMutableDictionary();
					dicoDetailConge.setObjectForKey((NSTimestamp)myDico.objectForKey("debut"), "DEBUT");
					dicoDetailConge.setObjectForKey((NSTimestamp)myDico.objectForKey("fin"), "FIN");
					dicoDetailConge.setObjectForKey((Integer)myDico.objectForKey("taux"), "TAUX");							
					dicoDetailConge.setObjectForKey(new Integer(DateCtrl.nbJoursEntre((NSTimestamp)myDico.objectForKey("debut"),(NSTimestamp)myDico.objectForKey("fin"),true)), "CALENDAIRE");
					dicoDetailConge.setObjectForKey(new Integer(DateCtrl.dureeComptable((NSTimestamp)myDico.objectForKey("debut"),(NSTimestamp)myDico.objectForKey("fin"),true)), "COMPTABLE");

					detailsConges.addObject(myDico);
				}
			}		

			return detailsConges;
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * Regroupement de periodes de conges ayant le meme taux de traitement
	 * Un conge peut donc etre fractionne en deux periodes à 100% et 50%
	 * @param arrayJours
	 * @return
	 */
	private NSMutableArray regrouperPeriodesTaux(NSMutableArray arrayJours) {
		// Regroupement de toutes les periodes au même taux
		Number taux = (Number)new Integer(-1);	// 25/09/08 - pour afficher les taux 0 des non titulaires
		NSMutableArray periodesTraitement = new NSMutableArray();
		NSTimestamp lastDate = null;
		for (java.util.Enumeration<NSDictionary> e1 = arrayJours.objectEnumerator();e1.hasMoreElements();) {
			NSDictionary dicoTemp = e1.nextElement();

			// Si on change d'etat, on ajoute la date et l'etat
			if (taux.intValue() != ((Number)dicoTemp.objectForKey("taux")).intValue()) {
				if (periodesTraitement.count() > 0) {
					NSMutableDictionary periodePrecedente = (NSMutableDictionary)periodesTraitement.objectAtIndex(periodesTraitement.count() - 1);
					periodePrecedente.setObjectForKey(DateCtrl.jourPrecedent((NSTimestamp)dicoTemp.objectForKey("jour")),"fin");
				}
				NSMutableDictionary dicoTraitement = new NSMutableDictionary();
				dicoTraitement.setObjectForKey((NSTimestamp)dicoTemp.objectForKey("jour"),"debut");
				dicoTraitement.setObjectForKey(dicoTemp.objectForKey("taux"),"taux");
				taux = (Number)dicoTemp.objectForKey("taux");
				periodesTraitement.addObject(dicoTraitement);
			}

			lastDate = (NSTimestamp)dicoTemp.objectForKey("jour");
		}
		// Pour la dernière période
		if (periodesTraitement.count() > 0) {
			NSMutableDictionary periodePrecedente = (NSMutableDictionary)periodesTraitement.objectAtIndex(periodesTraitement.count() - 1);
			periodePrecedente.setObjectForKey(lastDate,"fin");
		}

		return periodesTraitement;
	}



	// NB JOURS PLEIN TRAITEMENT : Renvoie le nombre de jours de maladie à plein traitement entre deux dates
	private static int nbJoursPleinTraitement(NSArray periodesDetail, NSTimestamp date1, NSTimestamp date2,boolean estDureeComptable) {
		NSTimestamp dateDebut = null, dateFin = null;
		float nbJours = (float)0.0;
		for (java.util.Enumeration<NSMutableDictionary> e = periodesDetail.objectEnumerator();e.hasMoreElements();) {
			NSMutableDictionary dicoDetail = e.nextElement();

			NSTimestamp detailDebut = (NSTimestamp)dicoDetail.objectForKey("DEBUT");
			NSTimestamp detailFin = (NSTimestamp)dicoDetail.objectForKey("FIN");
			Integer detailTaux = (Integer)dicoDetail.objectForKey("TAUX");

			// si le détail de congé démarre avant la date de fin et se termine après la date de début
			// ils ont une période commune : c'est la période comprise entre le max des dates début et le min des dates fin
			if (DateCtrl.isBeforeEq(detailDebut,date2) && DateCtrl.isAfterEq(detailFin,date1) && (detailTaux.intValue() == 100 || detailTaux.intValue() == 0 )) {
				if (DateCtrl.isBeforeEq(detailDebut,date1)) {
					//dateDebut = DateCtrl.jourSuivant(date1.timestampByAddingGregorianUnits);
					dateDebut = date1;	// on prend en compte les bornes dans les calculs
				} else {
					dateDebut = detailDebut;
					//dateDebut = DateCtrl.jourPrecedent(detail.dateDebut());
				}
				if (DateCtrl.isAfterEq(detailFin,date2)) {
					dateFin = date2;
				} else {
					dateFin = detailFin;
				}
				if (DateCtrl.isBeforeEq(dateDebut,dateFin)) {
					// cas aux limites ou la date de début est égale ou passe après la date de fin
					// Pour chaque conge maladie , on calcule le nombre de jours comptables de l'absence
					try {
						nbJours = nbJours + DateCtrl.nbJoursEntre(dateDebut,dateFin,true,estDureeComptable);	
					} catch (Exception e1) {e1.printStackTrace();}
				}
			}
		}
		return (int)nbJours;
	}



	private void fermer() {
		myView.setVisible(false);
	}



}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.conges;

import java.math.BigDecimal;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.conges.CongeGardeEnfantView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.select.EnfantSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.conges.EOCongeGardeEnfant;
import org.cocktail.mangue.modele.mangue.conges.EODroitsGardeEnfant;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Gere les conges pour raison familiale ou personnelle<BR>
 * 
 *
 */

public class CongeGardeEnfantCtrl extends ModelePageSaisie {

	private static final String CODE_PM = "pm";

	private static final String CODE_AM = "am";

	private static CongeGardeEnfantCtrl sharedInstance;

	private CongeGardeEnfantView myView;
	private EOCongeGardeEnfant 	currentConge;
	private EOEnfant 			currentEnfant;
	private EODroitsGardeEnfant currentDroit;

	public CongeGardeEnfantCtrl (EOEditingContext edc) {

		super(edc);

		myView = new CongeGardeEnfantView(new JFrame(), true);

		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setActionBoutonValiderListener(myView.getBtnValider());

		myView.getBtnGetEnfant().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {selectEnfant();}}
				);
		myView.getBtnImprimerArrete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerArrete();}}
				);

		CocktailUtilities.initTextField(myView.getTfIdentiteEnfant(), false, false);
		CocktailUtilities.initTextField(myView.getTfDroits(), false, false);

		setDateListeners(myView.getTfDebut());
		setDateListeners(myView.getTfFin());
		setDateListeners(myView.getTfDateJustificatif());
		
		myView.getBtnImprimerArrete().setVisible(false);

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CongeGardeEnfantCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CongeGardeEnfantCtrl(editingContext);
		return sharedInstance;
	}			


	public EOCongeGardeEnfant getCurrentConge() {
		return currentConge;
	}

	public EOEnfant getCurrentEnfant() {
		return currentEnfant;
	}

	public void setCurrentEnfant(EOEnfant currentEnfant) {
		this.currentEnfant = currentEnfant;
		CocktailUtilities.viderTextField(myView.getTfIdentiteEnfant());
		if (currentEnfant != null) {
			CocktailUtilities.setTextToField(myView.getTfIdentiteEnfant(), currentEnfant.identite());
		}
	}

	public void setCurrentConge(EOCongeGardeEnfant currentConge) {
		this.currentConge = currentConge;
		updateDatas();
	}

	public EODroitsGardeEnfant getCurrentDroit() {
		return currentDroit;
	}

	/**
	 * 
	 * @param currentDroit
	 */
	public void setCurrentDroit(EODroitsGardeEnfant currentDroit) {
		
		this.currentDroit = currentDroit;
		CocktailUtilities.viderTextField(myView.getTfDroits());

		if (currentDroit != null) {
			
			String texteDroits = "";

			texteDroits += currentDroit.nbDemiJournees() + " Demi-journées";
			if (CocktailUtilities.getDateFromField(myView.getTfDebut()) != null) {
				texteDroits += " - Consommées : " + totalGardesPourAnnee(new Integer(DateCtrl.getYear(CocktailUtilities.getDateFromField(myView.getTfDebut()))));
			}

			CocktailUtilities.setTextToField(myView.getTfDroits(), texteDroits);

		}
	}

	/**
	 * 
	 * @param conge
	 */
	public void modifier(EOCongeGardeEnfant conge)	{
		setCurrentConge(conge);
		myView.setVisible(true);
	}

	public void supprimer(EOCongeGardeEnfant conge) {
		try {
			conge.absence().setEstValide(false);
			conge.setEstValide(false);

			getEdc().saveChanges();
		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}

	private void selectEnfant() {

		EOEnfant enfant = EnfantSelectCtrl.sharedInstance(getEdc()).getEnfant(getCurrentConge().individu());
		if (enfant != null) {
			setCurrentEnfant(enfant);
		}

	}

	/**
	 * 
	 */
	public void imprimerArrete() {

		NSArray<EODestinataire> destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
				NSMutableArray<EOGlobalID> destinatairesGlobalIds = new NSMutableArray<EOGlobalID>();
				for (EODestinataire destinataire : destinatairesArrete) {
					destinatairesGlobalIds.addObject(getEdc().globalIDForObject(destinataire));
				}
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class, Boolean.class};
				Object[] parametres = new Object[]{getEdc().globalIDForObject(getCurrentConge()),destinatairesGlobalIds, false};
				UtilitairesImpression.imprimerSansDialogue(getEdc(),"clientSideRequestImprimerCongeGardeEnfant", classeParametres, parametres, "CongeGarde_" + getCurrentConge().individu().noIndividu() ,"Impression de l'arrêté");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDebut());
		CocktailUtilities.viderTextField(myView.getTfFin());
		CocktailUtilities.viderTextField(myView.getTfDateJustificatif());
		setCurrentEnfant(null);
	}



	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentConge() != null) {

			CocktailUtilities.setDateToField(myView.getTfDebut(), getCurrentConge().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfFin(), getCurrentConge().dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateJustificatif(), getCurrentConge().dJustificatif());

			CocktailUtilities.setTextToArea(myView.getTfCommentaires(), getCurrentConge().commentaire());

			myView.getCheckAmDebut().setSelected(getCurrentConge().cgAmPmDebut().equals(CODE_AM));			
			myView.getCheckPmDebut().setSelected(getCurrentConge().cgAmPmDebut().equals(CODE_PM));			
			myView.getCheckAmFin().setSelected(getCurrentConge().cgAmPmFin().equals(CODE_AM));
			myView.getCheckPmFin().setSelected(getCurrentConge().cgAmPmFin().equals(CODE_PM));

			majDroits();
			
			setCurrentEnfant(getCurrentConge().enfant());

		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 * @param annee
	 * @param nbJoursConge
	 * @return
	 */
	private void verifierDroitsPourAnnee(int annee, float nbJoursConge) {

		if (getCurrentDroit() == null) {
			throw new NSValidation.ValidationException("Les Droits de garde ne sont pas définis pour l'année " + annee + "\nVeuillez les définir avant de saisir un congé !");
		}

		int nbJours = new Integer(getCurrentConge().totalGardesPourAnnee()).intValue();
		if (nbJours > getCurrentDroit().nbDemiJournees().intValue()) {
			throw new NSValidation.ValidationException("Ce congé est trop long. Les droits de garde pour l'année " + annee + " sont épuisés.\nVeuillez réduire la durée du congé !");
		}
	}

	/**
	 * 
	 */
	private void majDroits() {
		setCurrentDroit(EODroitsGardeEnfant.findForIndividuAndDate( getEdc(), getCurrentConge().individu(), CocktailUtilities.getDateFromField(myView.getTfDebut())));

	}
	/** retourne le nombre de demi-journees deja consomme pour l'annee */
	public String totalGardesPourAnnee(Integer annee) {

		BigDecimal nbDemiJournees = new BigDecimal(0);
		NSArray<EOCongeGardeEnfant> absences = EOCongeGardeEnfant.findForAnnee(getEdc(), getCurrentConge().individu(), new Integer(annee));
		for (EOCongeGardeEnfant conge : absences) {
			if (conge != getCurrentConge()) {
				nbDemiJournees = nbDemiJournees.add(new BigDecimal(conge.nbDemiJournees()));
			}
		}
		return (nbDemiJournees.setScale(0)).toString();
	}
	
	@Override
	protected void traitementsAvantValidation() {

		NSTimestamp dateDebut = CocktailUtilities.getDateFromField(myView.getTfDebut());
		NSTimestamp dateFin = CocktailUtilities.getDateFromField(myView.getTfFin());

		if (dateDebut != null && dateFin != null) {

			// CREATION DE L ABSENCE
			EOAbsences absence = getCurrentConge().absence() ;
			if (absence == null) {
				absence = EOAbsences.creer(getEdc(), getCurrentConge().individu(), 
						(EOTypeAbsence)NomenclatureFinder.findForCode(getEdc(), EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_GARDE_ENFANT));
				getCurrentConge().setAbsenceRelationship(absence);
			}
			absence.setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
			absence.setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
			absence.setAbsAmpmDebut((myView.getCheckAmDebut().isSelected())?CODE_AM:CODE_PM);
			absence.setAbsAmpmFin((myView.getCheckAmFin().isSelected())?CODE_AM:CODE_PM);
			absence.modifierDureeAbsence();

			// Verification des droits
			int anneeDebut = DateCtrl.getYear(dateDebut);
			int anneeFin = DateCtrl.getYear(dateFin);

			if (anneeDebut == anneeFin) {
				verifierDroitsPourAnnee(anneeDebut,new Float(absence.absDureeTotale()).floatValue());
			} else {
				float nbJours = DateCtrl.nbJoursEntre(dateDebut, DateCtrl.finAnnee(anneeDebut),true);
				if (absence.absAmpmFin().equals(CODE_PM)) {
					nbJours -= 0.5;
				}
				verifierDroitsPourAnnee(anneeDebut,nbJours);

				nbJours = DateCtrl.nbJoursEntre(DateCtrl.debutAnnee(anneeFin),dateFin,true);
				if (absence.absAmpmFin().equals(CODE_PM)) {
					nbJours += 0.5;
				}
				verifierDroitsPourAnnee(anneeFin,nbJours);
			}

		}

		// TODO Auto-generated method stub
		getCurrentConge().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		getCurrentConge().setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		getCurrentConge().setDJustificatif(CocktailUtilities.getDateFromField(myView.getTfDateJustificatif()));
		getCurrentConge().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));

		getCurrentConge().setCgAmPmDebut((myView.getCheckAmDebut().isSelected())?CODE_AM:CODE_PM);
		getCurrentConge().setCgAmPmFin((myView.getCheckAmFin().isSelected())?CODE_AM:CODE_PM);

		getCurrentConge().setEnfantRelationship(getCurrentEnfant());


	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		majDroits();
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}



}
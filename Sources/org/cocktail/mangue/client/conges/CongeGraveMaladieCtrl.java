/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.conges;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.conges.CongeGraveMaladieView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.mangue.conges.EOCgntCgm;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * GESTION DES CONGES  DE GRAVE MALADIE
 * @author cpinsard
 *
 */
public class CongeGraveMaladieCtrl extends ModelePageSaisie {

	private static CongeGraveMaladieCtrl sharedInstance;

	private CongeGraveMaladieView myView;
	private EOCgntCgm currentConge;

	/**
	 * 
	 * @param globalEc
	 */
	public CongeGraveMaladieCtrl (EOEditingContext edc) {

		super(edc);
		myView = new CongeGraveMaladieView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnImprimerArrete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerArrete();}}
				);

		setDateListeners(myView.getTfDebut());
		setDateListeners(myView.getTfFin());
		setDateListeners(myView.getTfDateArrete());
		setDateListeners(myView.getTfDateComite());
		setDateListeners(myView.getTfDateArreteAnnulation());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CongeGraveMaladieCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CongeGraveMaladieCtrl(editingContext);
		return sharedInstance;
	}			

	public EOCgntCgm getCurrentConge() {
		return currentConge;
	}

	public void setCurrentConge(EOCgntCgm currentConge) {
		this.currentConge = currentConge;
		updateDatas();
	}

	/**
	 * 
	 * @param conge
	 */
	public void modifier(EOCgntCgm conge)	{
		setCurrentConge(conge);
		myView.setVisible(true);
	}
	public void supprimer(EOCgntCgm conge) {
		try {
			conge.absence().setEstValide(false);
			conge.setEstValide(false);
			getEdc().saveChanges();
		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void imprimerArrete() {

		NSArray<EODestinataire> destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
				NSMutableArray<EOGlobalID> destinatairesGlobalIds = new NSMutableArray<EOGlobalID>();
				for (EODestinataire destinataire : destinatairesArrete) {
					destinatairesGlobalIds.addObject(getEdc().globalIDForObject(destinataire));
				}
				Class[] classeParametres =  new Class[] {EOGlobalID.class, NSArray.class, Boolean.class};
				Object[] parametres = new Object[]{getEdc().globalIDForObject(getCurrentConge()), destinatairesGlobalIds, false};
				UtilitairesImpression.imprimerSansDialogue(getEdc(),"clientSideRequestImprimerArreteConge", classeParametres, parametres, "ArreteCgntCgm" + getCurrentConge().individu().noIndividu() ,"Impression de l'arrêté de maternité");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ActionListenerTemSigne implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{

			if (EODialogs.runConfirmOperationDialog("Attention","Confirmez-vous la validation (signature) de ce congé ?\n" +
					"Les données ne seront ensuite plus modifiables.","Oui","Non")) {

				//currentConge.setTemConfirme("O");
				updateInterface();
			}
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDebut());
		CocktailUtilities.viderTextField(myView.getTfFin());
		CocktailUtilities.viderTextField(myView.getTfDateArrete());
		CocktailUtilities.viderTextField(myView.getTfDateComite());
		CocktailUtilities.viderTextField(myView.getTfNoArrete());
		CocktailUtilities.viderTextArea(myView.getTfCommentaires());
	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentConge() != null) {

			CocktailUtilities.setDateToField(myView.getTfDebut(), getCurrentConge().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfFin(), getCurrentConge().dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateComite(), getCurrentConge().dComMedCgm());

			CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentConge().dateArrete());
			CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentConge().noArrete());
			CocktailUtilities.setDateToField(myView.getTfDateArreteAnnulation(), getCurrentConge().dAnnulation());
			CocktailUtilities.setTextToField(myView.getTfNoArreteAnnulation(), getCurrentConge().noArreteAnnulation());

			CocktailUtilities.setTextToArea(myView.getTfCommentaires(), getCurrentConge().commentaire());

		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnImprimerArrete().setEnabled(!isModeCreation());
	}

	/**
	 * 
	 */
	private void verifierAnciennete() {
		int nbAnneesAnciennete = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_ANCIENNETE);
		NSTimestamp debut = DateCtrl.dateAvecAjoutAnnees(getCurrentConge().dateDebut(), -nbAnneesAnciennete);
		if (getCurrentConge().individu().estContractuelSurPeriodeComplete(debut, getCurrentConge().dateDebut()) == false) {
			EODialogs.runInformationDialog("Attention","Pour bénéficier d'un congé de grave maladie, un agent doit avoir au moins " + nbAnneesAnciennete + " ans d'ancienneté");
		}
	}


	@Override
	protected void traitementsAvantValidation() {

		verifierAnciennete();

		if (CocktailUtilities.getDateFromField(myView.getTfDebut()) != null 
				&& CocktailUtilities.getDateFromField(myView.getTfFin()) != null) {

			if (getCurrentConge().individu().estContractuelSurPeriodeComplete(getCurrentConge().dateDebut(), getCurrentConge().dateFin()) == false) {
				EODialogs.runInformationDialog("Attention", "Le congé de grave maladie ne s'applique qu'aux contractuels ou considérés contractuels en activité pendant toute la période.\nVeuillez saisir de nouvelles dates.");
				getCurrentConge().setDateDebut(null);
				getCurrentConge().setDateFin(null);
			}

			// Validation de la duree du conge
			int nbMois = DateCtrl.calculerDureeEnMois(getCurrentConge().dateDebut(), getCurrentConge().dateFin(), true).intValue();
			int nbMoisMin = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_DUREE_MIN);
			if (nbMois < nbMoisMin) {
				EODialogs.runInformationDialog("Attention","La durée minimum d'un congé de grave maladie est de " + nbMoisMin + " mois");
			}
			// on ne vérifie pas pour les durées supérieures à 6 mois, cela doit générer une erreur qui est gérée dans le
			// validateForSave
			nbMois = ((CongeAvecArreteAnnulation)getCurrentConge()).dureeEnMoisCongesContinusAssocies();

			int nbMoisMaxContigus = EOCgntCgm.nbMoisMaxCongesContinus(getEdc());
			if (nbMois > nbMoisMaxContigus) {
				throw new NSValidation.ValidationException("La durée maximale de congés de grave maladie continus est de " + nbMoisMaxContigus / 12 + " ans");
			}


		}

		// TODO Auto-generated method stub
		getCurrentConge().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		getCurrentConge().setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		getCurrentConge().setDateArrete(CocktailUtilities.getDateFromField(myView.getTfDateArrete()));
		getCurrentConge().setDAnnulation(CocktailUtilities.getDateFromField(myView.getTfDateArreteAnnulation()));
		getCurrentConge().setDComMedCgm(CocktailUtilities.getDateFromField(myView.getTfDateComite()));

		getCurrentConge().setNoArrete(CocktailUtilities.getTextFromField(myView.getTfNoArrete()));
		getCurrentConge().setNoArreteAnnulation(CocktailUtilities.getTextFromField(myView.getTfNoArreteAnnulation()));
		getCurrentConge().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));

		// CREATION DE L ABSENCE
		EOAbsences absence = getCurrentConge().absence() ;
		if (absence == null) {
			absence = EOAbsences.creer(getEdc(), getCurrentConge().individu(), 
					(EOTypeAbsence)NomenclatureFinder.findForCode(getEdc(), EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_MALADIE_CGM));
			getCurrentConge().setAbsenceRelationship(absence);
		}
		absence.setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		absence.setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		absence.setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(getCurrentConge().dateDebut(), getCurrentConge().dateFin(), true)));

	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}



}
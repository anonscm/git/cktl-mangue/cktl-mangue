package org.cocktail.mangue.client.conges;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.conges.AbsencesView;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class AbsencesCtrl {

	private static final int INDEX_TOUS_CONGES = 0;
	private static final int INDEX_ANNEE_CIVILE = 1;
	private static final int INDEX_ANNEE_UNIVERSITAIRE = 2;
	private static AbsencesCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext ec;
	private AbsencesView myView;

	private ListenerAbsences listenerAbsences = new ListenerAbsences();
	private PopupPeriodeListener listenerPeriode = new PopupPeriodeListener();
	private PopupTypeCongeListener listenerTypeConge = new PopupTypeCongeListener();

	private EODisplayGroup eod;
	private EOAbsences currentAbsence;
	private Conge currentConge;
	private EOIndividu currentIndividu;

	public AbsencesCtrl(EOEditingContext editingContext) {

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new AbsencesView(null, MODE_MODAL.booleanValue(),eod);

		myView.getMyEOTable().addListener(listenerAbsences);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
		);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
		);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
		);

		myView.getPopupTypePeriode().setSelectedIndex(2);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		myView.getTfPeriodeDebut().addFocusListener(new FocusListenerDateDebut());
		myView.getTfPeriodeDebut().addActionListener(new ActionListenerDateDebut());
		myView.getTfPeriodeFin().addFocusListener(new FocusListenerDateFin());
		myView.getTfPeriodeFin().addActionListener(new ActionListenerDateFin());

		myView.getPopupTypePeriode().addActionListener(listenerPeriode);
		myView.getPopupTypeConge().addActionListener(listenerTypeConge);

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null) {
			currentIndividu = EOIndividu.rechercherIndividuAvecID(ec, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false);
			updateTypeConges();
			actualiser();
			updateUI();
		}

		myView.getPopupTypePeriode().setSelectedIndex(INDEX_ANNEE_CIVILE);
		updateUI();

	}

	public static AbsencesCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new AbsencesCtrl(editingContext);
		return sharedInstance;
	}


	public EOIndividu currentIndividu() {
		return currentIndividu;
	}
	public EOAbsences currentAbsence() {
		return currentAbsence;
	}
	
	
	public Conge currentConge() {
		return Conge.rechercherCongeAvecAbsence(ec, (String)EOTypeAbsence.entitesConge().objectForKey(currentTypeAbsence().code()), currentAbsence());
	}
	public EOTypeAbsence currentTypeAbsence() {
		try {
		return (EOTypeAbsence)myView.getPopupTypeConge().getSelectedItem();
		}
		catch (Exception e) {
			return null;
		}
	}


	public void open(EOIndividu individu)	{

		clearTextFields();
		currentIndividu = individu;
		if (currentIndividu != null) {
			myView.getTfTitre().setText(currentIndividu.identitePrenomFirst() + " - MODALITES DE SERVICE");
			updateTypeConges();
			actualiser();
		}

		myView.setVisible(true);
	}

	public NSTimestamp getPeriodeDebut() {
		try {
			return DateCtrl.stringToDate(myView.getTfPeriodeDebut().getText());
		}
		catch (Exception e) {
			return null;
		}
	}
	public NSTimestamp getPeriodeFin() {
		try {
			return DateCtrl.stringToDate(myView.getTfPeriodeFin().getText());
		}
		catch (Exception e) {
			return null;
		}
	}

	public void updateTypeConges() {
		NSArray types = EOTypeAbsence.rechercherTypesCongesLegaux(ec,currentIndividu(),getPeriodeDebut(),getPeriodeFin());
		myView.initTypesConge(types);	
	}

	public void employeHasChanged(NSNotification  notification) {

		clearTextFields();
		if (notification != null) {

			currentIndividu = EOIndividu.rechercherIndividuAvecID(ec,(Number)notification.object(), false);
			updateTypeConges();
			
			actualiser();

			updateUI();
		}
	}

	/**
	 * Recherche de toutes les absences en fonction des dates et du type de conge.
	 */
	public void actualiser() {

		eod.setObjectArray(new NSArray());

		if (myView.getPopupTypeConge().getItemCount() > 0) {
			if (myView.getPopupTypeConge().getSelectedIndex() == INDEX_TOUS_CONGES)	
				eod.setObjectArray(EOAbsences.rechercherAbsencesLegalesPourIndividuEtDates(ec, currentIndividu, getPeriodeDebut(), getPeriodeFin()));		
			else {
				eod.setObjectArray(EOAbsences.rechercherAbsencesPourIndividuDatesEtTypeAbsence(ec,currentIndividu(),getPeriodeDebut(),getPeriodeFin(), currentTypeAbsence().code()));
			}
		}

		myView.getMyEOTable().updateData();

	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewAbsences() {
		return myView.getViewAbsences();
	}

	public void toFront() {
		myView.toFront();
	}

	private void ajouter() {
		
		
		
		updateUI();
	}
	private void modifier() {
		updateUI();
	}


	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette absence ?", "Oui", "Non"))		
			return;			

		try {

			currentAbsence.setTemValide("N");

			ec.saveChanges();
			actualiser();
			toFront();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			ec.revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			ec.revert();
		}
	}

	public void nettoyerChamps(NSNotification notification) {

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();
		currentIndividu = null;
		updateUI();

	}

	private void clearTextFields() {
	}

	private void updateData() {
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PopupPeriodeListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			switch (myView.getPopupTypePeriode().getSelectedIndex()) {

			case 0:
				CocktailUtilities.viderTextField(myView.getTfPeriodeDebut());
				CocktailUtilities.viderTextField(myView.getTfPeriodeFin());
				break;
			case 1:
				myView.getTfPeriodeDebut().setText(DateCtrl.dateToString(CocktailUtilities.debutAnneeCivile(new NSTimestamp())));
				myView.getTfPeriodeFin().setText(DateCtrl.dateToString(CocktailUtilities.finAnneeCivile(new NSTimestamp())));
				break;
			case 2:
				myView.getTfPeriodeDebut().setText(DateCtrl.dateToString(CocktailUtilities.debutAnneeUniversitaire(new NSTimestamp())));
				myView.getTfPeriodeFin().setText(DateCtrl.dateToString(CocktailUtilities.finAnneeUniversitaire(new NSTimestamp())));
				break;
			}

			actualiser();
			
		}
	}

	private class PopupTypeCongeListener implements ActionListener {

		public void actionPerformed(ActionEvent anAction){

			actualiser();

		}
	}


	private class ListenerAbsences implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			modifier();
		}

		public void onSelectionChanged() {

			clearTextFields();
			currentAbsence = (EOAbsences)eod.selectedObject();
			if (currentAbsence != null)
				updateData();
			updateUI();
		}
	}

	private void updateUI() {

		myView.getBtnAjouter().setEnabled(currentTypeAbsence() != null);
		myView.getBtnModifier().setEnabled(currentAbsence() != null);
		myView.getBtnSupprimer().setEnabled(currentAbsence() != null);

	}

	
	private class ActionListenerDateDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfPeriodeDebut().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfPeriodeDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de début n'est pas valide !");
				myView.getTfPeriodeDebut().selectAll();
			}
			else	{
				myView.getTfPeriodeDebut().setText(myDate);
			}
		}
	}
	private class FocusListenerDateDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfPeriodeDebut().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfPeriodeDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de début saisie n'est pas valide !");
				myView.getTfPeriodeDebut().selectAll();
			}
			else
				myView.getTfPeriodeDebut().setText(myDate);
		}
	}
	private class ActionListenerDateFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfPeriodeFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfPeriodeFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin n'est pas valide !");
				myView.getTfPeriodeFin().selectAll();
			}
			else
				myView.getTfPeriodeFin().setText(myDate);
		}
	}
	private class FocusListenerDateFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfPeriodeFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfPeriodeFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date de fin saisie n'est pas valide !");
				myView.getTfPeriodeFin().selectAll();
			}
			else
				myView.getTfPeriodeFin().setText(myDate);
		}
	}



}

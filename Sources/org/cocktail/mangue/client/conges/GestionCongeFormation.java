/*
 * Created on 14 sept. 2005
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import org.cocktail.client.components.DialogueSimple;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecArrete;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl.IntRef;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.conges.EOCongeFormation;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Gestion des conges de formation<BR>
 * La date de debut, la date de fin et le nombre de jours doivent etre saisis<BR>
 * Si la date de debut d'indemnite est fournie, la date de fin d'indemnite doit l'etre aussi<BR>
 * Verifie si l'agent peut beneficier d'un conge formation<BR>
 * Toutes les autres regles de validation sont decrites dans la classe metier<BR>
 * @author christine<BR>
 *
 */
// 27/01/2011 - Adaptation Netbeans
public class GestionCongeFormation extends GestionCongeAvecArrete {
	private Integer nbMoisUtilises, nbJoursUtilises;

	public GestionCongeFormation() {
		super("CongeFormation","Gestion des congés de formation");
	}
	// Accesseurs
	public Integer nbMoisUtilises() {
		return nbMoisUtilises;
	}
	public Integer nbJoursUtilises() {
		return nbJoursUtilises;
	}
	//	 méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("dateFinFormatee")) {
			if (validerEntitePourDates()) {
				calculerDuree(false);
			}
			super.displayGroupDidSetValueForObject(group,value,eo,key);
		} else if (key.equals("dureeJjCgf")) {
			if (currentConge().dureeJjCgf() == null) {
				currentConge().setDureeJjCgf(new Integer(0));
			}
		}
		updateDisplayGroups();
	}
	// Actions
	public void calculerDureeConge() {
		calculerDuree(true);
		updateDisplayGroups();
	}
	public void afficherChapitre() {
		String nomNotification = "SelectionChapitre";
		DialogueSimple controleur = new DialogueSimple("Chapitre",nomNotification,"Liste des chapitres",false,false,true);
		controleur.init();
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getChapitre",new Class[] {NSNotification.class}),nomNotification,null);
		controleur.afficherFenetre();
	}
	public void supprimerChapitre() {
		currentConge().removeObjectFromBothSidesOfRelationshipWithKey(((EOCongeFormation)currentConge()).chapitre(),"chapitre");
	}

	// NOTIFICATIONS
	public void getChapitre(NSNotification aNotif) {
		if (ajouterRelation(currentConge(),aNotif.object(),"chapitre")) {
			displayGroup().updateDisplayedObjects();
		}
	}
	// méthodes du controller DG
	public boolean peutModifierConge() {
		return super.peutModifier();
	}
	/** on ne peut modifier les durees que si les dates de debut, et de fin sont fournies */
	public boolean peutModifierDuree() {
		if  (super.peutModifier()) {
			return currentConge().dateDebut() != null && currentConge().dateFin() != null;
		} else {
			return false;
		}
	}
	public boolean peutSupprimerChapitre() {
		return currentConge() != null && ((EOCongeFormation)currentConge()).chapitre() != null;
	}
	/** on ne peut valider que si les dates de debut, de fin et le nombre de jours sont fournis. Si la date de debut d'indemnite est
	 * fournie, celle de fin d'indemnite doit l'etre aussi  */
	public boolean peutValider() {
		if (super.peutValider()) {
			if (currentConge().dureeJjCgf() == null) {
				return false;
			}
			return currentConge().dDebIndemniteCgf() == null || currentConge().dFinIndemniteCgf() != null;
		} else {
			return false;
		}
	}
	// Autres
	public void afficherFenetre() {
		super.afficherFenetre();
		NSDictionary dict = currentConge().dureeEnJoursAutresCongesFormation();
		nbMoisUtilises = (Integer)dict.objectForKey("mois");
		nbJoursUtilises = (Integer)dict.objectForKey("jours");
	
		controllerDisplayGroup().redisplay();
	}
	// méthodes protégées
	/** methode surchargee par les sous-classes pour charger l'archive ad-hoc */
	protected  void chargerArchive() {
		EOArchive.loadArchiveNamed("GestionCongeFormation",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
	}
	protected boolean traitementsAvantSuppression() {
		return true;
	}
	/** Lorsque la dure en mois-jours est inferieure a la duree du conge, le conge est fractionne */
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) {
			NSTimestamp dateFinPourDuree = currentConge().dateFinPourDuree();
			currentConge().setEstFractionne(DateCtrl.isBefore(dateFinPourDuree,currentConge().dateFin()));
			if (currentConge().estFractionne()) {
				absenceCourante().modifierDureeAbsence(nbJoursConge());	
			}
			return true;
		} else {
			return false;
		}
	}
	protected void traitementsApresSuppression() {}
	protected void traitementsApresValidation() {}
	protected EOTypeGestionArrete typeGestionArrete() {
		return null;
	}
	protected boolean supporteSignature() {
		return false;
	}
	protected EOCongeFormation currentConge() {
		return (EOCongeFormation)displayGroup().selectedObject();
	}
	protected float nbJoursConge() {
		if (!currentConge().estFractionne()) {
			return super.nbJoursConge();
		} else {
			NSTimestamp dateFinPourDuree = currentConge().dateFinPourDuree();
			return (float)DateCtrl.nbJoursEntre(currentConge().dateDebut(),dateFinPourDuree,true);
		}
	}
	// methodes privees
	//	Verifie si il est possible de creer ce type de conge à cette date
	private boolean validerEntitePourDates() {
		if (currentConge().dateDebut() == null) {
			currentConge().setDateFin(null);
			currentConge().setDureeJjCgf(null);
			currentConge().setDureeMmCgf(null);
			return false;
		}
		boolean estValide = true;
		if (EOChangementPosition.fonctionnaireEnActivitePendantPeriodeComplete(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin()) == false) {
			EODialogs.runInformationDialog("Attention", "Le congé de formation ne s'applique qu'aux fonctionnaires en activité pendant toute la période.\nVeuillez saisir de nouvelles dates.");
			estValide = false;
		} else if (currentConge().individu().peutBeneficierCongeFormation(currentConge().dateDebut(),currentConge().dateFin()) == false) {
			EODialogs.runInformationDialog("Attention", "L'agent pendant ces dates n'est pas dans un corps ouvrant droit au congé formation.\nVeuillez saisir de nouvelles dates.");
			estValide = false;
		}
		if (!estValide) {
			currentConge().setDateDebut(null);
			currentConge().setDateFin(null);
			currentConge().setDureeJjCgf(null);
			currentConge().setDureeMmCgf(null);
		}
		return estValide;
	}
	private void calculerDuree(boolean forcerCalcul) {
		if (currentConge().dureeJjCgf() == null && currentConge().dureeMmCgf() == null || forcerCalcul) {
			IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
			DateCtrl.joursMoisAnneesEntre(currentConge().dateDebut(),currentConge().dateFin(),anneeRef,moisRef,jourRef,true);	// inclure les bornes
			int nbMois = anneeRef.value * 12 + moisRef.value;
			int nbJours = jourRef.value;
			if (nbJours == 31) {
				nbMois++;
				nbJours = 0;
			}
			currentConge().setDureeMmCgf(new Integer(nbMois));
			currentConge().setDureeJjCgf(new Integer(nbJours));
		}
	}
}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.conges;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.conges.DetailTraitementsView;
import org.cocktail.mangue.common.conges.CalculDetailsTraitementsCMNT;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.EOCgntMaladie;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

/**
 * Controleur du détail des traitements de congé maladie pour les agents non titulaires
 *
 */
public class DetailTraitementsCMNTCtrl {
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static final String ENTITY_NAME = "CgntMaladie";
	private static DetailTraitementsCMNTCtrl sharedInstance;
	private EOEditingContext ec;
	private EODisplayGroup eod;
	private DetailTraitementsView myView;

	private EOCgntMaladie currentConge;
	private EOIndividu currentIndividu;

	public DetailTraitementsCMNTCtrl(EOEditingContext globalEc) {
		ec = globalEc;
		eod = new EODisplayGroup();
		myView = new DetailTraitementsView(new JFrame(), true, eod);

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) { fermer(); } }
		);

		myView.getTfTitre().setText("DETAIL DES TRAITEMENTS DE CONGE MALADIE (NON TITULAIRE)");
		myView.getConsole().setEnabled(true);
	}

	public static DetailTraitementsCMNTCtrl sharedInstance(EOEditingContext editingContext) {
		if(sharedInstance == null) {
			sharedInstance = new DetailTraitementsCMNTCtrl(editingContext);
		}
		return sharedInstance;
	}

	public void enregistrerDetailsTraitement(EOCgntMaladie congeMaladie) {

		currentConge = congeMaladie;
		if (currentConge != null) {
			CalculDetailsTraitementsCMNT calculCMNonTitulaire = new CalculDetailsTraitementsCMNT();
			calculCMNonTitulaire.calculerDetails(ec, currentIndividu, currentConge.dateDebut(), currentConge.dateFin());
		}
	}


	public void afficherDetails(EOIndividu individu, EOAbsences absence)	{

		currentIndividu = individu;

		Conge conge = Conge.rechercherCongeAvecAbsence(ec, ENTITY_NAME , absence);
		currentConge = (EOCgntMaladie)conge;

		// Calcul des jours calendaires et comptables du conge
		myView.getTfJoursCalendaires().setText(String.valueOf(DateCtrl.nbJoursEntre(currentConge.dateDebut(), currentConge.dateFin(), true, false)));
		myView.getTfJoursComptables().setText(String.valueOf(DateCtrl.nbJoursEntre(currentConge.dateDebut(), currentConge.dateFin(), true, true)));

		if (currentConge != null){
			myView.getLblTitreConge().setText(individu.identitePrenomFirst()+" - Congé du " + conge.dateDebutFormatee() + " au " + conge.dateFinFormatee());
			CalculDetailsTraitementsCMNT calculCMNT = new CalculDetailsTraitementsCMNT();
			eod.setObjectArray(calculCMNT.calculerDetails(ec, currentIndividu, currentConge.dateDebut(), currentConge.dateFin()));
			myView.getMyEOTable().updateData();
			myView.getConsole().setText(calculCMNT.getMessageConsole());
		}
		else {
			EODialogs.runInformationDialog("ATTENTION", "Pas de congé associé à cette absence !");
		}

		myView.setVisible(true);
	}

	private void fermer() {
		myView.setVisible(false);
	}
}

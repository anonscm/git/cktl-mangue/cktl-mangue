/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.conges;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.conges.CongeRaisonPersonnelleView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.modalites_services.ModalitesServicesCtrl;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.select.MotifCgntRfpSelectCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOMotifCgntRfp;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.conges.EOCgntRaisonFamPerso;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation.ValidationException;

/** Gere les conges pour raison familiale ou personnelle<BR>
 * 
 *
 */

public class CongeLongueDureeCtrl {
	private static CongeLongueDureeCtrl sharedInstance;

	private EOEditingContext ec;		
	private CongeRaisonPersonnelleView myView;
	private NSArray destinatairesArrete;
	private EOCgntRaisonFamPerso currentConge;
	private EOMotifCgntRfp currentMotif;

	public CongeLongueDureeCtrl (EOEditingContext globalEc) {

		//super();

		myView = new CongeRaisonPersonnelleView(new JFrame(), true);

		ec = globalEc;

		myView.getBtnValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {valider();}}
		);
		myView.getBtnAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {annuler();}}
		);
		myView.getBtnGetMotif().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {getMotifConge();}}
		);
		myView.getBtnImprimerArrete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerArrete();}}
		);

		CocktailUtilities.initTextField(myView.getTfTypeDeclaration(), false, false);

		myView.getTfNoArrete().addActionListener(new ActionListenerNoArrete());
		myView.getTfNoArrete().addFocusListener(new FocusListenerNoArrete());
		myView.getCheckSigne().addActionListener(new ActionListenerTemSigne());

		myView.getTfDebut().addFocusListener(new FocusListenerDateDebut());
		myView.getTfDebut().addActionListener(new ActionListenerDateDebut());

		myView.getTfFin().addFocusListener(new FocusListenerDateFin());
		myView.getTfFin().addActionListener(new ActionListenerDateFin());

		myView.getTfDateArrete().addFocusListener(new FocusListenerDateArrete());
		myView.getTfDateArrete().addActionListener(new ActionListenerDateArrete());

	}
	
	protected void terminer() {
		
	}
	
	public NSArray create(int nombre) {
				
		return new NSArray();
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CongeLongueDureeCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CongeLongueDureeCtrl(editingContext);
		return sharedInstance;
	}			


	private EOCgntRaisonFamPerso currentConge() {
		return currentConge;
	}

	/**
	 * 
	 *
	 */
	private void clearTextFields()	{

		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		myView.getTfTypeDeclaration().setText("");

	}

	public void ajouter(EOIndividu individu) {

		currentConge = EOCgntRaisonFamPerso.creer(ec, individu);

		updateData();
		updateUI();

		myView.setVisible(true);

	}

	public void modifier(EOCgntRaisonFamPerso conge)	{

		currentConge = conge;

		actualiser();
		myView.setVisible(true);

	}

	public void supprimer(EOCgntRaisonFamPerso conge) {
		try {
			conge.absence().setEstValide(false);
			conge.setTemValide("N");
			
			ec.saveChanges();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}
	
	private void actualiser() {
		updateData();
	}

	private void getMotifConge() {
		
		EOMotifCgntRfp motif = MotifCgntRfpSelectCtrl.sharedInstance(ec).getMotif();
		if (motif != null) {
			currentMotif = motif;
			CocktailUtilities.setTextToField(myView.getTfTypeDeclaration(), currentMotif.llMotifCgRfp());
			updateUI();
		}
		
	}


	private void updateData() {

		clearTextFields();

		currentMotif = currentConge.motif();
		
		if (currentMotif != null)
			CocktailUtilities.setTextToField(myView.getTfTypeDeclaration(), currentMotif.llMotifCgRfp());
			
		if (currentConge.dateDebut() != null)
			myView.getTfDebut().setText(DateCtrl.dateToString(currentConge.dateDebut()));

		if (currentConge.dateFin() != null)
			myView.getTfFin().setText(DateCtrl.dateToString(currentConge.dateFin()));

		//myView.getCheckSigne().setSelected(currentConge.temConfirme().equals("O"));

		if (currentConge.commentaire() != null)
			myView.getTfCommentaires().setText(currentConge.commentaire());

		updateUI();

	}


	private void valider() {

		try {

			// CONGE
			if (myView.getTfDebut().getText().length() > 0)
				currentConge.setDateDebut(DateCtrl.stringToDate(myView.getTfDebut().getText()));

			if (myView.getTfFin().getText().length() > 0)
				currentConge.setDateFin(DateCtrl.stringToDate(myView.getTfFin().getText()));

			if (myView.getTfDateArrete().getText().length() > 0)
				currentConge.setDateArrete(DateCtrl.stringToDate(myView.getTfDateArrete().getText()));

			if (myView.getTfNoArrete().getText().length() > 0)
				currentConge.setNoArrete(myView.getTfNoArrete().getText());

			if (myView.getTfCommentaires().getText().length() > 0)
				currentConge.setCommentaire(myView.getTfCommentaires().getText());

			currentConge.setMotifRelationship(currentMotif);
			
			// CREATION DE L ABSENCE
			if (currentConge.absence() == null) {
			EOAbsences absence = EOAbsences.creer(ec, currentConge.individu(), (EOTypeAbsence)NomenclatureFinder.findForCode(ec, EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_CLD));
			absence.setDateDebut(currentConge.dateDebut());
			absence.setDateFin(currentConge.dateFin());

			currentConge.setAbsenceRelationship(absence);
			}

			ec.saveChanges();

			myView.dispose();

		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());

			//currentConge.setTemConfirme("O");
			updateUI();

			myView.toFront();
		}
		catch (Exception e) {
			ec.revert();
			e.printStackTrace();
		}
	}


	private void annuler()	{

		ec.revert();
		myView.dispose();

	}


	public void imprimerArrete() {

		destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(ec).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
				NSMutableArray destinatairesGlobalIds = new NSMutableArray();
				for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(ec.globalIDForObject(e.nextElement())));
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class, Boolean.class};
				Object[] parametres = new Object[]{ec.globalIDForObject(currentConge()),destinatairesGlobalIds, false};
				UtilitairesImpression.imprimerSansDialogue(ec,"clientSideRequestImprimerArreteConge",classeParametres,parametres,"ArreteMaternite" + currentConge().individu().noIndividu() ,"Impression de l'arrêté de maternité");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}


	/**
	 * Mise a jour de l'interface.
	 * Activation (ou non) de tous les elements de l'interface.
	 */
	private void updateUI() {
		
//		myView.getCheckSigne().setEnabled(!modeCreation && !currentConge.estSigne());
//
//		myView.getCheckSigne().setEnabled(
//				!currentConge.estSigne()
//				&& !StringCtrl.chaineVide(myView.getTfDateAccouchementPrev().getText())
//				&& !StringCtrl.chaineVide(myView.getTfNoArrete().getText())
//				&& !StringCtrl.chaineVide(myView.getTfDateConstatation().getText())
//				&& !StringCtrl.chaineVide(myView.getTfDebut().getText())
//				&& !StringCtrl.chaineVide(myView.getTfFin().getText()));
//
//		myView.getBtnImprimerArrete().setEnabled(!modeCreation);
//
//		CocktailUtilities.initTextField(myView.getTfDateArrete(), 
//				false,
//				!currentConge.estSigne());
//
//		CocktailUtilities.initTextField(myView.getTfNoArrete(), 
//				false,
//				!currentConge.estSigne());

	}


	private class ActionListenerTemSigne implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{

			if (EODialogs.runConfirmOperationDialog("Attention","Confirmez-vous la validation de ce congé ?\n" +
					"Les données ne seront ensuite plus modifiables.","Oui","Non")) {

				//currentConge.setTemConfirme("O");
				updateUI();

			}
		}
	}




	private class ActionListenerDateDebut implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDebut().getText()))	{
				updateUI();
				return;
			}
			String myDate = DateCtrl.dateCompletion(myView.getTfDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDebut().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDebut().setText(myDate);
				currentConge.setDateDebut(DateCtrl.stringToDate(myDate));
				updateUI();
			}
		}
	}
	private class FocusListenerDateDebut implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDebut().getText()))	{
				updateUI();
				return;
			}
			String myDate = DateCtrl.dateCompletion(myView.getTfDebut().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDebut().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else {
				myView.getTfDebut().setText(myDate);
				currentConge.setDateDebut(DateCtrl.stringToDate(myDate));
				updateUI();
			}
		}
	}



	private class ActionListenerDateFin implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfFin().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfFin().setText(myDate);
		}
	}
	private class FocusListenerDateFin implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfFin().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfFin().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfFin().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfFin().setText(myDate);
		}
	}


	private class ActionListenerDateArrete implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if ("".equals(myView.getTfDateArrete().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateArrete().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateArrete().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfDateArrete().setText(myDate);
		}
	}
	private class FocusListenerDateArrete implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			if ("".equals(myView.getTfDateArrete().getText()))	return;

			String myDate = DateCtrl.dateCompletion(myView.getTfDateArrete().getText());
			if ("".equals(myDate))	{
				EODialogs.runInformationDialog("Date Invalide","La date saisie n'est pas valide !");
				myView.getTfDateArrete().selectAll();
				ModalitesServicesCtrl.sharedInstance(ec).toFront();
			}
			else
				myView.getTfDateArrete().setText(myDate);
		}
	}


	private class ActionListenerNoArrete implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			updateUI();
		}
	}
	private class FocusListenerNoArrete implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			updateUI();
		}
	}



}
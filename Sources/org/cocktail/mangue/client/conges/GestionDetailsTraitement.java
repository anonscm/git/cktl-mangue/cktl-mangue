//
//  GestionDetailsTraitement.java
//  Mangue
//
//  Created by Christine Buttin on Thu Sep 15 2005.
//  Copyright (c) 2001 __MyCompanyName__. All rights reserved.
//

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;
/** Evalue les periodes de plein traitement pour un agent. Les regles different selon le type de conge :<BR>
 * Pour les conges de longue duree, les details sont fournis pas l'utilisateur lors de la saisie du cld<BR>
 * Pour les conges de maladie et de longue maladie, les durees sont calcules sur des annees glissantes 
 * (1 an pour un conge de maladie, 4 pour un clm). Les durees sont calculees en jours comptables (30 jours par mois,
 * 360 jours par an).Les durees sont calculees lors de l'affichage pour eviter le probleme de l'insertion
 * de nouveaux conges.<BR>
 * 
 */
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModalDialogController;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.DetailConge;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

// 28/01/2011 - Adaptation Netbeans
public class GestionDetailsTraitement extends EOInterfaceController {
	public EOTable listeAffichage;
	private String nomEntite;
	private boolean archiveLoaded;

	public GestionDetailsTraitement(String nomEntite) {
		super();
		this.nomEntite = nomEntite;
	}
	// 28/01/2011
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void prepareComponent() {
		// archiveLoaded est setté en 1er afin d'éviter un multi-chargement de l'archive dans certains cas
		// où le chargement entraîne un appel direct à establishConnection
		if (archiveLoaded == false) {
			archiveLoaded = true;
			try {
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				super.prepareComponent();
			}
		}
	}
	// 28/01/2011
	/** surcharge pour pouvoir supporter des interfaces dans un package dont le nom se termine par interfaces */
	public void establishConnection() {
		// archiveLoaded est sette en 1er afin d'éviter un multi-chargement de l'archive dans certains cas
		// où le chargement entraîne un appel direct à prepareComponent
		if (archiveLoaded == false) {
			archiveLoaded = true;
			try {
				controllerWillLoadArchive();
				String className = this.getClass().getName();
				String archivePackageName = className.substring(0,className.lastIndexOf(".") + 1) + "interfaces";
				NSDictionary namedObjects = EOArchive.loadArchiveNamed(archiveName(), this, archivePackageName, disposableRegistry());
				controllerDidLoadArchive(namedObjects);
				setConnected(true);
				connectionWasEstablished();
			} catch (Exception exc) {
				// Il s'agit d'une interface qui est dans le même package
				super.establishConnection();
			}
			archiveLoaded = true;
		}
	}
	public void initialiser(EOGlobalID individuID, NSTimestamp dateDebut,NSTimestamp dateFin) {
		EOIndividu individuCourant = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuID,editingContext());
		// préparer les traitements
		preparerDetails(individuCourant,dateDebut,dateFin);
		if (listeAffichage != null) {	// 28/01/2011 - connectionWasEstablished supprimé et traitement listeAffichage ici
			GraphicUtilities.rendreNonEditable(listeAffichage);
			GraphicUtilities.changerTaillePolice(listeAffichage,11);
		}
	}
	public void fermer() {
		((ModalDialogController)supercontroller()).closeWindow();
	}
	private void preparerDetails(EOIndividu individuCourant,NSTimestamp dateDebut,NSTimestamp dateFin) {
		NSArray details;
		if (nomEntite.equals("Cld")) {
			details = DetailConge.preparerDetailsCld(editingContext(),individuCourant,dateDebut,dateFin);
		} else if (nomEntite.equals("CgntCgm")) {
			details = DetailConge.preparerDetailsCgm(editingContext(),individuCourant,dateDebut,dateFin);
		} else if (nomEntite.startsWith("CongeAl")) {
			details = DetailConge.preparerDetailsCongeAl(editingContext(),nomEntite,individuCourant,dateDebut,dateFin);
		} else {
			details = DetailConge.preparerDetailsGlissantsPourPeriode(editingContext(),nomEntite,individuCourant,dateDebut,dateFin);
		}
		displayGroup().setObjectArray(details);
		displayGroup().updateDisplayedObjects();
	}



}

/*
 * Created on 13 sept. 2005
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.outils_interface.GestionCongeNaissance;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.mangue.conges.EOCongePaternite;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;

/** Gere les conges de paternite<BR>
* Regles de gestion<BR>
* Les conges de paternite ne sont accordes qu'aux hommes<BR>
* la date de naissance, la date de debut et la date fin doivent etre fournies<BR>
* On ne peut saisir la date de debut que si la date de naissance est saisie et la date de fin que si celle de debut est saisie
* Les dates sont gerees automatiquement<BR>
* Verification de dates<BR>
* Le conge de paternite est de 11 jours consecutifs en cas de naissance, 18 jours en cas de naissance multiple<BR>
* Les regles de validation sont dans la classe EOCongePaternite<BR>
* @author christine<BR>
**/
// 28/01/2011 - Adaptation Netbeans
public class GestionCongePaternite extends GestionCongeNaissance {
	private boolean enfantsADeclarer;
	
	public GestionCongePaternite() {
        super("CongePaternite","Gestion des congés de paternité");
    }
	// méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
	 	if (key.equals("dateDebutFormatee")) {
	 		if (currentConge().dateDebut() != null && DateCtrl.isBefore(currentConge().dateDebut(),DateCtrl.stringToDate("01/01/2002"))) {
	 			EODialogs.runErrorDialog("Erreur","Les congés de paternité n'existent que depuis 2002 !");
	 			currentConge().setDateDebut(null);
	 		}
	 		preparerDateFin();
	 	} else if (key.equals("estGrossesseMultiple")) {
	 		preparerDateFin();
	 	} else if (key.equals("dateNaissPrevFormatee") && currentConge().dNaisPrev() == null) {
	 		currentConge().setDateDebut(null);
	 		preparerDateFin();
	 	}
		// pour préparer les vues arrêté
	 	super.displayGroupDidSetValueForObject(group,value,eo,key);
	 }
	 // méthodes du controller DG
	 public boolean peutValider() {
	 	if (super.peutValider()) {
		 	return currentConge().dNaisPrev() != null;
	 	} else {
	 		return false;
	 	}
	 }
	 public boolean peutModifierDateDebut() {
	 	return super.peutModifierDateDebut() && currentConge().dNaisPrev() != null;
	 }
	 // methodes protegees
	 /** methode surchargee par les sous-classes pour charger l'archive ad-hoc */
	 protected  void chargerArchive() {
	 	EOArchive.loadArchiveNamed("GestionCongePaternite",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
	 }
	 /** g&egrave;re les contr&ocirc;les concernant les declarations d'enfants */
	 protected boolean traitementsAvantValidation() {	
	 	enfantsADeclarer = false;
	 	NSArray enfants = EOEnfant.rechercherEnfantsPourIndividuDate(editingContext(),currentConge().individu(),currentConge().dNaisPrev(),false);
	 	if (enfants.count() == 0) {
	 		enfantsADeclarer = true;
	 	} else {
	 		if (enfants.count() == 1 && currentConge().estGrossesseMultiple()) {
	 			EODialogs.runErrorDialog("ERREUR","Vous déclarez une grossesse multiple, or vous n'avez déclaré qu'un enfant né à cette date");
	 			return false;
	 		}
	 		if (enfants.count() > 1 && currentConge().estGrossesseMultiple() == false) {
	 			EODialogs.runErrorDialog("ERREUR","Vous déclarez une grossesse simple, or vous avez déclaré plus d'un enfant né à cette date");
	 			return false;
	 		}
	 	}
	 	return super.traitementsAvantValidation();
	 }
	 /** traitements apres validation : on propose si les enfants n'ont pas encore ete declares de le faire */
	 protected void traitementsApresValidation() {
	 	if (enfantsADeclarer) {
	 		String message = "Vous n'avez pas encore déclaré l'enfant";
	 		if (currentConge().estGrossesseMultiple()) {
	 			message = "Vous n'avez pas encore déclaré les enfants";
	 		}
	 		if (EODialogs.runConfirmOperationDialog("ATTENTION",message + "\nSouhaitez-vous le faire ?","Oui","Non")) {
	 			NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,"Enfants");
	 		}
	 	}
	 }
	protected boolean traitementsAvantSuppression() {
		return true;
	}
	// Gestion des arrêtés
   	protected void preparerPourRemplacement() {}
	protected String nomTableCongeContractuel() {
	 	return "CONGE_PATERNITE";
	}
	protected String nomTableCongeFonctionnaire() {
	 	return "CONGE_PATERNITE";
	}
	 
	 // méthodes privées
	private EOCongePaternite currentConge() {
	 	return (EOCongePaternite)displayGroup().selectedObject();
	}
	private void preparerDateFin() {
	 	if (currentConge().dateDebut() == null) {
	 		currentConge().setDateFin(null);
	 	} else {
			int nbJoursAAjouter= EOCongePaternite.NB_JOURS_SIMPLE; 	// 11 jours pour un congé simple
	 		if (currentConge().estGrossesseMultiple()) {
	 			nbJoursAAjouter = EOCongePaternite.NB_JOURS_MULTIPLE;	// 18 jours
	 			}
	 		currentConge().setDateFin(DateCtrl.dateAvecAjoutJours(currentConge().dateDebut(),nbJoursAAjouter - 1));
	 	}
	 	changerAffichageArrete();
	}
}

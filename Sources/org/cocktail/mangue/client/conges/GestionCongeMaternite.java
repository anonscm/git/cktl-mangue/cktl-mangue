/*
 * Created on 14 sept. 2005
 *
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import java.awt.Font;

import javax.swing.JLabel;

import org.cocktail.client.components.DialogueSimple;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecArreteAnnulation;
import org.cocktail.mangue.client.outils_interface.GestionCongeNaissance;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeCgMatern;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaternite;
import org.cocktail.mangue.modele.mangue.conges.EODeclarationMaternite;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

/** Gere les conges de maternite<BR>
 * Le conge de maternite est associe a une declaration de maternite. 
 * Lorsqu'on cree un nouveau conge de maternite, on cree la declaration de maternite.
 * On commence par definir un conge de type Maternite<BR>
 * L'ajout de conges maternite lies a cette meme declaration se fait depuis 
 * la fenetre geree par ce controleur.<BR>
 * Les conges de maternite pouvant etre fractionnes en cas d'hospitalisation (pour une femme). 
 * L'utilisateur doit modifier les dates de debut et de fin du conge principal, puis ajouter un conge avant de valider
 * pour avoir une duree globale sur plusieurs conges correspondant aux regles de gestion<BR>
 * Regles de gestion<BR>
 * Les conges de maternite peuvent etre accordes aux hommes en cas de deces de la mere a l'accouchement<BR>
 * Le type de conge, la date debut et la date fin doivent etre fournis<BR>
 * Une grossesse non menee a terme ne peut etre que de type "conge de type Etat pathologique du a la grossesse"<BR>
 * Un conge de type Etat pathologique du a la grossesse ne peut etre pris qu'avant le debut du conge maternite.<BR>
 * Un conge de type Etat pathologique suite a l'accouchement ne peut etre saisi que si un conge maternite a ete saisi avec la date d'accouchement.<BR>
 * Un conge de maternite prolonge une duree de stage, si l'agent est en stage pendant cette periode.<BR>
 * La declaration d'un conge de maternite ne peut etre annulee que si un arrete a ete produit. Dans ce cas,
 * on ne peut plus associe de conge a cette declaration. Dans le cas contraire (pas d'arrete), on peut supprimer le conge et sa declaration<BR>
 * Verification de dates<BR>
 * <UL>conge de type Etat pathologique du a la grossesse : la date de constat et date de naissance previsionnelle doivent etre fournies</UL>
 * <UL>conge de type Etat pathologique suite a l'accouchement : la date d'accouchement doit etre fournie</UL>
 * <UL>conge de type Maternite : la date de constat et la date de naissance previsionnelle doivent etre fournies (pour une femme) ou la date de constat et la date d'accouchement</UL>
 * Le conge de type Etat pathologique du a la grossesse est de 2 semaines<BR>
 * Le conge de type Etat pathologique suite a l'accouchement est de 4 semaines<BR>
 * Pour la naissance du 1er ou du 2eme enfant, le conge de maternite est de 16 semaines,<BR>
 * Pour la naissance du 3eme enfant et plus, le conge de maternite est de 26 semaines<BR>
 * Pour la naissance de jumeaux, le conge de maternite est de 34 semaines<BR>
 * Pour la naissance de triples ou plus, le conge de maternite est de 46 semaines<BR>
 * En cas d'accouchement apres la date presumee, la periode postnatale n'est pas reduite => la date de fin est repoussee du nombre de jours entre la date d'accouchement et la date presumee.<BR>
 * Les regles de validation sont dans la classe EOCongeMaternite<BR>
 * Ce controleur peut etre instancie depuis GestionAbsences ou depuis lui-meme (quand on ajoute des conges pour une meme
 * declaration de maternite). Dans ce dernier cas, on travaille dans le meme editingContext<BR>
 *
 */

public class GestionCongeMaternite extends GestionCongeNaissance {
	public JLabel labelDateAccouchement,labelDateNaissancePrev;
	private NSTimestamp previousDateFin = null;
	private int reliquatConge = 0;
	private static NSArray typesCongesMaternite;
	private boolean windowHidden;	// pour gérer l'empilement des fenêtres quand on ajoute un congé depuis la fenêtre congé : on masque alors la fenêtre précédente
	private boolean creerDeclarationMaternite;

	public GestionCongeMaternite(boolean creerDeclarationMaternite) {
		super("CongeMaternite","Gestion des congés de maternité");
		windowHidden = false;
		this.creerDeclarationMaternite = creerDeclarationMaternite;
	}
	public GestionCongeMaternite() {
		this(true);
	}
	// méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {

		if (key.equals("dateDebutFormatee")) {
			EOCongeMaternite congePrincipal = declarationMaternite().congePrincipal();
			// un congé de type pathologie grossesse ne peut se prendre qu'avant le début du congé maternité
			if (currentConge().estTypePathologieGrossesse() &&  congePrincipal != null &&
					DateCtrl.isAfter(currentConge().dateDebut(), congePrincipal.dateDebut())) {
				EODialogs.runErrorDialog("Erreur","Un congé de type grossesse pathologique ne peut se prendre qu'avant le congé de maternité");
				currentConge().setDateDebut(null);
				if (currentConge().dateFin() != null) {
					currentConge().setDateFin(null);
				}	
			} else
				preparerDateFin();
		} else if (key.equals("dateFinFormatee")) {
			if (previousDateFin != null && declarationMaternite() != null
					&& declarationMaternite().dAccouchement() != null 
					&& !DateCtrl.isBefore(declarationMaternite().dAccouchement(), declarationMaternite().dNaisPrev()) ) {
				if (DateCtrl.isAfter(currentConge().dateFin(),previousDateFin)) {
					EODialogs.runErrorDialog("Erreur","Vous ne pouvez pas prolonger ce type de congé, il a atteint sa durée maximum");
					currentConge().setDateFin(previousDateFin);
				} else if (currentConge().estTypeMaternite()) {
					boolean result = EODialogs.runConfirmOperationDialog("Attention","Vous avez modifié la fin du congé maternité. Un congé de maternité ne peut se fractionner que dans le cas d'une hospitalisation de l'enfant.\n Est-ce bien le cas ?","Oui","Non");
					if (result)
						ajouterConge(true);

				}
			}
		} else if (currentConge().estTypeMaternite()) {

			if (key.equals(EOCongeMaternite.DECLARATION_MATERNITE_KEY+".estGrossesseGemellaire")) {
				declarationMaternite().setEstGrossesseTriple(false);
				preparerDates();
			} else if  (key.equals(EOCongeMaternite.DECLARATION_MATERNITE_KEY+".estGrossesseTriple")) {
				declarationMaternite().setEstGrossesseGemellaire(false);
				preparerDates();
			} else if (key.equals(EOCongeMaternite.DECLARATION_MATERNITE_KEY+".dateNaissPrevFormatee") 
					|| key.equals(EOCongeMaternite.DECLARATION_MATERNITE_KEY+".nbEnfantsDecl")) {
				preparerDates();
			} else if (key.equals(EOCongeMaternite.DECLARATION_MATERNITE_KEY+".dateAccouchementFormatee")) {
				if (declarationMaternite().dAccouchement() != null) {
					if (currentConge().individu().estHomme()) {
						preparerDates();
					} else {

						if ( DateCtrl.isBefore(declarationMaternite().dAccouchement(), declarationMaternite().dNaisPrev()) ) {
							preparerDates();
						}
						else {
							preparerDates();
							if (currentConge().dateFin() != null) {
								// la date de naissance prévisionnelle doit être fournie auparavant.
								// Si la date d'accouchement est postérieure à la date prévisionnelle, on repousse la fin du congé maternité
								int nbJoursAAjouter = DateCtrl.nbJoursEntre(declarationMaternite().dNaisPrev(),declarationMaternite().dAccouchement(),false);
								if (nbJoursAAjouter > 0) {
									currentConge().setDateFin(DateCtrl.dateAvecAjoutJours(currentConge().dateFin(),nbJoursAAjouter));
								} else {
									preparerDateFin();	// pour être sûr d'avoir la bonne date
								}
							}
						}
					}
				} else {
					preparerDateFin();
				}
			} else if (key.equals(EOCongeMaternite.DECLARATION_MATERNITE_KEY+".estDeclarationAnnulee")) {
				modifierValiditeConges(declarationMaternite().estDeclarationAnnulee());
			} 
			else if (key.equals(EOCongeMaternite.DECLARATION_MATERNITE_KEY+".estGrossesseInterrompue")) {
				Font font = labelDateNaissancePrev.getFont();
				if (declarationMaternite().estGrossesseInterrompue()) {
					currentConge().setDateDebut(null);
					currentConge().setDateFin(null);
					labelDateNaissancePrev.setFont(new Font(font.getName(),Font.PLAIN,font.getSize()));
				} else {
					preparerDates();
					labelDateNaissancePrev.setFont(new Font(font.getName(),Font.BOLD,font.getSize()));
				}
			}
		} 
		super.displayGroupDidSetValueForObject(group,value,eo,key);
	}

	/**
	 * 
	 */
	public void afficherTypeConge() {
		EOQualifier qualifier = null;
		if (declarationMaternite() != null) {
			// Ne pas autoriser de sélectionner des congés de maternité de même type 
			// pour les autres congés pathologiques, ne pas dépasser la durée maximum
			// Ne pas autoriser le type Pathologique après accouchement si la date réelle d'accouchement n'est pas
			// définie
			// declarationMaternite est forcément non nul sinon on ne peut pas sélectionner le type de congé
			String stringQualifier = null;
			NSMutableArray typesConges = new NSMutableArray();
			NSMutableArray args = new NSMutableArray();
			if (declarationMaternite().dAccouchement() == null) {
				args.addObject(EOTypeCgMatern.PATHOLOGIE_ACCOUCHEMENT);
				stringQualifier = EOTypeCgMatern.CODE_KEY + " != %@";
				typesConges.addObject(SuperFinder.rechercherAvecAttributEtValeurEgale(editingContext(), EOTypeCgMatern.ENTITY_NAME, INomenclature.CODE_KEY, EOTypeCgMatern.PATHOLOGIE_ACCOUCHEMENT));
			}
			NSArray congesValides = declarationMaternite().congesValides();
			if (congesValides.count() > 0) {
				for (int i = 0; i < congesValides.count(); i++) {
					EOCongeMaternite conge = (EOCongeMaternite)congesValides.objectAtIndex(i);
					if (conge.toTypeCgMatern() != null && typesConges.containsObject(conge.toTypeCgMatern()) == false) {
						typesConges.addObject(conge.toTypeCgMatern());
						if (typeCongeEpuise(conge)) {	// on ne pourra plus sélectionner ce type
							args.addObject(conge.toTypeCgMatern().code());
							if (stringQualifier == null) {
								stringQualifier = EOTypeCgMatern.CODE_KEY + " != %@";
							} else {
								stringQualifier = stringQualifier + " AND " +  EOTypeCgMatern.CODE_KEY + " != %@";
							}
						}
					}
				}
			}
			if (stringQualifier != null) {
				qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
			}
		}
		afficherDialogue(EOTypeCgMatern.ENTITY_NAME, "getTypeConge", qualifier);
	}
	public void ajouterConge() {
		ajouterConge(false);
	}
	//	annulation des arrêtés
	public void annulerArrete() {
		windowHidden = true;
		window().setVisible(false);
		super.annulerArrete();

	}
	// NOTIFICATIONS
	// déclenchée suite à l'ajout d'un congé depuis cette fenêtre
	public void terminer(NSNotification aNotif) {
		if (!windowHidden) {
			terminer();
		}
	}
	// déclenchée suite à la validation ou l'annulation dans une fenêtre ouverte à partir de cette fenêtre
	public void unlockSaisie(NSNotification aNotif) {
		if (windowHidden) {
			// Il faut changer l'état avant d'afficher la fenêtre sinon il est changé à un moment imprévisible
			windowHidden = false;
			controllerDisplayGroup().redisplay();	// pour avoir l'état du bouton d'ajout correct
			//  Il y a eu empilement de fenêtre, locker de nouveau et rendre visible la fenêtre
			NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
			window().setVisible(true);
		}
	}
	public void getTypeConge(NSNotification aNotif) {
		Font font = labelDateAccouchement.getFont();
		Font font1 = labelDateNaissancePrev.getFont();
		ajouterRelation(currentConge(),aNotif.object(),"toTypeCgMatern");
		if (currentConge().estTypePathologieAccouchement()) {
			labelDateAccouchement.setFont(new Font(font.getName(),Font.BOLD,font.getSize()));
			labelDateNaissancePrev.setFont(new Font(font1.getName(),Font.PLAIN,font1.getSize()));

		} else {
			labelDateAccouchement.setFont(new Font(font.getName(),Font.PLAIN,font.getSize()));
			if (declarationMaternite() != null && !declarationMaternite().estGrossesseInterrompue()) {
				labelDateNaissancePrev.setFont(new Font(font1.getName(),Font.BOLD,font1.getSize()));
			} else {
				labelDateNaissancePrev.setFont(new Font(font1.getName(),Font.PLAIN,font1.getSize()));
			}
		}
		// on change de type de congé, revoir les dates
		if (currentConge().estTypeMaternite()) {
			preparerDates();	// pour le congé de maternité
		} else {
			if (currentConge().estTypePathologieAccouchement()) {
				// vérifier dans le cas d'un congé de type pathologie accouchement que la date d'accouchement est fournie
				// et qu'il existe un congé de maternité déjà défini pour récupérer sa date de fin
				if (declarationMaternite() != null) {
					NSTimestamp dateDebut = null;
					if (declarationMaternite().dAccouchement() == null) {
						EODialogs.runErrorDialog("ERREUR","Vous ne pouvez pas définir un congé de type \"" + currentConge().toTypeCgMatern().libelleLong() + "\" car la date d'accouchement n'est pas fournie");
					} else {
						for (java.util.Enumeration<EOCongeMaternite> e = declarationMaternite().congesValidesDeTypeMaternite().objectEnumerator();e.hasMoreElements();) {
							EOCongeMaternite conge = e.nextElement();
							if (dateDebut == null || DateCtrl.isBefore(dateDebut,conge.dateFin())) {
								dateDebut = conge.dateFin();
							}
						}
						if (dateDebut == null) {
							EODialogs.runErrorDialog("ERREUR","Vous ne pouvez pas définir un congé de type \"" + currentConge().toTypeCgMatern().libelleLong() + "\" car il n'y a pas de congé maternité de type \"Maternite\"");
						}	
					}
					if (dateDebut == null) {
						currentConge().removeObjectFromBothSidesOfRelationshipWithKey(currentConge().toTypeCgMatern(),"toTypeCgMatern");	
					} else {
						currentConge().setDateDebut(DateCtrl.jourSuivant(dateDebut));
					}
				}  else {
					currentConge().setDateDebut(null);
				}
				preparerDateFin();
			}
		}
		NSNotificationCenter.defaultCenter().removeObserver(this, getClass().getName() + "_SelectionTypeCgMatern", null);
		updateDisplayGroups();
	}
	public void annulationSelectionTypeConge(NSNotification aNotif) {
		NSNotificationCenter.defaultCenter().removeObserver(this, getClass().getName() + "_SelectionTypeCgMatern", null);
	}
	// autres
	public void afficherFenetre() {
		// garder la liste des types de congé maternité dans un tableau (pour accélérer les traitements)
		typesCongesMaternite = SuperFinder.rechercherEntite(editingContext(),"TypeCgMatern");
		if (declarationMaternite() == null && creerDeclarationMaternite) {
			LogManager.logDetail("GestionCongeMaternite - afficherFenetre declarationMaternite");
			// créer la déclaration de maternité et forcer le type à congé de maternité
			creerDeclarationMaternite();
			// Forcer pour le premier congé le type Maternité
			for (java.util.Enumeration<EOTypeCgMatern> e = typesCongesMaternite.objectEnumerator();e.hasMoreElements();) {
				EOTypeCgMatern type = e.nextElement();
				if (type.estMaternite()) {
					currentConge().setToTypeCgMaternRelationship(type);
					break;
				}
			}
		}

		if (declarationMaternite() != null && currentConge().dateFin() != null && currentConge() == declarationMaternite().congePrincipal()) {
			previousDateFin = currentConge().dateFin();		
		}
		Font font = labelDateNaissancePrev.getFont();
		labelDateNaissancePrev.setFont(new Font(font.getName(),Font.BOLD,font.getSize()));
		if (currentConge().estTypeMaternite()) {
			if (declarationMaternite() != null && declarationMaternite().estGrossesseInterrompue()) {
				labelDateNaissancePrev.setFont(new Font(font.getName(),Font.PLAIN,font.getSize()));
			} else {
				labelDateNaissancePrev.setFont(new Font(font.getName(),Font.BOLD,font.getSize()));
			}
		} else if (currentConge().estTypePathologieAccouchement()) {
			labelDateAccouchement.setFont(new Font(font.getName(),Font.BOLD,font.getSize()));
		}
		super.afficherFenetre();
	}
	// méthodes du controller DG
	public boolean peutValider() {
		if (super.peutValider()) {
			if (currentConge().toTypeCgMatern() == null) {
				return false;
			}	
			// pour un congé de maternité de type maternité ou pathologie grossesse sans interruption de la grossesse,
			// la date de naissance prévisionnelle doit être fournie (pour une femme)
			if (currentConge().estTypePathologieGrossesse() ||
					(currentConge().estTypeMaternite()  && declarationMaternite() != null && !declarationMaternite().estGrossesseInterrompue())) {
				if (declarationMaternite() != null && declarationMaternite().dNaisPrev() == null && currentConge().individu().estHomme() == false) {
					return false;
				}
			} else if (declarationMaternite() != null && currentConge().estTypePathologieAccouchement()) {
				if (declarationMaternite().dAccouchement() == null) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}
	public boolean peutSaisirDateConstatation() {
		return peutModifier() && currentConge().individu().estHomme() == false && currentConge().absence() != null && 
		currentConge().absence().estValide() && declarationMaternite().estDeclarationAnnulee() == false;
	}
	/** on ne peut modifier la declaration de maternite que pour le conge "principal", si elle n'est pas annulee et
	 * si le conge n'a pas ete fractionne ou qu'il s'agit d'un conge de remplacement */
	public boolean peutModifierDeclaration() {
		if (super.peutModifierDeclaration()) {

			if (declarationMaternite() == null  || declarationMaternite().estDeclarationAnnulee()) {
				return false;
			}
			if (currentConge().toTypeCgMatern() != null && (currentConge().estTypePathologieAccouchement() || currentConge().estTypePathologieGrossesse())) {
				return true;
			}
			EOCongeMaternite congePrincipal = declarationMaternite().congePrincipal();
			if (congePrincipal == null) {
				return true;
			} else if (congePrincipal == currentConge() && (!declarationMaternite().estCongeFractionne() || estRemplacementConge())) {
				// si il s'agit du congé principal et qu'il n'est pas fractionné ou que c'est le congé de remplacement,
				// on peut modifier la déclaration
				return true;
			} else {
				return false;
			}

		}

		return false;

	}

	public boolean aSaisirPourUneFemme() {
		return peutModifier() && peutModifierDeclaration() && currentConge().individu().estHomme() == false;	// pas d'arrêté signé
	}


	/** on ne peut signaler que la grossesse est interrompue que si l'accouchement n'a pas eu lieu
	 * ou si la date d'accouchement est antérieure à la date d'accouchement prévisionnelle
	 * @return
	 */
	public boolean peutDeclarerGrossesseInterrompue() {
		return aSaisirPourUneFemme() && ( declarationMaternite().dAccouchement() == null || declarationMaternite().dNaisPrev() == null || DateCtrl.isBefore(declarationMaternite().dAccouchement(), declarationMaternite().dNaisPrev()) ) ;
	}

	/** on ne peut modifier la date de naissance previsionnelle que si il n'y a qu'un seul conge pour cette declaration 
	 * de maternite, que la declaration de materntite n'est pas annulee et qu'il n'y a pas d'arrete signe */
	public boolean peutModifierDateNaisPrev() {
		return peutModifier() && peutModifierDeclaration() && (declarationMaternite().congesMaternite().count() == 1);
	}
	/** on ne peut modifier la date d'accouchement si on peut modifier la declaration et qu'il n'y a pas d'autre conge type "Maternite"
	 * ou "Pathologie apres accouchement" defini. La modification est autorisee meme lorsque l'arrete
	 * est signe et que la date d'accouchement n'est pas encore saisie
	 * */
	public boolean peutSaisirDateAccouchement() {
		boolean peutModifier = peutModifier();
		if (!peutModifier) {
			// Vérifier si c'est parce que l'arrêté est signé
			if (congeCourant() != null && congeCourant().individu() != null &&
					gestionArrete() != null && gestionArrete().arreteCourant() != null && gestionArrete().arreteCourant().estSigne()) {
				if (currentConge().declarationMaternite() != null && currentConge().declarationMaternite().dAccouchement() == null) {
					peutModifier = true;	// on autorise la modification de la date d'accouchement
				}
			}
		}

		if (peutModifier && peutModifierDeclaration()) {
			if (currentConge().individu().estHomme()) {
				return true;
			} else {
				if (currentConge().declarationMaternite() != null && currentConge().declarationMaternite().estGrossesseInterrompue()) {
					return false;
				}
				if (declarationMaternite() != null && declarationMaternite().dNaisPrev() != null) {
					// si c'est un congé de remplacement d'un congé de type maternité, on doit pouvoir le faire
					if (currentConge().estTypeMaternite() && estRemplacementConge()) {
						return true;
					}
					// il ne faut pas qu'il y ait plusieurs congés de maternité défini ou un congé pathologie après accouchement, 
					// pour pouvoir modifier la date d'accouchement
					int nbMaternite = 0;
					if (declarationMaternite() != null) {
						for (java.util.Enumeration<EOCongeMaternite> e = declarationMaternite().congesValides().objectEnumerator();e.hasMoreElements();) {
							EOCongeMaternite conge = e.nextElement();
							if (conge.estTypePathologieAccouchement()) { 
								return false;
							} else if (conge.estTypeMaternite()) {
								nbMaternite++;
							}
						}
					}
					return nbMaternite <= 1;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
	}	
	/** On ne peut choisir le type de conge qu'a la creation du conge */
	public boolean peutSelectionnerTypeConge() {
		return currentConge() != null && currentConge().toTypeCgMatern() == null && declarationMaternite() != null; 
	}

	/** on peut modifier la date de debut pour les conges de type maternite si la date d'accouchement
	 * n'est pas saisie et que le conge principal concerne des triples.<BR>
	 * On peut modifier la date de debut pour les conges de type pathologie accouchement si la date d'accouchement est definie
	 * On peut modifier la date de debut pour les conges de type pathologie avant la grossesse si la date de naissance previsionnelle est fournie */
	public boolean peutModifierDateDebut() {
		if (super.peutModifierDateDebut() && peutSaisirCongeMaternite()) {	// pas d'arrêté signé
			if (currentConge().toTypeCgMatern() == null 
					||  (currentConge().individu().estHomme() 
								&& declarationMaternite() != null 
								&& declarationMaternite().estCongeFractionne() == false)	) {
				return false;
			}
			if (currentConge().declarationMaternite() != null 
					&& (currentConge().declarationMaternite().estGrossesseInterrompue()
					|| (currentConge().declarationMaternite().dNaisPrev() != null && currentConge().declarationMaternite().dAccouchement() != null && DateCtrl.isBefore(currentConge().declarationMaternite().dAccouchement(), currentConge().declarationMaternite().dNaisPrev())  ) )
					) {
				return true;
			}
			if (currentConge().declarationMaternite() != null && currentConge().declarationMaternite().estGrossesseInterrompue()) {
				return true;
			}
			if (currentConge().estTypePathologieGrossesse() && declarationMaternite() != null) {
				return declarationMaternite().dNaisPrev() != null;
			}
			if (currentConge().estTypePathologieAccouchement() && declarationMaternite() != null) {
				return declarationMaternite().dAccouchement() != null;
			}
			// congé de type maternité
			// on ne peut modifier la date de début qu'en cas de fractionnement 
			// ou si la date d'accouchement n'est pas fournie et qu'il ne s'agit pas de triplés

			EOCongeMaternite congePrincipal = declarationMaternite().congePrincipal();
			return congePrincipal == null || (currentConge() != congePrincipal) || (declarationMaternite().dAccouchement() == null);
			
		} else {
			return false;
		}
	}
	
	/** la date de fin  peut être changee que dans le cas ou on veut fractionner un conge pour hospitalisation de
	 *  l'enfant ou pour les conges autres que type maternite si ils n'excedent pas la duree totale autorisee */
	public boolean peutModifierDateFin() {
		if (super.peutModifierDateFin()) {	// si pas d'arrêté signé et date début fournie et déclaration maternité modifiable
			if (declarationMaternite() != null && !declarationMaternite().estGrossesseInterrompue()) {
				if (currentConge().dateFin() != null && currentConge().toTypeCgMatern() != null) {		/// la date de fin a été saisie ou calculée auparavant
					if (currentConge().estTypeMaternite()) {
						// la date de fin ne peut être changée que dans le cas où on veut fractionner un congé pour hospitalisation de l'enfant (=> accouchement fait)
						// et que le fractionnement n'est pas déjà fait 
						return declarationMaternite().estCongeFractionne() == false && declarationMaternite().dAccouchement() != null;	
					} else {
						return true;
					}
				} else {
					return false;	// la date fin est calculée dans presque tous les cas
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	/** on n'annule que les declarations pour lesquelles un arreteest signe */
	public boolean peutAnnulerDeclaration() {
		return declarationMaternite() != null && currentConge() == declarationMaternite().congePrincipal() && !peutModifier(); // i.e l'arrêté a été signé
	}
	/** on ne peut remplacer un conge par un autre que si un arrete a ete signe et qu'il ne s'agit pas :
	 * d'un conge de type Etat Pathologique Accouchement, de type Etat Pathologique Grossesse avec la date d'accouchement fournie, du conge principal
	 */
	public boolean peutAnnulerArrete() {
		if (super.peutAnnulerArrete()) {
			if (currentConge().estTypePathologieGrossesse() && declarationMaternite() != null && declarationMaternite().dAccouchement() != null) {
				// on ne peut pas annuler un congé de type pathologie grossesse si l'accouchement a eu lieu
				return false;
			}
			if (currentConge().estTypePathologieAccouchement()) {
				return false;
			}
			if (currentConge().estTypeMaternite()) {
				if (declarationMaternite() != null) {
					return  currentConge() == declarationMaternite().congePrincipal();
				} else {
					return true;
				}
			}
			return true;
		} else {
			return false;
		}
	}
	
	/** le conge sans traitement ne s'applique qu'aux contractuels */
	public boolean peutAvoirCongeSansTraitement() {
		if (!peutSaisirCongeMaternite()) {
			return false;
		}
		return super.peutAvoirCongeSansTraitement();
	}
	
	/** on ne peut ajouter un nouveau conge associe a cette declaration que si celle-ci n'est
	 * pas annulee ou si la grossesse n'est pas interrompue, qu'il s'agit d'un conge de type maternite,
	 * que tous les types de conge n'ont pas ete pris ou que parmi les types restant, 
	 * il ne reste que le conge pathologique accouchement et la date d'accouchement est definie
	 */
	public boolean peutAjouterConge() {
		if (typeTraitement() == MODE_CREATION || declarationMaternite().estGrossesseInterrompue() || 
				currentConge().estAnnule() || currentConge().individu().estHomme() || currentConge().estTypeMaternite() == false) {
			return false;
		}
		NSMutableArray typesEpuises = new NSMutableArray();
		for (java.util.Enumeration<EOCongeMaternite> e = declarationMaternite().congesValides().objectEnumerator();e.hasMoreElements();) {
			EOCongeMaternite conge = e.nextElement();
			if (typesEpuises.containsObject(conge.toTypeCgMatern()) == false && typeCongeEpuise(conge)) {
				typesEpuises.addObject(conge.toTypeCgMatern());
			}
		}
		// Déterminer les types possibles et n'afficher que si le count est > 0
		NSMutableArray typesPossibles = new NSMutableArray(typesCongesMaternite);
		typesPossibles.removeObjectsInArray(typesEpuises);
		if (typesPossibles.count() > 0) {
			if (typesPossibles.count() == 1) {
				EOTypeCgMatern type = (EOTypeCgMatern)typesPossibles.objectAtIndex(0);
				return (type.estPathologieAccouchement() == false || declarationMaternite().dAccouchement() != null);
			} else {
				return true;
			}
		} else {
			return false;
		}

	}

	// méthodes protégées
	/** methode surchargee par les sous-classes pour charger l'archive ad-hoc */
	protected  void chargerArchive() {
		EOArchive.loadArchiveNamed("GestionCongeMaternite",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
	}
	protected void prepareInterface() {
		super.prepareInterface();
		// Pour forcer le lock quand plusieurs fenêtres de congé maternité sont empilées et rendre visible celle qui était invisible
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("unlockSaisie", new Class[] { NSNotification.class }), ModelePage.DELOCKER_ECRAN, null);
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("annulationSelectionTypeConge", new Class[] { NSNotification.class }), DialogueSimple.CANCEL_NOTIFICATION, null);
	}

	public Integer enfantsDeclares() {

		try {

			return new Integer(currentConge().declarationMaternite().nbEnfantsDecl().intValue());
		}
		catch (Exception e) {
			return new Integer(0);
		}

	}		
	//		return new Integer(2);
	//		if (currentConge().dateDebut() == null || currentConge().dateFin() == null)
	//			return null;
	//		else
	//			return DateCtrl.calculerDureeEnMois(currentConge().dateDebut(), currentConge().dateFin(),true);

	//		return ;
	//		
	//	}


	protected boolean traitementsAvantValidation() {
		if (verifierDureeConges()) {	// Pour afficher des warnings éventuels
			NSArray congesLies = congesLies();	// tous les congés de type maternité ou pathologie accouchement qui ont les mêmes dates d'arrêté d'annulation
			if (super.traitementsAvantValidation()) { 		// on le fait avant pour que les données des arrêtés soient récupérées
				if (currentConge().congeDeRemplacement() == null && currentConge().estTypeMaternite()) {
					modifierArreteCongesLies(congesLies);	// pour modifier les dates et numéro d'annulation
				}



				return true;
			}
		}
		return false;
	}
	protected boolean traitementsAvantSuppression() {
		if (declarationMaternite() != null && declarationMaternite().congesMaternite().count() == 1) {
			declarationMaternite().supprimerRelations();
			editingContext().deleteObject(declarationMaternite());
		}
		return true;
	}
	protected void traitementsApresValidation() {

		// Si l'agent est à temps partiel pendant cette période, afficher un message pour indiquer que l'agent est à temps complet
		// pendant la durée du temps partiel
		NSArray tempsPartiels = EOTempsPartiel.rechercherTempsPartielPourIndividuEtPeriode(editingContext(), currentConge().individu(), currentConge().dateDebut(), currentConge().dateFin());
		if (tempsPartiels.count() > 0) {
			EODialogs.runInformationDialog("Information","La personne est à temps partiel mais elle sera considérée comme à temps complet pendant son congé de maternité");
		}
		// Pour fermer toutes les fenêtres si un ajout de congé a été fait
		NSNotificationCenter.defaultCenter().postNotification(GestionCongeAvecArreteAnnulation.TERMINER_AFFICHAGE,null);
	}
	// Gestion des arrêtés
	protected boolean annulationAbsenceGeree() {
		return true;
	}
	protected String nomTableCongeContractuel() {
		return "CONGE_MATERNITE";
	}
	protected String nomTableCongeFonctionnaire() {
		return "CONGE_MATERNITE";
	}
	/** on ne peut annuler un arrete de conge maternite que si il n'est pas remplace
	 * ou qu'il n'existe pas pour cette declaration de conge qui le remplace
	 */
	protected boolean peutModifierArreteAnnulation() {
		if (super.peutModifierArreteAnnulation()) {
			// l'arrêté d'annulation n'est modifiable que si il ne concerne pas une famille de congés de maternité 
			// et qu'il n'y a pas un nouveau congé principal ou qu'il n'y a pas un congé valide de même type qui le remplace
			boolean estModifiable = true;
			if (currentConge().estAnnule()) {
				if (congesLies().count() > 0) {
					if (declarationMaternite() != null && declarationMaternite().congePrincipal() != null) {
						estModifiable = false;
					}
				} else {
					if (declarationMaternite() != null) {
						for (java.util.Enumeration<EOCongeMaternite> e = declarationMaternite().congesValides().objectEnumerator();e.hasMoreElements();) {
							EOCongeMaternite conge = e.nextElement();
							if (currentConge().toTypeCgMatern() == conge.toTypeCgMatern()) {
								estModifiable = false;
								break;
							}
						}
					}
				}
			}
			return estModifiable;
		} else {
			return false;
		}
	} 
	protected void preparerPourRemplacement() {
		EOCongeMaternite congeRemplace = (EOCongeMaternite)congeRemplace();
		// la déclaration maternité est partagée
		initialiserAvecConge(congeRemplace,true);
		preparerDates();
		// annuler les autres congés de type pathologie accouchement s'il s'agit d'un congé de maternité
		if (congeRemplace.estTypeMaternite()) {
			for (java.util.Enumeration<EOCongeMaternite> e = declarationMaternite().congesValides().objectEnumerator();e.hasMoreElements();) {
				EOCongeMaternite autreConge = e.nextElement();
				if (autreConge.estTypePathologieGrossesse() == false && autreConge.estAnnule() == false &&
						autreConge != currentConge() && autreConge != congeRemplace) {
					autreConge.setCongeDeRemplacementRelationship(currentConge());
					autreConge.absence().setEstValide(false);
				}	
			}

		}
	}
	protected void initialiserAvecConge(EOCongeMaternite conge,boolean associerTypeConge) {
		currentConge().setEstCongeSansTraitement(conge.estCongeSansTraitement());
		if (declarationMaternite() != null) {
			// elle a été ajoutée au moment de l'affichage de la fenêtre
			EODeclarationMaternite declaration = currentConge().declarationMaternite();
			currentConge().setDeclarationMaterniteRelationship(null);
			editingContext().deleteObject(declaration);
		}
		if (conge.declarationMaternite() != null) {
			currentConge().setDeclarationMaterniteRelationship(conge.declarationMaternite());
		}
		if (associerTypeConge) {
			currentConge().setToTypeCgMaternRelationship(conge.toTypeCgMatern());
			Font font = labelDateNaissancePrev.getFont();
			labelDateNaissancePrev.setFont(new Font(font.getName(),Font.BOLD,font.getSize()));
		}
		updateDisplayGroups();
	}
	protected void modifierCongeRemplace() {
		super.modifierCongeRemplace();
		EOCongeMaternite congeRemplace = (EOCongeMaternite)congeRemplace();
		// modifier les autres congés liés
		if (declarationMaternite() != null) {
			for (java.util.Enumeration<EOCongeMaternite> e = declarationMaternite().congesMaternite().objectEnumerator();e.hasMoreElements();) {
				EOCongeMaternite autreConge = e.nextElement();
				if (autreConge != congeRemplace && autreConge.congeDeRemplacement() == currentConge()) {
					modifierAvecArreteAnnulation(autreConge,currentConge().noArrete(),currentConge().dateArrete());
				}
			}
		}
	} 
	protected void modifierReliquatConge(int nbJours) {
		reliquatConge = nbJours;
	}
	//	 méthodes privées
	private EOCongeMaternite currentConge() {
		return (EOCongeMaternite)displayGroup().selectedObject();

	}
	private EODeclarationMaternite declarationMaternite() {
		if (currentConge() == null) {
			return null;
		} else {
			return currentConge().declarationMaternite();
		}
	}
	private boolean estRemplacementConge() {
		return congeRemplace() != null;
	}
	private void creerDeclarationMaternite() {
		EODeclarationMaternite declarationMaternite = new EODeclarationMaternite();
		declarationMaternite.initAvecIndividu(currentConge().individu());
		editingContext().insertObject(declarationMaternite);
		currentConge().setDeclarationMaterniteRelationship(declarationMaternite);
	}
	private boolean peutSaisirCongeMaternite() {
		return declarationMaternite() != null && declarationMaternite().estDeclarationAnnulee() == false && !currentConge().estAnnule();
	}
	private void modifierValiditeConges(boolean estDeclarationAnnulee) {
		if (declarationMaternite() != null) {
			for (java.util.Enumeration<EOCongeMaternite> e = declarationMaternite().congesValides().objectEnumerator();e.hasMoreElements();) {
				EOCongeMaternite conge = e.nextElement();
				modifierValiditeAbsencePourConge(conge,!estDeclarationAnnulee);
			}
		}
	}
	// peut être invoquée dans 2 cas de figure : fractionnement d'un congé de type maternité
	// ajout d'un congé partageant la même déclaration
	private void ajouterConge(boolean estFractionnement) {
		GestionCongeMaternite controleur = new GestionCongeMaternite(false);	// ne pas créer de déclaration de maternité
		controleur.init(editingContext(),null);	// pas de remplacement
		controleur.ajouter(editingContext().globalIDForObject(currentConge().individu()));
		window().setVisible(false);	// masquer la fenêtre car problème lors de l'empilement de dialogues modaux
		windowHidden = true;
		if (estFractionnement) {
			controleur.modifierReliquatConge(DateCtrl.nbJoursEntre(currentConge().dateFin(),previousDateFin,false));
			// on ajoute un congé de type maternité suite à un fractionnement
			// appeler le traitement avant validation pour cette pour que lors de la sauvegarde globale de l'editing context, toutes les infos soient justes
			traitementsAvantValidation();
		}
		// à la sortie de cette méthode le nouveau congé est initialisé
		controleur.initialiserAvecConge(currentConge(),estFractionnement);
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("terminer",new Class[] {NSNotification.class}),GestionCongeAvecArreteAnnulation.TERMINER_AFFICHAGE,null);
	}


	private void modifierArreteCongesLies(NSArray conges) {
		for (java.util.Enumeration<EOCongeMaternite> e = conges.objectEnumerator();e.hasMoreElements();) {
			EOCongeMaternite conge = e.nextElement();
			modifierAvecArreteAnnulation(conge,currentConge().noArreteAnnulation(),currentConge().dAnnulation());
		}
	}
	private NSArray congesLies() {
		// les congés liés sont les congés de type maternité ou pathologie accouchement qui ne sont pas des congés remplacés
		NSMutableArray conges = new NSMutableArray();
		if (declarationMaternite() != null) {
			for (java.util.Enumeration<EOCongeMaternite> e = declarationMaternite().congesMaternite().objectEnumerator();e.hasMoreElements();) {
				EOCongeMaternite conge = e.nextElement();
				if (conge != currentConge() && conge.congeDeRemplacement() != currentConge() && (conge.estTypeMaternite() || conge.estTypePathologieAccouchement())) {
					if (memesDatesArreteAnnulation(conge,currentConge().dAnnulation()) && memesNumeroArreteAnnulation(conge,currentConge().noArreteAnnulation())) {
						if (currentConge().estTypeMaternite()) {
							conges.addObject(conge);
						} /*else {
	 						// pour les congés pathologiques, on ne prend en compte le congé que lorsque dateAnnulation ou noAnnulation ne sont pas nuls
	 						if (currentConge().dAnnulation() != null || currentConge().noArreteAnnulation() != null) {
	 							conges.addObject(conge);
	 						}
	 					}*/
					}
				}
			}
		}
		return conges;	
	}
	// return true si on ne peut plus prendre ce type de congé
	private boolean typeCongeEpuise(EOCongeMaternite conge) {
		boolean typeCongeEpuise = conge.estTypeMaternite();
		if (typeCongeEpuise == false) {
			// congé pathologique pendant la grossesse ou après l'accouchement, ils sont fractionnables
			typeCongeEpuise =  conge.dureeTotaleCongeMemeType(true) >= conge.dureeLegale();
		}
		return typeCongeEpuise;
	}

	// prépare les dates en cas de congé de type Maternité
	private void preparerDates() {
		if (currentConge().individu().estHomme()) {
			// pour un homme, le congé de maternité débute le jour de l'accouchement
			if (declarationMaternite() != null && declarationMaternite().dAccouchement() != null) {
				currentConge().setDateDebut(declarationMaternite().dAccouchement());
			} else {
				currentConge().setDateDebut(null);
			}
			preparerDateFin();
		} else {
			// préparation de la date de début que si il s'agit du congé de maternité principal
			// (i.e qu'il n'y a pas d'autre congé de maternité défini suite à fractionnement)
			// et que la grossesse n'est pas interrompue
			
			
			if (declarationMaternite().dAccouchement() != null 
					&& DateCtrl.isBefore(declarationMaternite().dAccouchement(), declarationMaternite().dNaisPrev()) ) {

				//currentConge().setDateDebut(declarationMaternite().dAccouchement());
				preparerDateFin();
				
			}
			else {
			if (currentConge().estTypeMaternite() && declarationMaternite() != null && 
					!declarationMaternite().estGrossesseInterrompue() &&
					(estRemplacementConge() || !declarationMaternite().estCongeFractionne())) {
				if (declarationMaternite().dNaisPrev() != null) {

					int nbEnfants = 0;

					if (declarationMaternite().nbEnfantsDecl() != null)
						nbEnfants = declarationMaternite().nbEnfantsDecl().intValue();

					int dureePrenatale = EOCongeMaternite.NB_JOURS_NORMAL_PRENATAL;					// 6 semaines
					if (declarationMaternite().estGrossesseGemellaire()) {
						dureePrenatale = EOCongeMaternite.NB_JOURS_JUMEAUX_PRENATAL;				// 12 semaines
					} else if (declarationMaternite().estGrossesseTriple()) {
						dureePrenatale = EOCongeMaternite.NB_JOURS_TRIPLES_PRENATAL;				// 24 semaines
					} else if (nbEnfants >= 2) {
						dureePrenatale = EOCongeMaternite.NB_JOURS_2ENFANTS_OU_PLUS_PRENATAL;		// 8 semaines
					}
					currentConge().setDateDebut(DateCtrl.dateAvecAjoutJours(declarationMaternite().dNaisPrev(),-dureePrenatale));
					
				} else {
					currentConge().setDateDebut(null);
				}
				preparerDateFin();
			}
			}
		}
		preparerPourCongeSansTraitement();

	}

	private void preparerDateFin() {

		int nbEnfantsACharge = 0;

		if ( declarationMaternite().nbEnfantsDecl() != null)
			nbEnfantsACharge = (declarationMaternite().nbEnfantsDecl()).intValue();
		else
			nbEnfantsACharge = 0; 

		if (currentConge().dateDebut() == null) {
			currentConge().setDateFin(null);
		} else {	 
			if (currentConge().toTypeCgMatern() != null) {
				int nbJoursAAjouter = 0;
				if (currentConge().estTypePathologieGrossesse() || currentConge().estTypePathologieAccouchement()) {
					nbJoursAAjouter = currentConge().dureeLegale() - currentConge().dureeTotaleCongeMemeType(false);
				} else {
					if (reliquatConge > 0) {	// congé de maternité fractionné
						nbJoursAAjouter = reliquatConge;
					} else {
						if (declarationMaternite() != null && !declarationMaternite().estGrossesseInterrompue()) {
							if (declarationMaternite().estGrossesseGemellaire()) {
								if (currentConge().individu().estHomme()) {
									nbJoursAAjouter = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;// 22 semaines
								} else {
									nbJoursAAjouter = EOCongeMaternite.NB_JOURS_JUMEAUX_FEMME;				// 34 semaines
								}	
							} else if (declarationMaternite().estGrossesseTriple()) {
								if (currentConge().individu().estHomme()) {
									nbJoursAAjouter = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;	// 22 semaines
								} else {
									nbJoursAAjouter = EOCongeMaternite.NB_JOURS_TRIPLES_FEMME;					// 46 semaines
								}
							} else if ( nbEnfantsACharge >= 2) {
								if (currentConge().individu().estHomme()) {
									nbJoursAAjouter = EOCongeMaternite.NB_JOURS_2ENFANTS_OU_PLUS_POSTNATAL;	// 18 semaines
								} else {
									nbJoursAAjouter = EOCongeMaternite.NB_JOURS_2ENFANTS_OU_PLUS_FEMME;		// 26 semaines
								}
							} else {
								if (currentConge().individu().estHomme()) {
									nbJoursAAjouter = EOCongeMaternite.NB_JOURS_NORMAL_POSTNATAL;			// 10 semaines
								} else {
									nbJoursAAjouter = EOCongeMaternite.NB_JOURS_NORMAL_FEMME;				// 17 semaines
								}
							}
						}
					}
				}
				if (nbJoursAAjouter > 0) {
					currentConge().setDateFin(DateCtrl.dateAvecAjoutJours(currentConge().dateDebut(),nbJoursAAjouter-1));
					previousDateFin = currentConge().dateFin();
				}
			}
		}
		changerAffichageArrete();
	}
	private boolean verifierDureeConges() {
		String message = "";
		NSArray congesTypeMaternite = declarationMaternite().congesValidesDeTypeMaternite();
		int nbJoursPrenatal = currentConge().calculerDuree(congesTypeMaternite,true);
		int nbJoursPostnatal = currentConge().calculerDuree(congesTypeMaternite,false);

		LogManager.logDetail("CongeMaternite - verifierDureeConges, Durée prénatale : " + nbJoursPrenatal);
		LogManager.logDetail("CongeMaternite - verifierDureeConges, Durée postnatale : " + nbJoursPostnatal);

		if ((declarationMaternite().nbEnfantsDecl() == null 
				|| declarationMaternite().nbEnfantsDecl().intValue() <= 1) 
				&& declarationMaternite().estGrossesseGemellaire() == false && declarationMaternite().estGrossesseTriple() == false) {

			int joursPeriodePrenatale = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CMAT_PERIODE_PRENATALE);

			if (nbJoursPrenatal < (joursPeriodePrenatale * 7)) {	// minimum 2 semaines avant accouchement
				message = "La période prénatale devrait débuter au minimum 2 semaines avant la date présumée de l'accouchement";
			}
			int joursPeriodePostnatale = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CMAT_PERIODE_POSTNATALE);

			if (nbJoursPostnatal < (joursPeriodePostnatale * 7)) {	// minimum 6 semaines après accouchement
				if (message.length() > 0) {
					message += ", ";
				}
				message += "la période postnatale devrait durer au moins 6 semaines";
			}
		} else {
			int dureePrenatale = 56;														// 8 semaines
			int dureePostnatale = EOCongeMaternite.NB_JOURS_2ENFANTS_OU_PLUS_POSTNATAL;		// 18 semaines
			int transfertSemaine = 14;														// 2 semaines
			String message1  = "A partir du 3ème enfant ";
			if (declarationMaternite().estGrossesseGemellaire()) {
				dureePrenatale = 84;														// 12 semaines
				dureePostnatale = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;	// 22 semaines
				transfertSemaine = 28;														// 4 semaines
				message1 = "Pour des jumeaux ";
			} else if (declarationMaternite().estGrossesseTriple()) {
				dureePrenatale = 168;														// 24 semaines
				dureePostnatale = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;	// 22 semaines
				transfertSemaine = 28;														// 4 semaines
				message1 = "Pour des triplés ou plus ";
			}
			if (nbJoursPrenatal > dureePrenatale) {
				// 17/05/2010 - DT 2578 - affichage en semaines
				message = message1 + "la période prénatale devrait durer au plus " + (dureePrenatale / 7) + " semaines";
			}
			if (nbJoursPostnatal <= dureePostnatale) {
				if (nbJoursPostnatal < (dureePostnatale - transfertSemaine)) {
					if (message.length() == 0) {
						message = message1;
					} else {
						message += ", ";
					}
					message += "la période postnatale devrait durer au moins " + (dureePostnatale - transfertSemaine)/7 + " semaines";
				} else if (nbJoursPostnatal < dureePostnatale) {
					int transfertReel = dureePostnatale - nbJoursPostnatal;
					LogManager.logDetail("CongeMaternite - verifierDureeConges, transfert réel : " + transfertReel);
					LogManager.logDetail("CongeMaternite - verifierDureeConges somme durees : " + (dureePrenatale + transfertReel));
					if (nbJoursPrenatal != (dureePrenatale + transfertReel)) {
						if (message.length() == 0) {
							message = message1;
						} else {
							message += ", ";
						}
						message += "vous devriez transférer au plus " + transfertSemaine / 7 +" semaines pendant la période prénatale";
					} 
				}
			} else {
				// nbJoursPostnatal > dureePostnatale
				if (message.length() == 0) {
					message = message1;
				} else {
					message += ", ";
				}
				message += "la période postnatale devrait durer au plus " + dureePostnatale/7 + " semaines";
			}
		}
		if (message.length() > 0) {
			message = message + "\nVoulez-vous continuer ?";
			return EODialogs.runConfirmOperationDialog("ATTENTION", message,"Oui","Non");
		} else {
			return true;
		}
	}
}

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.conges;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.gui.conges.CongeLongueMaladieView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOStage;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

/**
 * GESTION DES CONGES  DE GRAVE MALADIE
 * @author cpinsard
 *
 */
public class CongeLongueMaladieCtrl extends ModelePageSaisie {

	private static CongeLongueMaladieCtrl sharedInstance;

	private CongeLongueMaladieView myView;
	private EOClm currentConge;

	/**
	 * 
	 * @param globalEc
	 */
	public CongeLongueMaladieCtrl (EOEditingContext edc) {

		super(edc);
		myView = new CongeLongueMaladieView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnImprimerArrete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerArrete();}}
				);

		setDateListeners(myView.getTfDebut());
		setDateListeners(myView.getTfFin());
		setDateListeners(myView.getTfDateArrete());
		setDateListeners(myView.getTfDateComite());
		setDateListeners(myView.getTfDateComiteSuperieur());
		setDateListeners(myView.getTfDateArreteAnnulation());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CongeLongueMaladieCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CongeLongueMaladieCtrl(editingContext);
		return sharedInstance;
	}			





	public EOClm getCurrentConge() {
		return currentConge;
	}

	public void setCurrentConge(EOClm currentConge) {
		this.currentConge = currentConge;
		updateDatas();
	}

	/**
	 * 
	 * @param conge
	 */
	public void modifier(EOClm conge)	{
		setCurrentConge(conge);
		myView.setVisible(true);
	}
	public void supprimer(EOClm conge) {
		try {
			conge.absence().setEstValide(false);
			conge.setEstValide(false);
			getEdc().saveChanges();
		}
		catch (Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void imprimerArrete() {

		NSArray<EODestinataire> destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
				NSMutableArray<EOGlobalID> destinatairesGlobalIds = new NSMutableArray<EOGlobalID>();
				for (EODestinataire destinataire : destinatairesArrete) {
					destinatairesGlobalIds.addObject(getEdc().globalIDForObject(destinataire));
				}
				Class[] classeParametres =  new Class[] {EOGlobalID.class, NSArray.class, Boolean.class};
				Object[] parametres = new Object[]{getEdc().globalIDForObject(getCurrentConge()), destinatairesGlobalIds, false};
				UtilitairesImpression.imprimerSansDialogue(getEdc(),"clientSideRequestImprimerArreteConge", classeParametres, parametres, "ArreteClm" + getCurrentConge().individu().noIndividu() ,"Impression de l'arrêté de maternité");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ActionListenerTemSigne implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{

			if (EODialogs.runConfirmOperationDialog("Attention","Confirmez-vous la validation (signature) de ce congé ?\n" +
					"Les données ne seront ensuite plus modifiables.","Oui","Non")) {

				//currentConge.setTemConfirme("O");
				updateInterface();
			}
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDebut());
		CocktailUtilities.viderTextField(myView.getTfFin());
		CocktailUtilities.viderTextField(myView.getTfDateArrete());
		CocktailUtilities.viderTextField(myView.getTfDateComite());
		CocktailUtilities.viderTextField(myView.getTfDateComiteSuperieur());
		CocktailUtilities.viderTextField(myView.getTfNoArrete());
		CocktailUtilities.viderTextArea(myView.getTfCommentaires());

		myView.getCheckProlongation().setSelected(false);
		myView.getCheckRemisEnCause().setSelected(false);
		myView.getCheckRequalifie().setSelected(false);

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentConge() != null) {

			CocktailUtilities.setDateToField(myView.getTfDebut(), getCurrentConge().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfFin(), getCurrentConge().dateFin());
			CocktailUtilities.setDateToField(myView.getTfDateComite(), getCurrentConge().dComMedClm());
			CocktailUtilities.setDateToField(myView.getTfDateComiteSuperieur(), getCurrentConge().dComMedClmSup());

			CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentConge().dateArrete());
			CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentConge().noArrete());
			CocktailUtilities.setDateToField(myView.getTfDateArreteAnnulation(), getCurrentConge().dAnnulation());
			CocktailUtilities.setTextToField(myView.getTfNoArreteAnnulation(), getCurrentConge().noArreteAnnulation());

			CocktailUtilities.setTextToArea(myView.getTfCommentaires(), getCurrentConge().commentaire());

			myView.getCheckProlongation().setSelected(getCurrentConge().estProlonge());
			myView.getCheckRequalifie().setSelected(getCurrentConge().estRequalifie());
			myView.getCheckRemisEnCause().setSelected(getCurrentConge().estRemisEnCause());

		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnImprimerArrete().setEnabled(!isModeCreation());
	}

	@Override
	protected void traitementsAvantValidation() {

		// TODO Auto-generated method stub

		if (CocktailUtilities.getDateFromField(myView.getTfDebut()) != null 
				&& CocktailUtilities.getDateFromField(myView.getTfFin()) != null) {
			
			// Controle des periodes de stage
			NSArray<EOStage> stages = EOStage.findForIndividuEtPeriode(getEdc(), getCurrentConge().individu(), getCurrentConge().dateDebut(), getCurrentConge().dateFin(),false);
			if (stages != null && stages.size() > 0) {
				EODialogs.runInformationDialog("Attention","L'agent est en stage pendant cette période, pensez à modifier la date de la fin de stage !");	        		
			}
			
			if (EOChangementPosition.fonctionnaireEnActivitePendantPeriodeComplete(getEdc(), getCurrentConge().individu(),getCurrentConge().dateDebut(), getCurrentConge().dateFin()) == false) {
				throw new NSValidation.ValidationException("Le congé de longue maladie ne s'applique qu'aux fonctionnaires en activité pendant toute la période.\nVeuillez saisir de nouvelles dates.");
			}

			// Validation de la duree
			int nbMois = DateCtrl.calculerDureeEnMois(getCurrentConge().dateDebut(), getCurrentConge().dateFin(),true).intValue();
			Integer nbMoisMin = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_DUREE_NON_BLOQUANTE);

			if (nbMoisMin != null && (nbMois < nbMoisMin.intValue()) ) {
				EODialogs.runInformationDialog("Attention","La durée minimum d'un congé de longue maladie est de " + nbMoisMin + " mois");
			}
			nbMois = ((CongeAvecArreteAnnulation)getCurrentConge()).dureeEnMoisCongesContinusAssocies();
			int nbMoisMax = EOClm.nbMoisMaxCongesContinus(getEdc());
			if (nbMois > nbMoisMax) {
				throw new NSValidation.ValidationException("La durée maximale de congés de longue maladie continus est de " + nbMoisMax / 12 + " ans");
			}

		}

		// Enregistrement des donnees
		getCurrentConge().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		getCurrentConge().setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		getCurrentConge().setDComMedClm(CocktailUtilities.getDateFromField(myView.getTfDateComite()));
		getCurrentConge().setDComMedClmSup(CocktailUtilities.getDateFromField(myView.getTfDateComiteSuperieur()));

		getCurrentConge().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTfCommentaires()));

		getCurrentConge().setDateArrete(CocktailUtilities.getDateFromField(myView.getTfDateArrete()));
		getCurrentConge().setDAnnulation(CocktailUtilities.getDateFromField(myView.getTfDateArreteAnnulation()));
		getCurrentConge().setNoArrete(CocktailUtilities.getTextFromField(myView.getTfNoArrete()));
		getCurrentConge().setNoArreteAnnulation(CocktailUtilities.getTextFromField(myView.getTfNoArreteAnnulation()));

		getCurrentConge().setEstRemisEnCause(myView.getCheckRemisEnCause().isSelected());
		getCurrentConge().setEstRequalifie(myView.getCheckRequalifie().isSelected());
		getCurrentConge().setEstProlonge(myView.getCheckProlongation().isSelected());

		// CREATION DE L ABSENCE
		EOAbsences absence = getCurrentConge().absence() ;
		if (absence == null) {
			absence = EOAbsences.creer(getEdc(), getCurrentConge().individu(), 
					(EOTypeAbsence)NomenclatureFinder.findForCode(getEdc(), EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_CLM));
			getCurrentConge().setAbsenceRelationship(absence);
		}
		absence.setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		absence.setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		absence.setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(getCurrentConge().dateDebut(), getCurrentConge().dateFin(), true)));

	}


	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}



}

//GestionCongeMaladie.java
//Mangue

//Created by Christine Buttin on Mon Sep 12 2005.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.


/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecRemiseEnCause;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOParamAncienneteMaladie;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.conges.CongeMaladie;
import org.cocktail.mangue.modele.mangue.conges.EOCgntMaladie;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaladie;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSValidation.ValidationException;

/** Gestion des conges de maladie pour titulaires et non-titulaires<BR>
 * La date de debut, la date de fin doivent etre saisies ainsi que la date d'anciennete pour les non-titulaires<BR>
 * La meme interface est appliquee dans les deux cas mais lors de la saisie de la date debut, on s'assure que ce type de conge
 * est valide pour l'agent. Si ce n'est pas le cas, un message d'erreur est affiche a l'utilisateur et la date remise a zero.<BR>
 * Pour les fonctionnaires, on verifie que la duree totale de conges maladie consecutifs est inferieure a un an et
 * qu'au dela de 6 mois, la date d'avis du comite medical est saisie. A partir de 4 mois consecutifs, on affiche un Warning<BR>
 */

public class GestionCongeMaladie extends GestionCongeAvecRemiseEnCause {
	private static final String DEBUT_JOUR_CARENCE = "01/01/2012";
	private static final String FIN_JOUR_CARENCE = "31/12/2013";
	public EOView vueContractuel,vueFonctionnaire,vueSecondaire;

	public GestionCongeMaladie(String nomEntite) {
		super(nomEntite,"Gestion des congés de maladie");
	}
	// accesseurs
	public String anciennete() {
		if (currentConge() == null || currentConge() instanceof EOCongeMaladie) {
			return null;
		} else {
			if (((EOCgntMaladie)currentConge()).anciennete() == null) {
				return null;
			} else {
				return ((EOCgntMaladie)currentConge()).anciennete().lAnciennete();
			}
		}
	}
	public boolean estContratContinu() {
		if (currentConge() == null || currentConge() instanceof EOCongeMaladie) {
			return false;
		} else {
			return ((EOCgntMaladie)currentConge()).estContratContinu();
		}
	}
	public void setEstContratContinu(boolean aBool) {
		if (currentConge() != null && currentConge() instanceof EOCgntMaladie) {
			((EOCgntMaladie)currentConge()).setEstContratContinu(aBool);
		}
	}
	public boolean estAccidentService() {
		if (currentConge() == null || currentConge() instanceof EOCgntMaladie) {
			return false;
		} else {
			return ((EOCongeMaladie)currentConge()).estAccidentService();
		}
	}
	public void setEstAccidentService(boolean aBool) {
		if (currentConge() != null && currentConge() instanceof EOCongeMaladie) {
			((EOCongeMaladie)currentConge()).setEstAccidentService(aBool);
		}
	}

	public void avisProlongationHasChanged() {

		if (currentConge().estProlonge() )
			currentConge().setEstJourDeCarence(false);

	}

	public String dateAvisComMedical() {
		if (currentConge() == null || currentConge() instanceof EOCgntMaladie) {
			return null;
		} else {
			return ((EOCongeMaladie)currentConge()).dateComMedFormatee();
		}
	}
	public void setDateAvisComMedical(String aStr) {
		if (currentConge() != null && currentConge() instanceof EOCongeMaladie) {
			((EOCongeMaladie)currentConge()).setDateComMedFormatee(aStr);	
		}
	}
	// méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {

		if (key.equals("dateDebutFormatee") || key.equals("dateFinFormatee")) {
			validerEntitePourDates();

			setAnciennete();
			
			super.displayGroupDidSetValueForObject(group,value,eo,key);
		}
		if (key.equals("estProlonge")) {
			currentConge().setEstJourDeCarence(currentConge().estProlonge() == false);
		}

		updateDisplayGroups();
	}

	// actions
	public void afficherAnciennete() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("dureePleinTrait", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("dureePleinTrait", EOSortOrdering.CompareAscending));
		afficherDialogue("ParamCgntMal","getAnciennete",null,false,sorts);
	}
	// Notifications
	public void getAnciennete(NSNotification aNotif) {
		ajouterRelation(currentConge(),(EOGlobalID)aNotif.object(),"anciennete");
		updateDisplayGroups();
	}
	
	// méthodes du controller DG
	public boolean peutModifierJourCarence() {		
		
		return peutModifierConge() && !currentConge().estProlonge() && currentConge().dateDebut() != null
			&& DateCtrl.isBetween(currentConge().dateDebut(), DateCtrl.stringToDate(DEBUT_JOUR_CARENCE), DateCtrl.stringToDate(FIN_JOUR_CARENCE));
		
	}

	public boolean peutModifier() {
		if (super.peutModifier()) {
			return currentConge().estRequalifie() == false;
		} else {
			return false;
		}
	}
	public boolean peutValider() {
		if (super.peutValider()) {
			if (currentConge() instanceof EOCgntMaladie) {
				return ((EOCgntMaladie)currentConge()).anciennete() != null;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	// autres
	public void afficherFenetre() {
		if (currentConge() instanceof EOCongeMaladie) {
			GraphicUtilities.swaperView(vueSecondaire,vueFonctionnaire);
		} else {
			GraphicUtilities.swaperView(vueSecondaire,vueContractuel);
		}
		super.afficherFenetre();
	}
	// méthodes protégées
	/** methode surchargee par les sous-classes pour charger l'archive ad-hoc */
	protected  void chargerArchive() {
		EOArchive.loadArchiveNamed("GestionCongeMaladie",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
		GraphicUtilities.preparerInterface(new NSArray(vueContractuel.getComponents()));
	}
	protected void prepareInterface() {
		super.prepareInterface();
	}
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) { 		// on le fait avant pour que les données de l'arrêté soit récupérée
			if (currentConge() instanceof EOCongeMaladie && currentConge().estAnnule() == false) {

				NSDictionary dicoConges = ((EOCongeMaladie)currentConge()).dureeEnMoisCongesMaladieAssocies();
				int nbMois = ((Integer)dicoConges.objectForKey("nbMois")).intValue();
				int nbJours = ((Integer)dicoConges.objectForKey("nbJours")).intValue();

				if (	nbMois > EOCongeMaladie.NB_MOIS_CONSECUTIFS_MAX
						|| ( nbMois == EOCongeMaladie.NB_MOIS_CONSECUTIFS_MAX && nbJours > 0)) {
					EODialogs.runErrorDialog("Erreur","La durée maximale de congés ordinaires de maladie continus est de " + EOCongeMaladie.NB_MOIS_CONSECUTIFS_MAX +" mois (Actuelle : " + nbMois + " Mois " + nbJours + " Jours )");
					return false;
				} else if (((EOCongeMaladie)currentConge()).dComMedCom() == null) {
					if (((EOCongeMaladie)currentConge()).verifierAvisComitePourDelaiSuperieur6Mois()) {
						// Il existe un des congés associés avec une date de comité
						return true;
					} else if (nbMois >= EOCongeMaladie.NB_MOIS_CONSECUTIFS_AVEC_AVIS) {
						EODialogs.runErrorDialog("Erreur"," Après " + EOCongeMaladie.NB_MOIS_CONSECUTIFS_AVEC_AVIS + " mois de congé ordinaire de maladie, le comité médical doit donner son avis sur une éventuelle prolongation.\nVeuillez fournir la date d'avis du comité médical");
						return false;
					} else if (nbMois >= EOCongeMaladie.NB_MOIS_CONSECUTIFS_AVEC_INFO) {
						EODialogs.runInformationDialog("Attention","Attention l'agent bénéficie de plus de " + EOCongeMaladie.NB_MOIS_CONSECUTIFS_AVEC_INFO + " mois de COM consécutifs.\nAu-delà de six mois de congés consécutifs l'avis du comité médical est nécessaire"); 
						return true;
					} else {
						return true;
					}
				} else {
					return true;
				}
			}
			return true;
		} else {
			return false;
		}
	}
	protected boolean traitementsAvantSuppression() {
		return true;
	}
	protected void traitementsApresSuppression() {
	}


	/**
	 * 
	 */
	protected void traitementsApresValidation() {
		
		try {
			// Calcul et enregistrement des jours comptables de l'absence
			int joursComptables = DateCtrl.nbJoursEntre(currentConge().dateDebut(), currentConge().dateFin(), true, true);
			currentConge().setNbJoursComptables(new Integer(joursComptables));
					
			editingContext().saveChanges();
		} catch (ValidationException e) {
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// Gestion des arrêtés
	protected EOTypeGestionArrete typeGestionArrete() {
		if (currentConge() != null && currentConge().individu() != null) {
			if (currentConge() instanceof EOCongeMaladie) {
				return currentConge().individu().rechercherTypeGestionArretePourPeriode(null,"CONGE_MALADIE",currentConge().dateDebut(),currentConge().dateFin());
			} else {
				return currentConge().individu().rechercherTypeGestionArretePourPeriode("CGNT_MALADIE",null,currentConge().dateDebut(),currentConge().dateFin());
			}
		} else {
			return null;
		}
	}
	protected void preparerPourRemplacement() {}
	protected boolean supporteSignature() {
		return true;
	}

	//	 méthodes privées
	private CongeMaladie currentConge() {
		return (CongeMaladie)displayGroup().selectedObject();

	}
	// Vérifie si il est possible de créer ce type de congé à cette date
	private void validerEntitePourDates() {
		if (currentConge().dateDebut() == null) {
			currentConge().setDateFin(null);
			return;
		}
		
		// Gestion du jour de carence
		currentConge().setTemJourCarence(CocktailConstantes.FAUX);
			
		if (currentConge() instanceof EOCongeMaladie) {
			boolean estFonctionnaire;
			// si la date de fin est nulle, on vérifie au jour de la date de début
			if (currentConge().dateFin() == null) {
				estFonctionnaire = EOChangementPosition.fonctionnaireEnActivitePendantPeriodeComplete(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateDebut());
			} else {
				estFonctionnaire = EOChangementPosition.fonctionnaireEnActivitePendantPeriodeComplete(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin());
			}
			if (!estFonctionnaire) {
				EODialogs.runInformationDialog("Attention", "Le congé de maladie ordinaire ne s'applique qu'aux fonctionnaires en activité pendant toute la période.\nVeuillez saisir de nouvelles dates.");
				currentConge().setDateDebut(null);
				currentConge().setDateFin(null);
			}
		} else {
			boolean estContractuel;
			// si la date de fin est nulle, on vérifie au jour de la date de début
			if (currentConge().dateFin() == null) {
				estContractuel = currentConge().individu().estContractuelSurPeriodeComplete(currentConge().dateDebut(),currentConge().dateDebut());
			} else {
				estContractuel = currentConge().individu().estContractuelSurPeriodeComplete(currentConge().dateDebut(),currentConge().dateFin());
			} 
			if (!estContractuel)  {
				EODialogs.runInformationDialog("Attention", "Le congé de maladie non titulaire ne s'applique qu'aux contractuels ou considérés contractuels en activité pendant toute la période.\nVeuillez saisir de nouvelles dates.");
				currentConge().setDateDebut(null);
				currentConge().setDateFin(null);
			}
		}
	}
}

/*
 * Created on 14 sept. 2005
 *
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.impression.GestionImpression;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecDemiJournee;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartEnfant;
import org.cocktail.mangue.modele.mangue.conges.EOCongeGardeEnfant;
import org.cocktail.mangue.modele.mangue.conges.EODroitsGardeEnfant;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

public class GestionCongeGardeEnfant extends GestionCongeAvecDemiJournee {
	
	private DestinatairesSelectCtrl ctrlDestinataires;

	public GestionCongeGardeEnfant() {
		super("CongeGardeEnfant","Gestion des congés de garde enfant");
	}
	public void connectionWasBroken() {
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}
	// actions
	public void afficherEnfant() {
		NSMutableArray args = new NSMutableArray(currentConge().individu());
		String stringQualifier = EORepartEnfant.PARENT_KEY + " = %@";
		stringQualifier = stringQualifier + " AND enfant.temValide = 'O'";

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		UtilitairesDialogue.afficherDialogue(this,EORepartEnfant.ENTITY_NAME,"getEnfant",false,qualifier,true);
	}
	public void imprimer() {
		imprimer(true);
	}
	public void imprimerArrete() {
		//	 On veut recevoir les notifications de fin d'impression pour pouvoir terminer
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("terminer",new Class [] {NSNotification.class}),GestionImpression.FIN_IMPRESSION,null);
		imprimer(false);	
	}
	//	 Notifications
	public void terminer(NSNotification aNotif) {
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}
	public void getEnfant(NSNotification aNotif) {
		if (aNotif.object() != null) {
			EORepartEnfant repart = (EORepartEnfant)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(), editingContext());
			currentConge().addObjectToBothSidesOfRelationshipWithKey(repart.enfant(), "enfant");
		}
	}


	// méthodes du controller DG
	public boolean peutImprimer() {
		return currentConge() != null && typeTraitement() == MODE_MODIFICATION ;
	}
	public boolean peutValider() {
		return super.peutValider();
	}
	// méthodes protegees
	/** methode surchargee par les sous-classes pour charger l'archive ad-hoc */
	protected  void chargerArchive() {
		EOArchive.loadArchiveNamed("GestionCongeGardeEnfant",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
	}
	/** verifie que le nombre legal de 1/2 journees n'est pas depasse */
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) {
			// vérifier les droits aux congés pour garde enfant
			int anneeDebut = DateCtrl.getYear(currentConge().dateDebut());
			int anneeFin = DateCtrl.getYear(currentConge().dateFin());

			currentConge().setCgAmPmDebut(currentConge().absence().absAmpmDebut());
			currentConge().setCgAmPmFin(currentConge().absence().absAmpmFin());

			if (anneeDebut == anneeFin) {
				return verifierDroitsPourAnnee(anneeDebut,new Float(currentConge().absence().absDureeTotale()).floatValue());
			} else {
				float nbJours = DateCtrl.nbJoursEntre(currentConge().dateDebut(),DateCtrl.finAnnee(anneeDebut),true);
				if (currentConge().absence().absAmpmDebut().equals("pm")) {
					nbJours -= 0.5;
				}
				if (verifierDroitsPourAnnee(anneeDebut,nbJours)) {
					nbJours = DateCtrl.nbJoursEntre(DateCtrl.debutAnnee(anneeFin),currentConge().dateFin(),true);
					if (currentConge().absence().absAmpmFin().equals("pm")) {
						nbJours += 0.5;
					}
					return verifierDroitsPourAnnee(anneeFin,nbJours);
				} else {
					return false;
				}
			}
		} else {
			return false;
		}


	}
	protected boolean traitementsAvantSuppression() {
		currentConge().setEnfantRelationship(null);
		return true;
	}
	protected void traitementsApresSuppression() {
	}
	protected void traitementsApresValidation() {			
	}
	protected EOTypeGestionArrete typeGestionArrete() {
		return null;
	}

	protected boolean supporteSignature() {
		return false;
	}

	protected EOCongeGardeEnfant currentConge() {
		return (EOCongeGardeEnfant)displayGroup().selectedObject();

	}

	/**
	 * 
	 * @param sauverDonnees
	 */
	protected void imprimer(boolean sauverDonnees) {
		if (!peutValider()) {
			EODialogs.runInformationDialog("Erreur", "Impossible d'imprimer l'arrêté.\nToutes les données nécessaires pour ce congé ne sont pas saisies");
			return;
		}
		if (sauverDonnees && !validerSansFermerFenetre()) {
			LogManager.logDetail("Impression de l'arrêté - Erreur lors de l'enregistrement des données");
			EODialogs.runInformationDialog("Erreur", "Impossible d'imprimer l'arrêté.\nProblème lors de l'enregistrement des données");
			return;
		}

		try {
			
			if (ctrlDestinataires == null)
				ctrlDestinataires = new DestinatairesSelectCtrl(editingContext());
			NSArray<EOGlobalID> destinatairesIds = ctrlDestinataires.getDestinatairesGlobalIds();

			if (destinatairesIds != null) {

				// Recuperer les destinataires (c'est un tableau de globalIDs de EODestinataires)
				// Lancer l'impression
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class};
				Object[] parametres = new Object[]{editingContext().globalIDForObject(currentConge()), destinatairesIds};
				try {
					// avec Thread
					UtilitairesImpression.imprimerAvecDialogue(editingContext(),"clientSideRequestImprimerCongeGardeEnfant",classeParametres,parametres,"CongeGarde_" +  currentConge().individu().noIndividu(),"Impression de l'arrêté");
				} catch (Exception e) {
					LogManager.logException(e);
					EODialogs.runErrorDialog("Erreur",e.getMessage());
				}

			}
		}
		catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}

	}
	
	/**
	 * 
	 * @param annee
	 * @param nbJoursConge
	 * @return
	 */
	private boolean verifierDroitsPourAnnee(int annee,float nbJoursConge) {
		
		EODroitsGardeEnfant droitPourAnnee = EODroitsGardeEnfant.rechercherDroitsGardePourIndividuEtAnnee(editingContext(),currentConge().individu(),new Integer(annee));
		if (droitPourAnnee == null) {
			EODialogs.runErrorDialog("Attention","Les Droits de garde ne sont pas définis pour l'année " + annee + "\nVeuillez les définir avant de saisir un congé");
			return false;
		}

		int nbJours = new Integer(currentConge().totalGardesPourAnnee()).intValue();
		//	if (nbJoursConge * 2 > droitPourAnnee.nbDemiJournees().floatValue()) {
		if (nbJours > droitPourAnnee.nbDemiJournees().intValue()) {
			EODialogs.runErrorDialog("Attention","Ce congé est trop long. Les droits de garde pour l'année " + annee + " sont épuisés.\nVeuillez réduire la durée du congé");
			return false;
		} else {
			return true;
		}
	}
}

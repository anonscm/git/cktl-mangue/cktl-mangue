/*
 * Created on 23 févr. 2006
 *
 * Gère les congés de type Hospitalo-Universitaire
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import java.awt.Font;

import javax.swing.JLabel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecRemiseEnCause;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.conges.CongeHospitaloUniversitaire;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/** Gere les conges de type Hospitalo-Universitaire<BR>
 * Le conge ne concerne que les chefs de clinique des universites assistant des hopitaux et les assistants hospitaliers
 * universitaires<BR>
 * La totalite du conge de maladie doit reposer sur un contrat.<BR>
 * Selon le type de conge, la duree legale du conge peut varier<BR>
 * La plupart des regles de validation sont definies dans les objets metiers<BR>
 * @author christine
 */
// 28/01/2011 - Adaptation Netbeans
public class GestionCongeHospitaloUniv extends GestionCongeAvecRemiseEnCause {
	public JLabel labelDateAvis;

	public GestionCongeHospitaloUniv(String nomEntite) {
		super(nomEntite,"Gestion des congés hospitalo-universitaire");
		String typeConge = nomEntite.substring(nomEntite.length() - 1);
		setDialogTitle("Gestion des congés hospitalo-universitaire art26-7 alinéa " + typeConge);
	}
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("dateDebutFormatee") || key.equals("dateFinFormatee")) {
			validerEntitePourDates(key.equals("dateDebutFormatee"));
			super.displayGroupDidSetValueForObject(group,value,eo,key);
		}
		updateDisplayGroups();
	}
	// méthodes du controller DG
	public boolean peutValider() {
		if (super.peutValider()) {
			if (currentConge().dateAvisObligatoire() && currentConge().dComMed() == null) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	/** on ne peut modifier la date de fin que si la duree legale du conge n'est pas imposee */
	public boolean peutModifierDateFin() {
		if (super.peutModifierDateFin()) {
			return (currentConge().dureeLegale() == 0);
		} else {
			return false;
		}
	}
	public void afficherFenetre() {
		if (currentConge() != null && currentConge().dateAvisObligatoire()) {
			Font font = labelDateAvis.getFont();
			labelDateAvis.setFont(new Font(font.getName(),Font.BOLD,font.getSize()));
		}
		super.afficherFenetre();
	}
	// methodes protegees
	/** methode surchargee par les sous-classes pour charger l'archive ad-hoc */
	protected  void chargerArchive() {
		EOArchive.loadArchiveNamed("GestionCongeHospitaloUniv",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
	}
	/*protected boolean traitementsAvantValidation() {
	   	if (super.traitementsAvantValidation()) {
	   		return true;
	   	} else {
	   		return false;
	   	}
	 }*/
	protected boolean traitementsAvantSuppression() {
		return true;
	}

	protected void traitementsApresSuppression() {}

	// Gestion des arrêtés
	protected boolean supporteSignature() {
		return true;
	}
	protected void preparerPourRemplacement() {
	}

	protected EOTypeGestionArrete typeGestionArrete() {
		if (currentConge() != null && currentConge().individu() != null) {
			String typeConge = entityName().substring(entityName().length() - 1);
			return currentConge().individu().rechercherTypeGestionArretePourPeriode("CONGE_AL" + typeConge,null,currentConge().dateDebut(),currentConge().dateFin());
		} else {
			return null;
		}
	}
	//	 méthodes privées
	private CongeHospitaloUniversitaire currentConge() {
		return (CongeHospitaloUniversitaire)displayGroup().selectedObject();

	}
	// Vérifie si il est possible de créer ce type de congé à cette date
	private void validerEntitePourDates(boolean estDateDebut) {
		if (currentConge().dateDebut() == null) {
			currentConge().setDateFin(null);
			return;
		}
		// vérifier que l'agent a bien un contrat de type hospitalo-universitaire
		NSArray contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin(),false);
		boolean contratOK = false;
		if (contrats.count() == 0) {
			EODialogs.runErrorDialog("ERREUR","Pas de contrat défini à ces dates pour cet agent");
		} else if (currentConge().dateFin() != null) {
			if (currentConge().individu().aContratHospitalierSurPeriode(currentConge().dateDebut(),currentConge().dateFin()) == false) {
				EODialogs.runErrorDialog("ERREUR","Ce type de congé ne concerne que les personnels hospitalo-universitaires");
			} else if (currentConge().individu().estContractuelSurPeriodeComplete(currentConge().dateDebut(),currentConge().dateFin()) == false) {
				EODialogs.runErrorDialog("ERREUR","L'agent n'a pas de contrats continus sur cette période");
			} else {
				contratOK = true;
			}
		} else {
			contratOK = true;
		}
		if (!contratOK) {
			currentConge().setDateDebut(null);
			currentConge().setDateFin(null);
		} else if (estDateDebut) {
			// ajouter par défaut la durée maxi du congé si elle est fournie
			int duree =  currentConge().dureeMaxiConge();
			if (duree > 0) {
				currentConge().setDateFin(DateCtrl.ajouterDureeComptable(currentConge().dateDebut(),duree * DateCtrl.NB_JOURS_COMPTABLES_MENSUELS));
			} else {
				duree = currentConge().dureeLegale();
				// ajouterla durée légale du congé si elle est fournie
				if (duree > 0) {
					currentConge().setDateFin(DateCtrl.ajouterDureeComptable(currentConge().dateDebut(),duree * DateCtrl.NB_JOURS_COMPTABLES_MENSUELS));
				} else if (currentConge().dateFin() != null && DateCtrl.isBefore(currentConge().dateFin(),currentConge().dateDebut())) {
					currentConge().setDateFin(null);
				}
			}
		}
	}
}

/*
 * Created on 13 sept. 2005
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JLabel;
import javax.swing.JTextField;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.outils_interface.GestionCongeNaissance;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOLienFiliationEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartEnfant;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAdoption;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/** Gere les conges d'adoption<BR>
 * La date d'arrivee au foyer, la date debut et la date fin doivent &ecir;tre fournies<BR>
 * On ne peut saisir la date de debut que si la date d'arrivee au foyer est saisie et la date de fin que si celle de debut est saisie
 * Les dates ne sont gerees automatiquement que lorsque le conge n'est pas partage<BR>
 * Les conges d'adoption peuvent etre accordes aux femmes et aux hommes<BR>
 * Lors de la validation, on verifie le nombre d'enfants declares (via la date d'arrivee au foyer) dans la table Enfant.
 * Si on en trouve, on effectue des controles supplementaires, sinon on propose apres validation de les saisir<BR> 
 * Un conge d'adoption prolonge une duree de stage, si l'agent est en stage pendant cette periode.<BR>
 * Un conge d'adoption ne peut etre annulee que si un arrete a ete produit. Dans le cas ou
 * il n'y a pas d'arrete signe, on peut supprimer le conge<BR>
 * En cas d'accouchement apres la date presumee, la periode postnatale n'est pas reduite => la date de fin est repoussee du nombre de jours entre la date d'accouchement et la date presumee.<BR>
 * Les regles de validation sont dans la classe EOCongeMaternite<BR>
 * L'enfant doit etre fourni si le conge est posterieur au 01/01/2004
 * */

public class GestionCongeAdoption extends GestionCongeNaissance {
	private boolean enfantsADeclarer;
	public JTextField tfEnfantsACharge;
	private NSTimestamp dateLimitePourDeclarationEnfant = DateCtrl.stringToDate(EOCongeAdoption.DATE_LIMITE_POUR_DECLARATION_ENFANT);
	public JLabel labelEnfant;

	public GestionCongeAdoption() {
		super("CongeAdoption","Gestion des congés d'adoption");
	}
	// méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("dateArriveeFormatee") || key.equals("nbEnfantACharge")  || key.equals("estAdoptionMultiple") || key.equals("estPartage")) {
			preparerDates();
		} else if (key.equals("dateDebutFormatee")) {
			preparerDateFin();
			modifierPoliceLabelEnfant();
		} 
		// pour préparer les vues arrêté
		super.displayGroupDidSetValueForObject(group,value,eo,key);
	}
	
	public void afficherEnfant() {
		NSMutableArray args = new NSMutableArray(currentConge().individu());
		String stringQualifier = EORepartEnfant.PARENT_KEY + " = %@";
		args.addObject(currentConge().dArriveeFoyer());
		stringQualifier = stringQualifier + " AND enfant.dArriveeFoyer >= %@";
		stringQualifier = stringQualifier + " AND enfant.temValide = 'O' AND lienFiliation.lfenCode = " + EOLienFiliationEnfant.LIEN_FILIATION_ADOPTION;
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
				
		UtilitairesDialogue.afficherDialogue(this,EORepartEnfant.ENTITY_NAME,"getEnfant",false,qualifier,true);
	}
	
	// Notifications
	public void getEnfant(NSNotification aNotif) {
		if (aNotif.object() != null) {
			EORepartEnfant repart = (EORepartEnfant)SuperFinder.objetForGlobalIDDansEditingContext((EOGlobalID)aNotif.object(), editingContext());
			if (repart != null) {
				currentConge().setEnfantRelationship(repart.enfant());
			}
			updateDisplayGroups();
		}
	}
	// méthodes du controller DG
	public boolean peutValider() {
		if (super.peutValider()) {
				return currentConge().dArriveeFoyer() != null;
		} else {
			return false;
		}
	}
	/** on ne peut modifier la date de debut que si la date d'arrivee au foyer est fournie */
	public boolean peutModifierDateDebut() {
		if (super.peutModifierDateDebut()) {
			return currentConge().dArriveeFoyer() != null;
		} else {
			return false;
		}
	}
	/** on ne peut modifier la date de fin que si l'arrete n'est pas signe et qu'il s'agit d'un conge partage */
	public boolean peutModifierDateFin() {
		if (super.peutModifierDateFin()) {
			return currentConge().estPartage();
		} else {
			return false;
		}
	}
	/** On ne peut selectionner un enfant que si la date d'arrivee au foyer est saisie */
	public boolean peutAfficherEnfant() {
		return  currentConge() != null && currentConge().individu() != null && currentConge().dateDebut() != null;
	}
	// méthodes protegees
	/** methode surchargee par les sous-classes pour charger l'archive ad-hoc */
	protected  void chargerArchive() {
		EOArchive.loadArchiveNamed("GestionCongeAdoption",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
		
		tfEnfantsACharge.addFocusListener(new FocusListenerNbEnfants());
		tfEnfantsACharge.addActionListener(new ActionListenerNbEnfants());

	}
	/** Surchargee pour modifier le label de enfant */
	protected void selectionnerConge() {
		super.selectionnerConge();
		modifierPoliceLabelEnfant();
	} 

	/** gere les controles concernant les declarations d'enfants */
	protected boolean traitementsAvantValidation() {	
		enfantsADeclarer = false;
		NSArray enfants = EOEnfant.rechercherEnfantsPourIndividuDate(editingContext(),currentConge().individu(),currentConge().dArriveeFoyer(),true);
		if (enfants.count() == 0) {
			enfantsADeclarer = true;
		} else {
			if (enfants.count() == 1 && currentConge().estAdoptionMultiple()) {
				EODialogs.runErrorDialog("ERREUR","Vous déclarez une adoption multiple, or vous n'avez déclaré qu'un enfant arrivé au foyer à cette date");
				return false;
			}
			if (enfants.count() > 1 && currentConge().estAdoptionMultiple() == false) {
				EODialogs.runErrorDialog("ERREUR","Vous déclarez une adoption simple, or vous avez déclaré plus d'un enfant arrivé au foyer à cette date");
				return false;
			}
		}
		return super.traitementsAvantValidation();
	}
	/** traitements apres validation : on propose si les enfants n'ont pas encore ete declares de le faire */
	protected void traitementsApresValidation() {
		if (enfantsADeclarer) {
			String message = "Vous n'avez pas encore déclaré l'enfant";
			if (currentConge().estAdoptionMultiple()) {
				message = "Vous n'avez pas encore déclaré les enfants adoptés";
			}
			if (EODialogs.runConfirmOperationDialog("ATTENTION",message + "\nSouhaitez-vous le faire ?","Oui","Non")) {
				NSNotificationCenter.defaultCenter().postNotification(ApplicationClient.ACTIVER_ACTION,"Enfants");
			}
		}
	}
	protected boolean traitementsAvantSuppression() {
		return true;
	}
	// Gestion des arrêtés
	protected void preparerPourRemplacement() {}
	protected String nomTableCongeContractuel() {
		return "CONGE_ADOPTION";
	}
	protected String nomTableCongeFonctionnaire() {
		return "CONGE_ADOPTION";
	}

	// méthodes privées
	private EOCongeAdoption currentConge() {
		return (EOCongeAdoption)displayGroup().selectedObject();
	}
	
	/**
	 * 
	 */
	private void preparerDates() {
		if (currentConge().dArriveeFoyer() != null) {
			currentConge().setDateDebut(currentConge().dArriveeFoyer());
		} else {
			currentConge().setDateDebut(null);
		}
		preparerDateFin();
		preparerPourCongeSansTraitement();
		modifierPoliceLabelEnfant();
	}
	
	/**
	 * 
	 */
	private void preparerDateFin() {
		if (currentConge().dateDebut() == null) {
			currentConge().setDateFin(null);
		} else {
			if (!currentConge().estPartage()) {
				int nbJoursAAjouter = currentConge().nbJoursLegaux(CocktailUtilities.getIntegerFromField(tfEnfantsACharge));
				currentConge().setDateFin(DateCtrl.dateAvecAjoutJours(currentConge().dateDebut(),nbJoursAAjouter-1));
			}
		}
		changerAffichageArrete();
	}
	private void modifierPoliceLabelEnfant() {
		int type = Font.PLAIN;
		if (currentConge() != null && currentConge().dateDebut() != null && DateCtrl.isAfter(currentConge().dateDebut(), dateLimitePourDeclarationEnfant)) {
			type = Font.BOLD;
		}
		Font font = labelEnfant.getFont();
		labelEnfant.setFont(new Font(font.getName(), type,font.getSize()));
	}
	
	
	private class ActionListenerNbEnfants implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			preparerDateFin();
		}
	}
	private class FocusListenerNbEnfants implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			preparerDateFin();
		}
	}

}

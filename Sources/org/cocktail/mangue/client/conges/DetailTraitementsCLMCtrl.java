package org.cocktail.mangue.client.conges;

import javax.swing.JFrame;

import org.cocktail.mangue.client.gui.conges.DetailTraitementsView;
import org.cocktail.mangue.common.conges.CalculDetailsTraitementsCLM;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;

/**
 * Controleur du détail des traitements de congé maladie pour les agents titulaires
 * 
 */
public class DetailTraitementsCLMCtrl {
	private static final long serialVersionUID = 0x7be9cfa4L;
	private static final String ENTITY_NAME = EOClm.ENTITY_NAME;
	private static DetailTraitementsCLMCtrl sharedInstance;
	private EOEditingContext 		ec;
	private EODisplayGroup 			eod;
	private DetailTraitementsView 	myView;

	private EOClm		 	currentConge;
	private EOIndividu 		currentIndividu;

	
	public DetailTraitementsCLMCtrl(EOEditingContext globalEc) {
		ec = globalEc;
		eod = new EODisplayGroup();
		myView = new DetailTraitementsView(new JFrame(), true, eod);

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) { fermer(); } }
		);

		myView.getTfTitre().setText("DETAIL DES TRAITEMENTS DE CONGE MALADIE ORDINAIRE");
		myView.getConsole().setEnabled(true);
	}

	public static DetailTraitementsCLMCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null) {
			sharedInstance = new DetailTraitementsCLMCtrl(editingContext);
		}
		return sharedInstance;
	}

	/**
	 * 
	 * Affichage de la fenetre de controle des détails de traitement
	 * 
	 * @param individu : individu
	 * @param absence : une absence
	 */
	public void afficherDetails(EOIndividu individu, EOAbsences absence) {

		currentIndividu = individu;

		Conge conge = Conge.rechercherCongeAvecAbsence(ec, ENTITY_NAME , absence);
		currentConge = (EOClm) conge;
		
		// Calcul des jours calendaires et comptables du conge
		myView.getTfJoursCalendaires().setText(String.valueOf(DateCtrl.nbJoursEntre(currentConge.dateDebut(), currentConge.dateFin(), true, false)));
		myView.getTfJoursComptables().setText(String.valueOf(DateCtrl.nbJoursEntre(currentConge.dateDebut(), currentConge.dateFin(), true, true)));

		if (currentConge != null) {
			myView.getLblTitreConge().setText(individu.identitePrenomFirst() + " - Congé du " + conge.dateDebutFormatee() + " au " + conge.dateFinFormatee());
			
			CalculDetailsTraitementsCLM calculCLM = new CalculDetailsTraitementsCLM();
			eod.setObjectArray(calculCLM.calculerDetails(ec, currentIndividu, currentConge.dateDebut(), currentConge.dateFin()));
			myView.getMyEOTable().updateData();
			myView.getConsole().setText(calculCLM.getMessageConsole());
		} else {
			EODialogs.runInformationDialog("ATTENTION", "Pas de congé associé à cette absence !");
		}

		myView.setVisible(true);
	}

	private void fermer() {
		myView.setVisible(false);
	}

}
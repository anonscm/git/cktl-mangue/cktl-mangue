/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.conges;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.conges.CongeAccidentServiceView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAccidentServ;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;

/** Gere les conges pour raison familiale ou personnelle<BR> */

public class CongeAccidentServiceCtrl extends ModelePageGestion {

	private static CongeAccidentServiceCtrl sharedInstance;

	private EODisplayGroup eod;
	private CongeAccidentServiceView myView;
	private ListenerConges 		listenerConges = new ListenerConges();

	private EODeclarationAccident currentDeclaration;
	private EOCongeAccidentServ currentConge;

	public CongeAccidentServiceCtrl (EOEditingContext edc) {

		super(edc);

		eod = new EODisplayGroup();
		myView = new CongeAccidentServiceView(new JFrame(), true, eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnImprimerArrete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerArrete(getCurrentConge());}}
				);
		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {myView.setVisible(false);}}
				);

		CocktailUtilities.initTextField(myView.getTfDateDeclaration(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeDéclaration(), false, false);

		myView.getTfNoArrete().addActionListener(new ActionListenerNoArrete());
		myView.getTfNoArrete().addFocusListener(new FocusListenerNoArrete());

		setDateListeners(myView.getTfDebut());
		setDateListeners(myView.getTfFin());
		setDateListeners(myView.getTfDateArrete());
		setDateListeners(myView.getTfDateComite());
		setDateListeners(myView.getTfDateCommission());

		myView.getMyEOTable().addListener(listenerConges);

	}

	/**
	 * 
	 * @param declaration
	 */
	public void open(EODeclarationAccident declaration) {

		setSaisieEnabled(false);
		setCurrentDeclaration(declaration);
		myView.setVisible(true);

	}

	public EODeclarationAccident getCurrentDeclaration() {
		return currentDeclaration;
	}

	public void setCurrentDeclaration(EODeclarationAccident currentDeclaration) {
		this.currentDeclaration = currentDeclaration;
		if (getCurrentDeclaration() != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDeclaration(), getCurrentDeclaration().daccDate());
			CocktailUtilities.setTextToField(myView.getTfTypeDéclaration(),  getCurrentDeclaration().typeAccident().libelleLong());
		}
		actualiser();
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CongeAccidentServiceCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CongeAccidentServiceCtrl(editingContext);
		return sharedInstance;
	}			


	public EOCongeAccidentServ getCurrentConge() {
		return currentConge;
	}
	public void setCurrentConge(EOCongeAccidentServ currentConge) {
		this.currentConge = currentConge;
		updateDatas();
	}

	/**
	 * 
	 */
	protected void traitementsApresValidation() {
		NSNotificationCenter.defaultCenter().postNotification(ListeAgents.CHANGER_EMPLOYE, getCurrentConge().individu().noIndividu(), null);

	}

	/**
	 * 
	 */
	public void imprimerArrete(Conge conge) {

		NSArray<EODestinataire> destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.size() > 0) {
			try {				
				NSMutableArray<EOGlobalID> destinatairesGlobalIds = new NSMutableArray<EOGlobalID>();
				for (EODestinataire destinataire : destinatairesArrete) {
					destinatairesGlobalIds.addObject(getEdc().globalIDForObject(destinataire));
				}
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class, Boolean.class};
				Object[] parametres = new Object[]{getEdc().globalIDForObject(conge),destinatairesGlobalIds, false};
				UtilitairesImpression.imprimerAvecDialogue(getEdc(),"clientSideRequestImprimerArreteConge",classeParametres,parametres,"CongeAccidentServ" + conge.individu().noIndividu() ,"Impression de l'arrêté de congé accident de service");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}


	private class ListenerConges implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentConge() != null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentConge((EOCongeAccidentServ)eod.selectedObject());
		}
	}

	private class ActionListenerNoArrete implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			updateInterface();
		}
	}
	private class FocusListenerNoArrete implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			updateInterface();
		}
	}


	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfDebut());
		CocktailUtilities.viderTextField(myView.getTfFin());
		CocktailUtilities.viderTextField(myView.getTfDateArrete());
		CocktailUtilities.viderTextField(myView.getTfNoArrete());
		CocktailUtilities.viderTextArea(myView.getTaCommentaires());
		CocktailUtilities.viderTextField(myView.getTfDateCommission());
		CocktailUtilities.viderTextField(myView.getTfDateComite());

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentConge() != null) {

			CocktailUtilities.setDateToField(myView.getTfDebut(), getCurrentConge().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfFin(), getCurrentConge().dateFin());

			CocktailUtilities.setDateToField(myView.getTfDateComite(), getCurrentConge().dateComMed());
			CocktailUtilities.setDateToField(myView.getTfDateCommission(), getCurrentConge().dComReforme());
			CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentConge().dateArrete());
			CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentConge().noArrete());

			CocktailUtilities.setTextToArea(myView.getTaCommentaires(), getCurrentConge().commentaire());

		}

		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnAjouter().setEnabled(!isSaisieEnabled());
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentConge() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentConge() != null);
		myView.getBtnImprimerArrete().setEnabled(!isSaisieEnabled() && getCurrentConge() != null);

		myView.getBtnValider().setEnabled(isSaisieEnabled() );
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled() );
		myView.getBtnFermer().setEnabled(!isSaisieEnabled() );

		CocktailUtilities.initTextField(myView.getTfNoArrete(), false, isSaisieEnabled());	
		CocktailUtilities.initTextField(myView.getTfDebut(), false, isSaisieEnabled());	
		CocktailUtilities.initTextField(myView.getTfFin(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateArrete(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateComite(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateCommission(), false, isSaisieEnabled());
		CocktailUtilities.initTextArea(myView.getTaCommentaires(), false, isSaisieEnabled());

	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		eod.setObjectArray(EOCongeAccidentServ.rechercherPourDeclaration(getEdc(), getCurrentDeclaration()));
		myView.getMyEOTable().updateData();

	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().updateUI();

	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		
		getCurrentConge().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		getCurrentConge().setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		getCurrentConge().setDateComMed(CocktailUtilities.getDateFromField(myView.getTfDateComite()));
		getCurrentConge().setDComReforme(CocktailUtilities.getDateFromField(myView.getTfDateCommission()));
		getCurrentConge().setDateArrete(CocktailUtilities.getDateFromField(myView.getTfDateArrete()));
		getCurrentConge().setNoArrete(CocktailUtilities.getTextFromField(myView.getTfNoArrete()));
		getCurrentConge().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTaCommentaires()));

		// CREATION DE L ABSENCE
		EOAbsences absence = getCurrentConge().absence() ;
		if (absence == null) {
			absence = EOAbsences.creer(getEdc(), getCurrentConge().individu(), 
					(EOTypeAbsence)NomenclatureFinder.findForCode(getEdc(), EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_ACC_SERV));
			getCurrentConge().setAbsenceRelationship(absence);
		}
		absence.setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		absence.setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		absence.setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(getCurrentConge().dateDebut(), getCurrentConge().dateFin(), true)));

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentConge(EOCongeAccidentServ.creer(getEdc(), getCurrentDeclaration()));
	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getCurrentConge().absence().setEstValide(false);
		getCurrentConge().setEstValide(false);

	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		// Rafraichissement des onglets
		NSNotificationCenter.defaultCenter().postNotification(ListeAgents.CHANGER_EMPLOYE, getCurrentConge().individu().noIndividu(), null);

	}



}
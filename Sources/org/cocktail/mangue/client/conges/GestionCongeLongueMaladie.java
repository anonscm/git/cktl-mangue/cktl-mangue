/*
 * Created on 13 sept. 2005
 *
 * Gestion des congés de longue et grave maladie
 * Window - Preferences - Java - Code Style - Code Templates
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JLabel;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecRemiseEnCause;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.mangue.conges.EOCgntCgm;
import org.cocktail.mangue.modele.mangue.conges.EOCgntMaladie;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaladie;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOStage;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/** Gestion des conges de longue maladie (fonctionnaire)<BR>
 * Conges de longue maladie :<BR>
 * La date de debut, de fin et de la date d'avis du Comite medical sont obligatoires.<BR>
 * Si La duree du conge est inferieure a 3 mois, on signale a l'utilisateur que la duree devrait etre superieure.<BR>
 * <BR>
 * On ne permet pas la production d'arrete si la date d'avis du comite medical superieur est fournie.<BR>
 * La duree maximale de conges continus est de 3 ans.<BR>
 * Dans le cas de conges discontinus pendant 3 ans, l'agent doit reprendre son travail pendant 1 an pour beneficier 
 * d'un nouveau conge de longue maladie.<BR>
 * <BR>
 * Conges de grave maladie :<BR>
 * La date de debut et de fin sont obligatoires.<BR>
 * Si La duree du conge est inferieure a 3 mois, on signale a l'utilisateur que la duree devrait etre superieure.<BR>
 * Si l'anciennete continue est inferieure a 3 ans, on signale a l'utilisateur que l'agent ne devrait pas beneficier d'un conge 
 * de grave maladie<BR>
 * <BR>
 * Gestion des requalifications des conges de maladie. Voir description dans la classe RequalificationConges<BR>
 */

public class GestionCongeLongueMaladie extends GestionCongeAvecRemiseEnCause {
	public EOView vueFonctionnaire;
	public JLabel labelAvis;

	public GestionCongeLongueMaladie(String nomEntite) {
		super(nomEntite,"Gestion des congés de longue maladie");
		if (nomEntite.equals(EOCgntCgm.ENTITY_NAME)) {
			setDialogTitle("Gestion des congés de grave maladie");
		}
	}

	// accesseurs
	public boolean estRequalifie() {
		if (currentConge() == null || currentConge() instanceof EOCgntCgm) {
			return false;
		} else {
			return ((EOClm)currentConge()).estRequalifie();
		}

	}
	public boolean estProlonge() {
		if (currentConge() == null || currentConge() instanceof EOCgntCgm) {
			return false;
		} else {
			return ((EOClm)currentConge()).estProlonge();
		}
	}
	public void setEstProlonge(boolean aBool) {
		if (currentConge() != null && currentConge() instanceof EOClm) {
			((EOClm)currentConge()).setEstProlonge(aBool);
		}
	}
	public String dateAvisComMedical() {
		if (currentConge() == null || currentConge() instanceof EOCgntCgm) {
			return null;
		} else {
			return ((EOClm)currentConge()).dateComMedFormatee();
		}
	}
	public void setDateAvisComMedical(String aStr) {
		if (currentConge() != null && currentConge() instanceof EOClm) {
			((EOClm)currentConge()).setDateComMedFormatee(aStr);
		}
	}
	public String dateAvisComMedicalSup() {
		if (currentConge() == null || currentConge() instanceof EOCgntCgm) {
			return null;
		} else {
			return ((EOClm)currentConge()).dateComMedSupFormatee();
		}
	}
	public void setDateAvisComMedicalSup(String aStr) {
		if (currentConge() != null && currentConge() instanceof EOClm) {
			((EOClm)currentConge()).setDateComMedSupFormatee(aStr);
			// on ne permet la saisie d'arrêté que si la date de commision médicale est nulle
			changerAffichageArrete(((EOClm)currentConge()).dComMedClmSup() == null);	
		}
	}
	// méthodes de délégation du DG
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (key.equals("dateDebutFormatee") || key.equals("dateFinFormatee")) {
			validerEntitePourDates();
			super.displayGroupDidSetValueForObject(group,value,eo,key);
		}
		updateDisplayGroups();
	}

	// méthodes du controller DG
	/** on ne peut pas modifier un conge de longue maladie requalifie */
	public boolean peutModifierConge() {
		if (currentConge() == null) {
			return false;
		}
		if (currentConge() instanceof EOClm) {
			return super.peutModifierConge(); //&& ((EOClm)currentConge()).estRequalifie() == false;
		} else {
			return true;
		}
	}
	public boolean peutAnnulerArrete() {
		if (currentConge() == null) {
			return false;
		}
		if (currentConge() instanceof EOCgntCgm) {
			return false;
		} else {
			return super.peutAnnulerArrete();
		}
	}
	public boolean peutAfficherArreteAnnulation() {
		if (currentConge() == null) {
			return false;
		}
		if (currentConge() instanceof EOCgntCgm) {
			return false;
		} else {
			return super.peutAfficherArreteAnnulation();
		}
	}
	// autres
	public void afficherFenetre() {
		vueFonctionnaire.setVisible(currentConge() instanceof EOClm);
		if (currentConge() instanceof EOCgntCgm) {
			((JDialog)window()).setTitle("Gestion des congés de grave maladie");	// fait maintenant car on ne pouvait pas le faire dans le constructeur
			// l'affichage des arrêtés est fait dans l'appel à super. Dans le cas où une date de commission médicale supérieure est saisie, il faut les masquer
		} else if (currentConge() instanceof EOClm) {
			changerAffichageArrete(((EOClm)currentConge()).dComMedClmSup() == null);
			Font font = labelAvis.getFont();
			labelAvis.setFont(new Font(font.getName(),Font.BOLD,font.getSize()));
		}
		super.afficherFenetre();

	}
	//	méthodes du controller DG
	public boolean peutValider() {
		if (super.peutValider()) {
			if (currentConge() instanceof EOClm) {
				return ((EOClm)currentConge()).dComMedClm() != null;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/** methode surchargee par les sous-classes pour charger l'archive ad-hoc */
	protected  void chargerArchive() {
		EOArchive.loadArchiveNamed("GestionCongeLongueMaladie",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
	}
	protected boolean traitementsAvantValidation() {
		if (super.traitementsAvantValidation()) { 		// on le fait avant pour que les données de l'arrêté soit récupérée
			if (currentConge() instanceof EOCgntCgm) {
				verifierAnciennete();
			} else {
				// Clm
				NSArray stages = EOStage.findForIndividuEtPeriode(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin(),false);
				if (stages != null && stages.count() > 0) {
					EODialogs.runInformationDialog("Attention","L'agent est en stage pendant cette période, pensez à modifier la date de la fin de stage");	        		
				}
			}
			if (validerDurees()) {
				// qualifier les congés de maladie pour cette période
				String nomEntiteConge = "CongeMaladie";
				String message = "longue maladie";
				if (currentConge() instanceof EOCgntCgm) {
					nomEntiteConge = "CgntMaladie";
					message = "grave maladie";
				}
				return RequalificationConges.requalifierCongesAssocies(currentConge(),nomEntiteConge,message,"maladie");
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	protected boolean traitementsAvantSuppression() {
		String nomEntiteMaladie = EOCgntMaladie.ENTITY_NAME;
		if (currentConge() instanceof EOClm)
			nomEntiteMaladie = EOCongeMaladie.ENTITY_NAME;

		RequalificationConges.annulerQualificationCongesAssocies(currentConge(),nomEntiteMaladie);
		return true;
	}
	protected void traitementsApresSuppression() {}

	// Gestion des arrêtés
	protected EOTypeGestionArrete typeGestionArrete() {
		if (currentConge() != null && currentConge().individu() != null) {
			if (currentConge() instanceof EOClm) {
				return currentConge().individu().rechercherTypeGestionArretePourPeriode(null, "CLM", currentConge().dateDebut(),currentConge().dateFin());
			} else {
				return currentConge().individu().rechercherTypeGestionArretePourPeriode("CGNT_CGM",null,currentConge().dateDebut(),currentConge().dateFin());
			}
		} else {
			return null;
		}

	}
	protected boolean supporteSignature() {
		return currentConge() != null && currentConge() instanceof EOClm;
	}
	/** si le conge de maladie annule requalifiait des conges de maladie, supprimer cette requalification */
	protected void preparerPourRemplacement() {
		String nomEntiteMaladie = "CgntMaladie";
		if (congeRemplace() instanceof EOClm) {
			nomEntiteMaladie = "CongeMaladie";
		}
		RequalificationConges.annulerQualificationCongesAssocies(congeRemplace(),nomEntiteMaladie);
	}

	//	 méthodes privées
	private CongeAvecArreteAnnulation currentConge() {
		return (CongeAvecArreteAnnulation)displayGroup().selectedObject();
	}
	// Vérifie si il est possible de créer ce type de congé à cette date
	private void validerEntitePourDates() {
		if (currentConge().dateDebut() == null) {
			currentConge().setDateFin(null);
			return;
		}
		if (currentConge() instanceof EOClm) {
			if (EOChangementPosition.fonctionnaireEnActivitePendantPeriodeComplete(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin()) == false) {
				EODialogs.runInformationDialog("Attention", "Le congé de longue maladie ne s'applique qu'aux fonctionnaires en activité pendant toute la période.\nVeuillez saisir de nouvelles dates.");
				currentConge().setDateDebut(null);
				currentConge().setDateFin(null);
			}
		} else {
			if (currentConge().individu().estContractuelSurPeriodeComplete(currentConge().dateDebut(),currentConge().dateFin()) == false) {
				EODialogs.runInformationDialog("Attention", "Le congé de grave maladie ne s'applique qu'aux contractuels ou considérés contractuels en activité pendant toute la période.\nVeuillez saisir de nouvelles dates.");
				currentConge().setDateDebut(null);
				currentConge().setDateFin(null);
			}
		}
	}
	private void verifierAnciennete() {
		int nbAnneesAnciennete = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_ANCIENNETE);
		NSTimestamp debut = DateCtrl.dateAvecAjoutAnnees(currentConge().dateDebut(), -nbAnneesAnciennete);
		if (currentConge().individu().estContractuelSurPeriodeComplete(debut,currentConge().dateDebut()) == false) {
			EODialogs.runInformationDialog("Attention","Pour bénéficier d'un congé de grave maladie, un agent doit avoir au moins " + nbAnneesAnciennete + " ans d'ancienneté");
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private boolean validerDurees() {
		
		if (currentConge() instanceof EOClm) {
			int nbMois = DateCtrl.calculerDureeEnMois(currentConge().dateDebut(),currentConge().dateFin(),true).intValue();
			Integer nbMoisMin = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_DUREE_NON_BLOQUANTE);

			if (nbMoisMin != null && (nbMois < nbMoisMin.intValue()) ) {
				EODialogs.runInformationDialog("Attention","La durée minimum d'un congé de longue maladie est de " + nbMoisMin + " mois");
			}
			nbMois = ((CongeAvecArreteAnnulation)currentConge()).dureeEnMoisCongesContinusAssocies();
			int nbMoisMax = EOClm.nbMoisMaxCongesContinus(editingContext());
			if (nbMois > nbMoisMax) {
				EODialogs.runErrorDialog("Erreur","La durée maximale de congés de longue maladie continus est de " + nbMoisMax / 12 + " ans");
				return false;
			} else {
				// traiter le cas des congés discontinus
				return true;
			}
		} else {
			int nbMois = DateCtrl.calculerDureeEnMois(currentConge().dateDebut(),currentConge().dateFin(),true).intValue();
			int nbMoisMin = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_DUREE_MIN);
			if (nbMois < nbMoisMin) {
				EODialogs.runInformationDialog("Attention","La durée minimum d'un congé de grave maladie est de " + nbMoisMin + " mois");
			}
			// on ne vérifie pas pour les durées supérieures à 6 mois, cela doit générer une erreur qui est gérée dans le
			// validateForSave
			nbMois = ((CongeAvecArreteAnnulation)currentConge()).dureeEnMoisCongesContinusAssocies();

			int nbMoisMaxContigus = EOCgntCgm.nbMoisMaxCongesContinus(editingContext());
			if (nbMois > nbMoisMaxContigus) {
				EODialogs.runErrorDialog("Erreur","La durée maximale de congés de grave maladie continus est de " + nbMoisMaxContigus / 12 + " ans");
				return false;
			}
			return true;
		}
	}

}

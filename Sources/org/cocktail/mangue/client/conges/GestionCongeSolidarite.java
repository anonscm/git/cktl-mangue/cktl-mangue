/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
/** Le conge de solidarite est commun aux fonctionnaires et contractuels.<BR>
 *  Si c'est un conge normal (pas fractionne et pas de temps partiel), il peut &ecirc;tre renouvele une fois sur une periode de 3 mois.<BR>
 * Gestion des suppressions : lorsqu'un conge de renouvellement est supprime, on le supprime du conge d'origine<BR>
 * Lorsqu'un conge renouvele est supprime, on supprime aussi le conge de renouvellement.
*/
package org.cocktail.mangue.client.conges;

import org.cocktail.client.components.utilities.UtilitairesDialogue;
import org.cocktail.client.composants.ModelePage;
import org.cocktail.common.LogManager;
import org.cocktail.component.COButton;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecArrete;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.conges.EOCongeSolidariteFamiliale;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;


public class GestionCongeSolidarite extends GestionCongeAvecArrete {
	public COButton boutonRenouveler;
	private boolean windowHidden;
	private EOCongeSolidariteFamiliale congeARenouveler;
	
	public GestionCongeSolidarite() {
		super("CongeSolidariteFamiliale", "Congé de solidarité familiale");
	}
	
	// Actions
	public void renouveler() {
		LogManager.logDetail("CongeSolidariteFamiliale - renouveler");
		GestionCongeSolidarite controleur = new GestionCongeSolidarite();
		controleur.setEditingContext(editingContext());	
		if (currentConge().congeRenouvele() != null && currentConge().congeRenouvele().estAnnule() == false) {
			controleur.modifier(editingContext.globalIDForObject(currentConge().congeRenouvele().absence()));
		} else {
			controleur.ajouter(editingContext().globalIDForObject(currentConge().individu()));
		}
		window().hide();	// masquer la fenêtre car problème lors de l'empilement de dialogues modaux
		windowHidden = true;
		// à la sortie de cette méthode le nouveau congé est initialisé
		controleur.initialiserAvecConge(currentConge());
		// s'enregistrer pour recevoir les notifications
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("terminer",new Class[] {NSNotification.class}),GestionCongeAvecArreteAnnulation.TERMINER_AFFICHAGE,null);
	}
	public void afficherTypeConge() {
		UtilitairesDialogue.afficherDialogue(this, "TypeCsf", "getTypeCsf");
	}
	// Autres
	public void initialiserAvecConge(EOCongeSolidariteFamiliale congeARenouveler) {
		this.congeARenouveler = congeARenouveler;
		currentConge().initAvecConge(congeARenouveler);
		boutonRenouveler.setVisible(false);
	}
	
	// Notifications
	public void getTypeCsf(NSNotification aNotif) {
		ajouterRelation(currentConge(), aNotif.object(), "typeCsf");
		updateDisplayGroups();
	}
	/** Surcharge du parent pour supprimer l'ecoute de la notification */
	public void getDestinataires(NSNotification aNotif) {
		super.getDestinataires(aNotif);
		NSNotificationCenter.defaultCenter().removeObserver(this, aNotif.name(), null);
	}
	/** declenchee suite a l'ajout d'un conge de renouvellement depuis cette fen&ecirc;tre */
	public void terminer(NSNotification aNotif) {
		if (!windowHidden) {
			terminer();
		}
	}
	/** declenchee suite a la validation ou l'annulation dans une fenetre ouverte &agrave; partir de cette fenetre */
	public void unlockSaisie(NSNotification aNotif) {
		if (windowHidden) {
			// Il faut changer l'état avant d'afficher la fenêtre sinon il est changé à un moment imprévisible
			windowHidden = false;
			controllerDisplayGroup().redisplay();	// pour avoir l'état du bouton d'ajout correct
			//  Il y a eu empilement de fenêtre, locker de nouveau et rendre visible la fenêtre
			NSNotificationCenter.defaultCenter().postNotification(ModelePage.LOCKER_ECRAN,this);
			window().show();
		}
	}
	// Méthodes du controller DG
	/** On ne peut renouveler un conge que si ce n'est pas un conge de renouvellement */
	public boolean peutRenouveler() {
		return typeTraitement() != MODE_CREATION && currentConge().estRenouvele() == false;
	}
	/** La date de debut, de fin et de certificat medical et le type de conge doivent &ecirc;tre saisis */
	public boolean peutValider() {
		return super.peutValider() && currentConge().dCertificatMedical() != null && currentConge().typeCsf() != null;
	}
	// Méthodes protégées
	protected boolean supporteSignature() {
		return true;
	}
	/** Arrete gere par l'etablissement */
	protected EOTypeGestionArrete typeGestionArrete() {
		return EOTypeGestionArrete.competenceEtablissement(editingContext());
	}
	protected void chargerArchive() {
		EOArchive.loadArchiveNamed("GestionCongeSolidarite",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
	}
	protected void traitementsApresSuppression() {
		// pas de traitement particulier
	}
	protected boolean traitementsAvantSuppression() {
		if (currentConge().estRenouvele() && congeARenouveler == null) {
			// En cas de suppression de l'absence
			congeARenouveler = currentConge().congeOrigine();
		}
		if (congeARenouveler != null) {
			congeARenouveler.removeObjectFromBothSidesOfRelationshipWithKey(currentConge(), "congeRenouvele");
		}
		return true;
	}
	protected void traitementsApresValidation() {
	}
	// Méthodes privées
	private EOCongeSolidariteFamiliale currentConge() {
		return (EOCongeSolidariteFamiliale)displayGroup.selectedObject();
	}

	
}

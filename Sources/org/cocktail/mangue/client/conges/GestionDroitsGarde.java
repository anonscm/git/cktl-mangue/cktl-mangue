
//GestionDroitsGarde.java
//Mangue

//Created by Christine Buttin on Thu Sep 15 2005.
//Copyright (c) 2001 __MyCompanyName__. All rights reserved.


/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import java.awt.Cursor;
import java.math.BigDecimal;
import java.util.TimeZone;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.outils_interface.ModelePageAvecIndividu;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.conges.EOCongeGardeEnfant;
import org.cocktail.mangue.modele.mangue.conges.EODroitsGardeEnfant;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

public class GestionDroitsGarde extends ModelePageAvecIndividu {
	NSArray congesPourAnnee;
	private boolean estEnTrainDeCalculer;
	private boolean estAnneeCivile;
	private String titre;
	
	public GestionDroitsGarde() {
		super();
	}
	public void connectionWasBroken() {
		super.connectionWasBroken();
		NSNotificationCenter.defaultCenter().postNotification(GestionAbsences.SYNCHRONISATION_DROITS_GARDE,null);
	}
	// Accesseurs
	public String titre() {
		return titre;
	}
	// méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		rechercherCongesPourAnneeSelectionnee();
		super.displayGroupDidChangeSelection(aGroup);
		estEnTrainDeCalculer = false;
	}
	public void displayGroupDidSetValueForObject(EODisplayGroup group,Object value, Object eo,String key) {
		if (estEnTrainDeCalculer) {
			return;
		}
		if (key.equals("annee")) {
			estEnTrainDeCalculer = true;
			calculerNbDemiJournees(); 		// déclenche une boucle infinie pour une raison ignorée	
			rechercherCongesPourAnneeSelectionnee();
			estEnTrainDeCalculer = false;
		}
	}
	
	public void recalculer() {
		if (estEnTrainDeCalculer) {
			return;
		}
		super.modifier();
		estEnTrainDeCalculer = true;
		calculerNbDemiJournees(); 		// déclenche une boucle infinie pour une raison ignorée	
		rechercherCongesPourAnneeSelectionnee();
		estEnTrainDeCalculer = false;	
	}
	
	// methodes du controller DG
	/** on ne peut supprimer que si il n'y a pas de conge defini pour l'annee */
	public boolean peutSupprimer() {
		return super.boutonModificationAutorise() && (congesPourAnnee == null || congesPourAnnee.count() == 0);
	}
	public boolean peutValider() {
		if (currentDroit() == null || currentDroit().annee() == null || currentDroit().nbDemiJournees() == null) {
			return false;
		} else {
			return super.peutValider();
		}
	}
	
	// Méthodes protégées
	protected void preparerFenetre() {
		super.preparerFenetre();
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
		String typeAnnee = EOGrhumParametres.getAnneeRefGardeEnfant();
		estAnneeCivile = typeAnnee == null || typeAnnee.equals(EOCongeGardeEnfant.ANNEE_CIVILE);
		if (estAnneeCivile) {
			titre = "Droits de garde par Année Civile";
		} else {
			titre = "Droits de garde par Année Universitaire";
		}
	}
	protected NSArray fetcherObjets() {
		if (currentIndividu() != null) {
			return EODroitsGardeEnfant.rechercherDroitsGardePourIndividu(editingContext(),currentIndividu());
		} else {
			return null;
		}
	}
	protected void parametrerDisplayGroup() {
		displayGroup().setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("annee",EOSortOrdering.CompareDescending)));
	}
	/** traitements a executer suite a la creation d'un objet */
	protected void traitementsPourCreation() {
		currentDroit().initAvecIndividu(currentIndividu());
	}
	/** traitements a executer avant la destruction d'un objet */
	protected  boolean traitementsPourSuppression() {
		currentDroit().supprimerRelations();
		deleteSelectedObjects();
		return true;
	}
	/** traitements a executer avant la destruction d'un objet */
	protected  void traitementsApresSuppression() {
		remettreEtatSelection();
		super.traitementsApresSuppression();
	}
	/** methode a surcharger pour faire des traitements avant l'enregistrement des donnees dans la base
	 * retourne true si on peut enregistrer */
	protected boolean traitementsAvantValidation() {
		if (verificationAnnee() == false) {
			// il y a déjà des congés définis pour cette année-là
			EODialogs.runErrorDialog("Erreur","Les droits sont déjà définis pour l'année " + currentDroit().annee());
			return false;
		}
		// vérifier que le nombre de demi-journees n'est pas inferieur au nombre deja pris
		if (modeCreation()) {
			return true;
		} else {
			float total = 0;
			for (java.util.Enumeration<EOCongeGardeEnfant> e = congesPourAnnee.objectEnumerator();e.hasMoreElements();) {
				EOCongeGardeEnfant conge = e.nextElement();
				total = total + new Float(conge.nbDemiJournees()).floatValue();
			}
			if (total > currentDroit().nbDemiJournees().floatValue()) {
				EODialogs.runErrorDialog("Erreur","Le nombre de demi-journées autorisé est trop petit.\n" + currentIndividu().identitePrenomFirst() + " a déjà pris " + new Float(total).intValue() + " demi-journées en " + currentDroit().annee());
				return false;
			} else {
				return true;
			}
		}
	}


	/** retourne le message de confirmation aupres de l'utilisateur avant d'effectuer une suppression  */
	protected String messageConfirmationDestruction() {
		return "Voulez-vous vraiment supprimer ces droits";
	}
	/** affiche un message suite &agrave; une exception de validation */
	protected void afficherExceptionValidation(String message) {
		EODialogs.runErrorDialog("ERREUR",message);
		controllerDisplayGroup().redisplay();
	}
	protected void terminer() {
	}
	// méthodes privées
	private EODroitsGardeEnfant currentDroit() {
		return (EODroitsGardeEnfant)displayGroup().selectedObject();
	}
	private boolean verificationAnnee() {
		for (java.util.Enumeration<EODroitsGardeEnfant> e = displayGroup().displayedObjects().objectEnumerator();e.hasMoreElements();) {
			EODroitsGardeEnfant droit = e.nextElement();
			if (droit != currentDroit() && droit.annee().intValue() == currentDroit().annee().intValue()) {
				return false;
			}
		}
		return true;
	}
	
	private void calculerNbDemiJournees() {
		component().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		LogManager.logDetail("GestionDroitsGarde - calculerNbDemiJournees, annee :" + currentDroit().annee());
		if (currentDroit().annee() != null) {
			NSTimestamp dateDebut = null, dateFin = null;
			if (estAnneeCivile) {
				dateDebut = new NSTimestamp(currentDroit().annee().intValue(),1,1,0,0,0,TimeZone.getTimeZone("Europe/Paris"));
				dateFin = new NSTimestamp(currentDroit().annee().intValue(),12,31,0,0,0,TimeZone.getTimeZone("Europe/Paris"));
			} else {
				dateDebut = new NSTimestamp(currentDroit().annee().intValue() - 1,9,1,0,0,0,TimeZone.getTimeZone("Europe/Paris"));
				dateFin = new NSTimestamp(currentDroit().annee().intValue(),8,31,0,0,0,TimeZone.getTimeZone("Europe/Paris"));
			}
			
			NSArray tempsPartiels = EOTempsPartiel.rechercherTempsPartielPourIndividuEtPeriode(editingContext(),currentIndividu(),dateDebut,dateFin);
			LogManager.logDetail("GestionDroitsGarde - calculerNbDemiJournees, nb temps partiels : " + tempsPartiels.count());
			if (tempsPartiels.count() == 0) {
				currentDroit().setNbDemiJournees(new Integer(EODroitsGardeEnfant.NB_DEMI_JOURNEES_GARDE + EODroitsGardeEnfant.NB_DEMI_JOURNEES_SUPPLEMENTAIRE));
			} else {
				tempsPartiels = EOSortOrdering.sortedArrayUsingKeyOrderArray(tempsPartiels,new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));
				float ratioDemiJournees = 0;
				for (int i = 0; i < tempsPartiels.count() ; i++) {
					float nbJours = 0, nbJoursComplet = 0;
					EOTempsPartiel tempsPartiel = (EOTempsPartiel)tempsPartiels.objectAtIndex(i);
					if (DateCtrl.isBeforeEq(tempsPartiel.dateDebut(),dateDebut)) {
						// Le temps partiel commence avant
						LogManager.logDetail("debut des jours de temps partiel " + DateCtrl.dateToString(dateDebut));	
						if (DateCtrl.isBeforeEq(tempsPartiel.dateFin(),dateFin)) {
							// Il se termine avant : il y a donc de dateDebut à tempsPartiel.dateFin de jours à temps partiel
							LogManager.logDetail("fin des jours de temps partiel " + DateCtrl.dateToString(tempsPartiel.dateFin()));
							nbJours = DateCtrl.nbJoursEntre(dateDebut,tempsPartiel.dateFin(),true);
							LogManager.logDetail("nb de jours à temps partiel " + nbJours);
							if (i == tempsPartiels.count() - 1) {
								// dernier temps partiel : calculer la part à 100% sur le reste de l'année, sinon on prendra en compte
								// à la prochaine itération
								NSTimestamp dateD = DateCtrl.jourSuivant(tempsPartiel.dateFin());
								LogManager.logDetail("Periode complete du  " + DateCtrl.dateToString(dateD) + " au " + DateCtrl.dateToString(dateFin));	
								nbJoursComplet = DateCtrl.nbJoursEntre(dateD,dateFin,true);
								LogManager.logDetail("Nb jours à temps complet " + nbJoursComplet);
							}
						} else {
							// Le temps partiel commence se termine après (il couvre toute la période
							nbJours = DateCtrl.nbJoursEntre(dateDebut,dateFin,true);
							LogManager.logDetail("nb de jours à temps partiel sur période complete " + nbJours);
						}
					} else {
						//	Le temps partiel commence après
						if (i == 0) {
							NSTimestamp dateF = DateCtrl.jourPrecedent(tempsPartiel.dateDebut());
							LogManager.logDetail("Periode complete du  " + DateCtrl.dateToString(dateDebut) + " au " + DateCtrl.dateToString(dateF));	
							// pour le premier élément, calculer la part à 100% pour la partie entre le début de la période et 
							// le début de temps partiel
							nbJoursComplet = DateCtrl.nbJoursEntre(dateDebut,dateF,false);
							LogManager.logDetail("Nb jours à temps complet " + nbJoursComplet);
						}
						LogManager.logDetail("debut des jours de temps partiel " + DateCtrl.dateToString(tempsPartiel.dateDebut()));	
						if (DateCtrl.isBeforeEq(tempsPartiel.dateFin(),dateFin)) {
							// Il se termine avant : il y a donc de tempsPartiel.dateDebut à tempsPartiel.dateFin de jours à temps partiel
							LogManager.logDetail("fin des jours de temps partiel " + DateCtrl.dateToString(tempsPartiel.dateFin()));
							nbJours = DateCtrl.nbJoursEntre(tempsPartiel.dateDebut(),tempsPartiel.dateFin(),true);
							LogManager.logDetail("nb de jours à temps partiel " + nbJours);
							if (i == tempsPartiels.count() - 1 && DateCtrl.dateToString(dateFin).equals(DateCtrl.dateToString(tempsPartiel.dateFin())) == false) {
								// dernier temps partiel : calculer la part à 100% sur le reste de l'année, sinon on prendra en compte
								// à la prochaine itération
								NSTimestamp dateD = DateCtrl.jourSuivant(tempsPartiel.dateFin());
								LogManager.logDetail("Periode complete du  " + DateCtrl.dateToString(dateD) + " au " + DateCtrl.dateToString(dateFin));	
								nbJoursComplet = DateCtrl.nbJoursEntre(dateD,dateFin,true);
								LogManager.logDetail("Nb jours à temps complet " + nbJoursComplet);
							}
						} else {
							nbJours = DateCtrl.nbJoursEntre(tempsPartiel.dateDebut(),dateFin,true);
							LogManager.logDetail("nb de jours à temps partiel " + nbJours);
						}
						
					}
					// Calculer la quotité à temps plein
					float ratioDemiJourneesPourTempsComplet = (nbJoursComplet / DateCtrl.NB_JOURS_COMPTABLES_ANNUELS) * (EODroitsGardeEnfant.NB_DEMI_JOURNEES_GARDE + EODroitsGardeEnfant.NB_DEMI_JOURNEES_SUPPLEMENTAIRE);
					LogManager.logDetail("ratio pour temps complet " + ratioDemiJourneesPourTempsComplet);
					// Pour la quotité à temps partiel, on calcule d'abord le ratio de demi-journées annuel en fonction de la quotité de 
					// temps partiel : quotité du nombre de demi-journées + 2
					float nbDemisJoursPourTempsPartiel = (tempsPartiel.quotite().floatValue() / 100) * EODroitsGardeEnfant.NB_DEMI_JOURNEES_GARDE;
					nbDemisJoursPourTempsPartiel += EODroitsGardeEnfant.NB_DEMI_JOURNEES_SUPPLEMENTAIRE;
					LogManager.logDetail("nb demi-journées temps partiel max : " + nbDemisJoursPourTempsPartiel + ", pour la quotite de temps partiel :" + tempsPartiel.quotite());
					// puis on pondère par la durée du temps partiel
					float ratioDemiJourneesPourTempsPartiel = (nbDemisJoursPourTempsPartiel * nbJours) / DateCtrl.NB_JOURS_COMPTABLES_ANNUELS;
					LogManager.logDetail("ratio temps partiel " + ratioDemiJourneesPourTempsPartiel);
					ratioDemiJournees += ratioDemiJourneesPourTempsComplet + ratioDemiJourneesPourTempsPartiel;	// on ajoute au ratio déjà calculé
					LogManager.logDetail("ratio totale pour la période " + ratioDemiJourneesPourTempsComplet + ratioDemiJourneesPourTempsPartiel);
					LogManager.logDetail("ratio totale " + ratioDemiJournees);
				}
				BigDecimal result = new BigDecimal(ratioDemiJournees).setScale(0,BigDecimal.ROUND_HALF_UP);
				LogManager.logDetail("nb jours resultat " + result.intValue());
				currentDroit().setNbDemiJournees(new Integer(result.intValue()));
			}
		} else {
			currentDroit().setNbDemiJournees(null);
		}
		updaterDisplayGroups();
		component().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

	}
	private void rechercherCongesPourAnneeSelectionnee() {
		if (currentDroit() == null) {
			congesPourAnnee = null;
			return;
		}
		if (!modeCreation()) {
			congesPourAnnee = EOCongeGardeEnfant.rechercherDureesPourIndividuEtPeriode(editingContext(),
					EOCongeGardeEnfant.ENTITY_NAME, currentIndividu(), 
					DateCtrl.debutAnnee(currentDroit().annee()),
					DateCtrl.finAnnee(currentDroit().annee()));
		} else {
			congesPourAnnee = null;
		}
	}
}

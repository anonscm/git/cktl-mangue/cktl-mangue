/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.client.conges;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Enumeration;

import javax.swing.JFrame;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.conges.CongeAccidentTravailView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageGestion;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOParamAnciennete;
import org.cocktail.mangue.modele.mangue.EODestinataire;
import org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident;
import org.cocktail.mangue.modele.mangue.conges.EOCgntAccidentTrav;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/** Gere les conges pour raison familiale ou personnelle<BR>
 * 
 *
 */

public class CongeAccidentTravailCtrl extends ModelePageGestion {

	private static CongeAccidentTravailCtrl sharedInstance;

	private EODisplayGroup eod;
	private CongeAccidentTravailView myView;
	private ListenerConges 		listenerConges = new ListenerConges();

	private EODeclarationAccident currentDeclaration;
	private EOCgntAccidentTrav currentConge;

	public CongeAccidentTravailCtrl (EOEditingContext edc) {

		super(edc);
		eod = new EODisplayGroup();
		myView = new CongeAccidentTravailView(new JFrame(), true, eod);

		setActionBoutonAjouterListener(myView.getBtnAjouter());
		setActionBoutonModifierListener(myView.getBtnModifier());
		setActionBoutonSupprimerListener(myView.getBtnSupprimer());
		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		
		myView.getBtnImprimerArrete().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {imprimerArrete();}}
				);
		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {myView.setVisible(false);}}
				);

		CocktailUtilities.initTextField(myView.getTfDateDeclaration(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeDéclaration(), false, false);
		CocktailUtilities.initTextField(myView.getTfPtDebut(), false, false);
		CocktailUtilities.initTextField(myView.getTfPtFin(), false, false);

		myView.getTfNoArrete().addActionListener(new ActionListenerNoArrete());
		myView.getTfNoArrete().addFocusListener(new FocusListenerNoArrete());

		setDateListeners(myView.getTfDebut());
		setDateListeners(myView.getTfFin());
		setDateListeners(myView.getTfDateArrete());
		setDateListeners(myView.getTfPtDebut());
		setDateListeners(myView.getTfPtFin());
		
		CocktailUtilities.initPopupAvecListe(myView.getPopupAnciennete(), EOParamAnciennete.fetchAll(getEdc()), true);
		myView.getPopupAnciennete().addActionListener(new AncienneteListener());

		myView.getMyEOTable().addListener(listenerConges);

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static CongeAccidentTravailCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new CongeAccidentTravailCtrl(editingContext);
		return sharedInstance;
	}			

	/**
	 * 
	 * @param declaration
	 */
	public void open(EODeclarationAccident declaration) {

		setSaisieEnabled(false);
		setCurrentDeclaration(declaration);
		myView.setVisible(true);

	}

	public EODeclarationAccident currentDeclaration() {
		return currentDeclaration;
	}

	public void setCurrentDeclaration(EODeclarationAccident currentDeclaration) {
		this.currentDeclaration = currentDeclaration;
		if (currentDeclaration() != null) {
			CocktailUtilities.setDateToField(myView.getTfDateDeclaration(), currentDeclaration().daccDate());
			CocktailUtilities.setTextToField(myView.getTfTypeDéclaration(),  currentDeclaration().typeAccident().libelleLong());
		}
		actualiser();
	}
	
	public EOCgntAccidentTrav getCurrentConge() {
		return currentConge;
	}
	public void setCurrentConge(EOCgntAccidentTrav currentConge) {
		this.currentConge = currentConge;
		updateDatas();
	}

	/**
	 * 
	 * @return
	 */
	private EOParamAnciennete getAnciennete() {
		if (myView.getPopupAnciennete().getSelectedIndex() > 0)
			return (EOParamAnciennete)myView.getPopupAnciennete().getSelectedItem();
		return null;
	}

	/**
	 * 
	 */
	protected void traitementsApresValidation() {

		if (getCurrentConge().absence() == null)
			getCurrentConge().setAbsenceRelationship(EOAbsences.creer(getEdc(), currentDeclaration().individu(),  
					(EOTypeAbsence)NomenclatureFinder.findForCode(getEdc(), EOTypeAbsence.ENTITY_NAME, EOTypeAbsence.TYPE_CONGE_ACC_TRAV)));

		getCurrentConge().absence().setDateDebut(getCurrentConge().dateDebut());
		getCurrentConge().absence().setDateFin(getCurrentConge().dateFin());
		getCurrentConge().absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(getCurrentConge().dateDebut(), getCurrentConge().dateFin(), true)));

	}

	/**
	 * 
	 */
	public void imprimerArrete() {

		NSArray<EODestinataire> destinatairesArrete = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinataires();
		if (destinatairesArrete  != null && destinatairesArrete.count() > 0) {
			try {				
				NSMutableArray<EOGlobalID> destinatairesGlobalIds = new NSMutableArray<EOGlobalID>();
				for (Enumeration<EODestinataire> e=destinatairesArrete.objectEnumerator();e.hasMoreElements();destinatairesGlobalIds.addObject(getEdc().globalIDForObject(e.nextElement())));
				Class[] classeParametres =  new Class[] {EOGlobalID.class,NSArray.class, Boolean.class};
				Object[] parametres = new Object[]{getEdc().globalIDForObject(getCurrentConge()),destinatairesGlobalIds, false};
				UtilitairesImpression.imprimerSansDialogue(getEdc(),"clientSideRequestImprimerArreteConge",classeParametres,parametres,"CongeAccidentTrav" + getCurrentConge().individu().noIndividu() ,"Impression de l'arrêté de congé accident travail");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
	}

	/**
	 * 
	 */
	private void majDatesPleinTraitement() {
		try {

			CocktailUtilities.setTextToField(myView.getTfPtDebut(), "");
			CocktailUtilities.setTextToField(myView.getTfPtFin(), "");

			EOParamAnciennete anciennete = (EOParamAnciennete)myView.getPopupAnciennete().getSelectedItem();			

			NSTimestamp dateDebutConge = CocktailUtilities.getDateFromField(myView.getTfDebut());
			NSTimestamp dateFinConge = CocktailUtilities.getDateFromField(myView.getTfFin());

			NSTimestamp dateFinPt = DateCtrl.dateAvecAjoutMois(dateDebutConge, anciennete.dureePleinTrait().intValue() );
			dateFinPt = DateCtrl.dateAvecAjoutJours(dateFinPt, -1 );

			if (DateCtrl.isAfter(dateFinPt, dateFinConge))
				dateFinPt = dateFinConge;

			CocktailUtilities.setDateToField(myView.getTfPtDebut(), dateDebutConge);
			CocktailUtilities.setDateToField(myView.getTfPtFin(), dateFinPt);

		}
		catch (Exception e) {
			//e.printStackTrace();
		}
	}
	private class AncienneteListener implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			majDatesPleinTraitement();
		}
	}

	private class ActionListenerNoArrete implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			updateInterface();
		}
	}
	private class FocusListenerNoArrete implements FocusListener	{
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			updateInterface();
		}
	}

	private class ListenerConges implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {
		public void onDbClick() {
			if (getCurrentConge() != null)
				modifier();
		}
		public void onSelectionChanged() {
			setCurrentConge((EOCgntAccidentTrav)eod.selectedObject());
		}
	}


	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		myView.getTfDebut().setText("");
		myView.getTfFin().setText("");
		myView.getTfDateArrete().setText("");
		myView.getTfNoArrete().setText("");
		myView.getTaCommentaires().setText("");

		myView.getTfPtDebut().setText("");
		myView.getTfPtFin().setText("");

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearDatas();

		if (getCurrentConge() != null) {

			myView.getPopupAnciennete().setSelectedItem(getCurrentConge().anciennete());

			CocktailUtilities.setDateToField(myView.getTfDebut(), getCurrentConge().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfFin(), getCurrentConge().dateFin());

			CocktailUtilities.setDateToField(myView.getTfPtDebut(), getCurrentConge().dDebPleinTrait());
			CocktailUtilities.setDateToField(myView.getTfPtFin(), getCurrentConge().dFinPleinTrait());

			CocktailUtilities.setDateToField(myView.getTfDateArrete(), getCurrentConge().dateArrete());
			CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentConge().noArrete());
			CocktailUtilities.setTextToArea(myView.getTaCommentaires(), getCurrentConge().commentaire());

		}

		updateInterface();


	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnAjouter().setEnabled(!isSaisieEnabled());
		myView.getBtnModifier().setEnabled(!isSaisieEnabled() && getCurrentConge() != null);
		myView.getBtnSupprimer().setEnabled(!isSaisieEnabled() && getCurrentConge() != null);

		myView.getBtnValider().setEnabled(isSaisieEnabled() );
		myView.getBtnAnnuler().setEnabled(isSaisieEnabled() );

		myView.getPopupAnciennete().setEnabled(isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDebut(), false, isSaisieEnabled());	
		CocktailUtilities.initTextField(myView.getTfFin(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfDateArrete(), false, isSaisieEnabled());
		CocktailUtilities.initTextField(myView.getTfNoArrete(), false, isSaisieEnabled());
		CocktailUtilities.initTextArea(myView.getTaCommentaires(), false, isSaisieEnabled());

	}

	@Override
	protected void actualiser() {
		// TODO Auto-generated method stub
		eod.setObjectArray(EOCgntAccidentTrav.rechercherPourDeclaration(getEdc(), currentDeclaration()));
		myView.getMyEOTable().updateData();

	}

	@Override
	protected void refresh() {
		// TODO Auto-generated method stub
		myView.getMyEOTable().updateUI();
		
		// Rafraichissement des onglets
		NSNotificationCenter.defaultCenter().postNotification(ListeAgents.CHANGER_EMPLOYE, getCurrentConge().individu().noIndividu(), null);

	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub

		getCurrentConge().setAncienneteRelationship(getAnciennete());

		getCurrentConge().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDebut()));
		getCurrentConge().setDateFin(CocktailUtilities.getDateFromField(myView.getTfFin()));
		getCurrentConge().setDateArrete(CocktailUtilities.getDateFromField(myView.getTfDateArrete()));
		getCurrentConge().setNoArrete(CocktailUtilities.getTextFromField(myView.getTfNoArrete()));

		getCurrentConge().setDDebPleinTrait(CocktailUtilities.getDateFromField(myView.getTfPtDebut()));
		getCurrentConge().setDFinPleinTrait(CocktailUtilities.getDateFromField(myView.getTfPtFin()));

		getCurrentConge().setCommentaire(CocktailUtilities.getTextFromArea(myView.getTaCommentaires()));

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		listenerConges.onSelectionChanged();
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub
		majDatesPleinTraitement();
	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub
		setCurrentConge(EOCgntAccidentTrav.creer(getEdc(), currentDeclaration()));

	}

	@Override
	protected void traitementsPourModification() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourSuppression() {
		// TODO Auto-generated method stub
		getCurrentConge().absence().setEstValide(false);
		getCurrentConge().setTemValide(CocktailConstantes.FAUX);

	}

	@Override
	protected void traitementsApresSuppression() {
		// TODO Auto-generated method stub
		// Rafraichissement des onglets
		NSNotificationCenter.defaultCenter().postNotification(ListeAgents.CHANGER_EMPLOYE, getCurrentConge().individu().noIndividu(), null);

	}
}
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import org.cocktail.client.components.ArchiveWithDisplayGroup;
import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.mangue.client.outils_interface.GestionTypeConge;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArrete;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecArreteAnnulation;
import org.cocktail.mangue.modele.mangue.conges.CongeAvecRemiseEnCause;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;

// 28/01/2011 - Adaptation Netbeans
public class InspecteurAbsence extends ArchiveWithDisplayGroup {
	private String nomEntiteAffichee;
	private Conge congeCourant;
	public EOTable listeAffichage;
	
	public InspecteurAbsence(EOEditingContext substitutionEditingContext,String nomEntite) {
  		super(substitutionEditingContext,"InspecteurAbsence","org.cocktail.mangue.client.conges.interfaces");
  		this.nomEntiteAffichee = nomEntite;
  		congeCourant = null;
	}
	public void init() {
		super.init();
		GraphicUtilities.changerTaillePolice(listeAffichage,11);
		GraphicUtilities.rendreNonEditable(listeAffichage);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getEntiteCourante", new Class[] {NSNotification.class}),GestionTypeConge.TYPE_CONGE_HAS_CHANGED,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("getAbsence", new Class[] {NSNotification.class}),GestionAbsences.CHANGER_ABSENCE,null);
  	}
	public void terminer() {
		super.terminer();
		NSNotificationCenter.defaultCenter().removeObserver(this);
	}	
	// Notifications
	public void getEntiteCourante(NSNotification aNotif) {
		nomEntiteAffichee = (String)aNotif.object();
	}
	public void getAbsence(NSNotification aNotif) {
  		if (aNotif.object() == null) {
  			congeCourant = null;
  		} else if (nomEntiteAffichee != null && nomEntiteAffichee.length() > 0) {
  				congeCourant = Conge.rechercherCongeAvecAbsence(editingContext,nomEntiteAffichee,(EOAbsences)aNotif.object());
  		} else {
  			congeCourant = null;
  		}
  		if (congeCourant == null) {
  			displayGroup().setSelectedObject(null);
  			displayGroup().setObjectArray(null);
  		} else {
  			CongePourInspecteur conge = new CongePourInspecteur(congeCourant);
  			displayGroup().setObjectArray(new NSArray(conge));
  			displayGroup().selectObject(conge);
  		}
  		displayGroup().updateDisplayedObjects();
  		controllerDisplayGroup().redisplay();
	}
	// 28/01/2011 - pour pouvoir afficher dans la table
	public class CongePourInspecteur implements NSKeyValueCoding {
		private String noArrete, noArreteAnnulation, temConfirme, temEnCause;
		private NSTimestamp dateArrete, dAnnulation;
		
		public CongePourInspecteur(Conge conge) {
			noArrete = "-";
			if (conge instanceof CongeAvecArrete) {
				noArrete = ((CongeAvecArrete) congeCourant).noArrete();
				dateArrete = ((CongeAvecArrete) congeCourant).dateArrete();
			} 
			noArreteAnnulation = "-";
			if (congeCourant instanceof CongeAvecArreteAnnulation) {
				noArreteAnnulation = ((CongeAvecArreteAnnulation) congeCourant).noArreteAnnulation();
				dAnnulation = ((CongeAvecArreteAnnulation) congeCourant).dAnnulation();
				temConfirme = ((CongeAvecArreteAnnulation) congeCourant).temConfirme();
			}
			temEnCause = "-";
			if (congeCourant instanceof CongeAvecRemiseEnCause) {
				temEnCause = ((CongeAvecRemiseEnCause) congeCourant).temEnCause();
			}
		}
		// Accesseurs
		public String noArrete() {
			return noArrete;
		}
		public NSTimestamp dateArrete() {
			return dateArrete;
		}
		public String noArreteAnnulation() {
			return noArreteAnnulation;
		}
		public NSTimestamp dAnnulation() {
			return dAnnulation;
		}
		public String temConfirme() {
			return temConfirme;
		}
		public String temEnCause() {
			return temEnCause;
		}
		//	 interface keyValueCoding
		public void takeValueForKey(Object valeur,String cle) {
			NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
		}
		public Object valueForKey(String cle) {
			return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
		}
		
	}
}

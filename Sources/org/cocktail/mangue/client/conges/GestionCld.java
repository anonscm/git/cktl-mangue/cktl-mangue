/*
 * Created on 14 sept. 2005
 *
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.client.conges;

import org.cocktail.client.components.utilities.GraphicUtilities;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.outils_interface.GestionCongeAvecArrete;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.conges.DetailConge;
import org.cocktail.mangue.modele.mangue.conges.EOCld;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
import org.cocktail.mangue.modele.mangue.individu.EOStage;

import com.webobjects.eoapplication.EOArchive;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

/**  Gestion des Clds et des detail de remuneration<BR>
 * Ces derniers ne peuvent etre saisis que lorsque la date de debut et de fin du conge sont saisis<BR>
 * Propose automatiquement les dates de debut et de fin d'un detail
 * Gestion des requalifications des conges de longue maladie et de maladie. Voir description dans la classe RequalificationConges<BR>
 * Gestion des occupations<BR>
 *<UL>Si l'agent a une occupation a l'interieur de la periode de conge, on la supprime</UL>
 *<UL>Si l'occupation se termine avant la fin du conge, on la termine le jour precedent le debut du conge</UL>
 *<UL>Si l'occupation debute avant la fin du conge, on la fait debuter le jour suivant la fin du conge</UL>
 *<UL>Si l'occupation a lieu pendant toute la duree du conge, on cree une nouvelle occupation qui commence a l'ancienne date de debut
 * et se termine le jour precedent le debut du conge et on modifie l'occupation pour qu'elle debute le jour suivant la fin du conge</UL>
 **/
//27/01/2011 - Adaptation Netbeans
public class GestionCld extends GestionCongeAvecArrete {
	public EODisplayGroup displayGroupDetail;
	public EOTable tableDetail;

	public GestionCld() {
		super("Cld","Gestion des congés longue durée");
	}
	public void connectionWasEstablished() {
		super.connectionWasEstablished();
		// afficher le détail de rémunération par ordre de début croissant
		displayGroupDetail.setSortOrderings(new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));
		GraphicUtilities.changerTaillePolice(tableDetail,11);
		GraphicUtilities.rendreNonEditable(tableDetail);
	}
	//	méthodes de délégation du display group
	public void displayGroupDidChangeSelection(EODisplayGroup aGroup) {
		if (controllerDisplayGroup != null) {
			controllerDisplayGroup().redisplay();
		}
	}
	// actions
	public void ajouterDetail() {
		DetailConge dernierDetail = (DetailConge)displayGroupDetail.displayedObjects().lastObject();
		displayGroupDetail.insert();
		NSTimestamp dateDebut = currentConge().dateDebut();
		if (dernierDetail != null) {
			dateDebut = DateCtrl.jourSuivant(dernierDetail.dateFin());
		}
		currentDetail().initAvecConge(currentConge(),dateDebut,null);
	}
	public void supprimerDetail() {
		DetailConge detail = currentDetail();
		displayGroupDetail.selectPrevious();
		detail.supprimerRelations();
		editingContext().deleteObject(detail);
		if (currentDetail() != null) {
			currentDetail().setDateFin(currentConge().dateFin());
		}
		displayGroupDetail.updateDisplayedObjects();
		controllerDisplayGroup().redisplay();
	}
	// méthodes du controller dg
	/** si des details ont ete saisis, la date de fin du dernier detail doit correspondre a la date de fin du conge */
	public boolean peutValider() {
		if (super.peutValider()) {
			DetailConge dernierDetail = (DetailConge)displayGroupDetail.displayedObjects().lastObject();
			return dernierDetail == null || (dernierDetail.dateFin() != null && DateCtrl.isSameDay(dernierDetail.dateFin(),currentConge().dateFin()));
		} else {
			return false;
		}
	}
	/** on ne peut modifier les dates d'un cld que si aucun detail n'est saisi */
	public boolean peutModifierDate() {
		return currentConge() != null && currentConge().details().count() == 0;
	}
	/** on ne peut prolonger un cld que si il existe un conge se terminant le jour precedent le jour de debut */
	public boolean peutProlonger() {
		if (currentConge() == null || currentConge().dateDebut() == null) {
			return false;
		}
		NSArray autresClds = Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(),"Cld",currentConge().individu(),currentConge().dateDebut());
		if (autresClds.count() == 0) {
			return false;
		}
		// trier par ordre décroissant
		autresClds = EOSortOrdering.sortedArrayUsingKeyOrderArray(autresClds,new NSArray(EOSortOrdering.sortOrderingWithKey("dateFin",EOSortOrdering.CompareDescending)));
		EOCld cld = (EOCld)autresClds.objectAtIndex(0);
		return DateCtrl.isSameDay(DateCtrl.jourSuivant(cld.dateFin()),currentConge().dateDebut());
	}
	/** on ne peut ajouter un detail que si la date debut et la date de fin sont fournies et que la date de fin du detail
	 * est anterieure a la date de fin du conge */
	public boolean peutAjouterDetail() {
		if (super.peutValider()) {
			DetailConge dernierDetail = (DetailConge)displayGroupDetail.displayedObjects().lastObject();
			return dernierDetail == null || (dernierDetail.dateFin() != null && DateCtrl.isBefore(dernierDetail.dateFin(),currentConge().dateFin()));
		} else {
			return false;
		}
	}
	public boolean peutModifierDetail() {
		return currentDetail() != null && currentDetail() == displayGroupDetail.displayedObjects().lastObject();
	}
	public boolean peutModifierQuotite() {
		return currentDetail() != null;
	}
	// methodes protegees
	/** methode surchargee par les sous-classes pour charger l'archive ad-hoc */
	protected  void chargerArchive() {
		EOArchive.loadArchiveNamed("GestionCld",this,"org.cocktail.mangue.client.conges.interfaces",this.disposableRegistry());
		GraphicUtilities.preparerInterface(new NSArray(component().getComponents()));
	}

	/** requalification eventuelle des conges de longue maladie ou de maladie */
	protected boolean traitementsAvantValidation() {
		NSArray stages = EOStage.findForIndividuEtPeriode(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin(),false);
		if (stages != null && stages.count() > 0) {
			EODialogs.runInformationDialog("Attention","L'agent est en stage pendant cette période, pensez à modifier la date de la fin de stage");	        		
		}
		//		if (typeTraitement() == MODE_CREATION) {
		//			try {
		//				EOPosition position = EOPosition.rechercherPositionPourType(editingContext(),EOPosition.EN_ACTIVITE);
		//				EOChangementPosition.insererEtEnregistrerChangementPourDatesIndividuEtPosition(editingContext, currentConge().dateDebut(), currentConge().dateFin(),currentConge().individu(), position);
		//			} catch (Exception e) {
		//				EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite\n" + e.getMessage() + "\nVeuillez créer le changement de position");
		//			}
		//		} else {
		//			EODialogs.runInformationDialog("Attention","Pensez à revoir les changements de position");
		//		}

		if (super.traitementsAvantValidation()) {
			if (RequalificationConges.requalifierCongesAssocies(currentConge(),EOClm.ENTITY_NAME,"longue durée","longue maladie")) {
				return RequalificationConges.requalifierCongesAssocies(currentConge(),"CongeMaladie","longue durée","maladie");
			} else {
				return false;
			}
		} else {
			return false;
		}

	}
	protected void traitementsApresValidation() {

		try {

			NSArray occupations = EOOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContext(),currentConge().individu(),currentConge().dateDebut(),currentConge().dateFin());
			if (occupations.count() == 0 ||
					EODialogs.runConfirmOperationDialog("Attention","Les occupations vont être modifiées. Voulez-vous continuer ?","Oui","Non") == false)
				return;

			for (java.util.Enumeration<EOOccupation> e = occupations.objectEnumerator();e.hasMoreElements();) {
				EOOccupation occupation = e.nextElement();

				// Si l'occupation est contenue dans toute la duree du conge,on la supprime
				if (DateCtrl.isBetween(occupation.dateDebut(), occupation.dateFin(), currentConge().dateDebut(), currentConge().dateFin()))
					editingContext().deleteObject(occupation);
				else {
					// Chevauchement de periode, on cree une nouvelle occupation
					if (DateCtrl.chevauchementPeriode(occupation.dateDebut(), occupation.dateFin(), currentConge().dateDebut(), currentConge().dateFin())) {

						// Si l'occupation debute avant le conge, on la termine le jour precedent le debut de conge et on 
						// en cree une nouvelle si la fin de l'occupation est posterieure au conge
						if (DateCtrl.isBefore(occupation.dateDebut(), currentConge().dateDebut())) {
							if (occupation.dateFin() == null || DateCtrl.isAfter(occupation.dateFin(), currentConge().dateFin())) {
								EOOccupation nouvelleOccupation = EOOccupation.creer(editingContext(), occupation.individu());
								nouvelleOccupation.takeValuesFromDictionary(occupation.snapshot());
								nouvelleOccupation.setDateDebut(DateCtrl.jourSuivant(currentConge().dateFin()));
								nouvelleOccupation.setDateFin(occupation.dateFin());
							}
							occupation.setDateFin(DateCtrl.jourPrecedent(currentConge().dateDebut()));
						}
						else {	// L'occupation se termine apres le conge
							if (DateCtrl.isAfter(occupation.dateDebut(), currentConge().dateDebut())) {
								occupation.setDateDebut(DateCtrl.jourSuivant(currentConge().dateFin()));
							}						
						}
					}				
				}

			}
			editingContext().saveChanges();
		} 
		catch (ValidationException e) {
			EODialogs.runErrorDialog("Erreur",e.getMessage());	
		}
		catch (Exception ex) {
			LogManager.logException(ex);
			EODialogs.runErrorDialog("Erreur","Un problème est survenu pendant la fermeture des occupations. Veuillez revoir les dates des occupations");
			editingContext().revert();
		}
	}
	
	protected boolean traitementsAvantSuppression() {
		RequalificationConges.annulerQualificationCongesAssocies(currentConge(),"Clm");
		RequalificationConges.annulerQualificationCongesAssocies(currentConge(),"CongeMaladie");
		currentConge().supprimerRelations();
		return true;
	}
	protected void traitementsApresSuppression() {
		EODialogs.runInformationDialog("Attention","Pensez à revoir les dates de fin d'occupation des emplois et les changements de position");
	}
	protected EOTypeGestionArrete typeGestionArrete() {
		if (currentConge() != null && currentConge().individu() != null) {
			return currentConge().individu().rechercherTypeGestionArretePourPeriode(null,"CLD",currentConge().dateDebut(),currentConge().dateFin());
		} else {
			return null;
		}

	}

	protected boolean supporteSignature() {
		return false;
	}

	// methodes privees
	private EOCld currentConge() {
		return (EOCld)displayGroup().selectedObject();
	}
	private DetailConge currentDetail() {
		return (DetailConge)displayGroupDetail.selectedObject();
	}
}

package org.cocktail.mangue.client.vacations;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.util.Enumeration;

import javax.swing.JFrame;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.client.contrats.EmployeursCtrl;
import org.cocktail.mangue.client.gui.vacations.SaisieVacationView;
import org.cocktail.mangue.client.select.CorpsSelectCtrl;
import org.cocktail.mangue.client.select.GradeSelectCtrl;
import org.cocktail.mangue.client.select.NomenclatureSelectCodeLibelleCtrl;
import org.cocktail.mangue.client.select.ProfessionSelectCtrl;
import org.cocktail.mangue.client.select.SaisieIndividuCtrl;
import org.cocktail.mangue.client.select.TypeContratListeSelectCtrl;
import org.cocktail.mangue.client.select.TypeContratArbreSelectCtrl;
import org.cocktail.mangue.client.select.UAISelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOCommune;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.EOProfession;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;
import org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation.ValidationException;

public class SaisieVacationCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;
	private static SaisieVacationCtrl sharedInstance;
	private SaisieVacationView myView;
	private TypeContratListeSelectCtrl myTypeContratSelectCtrl;

	private EmployeursCtrl 	employeursCtrl = null;
	private EOIndividu 		currentIndividu;
	private EOVacataires 	currentVacation;
	private EOAdresse 		currentAdresseVacation;
	private EOCorps 		currentCorps;
	private EOGrade			currentGrade;
	private EORne 			currentUai;
	private EOCnu 			currentCnu;
	private EOProfession 	currentProfession;
	private EOTypeContratTravail	currentTypeContrat;

	private UAISelectCtrl 	myUAISelectCtrl;

	public SaisieVacationCtrl(EOEditingContext edc) {

		super(edc);

		myView = new SaisieVacationView(new JFrame(), true);

		myView.getBtnValider().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {valider();}}
				);
		myView.getBtnAnnuler().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {annuler();}}
				);
		myView.getBtnGetCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectCorps();}}
				);
		myView.getBtnGetGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectGrade();}}
				);
		myView.getBtnDelCorps().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delCorps();}}
				);
		myView.getBtnDelGrade().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delGrade();}}
				);
		myView.getBtnDelAdresse().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delAdresseVacation();}}
				);
		myView.getBtnDelCnu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delCnu();}}
				);
		myView.getBtnGetCnu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectCnu();}}
				);
		myView.getBtnDelProfession().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delProfession();}}
				);
		myView.getBtnGetProfession().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectProfession();}}
				);
		myView.getBtnGetUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectUai();}}
				);
		myView.getBtnDelUai().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delUai();}}
				);

		myView.getBtnSelectTypeContrat().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectTypeContrat();}}
				);
		myView.getBtnDelTypeContrat().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delTypeContrat();}}
				);

		myView.getTfCodePostal().addFocusListener(new ListenerTextFieldCodePostal());
		myView.getTfCodePostal().addActionListener(new ActionListenerCodePostal());

		CocktailUtilities.initTextField(myView.getTfLibelleCorps(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleGrade(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleProfession(), false, false);
		CocktailUtilities.initTextField(myView.getTfIdentite(), false, false);
		CocktailUtilities.initTextField(myView.getTfCodeCnu(), false, false);
		CocktailUtilities.initTextField(myView.getTfLibelleCnu(), false, false);
		CocktailUtilities.initTextField(myView.getTfUai(), false, false);
		CocktailUtilities.initTextField(myView.getTfTypeContrat(), false, false);

		setDateListeners(myView.getTfDateDebut());
		setDateListeners(myView.getTfDateFin());

		employeursCtrl = new EmployeursCtrl(edc, "EMPLOYEUR PRINCIPAL");
		employeursCtrl.setClasseParent(getClass().getName());
		myView.getSwapViewEmployeurs().add("EMPLOYEURS",employeursCtrl.getView());

		myView.getCheckTitulaire().addActionListener(new ActionListenerTemoins());
		myView.getCheckEnseignant().addActionListener(new ActionListenerTemoins());
		myView.getCheckPersEtab().addActionListener(new ActionListenerTemoins());

		myView.getCheckTitulaire().setSelected(false);
		myView.getCheckAutorisationCumul().setSelected(false);
		myView.getCheckEnseignant().setSelected(false);
		myView.getCheckPaiementPonctuel().setSelected(true);

		myView.getViewTypeContrat().setVisible(EOGrhumParametres.isTypeContratVacataire());
		//myView.getViewAdresseVacation().setVisible(false);
		updateInterface();


	}

	public static SaisieVacationCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieVacationCtrl(editingContext);
		return sharedInstance;
	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}


	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		if (currentIndividu != null) {
			String identite = currentIndividu.cCivilite() + " " +  currentIndividu.identitePrenomFirst();
			identite += "  ( Sécu : " + currentIndividu.indNoInsee();
			if (currentIndividu.personnel() != null && currentIndividu.personnel().noMatricule() != null)
				identite += "  , Matricule : " + currentIndividu.personnel().noMatricule();
			identite += " )";
			CocktailUtilities.setTextToField(myView.getTfIdentite(), identite);
		}
	}

	public EOVacataires getCurrentVacation() {
		return currentVacation;
	}
	public void setCurrentVacation(EOVacataires currentVacation) {
		this.currentVacation = currentVacation;
		updateDatas();
	}

	public EOAdresse getCurrentAdresseVacation() {
		return currentAdresseVacation;
	}
	public void setCurrentAdresseVacation(EOAdresse currentAdresseVacation) {
		this.currentAdresseVacation = currentAdresseVacation;
	}

	public EOCorps getCurrentCorps() {
		return currentCorps;
	}
	public void setCurrentCorps(EOCorps currentCorps) {
		this.currentCorps = currentCorps;
		CocktailUtilities.viderTextField(myView.getTfLibelleCorps());
		getCurrentVacation().setToCorpsRelationship(currentCorps);
		getCurrentVacation().setToGradeRelationship(null);
		if (currentCorps != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleCorps(), currentCorps.llCorps());
			CocktailUtilities.viderTextField(myView.getTfLibelleGrade());
			updateInterface();			
		}
	}

	public EOGrade getCurrentGrade() {
		return currentGrade;
	}
	public void setCurrentGrade(EOGrade currentGrade) {
		this.currentGrade = currentGrade;
		CocktailUtilities.viderTextField(myView.getTfLibelleGrade());
		if (currentGrade != null) {

			CocktailUtilities.setTextToField(myView.getTfLibelleGrade(), currentGrade.llGrade());
			if (getCurrentCorps() == null)
				setCurrentCorps(currentGrade.toCorps());

			getCurrentVacation().setToGradeRelationship(currentGrade);
			myView.getTfLibelleGrade().setText(getCurrentVacation().toGrade().llGrade());
			updateInterface();

		}
	}



	public EOCnu getCurrentCnu() {
		return currentCnu;
	}
	public void setCurrentCnu(EOCnu currentCnu) {
		this.currentCnu = currentCnu;
		CocktailUtilities.viderTextField(myView.getTfCodeCnu());
		CocktailUtilities.viderTextField(myView.getTfLibelleCnu());
		if (currentCnu != null) {
			CocktailUtilities.setTextToField(myView.getTfCodeCnu(), currentCnu.code());
			CocktailUtilities.setTextToField(myView.getTfLibelleCnu(), currentCnu.libelleLong());
		}
	}

	public EOProfession getCurrentProfession() {
		return currentProfession;
	}
	public void setCurrentProfession(EOProfession currentProfession) {
		this.currentProfession = currentProfession;
		CocktailUtilities.viderTextField(myView.getTfLibelleProfession());
		if (currentProfession != null) {
			CocktailUtilities.setTextToField(myView.getTfLibelleProfession(), currentProfession.libelle());
		}
	}

	public EORne getCurrentUai() {
		return currentUai;
	}

	public void setCurrentUai(EORne currentUai) {
		this.currentUai = currentUai;
		CocktailUtilities.viderTextField(myView.getTfUai());
		if (currentUai != null)
			CocktailUtilities.setTextToField(myView.getTfUai(), currentUai.codeEtLibelle());
	}


	public EOTypeContratTravail getCurrentTypeContrat() {
		return currentTypeContrat;
	}

	public void setCurrentTypeContrat(EOTypeContratTravail currentTypeContrat) {
		this.currentTypeContrat = currentTypeContrat;
		CocktailUtilities.viderTextField(myView.getTfTypeContrat());
		if (currentTypeContrat != null)
			CocktailUtilities.setTextToField(myView.getTfTypeContrat(), currentTypeContrat.code() + " - " + currentTypeContrat.libelleLong());

	}


	private void clearAdresseVacation()	{

		CocktailUtilities.viderTextField(myView.getTfAdresse());
		CocktailUtilities.viderTextField(myView.getTfComplement());
		CocktailUtilities.viderTextField(myView.getTfCodePostal());
		myView.getPopupVilles().removeAllItems();

	}

	/**
	 * 
	 * @param individu
	 * @return
	 */
	public EOVacataires ajouter(EOIndividu individu)	{

		SaisieIndividuCtrl saisieCtrl = new SaisieIndividuCtrl(getEdc());

		myView.setTitle("Vacataires / AJOUT");
		clearDatas();
		((CardLayout)myView.getSwapViewEmployeurs().getLayout()).show(myView.getSwapViewEmployeurs(), "EMPLOYEURS");				

		EOIndividu selectedIndividu = individu;
		if (individu == null) {
			selectedIndividu = saisieCtrl.getIndividu(true, false);

			if (saisieCtrl.isNouvelIndividu()) {
				SaisieIdentiteVacataireCtrl.sharedInstance(getEdc()).modifier(selectedIndividu);	
			}
		}

		if (selectedIndividu != null) {

			setCurrentIndividu(selectedIndividu);
			setCurrentVacation(EOVacataires.creer(getEdc(), ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel() , getCurrentIndividu()));

			setCurrentUai((EORne)NomenclatureFinder.findForCode(getEdc(), EORne.ENTITY_NAME, EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE)));

			// Verifier si l'agent a une carriere ou un contrat dans l etablissement
			boolean isPersonnelEtablissement = false;

			if (!saisieCtrl.isNouvelIndividu()) {
				EOElementCarriere element = EOElementCarriere.rechercherElementADate(getEdc(), selectedIndividu, DateCtrl.today());
				if (element != null) {
					setCurrentGrade(element.toGrade());
					myView.getCheckTitulaire().setSelected(true);
					isPersonnelEtablissement = true;
					myView.getCheckEnseignant().setSelected(getCurrentCorps().toTypePopulation().estEnseignant());
				}
				else {
					EOContratAvenant avenant = EOContratAvenant.rechercherAvenantADate(getEdc(), selectedIndividu, DateCtrl.today());
					if (avenant != null) {
						setCurrentGrade(avenant.toGrade());
						myView.getCheckTitulaire().setSelected(false);
						isPersonnelEtablissement = true;
						myView.getCheckEnseignant().setSelected(avenant.contrat().toTypeContratTravail().estEnseignant());
					}
					else {
						// Mise a jour des temoins Enseignant et Titulaire
						myView.getCheckTitulaire().setSelected(false);
						myView.getCheckEnseignant().setSelected(false);
					}
				}
			}

			myView.getCheckPersEtab().setSelected(isPersonnelEtablissement);

			employeursCtrl.setSaisieEnabled(true);

			myView.setVisible(true);	// Lancement en mode MODAL

			return getCurrentVacation();

		}

		return null;
	}

	/**
	 * 
	 * @param vacation
	 * @return
	 */
	public EOVacataires renouveler(EOVacataires vacation) {

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		clearDatas();

		setCurrentVacation(EOVacataires.dupliquer(getEdc(), vacation, ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel()));
		setCurrentIndividu(vacation.toIndividu());
		updateDatas();

		myView.setVisible(true);		

		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

		return getCurrentVacation();
	}

	/**
	 * 
	 * @param vacation
	 * @return
	 */
	public boolean modifier(EOVacataires vacation) {

		myView.setTitle("Vacataires - MODIFICATION - " + vacation.toIndividu().identitePrenomFirst());

		setCurrentIndividu(vacation.toIndividu());

		clearDatas();

		((CardLayout)myView.getSwapViewEmployeurs().getLayout()).show(myView.getSwapViewEmployeurs(), "EMPLOYEURS");				

		setCurrentVacation(vacation);
		employeursCtrl.setSaisieEnabled(true);

		myView.setVisible(true);

		return getCurrentVacation() != null;
	}

	/**
	 * 
	 * @return
	 */
	public String getCurrentVille() {

		if (myView.getPopupVilles().getItemCount() > 0)
			return (String)myView.getPopupVilles().getSelectedItem();

		return null;
	}

	/**
	 * Si l'agent n'est QUE vacataires (Ni contrat Ni carriere) modifier le IND_ACTIVITE de la table Individu_ulr
	 * @throws ValidationException
	 */
	protected void traitementsAvantValidation() {

		EOStructure selectedStructure = employeursCtrl.getSelectedStructure();

		getCurrentVacation().setToCnuRelationship(getCurrentCnu());
		getCurrentVacation().setToProfessionRelationship(getCurrentProfession());
		getCurrentVacation().setToStructureRelationship(selectedStructure);
		getCurrentVacation().setToUaiRelationship(getCurrentUai());
		getCurrentVacation().setToTypeContratTravailRelationship(getCurrentTypeContrat());

		getCurrentVacation().setDateDebut(CocktailUtilities.getDateFromField(myView.getTfDateDebut()));
		getCurrentVacation().setDateFin(CocktailUtilities.getDateFromField(myView.getTfDateFin()));

		getCurrentVacation().setObservations(CocktailUtilities.getTextFromField(myView.getTfObservations()));
		getCurrentVacation().setEnseignement(CocktailUtilities.getTextFromField(myView.getTfEnseignement()));
		getCurrentVacation().setNoArrete(CocktailUtilities.getTextFromField(myView.getTfNoArrete()));
		getCurrentVacation().setNbrHeures(CocktailUtilities.getBigDecimalFromField(myView.getTfHeures()));
		getCurrentVacation().setNbrHeuresRealisees(CocktailUtilities.getBigDecimalFromField(myView.getTfHeuresRealisees()));
		getCurrentVacation().setTauxHoraire(CocktailUtilities.getBigDecimalFromField(myView.getTfTauxHoraire()));
		getCurrentVacation().setTauxHoraireRealise(CocktailUtilities.getBigDecimalFromField(myView.getTfTauxHoraireRealise()));

		getCurrentVacation().setEstSigne(myView.getCheckSigne().isSelected());
		getCurrentVacation().setEstAutorisationCumul(myView.getCheckAutorisationCumul().isSelected());
		getCurrentVacation().setEstEnseignant(myView.getCheckEnseignant().isSelected());
		getCurrentVacation().setEstTitulaire(myView.getCheckTitulaire().isSelected());
		getCurrentVacation().setPaiementPonctuel(myView.getCheckPaiementPonctuel().isSelected());
		getCurrentVacation().setEstPersonnelEtablissement(myView.getCheckPersEtab().isSelected());
		getCurrentVacation().setEstDepassementAuto(myView.getCheckDepassementAuto().isSelected());

		// Enregistrement de l'adresse
		if (getCurrentAdresseVacation() == null) {

			if (myView.getTfCodePostal().getText().length() > 0 && myView.getPopupVilles().getItemCount() > 0) {

				// Creation de l'adresse
				EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
				setCurrentAdresseVacation(new EOAdresse());

				//currentAdresseVacation.setAdrOrdre(new Integer(EOAdresse.construireAdresseOrdre(ec).intValue()));
				getCurrentAdresseVacation().setPersIdCreation(agent.toIndividu().persId());
				getCurrentAdresseVacation().setPersIdModification(agent.toIndividu().persId());
				getCurrentAdresseVacation().init();
				getEdc().insertObject(getCurrentAdresseVacation());					
			}								
		}

		if (getCurrentAdresseVacation() != null) {

			getCurrentAdresseVacation().setAdrAdresse1(CocktailUtilities.getTextFromField(myView.getTfAdresse()));
			getCurrentAdresseVacation().setAdrAdresse2(CocktailUtilities.getTextFromField(myView.getTfComplement()));
			getCurrentAdresseVacation().setCodePostal(CocktailUtilities.getTextFromField(myView.getTfCodePostal()));

			getCurrentAdresseVacation().setVille(getCurrentVille());

			getCurrentAdresseVacation().setToPaysRelationship(EOPays.getDefault(getEdc()));

			getCurrentVacation().setToAdresseRelationship(getCurrentAdresseVacation());

		}

		// Mise a jour de la notion d'ACTIVITE pour l'indiviud
		if (getCurrentVacation().dateDebut() != null) {

			EOContratAvenant contrat = EOContratAvenant.rechercherAvenantPourIndividuADate(getEdc(), getCurrentIndividu() , getCurrentVacation().dateDebut());
			if (contrat  == null) {
				EOCarriere carriere = EOCarriere.rechercherCarriereADate(getEdc(), getCurrentIndividu() , getCurrentVacation().dateDebut());		
				// L'agent n'est ni contractuel ni titulaire à la date de la vacation
				if (carriere == null) {
					getCurrentIndividu().setIndActivite(ManGUEConstantes.IND_ACTIVITE_VACATION);
				}
			}
		}

	}

	/**
	 * 
	 * @throws ValidationException
	 */
	protected void traitementsApresValidation()  {

		try {
			NSArray<EOVacatairesAffectation> affectations  = EOVacatairesAffectation.fetchForVacation(getEdc(), getCurrentVacation());
			if (affectations.count() == 1)
				affectations.objectAtIndex(0).setNbrHeures(getCurrentVacation().nbrHeures());
			getEdc().saveChanges();
		}
		catch (Exception e){
			e.printStackTrace();
		}

		myView.setVisible(false);

	}

	private void selectCorps() {

		CRICursor.setWaitCursor(myView);
		EOCorps corps = CorpsSelectCtrl.sharedInstance(getEdc()).getCorps();

		if (corps != null)	{
			setCurrentCorps(corps);
		}

		CRICursor.setDefaultCursor(myView);
	}
	private void delCorps() {
		setCurrentCorps(null);
	}
	private void selectGrade() {
		EOGrade grade = GradeSelectCtrl.sharedInstance(getEdc()).getGradePourCorps(getCurrentVacation().toCorps(), getCurrentVacation().dateDebut());
		if (grade != null) {
			setCurrentGrade(grade);
		}
	}
	private void delGrade() {
		setCurrentGrade(null);
	}

	private void selectCnu() {

		EOCnu cnu = (EOCnu)NomenclatureSelectCodeLibelleCtrl.sharedInstance(getEdc()).getObject(NomenclatureFinder.find(getEdc(), EOCnu.ENTITY_NAME, Nomenclature.SORT_ARRAY_CODE));
		if (cnu  != null)
			setCurrentCnu(cnu);
		updateInterface();

	}
	private void delCnu() {
		setCurrentCnu(null);
	}

	/**
	 * 
	 */
	private void selectTypeContrat() {

		if (myTypeContratSelectCtrl == null)
			myTypeContratSelectCtrl = new TypeContratListeSelectCtrl(getEdc());
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		EOQualifier qualifier = EOTypeContratTravail.getQualifierVacataires(true);
		EOTypeContratTravail typeContrat = (EOTypeContratTravail)myTypeContratSelectCtrl.getObject(DateCtrl.today(), qualifier, false);

		if (typeContrat != null) {
			setCurrentTypeContrat(typeContrat);
		}

		updateInterface();

	}
	private void delTypeContrat() {
		setCurrentTypeContrat(null);
	}



	private void selectProfession() {

		EOProfession profession = ProfessionSelectCtrl.sharedInstance(getEdc()).getProfession();
		if (profession != null) {
			setCurrentProfession(profession);
		}
	}
	private void delProfession() {
		setCurrentProfession(null);
	}


	private void delAdresseVacation() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Confirmez-vous la suppression de l'adresse de la vacation ?", "Oui", "Non"))		
			return;			

		getCurrentVacation().setToAdresseRelationship(null);
		setCurrentAdresseVacation(null);

		clearAdresseVacation();
		updateInterface();
	}

	private void selectUai() {

		if (myUAISelectCtrl == null)
			myUAISelectCtrl = new UAISelectCtrl(getEdc());
		EORne rne = (EORne)myUAISelectCtrl.getObject();
		if (rne != null)
			setCurrentUai(rne);
		updateInterface();
	}
	private void delUai() {
		setCurrentUai(null);
		updateInterface();
	}

	public void getCommune()	{

		NSArray<EOCommune> communes = EOCommune.rechercherCommunes(getEdc(), myView.getTfCodePostal().getText());

		myView.getPopupVilles().removeAllItems();
		for (Enumeration<EOCommune> e =  communes.objectEnumerator();e.hasMoreElements();)
			myView.getPopupVilles().addItem(e.nextElement().libelle());
	}

	public class ActionListenerCodePostal implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			//if (!temSaisieAutomatique.isSelected())	return;
			getCommune();
		}
	}

	private boolean estEnseignant() {
		return myView.getCheckEnseignant().isSelected();
	}
	private boolean estTitulaire() {
		return myView.getCheckTitulaire().isSelected();
	}
	private boolean estPersonnelEtablissement() {
		return myView.getCheckPersEtab().isSelected();
	}


	public class ActionListenerTemoins implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			if (estEnseignant() == false || estTitulaire() == false) {
				setCurrentCnu(null);
			}
			if (estPersonnelEtablissement() == false || estTitulaire() == false) {
				myView.getCheckAutorisationCumul().setEnabled(false);
			}
			updateInterface();
		}
	}

	/** 
	 * Classe d'ecoute sur le debut de contrat de travail.
	 * Permet d'effectuer la completion de cette date 
	 */
	public class ListenerTextFieldCodePostal implements  java.awt.event.FocusListener	{
		public void focusGained(FocusEvent e)		{
		}

		public void focusLost(FocusEvent e)	{
			getCommune();
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub
		CocktailUtilities.viderTextField(myView.getTfUai());
		CocktailUtilities.viderTextField(myView.getTfLibelleCorps());
		CocktailUtilities.viderTextField(myView.getTfLibelleGrade());
		CocktailUtilities.viderTextField(myView.getTfLibelleProfession());
		CocktailUtilities.viderTextField(myView.getTfEnseignement());
		CocktailUtilities.viderTextField(myView.getTfObservations());
		CocktailUtilities.viderTextField(myView.getTfHeures());
		CocktailUtilities.viderTextField(myView.getTfTauxHoraire());
		CocktailUtilities.viderTextField(myView.getTfCodeCnu());
		CocktailUtilities.viderTextField(myView.getTfLibelleCnu());

		employeursCtrl.clearDatas();

		clearAdresseVacation();

	}

	@Override
	protected void updateDatas() {

		clearDatas();

		if (getCurrentVacation() != null) {

			// TODO Auto-generated method stub
			setCurrentUai(getCurrentVacation().toUai());
			setCurrentCorps(getCurrentVacation().toCorps());
			setCurrentGrade(getCurrentVacation().toGrade());
			setCurrentCnu(getCurrentVacation().toCnu());
			setCurrentTypeContrat(getCurrentVacation().toTypeContratTravail());
			setCurrentProfession(getCurrentVacation().toProfession());

			CocktailUtilities.setDateToField(myView.getTfDateDebut(),getCurrentVacation().dateDebut());
			CocktailUtilities.setDateToField(myView.getTfDateFin(),getCurrentVacation().dateFin());

			CocktailUtilities.setTextToField(myView.getTfObservations(),getCurrentVacation().observations());
			CocktailUtilities.setTextToField(myView.getTfEnseignement(),getCurrentVacation().enseignement());
			CocktailUtilities.setTextToField(myView.getTfNoArrete(), getCurrentVacation().noArrete());

			CocktailUtilities.setNumberToField(myView.getTfHeures(), getCurrentVacation().nbrHeures());
			CocktailUtilities.setNumberToField(myView.getTfHeuresRealisees(), getCurrentVacation().nbrHeuresRealisees());

			CocktailUtilities.setNumberToField(myView.getTfTauxHoraire(), getCurrentVacation().tauxHoraire());
			CocktailUtilities.setNumberToField(myView.getTfTauxHoraireRealise(), getCurrentVacation().tauxHoraireRealise());

			myView.getCheckSigne().setSelected(getCurrentVacation().estSigne());
			myView.getCheckPaiementPonctuel().setSelected(getCurrentVacation().paiementPonctuel());
			myView.getCheckEnseignant().setSelected(getCurrentVacation().estEnseignant());
			myView.getCheckTitulaire().setSelected(getCurrentVacation().estTitulaire());
			myView.getCheckPersEtab().setSelected(getCurrentVacation().estPersonnelEtablissement());
			myView.getCheckDepassementAuto().setSelected(getCurrentVacation().estDepassementAuto());
			myView.getCheckAutorisationCumul().setSelected(getCurrentVacation().isAutorisationCumul());

			employeursCtrl.update(getCurrentVacation().toStructure(), null);

			// Adresse vacation
			setCurrentAdresseVacation(getCurrentVacation().toAdresse());
			clearAdresseVacation();
			if (getCurrentAdresseVacation() != null) {
				CocktailUtilities.setTextToField(myView.getTfAdresse(), getCurrentAdresseVacation().adrAdresse1());
				CocktailUtilities.setTextToField(myView.getTfComplement(), getCurrentAdresseVacation().adrAdresse2());
				CocktailUtilities.setTextToField(myView.getTfCodePostal(), getCurrentAdresseVacation().codePostal());
				if (getCurrentAdresseVacation().codePostal() != null) {
					getCommune();
				}
				if (getCurrentAdresseVacation().ville() != null)
					myView.getPopupVilles().setSelectedItem(getCurrentAdresseVacation().ville());
			}
		}

		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnDelCorps().setEnabled(getCurrentVacation() != null && getCurrentVacation().toCorps() != null);
		myView.getBtnDelGrade().setEnabled(getCurrentVacation() != null && getCurrentVacation().toGrade() != null);
		myView.getBtnDelAdresse().setEnabled(getCurrentAdresseVacation() != null);
		myView.getBtnDelProfession().setEnabled(getCurrentProfession() != null);
		myView.getBtnDelTypeContrat().setEnabled(getCurrentTypeContrat() != null);

		// On ne peut fournir d'autorisation de cumul que pour les personnels etablissement et fonctionnaires
		myView.getCheckAutorisationCumul().setEnabled(getCurrentVacation() != null 
				&& myView.getCheckTitulaire().isSelected());

		myView.getBtnDelCnu().setEnabled(getCurrentCnu() != null);

	}


	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		ServerProxy.clientSideRequestRevert(getEdc());
		setCurrentVacation(null);
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}
}

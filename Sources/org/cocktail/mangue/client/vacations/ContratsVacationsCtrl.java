package org.cocktail.mangue.client.vacations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.agents.ListeAgents;
import org.cocktail.mangue.client.gui.contrats.ContratsVacationsView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.common.utilities.AskForString;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;
import org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class ContratsVacationsCtrl {

	private static ContratsVacationsCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.FALSE;
	private EOEditingContext edc;
	private ContratsVacationsView myView;

	private ListenerVacation listenerVacation = new ListenerVacation();
	private ListenerAffectation listenerAffectation = new ListenerAffectation();

	private EODisplayGroup 			eod, eodAffectations;
	private SaisieVacationAffCtrl 	ctrlSaisieAff;

	private EOVacataires 			currentVacation;
	private EOVacatairesAffectation	currentAffectation;
	private EOIndividu 				currentIndividu;
	private EOAgentPersonnel		currentUtilisateur;
	private String 					noArrete;

	/**
	 * 
	 * @param editingContext
	 */
	public ContratsVacationsCtrl(EOEditingContext edc) {

		setEdc(edc);

		eod = new EODisplayGroup();
		eodAffectations = new EODisplayGroup();
		ctrlSaisieAff = new SaisieVacationAffCtrl(edc);

		myView = new ContratsVacationsView(null, MODE_MODAL.booleanValue(),eod, eodAffectations);

		myView.getMyEOTable().addListener(listenerVacation);
		myView.getMyEOTableAffectations().addListener(listenerAffectation);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);

		myView.getBtnAjouterAff().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterAffectation();}}
				);
		myView.getBtnModifierAff().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierAffectation();}}
				);
		myView.getBtnSupprimerAff().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerAffectation();}}
				);
		myView.getBtnImprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
				);

		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("nettoyerChamps",new Class[] {NSNotification.class}), ListeAgents.NETTOYER_CHAMPS,null);
		NSNotificationCenter.defaultCenter().addObserver(this,new NSSelector("employeHasChanged",new Class[] {NSNotification.class}), ListeAgents.CHANGER_EMPLOYE,null);

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		// Si un individu est selectionne, on met a jour les donnees
		if (((ApplicationClient)EOApplication.sharedApplication()).individuCourantID() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(edc, ((ApplicationClient)EOApplication.sharedApplication()).individuCourantID(), false));

		CocktailUtilities.initTextField(myView.getTfEmployeur(), false, false);
		CocktailUtilities.initTextField(myView.getTfCorps(), false, false);
		CocktailUtilities.initTextField(myView.getTfProfession(), false, false);
		CocktailUtilities.initTextField(myView.getTfUai(), false, false);
		CocktailUtilities.initTextArea(myView.getTaObservations(), false, false);

		majTotaux();

		updateUI();
	}


	public static ContratsVacationsCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new ContratsVacationsCtrl(editingContext);
		return sharedInstance;
	}

	public void employeHasChanged(NSNotification  notification) {
		clearTextFields();
		if (notification != null && notification.object() != null)
			setCurrentIndividu(EOIndividu.rechercherIndividuAvecID(getEdc(),(Number)notification.object(), false));
	}

	/**
	 * 
	 */
	public void actualiser() {

		eod.setObjectArray(EOVacataires.findForIndividu(getEdc(), getCurrentIndividu()));
		myView.getMyEOTable().updateData();

		majTotaux();

		updateUI();

	}

	/**
	 * 
	 */
	private void majTotaux() {

		BigDecimal total = CocktailUtilities.computeSumForKey(eod, EOVacataires.NBR_HEURES_KEY);

		BigDecimal totalEns = new BigDecimal(0);
		BigDecimal totalNonEns = new BigDecimal(0);

		for (EOVacataires myVacation : (NSArray<EOVacataires>)eod.displayedObjects()) {
			if (myVacation.nbrHeures() != null ) {
				if (myVacation.estEnseignant())
					totalEns = totalEns.add(myVacation.nbrHeures());
				else
					totalNonEns = totalNonEns.add(myVacation.nbrHeures());
			}
		}

		CocktailUtilities.setTextToLabel(myView.getLblMessage(), "TOTAL des vacations : " + total.toString() + " H ( " + totalEns + " H avec enseignement, " + totalNonEns + " H sans).");

	}

	/**
	 * @param currentIndividu
	 */
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		actualiser();
	}

	public EOVacataires getCurrentVacation() {
		return currentVacation;
	}


	public void setCurrentVacation(EOVacataires currentVacation) {
		this.currentVacation = currentVacation;
		updateData();
	}


	public EOEditingContext getEdc() {
		return edc;
	}


	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}


	public EOVacatairesAffectation getCurrentAffectation() {
		return currentAffectation;
	}


	public void setCurrentAffectation(EOVacatairesAffectation currentAffectation) {
		this.currentAffectation = currentAffectation;
	}


	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}


	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {

		this.currentUtilisateur = currentUtilisateur;

		// Gestion des droits
		myView.getBtnAjouter().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnModifier().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnSupprimer().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnImprimer().setVisible(currentUtilisateur.peutGererCarrieres());

		myView.getBtnAjouterAff().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnModifierAff().setVisible(currentUtilisateur.peutGererCarrieres());
		myView.getBtnSupprimerAff().setVisible(currentUtilisateur.peutGererCarrieres());

	}

	public JFrame getView() {
		return myView;
	}
	public JPanel getViewVacations() {
		return myView.getViewVacations();
	}

	public void nettoyerChamps(NSNotification notification) {

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();

		eod.setObjectArray(new NSArray());
		myView.getMyEOTable().updateData();

		eodAffectations.setObjectArray(new NSArray());
		myView.getMyEOTableAffectations().updateData();

		setCurrentIndividu(null);

		updateUI();

	}

	/**
	 * 
	 */
	private void clearTextFields() {

		CocktailUtilities.viderTextField(myView.getTfEmployeur());
		CocktailUtilities.viderTextField(myView.getTfProfession());
		CocktailUtilities.viderTextField(myView.getTfCorps());

	}

	/**
	 * 
	 */
	private void updateData() {

		clearTextFields();

		if (getCurrentVacation() != null) {

			if (getCurrentVacation().toUai() != null)
				CocktailUtilities.setTextToField(myView.getTfUai(), getCurrentVacation().toUai().codeEtLibelle());

			if (getCurrentVacation().toStructure() != null)
				CocktailUtilities.setTextToField(myView.getTfEmployeur(), getCurrentVacation().toStructure().llStructure());

			if (getCurrentVacation().toProfession() != null)
				CocktailUtilities.setTextToField(myView.getTfProfession(), getCurrentVacation().toProfession().libelle());

			if (getCurrentVacation().toCorps() != null)
				CocktailUtilities.setTextToField(myView.getTfCorps(), getCurrentVacation().toCorps().llCorps());

			CocktailUtilities.setTextToArea(myView.getTaObservations(), getCurrentVacation().observations());

		}

		updateUI();

	}

	/**
	 * 
	 */
	private void ajouter() {
		SaisieVacationCtrl.sharedInstance(getEdc()).ajouter(getCurrentIndividu());
		actualiser();
	}

	/**
	 * 
	 */
	private void modifier() {
		SaisieVacationCtrl.sharedInstance(getEdc()).modifier(getCurrentVacation());
		listenerVacation.onSelectionChanged();
		myView.getMyEOTable().updateUI();
		myView.getMyEOTableAffectations().updateUI();
		majTotaux();
	}

	/**
	 * Ajout d'une affectation. 
	 * Verifier si le nombre d'heures est bien > 0
	 */
	private void ajouterAffectation() {

		if (getCurrentVacation().nbrHeures() == null || getCurrentVacation().nbrHeures().intValue() == 0) {
			EODialogs.runErrorDialog("ERREUR","Veuillez auparavant renseigner un nombre d'heures de vacation !");
			return;
		}

		BigDecimal heuresAffectees = CocktailUtilities.computeSumForKey(eodAffectations.displayedObjects(), EOVacatairesAffectation.NBR_HEURES_KEY);
		ctrlSaisieAff.setAffectationsExistantes(eodAffectations.displayedObjects());
		ctrlSaisieAff.ajouter(getCurrentVacation(), heuresAffectees);
		listenerVacation.onSelectionChanged();
	}

	/**
	 * 
	 */
	private void modifierAffectation() {

		if (getCurrentVacation().nbrHeures() == null || currentVacation.nbrHeures().intValue() == 0) {
			EODialogs.runErrorDialog("ERREUR","Veuillez auparavant renseigner un nombre d'heures de vacation !");
			return;
		}

		ctrlSaisieAff.setAffectationsExistantes(eodAffectations.displayedObjects());
		ctrlSaisieAff.modifier(getCurrentAffectation());
		myView.getMyEOTableAffectations().updateUI();
	}

	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette vacation ?", "Oui", "Non"))		
			return;

		try {
			getEdc().deleteObject(getCurrentVacation());
			getEdc().saveChanges();
			eod.deleteSelection();
			myView.getMyEOTable().updateData();

		}
		catch(Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void supprimerAffectation() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Voulez-vous vraiment supprimer cette période d'affectation ?", "Oui", "Non"))		
			return;			

		try {

			getEdc().deleteObject(getCurrentAffectation());
			getEdc().saveChanges();
			
			NSArray<EOVacatairesAffectation> affectations = EOVacatairesAffectation.fetchForVacation(getEdc(), getCurrentVacation());
			if (affectations.size() == 1) {
				affectations.get(0).setEstPrincipale(true);
			}		
			getEdc().saveChanges();

			eodAffectations.deleteSelection();
			myView.getMyEOTableAffectations().updateData();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			getEdc().revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			getEdc().revert();
		}
	}

	/**
	 * Impression d'un contrat de vacations avec sélection de destinatires
	 */
	private void imprimer() {

		try {

			if (getCurrentVacation().toTypeContratTravail() != null) {
				imprimerVacation();
				return;
			}

			boolean shouldAskUser = true;

			if (getCurrentVacation().noArrete() != null) {
				noArrete = getCurrentVacation().noArrete();
				shouldAskUser = false;
			} else {

				if (EOGrhumParametres.isNoArreteAuto()) {
					shouldAskUser = !EODialogs.runConfirmOperationDialog("Attention", "Le numéro d'arrêté va être généré automatiquement. Est-ce ce que vous voulez ?", "Oui", "Non");
					if (!shouldAskUser) {
						int annee = DateCtrl.getYear(getCurrentVacation().dateDebut());
						String prefixNo = "" + annee + "/" ;
						noArrete = prefixNo + StringCtrl.stringCompletion(String.valueOf(numeroArreteIncremente(prefixNo,annee)), 4, "0", "G");

						// pour les avenants, on garde le noArrete dans l'avenant et on enregistre les données
						enregistrerNoArrete();
					}
				}

				if (shouldAskUser) {

					noArrete = AskForString.sharedInstance().getString("Numéro d'Arrêté", "Saisir le numéro d'arrêté :", "");
					enregistrerNoArrete();
					//CocktailUtilities.setTextToField(myView.getTfNoArrete(), noArrete);
				}
			}

			NSArray destinataires = new NSArray();//DestinatairesSelectCtrl.sharedInstance(edc).getDestinatairesGlobalIds();

			EOGlobalID globalIDDetail = getEdc().globalIDForObject(getCurrentVacation());
			Number noIndividu = getCurrentVacation().toIndividu().noIndividu();

			Class[] classeParametres =  new Class[] {String.class,EOGlobalID.class,NSArray.class};
			Object[] parametres = new Object[]{noArrete,globalIDDetail, destinataires};
			try {
				// avec Thread
				UtilitairesImpression.imprimerSansDialogue(getEdc(), "clientSideRequestImprimerContratVacation", classeParametres, parametres, "Contrat_Travail" +  "_" + noArrete + "_" + noIndividu,"Impression du contrat de travail");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur de lancement de l'impression !");
		}		
	}
	/**
	 * 
	 */
	private void imprimerVacation() {

		CRICursor.setWaitCursor(myView);
		boolean shouldAskUser = true;

		if (getCurrentVacation().noArrete() != null) {
			noArrete = getCurrentVacation().noArrete();
			shouldAskUser = false;
		} else {
			if (EOGrhumParametres.isNoArreteAuto()) {
				shouldAskUser = !EODialogs.runConfirmOperationDialog("Attention", "Le numéro d'arrêté va être généré automatiquement. Est-ce ce que vous voulez ?", "Oui", "Non");

				if (!shouldAskUser) {

					int annee = DateCtrl.getYear(getCurrentVacation().dateDebut());

					String prefixNo = "" + annee + "/" ;

					noArrete = prefixNo + StringCtrl.get0Int(numeroArreteIncremente(prefixNo,annee), 2);

					enregistrerNoArrete();
				}
			}
		}

		if (shouldAskUser)
			noArrete = AskForString.sharedInstance().getString("Numéro d'Arrêté", "Saisir le numéro d'arrêté :", "");

		NSArray<EOGlobalID> destinataires = DestinatairesSelectCtrl.sharedInstance(getEdc()).getDestinatairesGlobalIds();

		EOGlobalID globalIDDetail = getEdc().globalIDForObject(getCurrentVacation());
		Number noIndividu = getCurrentVacation().toIndividu().noIndividu();

		Class[] classeParametres =  new Class[] {String.class,EOGlobalID.class,NSArray.class};
		Object[] parametres = new Object[]{noArrete,globalIDDetail, destinataires};
		try {
			UtilitairesImpression.imprimerSansDialogue(getEdc(), "clientSideRequestImprimerContratVacationLocal", classeParametres,parametres,"Contrat_vacation" +  "_" + noArrete + "_" + noIndividu,"Impression du contrat de vacation");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}

		CRICursor.setDefaultCursor(myView);
	}


	/**
	 * 
	 * @param prefixNumeroAuto
	 * @param annee
	 * @return
	 */
	private int numeroArreteIncremente(String prefixNumeroAuto,int annee) {

		// Rechercher tous les avenants de l'année pour déterminer le dernier numéro automatique attribué
		NSArray<EOVacataires> vacations = EOVacataires.findForPeriode(getEdc(), DateCtrl.debutAnnee(annee), DateCtrl.finAnnee(annee));
		int valeurIncrementee = 0;
		if (vacations != null && vacations.count() > 0) {
			for (EOVacataires myVacataire : vacations) {
				String noArrete = myVacataire.noArrete();				
				if (noArrete != null && noArrete.indexOf(prefixNumeroAuto) >= 0) {	// Numéro automatique
					try {
						String num = noArrete.substring(prefixNumeroAuto.length());
						int numAuto = new Integer(num).intValue();
						if (numAuto > valeurIncrementee) {
							valeurIncrementee = numAuto;
						}
					} catch (Exception exc) {
					}
				}
			}
		}

		return valeurIncrementee + 1;
	}
	private void enregistrerNoArrete() {
		try {
			getCurrentVacation().setNoArrete(noArrete);
			if (getCurrentVacation().dateArrete() == null)
				getCurrentVacation().setDateArrete(new NSTimestamp());
			getEdc().saveChanges();
		} catch (Exception e) {
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite lors de l'enregistrement des données de l'arrêté. Pour imprimer l'arrêté, veuillez saisir dasn l'élément de carrière le numéro d'arrêté et la date");
			return;
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerVacation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererContrats())
				modifier();
		}

		public void onSelectionChanged() {

			clearTextFields();
			setCurrentVacation((EOVacataires)eod.selectedObject());

			eodAffectations.setObjectArray(new NSArray());

			if (getCurrentVacation() != null) {
				eodAffectations.setObjectArray(EOVacatairesAffectation.fetchForVacation(getEdc(), getCurrentVacation()));
			}

			myView.getMyEOTableAffectations().updateData();

			updateUI();

		}
	}

	private class ListenerAffectation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick() {
			if (getCurrentUtilisateur().peutGererContrats())
				modifierAffectation();
		}
		public void onSelectionChanged() {
			setCurrentAffectation((EOVacatairesAffectation)eodAffectations.selectedObject());
			updateUI();
		}
	}

	/**
	 * 
	 */
	private void updateUI() {

		myView.getBtnAjouter().setEnabled(getCurrentIndividu() != null);
		myView.getBtnModifier().setEnabled(getCurrentVacation() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentVacation() != null);
		myView.getBtnImprimer().setEnabled(getCurrentVacation() != null && getCurrentVacation().nbrHeures() != null && getCurrentVacation().nbrHeures().floatValue() > 0);

		myView.getBtnAjouterAff().setEnabled(getCurrentIndividu() != null && getCurrentVacation() != null);
		myView.getBtnModifierAff().setEnabled(getCurrentAffectation() != null);
		myView.getBtnSupprimerAff().setEnabled(getCurrentAffectation() != null);

	}

}
package org.cocktail.mangue.client.vacations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;

import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.vacations.SaisieIdentiteVacataireView;
import org.cocktail.mangue.client.select.DepartementSelectCtrl;
import org.cocktail.mangue.client.select.PaysSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.EODepartement;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOCivilite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;

public class SaisieIdentiteVacataireCtrl extends ModelePageSaisie {

	private static SaisieIdentiteVacataireCtrl sharedInstance;
	private static Boolean MODE_MODAL = Boolean.TRUE;
	private SaisieIdentiteVacataireView myView;

	private EOIndividu 				currentIndividu;
	private EOPersonnel 			currentPersonnel;
	private EODepartement 			currentDepartement;
	private EOPays		 			currentPaysNaissance;
	private DepartementSelectCtrl mySelectorDepartement;
	private PaysSelectCtrl mySelectorPaysNaissance;

	/**
	 * 
	 * @param edc
	 */
	public SaisieIdentiteVacataireCtrl(EOEditingContext edc) {

		super(edc);

		myView = new SaisieIdentiteVacataireView(null, MODE_MODAL.booleanValue());

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());
		setDateListeners(myView.getTfDateNaissance());

		CocktailUtilities.initPopupAvecListe(myView.getPopupCivilite(), EOCivilite.find(edc), false);

		myView.getBtnGetDepartement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectDepartement();}}
				);
		myView.getBtnDelDepartement().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delDepartement();}}
				);
		myView.getBtnGetPays().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectPaysNaissance();}}
				);
		myView.getBtnDelPays().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delPaysNaissance();}}
				);

		CocktailUtilities.initTextField(myView.getTfPays(), false, false);
		CocktailUtilities.initTextField(myView.getTfDepartement(), false, false);

	}

	public static SaisieIdentiteVacataireCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new SaisieIdentiteVacataireCtrl(editingContext);
		return sharedInstance;
	}


	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}
	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
		updateDatas();
	}

	public EOPersonnel getCurrentPersonnel() {
		return currentPersonnel;
	}

	public void setCurrentPersonnel(EOPersonnel currentPersonnel) {
		this.currentPersonnel = currentPersonnel;
	}

	public void modifier(EOIndividu individu) {
		setCurrentIndividu(individu);
		myView.setVisible(true);
	}



	public EODepartement getCurrentDepartement() {
		return currentDepartement;
	}

	public void setCurrentDepartement(EODepartement currentDepartement) {
		this.currentDepartement = currentDepartement;
		CocktailUtilities.viderTextField(myView.getTfDepartement());
		if (currentDepartement != null) {
			CocktailUtilities.setTextToField(myView.getTfDepartement(), currentDepartement.codeEtLibelle());
		}
	}

	public EOPays getCurrentPaysNaissance() {
		return currentPaysNaissance;
	}

	public void setCurrentPaysNaissance(EOPays currentPaysNaissance) {
		this.currentPaysNaissance = currentPaysNaissance;
		CocktailUtilities.viderTextField(myView.getTfPays());
		if (currentPaysNaissance != null) {
			CocktailUtilities.setTextToField(myView.getTfPays(), currentPaysNaissance.codeEtLibelle());
			if (!currentPaysNaissance.isDefault())
				setCurrentDepartement(null);
		}
	}

	/**
	 * 
	 * @param individu
	 */
	public void open(EOIndividu individu)	{

		setCurrentIndividu(individu);

		myView.setVisible(true);
	}


	public JDialog getView() {
		return myView;
	}

	/**
	 * 
	 */
	public void selectDepartement() {

		if (mySelectorDepartement == null)
			mySelectorDepartement = new DepartementSelectCtrl(getEdc());
		EODepartement departement = mySelectorDepartement.getDepartement(null, getCurrentIndividu().dNaissance());
		if (departement != null) {
			setCurrentDepartement(departement);
		}
	}
	public void delDepartement() {
		setCurrentDepartement(null);
	}
	/**
	 * 
	 */
	public void selectPaysNaissance() {

		if (mySelectorPaysNaissance == null)
			mySelectorPaysNaissance = new PaysSelectCtrl(getEdc());
		EOPays pays = mySelectorPaysNaissance.getPays(null, getCurrentIndividu().dNaissance());
		if (pays != null) {
			setCurrentPaysNaissance(pays);
		}
		updateInterface();
	}
	public void delPaysNaissance() {
		setCurrentPaysNaissance(null);
	}


	/**
	 * 
	 */
	private void creerPersonnel() {
		EOAgentPersonnel agent = ((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel();
		EOPersonnel personnel = getCurrentIndividu().personnel();
		if (personnel == null) {	
			personnel = new EOPersonnel();
			personnel.initPersonnel(getCurrentIndividu().noIndividu());
			personnel.setToIndividuRelationship(getCurrentIndividu());
			getEdc().insertObject(personnel);
		}
	}

	@Override
	protected void clearDatas() {

		CocktailUtilities.viderTextField(myView.getTfNomUsuel());
		CocktailUtilities.viderTextField(myView.getTfPrenom());
		CocktailUtilities.viderTextField(myView.getTfNomFamille());

		CocktailUtilities.viderTextField(myView.getTfDateNaissance());
		CocktailUtilities.viderTextField(myView.getTfInsee());
		CocktailUtilities.viderTextField(myView.getTfCleInsee());

		myView.getTfNpc().setText("00");
	}

	@Override
	protected void updateDatas() {

		clearDatas();

		if (getCurrentIndividu() != null) {
			
			myView.getPopupCivilite().setSelectedItem(EOCivilite.findForCode(getEdc(), getCurrentIndividu().cCivilite()));

			myView.getCheckProvisoire().setSelected(getCurrentIndividu().priseCptInsee().equals("P"));
			CocktailUtilities.setTextToField(myView.getTfNomUsuel(), getCurrentIndividu().nomUsuel());
			CocktailUtilities.setTextToField(myView.getTfNomFamille(), getCurrentIndividu().nomPatronymique());
			CocktailUtilities.setTextToField(myView.getTfPrenom(), getCurrentIndividu().prenom());

			if (getCurrentIndividu().toPaysNaissance() != null)
				setCurrentPaysNaissance(getCurrentIndividu().toPaysNaissance());
			else
				setCurrentPaysNaissance(EOPays.getDefault(getEdc()));
			
			setCurrentDepartement(getCurrentIndividu().toDepartement());

			if (getCurrentIndividu().estNoInseeProvisoire()) {
				CocktailUtilities.setTextToField(myView.getTfInsee(), getCurrentIndividu().indNoInseeProv());
				CocktailUtilities.setNumberToField(myView.getTfCleInsee(), getCurrentIndividu().indCleInseeProv());
			}
			else {
				CocktailUtilities.setTextToField(myView.getTfInsee(), getCurrentIndividu().indNoInsee());
				CocktailUtilities.setNumberToField(myView.getTfCleInsee(), getCurrentIndividu().indCleInsee());			
			}
			CocktailUtilities.setDateToField(myView.getTfDateNaissance(), getCurrentIndividu().dNaissance());

			if (getCurrentIndividu().personnel() != null)
				CocktailUtilities.setTextToField(myView.getTfNpc(), getCurrentIndividu().personnel().npc());
		}
		updateInterface();
	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub
		myView.getBtnGetDepartement().setEnabled(getCurrentPaysNaissance() != null && getCurrentPaysNaissance().isDefault());
		myView.getBtnDelDepartement().setEnabled(getCurrentDepartement() != null);
		myView.getBtnDelPays().setEnabled(getCurrentPaysNaissance() != null);
	}

	@Override
	protected void traitementsAvantValidation() {
		// TODO Auto-generated method stub
		try {

			creerPersonnel();

			getCurrentIndividu().setNomUsuel(CocktailUtilities.getTextFromField(myView.getTfNomUsuel()));
			getCurrentIndividu().setPrenom(CocktailUtilities.getTextFromField(myView.getTfPrenom()));
			getCurrentIndividu().setNomPatronymique(CocktailUtilities.getTextFromField(myView.getTfNomFamille()));
			getCurrentIndividu().setDNaissance(CocktailUtilities.getDateFromField(myView.getTfDateNaissance()));

			getCurrentIndividu().setIndNoInseeProv(null);
			getCurrentIndividu().setIndCleInseeProv(null);
			getCurrentIndividu().setIndNoInsee(null);
			getCurrentIndividu().setIndCleInsee(null);				

			getCurrentIndividu().setToDepartementRelationship(getCurrentDepartement());
			if (getCurrentDepartement() != null && getCurrentIndividu().toPaysNaissance() == null) {
				getCurrentIndividu().setToPaysNaissanceRelationship(EOPays.getDefault(getEdc()));
			}

			if (myView.getCheckProvisoire().isSelected()) {
				getCurrentIndividu().setIndNoInseeProv(CocktailUtilities.getTextFromField(myView.getTfInsee()));
				getCurrentIndividu().setIndCleInseeProv(CocktailUtilities.getIntegerFromField(myView.getTfCleInsee()));
				getCurrentIndividu().setPriseCptInsee("P");
			}
			else {
				getCurrentIndividu().setIndNoInsee(CocktailUtilities.getTextFromField(myView.getTfInsee()));
				getCurrentIndividu().setIndCleInsee(CocktailUtilities.getIntegerFromField(myView.getTfCleInsee()));				
				getCurrentIndividu().setPriseCptInsee("R");
			}
			getCurrentIndividu().personnel().setNpc(CocktailUtilities.getTextFromField(myView.getTfNpc()));

			getCurrentIndividu().setIndActivite(ManGUEConstantes.IND_ACTIVITE_VACATION);


		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void traitementsApresValidation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		myView.setVisible(false);

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

}

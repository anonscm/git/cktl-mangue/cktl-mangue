// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   CirSaisieFichieImportCtrl.java

package org.cocktail.mangue.client.vacations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JFrame;

import org.apache.tools.ant.taskdefs.Sync.MyCopy;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.gui.vacations.SaisieVacationAffView;
import org.cocktail.mangue.client.select.StructureSelectCtrl;
import org.cocktail.mangue.client.templates.ModelePageSaisie;
import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;
import org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class SaisieVacationAffCtrl extends ModelePageSaisie {

	private static final long serialVersionUID = 0x7be9cfa4L;
	private SaisieVacationAffView myView;
	private NSArray<EOVacatairesAffectation> affectationsExistantes;

	private EOIndividu currentIndividu;
	private EOVacatairesAffectation currentAffectation;
	private EOStructure currentStructure;

	public SaisieVacationAffCtrl(EOEditingContext edc) {

		super(edc);

		myView = new SaisieVacationAffView(new JFrame(), true);

		setActionBoutonValiderListener(myView.getBtnValider());
		setActionBoutonAnnulerListener(myView.getBtnAnnuler());

		myView.getBtnGetStructure().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectStructure();}}
				);

		CocktailUtilities.initTextField(myView.getTfStructure(), false, false);
		CocktailUtilities.initTextField(myView.getTfStructureDetail(), false, false);

	}

	public EOIndividu getCurrentIndividu() {
		return currentIndividu;
	}

	public void setCurrentIndividu(EOIndividu currentIndividu) {
		this.currentIndividu = currentIndividu;
	}

	private EOVacatairesAffectation getCurrentAffectation() {
		return currentAffectation;
	}
	public void setCurrentAffectation(EOVacatairesAffectation affectation) {
		this.currentAffectation = affectation;
		if (currentAffectation != null)
			myView.setTitle("AFFECTATIONS ( " + getCurrentAffectation().toVacataire().toIndividu().identitePrenomFirst() + ")");
		else
			myView.setTitle("AFFECTATIONS");
	}
	
	private EOStructure getCurrentStructure() {
		return currentStructure;
	}
	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
		CocktailUtilities.setTextToField(myView.getTfStructure(), "");
		CocktailUtilities.setTextToField(myView.getTfStructureDetail(), "");
		if (currentStructure != null) {
			CocktailUtilities.setTextToField(myView.getTfStructure(), currentStructure.llStructure());
			CocktailUtilities.setTextToField(myView.getTfStructureDetail(), currentStructure.libelleComplet());
		}		
	}


	public void setAffectationsExistantes(NSArray<EOVacatairesAffectation> affectations) {
		affectationsExistantes = affectations;
	}

	/**
	 * 
	 */
	private void selectStructure() {

		EOStructure structure = StructureSelectCtrl.sharedInstance(getEdc()).getStructureService();		
		//		EOStructure structure = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(getEdc(), currentAffectation.toVacataire().dDebVacation());
		if (structure != null) {
			currentStructure = structure;
			displayStructure(currentStructure);
		}
	}

	/**
	 * 
	 * @param structure
	 */
	private void displayStructure(EOStructure structure) {

		if (structure != null) {
			CocktailUtilities.setTextToField(myView.getTfStructure(), structure.llStructure());
			CocktailUtilities.setTextToField(myView.getTfStructureDetail(), structure.libelleComplet());
		}
	}

	private void clearTextFields()	{

		myView.getTfHeures().setText("");
		myView.getTfStructure().setText("");
		myView.getTfStructureDetail().setText("");

		currentStructure = null;

	}

	/**
	 * 
	 * @param vacation
	 * @param heuresAffectees
	 * @return
	 */
	public EOVacatairesAffectation ajouter(EOVacataires vacation, BigDecimal heuresAffectees)	{

		setCurrentAffectation(EOVacatairesAffectation.creer(getEdc(), vacation));
		getCurrentAffectation().setEstPrincipale(affectationsExistantes.size() == 0);
		updateDatas();

		BigDecimal heuresRestantes = vacation.nbrHeures().subtract(heuresAffectees);
		CocktailUtilities.setNumberToField(myView.getTfHeures(), heuresRestantes);

		myView.setVisible(true);

		return getCurrentAffectation();
	}

	/**
	 * 
	 * @param affectation
	 * @return
	 */
	public boolean modifier(EOVacatairesAffectation affectation) {

		setCurrentAffectation(affectation);
		updateDatas();

		myView.setVisible(true);
		return getCurrentAffectation() != null;
	}


	/**
	 * 
	 * @return
	 */
	protected void traitementsAvantValidation() {

		getCurrentAffectation().setToStructureRelationship(getCurrentStructure());
		getCurrentAffectation().setNbrHeures(CocktailUtilities.getBigDecimalFromField(myView.getTfHeures()));
		getCurrentAffectation().setEstPrincipale(myView.getCheckPrincipale().isSelected());

		if (getCurrentStructure() != null && CocktailUtilities.getBigDecimalFromField(myView.getTfHeures()) != null) {

			// Controle des chevauchements de periode avec quotite > 100 ou meme structure 
			NSMutableArray<EOVacatairesAffectation> affectations = new NSMutableArray<EOVacatairesAffectation>(affectationsExistantes);
			if (affectations.containsObject(getCurrentAffectation()) == false)
				affectations.addObject(getCurrentAffectation());

			BigDecimal totalHeuresVacation = getCurrentAffectation().toVacataire().nbrHeures();
			BigDecimal totalHeures = getCurrentAffectation().nbrHeures();

			for (EOVacatairesAffectation aff : affectations) {
				if (aff != getCurrentAffectation()) {

					// Gestion de l'unicite des affectations principales
					if (getCurrentAffectation().estPrincipale())
						aff.setEstPrincipale(false);

					totalHeures = totalHeures.add(aff.nbrHeures());
					if ( aff.toStructure() == getCurrentStructure() ) {
						throw new NSValidation.ValidationException("L'agent ne peut être affecté à la même structure pendant la même période !");
					}
				}

				if ( totalHeures.compareTo(totalHeuresVacation) > 0) {
					throw new NSValidation.ValidationException("Le nombre d'heures cumulé (" + totalHeures.toString() + ") ne peut dépasser " + totalHeuresVacation.toString() + " H !");
				}

			}
		}

	}

	/**
	 * 
	 */
	protected  void traitementsApresValidation() {

		try {
			//Ajoute le role enseignant si c'est le cas			
			gererAssociationEnseignant();

			NSArray<EOVacatairesAffectation> affectations = EOVacatairesAffectation.fetchForVacation(getEdc(), getCurrentAffectation().toVacataire());
			if (affectations.size() == 1) {
				affectations.get(0).setEstPrincipale(true);
			}		

		}
		catch (Exception e) {

		}
		
		myView.setVisible(false);

	}


	/**
	 * Creation d'une repart Association de type "Enseignant"
	 */
	private void gererAssociationEnseignant() {
		//Si l'affectation concerne un enseignant, on crée une répartition ou la met à jour
		if (getCurrentAffectation().toVacataire().estEnseignant()) {
			try {
				EORepartAssociation repartAssociation = EORepartAssociation.rechercherPourIndividuStructureAssociationEtPeriode(
						getEdc(), 
						getCurrentAffectation().toVacataire().toIndividu(), 
						getCurrentAffectation().toStructure(), 
						EOAssociation.LIBELLE_ASSOCIATION_ENSEIGNANT, 
						getCurrentAffectation().toVacataire().dateDebut(), getCurrentAffectation().toVacataire().dateFin());

				if (repartAssociation == null) {
					repartAssociation = EORepartAssociation.creerAssociationEnseignant(getEdc(), getCurrentStructure(), getCurrentAffectation().toVacataire().toIndividu(), 
							((ApplicationClient) EOApplication.sharedApplication()).getAgentPersonnel());
					getEdc().insertObject(repartAssociation);
				}

				repartAssociation.setStructureRelationship(getCurrentAffectation().toStructure());
				repartAssociation.setRasQuotite(ManGUEConstantes.QUOTITE_100);
				repartAssociation.setDateDebut(getCurrentAffectation().toVacataire().dateDebut());
				repartAssociation.setDateFin(getCurrentAffectation().toVacataire().dateFin());

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void clearDatas() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void updateDatas() {
		// TODO Auto-generated method stub
		clearTextFields();
		if (getCurrentAffectation() != null) {
			setCurrentStructure(getCurrentAffectation().toStructure());
			CocktailUtilities.setNumberToField(myView.getTfHeures(), getCurrentAffectation().nbrHeures());

			myView.getCheckPrincipale().setSelected(getCurrentAffectation().estPrincipale());

		}
		updateInterface();

	}

	@Override
	protected void updateInterface() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourAnnulation() {
		// TODO Auto-generated method stub
		setCurrentAffectation(null);
		myView.setVisible(false);
	}

	@Override
	protected void traitementsChangementDate() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void traitementsPourCreation() {
		// TODO Auto-generated method stub

	}

}

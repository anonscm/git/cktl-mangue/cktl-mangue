package org.cocktail.mangue.client.vacations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.math.BigDecimal;

import javax.swing.JFileChooser;
import javax.swing.JTextField;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ApplicationClient;
import org.cocktail.mangue.client.budget.BudgetCtrl;
import org.cocktail.mangue.client.gui.vacations.VacationsView;
import org.cocktail.mangue.client.impression.UtilitairesImpression;
import org.cocktail.mangue.client.individu.infosperso.AdressesCtrl;
import org.cocktail.mangue.client.individu.infosperso.TelephonesCtrl;
import org.cocktail.mangue.client.select.DestinatairesSelectCtrl;
import org.cocktail.mangue.client.select.StructureSelectCtrl;
import org.cocktail.mangue.common.utilities.AskForString;
import org.cocktail.mangue.common.utilities.CRICursor;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.UtilitairesFichier;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOVacataires;
import org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class VacationsCtrl {

	private static VacationsCtrl 	sharedInstance;
	private static Boolean 			MODE_MODAL = Boolean.TRUE;
	private EOEditingContext		edc;
	private VacationsView 			myView;
	private EODisplayGroup 			eodVacations, eodAffectations;
	private boolean 				saisieEnabled, isLocked;
	private String 					noArrete;

	private ListenerVacation 		listenerVacation = new ListenerVacation();
	private ListenerAffectation 	listenerAffectation = new ListenerAffectation();

	private SaisieVacationAffCtrl 	ctrlSaisieAff;
	private EOVacataires			currentVacation;
	private EOVacatairesAffectation	currentAffectation;
	private EOStructure 			currentService;
	private EOAgentPersonnel		currentUtilisateur;

	private TelephonesCtrl			ctrlTelephones;
	private BudgetCtrl 				myCtrlBudget;

	public VacationsCtrl(EOEditingContext editingContext) {

		setEdc(editingContext);

		ctrlSaisieAff = new SaisieVacationAffCtrl(getEdc());
		ctrlTelephones = new TelephonesCtrl(getEdc(), null, true);

		myCtrlBudget = new BudgetCtrl(getEdc());
		myCtrlBudget.setTypeGestion (BudgetCtrl.TYPE_GESTION_AGENT);

		eodVacations = new EODisplayGroup();
		eodAffectations = new EODisplayGroup();
		myView = new VacationsView(null, MODE_MODAL.booleanValue(), eodVacations, eodAffectations);

		myView.getBtnAjouter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouter();}}
				);
		myView.getBtnModifier().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifier();}}
				);
		myView.getBtnSupprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimer();}}
				);
		myView.getBtnGetService().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {selectService();}}
				);
		myView.getBtnDelService().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {delService();}}
				);
		myView.getBtnAjouterAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {ajouterAffectation();}}
				);
		myView.getBtnModifierAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierAffectation();}}
				);
		myView.getBtnSupprimerAffectation().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {supprimerAffectation();}}
				);
		myView.getBtnImprimer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {imprimer();}}
				);
		myView.getBtnRechercher().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {rechercher();}}
				);
		myView.getBtnExporter().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {exporter();}}
				);
		myView.getBtnDupliquer().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {dupliquer();}}
				);
		myView.getBtnEtatCivil().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {modifierEtatCivil();}}
				);


		myView.getBtnAdresses().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherAdresses();}}
				);
		myView.getBtnTelephone().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherTelephones();}}
				);
		myView.getBtnBudget().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {afficherBudget();}}
				);

		myView.getMyEOTable().addListener(listenerVacation);
		myView.getMyEOTableAff().addListener(listenerAffectation);

		CocktailUtilities.initPopupOuiNon(myView.getPopupEnseignant(), true);
		CocktailUtilities.initPopupOuiNon(myView.getPopupTitulaire(), true);

		CocktailUtilities.setDateToField(myView.getTfFiltrePeriodeDebut(), CocktailUtilities.debutAnneeUniversitaire(new NSTimestamp()));
		CocktailUtilities.setDateToField(myView.getTfFiltrePeriodeFin(), CocktailUtilities.finAnneeUniversitaire(new NSTimestamp()));
		CocktailUtilities.initTextField(myView.getTfFiltreService(), false, false);

		myView.getTfFiltreNom().addActionListener(new MyActionListener());
		myView.getTfFiltrePeriodeDebut().addFocusListener(new FocusListenerDateTextField(myView.getTfFiltrePeriodeDebut()));
		myView.getTfFiltrePeriodeDebut().addActionListener(new ActionListenerDateTextField(myView.getTfFiltrePeriodeDebut()));
		myView.getTfFiltrePeriodeFin().addFocusListener(new FocusListenerDateTextField(myView.getTfFiltrePeriodeFin()));
		myView.getTfFiltrePeriodeFin().addActionListener(new ActionListenerDateTextField(myView.getTfFiltrePeriodeFin()));

		setCurrentUtilisateur(((ApplicationClient)EOApplication.sharedApplication()).getAgentPersonnel());

		setSaisieEnabled(false);
		updateInterface();
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static VacationsCtrl sharedInstance(EOEditingContext editingContext)	{
		if(sharedInstance == null)
			sharedInstance = new VacationsCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 */
	public void open()	{

		if (!getCurrentUtilisateur().peutGererVacations()) {
			EODialogs.runErrorDialog("ERREUR", "Vous n'avez pas les droits de gestion des vacataires !");
			return;
		}

		CRICursor.setWaitCursor(myView);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(true);
		actualiser();
		CRICursor.setDefaultCursor(myView);
		myView.setVisible(true);
		((ApplicationClient)EOApplication.sharedApplication()).setGlassPane(false);

	}

	/**
	 * 
	 */
	private void afficherAdresses() {
		CRICursor.setWaitCursor(myView);
		AdressesCtrl.sharedInstance(edc).setDroitsGestion(((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());
		AdressesCtrl.sharedInstance(edc).open(getCurrentVacation().toIndividu());
		CRICursor.setDefaultCursor(myView);
	}

	/**
	 * 
	 */
	private void afficherTelephones() {

		CRICursor.setWaitCursor(myView);
		ctrlTelephones.setDroitsGestion(((ApplicationClient)ApplicationClient.sharedApplication()).getAgentPersonnel());
		ctrlTelephones.open(getCurrentVacation().toIndividu());
		CRICursor.setDefaultCursor(myView);

	}
	/**
	 * 
	 */
	private void afficherBudget() {
		CRICursor.setWaitCursor(myView);
		if (getCurrentVacation() != null) {
			myCtrlBudget.open(getCurrentVacation().toIndividu());
		}
		CRICursor.setDefaultCursor(myView);
	}


	/**
	 * 
	 * @return
	 */
	public EOEditingContext getEdc() {
		return edc;
	}

	/**
	 * 
	 * @param edc
	 */
	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}

	public EOAgentPersonnel getCurrentUtilisateur() {
		return currentUtilisateur;
	}
	public void setCurrentUtilisateur(EOAgentPersonnel currentUtilisateur) {
		this.currentUtilisateur = currentUtilisateur;
	}

	public EOVacataires getCurrentVacation() {
		return currentVacation;
	}

	public void setCurrentVacation(EOVacataires currentVacation) {
		this.currentVacation = currentVacation;
	}
	public EOStructure getCurrentService() {
		return currentService;
	}
	public void setCurrentService(EOStructure currentService) {
		this.currentService = currentService;
		CocktailUtilities.setTextToField(myView.getTfFiltreService(), "");
		if (currentService != null)
			CocktailUtilities.setTextToField(myView.getTfFiltreService(), currentService.llStructure());
	}
	public EOVacatairesAffectation getCurrentAffectation() {
		return currentAffectation;
	}
	public void setCurrentAffectation(EOVacatairesAffectation currentAffectation) {
		this.currentAffectation = currentAffectation;
	}

	public void toFront() {
		myView.toFront();
	}
	public boolean saisieEnabled() {
		return saisieEnabled;
	}
	public boolean isLocked() {
		return isLocked;
	}
	public void setIsLocked(boolean yn) {
		isLocked = yn;
		updateInterface();
	}

	public void setSaisieEnabled(boolean yn) {
		saisieEnabled = yn;
		updateInterface();
	}

	/**
	 * 
	 */
	private void actualiser()	{

		CRICursor.setWaitCursor(myView);
		rechercher();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void selectService() {

		EOStructure service = StructureSelectCtrl.sharedInstance(getEdc()).getStructureService();		

		//EOStructure service = StructureArbreSelectCtrl.sharedInstance().selectionnerStructure(getEdc());
		if (service != null) {
			setCurrentService(service);
			actualiser();
			myView.toFront();
		}
	}

	/**
	 * 
	 */
	private void dupliquer() {

		EOVacataires newVacataire = SaisieVacationCtrl.sharedInstance(getEdc()).renouveler(getCurrentVacation());

		if (newVacataire != null) {

			try {
				// Ajout des affectations
				for (EOVacatairesAffectation affectation : (NSArray<EOVacatairesAffectation>)eodAffectations.displayedObjects()) {

					EOVacatairesAffectation newAffectation = EOVacatairesAffectation.creer(getEdc(), newVacataire);
					newAffectation.setToStructureRelationship(affectation.toStructure());
					newAffectation.setNbrHeures(affectation.nbrHeures());

				}
				getEdc().saveChanges();
			}
			catch (Exception e) {
				e.printStackTrace();
			}

			actualiser();
			myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(newVacataire));
		}

	}



	private void delService() {
		setCurrentService(null);
	}
	/**
	 * Impression d'un contrat de vacations avec sélection de destinatires
	 */
	private void imprimer() {

		/**
		 * 
		 */
		if (getCurrentVacation().toTypeContratTravail() != null) {
			imprimerVacation();
			return;
		}

		try {
			boolean shouldAskUser = true;

			if (getCurrentVacation().noArrete() != null) {

				noArrete = getCurrentVacation().noArrete();
				shouldAskUser = false;

			} else {

				if (EOGrhumParametres.isNoArreteAuto()) {
					shouldAskUser = !EODialogs.runConfirmOperationDialog("Attention", "Le numéro d'arrêté va être généré automatiquement. Est-ce ce que vous voulez ?", "Oui", "Non");
					if (!shouldAskUser) {
						int annee = DateCtrl.getYear(getCurrentVacation().dateDebut());
						String prefixNo = "" + annee + "/" ;
						noArrete = prefixNo + StringCtrl.stringCompletion(String.valueOf(numeroArreteIncremente(prefixNo,annee)), 4, "0", "G");

						// pour les avenants, on garde le noArrete dans l'avenant et on enregistre les données
						enregistrerNoArrete();
					}
				}

				if (shouldAskUser) {

					noArrete = AskForString.sharedInstance().getString("Numéro d'Arrêté", "Saisir le numéro d'arrêté : ", "");
					enregistrerNoArrete();
					//CocktailUtilities.setTextToField(myView.getTfNoArrete(), noArrete);
				}
			}

			NSArray destinataires = new NSArray();//DestinatairesSelectCtrl.sharedInstance(ec).getDestinatairesGlobalIds();

			EOGlobalID globalIDDetail = getEdc().globalIDForObject(getCurrentVacation());
			Number noIndividu = getCurrentVacation().toIndividu().noIndividu();

			Class[] classeParametres =  new Class[] {String.class,EOGlobalID.class,NSArray.class};
			Object[] parametres = new Object[]{noArrete,globalIDDetail, destinataires};
			try {
				UtilitairesImpression.imprimerSansDialogue(getEdc(),"clientSideRequestImprimerContratVacation",classeParametres,parametres,"Contrat_vacation" +  "_" + noArrete + "_" + noIndividu,"Impression du contrat de vacation");
			} catch (Exception e) {
				LogManager.logException(e);
				EODialogs.runErrorDialog("Erreur",e.getMessage());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			EODialogs.runErrorDialog("ERREUR","Erreur de lancement de l'impression !");
		}		
	}

	/**
	 * 
	 */
	private void imprimerVacation() {

		CRICursor.setWaitCursor(myView);
		boolean shouldAskUser = true;

		if (getCurrentVacation().noArrete() != null) {
			noArrete = getCurrentVacation().noArrete();
			shouldAskUser = false;
		} else {
			if (EOGrhumParametres.isNoArreteAuto()) {

				shouldAskUser = !EODialogs.runConfirmOperationDialog("Attention", "Le numéro d'arrêté va être généré automatiquement. Est-ce ce que vous voulez ?", "Oui", "Non");

				if (!shouldAskUser) {

					int annee = DateCtrl.getYear(getCurrentVacation().dateDebut());

					String prefixNo = "" + annee + "/" ;

					noArrete = prefixNo + StringCtrl.get0Int(numeroArreteIncremente(prefixNo,annee), 2);

					enregistrerNoArrete();
				}
			}
		}

		if (shouldAskUser)
			noArrete = AskForString.sharedInstance().getString("Numéro d'Arrêté", "Saisir le numéro d'arrêté :", "");

		NSArray<EOGlobalID> destinataires = DestinatairesSelectCtrl.sharedInstance(edc).getDestinatairesGlobalIds();

		EOGlobalID globalIDDetail = edc.globalIDForObject(getCurrentVacation());
		Number noIndividu = getCurrentVacation().toIndividu().noIndividu();

		Class[] classeParametres =  new Class[] {String.class,EOGlobalID.class,NSArray.class};
		Object[] parametres = new Object[]{noArrete,globalIDDetail, destinataires};
		try {
			UtilitairesImpression.imprimerSansDialogue(edc,"clientSideRequestImprimerContratVacationLocal",classeParametres,parametres,"Contrat_vacation" +  "_" + noArrete + "_" + noIndividu,"Impression du contrat de vacation");
		} catch (Exception e) {
			LogManager.logException(e);
			EODialogs.runErrorDialog("Erreur",e.getMessage());
		}

		CRICursor.setDefaultCursor(myView);
	}


	/**
	 * 
	 * @param prefixNumeroAuto
	 * @param annee
	 * @return
	 */
	private int numeroArreteIncremente(String prefixNumeroAuto,int annee) {

		// Rechercher tous les avenants de l'année pour déterminer le dernier numéro automatique attribué
		NSArray<EOVacataires> vacations = EOVacataires.findForPeriode(getEdc(), DateCtrl.debutAnnee(annee), DateCtrl.finAnnee(annee));

		LogManager.logErreur("NO ARRETE INCREMENTE - Prefix : " + prefixNumeroAuto + " , Nb vacations : " + vacations.size());

		int valeurIncrementee = 0;

		if (vacations != null && vacations.count() > 0) {

			for (EOVacataires vacation : vacations) {

				String noArrete = vacation.noArrete();
				if (noArrete != null && noArrete.indexOf(prefixNumeroAuto) >= 0) {	// Numéro automatique
					try {
						String num = noArrete.substring(prefixNumeroAuto.length());
						int numAuto = new Integer(num).intValue();
						LogManager.logErreur("\t Num auto : " + numAuto);
						if (numAuto > valeurIncrementee) {
							valeurIncrementee = numAuto;
							LogManager.logErreur("\t\tValeur incrémentée : " + valeurIncrementee);
						}
					} catch (Exception exc) {
						exc.printStackTrace();
					}
				}
			}
		}

		return valeurIncrementee + 1;
	}

	/**
	 * 
	 */
	private void enregistrerNoArrete() {
		try {
			currentVacation.setNoArrete(noArrete);
			LogManager.logErreur("NO ARRETE GENERE : " + noArrete);
			if (currentVacation.dateArrete() == null)
				currentVacation.setDateArrete(new NSTimestamp());
			getEdc().saveChanges();
		} catch (Exception e) {
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite lors de l'enregistrement des données de l'arrêté. Pour imprimer l'arrêté, veuillez saisir dasn l'élément de carrière le numéro d'arrêté et la date");
			return;
		}
	}

	/**
	 * 
	 * @return
	 */
	private EOQualifier filterQualifier() {

		NSMutableArray qualifiers = new NSMutableArray();
		NSTimestamp dateReference = new NSTimestamp();

		if (!StringCtrl.chaineVide(myView.getTfFiltreNom().getText()))
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVacataires.TO_INDIVIDU_IDENTITE_KEY+"."+EOIndividu.NOM_USUEL_KEY + " caseInsensitiveLike %@", new NSArray("*"+myView.getTfFiltreNom().getText()+"*")));

		if (myView.getPopupEnseignant().getSelectedIndex() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVacataires.TEM_ENSEIGNANT_KEY+"=%@", new NSArray((String)myView.getPopupEnseignant().getSelectedItem())));

		if (myView.getPopupTitulaire().getSelectedIndex() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVacataires.TEM_TITULAIRE_KEY+"=%@", new NSArray((String)myView.getPopupTitulaire().getSelectedItem())));

		if (getCurrentService() != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVacataires.TO_AFFECTATIONS_KEY+"."+EOVacatairesAffectation.TO_STRUCTURE_KEY+"=%@", new NSArray(getCurrentService())));

		if (!StringCtrl.chaineVide(myView.getTfFiltrePeriodeDebut().getText())) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(
					EOVacataires.DATE_DEBUT_KEY, 
					CocktailUtilities.getDateFromField(myView.getTfFiltrePeriodeDebut()), 
					EOVacataires.DATE_FIN_KEY, 
					CocktailUtilities.getDateFromField(myView.getTfFiltrePeriodeFin())));
		}

		return new EOAndQualifier(qualifiers);
	}


	/**
	 * 
	 */
	private void rechercher() {

		CRICursor.setWaitCursor(myView);
		eodVacations.setObjectArray(EOVacataires.findForQualifier(getEdc(), filterQualifier()));
		myView.getMyEOTable().updateData();
		CocktailUtilities.setTextToLabel(myView.getLblNbVacations(), eodVacations.displayedObjects().count() + " Vacations");
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void exporter() {

		try {
			JFileChooser saveDialog= new JFileChooser();
			saveDialog.setDialogTitle("Enregistrer sous");
			saveDialog.setDialogType(JFileChooser.SAVE_DIALOG);

			String nomFichierExport = "VACATIONS_" + StringCtrl.replace(myView.getTfFiltrePeriodeDebut().getText(), "/","")+"-"+StringCtrl.replace(myView.getTfFiltrePeriodeFin().getText(), "/","");

			saveDialog.setSelectedFile(new File(nomFichierExport + CocktailConstantes.EXTENSION_CSV));

			if (saveDialog.showSaveDialog(myView) == JFileChooser.APPROVE_OPTION) {

				File file = saveDialog.getSelectedFile();
				CRICursor.setWaitCursor(myView);

				String texte = enteteExport();

				for (EOVacataires myRecord : new NSArray<EOVacataires>(EOSortOrdering.sortedArrayUsingKeyOrderArray(eodVacations.displayedObjects(), EOVacataires.SORT_ARRAY_NOM_ASC) ))
					texte += texteExportPourRecord(myRecord) + CocktailExports.SAUT_DE_LIGNE;

				UtilitairesFichier.enregistrerFichier(texte, file.getParent(), nomFichierExport, "csv", false);

				CRICursor.setDefaultCursor(myView);

				CocktailUtilities.openFile(file.getPath());			
				myView.toFront();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String enteteExport() {

		String entete = "";

		entete += "NOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "PRENOM";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "DEBUT";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "FIN";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "HEURES";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "TAUX";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "SERVICE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "ENS.";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "TIT.";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "SIGNE";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "EMPLOYEUR";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "CNU";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "Libellé CNU";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "Enseignement";
		entete += CocktailExports.SEPARATEUR_EXPORT;
		entete += "Commentaires";
		entete += CocktailExports.SAUT_DE_LIGNE;

		return entete;
	}

	private String texteExportPourRecord(EOVacataires record)    {

		String texte = "";

		NSArray<EOVacatairesAffectation> affectations = EOVacatairesAffectation.fetchForVacation(getEdc(), record);

		int indexVacations = 0;
		if (affectations.count() > 0) {
			for (EOVacatairesAffectation myAffectation : affectations) {			
				// NOM
				texte += CocktailExports.ajouterChamp(record.toIndividu().nomUsuel());
				// PRENOM
				texte += CocktailExports.ajouterChamp(record.toIndividu().prenom());
				// DEBUT
				texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.dateDebut()));
				// FIN
				texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.dateFin()));
				// HEURES
				texte += CocktailExports.ajouterChamp(StringCtrl.replace(myAffectation.nbrHeures().toString(), ".", ","));
				// TAUX
				if (record.tauxHoraire() != null)
					texte += CocktailExports.ajouterChamp(StringCtrl.replace(record.tauxHoraire().toString(), ".", ","));
				else
					texte += CocktailExports.ajouterChampVide();
				// SERVICE
				texte += CocktailExports.ajouterChamp(myAffectation.toStructure().llStructure());
				// ENSEIGNANT
				texte += CocktailExports.ajouterChamp(record.temEnseignant());
				// TITULAIRE
				texte += CocktailExports.ajouterChamp(record.temTitulaire());
				// SIGNE
				texte += CocktailExports.ajouterChamp(record.temSigne());
				// EMPLOYEUR
				if (record.toStructure() != null)	
					texte += CocktailExports.ajouterChamp(record.toStructure().llStructure());
				else
					texte += CocktailExports.ajouterChampVide();

				// CNU
				if (record.toCnu() != null) {
					String cnu = record.toCnu().code();
					if (record.toCnu().codeSousSection() != null)
						cnu += "-" + record.toCnu().codeSousSection();
					texte += CocktailExports.ajouterChamp(cnu);
					texte += CocktailExports.ajouterChamp(record.toCnu().libelleLong());
				}
				else {
					texte += CocktailExports.ajouterChampVide();
					texte += CocktailExports.ajouterChampVide();
				}

				// Enseignement
				texte += CocktailExports.ajouterChamp(record.enseignement());

				// Commentaires
				texte += CocktailExports.ajouterChamp(record.observations());


				indexVacations ++;
				if (indexVacations < affectations.count())
					texte += CocktailExports.SAUT_DE_LIGNE;
			}
		}
		else {
			// NOM
			texte += CocktailExports.ajouterChamp(record.toIndividu().nomUsuel());
			// PRENOM
			texte += CocktailExports.ajouterChamp(record.toIndividu().prenom());
			// DEBUT
			texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.dateDebut()));
			// FIN
			texte += CocktailExports.ajouterChamp(DateCtrl.dateToString(record.dateFin()));
			// HEURES
			texte += CocktailExports.ajouterChamp(record.nbrHeures().toString());

			// TAUX
			if (record.tauxHoraire() != null)
				texte += CocktailExports.ajouterChamp(StringCtrl.replace(record.tauxHoraire().toString(), ".", ","));
			else
				texte += CocktailExports.ajouterChampVide();

			// SERVICE
			texte += CocktailExports.ajouterChampVide();
			// ENSEIGNANT
			texte += CocktailExports.ajouterChamp(record.temEnseignant());
			// TITULAIRE
			texte += CocktailExports.ajouterChamp(record.temTitulaire());
			// SIGNE
			texte += CocktailExports.ajouterChamp(record.temSigne());
			// EMPLOYEUR
			if (record.toStructure() != null)	
				texte += CocktailExports.ajouterChamp(record.toStructure().llStructure());
			else
				texte += CocktailExports.ajouterChampVide();
			// CNU
			if (record.toCnu() != null) {
				String cnu = record.toCnu().code();
				if (record.toCnu().codeSousSection() != null)
					cnu += "-" + record.toCnu().codeSousSection();
				texte += CocktailExports.ajouterChamp(cnu);
				texte += CocktailExports.ajouterChamp(record.toCnu().libelleLong());
			}
			else {
				texte += CocktailExports.ajouterChampVide();
				texte += CocktailExports.ajouterChampVide();
			}

			// Enseignement
			texte += CocktailExports.ajouterChamp(record.enseignement());
			// Commentaires
			texte += CocktailExports.ajouterChamp(record.observations());

		}

		return texte;
	}

	/**
	 * 
	 */
	private void ajouter() {

		EOVacataires newVacataire = SaisieVacationCtrl.sharedInstance(getEdc()).ajouter(null);

		if (newVacataire != null) {
			actualiser();
			myView.getMyEOTable().forceNewSelectionOfObjects(new NSArray(newVacataire));
		}
	}

	/**
	 * 
	 */
	private void modifier() {
		SaisieVacationCtrl.sharedInstance(getEdc()).modifier(getCurrentVacation());
		myView.getMyEOTable().updateUI();
		myView.getMyEOTableAff().updateUI();
	}

	/**
	 * Ajout d'une affectation. 
	 * Verifier si le nombre d'heures est bien > 0
	 */
	private void ajouterAffectation() {

		if (getCurrentVacation().nbrHeures() == null || currentVacation.nbrHeures().intValue() == 0) {
			EODialogs.runErrorDialog("ERREUR","Veuillez auparavant renseigner un nombre d'heures de vacation !");
			return;
		}

		BigDecimal heuresAffectees = CocktailUtilities.computeSumForKey(eodAffectations.displayedObjects(), EOVacatairesAffectation.NBR_HEURES_KEY);
		ctrlSaisieAff.setAffectationsExistantes(eodAffectations.displayedObjects());
		ctrlSaisieAff.ajouter(getCurrentVacation(), heuresAffectees);
		listenerVacation.onSelectionChanged();
	}

	/**
	 * 
	 */
	private void modifierEtatCivil() {

		CRICursor.setWaitCursor(myView);
		SaisieIdentiteVacataireCtrl.sharedInstance(getEdc()).open(getCurrentVacation().toIndividu());
		myView.getMyEOTable().updateUI();
		CRICursor.setDefaultCursor(myView);

	}

	/**
	 * 
	 */
	private void modifierAffectation() {

		if (getCurrentVacation().nbrHeures() == null || getCurrentVacation().nbrHeures().intValue() == 0) {
			EODialogs.runErrorDialog("ERREUR","Veuillez auparavant renseigner un nombre d'heures de vacation !");
			return;
		}

		ctrlSaisieAff.setAffectationsExistantes(eodAffectations.displayedObjects());
		ctrlSaisieAff.modifier(getCurrentAffectation());
		myView.getMyEOTableAff().updateUI();
	}

	/**
	 * 
	 */
	private void supprimer() {

		if(!EODialogs.runConfirmOperationDialog("Attention", "Voulez-vous vraiment supprimer cette vacation ?", "Oui", "Non"))		
			return;

		try {
			getEdc().deleteObject(getCurrentVacation());
			getEdc().saveChanges();
			eodVacations.deleteSelection();
			myView.getMyEOTable().updateData();

		}
		catch(Exception e) {
			getEdc().revert();
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void supprimerAffectation() {

		if(!EODialogs.runConfirmOperationDialog("Attention", 
				"Confirmez-vous la suppression de cette période d'affectation ?", "Oui", "Non"))		
			return;			

		try {
			getEdc().deleteObject(getCurrentAffectation());			
			getEdc().saveChanges();

			NSArray<EOVacatairesAffectation> affectations = EOVacatairesAffectation.fetchForVacation(getEdc(), getCurrentVacation());
			if (affectations.size() == 1) {
				affectations.get(0).setEstPrincipale(true);
			}		
			getEdc().saveChanges();

			listenerVacation.onSelectionChanged();
		}
		catch (ValidationException vex) {
			EODialogs.runErrorDialog("ERREUR", vex.getMessage());
			getEdc().revert();
		}
		catch (Exception e) {
			e.printStackTrace();
			getEdc().revert();
		}
	}

	/**
	 * 
	 */
	private void updateInterface() {

		myView.getBtnAjouter().setEnabled(true);
		myView.getBtnModifier().setEnabled(getCurrentVacation() != null);
		myView.getBtnSupprimer().setEnabled(getCurrentVacation() != null);
		myView.getBtnImprimer().setEnabled(getCurrentVacation() != null);
		myView.getBtnDupliquer().setEnabled(getCurrentVacation() != null);

		myView.getBtnAjouterAffectation().setEnabled(getCurrentVacation() != null);
		myView.getBtnModifierAffectation().setEnabled(getCurrentAffectation() != null);
		myView.getBtnSupprimerAffectation().setEnabled(getCurrentAffectation() != null);

		if (saisieEnabled() || isLocked())
			myView.getMyEOTable().setForeground(CocktailConstantes.BG_COLOR_GREY);
		else
			myView.getMyEOTable().setForeground(CocktailConstantes.BG_COLOR_BLACK);

		myView.getBtnAdresses().setEnabled(getCurrentVacation() != null && getCurrentVacation().estPersonnelEtablissement() == false);
		myView.getBtnTelephone().setEnabled(getCurrentVacation() != null && getCurrentVacation().estPersonnelEtablissement() == false);
		myView.getBtnBudget().setEnabled(getCurrentVacation() != null);

	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			actualiser();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class ListenerVacation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			modifier();
		}

		public void onSelectionChanged() {

			setCurrentVacation((EOVacataires)eodVacations.selectedObject());
			eodAffectations.setObjectArray(new NSArray());

			if (getCurrentVacation() != null) {
				eodAffectations.setObjectArray(EOVacatairesAffectation.fetchForVacation(getEdc(), getCurrentVacation()));
			}

			myView.getMyEOTableAff().updateData();
			updateInterface();
		}
	}
	private class ListenerAffectation implements org.cocktail.application.client.swing.ZEOTable.ZEOTableListener {

		public void onDbClick()	{
			modifierAffectation();
		}

		public void onSelectionChanged() {
			setCurrentAffectation((EOVacatairesAffectation)eodAffectations.selectedObject());
			updateInterface();
		}
	}

	private void dateHasChanged(JTextField myTextField) {
		if ("".equals(myTextField.getText()))	return;

		String myDate = DateCtrl.dateCompletion(myTextField.getText());
		if ("".equals(myDate))	{
			myTextField.selectAll();
			EODialogs.runInformationDialog("Date non valide","La date saisie n'est pas valide !");
		}
		else {
			myTextField.setText(myDate);
			//actualiser();
		}
	}
	private class ActionListenerDateTextField implements ActionListener	{
		private JTextField myTextField;
		private ActionListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}		
		public void actionPerformed(ActionEvent e)	{
			dateHasChanged(myTextField);
		}
	}
	private class FocusListenerDateTextField implements FocusListener	{
		private JTextField myTextField;		
		private FocusListenerDateTextField(JTextField textField) {
			myTextField = textField;
		}
		public void focusGained(FocusEvent e) 	{}
		public void focusLost(FocusEvent e)	{
			dateHasChanged(myTextField);			
		}
	}
}

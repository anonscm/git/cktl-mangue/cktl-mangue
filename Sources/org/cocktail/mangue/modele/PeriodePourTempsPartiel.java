/*
 * Created on 29 nov. 2005
 *
 * Modélise les périodes de temps partiel avec interruption éventuelle
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

/** Mod&eacute;lise les p&eacute;riodes de temps partiel avec interruption &eacute;ventuelle.<BR>
 * Les objets sont g&eacute;n&eacute;r&eacute;s &agrave; la vol&eacute;e et non sauvegard&eacute;s dans la base
 * Impl&eacute;mente NSKeyValueCoding
 * @author christine
 *
 */
public class PeriodePourTempsPartiel implements NSKeyValueCoding {
	private NSTimestamp dateDebut;
	private NSTimestamp dateFin;
	private String motif;
	private int type;
	public static int TYPE_PERIODE_NORMALE = 0;
	public static int TYPE_PERIODE_INTERRUPION = 1;
	public static int TYPE_PERIODE_SUSPENSION = 2;
	
	// constructeur
	/**	constructeur pour une p&eacute;riode de temps partiel
	 * @param dateDebut	d&eacute;but de la p&eacute;riode
	 * @param dateFin	date fin de l'interruption de temps partiel (peut &ecirc;tre nulle)
	 * @param motif	motif	d'interruption (peut &ecirc;tre nul si pas d'interruption)
	 * @param type de p&eacute;riode (NORMALE, INTERRUPTION, SUSPENSION)
	 */
	public PeriodePourTempsPartiel(NSTimestamp dateDebut,NSTimestamp dateFin,String motif,int type) {
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.motif = motif;
		this.type = type;
	}
	// Accesseurs
	public NSTimestamp dateDebut() {
		return dateDebut;
	}
	public NSTimestamp dateFin() {
		return dateFin;
	}
	public String motif() {
		return motif;
	}
	public int type() {
		return type;
	}
	public String toString() {
		return "date début: " + dateDebut + ", date fin " + dateFin + ", motif " + motif + ", type " + type;
	}
	//	 interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
}

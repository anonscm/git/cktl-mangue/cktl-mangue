/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class StatutIndividu implements NSKeyValueCoding,NSCoding {
	private EOIndividu individu;
	private	EOStructure structureAffectation;
	private String statut;
	private NSTimestamp dateDebut;
	private NSTimestamp dateFin;
	private EOEditingContext editingContext;
	
	// constructeurs
	public StatutIndividu(IDureePourIndividu evenement,EOStructure structureAffectation) {
		this.individu = evenement.individu();
		this.editingContext = individu.editingContext();
		this.structureAffectation = structureAffectation;
		this.statut = evenement.typeEvenement();
		if (evenement.dateDebut() != null) {
			this.dateDebut = evenement.dateDebut();
		}
		if (evenement.dateFin() != null) {
			this.dateFin = evenement.dateFin();
		}
	}
	public StatutIndividu(NSDictionary aDict) {
		editingContext = new EOEditingContext();
		EOGlobalID globalID = (EOGlobalID)aDict.objectForKey("individu");
		this.individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(globalID, editingContext);
		globalID = (EOGlobalID)aDict.objectForKey("structure");
		this.structureAffectation = (EOStructure)SuperFinder.objetForGlobalIDDansEditingContext(globalID, editingContext);
		statut = (String)aDict.objectForKey("statut");
		String debut =  (String)aDict.objectForKey("dateDebut");
		if (debut != null) {
			dateDebut = DateCtrl.stringToDate(debut);
		}
		String fin = (String)aDict.objectForKey("dateFin");
		if (fin != null && fin.length() > 0) {
			dateFin = DateCtrl.stringToDate(fin);
		} 
	}
	// Accesseurs
	public EOEditingContext editingContext() {
		return editingContext;
	}
	public EOIndividu individu() {
		return individu;
	}
	public EOStructure structureAffectation() {
		return structureAffectation;
	}
	public String statut() {
		return statut;
	}
	public NSTimestamp dateDebut() {
		return dateDebut;
	}
	public void setDateDebut(NSTimestamp debut) {
		this.dateDebut = debut;
	}
	public NSTimestamp dateFin() {
		return dateFin;
	}
	public void setDateFin(NSTimestamp fin) {
		this.dateFin = fin;
	}
	public String dateDebutFormatee() {
		return DateCtrl.dateToString(dateDebut());
	}
	public String dateFinFormatee() {
		if (dateFin() != null) {
			return DateCtrl.dateToString(dateFin ());
		} else {
			return "";
		}
	}
	// Interface key-value Coding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	public Class classForCoder() {
		return StatutIndividu.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		NSMutableDictionary dict = new NSMutableDictionary();
		dict.setObjectForKey(editingContext.globalIDForObject(individu), "individu");
		dict.setObjectForKey(editingContext.globalIDForObject(structureAffectation), "structure");
		dict.setObjectForKey(statut, "statut");
		dict.setObjectForKey(dateDebutFormatee(), "dateDebut");
		dict.setObjectForKey(dateFinFormatee(), "dateFin");

		coder.encodeObject(dict);
	}
	public static Class decodeClass() {
		return StatutIndividu.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new StatutIndividu((NSDictionary)coder.decodeObject());
	}
	
}

/*
 * Created on 22 sept. 2005
 *
 * Classe modélisant la fiche de synthèse de carrière
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import java.math.BigDecimal;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOService;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.cir.EOBeneficeEtudes;
import org.cocktail.mangue.modele.mangue.cir.EOBonifications;
import org.cocktail.mangue.modele.mangue.cir.EOEtudesRachetees;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodesMilitaires;
import org.cocktail.mangue.modele.mangue.individu.EOStage;
import org.cocktail.mangue.modele.mangue.individu.EOValidationServices;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 *
 * Fiche de synthese de carriere
 */
public class FicheSynthese implements NSKeyValueCoding {

	public static final String CORPS_KEY = "corps";
	public static final String GRADE_KEY = "grade";
	public static final String ECHELON_KEY = "echelon";
	public static final String QUOTITE_KEY = "quotite";
	public static final String LC_POPULATION_KEY = "population";
	public static final String LL_POPULATION_KEY = "populationLong";
	public static final String STATUT_KEY = "statut";
	public static final String TEM_PC_ACQUITEES_KEY = "pcAcquitees";
	public static final String DATE_DEBUT_KEY = "periodeDeb";
	public static final String DATE_FIN_KEY = "periodeFin";
	public static final String INDIVIDU_KEY = "individu";

	public static final String SV_ANNEES_KEY = "svAnnees";
	public static final String SV_MOIS_KEY = "svMois";
	public static final String SV_JOURS_KEY = "svJours";

	private String corps,grade,echelon;
	private String population, populationLong;
	private String statut, pcAcquitees;
	private BigDecimal quotite;
	private Integer svAnnees, svJours, svMois;
	private NSTimestamp periodeDeb,periodeFin;
	private EOIndividu individu;

	public FicheSynthese() {
		super();
	}
	public String corps() {
		return corps;
	}
	public void setCorps(String value) {
		corps = value;
	}
	public String grade() {
		return grade;
	}
	public void setGrade(String value) {
		grade = value;
	}
	public String echelon() {
		return echelon;
	}
	public void setEchelon(String value) {
		echelon = value;
	}


	public String pcAcquitees() {
		return pcAcquitees;
	}
	public void setPcAcquitees(String value) {
		pcAcquitees = value;
	}


	public BigDecimal quotite() {
		return quotite;
	}
	public void setQuotite(BigDecimal value) {
		quotite = value;
	}


	public Integer svAnnees() {
		return svAnnees;
	}
	public void setSvAnnees(Integer value) {
		svAnnees = value;
	}
	public Integer svMois() {
		return svMois;
	}
	public void setSvMois(Integer value) {
		svMois = value;
	}
	public Integer svJours() {
		return svJours;
	}
	public void setSvJours(Integer value) {
		svJours = value;
	}


	public NSTimestamp periodeDeb() {
		return periodeDeb;
	}
	public void setPeriodeDeb(NSTimestamp value) {
		periodeDeb = value;
	}
	public NSTimestamp periodeFin() {
		return periodeFin;
	}
	public void setPeriodeFin(NSTimestamp value) {
		periodeFin = value;
	}

	public String populationLong() {
		return populationLong;
	}
	public void setPopulationLong(String value) {
		populationLong = value;
	}

	public String population() {
		return population;
	}
	public void setPopulation(String value) {
		population = value;
	}
	public String statut() {
		return statut;
	}
	public void setStatut(String value) {
		statut = value;
	}
	public EOIndividu individu() {
		return individu;
	}
	public void setIndividu(EOIndividu value) {
		individu = value;
	}
	// méthodes ajoutées
	public void initAvecIndividu(EOIndividu individu) {
		this.individu = individu;
		setPcAcquitees(CocktailConstantes.VRAI);
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// méthodes privées
	private void initAvecIndividuDatesEtElementCarriere(EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin,EOElementCarriere element) {
		initAvecIndividu(individu);
		setPeriodeDeb(dateDebut);
		setPeriodeFin(dateFin);
		setCorps(element.toCorps().lcCorps());
		setGrade(element.toGrade().lcGrade());
		setEchelon(element.cEchelon());
	}
	// méthodes statiques
	/** retourne un tableau contenant les fiches de synthese de la carriere d'un individu a une date donnee
	 * @param editingContext
	 * @param individu
	 * @param date jusqu'a laquelle on recherche les informations de carriere
	 * @return tableau de fiche de synthese
	 */
	public static NSArray getArrayCarrieres(EOEditingContext edc,EOIndividu individu, NSTimestamp date) {

		NSArray<EOCarriere> results = EOCarriere.carrieresAnterieuresADate(edc, individu, date);
		NSMutableArray resultArray = new NSMutableArray();

		int indexCarrieres = 1;
		for (EOCarriere carriere : results) {

			if (carriere.elements().size() == 0) {

				EOChangementPosition position = carriere.evaluerPositionADate(date);

				FicheSynthese fiche = new FicheSynthese();
				fiche.initAvecIndividu(individu);

				if (position  != null)
					//					&& position.temPcAcquitee() != null 
					//						&& (position.estDetachement() || position.estEnDispo()) )
					fiche.setPcAcquitees(position.temPcAcquitee());

				if (carriere.toTypePopulation() != null) {
					fiche.setPopulation(carriere.toTypePopulation().code());
					fiche.setPopulationLong(carriere.toTypePopulation().libelleCourt());
				}

				fiche.setPeriodeDeb(carriere.dateDebut());
				fiche.setPeriodeFin(carriere.dateFin());

				if (position != null && position.toPosition() != null) {
					fiche.setStatut(position.toPosition().code());
				}
				resultArray.addObject(fiche);

			} else {

				for (EOElementCarriere element :  (NSArray<EOElementCarriere>)carriere.elements()) {

					// ne prendre en compte que les éléments valides qui ne sont pas annulés
					if (element.estValide() && !element.estProvisoire() && element.dateArreteAnnulation() == null && element.noArreteAnnulation() == null) {
						NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(edc, individu, element.dateDebut(), element.dateFin());
						changements = EOSortOrdering.sortedArrayUsingKeyOrderArray(changements,EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);

						if (changements != null && changements.count() > 0) {
							// Créer autant d'entrées que de changement de position trouvés
							for (EOChangementPosition position : changements) {

								NSTimestamp dateDebut = element.dateDebut(),dateFin = element.dateFin();
								if (DateCtrl.isAfter(position.dateDebut(),dateDebut)) {
									dateDebut = position.dateDebut();
								}
								if (position.dateFin() != null) {
									if (dateFin == null || (dateFin != null && DateCtrl.isBefore(position.dateFin(), dateFin))) {
										dateFin = position.dateFin();
									}
								}
								FicheSynthese fiche = new FicheSynthese();
								fiche.initAvecIndividuDatesEtElementCarriere(individu, dateDebut, dateFin, element);
								fiche.setStatut(position.toPosition().code());
								fiche.setPopulation(carriere.toTypePopulation().code());

								if ( position.temPcAcquitee() != null) {
									fiche.setPcAcquitees(position.temPcAcquitee());
								}

								//FIXME Calcul de la quotite dans la fiche de synthese
								fiche.setQuotite(new BigDecimal(position.quotite()));
								EOModalitesService modalite = EOModalitesService.modalitePourDate(edc, individu, dateDebut);
								if (modalite != null && !modalite.estAnnule() 
										&& !modalite.estCrct() && !modalite.estDelegation()) {
									fiche.setQuotite(modalite.quotite());
								}
								//new BigDecimal(position.quotitePosition()));

								fiche.setPopulationLong(carriere.toTypePopulation().libelleCourt());
								resultArray.addObject(fiche);
							}
						} else {
							FicheSynthese fiche = new FicheSynthese();
							fiche.initAvecIndividuDatesEtElementCarriere(individu,element.dateDebut(),element.dateFin(),element);
							resultArray.addObject(fiche);
						}
					}
				}
			}
			indexCarrieres ++;
		}
		return 	(NSArray)resultArray;
	}


	/**
	 * 
	 * @param edc
	 * @param individu
	 * @param date
	 * @return
	 */
	public static NSArray<FicheSynthese> getArrayPasseSyntheseCIR(EOEditingContext edc,EOIndividu individu, NSTimestamp date) {

		NSMutableArray<EOPasse>results = new NSMutableArray<EOPasse>(EOPasse.rechercherPassesAnterieursADate(edc,individu, date));

		NSMutableArray<FicheSynthese> resultArray = new NSMutableArray<FicheSynthese>();


		for (EOPasse myPasse : results) {

			if (!myPasse.toTypeService().estTypeServiceNonValide()) {

				FicheSynthese fiche = new FicheSynthese();
				fiche.initAvecIndividu(individu);
				fiche.setPopulationLong("PASSE " + myPasse.toTypeService().libelleCourt());

				EOChangementPosition chgt = EOChangementPosition.findForPasse(edc, myPasse);
				if (chgt != null) {
					fiche.setStatut(chgt.toPosition().code());				
				}
				else
					fiche.setStatut("ACTI");

				fiche.setPeriodeDeb(myPasse.dateDebut());
				fiche.setPeriodeFin(myPasse.dateFin());
				fiche.setPcAcquitees(myPasse.pasPcAcquitee());
				fiche.setSvAnnees(myPasse.dureeValideeAnnees());
				fiche.setSvMois(myPasse.dureeValideeMois());
				fiche.setSvJours(myPasse.dureeValideeJours());

				resultArray.addObject(fiche);

			}
		}
		return 	resultArray.immutableClone();
	}


	public static NSArray<FicheSynthese> getArrayPasseSyntheseCarriere(EOEditingContext edc,EOIndividu individu, NSTimestamp date) {

		NSArray<EOPasse> results = EOPasse.rechercherPassesPourSyntheseCarriere(edc,individu, date);
		NSMutableArray<FicheSynthese> resultArray = new NSMutableArray<FicheSynthese>();

		for (EOPasse myPasse : results) {

			FicheSynthese fiche = new FicheSynthese();
			fiche.initAvecIndividu(individu);
			fiche.setPopulationLong("PASSE");
			fiche.setPeriodeDeb(myPasse.dateDebut());
			fiche.setPeriodeFin(myPasse.dateFin());
			fiche.setPcAcquitees(myPasse.pasPcAcquitee());
			fiche.setSvAnnees(myPasse.dureeValideeAnnees());
			fiche.setSvMois(myPasse.dureeValideeMois());
			fiche.setSvJours(myPasse.dureeValideeJours());

			resultArray.addObject(fiche);
		}
		return 	resultArray.immutableClone();
	}

	public static NSArray<FicheSynthese> getArrayServicesValides(EOEditingContext editingContext,EOIndividu individu, NSTimestamp date) {

		NSArray<EOValidationServices> results = Duree.rechercherDureesPourEntiteAnterieuresADate(editingContext, EOValidationServices.ENTITY_NAME, individu, date);
		NSMutableArray<FicheSynthese> resultArray = new NSMutableArray<FicheSynthese>();

		for (EOValidationServices validation : results) {

			FicheSynthese fiche = new FicheSynthese();
			fiche.initAvecIndividu(individu);
			fiche.setPopulationLong("SVAL");
			fiche.setPeriodeDeb(validation.dateDebut());
			fiche.setPeriodeFin(validation.dateFin());
			fiche.setPcAcquitees(validation.valPcAcquitee());
			fiche.setSvAnnees(validation.valAnnees());
			fiche.setSvMois(validation.valMois());
			fiche.setSvJours(validation.valJours());

			resultArray.addObject(fiche);
		}
		return 	resultArray.immutableClone();
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @return
	 */
	public static NSArray<FicheSynthese> getArrayServiceNational(EOEditingContext editingContext, EOIndividu individu) {

		NSArray results = EOPeriodesMilitaires.findForIndividu(editingContext,individu);
		NSMutableArray resultArray = new NSMutableArray();

		for (java.util.Enumeration<EOPeriodesMilitaires> e = results.objectEnumerator();e.hasMoreElements();) {

			EOPeriodesMilitaires periode = e.nextElement();

			// On verifie qu'une position n'ait pas ete saisie pour la meme periode
			if (! EOChangementPosition.individuAuServiceMilitairePourPeriode(editingContext, individu, periode.dateDebut(), periode.dateFin())) {

				FicheSynthese fiche = new FicheSynthese();
				fiche.initAvecIndividu(individu);

				fiche.setPopulationLong("MILITAIRE");
				fiche.setStatut("MIL");

				fiche.setPeriodeDeb(periode.dateDebut());
				fiche.setPeriodeFin(periode.dateFin());
				fiche.setPcAcquitees(null);
				fiche.setSvAnnees(null);
				fiche.setSvMois(null);
				fiche.setSvJours(null);

				resultArray.addObject(fiche);
			}

		}
		return 	(NSArray<FicheSynthese>)resultArray;
	}



	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @return
	 */
	public static NSArray<EOContratAvenant> getArrayContratsServicesValides(EOEditingContext editingContext,EOIndividu individu, NSTimestamp date) {

		NSArray results = EOContratAvenant.rechercherAvenantsPourIndividuAvecServicesValidesAnterieursADate(editingContext,individu, date);
		NSMutableArray resultArray = new NSMutableArray();

		for (java.util.Enumeration<EOContratAvenant> e = results.objectEnumerator();e.hasMoreElements();) {

			EOContratAvenant contrat = e.nextElement();

			FicheSynthese fiche = new FicheSynthese();
			fiche.initAvecIndividu(individu);
			fiche.setPopulationLong("PASSE");

			fiche.setPopulationLong(contrat.contrat().toTypeContratTravail().code());
			fiche.setCorps(contrat.toGrade().toCorps().lcCorps());
			fiche.setGrade(contrat.toGrade().lcGrade());
			if (contrat.cEchelon() != null)
				fiche.setEchelon(contrat.cEchelon());

			fiche.setPeriodeDeb(contrat.dateDebut());
			fiche.setPeriodeFin(contrat.dateFin());

			fiche.setPcAcquitees(contrat.ctraPcAcquitees());

			fiche.setSvAnnees(contrat.ctraDureeValideeAnnees());
			fiche.setSvMois(contrat.ctraDureeValideeMois());
			fiche.setSvJours(contrat.ctraDureeValideeJours());

			resultArray.addObject(fiche);
		}
		return 	(NSArray)resultArray;
	}



	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @return
	 */
	public static NSArray<EOModalitesService> getArrayModaliteServices(EOEditingContext editingContext,EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {

		NSArray<EOModalitesService> results = EOModalitesService.rechercherPourIndividuEtPeriode(editingContext, individu, debutPeriode, finPeriode);
		NSMutableArray resultArray = new NSMutableArray();

		for (EOModalitesService modalite : results) {

			if (!modalite.estAnnule()) {

				FicheSynthese fiche = new FicheSynthese();
				fiche.initAvecIndividu(individu);

				if (modalite.modLibelle() != null)
					fiche.setPopulationLong(modalite.modLibelle());

				if (modalite.quotite() != null)
					fiche.setQuotite(modalite.quotite());

				// Traitement de la quotite pour un CRCT
				if (modalite.estCrct() || modalite.estDelegation()) {
					fiche.setQuotite(ManGUEConstantes.QUOTITE_100);
				}

				fiche.setPeriodeDeb(modalite.dateDebut());
				fiche.setPeriodeFin(modalite.dateFin());

				resultArray.addObject(fiche);
			}
		}
		return 	(NSArray)resultArray;

	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @return
	 */
	public static NSArray<FicheSynthese> getArrayBonifications(EOEditingContext editingContext,EOIndividu individu) {

		NSArray results = EOBonifications.findForIndividu(editingContext,individu);
		NSMutableArray<FicheSynthese> resultArray = new NSMutableArray<FicheSynthese>();

		for (java.util.Enumeration<EOBonifications> e = results.objectEnumerator();e.hasMoreElements();) {

			EOBonifications object = e.nextElement();

			FicheSynthese fiche = new FicheSynthese();
			fiche.initAvecIndividu(individu);

			fiche.setPopulationLong("Bonifications");
			fiche.setStatut("BONI");
			fiche.setQuotite(ManGUEConstantes.QUOTITE_100);

			fiche.setPeriodeDeb(object.dateDebut());
			fiche.setPeriodeFin(object.dateFin());

			resultArray.addObject(fiche);
		}
		return 	resultArray.immutableClone();

	}


	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @return
	 */
	public static NSArray<FicheSynthese> getArrayEtudesRachetees(EOEditingContext editingContext,EOIndividu individu) {

		NSArray results = EOEtudesRachetees.findForIndividu(editingContext,individu);
		NSMutableArray<FicheSynthese> resultArray = new NSMutableArray<FicheSynthese>();

		for (java.util.Enumeration<EOEtudesRachetees> e = results.objectEnumerator();e.hasMoreElements();) {

			EOEtudesRachetees object = e.nextElement();

			FicheSynthese fiche = new FicheSynthese();
			fiche.initAvecIndividu(individu);

			fiche.setPopulationLong("Rachat Etu.");
			fiche.setStatut("ETU");

			fiche.setQuotite(ManGUEConstantes.QUOTITE_100);

			fiche.setPeriodeDeb(object.dateDebut());
			fiche.setPeriodeFin(object.dateFin());

			resultArray.addObject(fiche);
		}
		return resultArray.immutableClone();

	}


	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @return
	 */
	public static NSArray<FicheSynthese> getArrayBeneficesEtudes(EOEditingContext editingContext,EOIndividu individu) {

		NSArray results = EOBeneficeEtudes.findForIndividu(editingContext,individu);
		NSMutableArray<FicheSynthese> resultArray = new NSMutableArray<FicheSynthese>();

		for (java.util.Enumeration<EOBeneficeEtudes> e = results.objectEnumerator();e.hasMoreElements();) {

			EOBeneficeEtudes object = e.nextElement();

			FicheSynthese fiche = new FicheSynthese();
			fiche.initAvecIndividu(individu);

			fiche.setPopulationLong("Benef Etu.");
			fiche.setStatut("BENEF");

			fiche.setQuotite(ManGUEConstantes.QUOTITE_100);

			fiche.setPeriodeDeb(object.dateDebut());
			fiche.setPeriodeFin(object.dateFin());

			resultArray.addObject(fiche);
		}
		return 	resultArray.immutableClone();

	}



	/** retourne un tableau contenant les fiches de synthese des contrats d'un individu a une date donnee
	 * @param editingContext
	 * @param individu
	 * @param date jusqu'a laquelle on recherche les informations de contrat
	 * @return tableau de fiche de synthese
	 */
	public static NSArray<FicheSynthese> getArrayContrats(EOEditingContext editingContext,EOIndividu individu, NSTimestamp date) {

		NSMutableArray<FicheSynthese> resultArray = new NSMutableArray<FicheSynthese>();
		NSArray<EOContrat> contrats = EOContrat.rechercherContratsIndividuAnterieursADate(editingContext,individu,date);

		for (EOContrat contrat : contrats) {

			if (contrat.avenants().count() == 0 || (contrat.avenants().count() == 1 && ((EOContratAvenant)contrat.avenants().objectAtIndex(0)).estAnnule())) {
				FicheSynthese fiche = new FicheSynthese();
				fiche.initAvecIndividu(individu);
				fiche.setPopulationLong("CONTRAT");
				fiche.setQuotite(new BigDecimal("100"));
				fiche.setPeriodeDeb(contrat.dateDebut());
				// 28/09/09 - prise en compte de la date de fin anticipée
				if (contrat.dateFinAnticipee() != null) {
					fiche.setPeriodeFin(contrat.dateFinAnticipee());
				} else {
					fiche.setPeriodeFin(contrat.dateFin());
				}
				fiche.setStatut(contrat.toTypeContratTravail().code());
				resultArray.addObject(fiche);
			} else {
				for (java.util.Enumeration<EOContratAvenant> e1 = contrat.avenants().objectEnumerator();e1.hasMoreElements();) {
					EOContratAvenant avenant = e1.nextElement();
					if (avenant.estAnnule() == false) {
						FicheSynthese fiche = new FicheSynthese();
						fiche.initAvecIndividu(individu);
						fiche.setPopulationLong("CONTRAT");
						fiche.setQuotite(new BigDecimal(avenant.quotite()));
						fiche.setPeriodeDeb(avenant.dateDebut());

						NSTimestamp dateFin = avenant.dateFin();
						if (contrat.dateFinAnticipee() != null && (dateFin == null || (dateFin != null && contrat.dateFinAnticipee() != null && DateCtrl.isBefore(contrat.dateFinAnticipee(), dateFin)))) {
							dateFin = contrat.dateFinAnticipee();
						}
						fiche.setPeriodeFin(dateFin);
						fiche.setStatut(contrat.toTypeContratTravail().code());
						if (avenant.toGrade() != null) {
							fiche.setGrade(avenant.toGrade().lcGrade());
							fiche.setCorps(avenant.toGrade().toCorps().lcCorps()); 
						}
						fiche.setEchelon(avenant.cEchelon());
						resultArray.addObject(fiche);
					}
				}
			}
		}
		return 	resultArray.immutableClone();
	}

	/** retourne un tableau contenant les fiches de synthese des absences d'un individu pour une periode donnee
	 * @param editingContext
	 * @param individu
	 * @param date jusqu'a laquelle on recherche les informations de contrat
	 * @param entiteAbsence nom de l'entite a rechercher (sous-classe de Duree.java)
	 * @param type de l'entitee
	 * @return tableau de fiche de synthese
	 */
	public static NSArray<FicheSynthese> getArrayAbsencesCIR(EOEditingContext edc,EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode, String entiteAbsence,String typeEntite) {
		NSArray<Duree> durees = Duree.rechercherDureesPourIndividuEtPeriode(edc, entiteAbsence, individu, debutPeriode, finPeriode);
		return getArrayFromDurees(individu, typeEntite, durees);
	}


	/** retourne un tableau contenant les fiches de synthese des absences d'un individu a une date donnee
	 * @param editingContext
	 * @param individu
	 * @param date jusqu'a laquelle on recherche les informations de contrat
	 * @param entiteAbsence nom de l'entite a rechercher (sous-classe de Duree.java)
	 * @param type de l'entitee
	 * @return tableau de fiche de synthese
	 */
	public static NSArray<FicheSynthese> getArrayAbsences(EOEditingContext editingContext,EOIndividu individu, NSTimestamp date,String entiteAbsence,String typeEntite) {

		NSArray<Duree> durees = Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext,entiteAbsence,individu,date);
		return getArrayFromDurees(individu, typeEntite, durees);
	}

	/**
	 * 
	 * @param individu
	 * @param typeEntite
	 * @param durees
	 * @return
	 */
	private static NSArray<FicheSynthese> getArrayFromDurees(EOIndividu individu, String typeEntite, NSArray<Duree> durees) {

		NSMutableArray<FicheSynthese> resultArray = new NSMutableArray<FicheSynthese>();
		for (Duree duree : durees) {
			boolean estValide = true;
			if (duree instanceof IValidite) {
				estValide = ((IValidite)duree).estValide();
			}
			if (estValide) {
				FicheSynthese fiche = new FicheSynthese();
				fiche.setPopulationLong("CONGE");
				fiche.initAvecIndividu(individu);
				fiche.setPeriodeDeb(duree.dateDebut());
				fiche.setPeriodeFin(duree.dateFin());
				fiche.setStatut(typeEntite);
				if (typeEntite.equals("STA")) {
					EOStage stage = (EOStage)duree;
					if (stage.corps() != null) {
						fiche.setCorps(stage.corps().lcCorps());
						if (stage.corps().toTypePopulation() != null) {
							fiche.setPopulation(stage.corps().toTypePopulation().code());
							fiche.setPopulationLong(stage.corps().toTypePopulation().libelleCourt());
						}
					} 
				} else {
					fiche.setCorps(typeEntite);
				}

				if (typeEntite.equals(EOTypeAbsence.TYPE_CONGE_MATERNITE) || typeEntite.equals(EOTypeAbsence.TYPE_CONGE_PATERNITE))
					fiche.setPcAcquitees("N");

				resultArray.addObject(fiche);
			}
		}
		return 	resultArray.immutableClone();
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import java.util.Enumeration;

import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageChevron;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOConservationAnciennete;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOReliquatsAnciennete;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class IndividuPromouvable implements NSKeyValueCoding, NSCoding  {
	private EOElementCarriere elementCarriere;
	private String echelonSuivant;
	private String chevronSuivant;
	private String datePromotion;
	private String dateEffet;
	private EOEditingContext edc;
	private EOIndice indiceSuivant;
	private String affectationComp;
	private String affectationStructure;
	private boolean doitAttendreDureeMinimumEchelon;	

	public IndividuPromouvable(EOElementCarriere element) {
		elementCarriere = element;
		edc = element.editingContext();
		preparerIndiceSuivant(edc);

	}
	public IndividuPromouvable(NSDictionary aDict) {
		edc = new EOEditingContext();
		EOGlobalID globalID = (EOGlobalID)aDict.objectForKey("element");
		elementCarriere = (EOElementCarriere)SuperFinder.objetForGlobalIDDansEditingContext(globalID, edc);
		echelonSuivant = (String)aDict.objectForKey("echelon");
		datePromotion = (String)aDict.objectForKey("datePromotion");
		doitAttendreDureeMinimumEchelon = ((Boolean)aDict.objectForKey("doitAttendreDureeMiniumEchelon")).booleanValue();
	}
	public EOEditingContext editingContext() {
		return edc;
	}
	public EOElementCarriere elementCarriere() {
		return elementCarriere;
	}

	public String nomUsuel() {
		return individu().nomUsuel();
	}
	public String prenom() {
		return individu().prenom();
	}
	public String civilite() {
		return individu().cCivilite();
	}
	public String lcGrade() {
		return grade().lcGrade();
	}

	public String llGrade() {
		return grade().llGrade();
	}

	public EOIndividu individu() {
		return elementCarriere.toIndividu();
	}
	public String corps() {
		return elementCarriere.toCorps().lcCorps();
	}
	public EOGrade grade() {
		return elementCarriere.toGrade();
	}
	public NSArray visas() {
		return elementCarriere.visas();
	}
	public String echelon() {
		return elementCarriere.cEchelon();
	}
	public String chevron() {
		return elementCarriere.cChevron();
	}

	public String affectationComp() {
		return affectationComp;
	}
	public void setAffectationComp(String affectationComp) {
		this.affectationComp = affectationComp;
	}

	public String affectationStructure() {
		return affectationStructure;
	}
	public void setAffectationStructure(String affectationStructure) {
		this.affectationStructure = affectationStructure;
	}

	
	public String indiceMajore() {
		// 24/12/2/2010 - correction d'un bug quand pas d'indice dans l'élément de carrière
		if (elementCarriere.indiceMajore() != null) {
			return elementCarriere.indiceMajore().toString(); 
		} else {
			return "";
		}
	}
	public String indiceBrut() {
		return elementCarriere.indiceBrut();
	}
	public String indiceMajoreSuivant() {
		if (indiceSuivant != null) {
			return indiceSuivant.cIndiceMajore().toString();
		} else {
			return null;
		}
	}

	public EOIndice indiceSuivant() {		
		return indiceSuivant;
	}
	public void setIndiceSuivant(EOIndice indice) {
		indiceSuivant = indice;
	}

	public String indiceBrutSuivant() {
		if (indiceSuivant() != null) {
			return indiceSuivant().cIndiceBrut().toString();
		} else {
			return null;
		}
	}
	public NSTimestamp dateEffet() {
		return DateCtrl.stringToDate(dateEffetFormatee());
	}
	public void setDateEffet(String dateEffet) {
		this.dateEffet = dateEffet;
	}
	public String dateEffetFormatee() {
		return dateEffet;
	}
	public String echelonSuivant() {
		return echelonSuivant;
	}
	public void setEchelonSuivant(String echelon) {
		echelonSuivant = echelon;
	}
	public String chevronSuivant() {
		return chevronSuivant;
	}
	public void setChevronSuivant(String chevron) {
		chevronSuivant = chevron;
	}
	public NSTimestamp datePromotion() {
		return DateCtrl.stringToDate(datePromotionFormatee());
	}
	public String datePromotionFormatee() {
		return datePromotion;
	}
	public void setDatePromotion(String aString) {
		datePromotion = aString;
		// 24/12/2010 - déterminer l'indice suivant à cette date
		preparerIndiceSuivant(editingContext());
	}
	public boolean doitAttendreDureeMinimumEchelon() {
		return doitAttendreDureeMinimumEchelon;
	}
	public void setDoitAttendreDureeMinimumEchelon(boolean doitAttendreDureeMinimumEchelon) {
		this.doitAttendreDureeMinimumEchelon = doitAttendreDureeMinimumEchelon;
	}
	public String existeAnciennete() {
		NSArray conservations = EOConservationAnciennete.rechercherAnciennetesNonUtiliseesPourElementCarriere(edc, elementCarriere);
		NSArray reliquats = EOReliquatsAnciennete.rechercherReliquatsNonUtilisesPourElementCarriere(edc, elementCarriere);
		if ((conservations != null && conservations.count() > 0) || (reliquats != null && reliquats.count() > 0)) {
			return "O";
		} else {
			return "N";
		}
	}
	public NSArray conservationsAnciennete() {
		return EOConservationAnciennete.rechercherAnciennetesNonUtiliseesPourElementCarriere(edc, elementCarriere);
	}
	public NSArray reliquatsAnciennete() {
		return EOReliquatsAnciennete.rechercherReliquatsNonUtilisesPourElementCarriere(edc, elementCarriere);
	}
	// Interface key-value Coding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	public Class classForCoder() {
		return IndividuPromouvable.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		NSMutableDictionary dict = new NSMutableDictionary();
		dict.setObjectForKey(elementCarriere.editingContext().globalIDForObject(elementCarriere), "element");

		if (echelonSuivant != null)
			dict.setObjectForKey(echelonSuivant, "echelon");

		if (chevronSuivant != null)
			dict.setObjectForKey(chevronSuivant, "chevron");

		dict.setObjectForKey(datePromotion, "datePromotion");
		dict.setObjectForKey(new Boolean(doitAttendreDureeMinimumEchelon), "doitAttendreDureeMiniumEchelon");
		coder.encodeObject(dict);
	}
	public static Class decodeClass() {
		return IndividuPromouvable.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new IndividuPromouvable((NSDictionary)coder.decodeObject());
	}


	// 09/02/2010 - pour supporter la recherche de promouvabilité d'échelon dans les promotions
	public static IndividuPromouvable evaluerIndividuPromouvableEchelon(
			EOEditingContext ec,
			EOIndividu individu,
			EOPassageEchelon passage, 
			EOPassageEchelon passageSuivant, 
			EOQualifier qualifierIndividus,NSTimestamp debutPeriode, NSTimestamp finPeriode,boolean uniquementNonPromus) {

		// On récupère un tableau trié par individu et date d'effet décroissante
		NSArray<EOElementCarriere> elements = EOElementCarriere.rechercherElementsAvecCriteres(ec, qualifierIndividus, true, true);
		elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);	
		NSMutableArray promouvables = new NSMutableArray();

		// On vérifie qu'il y a au moins une position d'activite pour la periode
		NSArray changementsPosition = EOChangementPosition.rechercherChangementsEnActivitePourIndividuEtPeriode(
				ec, individu, debutPeriode, finPeriode);
		if (changementsPosition.count() == 0)
			return null;

		IndividuPromouvable individuP = individuPromouvablePourElementsEtPeriode(ec, individu, 
				passage, passageSuivant, elements, debutPeriode, finPeriode, promouvables, uniquementNonPromus);

		return individuP;
		
	}


	/** Evalue les individus promouvables en fonction du qualifier de recherche, de la p&eacute;riode
	 * @param editingContext
	 * @param qualifierIndividus qualifier sur le corps/grade pour rechercher les individus
	 * @param debutPeriode
	 * @param finPeriode
	 * @param uniquementNonPromus	true si on ne recherche que les individus qui ne sont pas promus
	 * @return
	 */
	public static NSArray<IndividuPromouvable> evaluerIndividusPromouvables(
			EOEditingContext edc, 
			EOQualifier qualifierIndividus,
			NSTimestamp debutPeriode, NSTimestamp finPeriode,boolean uniquementNonPromus) {

		// On récupère un tableau trié par individu et date d'effet décroissante
		NSArray<EOElementCarriere> elements = EOElementCarriere.rechercherElementsAvecCriteres(edc, qualifierIndividus, true, true);
		NSMutableArray<IndividuPromouvable> promouvables = new NSMutableArray<IndividuPromouvable>();
		
		for (EOElementCarriere myElement : elements) {
			IndividuPromouvable individuP = individuPromouvablePourElementEtPeriode(edc, myElement, debutPeriode, finPeriode, promouvables, uniquementNonPromus);
			if (individuP != null)						
				promouvables.addObject(individuP);
		}
		return promouvables;
	}

	
	/** Evalue les individus promouvables en fonction du qualifier de recherche, de la p&eacute;riode
	 * @param editingContext
	 * @param qualifierIndividus qualifier sur le corps/grade pour rechercher les individus
	 * @param debutPeriode
	 * @param finPeriode
	 * @param uniquementNonPromus	true si on ne recherche que les individus qui ne sont pas promus
	 * @return
	 */
	// 09/02/2010 - pour supporter la recherche de promouvabilité d'échelon dans les promotions
	public static NSArray evaluerIndividusPromouvablesChevron(EOEditingContext editingContext, EOQualifier qualifierIndividus,NSTimestamp debutPeriode, NSTimestamp finPeriode,boolean uniquementNonPromus) {
		NSArray elements = EOElementCarriere.rechercherElementsAvecCriteres(editingContext, qualifierIndividus,true,true);
		// On récupère un tableau trié par individu et date d'effet décroissante
		NSMutableArray promouvables = new NSMutableArray();
		for (java.util.Enumeration<EOElementCarriere> e = elements.objectEnumerator();e.hasMoreElements();) {

			EOElementCarriere element = (EOElementCarriere)e.nextElement();

			IndividuPromouvable individuP = individuPromouvableChevronPourElementEtPeriode(editingContext, element, debutPeriode, finPeriode, promouvables, uniquementNonPromus);
			if (individuP != null)						
				promouvables.addObject(individuP);
		}
		return promouvables;
	}

	/** Evalue si un individu est promouvable pour un element de carriere et la periode
	 * @param editingContext
	 * @param elementCarriere
	 * @param debutPeriode
	 * @param finPeriode
	 * @param promouvables liste des promouvables deja identifies (null si pas de promouvable identifie
	 * @param uniquementNonPromus	true si on ne recherche que les individus qui ne sont pas promus
	 * @return
	 */
	public static IndividuPromouvable individuPromouvablePourElementEtPeriode(EOEditingContext editingContext, 
			EOElementCarriere element, 
			NSTimestamp debutPeriode, 
			NSTimestamp finPeriode, 
			NSArray promouvables, 
			boolean uniquementNonPromus) {

		boolean ajouterPromouvable = true;
		boolean attendreDureeMinimumEchelon = false;

		NSArray<EODepart> departs = EODepart.rechercherDepartsValidesPourIndividuEtPeriode(editingContext,element.individu(), debutPeriode,finPeriode);
		if (departs.size() > 0) {
			EODepart dernierDepart = departs.lastObject();
			ajouterPromouvable = departs.size() == 0 || DateCtrl.isBefore(dernierDepart.dateDebut(), element.dateDebut()) ;
		}

		if (ajouterPromouvable && uniquementNonPromus) {
			// Vérifier si c'est le dernier élément de carrière pour cet individu
			// Si ce n'est pas le cas, ne pas le prendre en compte
			EOElementCarriere lastElement = EOElementCarriere.lastElementCarriere(editingContext, element.toIndividu());
			ajouterPromouvable = (lastElement == null || lastElement == element);
		}
		
		if (ajouterPromouvable && (promouvables == null || ((NSArray)promouvables.valueForKey("individu")).containsObject(element.toIndividu()) == false)) {
			
			// Il se peut qu'on ait plusieurs éléments de carrière pour la période, ne retenir que le dernier
			// On regarde si on est arrive au dernier echelon possible, auquel cas on n'insere pas la personne
			// on travaille avec un fetch car dans le cas de sélection de corps et non de sélection de grade
			// le display group échelon est vide
			NSArray<EOPassageEchelon> echelons = EOPassageEchelon.rechercherPassagesEchelonPourGradesValidesPourPeriode(editingContext, element.toGrade(), debutPeriode,finPeriode);
			echelons = EOSortOrdering.sortedArrayUsingKeyOrderArray(echelons,EOPassageEchelon.SORT_ARRAY_C_ECHELON_ASC); 

			EOPassageEchelon dernierEchelon = (EOPassageEchelon)echelons.lastObject();

			if (element.cEchelon() != null && element.cEchelon().equals(dernierEchelon.cEchelon()) == false)	{
				
				// ce n'est pas le dernier échelon
				IndividuPromouvable promouvable = new IndividuPromouvable(element);
				// Rechercher l'échelon suivant
				boolean takeNext = false;
				EOPassageEchelon echelonIndividu = null;
				for (EOPassageEchelon myEchelon : echelons) {
					if (element.cEchelon().equals(myEchelon.cEchelon())) {
						takeNext = true;
						echelonIndividu = myEchelon;
					} else if (takeNext) {
						promouvable.setEchelonSuivant(myEchelon.cEchelon());
						break;
					}
				}
			
				promouvable.preparerIndiceSuivant(editingContext);

				// 04/02/2010 - Evaluer la date minimum de changement d'échelon en prenant les dates ptAnnee et ptMois du passage échelon (le dernier trouvé est le bon)
				NSTimestamp dateMinimum = element.dateDebut();
				// 12/10/2010 - modifié pour calculer les dates minimum en prenant les dates de l'échelon de l'individu
				if (echelonIndividu != null) {
					if (echelonIndividu.dureePtChoixAnnees() != null)
						dateMinimum = DateCtrl.dateAvecAjoutAnnees(dateMinimum, echelonIndividu.dureePtChoixAnnees().intValue());

					if (echelonIndividu.dureePtChoixMois() != null)
						dateMinimum = DateCtrl.dateAvecAjoutMois(dateMinimum, echelonIndividu.dureePtChoixMois().intValue());

				}
				// Vérifier la promouvabilité de la personne
				NSArray<EOElementCarriere> elementsEquivalents = EOElementCarriere.findElementsEquivalents(editingContext, element);
				NSTimestamp debutPriseEnCompte = elementsEquivalents.get(0).dateDebut();
				promouvable.setDateEffet(DateCtrl.dateToString(debutPriseEnCompte));

				NSTimestamp datePromouvabilite = element.datePromotionEchelon(debutPriseEnCompte, debutPeriode,finPeriode);
				
				if (datePromouvabilite != null) {
					LogManager.logDetail("Date promouvabilite : " + DateCtrl.dateToString(datePromouvabilite));
					// 13/10/2010 - modifié pour ne pas prendre en compte la date mini lorsqu'il y a des conservations ou des reliquats d'ancienneté
					if (promouvable.existeAnciennete().equals(CocktailConstantes.VRAI)) {
						LogManager.logDetail(element.individu().identite() + ", pas de prise en compte de la date mini");
					} else {
						LogManager.logDetail(element.individu().identite() + ", date mini : " + DateCtrl.dateToString(dateMinimum));
						// 04/02/2010 - La date de promouvabilité ne peut être antérieure à la date minimum
						if (DateCtrl.isBefore(datePromouvabilite, dateMinimum)) {
							LogManager.logDetail("La date de promotion va être repoussée à la date mini pour prendre en compte les durées minimum avant changement d'échelon");
							datePromouvabilite = dateMinimum;
							// 25/02/2010 Stocker l'information pour l'arrêter
							attendreDureeMinimumEchelon = true;
						}
					}
					// 27/03/09 - Vérifier si à la date de promotion, l'échelon ou le grade sont toujours valides
					// car il se peut que la date de promouvabilité soit limite
					if (gradeEtEchelonValidesADate(editingContext,promouvable.grade(),promouvable.echelonSuivant(),datePromouvabilite)) {
						promouvable.setDoitAttendreDureeMinimumEchelon(attendreDureeMinimumEchelon);
						promouvable.setDatePromotion(DateCtrl.dateToString(datePromouvabilite));
						
						NSArray<EOAffectation> affs = EOAffectation.rechercherAffectationsADate(editingContext, promouvable.individu(), DateCtrl.today());
						if (affs.count() > 0) {
							promouvable.setAffectationStructure(affs.objectAtIndex(0).toStructureUlr().llStructure());
							EOStructure comp = EOStructure.getComposante(editingContext, affs.objectAtIndex(0).toStructureUlr().cStructure());
							if (comp != null)
								promouvable.setAffectationComp(comp.lcStructure());
							else
								promouvable.setAffectationComp(affs.objectAtIndex(0).toStructureUlr().llStructure());
						}
						
						return promouvable;
					}
				}
			}
		}
		return null;
	}


	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param passage
	 * @param elements
	 * @param debutPeriode
	 * @param finPeriode
	 * @param promouvables
	 * @param uniquementNonPromus
	 * @return
	 */
	public static IndividuPromouvable individuPromouvablePourElementsEtPeriode(
			EOEditingContext editingContext, 
			EOIndividu individu, EOPassageEchelon passage, EOPassageEchelon passageSuivant, 
			NSArray elements, 
			NSTimestamp debutPeriode, NSTimestamp finPeriode,NSArray promouvables, boolean uniquementNonPromus) {

		boolean ajouterPromouvable = true;

		// Ne pas prendre en compte les personnes ayant des départs
		NSArray departs = EODepart.rechercherDepartsValidesPourIndividuEtPeriode(editingContext,individu, debutPeriode,finPeriode);
		ajouterPromouvable = (departs.count() == 0) && elements.count() > 0 && ( passage.dureePassageAnnees() != null || passage.dureePassageMois() != null);

		if (ajouterPromouvable) {

			EOElementCarriere premierElement = (EOElementCarriere)elements.objectAtIndex(0);
			
			// Recuperation de la date de promouvabilite
			NSTimestamp datePromouvabilite = premierElement.dateDebut();
			if (passage.dureePassageAnnees() != null)
				datePromouvabilite = DateCtrl.dateAvecAjoutAnnees(datePromouvabilite, passage.dureePassageAnnees().intValue());
			if (passage.dureePassageMois() != null)
				datePromouvabilite = DateCtrl.dateAvecAjoutMois(datePromouvabilite, passage.dureePassageMois().intValue());

			NSTimestamp dateMinimum = premierElement.dateDebut();
			if (passage.dureePtChoixAnnees() != null)
				dateMinimum = DateCtrl.dateAvecAjoutAnnees(dateMinimum, passage.dureePtChoixAnnees().intValue());
			if (passage.dureePtChoixMois() != null)
				dateMinimum = DateCtrl.dateAvecAjoutMois(dateMinimum, passage.dureePtChoixMois().intValue());

			
			// CONSERVATIONS D ANCIENNETE
			NSMutableArray conservations = new NSMutableArray();
			for (Enumeration<EOElementCarriere> e = elements.objectEnumerator();e.hasMoreElements();) {
				EOElementCarriere element = e.nextElement();
				conservations.addObjectsFromArray(EOConservationAnciennete.rechercherAnciennetesNonUtiliseesPourElementCarriere(editingContext, element));				
			}

			for (Enumeration<EOConservationAnciennete> e1 = conservations.objectEnumerator();e1.hasMoreElements();) {
				EOConservationAnciennete conservation = e1.nextElement();
				if (conservation.ancNbAnnees() != null) {
					datePromouvabilite = DateCtrl.dateAvecAjoutAnnees(datePromouvabilite,  -conservation.ancNbAnnees().intValue());
				}
				if (conservation.ancNbMois() != null) {
					datePromouvabilite = DateCtrl.dateAvecAjoutMois(datePromouvabilite,  -conservation.ancNbMois().intValue());
				}
				if (conservation.ancNbJours() != null) {
					datePromouvabilite = DateCtrl.dateAvecAjoutJours(datePromouvabilite,  -conservation.ancNbJours().intValue());
				}
			}

			// RELIQUATS D ANCIENNETE
			NSMutableArray reliquats = new NSMutableArray();
			for (Enumeration<EOElementCarriere> e = elements.objectEnumerator();e.hasMoreElements();) {
				EOElementCarriere element = e.nextElement();
				reliquats.addObjectsFromArray(EOReliquatsAnciennete.rechercherReliquatsNonUtilisesPourElementCarriere(editingContext, element));
			}

			for (Enumeration<EOReliquatsAnciennete> e1 = reliquats.objectEnumerator();e1.hasMoreElements();) {
				EOReliquatsAnciennete reliquat = e1.nextElement();
				if (reliquat.ancNbAnnees() != null) {
					datePromouvabilite = DateCtrl.dateAvecAjoutAnnees(datePromouvabilite,  -reliquat.ancNbAnnees().intValue());
				}
				if (reliquat.ancNbMois() != null) {
					datePromouvabilite = DateCtrl.dateAvecAjoutMois(datePromouvabilite,  -reliquat.ancNbMois().intValue());
				}
				if (reliquat.ancNbJours() != null) {
					datePromouvabilite = DateCtrl.dateAvecAjoutJours(datePromouvabilite,  -reliquat.ancNbJours().intValue());
				}
			}

			// La date de promotion ne peut etre anterieure a la date minimale de changement d'echelon
			if (DateCtrl.isBeforeEq(datePromouvabilite, dateMinimum))
				datePromouvabilite = dateMinimum;
			
			// L'agent est il en position d'activite a la date de promotion
			NSArray positions = EOChangementPosition.rechercherChangementsEnActivitePourIndividuEtPeriode(editingContext, individu, datePromouvabilite, datePromouvabilite);
			if (positions.count() == 0)
				return null;

			// Ajout de l'individu promouvable si sa date de promouvabilite rentre dans la periode definie
			if (	DateCtrl.isBeforeEq(datePromouvabilite, finPeriode) 
				 && DateCtrl.isAfterEq(datePromouvabilite, debutPeriode)) {

				IndividuPromouvable promouvable = new IndividuPromouvable(premierElement);
				promouvable.setDatePromotion(DateCtrl.dateToString(datePromouvabilite));
				promouvable.setEchelonSuivant(passageSuivant.cEchelon());

				return promouvable;
			}

		}

		return null;
	}




	public static IndividuPromouvable individuPromouvableChevronPourElementEtPeriode(EOEditingContext editingContext, EOElementCarriere element,NSTimestamp debutPeriode, NSTimestamp finPeriode,NSArray promouvables,boolean uniquementNonPromus) {

		boolean ajouterPromouvable = true;
		// DT 64953 - Ne pas prendre en compte les personnes ayant des départs
		//		NSArray departs = EODepart.rechercherDepartsValidesPourIndividuEtPeriode(editingContext,element.individu(), debutPeriode,finPeriode);
		//		ajouterPromouvable = (departs.count() == 0);
		if (ajouterPromouvable && uniquementNonPromus) {
			// Vérifier si c'est le dernier élément de carrière pour cet individu
			// Si ce n'est pas le cas, ne pas le prendre en compte
			EOElementCarriere lastElement = EOElementCarriere.lastElementCarriere(editingContext, element.toIndividu());
			ajouterPromouvable = (lastElement == null || lastElement == element);
		}
		if (ajouterPromouvable && (promouvables == null || ((NSArray)promouvables.valueForKey("individu")).containsObject(element.toIndividu()) == false)) {
			// Il se peut qu'on est plusieurs éléments de carrière pour la période, ne retenir que le dernier
			// On regarde si on est arrive au dernier echelon possible, auquel cas on n'insere pas la personne
			// on travaille avec un fetch car dans le cas de sélection de corps et non de sélection de grade
			// le display group échelon est vide
			NSArray chevrons = EOPassageChevron.rechercherPassagesChevronPourGrade(editingContext, new NSArray(element.toGrade()),true);
			chevrons = EOSortOrdering.sortedArrayUsingKeyOrderArray(chevrons,EOPassageChevron.SORT_C_CHEVRON_ASC); 
			EOPassageChevron chevron = (EOPassageChevron)chevrons.lastObject();

			if (element.cChevron() != null && element.cChevron().equals(chevron.cChevron()) == false)	{
				// ce n'est pas le dernier échelon
				IndividuPromouvable promouvable = new IndividuPromouvable(element);
				// Rechercher l'échelon suivant
				boolean takeNext = false;
				EOPassageChevron chevronIndividu = null;
				for (java.util.Enumeration<EOPassageChevron> e1 = chevrons.objectEnumerator();e1.hasMoreElements();) {
					chevron = e1.nextElement(); 
					if (element.cChevron().equals(chevron.cChevron())) {
						takeNext = true;
						chevronIndividu = chevron;
					} else if (takeNext) {
						promouvable.setEchelonSuivant(chevron.cEchelon());
						promouvable.setChevronSuivant(chevron.cChevron());
						break;
					}
				}

				promouvable.preparerIndiceSuivant(editingContext);

				// 04/02/2010 - Evaluer la date minimum de changement de chevron en prenant les dates ptAnnee et ptMois du passage échelon (le dernier trouvé est le bon)
				NSTimestamp dateMinimum = element.dateDebut();				

				// 12/10/2010 - modifié pour calculer les dates minimum en prenant les dates de l'échelon de l'individu
				if (chevronIndividu != null) {
					if (chevronIndividu.dureeChevronAnnees() != null)
						dateMinimum = DateCtrl.dateAvecAjoutAnnees(dateMinimum, chevronIndividu.dureeChevronAnnees().intValue());
					if (chevronIndividu.dureeChevronMois() != null)
						dateMinimum = DateCtrl.dateAvecAjoutMois(dateMinimum, chevronIndividu.dureeChevronMois().intValue());
				}

				// Vérifier la promouvabilité de la personne
				NSTimestamp datePromouvabilite = element.datePromotionChevron(debutPeriode,finPeriode);

				if (datePromouvabilite != null) {
					promouvable.setDatePromotion(DateCtrl.dateToString(datePromouvabilite));
					return promouvable;
				}

				return null;
			}
		}
		return null;
	}




	//	Méthodes privées
	private void preparerIndiceSuivant(EOEditingContext editingContext) {
		NSTimestamp date = DateCtrl.stringToDate(datePromotion);
		// le passage échelon n'existe pas forcément car on peut être aux limites dans passage échelon

		NSArray passages = EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(editingContext, elementCarriere.toGrade().cGrade(), echelonSuivant, date, true);
		if (passages.count() > 0) {
			EOPassageEchelon passageEchelon = (EOPassageEchelon)passages.objectAtIndex(0);

			if (passageEchelon.cIndiceBrut() == null) {
				indiceSuivant = null;
			} else {
				// indice à la date de promotion
				indiceSuivant = EOIndice.indiceMajorePourIndiceBrutEtDate(editingContext, passageEchelon.cIndiceBrut(), date);
			}
		}

	}
	private static boolean gradeEtEchelonValidesADate(EOEditingContext editingContext,EOGrade grade,String echelon,NSTimestamp date) {
		if (grade.dFermeture() != null && DateCtrl.isBefore(grade.dFermeture(), date)) {
			// le grade est fermé
			return false;
		}
		NSArray passages = EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(editingContext, grade.cGrade(), echelon, date, true);
		return (passages.count() > 0);
	}	
}


/*
 * Created on 4 nov. 2005
 *
 * Modélise un arrêté
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

/**Mod&eacute;lise un arr&ecirc;t&eacute;
 * @author christine
 *
 */
public class Arrete implements NSKeyValueCoding {
	private NSTimestamp date;
	private String numero;
	private String temSignature;
	private boolean estModifiable;
	
	public Arrete() {
		this(null,null,null);
	}
	public Arrete(NSTimestamp date,String numero) {
		this(date,numero,CocktailConstantes.FAUX,true);
	}
	public Arrete(NSTimestamp date,String numero,String temSignature) {
		this(date,numero,temSignature,true);
	}
	public Arrete(NSTimestamp date,String numero,String temSignature,boolean estModifiable) {
		this.date = date;
		this.numero = numero;
		this.temSignature = temSignature;
		this.estModifiable = estModifiable;
	}
	// accesseurs
	public NSTimestamp date() {
		return date;
	}
	public void setDate(NSTimestamp uneDate) {
		date = uneDate;
	}
	public String temSignature() {
		return temSignature;
	}
	public String numero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public boolean estSigne() {
		return temSignature != null && temSignature.equals(CocktailConstantes.VRAI);
	}
	public void setEstSigne(boolean aBool) {
		if (aBool) {
			temSignature = CocktailConstantes.VRAI;
		} else if (temSignature != null) {
			temSignature = CocktailConstantes.FAUX;
		}
	}
	public boolean estModifiable() {
		return estModifiable;
	}
	public void setEstModifiable(boolean aBool) {
		estModifiable = aBool;
	}
	public boolean estImprimable() {
		return (numero() != null || date() != null) && estSigne() == false;
	}
	public String dateFormatee() {
		return SuperFinder.dateFormatee(this,"date");
	}
	public void setDateFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"date",uneDate);
	}
	public String toString() {
		return "date " + dateFormatee() + ", numéro " + numero() + "est signe :" + estSigne();
	}
	//	 interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
}

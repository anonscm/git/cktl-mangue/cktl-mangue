//EmploiPourEdition
//Created on Tue Aug 29 14:36:23 Europe/Paris 2006 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.mangue.common.metier.finder.gpeec.EmploiSpecialisationFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;
/** contient toutes les donn&eacute;es pour l'&eacute;dition des emplois sous la forme de valeurs simples.<BR>
 * On retient les informations les plus r&eacute;centes pour les carri&egrave;res ou les contrats (si pas de carri&egrave;re), 
 * Supporte l'interface NSKeyValueCoding pour l'acc&eagrave;s aux attributs et NSCoding pour le transfert des donn&eacute;es entre le client et le serveur */
public class EmploiPourEdition extends InfoPourEdition implements NSKeyValueCoding, NSCoding {
	/** types d'emploi */
	public static int TOUT_TYPE = 0,EMPLOI_BUDGETAIRE = 1, RESSOURCE_PROPRE = 2, ROMPUS = 3, VACANT = 4;
	private static String types[] = {"Indéfini","ETAT","RP","Rompu","Vacant"};
	public EmploiPourEdition() {
		super();
		// pour ne pas avoir de souci à l'export
		setCategorieEmploi("");
		setCodeSpecEmploi("");
		setReferenceContrat("");
		setTypePopulation("");
		setTypeEmploi(new Integer(TOUT_TYPE));
	}
	public EmploiPourEdition(IEmploi emploi,int typeEmploi) {
		this();
		setEmploi(emploi);
		setTypeEmploi(new Integer(typeEmploi));
		setQuotite(new Integer(0));
		setQuotiteFinanciere(new Integer(0));
	}
	public EmploiPourEdition(EOAffectation affectation,EOOccupation occupation, NSTimestamp dateDebut,NSTimestamp dateFin,int typeEmploi) {
		this();
		if (affectation != null) {
			setIndividu(affectation.individu());
			setAffectation(affectation);
			NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(affectation.editingContext(),affectation.individu(),dateDebut,dateFin);
			if (carrieres.count() > 0) {
				// on retiendra le segment de carrière le plus récent
				carrieres = EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
				EOCarriere carriere = (EOCarriere)carrieres.objectAtIndex(0);
				NSArray elements = carriere.elementsPourPeriode(dateDebut,dateFin);
				try {
					//	on retiendra l'élément de carrière le plus récent
					elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
					EOElementCarriere element = (EOElementCarriere)elements.objectAtIndex(0);
					setElementCarriere(element,true);
				} catch (Exception exc) {
					setCarriere(carriere,true,true,dateDebut,dateFin);
				}
			} else {
				NSArray contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(affectation.editingContext(),affectation.individu(),dateDebut,dateFin,true);
				if (contrats.count() > 0) {
					// on retiendra le contrat le plus récent
					contrats = EOSortOrdering.sortedArrayUsingKeyOrderArray(contrats, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
					EOContrat contrat = (EOContrat)contrats.objectAtIndex(0);
					setContratAvenant(contrat.avenantCourant(),true);
				}
			}
		}
		if (occupation != null) {
			setOccupation(occupation);
		}
		setTypeEmploi(new Integer(typeEmploi));
		if (typeEmploi == ROMPUS) {
			setQuotite(occupation.quotite());
		}
	}
	// constructeur pour encoding/decoding
	public EmploiPourEdition(NSDictionary aDict) {
		super(aDict);
	}
	public String categorieEmploi() {
		return (String)objectForKey("categorieEmploi");
	}
	public void setCategorieEmploi(String value) {
		setObjectForKey(value, "categorieEmploi");
	}
	public String codeSpecEmploi() {
		return (String)objectForKey("codeSpecEmploi");
	}
	public void setCodeSpecEmploi(String value) {
		setObjectForKey(value, "codeSpecEmploi");
	}
	public String referenceContrat() {
		return (String)objectForKey("referenceContrat");
	}
	public void setReferenceContrat(String value) {
		setObjectForKey(value, "referenceContrat");
	}
	public String typePopulation() {
		return (String)objectForKey("typePopulation");
	}
	public void setTypePopulation(String value) {
		setObjectForKey(value, "typePopulation");
	}
	public Number typeEmploi() {
		return (Number)objectForKey("typeEmploi");
	}
	public void setTypeEmploi(Number value) {
		setObjectForKey(value, "typeEmploi");
	}
	public String type() {
		if (typeEmploi() != null) {
			return types[typeEmploi().intValue()];
		} else {
			return types[0];
		}
	}

	
	/**
	 * 
	 */
	public void setContratAvenant(EOContratAvenant avenant,boolean recupererSpecialisation) {
		super.setContratAvenant(avenant,recupererSpecialisation);
		if (avenant == null) {
			return;
		}
		if (avenant.toGrade() != null && avenant.toGrade().toCorps() != null && avenant.toGrade().toCorps().toTypePopulation() != null) {
			setTypePopulation(avenant.toGrade().toCorps().toTypePopulation().libelleCourt());
		}
		EOContrat contrat = avenant.contrat();
		if (contrat.toRne() != null) {
			setImplantation(contrat.toRne().code());
		}
		if (avenant.referenceContrat() != null) {
			setReferenceContrat(avenant.referenceContrat());
		} else {
			setReferenceContrat("0000000");
		}
	}
	
	/** ajoute les informations de carri&egrave;re
	 * @param carriere
	 * @param uniquementCarriere true si les informations sur les &eacute;l&eacute;ments de carri&egrave;re ne sont pas fournis
	 */
	public void setCarriere(EOCarriere carriere,boolean uniquementCarriere,boolean recupererSpecialisation) {
		setCarriere(carriere, uniquementCarriere, recupererSpecialisation,null,null);
	}
	/** ajoute les informations de carri&egrave;re aux dates pass&eacute;es en param&egrave;tres
	 * @param carriere
	 * @param uniquementCarriere true si les informations sur les &eacute;l&eacute;ments de carri&egrave;re ne sont pas fournis
	 */
	public void setCarriere(EOCarriere carriere,boolean uniquementCarriere,boolean recupererSpecialisation,NSTimestamp dateDebut,NSTimestamp dateFin) {
		super.setCarriere(carriere,uniquementCarriere,recupererSpecialisation);
		try {
			EOChangementPosition changementPosition = null;
			if (dateDebut == null && dateFin == null) {
				// on retiendra le changement de position le plus récent
				changementPosition = carriere.dernierePosition();
			} else {
				NSArray changements = carriere.changementsPositionPourPeriode(dateDebut, dateFin);
				if (changements != null && changements.count() > 0) {
					// on retient le dernier trouvé (le tableau est trié par ordre de date croissant)
					changementPosition = (EOChangementPosition)changements.lastObject();
				}
			}
			setLcTypeContratTrav(changementPosition.toPosition().code());
		} catch (Exception exc) {
			// pas de changement de position
		}
	}
	public void setElementCarriere(EOElementCarriere element,boolean recupererSpecialisation) {
		super.setElementCarriere(element,recupererSpecialisation);
		setTypePopulation(element.toCorps().toTypePopulation().libelleCourt());
	}
	public void setEmploi(IEmploi emploi) {
		super.setEmploi(emploi);
		
		if (emploi != null) {
			setCategorieEmploi(emploi.categorieEmploiCourante());
			
			IEmploiSpecialisation emploiSpec = EmploiSpecialisationFinder.sharedInstance().findEmploiSpecialisationCourante(((EOEmploi) emploi).editingContext(), emploi);
			if (emploiSpec != null) {
				setCodeSpecEmploi(emploiSpec.libelleSpecialisation());
			}
		}
	}
	
	public void setOccupation(EOOccupation occupation) {
		super.setOccupation(occupation);
	}
	//  Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	public Class classForCoder() {
		return EmploiPourEdition.class;
	}
	public static Class decodeClass() {
		return EmploiPourEdition.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new EmploiPourEdition((NSDictionary)coder.decodeObject());
	}
}

/*
 * Created on 14 avr. 2005
 *
 * Modélise une durée avec un arrêté associé
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Mod&eacute;lise une duree avec un arrete associe
 * @author christine<BR>
 *
 
 */
public abstract class DureeAvecArrete extends Duree {
	
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String COMMENTAIRE_KEY = "commentaire";

	public DureeAvecArrete() {
        super();
    }
	public String commentaire() {
		return (String)storedValueForKey(COMMENTAIRE_KEY);
	}

	public void setCommentaire(String value) {
		takeStoredValueForKey(value, COMMENTAIRE_KEY);
	}
	
    public String noArrete() {
        return (String)storedValueForKey(NO_ARRETE_KEY);
    }

    public void setNoArrete(String value) {
        takeStoredValueForKey(value, NO_ARRETE_KEY);
    }

    public NSTimestamp dateArrete() {
        return (NSTimestamp)storedValueForKey(DATE_ARRETE_KEY);
    }

    public void setDateArrete(NSTimestamp value) {
        takeStoredValueForKey(value, DATE_ARRETE_KEY);
    }
	public String dateArreteFormatee() {
		return SuperFinder.dateFormatee(this,DATE_ARRETE_KEY);
	}
	public void setDateArreteFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,DATE_ARRETE_KEY,uneDate);
	}
	
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		super.validateForSave();
		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException("Un numéro d'arrêté comporte au plus 20 caractères");
		}
	}
	
	// méthodes protégées
	protected void init() {}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.goyave;

import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** R&egrave;les de validation <BR>
 * On v&eacute;rifie que le d&eacute;but de validit&eacute; est ant&eacute;rieur &agrave; la fin de validit&eacute;<BR>
 * @author christine
 *
 */
public abstract class DureeAvecValidite extends ObjetAvecValidite {
	public DureeAvecValidite() {
		super();
	}
	public NSTimestamp debutValidite() {
		return (NSTimestamp)storedValueForKey("debutValidite");
	}

	public void setDebutValidite(NSTimestamp value) {
		takeStoredValueForKey(value, "debutValidite");
	}

	public NSTimestamp finValidite() {
		return (NSTimestamp)storedValueForKey("finValidite");
	}

	public void setFinValidite(NSTimestamp value) {
		takeStoredValueForKey(value, "finValidite");
	}
	public String debutValiditeFormatee() {
		return SuperFinder.dateFormatee(this,"debutValidite");
	}
	public void setDebutValiditeFormatee(String uneDate) {
		if (uneDate == null) {
			setDebutValidite(null);
		} else {
			SuperFinder.setDateFormatee(this,"debutValidite",uneDate);
		}
	}
	public String finValiditeFormatee() {
		return SuperFinder.dateFormatee(this,"finValidite");
	}
	public void setFinValiditeFormatee(String uneDate) {
		if (uneDate == null) {
			setFinValidite(null);
		} else {
			SuperFinder.setDateFormatee(this,"finValidite",uneDate);
		}
	}
	/** la sous-classe doit absolument invoquer cette m&eacute;thode */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		// La date de fin ne doit pas etre inferieure a la date de debut
		if (debutValidite() == null) {
			throw new NSValidation.ValidationException("La date de début de validité doit être définie !");
		} else if (finValidite() != null && debutValidite().after(finValidite())) {
			throw new NSValidation.ValidationException("La date de fin de validité doit être postérieure à la date de début !");
		}
	}
	// Méthodes statiques
	/** Retourne le qualifier pour rechercher sur la dur&eacute;e de validit&eacute; */
	public static EOQualifier qualifierPourValidite(boolean estValide,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray(qualifierPourValidite(estValide));
		if (debutPeriode == null) {
			if (finPeriode != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("finValidite = nil OR finValidite >= %@", new NSArray(finPeriode)));
			}
		} else {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode("debutValidite", debutPeriode, "finValidite", finPeriode));
		}
		return new EOAndQualifier(qualifiers);
	}
	/** Retourne les objets de type DureeAvecValidite valides pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param nomEntite (sa classe doit h&eacute;riter de DureeAvecValidite)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherObjetsValidesPourPeriode(EOEditingContext editingContext,String nomEntite,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifierPourValidite(true,debutPeriode,finPeriode),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** Retourne les objets de type DureeAvecValidite valides pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param nomEntite (sa classe doit h&eacute;riter de DureeAvecValidite)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherObjetsInvalidesPourPeriode(EOEditingContext editingContext,String nomEntite,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifierPourValidite(false,debutPeriode,finPeriode),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
	}
}

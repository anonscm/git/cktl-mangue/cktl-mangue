//EOPrimeBudget.java
//Created on Fri Oct 24 08:38:11 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.goyave;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPrimeBudget extends EOGenericRecord {

	public EOPrimeBudget() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPrimeBudget(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */

	public Number pbudExercice() {
		return (Number)storedValueForKey("pbudExercice");
	}

	public void setPbudExercice(Number value) {
		takeStoredValueForKey(value, "pbudExercice");
	}

	public String pbudLibelle() {
		return (String)storedValueForKey("pbudLibelle");
	}

	public void setPbudLibelle(String value) {
		takeStoredValueForKey(value, "pbudLibelle");
	}

	public BigDecimal pbudCredit() {
		return (BigDecimal)storedValueForKey("pbudCredit");
	}

	public void setPbudCredit(BigDecimal value) {
		takeStoredValueForKey(value, "pbudCredit");
		calculerSolde();
	}

	public BigDecimal pbudEngage() {
		return (BigDecimal)storedValueForKey("pbudEngage");
	}

	public void setPbudEngage(BigDecimal value) {
		takeStoredValueForKey(value, "pbudEngage");
	}

	public BigDecimal pbudDebit() {
		return (BigDecimal)storedValueForKey("pbudDebit");
	}

	public void setPbudDebit(BigDecimal value) {
		takeStoredValueForKey(value, "pbudDebit");
		calculerSolde();
	}

	public BigDecimal pbudSolde() {
		return (BigDecimal)storedValueForKey("pbudSolde");
	}

	public void setPbudSolde(BigDecimal value) {
		takeStoredValueForKey(value, "pbudSolde");
	}
	public String pbudTemGlobal() {
		return (String)storedValueForKey("pbudTemGlobal");
	}
	public void setPbudTemGlobal(String value) {
		takeStoredValueForKey(value, "pbudTemGlobal");
	}

	// Méthodes ajoutées
	public void initAvecExercice(int exercice) {
		setPbudCredit(new BigDecimal(0));
		setPbudDebit(new BigDecimal(0));
		setPbudEngage(new BigDecimal(0));
		setPbudSolde(new BigDecimal(0));
		setEstGlobal(false);
		setPbudExercice(new Integer(exercice));
	}
	public void calculerSolde() {
		if (pbudCredit() != null && pbudDebit() != null) {
			setPbudSolde(pbudCredit().subtract(pbudDebit()));
		}
	}
	public boolean estGlobal() {
		return pbudTemGlobal() != null && pbudTemGlobal().equals(CocktailConstantes.VRAI);
	}
	public void setEstGlobal(boolean aBool) {
		if (aBool) {
			setPbudTemGlobal(CocktailConstantes.VRAI);
		} else {
			setPbudTemGlobal(CocktailConstantes.FAUX);
		}
	}
	/** V&eacute;rifie la longueur et la coh&eacute;rence des champs.<BR>
	 *  V&eacute;rifie que pour un exercice donn&eacute;, le budget global est unique<BR>
	 *   V&eacute;rifie que le montant du cr&eacute;dit n'est pas 0 sauf pour le budget global
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		if (pbudLibelle() != null && pbudLibelle().length() > 200) {
			throw new NSValidation.ValidationException("Le libellé ne peut dépasser 200 caractères");
		}
		if (pbudExercice() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir l'exercice");
		} else {
			int annee = pbudExercice().intValue();
			if (annee < 2000 && annee > 2100) {
				throw new NSValidation.ValidationException("L'exercice doit être donné sur 4 caractères et être postérieur à 2004");
				// Ici vérifier la relation avec la table organ
			}
		}
		if (pbudCredit() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un montant de crédit");
		} else  if (!estGlobal() &&  pbudCredit().doubleValue() == 0) {
			throw new NSValidation.ValidationException("Vous devez fournir un montant de crédit");
		}
		if (estGlobal()) {
			EOPrimeBudget budget = rechercherBudgetGlobalPourExercice(editingContext(), pbudExercice());
			if (budget != null && budget != this) {
				throw new NSValidation.ValidationException("Il ne peut y avoir qu'un seul budget global par exercice");
			}
		}
	}
	// Méthodes statiques
	/** Retourne le budget, null si non trouv&eacute; */
	public static EOPrimeBudget rechercherBudgetGlobalPourExercice(EOEditingContext editingContext,Number exercice) {
		EOFetchSpecification fs = new EOFetchSpecification("PrimeBudget",EOQualifier.qualifierWithQualifierFormat("pbudExercice = %@ AND pbudTemGlobal = 'O'", new NSArray(exercice)),null);
		try {
			return (EOPrimeBudget)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Retourne le budget, null si non trouv&eacute; */
	public static NSArray rechercherBudgetsPourExercice(EOEditingContext editingContext,Number exercice) {
		EOFetchSpecification fs = new EOFetchSpecification("PrimeBudget",EOQualifier.qualifierWithQualifierFormat("pbudExercice = %@", new NSArray(exercice)),null);
		return editingContext.objectsWithFetchSpecification(fs);
	
	}
}
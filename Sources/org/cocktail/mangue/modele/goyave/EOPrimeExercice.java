// EOPrimeExercice.java
// Created on Thu Oct 02 11:00:39 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.goyave;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPrimeExercice extends EOGenericRecord {

    public EOPrimeExercice() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPrimeExercice(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public EOPrime prime() {
        return (EOPrime)storedValueForKey("prime");
    }

    public void setPrime(EOPrime value) {
        takeStoredValueForKey(value, "prime");
    }

    public org.cocktail.mangue.modele.goyave.EOPrimeBudget budget() {
        return (org.cocktail.mangue.modele.goyave.EOPrimeBudget)storedValueForKey("budget");
    }

    public void setBudget(org.cocktail.mangue.modele.goyave.EOPrimeBudget value) {
        takeStoredValueForKey(value, "budget");
    }
    // Méthodes ajoutées
    public void initAvecBudgetEtPrime(EOPrimeBudget budget,EOPrime prime) {
    	addObjectToBothSidesOfRelationshipWithKey(budget, "budget");
    	addObjectToBothSidesOfRelationshipWithKey(prime, "prime");
    }
    public void supprimerRelations() {
    	removeObjectFromBothSidesOfRelationshipWithKey(budget(), "budget");
    	removeObjectFromBothSidesOfRelationshipWithKey(prime(), "prime");
    }
    /** V&eacute;rifie que la prime et le budget sont fournis et que la prime n'est affect&eacute;e qu'&grave; un seul budget
     * de l'exercice
     */
    public void validateForSave() throws NSValidation.ValidationException {
    	if (budget() == null) {
    		throw new NSValidation.ValidationException("Vous devez fournir un budget");
    	}
    	if (prime() == null) {
    		throw new NSValidation.ValidationException("Vous devez fournir une prime");
    	}
    	NSArray primesEx = rechercherPrimesExercices(editingContext(), prime(), budget().pbudExercice());
    	if (primesEx.count() > 0 && (EOPrimeExercice)primesEx.objectAtIndex(0) != this) {
    		throw new NSValidation.ValidationException("Une prime ne peut être associée qu'à un seul budget pour un exercice donné");
    	}
    }
    // méthodes statiques
    /** Retourne le budget associ&eacute; &agrave; une prime pour un exercice donn&eacute;, null si non trouv&eacute; */
    public static EOPrimeBudget budgetPourPrimeEtExercice(EOEditingContext editingContext,EOPrime prime,Number exercice) {
    	try {
    		return ((EOPrimeExercice)rechercherPrimesExercices(editingContext, prime, exercice).objectAtIndex(0)).budget();
    	} catch (Exception e) {
    		return null;
    	}
    }
    /** Retourne les primes exercoces associ&eacute; &agrave; un budget */
    public static NSArray rechercherPrimesExercicesPourBudget(EOEditingContext editingContext,EOPrimeBudget budget) {
    	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("budget = %@", new NSArray(budget)));

		EOFetchSpecification fs = new EOFetchSpecification("PrimeExercice",new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(fs);
    }
    /** Retourne les primes exercice pour une prime donn&eacute;e (tous les budgets si prime = null)
     * @param prime (peut &ecirc;tre nulle)
     * @param int exercice */
    public static NSArray rechercherPrimesExercices(EOEditingContext editingContext,EOPrime prime,Number exercice) {
    	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("budget.pbudExercice = %@", new NSArray(exercice)));
		if (prime != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("prime = %@", new NSArray(prime)));
		}
		EOFetchSpecification fs = new EOFetchSpecification("PrimeExercice",new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(fs);
    }
}

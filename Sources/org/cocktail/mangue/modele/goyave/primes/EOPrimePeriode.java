//EOPrimePeriode.java
//Created on Fri Sep 26 09:55:30 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.goyave.primes;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.goyave.EOPayeMois;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** R&egrave;gles de validation<BR>
 * V&eacute;rifie que l'attribution associ&eacute;e est fournie<BR>
 * V&eacute;rifie que les deux dates de rappel (ou aucune) sont fournies et qu'elles sont coh&eacute;rentes<BR>
 *  V&eacute;rifie qu'en cas de rappel, le mois de la p&eacute;riode est post&eacute;rieur &agrave; celui du rappel<BR>
 * V&eacute;rifie que le montant est fourni
 * @author christine
 *
 */
public class EOPrimePeriode extends EOGenericRecord {

	public EOPrimePeriode() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPrimePeriode(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */
	public Number pperExercice() {
		return (Number)storedValueForKey("pperExercice");
	}

	public void setPperExercice(Number value) {
		takeStoredValueForKey(value, "pperExercice");
	}

	public BigDecimal pperMontant() {
		return (BigDecimal)storedValueForKey("pperMontant");
	}

	public void setPperMontant(BigDecimal value) {
		takeStoredValueForKey(value, "pperMontant");
	}

	public String temRappel() {
		return (String)storedValueForKey("temRappel");
	}

	public void setTemRappel(String value) {
		takeStoredValueForKey(value, "temRappel");
	}

	public NSTimestamp pperDebRappel() {
		return (NSTimestamp)storedValueForKey("pperDebRappel");
	}

	public void setPperDebRappel(NSTimestamp value) {
		takeStoredValueForKey(value, "pperDebRappel");
	}

	public NSTimestamp pperFinRappel() {
		return (NSTimestamp)storedValueForKey("pperFinRappel");
	}

	public void setPperFinRappel(NSTimestamp value) {
		takeStoredValueForKey(value, "pperFinRappel");
	}
	public org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution primeAttribution() {
		return (org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution)storedValueForKey("primeAttribution");
	}

	public void setPrimeAttribution(org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution value) {
		takeStoredValueForKey(value, "primeAttribution");
	}
	public EOPayeMois moisPaiement() {
		return (EOPayeMois)storedValueForKey("moisPaiement");
	}

	public void setMoisPaiement(EOPayeMois value) {
		takeStoredValueForKey(value, "moisPaiement");
	}
	// Méthodes ajoutées
	public void initAvecAttribution(EOPrimeAttribution attribution, EOPayeMois mois) {
		setTemRappel(CocktailConstantes.FAUX);
		setPperExercice(attribution.pattExercice());
		addObjectToBothSidesOfRelationshipWithKey(attribution, "primeAttribution");
		addObjectToBothSidesOfRelationshipWithKey(mois, "moisPaiement");

	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(primeAttribution(), "primeAttribution");
		removeObjectFromBothSidesOfRelationshipWithKey(moisPaiement(), "moisPaiement");
	}
	public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
		if (primeAttribution() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une attribution");
		} 
		if (pperMontant() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un montant");
		} 
		// Vérifier la validité des dates de rappel
		if (pperDebRappel() == null) {
			if (pperFinRappel() == null) {
				setTemRappel(CocktailConstantes.FAUX);
			} else {
				throw new NSValidation.ValidationException("Vous devez fournir une date début et fin de rappel ou aucune date si il ne s'agit pas d'un rappel");
			}
		} else {
			if (pperFinRappel() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir une date début et fin de rappel ou aucune date si il ne s'agit pas d'un rappel");
			} else {
				setTemRappel(CocktailConstantes.VRAI);
				// Vérifier que le mois est postérieur ou égal au mois du rappel
			/*	if (DateCtrl.isAfter(pperFinRappel(), mois.moisDebut())) {
					throw new NSValidation.ValidationException("La date de fin du rappel doit être antérieure au début du mois de la période");
				}*/
			}
		}
	}
}

// EOPrimeParamPerso.java
// Created on Thu Oct 23 09:52:05 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.goyave.primes;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.EOPrimeCode;
import org.cocktail.mangue.modele.goyave.ObjetAvecValidite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Un param&egrave;tre personnel d&eacute;finit une donn&eacute;e n&eacute;cessaire pour le calcul d'une prime, il est n&eacute;cessairement unique
 * pour un code et une personnalisation donn&eacute;s<BR>
 * R&egrave;gles de validation : <BR>
 * Pr&eacute;sence des champs obligatoires et v&eacute;rification de leur longueur<BR>
 * V&eacute;rification de l'unicit&eacute; du param&egrave;tre (si il est valide) pour un code et une personnalisation
 * @author christine
 *
 */
public class EOPrimeParamPerso extends ObjetAvecValidite {

    public EOPrimeParamPerso() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPrimeParamPerso(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
    public NSTimestamp dCreation() {
		return (NSTimestamp)storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}

	public NSTimestamp dModification() {
		return (NSTimestamp)storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	
    public String pppeValeur() {
        return (String)storedValueForKey("pppeValeur");
    }

    public void setPppeValeur(String value) {
        takeStoredValueForKey(value, "pppeValeur");
    }

    public org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation personnalisation() {
        return (org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation)storedValueForKey("personnalisation");
    }

    public void setPersonnalisation(org.cocktail.mangue.modele.goyave.primes.EOPrimePersonnalisation value) {
        takeStoredValueForKey(value, "personnalisation");
    }

    public org.cocktail.mangue.modele.goyave.EOPrimeCode code() {
        return (org.cocktail.mangue.modele.goyave.EOPrimeCode)storedValueForKey("code");
    }

    public void setCode(org.cocktail.mangue.modele.goyave.EOPrimeCode value) {
        takeStoredValueForKey(value, "code");
    }
    // Méthodes ajoutées
    public void initAvecCodePersonnalisationEtValeur(EOPrimeCode code,EOPrimePersonnalisation personnalisation,String valeur) {
    	super.init();
    	setPppeValeur(valeur);
    	NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
    	addObjectToBothSidesOfRelationshipWithKey(personnalisation, "personnalisation");
    	addObjectToBothSidesOfRelationshipWithKey(code, "code");
    }
    public void validateForSave() throws NSValidation.ValidationException {
    	super.validateForSave();
    	if (code() == null) {
    		throw new NSValidation.ValidationException("Vous devez fournir un code");
    	}
    	if (personnalisation() == null) {
    		throw new NSValidation.ValidationException("Vous devez fournir une personnalisation");
    	}
    	if (pppeValeur() == null || pppeValeur().length() == 0) {
    		throw new NSValidation.ValidationException("Vous devez fournir une valeur");
    	} else if (pppeValeur().length() > 50) {
    		throw new NSValidation.ValidationException("La valeur comporte au maximum 50 caractères");
    	}
    	if (estValide()) {
    		// Vérifier qu'il est unique
    		EOPrimeParamPerso param = rechercherParametrePersonnelValidePourPersonnalisationEtCode(editingContext(), personnalisation(), code());
    		if (param != null && param != this) {
    			throw new NSValidation.ValidationException("Un paramètre personnel a une valeur unique");
    		}
    	}
    }
    /** Retourne un nouveau pram&egrave;tre personnel valide pour la p&eacute;riode pass&eacute; en param&grave;tre */
	public EOPrimeParamPerso clonerAvecDatesEtPersonnalisation(NSTimestamp debutPeriode,NSTimestamp finPeriode,EOPrimePersonnalisation personnalisation) {
		EOPrimeParamPerso nouveauParam = new EOPrimeParamPerso();
		nouveauParam.takeValuesFromDictionary(this.snapshot());
		NSTimestamp today = new NSTimestamp();
		nouveauParam.setDCreation(today);
		nouveauParam.setDModification(today);
		nouveauParam.addObjectToBothSidesOfRelationshipWithKey(personnalisation, "personnalisation");
		nouveauParam.setEstValide(true);
		return nouveauParam;
	}
    // Méthodes statiques
    /** Retourne le param&egrave;tre valide associ&eacute; &agrave; un code et une personnalisation */
    public static EOPrimeParamPerso rechercherParametrePersonnelValidePourPersonnalisationEtCode(EOEditingContext editingContext,EOPrimePersonnalisation personnalisation,EOPrimeCode code) {
    	NSArray params = rechercherParametresPersonnelsPourPersonnalisationEtCode(editingContext,personnalisation,code,true);
    	try {
    		return (EOPrimeParamPerso)params.objectAtIndex(0);
    	} catch (Exception e) {
    		return null;
    	}
    }
    /** Retourne les param&egrave;tres valides li&eacute;s &agrave; une personnalisation */
    public static NSArray rechercherParametresPersonnelsValidesPourPersonnalisation(EOEditingContext editingContext,EOPrimePersonnalisation personnalisation) {
    	return rechercherParametresPersonnelsPourPersonnalisationEtCode(editingContext,personnalisation,null,true);
    }
    /** Retourne les param&egrave;tres valides li&eacute;s &agrave; un individu, une prime et un code (ce dernier peut &ecirc;tre nul) */
    public static NSArray rechercherParametresPersonnelsValidesPourIndividuCodeEtAnnee(EOEditingContext editingContext,EOIndividu individu,String code,int annee) {
    	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("personnalisation.individu = %@", new NSArray(individu)));
    	NSTimestamp debutAnnee = DateCtrl.stringToDate("01/01/" + annee);
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("personnalisation.debutValidite >= %@", new NSArray(debutAnnee)));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("personnalisation.finValidite < %@", new NSArray(DateCtrl.dateAvecAjoutAnnees(debutAnnee, 1))));
    	if (code != null) {
    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("code.pcodCode = %@", new NSArray(code)));
    	}
    	qualifiers.addObject(qualifierPourValidite(true));
    	EOFetchSpecification fs = new EOFetchSpecification("PrimeParamPerso",new EOAndQualifier(qualifiers),null);
    	fs.setRefreshesRefetchedObjects(true);
    	return editingContext.objectsWithFetchSpecification(fs);
    }
    // Méthodes privées
    private static NSArray rechercherParametresPersonnelsPourPersonnalisationEtCode(EOEditingContext editingContext,EOPrimePersonnalisation personnalisation,EOPrimeCode code,boolean estValide) {
    	if (personnalisation != null) {
	    	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("personnalisation = %@", new NSArray(personnalisation)));
	    	if (code != null) {
	    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("code = %@", new NSArray(code)));
	    	}
	    	qualifiers.addObject(qualifierPourValidite(estValide));
	    	EOFetchSpecification fs = new EOFetchSpecification("PrimeParamPerso",new EOAndQualifier(qualifiers),null);
	    	fs.setRefreshesRefetchedObjects(true);
	    	return editingContext.objectsWithFetchSpecification(fs);
    	} else {
    		return null;
    	}
    }
}

//Created on Wed Jun 11 12:24:00 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.goyave.primes;

import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** Comporte toutes les donn&eacute;es n&eacute;cessaires pour v&eacute;rifier si un individu peut b&eacute;n&eacute;ficier d'une prime
 * et pour &eacute;valuer le montant de la prime */
public class IndividuPourPrime implements NSKeyValueCoding,NSCoding  {
	private EOCorps corps;
	private EOGrade grade;
	private EOAssociation fonction;
	String echelon,indice;
	private EOTypePopulation population;
	private EOTypeContratTravail typeContrat;
	private NSTimestamp periodeDebut,periodeFin;
	private EOIndividu individu;
	private EOGlobalID individuID,corpsID,gradeID,fonctionID,populationID,typeContratID;	// pour les transferts d'objets

	public IndividuPourPrime() {
		super();
	}
	/** Constructeur pour NSCoding */
	public IndividuPourPrime(NSDictionary dict) {
		individuID = (EOGlobalID)dict.objectForKey("individu");
		corpsID = (EOGlobalID)dict.objectForKey("corps");
		gradeID = (EOGlobalID)dict.objectForKey("grade");
		echelon = (String)dict.objectForKey("echelon");
		populationID = (EOGlobalID)dict.objectForKey("population");
		typeContratID = (EOGlobalID)dict.objectForKey("typeContrat");
		fonctionID =  (EOGlobalID)dict.objectForKey("fonction");
		periodeDebut = (NSTimestamp)dict.objectForKey("periodeDebut");
		periodeFin = (NSTimestamp)dict.objectForKey("periodeFin");
		indice = (String)dict.objectForKey("indice");
	}
	// Accesseurs
	public EOCorps corps() {
		return corps;
	}
	public void setCorps(EOCorps value) {
		corps = value;
	}
	public EOGrade grade() {
		return grade;
	}
	public void setGrade(EOGrade value) {
		grade = value;
	}
	public String echelon() {
		return echelon;
	}
	public void setEchelon(String value) {
		echelon = value;
	}
	public NSTimestamp periodeDebut() {
		return periodeDebut;
	}
	public void setPeriodeDebut(NSTimestamp value) {
		periodeDebut = value;
	}
	public NSTimestamp periodeFin() {
		return periodeFin;
	}
	public void setPeriodeFin(NSTimestamp value) {
		periodeFin = value;
	}
	public EOTypePopulation population() {
		return population;
	}
	public void setPopulation(EOTypePopulation value) {
		population = value;
	}
	public EOTypeContratTravail typeContrat() {
		return typeContrat;
	}
	public void setTypeContrat(EOTypeContratTravail value) {
		typeContrat = value;
	}
	public String indice() {
		return indice;
	}
	public void setIndice(String value) {
		indice = value;
	}
	public EOAssociation fonction() {
		return fonction;
	}
	public void setFonction(EOAssociation value) {
		fonction = value;
	}
	public EOIndividu individu() {
		return individu;
	}
	public String nomIndividu() {
		if (individu != null) {
			return individu.nomUsuel();
		} else {
			return null;
		}
	}
	public void setElementCarriere(EOElementCarriere element) {
		setCorps(element.toCorps());
		setGrade(element.toGrade());
		setEchelon(element.cEchelon());
		setIndice(element.indiceBrut());
	}
	public void setContratAvenant(EOContratAvenant avenant) {
		setTypeContrat(avenant.contrat().toTypeContratTravail());
		setGrade(avenant.toGrade());
		setEchelon(avenant.cEchelon());
		setIndice(avenant.indiceContrat());
	}
	public void initAvecIndividuEtDates(EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		this.individu = individu;
		setPeriodeDebut(dateDebut);
		setPeriodeFin(dateFin);
	}
	public boolean estTypeTitulaire() {
		return corps() != null || grade() != null;
	}
	public boolean estTypeContractuel() {
		return typeContrat() != null;
	}
	public boolean estTypeFonction() {
		return fonction() != null;
	}
	public String toString() {
		String texte = "";
		if (periodeDebut() != null) {
			texte += "debut : " + DateCtrl.dateToString(periodeDebut()) + ",";
		}
		if (periodeFin() != null) {
			texte += "fin : " + DateCtrl.dateToString(periodeFin()) + ",";
		}
		if (population() != null) {
			texte += "population : " + population().libelleCourt() + ",";
		}
		if (typeContrat() != null) {
			texte += "type contrat : " + typeContrat().libelleCourt() + ",";
		}
		if (corps() != null) {
			texte += "corps : " + corps().lcCorps() + ",";
		}
		if (grade() != null) {
			texte += "grade : " + grade().lcGrade() + ",";
		}
		if (echelon() != null) {
			texte += "echelon : " + echelon() + ",";
		}
		if (fonction() != null) {
			texte += "fonction : " + fonction().libelleLong() + ",";
		}
		if (indice() != null) {
			texte += "indice : " + indice() + ",";
		}
		if (texte.length() == 0) {
			return texte;
		} else {
		return texte.substring(0,texte.length() - 1);	// Pour supprimer la dernière virgule
		}
	}
	public boolean estEgalPourPrime(IndividuPourPrime individuPourPrime,EOPrime prime) {
		if (individu() != individuPourPrime.individu()) {
			return false;
		}
		if (prime.verifierPopulation() && population() != individuPourPrime.population()) {
			return false;
		}
		if (prime.verifierCorps() && corps() != individuPourPrime.corps()) {
			return false;
		}
		if (prime.verifierGrade() && grade() != individuPourPrime.grade()) {
			return false;
		}
		if (prime.verifierEchelon() && ((echelon() != null && individuPourPrime.echelon() != null && echelon().equals(individuPourPrime.echelon()) == false) ||
				echelon() != individuPourPrime.echelon())) {
			return false;
		}
		if (prime.verifierContrat() && typeContrat() != individuPourPrime.typeContrat()) {
			return false;
		}
		if (prime.verifierFonction() && fonction() != individuPourPrime.fonction()) {
			return false;
		}
		if (prime.verifierIndice() && ((indice() != null && individuPourPrime.indice() != null && indice().equals(individuPourPrime.indice()) == false) ||
				indice() != individuPourPrime.indice())) {
			return false;
		}
		return true;
	}
	/** Transforme les globalIDs en objet dans l'editing context */
	public void preparerPourEditingContext(EOEditingContext editingContext) {
		individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(individuID, editingContext);
		if (corpsID != null) {
			corps = (EOCorps)SuperFinder.objetForGlobalIDDansEditingContext(corpsID, editingContext);
		}
		if (gradeID != null) {
			grade = (EOGrade)SuperFinder.objetForGlobalIDDansEditingContext(gradeID, editingContext);
		}
		if (populationID != null) {
			population = (EOTypePopulation)SuperFinder.objetForGlobalIDDansEditingContext(populationID, editingContext);
		}
		if (typeContratID != null) {
			typeContrat = (EOTypeContratTravail)SuperFinder.objetForGlobalIDDansEditingContext(typeContratID, editingContext);
		}
		if (fonctionID != null) {
			fonction = (EOAssociation)SuperFinder.objetForGlobalIDDansEditingContext(fonctionID, editingContext);
		}
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Interface NSCoding
	public Class classForCoder() {
		return IndividuPourPrime.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		NSMutableDictionary dict = new NSMutableDictionary();
		EOEditingContext editingContext = individu().editingContext();
		dict.setObjectForKey(editingContext.globalIDForObject(individu()), "individu");
		if (corps() != null)
			dict.setObjectForKey(editingContext.globalIDForObject(corps()), "corps");
		if (grade() != null) 
			dict.setObjectForKey(editingContext.globalIDForObject(grade()), "grade");	
		if (echelon() != null)
			dict.setObjectForKey(echelon(), "echelon");	
		if (population() != null)
			dict.setObjectForKey(editingContext.globalIDForObject(population()), "population");	
		if (typeContrat() != null)
			dict.setObjectForKey(editingContext.globalIDForObject(typeContrat()), "typeContrat");	
		if (fonction() != null)
			dict.setObjectForKey(editingContext.globalIDForObject(fonction()), "fonction");	
		dict.setObjectForKey(periodeDebut(), "periodeDebut");
		if (periodeFin() != null)
			dict.setObjectForKey(periodeFin(), "periodeFin");
		if (indice() != null)
			dict.setObjectForKey(indice(), "indice");

		coder.encodeObject(dict);
	}
	public static Class decodeClass() {
		return IndividuPourPrime.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new IndividuPourPrime((NSDictionary)coder.decodeObject());
	}

	// méthodes privées
	
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.goyave.primes;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;
import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.goyave.EOPrimeCode;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** Comporte toutes les donn&eacute;es n&eacute;cessaires pour &eacute;valuer le montant d'une prime */
public class InformationPourPluginPrime  {
	private EOPrime prime;
	private IndividuPourPrime individuPourPrime;
	private NSArray parametresValides;
	private NSArray personnalisations;
	private NSTimestamp debutPeriodePourAttribution,finPeriodePourAttribution;

	/** Constructeur
	 * @param prime prime s&eacute;lectionn&eacute;e
	 * @param individuPourPrime individu &eacute;ligible pour cette prime
	 * @param debutPeriodePourAttribution d&eacute;but de la p&eacute;riode &agrave; laquelle la prime doit &circ;tre th&eacute;oriquement attribu&eacute;e
	 * @param finPeriodePourAttribution	 fin de la p&eacute;riode &agrave; laquelle la prime doit &circ;tre th&eacute;oriquement attribu&eacute;e
	 */
	public InformationPourPluginPrime(EOPrime prime,IndividuPourPrime individuPourPrime,NSTimestamp debutPeriodePourAttribution,NSTimestamp finPeriodePourAttribution) {
		super();
		this.prime = prime;
		this.individuPourPrime = individuPourPrime;
		this.debutPeriodePourAttribution = debutPeriodePourAttribution;
		this.finPeriodePourAttribution = finPeriodePourAttribution;
		preparerParametresValides();
		preparerPersonnalisations();
	}
	// Accesseurs
	public EOPrime prime() {
		return prime;
	}
	public EOIndividu individu() {
		if (individuPourPrime != null) {
			return individuPourPrime.individu();
		} else {
			return null;
		}
	}
	public EOGrade gradeIndividu() {
		if (individuPourPrime != null) {
			return individuPourPrime.grade();
		} else {
			return null;
		}
	} 
	public String indiceIndividu() {
		if (individuPourPrime != null) {
			return individuPourPrime.indice();
		} else {
			return null;
		}
	} 
	public EOAssociation fonctionIndividu() {
		if (individuPourPrime != null) {
			return individuPourPrime.fonction();
		} else {
			return null;
		}
	}
	public EOEditingContext editingContext() {
		if (prime == null) {
			return null;
		} else {
			return prime.editingContext();
		}
	}
	/** d&eacute;but de la p&eacute;riode &agrave; laquelle l'individu peut b&eacute;n&eacute;ficier de la prime */
	public NSTimestamp debutPeriode() {
		if (individuPourPrime == null) {
			return null;
		} else {
			return individuPourPrime.periodeDebut();
		}
	}
	/** fin de la p&eacute;riode &agrave; laquelle l'individu peut b&eacute;n&eacute;ficier de la prime */
	public NSTimestamp finPeriode() {
		if (individuPourPrime == null) {
			return null;
		} else {
			return individuPourPrime.periodeFin();
		}
	}
	/** d&eacute;but de la p&eacute;riode &agrave; laquelle la prime doit &circ;tre th&eacute;oriquement attribu&eacute;e */
	public NSTimestamp debutPeriodePourAttribution() {
		return debutPeriodePourAttribution;
	}
	/** fin de la p&eacute;riode &agrave; laquelle la prime doit &circ;tre th&eacute;oriquement attribu&eacute;e */
	public NSTimestamp finPeriodePourAttribution() {
		return finPeriodePourAttribution;
	}
	/** Retourne tous les codes (PrimeCode) associ&eacute;s &agrave; la prime */
	public NSArray codes() {
		if (prime == null) {
			return null;
		} else {
			return prime.codes();
		}
	}
	/** Retourne tous les param&egrave;tres (EOPrimeParam) valides pour la p&eacute;riode, associ&eacute;s &agrave; la prime */
	public NSArray parametresValides() {
		return parametresValides;
	}
	/** Retourne tous les param&egrave;tres (EOPrimeParam) valides pour la p&eacute;riode associ&eacute;s &agrave; un code */
	public NSArray parametresPourCodeEtDates(String code,NSTimestamp debutValidite,NSTimestamp finValidite) throws Exception {
		String message = getClass().getName() + " - ";
		// Rechercher dans les paramètres standard, le taux correspondant à la catégorie choisies
		if (parametresValides() == null) {
			message += "Contactez votre administrateur, la prime est insufisamment définie";
			LogManager.logDetail(message);
			throw new Exception(message);
		} else {
			NSMutableArray parametres = new NSMutableArray();
			// Recherche des valeurs pour la catégorie choisie
			java.util.Enumeration e = parametresValides().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.code().pcodCode().equals(code) && 
						Utilitaires.IntervalleTemps.intersectionPeriodes(debutValidite, finValidite, parametre.debutValidite(), parametre.finValidite()) != null) {
					parametres.addObject(parametre);
				}
			}
			if (parametres.count() == 0) {
				message += "Contactez votre administrateur, Pas de valeur du paramètre pour le code " + code;
				LogManager.logDetail(message);
				throw new Exception(message);
			}
			return parametres;
		}
	}
	/** Retourne tous les param&egrave;tres (EOPrimeParam) valides pour la p&eacute;riode associ&eacute;s &agrave; un code */
	public NSArray parametresPourCodeFonctionEtDates(String code,String fonction,NSTimestamp debutValidite,NSTimestamp finValidite) throws Exception {
		String message = getClass().getName() + " - ";
		// Rechercher dans les paramètres standard, le taux correspondant à la catégorie choisies
		if (parametresValides() == null) {
			message += "Contactez votre administrateur, la prime est insufisamment définie";
			LogManager.logDetail(message);
			throw new Exception(message);
		} else {
			NSMutableArray parametres = new NSMutableArray();
			// Recherche des valeurs pour la catégorie choisie
			java.util.Enumeration e = parametresValides().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.code().pcodCode().equals(code) && parametre.fonction().libelleLong().equals(fonction) &&
						Utilitaires.IntervalleTemps.intersectionPeriodes(debutValidite, finValidite, parametre.debutValidite(), parametre.finValidite()) != null) {
					parametres.addObject(parametre);
				}
			}
			if (parametres.count() == 0) {
				message += "Contactez votre administrateur, Pas de valeur du paramètre pour le code " + code;
				LogManager.logDetail(message);
				throw new Exception(message);
			}
			return parametres;
		}
	}
	/** Retourne tous les param&egrave;tres (EOPrimeParam) valides pour la p&eacute;riode associ&eacute;s &agrave; un code et un grade */
	public NSArray parametresPourCodeGradeEtDates(String code,EOGrade grade,NSTimestamp debutValidite,NSTimestamp finValidite) throws Exception {
		String message = getClass().getName() + " - ";
		// Rechercher dans les paramètres standard, le taux correspondant à la catégorie choisies
		if (parametresValides() == null) {
			message += "Contactez votre administrateur, la prime est insufisamment définie";
			LogManager.logDetail(message);
			throw new Exception(message);
		} else {
			NSMutableArray parametres = new NSMutableArray();
			// Recherche des valeurs pour la catégorie choisie
			java.util.Enumeration e = parametresValides().objectEnumerator();
			while (e.hasMoreElements()) {
				EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
				if (parametre.code().pcodCode().equals(code) && parametre.grade() == grade && 
						Utilitaires.IntervalleTemps.intersectionPeriodes(debutValidite, finValidite, parametre.debutValidite(), parametre.finValidite()) != null) {
					parametres.addObject(parametre);
				}
			}
			if (parametres.count() == 0) {
				message += "Contactez votre administrateur, Pas de valeur du paramètre pour le code " + code + " et grade " + grade.llGrade();
				LogManager.logDetail(message);
				throw new Exception(message);
			}
			return parametres;
		}
	}
	/** Retourne toutes les personnalisations de l'individu pour cette prime(EOPrimeParam) valides pour la p&eacute;riode, associ&eacute;s &agrave; la prime */
	public NSArray personnalisations() {
		return personnalisations;
	}
	public NSArray parametresPersonnels(EOPrimePersonnalisation personnalisation) {
		if (personnalisation == null) {
			return null;
		} else {
			return EOPrimeParamPerso.rechercherParametresPersonnelsValidesPourPersonnalisation(editingContext(), personnalisation);
		}
	}
	// Méthodes privées
	private void preparerParametresValides() {
		if (prime == null) {
			return;
		}
		NSMutableArray parametres = new NSMutableArray();
		java.util.Enumeration e = prime.codes().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeCode code = (EOPrimeCode)e.nextElement();
			NSArray result = EOPrimeParam.rechercherParametresValidesPourCodeEtPeriode(editingContext(), code, debutPeriode(), finPeriode());
			parametres.addObjectsFromArray(result);
		}
		parametresValides = new NSArray(parametres);
	}
	private void preparerPersonnalisations() {
		if (prime == null || individuPourPrime == null || debutPeriode() == null || finPeriode() == null) {
			return;
		}
		if (prime.estPersonnalisable()) {
			personnalisations = EOPrimePersonnalisation.rechercherPersonnalisationsValidesPourIndividuPrimeEtPeriode(editingContext(), individuPourPrime.individu(), prime, debutPeriode(), finPeriode());
		}
	}
}

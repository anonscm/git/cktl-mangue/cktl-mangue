// EOPrimePersonnalisation.java
// Created on Thu Oct 23 09:48:40 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.goyave.primes;

import java.math.BigDecimal;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.ObjetAvecDureeValiditePourPrime;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Une personnalisation peut avoir trois &eacute;tats : valide, utilis&eacute;e (pour des attributions qui ont &eacute;t&eacute; valid&eacute;es), invalides (annul&eacute;es)
 * 
 * @author christine
 *
 */
public class EOPrimePersonnalisation extends ObjetAvecDureeValiditePourPrime {
	public final static String TYPE_UTILISE = "U";
    public EOPrimePersonnalisation() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPrimePersonnalisation(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public NSTimestamp dCreation() {
		return (NSTimestamp)storedValueForKey("dCreation");
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, "dCreation");
	}

	public NSTimestamp dModification() {
		return (NSTimestamp)storedValueForKey("dModification");
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, "dModification");
	}
	
	public BigDecimal pmpeMontant() {
        return (BigDecimal)storedValueForKey("pmpeMontant");
    }

    public void setPmpeMontant(BigDecimal value) {
        takeStoredValueForKey(value, "pmpeMontant");
    }
    
    public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
        return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey("individu");
    }

    public void setIndividu(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
        takeStoredValueForKey(value, "individu");
    }
    
    // Méthodes ajoutées
    public void initAvecPrimeEtDates(EOPrime prime,NSTimestamp debutValidite, NSTimestamp finValidite) {
    	initAvecIndividuPrimeEtDates(null, prime, debutValidite, finValidite);
    }
    public void initAvecIndividuPrimeEtDates(EOIndividu individu, EOPrime prime,NSTimestamp debutValidite, NSTimestamp finValidite) {
    	super.initAvecPrime(prime);
    	setDebutValidite(debutValidite);
    	setFinValidite(finValidite);
    	if (individu != null) {
    		addObjectToBothSidesOfRelationshipWithKey(individu, "individu");
    	}
    }
    public void validateForSave() throws NSValidation.ValidationException {
    	super.validateForSave();
    	if (individu() == null) {
    		throw new NSValidation.ValidationException("Vous devez fournir un individu");
    	}
    }
    public boolean estUtilisee() {
    	return temValide() != null && temValide().equals(TYPE_UTILISE);
    }
    public void setEstUtilisee() {
    	setTemValide(TYPE_UTILISE);
    }
	/** Retourne un libell&eacute; pour indiquer la nature du montant */
	public String libelleMontant() {
		if (prime() == null) {
			return null;
		} else {
			return prime().libellePourMontant();
		}
	}
	/** Retourne une nouvelle personnalisation valide pour la p&eacute;riode pass&eacute; en param&grave;tre */
	public EOPrimePersonnalisation clonerAvecDates(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		EOPrimePersonnalisation nouvellePersonnalisation = new EOPrimePersonnalisation();
		nouvellePersonnalisation.takeValuesFromDictionary(this.snapshot());
		NSTimestamp today = new NSTimestamp();
		nouvellePersonnalisation.setDCreation(today);
		nouvellePersonnalisation.setDModification(today);
		nouvellePersonnalisation.setDebutValidite(debutPeriode);
		nouvellePersonnalisation.setFinValidite(finPeriode);
		nouvellePersonnalisation.setEstValide(true);
		return nouvellePersonnalisation;
	}
    // Méthodes statiques
    /** Retourne les personnalisations utilis&eacute;es pour des attributions pour une prime et une p&eacute;riode */
    public static NSArray rechercherPersonnalisationsUtiliseesPourPrimeEtPeriode(EOEditingContext editingContext,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	return rechercherPersonnalisationsPourTypePrimeEtPeriode(editingContext, TYPE_UTILISE, prime, debutPeriode, finPeriode);
    }
    /** Retourne les personnalisations valides (i.e non utilis&eacute;es) pour une prime et une p&eacute;riode */
    public static NSArray rechercherPersonnalisationsValidesPourPrimeEtPeriode(EOEditingContext editingContext,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	return rechercherPersonnalisationsPourTypePrimeEtPeriode(editingContext, CocktailConstantes.VRAI, prime, debutPeriode, finPeriode);
    }
    /** Retourne les personnalisations annul&eacute;es pour une prime et une p&eacute;riode */
    public static NSArray rechercherPersonnalisationsInvalidesPourPrimeEtPeriode(EOEditingContext editingContext,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	return rechercherPersonnalisationsPourTypePrimeEtPeriode(editingContext, CocktailConstantes.FAUX, prime, debutPeriode, finPeriode);
    }
    /** Retourne les personnalisations valides d'un individu pour la p&eacute;riode et la prime, class&eacute;es par ordre de date
     * d&eacute;croissante */
    public static NSArray rechercherPersonnalisationsValidesPourIndividuPrimeEtPeriode(EOEditingContext editingContext,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	return rechercherPersonnalisationsPourIndividuPrimeEtPeriode(editingContext,individu,prime,debutPeriode,finPeriode,true);
    }
    /** Retourne la personnalisation correspondant &agrave; la la p&eacute;riode pour un individu et une prime */
    public static EOPrimePersonnalisation rechercherPersonnalisationValidePourIndividuPrimeEtPeriode(EOEditingContext editingContext,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	try {
    		return (EOPrimePersonnalisation)rechercherPersonnalisationsPourIndividuPrimeEtPeriode(editingContext,individu,prime,debutPeriode,finPeriode,true).objectAtIndex(0);
    	} catch (Exception e) {
    		return null;
    	}
    }
    /** Retourne la personnalisation valide ou utilis&eacute;e correspondant &agrave; la la p&eacute;riode pour un individu et une prime */
    public static EOPrimePersonnalisation rechercherPersonnalisationEnCoursPourIndividuPrimeEtPeriode(EOEditingContext editingContext,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	try {
        	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("individu = %@", new NSArray(individu)));
        	qualifiers.addObject(qualifierPourPrime(prime));
        	qualifiers.addObject(SuperFinder.qualifierPourPeriode("debutValidite", debutPeriode, "finValidite", finPeriode));
        	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temValide = 'O' OR temValide = 'U'", null));
        	EOFetchSpecification fs = new EOFetchSpecification("PrimePersonnalisation",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending)));
        	fs.setRefreshesRefetchedObjects(true);
    		return (EOPrimePersonnalisation)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
    	} catch (Exception e) {
    		return null;
    	}
    }
    /** Retourne les personnalisations valides ant&eacute;rieures pour un individu et une prime */
    public static NSArray rechercherPersonnalisationAnterieuresPourIndividuPrime(EOEditingContext editingContext,NSTimestamp date,EOIndividu individu,EOPrime prime) {
    	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("individu = %@", new NSArray(individu)));
    	qualifiers.addObject(qualifierPourPrime(prime));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("debutValidite < %@ AND (temValide = 'O' OR temValide = 'U')", new NSArray(date)));
    	EOFetchSpecification fs = new EOFetchSpecification("PrimePersonnalisation",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending)));
    	fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
    }
    /** Retourne les personnalisations valides pour une ann&eacute;e */
    public static NSArray rechercherPersonnalisationPourAnneeEtPrime(EOEditingContext editingContext,int annee,EOPrime prime) {
    	NSTimestamp dateDebut = DateCtrl.stringToDate("01/01/" + annee), dateFin = DateCtrl.stringToDate("31/12/" + annee);
    	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("debutValidite >= %@", new NSArray(dateDebut)));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("finValidite <= %@", new NSArray(dateFin)));
    	qualifiers.addObject(qualifierPourPrime(prime));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(temValide = 'O' OR temValide = 'U')", null));
    	EOFetchSpecification fs = new EOFetchSpecification("PrimePersonnalisation",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending)));
    	fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
    }
    /** Retourne les personnalisations invalides d'un individu pour la p&eacute;riode et la prime, class&eacute;es par ordre de date
     * d&eacute;croissante */
    public static NSArray rechercherPersonnalisationsInvalidesPourIndividuPrimeEtPeriode(EOEditingContext editingContext,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	return rechercherPersonnalisationsPourIndividuPrimeEtPeriode(editingContext,individu,prime,debutPeriode,finPeriode,false);
    }
    // Méthodes privées
    private static NSArray rechercherPersonnalisationsPourIndividuPrimeEtPeriode(EOEditingContext editingContext,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean estValide) {
    	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("individu = %@", new NSArray(individu)));
    	qualifiers.addObject(qualifierPourPrime(prime));
    	qualifiers.addObject(qualifierPourValidite(estValide, debutPeriode, finPeriode));
    	EOFetchSpecification fs = new EOFetchSpecification("PrimePersonnalisation",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending)));
    	fs.setRefreshesRefetchedObjects(true);
    	return editingContext.objectsWithFetchSpecification(fs);
    }
    private static NSArray rechercherPersonnalisationsPourTypePrimeEtPeriode(EOEditingContext editingContext,String type,EOPrime prime,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("temValide = %@", new NSArray(type)));
    	qualifiers.addObject(qualifierPourPrime(prime));
    	qualifiers.addObject(SuperFinder.qualifierPourPeriode("debutValidite", debutPeriode, "finValidite", finPeriode));
    	EOFetchSpecification fs = new EOFetchSpecification("PrimePersonnalisation",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite", EOSortOrdering.CompareDescending)));
    	fs.setRefreshesRefetchedObjects(true);
    	return editingContext.objectsWithFetchSpecification(fs);
    }
}

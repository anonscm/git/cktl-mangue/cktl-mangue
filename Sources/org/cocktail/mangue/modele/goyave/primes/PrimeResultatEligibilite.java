/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.goyave.primes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;

/** D&eacute;crit le r&eacute;sultat de la recherche de l'&eacute;gilibit&eacute; d'individus avec un tableau des individus
 * &eacute;ligibles ou (exclusif) un diagnostic de non-&eacute;ligibilit&eacute;
 * @author christine
 *
 */
public class PrimeResultatEligibilite implements NSKeyValueCoding, NSCoding {
	private String diagnosticRefus;
	private NSArray individusEligibles;
	
	/** Constructeur par d&eacute;faut */
	public PrimeResultatEligibilite() {
	}
	/** Constructeur : Un diagnostic de refus non null supprime tous les individus &eacute;ligibles. 
	 * Fournir un tableau non null et non vide supprime le diagnostic de refus  */
	public PrimeResultatEligibilite(NSArray individusEligibles,String diagnosticRefus) {
		setIndividusEligibles(individusEligibles);
		setDiagnosticRefus(diagnosticRefus);
	}
	/** Constructeur pour NSCoder */
	public PrimeResultatEligibilite(NSDictionary aDict) {
		diagnosticRefus = (String)aDict.valueForKey("diagnosticRefus");
		individusEligibles = (NSArray)aDict.valueForKey("individusEligibles");
	}
	// Accesseurs
	public String diagnosticRefus() {
		return diagnosticRefus;
	}
	/** Un diagnostic de refus non null supprime tous les individus &eacute;ligibles */
	public void setDiagnosticRefus(String aString) {
		if (aString != null) {
			setIndividusEligibles(null);
		}
		this.diagnosticRefus = aString;
	}
	public NSArray individusEligibles() {
		return individusEligibles;
	}
	/** Fournir un tableau non null et non vide supprime le diagnostic de refus */
	public void setIndividusEligibles(NSArray anArray) {
		if (anArray != null && anArray.count() > 0) {
			setDiagnosticRefus(null);
		}
		this.individusEligibles = anArray;
	}
	/** Retourne le nombre d'individus &eacute;ligibles */
	public int nbIndividusEligibles() {
		return (individusEligibles == null) ? 0 : individusEligibles().count();
	}
	/** Retourne l'individu &eacute;ligible &agrave; la position */
	public IndividuPourPrime individuPourPrimeAtIndex(int index) {
		if (individusEligibles() == null) {
			return null;
		} else {
			return (IndividuPourPrime)individusEligibles().objectAtIndex(index);
		}
	}
	/** Pour transformer les individus &eacute;ligibles en IndividuPourPrime */
	public void preparerDansEditingContext(EOEditingContext editingContext) {
		java.util.Enumeration e = individusEligibles.objectEnumerator();
		while (e.hasMoreElements()) {
			IndividuPourPrime individuPrime = (IndividuPourPrime)e.nextElement();
			individuPrime.preparerPourEditingContext(editingContext);
		}
	}
	public String toString() {
		String s = "";
		
		if (diagnosticRefus() != null) {
			s += ", diagnostic refus : " + diagnosticRefus();
		}
		if (individusEligibles() != null) {
			s += ", nb individus eligibles : " + individusEligibles.count();
		}
		return s;
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Interface NSCoding
	public Class classForCoder() {
		return PrimeResultatEligibilite.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		NSMutableDictionary dict = new NSMutableDictionary();
		if (diagnosticRefus() != null)
			dict.setObjectForKey(diagnosticRefus(), "diagnosticRefus");
		if (individusEligibles() != null) {
			dict.setObjectForKey(individusEligibles, "individusEligibles");	
		}
		coder.encodeObject(dict);
	}
	public static Class decodeClass() {
		return PrimeResultatEligibilite.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new PrimeResultatEligibilite((NSDictionary)coder.decodeObject());
	}


}

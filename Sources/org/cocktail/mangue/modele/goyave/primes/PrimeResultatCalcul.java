/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.goyave.primes;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;

/** D&eacute;crit l'ensemble des r&eacute;sultats de l'&eacute;valuation et du calcul d'une prime pour un individu. Contient un diagnostic sur le
 * d&eacute;roulement du traitement
 * typeResultat indique si les r&eacute;sultats sont des PrimeAttribution ou des PrimeResulat. Dans le cas de PrimeAttribution, le tableau des resultats
 * contient des globalIDs sur les PrimeAttribution */
public class PrimeResultatCalcul implements NSKeyValueCoding, NSCoding {
	public final static int TYPE_ATTRIBUTION = 0;
	public final static int TYPE_PRIME_RESULTAT = 1;
	/** diagnostic quand un individu ne peut b&eacute;n&eacute;ficier de la prime */
	public final static String INDIVIDU_NON_ELIGIBLE = "Individu non eligible";
	/** diagnostic quand un individu n'a pas d'attribution */
	public final static String SANS_ATTRIBUTION = "Pas d'attribution";
	private String diagnostic;
	private NSArray resultats;
	private int typeResultat;
	
	// Constructeur pour NSCoder
	public PrimeResultatCalcul(NSDictionary dict) {
		diagnostic = (String)dict.objectForKey("diagnostic");
		typeResultat = ((Integer)dict.objectForKey("typeResultat")).intValue();
		resultats = (NSArray)dict.objectForKey("resultats");
	}
	public PrimeResultatCalcul(int typeResultat,String diagnostic,NSArray resultats) {
		this.typeResultat = typeResultat;
		this.diagnostic = diagnostic;
		this.resultats = resultats;
	}
	// Accesseurs
	public String diagnostic() {
		return diagnostic;
	}	
	public NSArray resultats() {
		return resultats;
	}
	public int typeResultat() {
		return typeResultat;
	}
	public String toString() {
		String s = "typeResultat : ";
		
		switch(typeResultat()) {
		case TYPE_ATTRIBUTION : s += "attributions";
		break;
		case TYPE_PRIME_RESULTAT : s += "prime resultats";
		break;
		}
		s += ", diagnostic : " + diagnostic();
		if (resultats() != null) {
			s += ", resultats : " + resultats;
		}
		return s;
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Interface NSCoding
	public Class classForCoder() {
		return PrimeResultatCalcul.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		NSMutableDictionary dict = new NSMutableDictionary();
		dict.setObjectForKey(new  Integer(typeResultat), "typeResultat");
		if (diagnostic() != null)
			dict.setObjectForKey(diagnostic(), "diagnostic");
		if (resultats() != null)
			dict.setObjectForKey(resultats, "resultats");	
	
		coder.encodeObject(dict);
	}
	public static Class decodeClass() {
		return PrimeResultatCalcul.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new PrimeResultatCalcul((NSDictionary)coder.decodeObject());
	}

}

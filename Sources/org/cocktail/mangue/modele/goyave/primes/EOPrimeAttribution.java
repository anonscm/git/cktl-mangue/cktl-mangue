// EOPrimeAttribution.java
// Created on Fri Sep 26 09:54:20 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.goyave.primes;

import java.math.BigDecimal;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOPrimeAttribution extends _EOPrimeAttribution {
	
	public static EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);

	/** Le montant de l'attribution est a calculer */
	private final static String ATTRIBUTION_A_CALCULER = "C";
	/** Le montant de l'attribution est a evalue */
	private final static String ATTRIBUTION_PROVISOIRE = "P";
	/** Le montant de l'attribution est suspendu (non disponible dans le budget) */
	private final static String ATTRIBUTION_SUSPENDUE = "S";
	
    public EOPrimeAttribution() {
        super();
    }

    /**
     * 
     * @param ec
     * @param individu
     * @return
     */
	public static EOPrimeAttribution creer(EOEditingContext ec, EOIndividu individu) {
		
		EOPrimeAttribution newObject = new EOPrimeAttribution();
		newObject.setIndividuRelationship(individu);
		newObject.init();
		ec.insertObject(newObject);
		return newObject;
	}

	/**
	 * 
	 * @param ec
	 * @param qualifier
	 * @return
	 */
	public static NSArray<EOPrimeAttribution> findForQualifier(EOEditingContext ec, EOQualifier qualifier) {
		try {
			return fetchAll(ec, qualifier, null );
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOPrimeAttribution>();
		}
	}
    /** Une attribution a calculer est une attribution qui n'a pas encore ete calculee */
    public boolean estCalculee() {
    	return temValide() != null && temValide().equals(ATTRIBUTION_A_CALCULER);
    }
    public void setEstCalculee() {
    	setTemValide(ATTRIBUTION_A_CALCULER);
    }
    /** Une attribution provisoire est une attribution qui a &eacute;t&eacute; &eacute;valu&eacute;e mais pas encore valid&eacute;e */
    public boolean estProvisoire() {
    	return temValide() != null && temValide().equals(ATTRIBUTION_PROVISOIRE);
    }
    public void setEstProvisoire() {
    	setTemValide(ATTRIBUTION_PROVISOIRE);
    }
    /** Une attribution suspendue est une attribution qui a &eacute;t&eacute; &eacute;valu&eacute;e mais qui ne rentre pas dans le budget
     */ 
    public boolean estSuspendue() {
    	return temValide() != null && temValide().equals(ATTRIBUTION_SUSPENDUE);
    }
    public void setEstSuspendue() {
    	setTemValide(ATTRIBUTION_SUSPENDUE);
    }
    public boolean estInvalide() {
    	return temValide() != null && temValide().equals(CocktailConstantes.FAUX);
    }
    public void setEstInvalide() {
    	setTemValide(CocktailConstantes.FAUX);
    }
    public void initAvecPrimeEtIndividu(EOPrime prime, EOIndividu individu) {
    	super.initAvecPrime(prime);
    	NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
    	setPattMontant(new BigDecimal(0.00));
    	setEstProvisoire();
    	addObjectToBothSidesOfRelationshipWithKey(individu, "individu");
    }
    public void initAvecPrimeEtDates(EOPrime prime,NSTimestamp dateDebut,NSTimestamp dateFin) {
    	super.initAvecPrime(prime);
    	setDebutValidite(dateDebut);
    	setFinValidite(dateFin);
    	if (prime.estCalculee()) {
    		setEstCalculee();
    	} else {
    		setEstProvisoire();
    	}
    }
    public void initAvecIndividuEtDates(EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
    	super.init();
    	setDebutValidite(dateDebut);
    	setFinValidite(dateFin);
    	setEstProvisoire();
    	addObjectToBothSidesOfRelationshipWithKey(individu, "individu");
    }
    /** Retourne le montant de l'attribution sur la p&eacute;riode ou un prorata du montant par rapport au nombre de jours 
     * de l'attribution si celle-ci d&eacute;passe la p&eacute;riode
     * @param debutPeriode
     * @param finPeriode
     * @return montant
     */
    public BigDecimal montantSurPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	if (DateCtrl.isAfterEq(debutValidite(), debutPeriode) && DateCtrl.isBeforeEq(finValidite(), finPeriode)) {
    		return pattMontant();
    	} else {
    		NSTimestamp debut = debutValidite();
    		if (DateCtrl.isBefore(debut, debutPeriode)) {
    			debut = debutPeriode;
    		}
    		NSTimestamp fin = finValidite();
    		if (DateCtrl.isAfter(fin, finPeriode)) {
    			fin = finPeriode;
    		}
    		int nbJoursPeriode = DateCtrl.nbJoursEntre(debut, fin, true);
    		int nbJoursTotal = DateCtrl.nbJoursEntre(debutValidite(), finValidite(), true);
    		double montant = (pattMontant().doubleValue() * nbJoursPeriode) / nbJoursTotal;
    		return new BigDecimal(montant).setScale(2,BigDecimal.ROUND_HALF_UP);
    	}
    	
    }
    public void modifierAvecResultat(PrimeResultat resultat) {
    	setPattMontant(resultat.montant());
    	setPattCalcul(resultat.formuleCalcul());
    	setDebutValidite(resultat.debutValidite());
    	setFinValidite(resultat.finValidite());
    }
    public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
		if (pattExercice() == null) {
			throw new NSValidation.ValidationException("Veuillez renseigner l'année !");
		} 
		if (individu() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_INDIVIDU_NON_RENSEIGNE, "Prime Attribution"));
		} 
    }
    // Méthodes statiques
    /** Retourne les attributions en cours des primes pass&eacute;es en param&grave;tre, d'un individu pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param individu (individu recherch&eacute;, peut &ecirc;tre nulle)
	 * @param primes NSArray (primes recherch&eacute;es, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherAttributionsEnCoursPourPrimesIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSArray primes,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray types = new NSMutableArray(ATTRIBUTION_A_CALCULER);
		types.addObject(ATTRIBUTION_PROVISOIRE);
		types.addObject(ATTRIBUTION_SUSPENDUE);
		types.addObject(CocktailConstantes.VRAI); // attribution valide
		return rechercherAttributionsPourIndividuPeriodeEtTypes(editingContext,individu,primes,debutPeriode,finPeriode,types);
	}
    /** Retourne les individus ayant une attribution valides pour une prime pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
     * @param editingContext
	 * @param individu (individu recherch&eacute;, peut &ecirc;tre nulle)
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
    public static NSArray rechercherIndividusAvecAttributionValidePourPrimeEtPeriode(EOEditingContext editingContext,EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
    	return (NSArray)rechercherAttributionsValidesPourPrimeIndividuEtPeriode(editingContext,null,prime,debutPeriode,finPeriode).valueForKey("individu");
    }
    /** Retourne les objets de type PrimeAttribution valides d'un individu pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param individu (individu recherch&eacute;, peut &ecirc;tre nulle)
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherAttributionsValidesPourPrimeIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSArray primes = null;
		if (prime != null) {
			primes = new NSArray(prime);
		}		return rechercherAttributionsPourIndividuPeriodeEtTypes(editingContext,individu,primes,debutPeriode,finPeriode,new NSArray(CocktailConstantes.VRAI));
	}
	/** Retourne les objets de type PrimeAttribution invalides d'un individu pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param individu (individu recherch&eacute;, peut &ecirc;tre nulle)
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherAttributionsInvalidesPourPrimeIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSArray primes = null;
		if (prime != null) {
			primes = new NSArray(prime);
		}
		return rechercherAttributionsPourIndividuPeriodeEtTypes(editingContext,individu,primes,debutPeriode,finPeriode,new NSArray(CocktailConstantes.FAUX));
	}
	/** Retourne les objets de type PrimeAttribution en attente de validation d'un individu pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param individu (individu recherch&eacute;, peut &ecirc;tre nulle)
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherAttributionsEnAttentePourPrimeIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray types = new NSMutableArray(ATTRIBUTION_A_CALCULER);
		types.addObject(ATTRIBUTION_PROVISOIRE);
		types.addObject(ATTRIBUTION_SUSPENDUE);
		NSArray primes = null;
		if (prime != null) {
			primes = new NSArray(prime);
		}
		return rechercherAttributionsPourIndividuPeriodeEtTypes(editingContext,individu,primes,debutPeriode,finPeriode,types);
	}
	/** Retourne les objets de type PrimeAttribution en cours (en attente ou valides) d'un individu pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param individu (individu recherch&eacute;, peut &ecirc;tre nulle)
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherAttributionsEnCoursPourPrimeIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray types = new NSMutableArray(ATTRIBUTION_A_CALCULER);
		types.addObject(ATTRIBUTION_PROVISOIRE);
		types.addObject(ATTRIBUTION_SUSPENDUE);
		types.addObject(CocktailConstantes.VRAI); // attribution valide
		NSArray primes = null;
		if (prime != null) {
			primes = new NSArray(prime);
		}
		return rechercherAttributionsPourIndividuPeriodeEtTypes(editingContext,individu,primes,debutPeriode,finPeriode,types);
	}
	/** Retourne les attributions d'un individu qui ont lieu sur l'exercice pass&eacute; en param&egrave;tre
	 * @param editingContext
	 * @param individu (individu recherch&eacute;)
	 * @param exercice
	 */
	public static NSArray rechercherAttributionsPourIndividuEtExercice(EOEditingContext editingContext,EOIndividu individu,Integer exercice) {
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(exercice);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND pattExercice = %@ AND temValide <> 'N'", args);
		EOFetchSpecification fs = new EOFetchSpecification("PrimeAttribution",qualifier,new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);	
	}
	/** Retourne les attributions valid&eacute;es d'un individu qui ont lieu sur les exercices pr&eacute;c&eacute;dent 
	 * l'exercice pass&eacute; en param&egrave;tre
	 * @param editingContext
	 * @param individu (individu recherch&eacute;)
	 * @param exercice
	 */
	public static NSArray rechercherAttributionsValidesPourIndividuAnterieuresExercice(EOEditingContext editingContext,EOIndividu individu,Integer exercice) {
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(exercice);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND pattExercice < %@ AND temValide = 'O' ", args);
		EOFetchSpecification fs = new EOFetchSpecification("PrimeAttribution",qualifier,new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);	
	}
	// Méthodes privées
	 /* Retourne les objets de type PrimeAttribution du type pass&eacute; en param&grave;tre d'un individu pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param individu (individu recherché, peut être nul)
	 * @param primes (primes recherchées, peut être nulle)
	 * @param debutPeriode (peut être nulle)
	 * @param finPeriode (peut être nulle)
	 * @param typesAttribution
	 */
	private static NSArray rechercherAttributionsPourIndividuPeriodeEtTypes(EOEditingContext editingContext,EOIndividu individu,NSArray primes,NSTimestamp debutPeriode, NSTimestamp finPeriode,NSArray typesAttribution) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (individu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("individu = %@", new NSArray(individu)));
		}
		if (typesAttribution != null) {
			qualifiers.addObject(SuperFinder.construireORQualifier("temValide", typesAttribution));
		}
		if (primes != null && primes.count() > 0) { 			
			qualifiers.addObject(qualifierPourPrimes(primes));
		}
		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode("debutValidite", debutPeriode, "finValidite", finPeriode));
		}
		EOFetchSpecification fs = new EOFetchSpecification("PrimeAttribution",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	private static EOQualifier qualifierPourPrimes(NSArray primes) {
		NSMutableArray qualifiers = new NSMutableArray();
		java.util.Enumeration e = primes.objectEnumerator();
		while (e.hasMoreElements()) {
			qualifiers.addObject(qualifierPourPrime((EOPrime)e.nextElement()));
		}
		return new EOOrQualifier(qualifiers);
	}
}

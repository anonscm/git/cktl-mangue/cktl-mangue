/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.goyave.primes;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** D&eacute;crit le r&eacute;sultat de l'&eacute;valuation et du calcul d'une prime pour un individu. Contient un diagnostic en
 * cas d'erreur */
public class PrimeResultat implements NSKeyValueCoding,NSCoding {
	/** Le r&eacute;sultat ne n&eacute;cessite pas de calcul */
	public final static int TYPE_SANS_CALCUL = 0;
	/** Le r&eacute;sultat  n&eacute;cessite la saisie d'un montant personnel */
	public final static int TYPE_MONTANT_PERSONNEL = 1;
	/** Le r&eacute;sultat  n&eacute;cessite un calcul */
	public final static int TYPE_CALCUL = 2;
	private BigDecimal montant;
	private int typeCalcul;
	private String formuleCalcul,messageErreur;
	private NSTimestamp debutValidite,finValidite;
	private EOGlobalID individuID;
	
	public PrimeResultat(int typeCalcul, EOGlobalID individuID, NSTimestamp debutValidite,NSTimestamp finValidite,BigDecimal montant,String formuleCalcul,String messageErreur) {
		this(individuID,debutValidite,finValidite,montant,formuleCalcul,messageErreur);
		this.typeCalcul = typeCalcul;
	}
	public PrimeResultat(EOGlobalID individuID,NSTimestamp debutValidite,NSTimestamp finValidite,BigDecimal montant,String formuleCalcul,String messageErreur) {
		this.debutValidite = debutValidite;
		this.finValidite = finValidite;
		this.montant = montant;
		this.formuleCalcul = formuleCalcul;
		this.messageErreur = messageErreur;
		this.individuID = individuID;
	}
	/** constructeur pour NSCoding */
	public PrimeResultat(NSDictionary dict) {
		individuID = (EOGlobalID)dict.objectForKey("individuID");
		debutValidite = (NSTimestamp)dict.objectForKey("debutValidite");
		finValidite = (NSTimestamp)dict.objectForKey("finValidite");
		montant = (BigDecimal)dict.objectForKey("montant");
		Integer type = (Integer)dict.objectForKey("typeCalcul");
		typeCalcul = type.intValue();
		formuleCalcul = (String)dict.objectForKey("formuleCalcul");
		messageErreur = (String)dict.objectForKey("messageErreur");
	}
	// Accesseurs
	public NSTimestamp debutValidite() {
		return debutValidite;
	}
	public void setDebutValidite(NSTimestamp debutValidite) {
		this.debutValidite = debutValidite;
	}
	public NSTimestamp finValidite() {
		return finValidite;
	}
	public void setFinValidite(NSTimestamp finValidite) {
		this.finValidite = finValidite;
	}
	public int typeCalcul() {
		return typeCalcul;
	}
	public void setTypeCalcul(int typeCalcul) {
		this.typeCalcul = typeCalcul;
	}
	public BigDecimal montant() {
		return montant;
	}
	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}
	public String formuleCalcul() {
		return formuleCalcul;
	}
	public void setFormuleCalcul(String formuleCalcul) {
		this.formuleCalcul = formuleCalcul;
	}
	public String messageErreur() {
		return messageErreur;
	}
	public void setMessageErreur(String messageErreur) {
		this.messageErreur = messageErreur;
	}
	public EOGlobalID individuID() {
		return individuID;
	}
	public String toString() {
		String s = "Individu : " + individuID;
		 s += ", type calcul : ";
		switch(typeCalcul()) {
		case TYPE_SANS_CALCUL : s += "sans";
		break;
		case TYPE_MONTANT_PERSONNEL : s += "montant personnel";
		break;
		case TYPE_CALCUL : s += "calcul";
		break;
		}
		s += ", debut validite : " + DateCtrl.dateToString(debutValidite);
		if (finValidite() != null) {
			s += ", fin validite : " + DateCtrl.dateToString(finValidite);
		}
		s += ", montant : " + montant();
		if (formuleCalcul() != null) {
			s += ", formule : " + formuleCalcul();
		}
		if (messageErreur() != null && messageErreur().length() > 0) {
			s += ", erreur : " + messageErreur();
		}
		return s;
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Interface NSCoding
	public Class classForCoder() {
		return PrimeResultat.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		NSMutableDictionary dict = new NSMutableDictionary();
		if (individuID() != null)
			dict.setObjectForKey(individuID, "individuID");	
		if (debutValidite() != null)
				dict.setObjectForKey(debutValidite(), "debutValidite");
		if (finValidite() != null)
			dict.setObjectForKey(finValidite(), "finValidite");
		if (montant() != null)
			dict.setObjectForKey(montant(), "montant");
		dict.setObjectForKey(new  Integer(typeCalcul), "typeCalcul");
		if (formuleCalcul() != null) {
			dict.setObjectForKey(formuleCalcul(), "formuleCalcul");
		}
		if (messageErreur() != null) {
			dict.setObjectForKey(messageErreur(), "messageErreur");
		}
		coder.encodeObject(dict);
	}
	public static Class decodeClass() {
		return PrimeResultat.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new PrimeResultat((NSDictionary)coder.decodeObject());
	}
}
// EOPlanComptable.java
// Created on Wed Dec 22 11:46:53 Europe/Paris 2010 by Apple EOModeler Version 5.2

package org.cocktail.mangue.modele.goyave;

import org.cocktail.common.utilities.RecordAvecLibelleEtCode;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class EOPlanComptable extends EOGenericRecord implements RecordAvecLibelleEtCode {

    public EOPlanComptable() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPlanComptable(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String pcoNum() {
        return (String)storedValueForKey("pcoNum");
    }

    public void setPcoNum(String value) {
        takeStoredValueForKey(value, "pcoNum");
    }

    public String pcoLibelle() {
        return (String)storedValueForKey("pcoLibelle");
    }

    public void setPcoLibelle(String value) {
        takeStoredValueForKey(value, "pcoLibelle");
    }

    public String pcoValidite() {
        return (String)storedValueForKey("pcoValidite");
    }

    public void setPcoValidite(String value) {
        takeStoredValueForKey(value, "pcoValidite");
    }
    // Interface RecordAvecLibelleEtCode
	public String code() {
		return pcoNum();
	}
	public String libelle() {
		return pcoLibelle();
	}
    // Méthodes ajoutées
    public static EOPlanComptable rechercherPlanAvecNum(EOEditingContext editingContext,String pcoNum) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("pcoNum = %@ AND pcoValidite = 'VALIDE'", new NSArray(pcoNum));
    	EOFetchSpecification fs = new EOFetchSpecification("PlanComptable", qualifier, null);
    	try {
    		return (EOPlanComptable)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
    	} catch (Exception exc) {
    		return null;
    	}
    }


}

// EOPrimePopulation.java
// Created on Fri Sep 26 09:55:44 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.goyave;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPrimePopulation extends ObjetValidePourPrime {

    public EOPrimePopulation() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPrimePopulation(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

 
    public org.cocktail.mangue.modele.grhum.EOCorps corps() {
        return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey("corps");
    }

    public void setCorps(org.cocktail.mangue.modele.grhum.EOCorps value) {
        takeStoredValueForKey(value, "corps");
    }

    public org.cocktail.mangue.modele.grhum.EOGrade grade() {
        return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey("grade");
    }

    public void setGrade(org.cocktail.mangue.modele.grhum.EOGrade value) {
        takeStoredValueForKey(value, "grade");
    }

    public org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail typeContratTravail() {
        return (org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail)storedValueForKey("typeContratTravail");
    }

    public void setTypeContratTravail(org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail value) {
        takeStoredValueForKey(value, "typeContratTravail");
    }

    public org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation typePopulation() {
        return (org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation)storedValueForKey("typePopulation");
    }

    public void setTypePopulation(org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation value) {
        takeStoredValueForKey(value, "typePopulation");
    }
    // Méthodes ajoutées
	public void validateForSave() throws NSValidation.ValidationException {
		if (typePopulation() == null && corps() == null && grade() == null && typeContratTravail() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le type de population ou le corps ou le grade ou le type de contrat de travail");
		}
	}
    // Méthodes statiques
    /** Retourne les objets valides de type PrimePopulation pour une prime
	 * @param editingContext
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @return NSArray de PrimePopulation
	 */
	public static NSArray rechercherPrimesPopulationValidesPourPrime(EOEditingContext editingContext,EOPrime prime) {
		return ObjetValidePourPrime.rechercherObjetsValidesPourPrime(editingContext, "PrimePopulation", prime);
	}
	/** Retourne les corps valides pour une prime
	 * @param editingContext
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @return NSArray de Corps
	 */
	public static NSArray rechercherCorpsValidesPourPrime(EOEditingContext editingContext,EOPrime prime) {
		return (NSArray)rechercherPrimesPopulationValidesPourPrime(editingContext, prime).valueForKey("corps");
	}
	/** Retourne les grades valides pour une prime
	 * @param editingContext
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @return NSArray de Grade
	 */
	public static NSArray rechercherGradesValidesPourPrime(EOEditingContext editingContext,EOPrime prime) {
		return (NSArray)rechercherPrimesPopulationValidesPourPrime(editingContext, prime).valueForKey("grade");
	}
	/** Retourne les populations valides pour une prime
	 * @param editingContext
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @return NSArray de TypePopulation
	 */
	public static NSArray rechercherPopulationsValidesPourPrime(EOEditingContext editingContext,EOPrime prime) {
		return (NSArray)rechercherPrimesPopulationValidesPourPrime(editingContext, prime).valueForKey("typePopulation");
	}
	/** Retourne les types de contrat valides pour une prime
	 * @param editingContext
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherTypesContratPourPrime(EOEditingContext editingContext,EOPrime prime) {
		return (NSArray)rechercherPrimesPopulationValidesPourPrime(editingContext, prime).valueForKey("typeContratTravail");
	}
}

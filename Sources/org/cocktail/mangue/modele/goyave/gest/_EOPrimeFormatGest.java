// _EOPrimeFormatGest.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrimeFormatGest.java instead.
package org.cocktail.mangue.modele.goyave.gest;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public abstract class _EOPrimeFormatGest extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "PrimeFormatGest";
	public static final String ENTITY_TABLE_NAME = "GOYAVE.PRIME_FORMAT_GEST";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pfogOrdre";

	public static final String MOIS_ORDRE_KEY = "moisOrdre";
	public static final String PFOG_CODE_INDEMNITE_KEY = "pfogCodeIndemnite";
	public static final String PFOG_CODE_PAIEMENT_KEY = "pfogCodePaiement";
	public static final String PFOG_CODE_SPECIAL_MAJ_KEY = "pfogCodeSpecialMaj";
	public static final String PFOG_CODE_TAUX_KEY = "pfogCodeTaux";
	public static final String PFOG_DATE_EFFET_KEY = "pfogDateEffet";
	public static final String PFOG_DONNEES_ZONE_A_KEY = "pfogDonneesZoneA";
	public static final String PFOG_DONNEES_ZONE_B_KEY = "pfogDonneesZoneB";
	public static final String PFOG_LIBELLE_KEY = "pfogLibelle";
	public static final String PFOG_MODE_CALCUL_KEY = "pfogModeCalcul";
	public static final String PFOG_MONTANT_KEY = "pfogMontant";
	public static final String PFOG_MOUVEMENT_TG_KEY = "pfogMouvementTg";
	public static final String PFOG_NOMBRE_UNITE_KEY = "pfogNombreUnite";
	public static final String PFOG_ORIGINE_KEY = "pfogOrigine";
	public static final String PFOG_SENS_KEY = "pfogSens";
	public static final String PFOG_TYPE_MOUVEMENT_KEY = "pfogTypeMouvement";

// Attributs non visibles
	public static final String PATT_ORDRE_KEY = "pattOrdre";
	public static final String PFOG_ORDRE_KEY = "pfogOrdre";

//Colonnes dans la base de donnees
	public static final String MOIS_ORDRE_COLKEY = "MOIS_ORDRE";
	public static final String PFOG_CODE_INDEMNITE_COLKEY = "PFOG_CODE_INDEMNITE";
	public static final String PFOG_CODE_PAIEMENT_COLKEY = "PFOG_CODE_PAIEMENT";
	public static final String PFOG_CODE_SPECIAL_MAJ_COLKEY = "PFOG_CODE_SPECIAL_MAJ";
	public static final String PFOG_CODE_TAUX_COLKEY = "PFOG_CODE_TAUX";
	public static final String PFOG_DATE_EFFET_COLKEY = "PFOG_DATE_EFFET";
	public static final String PFOG_DONNEES_ZONE_A_COLKEY = "PFOG_DONNEES_ZONE_A";
	public static final String PFOG_DONNEES_ZONE_B_COLKEY = "PFOG_DONNEES_ZONE_B";
	public static final String PFOG_LIBELLE_COLKEY = "PFOG_LIBELLE";
	public static final String PFOG_MODE_CALCUL_COLKEY = "PFOG_MODE_CALCUL";
	public static final String PFOG_MONTANT_COLKEY = "PFOG_MONTANT";
	public static final String PFOG_MOUVEMENT_TG_COLKEY = "PFOG_MOUVEMENT_TG";
	public static final String PFOG_NOMBRE_UNITE_COLKEY = "PFOG_NOMBRE_UNITE";
	public static final String PFOG_ORIGINE_COLKEY = "PFOG_ORIGINE";
	public static final String PFOG_SENS_COLKEY = "PFOG_SENS";
	public static final String PFOG_TYPE_MOUVEMENT_COLKEY = "PFOG_TYPE_MOUVEMENT";

	public static final String PATT_ORDRE_COLKEY = "PATT_ORDRE";
	public static final String PFOG_ORDRE_COLKEY = "PFOG_ORDRE";


	// Relationships
	public static final String PRIME_ATTRIBUTION_KEY = "primeAttribution";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer moisOrdre() {
    return (Integer) storedValueForKey(MOIS_ORDRE_KEY);
  }

  public void setMoisOrdre(Integer value) {
    takeStoredValueForKey(value, MOIS_ORDRE_KEY);
  }

  public String pfogCodeIndemnite() {
    return (String) storedValueForKey(PFOG_CODE_INDEMNITE_KEY);
  }

  public void setPfogCodeIndemnite(String value) {
    takeStoredValueForKey(value, PFOG_CODE_INDEMNITE_KEY);
  }

  public String pfogCodePaiement() {
    return (String) storedValueForKey(PFOG_CODE_PAIEMENT_KEY);
  }

  public void setPfogCodePaiement(String value) {
    takeStoredValueForKey(value, PFOG_CODE_PAIEMENT_KEY);
  }

  public String pfogCodeSpecialMaj() {
    return (String) storedValueForKey(PFOG_CODE_SPECIAL_MAJ_KEY);
  }

  public void setPfogCodeSpecialMaj(String value) {
    takeStoredValueForKey(value, PFOG_CODE_SPECIAL_MAJ_KEY);
  }

  public String pfogCodeTaux() {
    return (String) storedValueForKey(PFOG_CODE_TAUX_KEY);
  }

  public void setPfogCodeTaux(String value) {
    takeStoredValueForKey(value, PFOG_CODE_TAUX_KEY);
  }

  public String pfogDateEffet() {
    return (String) storedValueForKey(PFOG_DATE_EFFET_KEY);
  }

  public void setPfogDateEffet(String value) {
    takeStoredValueForKey(value, PFOG_DATE_EFFET_KEY);
  }

  public String pfogDonneesZoneA() {
    return (String) storedValueForKey(PFOG_DONNEES_ZONE_A_KEY);
  }

  public void setPfogDonneesZoneA(String value) {
    takeStoredValueForKey(value, PFOG_DONNEES_ZONE_A_KEY);
  }

  public String pfogDonneesZoneB() {
    return (String) storedValueForKey(PFOG_DONNEES_ZONE_B_KEY);
  }

  public void setPfogDonneesZoneB(String value) {
    takeStoredValueForKey(value, PFOG_DONNEES_ZONE_B_KEY);
  }

  public String pfogLibelle() {
    return (String) storedValueForKey(PFOG_LIBELLE_KEY);
  }

  public void setPfogLibelle(String value) {
    takeStoredValueForKey(value, PFOG_LIBELLE_KEY);
  }

  public String pfogModeCalcul() {
    return (String) storedValueForKey(PFOG_MODE_CALCUL_KEY);
  }

  public void setPfogModeCalcul(String value) {
    takeStoredValueForKey(value, PFOG_MODE_CALCUL_KEY);
  }

  public String pfogMontant() {
    return (String) storedValueForKey(PFOG_MONTANT_KEY);
  }

  public void setPfogMontant(String value) {
    takeStoredValueForKey(value, PFOG_MONTANT_KEY);
  }

  public String pfogMouvementTg() {
    return (String) storedValueForKey(PFOG_MOUVEMENT_TG_KEY);
  }

  public void setPfogMouvementTg(String value) {
    takeStoredValueForKey(value, PFOG_MOUVEMENT_TG_KEY);
  }

  public String pfogNombreUnite() {
    return (String) storedValueForKey(PFOG_NOMBRE_UNITE_KEY);
  }

  public void setPfogNombreUnite(String value) {
    takeStoredValueForKey(value, PFOG_NOMBRE_UNITE_KEY);
  }

  public String pfogOrigine() {
    return (String) storedValueForKey(PFOG_ORIGINE_KEY);
  }

  public void setPfogOrigine(String value) {
    takeStoredValueForKey(value, PFOG_ORIGINE_KEY);
  }

  public String pfogSens() {
    return (String) storedValueForKey(PFOG_SENS_KEY);
  }

  public void setPfogSens(String value) {
    takeStoredValueForKey(value, PFOG_SENS_KEY);
  }

  public String pfogTypeMouvement() {
    return (String) storedValueForKey(PFOG_TYPE_MOUVEMENT_KEY);
  }

  public void setPfogTypeMouvement(String value) {
    takeStoredValueForKey(value, PFOG_TYPE_MOUVEMENT_KEY);
  }

  public org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution primeAttribution() {
    return (org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution)storedValueForKey(PRIME_ATTRIBUTION_KEY);
  }

  public void setPrimeAttributionRelationship(org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution value) {
    if (value == null) {
    	org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution oldValue = primeAttribution();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRIME_ATTRIBUTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRIME_ATTRIBUTION_KEY);
    }
  }
  

/**
 * Créer une instance de EOPrimeFormatGest avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrimeFormatGest createEOPrimeFormatGest(EOEditingContext editingContext, org.cocktail.mangue.modele.goyave.primes.EOPrimeAttribution primeAttribution			) {
    EOPrimeFormatGest eo = (EOPrimeFormatGest) createAndInsertInstance(editingContext, _EOPrimeFormatGest.ENTITY_NAME);    
    eo.setPrimeAttributionRelationship(primeAttribution);
    return eo;
  }

  
	  public EOPrimeFormatGest localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrimeFormatGest)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrimeFormatGest creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrimeFormatGest creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPrimeFormatGest object = (EOPrimeFormatGest)createAndInsertInstance(editingContext, _EOPrimeFormatGest.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrimeFormatGest localInstanceIn(EOEditingContext editingContext, EOPrimeFormatGest eo) {
    EOPrimeFormatGest localInstance = (eo == null) ? null : (EOPrimeFormatGest)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrimeFormatGest#localInstanceIn a la place.
   */
	public static EOPrimeFormatGest localInstanceOf(EOEditingContext editingContext, EOPrimeFormatGest eo) {
		return EOPrimeFormatGest.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrimeFormatGest fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrimeFormatGest fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrimeFormatGest eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrimeFormatGest)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrimeFormatGest fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrimeFormatGest fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrimeFormatGest eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrimeFormatGest)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrimeFormatGest fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrimeFormatGest eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrimeFormatGest ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrimeFormatGest fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

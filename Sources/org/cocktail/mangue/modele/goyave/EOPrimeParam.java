//EOPrimeParam.java
//Created on Fri Sep 26 09:55:14 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.goyave;

import java.math.BigDecimal;

import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** R&egrave;les de validation des param&egrave;tres<BR>
 * Les champs obligatoires, les longueurs des cha&icirc;nes sont v&eacute;rifi&eacute;es<BR>
 * V&eacute;rifie que le d&eacute;but de validit&eacute; est ant&eacute;rieur &agrave; la fin de validit&eacute;<BR>
 * V&eacute;rifie que les donn&eacute;es saisies sont coh&eacute;rentes :<BR>
 * valeur d'&eacute;chelon saisie => le grade doit &circ;tre saisi<BR>
 * V&eacute;rifie qu'il n'y a pas de chevauchements de dates pour un m&egrave; code et des valeurs identiques des param&grave;tres (tous
 * les champs sont identiques sauf le montant)<BR>
 * @author christine
 *
 */
public class EOPrimeParam extends DureeAvecValidite {
	/** Le param&egrave;tre concerne un indice */
	public final static String TYPE_PARAM_INDICE = "I";
	/** Le param&egrave;tre concerne une fonction */
	public final static String TYPE_PARAM_FONCTION = "F";
	/** Le param&egrave;tre concerne un montant */
	public final static String TYPE_PARAM_MONTANT = "M";
	/** Le param&egrave;tre concerne un taux */
	public final static String TYPE_PARAM_TAUX = "T";
	/** Le param&egrave;tre concerne un corps */
	public final static String TYPE_PARAM_CORPS = "C";
	/** Le param&egrave;tre concerne un grade */
	public final static String TYPE_PARAM_GRADE = "G";
	/** Le param&egrave;tre concerne un &eacute;chelon */
	public final static String TYPE_PARAM_ECHELON = "E";
	/** Le param&egrave;tre concerne un entier */
	public final static String TYPE_PARAM_ENTIER = "N";
	public EOPrimeParam() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPrimeParam(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */

	public String pparLibelle() {
		return (String)storedValueForKey("pparLibelle");
	}

	public void setPparLibelle(String value) {
		takeStoredValueForKey(value, "pparLibelle");
	}

	public String pparType() {
		return (String)storedValueForKey("pparType");
	}

	public void setPparType(String value) {
		takeStoredValueForKey(value, "pparType");
	}

	public BigDecimal pparMontant() {
		return (BigDecimal)storedValueForKey("pparMontant");
	}

	public void setPparMontant(BigDecimal value) {
		takeStoredValueForKey(value, "pparMontant");
	}

	public BigDecimal pparTaux() {
		return (BigDecimal)storedValueForKey("pparTaux");
	}

	public void setPparTaux(BigDecimal value) {
		takeStoredValueForKey(value, "pparTaux");
	}
	public Number pparEntier() {
		return (Number)storedValueForKey("pparEntier");
	}

	public void setPparEntier(Number value) {
		takeStoredValueForKey(value, "pparEntier");
	}
	public String pparIndice() {
		return (String)storedValueForKey("pparIndice");
	}

	public void setPparIndice(String value) {
		takeStoredValueForKey(value, "pparIndice");
	}

	public String pparEchelonMini() {
		return (String)storedValueForKey("pparEchelonMini");
	}

	public void setPparEchelonMini(String value) {
		takeStoredValueForKey(value, "pparEchelonMini");
	}

	public String pparEchelonMaxi() {
		return (String)storedValueForKey("pparEchelonMaxi");
	}

	public void setPparEchelonMaxi(String value) {
		takeStoredValueForKey(value, "pparEchelonMaxi");
	}

	public org.cocktail.mangue.modele.grhum.EOCorps corps() {
		return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey("corps");
	}

	public void setCorps(org.cocktail.mangue.modele.grhum.EOCorps value) {
		takeStoredValueForKey(value, "corps");
	}

	public org.cocktail.mangue.modele.grhum.EOGrade grade() {
		return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey("grade");
	}

	public void setGrade(org.cocktail.mangue.modele.grhum.EOGrade value) {
		takeStoredValueForKey(value, "grade");
	}
	public org.cocktail.mangue.modele.goyave.EOPrimeCode code() {
		return (org.cocktail.mangue.modele.goyave.EOPrimeCode)storedValueForKey("code");
	}

	public void setCode(org.cocktail.mangue.modele.goyave.EOPrimeCode value) {
		takeStoredValueForKey(value, "code");
	}
	public EOAssociation fonction() {
		return (EOAssociation)storedValueForKey("fonction");
	}

	public void setFonction(EOAssociation value) {
		takeStoredValueForKey(value, "fonction");
	}
	// Méthodes ajoutées
	public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
		if (code() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un code");
		}
		if (pparEchelonMaxi() != null) {
			if (pparEchelonMaxi().length() > 2) {
				throw new NSValidation.ValidationException("L'échelon maximum ne peut dépasser 2 caractères");
			} else if (pparEchelonMini() == null) {
				throw new NSValidation.ValidationException("Si vous fournissez l'échelon maximum, fournissez l'échelon minimum");
			} else if (grade() == null) {
				throw new NSValidation.ValidationException("Si vous fournissez un échelon, vous devez aussi fournir le grade");
			}
		}
		if (pparEchelonMini() != null && pparEchelonMini().length() > 2) {
			throw new NSValidation.ValidationException("L'échelon minimum ne peut dépasser 2 caractères");
		}
		if (pparEchelonMaxi() != null && pparEchelonMaxi().length() > 2) {
			throw new NSValidation.ValidationException("L'échelon maximum ne peut dépasser 2 caractères");
		}
		if ((pparEchelonMini() != null || pparEchelonMaxi() != null) && grade() == null) {
			throw new NSValidation.ValidationException("Si vous fournissez un échelon, vous devez aussi fournir le grade");
		}
		if (pparIndice() != null && pparIndice().length() > 4) {
			throw new NSValidation.ValidationException("L'indice ne peut dépasser 6 caractères");
		}
		if (pparLibelle() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un libellé");
		} else if (pparLibelle().length() > 200) {
			throw new NSValidation.ValidationException("Le libellé court ne peut dépasser 200 caractères");
		}
		if (pparType() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir au moins une valeur (fonction, corps, grade, echelon, montant ou taux)");
		} else if (pparType().length() > 200) {
			throw new NSValidation.ValidationException("Le type peut dépasser 1 caractère");
		}
		// Vérifier si il y a chevauchement avec d'autres valeurs du même paramètre
		NSArray parametresValides = rechercherParametresValidesPourCodeEtPeriode(editingContext(), code(), debutValidite(), finValidite());
		java.util.Enumeration e = parametresValides.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeParam parametre = (EOPrimeParam)e.nextElement();
			if (parametre != this && parametre.aMemeValeur(this) && Utilitaires.IntervalleTemps.intersectionPeriodes(parametre.debutValidite(), parametre.finValidite(), debutValidite(), finValidite()) != null) {
				throw new NSValidation.ValidationException("Il y a un chevauchement des dates de validité avec une autre valeur de ce paramètre");
			}
		}
	}
	/** Affecte le type du param&egrave;tre<BR>
	 * TYPE_PARAM_FONCTION si comporte une fonction<BR>
	 * TYPE_PARAM_ECHELON si comporte un &eacute;chelon et pas de fonction<BR>
	 * TYPE_PARAM_GRADE si comporte un grade et pas d'&eacute;chelon<BR>
	 * TYPE_PARAM_CORPS si comporte un corps mais pas de grade<BR>
	 * TYPE_PARAM_INDICE si comporte un indice mais pas de corps, ni grade,...<BR>
	 * TYPE_PARAM_TAUX si comporte un taux et n'est pas d'un des types pr&eacute;c&eacute;dents<BR>
	 * TYPE_PARAM_MONTANT si comporte un montant mais pas de taux et n'est pas d'un des types pr&eacute;c&eacute;dents<BR>
	 */
	public void preparerTypeParametre() {
		if (fonction() != null) {
			setPparType(TYPE_PARAM_FONCTION);
		} else if (pparEchelonMini() != null || pparEchelonMaxi() != null) {
			setPparType(TYPE_PARAM_ECHELON);
		}  else if (grade() != null) {
			setPparType(TYPE_PARAM_GRADE);
		} else if (corps() != null) {
			setPparType(TYPE_PARAM_CORPS);
		} else if (pparIndice() != null) {
			setPparType(TYPE_PARAM_INDICE);
		}  else if (pparEntier() != null) {
			setPparType(TYPE_PARAM_ENTIER);
		} else if (pparTaux() != null) {
			setPparType(TYPE_PARAM_TAUX);
		} else if (pparMontant() != null) {
			setPparType(TYPE_PARAM_MONTANT);
		} else {
			setPparType(null);	// On n'a trouvé aucune valeur de saisie
		}
	}
	/** Pour la duplication de param&egrave;tre */
	public void updateAvecParametre(EOPrimeParam parametre) {
		takeValuesFromDictionary(parametre.snapshot());
		// Modifier le libellé
		// on duplique le code, si il se termine par un chiffre, on prend chiffre + 1, sinon on remplace le dernier caractère par 1
		String debut = pparLibelle().substring(0,pparLibelle().length() - 1);

		String lastChar = pparLibelle().substring(pparLibelle().length()-1,pparLibelle().length());
		if (StringCtrl.estNumerique(lastChar,true)) {
			lastChar = new Integer(StringCtrl.stringToInt(lastChar, 0) + 1).toString();
		} else {
			lastChar = "1";
		}
		if (debut.length() > 196) {
			debut = debut.substring(0,196);	
		}
		setPparLibelle(debut + " - " + lastChar);
		addObjectToBothSidesOfRelationshipWithKey(parametre.code(), "code");
		if (parametre.corps() != null) {
			addObjectToBothSidesOfRelationshipWithKey(parametre.corps(), "corps");
		}
		if (parametre.grade() != null) {
			addObjectToBothSidesOfRelationshipWithKey(parametre.grade(), "grade");
		}
	}
	/** Retourne true si deux param&egrave;tres ont le m&ecirc;me type. Si il ne s'agit pas d'un montant ou d'un taux,
	 * on v&eacute;rifie si il s'agit bien de valeurs identiques 
	 */
	public boolean aMemeType(EOPrimeParam param1) {
		return pparType().equals(param1.pparType());
	}
	/** Retourne true si deux param&egrave;tres ont la m&ecirc;me valeur
	 */
	public boolean aMemeValeur(EOPrimeParam param1) {
		if (!aMemeType(param1)) {
			return false;
		}
		if (this.pparType().equals(TYPE_PARAM_CORPS)) {
			return this.corps() == param1.corps();
		} else if (this.pparType().equals(TYPE_PARAM_GRADE)) {
			if (this.grade() == param1.grade()) {
				// Vérifier si les échelons sont identiques
				return ontMemeEchelon(param1);
			} else {
				return false;
			}
		} else if (this.pparType().equals(TYPE_PARAM_ECHELON)) {
			return this.grade() == param1.grade() && ontMemeEchelon(param1);
		} else if (this.pparType().equals(TYPE_PARAM_INDICE)) {
			return ((this.pparIndice() == null && param1.pparIndice() == null) ||
					(this.pparIndice() != null && param1.pparIndice() != null && this.pparIndice().equals(param1.pparIndice())));
		} 
		else if (this.pparType().equals(TYPE_PARAM_FONCTION)) {
			// return ((this.pparIndice() == null && param1.pparIndice() == null) ||
			//		 (this.pparIndice() != null && param1.pparIndice() != null && this.pparIndice().equals(param1.pparIndice())));
		} else if (this.pparType().equals(TYPE_PARAM_ENTIER)) {
			return (this.pparEntier().intValue() == param1.pparEntier().intValue());
		} else if (this.pparType().equals(TYPE_PARAM_MONTANT)) {
			return this.pparMontant().doubleValue() == param1.pparMontant().doubleValue();
		} else if (this.pparType().equals(TYPE_PARAM_TAUX)) {
			return this.pparTaux().doubleValue() == param1.pparTaux().doubleValue();
		}
		return false;	// Ne devrait pas se produire
	}
	public boolean ontMemeEchelon(EOPrimeParam param1) {
		if ((pparEchelonMini() == null && param1.pparEchelonMini() == null) ||
				(pparEchelonMini() != null && param1.pparEchelonMini() != null && pparEchelonMini().equals(param1.pparEchelonMini()))) {
			return ((pparEchelonMaxi() == null && param1.pparEchelonMaxi() == null) ||
					(pparEchelonMaxi() != null && param1.pparEchelonMaxi() != null && pparEchelonMini().equals(param1.pparEchelonMaxi())));
		} else {
			return false;	// Un des deux échelons mini est nul
		}
	}
	/** Retourne la valeur d'un param&egrave;tre ie. le montant, le taux ou l'indice. Les autres informations d'un param&egrave;tre
	 * sont utilis&eacute;es pour identifier le param&egrave;tre &agrave; utiliser pour un individu */
	public String valeurParametre() {
		if (pparType() != null) {
			if (pparType().equals(TYPE_PARAM_INDICE)) {
				return pparIndice();
			} else if (pparType().equals(TYPE_PARAM_TAUX)) {
				return pparTaux().toString();
			} else if (pparType().equals(TYPE_PARAM_MONTANT)) {
				return pparMontant().toString();
			}
		}
		return null;
	}
	// Méthodes statiques
	/** Retourne les param&egrave;tres avec le libell&eacute; pass&eacute; en param&egrave;tre pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param libelle (peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 * @param estValide true si on recherche les param&egrave;tres valides
	 */
	public static NSArray rechercherParametresPourLibelleEtPeriode(EOEditingContext editingContext,String libelle,NSTimestamp debutPeriode, NSTimestamp finPeriode, boolean estValide) {
		EOQualifier qualifier = qualifierPourValidite(estValide,debutPeriode,finPeriode);
		if (libelle != null && libelle.length() > 0) {
			NSMutableArray qualifiers = new NSMutableArray(qualifier);
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("pparLibelle caseinsensitivelike %@", new NSArray("*" + libelle + "*")));
			qualifier = new EOAndQualifier(qualifiers);
		}
		EOFetchSpecification fs = new EOFetchSpecification("PrimeParam",qualifier,new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** Retourne les param&egrave;tres associ&eacute; au code pass&eacute; en param&egrave;tre pendant la p&eacute;riode rang&eacute;s par date d&eacute;but croissante
	 * @param editingContext
	 * @param code (String correspond au pcodCode)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 * @param qualifier autre qualifier &agrave; appliquer (peut &ecirc;tre nul)
	 * @param estValide true si on recherche les param&egrave;tres valides
	 */
	public static NSArray rechercherParametresValidesPourCodePeriodeEtQualifier(EOEditingContext editingContext,String code,NSTimestamp debutPeriode, NSTimestamp finPeriode,EOQualifier qualifier) {
		NSMutableArray qualifiers = new NSMutableArray(qualifierPourValidite(true,debutPeriode,finPeriode));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("code.pcodCode = %@", new NSArray(code)));
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}
		EOFetchSpecification fs = new EOFetchSpecification("PrimeParam",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** Retourne les param&egrave;tres associ&eacute; au code pass&eacute; en param&egrave;tre pendant la p&eacute;riode rang&eacute;s par date d&eacute;but croissante
	 * @param editingContext
	 * @param code (PrimeCode)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 * @param estValide true si on recherche les param&egrave;tres valides
	 */
	public static NSArray rechercherParametresValidesPourCodeEtPeriode(EOEditingContext editingContext,EOPrimeCode code,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray(qualifierPourValidite(true,debutPeriode,finPeriode));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("code = %@", new NSArray(code)));
		EOFetchSpecification fs = new EOFetchSpecification("PrimeParam",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
	}
}

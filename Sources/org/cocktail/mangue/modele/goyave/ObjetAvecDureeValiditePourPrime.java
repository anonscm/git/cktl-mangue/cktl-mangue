/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.goyave;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Repr&eacute;sente une information avec une p&eacute;riode de validit&eacute; associ&eacute; &agrave; une prime<BR>
 * R&egrave;les de validation <BR>
 * On v&eacute;rifie que la prime est bien fournie */
public abstract class ObjetAvecDureeValiditePourPrime extends DureeAvecValidite {

	public ObjetAvecDureeValiditePourPrime() {
		super();
	}
	public EOPrime prime() {
		return (EOPrime)storedValueForKey("prime");
	}

	public void setPrime(EOPrime value) {
		takeStoredValueForKey(value, "prime");
	}
	public void initAvecPrime(EOPrime prime) {
		super.init();
		setDebutValidite(prime.debutValidite());
		setFinValidite(prime.finValidite());
		addObjectToBothSidesOfRelationshipWithKey(prime, "prime");
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(prime(), "prime");
	}
	/** la sous-classe doit absolument invoquer cette m&eacute;thode */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		super.validateForSave();
		if (prime() == null) {
			throw new NSValidation.ValidationException("Vous devez associer une prime");
		}
	}
	// Méthodes statiques
	/** retourne le qualifier pour une prime */
	public static EOQualifier qualifierPourPrime(EOPrime prime) {
		return EOQualifier.qualifierWithQualifierFormat("prime = %@", new NSArray(prime));
	}
	/** Retourne les objets de type DureeAvecValidite valides pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param nomEntite (sa classe doit h&eacute;riter de DureeAvecValidite)
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherObjetsValidesPourPrimeEtPeriode(EOEditingContext editingContext,String nomEntite,EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray(qualifierPourValidite(true,debutPeriode,finPeriode));
		if (prime != null) {
			qualifiers.addObject(qualifierPourPrime(prime));
		}
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** Retourne les objets de type DureeAvecValidite valides pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param nomEntite (sa classe doit h&eacute;riter de DureeAvecValidite)
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherObjetsInvalidesPourPrimeEtPeriode(EOEditingContext editingContext,String nomEntite,EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray(qualifierPourValidite(false,debutPeriode,finPeriode));
		if (prime != null) {
			qualifiers.addObject(qualifierPourPrime(prime));
		}
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		return editingContext.objectsWithFetchSpecification(fs);
	}
}

// EOPrimeStructure.java
// Created on Fri Sep 26 09:56:11 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.goyave;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOPrimeStructure extends ObjetAvecDureeValiditePourPrime {

    public EOPrimeStructure() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPrimeStructure(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
    public org.cocktail.mangue.modele.grhum.referentiel.EOStructure structure() {
        return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey("structure");
    }

    public void setStructure(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
        takeStoredValueForKey(value, "structure");
    }
    // Méthodes ajoutées
    public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
		if (structure() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une structure");
		} 
    }
    // Méthodes statiques
    /** Retourne les objets de type PrimeStructure valides pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param structure (structure recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherPrimesStructuresValidesPourPeriode(EOEditingContext editingContext,EOStructure structure,EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		if (structure == null) {
			return ObjetAvecDureeValiditePourPrime.rechercherObjetsValidesPourPrimeEtPeriode(editingContext, "PrimeStructure", prime, debutPeriode, finPeriode);
		}
		NSMutableArray qualifiers = new NSMutableArray(qualifierPourValidite(true,debutPeriode,finPeriode));
		if (prime != null) {
			qualifiers.addObject(qualifierPourPrime(prime));
		}
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("structure = %@", new NSArray(structure)));
		EOFetchSpecification fs = new EOFetchSpecification("PrimeStructure",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("debutValidite",EOSortOrdering.CompareDescending)));
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** Retourne les structures valides pendant la p&eacute;riode
	 * @param editingContext
	 * @param prime (prime recherch&eacute;e, peut &ecirc;tre nulle)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherStructuresValidesPourPrimeEtPeriode(EOEditingContext editingContext,EOPrime prime,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		return (NSArray)rechercherPrimesStructuresValidesPourPeriode(editingContext,null, prime, debutPeriode, finPeriode).valueForKey("structure");
	}
}

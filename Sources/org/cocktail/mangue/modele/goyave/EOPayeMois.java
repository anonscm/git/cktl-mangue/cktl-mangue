/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
// EOPayeMois.java
// Created on Tue Sep 30 09:57:54 Europe/Paris 2008 by Apple EOModeler Version 5.2

package org.cocktail.mangue.modele.goyave;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;

public class EOPayeMois extends EOGenericRecord {

    public EOPayeMois() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPayeMois(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public Number moisAnnee() {
        return (Number)storedValueForKey("moisAnnee");
    }

    public void setMoisAnnee(Number value) {
        takeStoredValueForKey(value, "moisAnnee");
    }

    public Number moisCode() {
        return (Number)storedValueForKey("moisCode");
    }

    public void setMoisCode(Number value) {
        takeStoredValueForKey(value, "moisCode");
    }

    public String moisComplet() {
        return (String)storedValueForKey("moisComplet");
    }

    public void setMoisComplet(String value) {
        takeStoredValueForKey(value, "moisComplet");
    }

    public NSTimestamp moisDebut() {
        return (NSTimestamp)storedValueForKey("moisDebut");
    }

    public void setMoisDebut(NSTimestamp value) {
        takeStoredValueForKey(value, "moisDebut");
    }

    public NSTimestamp moisFin() {
        return (NSTimestamp)storedValueForKey("moisFin");
    }

    public void setMoisFin(NSTimestamp value) {
        takeStoredValueForKey(value, "moisFin");
    }

    public String moisLibelle() {
        return (String)storedValueForKey("moisLibelle");
    }

    public void setMoisLibelle(String value) {
        takeStoredValueForKey(value, "moisLibelle");
    }

    public String temValide() {
        return (String)storedValueForKey("temValide");
    }

    public void setTemValide(String value) {
        takeStoredValueForKey(value, "temValide");
    }
}

//AffectationServicePublic.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */package org.cocktail.mangue.modele;

 import org.cocktail.mangue.common.modele.nomenclatures.EOTypeService;
 import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
 import org.cocktail.mangue.common.utilities.DateCtrl;
 import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
 import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
 import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
 import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
 import org.cocktail.mangue.modele.mangue.individu.EOContrat;
 import org.cocktail.mangue.modele.mangue.individu.EOOccupation;
 import org.cocktail.mangue.modele.mangue.individu.EOPasse;

 import com.webobjects.eocontrol.EOAndQualifier;
 import com.webobjects.eocontrol.EOOrQualifier;
 import com.webobjects.eocontrol.EOQualifier;
 import com.webobjects.eocontrol.EOSortOrdering;
 import com.webobjects.eocontrol.EOEditingContext.EditingContextEvent;
 import com.webobjects.foundation.NSArray;
 import com.webobjects.foundation.NSKeyValueCoding;
 import com.webobjects.foundation.NSMutableArray;
 import com.webobjects.foundation.NSTimestamp;

 /** Pour imprimer les affectations d'un individu dans et hors &eacute;tablissement dans les LA-TA */
 public class AffectationServicePublicIndividu implements NSKeyValueCoding {
	 private String localisation;
	 private String fonction;
	 private NSTimestamp dateDebut,dateFin;

	 public AffectationServicePublicIndividu(NSTimestamp dateDebut, NSTimestamp dateFin,String localisation,String fonction) {
		 this.localisation = localisation;
		 this.dateDebut = dateDebut;
		 this.dateFin = dateFin;
		 this.fonction = fonction;
	 }
	 public NSTimestamp dateDebut() {
		 return dateDebut;
	 }
	 public String dateDebutFormatee() {
		 if (dateDebut() == null) {
			 return "";
		 } else {
			 return DateCtrl.dateToString(dateDebut());
		 }
	 }
	 public void setDateDebut(NSTimestamp value) {
		 dateDebut = value;
	 }
	 public NSTimestamp dateFin() {
		 return dateFin;
	 }
	 public String dateFinFormatee() {
		 if (dateFin() == null) {
			 return "";
		 } else {
			 return DateCtrl.dateToString(dateFin());
		 }
	 }
	 public void setDateFin(NSTimestamp value) {
		 dateFin = value;
	 }
	 public String localisation() {
		 return localisation;
	 }
	 public void setLocalisation(String value) {
		 localisation = value;
	 }
	 public String fonction() {
		 return fonction;
	 }
	 public void setFonction(String value) {
		 fonction = value;
	 }
	 public String toString() {
		 return "fonction :" + fonction() + ", localisation : " + localisation() + ", debut : " + dateDebutFormatee() + ", fin : " + dateFinFormatee();
	 }
	 // interface keyValueCoding
	 public void takeValueForKey(Object valeur,String cle) {
		 NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	 }
	 public Object valueForKey(String cle) {
		 return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	 }


	 /**
	  * 
	  * @param individu
	  * @param dateLimite
	  * @return
	  */
	 public static NSArray<AffectationServicePublicIndividu> preparerAffectationsServicePublicPourIndividuADate(EOIndividu individu,NSTimestamp dateLimite) {

		 NSMutableArray<AffectationServicePublicIndividu> affectationsServices = new NSMutableArray<AffectationServicePublicIndividu>();
		 String serviceCourant = null;

		 NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		 NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

		 // Recuperation des différents contrats de l'agent
		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContrat.TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));
		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContrat.TEM_ANNULATION_KEY + " = %@", new NSArray("N")));
		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.TEM_VACATAIRE_KEY + " = %@", new NSArray("N")));
		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.TEM_SERVICE_PUBLIC_KEY + " = %@", new NSArray("O")));
		 orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContrat.DATE_DEBUT_KEY + " <= %@", new NSArray(dateLimite)));
		 orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOContrat.DATE_FIN_KEY + " = nil", null));

		 qualifiers.addObject(new EOOrQualifier(orQualifiers));

		 NSArray<EOContrat> contrats = EOContrat.fetchAll(individu.editingContext(), new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);

		 for (EOContrat contrat : contrats) {

			 NSArray<EOAffectation> affectations = EOAffectation.rechercherAffectationsADate(individu.editingContext(), individu, contrat.dateDebut());

			 if (affectations.size() > 0) {
				 serviceCourant = affectations.lastObject().toStructureUlr().llStructure();
			 }
			 else {
				 if (contrat.toRne() != null)
					 serviceCourant = contrat.toRne().libelleLong();
				 else
					 serviceCourant = "INCONNU";
			 }

			 ajouterAffectationServicePourIndividu(affectationsServices,individu, contrat.dateDebut(), contrat.dateFin(), serviceCourant, "Agent contractuel");

		 }

		 // Recuperation des différentes positions de l'agent
		 qualifiers = new NSMutableArray<EOQualifier>();
		 orQualifiers = new NSMutableArray<EOQualifier>();

		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOChangementPosition.TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));
		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOChangementPosition.CARRIERE_ACCUEIL_KEY + " != nil", null));
		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOChangementPosition.TEM_VALIDE_KEY + " = %@", new NSArray("O")));

		 orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOChangementPosition.DATE_FIN_KEY + " <= %@", new NSArray(dateLimite)));
		 orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOChangementPosition.DATE_FIN_KEY + " = nil", null));

		 qualifiers.addObject(new EOOrQualifier(orQualifiers));

		 NSArray<EOChangementPosition> changements = EOChangementPosition.fetchAll(individu.editingContext(), new EOAndQualifier(qualifiers), EOChangementPosition.SORT_ARRAY_DATE_DEBUT_DESC);

		 for (EOChangementPosition changement : changements) {

			 NSArray<EOAffectation> affectations = EOAffectation.rechercherAffectationsADate(individu.editingContext(), individu, changement.dateDebut());

			 if (affectations.size() > 0) {
				 serviceCourant = affectations.lastObject().toStructureUlr().llStructure();
			 }
			 else {
				 if (changement.toRne() != null)
					 serviceCourant = changement.toRne().libelleLong();
				 else
					 serviceCourant = "INCONNU";
			 }

			 ajouterAffectationServicePourIndividu(affectationsServices,individu, changement.dateDebut(), changement.dateFin(), serviceCourant, null);

		 }

		 // Recuperation des periodes de passe
		 qualifiers = new NSMutableArray<EOQualifier>();
		 orQualifiers = new NSMutableArray<EOQualifier>();

		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPasse.INDIVIDU_KEY + " = %@", new NSArray(individu)));
		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPasse.TEM_VALIDE_KEY + " = %@", new NSArray("O")));
		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPasse.SECTEUR_PUBLIC_KEY + " = %@", new NSArray("O")));
		 qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPasse.TO_TYPE_SERVICE_KEY+"."+EOTypeService.CODE_KEY + " != %@", new NSArray(EOTypeService.TYPE_SERVICE_NON_VALIDES)));

		 orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPasse.DATE_FIN_KEY + " <= %@", new NSArray(dateLimite)));
		 orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPasse.DATE_FIN_KEY + " = nil", null));

		 qualifiers.addObject(new EOOrQualifier(orQualifiers));

		 NSArray<EOPasse> passes = EOPasse.fetchAll(individu.editingContext(), new EOAndQualifier(qualifiers), EOPasse.SORT_ARRAY_DATE_DEBUT_DESC);

		 for (EOPasse passe : passes) {

			 if (passe.etablissementPasse() != null)
				 serviceCourant = passe.etablissementPasse();
			 else
				 serviceCourant = "INCONNU";

			 ajouterAffectationServicePourIndividu(affectationsServices,individu, passe.dateDebut(), passe.dateFin(), serviceCourant, "Passé Hors MEN / MESR");

		 }


		 // Tri des donnees

		 NSMutableArray<EOSortOrdering> sorts = new NSMutableArray(new EOSortOrdering("dateDebut", EOSortOrdering.CompareAscending));
		 sorts.addObject(new EOSortOrdering("dateFin", EOSortOrdering.CompareAscending));
		 EOSortOrdering.sortArrayUsingKeyOrderArray(affectationsServices,sorts);

		 return affectationsServices.immutableClone();

	 }


	 /**
	  * 
	  * @param libelle1
	  * @param libelle2
	  * @return
	  */
	 private static boolean memeEtablissement(String libelle1,String libelle2) {
		 return ((libelle1 == null && libelle2 == null) || (libelle1 != null && libelle2 != null && libelle1.equals(libelle2)));
	 }

	 /**
	  * 
	  * @param affectationsServices
	  * @param individu
	  * @param dateDebut
	  * @param dateFin
	  * @param libelleService
	  */
	 private static void ajouterAffectationServicePourIndividu(
			 NSMutableArray<AffectationServicePublicIndividu> affectationsServices,EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin,String libelleService, String fonction) {

		 affectationsServices.addObject(new AffectationServicePublicIndividu(dateDebut, dateFin, libelleService, fonction));

		 //		NSArray<Fonction> fonctions = Fonction.preparerFonctionsPourIndividu(individu, dateDebut,dateFin);
		 //		if (fonctions == null || fonctions.size() == 0) {
		 //			affectationsServices.addObject(new AffectationServicePublicIndividu(dateDebut, dateFin, libelleService, null));
		 //		}
		 //		else {
		 //			java.util.Enumeration e1 = fonctions.objectEnumerator();
		 //			while (e1.hasMoreElements()) {
		 //				Fonction fonction = (Fonction)e1.nextElement();
		 //				affectationsServices.addObject(new AffectationServicePublicIndividu(dateDebut, dateFin, libelleService,fonction.fonction()));
		 //			}
		 //		}
	 }

	 /**
	  * 
	  * @author cpinsard
	  *
	  */
	 private static class Fonction {
		 private String fonction;
		 private NSTimestamp dateDebut,dateFin;
		 private Fonction(NSTimestamp dateDebut, NSTimestamp dateFin,String fonction) {
			 this.fonction = fonction;
			 this.dateDebut = dateDebut;
			 this.dateFin = dateFin;
		 }
		 public NSTimestamp dateDebut() {
			 return dateDebut;
		 }
		 public NSTimestamp dateFin() {
			 return dateFin;
		 }
		 public String fonction() {
			 return fonction;
		 }


		 /**
		  * 
		  * @param libelle1
		  * @param libelle2
		  * @return
		  */
		 private static boolean memeFonction(String libelle1,String libelle2) {
			 return ((libelle1 == null && libelle2 == null) || (libelle1 != null && libelle2 != null && libelle1.equals(libelle2)));
		 }
	 }
 }

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

/** comporte les informations requises pour les &eacute;ditions g&eacute;n&eacute;r&eacute;s avec l'&eacute;diteur de requ&ecirc:tes ou autres
 * Supporte l'interface NSKeyValueCoding pour l'acc&eagrave;s aux attributs et NSCoding pour le transfert des donn&eacute;es entre le client et le serveur */
public class InfoPourEditionRequete implements NSKeyValueCoding,NSCoding {
	private String nomFichierJasper;
	private String titreEdition;
	private String sousTitreEdition;
	private NSTimestamp debutPeriode;
	private NSTimestamp finPeriode;
	
	// constructeur
	public InfoPourEditionRequete(String nomFichierJasper,String titreEdition,String sousTitreEdition,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		this.nomFichierJasper = nomFichierJasper;
		this.titreEdition = titreEdition;
		this.sousTitreEdition = sousTitreEdition;
		this.debutPeriode = debutPeriode;
		this.finPeriode = finPeriode;
	}
	public InfoPourEditionRequete(String nomFichierJasper,String titreEdition,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		this(nomFichierJasper,titreEdition,null, debutPeriode,finPeriode);
	}
	// accesseurs
	public String nomFichierJasper() {
		return nomFichierJasper;
	}
	public String titreEdition() {
		return titreEdition;
	}
	public String sousTitreEdition() {
		return sousTitreEdition;
	}
	public NSTimestamp debutPeriode() {
		return debutPeriode;
	}
	public NSTimestamp finPeriode() {
		return finPeriode;
	}
	public String toString() {
		String debut = "", fin = "";
		if (debutPeriode() != null) {
			debut = DateCtrl.dateToString(debutPeriode());
		}
		if (finPeriode() != null) {
			fin = DateCtrl.dateToString(finPeriode());
		}
		String result = nomFichierJasper() + ", " + titreEdition() + ", ";
		if (sousTitreEdition() != null) {
			result += sousTitreEdition() + ", ";
		}
		result += debut + ", " + fin;
		return result;
	}
	  // interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	public Class classForCoder() {
		return InfoPourEditionRequete.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		coder.encodeObject(nomFichierJasper());
		coder.encodeObject(titreEdition());
		if (debutPeriode() != null) {
			coder.encodeObject(debutPeriode());
		}  else {
			// on a besoin d'une date bidon pour ne pas avoir de problème
			coder.encodeObject(DateCtrl.stringToDate("01/01/1900"));
		}
		if (finPeriode() != null) {
			coder.encodeObject(finPeriode());
		} else {
			// on a besoin d'une date bidon pour ne pas avoir de problème
			coder.encodeObject(DateCtrl.stringToDate("01/01/1900"));
		}
	}
	public static Class decodeClass() {
		return InfoPourEditionRequete.class;
	}
	public static Object decodeObject(NSCoder coder) {
		String nomFichierJasper = (String)coder.decodeObject();
	    String titreEdition = (String)coder.decodeObject();
	    NSTimestamp debutPeriode = (NSTimestamp)coder.decodeObject();
	    if (DateCtrl.dateToString(debutPeriode).equals("01/01/1900")) {
	    		debutPeriode = null;
	    }
	    NSTimestamp finPeriode = (NSTimestamp)coder.decodeObject();
	    if (DateCtrl.dateToString(finPeriode).equals("01/01/1900")) {
	    		finPeriode = null;
	    }
	    return new InfoPourEditionRequete(nomFichierJasper,titreEdition,debutPeriode,finPeriode);
	}
}

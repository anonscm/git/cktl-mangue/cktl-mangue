//EOFluxMoyens.java
//Created on Fri Feb 14 15:54:09  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue;


import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitre;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitreArticle;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgramme;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgrammeTitre;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * R&egrave;gles de validation des flux :<BR>
 * La date, la valeur, la cat&eacute;gorie d'emploi et le chapitre doivent &ecirc;tre fournis<BR>
 */
public class EOFluxMoyens extends EOGenericRecord {
	public static String DURABILITE_PERMANENTE = "P";
	public static String DURABILITE_TEMPORAIRE = "T";
	public EOFluxMoyens() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOFluxMoyens(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */


	public NSTimestamp dDebFluxMoyens() {
		return (NSTimestamp)storedValueForKey("dDebFluxMoyens");
	}

	public void setDDebFluxMoyens(NSTimestamp value) {
		takeStoredValueForKey(value, "dDebFluxMoyens");
	}

	public NSTimestamp dFinFluxMoyens() {
		return (NSTimestamp)storedValueForKey("dFinFluxMoyens");
	}

	public void setDFinFluxMoyens(NSTimestamp value) {
		takeStoredValueForKey(value, "dFinFluxMoyens");
	}
	
	public Number valFluxMoyens() {
		return (Number)storedValueForKey("valFluxMoyens");
	}

	public void setValFluxMoyens(Number value) {
		takeStoredValueForKey(value, "valFluxMoyens");
	}

	public String durabiliteMoyens() {
		return (String)storedValueForKey("durabiliteMoyens");
	}

	public void setDurabiliteMoyens(String value) {
		takeStoredValueForKey(value, "durabiliteMoyens");
	}
	public Number stockMenAvant() {
		return (Number)storedValueForKey("stockMenAvant");
	}

	public void setStockMenAvant(Number value) {
		takeStoredValueForKey(value, "stockMenAvant");
	}

	public Number stockMenApres() {
		return (Number)storedValueForKey("stockMenApres");
	}

	public void setStockMenApres(Number value) {
		takeStoredValueForKey(value, "stockMenApres");
	}

	public EOChapitre toChapitre() {
		return (EOChapitre)storedValueForKey("toChapitre");
	}

	public void setToChapitre(EOChapitre value) {
		takeStoredValueForKey(value, "toChapitre");
	}

	public EOCategorieEmploi toCategorieEmploi() {
		return (EOCategorieEmploi)storedValueForKey("toCategorieEmploi");
	}

	public void setToCategorieEmploi(EOCategorieEmploi value) {
		takeStoredValueForKey(value, "toCategorieEmploi");
	}

	public EOChapitreArticle toChapitreArticle() {
		return (EOChapitreArticle)storedValueForKey("toChapitreArticle");
	}

	public void setToChapitreArticle(EOChapitreArticle value) {
		takeStoredValueForKey(value, "toChapitreArticle");
	}

	public EODecisionDelegation toDecisionDelegation() {
		return (EODecisionDelegation)storedValueForKey("toDecisionDelegation");
	}

	public void setToDecisionDelegation(EODecisionDelegation value) {
		takeStoredValueForKey(value, "toDecisionDelegation");
	}

	public EOProgramme toProgramme() {
		return (EOProgramme)storedValueForKey("toProgramme");
	}

	public void setToProgramme(EOProgramme value) {
		takeStoredValueForKey(value, "toProgramme");
	}

	public EOProgrammeTitre toTitre() {
		return (EOProgrammeTitre)storedValueForKey("toTitre");
	}

	public void setToTitre(EOProgrammeTitre value) {
		takeStoredValueForKey(value, "toTitre");
	}
	// méthodes ajoutées
	public String dateFluxFormatee() {
		return SuperFinder.dateFormatee(this,"dDebFluxMoyens");
	}
	public void setDateFluxFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dDebFluxMoyens",uneDate);
	}
	public void initAvecDecisionDelegation(EODecisionDelegation decision) {
		setStockMenAvant(new Float(0));
		setStockMenApres(new Float(0));
		setDDebFluxMoyens(decision.dDecDelegation());
		setDurabiliteMoyens(DURABILITE_PERMANENTE);
		addObjectToBothSidesOfRelationshipWithKey(decision,"toDecisionDelegation");
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(toDecisionDelegation(),"toDecisionDelegation");
		removeObjectFromBothSidesOfRelationshipWithKey(toCategorieEmploi(),"toCategorieEmploi");
		if (toChapitre() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toChapitre(),"toChapitre");
		}
		if (toChapitreArticle() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toChapitreArticle(),"toChapitreArticle");
		}
		if (toProgramme() == null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toProgramme(),"toProgramme");
		}
		if (toTitre() == null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toTitre(),"toTitre");
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (dDebFluxMoyens() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date");
		} else {
			if (DateCtrl.isBefore(dDebFluxMoyens(),DateCtrl.stringToDate("01/01/2006")) && toChapitre() == null)  {
				throw new NSValidation.ValidationException("Vous devez sélectionner un chapitre.");
			}
			if (DateCtrl.isAfterEq(dDebFluxMoyens(),DateCtrl.stringToDate("01/01/2006")) && toProgramme() == null) {
				throw new NSValidation.ValidationException("Vous devez sélectionner un programme.");
			}                 
		}
		if (valFluxMoyens() == null || valFluxMoyens().floatValue() == 0) {
			throw new NSValidation.ValidationException("Vous devez fournir une valeur");
		}
		if (toCategorieEmploi() == null) {
			throw new NSValidation.ValidationException("Vous devez sélectionner une catégorie d'emploi");
		}
	}
	// Méthodes statiques
	/** Retourne le nombre d'emplois possibles pour la cat&eacute;gorie d'emploi pendant la p&eacute;riode et null si pas de flux */
	public static Number nbEmploisPossiblesPourCategorieEmploiEtPeriode(EOEditingContext editingContext,EOCategorieEmploi categorie,NSTimestamp dateDebut,NSTimestamp dateFin) {
		NSArray flux = rechercherFluxPourCategorieEtPeriode(editingContext, categorie, dateDebut, dateFin);
		if (flux == null || flux.count() == 0) {
			return null;
		}
		// Trier les flux par ordre de date décroissante pour récupérer la valeur la plus récente
		flux = EOSortOrdering.sortedArrayUsingKeyOrderArray(flux, new NSArray(EOSortOrdering.sortOrderingWithKey("dDebFluxMoyens", EOSortOrdering.CompareDescending)));
		EOFluxMoyens fluxCourant = (EOFluxMoyens)flux.objectAtIndex(0);
		return fluxCourant.stockMenApres();
	}
	private static NSArray rechercherFluxPourCategorieEtPeriode(EOEditingContext editingContext,EOCategorieEmploi categorie,NSTimestamp dateDebut,NSTimestamp dateFin) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("toCategorieEmploi = %@", new NSArray(categorie)));
		qualifiers.addObject(SuperFinder.qualifierPourPeriode("dDebFluxMoyens", dateDebut, "dFinFluxMoyens", dateFin));
		EOFetchSpecification fs = new EOFetchSpecification("FluxMoyens",new EOAndQualifier(qualifiers), null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
}

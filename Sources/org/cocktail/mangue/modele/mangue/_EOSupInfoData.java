/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSupInfoData.java instead.
package org.cocktail.mangue.modele.mangue;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOSupInfoData extends  EOGenericRecord {
	public static final String ENTITY_NAME = "SupInfoData";
	public static final String ENTITY_TABLE_NAME = "MANGUE.ELECTRA_FICHIER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "eficOrdre";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EFIC_ANC_CONSERVEE_KEY = "eficAncConservee";
	public static final String EFIC_BOE_KEY = "eficBoe";
	public static final String EFIC_BOE_TYPE_KEY = "eficBoeType";
	public static final String EFIC_C_CHEVRON_KEY = "eficCChevron";
	public static final String EFIC_C_CORPS_KEY = "eficCCorps";
	public static final String EFIC_C_ECHELON_KEY = "eficCEchelon";
	public static final String EFIC_C_GRADE_KEY = "eficCGrade";
	public static final String EFIC_C_GRADE_PREV_KEY = "eficCGradePrev";
	public static final String EFIC_CIVILITE_KEY = "eficCivilite";
	public static final String EFIC_C_MOTIF_DEPART_KEY = "eficCMotifDepart";
	public static final String EFIC_C_MOTIF_TEMPS_PARTIEL_KEY = "eficCMotifTempsPartiel";
	public static final String EFIC_C_PAYS_NATIONALITE_KEY = "eficCPaysNationalite";
	public static final String EFIC_C_POSITION_KEY = "eficCPosition";
	public static final String EFIC_C_SECTION_CNU_KEY = "eficCSectionCnu";
	public static final String EFIC_C_SECTION_CNU_EC_KEY = "eficCSectionCnuEc";
	public static final String EFIC_C_TYPE_ABSENCE_KEY = "eficCTypeAbsence";
	public static final String EFIC_C_TYPE_ACCES_CORPS_KEY = "eficCTypeAccesCorps";
	public static final String EFIC_C_TYPE_ACCES_GRADE_KEY = "eficCTypeAccesGrade";
	public static final String EFIC_D_ARRETE_KEY = "eficDArrete";
	public static final String EFIC_D_DEB_ABSENCE_KEY = "eficDDebAbsence";
	public static final String EFIC_D_DEB_POSTION_KEY = "eficDDebPostion";
	public static final String EFIC_D_ECHELON_KEY = "eficDEchelon";
	public static final String EFIC_D_EFFET_ARRIVEE_KEY = "eficDEffetArrivee";
	public static final String EFIC_D_EFFET_DEPART_KEY = "eficDEffetDepart";
	public static final String EFIC_D_ENTREE_CATEGORIE_KEY = "eficDEntreeCategorie";
	public static final String EFIC_D_FIN_ABSENCE_KEY = "eficDFinAbsence";
	public static final String EFIC_D_FIN_POSTION_KEY = "eficDFinPostion";
	public static final String EFIC_D_GRADE_KEY = "eficDGrade";
	public static final String EFIC_DIPL_ANNEE_KEY = "eficDiplAnnee";
	public static final String EFIC_DIPL_CODE_KEY = "eficDiplCode";
	public static final String EFIC_DIPL_CODE_UAI_KEY = "eficDiplCodeUAI";
	public static final String EFIC_DIPL_LIBELLE_UAI_KEY = "eficDiplLibelleUAI";
	public static final String EFIC_D_NAISSANCE_KEY = "eficDNaissance";
	public static final String EFIC_D_NOMINATION_CORPS_KEY = "eficDNominationCorps";
	public static final String EFIC_D_PROMOTION_KEY = "eficDPromotion";
	public static final String EFIC_D_TITULARISATION_KEY = "eficDTitularisation";
	public static final String EFIC_ETAT_KEY = "eficEtat";
	public static final String EFIC_FONCTION_KEY = "eficFonction";
	public static final String EFIC_FORFAIT_KEY = "eficForfait";
	public static final String EFIC_INDICE_BRUT_KEY = "eficIndiceBrut";
	public static final String EFIC_LIEU_POSITION_KEY = "eficLieuPosition";
	public static final String EFIC_NO_ARRETE_KEY = "eficNoArrete";
	public static final String EFIC_NOM_PATRONYMIQUE_KEY = "eficNomPatronymique";
	public static final String EFIC_NOM_USUEL_KEY = "eficNomUsuel";
	public static final String EFIC_NUMEN_KEY = "eficNumen";
	public static final String EFIC_OBJET_KEY = "eficObjet";
	public static final String EFIC_OBSERVATIONS_KEY = "eficObservations";
	public static final String EFIC_PRENOM_KEY = "eficPrenom";
	public static final String EFIC_QUOTITE_KEY = "eficQuotite";
	public static final String EFIC_REMUN_HORAIRE_KEY = "eficRemunHoraire";
	public static final String EFIC_STATUT_KEY = "eficStatut";
	public static final String EFIC_TEM_CRCT_KEY = "eficTemCrct";
	public static final String EFIC_TEM_VALIDE_KEY = "eficTemValide";
	public static final String EFIC_TYPE_FINANCEMENT_KEY = "eficTypeFinancement";
	public static final String EFIC_UAI_KEY = "eficUai";
	public static final String EFIC_UAI_AFFECTATION_KEY = "eficUaiAffectation";

// Attributs non visibles
	public static final String EFIC_ORDRE_KEY = "eficOrdre";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String PARP_ORDRE_KEY = "parpOrdre";
	public static final String SUPF_ID_KEY = "supfId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String EFIC_ANC_CONSERVEE_COLKEY = "EFIC_ANC_CONSERVEE";
	public static final String EFIC_BOE_COLKEY = "EFIC_BOE";
	public static final String EFIC_BOE_TYPE_COLKEY = "EFIC_BOE_TYPE";
	public static final String EFIC_C_CHEVRON_COLKEY = "EFIC_C_CHEVRON";
	public static final String EFIC_C_CORPS_COLKEY = "EFIC_C_CORPS";
	public static final String EFIC_C_ECHELON_COLKEY = "EFIC_C_ECHELON";
	public static final String EFIC_C_GRADE_COLKEY = "EFIC_C_GRADE";
	public static final String EFIC_C_GRADE_PREV_COLKEY = "EFIC_C_GRADE_PREV";
	public static final String EFIC_CIVILITE_COLKEY = "EFIC_CIVILITE";
	public static final String EFIC_C_MOTIF_DEPART_COLKEY = "EFIC_C_MOTIF_DEPART";
	public static final String EFIC_C_MOTIF_TEMPS_PARTIEL_COLKEY = "EFIC_C_MOTIF_TEMPS_PARTIEL";
	public static final String EFIC_C_PAYS_NATIONALITE_COLKEY = "EFIC_C_PAYS_NATIONALITE";
	public static final String EFIC_C_POSITION_COLKEY = "EFIC_C_POSITION";
	public static final String EFIC_C_SECTION_CNU_COLKEY = "EFIC_C_SECTION_CNU";
	public static final String EFIC_C_SECTION_CNU_EC_COLKEY = "EFIC_C_SECTION_CNU_EC";
	public static final String EFIC_C_TYPE_ABSENCE_COLKEY = "EFIC_C_TYPE_ABSENCE";
	public static final String EFIC_C_TYPE_ACCES_CORPS_COLKEY = "EFIC_C_TYPE_ACCES_CORPS";
	public static final String EFIC_C_TYPE_ACCES_GRADE_COLKEY = "EFIC_C_TYPE_ACCES_GRADE";
	public static final String EFIC_D_ARRETE_COLKEY = "EFIC_D_ARRETE";
	public static final String EFIC_D_DEB_ABSENCE_COLKEY = "EFIC_D_DEB_ABSENCE";
	public static final String EFIC_D_DEB_POSTION_COLKEY = "EFIC_D_DEB_POSTION";
	public static final String EFIC_D_ECHELON_COLKEY = "EFIC_D_ECHELON";
	public static final String EFIC_D_EFFET_ARRIVEE_COLKEY = "EFIC_D_EFFET_ARRIVEE";
	public static final String EFIC_D_EFFET_DEPART_COLKEY = "EFIC_D_EFFET_DEPART";
	public static final String EFIC_D_ENTREE_CATEGORIE_COLKEY = "EFIC_D_ENTREE_CATEGORIE";
	public static final String EFIC_D_FIN_ABSENCE_COLKEY = "EFIC_D_FIN_ABSENCE";
	public static final String EFIC_D_FIN_POSTION_COLKEY = "EFIC_D_FIN_POSTION";
	public static final String EFIC_D_GRADE_COLKEY = "EFIC_D_GRADE";
	public static final String EFIC_DIPL_ANNEE_COLKEY = "EFIC_DIPL_ANNEE";
	public static final String EFIC_DIPL_CODE_COLKEY = "EFIC_DIPL_CODE";
	public static final String EFIC_DIPL_CODE_UAI_COLKEY = "EFIC_DIPL_CODE_UAI";
	public static final String EFIC_DIPL_LIBELLE_UAI_COLKEY = "EFIC_DIPL_LIBELLE_UAI";
	public static final String EFIC_D_NAISSANCE_COLKEY = "EFIC_D_NAISSANCE";
	public static final String EFIC_D_NOMINATION_CORPS_COLKEY = "EFIC_D_NOMINATION_CORPS";
	public static final String EFIC_D_PROMOTION_COLKEY = "EFIC_D_PROMOTION";
	public static final String EFIC_D_TITULARISATION_COLKEY = "EFIC_D_TITULARISATION";
	public static final String EFIC_ETAT_COLKEY = "EFIC_ETAT";
	public static final String EFIC_FONCTION_COLKEY = "EFIC_FONCTION";
	public static final String EFIC_FORFAIT_COLKEY = "EFIC_FORFAIT";
	public static final String EFIC_INDICE_BRUT_COLKEY = "EFIC_INDICE_BRUT";
	public static final String EFIC_LIEU_POSITION_COLKEY = "EFIC_LIEU_POSITION";
	public static final String EFIC_NO_ARRETE_COLKEY = "EFIC_NO_ARRETE";
	public static final String EFIC_NOM_PATRONYMIQUE_COLKEY = "EFIC_NOM_PATRONYMIQUE";
	public static final String EFIC_NOM_USUEL_COLKEY = "EFIC_NOM_USUEL";
	public static final String EFIC_NUMEN_COLKEY = "EFIC_NUMEN";
	public static final String EFIC_OBJET_COLKEY = "EFIC_OBJET";
	public static final String EFIC_OBSERVATIONS_COLKEY = "EFIC_OBSERVATIONS";
	public static final String EFIC_PRENOM_COLKEY = "EFIC_PRENOM";
	public static final String EFIC_QUOTITE_COLKEY = "EFIC_QUOTITE";
	public static final String EFIC_REMUN_HORAIRE_COLKEY = "EFIC_REMUN_HORAIRE";
	public static final String EFIC_STATUT_COLKEY = "EFIC_STATUT";
	public static final String EFIC_TEM_CRCT_COLKEY = "EFIC_TEM_CRCT";
	public static final String EFIC_TEM_VALIDE_COLKEY = "EFIC_TEM_VALIDE";
	public static final String EFIC_TYPE_FINANCEMENT_COLKEY = "EFIC_TYPE_FINANCEMENT";
	public static final String EFIC_UAI_COLKEY = "EFIC_UAI";
	public static final String EFIC_UAI_AFFECTATION_COLKEY = "EFIC_UAI_AFFECTATION";

	public static final String EFIC_ORDRE_COLKEY = "EFIC_ORDRE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String PARP_ORDRE_COLKEY = "PARP_ORDRE";
	public static final String SUPF_ID_COLKEY = "SUPF_ID";


	// Relationships
	public static final String TO_FICHIER_KEY = "toFichier";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_ORIGINE_FINANCEMENT_KEY = "toOrigineFinancement";
	public static final String TO_PARAM_PROMOTION_KEY = "toParamPromotion";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String eficAncConservee() {
    return (String) storedValueForKey(EFIC_ANC_CONSERVEE_KEY);
  }

  public void setEficAncConservee(String value) {
    takeStoredValueForKey(value, EFIC_ANC_CONSERVEE_KEY);
  }

  public String eficBoe() {
    return (String) storedValueForKey(EFIC_BOE_KEY);
  }

  public void setEficBoe(String value) {
    takeStoredValueForKey(value, EFIC_BOE_KEY);
  }

  public String eficBoeType() {
    return (String) storedValueForKey(EFIC_BOE_TYPE_KEY);
  }

  public void setEficBoeType(String value) {
    takeStoredValueForKey(value, EFIC_BOE_TYPE_KEY);
  }

  public String eficCChevron() {
    return (String) storedValueForKey(EFIC_C_CHEVRON_KEY);
  }

  public void setEficCChevron(String value) {
    takeStoredValueForKey(value, EFIC_C_CHEVRON_KEY);
  }

  public String eficCCorps() {
    return (String) storedValueForKey(EFIC_C_CORPS_KEY);
  }

  public void setEficCCorps(String value) {
    takeStoredValueForKey(value, EFIC_C_CORPS_KEY);
  }

  public String eficCEchelon() {
    return (String) storedValueForKey(EFIC_C_ECHELON_KEY);
  }

  public void setEficCEchelon(String value) {
    takeStoredValueForKey(value, EFIC_C_ECHELON_KEY);
  }

  public String eficCGrade() {
    return (String) storedValueForKey(EFIC_C_GRADE_KEY);
  }

  public void setEficCGrade(String value) {
    takeStoredValueForKey(value, EFIC_C_GRADE_KEY);
  }

  public String eficCGradePrev() {
    return (String) storedValueForKey(EFIC_C_GRADE_PREV_KEY);
  }

  public void setEficCGradePrev(String value) {
    takeStoredValueForKey(value, EFIC_C_GRADE_PREV_KEY);
  }

  public String eficCivilite() {
    return (String) storedValueForKey(EFIC_CIVILITE_KEY);
  }

  public void setEficCivilite(String value) {
    takeStoredValueForKey(value, EFIC_CIVILITE_KEY);
  }

  public String eficCMotifDepart() {
    return (String) storedValueForKey(EFIC_C_MOTIF_DEPART_KEY);
  }

  public void setEficCMotifDepart(String value) {
    takeStoredValueForKey(value, EFIC_C_MOTIF_DEPART_KEY);
  }

  public String eficCMotifTempsPartiel() {
    return (String) storedValueForKey(EFIC_C_MOTIF_TEMPS_PARTIEL_KEY);
  }

  public void setEficCMotifTempsPartiel(String value) {
    takeStoredValueForKey(value, EFIC_C_MOTIF_TEMPS_PARTIEL_KEY);
  }

  public String eficCPaysNationalite() {
    return (String) storedValueForKey(EFIC_C_PAYS_NATIONALITE_KEY);
  }

  public void setEficCPaysNationalite(String value) {
    takeStoredValueForKey(value, EFIC_C_PAYS_NATIONALITE_KEY);
  }

  public String eficCPosition() {
    return (String) storedValueForKey(EFIC_C_POSITION_KEY);
  }

  public void setEficCPosition(String value) {
    takeStoredValueForKey(value, EFIC_C_POSITION_KEY);
  }

  public String eficCSectionCnu() {
    return (String) storedValueForKey(EFIC_C_SECTION_CNU_KEY);
  }

  public void setEficCSectionCnu(String value) {
    takeStoredValueForKey(value, EFIC_C_SECTION_CNU_KEY);
  }

  public String eficCSectionCnuEc() {
    return (String) storedValueForKey(EFIC_C_SECTION_CNU_EC_KEY);
  }

  public void setEficCSectionCnuEc(String value) {
    takeStoredValueForKey(value, EFIC_C_SECTION_CNU_EC_KEY);
  }

  public String eficCTypeAbsence() {
    return (String) storedValueForKey(EFIC_C_TYPE_ABSENCE_KEY);
  }

  public void setEficCTypeAbsence(String value) {
    takeStoredValueForKey(value, EFIC_C_TYPE_ABSENCE_KEY);
  }

  public String eficCTypeAccesCorps() {
    return (String) storedValueForKey(EFIC_C_TYPE_ACCES_CORPS_KEY);
  }

  public void setEficCTypeAccesCorps(String value) {
    takeStoredValueForKey(value, EFIC_C_TYPE_ACCES_CORPS_KEY);
  }

  public String eficCTypeAccesGrade() {
    return (String) storedValueForKey(EFIC_C_TYPE_ACCES_GRADE_KEY);
  }

  public void setEficCTypeAccesGrade(String value) {
    takeStoredValueForKey(value, EFIC_C_TYPE_ACCES_GRADE_KEY);
  }

  public NSTimestamp eficDArrete() {
    return (NSTimestamp) storedValueForKey(EFIC_D_ARRETE_KEY);
  }

  public void setEficDArrete(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_ARRETE_KEY);
  }

  public NSTimestamp eficDDebAbsence() {
    return (NSTimestamp) storedValueForKey(EFIC_D_DEB_ABSENCE_KEY);
  }

  public void setEficDDebAbsence(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_DEB_ABSENCE_KEY);
  }

  public NSTimestamp eficDDebPostion() {
    return (NSTimestamp) storedValueForKey(EFIC_D_DEB_POSTION_KEY);
  }

  public void setEficDDebPostion(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_DEB_POSTION_KEY);
  }

  public NSTimestamp eficDEchelon() {
    return (NSTimestamp) storedValueForKey(EFIC_D_ECHELON_KEY);
  }

  public void setEficDEchelon(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_ECHELON_KEY);
  }

  public NSTimestamp eficDEffetArrivee() {
    return (NSTimestamp) storedValueForKey(EFIC_D_EFFET_ARRIVEE_KEY);
  }

  public void setEficDEffetArrivee(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_EFFET_ARRIVEE_KEY);
  }

  public NSTimestamp eficDEffetDepart() {
    return (NSTimestamp) storedValueForKey(EFIC_D_EFFET_DEPART_KEY);
  }

  public void setEficDEffetDepart(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_EFFET_DEPART_KEY);
  }

  public NSTimestamp eficDEntreeCategorie() {
    return (NSTimestamp) storedValueForKey(EFIC_D_ENTREE_CATEGORIE_KEY);
  }

  public void setEficDEntreeCategorie(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_ENTREE_CATEGORIE_KEY);
  }

  public NSTimestamp eficDFinAbsence() {
    return (NSTimestamp) storedValueForKey(EFIC_D_FIN_ABSENCE_KEY);
  }

  public void setEficDFinAbsence(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_FIN_ABSENCE_KEY);
  }

  public NSTimestamp eficDFinPostion() {
    return (NSTimestamp) storedValueForKey(EFIC_D_FIN_POSTION_KEY);
  }

  public void setEficDFinPostion(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_FIN_POSTION_KEY);
  }

  public NSTimestamp eficDGrade() {
    return (NSTimestamp) storedValueForKey(EFIC_D_GRADE_KEY);
  }

  public void setEficDGrade(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_GRADE_KEY);
  }

  public Integer eficDiplAnnee() {
    return (Integer) storedValueForKey(EFIC_DIPL_ANNEE_KEY);
  }

  public void setEficDiplAnnee(Integer value) {
    takeStoredValueForKey(value, EFIC_DIPL_ANNEE_KEY);
  }

  public String eficDiplCode() {
    return (String) storedValueForKey(EFIC_DIPL_CODE_KEY);
  }

  public void setEficDiplCode(String value) {
    takeStoredValueForKey(value, EFIC_DIPL_CODE_KEY);
  }

  public String eficDiplCodeUAI() {
    return (String) storedValueForKey(EFIC_DIPL_CODE_UAI_KEY);
  }

  public void setEficDiplCodeUAI(String value) {
    takeStoredValueForKey(value, EFIC_DIPL_CODE_UAI_KEY);
  }

  public String eficDiplLibelleUAI() {
    return (String) storedValueForKey(EFIC_DIPL_LIBELLE_UAI_KEY);
  }

  public void setEficDiplLibelleUAI(String value) {
    takeStoredValueForKey(value, EFIC_DIPL_LIBELLE_UAI_KEY);
  }

  public NSTimestamp eficDNaissance() {
    return (NSTimestamp) storedValueForKey(EFIC_D_NAISSANCE_KEY);
  }

  public void setEficDNaissance(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_NAISSANCE_KEY);
  }

  public NSTimestamp eficDNominationCorps() {
    return (NSTimestamp) storedValueForKey(EFIC_D_NOMINATION_CORPS_KEY);
  }

  public void setEficDNominationCorps(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_NOMINATION_CORPS_KEY);
  }

  public NSTimestamp eficDPromotion() {
    return (NSTimestamp) storedValueForKey(EFIC_D_PROMOTION_KEY);
  }

  public void setEficDPromotion(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_PROMOTION_KEY);
  }

  public NSTimestamp eficDTitularisation() {
    return (NSTimestamp) storedValueForKey(EFIC_D_TITULARISATION_KEY);
  }

  public void setEficDTitularisation(NSTimestamp value) {
    takeStoredValueForKey(value, EFIC_D_TITULARISATION_KEY);
  }

  public String eficEtat() {
    return (String) storedValueForKey(EFIC_ETAT_KEY);
  }

  public void setEficEtat(String value) {
    takeStoredValueForKey(value, EFIC_ETAT_KEY);
  }

  public String eficFonction() {
    return (String) storedValueForKey(EFIC_FONCTION_KEY);
  }

  public void setEficFonction(String value) {
    takeStoredValueForKey(value, EFIC_FONCTION_KEY);
  }

  public java.math.BigDecimal eficForfait() {
    return (java.math.BigDecimal) storedValueForKey(EFIC_FORFAIT_KEY);
  }

  public void setEficForfait(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EFIC_FORFAIT_KEY);
  }

  public String eficIndiceBrut() {
    return (String) storedValueForKey(EFIC_INDICE_BRUT_KEY);
  }

  public void setEficIndiceBrut(String value) {
    takeStoredValueForKey(value, EFIC_INDICE_BRUT_KEY);
  }

  public String eficLieuPosition() {
    return (String) storedValueForKey(EFIC_LIEU_POSITION_KEY);
  }

  public void setEficLieuPosition(String value) {
    takeStoredValueForKey(value, EFIC_LIEU_POSITION_KEY);
  }

  public String eficNoArrete() {
    return (String) storedValueForKey(EFIC_NO_ARRETE_KEY);
  }

  public void setEficNoArrete(String value) {
    takeStoredValueForKey(value, EFIC_NO_ARRETE_KEY);
  }

  public String eficNomPatronymique() {
    return (String) storedValueForKey(EFIC_NOM_PATRONYMIQUE_KEY);
  }

  public void setEficNomPatronymique(String value) {
    takeStoredValueForKey(value, EFIC_NOM_PATRONYMIQUE_KEY);
  }

  public String eficNomUsuel() {
    return (String) storedValueForKey(EFIC_NOM_USUEL_KEY);
  }

  public void setEficNomUsuel(String value) {
    takeStoredValueForKey(value, EFIC_NOM_USUEL_KEY);
  }

  public String eficNumen() {
    return (String) storedValueForKey(EFIC_NUMEN_KEY);
  }

  public void setEficNumen(String value) {
    takeStoredValueForKey(value, EFIC_NUMEN_KEY);
  }

  public String eficObjet() {
    return (String) storedValueForKey(EFIC_OBJET_KEY);
  }

  public void setEficObjet(String value) {
    takeStoredValueForKey(value, EFIC_OBJET_KEY);
  }

  public String eficObservations() {
    return (String) storedValueForKey(EFIC_OBSERVATIONS_KEY);
  }

  public void setEficObservations(String value) {
    takeStoredValueForKey(value, EFIC_OBSERVATIONS_KEY);
  }

  public String eficPrenom() {
    return (String) storedValueForKey(EFIC_PRENOM_KEY);
  }

  public void setEficPrenom(String value) {
    takeStoredValueForKey(value, EFIC_PRENOM_KEY);
  }

  public java.math.BigDecimal eficQuotite() {
    return (java.math.BigDecimal) storedValueForKey(EFIC_QUOTITE_KEY);
  }

  public void setEficQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EFIC_QUOTITE_KEY);
  }

  public java.math.BigDecimal eficRemunHoraire() {
    return (java.math.BigDecimal) storedValueForKey(EFIC_REMUN_HORAIRE_KEY);
  }

  public void setEficRemunHoraire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EFIC_REMUN_HORAIRE_KEY);
  }

  public String eficStatut() {
    return (String) storedValueForKey(EFIC_STATUT_KEY);
  }

  public void setEficStatut(String value) {
    takeStoredValueForKey(value, EFIC_STATUT_KEY);
  }

  public String eficTemCrct() {
    return (String) storedValueForKey(EFIC_TEM_CRCT_KEY);
  }

  public void setEficTemCrct(String value) {
    takeStoredValueForKey(value, EFIC_TEM_CRCT_KEY);
  }

  public String eficTemValide() {
    return (String) storedValueForKey(EFIC_TEM_VALIDE_KEY);
  }

  public void setEficTemValide(String value) {
    takeStoredValueForKey(value, EFIC_TEM_VALIDE_KEY);
  }

  public String eficTypeFinancement() {
    return (String) storedValueForKey(EFIC_TYPE_FINANCEMENT_KEY);
  }

  public void setEficTypeFinancement(String value) {
    takeStoredValueForKey(value, EFIC_TYPE_FINANCEMENT_KEY);
  }

  public String eficUai() {
    return (String) storedValueForKey(EFIC_UAI_KEY);
  }

  public void setEficUai(String value) {
    takeStoredValueForKey(value, EFIC_UAI_KEY);
  }

  public String eficUaiAffectation() {
    return (String) storedValueForKey(EFIC_UAI_AFFECTATION_KEY);
  }

  public void setEficUaiAffectation(String value) {
    takeStoredValueForKey(value, EFIC_UAI_AFFECTATION_KEY);
  }

  public org.cocktail.mangue.modele.mangue.EOSupInfoFichier toFichier() {
    return (org.cocktail.mangue.modele.mangue.EOSupInfoFichier)storedValueForKey(TO_FICHIER_KEY);
  }

  public void setToFichierRelationship(org.cocktail.mangue.modele.mangue.EOSupInfoFichier value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.EOSupInfoFichier oldValue = toFichier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_FICHIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_FICHIER_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOGrade toGrade() {
    return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey(TO_GRADE_KEY);
  }

  public void setToGradeRelationship(org.cocktail.mangue.modele.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GRADE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeOrigineFinancement toOrigineFinancement() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeOrigineFinancement)storedValueForKey(TO_ORIGINE_FINANCEMENT_KEY);
  }

  public void setToOrigineFinancementRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeOrigineFinancement value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeOrigineFinancement oldValue = toOrigineFinancement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORIGINE_FINANCEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORIGINE_FINANCEMENT_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.EOParamPromotion toParamPromotion() {
    return (org.cocktail.mangue.modele.mangue.EOParamPromotion)storedValueForKey(TO_PARAM_PROMOTION_KEY);
  }

  public void setToParamPromotionRelationship(org.cocktail.mangue.modele.mangue.EOParamPromotion value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.EOParamPromotion oldValue = toParamPromotion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PARAM_PROMOTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PARAM_PROMOTION_KEY);
    }
  }
  

/**
 * Créer une instance de EOSupInfoData avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOSupInfoData createEOSupInfoData(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String eficBoe
, String eficBoeType
, String eficCivilite
, NSTimestamp eficDNaissance
, String eficIndiceBrut
, String eficPrenom
, String eficTemCrct
, String eficTemValide
, String eficTypeFinancement
, org.cocktail.mangue.modele.mangue.EOSupInfoFichier toFichier, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu			) {
    EOSupInfoData eo = (EOSupInfoData) createAndInsertInstance(editingContext, _EOSupInfoData.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setEficBoe(eficBoe);
		eo.setEficBoeType(eficBoeType);
		eo.setEficCivilite(eficCivilite);
		eo.setEficDNaissance(eficDNaissance);
		eo.setEficIndiceBrut(eficIndiceBrut);
		eo.setEficPrenom(eficPrenom);
		eo.setEficTemCrct(eficTemCrct);
		eo.setEficTemValide(eficTemValide);
		eo.setEficTypeFinancement(eficTypeFinancement);
    eo.setToFichierRelationship(toFichier);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  
	  public EOSupInfoData localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSupInfoData)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSupInfoData creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSupInfoData creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOSupInfoData object = (EOSupInfoData)createAndInsertInstance(editingContext, _EOSupInfoData.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOSupInfoData localInstanceIn(EOEditingContext editingContext, EOSupInfoData eo) {
    EOSupInfoData localInstance = (eo == null) ? null : (EOSupInfoData)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOSupInfoData#localInstanceIn a la place.
   */
	public static EOSupInfoData localInstanceOf(EOEditingContext editingContext, EOSupInfoData eo) {
		return EOSupInfoData.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSupInfoData fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSupInfoData fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSupInfoData eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSupInfoData)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSupInfoData fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSupInfoData fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSupInfoData eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSupInfoData)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSupInfoData fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSupInfoData eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSupInfoData ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSupInfoData fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

// EODbVersion.java
// Created on Fri Feb 09 14:15:09 Europe/Paris 2007 by Apple EOModeler Version 5.2

package org.cocktail.mangue.modele.mangue;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EODbVersion extends EOGenericRecord {

    public EODbVersion() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EODbVersion(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String dbVersionLibelle() {
        return (String)storedValueForKey("dbVersionLibelle");
    }

    public void setDbVersionLibelle(String value) {
        takeStoredValueForKey(value, "dbVersionLibelle");
    }

    public NSTimestamp dbVersionDate() {
        return (NSTimestamp)storedValueForKey("dbVersionDate");
    }

    public void setDbVersionDate(NSTimestamp value) {
        takeStoredValueForKey(value, "dbVersionDate");
    }

    public NSTimestamp dbInstallDate() {
        return (NSTimestamp)storedValueForKey("dbInstallDate");
    }

    public void setDbInstallDate(NSTimestamp value) {
        takeStoredValueForKey(value, "dbInstallDate");
    }

    public String dbComment() {
        return (String)storedValueForKey("dbComment");
    }

    public void setDbComment(String value) {
        takeStoredValueForKey(value, "dbComment");
    }
    
    // mÆthodes ajoutÆes
    /** retourne la derni&egrave;re version du user Mangue et null si pas trouv&eacute; */
    public static EODbVersion derniereVersionBase(EOEditingContext editingContext) {
    	EOFetchSpecification fs = new EOFetchSpecification("DbVersion",null,new NSArray(EOSortOrdering.sortOrderingWithKey("dbVersionId", EOSortOrdering.CompareDescending)));
    	fs.setFetchLimit(1);
    	try {
    		return (EODbVersion)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
    	} catch (Exception e) {
    		return null;
    	}
    }
}

//EOCongeSsTraitement.java
//Created on Mon Oct 22 14:20:12 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.modalites;

import java.util.Enumeration;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de gestion :<BR>
 * L'individu doit avoir une position de stage pendant la periode<BR>
 * La duree maximale d'un conge sans traitement est de 1 an<BR>
 * Pour les conges autre que service national, le conge est renouvelable 2 fois<BR>
 * Le conge pour service national n'est valable que pour les hommes<BR>
 * @author christine
 *
 */
public class EOCongeSsTraitement extends _EOCongeSsTraitement {

	public EOCongeSsTraitement() {
		super();
	}

	public static EOCongeSsTraitement findForKey( EOEditingContext ec, Number key) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(CONGE_ORDRE_KEY + "=%@", new NSArray(key));
		return fetchFirstByQualifier(ec, qualifier);
	}

	/** Recherche des conges sans traitement d'un individu pendant une periode donnee 
	 * @param individu (peut etre nul)
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 */
	public static NSArray rechercherCongesSansTraitementPourIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@",new NSArray(CocktailConstantes.VRAI)));

		if (individu != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));

		if (debutPeriode !=  null)
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);

	}

	public static EOCongeSsTraitement creer(EOEditingContext ec, EOIndividu individu) {

		EOCongeSsTraitement newObject = (EOCongeSsTraitement) createAndInsertInstance(ec, EOCongeSsTraitement.ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setTemValide(CocktailConstantes.VRAI);

		EOTypeAbsence typeAbsence = EOTypeAbsence.findForCode(ec, EOTypeAbsence.TYPE_CST);
		EOAbsences absence = EOAbsences.creer(ec, individu, typeAbsence);
		newObject.setAbsenceRelationship(absence);

		return newObject;
	}


	public String typeEvenement() {
		return "CGST";
	}

	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (individu() == null)
			throw new NSValidation.ValidationException("Vous devez fournir un individu");
		if (dateDebut() == null)
			throw new NSValidation.ValidationException("La date de début doit etre définie !");
		if (dateFin() == null)
			throw new NSValidation.ValidationException("Vous devez fournir une date de fin");
		if (dateDebut().after(dateFin()))
			throw new NSValidation.ValidationException("La date de fin doit être postérieure à la date de début (FIN : " + dateFinFormatee() + " , DEBUT : " + dateDebutFormatee() + ")");
		if (noArrete() != null && noArrete().length() > 20)
			throw new NSValidation.ValidationException("Un numéro d'arrêté comporte au plus 20 caractères");

		//Existe t il deja un conge sans traitement pour cette periode (A verifier seulement sur les conges sans traitement non valides, donc en ajout).
		if (dModification() == null) {
			NSArray conges = EOModalitesService.rechercherPourIndividuEtPeriode(editingContext(), individu(), dateDebut() , dateFin());
			for (Enumeration<EOModalitesService> e = conges.objectEnumerator();e.hasMoreElements();) {
				EOModalitesService modalite = e.nextElement();
				throw new NSValidation.ValidationException("Les dates ne sont pas valides. Il existe déjà une modalité de service pendant cette période ! ( " + modalite.modLibelle() + " du " + DateCtrl.dateToString(modalite.dateDebut()) + " au " + DateCtrl.dateToString(modalite.dateFin()) + ")");
			}
		}

//		// Vérifier que l'individu est en stage
//		NSArray changements = EOChangementPosition.rechercherChangementsCongeStagiairePourIndividuEtPeriode(editingContext(), individu(), dateDebut(), dateFin());
//		if (changements == null || changements.count() == 0) {
//			throw new NSValidation.ValidationException("Cet individu n'a pas une position de congé stagiaire (Onglet Carrière/Position) pendant la période");
//		}
		
		if (motif() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un motif");
		}  else if (motif().estServiceNational() && individu().estHomme() == false) {
			throw new NSValidation.ValidationException("Le congé pour Service National n'est applicable qu'aux hommes");
		}
		
		NSTimestamp dateFinTheorique = DateCtrl.dateAvecAjoutAnnees(dateDebut(), 1);
		if (DateCtrl.isAfter(dateFin(), dateFinTheorique)) {
			throw new NSValidation.ValidationException("Un congé sans traitement dure au maximum un an");
		}

		// par date décroissante
		NSArray conges = Duree.rechercherDureesPourIndividuEtPeriode(editingContext(), ENTITY_NAME, individu(), dateDebut(), dateFin());
		if (motif().estServiceNational()) {
			// Vérifier si il y a un autre congé pour service national
			for (java.util.Enumeration<EOCongeSsTraitement> e = conges.objectEnumerator();e.hasMoreElements();) {
				EOCongeSsTraitement conge = e.nextElement();
				if (conge != this && conge.motif().estServiceNational()) {
					throw new NSValidation.ValidationException("Le congé pour Service National n'est pas renouvelable");
				}
			}
		} else {
			int total = 0;
			// Calculer le nombre de congé total
			for (java.util.Enumeration<EOCongeSsTraitement> e = conges.objectEnumerator();e.hasMoreElements();) {
				EOCongeSsTraitement conge = e.nextElement();
				if (conge != this && conge.motif().estServiceNational() == false) {
					total++;
				}
			}
			if (total > 2) {	// Il y a plus de deux autres congés sans traitement autre que le congé courant
				throw new NSValidation.ValidationException("Le congé sans traitement n'est renouvelable que deux fois");
			}
		}
		
		absence().setDateDebut(dateDebut());
		absence().setDateFin(dateFin());
		absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true)));

		if (dCreation() == null)	
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
	
	
	/**  methode de validation appelee par l'automate Cir (pour des donnees saisies avant l'implementation du Cir)
	 * et par la methode validateForSave
	 * @return
	 */
	public String validationsCir() {
		
		return null;
	}
	
	
	// méthodes protégées
	protected void init() {
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		removeObjectFromBothSidesOfRelationshipWithKey(motif(),MOTIF_KEY);
	}

}
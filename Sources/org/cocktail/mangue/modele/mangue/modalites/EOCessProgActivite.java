// EOCessProgActivite.java
// Created on Wed Nov 30 14:51:50 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.modalites;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation :<BR>
 * 	La date debut doit etre fournie<BR>
 *	Si la date de fin est fournie, celle de debut doit etre anterieure<BR> 
 *	Verifie la compatibilite avec les conges et les modalites de service <BR>
 *	Verifie que l'individu peut beneficier de la CPA (fonctionnaires hors personnel hospitalo-universitaire et comptable) 
 *  et age compris dans les limites<BR>
 *  Verifie que la surcotisation ou la quotite degressive ne sont pas selectionnees pour des CPAs demarrant avant le 01/01/2004
 * @author christine
 */
// 02/07/2010 - modification des r&grave;gles de validation
public class EOCessProgActivite extends _EOCessProgActivite {
	private static NSTimestamp DATE_LIMITE_ANCIEN_REGIME = DateCtrl.stringToDate("01/01/2004");
	
	public static EOCessProgActivite findForKey( EOEditingContext ec, Number key) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONGE_ORDRE_KEY + "=%@", new NSArray(key)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		
	}
	public static EOCessProgActivite creer(EOEditingContext ec, EOIndividu individu) {
		
		EOCessProgActivite newObject = (EOCessProgActivite) createAndInsertInstance(ec, EOCessProgActivite.ENTITY_NAME);    

		newObject.setIndividuRelationship(individu);
		newObject.setTemValide(CocktailConstantes.VRAI);

		EOTypeAbsence typeAbsence = EOTypeAbsence.findForCode(ec, EOTypeAbsence.TYPE_CPA);
		EOAbsences absence = EOAbsences.creer(ec, individu, typeAbsence);
		newObject.setAbsenceRelationship(absence);

		return newObject;
	}

	public EOCessProgActivite() {
		super();
	}

	public String typeEvenement() {
		return "CPA";
	}
	public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
		// 02/07/2010
		String message = validationsCir();
		if (message != null && message.length() > 0) {
			throw new NSValidation.ValidationException(message);
		}		NSArray<EOElementCarriere> elements = EOElementCarriere.findForPeriode(editingContext(),individu(),dateDebut(),dateFin());
		NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin(),false);
		if ((elements == null || elements.count() == 0) && (contrats == null || contrats.count() == 0)) {
			throw new NSValidation.ValidationException("Il doit y avoir au moins un élément de carrière ou un contrat défini sur cette période");
		} 
		if (elements != null && elements.count() > 0) {
			for (java.util.Enumeration<EOElementCarriere> e = elements.objectEnumerator();e.hasMoreElements();) {
				EOElementCarriere element = e.nextElement();
				if (element.toCorps().cCorps().equals(EOCorps.CORPS_POUR_COMPTABLE)) {
					throw new NSValidation.ValidationException("Un agent comptable ne peut pas bénéficier d'un CPA");
				} else if (element.toCarriere().toTypePopulation().estHospitalier()) {
					throw new NSValidation.ValidationException("Le personnel hospitalo-universitaire ne peut pas bénéficier d'un CPA");
				}
			}		
		}

		absence().setDateDebut(dateDebut());
		absence().setDateFin(dateFin());
		absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true)));

		if (dCreation() == null)	
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
	public boolean estCessationTotaleActivite() {
		return temCta() != null && temCta().equals(CocktailConstantes.VRAI);
	}
	public void setEstCessationTotaleActivite(boolean aBool) {
		if (aBool) {
			setTemCta(CocktailConstantes.VRAI);
		} else {
			setTemCta(CocktailConstantes.FAUX);
		}
	}
	public boolean surcotiseRetraite() {
		return temSurcotisation() != null && temSurcotisation().equals(CocktailConstantes.VRAI);
	}
	public void setSurcotiseRetraite(boolean aBool) {
		if (aBool) {
			setTemSurcotisation(CocktailConstantes.VRAI);
		} else {
			setTemSurcotisation(CocktailConstantes.FAUX);
		}
	}
	public boolean estQuotiteDegressive() {
		return temQuotDegressive() != null && temQuotDegressive().equals(CocktailConstantes.VRAI);
	}
	public void setEstQuotiteDegressive(boolean aBool) {
		if (aBool) {
			setTemQuotDegressive(CocktailConstantes.VRAI);
		} else {
			setTemQuotDegressive(CocktailConstantes.FAUX);
		}
	}
	/** Retourne true si la date est anterieure au 01/01/2004 */
	public boolean estAncienRegime() {
		return dateDebut() != null && DateCtrl.isBefore(dateDebut(),DATE_LIMITE_ANCIEN_REGIME);
	}
	/**  methode de validation appelee par l'automate Cir (pour des donnees saisies avant l'implementation du Cir)
	 * et par la methode validateForSave
	 * @return
	 */
	public String validationsCir() {
		if (estAncienRegime()) {
			String dateLimite = DateCtrl.dateToString(DATE_LIMITE_ANCIEN_REGIME);
			if (surcotiseRetraite()) {
				return "Un temps partiel avec surcotisation ne peut commencer qu'après le " + dateLimite;
			}
			if (estQuotiteDegressive()) {
				return "Un temps partiel avec quotité dégressive ne peut commencer qu'après le " + dateLimite;
			}
		}
		return null;
	}
	// méthodes protégées
	protected void init() {
		setTemCta(CocktailConstantes.FAUX);
		setTemQuotDegressive(CocktailConstantes.FAUX);
		setTemSurcotisation(CocktailConstantes.FAUX);
	}
}

// EODetailCrct.java
// Created on Mon Mar 06 12:35:29 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.modalites;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation<BR>
 * Les dates de debut et de fin doivent etre fournies et etre a l'interieur de la periode de crct<BR>
 * La date de fin doit etre posterieure a la date de debut.<BR>
 */
public class EOCrctDetail extends _EOCrctDetail {

    public EOCrctDetail() {
        super();
    }
    
    /**
     * 
     * @param ec
     * @param crct
     * @param insertObject
     * @return
     */
	public static EOCrctDetail creer(EOEditingContext ec, EOCrct crct, boolean insertObject) {
		
		EOCrctDetail newObject = new EOCrctDetail();    
		newObject.setToCrctRelationship(crct);
		newObject.setDCreation(new NSTimestamp());
		if (insertObject)
			ec.insertObject(newObject);
		return newObject;
	}
	
	/**
	 * 
	 * @param edc
	 * @param crct
	 * @return
	 */
	public static NSArray<EOCrctDetail> findForCrct(EOEditingContext edc, EOCrct crct) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_CRCT_KEY + " = %@", new NSArray(crct));
		return fetchAll(edc, qualifier);
	}
	
	
    public void initAveConge(EOCrct crct) {
    		setToCrctRelationship(crct);
    }
    public void supprimerRelations() {
		setToCrctRelationship(null);
    }

	public void validateForSave() {
		setDModification(DateCtrl.today());
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début de la période de fractionnement doit être fournie");
		} else if (DateCtrl.isBefore(dateDebut(), toCrct().dateDebut()) || DateCtrl.isAfter(dateDebut(), toCrct().dateFin())) {
			throw new NSValidation.ValidationException("Le début de la période de fractionnement doit se trouver dans la période du CRCT");
		}
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("La date de fin de la période de fractionnement doit être fournie");
		} else if (DateCtrl.isBefore(dateFin(), toCrct().dateDebut()) || DateCtrl.isAfter(dateFin(),toCrct().dateFin())) {
			throw new NSValidation.ValidationException("La fin de la période de fractionnement doit se trouver dans la période du CRCT");
		}
		if (DateCtrl.isBefore(dateFin(),dateDebut())) {
			throw new NSValidation.ValidationException("La date de fin  de la période de fractionnement ne peut être postérieure à celle de début");
		}
	}
}
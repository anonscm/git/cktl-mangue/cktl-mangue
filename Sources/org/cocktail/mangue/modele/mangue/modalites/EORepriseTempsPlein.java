// EORepriseTempsPlein.java
// Created on Tue Nov 22 14:31:48 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.modalites;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation des reprises de temps partiel :<BR>
 * 	La date de reprise et le temps partiel associe doivent etre fournies<BR>
 *	La date de reprise doit etre posterieure a la date de debut de temps partiel<BR> 
 */
public class EORepriseTempsPlein extends _EORepriseTempsPlein {

	public EORepriseTempsPlein() {
		super();
	}
	public String typeEvenement() {
		return "";
	}
	
	public static EORepriseTempsPlein creer(EOEditingContext ec, EOTempsPartiel tempsPartiel, boolean insertObject) {
		
		EORepriseTempsPlein newObject = (EORepriseTempsPlein) createAndInsertInstance(ec, EORepriseTempsPlein.ENTITY_NAME);    
		newObject.initAvecIndividuEtTempsPartiel(tempsPartiel.individu(), tempsPartiel);

		newObject.setDCreation(new NSTimestamp());
		newObject.setTemConfirme(CocktailConstantes.FAUX);
		newObject.setTemGestEtab(CocktailConstantes.FAUX);
		newObject.setTemValide("O");
		
		return newObject;
	}
	public void initAvecIndividuEtTempsPartiel(EOIndividu individu,EOTempsPartiel tempsPartiel) {
		setIndividuRelationship(individu);
		setTempsPartielRelationship(tempsPartiel);
	}
	public void supprimerRelations() {
		setIndividuRelationship(null);
		setTempsPartielRelationship(null);
	}
	public String dateRepriseFormatee() {
		return SuperFinder.dateFormatee(this,D_REPRISE_TEMPS_PLEIN_KEY);
	}
	public void setDateRepriseFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,D_REPRISE_TEMPS_PLEIN_KEY,uneDate);
	}
	public String dateArreteTempsPartiel() {
		if (tempsPartiel() == null) {
			return null;
		} else {
			return tempsPartiel().dateArreteFormatee();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public NSArray<EOVisa> visas() {
		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(), individu(), dRepriseTempsPlein(),dRepriseTempsPlein());
		if (carrieres.size() > 0) {
			// il n'y a pas de chevauchement de carrière, on prend le premier
			EOCarriere carriere = carrieres.get(0);
			return EOVisa.rechercherVisaPourTableCongeEtTypePopulation(editingContext(), ENTITY_NAME, carriere.toTypePopulation().code());
		} else {
			NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(), individu(), dRepriseTempsPlein(),dRepriseTempsPlein(),false);
			if (contrats.size() > 0) {
				// rechercher les contrats de rémunération principale
				for (EOContrat contrat : contrats) {
					return EOVisa.rechercherVisaPourTableCongeTypeContratEtCondition(editingContext(), ENTITY_NAME, contrat.toTypeContratTravail().code(),null);
				}
			}
		}
		return null;
	}
	

	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		
		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException("Un numéro d'arrêté comporte au plus 20 caractères");
		}
		if (tempsPartiel() == null) {
			throw new NSValidation.ValidationException("Une reprise de temps partiel doit être associée à un temps partiel");
		}
		if (dRepriseTempsPlein() == null) {
			throw new NSValidation.ValidationException("La date de reprise doit être fournie");			
		} else if (DateCtrl.isBefore(dRepriseTempsPlein(),tempsPartiel().dateDebut())) {
			throw new NSValidation.ValidationException("La date de reprise ne peut être antérieure à la date de début de temps partiel");
		}
		
		setDModification(new NSTimestamp());
		
	}
	

	// méthodes protégées
	protected void init() {
		setTemConfirme(CocktailConstantes.FAUX);
		setTemGestEtab(CocktailConstantes.FAUX);
		setTemValide("O");
	}

	// méthodes statiques
	public static NSArray<EORepriseTempsPlein> findForTempsPartiel(EOEditingContext ec,EOTempsPartiel tempsPartiel) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEMPS_PARTIEL_KEY + "= %@", new NSArray(tempsPartiel)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "= %@", new NSArray("O")));
		return fetchAll(ec, new EOAndQualifier(qualifiers), null);
	}
	public static EORepriseTempsPlein rechercherReprisePourTempsPartiel(EOEditingContext ec,EOTempsPartiel tempsPartiel) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEMPS_PARTIEL_KEY + "= %@", new NSArray(tempsPartiel)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "= %@", new NSArray("O")));
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}

}

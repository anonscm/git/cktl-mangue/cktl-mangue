//EOTempsPartiel.java
//Created on Mon Nov 21 16:34:45 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.modalites;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.Outils;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOQuotite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartEnfant;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodeHandicap;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation des temps partiel :<BR>
 * 	La date de debut et la quotite doivent etre fournies<BR>
 *	Si la date de fin est fournie, celle de debut doit etre anterieure<BR> 
 *	Il doit y avoir au moins un segment de carriere ou un contrat pour cette periode<BR>
 *	Le motif doit etre fourni<BR>
 *	Il ne peut pas y avoir de chevauchement entre deux periodes de temps partiel<BR>
 *	La quotite doit etre comprise entre 50% et 90%<BR>
 *	Pour un agent a temps partiel de droit, la quotite doit etre inferieure a 90%<BR>
 *  La periodicite doit etre definie et inferieure a 12 (mois). Le controle sur
 *  la borne inferieure 6 mois n'est pas fait car dans certains cas de terrain, il est inapplicable<BR>
 *  Un contractuel recrute a temps incomplet ne peut pas beneficier d'un temps partiel.<BR>
 *  Les enseignants hospitalo-universitaires ne peuvent pas beneficier d'un temps partiel.<BR>
 *  Pour un comptable, la quotite ne peut etre que 80% ou 90%<BR>
 *  Un temps partiel de droit pour handicap ne s'applique qu'aux titulaires<BR>
 *  On ne peut definir un temps partiel de droit pour handicap que si les periodes de handicap 
 *  chevauchent la periode de temps partiel et que la date d'avis medical pour ces periodes est saisie.<BRW
 *  Certains types de contrat ne sont pas compatibles avec un temps partiel.<BR>
 *  Un temps partiel de droit exige un contrat d'un an minimum a temps plein<BR>
 *  Un temps partiel sur autorisation exige un contrat continu minimum d'un an a temps plein<BR>
 *  Un temps partiel surcotise ne peut commencer qu'apres le 01/01/2004<BR>
 *  sauf si il s'agit d'un temps partiel pour la naissance d'un enfant<BR>
 *  La quotite de temps partiel pour un enseignant de second degre doit etre de 50%.<BR>
 *  Pour un temps partiel d'un enseignant, la périodicite est de 12 mois.<BR>
 *	Les periodes de temps partiel doivent etre compatibles avec les conges et modalites de service pendant la periode.<BR> 
 *	La declaration de l'enfant est obligatoire dans un temps partiel pour elever un enfant ou un enfant adopte apres le 01/01/2004.<BR>
 *	Le motif de temps partiel doit etre coherent avec le lien de filiation de l'enfant choisi<BR>
 *  Si un enfant est ajoute, il doit avoir moins de 3 ans ou etre arrive depuis moins de 3 ans au foyer pendant la duree du temps partiel<BR>
 */

public class EOTempsPartiel extends _EOTempsPartiel {

	private final static NSTimestamp DATE_LIMITE_SURCOTISATION = DateCtrl.stringToDate("01/01/2004");

	public EOTempsPartiel() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param key
	 * @return
	 */
	public static EOTempsPartiel findForKey( EOEditingContext ec, Number key) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_SEQ_ARRETE_KEY +" = %@", new NSArray(key)));
			return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static EOTempsPartiel creer(EOEditingContext ec, EOIndividu individu) {

		EOTempsPartiel newObject = (EOTempsPartiel) createAndInsertInstance(ec, EOTempsPartiel.ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setTemConfirme(CocktailConstantes.FAUX);
		newObject.setTemGestEtab(CocktailConstantes.FAUX);
		newObject.setTemSurcotisation(CocktailConstantes.FAUX);
		newObject.setTemRepriseTempsPlein(CocktailConstantes.FAUX);
		newObject.setTemValide(CocktailConstantes.VRAI);

		EOTypeAbsence typeAbsence = EOTypeAbsence.findForCode(ec, EOTypeAbsence.TYPE_TEMPS_PARTIEL);
		EOAbsences absence = EOAbsences.creer(ec, individu, typeAbsence);		
		newObject.setAbsenceRelationship(absence);

		return newObject;
	}

	public static EOTempsPartiel findForIndividuAndDate(EOEditingContext ec, EOIndividu individu, NSTimestamp dateRef) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = 'O'",null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY+"=%@", new NSArray(individu)));
		if (dateRef != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateRef, DATE_FIN_KEY,dateRef));
		}
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);				
	}

	public static EOTempsPartiel findForAbsence(EOEditingContext ec, EOAbsences absence) {
		return fetchFirstByQualifier(ec, EOQualifier.qualifierWithQualifierFormat(ABSENCE_KEY + "=%@", new NSArray(absence)));
	}

	/**
	 * 
	 * Renouvellement d'une periode de temps partiel
	 * 
	 * @param ec
	 * @param ancienTemps
	 * @return
	 */
	public static EOTempsPartiel renouveler(EOEditingContext ec, EOTempsPartiel ancienTemps) {

		EOTempsPartiel newObject = (EOTempsPartiel) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.takeValuesFromDictionary(ancienTemps.snapshot());

		newObject.setDFinExecution(null);
		newObject.setDateFin(null);
		newObject.setTemValide("O");

		return newObject;

	}

	public String typeEvenement() {
		return EOTypeAbsence.TYPE_TEMPS_PARTIEL;
	}
	public String dateReelle() {
		return SuperFinder.dateFormatee(this,D_FIN_EXECUTION_KEY);
	}
	public void setDateReelle(String uneDate) {
		SuperFinder.setDateFormatee(this,D_FIN_EXECUTION_KEY,uneDate);
	}

	public boolean estSigne() {
		return temConfirme() != null && temConfirme().equals(CocktailConstantes.VRAI);
	}
	public void setEstSigne(boolean aBool) {
		if (aBool) {
			setTemConfirme(CocktailConstantes.VRAI);
		} else {
			setTemConfirme(CocktailConstantes.FAUX);
		}
	}
	public boolean aReprisTempsPlein() {
		return temRepriseTempsPlein() != null && temRepriseTempsPlein().equals(CocktailConstantes.VRAI);
	}
	public void setAReprisTempsPlein(boolean aBool) {
		if (aBool) {
			setTemRepriseTempsPlein(CocktailConstantes.VRAI);
		} else {
			setTemRepriseTempsPlein(CocktailConstantes.FAUX);
		}
	}
	public boolean surcotisePourRetraite() {
		return temSurcotisation() != null && temSurcotisation().equals(CocktailConstantes.VRAI);
	}
	public void setSurcotisePourRetraite(boolean aBool) {
		if (aBool) {
			setTemSurcotisation(CocktailConstantes.VRAI);
		} else {
			setTemSurcotisation(CocktailConstantes.FAUX);
		}
	}
	/** retourne true si il y a un arrete d'annulation */
	public boolean estAnnule() {
		return dAnnulation() != null || noArreteAnnulation() != null;
	}
	/** retourne true si l'arrete est gere par l'etablissement */
	public boolean estGereParEtablissement() {
		return temGestEtab() != null && temGestEtab().equals(CocktailConstantes.VRAI);
	}
	/** Retourne true si la date est anterieure au 01/01/2004 */
	public boolean estAncienRegime() {
		return dateDebut() != null && DateCtrl.isBefore(dateDebut(),DATE_LIMITE_SURCOTISATION);
	}	

	/**
	 * 
	 */
	public NSArray<EOVisa> visas() {

		try {
			String condition = "TEMPS_PARTIEL.C_MOTIF_TEMPS_PARTIEL = '" + motif().code() + "' AND TEMPS_PARTIEL.C_MOTIF_TEMPS_PARTIEL IS NOT NULL";
			NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),individu(),dateDebut(),dateDebut());
			if (carrieres.count() > 0) {
				return EOVisa.rechercherVisaPourTableCongeTypePopulationEtCondition(editingContext(), 
						StringCtrl.replace(ENTITY_TABLE_NAME, "MANGUE.", ""), 
						carrieres.get(0).toTypePopulation().code(),condition);
			} else {
				NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateDebut(),false);
				for (EOContrat contrat : contrats) {
					EOVisa.rechercherVisaPourTableCongeTypeContratEtCondition(editingContext(), ENTITY_TABLE_NAME, contrat.toTypeContratTravail().code(),condition);
				}
			}

			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/** Retourne true si le temps partiel debute apres le 01/01/2004 et que le motif est "Pour elever un enfant..." */
	public boolean estDeclarationEnfantObligatoire() {
		return dateDebut() != null && motif() != null && 
				DateCtrl.isAfterEq(dateDebut(), ManGUEConstantes.DATE_LIMITE_DECLARATION_ENFANT) && (motif().estPourEleverEnfant() || motif().estPourEleverEnfantAdopte());
	}


	/**
	 * 
	 * @return
	 */
	public double quotiteFinanciere() {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(QUOTITE_KEY +" = %@", new NSArray(quotite())));
			return EOQuotite.fetchFirstByQualifier(editingContext(), 
					new EOAndQualifier(qualifiers)).quotiteFinanciere().doubleValue();
		} catch (Exception e) {
			return quotite().doubleValue();
		}
	}


	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (temValide().equals(CocktailConstantes.FAUX) )
			return;

		if (individu() == null)
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_INDIVIDU_NON_RENSEIGNE, "Temps Partiel"));
		if (dateDebut() == null)
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "Temps Partiel"));
		if (dateFin() != null && dateDebut().after(dateFin()))
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "Temps Partiel", dateFinFormatee(), dateDebutFormatee()));

		if (noArrete() != null && noArrete().length() > 20)	
			throw new NSValidation.ValidationException("Un numéro d'arrêté comporte au plus 20 caractères");

		//Existe t il deja un temps partiel pour cette periode (A verifier seulement sur les temps partiels non valides, donc en ajout).
		if (dModification() == null) {
			NSArray<EOTempsPartiel> tempsPartiels = rechercherTempsPartielPourIndividuEtPeriode(editingContext(), individu(), dateDebut() , dFinExecution());
			for (Enumeration<EOTempsPartiel> e = tempsPartiels.objectEnumerator();e.hasMoreElements();) {
				EOTempsPartiel tp = e.nextElement();
				throw new NSValidation.ValidationException("Les dates ne sont pas valides. Il existe déjà un temps partiel de ce type pendant la période du " + DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dFinExecution()) + " !\n (TP existant du " + tp.dateDebutFormatee() + " au " + tp.dateFinFormatee() + ").\nPensez à vérifier la date de fin réelle.");
			}
		}

		if (noArreteAnnulation() != null && noArreteAnnulation().length() > 20)
			throw new NSValidation.ValidationException("Un numéro d'arrêté comporte au plus 20 caractères");

		Integer quotiteMaximale = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_QUOT_MAX_DROIT);			
		Integer quotiteMinimale = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_QUOT_MIN_DROIT);	

		if (quotite() == null || quotite().doubleValue() == 0)
			throw new NSValidation.ValidationException("Vous devez fournir la quotité de temps partiel !");
		else {
			if (motif() != null && motif().aQuotiteLimitee()) {
				if (quotite().doubleValue() > quotiteMaximale.intValue())
					throw new NSValidation.ValidationException("Pour un agent à temps partiel de droit, la quotité doit être inférieure à " + quotiteMaximale + "% !");
			} else
				if (quotite().doubleValue() < quotiteMinimale.intValue() || quotite().doubleValue() > quotiteMaximale.intValue()) {
					throw new NSValidation.ValidationException("Pour un agent à temps partiel, la quotité doit être comprise entre " + quotiteMinimale + "% et " + quotiteMaximale + "%");
				}
		}

		String message = validationsCir();
		if (message != null && message.length() > 0)
			throw new NSValidation.ValidationException(message);

		if (motif() == null)
			throw new NSValidation.ValidationException("Vous devez sélectionner un motif de temps partiel");
		else if (motif().estPourEleverEnfant() || motif().estPourEleverEnfantAdopte()) {
			if (individu().enfants() == null || individu().enfants().count() == 0) {
				throw new NSValidation.ValidationException("L'agent n'a pas d'enfant, il ne peut pas bénéficier d'un congé pour élever les enfants");
			}
			// vérifier qu'il dure au maximum 3 ans
			if (dateFin() == null || DateCtrl.calculerDureeEnMois(dateDebut(),dateFin()).intValue() > ManGUEConstantes.DUREE_MOIS_MAX_TPS_PARTIEL_ENFANT) {
				throw new NSValidation.ValidationException("Un congé pour élever les enfants dure au maximum 3 ans !");
			} 

			// Verifier que l'enfant soit né après le 01/01/2004 pour un motif "Elever un enfant de - de 3 ans
			if (motif().estPourEleverEnfant() ) {
				if (DateCtrl.isBefore(enfant().dNaissance(), ManGUEConstantes.DATE_LIMITE_DECLARATION_ENFANT) ) 
					throw new NSValidation.ValidationException("Le motif 'Elever un enfant (-3 ans)' n'est pas autorisé pour un enfant né avant le 01/01/2004 !");
			}
		}

		if (periodicite() != null && periodicite().intValue() > 36)
			throw new NSValidation.ValidationException("La périodicité doit être comprise entre 6 et 36 (mois)");

		if (motif().estPourHandicape()) {
			validerPourHandicap();
		}

		validerPourCarriereEtContrat();

		if (absence() != null) {
			absence().setDateDebut(dateDebut());
			absence().setDateFin(dateFin());
			absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true)));
		}
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	/** Retourne la date de fin reelle d'un temps partiel a partir de la date de fin d'execution
	 * ou de la reprise de temps plein si elle existe (priorite a la date de fin d'execution)
	 * @return
	 */
	public NSTimestamp dateFinReelle() {
		// 24/11/2010 - la date fin peut etre nulle
		NSTimestamp dateFin = dateFin();
		if (dFinExecution() != null && (dateFin() == null || DateCtrl.isBefore(dFinExecution(), dateFin()))) {
			dateFin = dFinExecution();
		}
		if (aReprisTempsPlein() || dFinExecution() != null) {
			// Vérifier aussi si il n'y a pas une reprise à temps plein, elle est alors nécessairement unique car l'interface
			// de saisie ne permet pas autre chose
			NSArray reprises = EORepriseTempsPlein.findForTempsPartiel(editingContext(), this);
			if (reprises.count() > 0) {
				EORepriseTempsPlein reprise = (EORepriseTempsPlein)reprises.get(0);
				dateFin = DateCtrl.jourPrecedent(reprise.dRepriseTempsPlein());
			}
		}
		return dateFin;
	}

	/**  methode de validation appelee par l'automate Cir (pour des donnees saisies avant l'implementation du Cir)
	 * et par la methode validateForSave
	 * @return
	 */
	public String validationsCir() {

		if (surcotisePourRetraite() && DateCtrl.isBefore(dateDebut(), DATE_LIMITE_SURCOTISATION)) {
			return "Un temps partiel avec surcotisation ne peut commencer qu'après le " + DateCtrl.dateToString(DATE_LIMITE_SURCOTISATION);
		}
		// vérifier pour les enfants
		if (estDeclarationEnfantObligatoire() && enfant() == null) {
			return "Pour un temps partiel commençant après le " + DateCtrl.dateToString(ManGUEConstantes.DATE_LIMITE_DECLARATION_ENFANT) + ", vous devez saisir l'enfant concerné !";
		}
		if (enfant() != null) {
			if (motif().estPourEleverEnfant() == false && motif().estPourEleverEnfantAdopte() == false) {
				return "Vous ne devez pas fournir d'enfant pour ce motif !";
			}

			NSArray<EORepartEnfant> reparts = EORepartEnfant.rechercherRepartsPourEnfant(editingContext(), enfant());
			for (EORepartEnfant repart : reparts) {

				// Vérifier si l'enfant choisi est cohérent avec le motif
				if (repart.lienFiliation() != null) {
					if ((repart.lienFiliation().estEnfantLegitime() && motif().estPourEleverEnfant() == false) ||
							(repart.lienFiliation().estAdopte() && motif().estPourEleverEnfantAdopte() == false)) {
						return "Le lien de filiation de l'enfant ne correspond pas au motif choisi";
					}
				}
			}

			// Vérifier si il a moins de 3 ans au début du temps partiel
			NSTimestamp date = enfant().dNaissance();
			if (motif().estPourEleverEnfantAdopte()) {
				date = enfant().dArriveeFoyer();
				if (date == null)
					return "Pour un enfant adopté, la date d'arrivée au foyer est obligatoire";

			}
			NSTimestamp dateAnniversaireTroisAns = DateCtrl.dateAvecAjoutAnnees(date, 3);
			if (DateCtrl.isAfter(dateDebut(), dateAnniversaireTroisAns)) {
				return "L'enfant doit avoir moins de 3 ans ou avoir été adopté depuis moins de 3 ans";
			}
			if (dateFin() != null && DateCtrl.isAfter(dateFin(), dateAnniversaireTroisAns))
				return "L'enfant doit avoir moins de 3 ans ou avoir été adopté pendant toute la durée du temps partiel";

		}
		return null;
	}

	/**
	 * 
	 */
	protected void init() {
		setTemRepriseTempsPlein(CocktailConstantes.FAUX);
		setTemSurcotisation(CocktailConstantes.FAUX);
		setTemConfirme(CocktailConstantes.FAUX);
		setTemGestEtab(CocktailConstantes.FAUX);
		setPeriodicite(new Integer(12));
	}

	/** Recherche des temps partiels d'un individu pendant une periode donnee 
	 * @param individu (peut etre nul)
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 */
	public static NSArray rechercherTempsPartielPourIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@",new NSArray(CocktailConstantes.VRAI)));

		if (individu != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));

		if (debutPeriode !=  null)
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,D_FIN_EXECUTION_KEY,finPeriode));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);

	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void validerPourHandicap() throws NSValidation.ValidationException {
		// vérifier qu'il y a au moins une période de handicap et que l'avis médical est signalé
		NSArray periodesHandicap = Duree.rechercherDureesPourIndividuEtPeriode(editingContext(), "PeriodeHandicap", individu(), dateDebut(), dateFin());
		if (periodesHandicap == null || periodesHandicap.count() == 0) {
			throw new NSValidation.ValidationException("Vous avez choisi " + motif().libelleLong() + ". Il n'y a pas de période de handicap valable pour cette personnependant la période de temps partiel");
		}
		boolean estPossible = true;
		for (java.util.Enumeration<EOPeriodeHandicap> e = periodesHandicap.objectEnumerator();e.hasMoreElements();) {
			EOPeriodeHandicap periode = e.nextElement();
			if (periode.dateAvisMedecin() == null) {
				estPossible = false;
			}
		}
		if (!estPossible) {
			throw new NSValidation.ValidationException("Vous devez saisir la date d'avis du médecin de prévention dans les périodes de handicap pour pouvoir utiliser ce motif");
		}
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void validerPourCarriereEtContrat() throws NSValidation.ValidationException {

		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),individu(),dateDebut(),dateFin());
		NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin(),true);
		NSArray<EOPasse> passesEas = EOPasse.rechercherPassesEASPourPeriode(editingContext(),individu(),dateDebut(),dateFin());
		NSArray<EOContratAvenant> avenants = EOContratAvenant.rechercherAvenantsRemunerationPrincipalePourIndividuEtDates(editingContext(),individu(),dateDebut(),dateFin());
		if (passesEas.size() == 0) {
			if ( (carrieres == null || carrieres.count() == 0) 
					&& (contrats == null || contrats.count() == 0 || avenants == null || avenants.count() == 0)) {
				throw new NSValidation.ValidationException("Il doit y avoir au moins un segment de carrière ou un contrat défini pour cette période");
			}
		}
		if (carrieres != null && carrieres.count() > 0 && contrats != null && contrats.count() > 0) {
			throw new NSValidation.ValidationException("Un temps partiel ne peut être à cheval sur un contrat ET une carrière");
		}

		Integer dureeMaxEns = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_TP_DUREE_MAX_ENS);
		Integer dureeMinEns = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_TP_DUREE_MIN_ENS);

		if (carrieres != null && carrieres.count() > 0) {
			for (EOCarriere myCarriere : carrieres) {

				if (myCarriere.toTypePopulation() != null) {
					if (myCarriere.toTypePopulation().estHospitalier()) {
						throw new NSValidation.ValidationException("Les enseignants hospitalo-universitaires ne peuvent pas bénéficier d'un temps partiel");
					} else if (myCarriere.toTypePopulation().estEnseignant()) {

						if (periodicite() != null) {

							if (periodicite().intValue() < dureeMinEns.intValue()) {
								throw new NSValidation.ValidationException("Pour un temps partiel d'un enseignant, la périodicité est d'au minimum " + dureeMinEns + " mois");
							}
							if (periodicite().intValue() > dureeMaxEns.intValue()) {
								throw new NSValidation.ValidationException("Pour un temps partiel d'un enseignant, la périodicité ne doit pas dépasser " + dureeMaxEns + " mois ! (" + periodicite().intValue() + ")");
							}

						}

					}
				}
			}
			// vérifier si il s'agit d'un comptable car la quotité est >= 80%
			NSArray<EOElementCarriere> elements = EOElementCarriere.findForPeriode(editingContext(), individu(), dateDebut(),dateFin());
			for (EOElementCarriere myElement : elements) {
				if (myElement.toCorps().cCorps().equals(EOCorps.CORPS_POUR_COMPTABLE) && quotite().doubleValue() < 80) {
					throw new NSValidation.ValidationException("Un agent comptable ne peut bénéficier que d'une quotité à 80% ou 90%");
				}
			}
		} else {	// pas de carrière. Pour le motif "Fonctionnaire handicapé", il faut être titulaire
			if (motif() != null && motif().estPourHandicape()) {
				throw new NSValidation.ValidationException("Le motif " + motif().libelleLong() + " ne s'applique qu'aux fonctionnaires. L'agent n'est pas fonctionnaire pendant cette période");
			}
		}
		if (contrats != null && contrats.count() > 0) {
			for (EOContrat myContrat : contrats) {
				if (myContrat.toTypeContratTravail() != null) {
					if (myContrat.toTypeContratTravail().tempsPartielPossible() == false) {
						throw new NSValidation.ValidationException("L'agent a un type de contrat non compatible avec un temps partiel");
					}
					if (myContrat.estTypeHospitalierPourPeriode(dateDebut(),dateFin())) {
						throw new NSValidation.ValidationException("Les enseignants hospitalo-universitaires ne peuvent pas bénéficier d'un temps partiel");
					}
					if (myContrat.toTypeContratTravail().estEnseignant()) {
						Integer dureeMax = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_TP_DUREE_MAX_ENS);
						if (periodicite() != null && periodicite().intValue() != dureeMax.intValue()) {
							throw new NSValidation.ValidationException("Pour un temps partiel d'un enseignant, la périodicité est de " + dureeMax + " mois");
						}
						// Vérifier si cela correspond à une année universitaire
						if (dateFin() == null) {
							throw new NSValidation.ValidationException("Les dates de début et de fin d'un temps partiel pour un enseignant doivent correspondre à des dates de début et de fin d'années universitaires");
						} else {
							if (motif() != null && motif().estPourEleverEnfant() == false && motif().estPourEleverEnfantAdopte() == false) {
								// un temps partiel pour un enseignant (sauf en cas de naissance) doit être
								// calé sur l'année universitaire
								if (DateCtrl.isSameDay(dateDebut(), CocktailUtilities.debutAnneeUniversitaire(dateDebut())) == false ||
										DateCtrl.isSameDay(dateFin(), CocktailUtilities.finAnneeUniversitaire(dateDebut())) == false) {
									throw new NSValidation.ValidationException("Les dates de début et de fin d'un temps partiel pour un enseignant doivent correspondre à des dates de début et de fin d'années universitaires");
								}
							}
						}
					}		
				}
			}
		}

		// verifier que la quotite totale sur la periode est 100%
		if (avenants != null && avenants.count() > 0) {
			int total = 0;
			for (EOContratAvenant myAvenant : avenants) {
				if (myAvenant.quotite() != null)
					total = total + myAvenant.quotite().intValue();
			}
			if (total < 100) {
				throw new NSValidation.ValidationException("Un contractuel recruté à temps incomplet ne peut pas bénéficier d'un temps partiel");
			}
		}
	}

}

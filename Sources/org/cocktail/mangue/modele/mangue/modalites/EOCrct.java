//EOCrct.java
//Created on Wed Feb 22 16:45:05 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.modalites;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation<BR>
 * 1. Le conge de CRCT ne concerne qu'un personnel enseignant-chercheur.<BR>
 * 2. Le conge doit reposer sur une position d'activite ou de detachement avec gestion de la carriere d'accueil dans l'etablissement.<BR>
 * 3. Les enseignants-chercheurs nommes depuis au moins trois ans peuvent beneficier d'un CRCT. 
 * Toutefois une dispense de l'anciennete peut etre accordee, pour les conges demandes au titre de l'etablissement.<BR>
 * On deduit de la duree les durees des positions de hors cadre, service national, disponibilite et conge parental.<BR>
 * 4. Pour les demandes d'un ou deux semestres  de CRCT au titre du CNU, la periodicite entre chaque demande de CRCT est de 6 ans.
 * 5. Si le conge est propose au titre  du Conseil national des universites (CNU), il ne peut etre accorde que pour une periode de six mois ou d'un an.<BR>
 * 6. Si le conge est propose au titre  du Conseil national des universites (CNU), alors la section CNU doit etre renseignee.<BR>
 * 7. Seul le conge propose au titre de l'etablissement peut etre fractionne sur une periode de 6 ans.<BR>
 * 8. Le conge fractionne ou non au titre de l'etablissement est d'une duree maximum de 12 mois par periode de 6 ans.<BR>
 */
public class EOCrct extends _EOCrct {

	public static int ANCIENNETE_MINI = 36;	// mois
	public static int NB_MOIS_MI_TEMPS = 6;

	public EOCrct() {
		super();
	}

	public static EOCrct findForKey( EOEditingContext ec, Number key) {
		return fetchFirstByQualifier(ec, EOQualifier.qualifierWithQualifierFormat(CONGE_ORDRE_KEY + "=%@", new NSArray(key)));
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOCrct creer(EOEditingContext ec, EOIndividu individu) {

		EOCrct newObject = (EOCrct) createAndInsertInstance(ec, EOCrct.ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setEstFractionne(false);
		newObject.setTemConfirme("N");
		newObject.setTemGestEtab("N");
		newObject.setTemValide("O");

		EOTypeAbsence typeAbsence = EOTypeAbsence.findForCode(ec, EOTypeAbsence.TYPE_CRCT);
		EOAbsences absence = EOAbsences.creer(ec, individu, typeAbsence);
		absence.setCTypeExclusion("CRC");

		newObject.setAbsenceRelationship(absence);

		return newObject;
	}

	public String typeEvenement() {
		return EOTypeAbsence.TYPE_CRCT;
	}
	/** utilise pour la communication avec GRhum */
	public String parametreOccupation() {
		return "CL_CRCT";
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray<EOCrct> rechercherPourIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(getQualifierValide(true));
			qualifiers.addObject(getQualifierIndividu(individu));

			if (debutPeriode !=  null) {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
			}

			return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);
		}
		catch (Exception e) {
			return new NSArray<EOCrct>();
		}

	}

	/**
	 * 
	 * @param ec
	 * @param absence
	 * @return
	 */
	public static EOCrct rechercherPourAbsence(EOEditingContext ec, EOAbsences absence) {
		return fetchFirstByQualifier(ec, getQualifierAbsence(absence));
	}


	/**
	 * 
	 * @return
	 */
	public int dureeDetailsEnJours() {
		int dureeTotale = 0;
		for (java.util.Enumeration<EOCrctDetail> e = details().objectEnumerator();e.hasMoreElements();) {
			EOCrctDetail detail = e.nextElement();
			dureeTotale += DateCtrl.nbJoursEntre(detail.dateDebut(),detail.dateFin(),true);
		}
		return dureeTotale;
	}

	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (individu() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_INDIVIDU_NON_RENSEIGNE, "CRCT"));
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "CRCT"));
		} 
		if (dateFin() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_NON_RENSEIGNE, "CRCT"));
		}
		if (dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "CRCT", dateFinFormatee(), dateDebutFormatee()));
		}
		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException(String.format(MangueMessagesErreur.ERREUR_NUMERO_ARRETE, "CRCT", "20"));
		}

		if (noArreteAnnulation() != null && noArreteAnnulation().length() > 20) {
			throw new NSValidation.ValidationException("Un numéro d'arrêté d'annulation comporte au plus 20 caractères");
		}

		boolean estEnActivite = (EOChangementPosition.individuEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin()));
		if (!estEnActivite && !EOContrat.aContratEnCoursSurPeriode(editingContext(),individu(),dateDebut(),dateFin()) )
			throw new NSValidation.ValidationException("Le congé doit reposer sur une position d'activité ou de détachement dans l'établissement (ou sur un contrat)");

		// vérifier qu'il s'agit d'un enseignant chercheur
		if (!individu().peutBeneficierCrctPourPeriodeComplete(dateDebut(),dateFin()))
			throw new NSValidation.ValidationException("Le congé pour recherche et conversion thématique n'est possible que pour les enseignants-chercheurs");

		if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin()) == false)
			throw new NSValidation.ValidationException("Le congé pour recherche et conversion thématique doit reposer sur une position d'activité ou de détachement avec gestion de la carrière d'accueil dans l'établissement");

		if (origineDemande() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir l'origine de la demande");
		}

		// Deduction de la quotite de service 0% si 1 an, 50% si 6 Mois
		int nbMois = DateCtrl.calculerDureeEnMois(dateDebut(),dateFin()).intValue();
		if (nbMois <= NB_MOIS_MI_TEMPS)
			setQuotiteService(new BigDecimal(50));
		else
			setQuotiteService(new BigDecimal(0));

		if (origineDemande().estCnu()) {
			// Si le congé est proposé au titre  du CNU, alors la section CNU doit être renseignée
			if (toCnu() == null) {
				throw new NSValidation.ValidationException("Pour un congé d'origine Cnu, la Cnu doit etre fournie");
			}
			if (estFractionne()) {
				throw new NSValidation.ValidationException("Un congé d'origine Cnu ne peut etre fractionne");
			}
			// Si le congé est proposé au titre  du CNU, il ne peut être accordé que pour une période de six mois ou d'un an
			int dureeMini = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CRCT_DUREE_MIN);
			NSTimestamp dateFinTheorique = DateCtrl.dateAvecAjoutMois(dateDebut(), dureeMini);
			// Si datedebut = 02/01 => dateFinTheorique = 02/07 (on retire donc un jour pour que les bornes soient incluses dans le calcul des durées)
			dateFinTheorique = DateCtrl.jourPrecedent(dateFinTheorique);	

			if (DateCtrl.isSameDay(dateFin(), dateFinTheorique) == false) {
				int dureeMaxi = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CRCT_DUREE_MAX);
				dateFinTheorique = DateCtrl.dateAvecAjoutMois(dateDebut(), dureeMaxi);
				dateFinTheorique = DateCtrl.jourPrecedent(dateFinTheorique);
				if (DateCtrl.isSameDay(dateFin(), dateFinTheorique) == false) {
					throw new NSValidation.ValidationException("Un congé d'origine Cnu ne peut durer qu'un ou deux semestres");
				}
			}
			if (ancienneteOKPourCRCT() == false) {
				throw new NSValidation.ValidationException("L'agent doit avoir au moins 3 ans d'ancienneté comme enseignant-chercheur");
			}
		}
		else {
			nbMois = DateCtrl.calculerDureeEnMois(dateDebut(),dateFin()).intValue();
			int dureeTotale = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CRCT_DUREE_TOTALE) * 12;
			if (estFractionne()) {
				// Seul le congé proposé au titre de l'établissement peut être fractionné sur une période de 6 ans
				int dureeCrct = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CRCT_DUREE_ACTIVITE) * 12;
				if (nbMois > dureeCrct * 12) {
					throw new NSValidation.ValidationException("Un CRCT d'origine Etablissement ne peut être fractionné que sur une période de " + dureeCrct + " ans");
				}
				if (dureeDetailsEnJours() / 30 > dureeTotale) {
					throw new NSValidation.ValidationException("Un CRCT d'origine Etablissement fractionné ne peut durer au total que " + dureeTotale  + " mois. Modifiez les dates des périodes de fractionnement");
				}
			} else {
				if (nbMois > dureeTotale) {
					throw new NSValidation.ValidationException("Un CRCT d'origine Etablissement non fractionné ne peut durer plus de " + dureeTotale + " mois");
				}
			}		
		}

		NSArray<EOCrct> autresCRCT = rechercherPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin());
		for (EOCrct crct : autresCRCT) {
			if (crct != this && crct.estAnnule() == false) {
				throw new NSValidation.ValidationException("Les périodes de CRCT ne peuvent se chevaucher (CRCT du " + crct.dateDebutFormatee() + " au " + crct.dateFinFormatee() + ") !");				
			}
		}

		// La périodicité entre chaque demande de CRCT est de 6 ans
		NSArray autresCrct = Duree.rechercherDureesPourEntiteAnterieuresADate(editingContext(),ENTITY_NAME,individu(),dateDebut());
		if (autresCrct.count() > 0) {
			autresCrct = EOSortOrdering.sortedArrayUsingKeyOrderArray(autresCrct, SORT_ARRAY_DATE_DEBUT_DESC);
			EOCrct crctPrecedent = null;	// On recherche celui qui ne correspond pas au crct en cours en cas de modification
			for (java.util.Enumeration<EOCrct> e = autresCrct.objectEnumerator();e.hasMoreElements();) {
				EOCrct crct = e.nextElement();
				if (crct != this) {
					crctPrecedent = crct;
					break;
				}
			}
			if (crctPrecedent !=  null) {
				verifierDelaiPourCrct(crctPrecedent.dateFin(),dateDebut());
			}
		}

		autresCrct = Duree.rechercherDureesPourEntitePosterieuresADate(editingContext(),ENTITY_NAME,individu(),dateFin());
		if (autresCrct.count() > 0) {
			autresCrct = EOSortOrdering.sortedArrayUsingKeyOrderArray(autresCrct, SORT_ARRAY_DATE_DEBUT);
			EOCrct crctSuivant = null;	// On recherche celui qui ne correspond pas au crct en cours en cas de modification
			for (java.util.Enumeration<EOCrct> e = autresCrct.objectEnumerator();e.hasMoreElements();) {
				EOCrct crct = e.nextElement();
				if (crct != this) {
					crctSuivant = crct;
					break;
				}
			}
			if (crctSuivant !=  null) {
				verifierDelaiPourCrct(dateFin(),crctSuivant.dateDebut());
			}
		}

		absence().setDateDebut(dateDebut());
		absence().setDateFin(dateFin());
		absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true)));

		if (dCreation() == null)	
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(origineDemande(),ORIGINE_DEMANDE_KEY);
		if (toCnu() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toCnu(),TO_CNU_KEY);
		}
		int total = details().count();
		for (int i = 0; i < total;i++) {
			EOCrctDetail detail = (EOCrctDetail)details().objectAtIndex(0);
			detail.supprimerRelations();
			editingContext().deleteObject(detail);
		}
		super.supprimerRelations();
	}
	public boolean estFractionne() {
		return temFractionnement() != null && temFractionnement().equals(CocktailConstantes.VRAI);
	}	
	public boolean estAnnule() {
		return dAnnulation() != null;
	}
	public void setEstFractionne(boolean aBool) {
		setTemFractionnement((aBool)?CocktailConstantes.VRAI:CocktailConstantes.FAUX);
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean ancienneteOKPourCRCT() {
		// Récupérer tous les éléments de carrière de l'individu

		LogManager.logDetail("Anciennete CRCT ");
		NSArray elementsCarriere = EOElementCarriere.findForPeriode(editingContext(), individu(), null, null);
		elementsCarriere = EOSortOrdering.sortedArrayUsingKeyOrderArray(elementsCarriere, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);

		NSTimestamp debutPourCrct = null;
		// On prend comme finPourCrct, la date de début du congé Crct car il faut que l'individu ait déjà accumulé la bonne ancienneté
		NSTimestamp finPourCrct = dateDebut();	
		LogManager.logDetail("Anciennete CRCT - FIN POUR CRCT : " + finPourCrct);

		for (java.util.Enumeration<EOElementCarriere> e = elementsCarriere.objectEnumerator();e.hasMoreElements();) {
			EOElementCarriere element = e.nextElement();
			LogManager.logDetail("Anciennete CRCT DEBUT CRCT : " + debutPourCrct);
			LogManager.logDetail("Anciennete CRCT DEBUT Element : " + element.dateDebut());
			LogManager.logDetail("Anciennete CRCT FIN Element : " + element.dateFin());
			if (DateCtrl.isBeforeEq(element.dateDebut(), dateFin())) {
				if (element.toCorps().beneficieCrct()) {
					// Cet element de carriere est à prendre en compte pour le Crct mais il faut verifier
					// qu'il y a bien continuite des elements de carriere
					if (debutPourCrct == null || (element.dateFin() != null && DateCtrl.isSameDay(DateCtrl.jourPrecedent(debutPourCrct), element.dateFin()))) {
						debutPourCrct = element.dateDebut();
					} else {
						// Il y a rupture dans les elements de carriere
						LogManager.logDetail("Anciennete CRCT Element IS SAME DAY : " + DateCtrl.isSameDay(DateCtrl.jourPrecedent(debutPourCrct), element.dateFin()));
						LogManager.logDetail("Rupture Elements de carriere");
						break;
					}
				} else {
					// Il s'agit d'un element de carriere pour lequel le crct n'est pas accepte
					LogManager.logDetail("Pas de bénéfice CRCT pour le corps");
					break;
				}
			}
		}

		LogManager.logDetail("Debut pour CRCT : " + debutPourCrct);
		LogManager.logDetail("Fin pour CRCT : " + finPourCrct);

		if (debutPourCrct == null) {
			return false;
		} else {	
			int nbMois = DateCtrl.calculerDureeEnMois(debutPourCrct,finPourCrct,true).intValue();
			LogManager.logDetail("Debut pour CRCT - Nb MOIS : " + nbMois);
			// Rechercher tous les changements de position qui sont et retirer la durée de ceux qui sont hors cadre, congé parental, disponibilité et service national
			NSArray changementsPosition = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext(), individu(), debutPourCrct, dateDebut());
			changementsPosition = EOSortOrdering.sortedArrayUsingKeyOrderArray(changementsPosition, EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);
			for (Enumeration<EOChangementPosition> e1 = changementsPosition.objectEnumerator();e1.hasMoreElements();) {
				EOChangementPosition changement = e1.nextElement();
				if (!changement.toPosition().estValidePourCrct()) {
					NSTimestamp debut = changement.dateDebut();
					NSTimestamp fin = changement.dateFin();
					if (DateCtrl.isBefore(debut, debutPourCrct)) {
						debut = debutPourCrct;
					}
					if (fin == null || DateCtrl.isAfter(fin,finPourCrct)) {
						fin = finPourCrct;
					}
					int nbMoisInvalides = DateCtrl.calculerDureeEnMois(debut,fin,true).intValue();
					nbMois = nbMois - nbMoisInvalides;
				}
			}
			return (nbMois >= ANCIENNETE_MINI);
		}
	}
	// méthodes protégées
	protected void init() {
		super.init();
		setTemFractionnement(CocktailConstantes.FAUX);
	}

	/**
	 * 
	 * @param dateDebut
	 * @param dateFin
	 * @throws NSValidation.ValidationException
	 */
	private void verifierDelaiPourCrct(NSTimestamp dateDebut,NSTimestamp dateFin) throws NSValidation.ValidationException {

		int nbMois = DateCtrl.calculerDureeEnMois(dateDebut,dateFin).intValue();
		int dureeEntreCrct = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CRCT_PERIODICITE);

		if (nbMois < dureeEntreCrct * 12) {
			throw new NSValidation.ValidationException("La périodicité entre chaque demande de CRCT est de " + dureeEntreCrct + " ans.");
		}
	}

}

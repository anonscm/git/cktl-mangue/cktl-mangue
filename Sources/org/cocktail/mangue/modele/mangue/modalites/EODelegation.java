//EODelegation.java
//Created on Tue Mar 11 10:14:15  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.modalites;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation :<BR>
 * 	Le mode de delegation et la date debut doivent etre fournis<BR>
 *	Si la date de fin est fournie, celle de debut doit etre anterieure<BR> 
 *	Verifie la compatibilite avec les conges et les modalites de service <BR>
 *	Verifie que l'individu peut beneficier de la delegation (enseignant chercheur ou personnel hospitalo-universitaire).<BR>
 *	Verifie que l'occupation de l'emploi est fermee pour un personnel hospitalo-universitaire<BR>
 *  Verifie qu'il n'y a pas de chevauchement des delegations
 * @author christine
 */

public class EODelegation extends _EODelegation {

	private static final String TYPE_DELEGATION = "DELEGATION";
	public EODelegation() {
		super();
	}

	public static EODelegation findForKey( EOEditingContext ec, Number key) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONGE_ORDRE_KEY + "=%@", new NSArray(key)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}
	
	public static EODelegation creer(EOEditingContext ec, EOIndividu individu) {

		EODelegation newObject = (EODelegation) createAndInsertInstance(ec, EODelegation.ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setTemValide(CocktailConstantes.VRAI);
		newObject.setQuotite(new Integer(100));

		EOTypeAbsence typeAbsence = EOTypeAbsence.findForCode(ec, EOTypeAbsence.TYPE_DELEGATION);
		EOAbsences absence = EOAbsences.creer(ec, individu, typeAbsence);
		absence.setCTypeExclusion("DEL");

		newObject.setAbsenceRelationship(absence);

		return newObject;
	}

	public static EODelegation findForAbsence(EOEditingContext ec, EOAbsences absence) {
		return fetchFirstByQualifier(ec, EOQualifier.qualifierWithQualifierFormat(ABSENCE_KEY + "=%@", new NSArray(absence)));
	}


	public String typeEvenement() {
		return TYPE_DELEGATION;
	}
	public void validateForSave() throws NSValidation.ValidationException {

		if (temValide().equals(CocktailConstantes.FAUX))
			return;

		super.validateForSave();

		NSArray elements = EOElementCarriere.findForPeriode(editingContext(),individu(),dateDebut(),dateFin());
		NSArray contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin(),false);
		if ((elements == null || elements.count() == 0) && (contrats == null || contrats.count() == 0)) {
			throw new NSValidation.ValidationException("Il doit y avoir au moins un élément de carrière ou un contrat défini sur cette période");
		} 
		boolean peutBeneficierDelegation = true;
		boolean estHospitalier = false;

		if (elements != null && elements.count() > 0) {
			for (java.util.Enumeration<EOElementCarriere> e = elements.objectEnumerator();e.hasMoreElements();) {
				EOElementCarriere element = e.nextElement();
				if (element.toCorps() != null) {
					if (element.toCorps().beneficieDelegation() == false) {
						peutBeneficierDelegation = false;
						break;
					} else {
						estHospitalier = element.toCorps().toTypePopulation().estHospitalier();
					}
				}
			}
		}
		// si la carrière le permet, on ne vérifie pas dans les contrats
		if (!peutBeneficierDelegation && contrats != null && contrats.count() > 0) {
			peutBeneficierDelegation = true;	// pour voir si un contrat l'interdit
			for (java.util.Enumeration<EOContrat> e = contrats.objectEnumerator();e.hasMoreElements();) {
				EOContrat contrat = e.nextElement();
				if (contrat.toTypeContratTravail().beneficieDelegation() == false) {
					peutBeneficierDelegation = false;
					break;
				} else {
					estHospitalier = contrat.estTypeHospitalierPourPeriode(dateDebut(),dateFin());
				}
			}
		}
		if (!peutBeneficierDelegation) {
			throw new NSValidation.ValidationException("Les délégations ne sont autorisées que pour les enseignants chercheurs, enseignants du second degré ou personnel hospitalo-universitaire !");
		}

		// 07/06/2010 - Validation de la durée de délégation
		if (lieuDelegation() != null && lieuDelegation().length() > 200) {
			throw new NSValidation.ValidationException("Le lieu de délégation ne peut dépasser 200 caractères");
		}
		if (toTypeModDelegation() == null) {
			throw new NSValidation.ValidationException("Vous devez indiquer le mode de délégation !");
		}
		if (motifDelegation() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le motif de délégation !");
		}

		//		// vérifier si il s'agit d'un hospitalo universitaire
		//		if (estHospitalier) {
		//			if (EOOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin()).count() > 0) {
		//				throw new NSValidation.ValidationException("Pour le personnel hospitalo-universitaire, les occupations doivent être fermées !");
		//			}
		//		}


		// 07/06/2010 - Validation de la durée de délégation
		if (motifDelegation().dureeMaxiDelegation() != null) {
			int nbAnnees = motifDelegation().dureeMaxiDelegation().intValue();
			NSTimestamp dateApresAnnee = DateCtrl.dateAvecAjoutAnnees(dateDebut(), nbAnnees);
			if (dateFin() == null || DateCtrl.isAfterEq(dateFin(), dateApresAnnee)) {
				throw new NSValidation.ValidationException("La durée maximum de délégation pour ce motif est " + nbAnnees + " années !");

			}
		}
		if (absence() != null) {
			absence().setDateDebut(dateDebut());
			absence().setDateFin(dateFin());
			absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true)));
		}

		if (dCreation() == null)	
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	// méthodes protégées
	protected void init() {
		setQuotite(new Integer(100));
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		removeObjectFromBothSidesOfRelationshipWithKey(toTypeModDelegation(),TO_TYPE_MOD_DELEGATION_KEY);
		removeObjectFromBothSidesOfRelationshipWithKey(motifDelegation(),MOTIF_DELEGATION_KEY);
		if (toRne() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toRne(),TO_RNE_KEY);
		}
	}

	public boolean estValide() {
		return temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}


	public boolean estMontantAnnuel() {
		return temAnnuelDelegation() != null && temAnnuelDelegation().equals(CocktailConstantes.VRAI);
	}
	public void setEstMontantAnnuel(boolean aBool) {
		if (aBool) {
			setTemAnnuelDelegation(CocktailConstantes.VRAI);
		} else {
			setTemAnnuelDelegation(CocktailConstantes.FAUX);
		}
	}
	public String dateConventionFormatee() {
		return SuperFinder.dateFormatee(this,D_CONV_DELEGATION_KEY);
	}
	public void setDateConventionFormatee(String uneDate) {
		if (uneDate == null) {
			setDConvDelegation(null);
		} else {
			SuperFinder.setDateFormatee(this,D_CONV_DELEGATION_KEY,uneDate);
		}
	}
	public void dupliquerAvec(EODelegation delegation,NSTimestamp dateDebut,NSTimestamp dateFin) {
		setLieuDelegation(delegation.lieuDelegation());
		setQuotite(delegation.quotite());
		setDConvDelegation(delegation.dConvDelegation());
		setMontantDelegation(delegation.montantDelegation());
		setEstMontantAnnuel(delegation.estMontantAnnuel());

		setIndividuRelationship(delegation.individu());

		if (delegation.toTypeModDelegation() != null) {
			setToTypeModDelegationRelationship(delegation.toTypeModDelegation());
		}
		if (delegation.motifDelegation() != null) {
			setMotifDelegationRelationship(delegation.motifDelegation());
		}
		if (delegation.toRne() != null) {
			setToRneRelationship(delegation.toRne());
		}
		setDateDebut(dateDebut);
		setDateFin(dateFin);
		setNoArrete(null);
		setDateArrete(null);
		setCommentaire(delegation.commentaire());
	}

}

//EOMiTpsTherap.java
//Created on Wed Nov 30 14:18:51 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.modalites;

import java.util.Enumeration;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.Conge;
import org.cocktail.mangue.modele.mangue.conges.EOCld;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation : les temps partiels therapeuthiques existent depuis le 07/02/2007<BR>
 * 	La date debut doit etre fournie<BR>
 *	Si la date de fin est fournie, celle de debut doit etre anterieure<BR> 
 *	Verifie la compatibilite avec les conges et les modalites de service <BR>
 *	Verifie que l'individu peut beneficier d'un temps partiel therapeuthique 
 *	(fonctionnaire en activite ou detachement dans l'etablissement) ou contractuel
 * 	et que si c'est un fonctionnaire, il est en position d'activite ou de detachement vers l'etablissement pendant cette periode.<BR>
 * Une periode de temps partiel therapeuthique doit suivre un conge de longue maladie ou un conge de longue duree ou
 * un conge de maladie d'au-moins 6 mois  
 * ou un conge pour accident de service ou de travail ou une autre p&eacute;riode de temps partiel therapeuthique.<BR>
 * La date d'avis du comite medical superieur doit &ecirc;tre posterieure ou egale a celle du comite medical local.<BR>
 * La duree d'une periode de temps partiel therapeuthique associe a un C.L.D. ou un C.L.M ne doit pas depasser 3 mois.<BR>
 * La duree d'une periode de temps partiel therapeuthique associee e un accident de service/travail ne doit pas depasser 6 mois.<BR>
 * La duree d'une suite continue de temps partiel therapeuthiques ne peut pas depasser 12 mois.<BR>
 * @author christine
 */
// 30/06/2010 - ajout d'une méthode permettant de déterminer le congé qui a permis l'ajout d'un MTT
public class EOMiTpsTherap extends _EOMiTpsTherap {

	private static final String DATE_DEBUT_PEC_TPT = "07/02/2007";
	private static final int DUREE_MINI_CONGE_MALADIE = 6;
	private static final int DUREE_MINI_CONGE_MALADIE_NT = 0;
	public EOMiTpsTherap() {
		super();
	}

	public static EOMiTpsTherap findForKey( EOEditingContext ec, Number key) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_SEQ_ARRETE_KEY + "=%@", new NSArray(key)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));

	}
	public static EOMiTpsTherap creer(EOEditingContext ec, EOIndividu individu) {

		EOMiTpsTherap newObject = (EOMiTpsTherap) createAndInsertInstance(ec, EOMiTpsTherap.ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);

		newObject.setTemConfirme(CocktailConstantes.FAUX);
		newObject.setTemGestEtab(CocktailConstantes.FAUX);
		newObject.setTemValide(CocktailConstantes.VRAI);

		EOTypeAbsence typeAbsence = EOTypeAbsence.findForCode(ec, EOTypeAbsence.TYPE_TEMPS_PARTIEL_THERAP);
		EOAbsences absence = EOAbsences.creer(ec, individu, typeAbsence);		
		newObject.setAbsenceRelationship(absence);

		return newObject;
	}


	public String typeEvenement() {
		return "MTT";
	}
	public String dateComMedFormatee() {
		return SuperFinder.dateFormatee(this,D_COM_MED_MTTH_KEY);
	}
	public void setDateComMedFormatee(String uneDate) {
		if (uneDate == null) {
			setDComMedMtth(null);
		} else {
			SuperFinder.setDateFormatee(this,D_COM_MED_MTTH_KEY,uneDate);
		}
	}
	public String dateComMedSupFormatee() {
		return SuperFinder.dateFormatee(this,D_COM_MED_SUP_MTTH_KEY);
	}
	public void setDateComMedSupFormatee(String uneDate) {
		if (uneDate == null) {
			setDComMedSupMtth(null);
		} else {
			SuperFinder.setDateFormatee(this,D_COM_MED_SUP_MTTH_KEY,uneDate);
		}
	}
	public String dateComReformeFormatee() {
		return SuperFinder.dateFormatee(this,D_COM_REFORME_KEY);
	}
	public void setDateComReformeFormatee(String uneDate) {
		if (uneDate == null) {
			setDComReforme(null);
		} else {
			SuperFinder.setDateFormatee(this,D_COM_REFORME_KEY,uneDate);
		}
	}
	public boolean estSigne() {
		return temConfirme() != null && temConfirme().equals(CocktailConstantes.VRAI);
	}
	public void setEstSigne(boolean aBool) {
		if (aBool) {
			setTemConfirme(CocktailConstantes.VRAI);
		} else {
			setTemConfirme(CocktailConstantes.FAUX);
		}
	}
	/** retourne true si il y a un arrete d'annulation */
	public boolean estAnnule() {
		return dAnnulation() != null || noArreteAnnulation() != null;
	}

	/** retourne true si l'arrete est gere par l'etablissement */
	public boolean estGereParEtablissement() {
		return temGestEtab() != null && temGestEtab().equals(CocktailConstantes.VRAI);
	}

	/**
	 * 
	 */
	public NSArray visas() {

		NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),individu(),dateDebut(),dateFin());

		if (carrieres == null || carrieres.count() == 0) {
			NSArray contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin(),false);
			if (contrats.count() > 0) {
				EOContrat contrat = (EOContrat)contrats.objectAtIndex(0); 
				NSArray visas = EOVisa.rechercherVisaPourTypeCongeEtTypeContrat(editingContext(), EOTypeAbsence.TYPE_TEMPS_PARTIEL_THERAP, contrat.toTypeContratTravail().code());
				return visas;
			}
		} else {		
			EOCarriere carriere = (EOCarriere)carrieres.objectAtIndex(0); 
			return EOVisa.rechercherVisaPourTableCongeEtTypePopulation(editingContext(),ENTITY_NAME, carriere.toTypePopulation().code());
		}

		return new NSArray();

	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (individu() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_INDIVIDU_NON_RENSEIGNE, "TPT"));
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "TPT"));
		} 
		if (dateFin() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_NON_RENSEIGNE, "TPT"));
		}
		if (dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "TPT", dateDebutFormatee(), dateFinFormatee()));
		}
		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException(String.format(MangueMessagesErreur.ERREUR_NUMERO_ARRETE, "TPT", "20"));
		}		
		if (noArreteAnnulation() != null && noArreteAnnulation().length() > 20) {
			throw new NSValidation.ValidationException(String.format(MangueMessagesErreur.ERREUR_NUMERO_ARRETE, "TPT", "20"));
		}

		if (quotite() != null) {
			if (DateCtrl.isBefore(dateDebut(),DateCtrl.stringToDate(DATE_DEBUT_PEC_TPT)) && !quotite().equals("50%")) {
				throw new NSValidation.ValidationException("Un temps partiel thérapeutique ne peut commencer qu'après le 07/02/2007 (loi n° 2007-148)");  
			}
		}
		boolean isTitulaire = false;
		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),individu(),dateDebut(),dateFin());
		// vérifie si fonctionnaire sur période
		if (carrieres != null && carrieres.count() > 0) {

			for (java.util.Enumeration<EOCarriere> e = carrieres.objectEnumerator();e.hasMoreElements();) {
				EOCarriere carriere = e.nextElement();
				if (carriere.toTypePopulation().estFonctionnaire()) {
					isTitulaire = true;
					break;
				}
			}
			// vérifier que l'agent est en position d'activité ou de détachement vers l'établissement
			NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin());
			if (changements == null || changements.count() == 0) {
				throw new NSValidation.ValidationException("Un temps partiel thérapeutique doit reposer sur une position d'activité, de détachement avec carrière d'accueil ou de CLD");
			} else {
				for (Enumeration<EOChangementPosition> e1 = changements.objectEnumerator();e1.hasMoreElements();) {
					EOChangementPosition changement = e1.nextElement();
					// La période de mtt doit reposer sur une position d'activité ou de détachement avec carrière d'accueil ou sur un CLDS
					if (!changement.toPosition().estEnActiviteDansEtablissement() && (!changement.toPosition().estUnDetachement() || (changement.estDetachementSortant()))) {
						//if (changement.toPosition().estCld() == false) {
						throw new NSValidation.ValidationException("Un temps partiel thérapeutique doit reposer sur une position d'activité, de détachement avec carrière d'accueil ou de CLD");
						//}
					}
				}
			}
		} else {
			// Vérifier si contractuel sur la période
			NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(), individu(), dateDebut(),dateFin(),false);
			if (contrats == null || contrats.count() == 0) {
				throw new NSValidation.ValidationException("Un temps partiel thérapeutique doit reposer sur une carrière ou un contrat");
			}
		}
		if (dComMedMtth() != null && dComMedSupMtth() != null && DateCtrl.isBefore(dComMedSupMtth(),dComMedMtth())) {
			throw new NSValidation.ValidationException("La date d'avis du comité médical supérieur doit être postérieure ou égale à celle du comité médical local.");
		}

		if (isTitulaire) {
			NSMutableArray conges = congesPourMTT(isTitulaire);
			NSArray miTempsTheraps = Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(), ENTITY_NAME, individu(),dateDebut());
			conges.addObjectsFromArray(miTempsTheraps);
			if (conges.count() == 0) {
				throw new NSValidation.ValidationException("Aucun congé (Maladie, CLD, Accident service/Travail) n'a été trouvé pour cet agent !");
			}


			Duree congeTrouve = congePourMTT(conges, true);
			if (congeTrouve == null) {
				throw new NSValidation.ValidationException("Une période de temps partiel thérapeuthique doit suivre un congé de maladie d'au moins 6 mois ou un congé de longue maladie ou un congé de longue durée ou un congé pour accident de service/travail ou une autre période de temps partiel thérapeuthique");
			}

			if (EOGrhumParametres.isCtrlTptMal()) {
				if (congeTrouve.typeEvenement().equals(EOTypeAbsence.TYPE_CONGE_MALADIE)) {
					if (dureeInvalide(conges, congeTrouve, congeTrouve.typeEvenement())) {
						throw new NSValidation.ValidationException("Une période de temps partiel thérapeuthique (Titulaire) doit suivre un congé de maladie d'au moins 6 mois !");
					}
				}
				if (congeTrouve.typeEvenement().equals(EOTypeAbsence.TYPE_CONGE_MALADIE_NT)) {
					if (dureeInvalide(conges, congeTrouve, congeTrouve.typeEvenement())) {
						throw new NSValidation.ValidationException("Une période de temps partiel thérapeuthique (Non Titulaire) doit suivre un congé de maladie.");
					}
				}
			}

			int nbMois = DateCtrl.calculerDureeEnMois(dateDebut(),dateFin()).intValue();
			if (congeTrouve.typeEvenement().equals(EOTypeAbsence.TYPE_CLD) 
					|| congeTrouve.typeEvenement().equals(EOTypeAbsence.TYPE_CLM) 
					|| congeTrouve.typeEvenement().equals(EOTypeAbsence.TYPE_CONGE_MALADIE_CGM)) {
				int nbMoisMaxi = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_TPT_DIREE_CLD_CLM);
				if (nbMois > nbMoisMaxi) {
					throw new NSValidation.ValidationException("La durée d'une période de temps partiel thérapeuthique associée à un C.L.D. ou un C.L.M/C.G.M ne doit pas dépasser " + nbMoisMaxi + " mois.");
				} 
			} else if (congeTrouve.typeEvenement().equals("ACCSERV") || congeTrouve.typeEvenement().equals("ACCTRAV")) {
				int nbMoisMaxi = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_TPT_DUREE_ACC_SERV);
				if (nbMois > nbMoisMaxi) {
					throw new NSValidation.ValidationException("La durée d'une période de temps partiel thérapeuthique associée à un accident de service/travail ne doit pas dépasser 6 mois.");
				}
			} else {	// temps partiel thérapeutique
				miTempsTheraps = EOSortOrdering.sortedArrayUsingKeyOrderArray(miTempsTheraps,new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareDescending)));
				int total = 0;
				NSTimestamp dateToCompare = dateDebut();

				for (java.util.Enumeration<Duree> e = miTempsTheraps.objectEnumerator();e.hasMoreElements();) {
					Duree conge = e.nextElement();
					if (conge.dateFin() != null && DateCtrl.isSameDay(DateCtrl.jourSuivant(conge.dateFin()),dateToCompare)) {
						total = total + DateCtrl.calculerDureeEnMois(conge.dateDebut(),conge.dateFin()).intValue();
						dateToCompare = conge.dateDebut();
					} else if (conge != this) {
						// on compare par date descendante, il n'y a plus de candidat
						break;
					}
				}
				if (total > 12) {
					throw new NSValidation.ValidationException("La durée d'une suite continue de temps partiels thérapeuthiques ne peut pas dépasser 12 mois.");
				}
			} 
		}

		if (absence() != null) {
			absence().setDateDebut(dateDebut());
			absence().setDateFin(dateFin());
			absence().setAbsDureeTotale(String.valueOf(DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true)));
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	/** Recherche des Mi-temps_therapeutiques d'un individu pendant une periode donnee 
	 * @param individu (peut etre nul)
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 */
	public static NSArray rechercherPourIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@",new NSArray(CocktailConstantes.VRAI)));

		if (individu != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));

		if (debutPeriode !=  null)
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);

	}



	/** Retourne le conge associe a un MTT, il y en a forcement un sinon le MTT n'aurait pu etre defini */
	public Duree congePourMTT(boolean estFonctionnaire, boolean prendreEnCompteMTT) {
		return congePourMTT(congesPourMTTAvecMTT(estFonctionnaire), prendreEnCompteMTT);

	}
	// méthodes protégées
	protected void init() {
		setTemConfirme(CocktailConstantes.FAUX);
		setTemGestEtab(CocktailConstantes.FAUX);
	}
	// Méthodes privées
	private NSArray congesPourMTTAvecMTT(boolean estFonctionnaire) {
		// Une période de temps partiel thérapeuthique doit suivre un congé de longue maladie ou un congé de longue durée 
		// ou un congé pour accident de service/travail ou une autre période de temps partiel thérapeuthique.
		NSMutableArray conges = congesPourMTT(estFonctionnaire);
		NSArray miTempsTheraps = Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(),ENTITY_NAME,individu(),dateDebut());
		conges.addObjectsFromArray(miTempsTheraps);
		return conges;
	}
	private NSMutableArray congesPourMTT(boolean estFonctionnaire) {
		// Une période de temps partiel thérapeuthique doit suivre un congé de longue maladie ou un congé de longue durée 
		// ou un congé pour accident de service/travail
		NSMutableArray conges = new NSMutableArray();
		if (estFonctionnaire) {
			conges.addObjectsFromArray(Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(),EOCld.ENTITY_NAME,individu(),dateDebut()));
			conges.addObjectsFromArray(Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(), EOClm.ENTITY_NAME,individu(),dateDebut()));
			conges.addObjectsFromArray(Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(),"CongeAccidentServ",individu(),dateDebut()));
			conges.addObjectsFromArray(Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(),"CongeMaladie",individu(),dateDebut()));

		} else {
			conges.addObjectsFromArray(Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(),"CgntCgm",individu(),dateDebut()));
			conges.addObjectsFromArray(Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(),"CgntAccidentTrav",individu(),dateDebut()));
			conges.addObjectsFromArray(Duree.rechercherDureesPourIndividuAnterieuresADate(editingContext(),"CgntMaladie",individu(),dateDebut()));
		}

		return conges;
	}

	private Duree congePourMTT(NSArray conges,boolean prendreEnCompteMTT) {
		Duree congeTrouve = null;
		NSTimestamp dateDebut = dateDebut(); 		
		// Dans le cas où on ne prend pas en compte les MTT, il faut pouvoir modifier la date pour comparaison
		// Trier les congés par ordre de date début décroissante pour trouvé quel est le dernier congé concerné
		conges = EOSortOrdering.sortedArrayUsingKeyOrderArray(conges, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareDescending)));
		for (java.util.Enumeration<Duree> e = conges.objectEnumerator();e.hasMoreElements();) {
			Duree conge = e.nextElement();
			if (conge.dateFin() != null && DateCtrl.isSameDay(DateCtrl.jourSuivant(conge.dateFin()),dateDebut)) {
				if (conge instanceof EOMiTpsTherap) {
					dateDebut = conge.dateDebut();
				}
				if (prendreEnCompteMTT || conge instanceof EOMiTpsTherap == false) {
					congeTrouve = conge;
					break;
				}
			}
		}
		return congeTrouve;
	}



	/**
	 * 
	 * @param conges
	 * @param congeTrouve
	 * @return
	 */
	private boolean dureeInvalide(NSArray<Conge> conges, Duree congeTrouve, String typeConge) {

		NSArray<Conge> congesTries = EOSortOrdering.sortedArrayUsingKeyOrderArray(conges, new NSArray(EOSortOrdering.sortOrderingWithKey(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending)));

		NSTimestamp dateDebut = congeTrouve.dateDebut(),dateFin = congeTrouve.dateFin();
		for (Conge myConge : congesTries) {

			if (DateCtrl.isBefore(myConge.dateDebut(),dateDebut)) {
				if (myConge.typeEvenement().equals(EOTypeAbsence.TYPE_CONGE_MALADIE) == false 
						&& myConge.typeEvenement().equals(EOTypeAbsence.TYPE_CONGE_MALADIE_NT) == false) {
					break;
				} else {
					if (myConge.dateFin() == null /* ne devrait pas se produire */
							|| ( myConge.dateFin() != null && DateCtrl.isSameDay(DateCtrl.jourSuivant(myConge.dateFin()),dateDebut))) {			
						dateDebut = myConge.dateDebut();
					} else {
						break;		// Discontinuité des congés
					}
				}
			}
		}

		Integer dureeEnMois = DateCtrl.calculerDureeEnMois(dateDebut, dateFin,true);

		if (typeConge.equals(EOTypeAbsence.TYPE_CONGE_MALADIE))
			return dureeEnMois == null || dureeEnMois.intValue() < DUREE_MINI_CONGE_MALADIE;

		if (typeConge.equals(EOTypeAbsence.TYPE_CONGE_MALADIE_NT))
			return dureeEnMois == null || dureeEnMois.intValue() < DUREE_MINI_CONGE_MALADIE_NT;

		return true;
	}
}
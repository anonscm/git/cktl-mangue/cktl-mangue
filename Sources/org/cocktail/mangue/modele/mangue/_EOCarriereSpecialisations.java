// _EOCarriereSpecialisations.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCarriereSpecialisations.java instead.
package org.cocktail.mangue.modele.mangue;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOCarriereSpecialisations extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "CarriereSpecialisations";
	public static final String ENTITY_TABLE_NAME = "MANGUE.CARRIERE_SPECIALISATIONS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "specId";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String SPEC_DEBUT_KEY = "specDebut";
	public static final String SPEC_FIN_KEY = "specFin";

// Attributs non visibles
	public static final String C_CNECA_KEY = "cCneca";
	public static final String C_DISC_SD_DEGRE_KEY = "cDiscSdDegre";
	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String C_SPECIALITE_KEY = "cSpecialite";
	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String SPEC_ID_KEY = "specId";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String SPEC_DEBUT_COLKEY = "SPEC_DEBUT";
	public static final String SPEC_FIN_COLKEY = "SPEC_FIN";

	public static final String C_CNECA_COLKEY = "C_CNECA";
	public static final String C_DISC_SD_DEGRE_COLKEY = "C_DISC_SD_DEGRE";
	public static final String CODEEMPLOI_COLKEY = "CODEEMPLOI";
	public static final String C_SPECIALITE_COLKEY = "C_SPECIALITE_ITARF";
	public static final String C_SPECIALITE_ATOS_COLKEY = "C_SPECIALITE_ATOS";
	public static final String NO_CNU_COLKEY = "NO_CNU";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String NO_SEQ_CARRIERE_COLKEY = "NO_SEQ_CARRIERE";
	public static final String SPEC_ID_COLKEY = "SPEC_ID";


	// Relationships
	public static final String TO_BAP_KEY = "toBap";
	public static final String TO_CARRIERE_KEY = "toCarriere";
	public static final String TO_CNECA_KEY = "toCneca";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_DISC_SECOND_DEGRE_KEY = "toDiscSecondDegre";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_REFERENS_EMPLOI_KEY = "toReferensEmploi";
	public static final String TO_SPECIALITE_ATOS_KEY = "toSpecialiteAtos";
	public static final String TO_SPECIALITE_ITARF_KEY = "toSpecialiteItarf";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public NSTimestamp specDebut() {
    return (NSTimestamp) storedValueForKey(SPEC_DEBUT_KEY);
  }

  public void setSpecDebut(NSTimestamp value) {
    takeStoredValueForKey(value, SPEC_DEBUT_KEY);
  }

  public NSTimestamp specFin() {
    return (NSTimestamp) storedValueForKey(SPEC_FIN_KEY);
  }

  public void setSpecFin(NSTimestamp value) {
    takeStoredValueForKey(value, SPEC_FIN_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap toBap() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap)storedValueForKey(TO_BAP_KEY);
  }

  public void setToBapRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap oldValue = toBap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BAP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BAP_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOCarriere toCarriere() {
    return (org.cocktail.mangue.modele.mangue.individu.EOCarriere)storedValueForKey(TO_CARRIERE_KEY);
  }

  public void setToCarriereRelationship(org.cocktail.mangue.modele.mangue.individu.EOCarriere value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CARRIERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CARRIERE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSectionsCneca toCneca() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSectionsCneca)storedValueForKey(TO_CNECA_KEY);
  }

  public void setToCnecaRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSectionsCneca value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSectionsCneca oldValue = toCneca();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CNECA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CNECA_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu toCnu() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu)storedValueForKey(TO_CNU_KEY);
  }

  public void setToCnuRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CNU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CNU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre toDiscSecondDegre() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre)storedValueForKey(TO_DISC_SECOND_DEGRE_KEY);
  }

  public void setToDiscSecondDegreRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre oldValue = toDiscSecondDegre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DISC_SECOND_DEGRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DISC_SECOND_DEGRE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois toReferensEmploi() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois)storedValueForKey(TO_REFERENS_EMPLOI_KEY);
  }

  public void setToReferensEmploiRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois oldValue = toReferensEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_EMPLOI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_EMPLOI_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos toSpecialiteAtos() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos)storedValueForKey(TO_SPECIALITE_ATOS_KEY);
  }

  public void setToSpecialiteAtosRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos oldValue = toSpecialiteAtos();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SPECIALITE_ATOS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SPECIALITE_ATOS_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf toSpecialiteItarf() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf)storedValueForKey(TO_SPECIALITE_ITARF_KEY);
  }

  public void setToSpecialiteItarfRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf oldValue = toSpecialiteItarf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SPECIALITE_ITARF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SPECIALITE_ITARF_KEY);
    }
  }
  

/**
 * Créer une instance de EOCarriereSpecialisations avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCarriereSpecialisations createEOCarriereSpecialisations(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp specDebut
, org.cocktail.mangue.modele.mangue.individu.EOCarriere toCarriere, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu			) {
    EOCarriereSpecialisations eo = (EOCarriereSpecialisations) createAndInsertInstance(editingContext, _EOCarriereSpecialisations.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setSpecDebut(specDebut);
    eo.setToCarriereRelationship(toCarriere);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  
	  public EOCarriereSpecialisations localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCarriereSpecialisations)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCarriereSpecialisations creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCarriereSpecialisations creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCarriereSpecialisations object = (EOCarriereSpecialisations)createAndInsertInstance(editingContext, _EOCarriereSpecialisations.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCarriereSpecialisations localInstanceIn(EOEditingContext editingContext, EOCarriereSpecialisations eo) {
    EOCarriereSpecialisations localInstance = (eo == null) ? null : (EOCarriereSpecialisations)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCarriereSpecialisations#localInstanceIn a la place.
   */
	public static EOCarriereSpecialisations localInstanceOf(EOEditingContext editingContext, EOCarriereSpecialisations eo) {
		return EOCarriereSpecialisations.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCarriereSpecialisations fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCarriereSpecialisations fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCarriereSpecialisations eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCarriereSpecialisations)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCarriereSpecialisations fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCarriereSpecialisations fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCarriereSpecialisations eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCarriereSpecialisations)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCarriereSpecialisations fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCarriereSpecialisations eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCarriereSpecialisations ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCarriereSpecialisations fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

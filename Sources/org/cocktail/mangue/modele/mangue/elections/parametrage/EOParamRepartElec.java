//EOParamRepartElec.java
//Created on Wed Feb 21 12:11:59 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.elections.parametrage;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.modele.ParametrageElection;
import org.cocktail.mangue.modele.grhum.elections.EOBureauVote;
import org.cocktail.mangue.modele.grhum.elections.EOComposanteElective;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementBv;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementComposante;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementSecteur;
import org.cocktail.mangue.modele.grhum.elections.EOSecteur;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EOParamRepartElec extends _EOParamRepartElec {

	public EOParamRepartElec() {
		super();
	}

	public void init(EOInstance instance,EOComposanteElective composante,EOStructure structure,EOBureauVote bureauVote) {
		addObjectToBothSidesOfRelationshipWithKey(instance,"instance");
		addObjectToBothSidesOfRelationshipWithKey(composante,"composanteElective");
		addObjectToBothSidesOfRelationshipWithKey(structure,"structure");
		if (bureauVote != null) {
			addObjectToBothSidesOfRelationshipWithKey(bureauVote, "bureauVote");
		}
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(instance(),"instance");
		removeObjectFromBothSidesOfRelationshipWithKey(composanteElective(),"composanteElective");
		removeObjectFromBothSidesOfRelationshipWithKey(structure(),"structure");
	}
	// Méthodes statiques
	public static NSArray<EOParamRepartElec> rechercherParametragesPourInstance(EOEditingContext editingContext,EOInstance instance) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INSTANCE_KEY + " = %@", new NSArray(instance));
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("structure.llStructure", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("composanteElective.llComposanteElective", EOSortOrdering.CompareAscending));
		
		return fetchAll(editingContext, qualifier, sorts);

	}
	public static NSArray<EOParamRepartElec> rechercherParametragesPourBureauVote(EOEditingContext editingContext,EOBureauVote bureauVote) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("bureauVote = %@", new NSArray(bureauVote));
		return fetchAll(editingContext, qualifier, null);
	}
	public static NSArray<EOParamRepartElec> rechercherParametragesPourComposante(EOEditingContext editingContext,EOComposanteElective composanteElective) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("composanteElective = %@", new NSArray(composanteElective));
		EOFetchSpecification fs = new EOFetchSpecification("ParamRepartElec",qualifier,null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	// Méthodes statiques
	/** prepare les rubriques de parametrage en aggregeant toutes les informations de parametrage<BR>
	 * Detruit auparavant les parametrages et les listes d'electeurs pour cette instance 
	 * 
	 * @param instance instance concernee
	 */
	public static void preparerParametragesPourInstance(EOInstance instance) throws Exception {
		
		try {
			// Commencer par détruire les paramétrages existants
			instance.supprimerParametrageFinal();
			instance.supprimerElecteursPourInstance();	
			// Commencer par déterminer toutes les composantes électives pour le paramétrage de l'instance et vérifier
			//  que toutes les composantes électives attachées aux secteurs (dans le cas d'une élection sectorisée)
			// sont bien présentes
			LogManager.logDetail("Preparation parametrage pour instance " + instance.llInstance());
			EOEditingContext edc = instance.editingContext();
			NSArray<EOParamCompElec> paramsComposante = ParametrageElection.rechercherParametragesActifsPourInstance(edc, EOParamCompElec.ENTITY_NAME, instance);
			NSArray<EOComposanteElective> composantesActives = (NSArray<EOComposanteElective>)paramsComposante.valueForKey(EOParamCompElec.COMPOSANTE_ELECTIVE_KEY);
			LogManager.logDetail("Nombre de composantes électives : " + composantesActives.count());
			if (instance.typeInstance().estSectorise()) {
				verifierComposantesSecteur(instance,composantesActives);
			}
			// Construire un dictionnaire comportant les bureaux de vote pour chaque structure
			// clé : structure, objet : bureau de vote
			// lance une exception si un bureau de vote est affecté à plusieurs structures
			BureauVoteStructure bureauVoteStructure = creerBureauVoteStructure(instance);
			// A partir de cette liste construire les entrées de la table
			NSMutableArray paramsRepart = new NSMutableArray();
			
			for (EOComposanteElective myComposante : composantesActives) {

				// rechercher toutes les structures pour une composante				
				NSArray<EORegroupementComposante> regroupementsComp = EORegroupementComposante.rechercherRegroupementsPourComposanteElective(edc, myComposante);
				
				for (EORegroupementComposante myRegroupement : regroupementsComp) {

					paramsRepart.addObject(creerRepart(instance,myComposante,myRegroupement.structure(),bureauVoteStructure.bureauVotePourStructure(myRegroupement.structure()),edc));
					if (myRegroupement.selectionnerStructuresFilles()) {
						
						NSArray<EOStructure> structures = myRegroupement.structure().toutesStructuresFils();
						
						for (EOStructure myStructure : structures) {
							paramsRepart.addObject(creerRepart(instance, myComposante, myStructure, bureauVoteStructure.bureauVotePourStructure(myStructure), edc));
						}
					}
				}
			}

			instance.setEstEditionCoherente(true);
			instance.setDCalcul(new NSTimestamp());
			edc.saveChanges();
		} catch (Exception e) {
			throw e;
		}
	}
	
	/** Invalide toutes les instances auxquels sont rattaches les parametrages finaux pour un type d'objet donne
	 * (college,bureauVote,composanteElective,secteur,sectionElective).
	 * @param editingContext
	 * @param relationRecherchee
	 * @param objetRecherche
	 */
	public static void invaliderInstances(EOEditingContext edc,String relationRecherchee,EOGenericRecord objetRecherche) {
		if (relationRecherchee == null || objetRecherche == null) {
			return;
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(relationRecherchee + " = %@", new NSArray(objetRecherche));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOParamRepartElec.ENTITY_NAME,qualifier,null);
		NSArray<EOParamRepartElec> params = edc.objectsWithFetchSpecification(fs);
		if (params != null && params.count() > 0) {
			LogManager.logDetail("Invalidation des instances pour " + relationRecherchee + " sur l'objet de type " + objetRecherche.getClass().getName());
			invaliderInstances(((NSArray<EOInstance>)params.valueForKey(EOParamRepartElec.INSTANCE_KEY)));
		}
	}
	
	
	/** Invalide toutes les instances auxquels sont rattaches les parametrages finaux pour un type d'objet donne
	 * (college,bureauVote,composanteElective,secteur,sectionElective).
	 * @param editingContext
	 * @param relationRecherchee
	 * @param objetRecherche
	 */
	public static void invaliderInstancesEtSupprimerParams(EOEditingContext edc,String relationRecherchee,EOGenericRecord objetRecherche) {
		if (relationRecherchee == null || objetRecherche == null) {
			return;
		}
		LogManager.logDetail("Invalidation des instances pour " + relationRecherchee + " sur l'objet de type " + objetRecherche.getClass().getName());
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(relationRecherchee + " = %@", new NSArray(objetRecherche));
		EOFetchSpecification fs = new EOFetchSpecification(EOParamRepartElec.ENTITY_NAME,qualifier,null);
		NSArray<EOParamRepartElec> params = edc.objectsWithFetchSpecification(fs);
		if (params != null && params.count() > 0) {
			
			for (EOParamRepartElec param : params) {
				param.supprimerRelations();
				edc.deleteObject(param);
			}
		}
	}	
	
	
	/**
	 * 
	 * @param instances
	 */
	private static void invaliderInstances(NSArray<EOInstance> instances) {
		for (EOInstance myInstance : instances) {
			myInstance.setEstEditionCoherente(false);
		}
	}
	
	/**
	 * 
	 * @param instance
	 * @param composantes
	 * @throws Exception
	 */
	private static void verifierComposantesSecteur(EOInstance instance,NSArray<EOComposanteElective> composantes) throws Exception {
		
		EOEditingContext edc = instance.editingContext();
		
		NSArray<EOParamSecteur> paramsSecteur = ParametrageElection.rechercherParametragesActifsPourInstance(edc, EOParamSecteur.ENTITY_NAME, instance);
		
		String message = "";
		for (EOSecteur mySecteur : (NSArray<EOSecteur>)paramsSecteur.valueForKey(EOParamSecteur.SECTEUR_KEY)) {
 
			NSArray<EORegroupementSecteur> regroupements = EORegroupementSecteur.rechercherRegroupementsPourSecteur(edc, mySecteur);
			NSArray<EOComposanteElective> composantesSecteur = (NSArray<EOComposanteElective>)regroupements.valueForKey("composante");
			
			for (EOComposanteElective composante : composantesSecteur) {
				if (composantes.containsObject(composante) == false) {
					message = message + "\t" + composante.llComposanteElective() + "\n";
				}
			}
		}
		if (message.length() > 0) {
			// Il manque des composantes
			message = "Vérification des secteurs\nLes composantes :\n" + message + "doivent être rajoutées au paramétrage !";
			throw new Exception(message);
		}

	}
	
	/**
	 * 
	 * @param instance
	 * @param composante
	 * @param structure
	 * @param bureauVote
	 * @param editingContext
	 * @return
	 */
	private static EOParamRepartElec creerRepart(EOInstance instance,EOComposanteElective composante,EOStructure structure,EOBureauVote bureauVote,EOEditingContext edc) {
		EOParamRepartElec repart = new EOParamRepartElec();
		repart.init(instance, composante, structure,bureauVote);
		if (edc != null) {
			edc.insertObject(repart);
		}
		return repart;
	}
	
	/**
	 * 
	 * @param instance
	 * @return
	 * @throws Exception
	 */
	private static BureauVoteStructure creerBureauVoteStructure(EOInstance instance) throws Exception {
		
		EOEditingContext edc = instance.editingContext();
		
		// Rechercher tous les bureaux de vote retenus pour l'élection
		NSArray<EOParamBv> paramsBv = ParametrageElection.rechercherParametragesActifsPourInstance(edc, EOParamBv.ENTITY_NAME, instance);
		
		BureauVoteStructure bureauVoteStructure = new EOParamRepartElec.BureauVoteStructure();
		for (EOParamBv paramBv : paramsBv) {

			NSArray<EORegroupementBv> structuresPourBv = EORegroupementBv.rechercherRegroupementsPourBureauDeVote(edc, paramBv.bureauVote());
			
			for (EORegroupementBv regroupement : structuresPourBv) {

				ajouterBureauVotePourStructure(bureauVoteStructure,regroupement.bureauVote(),regroupement.structure());
				if (regroupement.selectionnerStructuresFilles()) {
					
					for (EOStructure structure : (NSArray<EOStructure>)regroupement.structure().toutesStructuresFils()) {
						ajouterBureauVotePourStructure(bureauVoteStructure, regroupement.bureauVote(), structure);
					}
				}
			}
			
			// Vérifier si il y a d'autres structures ajoutées pour ce paramétrage
			NSArray<EOParamStrBv> paramsStrBv = EOParamStrBv.rechercherParamsPourInstanceEtBureau(edc, instance, paramBv.bureauVote());			
			for (EOParamStrBv param : paramsStrBv) {
				ajouterBureauVotePourStructure(bureauVoteStructure,param.bureauVote(),param.structure());
			}	
		}
		
		return bureauVoteStructure;
		
	}

	/**
	 * 
	 * @param bureauVoteStructure
	 * @param bureau
	 * @param structure
	 * @throws Exception
	 */
	private static void ajouterBureauVotePourStructure(BureauVoteStructure bureauVoteStructure,EOBureauVote bureau, EOStructure structure) throws Exception {
		EOBureauVote bureauVote = bureauVoteStructure.bureauVotePourStructure(structure);
		if (bureauVote == null) {
			bureauVoteStructure.ajouterBureauVotePourStructure(bureau, structure);
		} else if (bureauVote != bureau) {
			throw new Exception("La structure " + structure.llStructure() + " est associée aux bureaux de vote\n" +
					"\t" + bureauVote.llBureauVote() + "\n" + "\t" + bureau.llBureauVote() + "\n" +
			"Veuillez revoir votre nomenclature ou votre paramétrage");
		}
	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private static class BureauVoteStructure {
		private NSMutableDictionary dictionnaire;
		// Le dictionnaire comporte le bureau de vote pour chaque structure
		// clé : structure, objet : bureau de vote

		public BureauVoteStructure() {
			dictionnaire = new NSMutableDictionary();
		}
		public void ajouterBureauVotePourStructure(EOBureauVote bureauVote,EOStructure structure) {
			dictionnaire.setObjectForKey(bureauVote, structure);
		}
		public EOBureauVote bureauVotePourStructure(EOStructure structure) {
			return (EOBureauVote)dictionnaire.objectForKey(structure);
		}
	}


}


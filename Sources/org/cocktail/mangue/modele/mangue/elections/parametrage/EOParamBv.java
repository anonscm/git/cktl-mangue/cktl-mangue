// EOParamBv.java
// Created on Fri Feb 16 11:21:13 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.elections.parametrage;

import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.elections.EOBureauVote;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class EOParamBv extends _EOParamBv implements RecordAvecLibelle {

    public EOParamBv() {
        super();
    }
    
	/**
	 * 
	 * @param ec
	 * @param instance
	 * @param college
	 * @return
	 */
	public static EOParamBv creer(EOEditingContext ec, EOInstance instance, EOBureauVote bureau) {

		EOParamBv newObject = (EOParamBv) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.setInstanceRelationship(instance);
		newObject.setBureauVoteRelationship(bureau);
		newObject.setTemSelection(CocktailConstantes.VRAI);
		return newObject;
	}


    public void supprimerRelations() {
    	super.supprimerRelations();
    	removeObjectFromBothSidesOfRelationshipWithKey(bureauVote(), "bureauVote");
    }

    public String libelle() {
    	return bureauVote().llBureauVote();
    }

    /**
     * 
     * @param edc
     * @param typeInstance
     * @return
     */
	public static NSArray<EOParamBv> findForInstance(EOEditingContext edc,EOInstance typeInstance) {
		try {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INSTANCE_KEY + " = %@", new NSArray(typeInstance));
		return fetchAll(edc, qualifier);
		}
		catch (Exception e) {
			return new NSArray<EOParamBv>();
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param bureauVote
	 * @return
	 */
    public static NSArray<EOParamBv> rechercherParametragePourBureauVote(EOEditingContext editingContext,EOBureauVote bureauVote) {
    	EOQualifier qualifier = null;
    	if (bureauVote != null) {
    		qualifier = EOQualifier.qualifierWithQualifierFormat(BUREAU_VOTE_KEY + " = %@", new NSArray(bureauVote));
    	}
		EOFetchSpecification fs = new EOFetchSpecification("ParamBv",qualifier,null);
		return editingContext.objectsWithFetchSpecification(fs);
    }
}

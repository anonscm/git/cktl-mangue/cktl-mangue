// EOParamSecteur.java
// Created on Fri Feb 16 15:29:53 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.elections.parametrage;

import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.elections.EOSecteur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOParamSecteur extends _EOParamSecteur implements RecordAvecLibelle {

	public EOParamSecteur() {
		super();
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		setSecteurRelationship(null);
	}
	/**
	 * 
	 * @param ec
	 * @param instance
	 * @param college
	 * @return
	 */
	public static EOParamSecteur creer(EOEditingContext ec, EOInstance instance, EOSecteur secteur, EOCollege college) {

		EOParamSecteur newObject = (EOParamSecteur) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.setInstanceRelationship(instance);
		newObject.setCollegeRelationship(college);
		newObject.setSecteurRelationship(secteur);
		newObject.setTemSelection(CocktailConstantes.VRAI);
		return newObject;
	}

	public String libelle() {
		return secteur().llSecteur();
	}

	/**
	 * 
	 * @param edc
	 * @param typeInstance
	 * @return
	 */
	public static NSArray<EOParamCollege> findForInstanceAndCollege(EOEditingContext edc,EOInstance instance, EOCollege college) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " = %@", new NSArray(college)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INSTANCE_KEY + " = %@", new NSArray(instance)));
			return fetchAll(edc, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOParamCollege>();
		}
	}

	/** retourne les parametrages associ&eacute;s au coll&egrave;ge
	 * @param editingContext
	 * @param college (peut etre nul)
	 * @param secteur (peut etre null)
	 */
	public static NSArray rechercherParametragesPourCollegeEtSecteur(EOEditingContext editingContext,EOCollege college,EOSecteur secteur) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (college != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("college = %@", new NSArray(college)));
		}
		if (secteur != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("secteur = %@", new NSArray(secteur)));
		}
		EOFetchSpecification fs = new EOFetchSpecification("ParamSecteur",new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** retourne les param&eacute;trages associ&eacute;s au coll&egrave;ge
	 * @param editingContext
	 * @param instance (peut &ecirc;tre nulle)
	 * @param college (peut &ecirc;tre nul)
	 */
	public static NSArray rechercherParametragesPourInstanceEtCollege(EOEditingContext editingContext,EOInstance instance,EOCollege college) {
		NSMutableArray qualifiers = new NSMutableArray();

		if (instance != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("instance = %@", new NSArray(instance)));
		}
		if (college != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("college = %@", new NSArray(college)));
		}
		EOFetchSpecification fs = new EOFetchSpecification("ParamSecteur",new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
}

// EOParamStrBv.java
// Created on Fri Feb 16 11:20:52 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.elections.parametrage;

import org.cocktail.mangue.modele.grhum.elections.EOBureauVote;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOParamStrBv extends _EOParamStrBv {

	public EOParamStrBv() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param instance
	 * @param college
	 * @return
	 */
	public static EOParamStrBv creer(EOEditingContext ec, EOBureauVote bureau, EOInstance instance, EOStructure structure) {

		EOParamStrBv newObject = (EOParamStrBv) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.setBureauVoteRelationship(bureau);
		newObject.setInstanceRelationship(instance);
		newObject.setStructureRelationship(structure);
		return newObject;
	}


	public void initAvecInstanceBureauEtStructure(EOInstance instance,EOBureauVote bureau,EOStructure structure) {
		addObjectToBothSidesOfRelationshipWithKey(instance, "instance");
		addObjectToBothSidesOfRelationshipWithKey(bureau, "bureauVote");
		addObjectToBothSidesOfRelationshipWithKey(structure,"structure");
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(instance(), "instance");
		removeObjectFromBothSidesOfRelationshipWithKey(bureauVote(), "bureauVote");
		removeObjectFromBothSidesOfRelationshipWithKey(structure(), "structure");
	}

	/**
	 * 
	 * @param edc
	 * @param typeInstance
	 * @return
	 */
	public static NSArray<EOParamStrBv> findForBureauAndInstance(EOEditingContext edc, EOBureauVote bureau, EOInstance instance) {
		try {
			
			if (instance == null && bureau == null)
				return new NSArray();
			
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			if (bureau != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(BUREAU_VOTE_KEY + " = %@", new NSArray(bureau)));
			if (instance != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INSTANCE_KEY + " = %@", new NSArray(instance)));
			return fetchAll(edc, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOParamStrBv>();
		}
	}

	/** recherche les param&egrave;tres pour une instance et un bureau de vote
	 * @param instance (peut &ecirc;tre nulle)
	 * @param bureau (peut &ecirc;tre nul)
	 * @return null si instance et bureau nuls sinon les param&eacute;trages
	 */
	public static NSArray rechercherParamsPourInstanceEtBureau(EOEditingContext editingContext,EOInstance instance,EOBureauVote bureau) {
		if (instance == null || bureau == null) {
			return null;
		}
		NSMutableArray qualifiers = new NSMutableArray();
		if (instance != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INSTANCE_KEY + " = %@", new NSArray(instance)));
		}
		if (bureau != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("bureauVote = %@", new NSArray(bureau)));
		}
		EOFetchSpecification fs = new EOFetchSpecification("ParamStrBv",new EOAndQualifier(qualifiers),new NSArray(EOSortOrdering.sortOrderingWithKey("structure.llStructure", EOSortOrdering.CompareAscending)));
		return editingContext.objectsWithFetchSpecification(fs);
	}

	public static NSArray rechercherParamsPourInstanceEtStructure(EOEditingContext editingContext,EOInstance instance,EOStructure structure) {
		if (instance == null || structure == null) {
			return null;
		}
		NSMutableArray args = new NSMutableArray(instance);
		args.addObject(structure);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("instance = %@ AND structure = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification("ParamStrBv",qualifier,null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
}

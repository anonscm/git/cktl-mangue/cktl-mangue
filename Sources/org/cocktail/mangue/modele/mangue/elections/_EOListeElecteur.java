// _EOListeElecteur.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOListeElecteur.java instead.
package org.cocktail.mangue.modele.mangue.elections;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOListeElecteur extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "ListeElecteur";
	public static final String ENTITY_TABLE_NAME = "LISTE_ELECTEUR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noOrdre";

	public static final String C_SECTION_ELECTIVE_KEY = "cSectionElective";
	public static final String C_TYPE_EXCLUSION_KEY = "cTypeExclusion";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_AFFECTATIONS_MULTIPLES_KEY = "temAffectationsMultiples";
	public static final String TEM_CHERCHEUR_ELECTEUR_KEY = "temChercheurElecteur";
	public static final String TEM_EXCLU_MAN_KEY = "temExcluMan";
	public static final String TEM_INS_MAN_KEY = "temInsMan";

// Attributs non visibles
	public static final String NO_SEQ_COLLEGE_KEY = "noSeqCollege";
	public static final String NO_ORDRE_KEY = "noOrdre";
	public static final String C_BUREAU_VOTE_KEY = "cBureauVote";
	public static final String C_INSTANCE_KEY = "cInstance";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String C_COMPOSANTE_ELECTIVE_KEY = "cComposanteElective";
	public static final String C_SECTEUR_KEY = "cSecteur";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String NO_SEQ_CONTRAT_KEY = "noSeqContrat";
	public static final String CTRH_ORDRE_KEY = "ctrhOrdre";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

//Colonnes dans la base de donnees
	public static final String C_SECTION_ELECTIVE_COLKEY = "C_SECTION_ELECTIVE";
	public static final String C_TYPE_EXCLUSION_COLKEY = "C_TYPE_EXCLUSION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String TEM_AFFECTATIONS_MULTIPLES_COLKEY = "TEM_AFFECTATIONS_MULTIPLES";
	public static final String TEM_CHERCHEUR_ELECTEUR_COLKEY = "TEM_CHERCHEUR_ELECTEUR";
	public static final String TEM_EXCLU_MAN_COLKEY = "TEM_EXCLU_MAN";
	public static final String TEM_INS_MAN_COLKEY = "TEM_INS_MAN";

	public static final String NO_SEQ_COLLEGE_COLKEY = "NO_SEQ_COLLEGE";
	public static final String NO_ORDRE_COLKEY = "LELE_ORDRE";
	public static final String C_BUREAU_VOTE_COLKEY = "C_BUREAU_VOTE";
	public static final String C_INSTANCE_COLKEY = "C_INSTANCE";
	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String NO_SEQ_CARRIERE_COLKEY = "NO_SEQ_CARRIERE";
	public static final String C_COMPOSANTE_ELECTIVE_COLKEY = "C_COMPOSANTE_ELECTIVE";
	public static final String C_SECTEUR_COLKEY = "C_SECTEUR";
	public static final String NO_CNU_COLKEY = "NO_CNU";
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String NO_SEQ_CONTRAT_COLKEY = "NO_SEQ_CONTRAT";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";


	// Relationships
	public static final String BUREAU_VOTE_KEY = "bureauVote";
	public static final String CARRIERE_KEY = "carriere";
	public static final String CNU_KEY = "cnu";
	public static final String COLLEGE_KEY = "college";
	public static final String COMPOSANTE_ELECTIVE_KEY = "composanteElective";
	public static final String CONTRAT_KEY = "contrat";
	public static final String TO_CONTRAT_HEBERGE_KEY = "toContratHeberge";
	public static final String CORPS_KEY = "corps";
	public static final String GRADE_KEY = "grade";
	public static final String INDIVIDU_KEY = "individu";
	public static final String INSTANCE_KEY = "instance";
	public static final String SECTEUR_KEY = "secteur";
	public static final String SECTION_ELECTIVE_KEY = "sectionElective";
	public static final String STRUCTURE_KEY = "structure";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cSectionElective() {
    return (String) storedValueForKey(C_SECTION_ELECTIVE_KEY);
  }

  public void setCSectionElective(String value) {
    takeStoredValueForKey(value, C_SECTION_ELECTIVE_KEY);
  }

  public String cTypeExclusion() {
    return (String) storedValueForKey(C_TYPE_EXCLUSION_KEY);
  }

  public void setCTypeExclusion(String value) {
    takeStoredValueForKey(value, C_TYPE_EXCLUSION_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String temAffectationsMultiples() {
    return (String) storedValueForKey(TEM_AFFECTATIONS_MULTIPLES_KEY);
  }

  public void setTemAffectationsMultiples(String value) {
    takeStoredValueForKey(value, TEM_AFFECTATIONS_MULTIPLES_KEY);
  }

  public String temChercheurElecteur() {
    return (String) storedValueForKey(TEM_CHERCHEUR_ELECTEUR_KEY);
  }

  public void setTemChercheurElecteur(String value) {
    takeStoredValueForKey(value, TEM_CHERCHEUR_ELECTEUR_KEY);
  }

  public String temExcluMan() {
    return (String) storedValueForKey(TEM_EXCLU_MAN_KEY);
  }

  public void setTemExcluMan(String value) {
    takeStoredValueForKey(value, TEM_EXCLU_MAN_KEY);
  }

  public String temInsMan() {
    return (String) storedValueForKey(TEM_INS_MAN_KEY);
  }

  public void setTemInsMan(String value) {
    takeStoredValueForKey(value, TEM_INS_MAN_KEY);
  }

  public org.cocktail.mangue.modele.grhum.elections.EOBureauVote bureauVote() {
    return (org.cocktail.mangue.modele.grhum.elections.EOBureauVote)storedValueForKey(BUREAU_VOTE_KEY);
  }

  public void setBureauVoteRelationship(org.cocktail.mangue.modele.grhum.elections.EOBureauVote value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.elections.EOBureauVote oldValue = bureauVote();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BUREAU_VOTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BUREAU_VOTE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOCarriere carriere() {
    return (org.cocktail.mangue.modele.mangue.individu.EOCarriere)storedValueForKey(CARRIERE_KEY);
  }

  public void setCarriereRelationship(org.cocktail.mangue.modele.mangue.individu.EOCarriere value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOCarriere oldValue = carriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CARRIERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CARRIERE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu cnu() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu)storedValueForKey(CNU_KEY);
  }

  public void setCnuRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu oldValue = cnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CNU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CNU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.elections.EOCollege college() {
    return (org.cocktail.mangue.modele.grhum.elections.EOCollege)storedValueForKey(COLLEGE_KEY);
  }

  public void setCollegeRelationship(org.cocktail.mangue.modele.grhum.elections.EOCollege value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.elections.EOCollege oldValue = college();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COLLEGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COLLEGE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.elections.EOComposanteElective composanteElective() {
    return (org.cocktail.mangue.modele.grhum.elections.EOComposanteElective)storedValueForKey(COMPOSANTE_ELECTIVE_KEY);
  }

  public void setComposanteElectiveRelationship(org.cocktail.mangue.modele.grhum.elections.EOComposanteElective value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.elections.EOComposanteElective oldValue = composanteElective();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPOSANTE_ELECTIVE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPOSANTE_ELECTIVE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOContrat contrat() {
    return (org.cocktail.mangue.modele.mangue.individu.EOContrat)storedValueForKey(CONTRAT_KEY);
  }
  public org.cocktail.mangue.modele.mangue.individu.EOContratHeberges toContratHeberge() {
	    return (org.cocktail.mangue.modele.mangue.individu.EOContratHeberges)storedValueForKey(TO_CONTRAT_HEBERGE_KEY);
	  }
  
  public void setContratRelationship(org.cocktail.mangue.modele.mangue.individu.EOContrat value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_KEY);
    }
  }
  
  public void setToContratHebergeRelationship(org.cocktail.mangue.modele.mangue.individu.EOContratHeberges value) {
	    if (value == null) {
	    	org.cocktail.mangue.modele.mangue.individu.EOContratHeberges oldValue = toContratHeberge();
	    	if (oldValue != null) {
	    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_HEBERGE_KEY);
	      }
	    } else {
	    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_HEBERGE_KEY);
	    }
	  }

  public org.cocktail.mangue.modele.grhum.EOCorps corps() {
    return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey(CORPS_KEY);
  }

  public void setCorpsRelationship(org.cocktail.mangue.modele.grhum.EOCorps value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOCorps oldValue = corps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CORPS_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOGrade grade() {
    return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey(GRADE_KEY);
  }

  public void setGradeRelationship(org.cocktail.mangue.modele.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOGrade oldValue = grade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GRADE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.elections.EOInstance instance() {
    return (org.cocktail.mangue.modele.grhum.elections.EOInstance)storedValueForKey(INSTANCE_KEY);
  }

  public void setInstanceRelationship(org.cocktail.mangue.modele.grhum.elections.EOInstance value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.elections.EOInstance oldValue = instance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INSTANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INSTANCE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.elections.EOSecteur secteur() {
    return (org.cocktail.mangue.modele.grhum.elections.EOSecteur)storedValueForKey(SECTEUR_KEY);
  }

  public void setSecteurRelationship(org.cocktail.mangue.modele.grhum.elections.EOSecteur value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.elections.EOSecteur oldValue = secteur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SECTEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SECTEUR_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.elections.EOSectionElective sectionElective() {
    return (org.cocktail.mangue.modele.grhum.elections.EOSectionElective)storedValueForKey(SECTION_ELECTIVE_KEY);
  }

  public void setSectionElectiveRelationship(org.cocktail.mangue.modele.grhum.elections.EOSectionElective value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.elections.EOSectionElective oldValue = sectionElective();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SECTION_ELECTIVE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SECTION_ELECTIVE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOStructure structure() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  

/**
 * Créer une instance de EOListeElecteur avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOListeElecteur createEOListeElecteur(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temExcluMan
, org.cocktail.mangue.modele.grhum.elections.EOCollege college, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu, org.cocktail.mangue.modele.grhum.elections.EOInstance instance			) {
    EOListeElecteur eo = (EOListeElecteur) createAndInsertInstance(editingContext, _EOListeElecteur.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemExcluMan(temExcluMan);
    eo.setCollegeRelationship(college);
    eo.setIndividuRelationship(individu);
    eo.setInstanceRelationship(instance);
    return eo;
  }

  
	  public EOListeElecteur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOListeElecteur)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOListeElecteur creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOListeElecteur creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOListeElecteur object = (EOListeElecteur)createAndInsertInstance(editingContext, _EOListeElecteur.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOListeElecteur localInstanceIn(EOEditingContext editingContext, EOListeElecteur eo) {
    EOListeElecteur localInstance = (eo == null) ? null : (EOListeElecteur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOListeElecteur#localInstanceIn a la place.
   */
	public static EOListeElecteur localInstanceOf(EOEditingContext editingContext, EOListeElecteur eo) {
		return EOListeElecteur.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOListeElecteur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOListeElecteur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOListeElecteur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOListeElecteur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOListeElecteur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOListeElecteur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOListeElecteur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOListeElecteur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOListeElecteur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOListeElecteur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOListeElecteur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOListeElecteur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

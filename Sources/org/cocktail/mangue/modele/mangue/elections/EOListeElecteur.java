//EOListeElecteur.java
//Created on Thu Feb 22 14:36:51 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.elections;

import java.util.Enumeration;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.ParametrageElection;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOCollegeSectionElective;
import org.cocktail.mangue.modele.grhum.elections.EOExclusion;
import org.cocktail.mangue.modele.grhum.elections.EOGroupeExclusion;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionContrat;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionCorps;
import org.cocktail.mangue.modele.grhum.elections.EOInclusionDiplome;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;
import org.cocktail.mangue.modele.grhum.elections.EORegroupementSecteur;
import org.cocktail.mangue.modele.grhum.elections.EOSectionElective;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCnu;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCollege;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamRepartElec;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSectionElective;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuTypeElectionHu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOListeElecteur extends _EOListeElecteur {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static NSArray<EOSectionElective> sectionsElectives;	// 24/02/2011 - pour éviter la recherche des sections électives pour chaque électeur
	private static NSArray<EOCollege> colleges;			// 24/02/2011 - pour éviter la recherche des collèges pour chaque électeur

	public EOListeElecteur() {
		super();
	}

	public String nomElecteur() {
		if (individu() != null) {
			return individu().nomUsuel();
		} else {
			return null;
		}
	}
	public String prenomElecteur() {
		if (individu() != null) {
			return individu().prenom();
		} else {
			return null;
		}
	}
	public String nomPatronymiqueElecteur() {
		if (individu() != null) {
			return individu().nomPatronymiqueAffichage();
		} else {
			return null;
		}
	}	
	public String inseeComplet() {
		if (individu() != null) {
			return individu().numeroInseeImpression()+ " " +individu().cleInseeImpression();
		} else {
			return null;
		}
	}		
	public boolean estInsereManuellement() {
		return temInsMan() != null && temInsMan().equals(CocktailConstantes.VRAI);
	}
	public void setEstInsereManuellement(boolean aBool) {
		if (aBool) {
			setTemInsMan(CocktailConstantes.VRAI);
		} else {
			setTemInsMan(CocktailConstantes.FAUX);
		}
	}
	public boolean estExclusManuellement() {
		return temExcluMan() != null && temExcluMan().equals(CocktailConstantes.VRAI);
	}
	public void setEstExclusManuellement(boolean aBool) {
		if (aBool) {
			setTemExcluMan(CocktailConstantes.VRAI);
		} else {
			setTemExcluMan(CocktailConstantes.FAUX);
		}
	}
	public boolean estChercheur() {
		return temChercheurElecteur() != null && temChercheurElecteur().equals(CocktailConstantes.VRAI);
	}
	public void setEstChercheur(boolean aBool) {
		if (aBool) {
			setTemChercheurElecteur(CocktailConstantes.VRAI);
		} else {
			setTemChercheurElecteur(CocktailConstantes.FAUX);
		}
	}
	public boolean aPlusieursAffectations() {
		return temAffectationsMultiples() != null && temAffectationsMultiples().equals(CocktailConstantes.VRAI);
	}
	public void setAPlusieursAffectations(boolean aBool) {
		if (aBool) {
			setTemAffectationsMultiples(CocktailConstantes.VRAI);
		} else {
			setTemAffectationsMultiples(CocktailConstantes.FAUX);
		}
	}
	public void initAvecInstance(EOInstance instance) {
		init();
		setInstanceRelationship(instance);
	}
	public void initAvecIndividuEtInstance(EOIndividu individu,EOInstance instance) {
		initAvecInstance(instance);
		setIndividuRelationship(individu);
	}
	public void supprimerRelations() {

		setIndividuRelationship(null);
		setInstanceRelationship(null);
		setCollegeRelationship(null);

		setBureauVoteRelationship(null);
		setCnuRelationship(null);
		setComposanteElectiveRelationship(null);
		setContratRelationship(null);
		setCorpsRelationship(null);
		setGradeRelationship(null);
		setSecteurRelationship(null);
		setSectionElectiveRelationship(null);
		setStructureRelationship(null);
	}

	/**
	 * 
	 */
	public void validateForSave() {

		if (individu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu !");
		}
		if (college() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un college !");
		}
		if (instance() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une instance !");
		}

	}


	/**
	 * 
	 */
	private void init() {
		setEstInsereManuellement(false);
		setEstExclusManuellement(false);
		setEstChercheur(false);
		setAPlusieursAffectations(false);
	}


	/**
	 * 
	 * Generation de la liste electorale pour une instance donnee
	 * 
	 * @param instance
	 * @throws Exception
	 */
	public static void genererListeElecteursPourInstance(EOInstance instance) throws Exception {

		try {

			String message = "Constitution des listes électorales pour " + instance.llInstance();
			if (instance.dScrutin() != null) {
				message = message + " pour le scrutin du " + instance.dateScrutinFormatee();
			}
			LogManager.logDetail(message);
			EOEditingContext edc = instance.editingContext();
			// Suppression de tous les électeurs existant déjà pour cette instance
			LogManager.logDetail("Suppression des électeurs existants");
			NSArray<EOListeElecteur> electeursAnciens = EOListeElecteur.rechercherElecteursPourInstance(instance,true);
			if (electeursAnciens != null && electeursAnciens.count() > 0) {
				for (java.util.Enumeration<EOListeElecteur> e = electeursAnciens.objectEnumerator();e.hasMoreElements();) {
					EOListeElecteur electeur = e.nextElement();
					edc.deleteObject(electeur);
				}
			}
			// 24/02/2011 - pour éviter les multiples fetch des paramètres
			if (instance.typeInstance().estTypeJuridDiscHu()) {
				// 24/02/2011 - pour éviter la recherche des paramètres pour chaque électeur
				sectionsElectives = (NSArray<EOSectionElective>)ParametrageElection.rechercherParametragesActifsPourInstance(edc, EOParamSectionElective.ENTITY_NAME, instance).valueForKey(EOParamSectionElective.SECTION_ELECTIVE_KEY);
				colleges = (NSArray<EOCollege>)ParametrageElection.rechercherParametragesActifsPourInstance(edc, EOParamCollege.ENTITY_NAME, instance).valueForKey(EOParamCollege.COLLEGE_KEY);
			} else {
				sectionsElectives = null;
				colleges = null;
			}

			// rechercher si il existe un paramétrage final défini sinon lancer une exception
			NSArray<EOParamRepartElec> paramsRepart = EOParamRepartElec.rechercherParametragesPourInstance(edc, instance);
			if (paramsRepart.count() == 0) {
				throw new Exception("Paramétrage final non défini pour cette instance !");
			}

			NSMutableArray<EOStructure> structures = new NSMutableArray<EOStructure>();

			for (EOStructure structure : (NSArray<EOStructure>)paramsRepart.valueForKey(EOParamRepartElec.STRUCTURE_KEY)) {
				if (structures.containsObject(structure) == false) {
					structures.addObject(structure);
				}
			}

			// Rechercher les affectations pour ces structures et la date de référence de cette élection
			LogManager.logDetail("Recherche des affectations");
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(SuperFinder.construireORQualifier(EOAffectation.TO_STRUCTURE_ULR_KEY + "." + EOStructure.C_STRUCTURE_KEY, (NSArray)structures.valueForKey(EOStructure.C_STRUCTURE_KEY)));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOAffectation.DATE_DEBUT_KEY, instance.dReference(), EOAffectation.DATE_FIN_KEY, instance.dReference()));
			EOQualifier qualifier = new EOAndQualifier(qualifiers);
			NSArray<EOAffectation> affectations = EOAffectation.rechercherAffectationsAvecCriteres(edc, qualifier);

			// trier les affectations par noIndividu et affectation croissante
			NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOAffectation.INDIVIDU_KEY + "." + EOIndividu.NO_INDIVIDU_KEY,EOSortOrdering.CompareAscending));
			sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOAffectation.DATE_DEBUT_KEY,EOSortOrdering.CompareAscending));
			affectations = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations, sorts);	
			NSMutableArray<EOListeElecteur> electeurs = new NSMutableArray();

			EOListeElecteur electeurCourant = null;

			for (EOAffectation affectation : affectations) {

				boolean signalerAffectationMultiple = false;
				if (electeurCourant != null) {
					if (electeurCourant.individu() == affectation.individu() &&  instance.typeInstance().estDoubleCarriere() == false) {
						// Cet individu a deux affectations
						electeurCourant.setAPlusieursAffectations(true);
						signalerAffectationMultiple = true;
					} 
					// on change d'individu ou c'est le premier individu
					// l'ajouter au tableau des électeurs
					electeurs.addObject(electeurCourant);
				}
				EOListeElecteur electeur = null;
				EOCarriere carriere = EOCarriere.carrierePourDate(edc, affectation.individu(), instance.dReference());
				if (carriere != null) {
					electeur = electeurValidePourInstanceEtCarriere(instance,carriere);
				} else if (instance.typeInstance().estTypeJuridDiscHu() || (!instance.typeInstance().estElcCnu() && !instance.typeInstance().estElcCap() &&
						!instance.typeInstance().estElcCses())) { // 23/02/2011 - pour les JDNPHU on prend compte les contrats

					EOContrat contrat = EOContrat.contratPrincipalPourDate(edc, affectation.individu(), instance.dReference());
					if (contrat != null) {
						electeur = electeurValidePourInstanceEtContrat(instance,contrat);
					}
					else {	// Prise en compte des heberges ?
						EOContratHeberges contratHeberge = EOContratHeberges.findForIndividuAndDate(edc, affectation.individu(), instance.dReference());
						if (contratHeberge != null) {
							electeur = electeurValidePourInstanceEtContratHeberge(instance,contratHeberge);
						}
					}
				}

				if (electeur != null) {
					electeurCourant = electeur;
					if (signalerAffectationMultiple) {
						electeurCourant.setAPlusieursAffectations(true);
					}
					affecterInformations(electeurCourant,instance,paramsRepart,affectation.toStructureUlr());
					edc.insertObject(electeurCourant);
				}
			} 
			try {
				LogManager.logDetail("Nb Electeurs " + electeurs.count());
				edc.saveChanges();
			} catch (Exception exc) {
				throw exc;
			}
		} catch (RuntimeException e) {
			throw e;
		}
	}

	/** recherche tous les electeurs d'une instance (sans refresh) */
	public static NSArray rechercherElecteursPourInstance(EOInstance instance) {
		return rechercherElecteursPourInstance(instance, false);
	}
	/** recherche tous les electeurs d'une instance
	 * @param forceRefresh true si on veut un refresh de la liste
	 */
	public static NSArray<EOListeElecteur> rechercherElecteursPourInstance(EOInstance instance,boolean forceRefresh) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INSTANCE_KEY + "=%@", new NSArray(instance));
		return fetchAll(instance.editingContext(), qualifier);
	}
	/** recherche tous les electeurs d'une instance d'un certain type (retenus, exclus, sans bureau de vote,double affectation, tous)
	 * @param instance
	 * @param type
	 * @param forceRefresh true si on veut un refresh de la liste
	 */
	public static NSArray<EOListeElecteur> rechercherElecteursPourInstanceEtType(EOInstance instance,int type,boolean forceRefresh) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(INSTANCE_KEY + "=%@", new NSArray(instance)));
		EOQualifier qualifier = null;
		switch(type) {
		case ManGUEConstantes.RETENUS :	
			qualifier = EOQualifier.qualifierWithQualifierFormat(TEM_EXCLU_MAN_KEY +" = 'N'", null);
			break;
		case ManGUEConstantes.EXCLUS :
			qualifier = EOQualifier.qualifierWithQualifierFormat("temExcluMan = 'O' OR cTypeExclusion <> nil", null);
			break;
		case ManGUEConstantes.SANS_BUREAU_VOTE :
			qualifier = EOQualifier.qualifierWithQualifierFormat(BUREAU_VOTE_KEY + " = nil",null);
			break;
		case ManGUEConstantes.MULTI_AFFECTATIONS :
			qualifier = EOQualifier.qualifierWithQualifierFormat(TEM_AFFECTATIONS_MULTIPLES_KEY +"='O'",null);
			break;
		case ManGUEConstantes.AJOUTES :
			qualifier = EOQualifier.qualifierWithQualifierFormat(TEM_INS_MAN_KEY + "='O'",null);

			break;
		case ManGUEConstantes.TOUS :
			break;
		}
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}

		return fetchAll(instance.editingContext(), new EOAndQualifier(qualifiers));
	}
	public static NSArray rechercherElecteursInclusManuellementPourInstance(EOInstance instance) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INSTANCE_KEY + "=%@",new NSArray(instance)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_INS_MAN_KEY + "='O'",null));

		return fetchAll(instance.editingContext(), new EOAndQualifier(qualifiers));
	}
	public static boolean estElecteurSelectionnePourInstance(EOInstance instance,EOIndividu individu) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INSTANCE_KEY + "=%@",new NSArray(instance)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@",new NSArray(individu)));

		EOListeElecteur liste = fetchFirstByQualifier(instance.editingContext(), new EOAndQualifier(qualifiers));
		return liste != null;
	}
	//	méthodes privées statiques
	private static EOListeElecteur electeurValidePourInstanceEtCarriere(EOInstance instance,EOCarriere carriere) throws Exception {

		EOEditingContext editingContext = instance.editingContext();

		NSArray<EOElementCarriere> elements = carriere.elementsPourPeriode(instance.dReference(),instance.dReference());

		// Normalement, il n'y a qu'un élément de carrière à une date donnée
		if (elements == null || elements.count() == 0) {
			return null;
		} else {

			EOElementCarriere element = elements.get(0);

			if (instance.typeInstance().estElcCap()) {
				// Vérifier si le corps fait partie des corps retenus pour cette élection
				NSArray<ParametrageElection> paramsCorps = ParametrageElection.rechercherParametragesActifsPourInstance(editingContext, EOParamCollege.ENTITY_NAME, instance);
				if (paramsCorps.count() > 0 && ((NSArray)paramsCorps.valueForKey("corps")).containsObject(element.toCorps()) == false) {
					// on ne retient pas ce corps
					return null;
				}
			}
			if (instance.typeInstance().estElcCnu()) {
				// Vérifier si la cnu n'est pas nulle et fait partie des cnus retenues pour cette élection
				if (element.toCarriere().toCnu() == null) {
					return null;
				}
				NSArray<EOParamCnu> paramsCnu = ParametrageElection.rechercherParametragesActifsPourInstance(editingContext, EOParamCnu.ENTITY_NAME, instance);
				if (paramsCnu.count() > 0 && ((NSArray)paramsCnu.valueForKey("cnu")).containsObject(element.toCarriere().toCnu()) == false) {
					// on ne retient pas cet individu
					return null;
				}
			}
			// On recherche une inclusion valide pour ce corps
			EOInclusionCorps inclusion = inclusionCorpsValidePour(editingContext,instance,element.toCorps());
			if (inclusion == null) {
				return null;
			}

			// 22/02/2011 - le collège est déterminer autrement pour les élections JDNPHU
			EOCollege college = inclusion.college();
			if (instance.typeInstance().estElcCs()) {	// pour les conseils scientifiques
				// vérifier les inclusions de diplôme et ensuite vérifier les diplômes de l'individu
				NSArray<EOInclusionDiplome> inclusionsDiplome = inclusionsDiplomesValidesPour(editingContext,instance);
				if (inclusionsDiplome.count() == 0) {
					throw new Exception("Pas d'inclusion de diplômes valide pour les collèges de cette instance");
				}
				// élection concernée par les diplômes, vérifier les diplômes de l'individu
				if (aIndividuDiplomes(carriere.toIndividu(),(NSArray)inclusionsDiplome.valueForKey(EOInclusionDiplome.DIPLOME_KEY)) == false) {
					return null;

				}
			}
			// 22/02/2011 - Déterminer le collège à partir individus types élection si ils existent, sinon des collèges-sections électives et du type de recrutement de l'individu
			// Si la carrière n'a pas de cnu, l'individu est rejeté
			EOSectionElective sectionElecteur = null;
			if (instance.typeInstance().estTypeJuridDiscHu()) {
				college = null;
				// Vérifier si il existe une information sur le type d'élection pour cet individu, elle est prioritaire sur les autres
				// informations
				NSArray individusType = EOIndividuTypeElectionHu.rechercherTypesPourIndividuEtDates(editingContext, carriere.toIndividu(), instance.dReference(), instance.dReference(), false);
				if (individusType.count() > 0) {
					// Il n'y a pas de chevauchement, on prend le premier qu'on trouve
					EOIndividuTypeElectionHu individuType = (EOIndividuTypeElectionHu)individusType.objectAtIndex(0);
					LogManager.logDetail("type election individu " + individuType.typeElection().tehuCode());
					// Rechercher un collège avec ce nom
					for (java.util.Enumeration<EOCollege> e = colleges.objectEnumerator();e.hasMoreElements();) {
						EOCollege collegeCourant=e.nextElement();
						if (collegeCourant.lcCollege().equals(individuType.typeElection().tehuCode())) {
							college = collegeCourant;
							LogManager.logDetail("college trouve avec type election hu " + college);
							break;
						}
					}
				} else {
					if (element.toCarriere().toCnu() == null) {
						return null;
					}
					LogManager.logDetail("cnu de l'element de carriere " + carriere.toCnu());
					LogManager.logDetail("type recrutement " + carriere.typeRecrutement());
					for (java.util.Enumeration<EOSectionElective> e = sectionsElectives.objectEnumerator();e.hasMoreElements();) {
						EOSectionElective section = e.nextElement();
						LogManager.logDetail("section Elective " + section.llSectionElective());
						if (((NSArray)section.valueForKey("cnus")).containsObject(element.toCarriere().toCnu())) {
							// Rechercher la section élective qui comporte la cnu de la carrière : normalement, il n'y en a qu'une.
							// si il y en a plusieurs, on prend celle qui a un ou plusieurs collèges associés
							NSArray collegesSection = EOCollegeSectionElective.findForSection(editingContext, section, false);
							for (java.util.Enumeration<EOCollegeSectionElective> e1 = collegesSection.objectEnumerator();e1.hasMoreElements();) {
								EOCollegeSectionElective collegeSection = e1.nextElement();
								LogManager.logDetail("college section " + collegeSection);
								if (colleges.containsObject(collegeSection.college())) {
									// C'est un collège valide pour le paramétrage
									if (collegeSection.typeRecrutement() == null || collegeSection.typeRecrutement() == carriere.typeRecrutement()) {
										// On a trouvé le collège qui correspond au type de recrutement
										college = collegeSection.college();
										sectionElecteur = collegeSection.sectionElective();
										break;
									}
								}
							}
						}
						if (college != null) {
							LogManager.logDetail("college trouve " + college);
							break;
						}
					}
				}
				if (college == null) {	// on n'a pas réussi à trouver de collège ou les types de recrutement ne sont pas définis
					LogManager.logDetail("Pas de collège trouvé");
					return null;
				}
			}
			// On peut maintenant créer un électeur et lui ajouter un certain nombre de caractéristiques
			EOListeElecteur electeur = new EOListeElecteur();
			electeur.initAvecIndividuEtInstance(carriere.toIndividu(),instance);
			electeur.setCollegeRelationship(college);
			electeur.setCarriereRelationship(carriere);
			electeur.setCorpsRelationship(element.toCorps());
			electeur.setGradeRelationship(element.toGrade());
			if (instance.typeInstance().estElcCnu() || instance.typeInstance().estTypeJuridDiscHu()) {
				if (element.toCarriere().toCnu() != null) {
					electeur.setCnuRelationship(element.toCarriere().toCnu());
				}
			}
			if (instance.typeInstance().estTypeJuridDiscHu() && sectionElecteur != null) {
				electeur.setSectionElectiveRelationship(sectionElecteur);
			}
			if (inclusion.groupeExclusion() != null) {
				preparerExclusions(electeur,inclusion.groupeExclusion(),instance.dReference(),true);
			}
			return electeur;
		}
	}

	/**
	 * 
	 * @param instance
	 * @param contrat
	 * @return
	 * @throws Exception
	 */
	private static EOListeElecteur electeurValidePourInstanceEtContratHeberge(EOInstance instance, EOContratHeberges contrat) throws Exception {

		EOEditingContext edc = instance.editingContext();
		EOCollege college = null;

		// On recherche une inclusion valide pour ce corps
		EOInclusionCorps inclusionCorps = inclusionCorpsValidePour(edc, instance, contrat.toCorps());
		if (inclusionCorps == null) {
			EOInclusionContrat inclusionContrat = inclusionContratValidePour(edc, instance, contrat.typeContratTravail());
			if (inclusionContrat == null) {
				return null;
			}
			else
				college = inclusionContrat.college();
		}
		else
			college = inclusionCorps.college();

		// On peut maintenant créer un électeur et lui ajouter un certain nombre de caractéristiques
		EOListeElecteur electeur = new EOListeElecteur();
		electeur.initAvecIndividuEtInstance(contrat.individu(),instance);
		electeur.setCollegeRelationship(college);
		if (contrat.toGrade() != null) {
			electeur.setCorpsRelationship(contrat.toGrade().toCorps());
			electeur.setGradeRelationship(contrat.toGrade());
		}
		
		electeur.setToContratHebergeRelationship(contrat);

		return electeur;

	}


	/**
	 * 
	 * @param instance
	 * @param contrat
	 * @return
	 * @throws Exception
	 */
	private static EOListeElecteur electeurValidePourInstanceEtContrat(EOInstance instance,EOContrat contrat) throws Exception {
		EOEditingContext edc = instance.editingContext();
		NSArray<EOContratAvenant> avenants = contrat.avenantsPourPeriode(instance.dReference(), instance.dReference());
		// Normalement, il n'y a qu'un avenant à une date donnée
		if (avenants == null || avenants.count() == 0) {
			return null;
		} else {
			EOContratAvenant avenant = (EOContratAvenant)avenants.objectAtIndex(0);
			// On recherche une inclusion valide pour ce type de contrat de travail
			EOInclusionContrat inclusion = inclusionContratValidePour(edc,instance,contrat.toTypeContratTravail());
			if (inclusion == null) {
				return null;
			}
			EOCollege college = inclusion.college();
			if (instance.typeInstance().estElcCs()) {	// pour les conseils scientifiques
				// vérifier les inclusions de diplôme et ensuite vérifier les diplômes de l'individu
				NSArray inclusionsDiplome = inclusionsDiplomesValidesPour(edc,instance);
				if (inclusionsDiplome.count() == 0) {
					throw new Exception("Pas d'inclusion de diplômes valide pour les collèges de cette instance");
				}
				// élection concernée par les diplômes, vérifier les diplômes de l'individu
				if (aIndividuDiplomes(contrat.toIndividu(),(NSArray)inclusionsDiplome.valueForKey(EOInclusionDiplome.DIPLOME_KEY)) == false) {
					return null;

				}
			}
			// 22/02/2011 - Déterminer le collège à partir des individus type élection si ils existent, sinon des collèges-sections électives et du type de recrutement de l'individu
			// Si le contrat n'a pas de cnu, l'individu est rejeté
			EOSectionElective sectionElecteur = null;
			if (instance.typeInstance().estTypeJuridDiscHu()) {
				college = null;
				// Vérifier si il existe une information sur le type d'élection pour cet individu, elle est prioritaire sur les autres
				// informations
				NSArray individusType = EOIndividuTypeElectionHu.rechercherTypesPourIndividuEtDates(edc, contrat.toIndividu(), instance.dReference(), instance.dReference(), false);
				if (individusType.count() > 0) {
					// Il n'y a pas de chevauchement, on prend le premier qu'on trouve
					EOIndividuTypeElectionHu individuType = (EOIndividuTypeElectionHu)individusType.objectAtIndex(0);
					LogManager.logDetail("type election individu " + individuType.typeElection().tehuCode());
					// Rechercher un collège avec ce nom
					for (java.util.Enumeration<EOCollege> e = colleges.objectEnumerator();e.hasMoreElements();) {
						EOCollege collegeCourant = e.nextElement();
						if (collegeCourant.lcCollege().equals(individuType.typeElection().tehuCode())) {
							college = collegeCourant;
							LogManager.logDetail("college trouve avec type election hu " + college);
							break;
						}
					}
				} else {
					if (avenant.toCnu() == null) {
						return null;
					}
					LogManager.logDetail("cnu de l'avenant " + avenant.toCnu().code());
					if (contrat.typeRecrutement() != null) {
						LogManager.logDetail("type recrutement " + contrat.typeRecrutement().trhuCode());
					} else {
						LogManager.logDetail("pas de type recrutement dans le contrat");
					}
					NSArray<EOParamSectionElective> paramsSection = ParametrageElection.rechercherParametragesActifsPourInstance(edc, EOParamSectionElective.ENTITY_NAME, instance);
					NSArray<EOSectionElective> sectionsElectives = (NSArray)paramsSection.valueForKey("sectionElective");
					for (java.util.Enumeration<EOSectionElective> e = sectionsElectives.objectEnumerator();e.hasMoreElements();) {
						EOSectionElective section = e.nextElement();
						LogManager.logDetail("section Elective " + section.llSectionElective());
						if (((NSArray)section.valueForKey("cnus")).containsObject(avenant.toCnu())) {
							// Rechercher la section élective qui comporte la cnu de l'avenant : normalement, il n'y en a qu'une.
							// si il y en a plusieurs, on prend celle qui a un ou plusieurs collèges associés
							NSArray collegesSection = EOCollegeSectionElective.findForSection(edc, section, false);
							for (java.util.Enumeration<EOCollegeSectionElective> e1 = collegesSection.objectEnumerator();e1.hasMoreElements();) {
								EOCollegeSectionElective collegeSection = e1.nextElement();
								LogManager.logDetail("college section " + collegeSection);
								if (colleges.containsObject(collegeSection.college())) {
									// C'est un collège valide pour le paramétrage
									if (collegeSection.typeRecrutement() == null || collegeSection.typeRecrutement() == contrat.typeRecrutement()) {
										// On a trouvé le collège qui correspond au type de recrutement
										college = collegeSection.college();
										sectionElecteur = collegeSection.sectionElective();
										break;
									}
								}
							}
						}
						if (college != null) {
							LogManager.logDetail("college trouve " + college);
							break;
						} 
					}
				}
				if (college == null) {	// on n'a pas réussi à trouver de collège ou les types de recrutement ne sont pas définis
					LogManager.logDetail("Pas de collège trouvé");
					return null;
				}
			}
			// vérifier les contraintes du contrat pour le collège sauf pour les élections JDNPHU et JDNPEH
			if (instance.typeInstance().estTypeJuridDiscHu() == false && instance.typeInstance().estTypeJuridDiscHuOd() == false && !contratValidePourCollegeEtDate(contrat,inclusion.college(),instance.dReference())) {
				return null;
			}
			// On peut maintenant créer un électeur et lui ajouter un certain nombre de caractéristiques
			EOListeElecteur electeur = new EOListeElecteur();
			electeur.initAvecIndividuEtInstance(contrat.toIndividu(),instance);
			electeur.setCollegeRelationship(college);
			electeur.setContratRelationship(contrat);
			if (avenant.toGrade() != null) {
				electeur.setCorpsRelationship(avenant.toGrade().toCorps());
				electeur.setGradeRelationship(avenant.toGrade());
			}
			if (instance.typeInstance().estTypeJuridDiscHu() && sectionElecteur != null) {
				electeur.setSectionElectiveRelationship(sectionElecteur);
			}
			if (instance.typeInstance().estTypeJuridDiscHu()) {
				if (avenant.toCnu() != null) {
					electeur.setCnuRelationship(avenant.toCnu());
				}
			}
			if (inclusion.groupeExclusion() != null) {
				preparerExclusions(electeur,inclusion.groupeExclusion(),instance.dReference(),false);
			}
			return electeur;
		}
	}
	//	une inclusion est valide si elle est définie pour ce corps et que le collège associé fait partie
	//	des collèges paramétrés pour cette instance
	private static EOInclusionCorps inclusionCorpsValidePour(EOEditingContext editingContext,EOInstance instance,EOCorps corps) {
		NSArray<EOInclusionCorps> inclusions = EOInclusionCorps.rechercherInclusionsPourTypeInstanceEtCorps(editingContext,instance.typeInstance(),corps);
		if (inclusions.count() == 0) {
			return null;
		}
		// trier par ordre de collège croissant
		inclusions = EOSortOrdering.sortedArrayUsingKeyOrderArray(inclusions, new NSArray(EOSortOrdering.sortOrderingWithKey(EOListeElecteur.COLLEGE_KEY + "." + EOCollege.NUM_ELC_ORDRE_KEY, EOSortOrdering.CompareAscending)));
		NSArray colleges = (NSArray)ParametrageElection.rechercherParametragesActifsPourInstance(editingContext, EOParamCollege.ENTITY_NAME, instance).valueForKey("college");
		for (java.util.Enumeration<EOInclusionCorps> e = inclusions.objectEnumerator();e.hasMoreElements();) {
			EOInclusionCorps inclusion = e.nextElement();
			if (colleges.containsObject(inclusion.college())) {
				return inclusion;
			}
		}
		return null;
	}
	
	//	une inclusion est valide si elle est définie pour ce type de contrat de travail et que le collège associé fait partie
	//	des collèges paramétrés pour cette instance
	private static EOInclusionContrat inclusionContratValidePour(EOEditingContext editingContext,EOInstance instance,EOTypeContratTravail typeContrat) {
		
		if (typeContrat == null)
			return null;
		
		NSArray<EOInclusionContrat> inclusions = EOInclusionContrat.rechercherInclusionsPourTypeInstanceEtTypeContrat(editingContext,instance.typeInstance(),typeContrat);
		if (inclusions.count() == 0) {
			return null;
		}
		// trier par ordre de collège croissant
		inclusions = EOSortOrdering.sortedArrayUsingKeyOrderArray(inclusions, new NSArray(EOSortOrdering.sortOrderingWithKey(EOListeElecteur.COLLEGE_KEY + "." + EOCollege.NUM_ELC_ORDRE_KEY, EOSortOrdering.CompareAscending)));
		NSArray colleges = (NSArray)ParametrageElection.rechercherParametragesActifsPourInstance(editingContext, EOParamCollege.ENTITY_NAME, instance).valueForKey("college");
		for (java.util.Enumeration<EOInclusionContrat> e = inclusions.objectEnumerator();e.hasMoreElements();) {
			EOInclusionContrat inclusion = e.nextElement();
			if (colleges.containsObject(inclusion.college())) {
				return inclusion;
			}
		}
		return null;
	}
	//	recherche les inclusions de diplômes pour les collèges paramétrés pour cet instance
	private static NSArray inclusionsDiplomesValidesPour(EOEditingContext editingContext, EOInstance instance) {
		NSArray paramsColleges = ParametrageElection.rechercherParametragesActifsPourInstance(editingContext, EOParamCollege.ENTITY_NAME, instance);
		EOQualifier qualifier = SuperFinder.construireORQualifier(EOListeElecteur.COLLEGE_KEY + "." + EOCollege.LL_COLLEGE_KEY, (NSArray)((NSArray)paramsColleges.valueForKey("college")).valueForKey("llCollege"));
		EOFetchSpecification fs = new EOFetchSpecification(EOInclusionDiplome.ENTITY_NAME, qualifier,null);
		return editingContext.objectsWithFetchSpecification(fs);
	}

	/**
	 * 
	 * @param individu
	 * @param diplomes
	 * @return
	 */
	private static boolean aIndividuDiplomes(EOIndividu individu,NSArray diplomes) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(SuperFinder.construireORQualifier(EOIndividuDiplomes.DIPLOME_KEY + "." + EODiplomes.LIBELLE_LONG_KEY,(NSArray)diplomes.valueForKey(EODiplomes.LIBELLE_LONG_KEY)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuDiplomes.INDIVIDU_KEY +"=%@", new NSArray(individu)));

		return EOIndividuDiplomes.fetchAll(individu.editingContext(), new EOAndQualifier(qualifiers)).count() > 0;
	}

	//	vérifie les règles de durée des contrats
	private static boolean contratValidePourCollegeEtDate(EOContrat contrat, EOCollege college,NSTimestamp dateReference) throws Exception {
		if (college.dureeContrat() == null & college.dureeMinContrat() == null) {
			throw new Exception("Le collège " + college.llCollege() + " n'a pas de durée de contrat et de durée minimum");
		}
		NSArray<EOContrat> contrats = EOContrat.rechercherContratsIndividuAnterieursADate(contrat.editingContext(), contrat.toIndividu(), contrat.dateDebut());
		// Rechercher le dernier contrat de rémunération principale
		contrats = EOSortOrdering.sortedArrayUsingKeyOrderArray(contrats, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		EOContrat contratPrecedent = null;
		for (EOContrat unContrat : contrats) {
				contratPrecedent = unContrat;
				break;
		}
		if (college.dureeContrat() != null) {
			if (!dureeValide(contrat.dateDebut(),contrat.dateFin(),college.dureeContrat().intValue(),college.dureeInterruptionContrat(),contratPrecedent)) {
				return false;
			}
		}
		if (college.dureeMinContrat()!= null) {
			if (!dureeValide(contrat.dateDebut(),dateReference,college.dureeMinContrat().intValue(),college.dureeInterruptionContrat(),contratPrecedent)) {
				return false;
			}
		}

		// le contrat satisfait toutes les conditions
		// i.e plus long que la durée minimum et durée écoulée à la date de référence
		//  plus longue que la durée minimum requise (ou bien les deux durées en prenant en compte le contrat
		// précédent avec une durée d'interruption inférieure à la durée max. d'interruption) satisfont les contraintes
		return true;
	}
	
	//	vérifie si la durée en mois écoulée entre la date début et fin est supérieure à la durée minimum.
	//	Si ce n'est pas le cas, prend en compte la durée du contrat précédent si la durée d'interruption entre les
	//	deux contrats est inférieure à la durée max. d'interruption
	private static boolean dureeValide(NSTimestamp dateDebut, NSTimestamp dateFin, int dureeMiniEnMois, Number dureeInterruption, EOContrat contratPrecedent) {

		if (dureeMiniEnMois > 0) {
			
			Integer val = DateCtrl.calculerDureeEnMois(dateDebut, dateFin,true);
			LogManager.logDetail("duree en mois : " + val);
			LogManager.logDetail("duree mini en mois : " + dureeMiniEnMois);
			if (val != null) {
				int dureeContrat = val.intValue();
				if (dureeContrat < dureeMiniEnMois) {
					if (dureeInterruption == null || contratPrecedent == null) {
						// pas d'interruption tolérée ou pas de contrat précédent
						return false;
					}
					int dureeMaxiInterruption = dureeInterruption.intValue();
					if (dureeMaxiInterruption == 0) {
						// les deux contrats doivent être consécutifs
						return DateCtrl.dateToString(DateCtrl.jourSuivant(contratPrecedent.dateFin())).equals(DateCtrl.dateToString((dateDebut)));
					}
					// Calculer la durée d'interruption en mois entre les deux contrats
					int dureeInterruptionContrat = DateCtrl.calculerDureeEnMois(contratPrecedent.dateFin(), dateDebut,true).intValue(); // 13/01/2011 - Inclure la date de fin
					if (dureeInterruptionContrat > dureeMaxiInterruption) {
						// durée d'interruption supérieure au maximum toléré
						return false;
					} else {
						if (contratPrecedent != null && contratPrecedent.dateDebut() != null && contratPrecedent.dateFin() != null) {
							dureeContrat = dureeContrat + DateCtrl.calculerDureeEnMois(contratPrecedent.dateDebut(), contratPrecedent.dateFin(),true).intValue(); 
							return dureeContrat > dureeMiniEnMois;
						}
						else {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	//	recherche les exclusions associées au groupe d'exclusion et rechercher les absences pour le
	//	type d'exclusion. Si on en trouve, on marque le type d'exclusion dans liste electeur
	private static void preparerExclusions(EOListeElecteur electeur,EOGroupeExclusion groupeExclusion,NSTimestamp dateReference,boolean estTitulaire) {
		EOEditingContext editingContext = groupeExclusion.editingContext();
		NSArray exclusions = EOExclusion.findForGroupe(editingContext, groupeExclusion);
		for (java.util.Enumeration<EOExclusion> e = exclusions.objectEnumerator();e.hasMoreElements();) {
			EOExclusion exclusion = e.nextElement();
			if ((estTitulaire && exclusion.typeExclusion().estPourFonctionnaire()) ||
					(!estTitulaire && !exclusion.typeExclusion().estPourFonctionnaire())) {
				// rechercher si il existe des absences avec le même type d'exclusion pour cet individu
				NSArray absences = EOAbsences.rechercherAbsencePourIndividuDateEtTypeExclusion(editingContext,electeur.individu(), dateReference, exclusion.typeExclusion());
				if (absences.count() > 0) {
					electeur.setCTypeExclusion(((EOAbsences)absences.objectAtIndex(0)).typeExclusion().cTypeExclusion());
					return;	// on a trouvé une exclusion. A priori, il n'existe qu'une absence à une date donnée
				}
			}	
		}	
	}
	private static void affecterInformations(EOListeElecteur electeur,EOInstance instance,NSArray paramsRepart,EOStructure structure) {
		EOEditingContext editingContext = structure.editingContext();
		// ajouter la structure
		electeur.setStructureRelationship(structure);
		for (java.util.Enumeration<EOParamRepartElec> e = paramsRepart.objectEnumerator();e.hasMoreElements();) {
			EOParamRepartElec paramRepart = e.nextElement();
			if (paramRepart.structure() == structure) {
				// ajouter la composante élective et le bureau de vote
				electeur.setComposanteElectiveRelationship(paramRepart.composanteElective());
				electeur.setBureauVoteRelationship(paramRepart.bureauVote());
				// si l'instance est sectorisése, ajouter le secteur correspondant à la composante
				// élective et au collège (le secteur est choisi parmi les paramétrages retenus
				if (instance.typeInstance().estSectorise()) {
					NSArray regroupements = EORegroupementSecteur.rechercherRegroupementsPourComposanteElective(editingContext, paramRepart.composanteElective());
					NSArray paramSecteurs = ParametrageElection.rechercherParametragesActifsPourInstance(editingContext, "ParamSecteur", instance);
					for (java.util.Enumeration<EOParamSecteur> e1 = paramSecteurs.objectEnumerator();e1.hasMoreElements();) {
						EOParamSecteur paramSecteur = e1.nextElement();
						for (java.util.Enumeration<EORegroupementSecteur> e2 = regroupements.objectEnumerator();e2.hasMoreElements();) {
							EORegroupementSecteur regroupement = e2.nextElement();
							if (regroupement.secteur() == paramSecteur.secteur() && electeur.college() == paramSecteur.college()) {
								electeur.setSecteurRelationship(regroupement.secteur());
								break;
							}
						}
					}
				}
				break;
			}
		}
		// 25/02/2011 - ne pas modifier les valeurs trouvées pour les élections JDNPHU
		if (instance.typeInstance().estElcCses() && instance.typeInstance().estTypeJuridDiscHu() == false) {
			// rajouter la sélection élective
			NSArray sectionsElec = ParametrageElection.rechercherParametragesActifsPourInstance(editingContext, "ParamSectionElective", instance);
			if (sectionsElec.count() > 0) {
				EOParamSectionElective param = (EOParamSectionElective)sectionsElec.objectAtIndex(0);
				electeur.setSectionElectiveRelationship(param.sectionElective());
			}
		}
	}
	/** Invalide toutes les instances auxquels sont rattaches les electeurs pour un type d'objet donne
	 * (college,bureauVote,composanteElective,secteur,sectionElective).
	 * @param editingContext
	 * @param relationRecherchee
	 * @param objetRecherche
	 */
	public static void invaliderInstances(EOEditingContext editingContext,String relationRecherchee,EOGenericRecord objetRecherche) {
		if (relationRecherchee == null || objetRecherche == null) {
			return;
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(relationRecherchee + " = %@", new NSArray(objetRecherche));
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
		NSArray electeurs = editingContext.objectsWithFetchSpecification(fs);
		if (electeurs != null && electeurs.count() > 0) {
			LogManager.logDetail("Invalidation des instances pour " + relationRecherchee + " sur l'objet de type " + objetRecherche.getClass().getName());
			invaliderInstances((NSArray)electeurs.valueForKey(INSTANCE_KEY));
		}
	}
	/** Invalide toutes les instances auxquels sont rattach&eacute;s les &eacute;lecteurs pour un type d'objet donn&eacute;
	 * (college,bureauVote,composanteElective,secteur,sectionElective).
	 * @param editingContext
	 * @param relationRecherchee
	 * @param objetRecherche
	 */
	public static void invaliderInstancesEtSuppressionElecteurs(EOEditingContext editingContext,String relationRecherchee,EOGenericRecord objetRecherche) {
		if (relationRecherchee == null || objetRecherche == null) {
			return;
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(relationRecherchee + " = %@", new NSArray(objetRecherche));
		EOFetchSpecification fs = new EOFetchSpecification("ListeElecteur",qualifier,null);
		NSArray electeurs = editingContext.objectsWithFetchSpecification(fs);
		if (electeurs != null && electeurs.count() > 0) {
			invaliderInstances((NSArray)electeurs.valueForKey("instance"));
			// 30/03/2010
			LogManager.logDetail("Suppression des électeurs suite à suppression de l'objet de type " + relationRecherchee);
			for (java.util.Enumeration<EOListeElecteur> e = electeurs.objectEnumerator();e.hasMoreElements();) {
				EOListeElecteur electeur = e.nextElement();
				electeur.supprimerRelations();
				editingContext.deleteObject(electeur);
			}
		}
	}
	// 30/03/2010
	private static void invaliderInstances(NSArray instances) {
		NSMutableArray instancesAInvalider = new NSMutableArray();
		for (java.util.Enumeration<EOInstance> e = instances.objectEnumerator();e.hasMoreElements();) {
			EOInstance instance = e.nextElement();
			if (instancesAInvalider.containsObject(instance) == false) {
				instancesAInvalider.addObject(instance);
			}
		}
		for (Enumeration<EOInstance> e1 = instancesAInvalider.objectEnumerator();e1.hasMoreElements();) {
			EOInstance instance = e1.nextElement();
			instance.setEstEditionCoherente(false);
		}
	}
}

// EOEmeritat.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
/** G&egrave; l'&eacute;m&eacute;ritat pour les individus dont le corps est d&eacute;fini dans la table CORPS_EMERITE:<BR>
 * La date d&eacute;but et la date de fin doivent &ecir;tre fournies<BR>
 * V&eacute;rification de dates :<BR>
 * la date de d&eacute;but doit &ecirc;tre post&eacute;rieure &agrave; la date de mise en retraite<BR>
 * V&eacute;rifie que l'agent fait partie du corps des professeurs d'universit&eacute; <BR>
 * @author christine
 *
 */

package org.cocktail.mangue.modele.mangue.prolongations;

import java.util.Enumeration;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOCorpsEmerite;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

// 31/01/2011 - ajout des hospitalo-univ et des visas
public class EOEmeritat extends _EOEmeritat{
	private static String TYPE_EMERITAT = "EMERITAT";

	public EOEmeritat() {
		super();
	}

	public static EOEmeritat findForKey( EOEditingContext ec, Number key) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EME_ORDRE_KEY + "=%@", new NSArray(key)));
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}

	public static EOEmeritat creer(EOEditingContext ec, EOIndividu individu) {

		EOEmeritat newObject = (EOEmeritat) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setDCreation(new NSTimestamp());
		newObject.setTemValide("O");

		return newObject;
	}


	public String typeEvenement() {
		return TYPE_EMERITAT;
	}
	public String dateDecisionFormatee() {
		return SuperFinder.dateFormatee(this,D_DECISION_KEY);
	}
	public void setDateDecisionFormatee(String uneDate) {
		if (uneDate == null) {
			setDateDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,D_DECISION_KEY,uneDate);
		}
	}
	public NSArray visas() {
		// Rechercher le dernier segment de carrière de l'individu
		EOCarriere carriere = dernierSegmentCarriere();
		if (carriere == null) {
			return new NSArray();
		} else {
			EOElementCarriere element = carriere.dernierElement();
			if (element == null) {
				return new NSArray();
			} else {
				return EOVisa.rechercherVisaPourTypeArreteTypePopulationEtCorps(editingContext(),TYPE_EMERITAT,carriere.toTypePopulation().code(),element.toCorps().cCorps());
			}
		}
	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		super.validateForSave();
		if (commentaire() != null && commentaire().length() > 2000) {
			throw new NSValidation.ValidationException("Le commentaire ne peut excéder 2000 caractères");
		}
		EOCarriere carriere = dernierSegmentCarriere();
		if (carriere == null) {
			throw new NSValidation.ValidationException("Pas de segment de carrière antérieur à la date de début");
		}

		NSArray<EODepart> departs = EODepart.rechercherDepartsValidesPourIndividuEtPeriode(editingContext(), individu(), dateDebut(), dateFin());
		if (departs.count() == 0) {
			throw new NSValidation.ValidationException("Cet individu n'est pas en retraite à cette date");
		} else {			
			EODepart depart = departs.get(0);
			if (depart.motifDepart().estRetraite() == false) {
				throw new NSValidation.ValidationException("Le motif de départ n'est pas \"Retraite\"");
			}
		}
		// Vérifier si le dernier élément de carrière correspond a un corps emerite
		EOElementCarriere element = carriere.dernierElement();
		if (EOCorpsEmerite.rechercherCorpsEmeritePourCorpsEtPeriode(editingContext(),element.toCorps(), dateDebut(), dateFin()) == null) {
			throw new NSValidation.ValidationException(element.toCorps().llCorps() + " n'est pas défini comme un corps Emérite");
		}
		// Vérifier que la date de début est postérieure à la date de fin de la carrière
		if (carriere.dateFin() == null || DateCtrl.isBeforeEq(dateDebut(), carriere.dateFin())) {
			throw new NSValidation.ValidationException("L'éméritat ne peut commencer qu'après la fin de la carrière. Modifier la date de début");
		}
		// Vérifier si il y a chevauchement des périodes
		NSArray<EOEmeritat> periodesEmeritat = fetchForIndividu(editingContext(),individu());
		for ( java.util.Enumeration<EOEmeritat> e = periodesEmeritat.objectEnumerator();e.hasMoreElements();) {
			EOEmeritat emeritat = e.nextElement();
			if (emeritat != this && Utilitaires.IntervalleTemps.intersectionPeriodes(dateDebut(), dateFin(), emeritat.dateDebut(), emeritat.dateFin()) != null) {
				throw new NSValidation.ValidationException("Les périodes d'éméritat ne peuvent se chevaucher");
			}
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		//individu().setDModification(new NSTimestamp());		

	}
	protected void init() {

	}
	// Méthodes privées
	public EOCarriere dernierSegmentCarriere() {
		// Rechercher les segments de carrière antérieurs à la date de début et vérifier :
		// si il y a un départ pour retraite
		// si le corps de l'individu dans les éléments de carrière est professeur d'université
		NSArray carrieres = EOCarriere.rechercherCarrieresPourIndividuAnterieursADate(editingContext(), individu(), dateDebut());
		if (carrieres.count() == 0) {
			return null;
		}
		// Trier les segments par ordre de date début croissante, le dernier segment est celui qui nous intéresse
		carrieres = EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		return (EOCarriere)carrieres.lastObject();
	}
	// Méthodes statiques
	public static NSArray fetchForIndividu(EOEditingContext ec,EOIndividu individu) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu));
		return fetchAll(ec, qualifier);
	}
	
	/** Retourne les periodes d'emeritat d'un individu pour la periode passee en parametre */
	public static NSArray fetchForIndividuAndPeriode(EOEditingContext ec,EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu));
		if (dateDebut != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateDebut,DATE_FIN_KEY,dateFin));
		}		
		return fetchAll(ec,new EOAndQualifier(qualifiers),SORT_ARRAY_DATE_DEBUT_DESC );		
	}	

	/** Retourne le qualifier pour les corps qui peuvent beneficier de l'emeritat
	 * 
	 * @return
	 */
	public static EOQualifier qualifierPourCorps(EOEditingContext editingContext) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSArray corpsEmerites = EOCorpsEmerite.fetchAll(editingContext);
		for (Enumeration<EOCorpsEmerite> e = corpsEmerites.objectEnumerator();e.hasMoreElements();) {
			EOCorpsEmerite corpsEmerite = e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps = %@", new NSArray(corpsEmerite.corps())));
		}
		return new EOOrQualifier(qualifiers);
	}



}

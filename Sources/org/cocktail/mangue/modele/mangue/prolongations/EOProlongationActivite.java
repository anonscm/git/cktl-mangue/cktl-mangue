//EOProlongationActivite.java
//Created on Wed Feb 18 13:27:03 Europe/Paris 2009 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.prolongations;

import java.util.Enumeration;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeMotProlongation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.Outils;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation<BR>
 * 1. La date de fin doit etre superieure a la date de debut.<BR>
 * 2. La date de fin reelle doit etre superieure a la date de debut.<BR>
 * 3. La date de fin reelle doit etre inferieure a la date de fin.<BR>
 * 4. Les prolongations d'activite commencent apres les periodes de recul d'age.<BR>
 * Regles de validation pour le surnombre<BR>
 * 4. Une periode de surnombre ne peut etre accordee qu'aux fonctionnaires de certains corps.<BR>
 * 5. Une periode de surnombre ne peut pas etre accordee a un enseignant stagiaire.<BR>
 * 6. La duree maximale d'une  periode de surnombre est de 3 ans mais elle peut durer jusqu'au 31/08.<BR>
 * 7. La periode de surnombre doit reposer sur une position d'activite ou de detachement avec carriere d'accueil.<BR> 
 * 8. Si l'agent beneficie d'une ou plusieurs periodes de recul de limite age alors la date de debut est egale la date de fin maximale de la ou des periodes de recul limite age + 1 jour.<BR>
 * 9. Si l'agent ne beneficie pas de periode de recul limite age alors la date de debut de la periode est egale au lendemain de son  65eme anniversaire.<BR>
 * 10. Si il existe une periode de maintien en fonction alors la date de fin est le jour precedent le debut de la periode de maintien en fonction.<BR>
 * 11. L'agent doit occuper un emploi de type Surnombre.<BR>
 * 12. L'agent ne doit pas avoir plus de 65 ans ou si il existe une prolongation de type Article 69, 65 ans + la dur&eacute;e de cette prolongation.<BR>
 * 13. La duree cumulee d'une prolongation de type Article 69 et de type Surnombre ne peut d&eacute;passer 3 ans<BR>
 * Regles de validation pour le maintien en fonction<BR>
 * 14. Une periode de maintien en fonction ne peut etre accordee qu'aux fonctionnaires enseignants.<BR>
 * 16. Si l'agent beneficie d'une periode de surnombre alors la date de debut est 
 *    egale a la date de fin du surnombre + 1 jour.<BR>
 * 17. Si l'agent beneficie d'une ou plusieurs periodes de recul de limite age et en l'absence de periode de surnombre 
 *    alors la date de debut est egale la date de fin maximale de la ou des periodes de recul limite age + 1 jour.<BR>
 * 18. Si l'agent ne beneficie pas de periode de surnombre et de recul limite age alors la date de debut 
 *    est egale au lendemain de son 60 ou 61 ou 62 ou 63 ou 64 ou 65eme anniversaire.<BR>
 * 19. La periode de maintien en fonction doit reposer sur une position d'activite ou de detachement avec carriere d'accueil.<BR>
 * 20. La durée de maintien en fonction ne peut être supérieure à 1 an<BR>
 * 21. Le motif est obligatoire<BR>
 *
 */
// 05/03/2010 - correction d'un bug lorsqu'on dépasse l'âge limite de surnombre sur la vérification des dates
// 24/09/2010 - prise en compte du nombre d'années de limite d'âge pour les maintiens en fonction dans l'intérêt du service (DT 2840)
public class EOProlongationActivite extends _EOProlongationActivite {

	public static EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(DATE_FIN_KEY, EOSortOrdering.CompareDescending);

	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_ASC = new NSArray<EOSortOrdering>(SORT_DATE_DEBUT_ASC);
	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_DESC = new NSArray<EOSortOrdering>(SORT_DATE_DEBUT_DESC);

	private final static String TYPE_EVENEMENT = "PROLONG";

	public EOProlongationActivite() {
		super();
	}

	// méthodes ajoutées
	public String typeEvenement() {
		return TYPE_EVENEMENT;
	}

	public static EOProlongationActivite creer(EOEditingContext ec, EOIndividu individu) {

		EOProlongationActivite newObject = (EOProlongationActivite) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setDCreation(new NSTimestamp());
		newObject.setTemValide("O");

		return newObject;
	}

	public static EOProlongationActivite rechercherPourAbsence(EOEditingContext ec, EOAbsences absence) {
		return fetchFirstByQualifier(ec, EOQualifier.qualifierWithQualifierFormat(TO_ABSENCE_KEY + "=%@", new NSArray(absence)));
	}

	public static EOProlongationActivite findForKey( EOEditingContext ec, Number key) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PROL_ORDRE_KEY + "=%@", new NSArray(key)));
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}


	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOProlongationActivite> findForIndividu(EOEditingContext ec, EOIndividu individu) {
		return findForIndividu(ec, individu, null, null);
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray<EOProlongationActivite> findForIndividu(EOEditingContext ec, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();	
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@",new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@",new NSArray(individu)));

		if (debutPeriode != null)
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode));

		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_ASC);
	}


	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOProlongationActivite> rechercherSurnombrePourPeriode(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + "<=%@", new NSArray(dateFin)));

		if (dateDebut != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + ">=%@", new NSArray(dateDebut)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MOTIF_KEY + "=%@", new NSArray(EOTypeMotProlongation.rechercherTypeProlongation(ec, EOTypeMotProlongation.TYPE_SURNOMBRE))));

		return fetchAll(ec, new EOAndQualifier(qualifiers), null);

	}


	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		super.validateForSave();
		int nbAnneesReculAge = 0;

		// Traitement des reculs d age
		try {

			// Un agent ne peut pas bénéficier d'une prolongation d'activité antérieure à une période de recul d'âge
			// 24/09/2010 - Modification pour éviter les refresh pendant le validateForSave
			NSArray<EOReculAge> reculsAge = EOReculAge.findForIndividu(editingContext(), individu());
			reculsAge = EOSortOrdering.sortedArrayUsingKeyOrderArray(reculsAge, EOReculAge.SORT_ARRAY_DATE_DEBUT_DESC);

			for (EOReculAge myReculAge : reculsAge) {
				if (DateCtrl.isBefore(dateDebut(),myReculAge.dateFin())) {
					throw new NSValidation.ValidationException(
							"Un agent ne peut pas bénéficier d'une période de prolongation d'activité antérieure à une période de recul d'âge.");
				}

				int nbJours = DateCtrl.nbJoursEntre(reculsAge.lastObject().dateDebut(), myReculAge.dateFin(), true, true);
				nbAnneesReculAge = nbJours/360;
			} 
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		// Vérifier qu'il n'existe pas d'autre type de prolongation d'activités du même type
		NSArray<EOProlongationActivite> prolongations = Duree.rechercherDureesPourIndividu(editingContext(),EOProlongationActivite.ENTITY_NAME,individu(),false);
		prolongations = EOSortOrdering.sortedArrayUsingKeyOrderArray(prolongations, EOReculAge.SORT_ARRAY_DATE_DEBUT);
		if (prolongations != null && prolongations.count() > 0) {

			for (java.util.Enumeration<EOProlongationActivite> e = prolongations.objectEnumerator();e.hasMoreElements();) {
				EOProlongationActivite prolongation = (EOProlongationActivite)e.nextElement();
				if (prolongation != this  && prolongation.motif() == this.motif()) {
					throw new NSValidation.ValidationException("Il existe une autre prolongation d'activité de même type.");
				}
			}

			// Vérifier si il n'y a pas de période de chevauchement des prolongations
			EOProlongationActivite periodePrecedente = null, periodeSuivante = null;
			boolean found = false;
			for (Enumeration<EOProlongationActivite> e1 = prolongations.objectEnumerator();e1.hasMoreElements();) {
				EOProlongationActivite prolongation = e1.nextElement();
				if (!found) {
					if (prolongation != this) {
						periodePrecedente = prolongation;
					} else {
						found = true;
					}
				} else {
					periodeSuivante = prolongation;
					break;
				}
			}
			if (periodePrecedente != null) {
				if (DateCtrl.isSameDay(dateDebut(),DateCtrl.jourSuivant(periodePrecedente.dateFin())) == false) {
					throw new NSValidation.ValidationException("Il ne peut pas  y avoir d'interruption entre deux périodes de prolongation d'activité");
				}
			}
			if (periodeSuivante != null) {
				if (DateCtrl.isSameDay(dateFin(),DateCtrl.jourPrecedent(periodeSuivante.dateDebut())) == false) {
					throw new NSValidation.ValidationException("Il ne peut pas  y avoir d'interruption entre deux périodes de prolongation d'activité");
				}
			}
		} else {
			// C'est la première prolongation, vérifier qu'elle commence bien le jour suivant la limite minimum
			int ageMaxi = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_PROLONG_SURNOMBRE_LIMITE_AGE) + nbAnneesReculAge;
			NSTimestamp dateLimite = DateCtrl.dateAvecAjoutAnnees(individu().dNaissance(),ageMaxi);
			// la date d'une prolongation pour surnombre doit commencer après la limite d'âge calculée à partir de la limite par défaut
			// à laquelle on a ajouté la durée de recul d'âge + 1 jour (commence le jour suivant)
			NSTimestamp jourSuivant = DateCtrl.jourSuivant(dateLimite);
			if (DateCtrl.isSameDay(dateDebut(), jourSuivant) == false) {
				throw new NSValidation.ValidationException("Cette prolongation doit commencer le jour suivant la limite d'âge, soit le " + DateCtrl.dateToString(jourSuivant));
			}
		}	

		if (motif().estSurnombre()) {
			validerSurnombre(prolongations);
		} else if (motif().estMaintienService()) {
			// 24/09/2010 - prise en compte du nombre d'années de limite d'âge pour les maintiens en fonction dans l'intérêt du service
			validerMaintienFonction(prolongations,nbAnneesReculAge);
		} else if (motif().estTypeArticle69()) {
			validerProlongationPourPension(prolongations);
		}
	}

	// Méthodes privées
	private void validerSurnombre(NSArray prolongations) throws NSValidation.ValidationException {


		// Vérifier si il existe une prolongation d'activités article 69 ou une prolongation pour maintien en fonction
		EOProlongationActivite prolongationPourArticle69 = null, prolongationPourMaintienService = null;
		for (Enumeration<EOProlongationActivite> e = prolongations.objectEnumerator();e.hasMoreElements();) {
			EOProlongationActivite prolongation = e.nextElement();
			if (prolongation.motif().estTypeArticle69()) {
				prolongationPourArticle69 = prolongation;
			} else if (prolongation.motif().estMaintienService()) {
				prolongationPourMaintienService = prolongation;
			}
		}

		int nbMois = 0;
		// Vérifier que le surnombre commence bien le jour suivant la prolongation pour article 69
		if (prolongationPourArticle69 != null) {
			if (DateCtrl.isSameDay(DateCtrl.jourSuivant(prolongationPourArticle69.dateFin()),dateDebut()) == false) {
				throw new NSValidation.ValidationException("Une prolongation pour surnombre doit commencer le jour suivant d'une prolongation Article 69");
			}
			nbMois = DateCtrl.calculerDureeEnMois(prolongationPourArticle69.dateDebut(), dateFin(),true).intValue();
		} else {
			nbMois = DateCtrl.calculerDureeEnMois(dateDebut(), dateFin(),true).intValue();
		}
		// Vérifier la durée maximum
		int nbAnneesMaxi = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_PROLONG_SURNOMBRE_DUREE);
		if (nbMois > (nbAnneesMaxi + 1) * 12) {
			if (prolongationPourArticle69 != null) {
				throw new NSValidation.ValidationException("Le cumul de durée d'une prolongation Article 69 et d'une prolongation pour Surnombre est au maximum de " + (nbAnneesMaxi +1) + " ans");
			} else {
				throw new NSValidation.ValidationException("La durée d'une prolongation pour Surnombre est au maximum de " + (nbAnneesMaxi + 1) + " ans");
			}
		} else if (nbMois > nbAnneesMaxi * 12) {
			// 05/03/2010 -
			NSTimestamp dateFin;
			if (prolongationPourArticle69 != null) {
				dateFin = DateCtrl.dateAvecAjoutMois(prolongationPourArticle69.dateDebut(), nbAnneesMaxi * 12);
			} else {
				dateFin = DateCtrl.dateAvecAjoutMois(dateDebut(), nbAnneesMaxi * 12);
			}
			String limite = DateCtrl.dateToString(dateFin);
			int lastDelim = limite.lastIndexOf("/");
			limite = "31/08/"  + limite.substring(lastDelim + 1);
			if (DateCtrl.dateToString(dateFin()).equals(limite) == false) {
				if (prolongationPourArticle69 != null) {
					throw new NSValidation.ValidationException("Une période de Surnombre dont la durée cumulée avec une prolongation Article 69 a dépassé la durée maximum (" + nbAnneesMaxi + " ans) doit se terminer le 31 août");
				} else {
					throw new NSValidation.ValidationException("Une période de Surnombre dont la durée a dépassé la durée maximum (" + nbAnneesMaxi + " ans) doit se terminer le 31 août");
				}
			}
		}
		if (prolongationPourMaintienService != null) {
			if (DateCtrl.isAfter(dateDebut(),prolongationPourMaintienService.dateDebut())) {
				throw new NSValidation.ValidationException("Une prolongation pour surnombre ne peut commencer après une prolongation pour maintien en service");
			}
		}
	}

	// 24/09/2010 - prise en compte du nombre d'années de limite d'âge pour les maintiens en fonction dans l'intérêt du service
	private void validerMaintienFonction(NSArray prolongations,int nbAnneesReculAge) throws NSValidation.ValidationException {

		if (DateCtrl.isSameDay(DateCtrl.stringToDate(DateCtrl.dateToString(dateFin())),CocktailUtilities.finAnneeUniversitaire(dateDebut())) == false) {
			throw new NSValidation.ValidationException("Pour une prolongation dans l'intérêt du service, la date de fin (" + DateCtrl.dateToString(dateFin()) + ") doit correspondre à la fin d'année universitaire (" + DateCtrl.dateToString(CocktailUtilities.finAnneeUniversitaire(dateDebut())) + ") !");
		}
		// Vérifier les positions
		NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsEnActivitePourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin());
		if (changements == null || changements.count() == 0) {
			throw new NSValidation.ValidationException("Une prolongation dans l'intérêt du service doit reposer sur une position d'activité ou de détachement avec carrière d'accueil");
		} else {
			for (java.util.Enumeration<EOChangementPosition> e = changements.objectEnumerator();e.hasMoreElements();) {
				EOChangementPosition changement = e.nextElement();
					// La prolongation dans l'intérêt du service en fonction doit reposer sur une position d'activité ou de détachement avec carrière d'accueil
					if (!changement.toPosition().estEnActiviteDansEtablissement() && (!changement.toPosition().estUnDetachement() || (changement.estDetachementSortant()))) {
						throw new NSValidation.ValidationException("Une prolongation dans l'intérêt du service doit reposer sur une position d'activité ou de détachement avec carrière d'accueil");
					}
					//  Une prolongation dans l'intérêt du service ne peut être accordée qu'aux fonctionnaires enseignants.
					if (changement.carriere() != null) {
					if (changement.carriere().toTypePopulation().estEnseignant() == false) {
						throw new NSValidation.ValidationException("Une prolongation dans l'intérêt du service ne peut être accordée qu'aux fonctionnaires enseignants");
					}
				}
			}
		}

		// Une période de maintien en service commence nécessairement après toutes les autres périodes.
		EOProlongationActivite lastProlongation = (EOProlongationActivite)prolongations.lastObject();	
		if (lastProlongation != null && lastProlongation != this) {
			if (DateCtrl.isSameDay(dateDebut(),DateCtrl.jourSuivant(lastProlongation.dateFin())) == false) {
				throw new NSValidation.ValidationException("Une prolongation pour maintien en service commence après toutes les autres prolongations");
			}
		} else {
			// pas de surnombre, pas de recul d'age
			// la date de début de la période de maintien en fonction est égale au lendemain de son 60 ou 61 ou 62 ou 63 ou 64 ou 65éme anniversaire.
			int ageMini = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_PROLONG_MAINTIEN_AGE_MIN);
			int ageMaxi = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_PROLONG_MAINTIEN_AGE_MAX);

			NSTimestamp dateAnniversaire = DateCtrl.dateAvecAjoutAnnees(individu().dNaissance(),ageMini);
			DateCtrl.IntRef annees = new DateCtrl.IntRef(),mois = new DateCtrl.IntRef(),jours = new DateCtrl.IntRef();
			DateCtrl.joursMoisAnneesEntre(dateAnniversaire,dateDebut(),annees,mois,jours,true);

			DateCtrl.joursMoisAnneesEntre(dateDebut(),dateFin(),annees,mois,jours,true);
			int dureeMax = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_PROLONG_MAINTIEN_DUREE_MAX);
			if (annees.value > dureeMax || (annees.value == dureeMax && (mois.value > 0 || jours.value > 0))) {
				throw new NSValidation.ValidationException("La prolongation dans l'intérêt du service dure au maximum " + dureeMax + " an");
			}
		}
	}
	private void validerProlongationPourPension(NSArray prolongations) throws NSValidation.ValidationException {
		// Vérifier que la date de fin est au maximum à 30 mois (10 trimestres) de la date de début
		int dureeMax = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_PROLONG_MAINTIEN_DUREE_MAX);
		NSTimestamp dateMax = DateCtrl.dateAvecAjoutMois(dateDebut(),dureeMax);
		if (DateCtrl.isAfter(dateFin(),dateMax)) {
			throw new NSValidation.ValidationException("La durée maximum de la prolongation Art. 69 est 10 trimestres");
		}
		if (prolongations.count() > 0) {
			// Vérifier qu'elle ne débute pas après une autre prolongation
			EOProlongationActivite firstProlongation = (EOProlongationActivite)prolongations.objectAtIndex(0);	
			if (firstProlongation != null && firstProlongation != this) {
				throw new NSValidation.ValidationException("Une prolongation Art. 69 doit commencer avant les autres prolongations");
			}
		}
	}
}


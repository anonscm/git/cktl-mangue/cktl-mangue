//EOReculAge.java
//Created on Thu Nov 24 14:05:56 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.prolongations;

import java.util.Enumeration;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.modele.DureeAvecArrete;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation<BR>
 * 1. La date de fin d'un recul limite ege doit etre superieure a la date de debut.<BR>
 * 2. La date de fin reelle saisie d'un recul limite age doit etre superieure a la date de debut.<BR>
 * 3. La date de fin reelle saisie d'un recul limite age doit etre inferieure a la date de fin.<BR>
 * 4. La 1ere periode de recul limite age doit commencer le lendemain des 65 ans de l'agent et elle dure un multiple d'annees.<BR>
 * 5. La duree d'une  periode de recul limite age est de 1 an pour le motif nombre enfants vivants a 50 ans. <BR>
 * 6. La duree maximale d'une  periode de recul limite age est de 3 ans pour le motif nombre enfants a charge a 65 ans.<BR>
 * 7. La duree d'une  periode de recul limite age est de 1 an par enfant decede pour la France.<BR>
 * 8. Il peut y avoir au maximum une periode de recul limite age pour chaque type de motif.<BR>
 * 9. Il ne peut pas  y avoir d'interruption entre deux periodes de recul limite age. et elles ne peuvents pas se chevaucher<BR>
 * 11. Une periode de recul limite age ne peut être accordee qu'aux fonctionnaires.<BR>
 * 12. La periode de recul limite age doit reposer sur une position d'activiteou de detachement avec carriere d'accueil. <BR>
 * 13. Un agent ne peut pas beneficier d'une periode de recul limite age posterieure a une periode de surnombre ou de maintien en fonction<BR>
 * 14. Le motif doit etre fourni.
 * @author christine
 */
// 09/06/2010 - ajout des enfants pour le CIR
public class EOReculAge extends _EOReculAge {

	public static final EOSortOrdering SORT_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY,EOSortOrdering.CompareAscending);

	public final static int NB_ANNEES_MAXIMUM_POUR_ENFANT_A_CHARGE_A_65 = 3;
	public final static int NB_ANNEES_POUR_ENFANT_A_CHARGE_A_50 = 1;

	private final static String TYPE_EVENEMENT = "RECUL";

	public EOReculAge() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOReculAge creer(EOEditingContext ec, EOIndividu individu) {

		EOReculAge newObject = (EOReculAge) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setDCreation(new NSTimestamp());
		newObject.setTemValide(CocktailConstantes.VRAI);

		return newObject;
	}

	/**
	 * 
	 */
	public static EOReculAge findForKey( EOEditingContext ec, Number key) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_SEQ_RECUL_AGE_KEY + "=%@", new NSArray(key)));
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}

	public static EOReculAge findForAbsence(EOEditingContext ec, EOAbsences absence) {
		return fetchFirstByQualifier(ec, EOQualifier.qualifierWithQualifierFormat(TO_ABSENCE_KEY + "=%@", new NSArray(absence)));
	}

	/**
	 * 
	 */
	public String typeEvenement() {
		return TYPE_EVENEMENT;
	}


	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (individu() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_INDIVIDU_NON_RENSEIGNE, "TPT"));
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "TPT"));
		} 
		if (dateFin() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_NON_RENSEIGNE, "TPT"));
		}
		if (dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "TPT", dateDebutFormatee(), dateFinFormatee()));
		}
		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException(String.format(MangueMessagesErreur.ERREUR_NUMERO_ARRETE, "TPT", "20"));
		}		

		if (motif() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un motif");
		}

		if (dFinExecution() !=  null) {
			if (DateCtrl.isBefore(dFinExecution(),dateDebut())) {
				throw new NSValidation.ValidationException("La date de début ne peut être postérieure à la date de fin réelle d'exécution");
			}
			if (DateCtrl.isBefore(dateFin(),dFinExecution())) {
				throw new NSValidation.ValidationException("La date de fin d'exécution ne peut être postérieure à la date de fin");
			}
		}

		String temp = validationsCir();
		if (temp != null && temp.length() > 0)
			throw new NSValidation.ValidationException(temp);

		// Vérifier que la durée minimum est respectée
		int nbAnneesMini = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_PROLONG_RA_MIN);

		int joursComptables = DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true, true);
		
		if (joursComptables%DateCtrl.NB_JOURS_COMPTABLES_ANNUELS != 0) {
			throw new NSValidation.ValidationException("La durée d'une période de recul d'âge est un multiple d'années");
		}
		if (joursComptables < (nbAnneesMini * DateCtrl.NB_JOURS_COMPTABLES_ANNUELS) )
			throw new NSValidation.ValidationException("La durée minimale d'une  période de recul de limite d'âge est de " + nbAnneesMini + " an(s)");

		// 1 an par enfant décédé pour la France
		int nbAnneesPourDeces = 0;
		int nbEnfantsVivantsA65ans = 0;
		int nbEnfantsVivantsA50ans = 0;
		NSMutableArray<EOEnfant> enfants = new NSMutableArray<EOEnfant>(enfant());
		if (enfant2() != null) {
			enfants.addObject(enfant2());
		}
		if (enfant3() != null) {
			enfants.addObject(enfant3());
		}

		for (EOEnfant myEnfant : enfants) {

			NSTimestamp date50ans = DateCtrl.dateAvecAjoutAnnees(individu().dNaissance(), 50);
			NSTimestamp date65ans = DateCtrl.dateAvecAjoutAnnees(individu().dNaissance(), 65);
			if (myEnfant.dDeces() != null) {
				if (myEnfant.estMortPourLaFrance()) {
					nbAnneesPourDeces++;
				}
				if (DateCtrl.isAfter(myEnfant.dDeces(), date50ans)) {
					nbEnfantsVivantsA50ans++;
				}
				if (DateCtrl.isAfter(myEnfant.dDeces(), date65ans)) {
					nbEnfantsVivantsA65ans++;
				}
			} else {

				if (DateCtrl.isBeforeEq(myEnfant.dNaissance(), date50ans)) {
					nbEnfantsVivantsA50ans++;
				}

				if (DateCtrl.isBeforeEq(myEnfant.dNaissance(), date65ans)) {
					nbEnfantsVivantsA65ans++;
				}
			}
		}

		// Vérifier la cohérence des motifs par rapport à la situation de l'agent
		if (motif().estEnfantMortPourFrance()) {
			if (nbAnneesPourDeces == 0) {
				throw new NSValidation.ValidationException("Aucun enfant n'est déclaré comme décédé");
			}
			NSTimestamp dateMaxi = DateCtrl.dateAvecAjoutAnnees(dateDebut(), nbAnneesPourDeces);
			if (DateCtrl.isAfter(dateFin(), dateMaxi)) {
				if (nbAnneesPourDeces == 1) {
					throw new NSValidation.ValidationException("1 enfant est déclaré comme décédé. Le recul d'âge est donc au maximum de 1 an");

				} else {
					throw new NSValidation.ValidationException(nbAnneesPourDeces + " enfants sont déclarés comme décédés. Le recul d'âge est donc au maximum de " + nbAnneesPourDeces + " ans");
				}
			}
		} else {
			int nbAnneesMax = 0;
			if (motif().est3EnfantsA50Ans()) {
				if (nbEnfantsVivantsA50ans < 3) {
					throw new NSValidation.ValidationException("Il faut au moins 3 enfants vivants à l'âge de 50 ans pour bénéficier d'un recul d'âge de ce type");
				}
				nbAnneesMax = NB_ANNEES_POUR_ENFANT_A_CHARGE_A_50;
			} else {
				if (motif().estEnfantAChargeA65Ans()) {
					if (nbEnfantsVivantsA65ans == 0) {
						throw new NSValidation.ValidationException("Il faut au moins un enfant à charge à 65 ans pour bénéficier d'un recul d'âge de ce type");
					}
					if (nbEnfantsVivantsA65ans >= 3) {
						nbAnneesMax = NB_ANNEES_MAXIMUM_POUR_ENFANT_A_CHARGE_A_65;
					} else {
						nbAnneesMax = nbEnfantsVivantsA65ans;
					}
				}

			}
			NSTimestamp dateMaxi = DateCtrl.dateAvecAjoutAnnees(dateDebut(), nbAnneesMax);
			if (DateCtrl.isAfter(dateFin(), dateMaxi))
				throw new NSValidation.ValidationException("Le recul d'âge est au maximum de " + nbAnneesMax + " an(s) !");

		}
		// Vérifier la cohérence de l'ensemble des reculs d'âge, ie pas de chevauchement 
		// pas de répétition des types et limités à 3 (cette limite est automatiquement vérifiée si tous les motifs sont différents)
		// vérifier que les périodes ont des durées correctes et des dates début-date fin correctes
		NSArray<EOReculAge> reculsAge = EOReculAge.findForIndividu(editingContext(),individu());
		if (reculsAge.count() == 0) {
			// 1ère période créée - Vérifier qu'il commence bien le lendemain du jour de l'anniversaire de la date limite
			int ageMini = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_PROLONG_RA_MIN);
			NSTimestamp dateAnniversaire = DateCtrl.dateAvecAjoutAnnees(individu().dNaissance(),ageMini);
			if (DateCtrl.isSameDay(dateDebut(),DateCtrl.jourSuivant(dateAnniversaire)) == false) {
				throw new NSValidation.ValidationException("La 1ère période de recul de limite d'âge doit commencer le lendemain des " + ageMini + " ans de l'individu (Soit le " + DateCtrl.dateToString(dateAnniversaire) + ")");
			}
		} else {
			// Vérifier l'unicité des motifs
			for (EOReculAge recul : reculsAge) {
				if (recul != this && recul.motif() == motif())
					throw new NSValidation.ValidationException("Il existe déjà un recul de limite d'âge avec ce motif");
			}
			// les reculs d'âge sont triés par ordre de date croisssant
			EOReculAge periodePrecedente = null, periodeSuivante = null;
			boolean found = false;
			for (Enumeration<EOReculAge> e2 = reculsAge.objectEnumerator();e2.hasMoreElements();) {
				EOReculAge recul = e2.nextElement();
				if (!found) {
					if (recul != this) {
						periodePrecedente = recul;
					} else {
						found = true;
					}
				} else {
					periodeSuivante = recul;
					break;
				}
			}
			if (periodePrecedente != null) {
				if (DateCtrl.isSameDay(dateDebut(),DateCtrl.jourSuivant(periodePrecedente.dateFin())) == false) {
					throw new NSValidation.ValidationException("Il ne peut pas  y avoir d'interruption entre deux périodes de recul de limite d'âge");
				}
			}
			if (periodeSuivante != null) {
				if (DateCtrl.isSameDay(dateFin(),DateCtrl.jourPrecedent(periodeSuivante.dateDebut())) == false) {
					throw new NSValidation.ValidationException("Il ne peut pas  y avoir d'interruption entre deux périodes de recul de limite d'âge");
				}
			}
		}
		// Vérifier les changements de position
		NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsEnActivitePourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin());
		if (changements == null || changements.count() == 0) {
			throw new NSValidation.ValidationException("La période de recul de limite d'âge doit reposer sur une position d'activité ou de détachement avec carrière d'accueil");
		} else {

			for (EOChangementPosition changement : changements) {
				// La période de recul limite age doit reposer sur une position d'activité ou de détachement avec carrière d'accueil
				if (!changement.toPosition().estEnActiviteDansEtablissement() && (!changement.toPosition().estUnDetachement() || (changement.estDetachementSortant()))) {
					throw new NSValidation.ValidationException("La période de recul de limite d'âge doit reposer sur une position d'activité ou de détachement avec carrière d'accueil");
				}
			}
		}
		
		// Un agent ne peut pas bénéficier d'une période de recul limite d'age postérieure à une prolongation d'activité
		NSArray periodes = DureeAvecArrete.rechercherDureesPourIndividu(editingContext(), EOProlongationActivite.ENTITY_NAME , individu());
		if (periodes != null && periodes.count() > 0) {
			periodes = EOSortOrdering.sortedArrayUsingKeyOrderArray(periodes, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
			EOProlongationActivite periode = (EOProlongationActivite)periodes.objectAtIndex(0);
			if (DateCtrl.isAfter(dateFin(),periode.dateDebut())) {
				throw new NSValidation.ValidationException("Un agent ne peut pas bénéficier d'une période de recul de limite d'âge postérieure à une période de prolongation d'activité.");
			}
		} 

		setDModification(new NSTimestamp());
	}

	/**
	 * Validations a effectuer pour le CIR
	 * @return
	 */
	public String validationsCir() {
		String temp = "";
		if (enfant() == null) {
			temp = "Un enfant au moins doit être saisi !";
		}
		if (motif().est3EnfantsA50Ans() && (enfant() == null || enfant2() == null || enfant3() == null)) {
			temp = "les trois enfants doivent être saisis";
		}
		return temp;
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOReculAge> findForIndividu(EOEditingContext ec, EOIndividu individu) {
		return findForIndividu(ec, individu, null, null);
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray<EOReculAge> findForIndividu(EOEditingContext ec, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();	
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@",new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@",new NSArray(individu)));

		if (debutPeriode != null)
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode));

		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT);
	}

}

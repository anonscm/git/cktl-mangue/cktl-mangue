package org.cocktail.mangue.modele.mangue.prolongations;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.DureeAvecArrete;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public abstract class ProlongationActivite extends DureeAvecArrete {

	// Attributes
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FIN_EXECUTION_KEY = "dFinExecution";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_VALIDE_KEY = "temValide";

	// Attributs non visibles
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String PROL_ORDRE_KEY = "prolOrdre";
	public static final String C_MOTIF_KEY = "cMotif";

	//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String DATE_ARRETE_COLKEY = "D_ARR_PROLONGATION";
	public static final String DATE_DEBUT_COLKEY = "D_DEB_PROLONGATION";
	public static final String DATE_FIN_COLKEY = "D_FIN_PROLONGATION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_FIN_EXECUTION_COLKEY = "D_FIN_EXEC_PROLONGATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NO_ARRETE_COLKEY = "NO_ARR_PROLONGATION";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String PROL_ORDRE_COLKEY = "PROL_ORDRE";
	public static final String C_MOTIF_COLKEY = "C_MOTIF_PROLONGATION";


	// Relationships
	public static final String MOTIF_KEY = "motif";

	public String temValide() {
		return (String) storedValueForKey("temValide");
	}

	public void setTemValide(String value) {
		takeStoredValueForKey(value, "temValide");
	}

	public NSTimestamp dFinExecution() {
		return (NSTimestamp)storedValueForKey("dFinExecution");
	}

	public void setDFinExecution(NSTimestamp value) {
		takeStoredValueForKey(value, "dFinExecution");
	}
	
	
	public org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeMotProlongation motif() {
		return (org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeMotProlongation)storedValueForKey(MOTIF_KEY);
	}

	public void setMotifRelationship(org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeMotProlongation value) {
		if (value == null) {
			org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypeMotProlongation oldValue = motif();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MOTIF_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, MOTIF_KEY);
		}
	}


	public String dateFinReelleFormatee() {
		return SuperFinder.dateFormatee(this,"dFinExecution");
	}
	public void setDateFinReelleFormatee(String uneDate) {
		if (uneDate == null) {
			setDFinExecution(null);
		} else {
			SuperFinder.setDateFormatee(this,"dFinExecution",uneDate);
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("La date de fin doit être fournie");
		}
		if (motif() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un motif");
		}
		super.validateForSave();
		if (dFinExecution() !=  null) {
			if (DateCtrl.isBefore(dFinExecution(),dateDebut())) {
				throw new NSValidation.ValidationException("La date de début ne peut être postérieure à la date de fin réelle d'exécution");
			}
			if (DateCtrl.isBefore(dateFin(),dFinExecution())) {
				throw new NSValidation.ValidationException("La date de fin d'exécution ne peut être postérieure à la date de fin");
			}
		}
	}


}

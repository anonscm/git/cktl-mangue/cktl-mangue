/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOProlongationsActivite.java instead.
package org.cocktail.mangue.modele.mangue.prolongations;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOProlongationsActivite extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ProlongationsActivite";
	public static final String ENTITY_TABLE_NAME = "MANGUE.V_PROLONGATIONS_ACTIVITE";



	// Attributes


	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String PRA_ID_KEY = "praId";
	public static final String PRA_INFOS_KEY = "praInfos";
	public static final String PRA_LIBELLE_KEY = "praLibelle";
	public static final String PRA_TYPE_KEY = "praType";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";

//Colonnes dans la base de donnees
	public static final String DATE_ARRETE_COLKEY = "PRA_DATE_ARRETE";
	public static final String DATE_DEBUT_COLKEY = "PRA_DEBUT";
	public static final String DATE_FIN_COLKEY = "PRA_FIN";
	public static final String NO_ARRETE_COLKEY = "PRA_NO_ARRETE";
	public static final String PRA_ID_COLKEY = "PRA_ID";
	public static final String PRA_INFOS_COLKEY = "PRA_INFOS";
	public static final String PRA_LIBELLE_COLKEY = "PRA_LIBELLE";
	public static final String PRA_TYPE_COLKEY = "PRA_TYPE";
	public static final String TEM_VALIDE_COLKEY = "PRA_VALIDE";

	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";


	// Relationships
	public static final String TO_INDIVIDU_KEY = "toIndividu";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey(DATE_ARRETE_KEY);
  }

  public void setDateArrete(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ARRETE_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public String noArrete() {
    return (String) storedValueForKey(NO_ARRETE_KEY);
  }

  public void setNoArrete(String value) {
    takeStoredValueForKey(value, NO_ARRETE_KEY);
  }

  public Integer praId() {
    return (Integer) storedValueForKey(PRA_ID_KEY);
  }

  public void setPraId(Integer value) {
    takeStoredValueForKey(value, PRA_ID_KEY);
  }

  public String praInfos() {
    return (String) storedValueForKey(PRA_INFOS_KEY);
  }

  public void setPraInfos(String value) {
    takeStoredValueForKey(value, PRA_INFOS_KEY);
  }

  public String praLibelle() {
    return (String) storedValueForKey(PRA_LIBELLE_KEY);
  }

  public void setPraLibelle(String value) {
    takeStoredValueForKey(value, PRA_LIBELLE_KEY);
  }

  public String praType() {
    return (String) storedValueForKey(PRA_TYPE_KEY);
  }

  public void setPraType(String value) {
    takeStoredValueForKey(value, PRA_TYPE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  

/**
 * Créer une instance de EOProlongationsActivite avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOProlongationsActivite createEOProlongationsActivite(EOEditingContext editingContext, NSTimestamp dateDebut
, Integer praId
, String praLibelle
, String praType
, String temValide
			) {
    EOProlongationsActivite eo = (EOProlongationsActivite) createAndInsertInstance(editingContext, _EOProlongationsActivite.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setPraId(praId);
		eo.setPraLibelle(praLibelle);
		eo.setPraType(praType);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOProlongationsActivite localInstanceIn(EOEditingContext editingContext) {
	  		return (EOProlongationsActivite)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOProlongationsActivite creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOProlongationsActivite creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOProlongationsActivite object = (EOProlongationsActivite)createAndInsertInstance(editingContext, _EOProlongationsActivite.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOProlongationsActivite localInstanceIn(EOEditingContext editingContext, EOProlongationsActivite eo) {
    EOProlongationsActivite localInstance = (eo == null) ? null : (EOProlongationsActivite)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOProlongationsActivite#localInstanceIn a la place.
   */
	public static EOProlongationsActivite localInstanceOf(EOEditingContext editingContext, EOProlongationsActivite eo) {
		return EOProlongationsActivite.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
	      return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
	    }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	        EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	        fetchSpec.setIsDeep(true);
	        fetchSpec.setUsesDistinct(distinct);
	        if (prefetchs != null)
	            fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	        NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	        return eoObjects;
	      }


		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOProlongationsActivite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOProlongationsActivite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOProlongationsActivite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOProlongationsActivite)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOProlongationsActivite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOProlongationsActivite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOProlongationsActivite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOProlongationsActivite)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOProlongationsActivite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOProlongationsActivite eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOProlongationsActivite ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOProlongationsActivite fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

// EODecisionDelegation.java
// Created on Fri Feb 14 15:07:05  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue;


import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * R&egrave;gles de validation des d&eacute;cisions de d&eacute;l&eacute;gation :<BR>
 * La date et le rne doivent &ecirc;tre fournis<BR>
 * V&eacute;rification des longueurs de cha&icirc;nes.<BR>
 */
public class EODecisionDelegation extends EOGenericRecord {
	public static String TYPE_DELEGATION = "D";
	public static String TYPE_ROMPU = "R";
    public EODecisionDelegation() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EODecisionDelegation(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String cTypeDecision() {
        return (String)storedValueForKey("cTypeDecision");
    }

    public void setCTypeDecision(String value) {
        takeStoredValueForKey(value, "cTypeDecision");
    }
    
    public NSTimestamp dDecDelegation() {
        return (NSTimestamp)storedValueForKey("dDecDelegation");
    }

    public void setDDecDelegation(NSTimestamp value) {
        takeStoredValueForKey(value, "dDecDelegation");
    }

    public String refCourrier() {
        return (String)storedValueForKey("refCourrier");
    }

    public void setRefCourrier(String value) {
        takeStoredValueForKey(value, "refCourrier");
    }

    public String bureauEmetteur() {
        return (String)storedValueForKey("bureauEmetteur");
    }

    public void setBureauEmetteur(String value) {
        takeStoredValueForKey(value, "bureauEmetteur");
    }

    public String lDecDelegation() {
        return (String)storedValueForKey("lDecDelegation");
    }

    public void setLDecDelegation(String value) {
        takeStoredValueForKey(value, "lDecDelegation");
    }

    public EORne toRne() {
        return (EORne)storedValueForKey("toRne");
    }

    public void setToRne(EORne value) {
        takeStoredValueForKey(value, "toRne");
    }

    public NSArray fluxMoyens() {
        return (NSArray)storedValueForKey("fluxMoyens");
    }

    public void setFluxMoyens(NSMutableArray value) {
        takeStoredValueForKey(value, "fluxMoyens");
    }

    public void addToFluxMoyens(EOFluxMoyens object) {
    		includeObjectIntoPropertyWithKey(object, "fluxMoyens");
    }

    public void removeFromFluxMoyens(EOFluxMoyens object) {
    		excludeObjectFromPropertyWithKey(object, "fluxMoyens");
    }
    // méthodes ajoutées
    public String dateDeclarationFormatee() {
		return SuperFinder.dateFormatee(this,"dDecDelegation");
	}
	public void setDateDeclarationFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dDecDelegation",uneDate);
	}
	public void init() {
		setCTypeDecision(TYPE_DELEGATION);
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(toRne(),"toRne");
		int total = fluxMoyens().count();
		for (int i = 0; i < total;i++) {
			EOFluxMoyens flux = (EOFluxMoyens)fluxMoyens().objectAtIndex(0);
			flux.supprimerRelations();
			editingContext().deleteObject(flux);
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (dDecDelegation() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de décision");
		}
		if (bureauEmetteur() != null && bureauEmetteur().length() > 10) {
			throw new NSValidation.ValidationException("Le bureau émetteur ne doit pas dépasser 10 caractères ...");
		}
		if (refCourrier() != null && refCourrier().length() > 8) {
			throw new NSValidation.ValidationException("La référence courrier ne doit pas dépasser 8 caractères ...");
		}
		if (lDecDelegation() != null && lDecDelegation().length() > 86) {
			throw new NSValidation.ValidationException("L'objet ne doit pas dépasser 86 caractères ...");
		}
		if (toRne() == null) {
			throw new NSValidation.ValidationException("Vous devez  sélectionner un  Rne");
		}
	}
}

// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   EOElectraFichier.java

package org.cocktail.mangue.modele.mangue;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOConservationAnciennete;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes;
import org.cocktail.mangue.modele.mangue.individu.EOStage;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOSupInfoData extends _EOSupInfoData {

	private static final String CODE_TEMPS_PLEIN = "MS100";
	private static final String DEFAULT_DIPL_CODE = "0264";
	private static final String DEFAULT_DIPL_ANNEE = "1900";
	private static final String DEFAULT_DIPL_LIBELLE_UAI = "Inconnu";
	private static final String DEFAULT_DIPL_CODE_UAI = "9912345H";

	private static final String STATUT_TITULAIRE = "TITU1";
	private static final String TITULAIRE_EC = "T";

	private static final String STATUT_DETACHE_SORTANT = "TITU1";
	private static final String POSITION_DETACHE_SORTANT = "DET00";
	private static final String STATUT_DETACHE_SORTANT_PROMO_EC = "T";
	private static final String STATUT_DETACHE_ENTRANT = "TITU1";
	private static final String POSITION_DETACHE_ENTRANT = "ACI00";
	public static final String STATUT_PROVISOIRE = "P";
	public static final String STATUT_TRANSMIS_MINISTERE = "T";
	public static final String STATUT_PROMU = "V";
	public static final String STATUT_NEGATIF = "N";
	private static final String POSITION_GESUP_EN_ACTIVITE = "ACI00";

	public EOSupInfoData() {
	}

	public void transmettreMinistere() {
		setEficEtat(STATUT_TRANSMIS_MINISTERE);
	}

	public void promouvoir()
	{
		setEficEtat(STATUT_PROMU);
	}

	public void refuserPromotion()
	{
		setEficEtat(STATUT_NEGATIF);
	}

	public boolean estProvisoire() {
		return eficEtat() != null && eficEtat().equals(STATUT_PROVISOIRE);
	}

	public boolean estValide() {
		return eficTemValide() != null && eficTemValide().equals(CocktailConstantes.VRAI);
	}

	public void setEstValide(boolean bool, String observations) {
		if (bool) {
			setEficTemValide(CocktailConstantes.VRAI);
			setEficObservations(null);
		}
		else {
			setEficTemValide(CocktailConstantes.FAUX);
			setEficObservations(eficObservations() + " - " + observations);
		}
	}

	public boolean estCrct() {
		return eficTemCrct() != null && eficTemCrct().equals(CocktailConstantes.VRAI);
	}
	public void setEstCrct(boolean bool) {
		if (bool)
			setEficTemCrct(CocktailConstantes.VRAI);
		else
			setEficTemCrct(CocktailConstantes.FAUX);
	}

	public boolean estTransmisMinistere()
	{
		return eficEtat() != null && eficEtat().equals(STATUT_TRANSMIS_MINISTERE);
	}

	public boolean individuPromu()
	{
		return eficEtat() != null && eficEtat().equals(STATUT_PROMU);
	}

	public boolean promotionRefusee()
	{
		return eficEtat() != null && eficEtat().equals(STATUT_NEGATIF);
	}

	public String dateNaissanceFormatee()
	{
		return SuperFinder.dateFormatee(this, EFIC_D_NAISSANCE_KEY);
	}

	public void setDateNaissanceFormate(String uneDate)
	{
		SuperFinder.setDateFormatee(this, EFIC_D_NAISSANCE_KEY, uneDate);
	}

	public String dateArreteFormatee()
	{
		return SuperFinder.dateFormatee(this, EFIC_D_ARRETE_KEY);
	}

	public void setDateArreteFormatee(String uneDate)
	{
		SuperFinder.setDateFormatee(this, EFIC_D_ARRETE_KEY, uneDate);
	}

	public String datePromotionFormatee()
	{
		return SuperFinder.dateFormatee(this, EFIC_D_PROMOTION_KEY);
	}

	public void setDatePromotionFormatee(String uneDate)
	{
		SuperFinder.setDateFormatee(this, EFIC_D_PROMOTION_KEY, uneDate);
	}

	/**
	 * 
	 * @param edc
	 * @param fichier
	 * @return
	 */
	public static EOSupInfoData creer(EOEditingContext edc, EOSupInfoFichier fichier) {

		EOSupInfoData record = new EOSupInfoData();
		record.setToFichierRelationship(fichier);
		record.init();

		edc.insertObject(record);

		return record;
	}



	/**
	 * 
	 */
	public void init() {

		setEstValide(true, null);		
		setEficEtat(STATUT_PROVISOIRE);
		setEficTemCrct(CocktailConstantes.FAUX);
		setEficObservations("");
		setEficQuotite(ManGUEConstantes.QUOTITE_100);
		setEficAncConservee("000000");
		setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	/**
	 * 
	 * @param individu
	 */
	public void initIndividu(EOIndividu individu) {

		setToIndividuRelationship(individu);

		if(individu.nomPatronymique() == null) {
			if(individu.nomUsuel().length() > 40)
				setEficNomPatronymique(individu.nomUsuel().substring(0, 40));
			else
				setEficNomPatronymique(individu.nomUsuel());
		} else {
			if(individu.nomPatronymique().length() > 40)
				setEficNomPatronymique(individu.nomPatronymique().substring(0, 40));
			else
				setEficNomPatronymique(individu.nomPatronymique());
		}

		if(individu.nomUsuel().length() > 20)
			setEficNomUsuel(individu.nomUsuel().substring(0, 20));
		else
			setEficNomUsuel(individu.nomUsuel());

		if(individu.prenom().length() > 15)
			setEficPrenom(individu.prenom().substring(0, 15));
		else
			setEficPrenom(individu.prenom());

		if(individu.estHomme())
			setEficCivilite(ManGUEConstantes.CODE_SEXE_HOMME);
		else
			setEficCivilite(ManGUEConstantes.CODE_SEXE_FEMME);

		if(individu.dNaissance() == null)
			setEficDNaissance(new NSTimestamp());
		else
			setEficDNaissance(individu.dNaissance());

		if(individu.toPaysNationalite() == null)
			setEficCPaysNationalite("");
		else {
			if(individu.toPaysNationalite().iso31663() != null)
				setEficCPaysNationalite(individu.toPaysNationalite().iso31663());
			else
				setEficCPaysNationalite(individu.toPaysNationalite().code());
		}

		if(individu.personnel().numen() == null)
			setEficNumen(null);
		else
			setEficNumen(individu.personnel().numen().toUpperCase());

		// Ajouter l'uai d'affectation de l'individu
		NSArray<EOAffectation> affectations = EOAffectation.findForIndividu(editingContext(), individu, toFichier().supfDateObs());
		try {
			EOStructure structure = affectations.get(0).toStructureUlr();
			setEficUaiAffectation(structure.etablissement().rne().code());
		} catch (Exception e) {	
			setEficUaiAffectation(" ");
		}

		// Departs
		NSArray<EODepart> departs = EODepart.rechercherDepartsValidesPourIndividuEtPeriode(editingContext(), individu, toFichier().supfDateObs(), null);
		if(departs.count() > 0) {
			setEficCMotifDepart(departs.get(0).motifDepart().cMotifDepartOnp());
			setEficDEffetDepart(departs.get(0).dateDebut());
		}
	}

	/**
	 * 
	 * @param carriere
	 */
	public void initSpecialisation(EOCarriere carriere) {

		if (carriere.toSpecialiteItarf() != null) {
			setEficCSectionCnu("R" + carriere.toSpecialiteItarf().toBap().code() + carriere.toSpecialiteItarf().code());
		}
		else {
			EOReferensEmplois referens = carriere.toReferensEmploi();
			if (referens != null) {

				EOBap bap = referens.toBap();

				if (bap == null) {
					setEstValide(false, "Erreur de spécialisation (Emploi type) ! " + ("Fermeture au " + DateCtrl.dateToString(referens.dateFermeture()) + ")"));
				}
				else {
					setEficCSectionCnu(bap.code() + "9999");										
					//					if (bap.code().length() > 1) {
					//						setEstValide(false, "Erreur de spécialisation (Emploi type) ! " + ("Fermeture au " + DateCtrl.dateToString(referens.dateFermeture()) + ")"));
					//					}
					//					else {
					//						if (referens.code().length() > 5) {
					//							setEstValide(false, "Code emploi type erroné ==> " + carriere.toReferensEmploi().code() + " (Fermeture au " + DateCtrl.dateToString(referens.dateFermeture()) + ")");					
					//						}
					//						else
					//							setEficCSectionCnu(referens.code());										
					//					}
				}
			}
			else {

				if (carriere.toBap() != null)
					setEficCSectionCnu(carriere.toBap().code());
				else {
					if (carriere.toDiscSecondDegre() != null) {
						if (carriere.toDiscSecondDegre().codeBcn() == null)
							setEstValide(false, "Pb de discipline 2nd degré " + carriere.toDiscSecondDegre().code() + " (Pas de code BCN associé)");
						setEficCSectionCnu(carriere.toDiscSecondDegre().codeBcn());
					}
					else
						if (carriere.toSpecialiteAtos() != null)
							setEficCSectionCnu(carriere.toSpecialiteAtos().code());
				}
			}

			EOCnu cnu = carriere.toCnu();
			if(cnu != null) {

				// code specialite a revoir pour l integration des sous sections CNU concernant les langues
				// Pour les sections CNU 13 14 15 
				if (cnu.code().equals("13") 
						|| cnu.code().equals("14")
						|| cnu.code().equals("15"))	 {	
					if (cnu.codeSousSection() != null 
							&& !cnu.codeSousSection().equals("000")
							&& !cnu.codeSousSection().equals("00")) {
						setEficCSectionCnuEc(cnu.codeSousSection()); 
					}
				}

				setEficCSectionCnu(cnu.code() + "00");            
			}
		}

	}


	/**
	 * 
	 * @param carriere
	 */
	public void initSpecialisation(EOContratAvenant avenant) {

		if (avenant.toSpecialiteItarf() != null) {
			setEficCSectionCnu("R" + avenant.toSpecialiteItarf().toBap().code() + avenant.toSpecialiteItarf().code());
		}
		else {
			EOReferensEmplois referens = avenant.toReferensEmploi();
			if (referens != null) {

				EOBap bap = referens.toBap();

				if (bap == null) {
					setEstValide(false, "Erreur de spécialisation (Emploi type) ! " + ("Fermeture au " + DateCtrl.dateToString(referens.dateFermeture()) + ")"));
				}
				else {
					setEficCSectionCnu(bap.code() + "9999");										
				}
			}
			else {

				if (avenant.toBap() != null)
					setEficCSectionCnu(avenant.toBap().code());
				else {
					if (avenant.toDiscSecondDegre() != null) {
						if (avenant.toDiscSecondDegre().codeBcn() == null)
							setEstValide(false, "Pb de discipline 2nd degré " + avenant.toDiscSecondDegre().code() + " (Pas de code BCN associé)");
						setEficCSectionCnu(avenant.toDiscSecondDegre().codeBcn());
					}
					else
						if (avenant.toSpecialiteAtos() != null)
							setEficCSectionCnu(avenant.toSpecialiteAtos().code());
				}
			}

			EOCnu cnu = avenant.toCnu();
			if(cnu != null) {

				// code specialite a revoir pour l integration des sous sections CNU concernant les langues
				// Pour les sections CNU 13 14 15 
				if (cnu.code().equals("13") 
						|| cnu.code().equals("14")
						|| cnu.code().equals("15"))	 {	
					if (cnu.codeSousSection() != null 
							&& !cnu.codeSousSection().equals("000")
							&& !cnu.codeSousSection().equals("00")) {
						setEficCSectionCnuEc(cnu.codeSousSection()); 
					}
				}

				setEficCSectionCnu(cnu.code() + "00");            
			}
		}

	}


	/**
	 * Mise a jour des donnees associees au DIPLOME
	 */
	public void initDiplomes() {

		// Initialisation des valeurs par défaut.
		setEficDiplCode(DEFAULT_DIPL_CODE);
		setEficDiplAnnee(new Integer(DEFAULT_DIPL_ANNEE));
		setEficDiplCodeUAI(DEFAULT_DIPL_CODE_UAI);
		setEficDiplLibelleUAI(DEFAULT_DIPL_LIBELLE_UAI);

		NSArray<EOIndividuDiplomes> diplomes = EOIndividuDiplomes.findForIndividu(editingContext(), toIndividu());		
		if (diplomes.size() > 0) {

			EOIndividuDiplomes indDiplome = diplomes.get(0);
			EODiplomes diplome = indDiplome.diplome();

			if (indDiplome.dDiplome() != null) {
				setEficDiplAnnee(new Integer(DateCtrl.getYear(indDiplome.dDiplome())));
			}
			if (diplome != null && diplome.code() != null) {
				setEficDiplCode("0" + diplome.code().substring(1, diplome.code().length()));
			}
			if (indDiplome.uaiObtention() != null) {
				setEficDiplCodeUAI(indDiplome.uaiObtention().code());
				setEficDiplLibelleUAI(indDiplome.uaiObtention().libelleLong());
			}
		}

	}

	/**
	 * Mise a jour des donnees associees à l'UAI
	 */
	public void initUAI(EORne uaiEtablissement) {

		// UAI ETABLISSEMENT
		if (uaiEtablissement != null) {
			setEficUai(uaiEtablissement.code());
			setEficUaiAffectation(uaiEtablissement.code());
		}
		else {
			setEficUai(" ");
			setEstValide(false, "UAI établissement non renseigné");
		}

		// UAI AFFECTATION
		NSArray<EOAffectation> affectations = EOAffectation.findForIndividuEtPeriode(editingContext(), toIndividu(), 
				DateCtrl.debutAnnee(toFichier().supfAnnee()), toFichier().supfDateObs());
		try {
			EOStructure structure = affectations.get(0).toStructureUlr();
			setEficUaiAffectation(structure.etablissement().rne().code());
		}
		catch(Exception e) {

		}
	}

	/**
	 * 
	 */
	public void initModalites() {

		setEficCMotifTempsPartiel(CODE_TEMPS_PLEIN);

		EOModalitesService modalite = EOModalitesService.modalitePourDate(editingContext(), toIndividu(), toFichier().supfDateObs());
		if (modalite != null) {
			if (modalite.estCrct() == false) {
				setEficQuotite(modalite.quotite());
			}
		}
		else {
			setEficQuotite(ManGUEConstantes.QUOTITE_100);
		}

		if(eficQuotite().floatValue() < 100) {
			NSArray<EOTempsPartiel> tempsPartiel = EOTempsPartiel.rechercherTempsPartielPourIndividuEtPeriode(editingContext(), toIndividu(), toFichier().supfDateObs(), toFichier().supfDateObs());
			if(tempsPartiel != null && tempsPartiel.size() > 0)
				setEficCMotifTempsPartiel(tempsPartiel.get(0).motif().codeOnp());
		}
	}

	/**
	 * 
	 */
	public void initAnciennete(EOElementCarriere element) {

		NSArray<EOConservationAnciennete> conservations = EOConservationAnciennete.rechercherAnciennetesNonUtiliseesPourElementCarriere(editingContext(), element);

		int nbAnnees = 0;
		int nbMois = 0;
		int nbJours = 0;
		for (EOConservationAnciennete myConservation : conservations) {
			if(myConservation.ancNbAnnees() != null)
				nbAnnees += myConservation.ancNbAnnees().intValue();
			if(myConservation.ancNbMois() != null)
				nbMois += myConservation.ancNbMois().intValue();
			if(myConservation.ancNbJours() != null)
				nbJours += myConservation.ancNbJours().intValue();
		}

		if(nbAnnees + nbMois + nbJours > 0) {
			String resultat = "";
			resultat = (new StringBuilder(String.valueOf(resultat))).append(StringCtrl.stringCompletion(String.valueOf(nbAnnees), 2, "0", "G")).toString();
			resultat = (new StringBuilder(String.valueOf(resultat))).append(StringCtrl.stringCompletion(String.valueOf(nbMois), 2, "0", "G")).toString();
			resultat = (new StringBuilder(String.valueOf(resultat))).append(StringCtrl.stringCompletion(String.valueOf(nbJours), 2, "0", "G")).toString();
			setEficAncConservee(resultat);
		}

	}

	/**
	 * 
	 */
	public void initConges() {

		NSArray<EOAbsences> congesLegaux = EOAbsences.rechercherAbsencesLegalesPourIndividuEtDates(editingContext(), toIndividu(), toFichier().supfDateObs(), toFichier().supfDateObs());
		if(congesLegaux.count() > 0) {
			EOAbsences absence = congesLegaux.get(0);
			if(absence.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_CLM) 
					|| absence.toTypeAbsence().code().equals(EOTypeAbsence.TYPE_CLD)) {
				setEficCTypeAbsence(absence.toTypeAbsence().codeOnp());
				setEficDDebAbsence(absence.dateDebut());
				setEficDFinAbsence(absence.dateFin());
			}
		}
	}


	/**
	 * 
	 * @param element
	 */
	public void initElement(EOElementCarriere element) {

		EOElementCarriere elementPrecedent = EOElementCarriere.elementCarrierePrecedent(editingContext(), element);
		if(elementPrecedent != null) {
			setEficCGradePrev(elementPrecedent.toGrade().cGrade());
		}

		NSMutableArray args = new NSMutableArray(element.toIndividu());
		args.addObject(element.toCorps());
		setEficCEchelon(element.cEchelon());
		boolean dateCorpsEvaluee = false;
		boolean dateGradeEvaluee = false;
		EOCarriere carrierePourCorps = null;
		NSArray<EOElementCarriere> elementsCarriere = EOElementCarriere.rechercherElementsAvecCriteres(editingContext(), EOQualifier.qualifierWithQualifierFormat("toIndividu = %@  AND temValide = 'O' AND toCarriere.temValide = 'O' AND temProvisoire = 'N' AND toCorps = %@", args), true, false);
		for (Enumeration<EOElementCarriere> e = elementsCarriere.reverseObjectEnumerator();e.hasMoreElements();) {

			EOElementCarriere elementCourant = e.nextElement();			
			if(!dateCorpsEvaluee) {
				setEficDNominationCorps(elementCourant.dateDebut());
				carrierePourCorps = elementCourant.toCarriere();
				dateCorpsEvaluee = true;
			}

			if(elementCourant.toGrade() != element.toGrade())
				continue;

			if(!dateGradeEvaluee) {
				setEficDGrade(elementCourant.dateDebut());
				dateGradeEvaluee = true;
			}

			if(elementCourant.cEchelon() == null || !elementCourant.cEchelon().equals(element.cEchelon()))
				continue;

			setEficDEchelon(elementCourant.dateDebut());

			break;
		}

		String categorieEnCours = element.toCorps().cCategorie();
		if (element.toCarriere().toTypePopulation().estEnseignant())
			categorieEnCours = null;

		if (categorieEnCours != null) {
			for(EOElementCarriere elementCourant : elementsCarriere) {
				if(elementCourant.toCorps().cCategorie() != null 
						&& elementCourant.toCorps().cCategorie().equals(categorieEnCours))
					setEficDEntreeCategorie(elementCourant.dateDebut());
			}
		}

		// ECHELON / CHEVRON
		if(eficDEchelon() == null)
			setEficDEchelon(element.dateDebut());
		if(element.cChevron() != null && element.cChevron().length() > 0)
			setEficCChevron(element.cChevron().substring(element.cChevron().length() - 1));

		// GRADE
		if (element.toGrade().dFermeture() != null && DateCtrl.isBefore(element.toGrade().dFermeture(), toFichier().supfDateObs()))
			setEstValide(false, "Grade invalide");
		setEficCGrade(element.toGrade().cGrade());

		// CORPS
		if (element.toCorps().cCorps().length() > 6) {
			setEstValide(false, "Problème de code corps");
		}
		else
			setEficCCorps(element.toCorps().cCorps());

		// Date de Titularisation
		// Date de fin de stage, sinon date de nomination dans le corps/

		if (carrierePourCorps != null) {
			NSArray<EOStage> stages = EOStage.findForCarriere(editingContext(), carrierePourCorps);
			for(EOStage stage : stages) {
				if(stage.corps() == element.toCorps() && stage.dateTitularisation() != null){
					setEficDTitularisation(stage.dateTitularisation());
					break;
				}
			}
		}

		if(eficDTitularisation() == null)
			setEficDTitularisation(eficDNominationCorps());		

	}

	/**
	 * 
	 * @param avenant
	 */
	public void initAvenant(EOContratAvenant avenant) {

		setEficStatut(avenant.contrat().toTypeContratTravail().code());
		setEficDDebPostion(avenant.dateDebut());
		setEficDFinPostion(avenant.dateFin());

		setToGradeRelationship(avenant.toGrade());

		setEficQuotite(new BigDecimal(avenant.quotite().floatValue()));

		setEficIndiceBrut(avenant.indiceBrut());
		if (avenant.contrat().toOrigineFinancement() != null) {
			setEficTypeFinancement(avenant.contrat().toOrigineFinancement().code());
		}
		setEficRemunHoraire(avenant.tauxHoraire());
		setEficForfait(avenant.montant());

		if (avenant.toGrade() != null) {
			setEficFonction(avenant.toGrade().toCorps().cCorps());
		}
	}

	/**
	 * 
	 * @param changement
	 */
	public void initChangementPosition(EOChangementPosition changement) {

		setEficDDebPostion(changement.dateDebut());
		setEficCPosition(changement.toPosition().codeOnp());   

		EORne uaiEtablissement = EOStructure.rechercherEtablissement(editingContext()).rne();

		if(changement.toRne() != null) {
			//			setEficUaiAffectation(changement.toRne().code());
			setEficLieuPosition(StringCtrl.stringCompletion(changement.toRne().libelleLong(), 50, "", "D"));
		}
		else {
			//			if (changement.toRneOrigine() != null)
			//				setEficUaiAffectation(changement.toRneOrigine().code());
			//			else
			//				setEficUaiAffectation(uaiEtablissement.code());

			if(uaiEtablissement != null && changement.toPosition().temActivite().equals(CocktailConstantes.VRAI))
				setEficLieuPosition(StringCtrl.stringCompletion(uaiEtablissement.libelleLong(), 50, "", "D"));
			else {
				if (changement.lieuPosition() != null)
					setEficLieuPosition(StringCtrl.stringCompletion(changement.lieuPosition(), 50, "", "D"));	            								
				else
					setEficLieuPosition("?");
			}
		}

		// PROMOTION DES ENSEIGNANTS CHERCHEURS
		if (toFichier().estRemonteePromouvables() || toFichier().estRemonteeCrct() ) {

			setEficStatut(TITULAIRE_EC);
			if(changement.toPosition().codeGesup() == null)
				setEficCPosition(POSITION_GESUP_EN_ACTIVITE);
			else
				setEficCPosition(changement.toPosition().codeOnp());   
			//				setEficCPosition(changement.toPosition().cPositionGesup());
		}
		else {		// SUPINFO
			setEficStatut(STATUT_TITULAIRE);
			setEficCPosition(changement.toPosition().codeOnp());   
		}

		setEficDDebPostion(changement.dateDebut());
		setEficDFinPostion(changement.dateFin());

		// Cas des detachements
		if (changement.toPosition().estUnDetachement()) {

			// DETACHEMENT SORTANT
			if(changement.estDetachementSortant()) {

				if (toFichier().estRemonteePromouvables() || toFichier().estRemonteeCrct() ) {
					setEficStatut(STATUT_DETACHE_SORTANT_PROMO_EC);
					setEficCPosition(POSITION_DETACHE_SORTANT);
				}
				else {
					setEficStatut(STATUT_DETACHE_SORTANT);
					setEficCPosition(POSITION_DETACHE_SORTANT);
				}

			}
			else {	// DETACHEMENT ENTRANT
				if (changement.estDetachementEntrant()) {
					setEficStatut(STATUT_DETACHE_ENTRANT);
					setEficCPosition(POSITION_DETACHE_ENTRANT);
				}
			}

		}		

	}

	/**
	 * 
	 * @return
	 */
	public String informationGradeCible() {
		if(toParamPromotion() == null)
			return "";
		String texte = (new StringBuilder(String.valueOf(toParamPromotion().gradeArrivee().llGrade()))).append("\t").toString();
		if(toParamPromotion().cEchelonArrivee() != null)
			texte = (new StringBuilder(String.valueOf(texte))).append(toParamPromotion().cEchelonArrivee()).toString();
		texte = (new StringBuilder(String.valueOf(texte))).append("\t").toString();
		if(toParamPromotion().cChevronArrivee() != null)
			texte = (new StringBuilder(String.valueOf(texte))).append(toParamPromotion().cChevronArrivee()).toString();
		return texte;
	}

	/**
	 * 
	 * @return
	 */
	public String informationGradeCibleAvecLibelle() {
		if(toParamPromotion() == null)
			return "";
		String texte = toParamPromotion().gradeArrivee().llGrade() + ", Echelon : ";
		if(toParamPromotion().cEchelonArrivee() != null)
			texte = texte + toParamPromotion().cEchelonArrivee();
		if(toParamPromotion().cChevronArrivee() != null) {
			texte = texte + ", Chevron : ";
			texte = texte + toParamPromotion().cChevronArrivee();
		}
		return texte;
	}

	/**
	 * 
	 * @param dateReference
	 * @return
	 */
	public String preparerConservationAnciennete(NSTimestamp dateReference) {

		String ancConservee = "000000";

		org.cocktail.mangue.common.utilities.DateCtrl.IntRef anneesRef = new org.cocktail.mangue.common.utilities.DateCtrl.IntRef();
		org.cocktail.mangue.common.utilities.DateCtrl.IntRef moisRef = new org.cocktail.mangue.common.utilities.DateCtrl.IntRef();
		org.cocktail.mangue.common.utilities.DateCtrl.IntRef joursRef = new org.cocktail.mangue.common.utilities.DateCtrl.IntRef();
		DateCtrl.joursMoisAnneesEntre(eficDEchelon(), dateReference, anneesRef, moisRef, joursRef, true, true);

		if(anneesRef.value != 0 || moisRef.value != 0 || joursRef.value != 0) {

			String nbJours = (new Integer(joursRef.value)).toString();
			if(nbJours.length() == 1)
				nbJours = "0" + nbJours;
			String nbMois = (new Integer(moisRef.value)).toString();
			if(nbMois.length() == 1)
				nbMois = "0" + nbMois;
			String nbAnnees = (new Integer(anneesRef.value)).toString();
			if(nbAnnees.length() == 1)
				nbAnnees = "0" + nbAnnees;

			ancConservee = nbAnnees + nbMois + nbJours;

		} 

		if (ancConservee.length() > 6) {
			ancConservee = "000000";
		}

		return ancConservee;

	}

	/**
	 * 
	 * @throws com.webobjects.foundation.NSValidation.ValidationException
	 */
	public void validerRecord() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (dCreation() == null)
			setDCreation(new NSTimestamp());

		if ( eficDNaissance() == null || DateCtrl.getYear(eficDNaissance()) >= 2010 ) {
			setEstValide(false, "Date de naissance erronée");
		}

		if(eficNomPatronymique().length() > 40) {
			setEstValide(false, "Nom de famille > 40 Car.");
		}        	

		if(toFichier().supfObjet().equals(EOSupInfoFichier.OBJET_PROM_EC) && (eficCSectionCnu() == null || eficCSectionCnu().equals("?")) ) {
			setEstValide(false, " Section CNU");
		}

		if (eficUai() == null || eficUai().equals("?")) {
			setEstValide(false, " - UAI absent");
		}

		if (eficCPaysNationalite() == null || eficCPaysNationalite().equals("?")) {
			setEstValide(false, "Pays Nationalité");
		}

		if(eficLieuPosition() == null || eficLieuPosition().equals("?")) {
			setEstValide(false, "Lieu Position ");
		}

		if(eficCPosition() == null) {
			setEstValide(false, "Code Position ");
		}

		if(eficDDebPostion() == null) {
			setEstValide(false, "Date Début Position ");
		}

		if(eficCCorps() == null) {
			setEstValide(false, "Corps non renseigné");
		}

		if(eficCGrade() == null) {
			setEstValide(false, "Grade non renseigné");
		}

		if(eficDGrade() == null) {
			setEstValide(false, "Date d'entrée dans le grade non renseigné");
		}

		if(eficDEchelon() == null) {
			setEstValide(false, "Date Echelon");
		}

		if(eficDNominationCorps() == null) {
			setEstValide(false, "Date Nomin Corps ");
		}

		if(eficDTitularisation() == null) {
			setEstValide(false, "Date Titularisation ");
		}

		setDModification(new NSTimestamp());

	}

	/**
	 * 
	 * @param ec
	 * @param fichier
	 * @param individu
	 * @return
	 */
	public static EOSupInfoData rechercherDemande(EOEditingContext ec, EOSupInfoFichier fichier, EOIndividu individu) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_FICHIER_KEY + "=%@", new NSArray(fichier)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));

	}

	/**
	 *
	 * @param edc
	 * @param fichier
	 * @return
	 */
	public static NSArray<EOSupInfoData> findForFichier(EOEditingContext edc, EOSupInfoFichier fichier) {
		try {
			NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_FICHIER_KEY + " = %@", new NSArray(fichier)));
			NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(TO_INDIVIDU_KEY + "." + EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
			sorts.addObject(EOSortOrdering.sortOrderingWithKey(TO_INDIVIDU_KEY + "." + EOIndividu.PRENOM_KEY, EOSortOrdering.CompareAscending));
			return fetchAll(edc, new EOAndQualifier(mesQualifiers), sorts);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return new NSArray();
	}


	/**
	 * 
	 * @param edc
	 * @param fichier
	 * @param individu
	 * @return
	 */
	public static EOSupInfoData findForFichierAndIndividu(EOEditingContext edc, EOSupInfoFichier fichier, EOIndividu individu) {
		try
		{
			NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_FICHIER_KEY + " = %@", new NSArray(fichier)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));
			return fetchFirstByQualifier(edc, new EOAndQualifier(mesQualifiers));
		}
		catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param edc
	 * @param fichier
	 * @param parametres
	 * @return
	 */
	public static NSArray<EOSupInfoData> findForFichierAndPromotions(EOEditingContext edc, EOSupInfoFichier fichier , NSArray<EOParamPromotion> parametres) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>(EOQualifier.qualifierWithQualifierFormat(TO_FICHIER_KEY + " = %@", new NSArray(fichier)));
		if(parametres != null && parametres.count() > 0) {
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			for (EOParamPromotion myParam : parametres) {
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_PARAM_PROMOTION_KEY + " = %@", new NSArray<EOParamPromotion>(myParam)));
			}			
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(TO_INDIVIDU_KEY + "." + EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
		sorts.addObject(new EOSortOrdering(TO_INDIVIDU_KEY + "."+EOIndividu.PRENOM_KEY, EOSortOrdering.CompareAscending));

		return fetchAll(edc, new EOAndQualifier(qualifiers), sorts);

	}

	/**
	 * 
	 */
	public void validateForInsert() throws com.webobjects.foundation.NSValidation.ValidationException	{
	}

	public void validateForUpdate() throws com.webobjects.foundation.NSValidation.ValidationException	{
	}

	public void validateForDelete()	throws com.webobjects.foundation.NSValidation.ValidationException	{
		super.validateForDelete();
	}

	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException  	{		
		setDModification(DateCtrl.today());
	}

	public void validateBeforeTransactionSave()	throws com.webobjects.foundation.NSValidation.ValidationException {
	}

}
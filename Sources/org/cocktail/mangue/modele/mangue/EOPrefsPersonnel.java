// EOPrefsPersonnel.java
// Created on Tue Mar 14 08:58:19 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue;

import org.cocktail.fwkcktlwebapp.common.util.SystemCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/** les valeurs par defaut en l'absence de preferences utilisateur sont reglees
 * dans le fichier JavaClient.wod. Cela permet de definir des reglages propres a chaque 
 * universite
 */
public class EOPrefsPersonnel extends _EOPrefsPersonnel {

	public EOPrefsPersonnel() {
		super();
	}

	public boolean afficherPhoto() {
		return photo() != null && photo().equals(CocktailConstantes.VRAI);
	}
	public void setAfficherPhoto(boolean aBool) {
		setPhoto((aBool)?"O":"N");
	}
	public boolean modeFenetre() {
		return temFenetre() != null && temFenetre().equals(CocktailConstantes.VRAI);
	}
	public void setModeFenetre(boolean aBool) {
		setTemFenetre((aBool)?"O":"N");
	}
	public boolean exclureHeberges() {
		return temExcluHeberges() != null && temExcluHeberges().equals(CocktailConstantes.VRAI);
	}
	public void setExclureHeberges(boolean aBool) {
		setTemExcluHeberges((aBool)?"O":"N");
	}
	public boolean afficherCongePourAnneeUniv() {
		return temCongeAnneeUniv() != null && temCongeAnneeUniv().equals(CocktailConstantes.VRAI);
	}
	public void setAfficherCongePourAnneeUniv(boolean aBool) {
		setTemCongeAnneeUniv((aBool)?"O":"N");
	}
	public boolean afficherNouvelleListe() {
		return temListeAgentsNew() != null && temListeAgentsNew().equals("O");
	}
	public void setAfficherNouvelleListe(boolean aBool) {
		setTemListeAgentsNew((aBool)?"O":"N");
	}	
	public boolean afficherListeAgentsAGauche() {
		return positionListeAgents() != null && positionListeAgents().equals("G");
	}
	public void setAfficherListeAgentsAGauche(boolean aBool) {
		setPositionListeAgents((aBool)?"G":"D");
	}
	public boolean afficherAgentsAuDemarrage() {
		return temAfficherAuDemarrage() != null && temAfficherAuDemarrage().equals(CocktailConstantes.VRAI);
	}
	public void setAfficherAgentsAuDemarrage(boolean aBool) {
		setTemAfficherAuDemarrage((aBool)?"O":"N");
	}
	public boolean afficherPostes() {
		return temAfficherPostes() != null && temAfficherPostes().equals(CocktailConstantes.VRAI);
	}
	public void setAfficherPostes(boolean aBool) {
		setTemAfficherPostes((aBool)?"O":"N");
	}
	public boolean afficherEmplois() {
		return temAfficherEmplois() != null && temAfficherEmplois().equals(CocktailConstantes.VRAI);
	}
	public void setAfficherEmplois(boolean aBool) {
		setTemAfficherEmplois((aBool)?"O":"N");
	}
	/** initialise les preferences avec un jeu de valeurs par defaut fourni dans un dictionnaire */
	public void initAvecAgent(EOAgentPersonnel agent,NSDictionary valeursParDefaut) {
		if (valeursParDefaut != null) {
			String valeurParDefaut = (String)valeursParDefaut.objectForKey("prefsModeFenetre");
			if (valeurParDefaut != null && (valeurParDefaut.equals(CocktailConstantes.VRAI) || valeurParDefaut.equals(CocktailConstantes.FAUX))) {
				setTemFenetre(valeurParDefaut);
			} else {
				setTemFenetre(CocktailConstantes.VRAI);
			}
			valeurParDefaut = (String)valeursParDefaut.objectForKey("prefsAfficherAgents");
			if (valeurParDefaut != null && (valeurParDefaut.equals(CocktailConstantes.VRAI) || valeurParDefaut.equals(CocktailConstantes.FAUX))) {
				setTemAfficherAuDemarrage(valeurParDefaut);
			} else {
				setTemAfficherAuDemarrage(CocktailConstantes.FAUX);
			}
			if (agent.peutAfficherInfosPerso()) {
				valeurParDefaut = (String)valeursParDefaut.objectForKey("prefsTypeAdresse");
				if (valeurParDefaut != null && (valeurParDefaut.equals(EOTypeAdresse.TYPE_PERSONNELLE) || valeurParDefaut.equals(EOTypeAdresse.TYPE_PROFESSIONNELLE))) {
					addObjectToBothSidesOfRelationshipWithKey(SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"TypeAdresse","code",valeurParDefaut),"typeAdresse");
				} else {
					addObjectToBothSidesOfRelationshipWithKey(SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"TypeAdresse","code",EOTypeAdresse.TYPE_PERSONNELLE),"typeAdresse");
				}
			} else {
				addObjectToBothSidesOfRelationshipWithKey(SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"TypeAdresse","code",EOTypeAdresse.TYPE_PROFESSIONNELLE),"typeAdresse");
			}
			valeurParDefaut = (String)valeursParDefaut.objectForKey("prefsAfficherEmplois");
			if (valeurParDefaut != null && (valeurParDefaut.equals(CocktailConstantes.VRAI) || valeurParDefaut.equals(CocktailConstantes.FAUX))) {
				setTemAfficherEmplois(valeurParDefaut);
			} else {
				setTemAfficherEmplois(CocktailConstantes.FAUX);
			}
			valeurParDefaut = (String)valeursParDefaut.objectForKey("prefsAfficherPhotos");
			if (valeurParDefaut != null && (valeurParDefaut.equals(CocktailConstantes.VRAI) || valeurParDefaut.equals(CocktailConstantes.FAUX))) {
				setPhoto(valeurParDefaut);
			} else {
				setPhoto(CocktailConstantes.FAUX);
			}
			valeurParDefaut = (String)valeursParDefaut.objectForKey("prefsAbsenceAnneeUniversitaireParDefaut");
			if (valeurParDefaut != null && (valeurParDefaut.equals(CocktailConstantes.VRAI) || valeurParDefaut.equals(CocktailConstantes.FAUX))) {
				setTemCongeAnneeUniv(valeurParDefaut);
			} else {
				setTemCongeAnneeUniv(CocktailConstantes.VRAI);
			}
			valeurParDefaut = (String)valeursParDefaut.objectForKey("prefsTypeConge");
			if (valeurParDefaut != null && valeurParDefaut.equals("TOUS") == false) {
				EOTypeAbsence typeAbsence = EOTypeAbsence.findForCode(editingContext(),valeurParDefaut);
				if (typeAbsence != null) {
					addObjectToBothSidesOfRelationshipWithKey(typeAbsence,"typeAbsence");
				}
			}
			if (agent.gereTousAgents()) {
				try {
					Integer nombreParDefaut = (Integer)valeursParDefaut.objectForKey("prefsTypePersonnel");
					if (nombreParDefaut.intValue() > ManGUEConstantes.TOUT_PERSONNEL) {	// en dehors des valeurs légales
						throw new Exception("");
					}
					setTypePersonnelAuDemarrage(nombreParDefaut);
				}	catch (Exception e) {			// valeur par défaut non définie ou non nombre entier ou en-dehors des valeurs légales
					setTypePersonnelAuDemarrage(valeurTypePersonnelPourAgent(agent));
				}
			} else {
				setTypePersonnelAuDemarrage(valeurTypePersonnelPourAgent(agent));
			}
			if (agent.peutAfficherEmplois() || agent.peutGererEmplois()) {
				valeurParDefaut = (String)valeursParDefaut.objectForKey("prefsAfficherEmplois");
				if (valeurParDefaut != null && (valeurParDefaut.equals(CocktailConstantes.VRAI) || valeurParDefaut.equals(CocktailConstantes.FAUX))) {
					setTemAfficherEmplois(valeurParDefaut);
				} else {
					setTemAfficherEmplois(CocktailConstantes.FAUX);
				}
				try {
					Integer nombreParDefaut = (Integer)valeursParDefaut.objectForKey("prefsTypeEmploi");
					if (nombreParDefaut.intValue() > ManGUEConstantes.TOUT_EMPLOI) {	// en dehors des valeurs légales
						throw new Exception("");
					}
					setTypeEmploiAuDemarrage(nombreParDefaut);
				} catch (Exception e) {			// valeur par défaut non définie ou non nombre entier ou en-dehors des valeurs légales
					setTypeEmploiAuDemarrage(new Integer(ManGUEConstantes.TOUT_EMPLOI));
				}
			} else {
				setTemAfficherEmplois(CocktailConstantes.FAUX);
				setTypeEmploiAuDemarrage(new Integer(ManGUEConstantes.TOUT_EMPLOI));
			}

			if (agent.peutAfficherPostes()  || agent.peutGererPostes()) {
				valeurParDefaut = (String)valeursParDefaut.objectForKey("prefsAfficherPostes");
				if (valeurParDefaut != null && (valeurParDefaut.equals(CocktailConstantes.VRAI) || valeurParDefaut.equals(CocktailConstantes.FAUX))) {
					setTemAfficherPostes(valeurParDefaut);
				} else {
					setTemAfficherPostes(CocktailConstantes.FAUX);
				}
				try {
					Integer nombreParDefaut = (Integer)valeursParDefaut.objectForKey("prefsTypePoste");
					if (nombreParDefaut.intValue() > ManGUEConstantes.TOUT_POSTE) {	// en dehors des valeurs légales
						throw new Exception("");
					}
					setTypePosteAuDemarrage(nombreParDefaut);
				} catch (Exception e) {			// valeur par défaut non définie ou non nombre entier ou en-dehors des valeurs légales
					setTypePosteAuDemarrage(new Integer(ManGUEConstantes.TOUT_POSTE));
				}
			} else {
				setTemAfficherPostes(CocktailConstantes.FAUX);
				setTypePosteAuDemarrage(new Integer(ManGUEConstantes.TOUT_POSTE));
			}
		} else {
			setTemFenetre(CocktailConstantes.VRAI);
			setTemAfficherAuDemarrage(CocktailConstantes.FAUX);
			setTemAfficherEmplois(CocktailConstantes.FAUX);
			setTypeEmploiAuDemarrage(new Integer(ManGUEConstantes.TOUT_EMPLOI));
			setTemAfficherPostes(CocktailConstantes.FAUX);
			setTypePosteAuDemarrage(new Integer(ManGUEConstantes.TOUT_POSTE));
			setPhoto(CocktailConstantes.FAUX);
			setTemCongeAnneeUniv(CocktailConstantes.VRAI);
			setTypePersonnelAuDemarrage(valeurTypePersonnelPourAgent(agent));
			if (agent.peutAfficherInfosPerso()) {
				addObjectToBothSidesOfRelationshipWithKey(SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"TypeAdresse","tadrCode",EOTypeAdresse.TYPE_PERSONNELLE),"typeAdresse");
			} else {
				addObjectToBothSidesOfRelationshipWithKey(SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"TypeAdresse","tadrCode",EOTypeAdresse.TYPE_PROFESSIONNELLE),"typeAdresse");
			}
		}
		// 24/09/2010 - valeur par défaut pour le témoin hébergé
		setExclureHeberges(false);
		setDirectoryImpression(SystemCtrl.tempDir());
		addObjectToBothSidesOfRelationshipWithKey(agent.toIndividu(),"individu");
	}
	/** retourne la liste des types de population gérées par cet agent (EOTypePopulation) */
	public NSArray populationsGerees() {
		return populationsGerees(editingContext());
	}
	/** retourne la liste des types de population gérées par cet agent (EOTypePopulation). On fait le fetch dans l'editingContext pass&eacute; en param&egrave;tre */
	public NSArray populationsGerees(EOEditingContext editingContext) {
		if (typesPopulation() != null) {
			NSArray types = NSArray.componentsSeparatedByString(typesPopulation(),"-");
			return EOTypePopulation.rechercherTypePopulationAvecCodes(editingContext,types);
		} else {
			return null;
		}
	}
	/** ajoute une liste de types de population (EOTypePopulation) &agrave; la liste courante des populations g&eacute;r&eacute;es */
	public void ajouterTypesPopulations(NSArray nouveauxTypes) {
		NSMutableArray types = new NSMutableArray();
		if (typesPopulation() != null) {
			types.addObjectsFromArray(NSArray.componentsSeparatedByString(typesPopulation(),"-"));
		}
		java.util.Enumeration e = nouveauxTypes.objectEnumerator();
		while (e.hasMoreElements()) {
			EOTypePopulation type = (EOTypePopulation)e.nextElement();
			if (types.containsObject(type.code()) == false) {		// population pas encore sélectionnée
				types.addObject(type.code());
			}
		}
		setTypesPopulation(types.componentsJoinedByString("-"));
	}
	/** supprime une liste de types de population (EOTypePopulation) de la liste courante des populations g&eacute;r&eacute;es */
	public void supprimerTypesPopulations(NSArray typesASupprimer) {
		if (typesPopulation() != null) {
			NSArray types = NSArray.componentsSeparatedByString(typesPopulation(),"-");
			NSMutableArray nouveauxTypes = new NSMutableArray(types);
			java.util.Enumeration e = typesASupprimer.objectEnumerator();
			while (e.hasMoreElements()) {
				EOTypePopulation type = (EOTypePopulation)e.nextElement();
				nouveauxTypes.removeObject(type.code());
			}
			if (nouveauxTypes.count() > 0) {
				setTypesPopulation(nouveauxTypes.componentsJoinedByString("-"));
			} else {
				setTypesPopulation(null);
			}
		}
	}

	// méthodes privées
	private Integer valeurTypePersonnelPourAgent(EOAgentPersonnel agent) {
		if (agent.gereTousAgents() == false) {	// dans les autres cas, on prend le paramétrage des préférences
			if (agent.gereEnseignants()) {
				return new Integer(ManGUEConstantes.ENSEIGNANT);
			} else if (agent.gereNonEnseignants()) {
				return new Integer(ManGUEConstantes.NON_ENSEIGNANT);
			} else {
				return null;
			}
		} else {
			return new Integer(ManGUEConstantes.TOUT_PERSONNEL);
		}
	}
	// méthodes statiques
	/** retourne les pr&eacute;f&eacute;rences utilisateur ou null si pas de pr&eacute;f&eacute;rences */
	public static EOPrefsPersonnel rechercherPreferencesPourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		NSArray args = new NSArray(individu);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@",args);
		EOFetchSpecification fs = new EOFetchSpecification("PrefsPersonnel",qualifier,null);
		fs.setRefreshesRefetchedObjects(true);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		try {
			return (EOPrefsPersonnel)results.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
}

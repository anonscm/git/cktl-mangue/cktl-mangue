// _EOVisa.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVisa.java instead.
package org.cocktail.mangue.modele.mangue.visa;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOVisa extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "Visa";
	public static final String ENTITY_TABLE_NAME = "MANGUE.VISA";



	// Attributes


	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String L_VISA_KEY = "lVisa";
	public static final String NO_SEQ_VISA_KEY = "noSeqVisa";
	public static final String TEM_DECONCENTRATION_KEY = "temDeconcentration";
	public static final String TEM_LOCAL_KEY = "temLocal";

// Attributs non visibles
	public static final String NO_SEQ_VISA_TEXTE_KEY = "noSeqVisaTexte";
	public static final String NO_SEQ_VISA_COMPLEMENT_KEY = "noSeqVisaComplement";
	public static final String C_TYPE_VISA_KEY = "cTypeVisa";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String L_VISA_COLKEY = "L_VISA";
	public static final String NO_SEQ_VISA_COLKEY = "NO_SEQ_VISA";
	public static final String TEM_DECONCENTRATION_COLKEY = "TEM_DECONCENTRATION";
	public static final String TEM_LOCAL_COLKEY = "TEM_LOCAL";

	public static final String NO_SEQ_VISA_TEXTE_COLKEY = "NO_SEQ_VISA_TEXTE";
	public static final String NO_SEQ_VISA_COMPLEMENT_COLKEY = "NO_SEQ_VISA_COMPLEMENT";
	public static final String C_TYPE_VISA_COLKEY = "C_TYPE_VISA";


	// Relationships
	public static final String CGMOD_CONS_KEY = "cgmodCons";
	public static final String CGMOD_POPS_KEY = "cgmodPops";
	public static final String COMPLEMENT_KEY = "complement";
	public static final String TEXTE_KEY = "texte";
	public static final String TYPE_KEY = "type";
	public static final String VISA_CONTRATS_KEY = "visaContrats";
	public static final String VISA_TYPES_ARR_CORPS_KEY = "visaTypesArrCorps";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey(D_DEB_VAL_KEY);
  }

  public void setDDebVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_VAL_KEY);
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey(D_FIN_VAL_KEY);
  }

  public void setDFinVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VAL_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String lVisa() {
    return (String) storedValueForKey(L_VISA_KEY);
  }

  public void setLVisa(String value) {
    takeStoredValueForKey(value, L_VISA_KEY);
  }

  public Integer noSeqVisa() {
    return (Integer) storedValueForKey(NO_SEQ_VISA_KEY);
  }

  public void setNoSeqVisa(Integer value) {
    takeStoredValueForKey(value, NO_SEQ_VISA_KEY);
  }

  public String temDeconcentration() {
    return (String) storedValueForKey(TEM_DECONCENTRATION_KEY);
  }

  public void setTemDeconcentration(String value) {
    takeStoredValueForKey(value, TEM_DECONCENTRATION_KEY);
  }

  public String temLocal() {
    return (String) storedValueForKey(TEM_LOCAL_KEY);
  }

  public void setTemLocal(String value) {
    takeStoredValueForKey(value, TEM_LOCAL_KEY);
  }

  public org.cocktail.mangue.modele.mangue.visa.EOVisaComplement complement() {
    return (org.cocktail.mangue.modele.mangue.visa.EOVisaComplement)storedValueForKey(COMPLEMENT_KEY);
  }

  public void setComplementRelationship(org.cocktail.mangue.modele.mangue.visa.EOVisaComplement value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.visa.EOVisaComplement oldValue = complement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPLEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPLEMENT_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.visa.EOVisaTexte texte() {
    return (org.cocktail.mangue.modele.mangue.visa.EOVisaTexte)storedValueForKey(TEXTE_KEY);
  }

  public void setTexteRelationship(org.cocktail.mangue.modele.mangue.visa.EOVisaTexte value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.visa.EOVisaTexte oldValue = texte();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TEXTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TEXTE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.visa.EOTypeVisa type() {
    return (org.cocktail.mangue.modele.mangue.visa.EOTypeVisa)storedValueForKey(TYPE_KEY);
  }

  public void setTypeRelationship(org.cocktail.mangue.modele.mangue.visa.EOTypeVisa value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.visa.EOTypeVisa oldValue = type();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_KEY);
    }
  }
  
  public NSArray cgmodCons() {
    return (NSArray)storedValueForKey(CGMOD_CONS_KEY);
  }

  public NSArray cgmodCons(EOQualifier qualifier) {
    return cgmodCons(qualifier, null, false);
  }

  public NSArray cgmodCons(EOQualifier qualifier, boolean fetch) {
    return cgmodCons(qualifier, null, fetch);
  }

  public NSArray cgmodCons(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier("visa", EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      EOFetchSpecification fetchSpec = new EOFetchSpecification("VisaCgmodCon", qualifier, sortOrderings);
      fetchSpec.setIsDeep(true);
      results = (NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
    }
    else {
      results = cgmodCons();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCgmodConsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CGMOD_CONS_KEY);
  }

  public void removeFromCgmodConsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CGMOD_CONS_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord createCgmodConsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("VisaCgmodCon");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CGMOD_CONS_KEY);
    return (com.webobjects.eocontrol.EOGenericRecord) eo;
  }

  public void deleteCgmodConsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CGMOD_CONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCgmodConsRelationships() {
    Enumeration objects = cgmodCons().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCgmodConsRelationship((com.webobjects.eocontrol.EOGenericRecord)objects.nextElement());
    }
  }

  public NSArray cgmodPops() {
    return (NSArray)storedValueForKey(CGMOD_POPS_KEY);
  }

  public NSArray cgmodPops(EOQualifier qualifier) {
    return cgmodPops(qualifier, null, false);
  }

  public NSArray cgmodPops(EOQualifier qualifier, boolean fetch) {
    return cgmodPops(qualifier, null, fetch);
  }

  public NSArray cgmodPops(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier("visa", EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      EOFetchSpecification fetchSpec = new EOFetchSpecification("VisaCgmodPop", qualifier, sortOrderings);
      fetchSpec.setIsDeep(true);
      results = (NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
    }
    else {
      results = cgmodPops();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCgmodPopsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CGMOD_POPS_KEY);
  }

  public void removeFromCgmodPopsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CGMOD_POPS_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord createCgmodPopsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("VisaCgmodPop");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CGMOD_POPS_KEY);
    return (com.webobjects.eocontrol.EOGenericRecord) eo;
  }

  public void deleteCgmodPopsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CGMOD_POPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCgmodPopsRelationships() {
    Enumeration objects = cgmodPops().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCgmodPopsRelationship((com.webobjects.eocontrol.EOGenericRecord)objects.nextElement());
    }
  }

  public NSArray visaContrats() {
    return (NSArray)storedValueForKey(VISA_CONTRATS_KEY);
  }

  public NSArray visaContrats(EOQualifier qualifier) {
    return visaContrats(qualifier, null, false);
  }

  public NSArray visaContrats(EOQualifier qualifier, boolean fetch) {
    return visaContrats(qualifier, null, fetch);
  }

  public NSArray visaContrats(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier("visa", EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      EOFetchSpecification fetchSpec = new EOFetchSpecification("VisaContrat", qualifier, sortOrderings);
      fetchSpec.setIsDeep(true);
      results = (NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
    }
    else {
      results = visaContrats();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToVisaContratsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    addObjectToBothSidesOfRelationshipWithKey(object, VISA_CONTRATS_KEY);
  }

  public void removeFromVisaContratsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, VISA_CONTRATS_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord createVisaContratsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("VisaContrat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, VISA_CONTRATS_KEY);
    return (com.webobjects.eocontrol.EOGenericRecord) eo;
  }

  public void deleteVisaContratsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, VISA_CONTRATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllVisaContratsRelationships() {
    Enumeration objects = visaContrats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteVisaContratsRelationship((com.webobjects.eocontrol.EOGenericRecord)objects.nextElement());
    }
  }

  public NSArray visaTypesArrCorps() {
    return (NSArray)storedValueForKey(VISA_TYPES_ARR_CORPS_KEY);
  }

  public NSArray visaTypesArrCorps(EOQualifier qualifier) {
    return visaTypesArrCorps(qualifier, null, false);
  }

  public NSArray visaTypesArrCorps(EOQualifier qualifier, boolean fetch) {
    return visaTypesArrCorps(qualifier, null, fetch);
  }

  public NSArray visaTypesArrCorps(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier("visa", EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      EOFetchSpecification fetchSpec = new EOFetchSpecification("VisaTypeArreteCorps", qualifier, sortOrderings);
      fetchSpec.setIsDeep(true);
      results = (NSArray)editingContext().objectsWithFetchSpecification(fetchSpec);
    }
    else {
      results = visaTypesArrCorps();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToVisaTypesArrCorpsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    addObjectToBothSidesOfRelationshipWithKey(object, VISA_TYPES_ARR_CORPS_KEY);
  }

  public void removeFromVisaTypesArrCorpsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, VISA_TYPES_ARR_CORPS_KEY);
  }

  public com.webobjects.eocontrol.EOGenericRecord createVisaTypesArrCorpsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("VisaTypeArreteCorps");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, VISA_TYPES_ARR_CORPS_KEY);
    return (com.webobjects.eocontrol.EOGenericRecord) eo;
  }

  public void deleteVisaTypesArrCorpsRelationship(com.webobjects.eocontrol.EOGenericRecord object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, VISA_TYPES_ARR_CORPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllVisaTypesArrCorpsRelationships() {
    Enumeration objects = visaTypesArrCorps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteVisaTypesArrCorpsRelationship((com.webobjects.eocontrol.EOGenericRecord)objects.nextElement());
    }
  }


  public static EOVisa createVisa(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dDebVal
, NSTimestamp dModification
, String lVisa
, Integer noSeqVisa
, String temDeconcentration
, String temLocal
, org.cocktail.mangue.modele.mangue.visa.EOVisaComplement complement, org.cocktail.mangue.modele.mangue.visa.EOVisaTexte texte) {
    EOVisa eo = (EOVisa) createAndInsertInstance(editingContext, _EOVisa.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDDebVal(dDebVal);
		eo.setDModification(dModification);
		eo.setLVisa(lVisa);
		eo.setNoSeqVisa(noSeqVisa);
		eo.setTemDeconcentration(temDeconcentration);
		eo.setTemLocal(temLocal);
    eo.setComplementRelationship(complement);
    eo.setTexteRelationship(texte);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVisa.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVisa.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOVisa localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVisa)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVisa localInstanceIn(EOEditingContext editingContext, EOVisa eo) {
    EOVisa localInstance = (eo == null) ? null : (EOVisa)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVisa#localInstanceIn a la place.
   */
	public static EOVisa localInstanceOf(EOEditingContext editingContext, EOVisa eo) {
		return EOVisa.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVisa fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVisa fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVisa eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVisa)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVisa fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVisa fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVisa eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVisa)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVisa fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVisa eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVisa ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVisa fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}

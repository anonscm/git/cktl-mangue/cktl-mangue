//EOVisa.java
//Created on Wed Jun 07 16:11:34 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.visa;

import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOVisa extends _EOVisa implements RecordAvecLibelleEtCode {

	public EOVisa() {
		super();
	}

	public static EOVisa creer(EOEditingContext ec, EOTypeVisa typeVisa) {

		EOVisa newObject = (EOVisa) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.setTypeRelationship(typeVisa);
		newObject.setEstDeconcentre(false);
		newObject.setEstLocal(true);

		return newObject;

	}

	public boolean estLocal() {
		return temLocal() != null && temLocal().equals(CocktailConstantes.VRAI);
	}
	public void setEstLocal(boolean aBool) {
		if (aBool) {
			setTemLocal(CocktailConstantes.VRAI);
		} else {
			setTemLocal(CocktailConstantes.FAUX);
		}
	}
	public boolean estDeconcentre() {
		return temDeconcentration() != null && temDeconcentration().equals(CocktailConstantes.VRAI);
	}
	public void setEstDeconcentre(boolean aBool) {
		if (aBool) {
			setTemDeconcentration(CocktailConstantes.VRAI);
		} else {
			setTemDeconcentration(CocktailConstantes.FAUX);
		}
	}
	public String dateDebut() {
		return SuperFinder.dateFormatee(this,D_DEB_VAL_KEY);
	}
	public void setDateDebut(String uneDate) {
		if (uneDate == null) {
			setDDebVal(null);
		} else {
			SuperFinder.setDateFormatee(this,D_DEB_VAL_KEY,uneDate);
		}
	}
	public String dateFin() {
		return SuperFinder.dateFormatee(this,D_FIN_VAL_KEY);
	}
	public void setDateFin(String uneDate) {
		if (uneDate == null) {
			setDFinVal(null);
		} else {
			SuperFinder.setDateFormatee(this,D_FIN_VAL_KEY,uneDate);
		}
	}
	public void initAvecType(EOTypeVisa typeVisa) {
		setEstLocal(true);
		setEstDeconcentre(false);
		setDateDebut(DateCtrl.dateToString(new NSTimestamp()));
		setTypeRelationship(typeVisa);
	}
	public void supprimerRelations() {		
		setTypeRelationship(null);
		setTexteRelationship(null);
		setComplementRelationship(null);
	}
	/** Verifie que la date de debut de validite est fournie et qu'elle est anterieure a la date 
	 * de fin et que le texte et le type du visa sont fournis
	 */
	public void validateForSave() {
		if (dDebVal() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de début de validité");
		}
		if (dFinVal() != null && dDebVal().after(dFinVal())) {
			throw new NSValidation.ValidationException("La date de fin doit être postérieure à la date de début !");
		}
		if (lVisa() == null || lVisa().length() == 0) {
			throw new NSValidation.ValidationException("Vous devez fournir un texte de visa");
		}
		if (type() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un type de visa");
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
	// Interface RecordAvecLibelleEtCode
	public String code() {
		if (type() != null) {
			return type().llTypeVisa();
		} else {
			return "";
		}
	}
	public String libelle() {
		return lVisa();
	}
	// Méthodes statiques
	
	/**
	 * 
	 * @param editingContext
	 * @param cTypeContratTrav
	 * @return Visas associes a un type de contrat
	 */
	public static NSArray<EOVisa> rechercherVisaPourTypeContrat(EOEditingContext editingContext,String cTypeContratTrav) {
		
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOVisaContrat.C_TYPE_CONTRAT_TRAV_KEY + " = %@",new NSArray(cTypeContratTrav));
		EOFetchSpecification fs = new EOFetchSpecification(EOVisaContrat.ENTITY_NAME, qualifier, criteresTri());
		return visasValides((NSArray<EOVisa>)editingContext.objectsWithFetchSpecification(fs).valueForKey(EOVisaContrat.VISA_KEY));
	}
	/** retourne tous les visas valides pour un type d'arrete, un type de population et un corps tries par ordre croissant de validite */
	public static NSArray rechercherVisaPourTypeArreteTypePopulationEtCorps(EOEditingContext editingContext,String cTypeArrete,String cTypePopulation,String cCorps) {
		NSMutableArray args = new NSMutableArray(cTypeArrete);
		args.addObject(cTypePopulation); args.addObject(cCorps);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("cTypeArrete = %@ AND cTypePopulation = %@ AND cCorps = %@",args);
		EOFetchSpecification fs = new EOFetchSpecification("VisaTypeArreteCorps",qualifier,criteresTri());
		return visasValides((NSArray)editingContext.objectsWithFetchSpecification(fs).valueForKey("visa"));
	}
	/** retourne tous les visas valides pour un type d'absence et un type de population tries par ordre croissant de validite */
	public static NSArray rechercherVisaPourTypeCongeEtTypePopulation(EOEditingContext editingContext,String cTypeAbsence,String cTypePopulation) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOTypeAbsence.CODE_KEY + "=%@",new NSArray(cTypeAbsence));
			EOTypeAbsence type = EOTypeAbsence.fetchFirstByQualifier(editingContext, qualifier);

			NSMutableArray args = new NSMutableArray(type.codeHarpege());
			args.addObject(cTypePopulation);
			qualifier = EOQualifier.qualifierWithQualifierFormat("nomTableCgmod = %@ AND (cTypePopulation = %@ OR cTypePopulation = nil OR cTypePopulation = '')",args);
			EOFetchSpecification fs = new EOFetchSpecification(EOVisaCgmodPop.ENTITY_NAME,qualifier,criteresTri());
			return visasValides((NSArray)editingContext.objectsWithFetchSpecification(fs).valueForKey("visa"));
		} catch (Exception e) {
			return null;
		}
	}
	/** retourne tous les visas valides pour un table conge et un type de population tries par ordre croissant de validite */
	public static NSArray rechercherVisaPourTableCongeEtTypePopulation(EOEditingContext editingContext,String tableConge,String cTypePopulation) {
		return rechercherVisaPourTableCongeTypePopulationEtCondition(editingContext, tableConge, cTypePopulation, null);
	}

	/** retourne tous les visas valides pour un table conge et un type de population tries par ordre croissant de validite verifiant
	 * une condition supplementaire
	 * @param tableConge nom de la table de conge
	 * @param cTypePopulation type de population recherchee
	 * @param condition supplementaire a remplir (peut etre nulle) */
	public static NSArray<EOVisa> rechercherVisaPourTableCongeTypePopulationEtCondition(EOEditingContext edc,String tableConge,String cTypePopulation,String condition) {

		try {			
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaCgmodPop.NOM_TABLE_CGMOD_KEY + "=%@", new NSArray(tableConge)));

			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaCgmodPop.C_TYPE_POPULATION_KEY + "=%@", new NSArray(cTypePopulation)));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaCgmodPop.C_TYPE_POPULATION_KEY + "=nil", null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaCgmodPop.C_TYPE_POPULATION_KEY + "=''", null));

			qualifiers.addObject(new EOOrQualifier(orQualifiers));		

			NSArray<EOVisaCgmodPop> visas = EOVisaCgmodPop.fetchAll(edc, 
					new EOAndQualifier(qualifiers), 
					criteresTri());

			if (condition != null) {
				visas = extraireVisasPourCondition(visas,condition);
			}
			return visasValides((NSArray<EOVisa>)visas.valueForKey(EOVisaCgmodPop.VISA_KEY));	
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param cTypeAbsence
	 * @param cTypeContratTrav
	 * @return
	 */
	public static NSArray rechercherVisaPourTypeCongeEtTypeContrat(EOEditingContext editingContext,String cTypeAbsence,String cTypeContratTrav) {

		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOTypeAbsence.CODE_KEY + "=%@",new NSArray(cTypeAbsence));
			EOTypeAbsence type = EOTypeAbsence.fetchFirstByQualifier(editingContext, qualifier);

			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaCgmodCon.NOM_TABLE_CGMOD_KEY + "=%@", new NSArray(type.codeHarpege())));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaCgmodCon.C_TYPE_CONTRAT_TRAV_KEY + "=%@", new NSArray(cTypeContratTrav)));			
			EOFetchSpecification fs = new EOFetchSpecification(EOVisaCgmodCon.ENTITY_NAME,new EOAndQualifier(qualifiers),criteresTri());
			return visasValides((NSArray)editingContext.objectsWithFetchSpecification(fs).valueForKey("visa"));

		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}


	/** retourne tous les visas valides pour un type d'absence et un type de contrat de travail tries par ordre croissant de validite, verifiant
	 * une condition supplementaire 
	 * @param tableConge nom de la table de conge
	 * @param cTypeContratTrav type de contrat recherche
	 * @param condition supplementaire a remplir (peut etre nulle) */

	public static NSArray<EOVisa> rechercherVisaPourTableCongeTypeContratEtCondition(EOEditingContext editingContext,String tableConge,String cTypeContratTrav,String condition) {
		NSMutableArray args = new NSMutableArray(tableConge);
		args.addObject(cTypeContratTrav);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOVisaCgmodCon.NOM_TABLE_CGMOD_KEY + " = %@ AND cTypeContratTrav = %@",args);
		EOFetchSpecification fs = new EOFetchSpecification(EOVisaCgmodCon.ENTITY_NAME,qualifier,criteresTri());
		NSArray<EOVisaCgmodCon> visas = (NSArray)editingContext.objectsWithFetchSpecification(fs);
		if (condition != null) {
			visas = extraireVisasPourCondition(visas,condition);
		}
		return visasValides((NSArray<EOVisa>)visas.valueForKey(EOVisaCgmodCon.VISA_KEY));	
	}
	// méthodes privees
	// Ne garde que les visas valides (ie date fin et postérieure à date du jour) 
	// Classe les visas dans par ordre de priorité (code, lois, décrets,…) croissante et par date croissante
	private static NSArray criteresTri() {
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("visa.type.niveauPriorite",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("visa.dDebVal",EOSortOrdering.CompareAscending));
		return sorts;
	}
	private static NSArray extraireVisasPourCondition(NSArray visas,String condition) {
		NSMutableArray results = new NSMutableArray();
		for (java.util.Enumeration<VisaPourConge> e = visas.objectEnumerator();e.hasMoreElements();) {
			VisaPourConge visa = e.nextElement();
			if (visa.conditionSupplementaire() == null) {
				results.addObject(visa);
			} else {
				if (visa.conditionSupplementaire().equals(condition)) {
					results.addObject(visa);
				}
			}
		}
		return results;
	}
	private static NSArray visasValides(NSArray visas) {
		NSMutableArray results = new NSMutableArray();
		//		 pour ramener à une date sans prendre en compte les heures
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		for (java.util.Enumeration<EOVisa> e = visas.objectEnumerator();e.hasMoreElements();) {
			EOVisa visa = e.nextElement();
			if (visa.dFinVal() == null || DateCtrl.isAfter(visa.dFinVal(),today)) {
				results.addObject(visa);
			}
		}
		return results;
	}


}

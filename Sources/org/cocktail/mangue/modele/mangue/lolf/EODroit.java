// EODroit.java
// Created on Wed Oct 26 08:25:06  2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.lolf;

import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** r&egrave;gles de validation 
 * validit&eacute; des dates
 * au moins un type d'info sur lequel s'applique le droit doit &ecirc;tre fourni
 * @author christine
 *
 */
public class EODroit extends EOGenericRecord {

  public static String CODE_DROIT_LECTURE = "R";
  public static String CODE_DROIT_ECRITURE = "W";
   
    public EODroit() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EODroit(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
      public EOIndividu toIndividu() {
        return (EOIndividu)storedValueForKey("toIndividu");
    }

    public void setToIndividu(EOIndividu value) {
        takeStoredValueForKey(value, "toIndividu");
    }

    public EOIndividu toDroitIndividu() {
        return (EOIndividu)storedValueForKey("toDroitIndividu");
    }

    public void setToDroitIndividu(EOIndividu value) {
        takeStoredValueForKey(value, "toDroitIndividu");
    }

    public EOStructure toDroitStructure() {
        return (EOStructure)storedValueForKey("toDroitStructure");
    }

    public void setToDroitStructure(EOStructure value) {
        takeStoredValueForKey(value, "toDroitStructure");
    }

    public org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste toDroitFicheDePoste() {
        return (org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste)storedValueForKey("toDroitFicheDePoste");
    }

    public void setToDroitFicheDePoste(org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste value) {
        takeStoredValueForKey(value, "toDroitFicheDePoste");
    }

    public org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf toDroitFicheLolf() {
        return (EOFicheLolf)storedValueForKey("toDroitFicheLolf");
    }

    public void setToDroitFicheLolf(EOFicheLolf value) {
        takeStoredValueForKey(value, "toDroitFicheLolf");
    }
    public org.cocktail.mangue.modele.mangue.lolf.EOPoste toDroitPoste() {
        return (org.cocktail.mangue.modele.mangue.lolf.EOPoste)storedValueForKey("toDroitPoste");
    }

    public void setToDroitPoste(org.cocktail.mangue.modele.mangue.lolf.EOPoste value) {
        takeStoredValueForKey(value, "toDroitPoste");
    }
    
    // methodes rajoutées
    public String libelle() {
    		if (toDroitPoste() != null) {
    			return toDroitPoste().libelle();
    		}
    		if (toDroitFicheDePoste() != null) {
    			return toDroitFicheDePoste().libelle();
    		}
    		if (toDroitFicheLolf() != null) {
    			return toDroitFicheLolf().libelle();
    		}
    		if (toDroitStructure() != null) {
    			return toDroitStructure().llStructure();
    		}
    		if (toDroitIndividu() != null) {
    			return toDroitIndividu().identite();
    		}
    		return null;
    }
    	public void initAvecIndividu(EOIndividu individu) {
    		addObjectToBothSidesOfRelationshipWithKey(individu,"toIndividu");
    	}
  	public void supprimerRelations() {
  		if (toDroitPoste() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toDroitPoste(),"toDroitPoste");
		}
		if (toDroitFicheDePoste() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toDroitFicheDePoste(),"toDroitFicheDePoste");
		}
		if (toDroitFicheLolf() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toDroitFicheLolf(),"toDroitFicheLolf");
		}
		if (toDroitStructure() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toDroitStructure(),"toDroitStructure");
		}
		if (toDroitIndividu() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toDroitIndividu(),"toDroitIndividu");
		}
		if (toIndividu() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(toIndividu(),"toIndividu");
		}
    	}
  	public void validateForSave() {
  		if (toIndividu() == null) {
  			throw new NSValidation.ValidationException("Vous devez fournir l'individu");
  		}
  		if (toDroitPoste() == null && toDroitFicheDePoste() == null && toDroitFicheLolf() == null && toDroitStructure() == null && toDroitIndividu() == null) {
  			throw new NSValidation.ValidationException("Vous devez fournir l'élément auquel ce droit est lié");
  		}
  	}
    // méthodes statiques
    /** recherche les droits d'acc&egrave;s d'un individu pour une certaine p&eacute;riode
     */
    public static NSArray rechercherDroitPourIndividuPourPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    		EOQualifier qualifier = SuperFinder.qualifierPourPeriode("droDDebut",debutPeriode,"droDFin",finPeriode);
    		NSMutableArray qualifiers = new NSMutableArray(qualifier);
    		qualifier = EOQualifier.qualifierWithQualifierFormat("toIndividu = %@", new NSArray(individu));
    		qualifiers.addObject(qualifier);
    		EOFetchSpecification fs = new EOFetchSpecification("Droit",new EOAndQualifier(qualifiers),null);
    		return editingContext.objectsWithFetchSpecification(fs);
			
    }
    /** recherche les droits d'acc&egrave;s d'un individu
     */
    public static NSArray rechercherDroitPourIndividu(EOEditingContext editingContext,EOIndividu individu) {
    		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("toIndividu = %@", new NSArray(individu));
    		EOFetchSpecification fs = new EOFetchSpecification("Droit",qualifier,null);
    		return editingContext.objectsWithFetchSpecification(fs);
			
    }
}

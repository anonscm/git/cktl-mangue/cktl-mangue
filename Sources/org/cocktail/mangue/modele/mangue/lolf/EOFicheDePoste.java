//EOFicheDePoste.java
//Created on Wed Oct 26 08:25:07  2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.lolf;

import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;


/** Regles de validation :<BR>
 * Verification des longueurs de chaines<BR>
 * Verification de la super classe<BR>
 * */
public class EOFicheDePoste extends _EOFicheDePoste {
	public static int LONGUEUR_CONTEXTE = 2000;
	public static int LONGUEUR_MISSION = 2000;

	public EOFicheDePoste() {
		super();
		typeFiche = FICHE_DE_POSTE;
	}

	public void initAvecPoste(EOPoste poste) {
		setFdpVisaAgent(CocktailConstantes.FAUX);
		setFdpVisaDirec(CocktailConstantes.FAUX);
		setFdpVisaResp(CocktailConstantes.FAUX);
		super.initAvecPoste(poste);
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		if (toEmploiType() != null) {
			setToEmploiTypeRelationship(null);
		}
		if (referensEmplois() != null) {
			setReferensEmploisRelationship(null);
		}
	}
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		super.validateForSave();
		if (fdpContexteTravail() != null && fdpContexteTravail().length() > LONGUEUR_CONTEXTE) {
			throw new NSValidation.ValidationException("Le contexte de travail comporte au plus "+ LONGUEUR_CONTEXTE + " caractères");
		}
		if (fdpMissionPoste() != null && fdpMissionPoste().length() > LONGUEUR_MISSION) {
			throw new NSValidation.ValidationException("La mission comporte au plus " + LONGUEUR_MISSION + " caractères");

		}
	}
	public boolean fdpVisaAgentBool()                    { return fdpVisaAgent() != null && CocktailConstantes.VRAI.equals(fdpVisaAgent()); }
	public void setFdpVisaAgentBool(boolean value)       { setFdpVisaAgent(CocktailConstantes.FAUX); if (value == true)  setFdpVisaAgent(CocktailConstantes.VRAI); }
	public boolean fdpVisaRespBool()                     { return fdpVisaResp() != null && CocktailConstantes.VRAI.equals(fdpVisaResp()); }
	public void setFdpVisaRespBool(boolean value)        { setFdpVisaResp(CocktailConstantes.FAUX); if (value == true)  setFdpVisaResp(CocktailConstantes.VRAI); }
	public boolean fdpVisaDirecBool()                    { return fdpVisaDirec() != null && CocktailConstantes.VRAI.equals(fdpVisaDirec()); }
	public void setFdpVisaDirecBool(boolean value)       { setFdpVisaDirec(CocktailConstantes.FAUX); if (value == true)  setFdpVisaDirec(CocktailConstantes.VRAI); }

	public String activites() {
		String s = "";
		for (java.util.Enumeration<EORepartFdpActi> e = tosRepartFdpActi().objectEnumerator();e.hasMoreElements();) {
			EORepartFdpActi repart = e.nextElement();
			s = s + "- " + repart.referensActivite().intitulactivite() + "\n";
		}
		return s;
	}
	public String competences() {
		String s = "";
		for (java.util.Enumeration<EORepartFdpComp> e = tosRepartFdpComp().objectEnumerator();e.hasMoreElements();) {
			EORepartFdpComp repart = e.nextElement();
			s = s + "- " + repart.referensCompetence().intitulcomp() + "\n";
		}
		return s;
	}
	public String autre() {
		String s = "";
		for (java.util.Enumeration<EORepartFdpAutre> e = tosRepartFdpAutre().objectEnumerator();e.hasMoreElements();) {
			EORepartFdpAutre repart = e.nextElement();
			s = s + "- " + repart.fauActiviteAutre() + "\n";
		}
		return s;
	}
	// méthodes statiques
	public static NSArray rechercherFichesPourPoste(EOEditingContext editingContext,EOPoste poste) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_POSTE_KEY + " = %@",new NSArray(poste));
		return fetchAll(editingContext, qualifier);
	}

}

//EOPoste.java
//Created on Wed Oct 26 08:25:09  2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.lolf;

import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation :<BR>
 * Le code, le libelle, la date debut et la structure doivent etre fournis<BR>
 *  Verification des longueurs de chaines<BR>
 *  Verification de la validite des dates<BR>
 * */
public class EOPoste extends _EOPoste implements RecordAvecLibelleEtCode {

	public EOPoste() {
		super();
	}

	public void supprimerRelations() {
		setToStructureRelationship(null);
		// normalement il n'y a plus d'affectations sinon on ne peut supprimer le poste
		//les fiches de poste et fiches lolf sont supprimées automatiquement car en cascade
		/*	int total = tosFicheDePoste().count();
    		for (int i = 0; i < total;i++) {
    			EOFicheDePoste ficheDePoste = (EOFicheDePoste)tosFicheDePoste().objectAtIndex(0);
    			ficheDePoste.supprimerRelations();
    			editingContext().deleteObject(ficheDePoste);
    		}
    		total = tosFicheLolf().count();
 		for (int i = 0; i < total;i++) {
 			EOFicheLolf fiche = (EOFicheLolf)tosFicheDePoste().objectAtIndex(0);
 			fiche.supprimerRelations();
 			editingContext().deleteObject(fiche);
 		}*/
	}
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		if (posLibelle() == null || posLibelle().length() == 0) {
			throw new NSValidation.ValidationException("Le libellé doit être défini");
		}
		if (posLibelle().length() > 128) {
			throw new NSValidation.ValidationException("Le libellé comporte au plus 128 caractères");
		}
		if (posCode() == null || posCode().length() == 0) {
			throw new NSValidation.ValidationException("Le code doit être défini");
		}
		if (posCode().length() > 30) {
			throw new NSValidation.ValidationException("Le code comporte au plus 30 caractères");
		}
		// La date de fin ne doit pas etre inferieure a la date de debut
		if (posDDebut() == null) {
			throw new NSValidation.ValidationException("La date de début doit être définie !");
		} else if (posDFin() != null && posDFin().after(posDFin())) {
			throw new NSValidation.ValidationException("La date de fin doit être postérieure à la date de début !");
		}
		// Le service doit être sélectionné
		if (toStructure() == null) {
			throw new NSValidation.ValidationException("La structure doit être définie !");
		}
	}	
	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this, POS_D_DEBUT_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setPosDDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,POS_D_DEBUT_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,POS_D_FIN_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setPosDFin(null);
		} else {
			SuperFinder.setDateFormatee(this, POS_D_FIN_KEY,uneDate);
		}
	}

	/**
	 * est-ce que les dates de validites sont actuelles
	 */
	public boolean isOuvert() {
		// pour ramener à une date sans prendre en compte les heures 
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		return DateCtrl.isBeforeEq(posDDebut(), today) && (posDFin() == null || DateCtrl.isAfterEq(posDFin(), today));
	}

	/**
	 * la date de fin est passee
	 */
	public boolean isFerme() {
		//	pour ramener à une date sans prendre en compte les heures 
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		return (posDFin() != null && DateCtrl.isBefore(posDFin(), today));
	}

	/**
	 * la date de debut est pas encore atteinte
	 */
	public boolean isFutur() {
		//	pour ramener à une date sans prendre en compte les heures 
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		return DateCtrl.isAfter(posDDebut(), today);
	}
	/** retourne true si le poste est occupe */
	public boolean estOccupe() {
		NSArray affectationsDetails = EOAffectationDetail.rechercherDetailsPourPoste(editingContext(),this,null,false);
		return affectationsDetails != null && affectationsDetails.count() > 0;
	}

	/**
	 * occupant du poste : 
	 * - si le poste est ouvert : l'agent de l'affectation en cours 
	 * - si le poste est ferme : l'agent de la derniere des affectations 
	 * - si le poste n'existe pas
	 * encore : rien
	 * 
	 * @return identite de l'agent
	 */
	public String occupantDuPoste() {
		EOIndividu individu = occupantActuel();
		if (individu != null) {
			return individu.identite();
		} else {
			return "";
		}
	}
	public EOIndividu occupantActuel() {
		NSTimestamp date = null;
		if (isOuvert()) {
			//	pour ramener à une date sans prendre en compte les heures 
			date = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		} 
		//   rechercher les détails, triés par date décroissante
		NSArray affectationsDetails = EOAffectationDetail.rechercherDetailsPourPoste(editingContext(),this,date,true);
		if (affectationsDetails.count() > 0) {
			EOAffectation affectation = ((EOAffectationDetail) affectationsDetails.objectAtIndex(0)).toAffectation();
			if (affectation != null && affectation.individu() != null) {
				return affectation.individu();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	public EOStructure toComposante() {
		if (toStructure() != null && toStructure().toComposante() != null) {
			return toStructure().toComposante();
		} else {
			return null;
		}
	}
	public String composante() {
		try {
			return toComposante().llStructure();
		} catch (Exception e) {
			return null;
		}
	}
	public String infoPourComposante() {
		return infoPourType(EOStructureInfo.TYPE_MISSION_COMPOSANTE);
	}
	public String infoPourStructure() {
		return infoPourType(EOStructureInfo.TYPE_MISSION_SERVICE);
	}
	public String infoPourProjet() {
		return infoPourType(EOStructureInfo.TYPE_PROJET_SERVICE);
	}
	public EOFicheDePoste ficheDePosteCourante() {
		NSArray fiches = EOSortOrdering.sortedArrayUsingKeyOrderArray(tosFicheDePoste(),new NSArray(EOSortOrdering.sortOrderingWithKey("fdpDDebut",EOSortOrdering.CompareDescending)));
		return (EOFicheDePoste)ficheCourante(fiches);
	}
	public EOFicheLolf ficheLolfCourante() {
		NSArray fiches = EOSortOrdering.sortedArrayUsingKeyOrderArray(tosFicheLolf(),new NSArray(EOSortOrdering.sortOrderingWithKey("floDDebut",EOSortOrdering.CompareDescending)));
		return (EOFicheLolf)ficheCourante(fiches);
	}
	// interface RecordAvecLibelleEtCode
	public String libelle() {
		return posLibelle();
	}
	public String code() {
		return posCode();
	}

	// méthodes privées
	private Fiche ficheCourante(NSArray fichesTriees) {
		NSTimestamp date = null;
		if (isOuvert()) {
			// pour ramener à une date sans prendre en compte les heures 
			date = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		} else {
			date = posDFin();
		}

		// on prend la première fiche trouvée dont la date est postérieure à date
		for (java.util.Enumeration<Fiche> e = fichesTriees.objectEnumerator();e.hasMoreElements();) {
			Fiche fiche = e.nextElement();
			if (DateCtrl.isBeforeEq(fiche.dDebut(),date) && (fiche.dFin() == null || DateCtrl.isAfterEq(fiche.dFin(),date))) {
				return fiche;
			}
		}
		return null;
	}
	private String infoPourType(Integer type) {
		NSTimestamp dateRef = null;  
		EOFicheDePoste fiche = ficheDePosteCourante();
		//	pour ramener à une date sans prendre en compte les heures 
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		if (fiche.fdpDFin() != null && DateCtrl.isAfter(today, fiche.fdpDFin())) {
			dateRef = fiche.fdpDFin();
		} else {
			dateRef = today;
		}
		EOStructureInfo info = EOStructureInfo.rechercherStructureInfoPourStructureDateEtType(editingContext(),toStructure(),dateRef,type);
		if (info != null) {
			return info.sinLibelle();
		} else {
			return "";
		}
	}
	// méthodes statiques
	/** retourne le qualifier pour determiner les postes vacants a une date
	 * retourne null si les postes sont vacants */
	public static EOQualifier qualifierPourPostesVacants(EOEditingContext editingContext,NSTimestamp date) {
		// rechercher les affectations non terminées à la date début
		NSArray details = EOAffectationDetail.rechercherDetailsNonTerminesADate(editingContext,date,new NSArray(EOAffectationDetail.TO_POSTE_KEY));
		if (details.count() > 0) {
			NSMutableArray postesTrouves = new NSMutableArray(),args = new NSMutableArray();
			String stringQualifier = "";
			for (int i = 0; i < details.count(); i++) {
				EOPoste poste = ((EOAffectationDetail)details.objectAtIndex(i)).toPoste();
				if (postesTrouves.containsObject(poste) == false) {
					postesTrouves.addObject(poste);
					args.addObject(poste);
					if (i == 0) {
						stringQualifier = TOS_AFFECTATION_DETAIL_KEY+"."+EOAffectationDetail.TO_POSTE_KEY + " != %@";
					} else {
						stringQualifier = stringQualifier + " AND " + TOS_AFFECTATION_DETAIL_KEY+"."+EOAffectationDetail.TO_POSTE_KEY + " != %@";
					}
				}
			}
			return EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		} else {
			return null;
		}
	}
	/** recherche les postes ouverts pendant la periode
	 * @param editingContext
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray rechercherPostesOuvertsPourPeriode(EOEditingContext editingContext,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		EOQualifier qualifier = SuperFinder.qualifierPourPeriode(POS_D_DEBUT_KEY,debutPeriode,POS_D_FIN_KEY,finPeriode);
		return fetchAll(editingContext, qualifier, null);
	}
	/** retourne les postes ouverts a la date du jour */
	public static NSArray rechercherPostesOuverts(EOEditingContext editingContext) {
		//pour ramener à une date sans prendre en compte les heures 
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		return rechercherPostesOuvertsPourPeriode(editingContext,today,today);
	}
	/** recherche les postes vacants pendant la periode
	 * @param editingContext
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray rechercherPostesVacantsPourPeriode(EOEditingContext editingContext,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		EOQualifier qualifier = SuperFinder.qualifierPourPeriode(POS_D_DEBUT_KEY,debutPeriode,POS_D_FIN_KEY,finPeriode);
		qualifiers.addObject(qualifier);
		qualifiers.addObject(qualifierPourPostesVacants(editingContext,debutPeriode));
		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);
	}
	
	/** recherche les postes occupes pendant la periode
	 * @param editingContext
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray rechercherPostesOccupesPourPeriode(EOEditingContext editingContext,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		EOQualifier qualifier = SuperFinder.qualifierPourPeriode(POS_D_DEBUT_KEY,debutPeriode,POS_D_FIN_KEY,finPeriode);
		qualifiers.addObject(qualifier);
		qualifier = EOQualifier.qualifierWithQualifierFormat(TOS_AFFECTATION_DETAIL_KEY + " <> nil",null);
		qualifiers.addObject(qualifier);
		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);
	}
}

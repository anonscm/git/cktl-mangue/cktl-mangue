// _EOFicheDePoste.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFicheDePoste.java instead.
package org.cocktail.mangue.modele.mangue.lolf;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOFicheDePoste extends  Fiche {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "FicheDePoste";
	public static final String ENTITY_TABLE_NAME = "MANGUE.FICHE_DE_POSTE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fdpKey";

	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FDP_CONTEXTE_TRAVAIL_KEY = "fdpContexteTravail";
	public static final String FDP_D_DEBUT_KEY = "fdpDDebut";
	public static final String FDP_D_FIN_KEY = "fdpDFin";
	public static final String FDP_MISSION_POSTE_KEY = "fdpMissionPoste";
	public static final String FDP_VISA_AGENT_KEY = "fdpVisaAgent";
	public static final String FDP_VISA_DIREC_KEY = "fdpVisaDirec";
	public static final String FDP_VISA_RESP_KEY = "fdpVisaResp";

// Attributs non visibles
	public static final String ETY_CODE_KEY = "etyCode";
	public static final String FDP_KEY_KEY = "fdpKey";
	public static final String POS_KEY_KEY = "posKey";

//Colonnes dans la base de donnees
	public static final String CODEEMPLOI_COLKEY = "CODEEMPLOI";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String FDP_CONTEXTE_TRAVAIL_COLKEY = "FDP_CONTEXTE_TRAVAIL";
	public static final String FDP_D_DEBUT_COLKEY = "FDP_D_DEBUT";
	public static final String FDP_D_FIN_COLKEY = "FDP_D_FIN";
	public static final String FDP_MISSION_POSTE_COLKEY = "FDP_MISSION_POSTE";
	public static final String FDP_VISA_AGENT_COLKEY = "FDP_VISA_AGENT";
	public static final String FDP_VISA_DIREC_COLKEY = "FDP_VISA_DIREC";
	public static final String FDP_VISA_RESP_COLKEY = "FDP_VISA_RESP";

	public static final String ETY_CODE_COLKEY = "ETY_CODE";
	public static final String FDP_KEY_COLKEY = "FDP_KEY";
	public static final String POS_KEY_COLKEY = "POS_KEY";


	// Relationships
	public static final String REFERENS_EMPLOIS_KEY = "referensEmplois";
	public static final String TO_EMPLOI_TYPE_KEY = "toEmploiType";
	public static final String TO_POSTE_KEY = "toPoste";
	public static final String TOS_REPART_FDP_ACTI_KEY = "tosRepartFdpActi";
	public static final String TOS_REPART_FDP_AUTRE_KEY = "tosRepartFdpAutre";
	public static final String TOS_REPART_FDP_COMP_KEY = "tosRepartFdpComp";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String codeemploi() {
    return (String) storedValueForKey(CODEEMPLOI_KEY);
  }

  public void setCodeemploi(String value) {
    takeStoredValueForKey(value, CODEEMPLOI_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String fdpContexteTravail() {
    return (String) storedValueForKey(FDP_CONTEXTE_TRAVAIL_KEY);
  }

  public void setFdpContexteTravail(String value) {
    takeStoredValueForKey(value, FDP_CONTEXTE_TRAVAIL_KEY);
  }

  public NSTimestamp fdpDDebut() {
    return (NSTimestamp) storedValueForKey(FDP_D_DEBUT_KEY);
  }

  public void setFdpDDebut(NSTimestamp value) {
    takeStoredValueForKey(value, FDP_D_DEBUT_KEY);
  }

  public NSTimestamp fdpDFin() {
    return (NSTimestamp) storedValueForKey(FDP_D_FIN_KEY);
  }

  public void setFdpDFin(NSTimestamp value) {
    takeStoredValueForKey(value, FDP_D_FIN_KEY);
  }

  public String fdpMissionPoste() {
    return (String) storedValueForKey(FDP_MISSION_POSTE_KEY);
  }

  public void setFdpMissionPoste(String value) {
    takeStoredValueForKey(value, FDP_MISSION_POSTE_KEY);
  }

  public String fdpVisaAgent() {
    return (String) storedValueForKey(FDP_VISA_AGENT_KEY);
  }

  public void setFdpVisaAgent(String value) {
    takeStoredValueForKey(value, FDP_VISA_AGENT_KEY);
  }

  public String fdpVisaDirec() {
    return (String) storedValueForKey(FDP_VISA_DIREC_KEY);
  }

  public void setFdpVisaDirec(String value) {
    takeStoredValueForKey(value, FDP_VISA_DIREC_KEY);
  }

  public String fdpVisaResp() {
    return (String) storedValueForKey(FDP_VISA_RESP_KEY);
  }

  public void setFdpVisaResp(String value) {
    takeStoredValueForKey(value, FDP_VISA_RESP_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois referensEmplois() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois)storedValueForKey(REFERENS_EMPLOIS_KEY);
  }

  public void setReferensEmploisRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois oldValue = referensEmplois();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, REFERENS_EMPLOIS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, REFERENS_EMPLOIS_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.lolf.EOEmploiType toEmploiType() {
    return (org.cocktail.mangue.modele.mangue.lolf.EOEmploiType)storedValueForKey(TO_EMPLOI_TYPE_KEY);
  }

  public void setToEmploiTypeRelationship(org.cocktail.mangue.modele.mangue.lolf.EOEmploiType value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.lolf.EOEmploiType oldValue = toEmploiType();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EMPLOI_TYPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EMPLOI_TYPE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.lolf.EOPoste toPoste() {
    return (org.cocktail.mangue.modele.mangue.lolf.EOPoste)storedValueForKey(TO_POSTE_KEY);
  }

  public void setToPosteRelationship(org.cocktail.mangue.modele.mangue.lolf.EOPoste value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.lolf.EOPoste oldValue = toPoste();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_POSTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_POSTE_KEY);
    }
  }
  
  public NSArray tosRepartFdpActi() {
    return (NSArray)storedValueForKey(TOS_REPART_FDP_ACTI_KEY);
  }

  public NSArray tosRepartFdpActi(EOQualifier qualifier) {
    return tosRepartFdpActi(qualifier, null, false);
  }

  public NSArray tosRepartFdpActi(EOQualifier qualifier, boolean fetch) {
    return tosRepartFdpActi(qualifier, null, fetch);
  }

  public NSArray tosRepartFdpActi(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpActi.TO_FICHE_DE_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.lolf.EORepartFdpActi.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFdpActi();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFdpActiRelationship(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpActi object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REPART_FDP_ACTI_KEY);
  }

  public void removeFromTosRepartFdpActiRelationship(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpActi object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_FDP_ACTI_KEY);
  }

  public org.cocktail.mangue.modele.mangue.lolf.EORepartFdpActi createTosRepartFdpActiRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartFdpActi");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REPART_FDP_ACTI_KEY);
    return (org.cocktail.mangue.modele.mangue.lolf.EORepartFdpActi) eo;
  }

  public void deleteTosRepartFdpActiRelationship(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpActi object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_FDP_ACTI_KEY);
  }

  public void deleteAllTosRepartFdpActiRelationships() {
    Enumeration objects = tosRepartFdpActi().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFdpActiRelationship((org.cocktail.mangue.modele.mangue.lolf.EORepartFdpActi)objects.nextElement());
    }
  }

  public NSArray tosRepartFdpAutre() {
    return (NSArray)storedValueForKey(TOS_REPART_FDP_AUTRE_KEY);
  }

  public NSArray tosRepartFdpAutre(EOQualifier qualifier) {
    return tosRepartFdpAutre(qualifier, null, false);
  }

  public NSArray tosRepartFdpAutre(EOQualifier qualifier, boolean fetch) {
    return tosRepartFdpAutre(qualifier, null, fetch);
  }

  public NSArray tosRepartFdpAutre(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpAutre.TO_FICHE_DE_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.lolf.EORepartFdpAutre.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFdpAutre();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFdpAutreRelationship(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpAutre object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REPART_FDP_AUTRE_KEY);
  }

  public void removeFromTosRepartFdpAutreRelationship(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpAutre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_FDP_AUTRE_KEY);
  }

  public org.cocktail.mangue.modele.mangue.lolf.EORepartFdpAutre createTosRepartFdpAutreRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartFdpAutre");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REPART_FDP_AUTRE_KEY);
    return (org.cocktail.mangue.modele.mangue.lolf.EORepartFdpAutre) eo;
  }

  public void deleteTosRepartFdpAutreRelationship(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpAutre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_FDP_AUTRE_KEY);
  }

  public void deleteAllTosRepartFdpAutreRelationships() {
    Enumeration objects = tosRepartFdpAutre().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFdpAutreRelationship((org.cocktail.mangue.modele.mangue.lolf.EORepartFdpAutre)objects.nextElement());
    }
  }

  public NSArray tosRepartFdpComp() {
    return (NSArray)storedValueForKey(TOS_REPART_FDP_COMP_KEY);
  }

  public NSArray tosRepartFdpComp(EOQualifier qualifier) {
    return tosRepartFdpComp(qualifier, null, false);
  }

  public NSArray tosRepartFdpComp(EOQualifier qualifier, boolean fetch) {
    return tosRepartFdpComp(qualifier, null, fetch);
  }

  public NSArray tosRepartFdpComp(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpComp.TO_FICHE_DE_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.lolf.EORepartFdpComp.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosRepartFdpComp();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosRepartFdpCompRelationship(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpComp object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_REPART_FDP_COMP_KEY);
  }

  public void removeFromTosRepartFdpCompRelationship(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpComp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_FDP_COMP_KEY);
  }

  public org.cocktail.mangue.modele.mangue.lolf.EORepartFdpComp createTosRepartFdpCompRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartFdpComp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_REPART_FDP_COMP_KEY);
    return (org.cocktail.mangue.modele.mangue.lolf.EORepartFdpComp) eo;
  }

  public void deleteTosRepartFdpCompRelationship(org.cocktail.mangue.modele.mangue.lolf.EORepartFdpComp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_REPART_FDP_COMP_KEY);
  }

  public void deleteAllTosRepartFdpCompRelationships() {
    Enumeration objects = tosRepartFdpComp().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosRepartFdpCompRelationship((org.cocktail.mangue.modele.mangue.lolf.EORepartFdpComp)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOFicheDePoste avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFicheDePoste createEOFicheDePoste(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, NSTimestamp fdpDDebut
, String fdpVisaAgent
, String fdpVisaDirec
, String fdpVisaResp
, org.cocktail.mangue.modele.mangue.lolf.EOPoste toPoste			) {
    EOFicheDePoste eo = (EOFicheDePoste) createAndInsertInstance(editingContext, _EOFicheDePoste.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setFdpDDebut(fdpDDebut);
		eo.setFdpVisaAgent(fdpVisaAgent);
		eo.setFdpVisaDirec(fdpVisaDirec);
		eo.setFdpVisaResp(fdpVisaResp);
    eo.setToPosteRelationship(toPoste);
    return eo;
  }

  
	  public EOFicheDePoste localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFicheDePoste)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFicheDePoste creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFicheDePoste creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOFicheDePoste object = (EOFicheDePoste)createAndInsertInstance(editingContext, _EOFicheDePoste.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOFicheDePoste localInstanceIn(EOEditingContext editingContext, EOFicheDePoste eo) {
    EOFicheDePoste localInstance = (eo == null) ? null : (EOFicheDePoste)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFicheDePoste#localInstanceIn a la place.
   */
	public static EOFicheDePoste localInstanceOf(EOEditingContext editingContext, EOFicheDePoste eo) {
		return EOFicheDePoste.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFicheDePoste fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFicheDePoste fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFicheDePoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFicheDePoste)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFicheDePoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFicheDePoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFicheDePoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFicheDePoste)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFicheDePoste fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFicheDePoste eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFicheDePoste ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFicheDePoste fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

// _EOPoste.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPoste.java instead.
package org.cocktail.mangue.modele.mangue.lolf;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOPoste extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Poste";
	public static final String ENTITY_TABLE_NAME = "MANGUE.POSTE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "posKey";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String POS_CODE_KEY = "posCode";
	public static final String POS_D_DEBUT_KEY = "posDDebut";
	public static final String POS_D_FIN_KEY = "posDFin";
	public static final String POS_LIBELLE_KEY = "posLibelle";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String POS_KEY_KEY = "posKey";
	public static final String C_LOGE_KEY = "cLoge";
	public static final String C_STRUCTURE_KEY = "cStructure";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String POS_CODE_COLKEY = "POS_CODE";
	public static final String POS_D_DEBUT_COLKEY = "POS_D_DEBUT";
	public static final String POS_D_FIN_COLKEY = "POS_D_FIN";
	public static final String POS_LIBELLE_COLKEY = "POS_LIBELLE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String POS_KEY_COLKEY = "POS_KEY";
	public static final String C_LOGE_COLKEY = "C_LOGE";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";


	// Relationships
	public static final String TOS_AFFECTATION_DETAIL_KEY = "tosAffectationDetail";
	public static final String TOS_FICHE_DE_POSTE_KEY = "tosFicheDePoste";
	public static final String TOS_FICHE_LOLF_KEY = "tosFicheLolf";
	public static final String TO_STRUCTURE_KEY = "toStructure";
	public static final String TYPE_LOGEMENT_KEY = "typeLogement";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String posCode() {
    return (String) storedValueForKey(POS_CODE_KEY);
  }

  public void setPosCode(String value) {
    takeStoredValueForKey(value, POS_CODE_KEY);
  }

  public NSTimestamp posDDebut() {
    return (NSTimestamp) storedValueForKey(POS_D_DEBUT_KEY);
  }

  public void setPosDDebut(NSTimestamp value) {
    takeStoredValueForKey(value, POS_D_DEBUT_KEY);
  }

  public NSTimestamp posDFin() {
    return (NSTimestamp) storedValueForKey(POS_D_FIN_KEY);
  }

  public void setPosDFin(NSTimestamp value) {
    takeStoredValueForKey(value, POS_D_FIN_KEY);
  }

  public String posLibelle() {
    return (String) storedValueForKey(POS_LIBELLE_KEY);
  }

  public void setPosLibelle(String value) {
    takeStoredValueForKey(value, POS_LIBELLE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOStructure toStructure() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOLoge typeLogement() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOLoge)storedValueForKey(TYPE_LOGEMENT_KEY);
  }

  public void setTypeLogementRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOLoge value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOLoge oldValue = typeLogement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_LOGEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_LOGEMENT_KEY);
    }
  }
  
  public NSArray tosAffectationDetail() {
    return (NSArray)storedValueForKey(TOS_AFFECTATION_DETAIL_KEY);
  }

  public NSArray tosAffectationDetail(EOQualifier qualifier) {
    return tosAffectationDetail(qualifier, null, false);
  }

  public NSArray tosAffectationDetail(EOQualifier qualifier, boolean fetch) {
    return tosAffectationDetail(qualifier, null, fetch);
  }

  public NSArray tosAffectationDetail(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail.TO_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosAffectationDetail();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosAffectationDetailRelationship(org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_AFFECTATION_DETAIL_KEY);
  }

  public void removeFromTosAffectationDetailRelationship(org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_AFFECTATION_DETAIL_KEY);
  }

  public org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail createTosAffectationDetailRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("AffectationDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_AFFECTATION_DETAIL_KEY);
    return (org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail) eo;
  }

  public void deleteTosAffectationDetailRelationship(org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_AFFECTATION_DETAIL_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosAffectationDetailRelationships() {
    Enumeration objects = tosAffectationDetail().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosAffectationDetailRelationship((org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail)objects.nextElement());
    }
  }

  public NSArray tosFicheDePoste() {
    return (NSArray)storedValueForKey(TOS_FICHE_DE_POSTE_KEY);
  }

  public NSArray tosFicheDePoste(EOQualifier qualifier) {
    return tosFicheDePoste(qualifier, null, false);
  }

  public NSArray tosFicheDePoste(EOQualifier qualifier, boolean fetch) {
    return tosFicheDePoste(qualifier, null, fetch);
  }

  public NSArray tosFicheDePoste(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste.TO_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosFicheDePoste();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosFicheDePosteRelationship(org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_FICHE_DE_POSTE_KEY);
  }

  public void removeFromTosFicheDePosteRelationship(org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_FICHE_DE_POSTE_KEY);
  }

  public org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste createTosFicheDePosteRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FicheDePoste");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_FICHE_DE_POSTE_KEY);
    return (org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste) eo;
  }

  public void deleteTosFicheDePosteRelationship(org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_FICHE_DE_POSTE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosFicheDePosteRelationships() {
    Enumeration objects = tosFicheDePoste().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosFicheDePosteRelationship((org.cocktail.mangue.modele.mangue.lolf.EOFicheDePoste)objects.nextElement());
    }
  }

  public NSArray tosFicheLolf() {
    return (NSArray)storedValueForKey(TOS_FICHE_LOLF_KEY);
  }

  public NSArray tosFicheLolf(EOQualifier qualifier) {
    return tosFicheLolf(qualifier, null, false);
  }

  public NSArray tosFicheLolf(EOQualifier qualifier, boolean fetch) {
    return tosFicheLolf(qualifier, null, fetch);
  }

  public NSArray tosFicheLolf(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf.TO_POSTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tosFicheLolf();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTosFicheLolfRelationship(org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_FICHE_LOLF_KEY);
  }

  public void removeFromTosFicheLolfRelationship(org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_FICHE_LOLF_KEY);
  }

  public org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf createTosFicheLolfRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FicheLolf");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_FICHE_LOLF_KEY);
    return (org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf) eo;
  }

  public void deleteTosFicheLolfRelationship(org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_FICHE_LOLF_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosFicheLolfRelationships() {
    Enumeration objects = tosFicheLolf().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosFicheLolfRelationship((org.cocktail.mangue.modele.mangue.lolf.EOFicheLolf)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPoste avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPoste createEOPoste(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String posCode
, NSTimestamp posDDebut
, String posLibelle
, String temValide
, org.cocktail.mangue.modele.grhum.referentiel.EOStructure toStructure			) {
    EOPoste eo = (EOPoste) createAndInsertInstance(editingContext, _EOPoste.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPosCode(posCode);
		eo.setPosDDebut(posDDebut);
		eo.setPosLibelle(posLibelle);
		eo.setTemValide(temValide);
    eo.setToStructureRelationship(toStructure);
    return eo;
  }

  
	  public EOPoste localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPoste)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPoste creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPoste creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPoste object = (EOPoste)createAndInsertInstance(editingContext, _EOPoste.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPoste localInstanceIn(EOEditingContext editingContext, EOPoste eo) {
    EOPoste localInstance = (eo == null) ? null : (EOPoste)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPoste#localInstanceIn a la place.
   */
	public static EOPoste localInstanceOf(EOEditingContext editingContext, EOPoste eo) {
		return EOPoste.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPoste fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPoste fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPoste)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPoste)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPoste fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPoste eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPoste ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPoste fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

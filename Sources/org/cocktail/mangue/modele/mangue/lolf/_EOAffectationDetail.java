// _EOAffectationDetail.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAffectationDetail.java instead.
package org.cocktail.mangue.modele.mangue.lolf;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOAffectationDetail extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "AffectationDetail";
	public static final String ENTITY_TABLE_NAME = "MANGUE.AFFECTATION_DETAIL";

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "adeKey";

	public static final String ADE_D_DEBUT_KEY = "adeDDebut";
	public static final String ADE_D_FIN_KEY = "adeDFin";
	public static final String AFF_QUOTITE_KEY = "affQuotite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

// Attributs non visibles
	public static final String NO_SEQ_AFFECTATION_KEY = "noSeqAffectation";
	public static final String POS_KEY_KEY = "posKey";
	public static final String ADE_KEY_KEY = "adeKey";

//Colonnes dans la base de donnees
	public static final String ADE_D_DEBUT_COLKEY = "ADE_D_DEBUT";
	public static final String ADE_D_FIN_COLKEY = "ADE_D_FIN";
	public static final String AFF_QUOTITE_COLKEY = "AFF_QUOTITE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";

	public static final String NO_SEQ_AFFECTATION_COLKEY = "NO_SEQ_AFFECTATION";
	public static final String POS_KEY_COLKEY = "POS_KEY";
	public static final String ADE_KEY_COLKEY = "ADE_KEY";


	// Relationships
	public static final String TO_AFFECTATION_KEY = "toAffectation";
	public static final String TO_POSTE_KEY = "toPoste";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp adeDDebut() {
    return (NSTimestamp) storedValueForKey(ADE_D_DEBUT_KEY);
  }

  public void setAdeDDebut(NSTimestamp value) {
    takeStoredValueForKey(value, ADE_D_DEBUT_KEY);
  }

  public NSTimestamp adeDFin() {
    return (NSTimestamp) storedValueForKey(ADE_D_FIN_KEY);
  }

  public void setAdeDFin(NSTimestamp value) {
    takeStoredValueForKey(value, ADE_D_FIN_KEY);
  }

  public java.math.BigDecimal affQuotite() {
    return (java.math.BigDecimal) storedValueForKey(AFF_QUOTITE_KEY);
  }

  public void setAffQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AFF_QUOTITE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOAffectation toAffectation() {
    return (org.cocktail.mangue.modele.mangue.individu.EOAffectation)storedValueForKey(TO_AFFECTATION_KEY);
  }

  public void setToAffectationRelationship(org.cocktail.mangue.modele.mangue.individu.EOAffectation value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOAffectation oldValue = toAffectation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_AFFECTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_AFFECTATION_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.lolf.EOPoste toPoste() {
    return (org.cocktail.mangue.modele.mangue.lolf.EOPoste)storedValueForKey(TO_POSTE_KEY);
  }

  public void setToPosteRelationship(org.cocktail.mangue.modele.mangue.lolf.EOPoste value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.lolf.EOPoste oldValue = toPoste();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_POSTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_POSTE_KEY);
    }
  }
  

/**
 * Créer une instance de EOAffectationDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAffectationDetail createEOAffectationDetail(EOEditingContext editingContext, NSTimestamp adeDDebut
, java.math.BigDecimal affQuotite
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.mangue.modele.mangue.individu.EOAffectation toAffectation, org.cocktail.mangue.modele.mangue.lolf.EOPoste toPoste			) {
    EOAffectationDetail eo = (EOAffectationDetail) createAndInsertInstance(editingContext, _EOAffectationDetail.ENTITY_NAME);    
		eo.setAdeDDebut(adeDDebut);
		eo.setAffQuotite(affQuotite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToAffectationRelationship(toAffectation);
    eo.setToPosteRelationship(toPoste);
    return eo;
  }

  
	  public EOAffectationDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAffectationDetail)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAffectationDetail creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAffectationDetail creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOAffectationDetail object = (EOAffectationDetail)createAndInsertInstance(editingContext, _EOAffectationDetail.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAffectationDetail localInstanceIn(EOEditingContext editingContext, EOAffectationDetail eo) {
    EOAffectationDetail localInstance = (eo == null) ? null : (EOAffectationDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAffectationDetail#localInstanceIn a la place.
   */
	public static EOAffectationDetail localInstanceOf(EOEditingContext editingContext, EOAffectationDetail eo) {
		return EOAffectationDetail.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAffectationDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAffectationDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAffectationDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAffectationDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAffectationDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAffectationDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAffectationDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAffectationDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAffectationDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAffectationDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAffectationDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAffectationDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

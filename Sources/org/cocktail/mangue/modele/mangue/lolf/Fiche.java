// EOFicheDePoste.java
// Created on Wed Oct 26 08:25:07  2005 by Apple EOModeler Version 5.2


/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.lolf;

import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


/** R&egrave;gles de validation :<BR>
 * La date d&eacute;but et le poste doivent &ecir;tre fournis<BR>
 * V&eacute;rification de la validit&eacute; des dates<BR>
 * V&eacute;rification de la validit&eacute; des dates par rapport aux dates du poste<BR>
 * */
public class Fiche extends EOGenericRecord implements RecordAvecLibelle {
	  public final static int FICHE_DE_POSTE = 1;
	  public final static int FICHE_LOLF = 2;
	
	  public int typeFiche;
	
	  public Fiche() {
	    super();
	  }
	
	  public NSTimestamp dDebut() {
	    if (typeFiche == FICHE_DE_POSTE) {
	      return (NSTimestamp) storedValueForKey("fdpDDebut");
	    } else {
	      return (NSTimestamp) storedValueForKey("floDDebut");
	    }
	  }
	
	  public void setDDebut(NSTimestamp value) {
	    if (typeFiche == FICHE_DE_POSTE) {
	      takeStoredValueForKey(value, "fdpDDebut");
	    } else {
	      takeStoredValueForKey(value, "floDDebut");
	    }
	  }
	
	  public NSTimestamp dFin() {
	    if (typeFiche == FICHE_DE_POSTE) {
	      return (NSTimestamp) storedValueForKey("fdpDFin");
	    } else {
	      return (NSTimestamp) storedValueForKey("floDFin");
	    }
	  }

	  public void setDFin(NSTimestamp value) {
	    if (typeFiche == FICHE_DE_POSTE) {
	      takeStoredValueForKey(value, "fdpDFin");
	    } else {
	      takeStoredValueForKey(value, "floDFin");
	    }
	  }
	
	  public org.cocktail.mangue.modele.mangue.lolf.EOPoste toPoste() {
	    return (org.cocktail.mangue.modele.mangue.lolf.EOPoste) storedValueForKey("toPoste");
	  }
	
	  public void setToPoste(org.cocktail.mangue.modele.mangue.lolf.EOPoste value) {
	    takeStoredValueForKey(value, "toPoste");
	  }
	
	  // methode rajoutees
	public void initAvecPoste(EOPoste poste) {
		addObjectToBothSidesOfRelationshipWithKey(poste,"toPoste");
	}
	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,"dDebut");
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setDDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,"dDebut",uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,"dFin");
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setDFin(null);
		} else {
			SuperFinder.setDateFormatee(this,"dFin",uneDate);
		}
	}
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		// La date de fin ne doit pas etre inferieure a la date de debut
		if (dDebut() == null) {
			throw new NSValidation.ValidationException("La date de début doit être définie !");
		} else if (dFin() != null && dDebut().after(dFin())) {
		throw new NSValidation.ValidationException("La date de fin doit être postérieure à la date de début !");
		}
		// Le poste doit être fourni
		if (toPoste() == null) {
			throw new NSValidation.ValidationException("Le poste doit être défini !");
		}
		// vérification par rapport aux dates du poste
		if (DateCtrl.isBefore(dDebut(),toPoste().posDDebut())) {
			throw new NSValidation.ValidationException("La date de début ne peut être antérieure à celle du poste !");
		}
		if (toPoste().posDFin() != null) {
			if (DateCtrl.isAfter(dDebut(),toPoste().posDFin())) {
				throw new NSValidation.ValidationException("La date de début ne peut être postérieure à la date de fin du poste !");
			}
			if (dFin() == null || (dFin() != null && DateCtrl.isAfter(dFin(),toPoste().posDFin()))) {
				throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de fin du poste !");
			}
		}
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(toPoste(),"toPoste");
	}
	// interface RecordAvecLibelle
	public String libelle() {
	  	String result = "";
	  	if (typeFiche == FICHE_DE_POSTE) {
	  		result = "Fdp du poste '"+ toPoste().posCode()  + "' du ";
	  	} else {
	  		result = "Flo du poste '"+ toPoste().posCode()  + "' du ";
	  	}
	  	result = result + DateCtrl.dateToString(dDebut()) + " au " + DateCtrl.dateToString(dFin());
	  	return result;
	}
	  /**
	   * les individus qui ont occupe ce poste pendant la duree de validite de la fiche du plus ancien au plus recent
	   */
	  public NSArray tosAffectationDetail() {
	    NSArray occupations = new NSArray();
	   /* for (int i = 0; i < toPoste().tosAffectationDetail().count(); i++) {
	      boolean conserver = false;
	      EOAffectationDetail affectationDetail = (EOAffectationDetail) toPoste().tosAffectationDetail().objectAtIndex(i);
	      if (affectationDetail.adeDFin() == null && dFin() == null) {
	        conserver = true;
	      } else if (affectationDetail.adeDFin() == null) {
	        if (DateCtrl.isBeforeEq(affectationDetail.adeDDebut(), dFin())) {
	          conserver = true;
	        }
	      } else if (dFin() == null) {
	        if (DateCtrl.isBeforeEq(dDebut(), affectationDetail.adeDFin())) {
	          conserver = true;
	        }
	      } else if (DateCtrl.isBeforeEq(dDebut(), affectationDetail.adeDDebut()) && DateCtrl.isAfterEq(dFin(), affectationDetail.adeDFin())) {
	        conserver = true;
	      } else if (DateCtrl.isAfterEq(dDebut(), affectationDetail.adeDDebut()) && DateCtrl.isAfterEq(dFin(), affectationDetail.adeDFin())) {
	        conserver = true;
	      } else if (DateCtrl.isBeforeEq(dDebut(), affectationDetail.adeDDebut()) && DateCtrl.isBeforeEq(dFin(), affectationDetail.adeDFin())) {
	        conserver = true;
	      } else if (DateCtrl.isAfterEq(dDebut(), affectationDetail.adeDDebut()) && DateCtrl.isBeforeEq(dFin(), affectationDetail.adeDFin())) {
	        conserver = true;
	      }
	      if (conserver) {
	        occupations = occupations.arrayByAddingObject(affectationDetail);
	      }
	    }
	    // classement
	*/    NSArray arraySort = new NSArray(EOSortOrdering.sortOrderingWithKey("adeDDebut", EOSortOrdering.CompareAscending));
	    return (NSArray) EOSortOrdering.sortedArrayUsingKeyOrderArray(occupations, arraySort);
	  }
	
	  /**
	   * occupant du poste : 
	   * - si le poste est ouvert : l'affectation en cours 
	   * - si le poste est fermÈ : la derniere des affectations 
	   * - si le poste n'existe pas
	   * encore : rien
	   * 
	   * @return
	   */
	  public EOAffectationDetail toAffectationDetailActuelle() {
	    EOAffectationDetail record = null;
	    if (toPoste().isOuvert()) {
	    	// pour ramener à une date sans prendre en compte les heures 
			NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
	      EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
	          "adeDDebut <= %@ AND (adeDFin >= %@ OR adeDFin = nil)", 
	          new NSArray(new NSTimestamp[] {today, today })
	      );
	      NSArray records = EOQualifier.filteredArrayWithQualifier(tosAffectationDetail(), qual);
	      if (records.count() > 0) {
	        record = (EOAffectationDetail) records.lastObject();
	      } 
	    } else if (toPoste().isFerme()) {
	      NSArray lesAffectationDetail = tosAffectationDetail();
	      if (lesAffectationDetail.count() > 0) {
	        record = (EOAffectationDetail) lesAffectationDetail.lastObject();
	      }
	    } 
	    return record;
	  }
}

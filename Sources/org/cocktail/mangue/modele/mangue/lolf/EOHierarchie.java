// EOHierarchie.java
// Created on Tue Nov 23 15:05:32  2004 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.lolf;

import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;


public class EOHierarchie extends EOGenericRecord {

    public EOHierarchie() {
        super();
    }
    
    public EOIndividuIdentite toIndividuResp() {
        return (EOIndividuIdentite)storedValueForKey("toIndividuResp");
    }

    public void setToIndividuResp(EOIndividuIdentite value) {
        takeStoredValueForKey(value, "toIndividuResp");
    }

    public EOIndividuIdentite toIndividu() {
        return (EOIndividuIdentite)storedValueForKey("toIndividu");
    }

    public void setToIndividu(EOIndividuIdentite value) {
        takeStoredValueForKey(value, "toIndividu");
    }
    
   public NSArray tosHierarchieNm1() {
    		return (NSArray) storedValueForKey("tosHierarchieNm1");
    }

    public void setTosHierarchieNm1(NSArray value) {
        takeStoredValueForKey(value, "tosHierarchieNm1");
    }
  
    public void addToTosHierarchieNm1(EOIndividuIdentite object) {
        includeObjectIntoPropertyWithKey(object, "tosHierarchieNm1");
    }

    public void removeFromTosHierarchieNm1(EOIndividuIdentite object) {
        excludeObjectFromPropertyWithKey(object, "tosHierarchieNm1");
    }
      
    // methodes rajoutees
    public Number noIndividu() {
    		if (toIndividu() != null) {
    			return toIndividu().noIndividu();
    		} else {
    			return null;
    		}
    }
    public Number noIndividuResp() {
    		if (toIndividuResp() == null) {
    			return null;
    		} else {
    			return toIndividuResp().noIndividu();
    		}
    }

    public String libelle() {
    		if (toIndividu() != null) {
    			return toIndividu().identite();
    		} else {
    			return null;
    		}
    }
    public void initAvecResponsable(EOIndividuIdentite responsable) {
    		if (responsable != null) {
    			addObjectToBothSidesOfRelationshipWithKey(responsable,"toIndividuResp");
    		}	
    }
    public void supprimerRelations() {
    		// supprimer les fils
    		int total = tosHierarchieNm1().count();
    		for (int i = 0;i < total;i++) {
    			EOHierarchie hierarchie = (EOHierarchie)tosHierarchieNm1().objectAtIndex(0);
    			hierarchie.supprimerRelations();
    			removeObjectFromBothSidesOfRelationshipWithKey(hierarchie,"tosHierarchieNm1");
    			editingContext().deleteObject(hierarchie);
    		}
    		removeObjectFromBothSidesOfRelationshipWithKey(toIndividu(),"toIndividu");
    		if (toIndividuResp() != null) {
    			removeObjectFromBothSidesOfRelationshipWithKey(toIndividuResp(),"toIndividuResp");
    		}
    }
   
}

// EOAffectationDetail.java
// Created on Tue Nov 15 16:04:14  2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.lolf;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
* Regles de validation des details d'affectation :<BR>
* 	La date de debut, le poste et l'affectattion doivent etre fournis<BR>
*	Si la date de fin est fournie, celle de debut doit etre anterieure<BR> 
*	Les dates de debut et de fin doivent etre a l'interieur des dates de l'affectation<BR>
* 	Les dates de debut et de fin doivent etre a l'interieur des dates du poste<BR>
*	La quotite doit etre positive et <= 100<BR>
*	La somme des quotites de tous les details sur la periode ne peut depasser 100<BR>
* @author christine
*/
public class EOAffectationDetail extends _EOAffectationDetail {

    public EOAffectationDetail() {
        super();
    }

    public void initAvecAffectation(EOAffectation affectation) {
    		setToAffectationRelationship(affectation);
    		setAdeDDebut(affectation.dateDebut());
    		setAdeDFin(affectation.dateFin());
    		setAffQuotite(new BigDecimal(100.00));
    }
    public void supprimerRelations() {
    		setToAffectationRelationship(null);
    		setToPosteRelationship(null);
    }
    public String dateDebut() {
		return SuperFinder.dateFormatee(this,ADE_D_DEBUT_KEY);
	}
	public void setDateDebut(String uneDate) {
		if (uneDate == null || uneDate.equals("")) {
			setAdeDDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,ADE_D_DEBUT_KEY,uneDate);
		}
	}
	public String dateFin() {
		return SuperFinder.dateFormatee(this,"adeDFin");
	}
	public void setDateFin(String uneDate) {
		if (uneDate == null || uneDate.equals("")) {
			setAdeDFin(null);
		} else {
			SuperFinder.setDateFormatee(this,"adeDFin",uneDate);
		}
	}
	public void validateForSave() {
		if (affQuotite() == null) {
			throw new NSValidation.ValidationException("La quotité doit être fournie");
		} else {
			double quotite = affQuotite().doubleValue();
			if (quotite <= 0) {
				throw new NSValidation.ValidationException("La quotité est une valeur positive");
			} else if (quotite > 100) {
				throw new NSValidation.ValidationException("La quotité ne peut pas dépasser 100%");
			}
		}
		// vérifie que la quotité totale sur la période ne dépasse pas 100
		verifierQuotites();
		if (toPoste() == null) {
			throw new NSValidation.ValidationException("Le poste doit être fourni");
		}
		if (toAffectation() == null) {
			throw new NSValidation.ValidationException("L'affectation doit être fournie");
		}
		if (DateCtrl.isBefore(adeDDebut(),toAffectation().dateDebut())) {
			throw new NSValidation.ValidationException("La date de début ne peut être antérieure à la date de début de l'affectation");
		}
		if (DateCtrl.isBefore(adeDDebut(),toPoste().posDDebut())) {
			throw new NSValidation.ValidationException("La date de début ne peut être antérieure à la date de début du poste l'affectation");
		}
		if (adeDFin() != null) {
			if (DateCtrl.isBefore(adeDFin(),adeDDebut())) {
				throw new NSValidation.ValidationException("La date de début doit être antérieure à la date de fin");
			}
			if (toAffectation().dateFin() != null && DateCtrl.isAfter(adeDFin(),toAffectation().dateFin())) {
				throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de fin de l'affectation");
			}
			if (toPoste().posDFin() != null && DateCtrl.isAfter(adeDFin(),toPoste().posDFin())) {
				throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de fin du poste");
			}
		} else {
			if (toAffectation().dateFin() != null) {
				throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de fin de l'affectation");
			}
			if (toPoste().posDFin() != null) {
				throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de fin du poste");
			}
		}
	}
	// Méthodes privées
	private void verifierQuotites() throws NSValidation.ValidationException {
		NSArray details = rechercherDetailsPourIndividuEtPeriode(editingContext(), toAffectation().individu(), adeDDebut(), adeDFin(),false);
		if (details.count() > 0) {
			double quotite = affQuotite().doubleValue();
			for (java.util.Enumeration<EOAffectationDetail> e = details.objectEnumerator();e.hasMoreElements();) {
				EOAffectationDetail detail = e.nextElement();
				if (detail != this) {
					quotite += affQuotite().doubleValue();
				}
			}
			if (quotite > 100) {
				throw new NSValidation.ValidationException("La quotité d'occupation de tous les postes sur la période est > 100");
			}
		}
		
	}
    // méthodes statiques
    /** Recherche les details d'affectation d'un poste a une date donnee 
	 * @param poste
	 * @param dateOcc date occupation (peut etre nulle) 
	 * @param trieParDate true si tries par date
	 */
	public static NSArray rechercherDetailsPourPoste(EOEditingContext ec, EOPoste poste, NSTimestamp dateOcc,boolean trieParDate) {
		NSMutableArray args = new NSMutableArray();
		args.addObject(poste);
		String stringQualifier = TO_POSTE_KEY + " = %@";
		if (dateOcc != null) {
			args.addObject(dateOcc);
			args.addObject(dateOcc);
			stringQualifier = stringQualifier + " and adeDDebut <=%@ and (adeDFin >= %@ or adeDFin = nil)";
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		NSArray sorts = null;
		if (trieParDate) {
			sorts = new NSArray(EOSortOrdering.sortOrderingWithKey("adeDDebut", EOSortOrdering.CompareDescending));
		}
		EOFetchSpecification myFetch = new EOFetchSpecification("AffectationDetail",qual,sorts);
		myFetch.setRefreshesRefetchedObjects(true);
		NSArray results = null;
		ec.lock();
		try {results = ec.objectsWithFetchSpecification(myFetch);}
		catch (Exception e) {}
		finally {ec.unlock();}
		return results;
	}
	/** Recherche les details d'affectation non termines a une certaine date */
	public static NSArray rechercherDetailsNonTerminesADate(EOEditingContext ec, NSTimestamp date,NSArray prefetches) {
		NSMutableArray args = new NSMutableArray();
		args.addObject(date);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(ADE_D_FIN_KEY + "> %@ OR adeDFin = nil",args);
		
		return fetchAll(ec, qual, null);
		
	}

	/** Recherche les details d'un un individu pour la periode passee en parametre
	 * @param individu
	 * @param dateDebut  
	 * @param dateFin
	 */
	public static NSArray rechercherDetailsPourIndividuEtPeriode(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut,NSTimestamp dateFin,boolean shouldRefresh) {
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_AFFECTATION_KEY+"."+EOAffectation.INDIVIDU_KEY + "=%@", new NSArray(individu)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_AFFECTATION_KEY+"."+EOAffectation.TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		if (dateDebut != null)
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(ADE_D_DEBUT_KEY, dateDebut, ADE_D_FIN_KEY, dateFin));

		return fetchAll(ec, new EOAndQualifier(qualifiers), null);
		
	}
}

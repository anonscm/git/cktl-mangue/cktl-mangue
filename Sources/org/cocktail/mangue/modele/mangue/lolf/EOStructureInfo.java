// EOStructureInfo.java
// Created on Mon Nov 14 15:32:24  2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.lolf;

import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EOStructureInfo extends EOGenericRecord {

  public static Integer TYPE_MISSION_COMPOSANTE = new Integer(1);
  public static Integer TYPE_MISSION_SERVICE = new Integer(2);
  public static Integer TYPE_PROJET_SERVICE = new Integer(3);
  
  
    public EOStructureInfo() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOStructureInfo(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String sinLibelle() {
        return (String)storedValueForKey("sinLibelle");
    }

    public void setSinLibelle(String value) {
        takeStoredValueForKey(value, "sinLibelle");
    }

    public Number sinType() {
        return (Number)storedValueForKey("sinType");
    }

    public void setSinType(Number value) {
        takeStoredValueForKey(value, "sinType");
    }

    public NSTimestamp sinDDebut() {
        return (NSTimestamp)storedValueForKey("sinDDebut");
    }

    public void setSinDDebut(NSTimestamp value) {
        takeStoredValueForKey(value, "sinDDebut");
    }

    public NSTimestamp sinDFin() {
        return (NSTimestamp)storedValueForKey("sinDFin");
    }

    public void setSinDFin(NSTimestamp value) {
        takeStoredValueForKey(value, "sinDFin");
    }
    

    public EOStructure toStructure() {
        return (EOStructure)storedValueForKey("toStructure");
    }

    public void setToStructure(EOStructure value) {
        takeStoredValueForKey(value, "toStructure");
    }
    // methodes rajoutees
    /** recherche l'info sur la structure d'un certain type &agrave; la date pass&eacute;e en r&eacute;f&eacute;rence */
    public static EOStructureInfo rechercherStructureInfoPourStructureDateEtType(EOEditingContext editingContext,EOStructure structure, NSTimestamp dateRef, Integer type) {
    		NSArray args = new NSArray(new Object[] { structure, type, dateRef, dateRef});	
    		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat( "toStructure = %@ AND sinType = %@ AND sinDDebut <= %@ AND (sinDFin >= %@ OR sinDFin = nil)",args);
    		EOFetchSpecification fs = new EOFetchSpecification("StructureInfo",qual,null);
    		NSArray records = editingContext.objectsWithFetchSpecification(fs);
    		try {
    			return (EOStructureInfo) records.lastObject();
    		} catch (Exception e) {
    			return null;
    		}
    }
}

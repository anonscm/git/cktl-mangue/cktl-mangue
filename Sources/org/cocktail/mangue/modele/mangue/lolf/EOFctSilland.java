// EOFctSilland.java
// Created on Fri Sep 23 15:52:47  2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.lolf;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;

public class EOFctSilland extends EOGenericRecord {

    public EOFctSilland() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOFctSilland(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String silLibelle() {
        return (String)storedValueForKey("silLibelle");
    }

    public void setFsiLibelle(String value) {
        takeStoredValueForKey(value, "silLibelle");
    }

    public String silIndicateur() {
        return (String)storedValueForKey("silIndicateur");
    }

    public void setFsiIndicateur(String value) {
        takeStoredValueForKey(value, "silIndicateur");
    }

    public String silProgAction() {
        return (String)storedValueForKey("silProgAction");
    }

    public void setFsiProgAction(String value) {
        takeStoredValueForKey(value, "silProgAction");
    }

    public NSTimestamp dCreation() {
        return (NSTimestamp)storedValueForKey("dCreation");
    }

    public void setDCreation(NSTimestamp value) {
        takeStoredValueForKey(value, "dCreation");
    }

    public NSTimestamp dModification() {
        return (NSTimestamp)storedValueForKey("dModification");
    }

    public void setDModification(NSTimestamp value) {
        takeStoredValueForKey(value, "dModification");
    }

    public String libelle() {
  		return silLibelle();
  }
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.impression;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

/** D&eacute;crit un param&egrave;tre d'impression avec sa valeur et sa description */
public class ParametreImpression implements NSKeyValueCoding {
	public static String TYPE_ENTIER = "E";
	public static String TYPE_REEL = "F";
	public static String TYPE_STRING = "S";
	public static String TYPE_DATE = "D";
	/** Types de param&egrave;tres g&eacute;r&eacute;s. Le premier caract&egrave;re du mot donne le type, il doit donc être unique */
	public static String[] typesParametres = {"String","Entier","Réel","Date"};	
	private EOParamModuleImpression parametreImpression;
	private Object valeur;
	
	// constructeur
	public ParametreImpression(EOParamModuleImpression parametreModule) {
		setParametreImpression(parametreModule);
		valeur = null;
	}
	// accesseurs
	public EOParamModuleImpression parametreImpression() {
		return parametreImpression;
	}
	public void setParametreImpression(EOParamModuleImpression parametreImpression) {
		this.parametreImpression = parametreImpression;
	}
	public Object valeur() {
		return valeur;
	}
	public void setValeur(Object valeur) {
		this.valeur = valeur;
	}
	public String descriptionParametre() {
		if (parametreImpression == null) {
			return null;
		} else {
			return parametreImpression.descriptionParametre();
		}
	}
	public boolean valeurValide() {
		if (valeur == null || parametreImpression().typeParametre() == null) {
			return false;
		}
		if (parametreImpression().typeParametre().equals(TYPE_ENTIER)) {
			try {
				Integer integer = new Integer(valeur.toString());
				valeur = integer;
				return true;
			} catch (Exception e) {
				valeur = null;
				return false;
			} 
		} else if (parametreImpression().typeParametre().equals(TYPE_REEL)) {
			try {
				Float nombreReel = new Float(valeur.toString());
				valeur = nombreReel;
				return true;
			} catch (Exception e) {
				valeur = null;
				return false;
			} 
		} else if (parametreImpression().typeParametre().equals(TYPE_DATE)) {
			NSTimestamp date = DateCtrl.stringToDate(valeur.toString());
			if (date != null) {
				valeur = date;
				return true;
			} else {
				valeur = null;
				return false;
			} 
		} else {
			return true;
		}
	}
	 // interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	public String toString() {
		return "parametreImpression : " + parametreImpression() + ", valeur : " + valeur;
	}
	// méthodes statiques
	public static String typeParametre(String unType) {
		if (unType == null) {
			return null;
		}
		for (int i= 0; i < ParametreImpression.typesParametres.length;i++) {
			if (ParametreImpression.typesParametres[i].startsWith(unType)) {
				return ParametreImpression.typesParametres[i];
			}
		}
		return null;
	}
}

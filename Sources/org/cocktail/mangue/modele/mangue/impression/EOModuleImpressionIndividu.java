// EOModuleImpressionIndividu.java
// Created on Wed Aug 02 13:35:57 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.impression;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSValidation;

public class EOModuleImpressionIndividu extends EOGenericRecord {
	private static int LG_NOM = 50;
	private static int LG_TEXTE = 80;
    public EOModuleImpressionIndividu() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOModuleImpressionIndividu(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String nom() {
        return (String)storedValueForKey("nom");
    }

    public void setNom(String value) {
        takeStoredValueForKey(value, "nom");
    }

    public String texteMenu() {
        return (String)storedValueForKey("texteMenu");
    }

    public void setTexteMenu(String value) {
        takeStoredValueForKey(value, "texteMenu");
    }

    public Number noOrdreMenu() {
        return (Number)storedValueForKey("noOrdreMenu");
    }

    public void setNoOrdreMenu(Number value) {
        takeStoredValueForKey(value, "noOrdreMenu");
    }
    // méthodes ajoutées
    public String intitulePourMenu() {
    		return "" + noOrdreMenu() + " - " + texteMenu();
    }
    public void initAvecPosition(int position) {
    		setNoOrdreMenu(new Integer(position));
    }
    public void validateForSave() {
		if (noOrdreMenu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un numéro d'ordre dans le menu affiché à l'utilisateur");
		}
		if (nom() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le nom du module d'impression");
		} else if (nom().length() > LG_NOM) {
			throw new NSValidation.ValidationException("La longueur du nom du module d'impression ne peut dépasser " + LG_NOM + " caractères");
		}
		if (texteMenu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le texte de l'item de menu affiché à l'utilisateur");
		} else if (texteMenu().length() > LG_TEXTE) {
			throw new NSValidation.ValidationException("La longueur du texte de l'item de menu ne peut dépasser " + LG_TEXTE + " caractères");
		}
    }
}

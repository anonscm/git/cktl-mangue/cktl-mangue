// EOContratImpression.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.impression;

import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/** Permet de fournir le fichier jasper utilis&eacute; pour l'impression d'un contrat de travail en fonction du type de contrat.
 * Le nom du fichier jasper (sans extension) et le type de contrat sont obligatoires. Il ne peut y avoir qu'un fichier par type de contrat
 * @author christine
 *
 */
public class EOContratImpression extends EOGenericRecord {
	private static int LG_NOM = 50;
	public static int LG_DESCRIPTION = 200;

    public EOContratImpression() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOContratImpression(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String cimpFichier() {
        return (String)storedValueForKey("cimpFichier");
    }

    public void setCimpFichier(String value) {
        takeStoredValueForKey(value, "cimpFichier");
    }

    public String cimpDescription() {
        return (String)storedValueForKey("cimpDescription");
    }

    public void setCimpDescription(String value) {
        takeStoredValueForKey(value, "cimpDescription");
    }

    public org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail typeContratTravail() {
        return (org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail)storedValueForKey("typeContratTravail");
    }

    public void setTypeContratTravail(org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail value) {
        takeStoredValueForKey(value, "typeContratTravail");
    }
    // Méthodes ajoutées
    public void supprimerRelations() {
    	removeObjectFromBothSidesOfRelationshipWithKey(typeContratTravail(), "typeContratTravail");
    }
    public void validateForSave() throws NSValidation.ValidationException {
    	if (cimpFichier() == null || cimpFichier().length() == 0) {
    		throw new NSValidation.ValidationException("Le nom du fichier est obligatoire");
    	} else if (cimpFichier().length() > 50) {
    		throw new NSValidation.ValidationException("Le nom du fichier comporte au plus " + LG_NOM + " caractères");
    	}
    	if (typeContratTravail() == null) {
    		throw new NSValidation.ValidationException("Le type de contrat est obligatoire");
    	}
    	if (cimpDescription() != null && cimpDescription().length() > LG_DESCRIPTION) {
    		throw new NSValidation.ValidationException("La description comporte au plus " + LG_DESCRIPTION + " caractères");
    	}
    	EOContratImpression contratImpression = rechercherContratImpressionPourTypeContrat(editingContext(), typeContratTravail(), false);
    	if (contratImpression != null && contratImpression != this) {
    		throw new NSValidation.ValidationException("Il existe déjà une entrée pour ce type de contrat");

    	}
    }
    // Méthodes statiques
    /** Recherche le contrat impression associ&eacute; &agrave; un type de contrat. Retourne nul si le type de contrat est nul ou si il
     * n'existe pas de contrat impression
     * @param editingContext
     * @param typeContratTravail type de contrat de travail. Ne doit pas &ecirc;tre nul
     * @param shouldRefresh true si il faut raffra&icirc;chir les donn&eacute;es
     */
    public static EOContratImpression rechercherContratImpressionPourTypeContrat(EOEditingContext editingContext, EOTypeContratTravail typeContratTravail, boolean shouldRefresh) {
    	if (typeContratTravail == null) {
    		return null;
    	}
    	EOFetchSpecification fs = new EOFetchSpecification("ContratImpression", EOQualifier.qualifierWithQualifierFormat("typeContratTravail = %@", new NSArray(typeContratTravail)), null);
    	fs.setRefreshesRefetchedObjects(shouldRefresh);
    	try {
    		return (EOContratImpression)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
    	} catch (Exception e) {
    		return null;
    	}
    }
}

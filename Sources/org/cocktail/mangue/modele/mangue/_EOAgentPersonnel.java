// _EOAgentPersonnel.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAgentPersonnel.java instead.
package org.cocktail.mangue.modele.mangue;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOAgentPersonnel extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;


	public static final String ENTITY_NAME = "AgentPersonnel";
	public static final String ENTITY_TABLE_NAME = "MANGUE.AGENT_PERSONNEL";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cptOrdre";

	public static final String AGT_ADMINISTRATION_KEY = "agtAdministration";
	public static final String AGT_BUDGET_KEY = "agtBudget";
	public static final String AGT_CIR_KEY = "agtCir";
	public static final String AGT_CONS_ACCIDENT_KEY = "agtConsAccident";
	public static final String AGT_CONS_AGENTS_KEY = "agtConsAgents";
	public static final String AGT_CONS_CARRIERE_KEY = "agtConsCarriere";
	public static final String AGT_CONS_CONGES_KEY = "agtConsConges";
	public static final String AGT_CONS_CONTRAT_KEY = "agtConsContrat";
	public static final String AGT_CONS_DOSSIER_KEY = "agtConsDossier";
	public static final String AGT_CONS_EMPLOIS_KEY = "agtConsEmplois";
	public static final String AGT_CONS_IDENTITE_KEY = "agtConsIdentite";
	public static final String AGT_CONS_OCCUPATION_KEY = "agtConsOccupation";
	public static final String AGT_CONS_POSTES_KEY = "agtConsPostes";
	public static final String AGT_EDITION_FOS_KEY = "agtEditionFos";
	public static final String AGT_EDITION_REQUETES_KEY = "agtEditionRequetes";
	public static final String AGT_ELECTION_KEY = "agtElection";
	public static final String AGT_INFOS_PERSO_KEY = "agtInfosPerso";
	public static final String AGT_NOMENCLATURES_KEY = "agtNomenclatures";
	public static final String AGT_OUTILS_KEY = "agtOutils";
	public static final String AGT_PRIME_KEY = "agtPrime";
	public static final String AGT_PROMOUVABLE_KEY = "agtPromouvable";
	public static final String AGT_TOUT_KEY = "agtTout";
	public static final String AGT_TYPE_POPULATION_KEY = "agtTypePopulation";
	public static final String AGT_UPD_ACCIDENT_KEY = "agtUpdAccident";
	public static final String AGT_UPD_AGENTS_KEY = "agtUpdAgents";
	public static final String AGT_UPD_CARRIERE_KEY = "agtUpdCarriere";
	public static final String AGT_UPD_CONGES_KEY = "agtUpdConges";
	public static final String AGT_UPD_CONTRAT_KEY = "agtUpdContrat";
	public static final String AGT_UPD_EMPLOIS_KEY = "agtUpdEmplois";
	public static final String AGT_UPD_IDENTITE_KEY = "agtUpdIdentite";
	public static final String AGT_UPD_OCCUPATION_KEY = "agtUpdOccupation";
	public static final String AGT_UPD_POSTES_KEY = "agtUpdPostes";
	public static final String AGT_MOD_VACATION_KEY = "agtModVacation";
	
	public static final String AGT_VACATIONS_KEY = "agtVacations";
	public static final String AGT_HEBERGES_KEY = "agtHeberges";

	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Attributs non visibles
	public static final String NO_INDIVIDU_KEY = "noIndividu";

	//Colonnes dans la base de donnees
	public static final String AGT_ADMINISTRATION_COLKEY = "AGT_ADMINISTRATION";
	public static final String AGT_BUDGET_COLKEY = "AGT_BUDGET";
	public static final String AGT_CIR_COLKEY = "AGT_CIR";
	public static final String AGT_CONS_ACCIDENT_COLKEY = "AGT_CONS_ACCIDENT";
	public static final String AGT_CONS_AGENTS_COLKEY = "AGT_CONS_AGENTS";
	public static final String AGT_CONS_CARRIERE_COLKEY = "AGT_CONS_CARRIERE";
	public static final String AGT_CONS_CONGES_COLKEY = "AGT_CONS_CONGES";
	public static final String AGT_CONS_CONTRAT_COLKEY = "AGT_CONS_CONTRAT";
	public static final String AGT_CONS_DOSSIER_COLKEY = "AGT_CONS_DOSSIER";
	public static final String AGT_CONS_EMPLOIS_COLKEY = "AGT_CONS_EMPLOIS";
	public static final String AGT_CONS_IDENTITE_COLKEY = "AGT_CONS_IDENTITE";
	public static final String AGT_CONS_OCCUPATION_COLKEY = "AGT_CONS_OCCUPATION";
	public static final String AGT_CONS_POSTES_COLKEY = "AGT_CONS_POSTES";
	public static final String AGT_EDITION_FOS_COLKEY = "AGT_EDITION_FOS";
	public static final String AGT_EDITION_REQUETES_COLKEY = "AGT_EDITION_REQUETES";
	public static final String AGT_ELECTION_COLKEY = "AGT_ELECTION";
	public static final String AGT_INFOS_PERSO_COLKEY = "AGT_INFOS_PERSO";
	public static final String AGT_NOMENCLATURES_COLKEY = "AGT_NOMENCLATURES";
	public static final String AGT_OUTILS_COLKEY = "AGT_OUTILS";
	public static final String AGT_PRIME_COLKEY = "AGT_PRIME";
	public static final String AGT_PROMOUVABLE_COLKEY = "AGT_PROMOUVABLE";
	public static final String AGT_TOUT_COLKEY = "AGT_TOUT";
	public static final String AGT_TYPE_POPULATION_COLKEY = "AGT_TYPE_POPULATION";
	public static final String AGT_UPD_ACCIDENT_COLKEY = "AGT_UPD_ACCIDENT";
	public static final String AGT_UPD_AGENTS_COLKEY = "AGT_UPD_AGENTS";
	public static final String AGT_UPD_CARRIERE_COLKEY = "AGT_UPD_CARRIERE";
	public static final String AGT_UPD_CONGES_COLKEY = "AGT_UPD_CONGES";
	public static final String AGT_UPD_CONTRAT_COLKEY = "AGT_UPD_CONTRAT";
	public static final String AGT_UPD_EMPLOIS_COLKEY = "AGT_UPD_EMPLOIS";
	public static final String AGT_UPD_IDENTITE_COLKEY = "AGT_UPD_IDENTITE";
	public static final String AGT_UPD_OCCUPATION_COLKEY = "AGT_UPD_OCCUPATION";
	public static final String AGT_UPD_POSTES_COLKEY = "AGT_UPD_POSTES";
	public static final String AGT_MOD_VACATION_COLKEY = "AGT_MOD_VACATION";
	
	public static final String AGT_HEBERGES_COLKEY = "AGT_HEBERGES";
	public static final String AGT_VACATIONS_COLKEY = "AGT_VACATIONS";

	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";

	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";


	// Relationships
	public static final String COMPTE_KEY = "compte";
	public static final String TO_AGENTS_DROITS_SERVICES_KEY = "toAgentsDroitsServices";
	public static final String TO_INDIVIDU_KEY = "toIndividu";



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
	public String agtAdministration() {
		return (String) storedValueForKey(AGT_ADMINISTRATION_KEY);
	}

	public void setAgtAdministration(String value) {
		takeStoredValueForKey(value, AGT_ADMINISTRATION_KEY);
	}

	public String agtConsAccident() {
		return (String) storedValueForKey(AGT_CONS_ACCIDENT_KEY);
	}

	public void setAgtConsAccident(String value) {
		takeStoredValueForKey(value, AGT_CONS_ACCIDENT_KEY);
	}

	public String agtConsAgents() {
		return (String) storedValueForKey(AGT_CONS_AGENTS_KEY);
	}

	public void setAgtConsAgents(String value) {
		takeStoredValueForKey(value, AGT_CONS_AGENTS_KEY);
	}

	public String agtConsCarriere() {
		return (String) storedValueForKey(AGT_CONS_CARRIERE_KEY);
	}

	public void setAgtConsCarriere(String value) {
		takeStoredValueForKey(value, AGT_CONS_CARRIERE_KEY);
	}

	public String agtConsConges() {
		return (String) storedValueForKey(AGT_CONS_CONGES_KEY);
	}

	public void setAgtConsConges(String value) {
		takeStoredValueForKey(value, AGT_CONS_CONGES_KEY);
	}

	public String agtConsContrat() {
		return (String) storedValueForKey(AGT_CONS_CONTRAT_KEY);
	}

	public void setAgtConsContrat(String value) {
		takeStoredValueForKey(value, AGT_CONS_CONTRAT_KEY);
	}

	public String agtConsDossier() {
		return (String) storedValueForKey(AGT_CONS_DOSSIER_KEY);
	}

	public void setAgtConsDossier(String value) {
		takeStoredValueForKey(value, AGT_CONS_DOSSIER_KEY);
	}

	public String agtConsEmplois() {
		return (String) storedValueForKey(AGT_CONS_EMPLOIS_KEY);
	}

	public void setAgtConsEmplois(String value) {
		takeStoredValueForKey(value, AGT_CONS_EMPLOIS_KEY);
	}

	public String agtConsIdentite() {
		return (String) storedValueForKey(AGT_CONS_IDENTITE_KEY);
	}

	public void setAgtConsIdentite(String value) {
		takeStoredValueForKey(value, AGT_CONS_IDENTITE_KEY);
	}

	public String agtConsOccupation() {
		return (String) storedValueForKey(AGT_CONS_OCCUPATION_KEY);
	}

	public void setAgtConsOccupation(String value) {
		takeStoredValueForKey(value, AGT_CONS_OCCUPATION_KEY);
	}

	public String agtConsPostes() {
		return (String) storedValueForKey(AGT_CONS_POSTES_KEY);
	}

	public void setAgtConsPostes(String value) {
		takeStoredValueForKey(value, AGT_CONS_POSTES_KEY);
	}

	public String agtEditionFos() {
		return (String) storedValueForKey(AGT_EDITION_FOS_KEY);
	}

	public void setAgtEditionFos(String value) {
		takeStoredValueForKey(value, AGT_EDITION_FOS_KEY);
	}

	public String agtEditionRequetes() {
		return (String) storedValueForKey(AGT_EDITION_REQUETES_KEY);
	}

	public void setAgtEditionRequetes(String value) {
		takeStoredValueForKey(value, AGT_EDITION_REQUETES_KEY);
	}

	public String agtElection() {
		return (String) storedValueForKey(AGT_ELECTION_KEY);
	}

	public void setAgtElection(String value) {
		takeStoredValueForKey(value, AGT_ELECTION_KEY);
	}

	public String agtInfosPerso() {
		return (String) storedValueForKey(AGT_INFOS_PERSO_KEY);
	}

	public void setAgtInfosPerso(String value) {
		takeStoredValueForKey(value, AGT_INFOS_PERSO_KEY);
	}

	public String agtNomenclatures() {
		return (String) storedValueForKey(AGT_NOMENCLATURES_KEY);
	}

	public void setAgtNomenclatures(String value) {
		takeStoredValueForKey(value, AGT_NOMENCLATURES_KEY);
	}
	public String agtBudget() {
		return (String) storedValueForKey(AGT_BUDGET_KEY);
	}

	public void setAgtBudget(String value) {
		takeStoredValueForKey(value, AGT_BUDGET_KEY);
	}

	public String agtCir() {
		return (String) storedValueForKey(AGT_CIR_KEY);
	}

	public void setAgtCir(String value) {
		takeStoredValueForKey(value, AGT_CIR_KEY);
	}

	
	public String agtOutils() {
		return (String) storedValueForKey(AGT_OUTILS_KEY);
	}

	public void setAgtOutils(String value) {
		takeStoredValueForKey(value, AGT_OUTILS_KEY);
	}

	public String agtPrime() {
		return (String) storedValueForKey(AGT_PRIME_KEY);
	}

	public void setAgtPrime(String value) {
		takeStoredValueForKey(value, AGT_PRIME_KEY);
	}

	public String agtPromouvable() {
		return (String) storedValueForKey(AGT_PROMOUVABLE_KEY);
	}

	public void setAgtPromouvable(String value) {
		takeStoredValueForKey(value, AGT_PROMOUVABLE_KEY);
	}

	public String agtTout() {
		return (String) storedValueForKey(AGT_TOUT_KEY);
	}

	public void setAgtTout(String value) {
		takeStoredValueForKey(value, AGT_TOUT_KEY);
	}

	public String agtTypePopulation() {
		return (String) storedValueForKey(AGT_TYPE_POPULATION_KEY);
	}

	public void setAgtTypePopulation(String value) {
		takeStoredValueForKey(value, AGT_TYPE_POPULATION_KEY);
	}

	public String agtUpdAccident() {
		return (String) storedValueForKey(AGT_UPD_ACCIDENT_KEY);
	}

	public void setAgtUpdAccident(String value) {
		takeStoredValueForKey(value, AGT_UPD_ACCIDENT_KEY);
	}

	public String agtUpdAgents() {
		return (String) storedValueForKey(AGT_UPD_AGENTS_KEY);
	}

	public void setAgtUpdAgents(String value) {
		takeStoredValueForKey(value, AGT_UPD_AGENTS_KEY);
	}

	public String agtUpdCarriere() {
		return (String) storedValueForKey(AGT_UPD_CARRIERE_KEY);
	}

	public void setAgtUpdCarriere(String value) {
		takeStoredValueForKey(value, AGT_UPD_CARRIERE_KEY);
	}

	public String agtUpdConges() {
		return (String) storedValueForKey(AGT_UPD_CONGES_KEY);
	}

	public void setAgtUpdConges(String value) {
		takeStoredValueForKey(value, AGT_UPD_CONGES_KEY);
	}

	public String agtUpdContrat() {
		return (String) storedValueForKey(AGT_UPD_CONTRAT_KEY);
	}

	public void setAgtUpdContrat(String value) {
		takeStoredValueForKey(value, AGT_UPD_CONTRAT_KEY);
	}

	public String agtUpdEmplois() {
		return (String) storedValueForKey(AGT_UPD_EMPLOIS_KEY);
	}

	public void setAgtUpdEmplois(String value) {
		takeStoredValueForKey(value, AGT_UPD_EMPLOIS_KEY);
	}

	public String agtUpdIdentite() {
		return (String) storedValueForKey(AGT_UPD_IDENTITE_KEY);
	}

	public void setAgtUpdIdentite(String value) {
		takeStoredValueForKey(value, AGT_UPD_IDENTITE_KEY);
	}

	public String agtUpdOccupation() {
		return (String) storedValueForKey(AGT_UPD_OCCUPATION_KEY);
	}

	public void setAgtUpdOccupation(String value) {
		takeStoredValueForKey(value, AGT_UPD_OCCUPATION_KEY);
	}

	public String agtUpdPostes() {
		return (String) storedValueForKey(AGT_UPD_POSTES_KEY);
	}

	public void setAgtUpdPostes(String value) {
		takeStoredValueForKey(value, AGT_UPD_POSTES_KEY);
	}

	public String agtVacations() {
		return (String) storedValueForKey(AGT_VACATIONS_KEY);
	}

	public void setAgtVacations(String value) {
		takeStoredValueForKey(value, AGT_VACATIONS_KEY);
	}

	public String agtHeberges() {
		return (String) storedValueForKey(AGT_HEBERGES_KEY);
	}

	public void setAgtHeberges(String value) {
		takeStoredValueForKey(value, AGT_HEBERGES_KEY);
	}

	
	public Integer cptOrdre() {
		return (Integer) storedValueForKey(CPT_ORDRE_KEY);
	}

	public void setCptOrdre(Integer value) {
		takeStoredValueForKey(value, CPT_ORDRE_KEY);
	}

	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}
	
	public String agtModVacation() {
		return (String) storedValueForKey(AGT_MOD_VACATION_KEY);
	}

	public void setAgtModVacation(String value) {
		takeStoredValueForKey(value, AGT_MOD_VACATION_KEY);
	}


	public org.cocktail.mangue.modele.grhum.referentiel.EOCompte compte() {
		return (org.cocktail.mangue.modele.grhum.referentiel.EOCompte)storedValueForKey(COMPTE_KEY);
	}

	public void setCompteRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOCompte value) {
		if (value == null) {
			org.cocktail.mangue.modele.grhum.referentiel.EOCompte oldValue = compte();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPTE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, COMPTE_KEY);
		}
	}

	public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
		return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
	}

	public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
		if (value == null) {
			org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
		}
	}

	public NSArray toAgentsDroitsServices() {
		return (NSArray)storedValueForKey(TO_AGENTS_DROITS_SERVICES_KEY);
	}

	public NSArray toAgentsDroitsServices(EOQualifier qualifier) {
		return toAgentsDroitsServices(qualifier, null);
	}

	public NSArray toAgentsDroitsServices(EOQualifier qualifier, NSArray sortOrderings) {
		NSArray results;
		results = toAgentsDroitsServices();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
		return results;
	}

	public void addToToAgentsDroitsServicesRelationship(org.cocktail.mangue.modele.mangue.EOAgentsDroitsServices object) {
		addObjectToBothSidesOfRelationshipWithKey(object, TO_AGENTS_DROITS_SERVICES_KEY);
	}

	public void removeFromToAgentsDroitsServicesRelationship(org.cocktail.mangue.modele.mangue.EOAgentsDroitsServices object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_AGENTS_DROITS_SERVICES_KEY);
	}

	public org.cocktail.mangue.modele.mangue.EOAgentsDroitsServices createToAgentsDroitsServicesRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("AgentsDroitsServices");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, TO_AGENTS_DROITS_SERVICES_KEY);
		return (org.cocktail.mangue.modele.mangue.EOAgentsDroitsServices) eo;
	}

	public void deleteToAgentsDroitsServicesRelationship(org.cocktail.mangue.modele.mangue.EOAgentsDroitsServices object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, TO_AGENTS_DROITS_SERVICES_KEY);
		editingContext().deleteObject(object);
	}

	public void deleteAllToAgentsDroitsServicesRelationships() {
		Enumeration objects = toAgentsDroitsServices().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteToAgentsDroitsServicesRelationship((org.cocktail.mangue.modele.mangue.EOAgentsDroitsServices)objects.nextElement());
		}
	}


	/**
	 * Créer une instance de EOAgentPersonnel avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOAgentPersonnel createEOAgentPersonnel(EOEditingContext editingContext, Integer cptOrdre
			, NSTimestamp dCreation
			, NSTimestamp dModification
			, org.cocktail.mangue.modele.grhum.referentiel.EOCompte compte			) {
		EOAgentPersonnel eo = (EOAgentPersonnel) createAndInsertInstance(editingContext, _EOAgentPersonnel.ENTITY_NAME);    
		eo.setCptOrdre(cptOrdre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setCompteRelationship(compte);
		return eo;
	}


	public EOAgentPersonnel localInstanceIn(EOEditingContext editingContext) {
		return (EOAgentPersonnel)localInstanceOfObject(editingContext, this);
	}


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOAgentPersonnel creerInstance(EOEditingContext editingContext) {
		return creerInstance(editingContext, null);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOAgentPersonnel creerInstance(EOEditingContext editingContext, NSArray specificites) {
		EOAgentPersonnel object = (EOAgentPersonnel)createAndInsertInstance(editingContext, _EOAgentPersonnel.ENTITY_NAME, specificites);
		return object;
	}



	public static EOAgentPersonnel localInstanceIn(EOEditingContext editingContext, EOAgentPersonnel eo) {
		EOAgentPersonnel localInstance = (eo == null) ? null : (EOAgentPersonnel)localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOAgentPersonnel#localInstanceIn a la place.
	 */
	public static EOAgentPersonnel localInstanceOf(EOEditingContext editingContext, EOAgentPersonnel eo) {
		return EOAgentPersonnel.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	public static NSArray fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier)null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOAgentPersonnel fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAgentPersonnel fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOAgentPersonnel eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOAgentPersonnel)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}




	public static EOAgentPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOAgentPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOAgentPersonnel eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOAgentPersonnel)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOAgentPersonnel fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOAgentPersonnel eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOAgentPersonnel ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	


	public static EOAgentPersonnel fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}

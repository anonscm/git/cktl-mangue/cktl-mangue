/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.budget;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOOrgan extends _EOOrgan {

	public static Integer NIVEAU_CR = new Integer(3);
	public static Integer NIVEAU_SOUS_CR = new Integer(4);
	
    public EOOrgan() {
        super();
    }

    /**
     * @return le libellé de l'établissement/UB/CR/Sous-CR
     */
	public String libelleLbud() {
		String libelle = "";
		if (orgEtab() != null)	{
			libelle = libelle + orgEtab() + " / ";
		} 
		
		return libelle + libelleUBEtCREtSsCR();
	}
	
	/**
     * @return le libellé de l'UB/CR/Sous-CR
     */
	public String libelleUBEtCREtSsCR() {
		String libelle = "";
		if (orgUb() != null)	{
			libelle = libelle + orgUb () + " / ";
		}
		if (orgCr() != null)	{
			libelle = libelle + orgCr () + " / ";
		}
		if (orgSouscr() != null)	{
			libelle = libelle + orgSouscr();
		}
		return libelle;
	}
	
	/** Retourne l'organ sous la forme d'une string */
	public String libelleLbudAvecId() {
		return libelleLbud() + "  - ( ID : " + orgId() + " )";
	}
	
	/** Retourne l'organ sous la forme d'une string */
	public String libelleLbudAvecLibelleId() {
		return libelleLbud() + " - "  + orgLib() + "  ( ID : " + orgId() + " )";
	}
	
	/** Retourne true le pourcentage de 100% est obligatoire pour les codes analytiques (orgCanalObligatoire = 4) */
	public boolean codeAnalytiqueObligatoire() {
		return orgCanalObligatoire() != null && orgCanalObligatoire().intValue() == 4;
	}
	/** Retourne true le pourcentage de 100% est obligatoire pour les conventions (tyorId = 2, correspond &agrave; une entr&eacute;e de Type_Organ) */
	public boolean conventionObligatoire() {
		return tyorId() != null && tyorId().intValue() == 2;
	}

	/** 
	 *	Liste des organ du parent restreintes aux organs valides sur la période
	 */
	public static NSArray rechercherOrgansPourPereEtPeriode(EOEditingContext ec, EOOrgan pere, NSTimestamp debutPeriode, NSTimestamp finPeriode)	{
		NSMutableArray qualifiers = new NSMutableArray(SuperFinder.qualifierPourPeriode(ORG_DATE_OUVERTURE_KEY, debutPeriode, ORG_DATE_CLOTURE_KEY, finPeriode));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ORGAN_PERE_KEY + " = %@", new NSArray(pere)));
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);

		return ec.objectsWithFetchSpecification(fs);
	}
	/**	Retourne les lignes budgetaires valides pour un niveau et une période
	 * 
	 * @param ec editing context
	 * @param niveau
	 * @param debutPeriode
	 * @param finPeriode
	 * @return les lignes budgetaires pour ce niveau
	 */
	public static NSArray rechercherLignesBudgetairesPourPeriodeEtNiveau(EOEditingContext ec, NSTimestamp debutPeriode, NSTimestamp finPeriode, Number niveau)  {
		NSMutableArray qualifiers = new NSMutableArray(SuperFinder.qualifierPourPeriode(ORG_DATE_OUVERTURE_KEY , debutPeriode, ORG_DATE_CLOTURE_KEY, finPeriode));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ORG_NIV_KEY + " = %@", new NSArray(niveau)));
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), sortOrderings());
		return ec.objectsWithFetchSpecification(fs);
	}
	// Méthodes privées statiques
	private static NSArray sortOrderings() {
		NSMutableArray mySort = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("orgEtab",EOSortOrdering.CompareAscending));
		mySort.addObject(EOSortOrdering.sortOrderingWithKey(ORG_UB_KEY,EOSortOrdering.CompareAscending));
		mySort.addObject(EOSortOrdering.sortOrderingWithKey(ORG_CR_KEY,EOSortOrdering.CompareAscending));
		mySort.addObject(EOSortOrdering.sortOrderingWithKey(ORG_SOUSCR_KEY,EOSortOrdering.CompareAscending));
		return mySort;
	}

	/**
	 * 
	 * @param ec
	 * @param niveau
	 * @return
	 */
		public static NSArray findOrgansForExercice(EOEditingContext ec, Integer exercice)  {

			try {

				NSMutableArray mySort =new NSMutableArray();
				mySort.addObject(new EOSortOrdering(ORG_ETAB_KEY, EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering(ORG_UB_KEY, EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering(ORG_CR_KEY, EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering(ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending));

				NSMutableArray mesQualifiers = new NSMutableArray();

				mesQualifiers.addObject(getQualifierForPeriodeAndExercice(EOOrgan.ORG_DATE_OUVERTURE_KEY, 
						EOOrgan.ORG_DATE_CLOTURE_KEY, exercice));
				
				EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
				fs.setRefreshesRefetchedObjects(true);

				return ec.objectsWithFetchSpecification(fs);
			}
			catch (Exception e)	{
				e.printStackTrace();
				return new NSArray();
			}
	    }
		
		
		/**
		 * 
		 * @param pathKeyOuverture
		 * @param pathKeyCloture
		 * @param exercice
		 * @return
		 */
	    public static EOQualifier getQualifierForPeriodeAndExercice(String pathKeyOuverture, String pathKeyCloture, Integer exercice){
	        NSMutableArray ou = new NSMutableArray();
	        NSMutableArray et1 = new NSMutableArray();
	        et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" = nil", null));
	        et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(DateCtrl.finAnnee(exercice))));
	        NSMutableArray et2 = new NSMutableArray();
	        et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" >= %@", new NSArray(DateCtrl.debutAnnee(exercice))));
	        et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(DateCtrl.finAnnee(exercice))));
	        NSMutableArray et3 = new NSMutableArray();
	        et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" >= %@", new NSArray(DateCtrl.finAnnee(exercice))));
	        et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(DateCtrl.debutAnnee(exercice))));
	        ou.addObject(new EOAndQualifier(et1));
	        ou.addObject(new EOAndQualifier(et2));
	        ou.addObject(new EOAndQualifier(et3));
	        return new EOOrQualifier(ou);
	    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

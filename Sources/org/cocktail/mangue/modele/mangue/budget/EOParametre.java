/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.budget;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOParametre extends _EOParametre {

	private static final String PARAM_LOLF_NIVEAU_DEPENSE = "LOLF_NIVEAU_DEPENSE";

    public EOParametre() {
        super();
    }

    /**
     * 
     * @param editingContext
     * @param cle
     * @param annee
     * @return
     */
    public static String valeurParametrePourCleEtAnnee(EOEditingContext editingContext,String cle,Number annee) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(PAR_KEY_KEY + " = %@",new NSArray(cle)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EXE_ORDRE_KEY + " = %@",new NSArray(annee)));
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers),null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		try {
			return ((EOParametre)results.objectAtIndex(0)).parValue();
		} catch(Exception e) {
			return null;
		}
	}
    
    /** Retourne la valeur du param&egrave;tre PARAM_LOLF_NIVEAU_DEPENSE et null si non trouv&eacute; ou erreur
     * @param annee doit etre fournie */
    public static Integer rechercheNiveauLolfDepense(EOEditingContext editingContext, Number annee) {
    	if (annee == null) {
    		return null;
    	} else {
    		String valeur = valeurParametrePourCleEtAnnee(editingContext, PARAM_LOLF_NIVEAU_DEPENSE, annee);
    		if (valeur == null) {
    			return null;
    		} else {
    			try {
					return new Integer(valeur);
				} catch (NumberFormatException e) {
					return null;
				}
    		}
    	}
    }

    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

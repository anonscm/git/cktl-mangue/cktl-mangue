/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.budget;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOLolfNomenclatureDepense extends _EOLolfNomenclatureDepense {

	private static String TYET_LIBELLE_VALIDE="VALIDE";

	public EOLolfNomenclatureDepense() {
		super();
	}

	public String codeEtLibelle() {
		return lolfCode() + " - " + lolfLibelle();
	}


	/**
	 * 
	 * @param ec
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOLolfNomenclatureDepense> findForDate(EOEditingContext ec, NSTimestamp dateReference) {

		try {
			Integer niveau = EOParametre.rechercheNiveauLolfDepense(ec, new Integer(DateCtrl.getYear(dateReference)));

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LOLF_NIVEAU_KEY + " = %@", new NSArray(niveau)));			

			qualifiers.addObject(SuperFinder.qualifierPourPeriode(LOLF_OUVERTURE_KEY, dateReference, LOLF_FERMETURE_KEY, dateReference));			

			return fetchAll(ec, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOLolfNomenclatureDepense>();
		}

	}


	/**
	 * 
	 * Retourne un qualifier pour trouver les lolf nomenclature depenses en fonction d'une periode donne 
	 * ET du niveau defini dans Parametre (cl&eacute; : "LOLF_NIVEAU_DEPENSE")

	 * @param editingContext
	 * @param debutPeriode d&eacute;but de la periode sur laquelle on recherche les codes analytiques
	 * @param finPeriode
	 * @return qualifier (null si le niveau n'est pas defini)
	 */
	public static EOQualifier qualifierPourLolfNomenclature(EOEditingContext editingContext, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		try {
			Integer niveau = EOParametre.rechercheNiveauLolfDepense(editingContext, new Integer(DateCtrl.getYear(debutPeriode)));
			if (niveau == null) {
				return null;
			}
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(LOLF_OUVERTURE_KEY, debutPeriode, LOLF_FERMETURE_KEY, finPeriode));			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_ETAT_KEY + ".tyetLibelle = %@", new NSArray(TYET_LIBELLE_VALIDE)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LOLF_NIVEAU_KEY + " = %@", new NSArray(niveau)));			
			return new EOAndQualifier(qualifiers);
		}
		catch (Exception e) {
			return null;
		}
	}
	public static EOQualifier qualifierPourExclusions(NSArray objetsAExclure) {
		NSMutableArray qualifiers = new NSMutableArray();
		for (java.util.Enumeration<EOLolfNomenclatureDepense> e = objetsAExclure.objectEnumerator();e.hasMoreElements();) {
			EOLolfNomenclatureDepense lolfDepense = e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LOLF_ID_KEY + " <> %@", new NSArray(lolfDepense.lolfId())));
		}
		return new EOAndQualifier(qualifiers);
	}


	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.budget;

import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOCodeAnalytiqueOrgan extends _EOCodeAnalytiqueOrgan implements RecordAvecLibelleEtCode{

	private static String TYET_LIBELLE_PUBLIC="OUI";
	private static String TYET_LIBELLE_UTILISABLE="OUI";
	private static String TYET_LIBELLE_VALIDE="VALIDE";

    public EOCodeAnalytiqueOrgan() {
        super();
    }

	// Méthodes ajoutées
	// Interface RecordAvecLibelleEtCode
	public String code() {
		if (codeAnalytique() == null) {
			return null;
		} else {
			return codeAnalytique().canCode();
		}
	}
	public String libelle() {
		if (codeAnalytique() == null) {
			return null;
		} else {
			return codeAnalytique().canLibelle();
		}
	}

	/**
	 * 
	 * Retourne un qualifier pour trouver les codes analytiques en fonction d'une exercice donne ET d'une ligne budgetaire
	 * 
	 * Codes analytiques Valides ET Utilisables ET (Publics ou associes a la ligne budgetaire) )
	 * 
	 * @param editingContext
	 * @param organ
	 * @param debutPeriode d&eacute;but de la p&eacute;riode sur laquelle on recherche les codes analytiques
	 * @param finPeriode
	 * @return qualifier
	 */
	public static final EOQualifier qualifierPourCodeAnalytiques (EOOrgan organ, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray mesQualifiers = new NSMutableArray();
		mesQualifiers.addObject(SuperFinder.qualifierPourPeriode("codeAnalytique.canOuverture", debutPeriode, "codeAnalytique.canFermeture", finPeriode));			
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("codeAnalytique.typeEtat.tyetLibelle = %@", new NSArray(TYET_LIBELLE_VALIDE)));			
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("codeAnalytique.typeEtatUtilisable.tyetLibelle = %@", new NSArray(TYET_LIBELLE_UTILISABLE)));			

		NSMutableArray qualifsPublic = new NSMutableArray();
		qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat("codeAnalytique.typeEtatPublic.tyetLibelle = %@", new NSArray(TYET_LIBELLE_PUBLIC)));			
		if (organ != null && organ.orgNiv().intValue() > 1) {
			NSMutableArray args = new NSMutableArray();
			args.addObject(organ.orgUb());
			qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat("organ.orgUb = %@ AND organ.orgNiv = 2", args));			
			if (organ.orgCr() != null) {
				args.addObject(organ.orgCr());
				qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat("organ.orgUb = %@ AND organ.orgCr = %@ AND organ.orgNiv = 3", args));			
			}
			if (organ.orgSouscr() != null) {
				args.addObject(organ.orgSouscr());
				qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat("organ.orgUb = %@ AND organ.orgCr = %@ AND organ.orgSouscr = %@ AND organ.orgNiv = 4", args));			
			}

		}
		mesQualifiers.addObject(new EOOrQualifier(qualifsPublic));
		return new EOAndQualifier(mesQualifiers);
	}
	public static EOQualifier qualifierPourExclusions(NSArray objetsAExclure) {
		NSMutableArray qualifiers = new NSMutableArray();
		for (java.util.Enumeration<EOCodeAnalytique> e = objetsAExclure.objectEnumerator();e.hasMoreElements();) {
			EOCodeAnalytique codeAnalytique = (EOCodeAnalytique)e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_ANALYTIQUE_KEY + " <> %@", new NSArray(codeAnalytique)));
		}
		return new EOAndQualifier(qualifiers);
	}

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

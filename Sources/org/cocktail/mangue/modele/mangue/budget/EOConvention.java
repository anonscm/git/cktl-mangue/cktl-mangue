/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.budget;

import org.cocktail.common.utilities.RecordAvecLibelle;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOConvention extends _EOConvention implements RecordAvecLibelle {

    public EOConvention() {
        super();
    }
    
    public String codeEtLibelle() {
    	return exeOrdre() + " - " + conIndex() + " - " + conReferenceExterne();
    }


	/**
	 *   	
	 *   Recuperation d'un objet de type EOConvention en fonction d'un Exercice / Organ / Type de credit
	 *   
	 * @param ec
	 * @param exercice
	 * @param organ
	 * @param typeCredit
	 * @return
	 */
	public static EOConvention findConventionRA(EOEditingContext ec, Integer exercice, EOOrgan organ, EOTypeCredit typeCredit) {

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.EXE_ORDRE_KEY + " = %@", new NSArray(exercice)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.TYPE_CREDIT_KEY + " = %@", new NSArray(typeCredit)));

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.ORGAN_KEY + " = %@", new NSArray(organ)));

			EOFetchSpecification fs = new EOFetchSpecification(EOConvention.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);

			return (EOConvention)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);

		}
		catch (Exception e) {

			return null;
		}

	}


	/**
  	 *   Recuperation d'une liste de conventions en fonction d'un Exercice / Organ / Type de credit
	 * @param ec
	 * @param exercice
	 * @param organ
	 * @param typeCredit
	 * @return
	 */
	public static NSArray findConventions(EOEditingContext ec, Integer exercice, EOOrgan organ, EOTypeCredit typeCredit) {
		try {

			NSMutableArray quals = new NSMutableArray();	
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.EXE_ORDRE_KEY + " = %@", new NSArray(exercice)));

			if (organ != null && typeCredit != null)	{
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.ORGAN_KEY + " = %@", new NSArray(new Object[] { organ })));
				
				NSMutableArray orQualifiers = new NSMutableArray<EOQualifier>();
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.TYPE_CREDIT_KEY + " = %@", new NSArray(typeCredit)));
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_CODE_KEY + " = %@", new NSArray(EOTypeCredit.DEFAULT_TYPE_CREDIT)));
				
				quals.addObject(new EOOrQualifier(orQualifiers));
			}
			
			EOFetchSpecification fs = new EOFetchSpecification(EOConvention.ENTITY_NAME, new EOAndQualifier(quals), null);
			
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}


    // Interface RecordAvecLibelle
	public String libelle() {
		return conObjet();
	}
	// Méthodes statiques
    public static EOQualifier qualifierPourOrganEtTypeCredit(Number annee,EOOrgan organ, EOTypeCredit typeCredit) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(EXE_ORDRE_KEY + " = %@", new NSArray(annee)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ORGAN_KEY + " = %@", new NSArray(organ)));			
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_CREDIT_KEY + " = %@", new NSArray(typeCredit)));			
		return new EOAndQualifier(qualifiers);
    }
    public static EOQualifier qualifierPourExclusions(NSArray objetsAExclure) {
		NSMutableArray qualifiers = new NSMutableArray();
		for (java.util.Enumeration<EOConvention> e = objetsAExclure.objectEnumerator();e.hasMoreElements();) {
			EOConvention convention = e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CON_ORDRE_KEY + " <> %@", new NSArray(convention.conOrdre())));
		}
		return new EOAndQualifier(qualifiers);
	}

    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

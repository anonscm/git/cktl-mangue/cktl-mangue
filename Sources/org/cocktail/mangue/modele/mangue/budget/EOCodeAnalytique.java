/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.budget;

import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOCodeAnalytique extends _EOCodeAnalytique {

	public static final EOSortOrdering SORT_CAN_CODE_ASC = new EOSortOrdering(EOCodeAnalytique.CAN_CODE_KEY, EOSortOrdering.CompareAscending);
    public static final EOQualifier QUAL_CODE_ANALYTIQUE_VALIDE = EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_KEY + "." +  EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(new Object[]{ EOTypeEtat.ETAT_VALIDE }));
    
    public static final String LONG_STRING_KEY  = "longString";
    public static final String CAN_ID_KEY = "canId";

	public static String TYET_LIBELLE_PUBLIC="OUI";
	public static String TYET_LIBELLE_PRIVE="NON";
	public static String TYET_LIBELLE_UTILISABLE="OUI";
	public static String TYET_LIBELLE_PAS_UTILISABLE="NON";

	public static String TYET_LIBELLE_VALIDE="VALIDE";
	public static String TYET_LIBELLE_ANNULE="ANNULE";

    public EOCodeAnalytique() {
        super();
    }

    public String codeEtLibelle() {
    	return canCode() + " - " + canLibelle();
    }
    
	/**
	 * 
	 * Retourne tous les codes analytiques d'un exercice donne
	 * 
	 * @param ec
	 * @param organ
	 * @param exercice
	 * @return
	 */
	public static NSArray getCodesAnalytiquesForPeriode (EOEditingContext ec, NSTimestamp debut, NSTimestamp fin) {

		NSMutableArray mesQualifiers = new NSMutableArray();

		mesQualifiers.addObject(SuperFinder.qualifierPourPeriode(CAN_OUVERTURE_KEY, debut, CAN_FERMETURE_KEY, fin));			

		//		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.EXERCICE_KEY + " = %@", new NSArray(exercice)));			

		mesQualifiers.addObject(QUAL_CODE_ANALYTIQUE_VALIDE);			

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_UTILISABLE_KEY + " = %@", 
				new NSArray(EOTypeEtat.findTypeEtat(ec, EOCodeAnalytique.TYET_LIBELLE_UTILISABLE))));			

		EOFetchSpecification fs = new EOFetchSpecification(EOCodeAnalytique.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());
		fs.setUsesDistinct(true);

		return ec.objectsWithFetchSpecification(fs);

	}


	/**
	 * 
	 * Retourne les codes analytiques en fonction d'une exercice donne ET d'une ligne budgetaire
	 * 
	 * Codes analytiques Valides ET Utilisables ET (Publics ou associes a la ligne budgetaire) )
	 * 
	 * @param ec
	 * @param organ
	 * @param exercice
	 * @return NSArray
	 */
	public static final NSArray getCodesAnalytiques (EOEditingContext ec, EOOrgan organ, NSTimestamp debut, NSTimestamp fin) {

		try {

			NSMutableArray codesAnalytiques = new NSMutableArray();
			
			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(SuperFinder.qualifierPourPeriode(CAN_OUVERTURE_KEY, debut, CAN_FERMETURE_KEY, fin));			

			mesQualifiers.addObject(QUAL_CODE_ANALYTIQUE_VALIDE);			

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_UTILISABLE_KEY + " = %@", 
					new NSArray(EOTypeEtat.findTypeEtat(ec, EOCodeAnalytique.TYET_LIBELLE_UTILISABLE))));			

			NSMutableArray qualifsPrive = new NSMutableArray();
			if (organ != null && organ.orgNiv().intValue() > 1) {

				NSMutableArray args = new NSMutableArray();

				args.addObject(organ.orgUb());

				//qualifsPublic.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + " = %@", new NSArray(organ)));			
				qualifsPrive.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY + " = %@ and " +
						EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_NIV_KEY + " = 2", args));			

				args.addObject(organ.orgCr());
				qualifsPrive.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY + " = %@ and " +
						EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY + " = %@ and " +
						EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_NIV_KEY + " = 3", args));			


				if (organ.orgSouscr() != null) {
					args.addObject(organ.orgSouscr());
					qualifsPrive.addObject(EOQualifier.qualifierWithQualifierFormat(
							EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY + " = %@ and " +
							EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY + " = %@ and " +
							EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_SOUSCR_KEY + " = %@ and " +
							EOCodeAnalytique.CODE_ANALYTIQUE_ORGAN_KEY + "."+EOCodeAnalytiqueOrgan.ORGAN_KEY+"."+EOOrgan.ORG_NIV_KEY + " = 4", args));			
				}

			}

			mesQualifiers.addObject(new EOOrQualifier(qualifsPrive));

			// Codes analytiques Prives
			codesAnalytiques.addObjectsFromArray(EOCodeAnalytique.fetchAll(ec, new EOAndQualifier(mesQualifiers), sort(), true));

			// Codes analytiques Publics
			codesAnalytiques.addObjectsFromArray(getCodesAnalytiquesPublics(ec, debut, fin));

			return codesAnalytiques;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}

	}


	public static final NSArray getCodesAnalytiquesPublics (EOEditingContext ec, NSTimestamp debut, NSTimestamp fin) {

		try {

			NSMutableArray mesQualifiers = new NSMutableArray();

			mesQualifiers.addObject(SuperFinder.qualifierPourPeriode(CAN_OUVERTURE_KEY, debut, CAN_FERMETURE_KEY, fin));			

			mesQualifiers.addObject(QUAL_CODE_ANALYTIQUE_VALIDE);			

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_UTILISABLE_KEY + " = %@", 
					new NSArray(EOTypeEtat.findTypeEtat(ec, EOCodeAnalytique.TYET_LIBELLE_UTILISABLE))));			

			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_PUBLIC_KEY + " = %@", 
					new NSArray(EOTypeEtat.findTypeEtat(ec, EOCodeAnalytique.TYET_LIBELLE_PUBLIC))));			

			return EOCodeAnalytique.fetchAll(ec, new EOAndQualifier(mesQualifiers), sort(), true);
			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}

	}

	
	/**
	 * Methode de tri
	 * @return
	 *        un NSArray contenant des EOSortOrdering
	 */
	private static NSArray sort() {
		return new NSArray(EOSortOrdering.sortOrderingWithKey(EOCodeAnalytique.CAN_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
	}

    
    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

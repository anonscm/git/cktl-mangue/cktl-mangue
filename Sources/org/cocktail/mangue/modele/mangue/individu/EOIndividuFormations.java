// EOIndividuFormations.java
// Created on Sun Feb 23 12:24:09  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeUniteTemps;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
/** Regles de validation<BR>
 * 	Verifie la longueur des chaines de caracteres<BR>
 * 	Verifie que la date de debut et le libelle sont fournis<BR>
 * 	Verifie que la date de debut n'est pas posterieure a la date de fin<BR>
 * 	Verifie que la duree ne depasse pas la duree entre les dates de debut et de fin<BR>
 */
// 12/05/2010 - Correction d'un bug si la date début ou fin est nulle
// 23/09/2010 - ajout de l'unité de durée de formations (DT 2711) et vérification sur la durée
// 05/11/2010 - Correction d'un bug sur la validation de la durée
public class EOIndividuFormations extends _EOIndividuFormations {

	public static EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(TO_INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);

	public static NSArray SORT_DEBUT_DESC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));
	public static NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(SORT_DEBUT_DESC);
	public static NSArray SORT_DEBUT_ASC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));
	public static NSArray SORT_ARRAY_DEBUT_ASC = new NSArray(SORT_DEBUT_ASC);

	public EOIndividuFormations() {
		super();
	}

	public static EOIndividuFormations creer(EOEditingContext ec, EOIndividu individu) {
		EOIndividuFormations newObject = (EOIndividuFormations) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setToIndividuRelationship(individu);				
		return newObject;		
	}

	/**
	 * 
	 * @param ec
	 * @param qualifier
	 * @return
	 */
	public static NSArray<EOIndividuFormations> findForQualifier(EOEditingContext ec, EOQualifier qualifier) {
		try {
			return fetchAll(ec, qualifier, SORT_ARRAY_DEBUT_DESC );
		}
		catch (Exception e) {
			return new NSArray<EOIndividuFormations>();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOIndividuFormations> findForIndividu(EOEditingContext ec,EOIndividu individu) {
		try {
			NSMutableArray args = new NSMutableArray(individu);
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@",args);
			return fetchAll(ec,myQualifier, SORT_ARRAY_DEBUT_DESC );
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	public String dureeFormation() {

		if (duree() != null && typeDuree() != null)
			return duree().toString() + " " + typeDuree().libelleLong();

		return "";
	}

	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (llFormation() == null) {
			throw new NSValidation.ValidationException("Le libellé est obligatoire");
		}
		if (llFormation().length() > 100) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé ne doit pas comporter plus de 100 caractères !");
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début est obligatoire");
		}
		if (dateFin() != null && DateCtrl.isBefore(dateFin(), dateDebut())) {
			throw new NSValidation.ValidationException("La date de fin ne peut être antérieure à la date de début");
		}
		if (duree() != null) {
			int duree = 0;
			try {
				duree = new Integer(duree()).intValue();
			} catch (Exception e) {
				throw new NSValidation.ValidationException("La durée est un nombre");
			}
			// Vérifier si la durée est  est cohérente avec les dates
			if (typeDuree() != null && dateFin() != null) {
				NSTimestamp dateFin = dateDebut();
				String typeDuree =  typeDuree().code();
				if (typeDuree.equals(EOTypeUniteTemps.TYPE_CODE_HEURES) == false) {	// pour les heures on ne fait pas de vérification
					if (typeDuree.equals(EOTypeUniteTemps.TYPE_CODE_JOURS)) {
						dateFin = DateCtrl.dateAvecAjoutJours(dateFin, duree);
					} else if (typeDuree.equals(EOTypeUniteTemps.TYPE_CODE_SEMAINES)) {	
						duree = duree * 7;
						dateFin = DateCtrl.dateAvecAjoutJours(dateFin, duree);
					} else if (typeDuree.equals(EOTypeUniteTemps.TYPE_CODE_MOIS)) {	
						dateFin = DateCtrl.dateAvecAjoutMois(dateFin, duree);
					} else if (typeDuree.equals(EOTypeUniteTemps.TYPE_CODE_ANNEES)) {
						dateFin = DateCtrl.dateAvecAjoutAnnees(dateFin, duree);
					}
					// 05/11/2010 - Pour que la durée soit effectivement la même qu'entre [dateDébut, dateFin];
					// il faut prendre le jour précédent de la date trouvée
					dateFin = DateCtrl.jourPrecedent(dateFin);

					if (DateCtrl.isAfter(dateFin, dateFin())) {
						throw new NSValidation.ValidationException("La durée dépasse la durée effective entre les dates de début et de fin");
					}
				}
			}
		}
	}
}

// EOIndividuDiplomes.java
// Created on Mon Mar 17 16:07:10  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.modele.nomenclatures.EOTitulaireDiplome;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation<BR>
 * 	Verifie la longueur des chaines de caracteres<BR>
	Verifie que la date du diplome n'est pas posterieure a la date de deces de l'individu<BR>
	Verifie que la date du diplome n'est pas posterieure a la date du jour<BR>
	Verifie que la date du diplome n'est pas anterieure a la date de naissance de l'individu<BR>
 */

public class EOIndividuDiplomes extends _EOIndividuDiplomes {

	public static NSArray SORT_ARRAY_D_OBTENTION_ASC = new NSArray(new EOSortOrdering(D_DIPLOME_KEY, EOSortOrdering.CompareAscending));
	public static NSArray SORT_ARRAY_D_OBTENTION_DESC = new NSArray(new EOSortOrdering(D_DIPLOME_KEY, EOSortOrdering.CompareDescending));

	public EOIndividuDiplomes() {
		super();
	}


	public static EOIndividuDiplomes creer(EOEditingContext ec, EOIndividu individu) {
		EOIndividuDiplomes newObject = (EOIndividuDiplomes) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.setIndividuRelationship(individu);		
		newObject.setTemValide(CocktailConstantes.VRAI);
		newObject.setToTitulaireRelationship(EOTitulaireDiplome.valeurParDefaut(ec));

		return newObject;		
	}
	public static NSArray findForIndividu(EOEditingContext ec,EOIndividu individu) {
		try {
			NSMutableArray args = new NSMutableArray(individu);
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",args);
			return fetchAll(ec,myQualifier, SORT_ARRAY_D_OBTENTION_DESC );
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	public String anneeDiplome() {
		if (dDiplome() == null) {
			return "";
		}
		int annee = DateCtrl.getYear(dDiplome());
		return new Integer(annee).toString();
	}

	public void validateForSave() { 

		if (diplome() == null)
			throw new NSValidation.ValidationException("Le diplôme doit être renseigné !");			

		if (specialite() != null && specialite().length() > 40) {
			throw new NSValidation.ValidationException("La spécialité ne doit pas comporter plus de 40 caractères !");
		}
		if (lieuDiplome() != null && lieuDiplome().length() > 40) {
			throw new NSValidation.ValidationException("Le lieu de diplôme ne doit pas comporter plus de 40 caractères !");
		}
		if (dDiplome() != null) {
			if (DateCtrl.isBefore(dDiplome(),individu().dNaissance())) {
				throw new NSValidation.ValidationException("La date de diplôme ne peut être antérieure à la date de naissance !!");
			}
			if (individu().dDeces() != null && DateCtrl.isAfter(dDiplome(),individu().dDeces())) {
				throw new NSValidation.ValidationException("La date de diplôme ne peut être postérieure à la date de décès !");
			}
			if (DateCtrl.getYear(dDiplome()) > DateCtrl.getYear(new NSTimestamp())) {
				throw new NSValidation.ValidationException("L'année d'obtention du diplôme ne peut être postérieure à " + DateCtrl.getYear(new NSTimestamp()) + " !");
			}
		}
		
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		
	}

}

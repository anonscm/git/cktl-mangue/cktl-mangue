/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOContratAvenant.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOContratAvenant extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ContratAvenant";
	public static final String ENTITY_TABLE_NAME = "MANGUE.CONTRAT_AVENANT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "avenantOrdre";

	public static final String C_DEA_KEY = "cDea";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String CTRA_DUREE_KEY = "ctraDuree";
	public static final String CTRA_DUREE_VALIDEE_ANNEES_KEY = "ctraDureeValideeAnnees";
	public static final String CTRA_DUREE_VALIDEE_JOURS_KEY = "ctraDureeValideeJours";
	public static final String CTRA_DUREE_VALIDEE_MOIS_KEY = "ctraDureeValideeMois";
	public static final String CTRA_PC_ACQUITEES_KEY = "ctraPcAcquitees";
	public static final String CTRA_QUOTITE_COTISATION_KEY = "ctraQuotiteCotisation";
	public static final String CTRA_TYPE_TEMPS_KEY = "ctraTypeTemps";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_REF_CTR_AVENANT_KEY = "dRefCtrAvenant";
	public static final String D_VAL_CONTRAT_AV_KEY = "dValContratAv";
	public static final String FONCTION_CTR_AVENANT_KEY = "fonctionCtrAvenant";
	public static final String INDICE_CONTRAT_KEY = "indiceContrat";
	public static final String MONTANT_KEY = "montant";
	public static final String NBR_UNITE_KEY = "nbrUnite";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_AVENANT_KEY = "noAvenant";
	public static final String POURCENT_SMIC_KEY = "pourcentSmic";
	public static final String QUOTITE_KEY = "quotite";
	public static final String REFERENCE_CONTRAT_KEY = "referenceContrat";
	public static final String TAUX_HORAIRE_KEY = "tauxHoraire";
	public static final String TEM_ANNULATION_KEY = "temAnnulation";
	public static final String TEM_ARRETE_SIGNE_KEY = "temArreteSigne";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";
	public static final String TEM_PAIEMENT_PONCTUEL_KEY = "temPaiementPonctuel";
	public static final String TYPE_MONTANT_KEY = "typeMontant";

// Attributs non visibles
	public static final String AVENANT_ORDRE_KEY = "avenantOrdre";
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_DISC_SECOND_DEGRE_KEY = "cDiscSecondDegre";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_SPECIALITE_ATOS_KEY = "cSpecialiteAtos";
	public static final String C_SPECIALITE_ITARF_KEY = "cSpecialiteItarf";
	public static final String CODEEMPLOI_KEY = "codeemploi";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String NO_SEQ_CONTRAT_KEY = "noSeqContrat";
	public static final String TUT_ORDRE_KEY = "tutOrdre";
	public static final String ID_COND_RECRUT_KEY = "idCondRecrut";
	public static final String BAP_ID_KEY = "bapId";

//Colonnes dans la base de donnees
	public static final String C_DEA_COLKEY = "C_DEA";
	public static final String C_ECHELON_COLKEY = "C_ECHELON";
	public static final String CTRA_DUREE_COLKEY = "CTRA_DUREE";
	public static final String CTRA_DUREE_VALIDEE_ANNEES_COLKEY = "CTRA_DUREE_VALIDEE_ANNEES";
	public static final String CTRA_DUREE_VALIDEE_JOURS_COLKEY = "CTRA_DUREE_VALIDEE_JOURS";
	public static final String CTRA_DUREE_VALIDEE_MOIS_COLKEY = "CTRA_DUREE_VALIDEE_MOIS";
	public static final String CTRA_PC_ACQUITEES_COLKEY = "CTRA_PC_ACQUITEES";
	public static final String CTRA_QUOTITE_COTISATION_COLKEY = "CTRA_QUOTITE_COTISATION";
	public static final String CTRA_TYPE_TEMPS_COLKEY = "CTRA_TYPE_TEMPS";
	public static final String DATE_ARRETE_COLKEY = "DATE_ARRETE";
	public static final String DATE_DEBUT_COLKEY = "D_DEB_CONTRAT_AV";
	public static final String DATE_FIN_COLKEY = "D_FIN_CONTRAT_AV";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_REF_CTR_AVENANT_COLKEY = "D_REF_CTR_AVENANT";
	public static final String D_VAL_CONTRAT_AV_COLKEY = "D_VAL_CONTRAT_AV";
	public static final String FONCTION_CTR_AVENANT_COLKEY = "FONCTION_CTR_AV";
	public static final String INDICE_CONTRAT_COLKEY = "INDICE_CONTRAT";
	public static final String MONTANT_COLKEY = "MONTANT";
	public static final String NBR_UNITE_COLKEY = "NBR_UNITE";
	public static final String NO_ARRETE_COLKEY = "NO_ARRETE";
	public static final String NO_AVENANT_COLKEY = "NO_AVENANT";
	public static final String POURCENT_SMIC_COLKEY = "POURCENT_SMIC";
	public static final String QUOTITE_COLKEY = "NUM_QUOT_RECRUTEMENT";
	public static final String REFERENCE_CONTRAT_COLKEY = "REFERENCE_CONTRAT";
	public static final String TAUX_HORAIRE_COLKEY = "TAUX_HORAIRE";
	public static final String TEM_ANNULATION_COLKEY = "TEM_ANNULATION";
	public static final String TEM_ARRETE_SIGNE_COLKEY = "TEM_ARRETE_SIGNE";
	public static final String TEM_GEST_ETAB_COLKEY = "TEM_GEST_ETAB";
	public static final String TEM_PAIEMENT_PONCTUEL_COLKEY = "TEM_PAIEMENT_PONCTUEL";
	public static final String TYPE_MONTANT_COLKEY = "TYPE_MONTANT";

	public static final String AVENANT_ORDRE_COLKEY = "CTRA_ORDRE";
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_DISC_SECOND_DEGRE_COLKEY = "C_DISC_SECOND_DEGRE";
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String C_SPECIALITE_ATOS_COLKEY = "C_SPECIALITE_ATOS";
	public static final String C_SPECIALITE_ITARF_COLKEY = "C_SPECIALITE_ITARF";
	public static final String CODEEMPLOI_COLKEY = "CODEEMPLOI";
	public static final String NO_CNU_COLKEY = "NO_CNU";
	public static final String NO_SEQ_CONTRAT_COLKEY = "NO_SEQ_CONTRAT";
	public static final String TUT_ORDRE_COLKEY = "TUT_ORDRE";
	public static final String ID_COND_RECRUT_COLKEY = "ID_COND_RECRUT";
	public static final String BAP_ID_COLKEY = "BAP_ID";


	// Relationships
	public static final String CONDITION_RECRUTEMENT_KEY = "conditionRecrutement";
	public static final String CONTRAT_KEY = "contrat";
	public static final String TO_BAP_KEY = "toBap";
	public static final String TO_CATEGORIE_KEY = "toCategorie";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_DISC_SECOND_DEGRE_KEY = "toDiscSecondDegre";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_REFERENS_EMPLOI_KEY = "toReferensEmploi";
	public static final String TO_SPECIALITE_ATOS_KEY = "toSpecialiteAtos";
	public static final String TO_SPECIALITE_ITARF_KEY = "toSpecialiteItarf";
	public static final String TO_TYPE_MONTANT_KEY = "toTypeMontant";
	public static final String TYPE_DUREE_KEY = "typeDuree";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cDea() {
    return (String) storedValueForKey(C_DEA_KEY);
  }

  public void setCDea(String value) {
    takeStoredValueForKey(value, C_DEA_KEY);
  }

  public String cEchelon() {
    return (String) storedValueForKey(C_ECHELON_KEY);
  }

  public void setCEchelon(String value) {
    takeStoredValueForKey(value, C_ECHELON_KEY);
  }

  public Double ctraDuree() {
    return (Double) storedValueForKey(CTRA_DUREE_KEY);
  }

  public void setCtraDuree(Double value) {
    takeStoredValueForKey(value, CTRA_DUREE_KEY);
  }

  public Integer ctraDureeValideeAnnees() {
    return (Integer) storedValueForKey(CTRA_DUREE_VALIDEE_ANNEES_KEY);
  }

  public void setCtraDureeValideeAnnees(Integer value) {
    takeStoredValueForKey(value, CTRA_DUREE_VALIDEE_ANNEES_KEY);
  }

  public Integer ctraDureeValideeJours() {
    return (Integer) storedValueForKey(CTRA_DUREE_VALIDEE_JOURS_KEY);
  }

  public void setCtraDureeValideeJours(Integer value) {
    takeStoredValueForKey(value, CTRA_DUREE_VALIDEE_JOURS_KEY);
  }

  public Integer ctraDureeValideeMois() {
    return (Integer) storedValueForKey(CTRA_DUREE_VALIDEE_MOIS_KEY);
  }

  public void setCtraDureeValideeMois(Integer value) {
    takeStoredValueForKey(value, CTRA_DUREE_VALIDEE_MOIS_KEY);
  }

  public String ctraPcAcquitees() {
    return (String) storedValueForKey(CTRA_PC_ACQUITEES_KEY);
  }

  public void setCtraPcAcquitees(String value) {
    takeStoredValueForKey(value, CTRA_PC_ACQUITEES_KEY);
  }

  public Double ctraQuotiteCotisation() {
    return (Double) storedValueForKey(CTRA_QUOTITE_COTISATION_KEY);
  }

  public void setCtraQuotiteCotisation(Double value) {
    takeStoredValueForKey(value, CTRA_QUOTITE_COTISATION_KEY);
  }

  public String ctraTypeTemps() {
    return (String) storedValueForKey(CTRA_TYPE_TEMPS_KEY);
  }

  public void setCtraTypeTemps(String value) {
    takeStoredValueForKey(value, CTRA_TYPE_TEMPS_KEY);
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey(DATE_ARRETE_KEY);
  }

  public void setDateArrete(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ARRETE_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dRefCtrAvenant() {
    return (NSTimestamp) storedValueForKey(D_REF_CTR_AVENANT_KEY);
  }

  public void setDRefCtrAvenant(NSTimestamp value) {
    takeStoredValueForKey(value, D_REF_CTR_AVENANT_KEY);
  }

  public NSTimestamp dValContratAv() {
    return (NSTimestamp) storedValueForKey(D_VAL_CONTRAT_AV_KEY);
  }

  public void setDValContratAv(NSTimestamp value) {
    takeStoredValueForKey(value, D_VAL_CONTRAT_AV_KEY);
  }

  public String fonctionCtrAvenant() {
    return (String) storedValueForKey(FONCTION_CTR_AVENANT_KEY);
  }

  public void setFonctionCtrAvenant(String value) {
    takeStoredValueForKey(value, FONCTION_CTR_AVENANT_KEY);
  }

  public String indiceContrat() {
    return (String) storedValueForKey(INDICE_CONTRAT_KEY);
  }

  public void setIndiceContrat(String value) {
    takeStoredValueForKey(value, INDICE_CONTRAT_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_KEY);
  }

  public java.math.BigDecimal nbrUnite() {
    return (java.math.BigDecimal) storedValueForKey(NBR_UNITE_KEY);
  }

  public void setNbrUnite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, NBR_UNITE_KEY);
  }

  public String noArrete() {
    return (String) storedValueForKey(NO_ARRETE_KEY);
  }

  public void setNoArrete(String value) {
    takeStoredValueForKey(value, NO_ARRETE_KEY);
  }

  public String noAvenant() {
    return (String) storedValueForKey(NO_AVENANT_KEY);
  }

  public void setNoAvenant(String value) {
    takeStoredValueForKey(value, NO_AVENANT_KEY);
  }

  public Double pourcentSmic() {
    return (Double) storedValueForKey(POURCENT_SMIC_KEY);
  }

  public void setPourcentSmic(Double value) {
    takeStoredValueForKey(value, POURCENT_SMIC_KEY);
  }

  public Double quotite() {
    return (Double) storedValueForKey(QUOTITE_KEY);
  }

  public void setQuotite(Double value) {
    takeStoredValueForKey(value, QUOTITE_KEY);
  }

  public String referenceContrat() {
    return (String) storedValueForKey(REFERENCE_CONTRAT_KEY);
  }

  public void setReferenceContrat(String value) {
    takeStoredValueForKey(value, REFERENCE_CONTRAT_KEY);
  }

  public java.math.BigDecimal tauxHoraire() {
    return (java.math.BigDecimal) storedValueForKey(TAUX_HORAIRE_KEY);
  }

  public void setTauxHoraire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TAUX_HORAIRE_KEY);
  }

  public String temAnnulation() {
    return (String) storedValueForKey(TEM_ANNULATION_KEY);
  }

  public void setTemAnnulation(String value) {
    takeStoredValueForKey(value, TEM_ANNULATION_KEY);
  }

  public String temArreteSigne() {
    return (String) storedValueForKey(TEM_ARRETE_SIGNE_KEY);
  }

  public void setTemArreteSigne(String value) {
    takeStoredValueForKey(value, TEM_ARRETE_SIGNE_KEY);
  }

  public String temGestEtab() {
    return (String) storedValueForKey(TEM_GEST_ETAB_KEY);
  }

  public void setTemGestEtab(String value) {
    takeStoredValueForKey(value, TEM_GEST_ETAB_KEY);
  }

  public String temPaiementPonctuel() {
    return (String) storedValueForKey(TEM_PAIEMENT_PONCTUEL_KEY);
  }

  public void setTemPaiementPonctuel(String value) {
    takeStoredValueForKey(value, TEM_PAIEMENT_PONCTUEL_KEY);
  }

  public String typeMontant() {
    return (String) storedValueForKey(TYPE_MONTANT_KEY);
  }

  public void setTypeMontant(String value) {
    takeStoredValueForKey(value, TYPE_MONTANT_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.contrat.EOConditionRecrutement conditionRecrutement() {
    return (org.cocktail.mangue.common.modele.nomenclatures.contrat.EOConditionRecrutement)storedValueForKey(CONDITION_RECRUTEMENT_KEY);
  }

  public void setConditionRecrutementRelationship(org.cocktail.mangue.common.modele.nomenclatures.contrat.EOConditionRecrutement value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.contrat.EOConditionRecrutement oldValue = conditionRecrutement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONDITION_RECRUTEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONDITION_RECRUTEMENT_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOContrat contrat() {
    return (org.cocktail.mangue.modele.mangue.individu.EOContrat)storedValueForKey(CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.mangue.modele.mangue.individu.EOContrat value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOContrat oldValue = contrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap toBap() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap)storedValueForKey(TO_BAP_KEY);
  }

  public void setToBapRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap oldValue = toBap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BAP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BAP_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOCategorie toCategorie() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOCategorie)storedValueForKey(TO_CATEGORIE_KEY);
  }

  public void setToCategorieRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOCategorie value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOCategorie oldValue = toCategorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CATEGORIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CATEGORIE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu toCnu() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu)storedValueForKey(TO_CNU_KEY);
  }

  public void setToCnuRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CNU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CNU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre toDiscSecondDegre() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre)storedValueForKey(TO_DISC_SECOND_DEGRE_KEY);
  }

  public void setToDiscSecondDegreRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre oldValue = toDiscSecondDegre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DISC_SECOND_DEGRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DISC_SECOND_DEGRE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOGrade toGrade() {
    return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey(TO_GRADE_KEY);
  }

  public void setToGradeRelationship(org.cocktail.mangue.modele.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GRADE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois toReferensEmploi() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois)storedValueForKey(TO_REFERENS_EMPLOI_KEY);
  }

  public void setToReferensEmploiRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois oldValue = toReferensEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REFERENS_EMPLOI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REFERENS_EMPLOI_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos toSpecialiteAtos() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos)storedValueForKey(TO_SPECIALITE_ATOS_KEY);
  }

  public void setToSpecialiteAtosRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos oldValue = toSpecialiteAtos();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SPECIALITE_ATOS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SPECIALITE_ATOS_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf toSpecialiteItarf() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf)storedValueForKey(TO_SPECIALITE_ITARF_KEY);
  }

  public void setToSpecialiteItarfRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf oldValue = toSpecialiteItarf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SPECIALITE_ITARF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SPECIALITE_ITARF_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeRemunContrat toTypeMontant() {
    return (org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeRemunContrat)storedValueForKey(TO_TYPE_MONTANT_KEY);
  }

  public void setToTypeMontantRelationship(org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeRemunContrat value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeRemunContrat oldValue = toTypeMontant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_MONTANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_MONTANT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeUniteTemps typeDuree() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeUniteTemps)storedValueForKey(TYPE_DUREE_KEY);
  }

  public void setTypeDureeRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeUniteTemps value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeUniteTemps oldValue = typeDuree();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_DUREE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_DUREE_KEY);
    }
  }
  

/**
 * Créer une instance de EOContratAvenant avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOContratAvenant createEOContratAvenant(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Double quotite
, org.cocktail.mangue.modele.mangue.individu.EOContrat contrat			) {
    EOContratAvenant eo = (EOContratAvenant) createAndInsertInstance(editingContext, _EOContratAvenant.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setQuotite(quotite);
    eo.setContratRelationship(contrat);
    return eo;
  }

  
	  public EOContratAvenant localInstanceIn(EOEditingContext editingContext) {
	  		return (EOContratAvenant)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOContratAvenant creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOContratAvenant creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOContratAvenant object = (EOContratAvenant)createAndInsertInstance(editingContext, _EOContratAvenant.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOContratAvenant localInstanceIn(EOEditingContext editingContext, EOContratAvenant eo) {
    EOContratAvenant localInstance = (eo == null) ? null : (EOContratAvenant)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOContratAvenant#localInstanceIn a la place.
   */
	public static EOContratAvenant localInstanceOf(EOEditingContext editingContext, EOContratAvenant eo) {
		return EOContratAvenant.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOContratAvenant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOContratAvenant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOContratAvenant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOContratAvenant)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOContratAvenant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOContratAvenant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOContratAvenant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOContratAvenant)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOContratAvenant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOContratAvenant eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOContratAvenant ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOContratAvenant fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

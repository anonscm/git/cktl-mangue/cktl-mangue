// EODifDetail.java
// Created on Fri Mar 14 10:43:15 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.PeriodePourIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** DifDetail - Regles de validation<BR>
 * Longueur des champs<BR>
 * Annee de demarrage posterieure a 2007<BR>
 * Le detail doit etre la meme anne que le DIF<BR>
 *
 */
public class EODifDetail extends _EODifDetail {

    public EODifDetail() {
        super();
    }

	public static EODifDetail creer(EOEditingContext edc, EODif dif) {

		EODifDetail newObject = new EODifDetail();
		edc.insertObject(newObject);
		
		newObject.setDifRelationship(dif);
		newObject.setDateDebut(dif.dateDebut());
		newObject.setDateFin(dif.dateFin());

		return newObject;

	}

	/** Retourne les difs pour une annee */
	public static NSArray<EODif> rechercherDetailsAnterieursAAnnee(EOEditingContext edc, int annee) {		
		return fetchAll(edc, CocktailFinder.getQualifierBeforeEq(DIF_KEY+"."+EODif.ANNEE_CALCUL_KEY, annee));
	}

	/**
	 * 
	 * @param ec
	 * @param dif
	 * @return
	 */
    public static NSArray<EODifDetail> findForDif(EOEditingContext ec, EODif dif) {   	
    	return fetchAll(ec, CocktailFinder.getQualifierEqual(DIF_KEY, dif), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
    }

   	public void initAvecDates(NSTimestamp dateDebut,NSTimestamp dateFin) {
		setDateDebut(dateDebut);
		setDateFin(dateFin);
	}
	public void initAvecDif(EODif dif) {
		setDifRelationship(dif);
		initAvecDates(dif.dateDebut(), dif.dateFin());
	}
	public void supprimerRelations() {
		if (dif() != null) {
			setDifRelationship(null);
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (difDetailLibelle() != null && difDetailLibelle().length() > 2000) {
			throw new NSValidation.ValidationException("Le libellé ne peut dépasser 2000 caractères");
		}
		// La date de fin ne doit pas etre inferieure a la date de debut
		
		if (dif() == null) {
			throw new NSValidation.ValidationException("Chaque détail doit être relié à un DIF !");			
		}
		
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début doit etre définie !");
		} else if (dateFin() != null && dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException("La date de fin doit être postérieure à la date de début !");
		}
		int anneeDebut = DateCtrl.getYear(dateDebut());
		if (anneeDebut < ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF) {
			throw new NSValidation.ValidationException("Le droit individuel à la formation n'est consommable qu'à partir de " + ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF + " !");
		} else {
			int anneeDif = DateCtrl.getYear(dif().dateDebut());
			if (anneeDebut != anneeDif) {
				throw new NSValidation.ValidationException("Le détail doit être la même année que le DIF");
			}
		}
		
		if (dCreation() == null)
			setDCreation(DateCtrl.today());
		setDModification(DateCtrl.today());
	}
}

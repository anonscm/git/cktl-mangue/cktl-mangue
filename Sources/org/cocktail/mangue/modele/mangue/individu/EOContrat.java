//EOContrat.java
//Created on Wed Mar 19 08:44:14  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;


import java.util.Enumeration;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.IDureePourIndividu;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation des contrats :<BR>
 *	un contrat doit avoir :
 *	<UL>un type de contrat</UL>
 *	<UL>une date de debut</UL>
 *	<UL>pour un CDI, pas de date de fin</UL>
 * La date de debut ne peut etre posterieure a la date de fin<BR>
 * La duree du contrat doit respecter les limites min et max definies dans le type de contrat<BR>
 * La date de fin de contrat ne peut etre posterieure a celle des Details de contrat<BR>
 * Il ne peut pas y avoir de contrat d'heberge pendant la periode<BR>
 * Un contrat ne peut etre defini pour une periode que si il n'y a pas d'autres contrats en cours avec une quotite totale egale a 100%<BR>
 **/
public class EOContrat extends _EOContrat implements RecordAvecLibelle,IDureePourIndividu {

	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";

	public EOContrat() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOContrat creer(EOEditingContext ec, EOIndividu individu) {

		EOContrat newObject = (EOContrat) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setTemBudgetPropre(CocktailConstantes.FAUX);
		newObject.setTemRecherche(CocktailConstantes.FAUX);
		newObject.setTemFonctionnaire(CocktailConstantes.FAUX);
		newObject.setNoDossierPers(individu.noIndividu());
		newObject.setToIndividuRelationship(individu);
		newObject.setTemAnnulation(CocktailConstantes.FAUX);
		newObject.setEstSauvadet(false);
		newObject.setEstCIR(false);
		return newObject;

	}
	public void invalider() {
		setTemAnnulation(CocktailConstantes.VRAI);
		if (avenants() != null || avenants().count() > 0) {
			for (java.util.Enumeration<EOContratAvenant> e = avenants().objectEnumerator();e.hasMoreElements();) {
				EOContratAvenant avenant = e.nextElement();
				avenant.invalider();
			}
		}
	}

	/**
	 * 
	 * @param ec
	 * @param ancienContrat
	 * @return
	 */
	public static EOContrat renouveler(EOEditingContext ec, EOContrat ancienContrat) {

		EOContrat newObject = (EOContrat) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setNoDossierPers(ancienContrat.individu().noIndividu());
		newObject.setToIndividuRelationship(ancienContrat.individu());

		newObject.setDateDebut(ancienContrat.dateDebut());
		newObject.setDateFinAnticipee(ancienContrat.dateFinAnticipee());
		newObject.setDateFin(ancienContrat.dateFin());
		newObject.setCommentaire(ancienContrat.commentaire());

		newObject.setToTypeContratTravailRelationship(ancienContrat.toTypeContratTravail());
		newObject.setToRneRelationship(ancienContrat.toRne());
		newObject.setToOrigineFinancementRelationship(ancienContrat.toOrigineFinancement());
		newObject.setTypeRecrutementRelationship(ancienContrat.typeRecrutement());

		newObject.setTemAnnulation(CocktailConstantes.FAUX);
		newObject.setTemBudgetPropre(ancienContrat.temBudgetPropre());
		newObject.setTemFonctionnaire(ancienContrat.temFonctionnaire());
		newObject.setTemRecherche(ancienContrat.temRecherche());
		newObject.setTemSauvadet(ancienContrat.temSauvadet());

		if (ancienContrat.dateFinAnticipee() != null)
			newObject.setDateDebut(DateCtrl.jourSuivant(ancienContrat.dateFinAnticipee()));
		else
			newObject.setDateDebut(DateCtrl.jourSuivant(ancienContrat.dateFin()));
		newObject.setDateFin(null);

		return newObject;
	}

	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,DATE_DEBUT_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setDateDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_DEBUT_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,DATE_FIN_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setDateFin(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_FIN_KEY,uneDate);
		}
	}

	public String dateFinAnticipeeFormatee() {
		return SuperFinder.dateFormatee(this,DATE_FIN_ANTICIPEE_KEY);
	}
	public void setDateFinAnticipeeFormatee(String uneDate) {
		if (uneDate == null) {
			setDateFinAnticipee(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_FIN_ANTICIPEE_KEY,uneDate);
		}
	}

	public boolean estSauvadet() {
		return temSauvadet() != null && temSauvadet().equals("O");
	}
	public void setEstSauvadet(boolean value) {
		if (value)
			setTemSauvadet(CocktailConstantes.VRAI);
		else
			setTemSauvadet(CocktailConstantes.FAUX);
	}
	public boolean estFonctionnaire() {
		return temFonctionnaire() != null && temFonctionnaire().equals(CocktailConstantes.VRAI);
	}
	public void setEstFonctionnaire(boolean value) {
		if (value)
			setTemFonctionnaire(CocktailConstantes.VRAI);
		else
			setTemFonctionnaire(CocktailConstantes.FAUX);
	}

	public boolean estAnnule() {
		return temAnnulation() != null && temAnnulation().equals(CocktailConstantes.VRAI);
	}
	public void setEstAnnule(boolean aBool) {
		if (aBool) {
			setTemAnnulation(CocktailConstantes.VRAI);
		} else {
			setTemAnnulation(CocktailConstantes.FAUX);
		}
	}
	
	/**
	 * Est-ce que le contrat doit remonter au CIR?
	 * @return Vrai/Faux
	 */
	public boolean estCIR() {
		return temCIR() != null && CocktailConstantes.VRAI.equals(temCIR());
	}
	
	/**
	 * Setter
	 * @param value : booléen
	 */
	public void setEstCIR(boolean value) {
		if (value) {
			setTemCIR(CocktailConstantes.VRAI);
		} else {
			setTemCIR(CocktailConstantes.FAUX);
		}
	}

	/**
	 * 
	 * @return
	 */
	public EOContratAvenant avenantCourant() {

		NSArray<EOContratAvenant> results = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenants(), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC );
		if (results != null && results.count() > 0) {
			for (EOContratAvenant myAvenant : results) {

				if (!myAvenant.estAnnule() && myAvenant.dateDebut() != null) {
					if (myAvenant.dateFin() == null) {
						if (DateCtrl.isBeforeEq(myAvenant.dateDebut(), DateCtrl.today())) {
							return myAvenant;
						}
					}
					else
						if (DateCtrl.isBetween(DateCtrl.today(), myAvenant.dateDebut(), myAvenant.dateFin())) {
							return myAvenant;
						}
				}
			}
		}
		return null;
	}

	/** Retourne le temoin enseignant de la vacation ou du detail de contrat courant ou "" si pas de vacation ou de detail de contrat */
	public String temEnseignant() {

		EOContratAvenant avenant = avenantCourant();
		if (avenant != null) {
			if (avenant.toGrade() != null && !avenant.toGrade().estNonRenseigne() && avenant.toGrade().toCorps().toTypePopulation() != null) {
				return avenant.toGrade().toCorps().toTypePopulation().temEnseignant();
			} else {
				return toTypeContratTravail().temEnseignant();
			}
		}
		else
			return toTypeContratTravail().temEnseignant();

	}

	/** retourne true si le contrat a des details de contrat ou des vacations pour une population de type hospitalier */
	public boolean estTypeHospitalierPourPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		boolean estHospitalier = false;
		NSArray<EOContratAvenant> avenants = avenantsPourPeriode(debutPeriode,finPeriode);
		if (avenants != null && avenants.count() > 0) {
			for (EOContratAvenant myAvenant : avenants) {

				if ( myAvenant.contrat() != null && myAvenant.contrat().toTypeContratTravail() != null 
						&& myAvenant.contrat().toTypeContratTravail().estHospitalier()) {
					estHospitalier = true;
					break;
				}
			}
		}

		return estHospitalier;
	}

	/** Pour l'impression des lignes budgetaires de l'annee a la date de debut du contrat si plusieurs lignes budgetaires */
	public String ligneBudgetaire() {
		return null;
	}

	/** Retourne les details de contrat pendant la periode passee en parametre
	 * @param dateDebut
	 * @param dateFin peut etre nulle
	 * @return tableau des details de contrat ou null
	 */
	public NSArray<EOContratAvenant> avenantsPourPeriode(NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (dateDebut == null || avenants() == null || avenants().count() == 0) {
			return new NSArray<EOContratAvenant>();
		}
		NSArray<EOContratAvenant> avenantsTries = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenants(), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		NSMutableArray<EOContratAvenant> results = new NSMutableArray<EOContratAvenant>();
		for (EOContratAvenant myAvenant : avenantsTries) {
			if (!myAvenant.estAnnule()) {
				if (Utilitaires.IntervalleTemps.intersectionPeriodes(dateDebut, dateFin, myAvenant.dateDebut(), myAvenant.dateFin()) != null) {
					results.addObject((myAvenant));
				}
			}
		}
		return results;
	}

	/** retourne true si on peut fermer le contrata la nouvelle date (i.e nouvelle date fin > date debut) et
	 * si on peut fermer les details de contrat a la date : la date de debut 
	 * des details de contrat doit etre posterieure a la date passee en parametre
	 * @param date
	 * @return
	 */
	public boolean peutFermerADate(NSTimestamp date) {
		if (DateCtrl.isBefore(date, dateDebut())) {
			return false;
		}
		if (avenantCourant() == null) {
			return true;
		} else {
			return (DateCtrl.isBefore(avenantCourant().dateDebut(),date));
		}
	}
	public void fermerContratADate(NSTimestamp date) {
		setDateFin(date);
		// les détails de contrat
		EOContratAvenant avenant = avenantCourant();
		if (avenant != null && (avenant.dateFin() == null || DateCtrl.isAfter(avenant.dateFin(), date))) {
			avenant.setDateFin(date);
		}
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectForSave() throws NSValidation.ValidationException {

		if (estAnnule()) 
			return;

		if (toTypeContratTravail() == null) {
			throw new NSValidation.ValidationException("Vous devez choisir un type de contrat");
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début de contrat est obligatoire");
		}
		
		if (!toTypeContratTravail().estCdi()) {
			if (dateFin() == null) {
				throw new NSValidation.ValidationException("Pour un CDD, la date de fin de contrat est obligatoire");
			}
		}
		if (dateFin() != null && DateCtrl.isBefore(dateFin(), dateDebut())) {
			throw new NSValidation.ValidationException("CONTRAT\nLa date de fin doit être postérieure à la date de début");
		}
		if (dateFin() != null && dateFin() != null && DateCtrl.isBefore(dateFin(),dateFin())) {
			throw new NSValidation.ValidationException("La date de fin anticipée doit être antérieure à la date de fin");
		}

		// vérifier si la durée du contrat est inférieure à la période min. définie dans le type de contrat
		if (toTypeContratTravail().dureeInitContrat() != null) {
			int dureeMin = toTypeContratTravail().dureeInitContrat().intValue();
			Integer dureeContratJours = DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true, true);
			if (dureeContratJours == null || dureeContratJours.intValue() <( dureeMin * 30)) {
				throw new NSValidation.ValidationException("Ce contrat doit durer au moins " + dureeMin + " mois !");
			}
		}
		// vérifier si la durée du contrat dépasse la période max. définie dans le type de contrat
		if (toTypeContratTravail().dureeMaxContrat() != null) {
			int dureeMax = toTypeContratTravail().dureeMaxContrat().intValue();
			Integer dureeContratJours = DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true, true);
			// ce type de contrat est forcément un cdd
			if (dureeContratJours == null || dureeContratJours.intValue() > (dureeMax * 30)) {
				throw new NSValidation.ValidationException("Ce contrat ne peut pas durer plus de " + dureeMax + " mois !");
			}
		}
		// vérifier si il y a des détails que la date de fin de contrat ne soit pas inférieure à celle des détails
		// on suppose ici qu'il n'y a pas de détails qui se chevauchent
		if (dateFin() != null) {
			EOContratAvenant avenant = avenantCourant();	// dernier avenant
			if (avenant != null) {
				if (avenant.dateFin() == null) {
					// le détail devrait être terminé
					throw new NSValidation.ValidationException("Le contrat a une date de fin, les détails de contrat doivent aussi l'avoir");
				} else if (DateCtrl.isBefore(dateFin(),avenant.dateFin())) {
					throw new NSValidation.ValidationException("Le contrat ne peut se terminer avant la fin du dernier détail de contrat");
				}
			}
		}

		// gestion  des hospitalo-univ
		if (typeRecrutement() != null && toTypeContratTravail().estHospitalier() == false) {
			throw new NSValidation.ValidationException("Le type de recrutement ne s'applique qu'aux personnels hospitalo-universitaires");
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	// interface RecordAvecLibelle
	public String libelle() {
		if (toTypeContratTravail() == null) {
			return "";
		}
		String libelle = toTypeContratTravail().libelleCourt();
		if (dateDebut() != null) {
			if (dateFin() != null) {
				libelle = libelle + " du " + DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dateFin());
			} else {
				libelle = libelle + " à partir du " + DateCtrl.dateToString(dateDebut());
			}
		}
		return libelle;
	}

	public EOIndividu individu() {
		return toIndividu();
	}
	public boolean supportePlusieursTypesEvenement() {
		return false;
	}
	public String typeEvenement() {
		if (toTypeContratTravail() == null) {
			return "";
		} else {
			return toTypeContratTravail().code();
		}
	}

	/** recherche les contrats d'un individu qui ne sont pas annules et qui ne sont ni des contrats de titulaire ni de remuneration accessoire
	 * @param editingContext
	 * @param individu
	 * @param prefecth true si il faut faire les prefetchs
	 * @return contrats trouves
	 */

	public static NSArray<EOContrat> rechercherContratsPourIndividu(EOEditingContext editingContext,EOIndividu individu,boolean prefetch) {
		try {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY+" = %@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY+" <> %@", new NSArray(CocktailConstantes.VRAI)));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);		
		}
		catch (Exception e) {
			return new NSArray<EOContrat>();
		}
	}
	/** recherche les contrats d'un individu qui ne sont pas annules et qui ne sont pas des contrats de titulaire
	 * et qui donnent droits a des conges
	 * @param editingContext
	 * @param individu
	 * @param prefecth true si il faut faire les prefetchs
	 * @return contrats troues
	 */

	public static NSArray<EOContrat> rechercherContratsSupportantCongePourIndividu(EOEditingContext editingContext,EOIndividu individu) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY+" = %@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY+" <> %@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.TEM_POUR_TITULAIRE_KEY+" <> %@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.DROIT_CONGES_CONTRAT_KEY+" <> %@", new NSArray(CocktailConstantes.VRAI)));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);		
	}
	/** recherche les contrats de remuneration principale valides d'un individu dont les dates de debut et fin sont a cheval sur deux dates
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @param prefetch true si prefetcher les donnees liees
	 * @param shouldRefresh true si faire un refresh des carrieres
	 * @return contrats trouves
	 */
	public static NSArray<EOContrat> rechercherContratsPourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean prefetch,boolean shouldRefresh) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(qualifierPourPeriode(individu,debutPeriode,finPeriode));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY+" <> %@", new NSArray(CocktailConstantes.VRAI)));
			return fetchAll(editingContext, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);		
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	/** recherche les contrats de remuneration principale valides d'un individu dont les dates de debut et fin sont a cheval sur deux dates
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @param prefetch true si prefetcher les donnees liees
	 * @return contrats trouves
	 */
	public static NSArray<EOContrat> rechercherContratsPourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean prefetch) {
		return rechercherContratsPourIndividuEtPeriode(editingContext, individu, debutPeriode, finPeriode, prefetch,true);
	}

	/**
	 * 
	 * @param edc
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOContrat> findForType(EOEditingContext edc, EOTypeContratTravail type) {
		try {			
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_CONTRAT_TRAVAIL_KEY + " = %@", new NSArray(type)));
			return fetchAll(edc, new EOAndQualifier(qualifiers), null);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOContrat>();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOContrat> findContratsCIRPourIndividuAndDate(EOEditingContext ec, EOIndividu individu, NSTimestamp dateReference) {

		try {

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(getQualifierIndividu(individu));
			qualifiers.addObject(getQualifierValide());
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateReference, DATE_FIN_KEY, dateReference));
			qualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_CIR_KEY, "O"));

			return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);		
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param edc
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOContrat> findForPeriode(EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {

		try {			
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(getQualifierValide());
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateDebut, DATE_FIN_KEY,dateFin));
			return fetchAll(edc, new EOAndQualifier(qualifiers), null);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}


	/** recherche les contrats valides d'un individu dont les dates de debut et fin sont a cheval sur deux dates
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode 	peut etre nulle
	 * @param prefetch true si prefetcher les donnees liees
	 * @return contrats trouves
	 */
	public static NSArray<EOContrat> rechercherTousContratsPourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(qualifierPourPeriode(individu,debutPeriode,finPeriode));
			qualifiers.addObject(getQualifierValide());

			return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);		
		}
		catch (Exception e) {
			return new NSArray();
		}
	}
	/** recherche les contrats d'un individu posterieurs a la date fournie
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @return contrats trouves
	 */
	public static NSArray<EOContrat> rechercherContratsIndividuPosterieursADate(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY+"."+EOIndividu.NO_INDIVIDU_KEY + " = %@", new NSArray(individu.noIndividu())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY+" > %@", new NSArray(date)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY+" = %@", new NSArray("N")));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);		
	}
	/** recherche les contrats de remuneration principale d'un individu anterieurs a la date fournie
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @return contrats trouves
	 */
	public static NSArray<EOContrat> rechercherContratsIndividuAnterieursADate(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY+" <= %@", new NSArray(date)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY+" = %@", new NSArray("N")));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);		
	}
	/** recherche les contrats de remuneration principale et non invites d'un individu 
	 * anterieurs a la date fournie
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @return contrats trouves
	 */
	public static NSArray<EOContrat> rechercherContratsNonInvitesIndividuAnterieursADate(EOEditingContext edc,EOIndividu individu,NSTimestamp date) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY+" <= %@", new NSArray(date)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.TEM_INVITE_ASSOCIE_KEY+" =%@", new NSArray(CocktailConstantes.FAUX)));
		qualifiers.addObject(getQualifierValide());

		return fetchAll(edc, new EOAndQualifier(qualifiers), null);		

	}
	/** retourne le contrat de remun&eacute;ration principale a la date donnee ou null
	 * @param editingContext
	 * @param individu
	 * @param date	
	 */
	public static EOContrat contratPrincipalPourDate(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) {
		NSArray<EOContrat> contrats = rechercherContratsPourIndividuEtPeriode(editingContext, individu, date, date, true,false);
		try {
			return contrats.get(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Retourne les contrats associes au depart passe en parametre
	 * @param depart
	 */
	public static NSArray<EOContrat> rechercherContratsPourDepart(EOEditingContext ec,EODepart depart, boolean refresh) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DEPART_KEY +" = %@", new NSArray(depart)));
			return fetchAll(ec, new EOAndQualifier(qualifiers),null);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
	/** recherche les contrats non annules commencant avant dateDebut et terminant apres dateFin et dont la date anticipee
	 * est nulle ou posterieure a date fin
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateDebut peut etre nulle
	 * @param dateFin	 peut etre nulle
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants, vacataires
	 * @param supprimerDoublons true si supprimer les doublons
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @param prefetchIdentite true si il faut prefetcher la relation identiteIndividu
	 * @return contrats trouves
	 */
	public static NSArray rechercherContratsPourPeriodeEtTypePersonnel(EOEditingContext editingContext,String nom,String prenom,NSTimestamp dateDebut,NSTimestamp dateFin,int typePersonnel,boolean supprimerDoublons,boolean estNomPatronymique,boolean prefetchIdentite) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (dateDebut != null && dateFin != null) { 
			NSMutableArray args = new NSMutableArray();
			String stringQualifier = "";
			args.addObject(dateDebut);
			args.addObject(dateFin);
			args.addObject(dateFin);
			stringQualifier = "temAnnulation != 'O' AND dateDebut <= %@ AND (dateFin >= %@ OR dateFin = nil) AND (dateFinAnticipee >= %@ OR dateFinAnticipee = nil)";
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(stringQualifier, args));
		} else {
			// tous les contrats non annulés
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("temAnnulation != 'O'",null));
		}
		qualifiers.addObject(qualifierPourNom(TO_INDIVIDU_KEY, nom, prenom, estNomPatronymique));
		NSMutableArray allQualifiers = new NSMutableArray(qualifiers);
		if (typePersonnel != ManGUEConstantes.TOUT_EMPLOYE) {
			allQualifiers.addObject(qualifierPourTypePersonnel("",typePersonnel));
		}
		EOQualifier qualifier = new EOAndQualifier(allQualifiers);
		LogManager.logDetail("rechercherContratsPourPeriodeEtTypePersonnel - qualifier : " + qualifier);
		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(NO_DOSSIER_PERS_KEY,EOSortOrdering.CompareAscending));
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,sorts);

		myFetch.setUsesDistinct(supprimerDoublons);
		if (prefetchIdentite) {
			myFetch.setPrefetchingRelationshipKeyPaths(new NSArray(TO_INDIVIDU_KEY));
		}
		myFetch.setRefreshesRefetchedObjects(true);
		NSMutableArray contrats = new NSMutableArray(editingContext.objectsWithFetchSpecification(myFetch));
		EOQualifier autreQualifier = autreQualifierPourTypePersonnel("",typePersonnel);
		if (autreQualifier != null) {
			qualifiers.addObject(autreQualifier);
			qualifier = new EOAndQualifier(qualifiers);
			LogManager.logDetail("rechercherContratsPourPeriodeEtTypePersonnel - autre qualifier : " + qualifier);
			myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,sorts);
			myFetch.setUsesDistinct(supprimerDoublons);
			myFetch.setRefreshesRefetchedObjects(true);
			NSArray result = editingContext.objectsWithFetchSpecification(myFetch);
			for (java.util.Enumeration<EOContrat> e = result.objectEnumerator();e.hasMoreElements();) {
				EOContrat contrat = e.nextElement();
				if (contrats.containsObject(contrat) == false) {
					contrats.addObject(contrat);
				}
			}
			EOSortOrdering.sortArrayUsingKeyOrderArray(contrats, sorts);
		}
		return contrats;
	}
	/** recherche les contrats non annules commencant avant dateDebut et terminant a dateFin
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateDebut peut etre nulle
	 * @param dateFin	 peut etre nulle
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants, vacataires
	 * @param supprimerDoublons true si supprimer les doublons
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @return contrats trouv&eacute;s
	 */
	public static NSArray rechercherContratsPourPeriodeEtTypePersonnel(EOEditingContext editingContext,String nom,String prenom,NSTimestamp dateDebut,NSTimestamp dateFin,int typePersonnel,boolean supprimerDoublons,boolean estNomPatronymique) {
		return rechercherContratsPourPeriodeEtTypePersonnel(editingContext, nom, prenom, dateDebut, dateFin, typePersonnel, supprimerDoublons, estNomPatronymique, false);
	}
	/** recherche les individus avec des contrats non annules commencant avant dateDebut et terminant apres dateFin
	 * et dont la date anticipee est nulle ou posterieure a date fin
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateDebut peut etre nulle
	 * @param dateFin	 peut etre nulle
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants
	 * @param supprimerDoublons true si supprimer les doublons
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @return individus trouves (type IndividuIdentite)
	 */
	public static NSArray rechercherIndividusAvecContratsPourPeriodeEtTypePersonnel(EOEditingContext editingContext,String nom,String prenom,NSTimestamp dateDebut,NSTimestamp dateFin,int typePersonnel,boolean supprimerDoublons,boolean estNomPatronymique) {
		LogManager.logDetail("rechercherIndividusAvecContratsPourPeriodeEtTypePersonnel - typePersonnel : " + typePersonnel);
		EOQualifier qualifierDate = null;
		if (dateDebut != null && dateFin != null) { 
			NSMutableArray args = new NSMutableArray();
			String stringQualifier = "";
			args.addObject(dateDebut);
			args.addObject(dateFin);
			args.addObject(dateFin);
			stringQualifier = "contrats.dateDebut <= %@ AND (contrats.dateFin >= %@ OR contrats.dateFin = nil) AND (contrats.dateFinAnticipee >= %@ OR contrats.dateFinAnticipee = nil)";
			qualifierDate = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		} 
		return rechercherIndividusAvecQualifiersDatePourTypePersonnel(editingContext, qualifierDate, nom, prenom,typePersonnel, supprimerDoublons, estNomPatronymique);
	}
	/** recherche les individus ayant des contrats non annules se terminant avant la date fournie (on regarde la date de fin
	 * ou la dat de fin anticipee
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateReference peut etre nulle (date du jour)
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants
	 * @param estNomPatronymique true si la recherche doit etre faite sur le nom patronymique
	 * @return individus trouves (type IndividuIdentite)
	 */
	public static NSArray rechercherIndividusPourTypePersonnelEtContratsAnterieureDate(EOEditingContext editingContext,String nom,String prenom,NSTimestamp dateReference,int typePersonnel,boolean estNomPatronymique) {
		LogManager.logDetail("rechercherIndividusPourTypePersonnelEtContratsAnterieureDate - typePersonnel : " + typePersonnel);
		if (dateReference == null) {
			// pour ramener à une date sans prendre en compte les heures
			dateReference =  DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		}
		NSMutableArray args = new NSMutableArray(dateReference);
		args.addObject(dateReference);
		EOQualifier qualifierDate = EOQualifier.qualifierWithQualifierFormat("contrats.dateFin < %@ AND contrats.dateFinAnticipee < %@", args);
		return rechercherIndividusAvecQualifiersDatePourTypePersonnel(editingContext, qualifierDate, nom, prenom, typePersonnel, true, estNomPatronymique);
	} 
	/** recherche les individus avec des contrats non annules commencant apres la date de reference
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut &ecirc;tre nul
	 * @param prenom sur lequel restreindre la recherche - peut &ecirc;tre nul
	 * @param dateReference date apres laquelle le contrat commence (si nulle date du jour)
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @return individus trouves (type IndividuIdentite)
	 */
	public static NSArray rechercherIndividusAvecContratsFuturPourTypePersonnel(EOEditingContext editingContext,String nom,String prenom,NSTimestamp dateReference,int typePersonnel,boolean estNomPatronymique) {
		LogManager.logDetail("rechercherIndividusAvecContratsFuturPourTypePersonnel - typePersonnel : " + typePersonnel);
		if (dateReference == null) {
			// pour ramener à une date sans prendre en compte les heures
			dateReference =  DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		}
		EOQualifier qualifierDate = EOQualifier.qualifierWithQualifierFormat("contrats.dateDebut > %@", new NSArray(dateReference));
		return rechercherIndividusAvecQualifiersDatePourTypePersonnel(editingContext, qualifierDate, nom, prenom, typePersonnel, true, estNomPatronymique);
	}
	/** retourne true si un individu a un contrat en cours pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param dateDebut	
	 * @param dateFin	peut etre nul
	 */
	public static boolean aContratToutTypeEnCoursSurPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		return rechercherTousContratsPourIndividuEtPeriode(editingContext,individu,dateDebut,dateFin).count() > 0;
	}
	/** retourne true si un individu a un contrat en cours pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param dateDebut	
	 * @param dateFin	peut etre nul
	 */
	public static boolean aContratEnCoursSurPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		return rechercherContratsPourIndividuEtPeriode(editingContext,individu,dateDebut,dateFin,false).count() > 0;
	}
	/** retourne le qualifier pour determiner les contrats sur une periode (on prend en compte la date de fin anticipee
	 * @param individu
	 * @param debutPeriode peut etre nulle
	 * @param finPeriode	 peut etre nulle
	 */
	public static EOQualifier qualifierPourPeriode(EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY+"=%@", new NSArray(individu)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY+"<>%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));

			if (finPeriode == null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + " = nil",null));
			} else {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_ANTICIPEE_KEY + " = NIL OR dateFinAnticipee >= %@",new NSArray(debutPeriode)));
			}

			return new EOAndQualifier(qualifiers);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * ferme tout ce qui se rattache aux contrats d'un individu valides a cette date et supprime tout ce qui commence au-dela
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @param depart
	 */
	public static void fermerContrats(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date, EODepart depart) {

		NSArray<EOContrat> objets = rechercherContratsPourIndividuEtPeriode(editingContext,individu,date,date,true);

		// fermer le contrat
		for (EOContrat myContrat : objets) {

			myContrat.setDateFinAnticipee(date);
			if (myContrat.dateFin() == null)
				myContrat.setDateFin(date);
			NSArray<EOContratAvenant> avenants = myContrat.avenantsPourPeriode(date,null);
			for (EOContratAvenant myAvenant : avenants) {
				myAvenant.fermerAvenant(date);
			}
		}

		// supprimer les informations postérieures à cette date
		for (Enumeration<EOContrat> e2 = rechercherContratsIndividuPosterieursADate(editingContext,individu,date).objectEnumerator();e2.hasMoreElements();) {
			// annuler le contrat
			EOContrat contrat = e2.nextElement();
			contrat.invalider();
			// annuler les détails
			for (java.util.Enumeration<EOContratAvenant> e3 = contrat.avenants().objectEnumerator();e3.hasMoreElements();) {
				EOContratAvenant avenant = e3.nextElement();
				avenant.invalider();
			}
		}
	}

	// méthodes privées
	private static String andQualifier(String qualifier) {
		if (qualifier.length() > 0) {
			qualifier = qualifier + " AND ";
		}
		return qualifier;
	}
	private static EOQualifier qualifierPourNom(String relation,String nom,String prenom,boolean estNomPatronymique) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (relation.length() > 0) {
			relation = relation + ".";
		}
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "temValide = 'O'", null));
		if (nom != null) {
			if (estNomPatronymique) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "nomUsuel like '*'",null));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "nomPatronymique caseInsensitiveLike %@", new NSArray(nom + "*")));
			} else {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "nomUsuel caseInsensitiveLike %@",new NSArray(nom + "*")));
			}
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "nomUsuel like '*'",null));
		}
		if (prenom != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "prenom caseInsensitiveLike %@",new NSArray(prenom + "*")));
		}
		return new EOAndQualifier(qualifiers);
	}
	
	/**
	 * 
	 * @param relation
	 * @param typePersonnel
	 * @return
	 */
	private static EOQualifier qualifierPourTypePersonnel(String relation, int typePersonnel) {
		String stringQualifier = "";
		if (relation.length() > 0) {
			relation = relation + ".";
		}
		if (typePersonnel == ManGUEConstantes.HEBERGE) {
			return null;
//			stringQualifier = relation + "toTypeContratTravail." + EOTypeContratTravail.TEM_INVITE_ASSOCIE_KEY + " = 'O'";
		} else {
			
			stringQualifier = relation + "toTypeContratTravail." + EOTypeContratTravail.TEM_POUR_TITULAIRE_KEY + " != 'O'";

			if (typePersonnel == ManGUEConstantes.ENSEIGNANT) {
				stringQualifier = andQualifier(stringQualifier) +  relation + AVENANTS_KEY + "." + EOContratAvenant.TO_GRADE_KEY + " != NIL  AND " +
						relation + "avenants.toGrade.cGrade != '" + EOGrade.CODE_SANS_GRADE + "' AND " + relation +
						"avenants.toGrade.toCorps.toTypePopulation." + EOTypeContratTravail.TEM_ENSEIGNANT_KEY + " = 'O'" ;
			} else if (typePersonnel == ManGUEConstantes.NON_ENSEIGNANT) {
				stringQualifier = andQualifier(stringQualifier) + relation + "avenants.toGrade != NIL AND " + relation + 
						"avenants.toGrade.cGrade != '" + EOGrade.CODE_SANS_GRADE + "' AND " + relation +
						"avenants.toGrade.toCorps.toTypePopulation." + EOTypeContratTravail.TEM_ENSEIGNANT_KEY + " = 'N'" ;
			}
		}
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier,null);
	}

	/**
	 * 
	 * @param relation
	 * @param typePersonnel
	 * @return
	 */
	private static EOQualifier autreQualifierPourTypePersonnel(String relation, int typePersonnel) {
		String stringQualifier = "";
		if (relation.length() > 0) {
			relation = relation + ".";
		}
		if (typePersonnel == ManGUEConstantes.VACATAIRE || typePersonnel == ManGUEConstantes.HEBERGE) {
			return null;
		} else {

			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
			
			if (typePersonnel == ManGUEConstantes.ENSEIGNANT) {
				andQualifiers.addObject(CocktailFinder.getQualifierEqual(relation+TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.TEM_ENSEIGNANT_KEY, "O"));
			} else if (typePersonnel == ManGUEConstantes.NON_ENSEIGNANT) {
				andQualifiers.addObject(CocktailFinder.getQualifierEqual(relation+TO_TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.TEM_ENSEIGNANT_KEY, "N"));
			}
			return EOQualifier.qualifierWithQualifierFormat(stringQualifier,null);
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param qualifierDate
	 * @param nom
	 * @param prenom
	 * @param typePersonnel
	 * @param supprimerDoublons
	 * @param estNomPatronymique
	 * @return
	 */
	private static NSArray rechercherIndividusAvecQualifiersDatePourTypePersonnel(EOEditingContext editingContext,EOQualifier qualifierDate,String nom,String prenom,int typePersonnel,boolean supprimerDoublons,boolean estNomPatronymique) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (qualifierDate != null) { 
			qualifiers.addObject(qualifierDate);
		}
		// tous les contrats non annulés
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("contrats.temAnnulation != 'O'",null));
		// avec les noms et prénoms recherchés
		qualifiers.addObject(qualifierPourNom("",nom, prenom, estNomPatronymique));

		NSMutableArray individus = new NSMutableArray(rechercherIndividusAvecQualifierDatePourTypePersonnel(editingContext, qualifierDate, nom, prenom, typePersonnel, supprimerDoublons, estNomPatronymique));
		// 08/10/2010 - Modification de la recherche des contrats non-enseignants : on recherche tous les avenants avec un grade et on ne retient
		// que les non-enseignants puis on recherche tous les contrats de type non-enseignant et on exclut tous les individus ayant un grade
		// d'enseignant dans leur avenant
		NSArray individusEnseignants = new NSArray();
		if (typePersonnel == ManGUEConstantes.NON_ENSEIGNANT) {
			individusEnseignants = rechercherIndividusAvecQualifierDatePourTypePersonnel(editingContext, qualifierDate, nom, prenom, ManGUEConstantes.ENSEIGNANT, supprimerDoublons, estNomPatronymique);
		}
		// On est obligé de séparer les requêtes pour déterminer les enseignants
		EOQualifier autreQualifier = autreQualifierPourTypePersonnel("contrats",typePersonnel);
		if (autreQualifier != null) {
			qualifiers.addObject(autreQualifier);
			EOQualifier qualifier = new EOAndQualifier(qualifiers);
			LogManager.logDetail("rechercherIndividusAvecQualifierDatePourTypePersonnel - autre qualifier : " + qualifier);
			NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey("noIndividu",EOSortOrdering.CompareAscending));
			EOFetchSpecification myFetch = new EOFetchSpecification(EOIndividuIdentite.ENTITY_NAME, qualifier, sorts);
			myFetch.setUsesDistinct(supprimerDoublons);
			myFetch.setRefreshesRefetchedObjects(true);
			NSArray<EOIndividuIdentite> result = editingContext.objectsWithFetchSpecification(myFetch);
			for (EOIndividuIdentite individu : result) {
				if (individus.containsObject(individu) == false && (typePersonnel != ManGUEConstantes.NON_ENSEIGNANT || individusEnseignants.containsObject(individu) == false)) {
					individus.addObject(individu);
				}
			}
			EOSortOrdering.sortArrayUsingKeyOrderArray(individus, sorts);
		}
		return individus;
	}


	/**
	 * 
	 * @param editingContext
	 * @param qualifierDate
	 * @param nom
	 * @param prenom
	 * @param typePersonnel
	 * @param supprimerDoublons
	 * @param estNomPatronymique
	 * @return
	 */
	private static NSArray rechercherIndividusAvecQualifierDatePourTypePersonnel(EOEditingContext editingContext,EOQualifier qualifierDate,String nom,String prenom,int typePersonnel,boolean supprimerDoublons,boolean estNomPatronymique) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (qualifierDate != null) { 
			qualifiers.addObject(qualifierDate);
		}
		// tous les contrats non annulés
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("contrats.temAnnulation != 'O'",null));
		// avec les noms et prénoms recherchés
		qualifiers.addObject(qualifierPourNom("",nom, prenom, estNomPatronymique));
		// en fonction du type de personnel
		NSMutableArray allQualifiers = new NSMutableArray(qualifiers);
		if (typePersonnel != ManGUEConstantes.TOUT_EMPLOYE) {
			allQualifiers.addObject(qualifierPourTypePersonnel("contrats",typePersonnel));
		}
		EOQualifier qualifier = new EOAndQualifier(allQualifiers);
		LogManager.logDetail("rechercherIndividusAvecQualifierDatePourTypePersonnel - qualifier : " + qualifier);
		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey("noIndividu",EOSortOrdering.CompareAscending));
		EOFetchSpecification myFetch = new EOFetchSpecification(EOIndividuIdentite.ENTITY_NAME,qualifier,sorts);
		myFetch.setUsesDistinct(supprimerDoublons);
		myFetch.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**
	 * Crée un avenant du contrat
	 */
	public void creationAvenantContrat() {
		EOContratAvenant.creer(this.editingContext(), this);
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierIndividu(EOIndividu individu) {
		return  EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray<EOIndividu>(individu));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierValide() {
		return  EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY + "=%@", new NSArray<String>("N"));
	}
}

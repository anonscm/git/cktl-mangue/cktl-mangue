// _EOHistoPromotion.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOHistoPromotion.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOHistoPromotion extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "HistoPromotion";
	public static final String ENTITY_TABLE_NAME = "MANGUE.HISTO_PROMOTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "hproOrdre";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String HPRO_ANNEE_KEY = "hproAnnee";
	public static final String HPRO_BAREME_KEY = "hproBareme";
	public static final String HPRO_CLASSEMENT_KEY = "hproClassement";
	public static final String HPRO_DUREE_CATEG_SERV_PUBLICS_KEY = "hproDureeCategServPublics";
	public static final String HPRO_DUREE_CORPS_KEY = "hproDureeCorps";
	public static final String HPRO_DUREE_ECHELON_KEY = "hproDureeEchelon";
	public static final String HPRO_DUREE_GRADE_KEY = "hproDureeGrade";
	public static final String HPRO_DUREE_SERV_EFFECTIFS_KEY = "hproDureeServEffectifs";
	public static final String HPRO_DUREE_SERV_PUBLIC_KEY = "hproDureeServPublic";
	public static final String HPRO_ECHELON_KEY = "hproEchelon";
	public static final String HPRO_REMPLI_KEY = "hproRempli";
	public static final String HPRO_STATUT_KEY = "hproStatut";

// Attributs non visibles
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String PARP_ORDRE_KEY = "parpOrdre";
	public static final String HPRO_ORDRE_KEY = "hproOrdre";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String HPRO_ANNEE_COLKEY = "HPRO_ANNEE";
	public static final String HPRO_BAREME_COLKEY = "HPRO_BAREME";
	public static final String HPRO_CLASSEMENT_COLKEY = "HPRO_CLASSEMENT";
	public static final String HPRO_DUREE_CATEG_SERV_PUBLICS_COLKEY = "HPRO_DUREE_CATEG_SERV_PUBLICS";
	public static final String HPRO_DUREE_CORPS_COLKEY = "HPRO_DUREE_CORPS";
	public static final String HPRO_DUREE_ECHELON_COLKEY = "HPRO_DUREE_ECHELON";
	public static final String HPRO_DUREE_GRADE_COLKEY = "HPRO_DUREE_GRADE";
	public static final String HPRO_DUREE_SERV_EFFECTIFS_COLKEY = "HPRO_DUREE_SERV_EFFECTIFS";
	public static final String HPRO_DUREE_SERV_PUBLIC_COLKEY = "HPRO_DUREE_SERV_PUBLICS";
	public static final String HPRO_ECHELON_COLKEY = "HPRO_ECHELON";
	public static final String HPRO_REMPLI_COLKEY = "HPRO_REMPLI";
	public static final String HPRO_STATUT_COLKEY = "HPRO_STATUT";

	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String PARP_ORDRE_COLKEY = "PARP_ORDRE";
	public static final String HPRO_ORDRE_COLKEY = "HPRO_ORDRE";


	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String PARAM_PROMOTION_KEY = "paramPromotion";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer hproAnnee() {
    return (Integer) storedValueForKey(HPRO_ANNEE_KEY);
  }

  public void setHproAnnee(Integer value) {
    takeStoredValueForKey(value, HPRO_ANNEE_KEY);
  }

  public java.math.BigDecimal hproBareme() {
    return (java.math.BigDecimal) storedValueForKey(HPRO_BAREME_KEY);
  }

  public void setHproBareme(java.math.BigDecimal value) {
    takeStoredValueForKey(value, HPRO_BAREME_KEY);
  }

  public Integer hproClassement() {
    return (Integer) storedValueForKey(HPRO_CLASSEMENT_KEY);
  }

  public void setHproClassement(Integer value) {
    takeStoredValueForKey(value, HPRO_CLASSEMENT_KEY);
  }

  public String hproDureeCategServPublics() {
    return (String) storedValueForKey(HPRO_DUREE_CATEG_SERV_PUBLICS_KEY);
  }

  public void setHproDureeCategServPublics(String value) {
    takeStoredValueForKey(value, HPRO_DUREE_CATEG_SERV_PUBLICS_KEY);
  }

  public String hproDureeCorps() {
    return (String) storedValueForKey(HPRO_DUREE_CORPS_KEY);
  }

  public void setHproDureeCorps(String value) {
    takeStoredValueForKey(value, HPRO_DUREE_CORPS_KEY);
  }

  public String hproDureeEchelon() {
    return (String) storedValueForKey(HPRO_DUREE_ECHELON_KEY);
  }

  public void setHproDureeEchelon(String value) {
    takeStoredValueForKey(value, HPRO_DUREE_ECHELON_KEY);
  }

  public String hproDureeGrade() {
    return (String) storedValueForKey(HPRO_DUREE_GRADE_KEY);
  }

  public void setHproDureeGrade(String value) {
    takeStoredValueForKey(value, HPRO_DUREE_GRADE_KEY);
  }

  public String hproDureeServEffectifs() {
    return (String) storedValueForKey(HPRO_DUREE_SERV_EFFECTIFS_KEY);
  }

  public void setHproDureeServEffectifs(String value) {
    takeStoredValueForKey(value, HPRO_DUREE_SERV_EFFECTIFS_KEY);
  }

  public String hproDureeServPublic() {
    return (String) storedValueForKey(HPRO_DUREE_SERV_PUBLIC_KEY);
  }

  public void setHproDureeServPublic(String value) {
    takeStoredValueForKey(value, HPRO_DUREE_SERV_PUBLIC_KEY);
  }

  public String hproEchelon() {
    return (String) storedValueForKey(HPRO_ECHELON_KEY);
  }

  public void setHproEchelon(String value) {
    takeStoredValueForKey(value, HPRO_ECHELON_KEY);
  }

  public String hproRempli() {
    return (String) storedValueForKey(HPRO_REMPLI_KEY);
  }

  public void setHproRempli(String value) {
    takeStoredValueForKey(value, HPRO_REMPLI_KEY);
  }

  public String hproStatut() {
    return (String) storedValueForKey(HPRO_STATUT_KEY);
  }

  public void setHproStatut(String value) {
    takeStoredValueForKey(value, HPRO_STATUT_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.EOParamPromotion paramPromotion() {
    return (org.cocktail.mangue.modele.mangue.EOParamPromotion)storedValueForKey(PARAM_PROMOTION_KEY);
  }

  public void setParamPromotionRelationship(org.cocktail.mangue.modele.mangue.EOParamPromotion value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.EOParamPromotion oldValue = paramPromotion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PARAM_PROMOTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PARAM_PROMOTION_KEY);
    }
  }
  

/**
 * Créer une instance de EOHistoPromotion avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOHistoPromotion createEOHistoPromotion(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, Integer hproAnnee
, String hproRempli
, String hproStatut
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu, org.cocktail.mangue.modele.mangue.EOParamPromotion paramPromotion			) {
    EOHistoPromotion eo = (EOHistoPromotion) createAndInsertInstance(editingContext, _EOHistoPromotion.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setHproAnnee(hproAnnee);
		eo.setHproRempli(hproRempli);
		eo.setHproStatut(hproStatut);
    eo.setIndividuRelationship(individu);
    eo.setParamPromotionRelationship(paramPromotion);
    return eo;
  }

  
	  public EOHistoPromotion localInstanceIn(EOEditingContext editingContext) {
	  		return (EOHistoPromotion)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOHistoPromotion creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOHistoPromotion creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOHistoPromotion object = (EOHistoPromotion)createAndInsertInstance(editingContext, _EOHistoPromotion.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOHistoPromotion localInstanceIn(EOEditingContext editingContext, EOHistoPromotion eo) {
    EOHistoPromotion localInstance = (eo == null) ? null : (EOHistoPromotion)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOHistoPromotion#localInstanceIn a la place.
   */
	public static EOHistoPromotion localInstanceOf(EOEditingContext editingContext, EOHistoPromotion eo) {
		return EOHistoPromotion.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray prefetchs) {
		  return fetchAll(editingContext, qualifier, null, false, prefetchs);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOHistoPromotion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOHistoPromotion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOHistoPromotion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOHistoPromotion)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOHistoPromotion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOHistoPromotion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOHistoPromotion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOHistoPromotion)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOHistoPromotion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOHistoPromotion eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOHistoPromotion ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOHistoPromotion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

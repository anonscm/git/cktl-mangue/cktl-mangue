// EODif.java
// Created on Fri Mar 14 10:43:01 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import java.math.BigDecimal;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.modalites.EOMotifTempsPartiel;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.PeriodeAvecQuotite;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Dif
 * Les dates de debut et fin representent la periode sur laquelle les jours de cif ont ete cumules
 * Regles de validation<BR>
 * Le dif est defini pour chaque annee posterieure ou egale a 2007.<BR>
 * Le credit et le debit doivent avoir des valeurs positives ou nulles <BR>
 * Le debit est calcule automatiquement et represente la somme de tous les debits de details
 * pour l'annee en cours<BR>
 * Validite des dates<BR>
 * Il doit y avoir un contrat de remuneration principale ou un segment au debut ou &agrave; la fin de l'annee precedente<BR>
 * Annee de demarrage posterieure ou egale a 2007<BR>
 * Si l'agent a un dif l'ann&eacute;e precedente, le dif commence au 01/01<BR>
 * Le credit cumule est egal a la somme des credits legaux &agrave; partir de
 * l'annee de reference<BR>
 * Le credit doit etre superieur ou egal au debit sauf &grave; partir de 2009 ou
 * il peut etre inferieur ou egal au minimum entre le plafond annuel et le double du credit <BR>
 */

public class EODif extends _EODif {

	public static final EOSortOrdering SORT_NOM = new EOSortOrdering(EODif.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY,EOSortOrdering.CompareAscending);
	public static final NSArray<EOSortOrdering> SORT_ARRAY_NOM = new NSArray<EOSortOrdering>(SORT_NOM);

	public static final String BALANCE_KEY = "balance";

	public final static int NB_HEURES_2007 = 10;
	public final static int NB_HEURES_ANNUEL = 20;
	public final static int VALEUR_PLAFOND = NB_HEURES_ANNUEL * 6;
	public static int TITULAIRES = 0,CONTRACTUELS = 1,TOUT_TYPE = 2;
	/** Notification envoyee par les objets pour communiquer des messages pendant la preparation des difs */
	public final static String NOTIFICATION_MESSAGE_PREPARATION = "NotificationMessage";
	public EODif() {
		super();
	}


	/**
	 * Creation d'un nouvel objet EODif
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EODif creer(EOEditingContext ec, EOIndividu individu) {

		EODif newObject = (EODif) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setDifDebitCumule(new Integer(0));
		newObject.setDifDebitAnnuel(new Integer(0));
		newObject.setDifCreditCumule(new Integer(0));
		newObject.setDifCreditAnnuel(new Integer(0));
		return newObject;

	}


	// Méthodes ajoutées
	public void init() {
		setDifDebitCumule(new Integer(0));
		setDifDebitAnnuel(new Integer(0));
		setDifCreditCumule(new Integer(0));
		setDifCreditAnnuel(new Integer(0));
	}

	public Integer balance() {
		return new Integer(difCreditCumule().intValue() - difDebitCumule().intValue());
	}

	/** Initialise les dates ainsi que le credit<BR>
	 * L'annee de calcul est l'annee suivant le debut du dif.<BR>
	 * Le credit est egal a la somme du credit de l'annee precedente +
	 * le nombre d'heures legal (proratise en fonction de la duree du segment de carriere ou
	 * du contrat sur l'annee ainsi que de la quotite de temps partiel)<BR>
	 *
	 */
	public void initAvecDates(NSTimestamp dateDebut, NSTimestamp dateFin) {
		setDateDebut(dateDebut);
		setDateFin(dateFin);
		int anneeCalcul = DateCtrl.getYear(dateDebut()) + 1;
		// L'année de calcul est l'année correspondant à l'année du dif + 1
		setAnneeCalcul(new Integer(anneeCalcul));
		if (anneeCalcul < ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF) {
			setDifCreditCumule(new Integer(0));
		} else {	// Les droits sont crédités au 1er janvier de l'année suivante
			if (individu() != null) {
				calculerCredit();
			}
		}
	}

	/**
	 * 
	 */
	public void calculerCredit() {

		float total = 0;

		BigDecimal creditAnnuel = new BigDecimal(0);
		BigDecimal creditCumule = new BigDecimal(0);

		LogManager.logDetail("Montant du crédit précédent " + total);
		NSArray periodesAvecQuotite = preparerPeriodesOrdonnees();
		if (periodesAvecQuotite.count() > 0) {
			LogManager.logDetail("Nombre de périodes trouvées " + periodesAvecQuotite.count());
			// Les temps partiels ne peuvent être à cheval sur une carrière et un contrat
			// Les contrats ne peuvent être à cheval sur les carrières, on peut donc avancer période par période
			PeriodeAvecQuotite periodeReference = (PeriodeAvecQuotite)periodesAvecQuotite.get(0);
			NSTimestamp dateDebutReference = periodeReference.dateDebut();
			// Si le début de la période de référence est postérieur à la date début, prendre comme date début
			// le début de la période de référence
			if (DateCtrl.isAfter(dateDebutReference, dateDebut())) {
				setDateDebut(dateDebutReference);
			}
			for (int i = 1; i < periodesAvecQuotite.count();i++) {
				PeriodeAvecQuotite periodeCourante = (PeriodeAvecQuotite)periodesAvecQuotite.get(i);
				LogManager.logDetail("Période référence - date début : " + DateCtrl.dateToString(dateDebutReference) + ", date fin : " + DateCtrl.dateToString(periodeReference.dateFin()));
				LogManager.logDetail("Période courante - date début : " + DateCtrl.dateToString(periodeCourante.dateDebut()) + ", date fin : " + DateCtrl.dateToString(periodeCourante.dateFin()));
				if (DateCtrl.isBefore(periodeCourante.dateDebut(), periodeReference.dateFin())) {
					// Chevauchement, calculer le prorata sur la partie sans chevauchement
					if (DateCtrl.isSameDay(periodeCourante.dateDebut(), dateDebutReference) == false) { // il ne s'agit pas d'un chevauchement complet
						int nbJours = DateCtrl.nbJoursEntre(dateDebutReference, DateCtrl.jourPrecedent(periodeCourante.dateDebut()), true);
						float prorata = calculerProrata(nbJours, periodeReference.quotite().floatValue() / 100);
						LogManager.logDetail("Calcul prorata :" + prorata + " pour nbJours " + nbJours + " et quotité " + periodeReference.quotite());
						total += prorata;
					}
					if (DateCtrl.isBefore(periodeCourante.dateFin(), periodeReference.dateFin())) {
						// la période courante est entièrement incluse dans la période de référence, on peut donc ajouter son
						// prorata puisqu'on sait qu'il ne pourra pas y avoir des chevauchements avec une autre période
						int nbJours = DateCtrl.nbJoursEntre(periodeCourante.dateDebut(), periodeCourante.dateFin(), true);
						float prorata = calculerProrata(nbJours, periodeCourante.quotite().floatValue() / 100);
						LogManager.logDetail("Calcul prorata :" + prorata + " pour nbJours " + nbJours + " et quotité " + periodeCourante.quotite());
						total += prorata;
						dateDebutReference = DateCtrl.jourSuivant(periodeCourante.dateFin());
					} else {
						// La période courante se termine au-delà de la période, elle devient la période de référence
						periodeReference = periodeCourante;
						dateDebutReference = periodeReference.dateDebut();
					}
				} else {
					// La période courante ne chevauche pas la période de référence, on applique la quotité
					// sur l'intégralité de la période puis on change de période de référence
					int nbJours = DateCtrl.nbJoursEntre(dateDebutReference, periodeReference.dateFin(), true);
					total += calculerProrata(nbJours, periodeReference.quotite().floatValue() / 100);
					periodeReference = periodeCourante;
					dateDebutReference = periodeReference.dateDebut();
				}
			}
			// Prendre en compte la dernière période de référence
			LogManager.logDetail("Période référence - date début : " + DateCtrl.dateToString(dateDebutReference) + ", date fin : " + DateCtrl.dateToString(periodeReference.dateFin()));
			int nbJours = DateCtrl.nbJoursEntre(dateDebutReference, periodeReference.dateFin(), true);
			float prorata = calculerProrata(nbJours, periodeReference.quotite().floatValue() / 100);
			LogManager.logDetail("Calcul prorata :" + prorata + " pour nbJours " + nbJours + " et quotité " + periodeReference.quotite());
			total += prorata;


			//			int nbJoursCredit = new BigDecimal(total).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
			//			if (nbJoursCredit > VALEUR_PLAFOND) {	
			//				nbJoursCredit = VALEUR_PLAFOND;
			//			}
			//			LogManager.logDetail("Total cumulé " + nbJoursCredit);

			// Récupérer le crédit de l'annee precedente
			EODif difPrecedent = rechercherDifAnneePrecedente();
			if (difPrecedent != null) {
				creditCumule = new BigDecimal(difPrecedent.difCreditCumule()); 
			}

			creditAnnuel = new BigDecimal(total).setScale(0, BigDecimal.ROUND_HALF_UP);
			setDifCreditAnnuel(creditAnnuel.intValue());
			setDifCreditCumule((creditAnnuel.add(creditCumule)).intValue());
			if (difCreditCumule() > VALEUR_PLAFOND)
				setDifCreditCumule(VALEUR_PLAFOND);

			// Si la dernière période de référence se termine avant la date fin, prendre comme date fin, la période de date fin
			if (periodeReference.dateFin() != null && DateCtrl.isBefore(periodeReference.dateFin(), dateFin())) {
				setDateFin(periodeReference.dateFin());
			}
		}
		else {
			setDifCreditCumule(new Integer(0));
		}
	}

	/**
	 * 
	 */
	public void calculerDebit() {
		int debitCumule = 0;
		EODif difPrecedent = rechercherDifAnneePrecedente();
		if (difPrecedent != null) {
			debitCumule += difPrecedent.difDebitCumule().intValue();
		}

		int debitAnnuel = 0;
		for (EODifDetail detail : difDetails()) {
			if (detail.difDetailDebit() != null) {
				debitAnnuel += detail.difDetailDebit().intValue();
			}
		}
		setDifDebitAnnuel(new Integer(debitAnnuel));
		setDifDebitCumule(new Integer(debitCumule + debitAnnuel));
	}

	/**
	 * 
	 */
	public void supprimerRelations() {
		supprimerDetails();
		super.supprimerRelations();
	}


	/**
	 * 
	 */
	public void supprimerDetails() {
		for (EODifDetail detail : difDetails()) {
			detail.setDifRelationship(null);
			editingContext().deleteObject(detail);
		}
	}

	/**
	 * 
	 */
	public void validateForSave() {

		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début doit être définie !");
		} else if (dateFin() != null && dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException("DIF\nLa date de fin doit être postérieure à la date de début (FIN : " + dateFinFormatee() + " , DEBUT : " + dateDebutFormatee() + ")");
		}
		if (individu() == null) {
			throw new ValidationException("Vous devez fournir un individu !");
		}

		int anneeCalcul = DateCtrl.getYear(dateDebut()) + 1;
		if (anneeCalcul < ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF) {
			throw new NSValidation.ValidationException("Le droit individuel à la formation n'est valable qu'à partir de 2007 !");
		}
		int anneeDebut = DateCtrl.getYear(dateDebut());
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("Les difs sont définis sur l'année civile");
		} else {
			int anneeFin = DateCtrl.getYear(dateFin());
			if (anneeDebut != anneeFin) {
				throw new NSValidation.ValidationException("Les difs sont définis sur l'année civile");
			}
		}

		if (difCreditCumule().intValue() < 0) {
			throw new NSValidation.ValidationException("Le crédit doit avoir une valeur positive ou nulle !");
		}
		if (difDebitCumule().intValue() < 0) {
			throw new NSValidation.ValidationException("Le débit doit avoir une valeur positive ou nulle !");
		}
		// Le crédit est supérieur ou égal au débit
		if (anneeCalcul < ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF + 1) {
			if (difDebitCumule().intValue() > difCreditCumule().intValue()) {
				throw new NSValidation.ValidationException("Le débit doit être inférieur ou égal au crédit");
			}
		} else {
			int max = difCreditCumule().intValue() * 2;
			if (max > VALEUR_PLAFOND) {
				max = VALEUR_PLAFOND;
			}
			if (difDebitCumule().intValue() > max) {
				throw new NSValidation.ValidationException("Le débit cumulé ne peut être supérieur à " + max + " heures");
			}
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	/**
	 * Retourne le dif commencant l'annee precedente, null si non trouve ou si le dif est anterieur a 2008
	 * @return
	 */
	public EODif rechercherDifAnneePrecedente() {

		try {
			// on prend comme année de calcul, l'année suivant la date début dif
			// il faut donc bien prendre comme année de calcul précédente l'année de la date début du dif
			int anneeCalculPrecedente = DateCtrl.getYear(dateDebut());
			if (anneeCalculPrecedente < ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF) {
				return null;
			}

			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu())));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ANNEE_CALCUL_KEY + " = %@", new NSArray(new Integer(anneeCalculPrecedente))));

			return fetchFirstByQualifier(editingContext(), new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	private NSArray preparerPeriodesOrdonnees() {
		NSTimestamp debutPeriode = dateDebut();
		NSTimestamp finPeriode = dateFin();
		NSMutableArray periodesAvecQuotite = new NSMutableArray();
		// Rechercher les segments de carrière pour l'année
		NSArray<EOCarriere> segments = EOCarriere.rechercherCarrieresPourIndividuAnterieursADate(editingContext(), individu(), finPeriode);
		segments = EOSortOrdering.sortedArrayUsingKeyOrderArray(segments, new NSArray(EOSortOrdering.sortOrderingWithKey(EOCarriere.DATE_DEBUT_KEY, EOSortOrdering.CompareAscending)));

		// Eliminer tous les segments qui se terminent avant le début de la période
		for (EOCarriere carriere : segments) {
			if (carriere.dateFin() != null && DateCtrl.isBefore(carriere.dateFin(), debutPeriode)) {
			} else {
				ajouterPeriode(periodesAvecQuotite, carriere.dateDebut(),carriere.dateFin(),new Integer(100));
			}
		}
		// Rechercher aussi les contrats principaux sur l'année en cours (pour les agents passant de carrière à contrat)
		// Eliminer tous les contrats qui se terminent avant le début de la période
		NSArray<EOContratAvenant> avenants = EOContratAvenant.rechercherAvenantsRemunerationPrincipaleAnterieursADate(editingContext(), individu(),finPeriode);
		avenants = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenants, new NSArray(EOSortOrdering.sortOrderingWithKey(EOContratAvenant.DATE_DEBUT_KEY, EOSortOrdering.CompareAscending)));
		for (EOContratAvenant avenant : avenants) {
			if (avenant.dateFin() != null && DateCtrl.isBefore(avenant.dateFin(), debutPeriode)) {
			} else {
				ajouterPeriode(periodesAvecQuotite, avenant.dateDebut(), avenant.dateFin(), avenant.quotite());
			}
		}
		// Il se peut qu'il s'agisse d'un contrat sans avenant
		if (avenants.count() == 0) {
			// Rechercher les contrats et mettre une quotité 100%
			NSArray<EOContrat> contrats = EOContrat.rechercherContratsNonInvitesIndividuAnterieursADate(editingContext(), individu(),finPeriode);
			contrats = EOSortOrdering.sortedArrayUsingKeyOrderArray(contrats, new NSArray(EOSortOrdering.sortOrderingWithKey(EOContrat.DATE_DEBUT_KEY, EOSortOrdering.CompareAscending)));
			// Eliminer tous les contrats qui se terminent avant le début de la période
			for (EOContrat contrat : contrats) {
				if (contrat.dateFin() != null && DateCtrl.isBefore(contrat.dateFin(), debutPeriode)) {
				} else {
					ajouterPeriode(periodesAvecQuotite, contrat.dateDebut(),contrat.dateFin(),new Integer(100));
				}
			}
		}

		// Eliminer tous les temps partiels qui se terminent avant le début de la période
		NSArray<EOTempsPartiel> tempsPartiels = EOTempsPartiel.rechercherTempsPartielPourIndividuEtPeriode(editingContext(), individu(),dateDebut(),dateFin());
		tempsPartiels = EOSortOrdering.sortedArrayUsingKeyOrderArray(tempsPartiels, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
		for (EOTempsPartiel tempsPartiel : tempsPartiels) {
			if (tempsPartiel.dateFin() != null && DateCtrl.isBefore(tempsPartiel.dateFin(), debutPeriode)) {
			} else {
				// Ne prendre en compte que les temps partiels qui sont sur autorisation car la quotité doit être appliquée
				if (tempsPartiel.motif().code().equals(EOMotifTempsPartiel.SUR_AUTORISATION)) {
					ajouterPeriode(periodesAvecQuotite, tempsPartiel.dateDebut(),tempsPartiel.dateFin(),tempsPartiel.quotite());
				}
			}
		}
		// Toutes les périodes sont créées, on les trie par ordre croissant puison applique la proratisation 
		// en fonction de la quotité et de la durée de la période, on prend comme base une base 30 jours
		EOSortOrdering.sortArrayUsingKeyOrderArray(periodesAvecQuotite, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
		return periodesAvecQuotite;
	}

	/**
	 * 
	 * @param periodesPourQuotite
	 * @param debutPeriode
	 * @param finPeriode
	 * @param quotite
	 */
	private void ajouterPeriode(NSMutableArray periodesPourQuotite, NSTimestamp debutPeriode, NSTimestamp finPeriode, Number quotite) {
		NSTimestamp dateDebut = dateDebut();
		NSTimestamp dateFin = dateFin();
		if (DateCtrl.isAfter(debutPeriode,dateDebut)) {
			dateDebut = debutPeriode;
		}
		if (finPeriode != null && DateCtrl.isBefore(finPeriode,dateFin)) {
			dateFin = finPeriode;
		}
		LogManager.logDetail("Ajout d'une période du " + DateCtrl.dateToString(dateDebut) + " au " + DateCtrl.dateToString(dateFin));
		periodesPourQuotite.addObject(new PeriodeAvecQuotite(dateDebut,dateFin,quotite));
	}

	/**
	 * 
	 * @param nbJours
	 * @param quotite
	 * @return
	 */
	private float calculerProrata(int nbJours,float quotite) {
		int nbJoursComptables = nbJours;
		try {
			nbJoursComptables = DateCtrl.dureeComptable(dateDebut(), DateCtrl.dateAvecAjoutJours(dateDebut(),nbJours), true);
		} catch (Exception e) {}
		int nbHeuresDif = NB_HEURES_ANNUEL;
		if (DateCtrl.getYear(dateDebut()) == (ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF - 1)) {
			nbHeuresDif = NB_HEURES_2007;
		}
		float prorata = ((float)(nbJoursComptables * nbHeuresDif)) / DateCtrl.NB_JOURS_COMPTABLES_ANNUELS ;
		return prorata * quotite;
	}

	/** Retourne les difs des individus dont le nom et le prenom commencent par les valeurs 
	 * passees en parametre, pour une anne de calcul
	 * @param annee annee de calcul
	 * @param nom nom a rechercher - peut etre nul
	 * @param prenom prenoma a rechercher - peut etre nul
	 */
	public static NSArray<EODif> rechercherDifsPourAnneeNomEtPrenom(EOEditingContext editingContext, int annee,String nom,String prenom,boolean estNomPatronymique) {
		if (annee < ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF) {
			return new NSArray();
		}
		NSMutableArray args = new NSMutableArray(new Integer(annee));
		String stringQualifier = "anneeCalcul = %@";
		if (nom != null) {
			args.addObject(nom + "*");
			if (estNomPatronymique) {
				stringQualifier += " AND individu.nomPatronymique caseInsensitiveLike %@ AND individu.nomUsuel like '*'";
			} else {
				stringQualifier += " AND individu.nomUsuel caseinsensitivelike %@";
			}
		}
		if (prenom != null) {
			args.addObject(prenom + "*");
			stringQualifier += " AND individu.prenom caseinsensitivelike %@";
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		LogManager.logDetail("Recherche DIF qualifier " + qualifier);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);

	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param annee
	 * @return
	 */
	public static EODif findForAnneeEtAgent(EOEditingContext editingContext, EOIndividu individu, Integer annee) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
		qualifiers.addObject(CocktailFinder.getQualifierEqual(ANNEE_CALCUL_KEY, annee));

		return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
	}

	/** Retourne les difs pour une annee */
	public static NSArray<EODif> rechercherDifsPourAnnee(EOEditingContext editingContext, int annee) {
		return rechercherDifsPourAnneeNomEtPrenom(editingContext, annee, null, null,false);
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EODif> findForIndividu(EOEditingContext ec, EOIndividu individu) {
		try {
			return fetchAll(ec, CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EODif> findDifsASupprimer(EOEditingContext edc) {

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(DIF_CREDIT_ANNUEL_KEY + "=%@", new NSArray(new Integer(0)));
		return fetchAll(edc, qualifier, null);
	}

	/** Recherche les difs pour une annee, un type de personnel et un type de population, un nom et un prenom */
	public static NSArray<EODif> rechercherDifsPourAnneeTypePopulationTypePersonnel(EOEditingContext editingContext, int annee,String nom, String prenom,int typePopulation,int typePersonnel,boolean estNomPatronymique) {
		// Commencer par rechercher tous les difs d'une année	
		NSArray difs = (NSArray)rechercherDifsPourAnneeNomEtPrenom(editingContext, annee,nom,prenom,estNomPatronymique);
		if (typePopulation == TOUT_TYPE && typePersonnel == TOUT_TYPE) {
			return difs;
		}
		NSMutableArray resultat = new NSMutableArray();
		NSArray individus = rechercherIndividusAptesPourDif(editingContext, annee, typePopulation, typePersonnel);
		for (java.util.Enumeration<EODif> e = difs.objectEnumerator();e.hasMoreElements();) {
			EODif dif = e.nextElement();
			if (individus.containsObject(dif.individu())) {
				resultat.addObject(dif);
			}
		}
		return resultat;
	}

	/**
	 * Prepare les difs d'une annee pour les agents ayant un certain type de personnel (tous, titulaire, contractuel), type de population (tous, enseignants, non-enseignants)
	 * et les insere dans l'editing context. Si le dif existe deja, il n'est pas recree.
	 * Fournit des messages au cours du traitement en postant des notifications
	 * @param editingContext
	 * @param annee
	 * @param typePopulation
	 * @param typePersonnel
	 * @return
	 */
	public static int preparerDifsPourAnneeTypePopulationEtTypePersonnel(EOEditingContext edc,int annee,int typePopulation, int typePersonnel) {

		if (annee < ManGUEConstantes.ANNEE_DEMARRAGE_CALCUL_DIF) {
			return 0;
		}
		// Commencer par rechercher tous les difs d'une année	
		NSArray<EODif> individusAvecDifs = (NSArray<EODif>)rechercherDifsPourAnnee(edc, annee).valueForKey(INDIVIDU_KEY);
		NSArray<EOIndividu> individus = rechercherIndividusAptesPourDif(edc,annee,typePopulation,typePersonnel);

		NSMutableArray individusTraites = new NSMutableArray();
		NSTimestamp debutPeriode = DateCtrl.stringToDate("01/01/" + (annee-1)), finPeriode = DateCtrl.stringToDate("31/12/" + (annee-1));
		int total = individus.count();
		NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_MESSAGE_PREPARATION, "0/" + ((total / 10) * 10));	// on les affiche par 10
		int nbIndividu = 0,nbDif = 0;
		for (EOIndividu individu : individus) {
			nbIndividu++;
			// Ne créer le dif que si l'individu n'a pas été traité et que le dif n'existe pas
			if (individusTraites.containsObject(individu) == false) {
				if (individusAvecDifs.containsObject(individu) == false) {
					LogManager.logDetail("Creation Dif pour " + individu.identite());
					EODif dif = new EODif();
					edc.insertObject(dif);
					dif.initAvecIndividu(individu);
					dif.initAvecDates(debutPeriode,finPeriode);
					NSTimestamp today = new NSTimestamp();
					dif.setDCreation(today);
					dif.setDModification(today);
					nbDif++;
				}
				individusTraites.addObject(individu);
			}
			if (nbIndividu % 10 == 0) {
				NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_MESSAGE_PREPARATION,"" + nbIndividu + "/" + total);
			}
		}
		return nbDif;
	}

	/**
	 * 
	 * @param editingContext
	 * @param annee
	 * @param typePopulation
	 * @param typePersonnel
	 * @return
	 */
	public static NSArray<EOIndividu> rechercherIndividusAptesPourDif(EOEditingContext editingContext,Integer annee, int typePopulation, int typePersonnel) {

		NSMutableArray<EOIndividu> individus = new NSMutableArray<EOIndividu>();
		NSTimestamp debutPeriode = DateCtrl.stringToDate("01/01/" + (annee-1));
		NSTimestamp finPeriode = DateCtrl.stringToDate("31/12/" + (annee-1));

		typePersonnel = 0;

		// Rechercher les éléments de carrière valides au début de la période
		NSArray<EOElementCarriere> elements = EOElementCarriere.rechercherElementsPourPeriodeEtTypePersonnel(editingContext, null, null, debutPeriode, debutPeriode, typePersonnel, true, false);
		// N'ajouter que ceux qui ont bien une carrière sur la période car dans certains cas (historique Gepeto), 
		// carrière et éléments de carrière sont décalés (par exemple, les éléments de carrière n'ont pas de fin)
		for (EOElementCarriere element : elements) {
			NSTimestamp dateFinCarriere = element.toCarriere().dateFin();
			if (dateFinCarriere == null || DateCtrl.isAfterEq(dateFinCarriere,debutPeriode) && individus.containsObject(element.toIndividu()) == false) {
				individus.addObject(element.toIndividu());
			}
		}
		// Rechercher ceux valides à la fin de la période et ajouter ceux qui n'ont pas encore été retenus
		elements = EOElementCarriere.rechercherElementsPourPeriodeEtTypePersonnel(editingContext, null, null, finPeriode, finPeriode, typePersonnel, true, false);
		for (EOElementCarriere element : elements) {
			NSTimestamp dateFinCarriere = element.toCarriere().dateFin();
			if (dateFinCarriere == null || DateCtrl.isAfterEq(dateFinCarriere,finPeriode) && individus.containsObject(element.toIndividu()) == false) {
				individus.addObject(element.toIndividu());
			}
		}

		NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourPeriodeEtTypePersonnel(editingContext, null, null, debutPeriode, debutPeriode, typePersonnel, true, false);

		ajouterIndividusPourContratsPrincipaux(individus,contrats);
		contrats = EOContrat.rechercherContratsPourPeriodeEtTypePersonnel(editingContext, null, null, finPeriode, finPeriode, typePersonnel, true, false);
		ajouterIndividusPourContratsPrincipaux(individus,contrats);

		EOSortOrdering.sortArrayUsingKeyOrderArray(individus, new NSArray(EOSortOrdering.sortOrderingWithKey(EOIndividu.NO_INDIVIDU_KEY, EOSortOrdering.CompareAscending)));
		return individus;
	}


	// Ne prendre en compte que les contrats de rémunération principale mais pas les invités
	private static void ajouterIndividusPourContratsPrincipaux(NSMutableArray individus,NSArray<EOContrat> contrats) {
		for (EOContrat contrat : contrats) {
			if (individus.containsObject(contrat.toIndividu()) == false &&
					contrat.toTypeContratTravail().estInvite() == false) {
				individus.addObject(contrat.toIndividu());
			}
		}
	}

	// Ne prendre en compte que les contrats de rémunération principale mais pas les invités
	private static boolean aContratsPrincipaux(NSArray<EOContrat> contrats) {
		for (EOContrat contrat : contrats) {
			if (contrat.toTypeContratTravail().estInvite() == false) {
				return true;
			}
		}
		return false;
	}

}

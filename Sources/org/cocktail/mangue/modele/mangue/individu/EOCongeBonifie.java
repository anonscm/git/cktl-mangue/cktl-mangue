/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/** 	
* Regles de validation :<BR>
* La date debut et la date de fin  doivent etre fournies<BR>
* la date de debut doit etre anterieure a celle de fin<BR> 
* Verifie la compatibilite avec les conges et les modalites de service <BR> 
* Verifie que l'individu peut beneficier d'une conge bonifie (fonctionnaire titulaire ou stagiaire).<BR>
* Le conge bonifie doit reposer sur une position d'activite ou de detachement dans l'etablissement.<BR>
* @author christine
*/
public class EOCongeBonifie extends _EOCongeBonifie {
	public static int DUREE_MAX = 65;		// nombre de jours maximum
	public static int DUREE_ENTRE_DEUX_CONGES = 36;	// nombre de mois entre deux congés, le congé précédent étant inclus
	private static String METROPOLE_VERS_DOM = "M";
	private static String DOM_VERS_METROPOLE = "D";
	 public EOCongeBonifie() {
		 super();
	 }

	public String typeEvenement() {
		return "CBONIF";
	}
	public boolean estDeMetroVersDom() {
		return temMetroDom() != null && temMetroDom().equals(METROPOLE_VERS_DOM);
	}
	public void setEstDeMetroVersDom(boolean aBool) {
		if (aBool) {
			setTemMetroDom(METROPOLE_VERS_DOM);
		} else {
			setTemMetroDom(DOM_VERS_METROPOLE);
		}
	}
	public String deMetroVersDom() {
		if (estDeMetroVersDom()) {
			return CocktailConstantes.VRAI;
		} else {
			return CocktailConstantes.FAUX;
		}
	}
	//  méthodes protégées
	protected void init() {
		setTemValide("O");
		setTemMetroDom(METROPOLE_VERS_DOM);
	}
	 public void validateForSave() throws NSValidation.ValidationException {
 		super.validateForSave();
 		if (dateFin() == null) {
 			throw new NSValidation.ValidationException("Vous devez fournir une date de fin");
 		}
	    	NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),individu(),dateDebut(),dateFin());
	    	// vérifie si fonctionnaire titulaire, non en stage
	    if (carrieres == null || carrieres.count() == 0) {
	        	throw new NSValidation.ValidationException("Il doit y avoir au moins un segment de carrière défini pour cette période");
	    } else {
	     	EOCarriere carriereTrouvee = null;
	     	boolean enActivite = false;
	     	for (java.util.Enumeration<EOCarriere> e = carrieres.objectEnumerator();e.hasMoreElements();) {
     			EOCarriere carriere = e.nextElement();
     			if (carriere.toTypePopulation().estFonctionnaire() && individu().estTitulaire()) {
     				carriereTrouvee = carriere;
     				NSArray changements = carriere.changementsPositionPourPeriode(dateDebut(),dateFin());
     				if (changements != null) {
	     				for (java.util.Enumeration<EOChangementPosition> e1 = changements.objectEnumerator();e1.hasMoreElements();) {
	     					EOChangementPosition changement = e1.nextElement();
	     					// La période de maintien en fonction doit reposer sur une position d'activité ou de détachement avec carrière d'accueil
	     					if (!changement.toPosition().estEnActiviteDansEtablissement() && (!changement.toPosition().estUnDetachement() || (changement.estDetachementSortant()))) {
	 	    						enActivite = false;
	     					} else {
	     						enActivite = true;
	     					}
	     				}
	     				if (enActivite) {
	     					break;
	     				}
     				}
     			}
     		}
     		if (carriereTrouvee == null) {
     			throw new NSValidation.ValidationException("Les congés bonifiés ne sont valides que pour les fonctionnaires titulaires ou stagiaires");
     		}
     		if (!enActivite) {
     			throw new NSValidation.ValidationException("Un congé bonifié doit reposer sur une position d'activité ou de détachement avec carrière d'accueil");
			}
	    }
	 }
}

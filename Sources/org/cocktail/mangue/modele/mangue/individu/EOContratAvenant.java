//EOContratAvenant.java
//Created on Mon Jul 25 12:16:29 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeUniteTemps;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.IIndividuAvecDuree;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOTypeContratGrades;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * @author christine<BR>
 *
 * Regles de validation des details de contrat : <BR>
 *	un detail doit avoir :
 *	<UL>une date de debut</UL>
 *	<UL>une quotite non nulle</UL>
 *	<UL>un grade si le type de contrat l'exige</UL>
 * La date de debut ne peut etre posterieure a la date de fin<BR>
 * La date de debut ne peut etre anterieure a la date de debut du contrat<BR>
 * La date de fin d'un detail ne peut etre posterieure a celle du contrat<BR>
 * Si un contrat a une date fin, le detail doit l'avoir aussi<BR>
 * Le temoin indiquant si les cotisations ont ete completements payees est obligatoire<BR>
 * La quotite totale d'un detail et de celles des autres contrats en cours ne peut depasser 100% (on ne prend en compte que les remunerations principales)<BR>
 * Les longueurs des strings sont verifiees<BR>
 * L'indice du contrat est une value numerique<BR>>
 * La quotite des avenants sur une periode est superieure ou egale a la quotite d'occupation<BR>
 * Supprime les baps ou baps corps si ils ne respectent pas les dates de validite de ces nomenclatures<BR>
 * Met a jour les caracteristiques de l'individu (indQualite) si le contrat est un contrat de remuneration principale<BR><BR>
 * Note : indiceContrat represente l'indice brut (pour Papaye)
 **/

public class EOContratAvenant extends _EOContratAvenant implements IIndividuAvecDuree {

	/** types de montant */

	public static final String DUREE_TOTALE_KEY = "dureeTotale";
	private NSArray<EOOccupation> occupationsPourAvenant;
	private NSArray<EOAffectation> affectationsPourAvenant;
	private String activitesPourAvenant;
	private String noArretePourEdition;

	public EOContratAvenant() {
		super();
	}

	public Number dureeTotale() {

		if (typeDuree() != null && typeDuree().estDureeEnHeures())
			return ctraDuree();

		return null;
	}

	public String dateValidationFormatee() {
		return SuperFinder.dateFormatee(this,D_VAL_CONTRAT_AV_KEY);
	}
	public void setDateValidationFormatee(String uneDate) {
		if (uneDate == null) {
			setDValContratAv(null);
		} else {
			SuperFinder.setDateFormatee(this,D_VAL_CONTRAT_AV_KEY,uneDate);
		}
	}
	public String dateArreteFormatee() {
		return SuperFinder.dateFormatee(this,DATE_ARRETE_KEY);
	}
	public void setDateArreteFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,DATE_ARRETE_KEY,uneDate);
	}
	public boolean estArreteSigne() {
		return temArreteSigne() != null && temArreteSigne().equals(CocktailConstantes.VRAI);
	}
	public void setEstArreteSigne(boolean aBool) {
		if (aBool) {
			setTemArreteSigne(CocktailConstantes.VRAI);
		} else {
			setTemArreteSigne(CocktailConstantes.FAUX);
		}
	}
	public boolean estAnnule() {
		return temAnnulation() != null && temAnnulation().equals(CocktailConstantes.VRAI);
	}
	public void setEstAnnule(boolean aBool) {
		if (aBool) {
			setTemAnnulation(CocktailConstantes.VRAI);
		} else {
			setTemAnnulation(CocktailConstantes.FAUX);
		}
	}

	/**
	 * 
	 * @return
	 */
	public boolean aDispenseDea() {
		return cDea() != null && cDea().equals(CocktailConstantes.VRAI);
	}
	public void setADispenseDea(boolean aBool) {
		if (aBool) {
			setCDea(CocktailConstantes.VRAI);
		} else {
			setCDea(CocktailConstantes.FAUX);
		}
	}

	public boolean estTempsComplet() {
		return ctraTypeTemps().equals("C");
	}
	public boolean estTempsIncomplet() {
		return ctraTypeTemps().equals("I");
	}
	public boolean estTempsPartiel() {
		return ctraTypeTemps().equals("P");
	}
	public boolean estServiceValide() {
		return dValContratAv() != null;
	}
	public boolean pcAcquitees() {
		return ctraPcAcquitees() != null && ctraPcAcquitees().equals(CocktailConstantes.VRAI);
	}
	public void setEstPcAcquitees(boolean aBool) {
		if (aBool) {
			setCtraPcAcquitees(CocktailConstantes.VRAI);
		} else {
			setCtraPcAcquitees(CocktailConstantes.FAUX);
		}
	}
	public boolean paiementPonctuel() {
		return temPaiementPonctuel() != null && temPaiementPonctuel().equals(CocktailConstantes.VRAI);
	}
	public void setPaiementPonctuel(boolean aBool) {
		if (aBool) {
			setTemPaiementPonctuel(CocktailConstantes.VRAI);
		} else {
			setTemPaiementPonctuel(CocktailConstantes.FAUX);
		}
	}
	public String noArretePourEdition() {
		if (noArretePourEdition == null) {
			// On construit le N° arrêté à partir de la globalID
			EOGlobalID gid = editingContext().globalIDForObject(this);
			if (gid instanceof  EOKeyGlobalID) {
				return "" + DateCtrl.getYear(new NSTimestamp()) + "/" + ((EOKeyGlobalID)gid).keyValues()[0];
			} else {	// Ne devrait pas se produire, on a toujours une key global ID
				return "" + DateCtrl.getYear(new NSTimestamp());
			}
		} else {
			return noArretePourEdition;
		}
	}

	/**
	 * 
	 * @param prefixNumeroAuto
	 * @param annee
	 * @return
	 */
	public static String getNumeroArreteAutomatique(EOEditingContext edc, Integer annee) {

		String prefixNumeroAuto = "" + annee + "/" ;

		// Rechercher tous les éléments de carrière de l'année pour déterminer le dernier numéro automatique attribué
		EOQualifier qualifier = SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, DateCtrl.stringToDate("01/01/" + annee), DATE_FIN_KEY,DateCtrl.stringToDate("31/12/" + annee));
		NSArray<EOContratAvenant> avenants = EOContratAvenant.rechercherAvenantsAvecCriteres(edc, qualifier, true, true);
		int valeurIncrementee = 0;
		if (avenants != null && avenants.size() > 0) {
			avenants = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenants, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
			for (EOContratAvenant myAvenant : avenants) {
				String noArrete = myAvenant.noArrete();
				if (noArrete != null && noArrete.indexOf(prefixNumeroAuto) >= 0) {	// Vérifier si cela correspond à un numéro automatique
					String num = noArrete.substring(prefixNumeroAuto.length());
					try {
						int numAuto = new Integer(num).intValue();
						if (numAuto > valeurIncrementee)
							valeurIncrementee = numAuto;

					} catch (Exception exc) {
						// Le numéro d'arrêté n'est pas automatique
					}
				}
			}
		}
		return prefixNumeroAuto + StringCtrl.get0Int((valeurIncrementee + 1), 2);
	}

	
	public String indiceBrut() {
		if (indiceContrat() == null) {
			return "";
		}
		NSArray<EOIndice> indices = EOIndice.indicesPourIndiceMajoreEtDate(editingContext(), new Integer(indiceContrat()), dateDebut());
		if (indices.size() == 0) {
			return "";
		} else {
			indices = EOSortOrdering.sortedArrayUsingKeyOrderArray(indices, EOIndice.SORT_ARRAY_BRUT_DESC);
			return indices.get(0).cIndiceBrut();
		}
	}
	/**
	 * 
	 * @return
	 */
	public String indiceMajore() {
		return indiceContrat();
	}

	public void setNoArretePourEdition(String aStr) {
		this.noArretePourEdition = aStr;
	}
	public NSArray<EOVisa> visas() {
		return EOVisa.rechercherVisaPourTypeContrat(editingContext(), contrat().toTypeContratTravail().code());
	}

	/**
	 * Creation d'un avenant
	 * 
	 * @param ec
	 * @param contrat
	 * @return
	 */
	public static EOContratAvenant creer(EOEditingContext ec, EOContrat contrat) {

		NSArray avenants = findForContrat(ec, contrat);

		EOContratAvenant newObject = (EOContratAvenant) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.setContratRelationship(contrat);

		if (contrat.toTypeContratTravail().requiertGrade()) {
			NSArray<EOTypeContratGrades> grades = EOTypeContratGrades.rechercherPourTypeContrat(ec, contrat.toTypeContratTravail());

			//S'il n'y a qu'un seul grade pour le type de contrat, on met à jour l'avenant
			if (grades.count() == 1) {
				newObject.setToGradeRelationship(grades.get(0).toGrade());
			}
		}


		//Met par défaut le nombre d'heures du potentiel brut
		if (contrat.toTypeContratTravail().potentielBrut() > Double.valueOf(0.0)) {
			newObject.setCtraDuree(contrat.toTypeContratTravail().potentielBrut());
			newObject.setTypeDureeRelationship((EOTypeUniteTemps)NomenclatureFinder.findForCode(ec, EOTypeUniteTemps.ENTITY_NAME, EOTypeUniteTemps.TYPE_CODE_HEURES));
		}

		if (avenants.count() > 0) {
		} else {
			newObject.setDateDebut(contrat.dateDebut());
			newObject.setDateFin(contrat.dateFin());
		}

		newObject.setQuotite(new Double(100));
		newObject.setTemPaiementPonctuel(CocktailConstantes.FAUX);
		newObject.setCtraQuotiteCotisation(new Double(100));

		newObject.setTemArreteSigne(CocktailConstantes.FAUX);
		newObject.setTemGestEtab(CocktailConstantes.FAUX);
		newObject.setDRefCtrAvenant(new NSTimestamp());
		newObject.setTemAnnulation(CocktailConstantes.FAUX);

		// DUREES VALIDEES
		newObject.setCtraPcAcquitees(CocktailConstantes.FAUX);
		newObject.setCtraTypeTemps("C");	// Temps complet par defaut
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		return newObject;

	}

	/**
	 * Renouvellement d'un avenant
	 * 
	 * @param ec
	 * @param ancienAvenant
	 * @return
	 */
	public static EOContratAvenant renouveler(EOEditingContext ec, EOContratAvenant ancienAvenant) {
		EOContratAvenant newObject = (EOContratAvenant) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.takeValuesFromDictionary(ancienAvenant.snapshot());

		newObject.setCtraDureeValideeAnnees(null);
		newObject.setCtraDureeValideeMois(null);
		newObject.setCtraDureeValideeJours(null);
		newObject.setDValContratAv(null);
		newObject.setCtraTypeTemps("C");
		newObject.setCtraQuotiteCotisation(null);
		newObject.setDateDebut(DateCtrl.jourSuivant(ancienAvenant.dateFin()));
		newObject.setDateFin(null);

		return newObject;

	}

	/**
	 * 
	 * @throws com.webobjects.foundation.NSValidation.ValidationException
	 */
	public void validateObjectForSave () throws com.webobjects.foundation.NSValidation.ValidationException {

		if (estAnnule()) {
			return;		// On ne fait pas les validations, elles ont été faites quand l'avenant était valide
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "AVENANT"));
		}

		if (toGrade() == null && contrat().toTypeContratTravail() != null && contrat().toTypeContratTravail().requiertGrade()) {
			throw new NSValidation.ValidationException("Le type de contrat rend le grade obligatoire");
		}

		if (quotite() == null || quotite().doubleValue() == 0 || quotite().doubleValue() < 0 || quotite().doubleValue() > 100.00 ) {
			throw new NSValidation.ValidationException("La quotité doit être renseignée et comprise entre 0 et 100% !"); 
		}
		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException(String.format(MangueMessagesErreur.ERREUR_NUMERO_ARRETE, "AVENANT", "20"));
		}
		if (indiceContrat() != null) {
			try {
				int num = new Integer(indiceContrat()).intValue();
				if (num < 0) {
					throw new NSValidation.ValidationException("L'indice majoré est une valeur positive");
				}
			} catch (Exception e) {
				throw new NSValidation.ValidationException("L'indice majoré est une valeur numérique");
			}
		}

		String message = validationsCir();

		if (message != null && message.length() > 0) {
			throw new NSValidation.ValidationException(message);
		}

		// Vérifier si la nomenclature est correcte par rapport aux dates
		if (toBap() != null) {
			if (DateCtrl.isAfterEq(dateDebut(),DateCtrl.stringToDate(ManGUEConstantes.DEBUT_VALIDITE_REFERENS))) {
				// La bap n'est pas légitime passée cette date, la supprimer
				setToBapRelationship(null);
			}
		}
		if (dateFin() != null) {
			// vérifier que la date de fin est postérieure à la date de début
			if (DateCtrl.isBefore(dateFin(),dateDebut())) {
				throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "AVENANT", DateCtrl.dateToString(dateFin()), DateCtrl.dateToString(dateDebut())));
			}
			//  vérifier la cohérence de la date de fin par rapport au contrat
			if (contrat() != null && (DateCtrl.isBefore(dateFin(),contrat().dateDebut()) || 
					(contrat().dateFin() != null && DateCtrl.isAfter(dateFin(),contrat().dateFin())))) {
				throw new NSValidation.ValidationException("La date de fin d'un détail (" + DateCtrl.dateToString(dateFin()) + ") doit être comprise entre la date de début et la date de fin de contrat (" + contrat().dateFin() + ") !");
			}
			if (conditionRecrutement() != null) {
				// vérifier si les durées maximum ne sont pas dépassées
				int dureeMax = conditionRecrutement().dureeMaxCr().intValue();		// durée en mois
				NSTimestamp dateFinMax = DateCtrl.dateAvecAjoutMois(dateDebut(),dureeMax);
				if (DateCtrl.isAfter(dateFin(),dateFinMax)) {
					throw new NSValidation.ValidationException("La condition de recrutement impose que le détail dure au maximum " + dureeMax + " mois");
				}
			}
		} else {
			if (contrat() != null && contrat().dateFin() != null) {
				// si le contrat a une date de fin, le détail doit aussi en avoir une
				throw new NSValidation.ValidationException("Le contrat a une date de fin, le détail doit aussi l'avoir");
			}
			if (conditionRecrutement() != null) {
				// les conditions de recrutement imposent des durées limites
				throw new NSValidation.ValidationException("Les conditions de recrutement imposent aux détails des durées maximum");
			}
		}
		// vérifier la cohérence de la date début par rapport au contrat
		if (contrat() != null && (DateCtrl.isBefore(dateDebut(),contrat().dateDebut()) || 
				(contrat().dateFin() != null && DateCtrl.isAfter(dateDebut(),contrat().dateFin())))) {
			throw new NSValidation.ValidationException("La date de début d'un détail doit être comprise entre la date de début et la date de fin de contrat");
		} 
		// pour les contrats de rémunération principale
		if (!estAnnule()) {
			double quotiteTotale = calculerQuotiteTotaleAvecContratOuAvenant(editingContext(), null, this);
			if (quotiteTotale  > 100) {
				throw new NSValidation.ValidationException("Cet agent a déjà d'autres contrats en cours dont la quotité totale est " + quotiteTotale + "%.\nVeuillez modifier la quotité ou les dates de l'avenant");
			}
		}
		// vérifier si l'agent a un DEA ou si dispenseDEA est sélectonné
		if (contrat().toTypeContratTravail().code().equals(EOTypeContratTravail.CODE_ALLOCATAIRE_RECHERCHE)) {
			if (aDispenseDea() == false && !contrat().toIndividu().aDea()) {
				throw new NSValidation.ValidationException("Pour ce type de contrat, cet agent doit avoir un DEA ou une dispense de DEA");
			}
		}

		verifierQuotitesOccupation();
		// Si le détail se termine après la date courante et que c'est un contrat de rémunération principale
		// on met à jour le champ IND_ACTIVITE de l'agent
		// pour ramener à une date sans prendre en compte les heures
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		if (dateFin() == null || DateCtrl.isAfterEq(dateFin(),today)) {
			if (toGrade() != null && !toGrade().estSansGrade()) {
				contrat().toIndividu().setIndActivite(toGrade().llGrade());
			} else {
				contrat().toIndividu().setIndActivite(contrat().toTypeContratTravail().libelleLong());
			}
		}

		// Quotite de cotisation > 50 et < 100
		if (estTempsPartiel()) {
			if (ctraQuotiteCotisation() != null && (ctraQuotiteCotisation().floatValue() < 50 || ctraQuotiteCotisation().floatValue() > 99 ) ) {
				throw new NSValidation.ValidationException("Pour un temps partiel la quotité de cotisation doit être comprise entre 50 et 99% !");				
			}
		}

		// 
		if (montant() == null) 
			setMontant(new BigDecimal(0));
		if (tauxHoraire() == null) 
			setTauxHoraire(new BigDecimal(0));
		if (nbrUnite() == null) 
			setNbrUnite(new BigDecimal(0));

		if (dCreation() == null) {
			setDCreation(new NSTimestamp());
		}
		setDModification(new NSTimestamp());

	}


	/**
	 * 
	 * @return
	 */
	public String validationsCir() {

		// Validation des services valides
		if ((ctraDureeValideeAnnees() != null && ctraDureeValideeAnnees().intValue() > 0) ||
				(ctraDureeValideeMois() != null && ctraDureeValideeMois().intValue() > 0) ||
				(ctraDureeValideeJours() != null && ctraDureeValideeJours().intValue() > 0)) {

			if (ctraDureeValideeMois() != null && (ctraDureeValideeMois().intValue() < 0 || ctraDureeValideeMois().intValue() > 11)) {
				throw new NSValidation.ValidationException("Les mois validés doivent être compris entre 0 et 11 !");
			}
			if (ctraDureeValideeJours() != null && (ctraDureeValideeJours().intValue() < 0 || ctraDureeValideeJours().intValue() > 29)) {
				throw new NSValidation.ValidationException("Les jours validés doivent être compris entre 0 et 29 !");
			}

			if (dValContratAv() == null)
				throw new NSValidation.ValidationException("Vous devez fournir une date de validation des services !");

			int annees = 0,mois = 0, jours = 0;
			if (ctraDureeValideeAnnees() != null && ctraDureeValideeAnnees().intValue() > 0) {
				annees = ctraDureeValideeAnnees().intValue();
			}
			if (ctraDureeValideeMois() != null && ctraDureeValideeMois().intValue() > 0) {
				mois = ctraDureeValideeMois().intValue();
			}
			if (ctraDureeValideeJours() != null && ctraDureeValideeJours().intValue() > 0) {
				jours = ctraDureeValideeJours().intValue();
			}

			int nbJoursValides = (annees * 360) + (mois * 30) + jours;


			int nbJoursCalcules = DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true);

			if (DateCtrl.dateToString(dateDebut(), "%d/%m").equals("01/02") && 
					DateCtrl.dateToString(dateFin(), "%d/%m").equals("28/02"))
				nbJoursCalcules += 2;

			if (DateCtrl.dateToString(dateDebut(), "%d/%m").equals("01/02") && 
					DateCtrl.dateToString(dateFin(), "%d/%m").equals("29/02")) {
				nbJoursCalcules ++;
			}

			// Test de l'année bisextile
			int annee = DateCtrl.getYear(dateDebut());
			boolean anneeBisextile = (annee % 4) == 0;
			if (anneeBisextile) {
				if (DateCtrl.isBefore(dateDebut(), DateCtrl.stringToDate("29/02/"+annee))
						&& DateCtrl.isAfter(dateFin(), DateCtrl.stringToDate("29/02/"+annee)) ) {
					nbJoursCalcules ++;
				}				
			}
			else {
				if (DateCtrl.isBefore(dateDebut(), DateCtrl.stringToDate("28/02/"+annee))
						&& DateCtrl.isAfter(dateFin(), DateCtrl.stringToDate("28/02/"+annee)) ) {
					nbJoursCalcules +=2;
				}								
			}

			if (nbJoursValides > nbJoursCalcules) {
				throw new NSValidation.ValidationException("Le nombre de jours validés ("+nbJoursValides + " Jours) est supérieur au nombre d'années, mois, jours écoulés entre la date début et la date fin ("+nbJoursCalcules+" Jours) !");
			}	
		}


		if (dValContratAv() != null) {
			if (dateFin() == null || DateCtrl.isAfter(dateFin(), dValContratAv())) {
				return "la date de fin de l'avenant doit être saisie et antérieure à la date de validation de service";
			}
		}
		if (ctraQuotiteCotisation() != null) {
			double quotite = ctraQuotiteCotisation().doubleValue();
			if ((quotite != 0 && (quotite < 50.00 || quotite  > 100.00)) || quotite < 0) {
				return "la quotité  de cotisation doit être égale à zéro ou comprise entre 50% et 100%";
			}
		}
		return null;
	}

	/** Rend un detail invalide et supprime les lignes budgetaires associees */
	public void invalider() {
		setTemAnnulation(CocktailConstantes.VRAI);
	}
	/** Modifie la date de fin de l'avenant et des lignes budgetaires associees */
	// 08/02/2010 - suite au transfert des lignes budgétaires dans les avenants
	public void fermerAvenant(NSTimestamp date) {
		setDateFin(date);
	}
	public void preparerEmplois() {
		occupationsPourAvenant = EOOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContext(), contrat().individu(), dateDebut(), dateFin());
	}
	public void preparerAffectations() {
		affectationsPourAvenant = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext(), contrat().individu(), dateDebut(), dateFin());
		// les trier par ordre de quotité croissante
		affectationsPourAvenant = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectationsPourAvenant, EOAffectation.SORT_ARRAY_QUOTITE_DESC);
	}

	public void preparerActivites() {
		NSArray activites = EORepartCtrActivites.rechercherRepartsActivitesPourAvenant(this, true);
		activitesPourAvenant = "";
		if (activites != null && activites.count() > 0) {
			for (java.util.Enumeration<EORepartCtrActivites> e = activites.objectEnumerator();e.hasMoreElements();) {
				EORepartCtrActivites repart = e.nextElement();
				if (activitesPourAvenant.length() > 0) {
					activitesPourAvenant += " / ";
				}
				activitesPourAvenant += repart.activiteTypeContrat().atcoLibelle();
			}
		}
	}

	/**
	 * 
	 */
	public String activitesPourAvenant() {
		return activitesPourAvenant;
	}
	/** Utilisee pour l'impression du contrat de travail. Retourne le lieu d'affectation pour l'affectation ayant la
	 * quotite la plus elevee */
	public String lieuAffectation() {
		if (affectationsPourAvenant == null || affectationsPourAvenant.count() == 0) {
			return "";
		} else {
			return ((EOAffectation)affectationsPourAvenant.objectAtIndex(0)).toStructureUlr().llStructure();
		}
	}

	/** Utilisee pour l'impression du contrat de travail. Retourne le numero de l'emploi associe si ce n'est pas un
	 * rompu de temps partiel ie que l'occupation indique l'individu est titulaire de l'emploi */
	public String numeroEmploi() {
		if (occupationsPourAvenant == null || occupationsPourAvenant.count() == 0) {
			return "";
		}
		for (java.util.Enumeration<EOOccupation> e = occupationsPourAvenant.objectEnumerator();e.hasMoreElements();) {
			EOOccupation occupation = e.nextElement();
			if (occupation.estTitulaireEmploi()) {	// l'individu est titulaire de l'emploi, il ne s'agit pas de rompus
				return occupation.toEmploi().getNoEmploi();
			}
		}
		// Si il y a plusieurs occupations, on ne retourne rien
		if (occupationsPourAvenant.count() == 1) {
			return ((EOOccupation)occupationsPourAvenant.objectAtIndex(0)).toEmploi().getNoEmploi();
		}

		return "";
	}

	/** Utilisee pour l'impression du contrat de travail. Retourne true si l'occupation indique l'individu est titulaire
	de l'emploi */
	public boolean estTitulaireEmploi() {
		if (occupationsPourAvenant == null || occupationsPourAvenant.count() == 0) {
			return false;
		}
		for (java.util.Enumeration<EOOccupation> e = occupationsPourAvenant.objectEnumerator();e.hasMoreElements();) {
			EOOccupation occupation = e.nextElement();
			if (occupation.estTitulaireEmploi()) {	
				return true;
			}
		}
		return false;
	}

	/** Utilisee pour l'impression du contrat de travail. Retourne les programmes associes a l'emploi */
	public String programmeEmploi() {
		if (occupationsPourAvenant == null || occupationsPourAvenant.count() == 0) {
			return "";
		}
		QuotitePourProgramme quotitePourProgramme = new QuotitePourProgramme();
		for (EOOccupation occupation : occupationsPourAvenant) {
			if (occupation.quotite() != null && occupation.toEmploi().toProgramme() != null)
				quotitePourProgramme.addQuotitePourProgramme(occupation.quotite().floatValue(), occupation.toEmploi().toProgramme().cProgramme() + " " + occupation.toEmploi().toProgramme().llProgramme());
		}

		return quotitePourProgramme.toString();
	}

	/** Utilisee pour l'impression du contrat de travail. Retourne les quotites de rompus utilisees */
	public String quotitesFractionnement() {
		if (occupationsPourAvenant == null || occupationsPourAvenant.count() == 0) {
			return "";
		}
		String quotite = "";
		for (EOOccupation occupation : occupationsPourAvenant) {

			if (occupation.estTitulaireEmploi()) {	// l'individu est titulaire de l'emploi, il ne s'agit pas de rompus
				return "";
			}
			if (quotite.length() > 0) {
				quotite = quotite + " - ";
			}
			quotite = quotite + occupation.quotite() + " %";
		}
		return quotite;
	}

	// InterfaceIndividuAvecDuree
	public NSTimestamp debutPeriode() {
		return dateDebut();
	}

	public NSTimestamp finPeriode() {
		return dateFin();
	}
	public EOIndividu individu() {
		return contrat().individu();
	}
	// Méthodes statiques


	public static NSArray rechercherAvenantsPourIndividuAvecServicesValidesAnterieursADate(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateReference) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONTRAT_KEY+"."+EOContrat.TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONTRAT_KEY+"."+EOContrat.TEM_ANNULATION_KEY+ "=%@", new NSArray("N")));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY+ "=%@", new NSArray("N")));

		if (dateReference != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY+ "<=%@", new NSArray(dateReference)));
		}

		NSMutableArray orQualifiers = new NSMutableArray();

		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CTRA_DUREE_VALIDEE_ANNEES_KEY + ">=%@", new NSArray(0)));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CTRA_DUREE_VALIDEE_MOIS_KEY + ">=%@", new NSArray(0)));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CTRA_DUREE_VALIDEE_JOURS_KEY + ">=%@", new NSArray(0)));

		qualifiers.addObject(new EOOrQualifier(orQualifiers));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending)));
	}

	public static EOContratAvenant rechercherAvenantADate(EOEditingContext ec,EOIndividu individu,NSTimestamp dateReference) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONTRAT_KEY+"."+EOContrat.TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULATION_KEY+ "=%@", new NSArray("N")));
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateReference, DATE_FIN_KEY,dateReference));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
	}


	/** recherche les details valides a une certaine date qui correspondent au type enseignant passe en parametre et renvoie un tableau ordonn&eacute; par individu
	 * @param editingContext
	 * @param qualifier
	 * @return tableau des details
	 */
	public static NSArray<EOContratAvenant> rechercherAvenantsPourDateEtTypePersonnel(EOEditingContext editingContext,NSTimestamp date,int typeEnseignant) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (date != null) {
			NSMutableArray args = new NSMutableArray();
			args.addObject(date);
			args.addObject(date);
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + "  <= %@ AND (dateFin >= %@ OR dateFin = nil) AND contrat.toIndividu.temValide = 'O'", args));
			// Ajouter un qualifier pour les dates du contrats
			args.addObject(date);
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("contrat.dateDebut <= %@ AND (contrat.dateFin >= %@ OR contrat.dateFin = nil) AND (contrat.dateFinAnticipee >= %@ OR contrat.dateFinAnticipee = nil) AND contrat.temAnnulation = 'N'", args));
		}

		qualifiers.addObject(qualifierPourTypeEnseignant(typeEnseignant));

		EOQualifier qualifier = new EOAndQualifier(qualifiers);
		LogManager.logDetail("rechercherAvenantsPourDateEtTypePersonnel - qualifier : " + qualifier);
		return rechercherAvenantsAvecCriteres(editingContext, qualifier, true, true);
	}


	public static NSArray<EOContratAvenant> findForIndividu(EOEditingContext ec, EOIndividu individu) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(getQualifierIndividu(individu));
			qualifiers.addObject(getQualifierValide());

			return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOContratAvenant> findForCir(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			if (individu != null) {
				qualifiers.addObject(getQualifierIndividu(individu));				
			}
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONTRAT_KEY+"."+EOContrat.TEM_CIR_KEY + "=%@", new NSArray("O")));
			qualifiers.addObject(getQualifierValide());
			
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin));

			return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @param prefetch
	 * @param shouldRefresh
	 * @return
	 */
	public static NSArray<EOContratAvenant> finForIndividuAndPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		try {

			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONTRAT_KEY + "."+ EOContrat.TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONTRAT_KEY + "."+ EOContrat.TEM_ANNULATION_KEY + " = %@", new NSArray("N")));
			qualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY,debutPeriode , DATE_FIN_KEY ,finPeriode));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);		
		}
		catch (Exception e) {
			return new NSArray<EOContratAvenant>();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param contrat
	 * @return
	 */
	public static NSArray<EOContratAvenant> findForContrat(EOEditingContext ec, EOContrat contrat) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(getQualifierContrat(contrat));
			qualifiers.addObject(getQualifierValide());

			return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOContratAvenant>();
		}

	}

	/** recherche les details selon certains criteres et renvoie un tableau ordonne par individu
	 * @param editingContext
	 * @param qualifier
	 * @param sort true si le tableau doit etre classe
	 * @param refresh true si refresh des details
	 * @return tableau des details
	 */
	public static NSArray rechercherAvenantsAvecCriteres(EOEditingContext edc,EOQualifier qualifier,boolean sort,boolean refresh) {
		NSMutableArray sorts = null;
		if (sort) {
			sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(CONTRAT_KEY + "." + EOContrat.TO_INDIVIDU_KEY + "." + EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
			sorts.addObject(PeriodePourIndividu.SORT_DATE_DEBUT_DESC);
		}
		NSMutableArray qualifiers = new NSMutableArray();
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}
		qualifiers.addObject(getQualifierValide());

		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,sorts);
		myFetch.setRefreshesRefetchedObjects(refresh);
		return edc.objectsWithFetchSpecification(myFetch);
	}


	public static EOContratAvenant rechercherAvenantPourIndividuADate(EOEditingContext edc,EOIndividu individu,NSTimestamp date) {
		
		NSMutableArray andQualifiers = new NSMutableArray();

		andQualifiers.addObject(getQualifierIndividu(individu));
		andQualifiers.addObject(getQualifierValide());
		andQualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, date, DATE_FIN_KEY, date));

		return fetchFirstByQualifier(edc, new EOAndQualifier(andQualifiers));
	}


	/** retourne tous les details de contrat non annules, de remuneration principale d'un individu dont les dates de debut et fin sont &agrave; cheval sur deux dates
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode peut etre nulle
	 * @return details trouves
	 */
	public static NSArray<EOContratAvenant> rechercherAvenantsRemunerationPrincipalePourIndividuEtDates(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		try {
			NSMutableArray andQualifiers = new NSMutableArray();

			andQualifiers.addObject(getQualifierIndividu(individu));
			andQualifiers.addObject(getQualifierValide());
			andQualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode));

			return fetchAll(editingContext, new EOAndQualifier(andQualifiers), null);
		}
		catch (Exception e) {
			return new NSArray<EOContratAvenant>();
		}
	}	

	/** retourne tous les details de contrat non annules, de remuneration principale d'un individu commencant avant la date
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @return details trouves
	 */
	public static NSArray<EOContratAvenant> rechercherAvenantsRemunerationPrincipaleAnterieursADate(EOEditingContext edc,EOIndividu individu,NSTimestamp date) {

		try {
			NSMutableArray andQualifiers = new NSMutableArray();

			andQualifiers.addObject(getQualifierIndividu(individu));
			
			andQualifiers.addObject(getQualifierValide());

			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + "<=%@", new NSArray(date)));

			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONTRAT_KEY + "." + EOContrat.TO_TYPE_CONTRAT_TRAVAIL_KEY + "."+ EOTypeContratTravail.TEM_INVITE_ASSOCIE_KEY + "=%@", new NSArray(CocktailConstantes.FAUX)));

			return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
		}
		catch (Exception e) {
			return new NSArray<EOContratAvenant>();
		}

	}
	/** Calcule la quotite totale de tous les details d'un individu en fonction des dates du contrat ou du detail passe en parametre */
	public static double calculerQuotiteTotaleAvecContratOuAvenant(EOEditingContext edc, EOContrat contrat,EOContratAvenant avenant) {

		if (contrat == null && avenant == null) {
			return 0;
		}
		if (avenant != null) {
			contrat = avenant.contrat();
		} 

		// Vérifier si la quotité est obligatoire et dans ce cas  vérifier la que la quotité des détails ne dépasse pas 100 sur la même période 
		//		EOParamTypeContrat param = EOParamTypeContrat.rechercherParametrePourTypeContrat(editingContext, contrat.toTypeContratTravail());
		//		if (param != null && param.fournirQuotite()) {
		NSTimestamp debutPeriode = null, finPeriode = null;
		double quotiteTotale = 0;
		NSMutableArray avenantsAConsiderer = new NSMutableArray();
		if (avenant != null) {
			contrat = avenant.contrat();
			String texte = "détail du " + avenant.dateDebut();
			if (avenant.dateFin() != null) {
				texte += " au " + avenant.dateFin();
			}
			texte += ", quotité : " + avenant.quotite();
			LogManager.logDetail(texte);
			debutPeriode = avenant.dateDebut();
			finPeriode = avenant.dateFin();
			quotiteTotale = avenant.quotite().doubleValue();
			avenantsAConsiderer.addObject(avenant);
		} else {
			debutPeriode = contrat.dateDebut();
			finPeriode = contrat.dateFin();
		}

		NSArray autresAvenants = EOContratAvenant.rechercherAvenantsRemunerationPrincipalePourIndividuEtDates(edc,contrat.toIndividu(),debutPeriode,finPeriode);
		// Les ranger par ordre croissant pour évaluer la quotité sur les périodes
		autresAvenants = EOSortOrdering.sortedArrayUsingKeyOrderArray(autresAvenants, new NSArray(EOSortOrdering.sortOrderingWithKey(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending)));
		for (java.util.Enumeration<EOContratAvenant> e = autresAvenants.objectEnumerator();e.hasMoreElements();) {
			EOContratAvenant avenantCourant = e.nextElement();
			if (avenantCourant.estAnnule() == false && avenantsAConsiderer.containsObject(avenantCourant) == false) {
				//					param = EOParamTypeContrat.rechercherParametrePourTypeContrat(editingContext, avenantCourant.contrat().toTypeContratTravail());
				//					if (param != null && param.fournirQuotite()) {
				String texte = "détail du " + avenantCourant.dateDebut();
				if (avenantCourant.dateFin() != null) {
					texte += " au " + avenantCourant.dateFin();
				}
				texte += ", quotité : " + avenantCourant.quotite();
				LogManager.logDetail(texte);
				quotiteTotale = quotiteTotale + avenantCourant.quotite().doubleValue();
				LogManager.logDetail("quotité totale " + quotiteTotale);
				//	 Vérifier si l'avenant courant est postérieur aux avenants pris en compte, en quel cas il ne faut
				// plus prendre en compte leur quotité
				if (avenantsAConsiderer.count() > 0) {
					// 26/03/2010 - le remove peut entraîner une exception plus tard, on stocke les avenants à considérer
					// dans un tableau temporaire
					NSMutableArray avenantsGardes = new NSMutableArray();
					for (java.util.Enumeration<EOContratAvenant> e1 = avenantsAConsiderer.objectEnumerator();e1.hasMoreElements();) {
						EOContratAvenant avenantASupprimer = e1.nextElement();
						if (avenantASupprimer.dateFin() != null && DateCtrl.isBefore(avenantASupprimer.dateFin(), avenantCourant.dateDebut())) {
							// L'avenant n'a plus court sur la période en cours d'analyse
							// Enlever sa quotité et le supprimer des avenants pris en compte, les avenants étant classés
							// par date début croissant, on trouvera ensuite toujours des avenants postérieurs
							LogManager.logDetail("Suppression de la quotité du detail du " + avenantASupprimer.dateDebut());
							quotiteTotale = quotiteTotale - avenantASupprimer.quotite().doubleValue();
							LogManager.logDetail("quotité totale après suppression " + quotiteTotale);
						} else {
							avenantsGardes.addObject(avenantASupprimer);
						}
					}
					avenantsAConsiderer = new NSMutableArray(avenantsGardes);
				}
				avenantsAConsiderer.addObject(avenantCourant);
				//}
			}
			//			}
			return quotiteTotale;
		}
		return 0;
	}

	// Méthodes privées
	private void verifierQuotitesOccupation() throws NSValidation.ValidationException {
		NSArray occupations = EOOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContext(),individu(), dateDebut(), dateFin());
		double quotiteTotaleOccupation = 0;
		if (occupations == null || occupations.count() == 0) {
			return;
		}
		// vérifier si la somme des quotités sur la période est valide	
		for (java.util.Enumeration<EOOccupation> e = occupations.objectEnumerator();e.hasMoreElements();) {
			EOOccupation occupation = e.nextElement();
			if (occupation.aPrendreEnComptePourPeriode(dateDebut(),dateFin())) {
				LogManager.logDetail("occupation du " + occupation.dateDebut() + " au " + occupation.dateFin() + ", quotite " + occupation.quotite());
				quotiteTotaleOccupation = quotiteTotaleOccupation + occupation.quotite().doubleValue();
			}
		}
		// Ne pas effectuer les vérifications si il n'y a pas d'occupation
		LogManager.logDetail("quotite occupation totale sans occupation courante " + quotiteTotaleOccupation);
		NSArray avenants = EOContratAvenant.rechercherAvenantsRemunerationPrincipalePourIndividuEtDates(editingContext(), individu(), dateDebut(),dateFin());
		double quotiteAvenant = 0;
		// vérification des quotités par rapport aux quotités des avenants
		if (avenants != null && avenants.count() > 0) {
			for (Enumeration<EOContratAvenant> e1= avenants.objectEnumerator();e1.hasMoreElements();) {
				EOContratAvenant avenant = e1.nextElement();
				if (avenant != this) {
					LogManager.logDetail("avenant du " + avenant.dateDebut() + " au " + avenant.dateFin());
					double quotite = 100;
					if (avenant.quotite() != null) {
						quotite = avenant.quotite().doubleValue();
					}
					quotiteAvenant = quotiteAvenant + quotite;
				}
			}
		}
		// Ajouter la quotité du changement de position
		quotiteAvenant = quotiteAvenant + quotite().doubleValue();
		// Faire des comparaisons de big decimaux pour être sûr de ne pas avoir de problème d'arrondi
		BigDecimal quotiteOccupation = new BigDecimal(quotiteTotaleOccupation).setScale(2,BigDecimal.ROUND_UP);
		BigDecimal quotiteAv =  new BigDecimal(quotiteAvenant).setScale(2,BigDecimal.ROUND_UP);
		LogManager.logDetail("quotite avenants totale : " + quotiteAv);
		LogManager.logDetail("quotite occupation totale : " + quotiteOccupation);
		//		if (quotiteOccupation.compareTo(quotiteAv) > 0) {
		//			EODialogs.runInformationDialog("ATTENTION","La quotité de recrutement de ce contractuel (" + quotiteAv + "%) pendant cette période doit être supérieure ou égale à sa quotité d'occupation (" + quotiteOccupation + "%)");
		//			//throw new NSValidation.ValidationException("La quotité de recrutement de ce contractuel (" + quotiteAv + "%) pendant cette période doit être supérieure ou égale à sa quotité d'occupation (" + quotiteOccupation + "%)");
		//		}

	}


	public static EOQualifier getQualifierContrat(EOContrat contrat) {
		return CocktailFinder.getQualifierEqual(CONTRAT_KEY , contrat);
	}
	public static EOQualifier getQualifierIndividu(EOIndividu individu) {
		return CocktailFinder.getQualifierEqual(CONTRAT_KEY + "." + EOContrat.TO_INDIVIDU_KEY , individu);
	}
	public static EOQualifier getQualifierGrade(EOGrade grade) {
		return CocktailFinder.getQualifierEqual(TO_GRADE_KEY, grade);
	}
	public static EOQualifier getQualifierValide() {
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_ANNULATION_KEY, "N"));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(CONTRAT_KEY + "." + EOContrat.TEM_ANNULATION_KEY, "N"));
		
		return new EOAndQualifier(andQualifiers);
	}

	// Méthodes privées statiques
	private static EOQualifier qualifierPourTypeEnseignant(int typeEnseignant) {
		//05/02/2010 - Modification du critère de recherche des contrats non-vacataires, on se base sur le témoin temRemunerationPrincipale
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("contrat.toTypeContratTravail.temTitulaire != 'O'", null));
		if (typeEnseignant == ManGUEConstantes.ENSEIGNANT) {
			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toGrade != NIL  AND toGrade.cGrade != '" + EOGrade.CODE_SANS_GRADE + "' AND toGrade.toCorps.toTypePopulation.temEnseignant = 'O'",null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("contrat.toTypeContratTravail.temEnseignant = 'O'",null));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		} else if (typeEnseignant == ManGUEConstantes.NON_ENSEIGNANT) {
			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toGrade != NIL  AND toGrade.cGrade != '" + EOGrade.CODE_SANS_GRADE + "' AND toGrade.toCorps.toTypePopulation.temEnseignant = 'N'",null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("contrat.toTypeContratTravail.temEnseignant = 'N'",null));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		EOQualifier qualifier = new EOAndQualifier(qualifiers);

		return qualifier;
	}


	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class QuotitePourProgramme {
		NSMutableDictionary dictQuotites;

		public QuotitePourProgramme() {
			dictQuotites = new NSMutableDictionary();
		}
		public void addQuotitePourProgramme(float quotite,String programme) {
			Float quotiteCourante = (Float)dictQuotites.valueForKey(programme);
			if (quotiteCourante == null) {
				quotiteCourante = new Float(quotite);
			} else {
				quotiteCourante = new Float(quotiteCourante.floatValue() + quotite);
			}
			dictQuotites.takeValueForKey(quotiteCourante, programme);
		}
		
		/**
		 * 
		 */
		public String toString() {
			try {
				String resultat = "";
				// On trie les quotités par ordre décroissant puis on récupère tous les programmes qui ont la quotité courante
				NSMutableArray quotitesTraites = new NSMutableArray();
				NSArray quotites = dictQuotites.allValues().sortedArrayUsingComparator(NSComparator.DescendingNumberComparator);
				for (java.util.Enumeration<Float> e = quotites.objectEnumerator();e.hasMoreElements();) {
					Float quotite = e.nextElement();
					if (quotitesTraites.containsObject(quotite) == false) {
						NSArray keys = dictQuotites.allKeysForObject(quotite);
						keys = keys.sortedArrayUsingComparator(NSComparator.AscendingStringComparator);
						for (java.util.Enumeration e1 = keys.objectEnumerator();e1.hasMoreElements();) {
							NSNumberFormatter formatter = new NSNumberFormatter("0.00");
							formatter.setLocalizesPattern(true);
							resultat = resultat + e1.nextElement() + " (" + formatter.format(quotite) + "%)\n";
						}
						quotitesTraites.addObject(quotite);
					}
				}
				return resultat.substring(0,resultat.length() - 1);	// Pour supprimer le dernier \n
			} catch (Exception exc) {
				return "";
			}
		}
	}

}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOContrat.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOContrat extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Contrat";
	public static final String ENTITY_TABLE_NAME = "MANGUE.CONTRAT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noSeqContrat";

	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_FIN_ANTICIPEE_KEY = "dateFinAnticipee";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String TEM_ANNULATION_KEY = "temAnnulation";
	public static final String TEM_BUDGET_PROPRE_KEY = "temBudgetPropre";
	public static final String TEM_CIR_KEY = "temCIR";
	public static final String TEM_FONCTIONNAIRE_KEY = "temFonctionnaire";
	public static final String TEM_RECHERCHE_KEY = "temRecherche";
	public static final String TEM_SAUVADET_KEY = "temSauvadet";
	public static final String TOF_CODE_KEY = "tofCode";

// Attributs non visibles
	public static final String C_RNE_KEY = "cRne";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String NO_DEPART_KEY = "noDepart";
	public static final String NO_SEQ_CONTRAT_KEY = "noSeqContrat";
	public static final String TRHU_ORDRE_KEY = "trhuOrdre";

//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String DATE_DEBUT_COLKEY = "D_DEB_CONTRAT_TRAV";
	public static final String DATE_FIN_COLKEY = "D_FIN_CONTRAT_TRAV";
	public static final String DATE_FIN_ANTICIPEE_COLKEY = "D_FIN_ANTICIPEE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String TEM_ANNULATION_COLKEY = "TEM_ANNULATION";
	public static final String TEM_BUDGET_PROPRE_COLKEY = "TEM_BUDGET_PROPRE";
	public static final String TEM_CIR_COLKEY = "TEM_CIR";
	public static final String TEM_FONCTIONNAIRE_COLKEY = "TEM_FONCTIONNAIRE";
	public static final String TEM_RECHERCHE_COLKEY = "TEM_RECHERCHE";
	public static final String TEM_SAUVADET_COLKEY = "TEM_SAUVADET";
	public static final String TOF_CODE_COLKEY = "TOF_CODE";

	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_TYPE_CONTRAT_TRAV_COLKEY = "C_TYPE_CONTRAT_TRAV";
	public static final String NO_DEPART_COLKEY = "NO_DEPART";
	public static final String NO_SEQ_CONTRAT_COLKEY = "NO_SEQ_CONTRAT";
	public static final String TRHU_ORDRE_COLKEY = "TRHU_ORDRE";


	// Relationships
	public static final String AVENANTS_KEY = "avenants";
	public static final String DEPART_KEY = "depart";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_ORIGINE_FINANCEMENT_KEY = "toOrigineFinancement";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_TYPE_CONTRAT_TRAVAIL_KEY = "toTypeContratTravail";
	public static final String TYPE_RECRUTEMENT_KEY = "typeRecrutement";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String commentaire() {
    return (String) storedValueForKey(COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    takeStoredValueForKey(value, COMMENTAIRE_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dateFinAnticipee() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_ANTICIPEE_KEY);
  }

  public void setDateFinAnticipee(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_ANTICIPEE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey(NO_DOSSIER_PERS_KEY);
  }

  public void setNoDossierPers(Integer value) {
    takeStoredValueForKey(value, NO_DOSSIER_PERS_KEY);
  }

  public String temAnnulation() {
    return (String) storedValueForKey(TEM_ANNULATION_KEY);
  }

  public void setTemAnnulation(String value) {
    takeStoredValueForKey(value, TEM_ANNULATION_KEY);
  }

  public String temBudgetPropre() {
    return (String) storedValueForKey(TEM_BUDGET_PROPRE_KEY);
  }

  public void setTemBudgetPropre(String value) {
    takeStoredValueForKey(value, TEM_BUDGET_PROPRE_KEY);
  }

  public String temCIR() {
    return (String) storedValueForKey(TEM_CIR_KEY);
  }

  public void setTemCIR(String value) {
    takeStoredValueForKey(value, TEM_CIR_KEY);
  }

  public String temFonctionnaire() {
    return (String) storedValueForKey(TEM_FONCTIONNAIRE_KEY);
  }

  public void setTemFonctionnaire(String value) {
    takeStoredValueForKey(value, TEM_FONCTIONNAIRE_KEY);
  }

  public String temRecherche() {
    return (String) storedValueForKey(TEM_RECHERCHE_KEY);
  }

  public void setTemRecherche(String value) {
    takeStoredValueForKey(value, TEM_RECHERCHE_KEY);
  }

  public String temSauvadet() {
    return (String) storedValueForKey(TEM_SAUVADET_KEY);
  }

  public void setTemSauvadet(String value) {
    takeStoredValueForKey(value, TEM_SAUVADET_KEY);
  }

  public String tofCode() {
    return (String) storedValueForKey(TOF_CODE_KEY);
  }

  public void setTofCode(String value) {
    takeStoredValueForKey(value, TOF_CODE_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EODepart depart() {
    return (org.cocktail.mangue.modele.mangue.individu.EODepart)storedValueForKey(DEPART_KEY);
  }

  public void setDepartRelationship(org.cocktail.mangue.modele.mangue.individu.EODepart value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EODepart oldValue = depart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPART_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPART_KEY);
    }
  }
    
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeOrigineFinancement toOrigineFinancement() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeOrigineFinancement)storedValueForKey(TO_ORIGINE_FINANCEMENT_KEY);
  }

  public void setToOrigineFinancementRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeOrigineFinancement value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeOrigineFinancement oldValue = toOrigineFinancement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORIGINE_FINANCEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORIGINE_FINANCEMENT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EORne toRne() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EORne)storedValueForKey(TO_RNE_KEY);
  }

  public void setToRneRelationship(org.cocktail.mangue.common.modele.nomenclatures.EORne value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail toTypeContratTravail() {
    return (org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail)storedValueForKey(TO_TYPE_CONTRAT_TRAVAIL_KEY);
  }

  public void setToTypeContratTravailRelationship(org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail oldValue = toTypeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CONTRAT_TRAVAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CONTRAT_TRAVAIL_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu typeRecrutement() {
    return (org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu)storedValueForKey(TYPE_RECRUTEMENT_KEY);
  }

  public void setTypeRecrutementRelationship(org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu oldValue = typeRecrutement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_RECRUTEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_RECRUTEMENT_KEY);
    }
  }
  
  public NSArray avenants() {
    return (NSArray)storedValueForKey(AVENANTS_KEY);
  }

  public NSArray avenants(EOQualifier qualifier) {
    return avenants(qualifier, null, false);
  }

  public NSArray avenants(EOQualifier qualifier, boolean fetch) {
    return avenants(qualifier, null, fetch);
  }

  public NSArray avenants(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.EOContratAvenant.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.EOContratAvenant.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = avenants();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAvenantsRelationship(org.cocktail.mangue.modele.mangue.individu.EOContratAvenant object) {
    addObjectToBothSidesOfRelationshipWithKey(object, AVENANTS_KEY);
  }

  public void removeFromAvenantsRelationship(org.cocktail.mangue.modele.mangue.individu.EOContratAvenant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, AVENANTS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOContratAvenant createAvenantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ContratAvenant");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, AVENANTS_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.EOContratAvenant) eo;
  }

  public void deleteAvenantsRelationship(org.cocktail.mangue.modele.mangue.individu.EOContratAvenant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, AVENANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAvenantsRelationships() {
    Enumeration objects = avenants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAvenantsRelationship((org.cocktail.mangue.modele.mangue.individu.EOContratAvenant)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOContrat avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOContrat createEOContrat(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, String temBudgetPropre
, String temCIR
, String temFonctionnaire
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu, org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail toTypeContratTravail			) {
    EOContrat eo = (EOContrat) createAndInsertInstance(editingContext, _EOContrat.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setTemBudgetPropre(temBudgetPropre);
		eo.setTemCIR(temCIR);
		eo.setTemFonctionnaire(temFonctionnaire);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToTypeContratTravailRelationship(toTypeContratTravail);
    return eo;
  }

  
	  public EOContrat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOContrat)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOContrat creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOContrat creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOContrat object = (EOContrat)createAndInsertInstance(editingContext, _EOContrat.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOContrat localInstanceIn(EOEditingContext editingContext, EOContrat eo) {
    EOContrat localInstance = (eo == null) ? null : (EOContrat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOContrat#localInstanceIn a la place.
   */
	public static EOContrat localInstanceOf(EOEditingContext editingContext, EOContrat eo) {
		return EOContrat.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOContrat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOContrat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOContrat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOContrat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOContrat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOContrat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOContrat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOContrat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOContrat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOContrat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

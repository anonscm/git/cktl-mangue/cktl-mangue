/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODif.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.mangue.modele.PeriodePourIndividu;


public abstract class _EODif extends  PeriodePourIndividu {
	public static final String ENTITY_NAME = "Dif";
	public static final String ENTITY_TABLE_NAME = "MANGUE.DIF";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "difOrdre";

	public static final String ANNEE_CALCUL_KEY = "anneeCalcul";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DIF_CREDIT_ANNUEL_KEY = "difCreditAnnuel";
	public static final String DIF_CREDIT_CUMULE_KEY = "difCreditCumule";
	public static final String DIF_DEBIT_ANNUEL_KEY = "difDebitAnnuel";
	public static final String DIF_DEBIT_CUMULE_KEY = "difDebitCumule";
	public static final String D_MODIFICATION_KEY = "dModification";

// Attributs non visibles
	public static final String DIF_ORDRE_KEY = "difOrdre";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";

//Colonnes dans la base de donnees
	public static final String ANNEE_CALCUL_COLKEY = "ANNEE_CALCUL";
	public static final String DATE_DEBUT_COLKEY = "D_DEB_DIF";
	public static final String DATE_FIN_COLKEY = "D_FIN_DIF";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DIF_CREDIT_ANNUEL_COLKEY = "DIF_CREDIT_ANNUEL";
	public static final String DIF_CREDIT_CUMULE_COLKEY = "DIF_CREDIT";
	public static final String DIF_DEBIT_ANNUEL_COLKEY = "DIF_DEBIT_ANNUEL";
	public static final String DIF_DEBIT_CUMULE_COLKEY = "DIF_DEBIT";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";

	public static final String DIF_ORDRE_COLKEY = "DIF_ORDRE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";


	// Relationships
	public static final String DIF_DETAILS_KEY = "difDetails";
	public static final String INDIVIDU_KEY = "individu";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer anneeCalcul() {
    return (Integer) storedValueForKey(ANNEE_CALCUL_KEY);
  }

  public void setAnneeCalcul(Integer value) {
    takeStoredValueForKey(value, ANNEE_CALCUL_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public Integer difCreditAnnuel() {
    return (Integer) storedValueForKey(DIF_CREDIT_ANNUEL_KEY);
  }

  public void setDifCreditAnnuel(Integer value) {
    takeStoredValueForKey(value, DIF_CREDIT_ANNUEL_KEY);
  }

  public Integer difCreditCumule() {
    return (Integer) storedValueForKey(DIF_CREDIT_CUMULE_KEY);
  }

  public void setDifCreditCumule(Integer value) {
    takeStoredValueForKey(value, DIF_CREDIT_CUMULE_KEY);
  }

  public Integer difDebitAnnuel() {
    return (Integer) storedValueForKey(DIF_DEBIT_ANNUEL_KEY);
  }

  public void setDifDebitAnnuel(Integer value) {
    takeStoredValueForKey(value, DIF_DEBIT_ANNUEL_KEY);
  }

  public Integer difDebitCumule() {
    return (Integer) storedValueForKey(DIF_DEBIT_CUMULE_KEY);
  }

  public void setDifDebitCumule(Integer value) {
    takeStoredValueForKey(value, DIF_DEBIT_CUMULE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public NSArray<EODifDetail> difDetails() {
    return (NSArray)storedValueForKey(DIF_DETAILS_KEY);
  }

  public NSArray difDetails(EOQualifier qualifier) {
    return difDetails(qualifier, null, false);
  }

  public NSArray difDetails(EOQualifier qualifier, boolean fetch) {
    return difDetails(qualifier, null, fetch);
  }

  public NSArray difDetails(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.EODifDetail.DIF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.EODifDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = difDetails();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDifDetailsRelationship(org.cocktail.mangue.modele.mangue.individu.EODifDetail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DIF_DETAILS_KEY);
  }

  public void removeFromDifDetailsRelationship(org.cocktail.mangue.modele.mangue.individu.EODifDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DIF_DETAILS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EODifDetail createDifDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("DifDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DIF_DETAILS_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.EODifDetail) eo;
  }

  public void deleteDifDetailsRelationship(org.cocktail.mangue.modele.mangue.individu.EODifDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DIF_DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDifDetailsRelationships() {
    Enumeration objects = difDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDifDetailsRelationship((org.cocktail.mangue.modele.mangue.individu.EODifDetail)objects.nextElement());
    }
  }


/**
 * Créer une instance de EODif avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODif createEODif(EOEditingContext editingContext, Integer anneeCalcul
, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu			) {
    EODif eo = (EODif) createAndInsertInstance(editingContext, _EODif.ENTITY_NAME);    
		eo.setAnneeCalcul(anneeCalcul);
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  
	  public EODif localInstanceIn(EOEditingContext editingContext) {
	  		return (EODif)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODif creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODif creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODif object = (EODif)createAndInsertInstance(editingContext, _EODif.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODif localInstanceIn(EOEditingContext editingContext, EODif eo) {
    EODif localInstance = (eo == null) ? null : (EODif)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODif#localInstanceIn a la place.
   */
	public static EODif localInstanceOf(EOEditingContext editingContext, EODif eo) {
		return EODif.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODif fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODif fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODif)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODif)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODif fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODif eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODif ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODif fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

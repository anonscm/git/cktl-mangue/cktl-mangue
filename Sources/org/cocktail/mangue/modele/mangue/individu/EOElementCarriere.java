//EOElementCarriere.java
//Created on Thu Feb 27 09:41:14  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;


import java.util.Enumeration;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl.IntRef;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.IIndividuAvecDuree;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageChevron;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.prolongations.EOEmeritat;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * R&egrave;gles de validation des elements de carriere :<BR>
 * 	V&eacute;rification des longueurs de strings<BR>
 * 	Le corps et le le grade doivent etre fournis<BR>
 *	La date d'effet doit &ecirc;tre fournie<BR>
 *	Si la date de fin est fournie, celle d'effet doit etre anterieure<BR> 
 *	Les dates d'effet et de fin doivent etre a l'intarieur de celles du segment de carriere<BR>
 *	L'indice ne peut etre inferieur a l'indice de l'element precedent<BR>
 *  L'indice effectif doit etre superieur a l'indice du grade<BR>
 *  Pas de chevauchement des elements de carriere sauf si ils sont provisoires ou annules<BR><BR>
 *	Met a jour les caracteristiques de l'individu (indQualite et temTitulaire)<BR>
 * Supprime les baps ou baps corps si ils ne respectent pas les dates de validite de ces nomenclatures<BR>
 * Lors de la suppression, les elements sont invalides
 *
 * */

public class EOElementCarriere extends _EOElementCarriere implements IIndividuAvecDuree {	

	public static EOSortOrdering SORT_ECH_DESC = new EOSortOrdering(C_ECHELON_KEY, EOSortOrdering.CompareDescending);
	public static NSArray SORT_ARRAY_ECH_DESC = new NSArray(SORT_ECH_DESC);


	public EOElementCarriere() {
		super();
	}

	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI) && temProvisoire() != null && temProvisoire().equals ("N");
	}
	public void setEstValide(boolean aBool) {
		setTemValide((aBool)?CocktailConstantes.VRAI:CocktailConstantes.FAUX);
	}
	public boolean estProvisoire() {
		return temProvisoire() != null && temProvisoire().equals(CocktailConstantes.VRAI);
	}
	public void setEstProvisoire(boolean aBool) {
		if (aBool) {
			setTemProvisoire(CocktailConstantes.VRAI);
		} else {
			setTemProvisoire(CocktailConstantes.FAUX);
		}
	}
	/** Un element de carriere peut etre pris en compte lorsqu'il n'est ni provisoire ni annule par arrete
	 */
	public boolean estAPrendreEnCompte() {
		return estProvisoire() == false && estAnnule() == false;
	}

	/**
	 * 
	 * @return Indice brut d'un element de carriere a sa date de fin
	 */
	public String indiceBrut() {

		if (toGrade() != null && cEchelon() != null) {
			if (cChevron() != null) {
				NSArray<EOPassageChevron> passagesChevron = SuperFinder.rechercherPassagesChevronOuvertsPourGradeEchelonEtChevron(editingContext(), toGrade().cGrade(), cEchelon(), cChevron(), dateFin());
				if (passagesChevron.size() > 0) {
					return passagesChevron.get(0).cIndiceBrut();
				}
			}
			else {
				// On n'a pas de chevron ou on n'a pas trouvé de passageChevron
				NSArray<EOPassageEchelon> passagesEchelon = EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(editingContext(), toGrade().cGrade(), cEchelon(), dateFin(), false);
				if (passagesEchelon.size() > 0) {
					return passagesEchelon.get(0).cIndiceBrut();
				}
			}
		}

		return null;		

	}

	/**
	 * 
	 * @return
	 */
	public Number indiceMajore() {

		String indiceBrut = indiceBrut();
		if (indiceBrut != null) {
			EOIndice indice = EOIndice.indiceMajorePourIndiceBrutEtDate(editingContext(), indiceBrut, dateFin());
			if (indice != null) {
				return indice.cIndiceMajore();
			}
		}

		return null;

	}

	/**
	 * 
	 * @param prefixNumeroAuto
	 * @param annee
	 * @return
	 */
	public static String getNumeroArreteAutomatique(EOEditingContext edc, Integer annee, EOIndividu individu) {

		String prefixNumeroAuto = "" + annee + "/" ;

		// Rechercher tous les éléments de carrière de l'année pour déterminer le dernier numéro automatique attribué
		NSArray<EOElementCarriere> elements = EOElementCarriere.findForPeriode(edc, individu, DateCtrl.debutAnnee(annee), DateCtrl.finAnnee(annee));
		int valeurIncrementee = 0;
		if (elements != null && elements.count() > 0) {
			elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
			for (EOElementCarriere myElement : elements) {
				String noArrete = myElement.noArrete();
				if (noArrete != null && noArrete.indexOf(prefixNumeroAuto) >= 0) {	// Vérifier si cela correspond à un numéro automatique
					String num = noArrete.substring(prefixNumeroAuto.length());
					try {
						int numAuto = new Integer(num).intValue();
						if (numAuto > valeurIncrementee)
							valeurIncrementee = numAuto;

					} catch (Exception exc) {
						// Le numéro d'arrêté n'est pas automatique
					}
				}
			}
		}
		return prefixNumeroAuto + StringCtrl.get0Int((valeurIncrementee + 1), 2);
	}

	/**
	 * 
	 * @param ec
	 * @param ancienElement
	 * @return
	 */
	public static EOElementCarriere renouveler(EOEditingContext ec, EOElementCarriere ancienElement) {

		EOElementCarriere newObject = (EOElementCarriere) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.initAvecElement(ancienElement);
		newObject.setCEchelon(ancienElement.cEchelon());
		newObject.setCChevron(ancienElement.cChevron());
		newObject.setDateDebut(DateCtrl.jourSuivant(ancienElement.dateFin()));
		newObject.setDateFin(null);

		return newObject;

	}



	/** retourne true si il y a un arrete d'annulation */
	public boolean estAnnule() {
		return dateArreteAnnulation() != null || noArreteAnnulation() != null;
	}
	/** retourne true si l'arrete est signe */
	public boolean estSigne() {
		return temArreteSigne() != null && temArreteSigne().equals(CocktailConstantes.VRAI);
	}

	/** retourne true si l'arrete est gere par l'etablissement */
	public boolean estGereParEtablissement() {
		return temGestEtab() != null && temGestEtab().equals(CocktailConstantes.VRAI);
	}

	/** return true si une promotion est possible : le type d'acces doit le permettre et
	 * le changement de position en debut et fin doit etre activite ou detachement */
	public boolean promotionPossible() {
		if (promotionOK()) {		// integration ou reclassement ou pas d'accès défini
			return true;
		}
		NSArray<EOChangementPosition> changementsPosition = toCarriere().changementsPositionPourPeriode(dateDebut(),dateFin());
		if (changementsPosition == null || changementsPosition.size() == 0) {
			return true;
		} else {
			boolean peutModifier = true;
			for (EOChangementPosition changementPosition : changementsPosition) {
				boolean bonnePeriode = false;
				// Vérifier si il s'agit de la période à la date de début ou celle à la date de fin
				if (DateCtrl.isBeforeEq(changementPosition.dateDebut(),dateDebut())) {
					bonnePeriode = true;
				}
				if (changementPosition.dateFin() == null || (dateFin() != null && DateCtrl.isAfterEq(changementPosition.dateFin(), dateFin()))) {
					bonnePeriode = true;
				}
				if (bonnePeriode) {
					// on combine de manière qu'au premier false, on reste à false
					peutModifier = peutModifier && 
							(changementPosition.toPosition().estEnActivite() || changementPosition.toPosition().estUnDetachement());
				}
			}
			return peutModifier;
		}
	}
	/** return true si une promotion de grade est possible : le type d'acces doit le permettre et
	 * le changement de position en debut et fin	doit permettre le changement de grade */
	public boolean promotionGradePossible() {
		if (promotionOK()) {	// intégration ou reclassement ou pas d'accès défini
			return true;
		}
		NSArray<EOChangementPosition> changementsPosition = toCarriere().changementsPositionPourPeriode(dateDebut(),dateFin());
		if (changementsPosition == null || changementsPosition.size() == 0) {
			return true;
		} else {
			boolean peutModifier = true;
			for (EOChangementPosition changementPosition : changementsPosition) {

				boolean bonnePeriode = false;
				// Vérifier si il s'agit de la période à la date de début ou celle à la date de fin
				if (DateCtrl.isBeforeEq(changementPosition.dateDebut(),dateDebut())) {
					bonnePeriode = true;
				}
				if (changementPosition.dateFin() == null || (dateFin() != null && DateCtrl.isAfterEq(changementPosition.dateFin(), dateFin()))) {
					bonnePeriode = true;
				}
				if (bonnePeriode) {
					//	on combine de manière qu'au premier false, on reste à false
					peutModifier = peutModifier && changementPosition.toPosition().promotionGradePossible();
				}
			}
			return peutModifier;
		}
	}
	/** return true si une promotion d'echelon est possible : le type d'acces doit le permettre et
	 * le changement de position en debut et fin doit permettre un changement d'echelon */
	public boolean promotionEchelonPossible() {
		if (promotionOK()) {	// intégration ou reclassement ou pas d'accès défini
			return true;
		}
		NSArray<EOChangementPosition> changementsPosition = toCarriere().changementsPositionPourPeriode(dateDebut(),dateFin());
		if (changementsPosition == null || changementsPosition.size() == 0) {
			return true;
		} else {
			boolean peutModifier = true;
			for (EOChangementPosition changementPosition : changementsPosition) {
				boolean bonnePeriode = false;
				// Vérifier si il s'agit de la période à la date de début ou celle à la date de fin
				if (DateCtrl.isBeforeEq(changementPosition.dateDebut(),dateDebut())) {
					bonnePeriode = true;
				}
				if (changementPosition.dateFin() == null || (dateFin() != null && DateCtrl.isAfterEq(changementPosition.dateFin(), dateFin()))) {
					bonnePeriode = true;
				}
				if (bonnePeriode) {
					//	on combine de manière qu'au premier false, on reste à false
					peutModifier = peutModifier && changementPosition.toPosition().promotionEchelonPossible();
				}
			}
			return peutModifier;
		}
	}

	/**
	 * 
	 * @param element
	 * @return
	 */
	public static NSArray<EOElementCarriere> findElementsEquivalents(EOEditingContext edc, EOElementCarriere element) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(getQualifierIndividu(element.toIndividu()));
		qualifiers.addObject(getQualifierGrade(element.toGrade()));
		qualifiers.addObject(getQualifierEchelon(element.cEchelon()));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(getQualifierNonProvisoire());

		return fetchAll(edc, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);

	}

	/** determine la date de promotion en fonction des durees pour cet echelon<BR>
	 * On prend la date de la derniere promotion d'echelon et on soustrait l'anciennete conservee puis
	 * on ajoute la duree moyenne d'anciennete requise pour passer a l'echelon superieur en
	 * appliquant la proratisation sur la duree du conge parental qui compte pour moitie et en soustrayant
	 * les periodes de disponibilite, hors cadres depuis le dernier avancement d'echelon,<BR>
	 * on applique ensuite les reductions d'anciennete si l'individu est toujours promouvable
	 * @return datepromotion
	 */
	public NSTimestamp datePromotionEchelon(NSTimestamp debutPriseEnCompte, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSArray<EOChangementPosition> changementsPosition = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext(), toIndividu(), dateDebut(), dateFin());

		if (changementsPosition.count() == 0) {
			LogManager.logDetail(toIndividu().identite() + " n'est pas en activite pendant la periode");
			return null;
		}

		boolean aChangementEnActivite = false;
		for (EOChangementPosition changement : changementsPosition) {
			if (changement.toPosition().estEnActivite()) {
				aChangementEnActivite = true;
			}
		}
		if (!aChangementEnActivite) {
			LogManager.logDetail(toIndividu().identite() + " n'est pas en activite pendant la periode");
			return null;
		}

		// On retire les annees, mois et jours des conservations d'anciennete
		int nbMois = 0,nbAnnees = 0,nbJours = 0;
		NSArray<EOConservationAnciennete> conservations = EOConservationAnciennete.rechercherAnciennetesNonUtiliseesPourElementCarriere(editingContext(), this);
		for (EOConservationAnciennete conservation : conservations) {
			if (conservation.ancNbAnnees() != null) {
				nbAnnees -= conservation.ancNbAnnees().intValue();
			}
			if (conservation.ancNbMois() != null) {
				nbMois -= conservation.ancNbMois().intValue();
			}
			if (conservation.ancNbJours() != null) {
				nbJours -= conservation.ancNbJours().intValue();
			}
		}

		LogManager.logDetail("après conservation, nbAnnees : " + nbAnnees + ", nbMois : " + nbMois + ", nbJours : " + nbJours);

		try {
			EOPassageEchelon echelon = (EOPassageEchelon)EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(editingContext(), toGrade().cGrade(), cEchelon(), dateFin(),false).objectAtIndex(0);
			int anneeEchelon = 0, moisEchelon = 0;
			if (echelon.dureePassageAnnees() != null) {
				anneeEchelon = echelon.dureePassageAnnees().intValue();
				nbAnnees += anneeEchelon;
			}
			if (echelon.dureePassageMois() != null) {
				moisEchelon = echelon.dureePassageMois().intValue();
				nbMois += moisEchelon;
			}

			LogManager.logDetail("Echelon " + echelon.cEchelon() + ", nbAnnees : " + anneeEchelon + ", nbMois : " + moisEchelon);
			LogManager.logDetail("Resultat apres echelon nbAnnees : " + nbAnnees + ", nbMois : " + nbMois + ", nbJours : " + nbJours);

			// 01/02/2011 - On calcule la durée en activité en prenant en compte la durée des changements de position en activité et la moitié des durées des congés parentaux pendant la période de l'élément de carrière
			changementsPosition = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext(), toIndividu(), dateDebut(), dateFin());
			// 01/02/2011 - on a calculé la date théorique de la promotion, on la décale d'autant en fonction des changements en disponibilité ou de congé parental
			NSTimestamp datePromotion = datePromotionApresPriseEnCompteChangements(debutPriseEnCompte, finPeriode, nbAnnees, nbMois, nbJours, changementsPosition);

			LogManager.logDetail(">>>> 1ere DATE promotion : " + DateCtrl.dateToString(datePromotion));
			LogManager.logDetail(">>>> Fin periode : " + finPeriode);

			if (datePromotion == null || DateCtrl.isBefore(datePromotion, debutPriseEnCompte) 
					||	DateCtrl.isBefore(datePromotion,debutPeriode) ){
				//					|| (finPeriode != null && DateCtrl.isAfter(datePromotion,DateCtrl.jourSuivant(finPeriode)) )) {
				LogManager.logDetail("Personne non promouvable pendant la periode");
				return null;
			}

			// On retire les annees, mois et jours des reliquats d'anciennete	
			NSArray<EOReliquatsAnciennete> reductions = EOReliquatsAnciennete.rechercherReliquatsNonUtilisesPourElementCarriere(editingContext(), this);
			for (EOReliquatsAnciennete reliquat : reductions) {
				if (reliquat.ancNbAnnees() != null) {
					nbAnnees -= reliquat.ancNbAnnees().intValue();
				}
				if (reliquat.ancNbMois() != null) {
					nbMois -= reliquat.ancNbMois().intValue();
				}
				if (reliquat.ancNbJours() != null) {
					nbJours -= reliquat.ancNbJours().intValue();
				}

				LogManager.logDetail("reliquats ajoutes, nbAnnees : " + nbAnnees + ", nbMois : " + nbMois + ", nbJours : " + nbJours);
				datePromotion = DateCtrl.dateAvecAjoutAnnees(debutPriseEnCompte, nbAnnees);		// On repart à zéro
				LogManager.logDetail("Apres ajout annees : " + DateCtrl.dateToString(datePromotion));
				datePromotion = DateCtrl.dateAvecAjoutMois(datePromotion, nbMois);
				LogManager.logDetail("Apres ajout mois : " + DateCtrl.dateToString(datePromotion));
				if (nbJours < 0) {
					// On ramène à une base 30 jours par mois
					datePromotion = DateCtrl.dateAvecAjoutJours(datePromotion, nbJours);
					LogManager.logDetail("Apres ajout jours : " + DateCtrl.dateToString(datePromotion));
					int nbJoursDansMois = DateCtrl.nbJoursDansMois(datePromotion);
					switch (nbJoursDansMois) {
					case 31 : datePromotion = DateCtrl.jourPrecedent(datePromotion);break;
					case 29 : datePromotion = DateCtrl.jourSuivant(datePromotion);break;
					case 28 : datePromotion = DateCtrl.dateAvecAjoutJours(datePromotion, 2);
					}
					if (DateCtrl.nbJoursDansMois(datePromotion) == DateCtrl.getDayOfMonth(datePromotion)) {
						// Si on tombe sur le dernier jour du mois, on incrémente
						datePromotion = DateCtrl.jourSuivant(datePromotion);	// On prend le jour suivant
					}
				}
				LogManager.logDetail("Date promotion après reliquats : " + DateCtrl.dateToString(datePromotion));

				// 01/02/2011 - on a calculé la date théorique de la promotion, on la décale d'autant en fonction des changements en disponibilité ou de congé parental
				datePromotion = datePromotionApresPriseEnCompteChangements(debutPriseEnCompte, finPeriode, nbAnnees, nbMois, nbJours, changementsPosition);
			}
			// 01/02/2011 - Vérifier si la date de promotion tombe dans un changement en activité, on retourne null sinon
			for (EOChangementPosition changement : changementsPosition) {
				if (DateCtrl.isAfterEq(datePromotion, changement.dateDebut()) && (changement.dateFin() == null || DateCtrl.isBeforeEq(datePromotion,changement.dateFin()))) {
					// il s'agit du bon changement de position
					if (changement.toPosition().estEnActivite() == false) {
						LogManager.logDetail("La position n'est pas en activité pour cette date de promotion");
						datePromotion = null;
					}
				}
			}

			return datePromotion;		// On retourne toutes les dates même si elles sont antérieures à la campagne
		} catch (Exception exc) {
			// pas de passage échelon trouvé
			return null;
		}
	}


	/** determine la date de promotion en fonction des durees pour cet echelon<BR>
	 * On prend la date de la derniere promotion d'echelon et on soustrait l'anciennete conservee puis
	 * on ajoute la duree moyenne d'anciennete requise pour passer a l'echelon superieur en
	 * appliquant la proratisation sur la duree du conge parental qui compte pour moitie et en soustrayant
	 * les periodes de disponibilite, hors cadres depuis le dernier avancement d'echelon,<BR>
	 * on applique ensuite les r&eacute;ductions d'anciennete si l'individu est toujours promouvable
	 * @return datepromotion
	 */
	public NSTimestamp datePromotionChevron(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray changementsPosition = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext(), toIndividu(), dateDebut(), dateFin());

		if (changementsPosition.count() == 0) {
			LogManager.logDetail(toIndividu().identite() + " n'est pas en activite pendant la periode");
			return null;
		}
		// 01/02/2011 - On vérifie qu'il y a au moins un changement en activité
		boolean aChangementEnActivite = false;
		for (java.util.Enumeration<EOChangementPosition> e = changementsPosition.objectEnumerator();e.hasMoreElements();) {
			EOChangementPosition changement = e.nextElement();
			if (changement.toPosition().estEnActivite())
				aChangementEnActivite = true;
		}
		if (!aChangementEnActivite) {
			LogManager.logDetail(toIndividu().identite() + " n'est pas en activite pendant la periode");
			return null;
		}

		int nbMois = 0,nbAnnees = 0,nbJours = 0;
		try {
			EOPassageChevron chevron = (EOPassageChevron)EOPassageChevron.rechercherPassageChevronOuvertPourGradeEtEchelon(editingContext(), toGrade().cGrade(), cEchelon(), dateFin(),false).objectAtIndex(0);
			int anneeChevron = 0, moisChevron = 0;
			if (chevron.dureeChevronAnnees() != null) {
				anneeChevron = chevron.dureeChevronAnnees().intValue();
				nbAnnees += anneeChevron;
			}
			if (chevron.dureeChevronMois() != null) {
				moisChevron = chevron.dureeChevronMois().intValue();
				nbMois += moisChevron;
			}

			// 01/02/2011 - On calcule la durée en activité en prenant en compte la durée des changements de position en activité et la moitié des durées des congés parentaux pendant la période de l'élément de carrière
			changementsPosition = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext(), toIndividu(), dateDebut(), dateFin());
			// 01/02/2011 - on a calculé la date théorique de la promotion, on la décale d'autant en fonction des changements en disponibilité ou de congé parental
			NSTimestamp datePromotion = datePromotionApresPriseEnCompteChangements(dateDebut(),finPeriode,nbAnnees, nbMois, nbJours,changementsPosition);
			if (DateCtrl.isBefore(datePromotion,dateDebut()) || 
					DateCtrl.isBefore(datePromotion,debutPeriode) || (finPeriode != null && DateCtrl.isAfter(datePromotion,finPeriode))) {
				// Personne non promouvable pendant cette période
				LogManager.logDetail("Personne non promouvable pendant la periode");
				return null;
			}

			// 01/02/2011 - Vérifier si la date de promotion tombe dans un changement en activité, on retourne null sinon
			for (Enumeration<EOChangementPosition> e2 = changementsPosition.objectEnumerator();e2.hasMoreElements();) {
				EOChangementPosition changement = e2.nextElement();
				if (DateCtrl.isAfterEq(datePromotion, changement.dateDebut()) && (changement.dateFin() == null || DateCtrl.isBeforeEq(datePromotion,changement.dateFin()))) {
					// il s'agit du bon changement de position
					if (changement.toPosition().estEnActivite() == false) {
						LogManager.logDetail("La position n'est pas en activité pour cette date de promotion");
						datePromotion = null;
					}
				}
			}

			return datePromotion;		// On retourne toutes les dates même si elles sont antérieures à la campagne
		} catch (Exception exc) {
			// pas de passage chevron trouvé
			return null;
		}
	}



	public NSArray visas() {
		NSArray visas = EOVisa.rechercherVisaPourTypeArreteTypePopulationEtCorps(editingContext(),EOTypeGestionArrete.TYPE_ARRETE_AVANCEMENT,toCarriere().toTypePopulation().code(),"");
		NSArray visas1 = EOVisa.rechercherVisaPourTypeArreteTypePopulationEtCorps(editingContext(),EOTypeGestionArrete.TYPE_ARRETE_AVANCEMENT,toCarriere().toTypePopulation().code(),toCorps().cCorps());
		return visas.arrayByAddingObjectsFromArray(visas1);
	}


	public static EOElementCarriere creer(EOEditingContext ec, EOCarriere carriere) {

		EOElementCarriere newObject = (EOElementCarriere) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.initAvecCarriere(carriere);

		return newObject;
	}


	public void initAvecCarriere(EOCarriere carriere) {
		setTemValide("O");
		setTemProvisoire(CocktailConstantes.FAUX);
		setTemArreteSigne(CocktailConstantes.FAUX);
		setNoSeqElement(new Integer(obtenirNumeroSequence(carriere)));
		setNoSeqCarriere(carriere.noSeqCarriere());
		carriere.addObjectToBothSidesOfRelationshipWithKey(this,"elements");
		setNoDossierPers(carriere.toIndividu().noIndividu());
		setToIndividuRelationship(carriere.toIndividu());
		setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		if (carriere.dateFin() != null) {
			setDateFin(carriere.dateFin());
		}
	}
	/** Pour les renouvellements d'elements de carriere */
	public void initAvecElement(EOElementCarriere element) {
		initAvecCarriere(element.toCarriere());
		if (element.dateFin() != null)
			setDateDebut(DateCtrl.jourSuivant(element.dateFin()));
		setToCorpsRelationship(element.toCorps());
		setToGradeRelationship(element.toGrade());
	}

	/** Invalide l'element de carriere et les conservations et reliquats d'anciennete */
	public static void invalider(EOEditingContext ec, EOElementCarriere element) {

		// invalider aussi les reliquats d'ancienneté et les conservations d'ancienneté
		Enumeration<EOConservationAnciennete> e = EOConservationAnciennete.rechercherAnciennetesNonUtiliseesPourElementCarriere(ec,element).objectEnumerator();
		while (e.hasMoreElements()) {
			EOConservationAnciennete anciennete = (EOConservationAnciennete)e.nextElement();
			anciennete.setTemValide("N");
		}
		Enumeration<EOReliquatsAnciennete> e1 = EOReliquatsAnciennete.rechercherReliquatsNonUtilisesPourElementCarriere(ec,element).objectEnumerator();
		while (e1.hasMoreElements()) {
			EOReliquatsAnciennete anciennete = (EOReliquatsAnciennete)e1.nextElement();
			anciennete.setTemValide("N");
		}

		element.setEstValide(false);

	}

	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (estValide() == false) {
			return;	// Lors de l'invalidation d'un élément de carrière, les validations avaient été faites lors de précédentes modifications
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date d'effet doit être fournie");
		} else if (DateCtrl.isBefore(dateDebut(),toCarriere().dateDebut())) {
			throw new NSValidation.ValidationException("La date d'effet doit être postérieure à celle du segment de carrière ! (Elément du " 
					+ DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dateFin()) + ")");
		}
		if (toCorps() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le corps");
		}
		if (toGrade() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le grade");
		}
		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException("Le numéro d'arrêté comporte au plus 20 caractères");
		}

		if (inmEffectif() != null && indiceMajore() != null && inmEffectif().intValue() <= indiceMajore().intValue()) {
			throw new NSValidation.ValidationException("L'indice effectif ("+ inmEffectif() + ") doit être supérieur à l'indice majoré (" + indiceMajore() + ") !");
		}

		if (noArreteAnnulation() != null && noArreteAnnulation().length() > 20) {
			throw new NSValidation.ValidationException("Le numéro d'arrêté d'annulation comporte au plus 20 caractères");
		}

		if (dateFin() == null) {
			if (toCarriere().dateFin() != null) {
				throw new NSValidation.ValidationException("Vous devez fournir une date de fin car le segment de carrière en a une !");
			}
		} else {
			// verifier que la date d'effet soit anterieure a la date de fin
			if (DateCtrl.isAfter(dateDebut(),dateFin())) {
				throw new NSValidation.ValidationException("La date de fin de l'élément ("+DateCtrl.dateToString(dateFin()) + ") doit être postérieure à la date d'effet ("+DateCtrl.dateToString(dateDebut())+")");
			}
			// vérifier que la date de fin est antérieure a la date de fin du segment
			if (toCarriere().dateFin() != null && DateCtrl.isAfter(dateFin(),toCarriere().dateFin())) {
				throw new NSValidation.ValidationException("La date de fin de l'élément ("+DateCtrl.dateToString(dateFin()) + ") doit être antérieure à celle du segment ("+DateCtrl.dateToString(toCarriere().dateFin())+")");
			}
		}
		// pour les elements definitifs, verifier qu'il n'y a de chevauchement d'elements qu'avec des elements provisoires
		// ou avec des arretes d'annulation
		if (estProvisoire() == false && noArreteAnnulation() == null && dateArreteAnnulation() == null) {
			NSArray<EOElementCarriere> elements = toCarriere().elementsPourPeriode(dateDebut(),dateFin());
			if (elements != null && elements.count() > 0) {
				for (java.util.Enumeration<EOElementCarriere> e = elements.objectEnumerator();e.hasMoreElements();) {
					EOElementCarriere element = e.nextElement();
					if (element != this && element.estProvisoire() == false && element.noArreteAnnulation() == null && element.dateArreteAnnulation() == null) { 
						throw new NSValidation.ValidationException("Il y a déjà un élément de carrière défini sur cette période !");
					}
				}
			}
		}
		// Remettre en ordre temGestEtab au cas où on ait supprimé l'arrêté et la date d'arrêté
		if (dateArrete() == null && noArrete() == null) {
			setTemGestEtab(CocktailConstantes.FAUX);
		}

		//	Si l'element carriere se termine après la date courante, on met à jour le champ IND_QUALITE de l'agent
		// pour ramener à une date sans prendre en compte les heures
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		if (dateFin() == null || DateCtrl.isAfterEq(dateFin(),today)) {
			if (!toGrade().estSansGrade()) {
				toCarriere().toIndividu().setIndActivite(toGrade().llGrade());
			} 
		}

		setDModification(new NSTimestamp());
	}

	public NSTimestamp debutPeriode() {
		return dateDebut();
	}

	public NSTimestamp finPeriode() {
		return dateFin();
	}
	public EOIndividu individu() {
		return toIndividu();
	}
	// Pour le CIR
	/** Non gere. Retourne null */
	public String indiceSuperieur() {
		return null;
	}
	/** Non gere. Retourne null */
	public String pointsIndiceSupplementaire() {
		return null;
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param corps
	 * @return
	 */
	public static NSArray<EOElementCarriere> findforIndividuAndCorps(EOEditingContext ec, EOIndividu individu, EOCorps corps) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(getQualifierIndividu(individu));
		andQualifiers.addObject(getQualifierCorps(corps));
		andQualifiers.addObject(getQualifierValide());
		andQualifiers.addObject(getQualifierNonProvisoire());

		return fetchAll(ec, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);		
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param grade
	 * @return
	 */
	public static NSArray<EOElementCarriere> rechercherElementsPourIndividuEtGrade(EOEditingContext ec, EOIndividu individu, EOGrade grade) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(getQualifierGrade(grade));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(getQualifierNonProvisoire());

		return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);		
	}


	/**
	 * 
	 * @param ec
	 * @param carriere
	 * @return
	 */
	public static NSArray<EOElementCarriere> findForCarriere(EOEditingContext ec, EOCarriere carriere) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(getQualifierCarriere(carriere));
		qualifiers.addObject(getQualifierIndividu(carriere.toIndividu()));
		qualifiers.addObject(getQualifierValide());

		NSMutableArray sort = new NSMutableArray(PeriodePourIndividu.SORT_DATE_DEBUT_DESC);
		sort.addObject(SORT_ECH_DESC);

		return fetchAll(ec, new EOAndQualifier(qualifiers), sort);
	}

	/**
	 * 
	 * @param edc
	 * @param individu
	 * @return
	 */
	public static NSArray<EOElementCarriere> findForEmeritat(EOEditingContext edc , EOIndividu individu) {

		try {
			NSMutableArray qualifiers = new NSMutableArray(EOEmeritat.qualifierPourCorps(edc));
			qualifiers.addObject(getQualifierIndividu(individu));
			qualifiers.addObject(getQualifierValide());
			qualifiers.addObject(getQualifierNonProvisoire());

			return fetchAll(edc, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray();
		}


	}


	/** recherche les elements de carriere selon certains criteres et renvoie un tableau ordonne par individu
	 * @param editingContext
	 * @param qualifier
	 * @param sort true si le tableau doit etre classz
	 * @param refresh true si refresh des elements
	 * @return tableau des elements de carriere
	 */
	public static NSArray<EOElementCarriere> rechercherElementsAvecCriteres(EOEditingContext edc,EOQualifier qualifier,boolean sort,boolean refresh) {

		NSMutableArray sorts = null;
		if (sort) {
			sorts = new NSMutableArray(new EOSortOrdering(EOElementCarriere.TO_INDIVIDU_KEY+"." + EOIndividu.NO_INDIVIDU_KEY, EOSortOrdering.CompareAscending));
			sorts.addObject(EOSortOrdering.sortOrderingWithKey(DATE_DEBUT_KEY,EOSortOrdering.CompareDescending));
		}

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(getQualifierNonProvisoire());

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), sorts);
		myFetch.setRefreshesRefetchedObjects(refresh);
		NSArray<EOElementCarriere> elements = edc.objectsWithFetchSpecification(myFetch);
		return elements;
	}

	/** recherche les elements de carriere non annules
	 * commencant avant dateDebut et terminant apres dateFin
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateEffet	peut etre nulle
	 * @param dateFin	peut etre nulle
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants
	 * @param supprimerDoublons si true, supprimer les doublons
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @return tableau des elements de carriere prenant effet avant la date fournie et se terminant apres la date fournie en param&egrave;tre
	 */
	public static NSArray<EOElementCarriere> rechercherElementsPourPeriodeEtTypePersonnel(EOEditingContext edc,String nom,String prenom,NSTimestamp dateEffet,NSTimestamp dateFin,int typePersonnel,boolean supprimerDoublons,boolean estNomPatronymique) {
		return rechercherElementsPourPeriodeEtTypePersonnel(edc, nom, prenom, dateEffet, dateFin, typePersonnel, supprimerDoublons, estNomPatronymique, false);
	}
	/** recherche les elements de carriere non annules
	 * commencant avant dateDebut et terminant apres dateFin
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateEffet	peut etre nulle
	 * @param dateFin	peut etre nulle
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants
	 * @param supprimerDoublons si true, supprimer les doublons
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @param prefetchIdentite true si il faut prefetcher la relation identiteIndividu
	 * @return tableau des elements de carriere prenant effet avant la date fournie et se terminant apres la date fournie en param&egrave;tre
	 */
	public static NSArray rechercherElementsPourPeriodeEtTypePersonnel(EOEditingContext edc,String nom,String prenom,NSTimestamp dateEffet,NSTimestamp dateFin,int typePersonnel,boolean supprimerDoublons,boolean estNomPatronymique,boolean prefetchIdentite) {

		NSMutableArray qualifiers = new NSMutableArray();

		// Recherche pour des elements valides (non provisoires, et non annules)
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY+"."+EOIndividu.TEM_VALIDE_KEY, "O"));

		qualifiers.addObject(getQualifierNonProvisoire());

		qualifiers.addObject(qualifierPourNom(TO_INDIVIDU_KEY, nom, prenom, estNomPatronymique));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_ARRETE_ANNULATION_KEY + "=nil", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_ARRETE_ANNULATION_KEY + "=nil", null));

		if (dateEffet != null && dateFin != null) {

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + "<= %@", new NSArray(dateEffet)));

			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + ">= %@", new NSArray(dateFin)));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + "=nil", new NSArray(dateEffet)));

			qualifiers.addObject(new EOOrQualifier(orQualifiers));

		}

		if (typePersonnel != ManGUEConstantes.TOUT_PERSONNEL) {
			qualifiers.addObject(qualifierPourTypePersonnel("",typePersonnel));
		}

		EOQualifier qualifier = new EOAndQualifier(qualifiers);

		LogManager.logDetail("rechercherElementsPourPeriodeEtType - qualifier : " + qualifier);

		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(NO_DOSSIER_PERS_KEY,EOSortOrdering.CompareAscending));
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,sorts);

		if ((typePersonnel != ManGUEConstantes.TOUT_PERSONNEL) || prefetchIdentite) {
			NSMutableArray prefetches = new NSMutableArray();
			if (typePersonnel != ManGUEConstantes.TOUT_PERSONNEL) {
				prefetches.addObject(TO_CORPS_KEY);
				prefetches.addObject(EOElementCarriere.TO_CORPS_KEY+"." + EOCorps.TO_TYPE_POPULATION_KEY);
			}
			myFetch.setPrefetchingRelationshipKeyPaths(prefetches);
		}
		myFetch.setUsesDistinct(supprimerDoublons);
		myFetch.setRefreshesRefetchedObjects(true);
		NSArray resultats = edc.objectsWithFetchSpecification(myFetch);

		return resultats;
	}
	/** recherche les individus ayant des &eacute;l&eacute;ments de carriere non annules
	 * commencant avant dateDebut et terminant apr&egrave; dateFin
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateEffet	peut etre nulle
	 * @param dateFin	peut etre nulle
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants
	 * @param supprimerDoublons si true, supprimer les doublons
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @return individus trouves (type IndividuIdentite)
	 */
	public static NSArray rechercherIndividusAvecElementsPourPeriodeEtTypePersonnel(EOEditingContext edc,String nom,String prenom,NSTimestamp dateEffet,NSTimestamp dateFin,int typePersonnel,boolean supprimerDoublons,boolean estNomPatronymique) {
		LogManager.logDetail("rechercherIndividusAvecElementsPourPeriodeEtTypePersonnel - typePersonnel : " + typePersonnel);
		EOQualifier qualifierDate = null;
		if (dateEffet != null && dateFin != null) {	
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(dateEffet);
			qualifiers.addObject(dateFin);
			String stringQualifier = "carrieres.temValide = 'O' AND carrieres.elements.dateDebut <= %@ AND (carrieres.elements.dateFin >= %@ OR carrieres.elements.dateFin = nil)";
			qualifierDate = EOQualifier.qualifierWithQualifierFormat(stringQualifier, qualifiers);
		} 
		return rechercherIndividusAvecQualifierDatePourTypePersonnel(edc, qualifierDate, nom, prenom, typePersonnel, estNomPatronymique);
	}

	/** recherche les individus ayant des elements de carriere se terminant avant la date fournie
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut &ecirc;tre nul
	 * @param prenom sur lequel restreindre la recherche - peut &ecirc;tre nul
	 * @param dateReference	peut etre nulle (date du jour)
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @return individus trouves (type IndividuIdentite)
	 */
	public static NSArray rechercherIndividusAvecElementsAnterieursDatePourTypePersonnel(EOEditingContext edc,String nom,String prenom,NSTimestamp dateReference,int typePersonnel,boolean estNomPatronymique) {
		if (dateReference == null) {
			// pour ramener à une date sans prendre en compte les heures
			dateReference =  DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		}
		EOQualifier qualifierDate = EOQualifier.qualifierWithQualifierFormat("carrieres.elements.dFinElement < %@", new NSArray(dateReference));
		return rechercherIndividusAvecQualifierDatePourTypePersonnel(edc, qualifierDate, nom, prenom, typePersonnel, estNomPatronymique);
	}

	/** recherche les individus ayant elements de carriere prenant effet apres la date fournie
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateReference	peut etre nulle (date du jour)
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @return individus trouves (type IndividuIdentite)
	 * */
	public static NSArray rechercherIndividusAvecElementsFutursPourTypePersonnel(EOEditingContext edc, String nom,String prenom,NSTimestamp dateReference,int typePersonnel,boolean estNomPatronymique) {
		if (dateReference == null) {
			//	pour ramener à une date sans prendre en compte les heures
			dateReference =  DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		}
		EOQualifier qualifierDate = EOQualifier.qualifierWithQualifierFormat("carrieres.elements.dateDebut > %@", new NSArray(dateReference));
		return rechercherIndividusAvecQualifierDatePourTypePersonnel(edc, qualifierDate, nom, prenom, typePersonnel, estNomPatronymique);
	}


	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static EOElementCarriere rechercherElementADate(EOEditingContext edc,EOIndividu individu,NSTimestamp dateReference) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(getQualifierNonProvisoire());
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateReference,DATE_FIN_KEY,dateReference));

		return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
	}



	/** recherche les elements de carriere d'un individu a cheval sur la periode fournie en parametre
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode date debut (peut etre nulle)
	 * @param finPeriode (peut etre nulle)
	 * @return tableau des elements de carriere
	 */
	public static NSArray<EOElementCarriere> findForPeriode(EOEditingContext edc,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(getQualifierNonProvisoire());

		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
		}

		return fetchAll(edc, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);

	}

	/** recherche le grade d'un agent a une date donnee
	 * retourne null si non trouve
	 * @param individu
	 * @param date
	 * @return grade trouve
	 */
	public static EOGrade rechercherGradePourIndividuADate(EOEditingContext edc,EOIndividu individu, NSTimestamp date) {
		NSArray results = findForPeriode(edc, individu, date, date);
		if (results.count() > 0) {
			return ((EOElementCarriere)results.objectAtIndex(0)).toGrade();
		} else {
			return null;
		}
	}

	public static EOElementCarriere rechercherElementPourIndividuADate(EOEditingContext edc, EOIndividu individu, NSTimestamp date) {
		NSArray results = findForPeriode(edc, individu, date, date);
		if (results.size() > 0) {
			return (EOElementCarriere)results.get(0);
		} else {
			return null;
		}
	}


	/** Recherche le dernier element de carriere d'un individu */
	public static EOElementCarriere elementCarrierePrecedent(EOEditingContext edc, EOElementCarriere element) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(getQualifierIndividu(element.toIndividu()));

		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(getQualifierNonProvisoire());

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + "<%@", new NSArray(element.dateDebut())));

		return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
	}

	/** Recherche le dernier element de carriere d'un individu */
	public static EOElementCarriere lastElementCarriere(EOEditingContext edc,EOIndividu individu) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(getQualifierNonProvisoire());

		return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
	}


	// méthodes privées
	private int obtenirNumeroSequence(EOCarriere carriere) {
		if (carriere.elements().count() == 0) {
			return 1;
		} else {
			NSArray temp = EOSortOrdering.sortedArrayUsingKeyOrderArray(carriere.elements(), new NSArray(EOSortOrdering.sortOrderingWithKey("noSeqElement",EOSortOrdering.CompareDescending)));
			EOElementCarriere element = (EOElementCarriere)temp.objectAtIndex(0);
			return element.noSeqElement().intValue() + 1;
		}
	}

	private boolean promotionOK() {
		return (toTypeAcces() == null) || (toTypeAcces() != null && (toTypeAcces().estIntegration() || toTypeAcces().estReclassement()));
	}

	/**
	 * 
	 * @param debutPeriode
	 * @param finPeriode
	 * @param nbAnneesAnciennete
	 * @param nbMoisAnciennete
	 * @param nbJoursAnciennete
	 * @param changementsPosition
	 * @return
	 */
	private NSTimestamp datePromotionApresPriseEnCompteChangements(NSTimestamp debutPeriode, NSTimestamp finPeriode, int nbAnneesAnciennete, int nbMoisAnciennete, int nbJoursAnciennete, NSArray<EOChangementPosition> changementsPosition) {

		LogManager.logDetail(" ==> Calculs Changements de position : " + DateCtrl.dateToString(debutPeriode) + " : " +  nbAnneesAnciennete + " annees, " + nbMoisAnciennete + " mois");

		int nbAnneesActivite = 0, nbMoisActivite = 0, nbJoursActivite = 0;

		for (EOChangementPosition myChangement : changementsPosition) {

			if (myChangement.toPosition().prctDroitAvctEch() != null) {

				int pourcentageAvancement = myChangement.toPosition().prctDroitAvctEch().intValue();

				if (pourcentageAvancement == 0) {
					// Ce changement de position n'est pas à prendre en compte
					// Déduire de l'ancienneté, les périodes d'activité déjà écoulées
					nbAnneesAnciennete -= nbAnneesActivite;
					nbMoisAnciennete -= nbMoisActivite;
					if (nbMoisAnciennete < 0) {
						nbAnneesAnciennete = nbAnneesAnciennete - 1;
						nbMoisAnciennete += 12;
					}
					LogManager.logDetail("\tAnciennete apres changement non activite : " + nbAnneesAnciennete + " annees, " + nbMoisAnciennete + " mois");
					// Mettre à null le début de période
					debutPeriode = null;
					nbAnneesActivite = 0; nbMoisActivite = 0;
				} else {
					LogManager.logDetail("\tChangement de position à prendre en compte de type : " + myChangement.toPosition().code() +
							", debut changement : " + myChangement.dateDebutFormatee() + ", fin : " + myChangement.dateFinFormatee());
					IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
					NSTimestamp dateFin = myChangement.dateFin();
					if (dateFin == null) {
						dateFin = finPeriode;
					}
					NSTimestamp dateDebut = myChangement.dateDebut();
					if (debutPeriode == null) {	// On a rencontré une position non en activité
						debutPeriode = dateDebut;
					} else if (DateCtrl.isBefore(dateDebut,debutPeriode)) {
						dateDebut = debutPeriode;
					}
					LogManager.logDetail("		Debut : " + dateDebut + " , Fin : " + dateFin);
					DateCtrl.joursMoisAnneesEntre(dateDebut, dateFin, anneeRef, moisRef, jourRef, true);
					// Ajouter les mois d'activité au prorata du pourcentage d'avancement
					switch(pourcentageAvancement) {
					case 100 :
						nbAnneesActivite += anneeRef.value;
						nbMoisActivite += moisRef.value;
						nbJoursActivite += jourRef.value;
						break;
					case 50 :
						if (anneeRef.value % 2 > 0) {	// Division impaire
							nbAnneesActivite += (anneeRef.value - 1) / 2;
							nbMoisActivite += 6;
						} else {
							nbAnneesActivite += anneeRef.value / 2;
						}
						if (moisRef.value % 2 > 0) {	// Division impaire
							nbMoisActivite += (moisRef.value - 1) / 2;
							nbJoursActivite += 15;
						} else {
							nbMoisActivite += moisRef.value / 2;
						}
						if (jourRef.value % 2 > 0) {	// Division impaire, le calcul est fait en faveur de l'individu => + 1
							nbJoursActivite += (jourRef.value + 1) / 2;
						} else {
							nbJoursActivite += jourRef.value / 2;
						}
						break;

					}	
					// Ramener les mois en année si nécessaire et les jours en mois en se basant sur la durée comptable
					if (nbJoursActivite >= 30) {
						nbMoisActivite += (nbJoursActivite / 30);
						nbJoursActivite = (nbJoursActivite % 30);
					}
					if (nbMoisActivite > 12) {
						nbAnneesActivite += (nbMoisActivite / 12);
						nbMoisActivite = (nbMoisActivite % 12);
					}
					LogManager.logDetail("\t Activité : " + nbAnneesActivite + " ans, " + nbMoisActivite + " mois, " + nbJoursActivite + " jours");
					LogManager.logDetail("\t Ancienneté : " + nbAnneesAnciennete + " ans " +  nbMoisAnciennete + " mois.");

					// Si on a dépassé la durée attendue, on est sur le bon changement de position
					if (nbAnneesActivite > nbAnneesAnciennete 
							|| (nbAnneesActivite == nbAnneesAnciennete && nbMoisActivite >= nbMoisAnciennete)) {

						// On fait les calculs de date ici pour voir si on tombe dans les dates de l'élément de carrière
						NSTimestamp datePromotion = DateCtrl.dateAvecAjoutAnnees(debutPeriode, nbAnneesAnciennete);
						LogManager.logDetail("\tApres ajout annee " + DateCtrl.dateToString(datePromotion));
						datePromotion = DateCtrl.dateAvecAjoutMois(datePromotion, nbMoisAnciennete);
						LogManager.logDetail("\tApres ajout mois " + DateCtrl.dateToString(datePromotion));
						if (nbJoursAnciennete < 0) {
							// On ramène à une base 30 jours par mois
							datePromotion = DateCtrl.dateAvecAjoutJours(datePromotion, nbJoursAnciennete);
							LogManager.logDetail("\tApres ajout jour " + DateCtrl.dateToString(datePromotion));
							int nbJoursDansMois = DateCtrl.nbJoursDansMois(datePromotion);
							switch (nbJoursDansMois) {
							case 31 : datePromotion = DateCtrl.jourPrecedent(datePromotion);break;
							case 29 : datePromotion = DateCtrl.jourSuivant(datePromotion);break;
							case 28 : datePromotion = DateCtrl.dateAvecAjoutJours(datePromotion, 2);
							}
							if (DateCtrl.nbJoursDansMois(datePromotion) == DateCtrl.getDayOfMonth(datePromotion)) {
								// Si on tombe sur le dernier jour du mois, on incrémente
								datePromotion = DateCtrl.jourSuivant(datePromotion);	// On prend le jour suivant
							}
						}
						LogManager.logDetail("Date promotion : " + DateCtrl.dateToString(datePromotion));
						return datePromotion;
					}
				}
			}
		} 

		LogManager.logDetail(">>>> DATE PROMOTION APRES PRISE EN COMPTE CHANGEMENTS DE POSITION  ==> RETURN NULL ");
		return null;	// On n'a pas réussi à dépasser l'ancienneté
	}
	// méthodes privées statiques
	private static String andQualifier(String qualifier) {
		if (qualifier.length() > 0) {
			qualifier = qualifier + " AND ";
		}
		return qualifier;
	}
	private static EOQualifier qualifierPourNom(String relation,String nom,String prenom,boolean estNomPatronymique) {
		NSMutableArray args = new NSMutableArray();
		String stringQualifier = "";
		if (relation.length() > 0) {
			relation += ".";
		}
		if (nom != null) {
			args.addObject(nom + "*");
			if (estNomPatronymique) {
				stringQualifier = relation + "nomPatronymique caseInsensitiveLike %@ AND " + relation + "nomUsuel like '*' AND "+ relation + "temValide = 'O'";
			} else {
				stringQualifier = andQualifier(stringQualifier) + relation + "nomUsuel caseInsensitiveLike %@";
			}
		} else {
			stringQualifier = relation + "nomUsuel like '*'";
		}
		if (prenom != null) {
			stringQualifier = andQualifier(stringQualifier) + relation + "prenom caseInsensitiveLike %@";
			args.addObject(prenom + "*");
		}
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
	}

	private static EOQualifier qualifierPourTypePersonnel(String relation, int typePersonnel) {
		String stringQualifier = "";
		if (relation.length() > 0) {
			relation += ".";
		}
		if (typePersonnel == ManGUEConstantes.ENSEIGNANT) {
			stringQualifier = "(" + relation + "toCorps.cCorps = '776' OR " + 
					relation + "toCorps.cCorps = '523' OR (" + 
					relation + "toCorps.toTypePopulation != nil AND (" + 
					relation + "toCorps.toTypePopulation.temEnseignant = 'O' OR " + 
					relation + "toCorps.toTypePopulation.temEnsSup = 'O' OR " +  
					relation + "toCorps.toTypePopulation.tem2Degre = 'O')))";
		} else if (typePersonnel == ManGUEConstantes.NON_ENSEIGNANT) {
			stringQualifier = relation + "toCorps.toTypePopulation.temEnseignant = 'N'";
		}
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier, null);
	}

	/**
	 * 
	 * @param editingContext
	 * @param qualifierDate
	 * @param nom
	 * @param prenom
	 * @param typePersonnel
	 * @param estNomPatronymique
	 * @return
	 */
	private static NSArray rechercherIndividusAvecQualifierDatePourTypePersonnel(EOEditingContext editingContext,EOQualifier qualifierDate,String nom,String prenom,int typePersonnel,boolean estNomPatronymique) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (qualifierDate != null) {
			qualifiers.addObject(qualifierDate);
		}
		// Rechercher les éléments valides
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("carrieres.elements.dateArreteAnnulation = nil AND carrieres.elements.noArreteAnnulation = nil AND carrieres.elements.temValide = 'O'", null));
		// Limiter aux noms et prénoms recherchés
		qualifiers.addObject(qualifierPourNom("",nom, prenom, estNomPatronymique));

		// Limiter aux types de peersonnel
		if (typePersonnel != ManGUEConstantes.TOUT_PERSONNEL && typePersonnel < ManGUEConstantes.TOUT_PERSONNEL) {
			EOQualifier qual = qualifierPourTypePersonnel("carrieres.elements", typePersonnel);
			if (qual != null)
				qualifiers.addObject(qualifierPourTypePersonnel("carrieres.elements", typePersonnel));
		}
		
		EOQualifier qualifier = new EOAndQualifier(qualifiers);

		LogManager.logDetail("rechercherIndividusAvecQualifierDatePourTypePersonnel - qualifier : " + qualifier);
		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey("noIndividu",EOSortOrdering.CompareAscending));
		EOFetchSpecification myFetch = new EOFetchSpecification("IndividuIdentite",qualifier,sorts);
		myFetch.setRefreshesRefetchedObjects(true);

		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierCarriere(EOCarriere carriere) {
		return  EOQualifier.qualifierWithQualifierFormat(TO_CARRIERE_KEY + "=%@", new NSArray<EOCarriere>(carriere));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierGrade(EOGrade grade) {
		return  EOQualifier.qualifierWithQualifierFormat(TO_GRADE_KEY + "=%@", new NSArray<EOGrade>(grade));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierEchelon(String cEchelon) {
		return  EOQualifier.qualifierWithQualifierFormat(C_ECHELON_KEY + "=%@", new NSArray<String>(cEchelon));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierCorps(EOCorps corps) {
		return  EOQualifier.qualifierWithQualifierFormat(TO_CORPS_KEY + "=%@", new NSArray<EOCorps>(corps));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierIndividu(EOIndividu individu) {
		return  EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray<EOIndividu>(individu));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierValide() {
		return  EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray<String>("O"));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierNonProvisoire() {
		return  EOQualifier.qualifierWithQualifierFormat(TEM_PROVISOIRE_KEY + "=%@", new NSArray<String>("N"));
	}

}

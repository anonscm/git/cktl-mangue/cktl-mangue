/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPasse.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.mangue.modele.PeriodePourIndividu;


public abstract class _EOPasse extends  PeriodePourIndividu {
	public static final String ENTITY_NAME = "Passe";
	public static final String ENTITY_TABLE_NAME = "MANGUE.PASSE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "passOrdre";

	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_VALIDEE_ANNEES_KEY = "dureeValideeAnnees";
	public static final String DUREE_VALIDEE_JOURS_KEY = "dureeValideeJours";
	public static final String DUREE_VALIDEE_MOIS_KEY = "dureeValideeMois";
	public static final String D_VALIDATION_SERVICE_KEY = "dValidationService";
	public static final String ETABLISSEMENT_PASSE_KEY = "etablissementPasse";
	public static final String FONCTION_PASSE_KEY = "fonctionPasse";
	public static final String PAS_MINISTERE_KEY = "pasMinistere";
	public static final String PAS_PC_ACQUITEE_KEY = "pasPcAcquitee";
	public static final String PAS_QUOTITE_KEY = "pasQuotite";
	public static final String PAS_QUOTITE_COTISATION_KEY = "pasQuotiteCotisation";
	public static final String PAS_TYPE_TEMPS_KEY = "pasTypeTemps";
	public static final String SECTEUR_PUBLIC_KEY = "secteurPublic";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_TYPE_POPULATION_KEY = "cTypePopulation";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String PASS_ORDRE_KEY = "passOrdre";
	public static final String PAS_TYPE_FCT_PUBLIQUE_KEY = "pasTypeFctPublique";
	public static final String PAS_TYPE_SERVICE_KEY = "pasTypeService";
	public static final String CTRA_ORDRE_KEY = "ctraOrdre";
	public static final String ID_MINISTERE_KEY = "idMinistere";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_COLKEY = "D_DEB_PASSE";
	public static final String DATE_FIN_COLKEY = "D_FIN_PASSE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String DUREE_VALIDEE_ANNEES_COLKEY = "DUREE_VALIDEE_ANNEES";
	public static final String DUREE_VALIDEE_JOURS_COLKEY = "DUREE_VALIDEE_JOURS";
	public static final String DUREE_VALIDEE_MOIS_COLKEY = "DUREE_VALIDEE_MOIS";
	public static final String D_VALIDATION_SERVICE_COLKEY = "D_VALIDATION_SERVICE";
	public static final String ETABLISSEMENT_PASSE_COLKEY = "ETABLISSEMENT_PASSE";
	public static final String FONCTION_PASSE_COLKEY = "FONCTION_PASSE";
	public static final String PAS_MINISTERE_COLKEY = "PAS_MINISTERE";
	public static final String PAS_PC_ACQUITEE_COLKEY = "PAS_PC_ACQUITEE";
	public static final String PAS_QUOTITE_COLKEY = "PAS_QUOTITE";
	public static final String PAS_QUOTITE_COTISATION_COLKEY = "PAS_QUOTITE_COTISATION";
	public static final String PAS_TYPE_TEMPS_COLKEY = "PAS_TYPE_TEMPS";
	public static final String SECTEUR_PUBLIC_COLKEY = "SECTEUR_PUBLIC";
	public static final String TEM_TITULAIRE_COLKEY = "TEM_TITULAIRE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_TYPE_POPULATION_COLKEY = "C_TYPE_POPULATION";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String PASS_ORDRE_COLKEY = "PAS_ORDRE";
	public static final String PAS_TYPE_FCT_PUBLIQUE_COLKEY = "PAS_TYPE_FCT_PUBLIQUE";
	public static final String PAS_TYPE_SERVICE_COLKEY = "PAS_TYPE_SERVICE";
	public static final String CTRA_ORDRE_COLKEY = "CTRA_ORDRE";
	public static final String ID_MINISTERE_COLKEY = "ID_MINISTERE";


	// Relationships
	public static final String CATEGORIE_KEY = "categorie";
	public static final String CONTRAT_AVENANT_KEY = "contratAvenant";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_MINISTERE_KEY = "toMinistere";
	public static final String TO_TYPE_FONCTION_PUBLIQUE_KEY = "toTypeFonctionPublique";
	public static final String TO_TYPE_SERVICE_KEY = "toTypeService";
	public static final String TYPE_POPULATION_KEY = "typePopulation";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer dureeValideeAnnees() {
    return (Integer) storedValueForKey(DUREE_VALIDEE_ANNEES_KEY);
  }

  public void setDureeValideeAnnees(Integer value) {
    takeStoredValueForKey(value, DUREE_VALIDEE_ANNEES_KEY);
  }

  public Integer dureeValideeJours() {
    return (Integer) storedValueForKey(DUREE_VALIDEE_JOURS_KEY);
  }

  public void setDureeValideeJours(Integer value) {
    takeStoredValueForKey(value, DUREE_VALIDEE_JOURS_KEY);
  }

  public Integer dureeValideeMois() {
    return (Integer) storedValueForKey(DUREE_VALIDEE_MOIS_KEY);
  }

  public void setDureeValideeMois(Integer value) {
    takeStoredValueForKey(value, DUREE_VALIDEE_MOIS_KEY);
  }

  public NSTimestamp dValidationService() {
    return (NSTimestamp) storedValueForKey(D_VALIDATION_SERVICE_KEY);
  }

  public void setDValidationService(NSTimestamp value) {
    takeStoredValueForKey(value, D_VALIDATION_SERVICE_KEY);
  }

  public String etablissementPasse() {
    return (String) storedValueForKey(ETABLISSEMENT_PASSE_KEY);
  }

  public void setEtablissementPasse(String value) {
    takeStoredValueForKey(value, ETABLISSEMENT_PASSE_KEY);
  }

  public String fonctionPasse() {
    return (String) storedValueForKey(FONCTION_PASSE_KEY);
  }

  public void setFonctionPasse(String value) {
    takeStoredValueForKey(value, FONCTION_PASSE_KEY);
  }

  public String pasMinistere() {
    return (String) storedValueForKey(PAS_MINISTERE_KEY);
  }

  public void setPasMinistere(String value) {
    takeStoredValueForKey(value, PAS_MINISTERE_KEY);
  }

  public String pasPcAcquitee() {
    return (String) storedValueForKey(PAS_PC_ACQUITEE_KEY);
  }

  public void setPasPcAcquitee(String value) {
    takeStoredValueForKey(value, PAS_PC_ACQUITEE_KEY);
  }

  public java.math.BigDecimal pasQuotite() {
    return (java.math.BigDecimal) storedValueForKey(PAS_QUOTITE_KEY);
  }

  public void setPasQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAS_QUOTITE_KEY);
  }

  public java.math.BigDecimal pasQuotiteCotisation() {
    return (java.math.BigDecimal) storedValueForKey(PAS_QUOTITE_COTISATION_KEY);
  }

  public void setPasQuotiteCotisation(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAS_QUOTITE_COTISATION_KEY);
  }

  public String pasTypeTemps() {
    return (String) storedValueForKey(PAS_TYPE_TEMPS_KEY);
  }

  public void setPasTypeTemps(String value) {
    takeStoredValueForKey(value, PAS_TYPE_TEMPS_KEY);
  }

  public String secteurPublic() {
    return (String) storedValueForKey(SECTEUR_PUBLIC_KEY);
  }

  public void setSecteurPublic(String value) {
    takeStoredValueForKey(value, SECTEUR_PUBLIC_KEY);
  }

  public String temTitulaire() {
    return (String) storedValueForKey(TEM_TITULAIRE_KEY);
  }

  public void setTemTitulaire(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.EOCategorie categorie() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOCategorie)storedValueForKey(CATEGORIE_KEY);
  }

  public void setCategorieRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOCategorie value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOCategorie oldValue = categorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATEGORIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATEGORIE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOContratAvenant contratAvenant() {
    return (org.cocktail.mangue.modele.mangue.individu.EOContratAvenant)storedValueForKey(CONTRAT_AVENANT_KEY);
  }

  public void setContratAvenantRelationship(org.cocktail.mangue.modele.mangue.individu.EOContratAvenant value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOContratAvenant oldValue = contratAvenant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONTRAT_AVENANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONTRAT_AVENANT_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres toMinistere() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres)storedValueForKey(TO_MINISTERE_KEY);
  }

  public void setToMinistereRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres oldValue = toMinistere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MINISTERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MINISTERE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique toTypeFonctionPublique() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique)storedValueForKey(TO_TYPE_FONCTION_PUBLIQUE_KEY);
  }

  public void setToTypeFonctionPubliqueRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique oldValue = toTypeFonctionPublique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_FONCTION_PUBLIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_FONCTION_PUBLIQUE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeService toTypeService() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeService)storedValueForKey(TO_TYPE_SERVICE_KEY);
  }

  public void setToTypeServiceRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeService value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeService oldValue = toTypeService();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_SERVICE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation typePopulation() {
    return (org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation)storedValueForKey(TYPE_POPULATION_KEY);
  }

  public void setTypePopulationRelationship(org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation oldValue = typePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_POPULATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_POPULATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOPasse avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPasse createEOPasse(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String pasMinistere
, String secteurPublic
, String temTitulaire
, String temValide
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu, org.cocktail.mangue.common.modele.nomenclatures.EOTypeService toTypeService			) {
    EOPasse eo = (EOPasse) createAndInsertInstance(editingContext, _EOPasse.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPasMinistere(pasMinistere);
		eo.setSecteurPublic(secteurPublic);
		eo.setTemTitulaire(temTitulaire);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setToTypeServiceRelationship(toTypeService);
    return eo;
  }

  
	  public EOPasse localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPasse)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPasse creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPasse creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPasse object = (EOPasse)createAndInsertInstance(editingContext, _EOPasse.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPasse localInstanceIn(EOEditingContext editingContext, EOPasse eo) {
    EOPasse localInstance = (eo == null) ? null : (EOPasse)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPasse#localInstanceIn a la place.
   */
	public static EOPasse localInstanceOf(EOEditingContext editingContext, EOPasse eo) {
		return EOPasse.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPasse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPasse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPasse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPasse)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPasse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPasse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPasse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPasse)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPasse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPasse eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPasse ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPasse fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

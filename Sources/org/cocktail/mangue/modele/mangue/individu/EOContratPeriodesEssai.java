/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOContratPeriodesEssai extends _EOContratPeriodesEssai {

	public final static EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering (DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public final static EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering (DATE_FIN_KEY, EOSortOrdering.CompareDescending);

	public final static NSArray SORT_ARRAY_DATE_DEBUT_ASC = new NSArray(SORT_DATE_DEBUT_ASC);
	public final static NSArray SORT_ARRAY_DATE_DEBUT_DESC = new NSArray(SORT_DATE_DEBUT_DESC);


    public EOContratPeriodesEssai() {
        super();
    }

	public static EOContratPeriodesEssai creer(EOEditingContext ec, EOContrat contrat, boolean insertObject) {
		
		EOContratPeriodesEssai newObject = new EOContratPeriodesEssai();    
		newObject.setToContratRelationship(contrat);
		newObject.setDCreation(new NSTimestamp());
		if (insertObject)
			ec.insertObject(newObject);
		return newObject;
	}
	
	/**
	 * 
	 * @param edc
	 * @param crct
	 * @return
	 */
	public static NSArray<EOContratPeriodesEssai> findForContrat(EOEditingContext edc, EOContrat contrat) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_CONTRAT_KEY + " = %@", new NSArray(contrat));
		return fetchAll(edc, qualifier);
	}
	
	/**
	 * 
	 * @param edc
	 * @param crct
	 * @return
	 */
	public static EOContratPeriodesEssai getLastPeriodeEssai(EOEditingContext edc, EOContrat contrat) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_CONTRAT_KEY + " = %@", new NSArray(contrat));
		return fetchFirstByQualifier(edc, qualifier, SORT_ARRAY_DATE_DEBUT_DESC);
	}
	public void validateForSave() {
		
		setDModification(DateCtrl.today());

		if (toContrat() == null)
			throw new NSValidation.ValidationException("Le contrat doit être renseigné !");

		if (dateDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "ESSAI"));		
		}
		
		if (dateFin() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_NON_RENSEIGNE, "ESSAI"));			
		}

		if (toContrat().dateDebut() != null && DateCtrl.isBefore(dateDebut(), toContrat().dateDebut())) {
			throw new NSValidation.ValidationException("La période d'essai ne peut commencer avant le contrat !");						
		}
		
		if (toContrat().dateFin() != null && dateFin() != null && DateCtrl.isAfter(dateFin(), toContrat().dateFin())) {
			throw new NSValidation.ValidationException("La période d'essai ne peut se terminer après la date de fin de contrat !");						
		}

	}


}

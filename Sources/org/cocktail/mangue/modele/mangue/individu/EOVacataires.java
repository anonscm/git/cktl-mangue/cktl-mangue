/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.individu;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOVacataires extends _EOVacataires {

	public static final EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(TO_INDIVIDU_IDENTITE_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);
	public static final EOSortOrdering SORT_DATE_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_DATE_DESC = new NSArray(SORT_DATE_DESC);
	private String noArretePourEdition;

	private NSArray<EOOccupation> occupationsPourVacation;
	private NSArray<EOAffectation> affectationsPourVacation;
	private String activitesPourVacation;

	public EOVacataires() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param utilisateur
	 * @param individu
	 * @return
	 */
	public static EOVacataires creer(EOEditingContext ec, EOAgentPersonnel utilisateur, EOIndividu individu) {

		EOVacataires newObject = (EOVacataires) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setToIndividuRelationship(individu);
		newObject.setEstTitulaire(false);
		newObject.setEstSigne(false);
		newObject.setEstEnseignant(true);
		newObject.setEstValide(true);
		newObject.setTemDepassementAuto("N");
		newObject.setTemPaiementPonctuel("N");
		newObject.setTemAutorisationCumul("N");

		newObject.setPersIdCreation(utilisateur.toIndividu().persId());
		newObject.setPersIdModification(utilisateur.toIndividu().persId());

		return newObject;		
	}
	
	/**
	 * 
	 * @param edc
	 * @param oldVacataire
	 */
	public static EOVacataires dupliquer(EOEditingContext edc, EOVacataires oldVacataire, EOAgentPersonnel utilisateur) {
		
		EOVacataires vacataire = new EOVacataires();
		
		vacataire.setToIndividuRelationship(oldVacataire.toIndividu());
		vacataire.setToIndividuIdentiteRelationship(oldVacataire.toIndividuIdentite());
		vacataire.setToCnuRelationship(oldVacataire.toCnu());
		vacataire.setToCorpsRelationship(oldVacataire.toCorps());
		vacataire.setToGradeRelationship(oldVacataire.toGrade());
		vacataire.setToIndividuIdentiteRelationship(oldVacataire.toIndividuIdentite());
		vacataire.setToProfessionRelationship(oldVacataire.toProfession());
		vacataire.setToStructureRelationship(oldVacataire.toStructure());
		vacataire.setToAdresseRelationship(oldVacataire.toAdresse());
		
		vacataire.setTemEnseignant(oldVacataire.temEnseignant());
		vacataire.setTemPaiementPonctuel(oldVacataire.temEnseignant());
		vacataire.setTemPersEtab(oldVacataire.temPersEtab());
		vacataire.setTemSigne(oldVacataire.temSigne());
		vacataire.setTemTitulaire(oldVacataire.temTitulaire());
		vacataire.setTemValide(oldVacataire.temValide());
		
		vacataire.setPersIdCreation(utilisateur.toIndividu().persId());
		vacataire.setPersIdModification(utilisateur.toIndividu().persId());
		
		//vacataire.takeValuesFromDictionary(oldVacataire.snapshot());
		
		vacataire.setDateDebut(null);
		vacataire.setDateFin(null);
		vacataire.setNbrHeures(new BigDecimal(0));
		vacataire.setNbrHeuresRealisees(new BigDecimal(0));
		
		edc.insertObject(vacataire);
		
		return vacataire;
		
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOVacataires> findForIndividu(EOEditingContext ec, EOIndividu individu) {

		try {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY +" = %@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY +" = %@", new NSArray(individu)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DESC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}


	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOVacataires> findForIndividuAndPeriode(EOEditingContext ec,EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		try {
			
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));
			if (dateDebut != null) {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateDebut,DATE_FIN_KEY,dateFin));
			}		
			return fetchAll(ec,new EOAndQualifier(qualifiers),SORT_ARRAY_DATE_DESC );
		}
		catch (Exception e) {
			return new NSArray();
		}
	}	
	/**
	 * 
	 * @param edc
	 * @param qualifier
	 * @return
	 */
	public static NSArray<EOVacataires> findForQualifier(EOEditingContext edc, EOQualifier qualifier) {
		
		NSMutableArray prefetchs = new NSMutableArray();
		//prefetchs.add(TO_INDIVIDU_KEY);
		prefetchs.add(TO_INDIVIDU_IDENTITE_KEY);
		return fetchAll(edc, qualifier, SORT_ARRAY_NOM_ASC, prefetchs);

	}
			
	/**
	 * 
	 * @param editingContext
	 * @param structure
	 * @param debut
	 * @param fin
	 * @return
	 */
	public static NSArray<EOVacataires> findForStructureAndPeriode(EOEditingContext editingContext, NSArray<EOStructure> structures, NSTimestamp debut, NSTimestamp fin) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();		

			if (structures != null && structures.size() > 0)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_AFFECTATIONS_KEY+"."+EOVacatairesAffectation.TO_STRUCTURE_KEY+"=%@", new NSArray(structures.get(0))));

			if (debut != null)
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debut, DATE_FIN_KEY, fin));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers), SORT_ARRAY_NOM_ASC, new NSArray(TO_INDIVIDU_IDENTITE_KEY));
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param debut
	 * @param fin
	 * @return
	 */
	public static NSArray<EOVacataires> findForPeriode(EOEditingContext editingContext, NSTimestamp debut, NSTimestamp fin) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debut, DATE_FIN_KEY, fin));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);

	}

	public String noArretePourEdition() {
		if (noArretePourEdition == null) {
			// On construit le N° arrêté à partir de la globalID
			EOGlobalID gid = editingContext().globalIDForObject(this);
			if (gid instanceof  EOKeyGlobalID) {
				return "" + DateCtrl.getYear(new NSTimestamp()) + "/" + ((EOKeyGlobalID)gid).keyValues()[0];
			} else {	// Ne devrait pas se produire, on a toujours une key global ID
				return "" + DateCtrl.getYear(new NSTimestamp());
			}
		} else {
			return noArretePourEdition;
		}
	}
	public void setNoArretePourEdition(String aStr) {
		this.noArretePourEdition = aStr;
	}

	public boolean estValide() {
		return temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}


	public boolean estTitulaire() {
		return temTitulaire().equals(CocktailConstantes.VRAI);
	}
	public void setEstTitulaire(boolean aBool) {
		if (aBool) {
			setTemTitulaire(CocktailConstantes.VRAI);
		} else {
			setTemTitulaire(CocktailConstantes.FAUX);
		}
	}

	public boolean estPersonnelEtablissement() {
		return temPersEtab() != null && temPersEtab().equals(CocktailConstantes.VRAI);
	}
	public void setEstPersonnelEtablissement(boolean aBool) {
		if (aBool) {
			setTemPersEtab(CocktailConstantes.VRAI);
		} else {
			setTemPersEtab(CocktailConstantes.FAUX);
		}
	}

	
	public boolean isAutorisationCumul() {
		return temAutorisationCumul() != null && temAutorisationCumul().equals(CocktailConstantes.VRAI);
	}
	public void setEstAutorisationCumul(boolean aBool) {
		if (aBool) {
			setTemAutorisationCumul(CocktailConstantes.VRAI);
		} else {
			setTemAutorisationCumul(CocktailConstantes.FAUX);
		}
	}
	
	
	public boolean estDepassementAuto() {
		return temDepassementAuto() != null && temDepassementAuto().equals(CocktailConstantes.VRAI);
	}
	public void setEstDepassementAuto(boolean aBool) {
		if (aBool) {
			setTemDepassementAuto(CocktailConstantes.VRAI);
		} else {
			setTemDepassementAuto(CocktailConstantes.FAUX);
		}
	}
	
	public boolean estSigne() {
		return temSigne().equals(CocktailConstantes.VRAI);
	}
	public void setEstSigne(boolean aBool) {
		if (aBool) {
			setTemSigne(CocktailConstantes.VRAI);
		} else {
			setTemSigne(CocktailConstantes.FAUX);
		}
	}

	public boolean estEnseignant() {
		return temEnseignant().equals(CocktailConstantes.VRAI);
	}
	public void setEstEnseignant(boolean aBool) {
		if (aBool) {
			setTemEnseignant(CocktailConstantes.VRAI);
		} else {
			setTemEnseignant(CocktailConstantes.FAUX);
		}
	}

	public boolean paiementPonctuel() {
		return temPaiementPonctuel() != null && temPaiementPonctuel().equals(CocktailConstantes.VRAI);
	}
	public void setPaiementPonctuel(boolean aBool) {
		if (aBool) {
			setTemPaiementPonctuel(CocktailConstantes.VRAI);
		} else {
			setTemPaiementPonctuel(CocktailConstantes.FAUX);
		}
	}
	
	
	/**
	 * 
	 */
	public void preparerEmplois() {
		occupationsPourVacation = EOOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContext(), toIndividu(), dateDebut(), dateFin());
	}
	public void preparerAffectations() {
		affectationsPourVacation = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext(), toIndividu(), dateDebut(), dateFin());
		affectationsPourVacation = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectationsPourVacation, EOAffectation.SORT_ARRAY_QUOTITE_DESC);
	}
	/** Utilisee pour l'impression du contrat de travail. Retourne le lieu d'affectation pour l'affectation ayant la
	 * quotite la plus elevee */
	public String lieuAffectation() {
		if (affectationsPourVacation == null || affectationsPourVacation.count() == 0) {
			return "";
		} else {
			return ((EOAffectation)affectationsPourVacation.get(0)).toStructureUlr().llStructure();
		}
	}
	
	/** Utilisee pour l'impression du contrat de travail. Retourne le numero de l'emploi associe si ce n'est pas un
	 * rompu de temps partiel ie que l'occupation indique l'individu est titulaire de l'emploi */
	public String numeroEmploi() {
		if (occupationsPourVacation == null || occupationsPourVacation.count() == 0) {
			return "";
		}
		for (EOOccupation myOccupation : occupationsPourVacation) {
			if (myOccupation.estTitulaireEmploi()) {	// l'individu est titulaire de l'emploi, il ne s'agit pas de rompus
				return myOccupation.toEmploi().getNoEmploi();
			}
		}
		// Si il y a plusieurs occupations, on ne retourne rien
		if (occupationsPourVacation.count() == 1) {
			return (occupationsPourVacation.get(0)).toEmploi().getNoEmploi();
		}
		
		return "";
	}
	
	/** Utilisee pour l'impression du contrat de travail. Retourne true si l'occupation indique l'individu est titulaire
	de l'emploi */
	public boolean estTitulaireEmploi() {
		if (occupationsPourVacation == null || occupationsPourVacation.count() == 0) {
			return false;
		}
		for (EOOccupation myOccupation : occupationsPourVacation) {
			if (myOccupation.estTitulaireEmploi()) {	
				return true;
			}
		}
		return false;
	}
	
	/** Utilisee pour l'impression du contrat de travail. Retourne les programmes associes a l'emploi */
	public String programmeEmploi() {
		if (occupationsPourVacation == null || occupationsPourVacation.count() == 0) {
			return "";
		}
		QuotitePourProgramme quotitePourProgramme = new QuotitePourProgramme();
		for (EOOccupation myOccupation : occupationsPourVacation) {
			if (myOccupation.quotite() != null && myOccupation.toEmploi().toProgramme() != null)
				quotitePourProgramme.addQuotitePourProgramme(myOccupation.quotite().floatValue(), myOccupation.toEmploi().toProgramme().cProgramme() + " " + myOccupation.toEmploi().toProgramme().llProgramme());
		}
		
		return quotitePourProgramme.toString();
	}
	// Classe QuotitePourProgramme
	private class QuotitePourProgramme {
		NSMutableDictionary dictQuotites;

		public QuotitePourProgramme() {
			dictQuotites = new NSMutableDictionary();
		}
		public void addQuotitePourProgramme(float quotite,String programme) {
			Float quotiteCourante = (Float)dictQuotites.valueForKey(programme);
			if (quotiteCourante == null) {
				quotiteCourante = new Float(quotite);
			} else {
				quotiteCourante = new Float(quotiteCourante.floatValue() + quotite);
			}
			dictQuotites.takeValueForKey(quotiteCourante, programme);
		}
		public String toString() {
			try {
				String resultat = "";
				// On trie les quotités par ordre décroissant puis on récupère tous les programmes qui ont la quotité courante
				NSMutableArray quotitesTraites = new NSMutableArray();
				NSArray quotites = dictQuotites.allValues().sortedArrayUsingComparator(NSComparator.DescendingNumberComparator);
				for (java.util.Enumeration<Float> e = quotites.objectEnumerator();e.hasMoreElements();) {
					Float quotite = e.nextElement();
					if (quotitesTraites.containsObject(quotite) == false) {
						NSArray keys = dictQuotites.allKeysForObject(quotite);
						keys = keys.sortedArrayUsingComparator(NSComparator.AscendingStringComparator);
						for (java.util.Enumeration e1 = keys.objectEnumerator();e1.hasMoreElements();) {
							NSNumberFormatter formatter = new NSNumberFormatter("0.00");
							formatter.setLocalizesPattern(true);
							resultat = resultat + e1.nextElement() + " (" + formatter.format(quotite) + "%)\n";
						}
						quotitesTraites.addObject(quotite);
					}
				}
				return resultat.substring(0,resultat.length() - 1);	// Pour supprimer le dernier \n
			} catch (Exception exc) {
				exc.printStackTrace();
				return "";
			}
		}
	}
	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		setDCreation(new NSTimestamp());
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		if (estValide() == false)
			return ;
		
		if (dateDebut() == null)
			throw new ValidationException("La date de début de vacation est obligatoire !");

		if (dateFin() == null)
			throw new ValidationException("La date de fin de vacation est obligatoire !");

		if (DateCtrl.isBefore(dateFin(),dateDebut())) {
			throw new NSValidation.ValidationException("La date de début de vacation ne peut pas être postérieure à la date de fin");
		}

		if (nbrHeures() == null)
			throw new ValidationException("Veuillez renseigner le nombre d'heures !");

		if (nbrHeures().floatValue() == 0)
			throw new ValidationException("Le nombre d'heures de vacations ne peut être égal à 0 !");

		setDModification(new NSTimestamp());

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

// EOIndividuFamiliale.java
// Created on Fri Feb 29 10:24:47 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation<BR>
 * Presence des champs obligatoires<BR>
 * Pas de chevauchement des situations<BR>
 * Compatibite de la situation avec la situation precedente
 *
 */
public class EOIndividuFamiliale extends _EOIndividuFamiliale {

	public static EOSortOrdering SORT_DEBUT_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(SORT_DEBUT_DESC);
	public static EOSortOrdering SORT_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_DEBUT_ASC = new NSArray(SORT_DEBUT_ASC);

	public EOIndividuFamiliale() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOIndividuFamiliale creer(EOEditingContext ec, EOIndividu individu) {
		EOIndividuFamiliale newObject = (EOIndividuFamiliale) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setToIndividuRelationship(individu);		
		newObject.setEstValide(true);
		return newObject;		
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOIndividuFamiliale> findForIndividu(EOEditingContext ec,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
			qualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DEBUT_DESC );
		}
		catch (Exception e) {
			return new NSArray<EOIndividuFamiliale>();
		}
	}

	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,DATE_DEBUT_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setDateDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_DEBUT_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,DATE_FIN_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setDateFin(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_FIN_KEY,uneDate);
		}
	}
	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (toIndividu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu");
		}
		if (situationFamiliale() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une situation familiale");
		}
		// La date de fin ne doit pas etre inferieure a la date de debut
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début doit etre définie !");
		} else if (dateFin() != null && dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException("SIT FAMILIALE\nLa date de fin doit être postérieure à la date de début !");
		}
		if (estValide() == false) {
			return;		// pas d'autres validation à faire
		}

		// Vérifier si cette situation en chevauche une autre
		NSArray<EOIndividuFamiliale> situationsPourPeriode = situationsFamilialesPourIndividuEtPeriode(editingContext(), toIndividu(),dateDebut(),dateFin());
		if (situationsPourPeriode.count() > 1) {
			throw new NSValidation.ValidationException("Il y a d'autres situations définies pour cette période"); 
		} else if (situationsPourPeriode.count() == 1) {
			// Vérifier si il ne s'agirait pas de la situation courante ou d'une situation qui vient d'être fermée
			EOIndividuFamiliale situation = (EOIndividuFamiliale)situationsPourPeriode.objectAtIndex(0);
			if (situation != this && 
					((situation.dateFin() != null && DateCtrl.isAfter(situation.dateFin(),dateDebut())) ||
							situation.dateFin() == null && this.dateFin() == null)) {
				throw new NSValidation.ValidationException("Il y a d'autres situations définies pour cette période");
			}
		}
		// Vérifier la situation précédente
		NSArray<EOIndividuFamiliale> situations = findForIndividu(editingContext(), toIndividu());
		situations = EOSortOrdering.sortedArrayUsingKeyOrderArray(situations, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));

		EOIndividuFamiliale situationPrecedente = null;

		for (EOIndividuFamiliale situation : situations) {
			if (situation.dateFin() != null && DateCtrl.isSameDay(DateCtrl.jourSuivant(situation.dateFin()), dateDebut())) {
				situationPrecedente = situation;
				break;
			}
		}
		// Vérifier si il s'agit d'une situation qui se termine le jour précédent
		if (situationPrecedente != null && situationPrecedente.equals(EOSituationFamiliale.NON_PRECISEE) == false) {
			if (situationFamiliale().code().equals(EOSituationFamiliale.CELIBATAIRE)) {
				if (situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.CELIBATAIRE) == false
						&& situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.PACS) == false) {
					throw new NSValidation.ValidationException("Seule une situation non précisée ou de célibataire peut précéder une situation de célibataire");
				}
			} else if (situationFamiliale().code().equals(EOSituationFamiliale.VEUF)) {
				if (situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.CELIBATAIRE)) {
					throw new NSValidation.ValidationException("Une situation de célibataire ne peut précéder une situation de veuvage");
				}
			} else if (situationFamiliale().code().equals(EOSituationFamiliale.DIVORCE)) {
				if (situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.MARIE) == false
						&& situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.SEPARE) == false
						&& situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.PACS) == false) {
					throw new NSValidation.ValidationException("Seule une situation de marié ou séparé peut précéder une situation de divorce !");
				}
			} else if (situationFamiliale().code().equals(EOSituationFamiliale.CONCUBINAGE)) {
				if (situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.MARIE)) {
					throw new NSValidation.ValidationException("Une situation de marié ne peut précéder une situation de concubinage");
				}
			} else if (situationFamiliale().code().equals(EOSituationFamiliale.PACS)) {
				if (situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.MARIE)) {
					throw new NSValidation.ValidationException("Une situation de marié ne peut précéder une situation de pacs");
				}
			} else if (situationFamiliale().code().equals(EOSituationFamiliale.SEPARE)) {
				if (situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.CONCUBINAGE) == false &&
						situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.PACS) == false && 
						situationPrecedente.situationFamiliale().code().equals(EOSituationFamiliale.MARIE) == false) {
					throw new NSValidation.ValidationException("Une situation de séparation ne peut précéder qu'une situation de concubinage ou de pacs");
				}
			}
		}
	}
	// Méthodes statiques
	/** Retourne la situation familiale qui n'a pas de date de fin, c'est donc la courante */
	public static EOIndividuFamiliale situationCourantePourIndividu(EOEditingContext edc,EOIndividu individu) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ AND dFinSitFamiliale = nil AND temValide = 'O'", new NSArray(individu));
			return fetchFirstByQualifier(edc , qualifier);
		} catch (Exception e) {
			return null;
		}
	}
	/** Retourne l'historique des situations familiales d'un individu triees par date decroissante */
	public static NSArray<EOIndividuFamiliale> situationsFamilialesPourIndividuEtPeriode(EOEditingContext edc,EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		try {
			NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ AND temValide = 'O'", new NSArray(individu)));
			if (dateDebut != null) {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin));
			}
			return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DEBUT_DESC);

		}
		catch (Exception e) {
			return new NSArray<EOIndividuFamiliale>();
		}
	}
}

//EOChangementPosition.java
//Created on Fri Feb 14 11:36:46  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit francais et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mémes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;


import java.util.Enumeration;

import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.modele.IDureePourIndividu;
import org.cocktail.mangue.modele.PeriodeAvecQuotite;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	Changement de position
 * Definition des champs<BR>
 * carriere : designe le segment de carriere associe au changement de position, quelque soit la nature de ce dernier<BR>
 * carriereOrigine : non utilise<BR>
 * rne : rne de l'etablissement dans lequel l'agent exerce la position<BR>
 * lieuPosition, txtPosition : position hors education nationale, NULL sinon<BR>
 * rneOrigine : lieu dans lequel l'agent exer&ccedil;ait une position auparavant en cas de detachement vers l'etablissement<BR>
 * lieuPositionOrig, txtPositionOrig : position hors education nationale, en cas de detachement vers l'etablissement<BR>
 * rneOrigine et lieuPositionOrig ne sont definis que dans le cas des detachements
 * <BR>
 * Gestion des changements de position<BR>
 * Toutes les positions ormis "en activite" et "detachement vers l'etablissement" impliquent la fermeture des occupations 
 * a la periode du changement de position<BR>
 * <BR>
 * Gestion des detachements<BR>
 * 1. detachement vers un autre etablissement ou organisme.<BR>
 *		position = 'DETA'<BR>
 *		rneOrigine<BR>
 *		rne ou lieuPosition <BR>
 * rne ­ rneOrigine et rne. cRnePere ­ code rne pour l'etablissement<BR>
 * Mettre fin a l'occupation de l'emploi.<BR>
 * Ne pas mettre fin a l'affectation de l'emploi (pour la gestion des listes electorales)<BR><BR>
 * destachement d'un autre etablissement ou organisme vers  l'etablissement<BR>
 *		position = 'DETA'<BR>
 *		rneOrigine ou ou lieuPositionOrig<BR>
 *		rne <BR>
 * rne ­ rneOrigine et rneOrigine.cRnePere ­ code rne pour l'etablissement<BR>
 * Saisir une occupation<BR>
 * Saisir une affectation<BR><BR>
 * 3. detachement de corps dans le meme etablissement<BR>
 *		position = 'DETA'<BR>
 *		rneOrigine et rne <BR>
 * rneOrigine.cRnePere = code rne pour l'etablissement<BR>
 * Mettre fin a l'occupation<BR>

 * Regles de validation des changements de position : <BR>
 * 	La quotite doit etre fournie et non nulle<BR>
 * 	Verification des longueurs de strings<BR>
 * 	La position doit etre fournie<BR>
 * 	Le motif doit etre fourni pour les positions qui l'exigent<BR>
 *	La date debut doit etre fournie<BR>
 * 	La date de fin doit etre fournie pour les positions ou elle est obligatoire<BR>
 *  Il n'y a pas de chevauchement de changements de position
 * 	Les dates de debut et de fin doivent etre a l'interieur de celles du segment de carriere<BR>
 * 	La duree ne doit pas exceder la duree max fournie dans la position<BR>
 * 	L'agent ne peut pas etre en stage a cette periode si la position ne l'autorise pas<BR>
 *  On ne peut pas supprimer un changement de position si il n'existe pas d'occupation ou d'affectation pour cette periode<BR>
 *  Il ne peut y avoir de changement de position (sauf pour les positions en activite si l'individu est en temps partiel<BR>
 *  La quotite des changements de position sur une periode est superieure ou egale a la quotite d'occupation<BR>
 *	 */

public class EOChangementPosition extends _EOChangementPosition implements IDureePourIndividu {

	public static EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static EOSortOrdering SORT_DATE_FIN_ASC = new EOSortOrdering(DATE_FIN_KEY, EOSortOrdering.CompareAscending);

	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_ASC = new NSArray<EOSortOrdering>(SORT_DATE_DEBUT_ASC);
	public static NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_DESC = new NSArray<EOSortOrdering>(SORT_DATE_DEBUT_DESC);


	private final static NSTimestamp DATE_DECLARATION_ENFANT = DateCtrl.stringToDate("01/01/2004");
	private String cRneEtablissement;

	public EOChangementPosition() {
		super();
	}


	public static EOChangementPosition creer(EOEditingContext ec, EOCarriere carriere) {

		EOChangementPosition newObject = new EOChangementPosition();  
		ec.insertObject(newObject);
		newObject.initAvecCarriere(carriere);
		return newObject;
	}
	public static EOChangementPosition creer(EOEditingContext ec, EOPasse passe) {
		EOChangementPosition newObject = new EOChangementPosition();  
		ec.insertObject(newObject);

		newObject.initAvecPasse(passe);

		return newObject;
	}

	public boolean supportePlusieursTypesEvenement() {
		return true;
	}
	public String typeEvenement() {
		if (toPosition() != null) {
			return toPosition().code();
		} else {
			return "";
		}
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateRef
	 * @return
	 */
	public static EOChangementPosition positionPourDate(EOEditingContext ec, EOIndividu individu, NSTimestamp dateRef) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@",new NSArray (CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY+" =%@", new NSArray(individu)));
		if (dateRef != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateRef, DATE_FIN_KEY, dateRef));
		}
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);				
	}

	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		setTemValide((aBool)?CocktailConstantes.VRAI:CocktailConstantes.FAUX);
	}
	
	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,DATE_DEBUT_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,DATE_DEBUT_KEY,uneDate);
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,DATE_FIN_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,DATE_FIN_KEY,uneDate);
	}
	public String dateArreteFormatee() {
		return SuperFinder.dateFormatee(this,DATE_ARRETE_KEY);
	}
	public void setDateArreteFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,DATE_ARRETE_KEY,uneDate);
	}
	public String lieuOrigine() {
		if (toRneOrigine() != null) {
			return toRneOrigine().libelleLong();
		} else {
			return lieuPositionOrig();
		}
	} 
	public String lieuDestin() {
		if (toRne() != null) {
			return toRne().libelleLong();
		} else {
			return lieuPosition();
		}
	} 
	public boolean peutSuivreStage() {
		return toPosition().temStagiaire() != null && toPosition().temStagiaire().equals(CocktailConstantes.VRAI);
	}

	/** retourne true si il s'agit d'un detachement interne i.e le rne d'origine et le rne d'accueil sont identiques. Le lieu
	 * d'origine ainsi que le lieu de destination ne doivent pas etre fournis. On prend
	 * pas defaut le rne de l'etablissement si rne origine ou rne est nul */
	public boolean estDetachementInterne() {
		if (toPosition().estUnDetachement() == false)
			return false;

		if (lieuPositionOrig() != null || lieuPosition() != null)
			return false;

		// les deux lieux sont nuls
		if (toRneOrigine() == null && toRne() == null) 	// Si rien d'indique, on considere que c'est vrai
			return true;


		return estRneEtablissement(toRneOrigine()) && estRneEtablissement(toRne());
	}

	/**
	 * L'UAI ou le lieu de destination doivent etre renseigne et ne pas correspondre a l'etablissement
	 * 
	 * @return
	 */
	public boolean estDetachementSortant() {

		if (toPosition().estUnDetachement()) {

			if (toRne() == null)
				return lieuPosition() != null;
			else
				return estRneEtablissement(toRneOrigine()) && !estRneEtablissement(toRne());
		}

		return false;

	}

	public boolean estPcAcquitee() {
		return temPcAcquitee() != null && temPcAcquitee().equals(CocktailConstantes.VRAI);
	}
	public void setEstPcAcquitee(boolean aBool) {
		if (aBool) {
			setTemPcAcquitee(CocktailConstantes.VRAI);
		} else {
			setTemPcAcquitee(CocktailConstantes.FAUX);
		}
	}

	public boolean estEnDispo() {
		try {
			return toPosition().estUneDispo();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
	public boolean estDetachement() {
		return toPosition().estUnDetachement();
	}

	public boolean estServiceNational() {
		return toPosition().estServiceNational();
	}
	public boolean estCongeParental() {
		return toPosition().estCongeParental();
	}


	/**
	 * Pour un detachement entrant, la position doit être typée "DETACHEMENT" et l'etablissement d'accueil (toRne)
	 * doit être nul ou egal à l'établissement.
	 * @return
	 */
	public boolean estDetachementEntrant() {

		if (toPosition().estUnDetachement()) {
			if ( toRne() == null || estRneEtablissement(toRne()))  {

				// Si on a saisi un lieu , l'UAI n'est donc pas connu ==> PAS DE DETACHEMENT ENTRANT
				if (toRne() == null && lieuDestin() != null)
					return false;

				return (toRneOrigine() == null || (toRneOrigine() != null && !estRneEtablissement(toRneOrigine())) );
			}			
		}

		return false;
	}

	/** returne true si la suppression du changement de position est possible */
	public boolean sansOccupationOuAffectation() {
		NSArray results = EOOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin());
		if (results.count() > 0) {
			return false;
		}
		results = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin());
		if (results.count() > 0) {
			return false;
		}
		return true;
	}
	/** Retourne true si les dates du changement de position sont a l'interieur de celles de son segment de carriere */
	public boolean datesValides() {

		if (carriere() != null) {
			if (DateCtrl.isBefore(dateDebut(),carriere().dateDebut())) {
				return false;
			}
			if (dateFin() == null) {
				return carriere().dateFin() == null;
			} else if (carriere().dateFin() != null) {
				return DateCtrl.isBeforeEq(dateFin(), carriere().dateFin());
			}
		}
		return true;

	}

	/**
	 * 
	 */
	public void init() {
		setToPositionRelationship(EOPosition.getPositionActivite(editingContext()));
		setTemValide(CocktailConstantes.VRAI);
		setTemPcAcquitee(CocktailConstantes.VRAI);
		setTemoinPositionPrev(CocktailConstantes.FAUX);
		setQuotite(new Integer(100));
	}

	/** Initialise un changement de position avec une carriere
	 * @param carriere
	 */
	public void initAvecCarriere(EOCarriere carriere) {

		init();
		if (carriere != null) {
			setCarriereRelationship(carriere);
			setToIndividuRelationship(carriere.toIndividu());
		}
	}

	public void initAvecPasse(EOPasse passe) {
		init();
		setToPasseRelationship(passe);
		setToIndividuRelationship(passe.individu());
	}
	/** invalide le changement de position et l'evenement associe */
	public void invalider() {

		setEstValide(false);
		if (absence() != null)
			absence().setEstValide(false);
	}

	/** effectue les validations avant enregistrement et mets a jour les occupations/affectations si necessaire */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (estValide() == false) {
			return;		// On ne vérifie pas car les vérifications ont été effectuées lors des précédentes modifications
		}

		if (toPosition() == null)
			throw new NSValidation.ValidationException("Un changement de position doit être associé à une position");

		if (!estEnDispo()  && !estCongeParental() && toRne() == null && lieuPosition() == null)
			throw new NSValidation.ValidationException("Vous devez renseigner soit une UAI soit un lieu de destination pour la position du " + DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dateFin()) + " (RNE : " + toRne() + " , LIEU : " + lieuPosition() + ") !");

		if (lieuPosition() != null && lieuPosition().length() > 80) {
			throw new NSValidation.ValidationException("Le lieu de la nouvelle position ne peut comporter plus de 80 caractères");
		}
		if (lieuPositionOrig() != null && lieuPositionOrig().length() > 80) {
			throw new NSValidation.ValidationException("Le lieu de la position d'origine ne peut comporter plus de 80 caractères");
		}
		if (txtPosition() != null && txtPosition().length() > 80) {
			throw new NSValidation.ValidationException("Le commentaire de la nouvelle position ne peut comporter plus de 80 caractères");
		}
		if (txtPositionOrig() != null && txtPositionOrig().length() > 80) {
			throw new NSValidation.ValidationException("Le commentaire de la position d'origine ne peut comporter plus de 80 caractères");
		}
		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException(String.format(MangueMessagesErreur.ERREUR_NUMERO_ARRETE, "POSITIONS", "20"));
		}
		if (toMotifPosition() == null && toPosition().requiertMotif()) {
			throw new NSValidation.ValidationException("Le motif est obligatoire pour cette position");
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "POSITIONS"));
		} else if ( carriere() != null && DateCtrl.isBefore(dateDebut(),carriere().dateDebut())) {
			throw new NSValidation.ValidationException("La date de début d'un changement de position doit être postérieure à celle du segment de carrière");
		}
		// vérifier si la date de fin est obligatoire pour cette position
		if (dateFin() == null && toPosition().dateFinObligatoire()) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_NON_RENSEIGNE, "POSITIONS"));
		}
		if (dateFin() != null && DateCtrl.isBefore(dateFin(),dateDebut())) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "POSITIONS", dateFin(), dateDebut()));
		}
		String message = validationsCir();
		if (message != null && message.length() > 0) {
			throw new NSValidation.ValidationException(message);
		}
		// pas de chevauchements de position
		verifierChevauchements();

		if (carriere() != null && carriere().dateFin() != null) {
			if (dateFin() == null) {
				throw new NSValidation.ValidationException("La carrière associée ayant une date de fin, vous devez en fournir une pour la position !");
			} else if (DateCtrl.isAfter(dateFin(),carriere().dateFin())) {
				throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à celle du segment de carrière");
			}
		}
		//	vérifier si la durée dépasse la durée max. définie dans la position
		if (toPosition().dureeMaxPeriodPos() != null) {
			int dureeMax = toPosition().dureeMaxPeriodPos().intValue();
			Integer dureeChangement = DateCtrl.calculerDureeEnMois(dateDebut(),dateFin());
			if (dureeChangement == null || dureeChangement.intValue()  > dureeMax) {
				throw new NSValidation.ValidationException("Ce changement de position ne peut pas durer plus de " + dureeMax + " mois");
			}
		}

		// Vérifier si il y a un temps partiel en cours pendant la période
		if (!toPosition().estEnActivite()) {
			NSArray<EOTempsPartiel> tempsPartiels = EOTempsPartiel.rechercherTempsPartielPourIndividuEtPeriode(editingContext(), individu(), dateDebut(), dateFin());
			if (tempsPartiels.count() > 0) {
				throw new NSValidation.ValidationException("Une personne à temps partiel ne peut avoir la position " + toPosition().libelleLong() + ".\nVeuillez d'abord interrompre le temps partiel de cette personne");
			}
		}

		// toutes les positions autres qu'activitéé et détachement entraînent la fermeture de l'occupation
		if (!toPosition().estEnActivite() && !estDetachementEntrant()) {
			fermerOccupations();
			fermerAffectations();
		}

		// Verification de l'age de l'enfant pour une position de DISPO pour enfant de moins de 8 ans
		if ( (toMotifPosition() != null && toMotifPosition().code().equals("EE")) || toPosition().estCongeParental() ) {		

			if (enfant() == null)
				throw new NSValidation.ValidationException("Pour ce motif de position, veuillez associer un enfant !");				

			NSTimestamp dateLimite = DateCtrl.timestampByAddingGregorianUnits(enfant().dNaissance(),8, 0, 0, 0, 0, 0);			

			// L'enfant doit etre né au début de la dispo
			if (DateCtrl.isBefore(dateDebut(), enfant().dNaissance())) {
				throw new NSValidation.ValidationException(" L'enfant n'était pas né le " + dateDebutFormatee() + " !");				
			}

			// La fin de la dispo doit se trouver après les 8 ans de l'enfant.
			if (DateCtrl.isAfter(dateFin(), dateLimite)) {
				throw new NSValidation.ValidationException("L'enfant doit avoir moins de 8 ans pendant toute la durée du congé parental !");				
			}
		}

		if (temPcAcquitee() == null)
			setTemPcAcquitee(CocktailConstantes.FAUX);

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());		
	}

	/** En cas de detachement, un lieu de destination ou (exclusif) un rne doivent etre fournis.<BR>
	 * Si la position concerne les enfants, l'enfant doit etre fourni.
	 * @return
	 */
	public String validationsCir() {
		String temp = "";
		if (toPosition().estUnDetachement() && lieuDestin() == null) {
			temp = "Pour un détachement, le changement de position doit fournir un rne ou un lieu de destination";
		}

		if (toPosition().requiertEnfant()) {

			if (DateCtrl.isAfterEq(dateDebut(),DATE_DECLARATION_ENFANT)  && enfant() == null) {
				temp = "La position " + toPosition().libelleLong() + " débutant le " + dateDebutFormatee() + " requiert que l'enfant soit fourni !";
			}
		}
		if (toMotifPosition() != null && toMotifPosition().requiertEnfant() && enfant() == null) { // 16/03/2011 ajout enfant == null
			temp = "Le motif de position ( " + toMotifPosition().libelleLong() + ") requiert que l'enfant soit fourni !";
		}
		return temp;
	}


	// méthodes statiques
	/** retourne true si un individu peut avoir un contrat pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param dateDebut	
	 * @param dateFin	peut etre nul
	 */
	public static boolean peutAvoirContratSurPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		NSArray changements =  rechercherChangementsPourIndividuEtPeriode(editingContext,individu,dateDebut,dateFin);
		if (changements.count() == 0) {
			return true;
		}
		for (java.util.Enumeration<EOChangementPosition>e = changements.objectEnumerator();e.hasMoreElements();) {
			EOChangementPosition changement = e.nextElement();
			if (changement.toPosition().autoriseContrat() == false) {
				return false;
			}
		}
		return true;
	}


	/**
	 * 
	 * @param ec
	 * @param carriere
	 * @return
	 */
	public static NSArray<EOChangementPosition> rechercherChangementsPourCarriere(EOEditingContext ec, EOCarriere carriere) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = 'O'",null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CARRIERE_KEY + " = %@", new NSArray(carriere)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@", new NSArray(carriere.toIndividu())));

		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
	}


	/**
	 * 
	 * @param edc
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray<EOChangementPosition> findForPeriode(EOEditingContext edc,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOChangementPosition.TEM_VALIDE_KEY + " = 'O'",null));

		if (debutPeriode != null) {
			andQualifiers.addObject(SuperFinder.qualifierPourPeriode(EOChangementPosition.DATE_DEBUT_KEY,debutPeriode, EOChangementPosition.DATE_FIN_KEY,finPeriode));
		}

		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOChangementPosition.TO_INDIVIDU_KEY + ".noIndividu",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(EOChangementPosition.DATE_DEBUT_KEY,EOSortOrdering.CompareDescending));

		return EOChangementPosition.fetchAll(edc, new EOAndQualifier(andQualifiers), null);

	}


	/** Recherche les changements de position des types de position passes en parametre d'un individu  pour une periode classes
	 * par ordre de debut croissant 
	 * @param individu
	 * @param positions peut etre nul
	 * @param debutPeriode debut de la periode recherchee (peut etre nulle)
	 * @param finPeriode fin de la periode recherchee (peut etre nulle)
	 * */
	public static NSArray rechercherChangementsPourIndividuPositionsEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSArray positions,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray args = new NSMutableArray(individu);
		String stringQualifier = TO_INDIVIDU_KEY + " = %@ AND " + TEM_VALIDE_KEY + " = 'O'";
		NSMutableArray qualifiers = new NSMutableArray();
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		qualifiers.addObject(qualifier);
		if (debutPeriode != null) {
			qualifier = SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode);
			qualifiers.addObject(qualifier);
		}
		if (positions != null && positions.count() > 0) {
			NSMutableArray qualifiers1 = new NSMutableArray();
			java.util.Enumeration<EOPosition> e = positions.objectEnumerator();
			while (e.hasMoreElements()) {
				EOPosition position = e.nextElement();
				qualifiers1.addObject(EOQualifier.qualifierWithQualifierFormat(TO_POSITION_KEY + " = %@", new NSArray(position)));
			}
			qualifiers.addObject(new EOOrQualifier(qualifiers1));
		}
		qualifier = new EOAndQualifier(qualifiers);

		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,SORT_ARRAY_DATE_DEBUT_ASC);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/** Recherche les changements de position d'un individu pour une periode classes
	 * par ordre de debut croissant 
	 * @param individu
	 * @param debutPeriode debut de la periode recherchee (peut etre nulle)
	 * @param finPeriode fin de la periode recherchee (peut etre nulle)
	 */
	public static NSArray<EOChangementPosition> rechercherChangementsPourIndividuEtPeriode(EOEditingContext edc,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_ASC );

	}


	/**
	 * 
	 * @param edc
	 * @param passe
	 * @return
	 */
	public static EOChangementPosition findForPasse(EOEditingContext edc, EOPasse passe) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_PASSE_KEY + "=%@", new NSArray(passe)));

		return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));

	}

	/** Recherche les changements de position d'un individu pour une periode classes
	 * par ordre de debut croissant 
	 * @param individu
	 * @param debutPeriode debut de la periode recherchee (peut etre nulle)
	 * @param finPeriode fin de la periode recherchee (peut etre nulle)
	 */
	public static EOChangementPosition findChangementPourDate(EOEditingContext edc, EOIndividu individu, NSTimestamp dateReference) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CARRIERE_KEY+"." + EOCarriere.TEM_CIR_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateReference, DATE_FIN_KEY, dateReference));

		return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));

	}

	/** Recherche les changements de position d'un individu pour une periode classes
	 * par ordre de debut croissant 
	 * @param individu
	 * @param debutPeriode debut de la periode recherchee (peut etre nulle)
	 * @param finPeriode fin de la periode recherchee (peut etre nulle)
	 */
	public static NSArray<EOChangementPosition> findDetachementsForPeriode(EOEditingContext edc,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CARRIERE_KEY+"."+EOCarriere.TEM_CIR_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_POSITION_KEY + "=%@", new NSArray(EOPosition.getPositionDetachement(edc))));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_ASC );

	}

	/** Recherche les changements de position d'un individu pour une periode classes
	 * par ordre de debut croissant 
	 * @param individu
	 * @param debutPeriode debut de la periode recherchee (peut etre nulle)
	 * @param finPeriode fin de la periode recherchee (peut etre nulle)
	 */
	public static NSArray<EOChangementPosition> rechercherChangementsCirPourIndividuEtPeriode(EOEditingContext edc,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CARRIERE_KEY+"."+EOCarriere.TEM_CIR_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));
		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_ASC );

	}
	/** Recherche les changements de position d'un individu pour une periode classes
	 * par ordre de debut croissant 
	 * @param individu
	 * @param debutPeriode debut de la periode recherchee (peut etre nulle)
	 * @param finPeriode fin de la periode recherchee (peut etre nulle)
	 */
	public static NSArray<EOChangementPosition> rechercherChangementsCirPassePourIndividuEtPeriode(EOEditingContext edc,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_PASSE_KEY + " != nil", null));		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));
		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_ASC );

	}

	/** recherche les affectations valides selon certains criteres
	 * @param editingContext
	 * @param qualifier
	 * @param relation a prefetcher
	 * @return tableau des affectations
	 */
	public static NSArray rechercherChangementsAvecCriteres(EOEditingContext ec,EOQualifier qualifier) {
		return fetchAll(ec, qualifier, SORT_ARRAY_DATE_DEBUT_DESC);
	}

	/** Recherche les changements de position d'un individu avec une position en activite ou en detachement interne pour une periode classees
	 * par ordre de debut decroissant
	 * @param individu
	 * @param debutPeriode debut de la periode recherchee (peut etre nulle)
	 * @param finPeriode fin de la periode recherchee (peut etre nulle)
	 */
	public static NSArray<EOChangementPosition> rechercherChangementsEnActivitePourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_POSITION_KEY+"."+EOPosition.TEM_ACTIVITE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));		
		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		NSArray changements = editingContext.objectsWithFetchSpecification(myFetch);
		NSMutableArray results = new NSMutableArray();
		for (Enumeration<EOChangementPosition> e = changements.objectEnumerator();e.hasMoreElements();) {
			EOChangementPosition changement = (EOChangementPosition)e.nextElement();
			if (changement.toPosition().estEnActiviteDansEtablissement() || 
					(changement.toPosition().estUnDetachement() 
							&& (changement.estDetachementInterne() || changement.estDetachementEntrant()))) {
				results.addObject(changement);
			}
		}
		return results;
	}


	/**
	 * 
	 * Recherche de toutes les positions d'activite d'un individu en fonction d'une periode donnee
	 * 
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray rechercherChangementsActivitePourPeriode(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSArray mySort = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));

		try {

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray("O")));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_PASSE_KEY + " = nil", null));

			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(
					EOQualifier.qualifierWithQualifierFormat(TO_POSITION_KEY + "." + EOPosition.TEM_ACTIVITE_KEY + " = %@", new NSArray("O")));
			orQualifiers.addObject(
					EOQualifier.qualifierWithQualifierFormat(TO_POSITION_KEY + "." + EOPosition.TEM_DETACHEMENT_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			qualifiers.addObject(new EOOrQualifier(orQualifiers));

			if (dateFin != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@", new NSArray(dateFin)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + " = nil or " + DATE_FIN_KEY + " >= %@", new NSArray(dateDebut)));

			EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers),mySort);

			return ec.objectsWithFetchSpecification(myFetch);

		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}

	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOChangementPosition> rechercherChangementsPourPeriode(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSArray mySort = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));

		try {

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI)));

			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin));

			//			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_DEB_POSITION_KEY + " <= %@", new NSArray(dateFin)));
			//
			//			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FIN_POSITION_KEY + " = nil or " + D_FIN_POSITION_KEY + " >= %@", new NSArray(dateDebut)));

			EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers),mySort);

			return ec.objectsWithFetchSpecification(myFetch);

		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}

	}

	/** Recherche les changements de position d'un individu avec une position en activite ou en détachement pour une periode classes
	 * par ordre de debut decroissant */
	public static NSArray rechercherChangementsEnActiviteOuDetachementPourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_POSITION_KEY+"."+EOPosition.TEM_ACTIVITE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);

	}

	public static EOChangementPosition rechercherPourAbsence(EOEditingContext ec, EOAbsences absence) {
		return fetchFirstByQualifier(ec, EOQualifier.qualifierWithQualifierFormat(ABSENCE_KEY + "=%@", new NSArray(absence)));
	}

	/** Recherche les changements de position d'un individu pour le service national */
	public static NSArray rechercherChangementsServiceNationalPourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		NSMutableArray args = new NSMutableArray(individu);
		String stringQualifier = "individu = %@ AND temValide = 'O' AND toPosition.code = 'SNAT'";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
	/** Recherche les changements de position d'un individu pour le conge stagiaire pendant la periode */
	public static NSArray rechercherChangementsCongeStagiairePourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return positionsPourTypePendantPeriode(editingContext, individu, "CGST", debutPeriode, finPeriode);
	}
	/** Recherche si un individu a une position de Service Nationale pendant une periode */
	public static boolean individuAuServiceMilitairePourPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return individuOccupePositionPendantPeriode(editingContext, individu, "SNAT", debutPeriode, finPeriode);
	}
	/** Recherche si un individu a une position de Conge Stagiaire pendant une periode */
	public static boolean individuEnCongeStagiairePourPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return individuOccupePositionPendantPeriode(editingContext, individu, "CGST", debutPeriode, finPeriode);
	}
	/** Recherche  tous les changements de position d'un individu avec une position en activite classes
	 * par ordre de debut decroissant */
	public static NSArray rechercherChangementsEnActivitePourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		return rechercherChangementsEnActivitePourIndividuEtPeriode(editingContext, individu, null, null);
	}

	/** Retourne true si l'agent est en activite pendant cette periode
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode (peut etre nulle)
	 * @return true si en activite
	 */
	public static boolean individuEnActivitePendantPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return rechercherChangementsEnActivitePourIndividuEtPeriode(editingContext,individu,debutPeriode,finPeriode).count() > 0;
	}
	/** Retourne true si l'agent est fonctionnaire et en activite pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode (peut etre nulle)
	 * @return true si en activite et fonctionnaire
	 */
	public static boolean fonctionnaireEnActivitePendantPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return estDeTypePendantPeriode(editingContext,individu,debutPeriode,finPeriode,true);
	}
	/** Retourne true si l'agent est toujours en activite 
	 *  @param editingContext
	 * @param individu
	 * @return true si en activite et fonctionnaire
	 */
	public static boolean fonctionnaireEnActivite(EOEditingContext editingContext,EOIndividu individu) {
		return estDeType(editingContext,individu,true);
	}
	/** Retourne true si l'agent est assimile contractuel et en activite pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode (peut etre nulle)
	 * @return true si est assimle contractuel et en activite
	 */
	public static boolean assimileContractuelEnActivitePendantPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return estDeTypePendantPeriode(editingContext,individu,debutPeriode,finPeriode,false);
	}
	/** Retourne true si l'agent est fonctionnaire et en activite pendant la periode complete
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode (peut etre nulle)
	 * @return true si en activite et fonctionnaire
	 */
	public static boolean fonctionnaireEnActivitePendantPeriodeComplete(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return estDeTypePendantPeriodeComplete(editingContext,individu,debutPeriode,finPeriode,true);
	}
	/** Retourne true si l'agent est assimile contractuel et en activite pendant la periode complete
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode (peut etre nulle)
	 * @return true si est assimle contractuel et en activite
	 */
	public static boolean assimileContractuelEnActivitePendantPeriodeComplete(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return estDeTypePendantPeriodeComplete(editingContext,individu,debutPeriode,finPeriode,false);
	}


	// méthodes privées
	private boolean estRneEtablissement(EORne rne) {

		if (cRneEtablissement == null) {	// On le stocke pour ne pas le charger à chaque fois
			cRneEtablissement = EOGrhumParametres.getDefaultRne();
		}

		if (rne == null || rne.code().equals(cRneEtablissement)) {
			return true;
		}
		// Vérifier si le rne est celui d'un fils l'établissement
		NSArray<String> rneEtablissements = EOStructure.rneEtablissements(editingContext());
		for (String cRne : rneEtablissements) {
			if (rne.toRnePere() != null && rne.toRnePere().code() != null && rne.toRnePere().code().equals(cRne)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 */
	private void fermerOccupations() {
		NSArray<EOOccupation> occupations = EOOccupation.findForIndividuAndDate(editingContext(), individu(),dateDebut());
		for (EOOccupation occupation : occupations) {
			if (DateCtrl.isSameDay(occupation.dateDebut(), dateDebut())) {
				occupation.setDateFin(dateDebut());
			} else {
				occupation.setDateFin(DateCtrl.jourPrecedent(dateDebut()));
			}
		}
	}

	/**
	 * 
	 */
	private void fermerAffectations() {
		NSArray<EOAffectation> affectations = EOAffectation.rechercherAffectationsADate(editingContext(),individu(),dateDebut());
		for (EOAffectation affectation : affectations) {
			affectation.fermer(DateCtrl.jourPrecedent(dateDebut()));			
		}
	}


	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void verifierChevauchements() throws NSValidation.ValidationException {
		// pas de chevauchements de position : vérifier si la quotité totale sur la période est > 100
		NSArray<EOChangementPosition> changementsPosition = rechercherChangementsPourIndividuEtPeriode(editingContext(), individu(), dateDebut(), dateFin());

		NSMutableArray<PeriodeAvecQuotite> periodesAvecQuotite = new NSMutableArray<PeriodeAvecQuotite>();
		for (EOChangementPosition changement : changementsPosition) {
			periodesAvecQuotite.addObject(new PeriodeAvecQuotite(changement.dateDebut(),changement.dateFin(), quotite()));
		}
		if (changementsPosition.containsObject(this) == false) {
			periodesAvecQuotite.addObject(new PeriodeAvecQuotite(dateDebut(),dateFin(), quotite()));
		}
		// Vérifier la quotité totale
		Number quotiteTotale = PeriodeAvecQuotite.calculerQuotiteTotale(periodesAvecQuotite);
		if (quotiteTotale != null && quotiteTotale.doubleValue() > 100.00) {
			throw new NSValidation.ValidationException("La quotité des changements de position (" + quotiteTotale + "%) pendant cette période est > 100%");
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param position
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	private static NSArray<EOChangementPosition> positionsPourTypePendantPeriode(EOEditingContext edc,EOIndividu individu, String position, NSTimestamp debutPeriode, NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " =%@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " =%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_POSITION_KEY + "." + EOPosition.CODE_KEY + " =%@", new NSArray(position)));

		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		}

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param position
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	private static boolean individuOccupePositionPendantPeriode(EOEditingContext editingContext,EOIndividu individu,String position,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray<EOChangementPosition> changements = positionsPourTypePendantPeriode(editingContext, individu, position, debutPeriode, finPeriode);
		return changements != null && changements.size() > 0;
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param estTypeFonctionnaire
	 * @return
	 */
	private static boolean estDeType(EOEditingContext editingContext,EOIndividu individu,boolean estTypeFonctionnaire) {

		NSArray<EOChangementPosition> changements = rechercherChangementsEnActivitePourIndividu(editingContext,individu);
		if (changements.count() == 0) {
			return false;
		}
		for (EOChangementPosition changement : changements) {
			if (changement.carriere() != null && changement.carriere().toTypePopulation().estFonctionnaire() != estTypeFonctionnaire) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @param estTypeFonctionnaire
	 * @return
	 */
	private static boolean estDeTypePendantPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean estTypeFonctionnaire) {
		NSArray<EOChangementPosition> changements = rechercherChangementsEnActivitePourIndividuEtPeriode(editingContext,individu,debutPeriode,finPeriode);
		for (EOChangementPosition changement : changements) {
			if (changement.carriere() != null && changement.carriere().toTypePopulation().estFonctionnaire() == estTypeFonctionnaire) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @param estTypeFonctionnaire
	 * @return
	 */
	private static boolean estDeTypePendantPeriodeComplete(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean estTypeFonctionnaire) {
		
		NSArray<EOChangementPosition> changements = rechercherChangementsEnActivitePourIndividuEtPeriode(editingContext,individu,debutPeriode,finPeriode);
		changements = EOSortOrdering.sortedArrayUsingKeyOrderArray(changements, EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);
		
		NSTimestamp dateDebut = null,dateFin = null;
				
		for (EOChangementPosition changement : changements) {

			if (changement.toPasse() != null || changement.carriere().toTypePopulation().estFonctionnaire() == estTypeFonctionnaire) {
				
				if (dateDebut == null) {
					dateDebut = changement.dateDebut();
				}
				if (dateFin == null) {
					dateFin = changement.dateFin();
				} else if  (changement.dateFin() == null || DateCtrl.isAfter(changement.dateFin(),dateFin)) {
					dateFin = changement.dateFin();
				}
			}
		}
		
		return dateDebut != null && DateCtrl.isBeforeEq(dateDebut,debutPeriode) && (dateFin == null || DateCtrl.isAfterEq(dateFin,finPeriode));
	}


	public EOIndividu individu() {
		// TODO Auto-generated method stub
		return toIndividu();
	}
	
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu.medical;

import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public abstract class InfoMedicalePourIndividu extends EOGenericRecord {

	
	public static final String DATE_KEY = "date";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";

	public static final String ADRESSE_KEY = "adresse";
	public static final String INDIVIDU_KEY = "individu";
	public static final String MEDECIN_KEY = "medecin";

	public InfoMedicalePourIndividu() {
	        super();
	    }  
	 public NSTimestamp date() {
	        return (NSTimestamp)storedValueForKey(DATE_KEY);
	    }

	    public void setDate(NSTimestamp value) {
	        takeStoredValueForKey(value, DATE_KEY);
	    }

	    public org.cocktail.mangue.modele.grhum.referentiel.EOAdresse adresse() {
	        return (org.cocktail.mangue.modele.grhum.referentiel.EOAdresse)storedValueForKey(ADRESSE_KEY);
	      }

	      public void setAdresseRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOAdresse value) {
	        if (value == null) {
	        	org.cocktail.mangue.modele.grhum.referentiel.EOAdresse oldValue = adresse();
	        	if (oldValue != null) {
	        		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_KEY);
	          }
	        } else {
	        	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_KEY);
	        }
	      }
	      
	      public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
	        return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
	      }

	      public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
	        if (value == null) {
	        	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
	        	if (oldValue != null) {
	        		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
	          }
	        } else {
	        	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
	        }
	      }
	      
	      public org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail medecin() {
	        return (org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail)storedValueForKey(MEDECIN_KEY);
	      }

	      public void setMedecinRelationship(org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail value) {
	        if (value == null) {
	        	org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail oldValue = medecin();
	        	if (oldValue != null) {
	        		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MEDECIN_KEY);
	          }
	        } else {
	        	addObjectToBothSidesOfRelationshipWithKey(value, MEDECIN_KEY);
	        }
	      }

	      public NSTimestamp dCreation() {
		        return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
		      }

	      public void setDCreation(NSTimestamp value) {
	        takeStoredValueForKey(value, D_CREATION_KEY);
	      }

	      public NSTimestamp dModification() {
	        return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	      }

	      public void setDModification(NSTimestamp value) {
	        takeStoredValueForKey(value, D_MODIFICATION_KEY);
	      }

	    // Méthodes ajoutées
	    public void initAvecIndividu(EOIndividu individu) {
	    	setIndividuRelationship(individu);
	    }
	    public void supprimerRelations() {
			removeObjectFromBothSidesOfRelationshipWithKey(type(), nomRelationType());
			setIndividuRelationship(null);
			setMedecinRelationship(null);
			setAdresseRelationship(null);
	    }
		public String dateFormatee() {
			return SuperFinder.dateFormatee(this,DATE_KEY);
		}
		public void setDateFormatee(String uneDate) {
			if (uneDate == null) {
				setDate(null);
			} else {
				SuperFinder.setDateFormatee(this,DATE_KEY,uneDate);
			}
		}
		public abstract String nomRelationType();
		/** Retourne le type d'info m&eacute;dicale */
		public EOGenericRecord type() {
			return (EOGenericRecord)valueForKey(nomRelationType());
		}
		/** Retourne le libelle du type d'info medicale */
		public String libelleType() {
			return (type() == null) ? null : (String)type().valueForKey("libelle");	
		}
		public void validateForSave() throws NSValidation.ValidationException {
			if (date() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir la date de l'examen");
			}
			if (individu() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir un individu");
			}
		}
		public String statutPourDate(NSTimestamp date) {
			if (individu().estTitulaireSurPeriodeComplete(date, date)) {
				return "Titulaire";
			}
			else {
				return "Contractuel";
			}
		}
		public String positionPourDate(NSTimestamp date) {
			NSArray changements = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext(), individu(), date, date);
			if (changements.count() > 0) {
				return ((EOChangementPosition)changements.objectAtIndex(0)).toPosition().libelleLong();
			} else {
				return "";
			}
		}
		public String congePourDate(NSTimestamp date) {
			NSArray absences = EOAbsences.rechercherAbsencesPourIndividuEtPeriode(editingContext(), individu(), date, date);
			if (absences.count() > 0) {
				return ((EOAbsences)absences.objectAtIndex(0)).toTypeAbsence().libelleLong();
			} else {
				return "";
			}
		}
		public String composantePourDate(NSTimestamp date) {
			NSArray affectations = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext(), individu(), date, date, true);
			// Les trier par quotité décroissante, pour avoir les activités les plus importantes en tête
			if (affectations.count() > 0) {
				affectations = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations, EOAffectation.SORT_ARRAY_QUOTITE_DESC);
				EOStructure structure = ((EOAffectation)affectations.objectAtIndex(0)).toStructureUlr();
				if (structure.estComposante()) {
					return structure.llStructure();
				} else {
					EOStructure composante = structure.toComposante();
					if (composante != null) {
						return composante.llStructure();
					} else {
						return "";
					}
				}
			} else {
				return "";
			}
		}
		public String servicePourDate(NSTimestamp date) {
			NSArray affectations = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext(), individu(), date, date, true);
			// Les trier par quotité décroissante, pour avoir les activités les plus importantes en tête
			if (affectations.count() > 0) {
				affectations = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations,  EOAffectation.SORT_ARRAY_QUOTITE_DESC);
				EOStructure structure = ((EOAffectation)affectations.objectAtIndex(0)).toStructureUlr();
				if (structure.isService()) {
					return structure.llStructure();
				} else {
						return "";
				}
			} else {
				return "";
			}
		}
		// Méthodes statiques
		/** Retourne les informations m&eacute;dicales d'un individu tri&eacute;es par date d&eacute;croissante
		 * @param editingContext
		 * @param nomEntiteInfoMedicale (ExamenMedical, Vaccin, VisiteMedicale)
		 * @param individu
		 */
		public static NSArray rechercherInfosMedicalesPourIndividu(EOEditingContext editingContext, String nomEntiteInfoMedicale, EOIndividu individu) {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@", new NSArray(individu));
			EOFetchSpecification fs = new EOFetchSpecification(nomEntiteInfoMedicale,qualifier,new NSArray(EOSortOrdering.sortOrderingWithKey("date",EOSortOrdering.CompareDescending)));
			return editingContext.objectsWithFetchSpecification(fs);
		}
		/** Retourne les informations m&eacute;dicales des individus tri&eacute;es par nom usuel et par date d&eacute;croissante
		 * @param editingContext
		 * @param nomEntiteInfoMedicale (ExamenMedical, Vaccin, VisiteMedicale)
		 * @param debutPeriode (peut &ecir;tre nulle)
		 * @param finPeriode (peut &ecir;tre nulle)
		 */
		public static NSArray rechercherInfosMedicalesPourPeriode(EOEditingContext editingContext, String nomEntiteInfoMedicale, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
			return rechercherInfosMedicalesPourPeriodeEtIndividus(editingContext, nomEntiteInfoMedicale, debutPeriode, finPeriode,null,null);
		}
		/** Retourne les informations m&eacute;dicales des individus tri&eacute;es par nom usuel et par date d&eacute;croissante
		 * @param editingContext
		 * @param nomEntiteInfoMedicale (ExamenMedical, Vaccin, VisiteMedicale)
		 * @param debutPeriode (peut &ecir;tre nulle)
		 * @param finPeriode (peut &ecir;tre nulle)
		 * @param nom (peut &ecir;tre nul)
		 * @param prenom (peut &ecirc;tre nul)
		 */
		public static NSArray rechercherInfosMedicalesPourPeriodeEtIndividus(EOEditingContext editingContext, String nomEntiteInfoMedicale, NSTimestamp debutPeriode,NSTimestamp finPeriode,String nom,String prenom) {
			NSMutableArray qualifiers = new NSMutableArray();
			if (debutPeriode != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("date >= %@", new NSArray(debutPeriode)));
			}
			if (finPeriode != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("date <= %@", new NSArray(finPeriode)));
			}
			if (nom != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("individu.nomUsuel caseinsensitivelike %@", new NSArray(nom + "*")));
			}
			if (prenom != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("individu.prenom caseinsensitivelike %@", new NSArray(prenom + "*")));
			}
			EOQualifier qualifier = new EOAndQualifier(qualifiers);
			NSMutableArray sorts = new NSMutableArray();
			sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel",EOSortOrdering.CompareAscending));
			sorts.addObject(EOSortOrdering.sortOrderingWithKey("date",EOSortOrdering.CompareDescending));
			EOFetchSpecification fs = new EOFetchSpecification(nomEntiteInfoMedicale,qualifier,sorts);
			return editingContext.objectsWithFetchSpecification(fs);
		}
}

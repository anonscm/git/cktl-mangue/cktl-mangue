// _EOVaccin.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVaccin.java instead.
package org.cocktail.mangue.modele.mangue.individu.medical;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOVaccin extends  InfoMedicalePourIndividu {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Vaccin";
	public static final String ENTITY_TABLE_NAME = "MANGUE.VACCIN";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "vaccOrdre";

	public static final String DATE_KEY = "date";
	public static final String DATE_RAPPEL_KEY = "dateRappel";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String HEURE_KEY = "heure";

// Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String VACC_ORDRE_KEY = "vaccOrdre";
	public static final String MTRA_ORDRE_KEY = "mtraOrdre";
	public static final String TVAC_ORDRE_KEY = "tvacOrdre";
	public static final String RVAC_ORDRE_KEY = "rvacOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_COLKEY = "VACC_DATE";
	public static final String DATE_RAPPEL_COLKEY = "VACC_DATE_RAPPEL";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String HEURE_COLKEY = "VACC_HEURE";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String VACC_ORDRE_COLKEY = "VACC_ORDRE";
	public static final String MTRA_ORDRE_COLKEY = "MTRA_ORDRE";
	public static final String TVAC_ORDRE_COLKEY = "TVAC_ORDRE";
	public static final String RVAC_ORDRE_COLKEY = "RVAC_ORDRE";


	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String INDIVIDU_KEY = "individu";
	public static final String MEDECIN_KEY = "medecin";
	public static final String RAPPEL_VACCIN_KEY = "rappelVaccin";
	public static final String TYPE_VACCIN_KEY = "typeVaccin";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp date() {
    return (NSTimestamp) storedValueForKey(DATE_KEY);
  }

  public void setDate(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_KEY);
  }

  public NSTimestamp dateRappel() {
    return (NSTimestamp) storedValueForKey(DATE_RAPPEL_KEY);
  }

  public void setDateRappel(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_RAPPEL_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String heure() {
    return (String) storedValueForKey(HEURE_KEY);
  }

  public void setHeure(String value) {
    takeStoredValueForKey(value, HEURE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOAdresse adresse() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOAdresse)storedValueForKey(ADRESSE_KEY);
  }

  public void setAdresseRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOAdresse value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail medecin() {
    return (org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail)storedValueForKey(MEDECIN_KEY);
  }

  public void setMedecinRelationship(org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.medical.EOMedecinTravail oldValue = medecin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MEDECIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MEDECIN_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.medical.EORappelVaccin rappelVaccin() {
    return (org.cocktail.mangue.common.modele.nomenclatures.medical.EORappelVaccin)storedValueForKey(RAPPEL_VACCIN_KEY);
  }

  public void setRappelVaccinRelationship(org.cocktail.mangue.common.modele.nomenclatures.medical.EORappelVaccin value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.medical.EORappelVaccin oldValue = rappelVaccin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RAPPEL_VACCIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RAPPEL_VACCIN_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeVaccin typeVaccin() {
    return (org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeVaccin)storedValueForKey(TYPE_VACCIN_KEY);
  }

  public void setTypeVaccinRelationship(org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeVaccin value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeVaccin oldValue = typeVaccin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_VACCIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_VACCIN_KEY);
    }
  }
  

/**
 * Créer une instance de EOVaccin avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVaccin createEOVaccin(EOEditingContext editingContext, NSTimestamp date
, NSTimestamp dCreation
, NSTimestamp dModification
			) {
    EOVaccin eo = (EOVaccin) createAndInsertInstance(editingContext, _EOVaccin.ENTITY_NAME);    
		eo.setDate(date);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  
	  public EOVaccin localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVaccin)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVaccin creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVaccin creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVaccin object = (EOVaccin)createAndInsertInstance(editingContext, _EOVaccin.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVaccin localInstanceIn(EOEditingContext editingContext, EOVaccin eo) {
    EOVaccin localInstance = (eo == null) ? null : (EOVaccin)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVaccin#localInstanceIn a la place.
   */
	public static EOVaccin localInstanceOf(EOEditingContext editingContext, EOVaccin eo) {
		return EOVaccin.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVaccin fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVaccin fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVaccin eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVaccin)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVaccin fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVaccin fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVaccin eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVaccin)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVaccin fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVaccin eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVaccin ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVaccin fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

// EOVaccin.java
// Created on Wed Sep 10 08:18:46 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.mangue.individu.medical;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOVaccin extends _EOVaccin {

    public EOVaccin() {
        super();
    }

	public static EOVaccin findForKey( EOEditingContext ec, Number key) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(VACC_ORDRE_KEY + "=%@", new NSArray(key)));
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}

	public static EOVaccin creer(EOEditingContext ec, EOIndividu individu) {
		EOVaccin newObject = (EOVaccin) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setDCreation(new NSTimestamp());
		return newObject;
	}

    public void supprimerRelations() {
		super.supprimerRelations();
		setRappelVaccinRelationship(null);
	}
	public String nomRelationType() {
		return TYPE_VACCIN_KEY;
	}
	public String dateRappelFormatee() {
		return SuperFinder.dateFormatee(this,DATE_RAPPEL_KEY);
	}
	public void setDateRappelFormatee(String uneDate) {
		if (uneDate == null) {
			setDateRappel(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_RAPPEL_KEY,uneDate);
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		
		if (date() == null) {
			throw new NSValidation.ValidationException("La date de convocation est obligatoire !");			
		}
		
		if (DateCtrl.estHeureValide(heure(),false) == false) {
			throw new NSValidation.ValidationException("L'heure n'a pas un format valide (HH:MM) !");
		}
		setDModification(new NSTimestamp());
	}
}

// EOVisiteMedicale.java
// Created on Wed Sep 10 12:35:51 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.mangue.individu.medical;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOVisiteMedicale extends _EOVisiteMedicale {
	
    public EOVisiteMedicale() {
        super();
    }
    
    public static EOVisiteMedicale findForKey( EOEditingContext ec, Number key) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(VMED_ORDRE_KEY + "=%@", new NSArray(key)));
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}

	public static EOVisiteMedicale creer(EOEditingContext ec, EOIndividu individu) {
		EOVisiteMedicale newObject = (EOVisiteMedicale) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setDCreation(new NSTimestamp());
		newObject.setDateConvocation(new NSTimestamp());
		return newObject;
	}

    public String nomRelationType() {
		return TYPE_VISITE_KEY;
	}
	public void initAvecIndividu(EOIndividu individu) {
		setDateConvocation(new NSTimestamp());
		super.initAvecIndividu(individu);
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		if (resultatVisite() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(resultatVisite(), RESULTAT_VISITE_KEY);
		}
	}
	public String dateConvocationFormatee() {
		return SuperFinder.dateFormatee(this,DATE_CONVOCATION_KEY);
	}
	public void setDateConvocationFormatee(String uneDate) {
		if (uneDate == null) {
			setDateConvocation(null);
		} else {
			SuperFinder.setDateFormatee(this,"dateConvocation",uneDate);
		}
	}
	public String rythmeSurveillanceFormate() {
		return SuperFinder.dateFormatee(this,RYTHME_SURVEILLANCE_KEY);
	}
	public void setRythmeSurveillanceFormate(String uneDate) {
		if (uneDate == null) {
			setRythmeSurveillance(null);
		} else {
			SuperFinder.setDateFormatee(this,RYTHME_SURVEILLANCE_KEY,uneDate);
		}
	}
	public boolean estResponsableAcmo() {
		if (individu() == null) {
			return false;
		} else {
			return EORepartAssociation.rechercherRepartAssociationsPourIndividuEtAssociation(editingContext(), individu(), "ACMO").count() > 0;
		}
	}
	
	
	/** Retourne "Oui" si l'individu est un responsable Acmo */
	public String responsableAcmo() {
		return (estResponsableAcmo()) ? "Oui" : "";
	}
	
	
	public void validateForSave() throws NSValidation.ValidationException {		
		if (date() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date de la visite !");
		}
		if (individu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu !");
		}
		if (heure() != null && heure().length() > 5) {
			throw new NSValidation.ValidationException("L'heure comporte au plus 5 caractères !");
		}
		if (DateCtrl.estHeureValide(heure(),false) == false) {
			throw new NSValidation.ValidationException("L'heure n'a pas un format valide (HH:MM) !");
		}
		if (observation() != null && observation().length() > 2000) {
			throw new NSValidation.ValidationException("L'observation comporte au plus 2000 caractères !");
		}
		if (lieu() != null && lieu().length() > 2000) {
			throw new NSValidation.ValidationException("Le lieu comporte au plus 2000 caractères");
		}		
	}
	
	
	/** Retourne true si le type de visite est fourni et que les annees, mois et jours de ce type ne sont pas nulles ou 0 */
	public boolean utiliseRythmeSurveillance() {
		if (typeVisite() == null) {
			return false;
		}
		if ((typeVisite().tvmeAnnee() == null || typeVisite().tvmeAnnee().intValue() == 0) &&
				(typeVisite().tvmeMois() == null || typeVisite().tvmeMois().intValue() == 0) &&
				(typeVisite().tvmeJour() == null || typeVisite().tvmeJour().intValue() == 0)) {
			return false;
		} else {
			return true;
		}
		
	}
	// Méthodes statiques
	/** Retourne les visites medicales des individus triees par nom usuel et par date decroissante
	 * @param editingContext
	 * @param debutPeriode (peut etre nulle)
	 * @param finPeriode (peut etre nulle)
	 * @param nom (peut etre nul)
	 * @param prenom (peut etre nul)
	 * @param lieu (peut etre nul)
	 */
	public static NSArray rechercherVisitesMedicalesPourPeriodeLieuEtIndividus(EOEditingContext ec,  NSTimestamp debutPeriode,NSTimestamp finPeriode,String nom,String prenom,String lieu) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (debutPeriode != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_KEY + " >= %@", new NSArray(debutPeriode)));
		}
		if (finPeriode != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_KEY + " <= %@", new NSArray(finPeriode)));
		}
		if (lieu != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LIEU_KEY + " caseinsensitivelike %@", new NSArray(lieu + "*")));
		}
		if (nom != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("individu.nomUsuel caseinsensitivelike %@", new NSArray(nom + "*")));
		}
		if (prenom != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("individu.prenom caseinsensitivelike %@", new NSArray(prenom + "*")));
		}
		EOQualifier qualifier = new EOAndQualifier(qualifiers);
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("individu.nomUsuel",EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(DATE_KEY,EOSortOrdering.CompareDescending));
		return fetchAll(ec , qualifier, sorts);
	}
}

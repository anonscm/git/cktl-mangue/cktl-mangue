// EOMedecinTravail.java
// Created on Thu Sep 11 08:37:18 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.mangue.individu.medical;

import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOMedecinTravail extends _EOMedecinTravail implements RecordAvecLibelle {

    public EOMedecinTravail() {
        super();
    }

	public static EOMedecinTravail creer(EOEditingContext ec, EOIndividu individu) {
		EOMedecinTravail newObject = (EOMedecinTravail) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		return newObject;
	}

    public static EOMedecinTravail getDefault(EOEditingContext ec) {
    	return fetchFirstByQualifier(ec, null);
    }
    
    public String toString() {
    	return individu().identitePrenomFirst();
    }
    
    public void supprimerRelations() {
    	setIndividuRelationship(null);
    }
    public void validateForSave() throws NSValidation.ValidationException {
    	if (individu() == null) {
    		throw new NSValidation.ValidationException("Vous devez fournir l'individu !");
    	}
    	if (dCreation() == null)
    		setDCreation(new NSTimestamp());
    	setDModification(new NSTimestamp());
    
    }
    
    public NSArray adressesProfessionnelles() {
    	return (NSArray)EORepartPersonneAdresse.adressesProValides(editingContext(), individu()).valueForKey("toAdresse");
    }
    // Interface RecordAvecLibelle
    public String libelle() {
    	if (individu() == null) {
    		return null;
    	} else {
    		return individu().identite();
    	}
    }
   
}

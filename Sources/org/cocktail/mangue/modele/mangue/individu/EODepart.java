//EODepart.java
//Created on Thu Dec 01 10:00:20 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import java.util.Enumeration;

import org.cocktail.mangue.common.modele.nomenclatures.EOMotifDepart;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.IValidite;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartStructure;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.conges.EOCld;
import org.cocktail.mangue.modele.mangue.conges.EOClm;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAccidentServ;
import org.cocktail.mangue.modele.mangue.conges.EOCongeAdoption;
import org.cocktail.mangue.modele.mangue.conges.EOCongeFormation;
import org.cocktail.mangue.modele.mangue.conges.EOCongeGardeEnfant;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaladie;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaternite;
import org.cocktail.mangue.modele.mangue.conges.EOCongePaternite;
import org.cocktail.mangue.modele.mangue.modalites.EOCessProgActivite;
import org.cocktail.mangue.modele.mangue.modalites.EOCrct;
import org.cocktail.mangue.modele.mangue.modalites.EODelegation;
import org.cocktail.mangue.modele.mangue.modalites.EOMiTpsTherap;
import org.cocktail.mangue.modele.mangue.modalites.EOTempsPartiel;
import org.cocktail.mangue.modele.mangue.prolongations.EOProlongationActivite;
import org.cocktail.mangue.modele.mangue.prolongations.EOReculAge;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Le nom des entites pour lesquelles changer la date de fin en cas de depart est stocke dans un tableau static qu'il convient de modifier si
 * de nouvelles entites sont ajoutees<BR>
 * Regles de validation :<BR>
 * 	Ve;rifie les longueurs de strings<BR>
 * 	Le motif et la date debut doivent etre fournis<BR>
 *	Si la date de fin est fournie, celle de debut doit etre anterieure<BR> 
 *	Verifie en fonction du motif si la rne doit etre saisie <BR>
 *  Si le motif de depart est deces, il ne peut pas y avoir de depart posterieur<BR>
 *  Pour un depart en cas de deces, la date de fin ne doit pas etre fournie<BR>
 *  Si le motif de cessation est deces alors la date de deces de l'individu doit etre fournie<BR>
 *	Si la date de deces est fournie alors la date de cessation doit etre inferieure ou egale a date de deces.<BR>
 * 	La date de cessation de service doit etre inferieure ou egale a la date de radiation des cadres
 * 	sauf en cas de maintien en fonction dans l’interet du service et maintien en activite en surnombre<BR>
 * 	Si la cessation definitive de fonctions est prononcee pour un motif qui met fin a la carriere autre que le deces alors la date d'effet de 
 *  radiation des cadres doit etre fournie. 
 *  
 */

public class EODepart extends _EODepart implements IValidite {

	public static NSArray SORT_DATE_DEBUT_ASC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));
	public static NSArray SORT_DATE_DEBUT_DESC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));

	// la classe des entités doit être une sous-classe de Duree */
	private static String[] ENTITES_A_FERMER_POUR_DEPART = {
		EOCessProgActivite.ENTITY_NAME,
		EOCgFinActivite.ENTITY_NAME,
		EOCld.ENTITY_NAME,
		EOClm.ENTITY_NAME,
		EOCongeAccidentServ.ENTITY_NAME,
		EOCongeAdoption.ENTITY_NAME,
		EOCongeFormation.ENTITY_NAME,
		EOCongeGardeEnfant.ENTITY_NAME,
		EOCongeMaladie.ENTITY_NAME,
		EOCongeMaternite.ENTITY_NAME,
		EOCongePaternite.ENTITY_NAME,
		EOCrct.ENTITY_NAME,
		EODelegation.ENTITY_NAME,
		EOMiTpsTherap.ENTITY_NAME,
		EOStage.ENTITY_NAME,
		EOTempsPartiel.ENTITY_NAME,
		"CongeInsufRes",
		"Ccp",
		"Rdt"};

	private EODepart departDeces;
	private boolean estTitulaireAvantDepart;

	public EODepart() {
		super();
	}

	public static EODepart findForKey( EOEditingContext ec, Number key) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat( NO_DEPART_KEY + "=%@", new NSArray(key));
		return fetchFirstByQualifier(ec, qualifier);
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EODepart creer(EOEditingContext ec, EOIndividu individu) {

		EODepart newObject = (EODepart) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.initAvecIndividu(individu);
		return newObject;
	}

	/**
	 * 
	 * @param ec
	 * @param absence
	 * @return
	 */
	public static EODepart rechercherPourAbsence(EOEditingContext ec, EOAbsences absence) {
		return fetchFirstByQualifier(ec, EOQualifier.qualifierWithQualifierFormat(ABSENCE_KEY + "=%@", new NSArray(absence)));
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EODepart dernierDepartPourIndividu( EOEditingContext ec, EOIndividu individu) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat( INDIVIDU_KEY +"=%@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat( TEM_VALIDE_KEY +"=%@", new NSArray(CocktailConstantes.VRAI)));
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
	}

	public String typeEvenement() {
		return "DEPA";
	}
	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}		
	}
		
	/**
	 * 
	 * @return
	 */
	public boolean estDepartPrevisionnel() {
		return temDepartPrevisionnel() != null && temDepartPrevisionnel().equals(CocktailConstantes.VRAI);
	}
	
	/**
	 * 
	 * @param aBool
	 */
	public void setEstDepartPrevisionnel(boolean aBool) {
		if (aBool) {
			setTemDepartPrevisionnel(CocktailConstantes.VRAI);
		} else {
			setTemDepartPrevisionnel(CocktailConstantes.FAUX);
		}
	}

	/**
	 * L'agent avait il une carriere a la date du depart
	 * @return
	 */
	public boolean estTitulaireAvantDepart() {
		if (dateDebut() != null && individu() != null) {
			NSArray carrieres = EOCarriere.rechercherCarrieresPourIndividuAnterieursADate(editingContext(), individu(), dateDebut());
			return carrieres.count() > 0;
		}
		return false;
	}

	/** Invalide le depart et l'absence associee */
	public void invalider() {
		setEstValide(false);
		if (absence() != null) {
			absence().setEstValide(false);
		}
	}

	public void initAvecIndividu(EOIndividu individu) {
		super.initAvecIndividu(individu);
		estTitulaireAvantDepart = false;
		setEstDepartPrevisionnel(true);
		departDeces = departDecesValidePourIndividu(individu.editingContext(), individu);
	}

	/** Surcharge du parent pour preparer le temoin titulaireAvantDepart et pour preparer la date de cessation de service */
	public void setDateDebut(NSTimestamp dateDebut) {
		super.setDateDebut(dateDebut);
		preparerTemoinTitulaire();
	}


	public String dateCessationServiceFormatee() {
		return SuperFinder.dateFormatee(this,D_CESSATION_SERVICE_KEY);
	}
	public void setDateCessationServiceFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,D_CESSATION_SERVICE_KEY, uneDate);
	}
	public String dateEffetRadiationFormatee() {
		return SuperFinder.dateFormatee(this,D_EFFET_RADIATION_KEY);
	}
	public void setDateEffetRadiationFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,D_EFFET_RADIATION_KEY, uneDate);
	}
	public String dateRadiationEmploiFormatee() {
		return SuperFinder.dateFormatee(this,D_RADIATION_EMPLOI_KEY);
	}

	public NSTimestamp dateRadiationCir() {

		if (dEffetRadiation() != null
				&& DateCtrl.isBefore(dEffetRadiation(), new NSTimestamp())) 
			return dEffetRadiation();

		if (dateEffetRadiationObligatoire()
				&& DateCtrl.isBefore(dateDebut(), new NSTimestamp())) 

			return dateDebut();

		return null;		
	}

	public void setDateRadiationEmploiFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this, D_RADIATION_EMPLOI_KEY, uneDate);
	}


	/** La date d'effet de radiation est obligatoire pour les departs de titulaires qui ne sont pas previsionnels
	 * declares dans le Cir (le motif
	 * met fin a la carriere, ce n'est pas le motif de deces et c'est un motif ONP)
	 * @return
	 */
	public boolean dateEffetRadiationObligatoire() {

		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresPourIndividuAnterieursADate(editingContext(), individu(), dateDebut());
		estTitulaireAvantDepart = carrieres.count() > 0;

		return estTitulaireAvantDepart && motifDepart().metFinACarriere() ;
	}

	/**
	 * Validation des donnees lors de l'enregistrement d'un nouveau depart
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (individu() == null) {
			throw new NSValidation.ValidationException(CocktailMessagesErreur.ERREUR_INDIVIDU_NON_RENSEIGNE);
		}

		if (dateDebut() == null)
			throw new NSValidation.ValidationException("Veuillez fournir la date de départ !");

		if (lieuDepart() != null && lieuDepart().length() > 80) {
			throw new NSValidation.ValidationException("Le lieu d'accueil comporte au maximum 80 caractères.");
		}

		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException("Un numéro d'arrêté comporte au plus 20 caractères.");
		}

		if (estValide()) {

			if (motifDepart() == null) {
				throw new NSValidation.ValidationException("Vous devez sélectionner le motif de départ !");
			} 
			
			if (motifDepart().estDeces() && DateCtrl.isAfter(dateDebut(),DateCtrl.today())) {
				throw new NSValidation.ValidationException("La date de décès ne peut être postérieure à aujourd'hui");
			}

			if (dateEffetRadiationObligatoire() && dEffetRadiation() == null) {
				throw new NSValidation.ValidationException("Veuillez renseigner une date de radiation des cadres !");
			}

			if (motifDepart().estDeces()) {
				
				setDateFin(null);
				
				individu().setDDeces(DateCtrl.dateAvecAjoutJours(dateDebut(), -1));
				
				if (departDeces != null && departDeces != this) {
					throw new NSValidation.ValidationException("Il existe déjà un départ pour décès");
				}
				
			} else if (departDeces != null 
					&& (DateCtrl.isAfter(dateDebut(), departDeces.dateDebut()) || dateFin() == null || (dateFin() != null && DateCtrl.isAfter(dateFin(), departDeces.dateDebut())))) {
				throw new NSValidation.ValidationException("Il ne peut y avoir de départ après un départ pour décès") ;
			} else if (estValide() && motifDepart().doitFournirLieu() && rne() == null && lieuDepart() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir une UAI ou un lieu d'accueil !");
			}
		}

		String message = validationsCir();
		if (message != null) {
			throw new NSValidation.ValidationException(message);
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}


	/** Ces validations ne sont appelees que pour les departs valides et definitifs (i.e non previsionnels) */
	public String validationsCir() {
		if (estValide()) {
			// Si le motif de cessation est décès alors la date de décès de l'individu doit être fournie
			if (motifDepart().estDeces() && individu().dDeces() == null) {
				return "Départ pour décès et pas de date de décès fournie pour l'individu";
			} 

			if ( estDepartPrevisionnel() == false || dateArrete() != null) {		

				if (motifDepart() != null && motifDepart().metFinACarriere() && dCessationService() == null) {
					return "La date de cessation de service doit être fournie";
				}
				// Si la date de décès est fournie alors la date de cessation de service doit être <= date de décès. 
				if (individu().dDeces() != null) {
					if (motifDepart().estDeces() == false) {
						return "En cas de décès, le motif de départ ne peut être que décès";
					} else if (DateCtrl.isAfter(dCessationService(), individu().dDeces())) {
						return "La date de cessation de service ne peut être postérieure à la date de décès de l'individu";
					}
				}
				// Si la date de cessation de service est fournie, elle doit être inférieure ou égale à la date de radiation des cadres
				// sauf en cas de maintien en fonction dans l’intérêt du service et maintien en activité en surnombre
				if (dCessationService() != null && dEffetRadiation() != null) {
					boolean controlerDate = true;
					NSArray<EOProlongationActivite> prolongations = EOProlongationActivite.rechercherDureesPourIndividu(editingContext(), "ProlongationActivite", individu(),false);
					for (java.util.Enumeration<EOProlongationActivite> e = prolongations.objectEnumerator();e.hasMoreElements();) {
						EOProlongationActivite prolongation = e.nextElement();
						if (prolongation.motif().estMaintienService() || prolongation.motif().estSurnombre()) {
							controlerDate = false;
							break;
						}
					}
					if (controlerDate && DateCtrl.isAfter(dCessationService(), dEffetRadiation()))	 {
						return "DEPART - La date de cessation de service ne peut être postérieure à la date d'effet de la radiation";
					}
				}
				// 23/11/2010 - Si la date d'effet de la radiation des cadres est postérieure au lendemain de la date d'atteinte de la limite d'âge
				// ET SI la limite d'âge personnelle est précisée, alors la date d'effet de la radiation des cadres est antérieure ou égale 
				// au lendemain de la date d'atteinte de la limite d'âge personnelle.
				if (dEffetRadiation() != null && individu().personnel().limiteAge() != null) {
					String cLimite = individu().personnel().limiteAge().code(); 
					// C'est une string sous la forme XX00 avec XX le nombre d'années
					int nbAnnees = new Integer(cLimite.substring(0,2)).intValue();
					NSTimestamp dateLimiteAge = DateCtrl.dateAvecAjoutAnnees(individu().dNaissance(), nbAnnees);
					if (DateCtrl.isAfter(dEffetRadiation(),dateLimiteAge)) {
						NSTimestamp dateLimite = null;
						NSArray<EOReculAge> reculsAge = EOReculAge.findForIndividu(editingContext(), individu());
						if (reculsAge.count() > 0) {
							// ils sont triés par ordre croissant
							EOReculAge reculAge = (EOReculAge)reculsAge.lastObject();
							dateLimite = reculAge.dateFin();
						}
						NSArray<EOProlongationActivite> prolongations = EOProlongationActivite.findForIndividu(editingContext(), individu());
						if (prolongations.count() > 0) {
							prolongations = EOSortOrdering.sortedArrayUsingKeyOrderArray(prolongations, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
							EOProlongationActivite prolongation = (EOProlongationActivite)prolongations.lastObject();
							if (dateLimite == null || DateCtrl.isAfter(prolongation.dateFin(), dateLimite)) {
								dateLimite = prolongation.dateFin();
							}
						}
						dateLimite = DateCtrl.dateAvecAjoutJours(dateLimite, 1);
						if (dateLimite != null && DateCtrl.isAfter(dEffetRadiation(), dateLimite)) {
							return "DEPART - La date de radation des cadres (" + DateCtrl.dateToString(dEffetRadiation()) + " doit être antérieure ou égale à la date de limite d'âge personnel : " + DateCtrl.dateToString(dateLimite); 
						}
					}
				}
			}
		}
		return null;
	}


	/** Ajoute le depart aux segments de carriere de la periode */
	public void signalerDepartDansCarriereContrat() {
		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(), individu(), dateDebut(), dateFin());
		// fermer la carrière
		for (EOCarriere carriere : carrieres) {
			carriere.setDepartRelationship(this);
		}
		// fermer le contrat
		NSArray<EOContrat> contrats = EOContrat.rechercherTousContratsPourIndividuEtPeriode(editingContext(), individu(), dateDebut(),dateFin());
		for (EOContrat contrat : contrats) {
			contrat.setDepartRelationship(this);
		}
	}

	// méthodes protégées
	protected void init() {
		setTemDepartPrevisionnel(CocktailConstantes.FAUX);
		setEstValide(true);
		super.init();
	}

	private void preparerTemoinTitulaire() {
		if (dateDebut() != null && individu() != null) {
			NSArray carrieres = EOCarriere.rechercherCarrieresPourIndividuAnterieursADate(editingContext(), individu(), dateDebut());
			estTitulaireAvantDepart = (carrieres.count() > 0);
		} else {
			estTitulaireAvantDepart = false;
		}
	}

	// Méthodes statiques
	/** Pas de refresh des departs */
	public static NSArray rechercherDepartsValidesPourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ AND temValide = 'O'", new NSArray(individu));
		return rechercherDureePourEntiteAvecCriteres(editingContext, ENTITY_NAME, qualifier,false);
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EODepart rechercherDernierDepartPourIndividu(EOEditingContext ec, EOIndividu individu) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));

			return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}



	/** recherche les departs valides pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode	 peut etre nulle
	 * @return departs trouves
	 */
	public static NSArray<EODepart> rechercherDepartsValidesPourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));

			EOQualifier qualifier = SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode);
			if (qualifier != null)
				qualifiers.addObject(qualifier);

			return rechercherDureePourEntiteAvecCriteres(editingContext,ENTITY_NAME,new EOAndQualifier(qualifiers),false);
		}
		catch (Exception e) {
			return new NSArray<EODepart>();
		}
	}


	/** Recherche les departs definitifs pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode peut etre nulle
	 * @return departs trouves
	 */
	public static NSArray<EODepart> rechercherDepartsValidesEtDefinitifsPourIndividuEtPeriode(
			EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY+"=%@", new NSArray(individu)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(MOTIF_DEPART_KEY+"."+EOMotifDepart.C_MOTIF_DEPART_ONP_KEY+" != nil", null));

		EOQualifier qualifier = SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_DEBUT_KEY,finPeriode);
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}

		return fetchAll(editingContext, new EOAndQualifier(qualifiers));
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @return
	 */
	public static EODepart departDecesValidePourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		NSArray<EODepart> departs = rechercherDepartsValidesPourIndividu(editingContext, individu);
		for (EODepart depart : departs) {
			if (depart.motifDepart().estDeces())
				return depart;
		}
		return null;
	}

	/** Ferme les occupations et affectations lie a l'agent a la date debut<BR>
	 * 	Informations de carriere, informations de contrat, Affectation, Occupation, Modalites de service, Conges
	 * @param editingContext
	 * @param individu (ne peut etre nul)
	 * @param date date a laquelle les informations doivent etre fermees (ne peut etre nulle)
	 */
	public static void fermerOccupationsPourAgent(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) throws Exception {
		if (date == null) {
			throw new Exception("Vous devez fournir une date");
		}
		if (individu == null) {
			throw new Exception("Vous devez fournir un agent");
		}
		EOOccupation.fermerOccupations(editingContext,individu,date);
		EOAffectation.fermerAffectations(editingContext,individu,date);
	}

	/** Ferme toutes les informations liees a l'agent a la date debut<BR>
	 * 	Informations de carriere, informations de contrat, Affectation, Occupation, Modalites de service, Conges
	 * @param editingContext
	 * @param individu (ne peut etre nul)
	 * @param date date a laquelle les informations doivent etre fermees (ne peut etre nulle)
	 */
	public static void fermerInformationsPourAgent(EOEditingContext edc,EOIndividu individu, NSTimestamp date, EODepart depart) throws Exception {

		if (date == null) {
			throw new Exception("DEPART - Vous devez fournir une date !");
		}
		if (individu == null) {
			throw new Exception("DEPART - Vous devez fournir un agent !");
		}
		
		EOContratHeberges.fermerContrats(edc,individu, date, depart.motifDepart());
		EOCarriere.fermerCarrieres(edc, individu, date, depart);
		EOContrat.fermerContrats(edc, individu, date, depart);
		fermerOccupationsPourAgent(edc, individu, date);
		EOAbsences.fermerAbsencesSaufAbsencesDepart(edc, individu, date);
		EOCarriereSpecialisations.fermerSpecialisationDate(edc, individu, date);

		for (int i = 0; i < ENTITES_A_FERMER_POUR_DEPART.length;i++) {

			String nomEntite = ENTITES_A_FERMER_POUR_DEPART[i];

			NSArray objets = Duree.rechercherDureesPourIndividuEtPeriode(edc, nomEntite, individu, date, date);
			for (java.util.Enumeration<Duree> e = objets.objectEnumerator();e.hasMoreElements();) {
				Duree duree = e.nextElement();
				if (DateCtrl.isBefore(duree.dateDebut(), date)  && DateCtrl.isAfter(duree.dateFin(), date))
					duree.setDateFin(date);
			}
			objets = Duree.rechercherDureesPourEntitePosterieuresADate(edc, nomEntite, individu, date);
			// supprimer les durées commençant après la date. Pour certaines, il faut détruire les relations
			for (Enumeration<Duree> e1= objets.objectEnumerator();e1.hasMoreElements();) {
				Duree duree = e1.nextElement();
				duree.supprimerRelations();
				edc.deleteObject(duree);
			}

		}

		// On ne ferme les repart que si la date est antérieure à la date du jour car sinon l'agent doit rester visible dans son service
		if (DateCtrl.isBefore(date, new NSTimestamp())) {
			EORepartStructure.supprimerRepartPourIndividu(edc,individu);
			
		}
	}

}

//EOOccupation.java
//Created on Wed Feb 26 13:49:14  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;


import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.EOCld;
import org.cocktail.mangue.modele.mangue.conges.EOCongeFormation;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation des occupations :<BR>
 *  Verification des longueurs de strings<BR>
 * 	La date de debut d'occupation, l'emploi et la quotite doivent etre fournis<BR>
 *	Si la date de fin est fournie, celle de debut doit etre anterieure<BR> 
 *	La date de debut ne peut etre anterieure a la date de creation de l'emploi
 *	La date de fin ne peut etre posterieure a la date de suppression de l'emploi
 *	Il doit y avoir au moins un segment de carriere en activite ou un contrat pour cette periode et la date de fin d'occupation ne doit pas depasser la date maximum de fin de contrat ou de carriere<BR>
 *  Si l'agent est fonctionnaire sur la periode :
 * 	<UL>controle de coherence entre la categorie emploi de l'emploi et le corps de l'agent.</UL>
 *	<UL>La somme des quotites d'occupation a un jour j ne peut etre superieure a la quotite de creation de l'emploi</UL>
 * 	Si l'agent est contractuel sur la periode :
 *   <UL>controle de coherence entre la categorie d'emploi de l'emploi et le type de contrat de l'agent, pour les emplois budget etat.</UL>
 *   <UL>La somme des quotites d'occupation a un jour j ne peut etre superieure a la quotite de recrutement.</UL>
 *	La somme des quotites d'occupations de l'emploi sur la periode ne peut pas depasser la quotite de creation de l'emploi<BR>
 *	Un agent ne peut pas occuper plus d'une fois le meme emploi pendant une meme periode.<BR>
 *  Une occupation ne peut pas chevaucher un conge de fin d'activite.<BR>
 *  Une occupation ne peut pas chevaucher un conge de longue duree.<BR>
 *  Une occupation ne peut pas chevaucher un conge de formation non fractionne.<BR>
 *  Une occupation ne peut pas chevaucher un conge specifique des stagiaires.<BR>
 *  Une occupation ne peut pas chevaucher  un conge specifique des non titulaires.<BR>
 *  Pas d'occupation pour les contrats Invites<BR>
 *  Un agent en conge de formation fractionne ne peut pas occuper un emploi a 100%.
 *
 */

public class EOOccupation extends _EOOccupation {
	
	public static final String STATUT_INDIVIDU = "statutIndividu";

	public EOOccupation() {
		super();
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOOccupation creer(EOEditingContext ec, EOIndividu individu) {

		EOOccupation newObject = (EOOccupation) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);

		newObject.setTemValide(CocktailConstantes.VRAI);
		newObject.setTemTitulaire(CocktailConstantes.FAUX);
		newObject.setQuotite(ManGUEConstantes.QUOTITE_100);

		return newObject;

	}

	/**
	 * 
	 */
	public void setToCarriereRelationship(EOCarriere carriere) {
		
		if (carriere != null) {
			
			addObjectToBothSidesOfRelationshipWithKey(carriere, TO_CARRIERE_KEY);
			Integer idCarriere = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(editingContext(), carriere)).valueForKey("noSeqCarriere");
			setNoSeqCarriere(idCarriere);
			
		}
		else {
			removeObjectFromBothSidesOfRelationshipWithKey(carriere, TO_CARRIERE_KEY);
			setNoSeqCarriere(null);		
		}
	}

	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}
	
	public boolean estTitulaireEmploi() {
		return temTitulaire().equals(CocktailConstantes.VRAI);
	}
	public void setEstTitulaireEmploi(boolean aBool) {
		if (aBool) {
			setTemTitulaire(CocktailConstantes.VRAI);
		} else {
			setTemTitulaire(CocktailConstantes.FAUX);
		}
	}
	
	public void initAvecIndividu(EOIndividu individu) {
		setTemValide(CocktailConstantes.VRAI);
		setTemTitulaire(CocktailConstantes.FAUX);
		setQuotite(new BigDecimal(100.00));
		setIndividuRelationship(individu);
	}
	
	public void invalider() {
		setTemValide(CocktailConstantes.FAUX);
	}
	
	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (!estValide()) {
			return;	
		}

		if (motifFin() != null && motifFin().length() > 200) {
			throw new NSValidation.ValidationException("OCCUPATION - \nLe motif de fin comporte 200 caractères maximum");
		}
		if (observations() != null && observations().length() > 240) {
			throw new NSValidation.ValidationException("OCCUPATION - \nLes observations comportent 240 caractères maximum");
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("OCCUPATION - \nLa date de début doit être fournie");
		}
		if (dateFin() != null && DateCtrl.isBefore(dateFin(),dateDebut())) {
			throw new NSValidation.ValidationException("OCCUPATION - \nLa date de début ("+DateCtrl.dateToString(dateDebut())+") ne peut être postérieure à la date de fin ("+DateCtrl.dateToString(dateFin())+")");
		}
		if (quotite() == null || quotite().floatValue() == 0) {
			throw new NSValidation.ValidationException("OCCUPATION - \nLa quotité ne peut pas être nulle");
		}
		if (quotite().floatValue() > 100) {
			throw new NSValidation.ValidationException("OCCUPATION - \nLa quotité ne peut être supérieure à 100");
		}

		NSArray<EOOccupation> occupations = rechercherOccupationsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin());
		NSArray<EOCarriere> carrieresSurPeriode = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),individu(),dateDebut(),dateFin());
		NSArray<EOContrat> contratsSurPeriode = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin(),false);
		NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsEnActivitePourIndividuEtPeriode(editingContext(), individu(), dateDebut(), dateFin());
		NSArray<EOModalitesService> modalites = EOModalitesService.rechercherPourIndividuEtPeriode(editingContext(), individu(), dateDebut(),dateFin());

		validationActivitesAgent(carrieresSurPeriode,contratsSurPeriode,changements);
		validationChoixEmploi(occupations,carrieresSurPeriode,contratsSurPeriode);
		validationQuotitesOccupation(occupations,carrieresSurPeriode,contratsSurPeriode,changements, modalites);

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
	public boolean aPrendreEnComptePourPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		if ((dateFin() != null && DateCtrl.isAfter(debutPeriode,dateDebut()) && DateCtrl.isBefore(debutPeriode,dateFin())) ||
				(finPeriode != null && DateCtrl.isAfter(dateDebut(),debutPeriode) &&DateCtrl.isBefore(dateDebut(),finPeriode)) ||
				(finPeriode == null && dateFin() == null)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Recherche des occupations d'un emploi a une date donnee
	 * @param ec : editingContext
	 * @param emploi : l'emploi
	 * @param dateOccupation : date occupation (peut etre nulle) 
	 * @param trieParDate : true si tries par date
	 * @return liste des occupations d'un emploi
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOOccupation> rechercherOccupationsPourEmploi(EOEditingContext ec, IEmploi emploi, NSTimestamp dateOccupation, boolean trieParDate) {
		try {
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
	
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			
			if (emploi != null) {
				andQualifiers.addObject(CocktailFinder.getQualifierEqual(EOOccupation.TO_EMPLOI_KEY, emploi));
			}
	
			if (dateOccupation != null) {
				andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY, dateOccupation, DATE_FIN_KEY, dateOccupation));
			}
			
			NSArray<EOSortOrdering> sorts = null;
			if (trieParDate) {
				sorts = PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC;
			}
			
			return EOOccupation.fetchAll(ec, new EOAndQualifier(andQualifiers), sorts);
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/** Recherche des occupations d'un individu a une date donnee
	 * @param ec editing context
	 * @param individu concerne
	 * @param dateOcc date recherchee (peut etre nulle) */
	public static NSArray<EOOccupation> findForIndividuAndDate(EOEditingContext edc, EOIndividu individu, NSTimestamp dateOcc) {

		try {
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));

			if (dateOcc != null) {
				andQualifiers.addObject(CocktailFinder.getQualifierBeforeEq(DATE_DEBUT_KEY, dateOcc));

				orQualifiers.addObject(CocktailFinder.getQualifierAfterEq(DATE_FIN_KEY, dateOcc));
				orQualifiers.addObject(CocktailFinder.getQualifierNullValue(DATE_FIN_KEY));
				andQualifiers.addObject(new EOOrQualifier(orQualifiers));
			}

			return fetchAll(edc, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOOccupation>();
		}
	}

	/** Recherche des occupations pendant une periode donnee 
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 */
	public static NSArray<EOOccupation> findForPeriode(EOEditingContext editingContext,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return rechercherOccupationsPourIndividuEtPeriode(editingContext,null,debutPeriode,finPeriode);
	}
	/** recherche les occupations d'un individu commencant apres la date fournie en parametre */
	public static NSArray<EOOccupation> occupationsPosterieuresADate(EOEditingContext ec,EOIndividu individu,NSTimestamp date) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
		andQualifiers.addObject(CocktailFinder.getQualifierAfter(DATE_DEBUT_KEY, date));

		return fetchAll(ec, new EOAndQualifier(andQualifiers), null);
	}
	
	/** Recherche des occupations d'un individu pendant une periode donnee 
	 * @param individu (peut etre nul)
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 */
	public static NSArray<EOOccupation> rechercherOccupationsPourEmploiEtPeriode(EOEditingContext ec, IEmploi emploi, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));

		if (emploi != null)
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_EMPLOI_KEY, emploi));

		andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
		
		return fetchAll(ec, new EOAndQualifier(andQualifiers), null);
	}
	
	/** Recherche des occupations d'un individu pendant une periode donnee 
	 * @param individu (peut etre nul)
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 */
	public static NSArray<EOOccupation> rechercherOccupationsPourIndividuEtPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));

		if (individu != null)
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));

		andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
		
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(andQualifiers), null);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray(TO_EMPLOI_KEY));

		return edc.objectsWithFetchSpecification(fs);
	}

	/** ferme les occupations d'un agent valides a la date passee en parametre  */
	public static void fermerOccupations(EOEditingContext edc, EOIndividu individu,NSTimestamp date) {
		NSArray<EOOccupation> occupations = rechercherOccupationsPourIndividuEtPeriode(edc,individu,date,date);
		for (EOOccupation occupation : occupations) {
			occupation.setDateFin(date);
		}
		// supprimer les occupations postérieures à cette date
		occupations = occupationsPosterieuresADate(edc,individu,date);
		for (EOOccupation occupation : occupations) {
			occupation.invalider(); 
		}
	}

	/**
	 * 
	 * Verification de la coherence des activites de l'agent en fonction des dates d'occupation
	 * 
	 * - Un agent contractuel doit avoir un contrat sur toute la periode (Vacations exclues)
	 * - Un agent titulaire doit etre en activite pendant toute la periode
	 * - Un agent ne peut etre en CLD ou en Conge de fin d'activite sur la periode d'occupation
	 * 
	 * @param carrieresSurPeriode
	 * @param contratsSurPeriode
	 * @param changements
	 * @throws NSValidation.ValidationException
	 */
	private void validationActivitesAgent(NSArray<EOCarriere> carrieresSurPeriode,NSArray<EOContrat> contratsSurPeriode,NSArray<EOChangementPosition> changements) throws NSValidation.ValidationException {

		if (carrieresSurPeriode.count() == 0 && contratsSurPeriode.count() == 0) {
			throw new NSValidation.ValidationException("L'agent n'a pas de segment de carrière ou de contrat défini pour cette période");
		}
				
		boolean verifierContrat = (contratsSurPeriode.size() > 0) && (carrieresSurPeriode.size() == 0);
		
		if (carrieresSurPeriode.count() > 0) {
			
			// On  vérifie les carrières
			if	(changements == null || changements.count() == 0) {
				throw new NSValidation.ValidationException("L'agent n'est pas en activité à cette période. Veuillez définir dans les carrières une position pour cette période !");
			} 
			
			// vérifier si la date de fin d'occupation n'est pas postérieure aux dates du dernier segment de carrière 
			// l'agent doit être en activité, on regarde donc les changements de position
			carrieresSurPeriode = EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieresSurPeriode, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
			EOCarriere lastCarriere = carrieresSurPeriode.get(0);
			if (lastCarriere.dateFin() != null) {
				if (dateFin() == null || DateCtrl.isAfter(dateFin(), lastCarriere.dateFin())) {
					throw new NSValidation.ValidationException("L'occupation ne peut se terminer après la fin du dernier segment de carrière");
				}
			} else
				verifierContrat = false;

			// L'agent doit etre en activite dans l'etablissement pendant toute la periode d occupation
			for (EOChangementPosition myChangement : changements) {
				if (!(myChangement.toPosition().estEnActiviteDansEtablissement() 
						|| (myChangement.toPosition().estUnDetachement() && !myChangement.estDetachementSortant()))) {
					throw new NSValidation.ValidationException("L'agent a des positions d'inactivité ou de détachement externe pendant cette période. Veuillez modifier les dates de début et de fin d'occupation !");
				}
			}

			// Vérifier si l'agent est en CLD
			NSArray conges = Duree.rechercherDureesPourIndividuEtPeriode(editingContext(),EOCld.ENTITY_NAME ,individu(),dateDebut(),dateFin());
			if (conges != null && conges.count() > 0) {
				throw new NSValidation.ValidationException("L'agent est en congé de longue durée pendant cette période");
			}

			// vérifier si il existe un CFA
			conges = Duree.rechercherDureesPourIndividuEtPeriode(editingContext(), EOCgFinActivite.ENTITY_NAME ,individu(),dateDebut(),dateFin());
			if (conges != null && conges.count() > 0) {
				throw new NSValidation.ValidationException("L'agent est en congé de fin d'activité pendant cette période");
			}

			// Vérifier qu'un fonctionnaire sur budget état occupe l'emploi à 100% si il n'est pas en congé de formation fractionné
			// Vérifier qu'il n'y a pas d'occupation si il y a un congé de formation non fractionné ou que la quotité est < 100% sinon
			conges = Duree.rechercherDureesPourIndividuEtPeriode(editingContext(),EOCongeFormation.ENTITY_NAME,individu(),dateDebut(),dateFin());
			if (conges != null && conges.count() > 0) {
				// gestion du fractionnement
				for (Enumeration<EOCongeFormation> e1= conges.objectEnumerator();e1.hasMoreElements();) {
					EOCongeFormation conge = e1.nextElement();
					if (conge.estValide()) {
						if (conge.estFractionne()) {
							if (quotite().doubleValue() == 100) {
								throw new NSValidation.ValidationException("L'occupation d'un emploi pendant un congé formation fractionné ne peut être à 100%.\nVeuillez modifier la quotité");
							}
						} else {
							throw new NSValidation.ValidationException("L'agent est en congé formation non fractionné, il ne peut y avoir d'occupation pendant cette période");
						}
					}
				}
			}
		}

		// Recuperation du dernier contrat
		if ( verifierContrat ) {
			contratsSurPeriode = EOSortOrdering.sortedArrayUsingKeyOrderArray(contratsSurPeriode, PeriodePourIndividu.SORT_ARRAY_DATE_FIN_DESC);
			EOContrat lastContrat = contratsSurPeriode.get(0);
			if (lastContrat.dateFin() != null) {
				if (dateFin() == null || DateCtrl.isAfter(dateFin(), lastContrat.dateFin())) {
					throw new NSValidation.ValidationException("L'occupation ne peut se terminer après la fin du dernier contrat !");
				}
			}
		}
	}


	/**
	 * 
	 * @param occupations
	 * @param carrieresSurPeriode
	 * @param contratsSurPeriode
	 * @throws NSValidation.ValidationException
	 */
	private void validationChoixEmploi(NSArray<EOOccupation> occupations,NSArray<EOCarriere> carrieresSurPeriode,NSArray<EOContrat> contratsSurPeriode) throws NSValidation.ValidationException {
		if (toEmploi() == null) {
			throw new NSValidation.ValidationException("Vous devez choisir un emploi !");
		}
		
		//L'emploi contractuel est réservé uniquement aux agents contractuels
		if (toEmploi().isEmploiContractuel() && carrieresSurPeriode.size() > 0) {
			throw new NSValidation.ValidationException(MangueMessagesErreur.ERREUR_OCCUPATION_EMPLOI_POUR_CONTRACTUEL);
		}
		
		if (DateCtrl.isBefore(dateDebut(), toEmploi().getDateEffetEmploi())) {
			throw new NSValidation.ValidationException("La date de début ne peut être antérieure au " + DateCtrl.dateToString(toEmploi().getDateEffetEmploi()) + ", date de création de l'emploi");
		}
		
		if (dateFin() != null) {
			if (toEmploi().getDateFermetureEmploi() != null && DateCtrl.isAfter(dateFin(), toEmploi().getDateFermetureEmploi())) {
				throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à " + DateCtrl.dateToString(toEmploi().getDateFermetureEmploi()) + ", date de fermeture de l'emploi");
			}
		}
		
		// vérifier que l'emploi ne soit pas occupé deux fois par l'agent pendant cette période
		// on regarde les dates de début et fin des deux occupations car cette méthode est appelée lorsqu'on modifie des dates d'occupations
		// depuis la gestion des congés de longue durée et les dates peuvent être modifiées
		for (EOOccupation occupation : occupations) {
			if (occupation != this && occupation.estValide() 
					&& occupation.aPrendreEnComptePourPeriode(dateDebut(), dateFin()) 
					&& occupation.toEmploi() == toEmploi()) {
				throw new NSValidation.ValidationException("L'agent occupe déjà cet emploi pendant cette période");
			}
		}
	}

	/**
	 * 
	 * @param occupations
	 * @param carrieresSurPeriode
	 * @param contratsSurPeriode
	 * @param changements
	 * @param modalites
	 * @throws NSValidation.ValidationException
	 */
	private void validationQuotitesOccupation(NSArray<EOOccupation> occupations,NSArray<EOCarriere> carrieresSurPeriode,NSArray<EOContrat> contratsSurPeriode,
			NSArray<EOChangementPosition> changements, NSArray<EOModalitesService> modalites) throws NSValidation.ValidationException {

		double quotiteTotale = 0;
		
		LogManager.logDetail("occupation courante du " + dateDebut() + " au " + dateFin() + ", quotite " + quotite());
		if (occupations != null && occupations.count() > 0) {
			// vérifier si la somme des quotités sur la période est valide	
			for (EOOccupation occupation : occupations) {
				if (occupation != this && occupation.aPrendreEnComptePourPeriode(dateDebut(), dateFin())) {
					LogManager.logDetail("occupation du " + occupation.dateDebut() + " au " + occupation.dateFin() + ", quotite " + occupation.quotite());
					quotiteTotale = quotiteTotale + occupation.quotite().doubleValue();
				}
			}
		}
		
		BigDecimal quotiteOccupation = new BigDecimal(quotiteTotale + quotite().doubleValue()).setScale(2,BigDecimal.ROUND_UP);

		for (EOModalitesService myModalite : modalites) {

			if (myModalite.estTempsPartiel()) {
				if (quotiteOccupation.compareTo(myModalite.quotite()) > 0) {
					throw new NSValidation.ValidationException("La quotité d'occupation de ce fonctionnaire (" + quotiteOccupation + "%) pendant cette période ne peut pas dépasser sa quotité de recrutement ( " + myModalite.quotite() + "%)");
				}
			}

			// vérification des quotités par rapport aux quotités du contrat
			if (contratsSurPeriode.count() > 0) {
				double quotiteContractuel = 0;
				for (EOContrat myContrat : contratsSurPeriode) {
					NSArray<EOContratAvenant> avenants = myContrat.avenants();
					if (avenants.count() == 0) {
						throw new NSValidation.ValidationException("Pas de détail de contrat défini pour le contrat du " + myContrat.dateDebutFormatee() + "au " + myContrat.dateFinFormatee() + ". Veuillez définir les avenants");
					} else {
						for (EOContratAvenant myAvenant : avenants) {
							quotiteContractuel = quotiteContractuel + myAvenant.quotite().doubleValue();
						}
					}
				}
				BigDecimal quotiteContrac =  new BigDecimal(quotiteContractuel).setScale(2,BigDecimal.ROUND_UP);

				if (quotiteOccupation.compareTo(quotiteContrac) > 0) {
					throw new NSValidation.ValidationException("La quotité d'occupation de ce contractuel (" + quotiteOccupation + "%) pendant cette période ne peut pas dépasser sa quotité de recrutement ( " +quotiteContrac + "%)");
				}
			}	
		}
	}
	
	/**
	 *  Setter pour un emploi 
	 * @param value : un emploi {@link IEmploi}
	 **/
	public void setToEmploiRelationship(IEmploi value) {
		super.setToEmploiRelationship((EOEmploi) value);
	}

	/**
	 * @return libellé de l'occupation à étoffer
	 */
	public String libellePourEmploi() {
		return this.individu().identite() + getCodeStatut() + getDatesOccupation() + " à " + this.quotite().toString() + "%";
	}
	
	/**
	 * @return les dates de début et de fin (si renseigné) de l'occupation
	 */
    public String getDatesOccupation() {
    	String libelle = "";
    	if (dateFin() != null) {
    		libelle += " (" + this.dateDebut() + " - " + this.dateFin() + ")";
    	} else {
    		libelle += " depuis le " + this.dateDebut();
    	}
    	
    	return libelle;
    }
	
	/**
	 * Retourne le statut de l'individu 
	 * 	=> corps pour les titulaires ou type de contrat pour les contractuels
	 * @return libellé
	 */
	public String getStatutIndividu() {
		String libelle = "";
				
		NSTimestamp dateReference = DateCtrl.today();
		if (dateFin() != null) {
			dateReference = dateFin();
		}
		
		//Si la personne a une carrière => on récupère le corps
		EOElementCarriere element = EOElementCarriere.rechercherElementADate(editingContext(), individu(), dateReference);		
		if (element != null) {
			libelle = element.toCorps().lcCorps();
		} else if (EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(), individu(), dateDebut(), dateFin(), false).size() > 0) { 
			//Si la personne a un contrat => on récupère le type du contrat
			libelle = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(), individu(), dateDebut(), dateFin(), false)
					.get(0).toTypeContratTravail().libelleCourt();
		}
		
		return libelle;
	}
	
	/**
	 * Retourne le statut abrégé de l'individu 
	 * 	=> "T" pour les titulaires ou "C" pour les contractuels
	 * @return libellé
	 */
	public String getCodeStatut() {
		String libelle = "";
		
		//Si la personne a une carrière => on affiche "T" pour titulaire
		if (EOCarriere.rechercherCarrieresSurPeriode(editingContext(), individu(), dateDebut(), dateFin()).size() > 0) {
			libelle = " (T)";
		} else {
			//Si la personne a un contrat => on affiche "C" pour contractuel
			libelle = " (C)";
		}
		
		return libelle;
	}

}

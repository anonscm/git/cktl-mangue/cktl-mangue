//EOAffectation.java
//Created on Wed Feb 26 13:46:13  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;


import java.util.Enumeration;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.client.ServerProxy;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation des affectations :<BR>
 * 	La date de debut d'affectation, la structure et la quotite doivent etre fournies<BR>
 *	Si la date de fin est fournie, celle de debut doit etre anterieure<BR> 
 *	Il doit y avoir au moins un segment de carriere ou un contrat pour cette periode<BR>
 *	Un agent ne peut etre affecte deux fois a la meme structure pendant une periode<BR>
 *	La somme des quotites des affectations sur la periode ne doit pas depasser 100% ou 50% (CPA)<BR>
 *	Si l'individu est en MAD, la somme des quotites d'affectation et de MAD ne doit pas depasser 100%<BR>
 */
public class EOAffectation extends _EOAffectation {

	public static final EOSortOrdering SORT_AFF_NOM_ASC = new EOSortOrdering(INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_AFF_PRINCIPALE_DESC = new EOSortOrdering(TEM_PRINCIPALE_KEY, EOSortOrdering.CompareDescending);
	public static final EOSortOrdering SORT_AFF_QUOTITE_DESC = new EOSortOrdering(QUOTITE_KEY, EOSortOrdering.CompareDescending);

	public static final NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_AFF_NOM_ASC);
	public static final NSArray SORT_ARRAY_AFF_PRINCIPALE_DESC = new NSArray(SORT_AFF_PRINCIPALE_DESC);
	public static final NSArray SORT_ARRAY_QUOTITE_DESC = new NSArray(SORT_AFF_QUOTITE_DESC);

	public EOAffectation() {
		super();
	}

	public static EOAffectation creer(EOEditingContext ec, EOIndividu individu) {

		EOAffectation newObject = new EOAffectation();    
		newObject.setIndividuRelationship(individu);

		newObject.setTemValide(CocktailConstantes.VRAI);
		newObject.setTemPrincipale(CocktailConstantes.VRAI);
		newObject.setQuotite(new Integer(100));

		ec.insertObject(newObject);
		return newObject;

	}

	/**
	 * REnouvellement d'une periode d'affectation
	 * @param ec
	 * @param ancienneAffectation
	 * @return
	 */
	public static EOAffectation renouveler(EOEditingContext ec, EOAffectation ancienneAffectation) {

		EOAffectation newObject = (EOAffectation) createAndInsertInstance(ec, ENTITY_NAME);    
		//newObject.takeValuesFromDictionary(ancienneAffectation.snapshot());

		newObject.setToStructureUlrRelationship(ancienneAffectation.toStructureUlr());
		newObject.setIndividuRelationship(ancienneAffectation.individu());
		if (ancienneAffectation.dateFin() != null)
			newObject.setDateDebut(DateCtrl.jourSuivant(ancienneAffectation.dateFin()));
		else
			newObject.setDateDebut(ancienneAffectation.dateDebut());
		newObject.setDateFin(null);
		newObject.setQuotite(new Integer(100));
		newObject.setTemValide(CocktailConstantes.VRAI);
		newObject.setTemPrincipale(CocktailConstantes.VRAI);

		return newObject;

	}

	/**
	 * 
	 */
	public void setIndividuRelationship(EOIndividu individu) {
		
		if (individu != null) {
			addObjectToBothSidesOfRelationshipWithKey(individu, INDIVIDU_KEY);
			setNoDossierPers(individu.noIndividu());
		}
		else {
			removeObjectFromBothSidesOfRelationshipWithKey(individu, INDIVIDU_KEY);
			setNoDossierPers(null);		
		}
	}
	
	/**
	 * 
	 */
	public void setToCarriereRelationship(EOCarriere carriere) {
		
		if (carriere != null) {
			
			addObjectToBothSidesOfRelationshipWithKey(carriere, TO_CARRIERE_KEY);
			Integer idCarriere = (Integer)(ServerProxy.clientSideRequestPrimaryKeyForObject(editingContext(), carriere)).valueForKey("noSeqCarriere");
			setNoSeqCarriere(idCarriere);
			
		}
		else {
			removeObjectFromBothSidesOfRelationshipWithKey(carriere, TO_CARRIERE_KEY);
			setNoSeqCarriere(null);		
		}
	}
	
	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (!estValide()) {
			return;
		}
		
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début doit être fournie");
		}
		if (dateFin() != null && DateCtrl.isBefore(dateFin(),dateDebut())) {
			throw new NSValidation.ValidationException("Affectation - La date de début ne peut être postérieure à la date de fin");
		}
		if (toStructureUlr() == null){
			throw new NSValidation.ValidationException("Vous devez sélectionner une structure");
		}
		if (quotite() == null || quotite().intValue() == 0) {
			throw new NSValidation.ValidationException("La quotité ne peut pas être nulle");
		}
		if (quotite().intValue() > 100) {
			throw new NSValidation.ValidationException("La quotité ne peut être supérieure à 100");
		}

		if (temPrincipale() == null)
			setTemPrincipale(CocktailConstantes.VRAI);
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
	}

	public boolean estValide() {
		return temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}
	public boolean estPrincipale() {
		return temPrincipale().equals(CocktailConstantes.VRAI);
	}
	public void setEstPrincipale(boolean aBool) {
		if (aBool) {
			setTemPrincipale(CocktailConstantes.VRAI);
		} else {
			setTemPrincipale(CocktailConstantes.FAUX);
		}
	}


	/** Recherche des affectations d'un individu a une date donnee
	 * @param ec editing context
	 * @param individu concerne
	 * @param dateOcc date recherchee (peut etre nulle) */
	public static NSArray<EOAffectation> rechercherAffectationsADate(EOEditingContext ec, EOIndividu individu, NSTimestamp dateOcc) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			NSMutableArray orQualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));

			if (dateOcc != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + "<=%@", new NSArray(dateOcc)));

				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + ">=%@", new NSArray(dateOcc)));
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + "= nil", null));
				qualifiers.addObject(new EOOrQualifier(orQualifiers));
			}

			return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}




	/** recherche les affectations valides selon certains criteres
	 * @param editingContext
	 * @param qualifier
	 * @param relation a prefetcher
	 * @return tableau des affectations
	 */
	public static NSArray<EOAffectation> rechercherAffectationsAvecCriteres(EOEditingContext ec,EOQualifier qualifier) {
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		if (qualifier != null)
			andQualifiers.addObject(qualifier);

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
		return fetchAll(ec,new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOAffectation> findForIndividuEtPeriode(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
		andQualifiers.addObject(CocktailFinder.getQualifierBeforeEq(DATE_DEBUT_KEY, dateFin));

		NSMutableArray orQualifiers = new NSMutableArray();
		orQualifiers.addObject(CocktailFinder.getQualifierAfterEq(DATE_FIN_KEY, dateDebut));
		orQualifiers.addObject(CocktailFinder.getQualifierNullValue(DATE_FIN_KEY));
		andQualifiers.addObject(new EOOrQualifier(orQualifiers));

		return fetchAll(ec, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC );
	}


	/** Recherche des affectations d'un individu occupant un emploi a une date donnee 
	 * @param individu
	 * @param dateAff date affectation (peut etre nulle) 
	 * @return tableau des affectations
	 */
	public static NSArray<EOAffectation> findForIndividu(EOEditingContext ec, EOIndividu individu, NSTimestamp dateAff) {

		try {
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));

			if (dateAff != null) {			
				andQualifiers.addObject(CocktailFinder.getQualifierBeforeEq(DATE_DEBUT_KEY, dateAff));

				NSMutableArray orQualifiers = new NSMutableArray();
				orQualifiers.addObject(CocktailFinder.getQualifierAfterEq(DATE_FIN_KEY, dateAff));
				orQualifiers.addObject(CocktailFinder.getQualifierNullValue(DATE_FIN_KEY));
				andQualifiers.addObject(new EOOrQualifier(orQualifiers));
			} 

			return rechercherAffectationsAvecCriteres(ec, new EOAndQualifier(andQualifiers));	
		}
		catch (Exception e) {
			return new NSArray<EOAffectation>();
		}
	}
	/** Recherche des affectations d'un individu pendant une periode donnee. Les donnees ne sont pas raffraichies 
	 * @param individu
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 */
	public static NSArray<EOAffectation> rechercherAffectationsPourIndividuEtPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return rechercherAffectationsPourIndividuEtPeriode(edc,individu,debutPeriode,finPeriode,false);
	}

	/** Recherche des affectations d'un individu pendant une periode donnee 
	 * @param individu
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 * @param shouldRefresh true si on veut forcer le raffraichissement
	 */
	public static NSArray<EOAffectation> rechercherAffectationsPourIndividuEtPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean shouldRefresh) {
		try {
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));
			return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
		}
		catch (Exception e) {
			return new NSArray<EOAffectation>();
		}
	}

	/** recherche les occupations d'un individu commencant apres la date fournie en parametre */
	public static NSArray affectationsPosterieuresADate(EOEditingContext ec,EOIndividu individu,NSTimestamp date) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
		andQualifiers.addObject(CocktailFinder.getQualifierAfter(DATE_DEBUT_KEY, date));

		return fetchAll(ec, new EOAndQualifier(andQualifiers), null);
	}

	/**
	 * 
	 * @param dateFermeture
	 */
	public void  fermer(NSTimestamp dateFermeture) {
		if (dateFin() == null)
			setDateFin(dateFermeture);
	}

	/** ferme les affectations d'un agent valides a la date passee en parametre 
	 * et envoie si necessaire un message vers l'application de Conges */
	public static void fermerAffectations(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) throws Exception {
		NSArray objets = rechercherAffectationsPourIndividuEtPeriode(editingContext,individu,date,date);
		for (java.util.Enumeration<EOAffectation> e = objets.objectEnumerator();e.hasMoreElements();) {
			EOAffectation affectation = e.nextElement();
			affectation.setDateFin(date);
		}
		// supprimer les affectations postérieures à cette date
		objets = affectationsPosterieuresADate(editingContext,individu,date);
		for (Enumeration<EOAffectation> e1 = objets.objectEnumerator();e1.hasMoreElements();) {
			EOAffectation affectation = e1.nextElement();
			affectation.setEstValide(false); 
		}
	}
	
	/** Retourne la premiere affectation d'un individu ou null si pas d'affectation */
	public static EOAffectation rechercherPremiereAffectationPourIndividu(EOEditingContext edc,EOIndividu individu) {
		try {
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			return fetchFirstByQualifier(edc, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAffectation.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOAffectation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Affectation";
	public static final String ENTITY_TABLE_NAME = "MANGUE.AFFECTATION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noSeqAffectation";

	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String QUOTITE_KEY = "quotite";
	public static final String TEM_PRINCIPALE_KEY = "temPrincipale";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String NO_SEQ_AFFECTATION_KEY = "noSeqAffectation";
	public static final String NO_SEQ_CONTRAT_KEY = "noSeqContrat";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_COLKEY = "D_DEB_AFFECTATION";
	public static final String DATE_FIN_COLKEY = "D_FIN_AFFECTATION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String NO_SEQ_CARRIERE_COLKEY = "NO_SEQ_CARRIERE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String QUOTITE_COLKEY = "NUM_QUOT_AFFECTATION";
	public static final String TEM_PRINCIPALE_COLKEY = "TEM_PRINCIPALE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String NO_SEQ_AFFECTATION_COLKEY = "NO_SEQ_AFFECTATION";
	public static final String NO_SEQ_CONTRAT_COLKEY = "NO_SEQ_CONTRAT";


	// Relationships
	public static final String DETAILS_KEY = "details";
	public static final String INDIVIDU_KEY = "individu";
	public static final String PERSONNE_KEY = "personne";
	public static final String TO_CARRIERE_KEY = "toCarriere";
	public static final String TO_CONTRAT_KEY = "toContrat";
	public static final String TO_STRUCTURE_ULR_KEY = "toStructureUlr";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey(NO_DOSSIER_PERS_KEY);
  }

  public void setNoDossierPers(Integer value) {
    takeStoredValueForKey(value, NO_DOSSIER_PERS_KEY);
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey(NO_SEQ_CARRIERE_KEY);
  }

  public void setNoSeqCarriere(Integer value) {
    takeStoredValueForKey(value, NO_SEQ_CARRIERE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public Integer quotite() {
    return (Integer) storedValueForKey(QUOTITE_KEY);
  }

  public void setQuotite(Integer value) {
    takeStoredValueForKey(value, QUOTITE_KEY);
  }

  public String temPrincipale() {
    return (String) storedValueForKey(TEM_PRINCIPALE_KEY);
  }

  public void setTemPrincipale(String value) {
    takeStoredValueForKey(value, TEM_PRINCIPALE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite personne() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOCarriere toCarriere() {
    return (org.cocktail.mangue.modele.mangue.individu.EOCarriere)storedValueForKey(TO_CARRIERE_KEY);
  }

  public void setToCarriereRelationship(org.cocktail.mangue.modele.mangue.individu.EOCarriere value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CARRIERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CARRIERE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOContrat toContrat() {
    return (org.cocktail.mangue.modele.mangue.individu.EOContrat)storedValueForKey(TO_CONTRAT_KEY);
  }

  public void setToContratRelationship(org.cocktail.mangue.modele.mangue.individu.EOContrat value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOContrat oldValue = toContrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOStructure toStructureUlr() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(TO_STRUCTURE_ULR_KEY);
  }

  public void setToStructureUlrRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = toStructureUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_ULR_KEY);
    }
  }
  
  public NSArray details() {
    return (NSArray)storedValueForKey(DETAILS_KEY);
  }

  public NSArray details(EOQualifier qualifier) {
    return details(qualifier, null, false);
  }

  public NSArray details(EOQualifier qualifier, boolean fetch) {
    return details(qualifier, null, fetch);
  }

  public NSArray details(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail.TO_AFFECTATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = details();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDetailsRelationship(org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DETAILS_KEY);
  }

  public void removeFromDetailsRelationship(org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DETAILS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail createDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("AffectationDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DETAILS_KEY);
    return (org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail) eo;
  }

  public void deleteDetailsRelationship(org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDetailsRelationships() {
    Enumeration objects = details().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDetailsRelationship((org.cocktail.mangue.modele.mangue.lolf.EOAffectationDetail)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOAffectation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAffectation createEOAffectation(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, String temPrincipale
, String temValide
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu, org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite personne			) {
    EOAffectation eo = (EOAffectation) createAndInsertInstance(editingContext, _EOAffectation.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setTemPrincipale(temPrincipale);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setPersonneRelationship(personne);
    return eo;
  }

  
	  public EOAffectation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAffectation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAffectation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAffectation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOAffectation object = (EOAffectation)createAndInsertInstance(editingContext, _EOAffectation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAffectation localInstanceIn(EOEditingContext editingContext, EOAffectation eo) {
    EOAffectation localInstance = (eo == null) ? null : (EOAffectation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAffectation#localInstanceIn a la place.
   */
	public static EOAffectation localInstanceOf(EOEditingContext editingContext, EOAffectation eo) {
		return EOAffectation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAffectation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAffectation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAffectation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAffectation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAffectation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAffectation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAffectation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAffectation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAffectation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAffectation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAffectation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAffectation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

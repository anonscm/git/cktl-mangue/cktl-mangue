// EOConservationAnciennete.java
// Created on Fri Feb 21 10:26:56  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;


import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
public class EOConservationAnciennete extends _EOConservationAnciennete
{

	public EOConservationAnciennete() {
		super();
	}

	public boolean estUtilisee() {
		return temUtilise().equals("O");
	}
	public void setEstUtilisee(boolean yn) {
		if (yn)
			setTemUtilise("O");
		else
			setTemUtilise("N");
	}

	public static EOConservationAnciennete creer(EOEditingContext ec, EOElementCarriere element) {

		EOConservationAnciennete newObject = new EOConservationAnciennete();   

		newObject.setElementCarriereRelationship(element);

		newObject.setNoDossierPers(element.noDossierPers());
		newObject.setNoSeqElement(element.noSeqElement());
		newObject.setNoSeqCarriere(element.noSeqCarriere());

		newObject.setTemValide("O");
		newObject.setTemUtilise("N");
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		ec.insertObject(newObject);
		return newObject;
	}


	/**
	 * 
	 * @param editingContext
	 * @param elementCarriere
	 * @return
	 */
	public static NSArray<EOConservationAnciennete> rechercherAnciennetesNonUtiliseesPourElementCarriere(EOEditingContext editingContext,EOElementCarriere elementCarriere) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ELEMENT_CARRIERE_KEY + "=%@", new NSArray(elementCarriere)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY  + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_UTILISE_KEY  + "=%@", new NSArray("N")));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers));

	}
	
	/**
	 * 
	 * @param ec
	 * @param elementCarriere
	 * @return
	 */
	public static NSArray<EOConservationAnciennete> findForElementCarriere(EOEditingContext ec,EOElementCarriere elementCarriere) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ELEMENT_CARRIERE_KEY + "=%@", new NSArray(elementCarriere)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		return fetchAll(ec, new EOAndQualifier(qualifiers));
	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		if (ancNbAnnees() == null && ancNbMois() == null && ancNbJours() == null) {
			throw new NSValidation.ValidationException("Veuillez entrer une durée de conservation !");
		}
	}

}

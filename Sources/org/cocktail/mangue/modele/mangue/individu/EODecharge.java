// EODecharge.java
// Created on Fri Oct 20 14:57:15 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import java.math.BigDecimal;

import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EODecharge extends _EODecharge implements IDecharge {

	public static EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);

	public static NSArray SORT_ANNEE_DESC = new NSArray(new EOSortOrdering(PERIODE_DECHARGE_KEY, EOSortOrdering.CompareDescending));
	public static NSArray SORT_ARRAY_ANNEE_DESC = new NSArray(SORT_ANNEE_DESC);
	public static NSArray SORT_ANNEE_ASC = new NSArray(new EOSortOrdering(PERIODE_DECHARGE_KEY, EOSortOrdering.CompareAscending));
	public static NSArray SORT_ARRAY_ANNEE_ASC = new NSArray(SORT_ANNEE_ASC);

	public EODecharge() {
		super();
	}

	public static EODecharge creer(EOEditingContext ec, EOIndividu individu) {
		EODecharge newObject = (EODecharge) createAndInsertInstance(ec,ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setNbHDecharge(new BigDecimal(0));
		newObject.setIndividuRelationship(individu);
		return newObject;
	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		if (individu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu");
		}
		if (typeDecharge() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un type de décharge");
		}
		if (nbHDecharge() == null || nbHDecharge().floatValue() == 0) {
			throw new NSValidation.ValidationException("Vous devez fournir un nombre d'heures de décharge");
		}
		if (periodeDecharge() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une période de décharge");
		} else {
			int position = periodeDecharge().indexOf("/");
			if (position == -1 || position != 4 || periodeDecharge().length() > 9) {
				throw new NSValidation.ValidationException("Vous devez fournir une période de décharge sous la forme AAAA/BBBB, AAAA et BBBB correspondant à une année universitaire");
			}
			String anneeDebut = periodeDecharge().substring(0,position);
			String anneeFin = periodeDecharge().substring(position + 1);
			try {
				int anneeD = new Integer(anneeDebut).intValue();
				int anneeF = new Integer(anneeFin).intValue();
				if (anneeF != anneeD + 1) {
					throw new Exception("");
				}
			} catch (Exception e) {
				throw new NSValidation.ValidationException("Vous devez fournir une période de décharge sous la forme AAAA/BBBB, AAAA et BBBB correspondant à une année universitaire");
			}
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	/**
	 * 
	 * @return
	 */
	public static String stringAnneeUniversitaire(Integer annee) {
		return annee+"/"+(annee+1);
	}
	
	/* (non-Javadoc)
	 * @see org.cocktail.mangue.modele.mangue.individu.IDecharge#anneeUniversitaire()
	 */
	public Integer anneeUniversitaire() {
		if (periodeDecharge() != null) {
			int position = periodeDecharge().indexOf("/");
			return new Integer(periodeDecharge().substring(0,position));
		} else {
			return null;
		}
	}
	/* (non-Javadoc)
	 * @see org.cocktail.mangue.modele.mangue.individu.IDecharge#setAnneeUniversitaire(java.lang.Integer)
	 */
	public void setAnneeUniversitaire(Integer annee) {
		if (annee == null) {
			setPeriodeDecharge(null);
		} else {
			int anneeCourante = annee.intValue();
			setPeriodeDecharge("" + anneeCourante + "/" + (anneeCourante + 1));
		}
	}

	/**
	 * 
	 * @param ec
	 * @param qualifier
	 * @return
	 */
	public static NSArray<EODecharge> findForQualifier(EOEditingContext ec, EOQualifier qualifier) {
		try {
			return fetchAll(ec, qualifier, SORT_ARRAY_NOM_ASC );
		}
		catch (Exception e) {
			return new NSArray<EODecharge>();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EODecharge> findForIndividu(EOEditingContext ec,EOIndividu individu) {
		try {
			NSMutableArray args = new NSMutableArray(individu);
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",args);
			return fetchAll(ec,myQualifier, SORT_ARRAY_ANNEE_DESC );
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAbsences.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOAbsences extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Absences";
	public static final String ENTITY_TABLE_NAME = "MANGUE.ABSENCES";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "absNumero";

	public static final String ABS_AMPM_DEBUT_KEY = "absAmpmDebut";
	public static final String ABS_AMPM_FIN_KEY = "absAmpmFin";
	public static final String ABS_DUREE_TOTALE_KEY = "absDureeTotale";
	public static final String ABS_MOTIF_KEY = "absMotif";
	public static final String C_TYPE_EXCLUSION_KEY = "cTypeExclusion";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String ABS_NUMERO_KEY = "absNumero";
	public static final String ABS_TYPE_CODE_KEY = "absTypeCode";
	public static final String NO_INDIVIDU_KEY = "noIndividu";

//Colonnes dans la base de donnees
	public static final String ABS_AMPM_DEBUT_COLKEY = "ABS_AMPM_DEBUT";
	public static final String ABS_AMPM_FIN_COLKEY = "ABS_AMPM_FIN";
	public static final String ABS_DUREE_TOTALE_COLKEY = "ABS_DUREE_TOTALE";
	public static final String ABS_MOTIF_COLKEY = "ABS_MOTIF";
	public static final String C_TYPE_EXCLUSION_COLKEY = "C_TYPE_EXCLUSION";
	public static final String DATE_DEBUT_COLKEY = "ABS_DEBUT";
	public static final String DATE_FIN_COLKEY = "ABS_FIN";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String TEM_VALIDE_COLKEY = "ABS_VALIDE";

	public static final String ABS_NUMERO_COLKEY = "ABS_NUMERO";
	public static final String ABS_TYPE_CODE_COLKEY = "ABS_TYPE_CODE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";


	// Relationships
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_TYPE_ABSENCE_KEY = "toTypeAbsence";
	public static final String TYPE_EXCLUSION_KEY = "typeExclusion";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String absAmpmDebut() {
    return (String) storedValueForKey(ABS_AMPM_DEBUT_KEY);
  }

  public void setAbsAmpmDebut(String value) {
    takeStoredValueForKey(value, ABS_AMPM_DEBUT_KEY);
  }

  public String absAmpmFin() {
    return (String) storedValueForKey(ABS_AMPM_FIN_KEY);
  }

  public void setAbsAmpmFin(String value) {
    takeStoredValueForKey(value, ABS_AMPM_FIN_KEY);
  }

  public String absDureeTotale() {
    return (String) storedValueForKey(ABS_DUREE_TOTALE_KEY);
  }

  public void setAbsDureeTotale(String value) {
    takeStoredValueForKey(value, ABS_DUREE_TOTALE_KEY);
  }

  public String absMotif() {
    return (String) storedValueForKey(ABS_MOTIF_KEY);
  }

  public void setAbsMotif(String value) {
    takeStoredValueForKey(value, ABS_MOTIF_KEY);
  }

  public String cTypeExclusion() {
    return (String) storedValueForKey(C_TYPE_EXCLUSION_KEY);
  }

  public void setCTypeExclusion(String value) {
    takeStoredValueForKey(value, C_TYPE_EXCLUSION_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence toTypeAbsence() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence)storedValueForKey(TO_TYPE_ABSENCE_KEY);
  }

  public void setToTypeAbsenceRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence oldValue = toTypeAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ABSENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ABSENCE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion typeExclusion() {
    return (org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion)storedValueForKey(TYPE_EXCLUSION_KEY);
  }

  public void setTypeExclusionRelationship(org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion oldValue = typeExclusion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_EXCLUSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_EXCLUSION_KEY);
    }
  }
  

/**
 * Créer une instance de EOAbsences avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAbsences createEOAbsences(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
			) {
    EOAbsences eo = (EOAbsences) createAndInsertInstance(editingContext, _EOAbsences.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOAbsences localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAbsences)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAbsences creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAbsences creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOAbsences object = (EOAbsences)createAndInsertInstance(editingContext, _EOAbsences.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAbsences localInstanceIn(EOEditingContext editingContext, EOAbsences eo) {
    EOAbsences localInstance = (eo == null) ? null : (EOAbsences)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAbsences#localInstanceIn a la place.
   */
	public static EOAbsences localInstanceOf(EOEditingContext editingContext, EOAbsences eo) {
		return EOAbsences.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAbsences fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAbsences fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAbsences eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAbsences)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAbsences fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAbsences fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAbsences eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAbsences)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAbsences fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAbsences eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAbsences ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAbsences fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

//EOCarriere.java
//Created on Wed Mar 19 08:20:50  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;


import java.util.Enumeration;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation des segments de carriere :<BR>
 * 	Verification des longueurs de strings<BR>
 * 	La date de debut de carriere, le type de population doivent etre fournis<BR>
 *	Si la date de fin est fournie, celle de debut doit etre anterieure<BR> 
 *	La date de fin doit etre posterieure a celle des elements de carriere<BR>
 *	Il ne peut pas y avoir plus de deux segments de carriere sur une meme periode<BR>
 *  Il ne peut pas y avoir de contrat d'heberge pendant la periode<BR>
 * 	Lors de la suppression d'un segment, il est invalide ainsi que les elements de carriere, les
 * changements de position et les stages s'y rattachant.
 */

public class EOCarriere extends _EOCarriere implements RecordAvecLibelle {

	public static final String QUOTITE_ACTUELLE_KEY = "quotiteActuelle";

	private EOChangementPosition dernierePosition;

	public EOCarriere() {
		super();
	}

	public void awakeFromInsertion(EOEditingContext ed) {
		setEstValide(true);
	}

	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		setTemValide((aBool)?CocktailConstantes.VRAI : CocktailConstantes.FAUX);
	}

	public boolean estPriseEnChargeCir() {
		return temCir() != null && temCir().equals(CocktailConstantes.VRAI);
	}
	public void setEstPriseEnChargeCir(boolean aBool) {
		setTemCir((aBool)?CocktailConstantes.VRAI : CocktailConstantes.FAUX);
	}

	/** Retourne les changements de position valides pendant la periode passee en parametre
	 * @param dateDebut
	 * @param dateFin peut etre nulle
	 * @return tableau de changements de position ou null
	 */
	public NSArray<EOChangementPosition> changementsPositionPourPeriode(NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (dateDebut == null || changementsPosition() == null || changementsPosition().count() == 0) {
			return null;
		}
		NSArray<EOChangementPosition> positionsTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(changementsPosition(),EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);
		NSMutableArray<EOChangementPosition> results = new NSMutableArray<EOChangementPosition>();
		for (EOChangementPosition changement : positionsTriees) {
			if (changement.estValide()) {
				if (dateFin == null) {
					if (changement.dateFin() == null || DateCtrl.isAfterEq(changement.dateFin(),dateDebut)) {
						results.addObject(changement);
					}
				} else {
					if ((changement.dateFin() == null || DateCtrl.isAfterEq(changement.dateFin(),dateDebut)) &&
							DateCtrl.isBeforeEq(changement.dateDebut(),dateFin)) {
						results.addObject((changement));
					}
				}
			}
		}
		return results;
	}
	/** Retourne les changements de position valides hors de la periode passee en parametre
	 * @param dateDebut
	 * @param dateFin peut etre nulle
	 * @return tableau d'elements ou null
	 */
	public NSArray<EOChangementPosition> changementsHorsPeriode(NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (dateDebut == null || changementsPosition() == null || changementsPosition().count() == 0) {
			return null;
		}
		NSArray<EOChangementPosition> positionsTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(changementsPosition(),EOChangementPosition.SORT_ARRAY_DATE_DEBUT_ASC);
		NSMutableArray<EOChangementPosition> results = new NSMutableArray<EOChangementPosition>();
		for (EOChangementPosition changement : positionsTriees) {

			if (changement.estValide()) {
				if (dateFin == null) {
					if ((changement.dateFin() != null && DateCtrl.isBefore(changement.dateFin(),dateDebut)) ||
							DateCtrl.isBefore(changement.dateDebut(),dateDebut)) {
						results.addObject(changement);
					}
				} else {
					if (changement.dateFin() == null || 
							DateCtrl.isBefore(changement.dateDebut(),dateDebut) ||
							DateCtrl.isAfter(changement.dateFin(),dateFin)) {
						results.addObject((changement));
					}
				}
			}
		}
		return results;
	}


	/**
	 * Retourne la specialisation la plus recente
	 * @return
	 */
	public EOCarriereSpecialisations derniereSpecialisation() {
		if (specialisations() != null && specialisations().count() > 0) {
			NSArray<EOCarriereSpecialisations> specs = EOSortOrdering.sortedArrayUsingKeyOrderArray(specialisations(),  EOCarriereSpecialisations.SORT_ARRAY_DATE_DEBUT_DESC);
			return specs.get(0);
		} 
		return null;
	}



	/** retourne la position valide dont la date de debut est la plus recente */
	public EOChangementPosition dernierePosition() {
		if (changementsPosition() != null) {
			NSArray positionsTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(changementsPosition(),EOChangementPosition.SORT_ARRAY_DATE_DEBUT_DESC);
			// Retourne le premier changement de position valide trouvé
			for (java.util.Enumeration<EOChangementPosition> e = positionsTriees.objectEnumerator();e.hasMoreElements();) {
				EOChangementPosition changement = e.nextElement();
				if (changement.estValide()) {
					return changement;
				}
			}
		} 
		return null;
	}

	/** retourne l'element de carriere valide le plus recent */
	public EOElementCarriere dernierElement() {
		if (elements() != null && elements().count() > 0) {
			// les trier par ordre décroissant et afficher la spécialisation de l'élément le plus récent
			NSArray<EOElementCarriere> elements = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements(),  PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
			// Retourne le premier élément valide trouvé
			for (EOElementCarriere element : elements) {
				if (element.estValide()) {
					return element;
				}
			}
		} 
		return null;
	}

	/**
	 * Renvoie la quotite reelle recherchee dans les modalites de service a la date du jour
	 * @return
	 */
	public String quotiteActuelle() {
		EOModalitesService modalite = EOModalitesService.modalitePourDate(editingContext(), toIndividu(), DateCtrl.today());
		if (modalite != null)
			return modalite.quotite().toString() + "%";
		return "100%";
	}


	public EOBap toBap() {
		EOCarriereSpecialisations spec = derniereSpecialisation();
		if (spec != null)
			return spec.toBap();
		return null;
	}
	public EOCnu toCnu() {
		EOCarriereSpecialisations spec = derniereSpecialisation();
		if (spec != null) {
			return spec.toCnu();
		}
		return null;
	}
	public EODiscSecondDegre toDiscSecondDegre() {
		EOCarriereSpecialisations spec = derniereSpecialisation();
		if (spec != null) {
			return spec.toDiscSecondDegre();
		}
		return null;
	}
	public EOSpecialiteAtos toSpecialiteAtos() {
		EOCarriereSpecialisations spec = derniereSpecialisation();
		if (spec != null) {
			return spec.toSpecialiteAtos();
		}
		return null;
	}
	public EOSpecialiteItarf toSpecialiteItarf() {
		EOCarriereSpecialisations spec = derniereSpecialisation();
		if (spec != null) {
			return spec.toSpecialiteItarf();
		}
		return null;
	}
	public EOReferensEmplois toReferensEmploi() {
		EOCarriereSpecialisations spec = derniereSpecialisation();
		if (spec != null) {
			return spec.toReferensEmploi();
		}
		return null;
	}

	/** Retourne les elements de carriere valides pendant la periode passee en parametre
	 * @param dateDebut
	 * @param dateFin peut etre nulle
	 * @return tableau d'elements ou null
	 */
	public NSArray<EOElementCarriere> elementsPourPeriode(NSTimestamp dateDebut,NSTimestamp dateFin) {

		if (dateDebut == null || elements() == null || elements().count() == 0) {
			return new NSArray<EOElementCarriere>();
		}
		NSArray<EOElementCarriere> elementsTries = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements(), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		NSMutableArray<EOElementCarriere> results = new NSMutableArray<EOElementCarriere>();
		java.util.Enumeration<EOElementCarriere> e = elementsTries.objectEnumerator();
		while (e.hasMoreElements()) {
			EOElementCarriere element = e.nextElement();
			if (element.estValide()) {

				if (dateFin == null) {
					if (DateCtrl.isAfterEq(element.dateDebut(),dateDebut) || element.dateFin() == null || 
							DateCtrl.isAfterEq(element.dateFin(),dateDebut)) {
						results.addObject(element);
					}
				} else {
					if (!((element.dateFin() != null && DateCtrl.isBefore(element.dateFin(),dateDebut)) || DateCtrl.isAfter(element.dateDebut(),dateFin))) {
						results.addObject((element));
					}
				}
			}
		}
		return results;
	}

	/** Retourne les elements de carriere valides hors de la periode passee en parametre
	 * @param dateDebut
	 * @param dateFin peut etre nulle
	 * @return tableau d'elements ou null
	 */
	public NSArray<EOElementCarriere> elementsHorsPeriode(NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (dateDebut == null || elements() == null || elements().count() == 0) {
			return null;
		}
		NSArray elementsTries = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements(), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		NSMutableArray results = new NSMutableArray();
		for (java.util.Enumeration<EOElementCarriere> e = elementsTries.objectEnumerator();e.hasMoreElements();) {
			EOElementCarriere element = e.nextElement();
			if (element.estValide() && element.estProvisoire() == false) {
				if (dateFin == null) {
					if ((element.dateFin() != null && DateCtrl.isBefore(element.dateFin(),dateDebut)) ||
							DateCtrl.isBefore(element.dateDebut(),dateDebut)) {
						results.addObject(element);
					}
				} else {
					if (element.dateFin() == null || 
							DateCtrl.isBefore(element.dateDebut(),dateDebut) ||
							DateCtrl.isAfter(element.dateDebut(),dateFin) ||
							DateCtrl.isAfter(element.dateFin(),dateFin)) {
						results.addObject((element));
					}
				}
			}
		}
		return results;
	}

	/**
	 * 
	 * @return
	 */
	public NSArray<EOElementCarriere> elementsValides() {
		NSMutableArray<EOElementCarriere> results = new NSMutableArray<EOElementCarriere>();
		for (java.util.Enumeration<EOElementCarriere> e = elements().objectEnumerator();e.hasMoreElements();) {
			EOElementCarriere element = e.nextElement();
			if (element.estValide() && element.estProvisoire() == false) {
				results.addObject(element);
			} 
		}
		return results;
	}


	/** Retourne les elements de carriere valides associe au corps passe en parametre
	 * @param corps
	 * @return tableau d'elements ou null
	 */
	public NSArray<EOElementCarriere> elementsValidesPourCorps(EOCorps corps) {
		if (corps == null || elements() == null || elements().count() == 0) {
			return null;
		}
		NSArray<EOElementCarriere> elementsTries = EOSortOrdering.sortedArrayUsingKeyOrderArray(elements(), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		NSMutableArray<EOElementCarriere> results = new NSMutableArray<EOElementCarriere>();
		for (EOElementCarriere element : elementsTries) {
			if (element.toCorps() == corps && element.estValide() && element.estProvisoire() == false && element.estAnnule() == false) { // Faut-il les rejeter ?
				results.addObject(element);
			} 
		}
		return results;
	}

	/** Retourne les stages en cours pendant la periode passee en parametre
	 * @param dateDebut
	 * @param dateFin peut etre nulle
	 * @return tableau d'elements ou null
	 */
	public NSArray<EOStage> stagesPourPeriode(NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (dateDebut == null || stages() == null || stages().count() == 0) {
			return new NSArray<EOStage>();
		}
		NSArray<EOStage> stagesTries = EOSortOrdering.sortedArrayUsingKeyOrderArray(stages(), EOStage.SORT_ARRAY_DATE_DEBUT_ASC);
		NSMutableArray<EOStage> results = new NSMutableArray<EOStage>();

		for (EOStage stage : stagesTries) {

			if (stage.estValide()) {
				if (dateFin == null) {
					if (DateCtrl.isAfterEq(stage.dateDebut(),dateDebut) || stage.dateFin() == null || 
							DateCtrl.isAfterEq(stage.dateDebut(),dateDebut)) {
						results.addObject(stage);
					}
				} else {
					if ((DateCtrl.isBeforeEq(stage.dateDebut(),dateDebut) && (stage.dateFin() == null || DateCtrl.isAfterEq(stage.dateFin(),dateFin))) ||
							(DateCtrl.isAfterEq(stage.dateDebut(),dateDebut) && DateCtrl.isBeforeEq(stage.dateDebut(),dateFin)) ||
							(DateCtrl.isAfterEq(stage.dateFin(),dateDebut) && DateCtrl.isBeforeEq(stage.dateFin(),dateFin))) {
						results.addObject((stage));
					}
				}
			}
		}
		return results;
	}

	/** Retourne les stages valides hors de la periode passee en parametre
	 * @param dateDebut
	 * @param dateFin peut etre nulle
	 * @return tableau de stages ou null
	 */
	public NSArray<EOStage> stagesHorsPeriode(NSTimestamp dateDebut,NSTimestamp dateFin) {
		if (dateDebut == null || stages() == null || stages().count() == 0) {
			return null;
		}
		NSArray<EOStage> stagesTries = EOSortOrdering.sortedArrayUsingKeyOrderArray(stages(),EOStage.SORT_ARRAY_DATE_DEBUT_ASC);
		NSMutableArray<EOStage> results = new NSMutableArray<EOStage>();
		for (EOStage stage : stagesTries) {
			if (stage.estValide()) {
				if (dateFin == null) {
					if ((stage.dateFin() != null && DateCtrl.isBefore(stage.dateFin(),dateDebut))) {
						//|| DateCtrl.isBefore(stage.dateDebut(),dateDebut)) {
						results.addObject(stage);
					}
				} else {
					if (stage.dateFin() == null || 
							DateCtrl.isBefore(stage.dateDebut(),dateDebut) ||
							DateCtrl.isAfter(stage.dateDebut(),dateFin) ||
							DateCtrl.isAfter(stage.dateFin(),dateFin)) {
						results.addObject((stage));
					}
				}
			}
		}
		return results;
	}
	public String positionActivite() {
		if (dernierePosition != null && dernierePosition.toPosition() != null) {
			return dernierePosition.toPosition().temActivite();
		} else {
			return null;
		}
	}

	public String libellePosition() {
		if (dernierePosition != null && dernierePosition.toPosition() != null) {
			return dernierePosition.toPosition().libelleCourt();
		} else {
			return null;
		}
	}
	public String libelleMotif() {
		if (dernierePosition != null && dernierePosition.toMotifPosition() != null) {
			return dernierePosition.toMotifPosition().libelleLong();
		} else {
			return null;
		}
	}
	public String lieuOriginePosition() {
		if (dernierePosition != null) {
			return dernierePosition.lieuOrigine();
		} else {
			return null;
		}
	}
	public String lieuPosition() {
		if (dernierePosition != null) {
			return dernierePosition.lieuDestin();
		} else {
			return null;
		}
	}
	public String temEnseignant() {
		return toTypePopulation().temEnseignant();
	}
	/** determine le changement de position dont la date de debut est la plus recente */
	public void evaluerDernierePosition() {
		// déterminer le dernier changement position
		dernierePosition = dernierePosition();
	}
	/** determine le changement de position valide a la date passee en parametre
	 * Il n'y en a qu'une puisqu'il n'y a pas de chevauchement de periode */
	public EOChangementPosition evaluerPositionADate(NSTimestamp dateReference) {
		// déterminer le dernier changement position en prenant les changments de position valides pour la période
		NSArray<EOChangementPosition> changements = changementsPositionPourPeriode(dateReference, dateReference);
		if (changements != null && changements.size() > 0) {
			// On peut prendre le premier, ils sont tous valides
			dernierePosition = changements.get(0);
		} else {
			dernierePosition = null;
		}
		return dernierePosition;
	}


	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOCarriere creer(EOEditingContext edc, EOIndividu individu) {

		EOCarriere newObject = (EOCarriere) createAndInsertInstance(edc, ENTITY_NAME);    
		newObject.initAvecIndividu(individu);

		return newObject;

	}


	/** Initialise un segment de carriere pour un individu
	 * Signale que l'individu est un titulaire
	 * @param individu
	 */
	public void initAvecIndividu(EOIndividu individu) {
		setNoSeqCarriere(new Integer(obtenirNumeroSeqCarriere(individu)));
		setEstValide(true);
		setEstPriseEnChargeCir(true);
		setNoDossierPers(individu.noIndividu());
		setToIndividuRelationship(individu);
		if (individu.personnel().temTitulaire() == null || individu.personnel().temTitulaire().equals(CocktailConstantes.FAUX)) {
			individu.personnel().setTemTitulaire(CocktailConstantes.VRAI);
		}
		dernierePosition = null;
	}

	/**
	 * 
	 * @param ec
	 * @param carriere
	 */
	public static void invaliderElements(EOEditingContext edc, EOCarriere carriere) {

		NSArray<EOElementCarriere> elements = EOElementCarriere.findForCarriere(edc, carriere);
		for (java.util.Enumeration<EOElementCarriere> e = elements.objectEnumerator();e.hasMoreElements();) {
			EOElementCarriere.invalider(edc, e.nextElement());
		}
	}



	/**
	 * Invalide la carriere et les elements lies : positions, elements, stages
	 * @param ec
	 * @param carriere
	 */
	public static void invaliderCarriere(EOEditingContext ec, EOCarriere carriere) {

		NSArray<EOChangementPosition> changementsPosition = EOChangementPosition.rechercherChangementsPourCarriere(ec, carriere);
		for (EOChangementPosition changementPosition : changementsPosition) {
			changementPosition.invalider();
		}		

		NSArray<EOElementCarriere> elements = EOElementCarriere.findForCarriere(ec, carriere);
		for (EOElementCarriere element : elements) {
			EOElementCarriere.invalider(ec, element);
		}

		NSArray<EOStage> stages = EOStage.findForCarriere(ec, carriere);
		for (EOStage stage : stages) {
			stage.invalider();
		}

		if (carriere.depart() != null) {
			carriere.depart().invalider();
		}

		carriere.setEstValide(false);

	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (!estValide()) {
			return;		// On ne fait pas les validations, elles ont été faites quand le segment était valide
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "CARRIERE"));
		}
		if (dateFin() != null && DateCtrl.isBefore(dateFin(),dateDebut())) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "CARRIERE", dateFin(), dateDebut()));
		}

		if (toTypePopulation() == null) {
			throw new NSValidation.ValidationException("Vous devez  sélectionner un type de population");
		}

		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(), toIndividu(), dateDebut(), dateFin());
		if ((carrieres.count() == 2 && carrieres.containsObject(this) == false) || carrieres.count() > 2) { 	// 2 parce qu'un agent peut être ASU et ITARFs
			throw new NSValidation.ValidationException("On ne peut pas définir plus de deux segments de carrière sur une même période");
		}
		NSArray<EOElementCarriere> elements = elementsHorsPeriode(dateDebut(), dateFin());
		if (elements != null && elements.count() > 0) {
			throw new NSValidation.ValidationException("Il y a des éléments de carrière dont les dates sont antérieures ou postérieures aux date début et fin du segment.\nModifier d'abord les dates des éléments de carrière");
		}
		NSArray<EOChangementPosition> changements = changementsHorsPeriode(dateDebut(), dateFin());
		if (changements != null && changements.count() > 0) {
			throw new NSValidation.ValidationException("Il y a des changements de position dont les dates sont antérieures ou postérieures aux date début et fin du segment.\nModifier d'abord les dates des changements de position");
		}
		NSArray stages = stagesHorsPeriode(dateDebut(), dateFin());
		if (stages != null && stages.count() > 0) {
			throw new NSValidation.ValidationException("Il y a des stages dont les dates sont antérieures ou postérieures aux date début et fin du segment.\nModifier d'abord les dates des stages");
		}
		if (typeRecrutement() != null && toTypePopulation().estHospitalier() == false) {
			throw new NSValidation.ValidationException("Le type de recrutement ne s'applique qu'aux personnels hospitalo-universitaires");
		}

		setDModification(new NSTimestamp());

	}


	/** retourne true si on peut fermer le segment de carriere (nouvelle date fin > date debut),
	 * les elements de carriere, les changements
	 * de position et les stages a la date : la date de debut des elements
	 * des changements de position et des stages doit &ecirc;tre posterieure a la date
	 * passee en parametre
	 * @param date
	 * @return
	 */
	public boolean peutFermerADate(NSTimestamp date) {

		if (DateCtrl.isBefore(date, dateDebut())) {
			return false;
		}
		// les éléments de carrière
		NSArray<EOElementCarriere> elements = elementsPourPeriode(date,null);
		for (EOElementCarriere element : elements) {
			if (DateCtrl.isAfterEq(element.dateDebut(), date)) {
				return false;
			}
		}
		// les changements de position
		NSArray<EOChangementPosition> positions = changementsPositionPourPeriode(date,null);
		for (EOChangementPosition changement : positions) {
			if (DateCtrl.isAfterEq(changement.dateDebut(), date)) {
				return false;
			}
		}
		// les stages
		NSArray<EOStage> stages = stagesPourPeriode(date,null);
		for (EOStage stage : stages) {
			if (DateCtrl.isAfterEq(stage.dateDebut(), date)) {
				return false;
			}
		}
		return true;
	}


	/**
	 * Fermeture de la carriere, du dernier element associe, du dernier changement de position
	 * @param date
	 */
	public void fermerCarriereADate(NSTimestamp date) {


		// Fermeture du dernier element de carriere
		for (EOElementCarriere element : (NSArray<EOElementCarriere>)elements()) {
			
			if (DateCtrl.isAfter(element.dateDebut(), date)) {
				editingContext().deleteObject(element);
			}
			else {
				if (element.dateFin() == null || DateCtrl.isAfter(element.dateFin(), date)) {
					element.setDateFin(date);					
				}
			}
		}

		// Fermeture des changements de position
		NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsPourCarriere(editingContext(),this);
		if (changements != null && changements.count() > 0)
			changements.get(0).setDateFin(date);

		// Fermeture des stages
		NSArray<EOStage> stages = stagesPourPeriode(date,null);
		for(EOStage stage : stages) {
			if (stage.dateFin() == null || DateCtrl.isAfter(stage.dateFin(), date)) {
				stage.setDateFin(date);
			}
		}

		// Fermeture de la carriere
		setDateFin(date);

	}

	/**
	 * Changement des dates de fin des elements de carriere et des changements de position en fonction de la
	 * date de fin du segment de carriere
	 * @param ancienneDate
	 */
	public void modifierDateFinAutresInfos(NSTimestamp ancienneDate) {

		// Eléments de carrière
		NSArray<EOElementCarriere> elements = elementsPourPeriode(dateDebut(),dateFin());
		if (elements != null && elements.size() > 0) {
			for (EOElementCarriere element : elements) {

				if (element.dateFin() == null) {
					if (dateFin() != null && DateCtrl.isAfterEq(dateFin(), element.dateDebut())) {
						element.setDateFin(dateFin());
					}
				}
				else {
					if (element.dateFin() == null && ancienneDate == null || 
							(element.dateFin() != null && ancienneDate != null &&
							element.dateFin().equals(DateCtrl.dateToString(ancienneDate))) ||
							(element.dateFin() != null && dateFin() != null && 
							DateCtrl.isAfter(element.dateFin(), dateFin()))) {
						element.setDateFin(dateFin());
					}
				}
			}

			// Changements de position
			NSArray<EOChangementPosition> changements = changementsPositionPourPeriode(dateDebut(), dateFin());
			for (EOChangementPosition changement : changements) {
				if (changement.dateFin() == null) {
					if (dateFin() != null && DateCtrl.isAfterEq(dateFin(), changement.dateDebut())) {
						changement.setDateFin(dateFin());
					}
				}
				else {
					if (changement.dateFin() == null && ancienneDate == null || 
							(changement.dateFin() != null && ancienneDate != null &&
							changement.dateFinFormatee().equals(DateCtrl.dateToString(ancienneDate))) ||
							(changement.dateFin() != null && dateFin() != null && 
							DateCtrl.isAfter(changement.dateFin(), dateFin()))) {
						changement.setDateFin(dateFin());
					}
				}
			}

			// Stages. On ne change la date que si elle est postérieure
			NSArray<EOStage> stages = stagesPourPeriode(dateDebut(),dateFin());
			for (EOStage stage : stages) {
				if (stage.dateFin() == null || 
						(dateFin() != null && DateCtrl.isAfter(stage.dateFin(), dateFin()))) {
					stage.setDateFin(dateFin());
				}
			}
		}
	}

	/**
	 * 
	 */
	public String libelle() {
		if (toTypePopulation() == null) {
			return "";
		}
		String libelle = toTypePopulation().libelleCourt();
		if (dateDebut() != null) {
			if (dateFin() != null) {
				libelle = libelle + " du " + DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dateFin());
			} else {
				libelle = libelle + " à partir du " + DateCtrl.dateToString(dateDebut());
			}
		}
		return libelle;
	}


	/** 
	 * @param ec
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static EOCarriere rechercherCarriereADate(EOEditingContext ec,EOIndividu individu,NSTimestamp dateReference) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
		andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY, dateReference,DATE_FIN_KEY, dateReference));

		return fetchFirstByQualifier(ec, new EOAndQualifier(andQualifiers));
	}

	/**
	 * 
	 * @param edc
	 * @param typePopulation
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOCarriere> fetchForDateAndType(EOEditingContext edc, EOTypePopulation typePopulation,NSTimestamp dateReference) {

		try {

			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY,dateReference, DATE_FIN_KEY,dateReference));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_TYPE_POPULATION_KEY, typePopulation));

			return fetchAll(edc, new EOAndQualifier(andQualifiers));
		}
		catch (Exception e) {
			return null;			
		}
	}


	/** retourne les carrieres valides d'un individu pendant la periode fournie
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode peut etre nulle
	 * @param finPeriode peut etre nulle
	 * @param prefetch true si effectuer les prefetchs
	 * @param shouldRefresh true si faire un refresh des carrieres
	 */
	public static NSArray<EOCarriere> rechercherCarrieresSurPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {

		try {
			return fetchAll(edc, qualifierPourPeriode(individu, debutPeriode, finPeriode));
		}
		catch (Exception e) {
			return new NSArray();			
		}

	}

	/** retourne tous les segments de carrieres d'un individu
	 * @param editingContext
	 * @param individu
	 */
	public static NSArray<EOCarriere> rechercherTouteCarrierePourIndividu(EOEditingContext edc, EOIndividu individu) {
		return fetchAll(edc, CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
	}


	/** Recherche des carrieres d'un individu a une date donnee
	 * @param ec editing context
	 * @param individu concerne
	 * @param dateRef date recherchee (peut etre nulle) */
	public static NSArray<EOCarriere> rechercherCarrieresADate(EOEditingContext edc, EOIndividu individu, NSTimestamp dateRef) {

		try {

			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
			NSMutableArray orQualifiers = new NSMutableArray();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));

			if (dateRef != null) {
				
				andQualifiers.addObject(CocktailFinder.getQualifierBeforeEq(DATE_DEBUT_KEY, dateRef));
				
				orQualifiers.addObject(CocktailFinder.getQualifierAfterEq(DATE_FIN_KEY, dateRef));
				orQualifiers.addObject(CocktailFinder.getQualifierNullValue(DATE_FIN_KEY));
				andQualifiers.addObject(new EOOrQualifier(orQualifiers));
			}

			return fetchAll(edc, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);

		}
		catch (Exception e) {
			return new NSArray<EOCarriere>();
		}
	}



	/** recherche les segments de carriere valides d'un individu anterieurs a la date fournie
	 * @param editingContext
	 * @param individu
	 * @return tableau des elements de carriere
	 */
	public static NSArray<EOCarriere> rechercherCarrieresPourIndividuAnterieursADate(EOEditingContext ec,EOIndividu individu,NSTimestamp date) {

		try {

			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
			andQualifiers.addObject(CocktailFinder.getQualifierBeforeEq(DATE_DEBUT_KEY, date));

			return fetchAll(ec, new EOAndQualifier(andQualifiers), null);			
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/** retourne les carrieres valides d'un individu commencant a avant la date fournie
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @param prefetch true si prefetcher les relations sur elements et changements de position
	 */
	public static NSArray<EOCarriere> carrieresAnterieuresADate(EOEditingContext ec, EOIndividu individu, NSTimestamp date) {
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
		andQualifiers.addObject(CocktailFinder.getQualifierBeforeEq(DATE_DEBUT_KEY, date));

		return fetchAll(ec, new EOAndQualifier(andQualifiers));		
	}
	/** retourne les carrieres valides d'un individu commencant a apres la date fournie
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @param prefetch true si prefetcher les relations sur elements et changements de position
	 */
	public static NSArray<EOCarriere> carrieresPosterieuresADate(EOEditingContext ec,EOIndividu individu,NSTimestamp date,boolean prefetch) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
		andQualifiers.addObject(CocktailFinder.getQualifierAfter(DATE_DEBUT_KEY, date));

		return fetchAll(ec, new EOAndQualifier(andQualifiers));		
	}

	/** retourne la carriere a la date donnee ou null
	 * @param editingContext
	 * @param individu
	 * @param date	
	 */
	public static EOCarriere carrierePourDate(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) {
		// On ne peut en trouver qu'une, pas de chevauchement de segments de carrière
		NSArray<EOCarriere> carrieres = rechercherCarrieresSurPeriode(editingContext,individu,date,date);
		try {
			return carrieres.get(0);
		} catch (Exception e) {
			return null;
		}
	}

	/** retourne true si un individu a une carriere en cours pendant la periode
	 * @param editingContext
	 * @param individu
	 * @param dateDebut	
	 * @param dateFin	peut etre nul
	 */
	public static boolean aCarriereEnCoursSurPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		return rechercherCarrieresSurPeriode(editingContext,individu,dateDebut,dateFin).size() > 0;
	}

	/** 
	 * Retourne les periodes de carriere d'un individu pour la periode passee en parametre 
	 */
	public static NSArray<EOCarriere> fetchForIndividuAndPeriode(EOEditingContext ec,EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
		andQualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateDebut,DATE_FIN_KEY,dateFin));

		return fetchAll(ec,new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC );		
	}	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOCarriere> findCarrieresCIRPourIndividu(EOEditingContext ec, EOIndividu individu) {

		try {

			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_CIR_KEY, "O"));
			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY+"."+EOIndividu.TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			return fetchAll(ec, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);		
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}



	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOCarriere> findForPeriode(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		try {

			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));

			if (individu != null) {
				andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
				andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY+"."+EOIndividu.TEM_VALIDE_KEY + "=%@", "O"));
			}

			if (dateDebut != null && dateFin != null) {
				andQualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin));
			}

			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_POPULATION_KEY + "." + EOTypePopulation.CODE_KEY + " != 'N'", null));

			return fetchAll(ec, new EOAndQualifier(andQualifiers));
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/** retourne le qualifier pour determiner les segments de carriere sur une periode
	 * @param individu
	 * @param debutPeriode peut etre nulle
	 * @param finPeriode	 peut etre nulle
	 */
	public static EOQualifier qualifierPourPeriode(EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));

		if (debutPeriode != null) {
			andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
		}
		return new EOAndQualifier(andQualifiers);
	}

    /** retourne les carrieres valides d'un individu
     * @param editingContext
     * @param individu    
     * @param prefetch true si effectuer les prefetchs
     */
    public static NSArray<EOCarriere> findForIndividu(EOEditingContext edc,EOIndividu individu) {
    	try {
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
		return fetchAll(edc, new EOAndQualifier(andQualifiers));
    	}
    	catch (Exception e) {
    		return new NSArray<EOCarriere>();
    	}
    }

	/** Retourne les segments de carriere associes au depart passe en parametre
	 * @param depart
	 */
	public static NSArray<EOCarriere> rechercherCarrieresPourDepart(EOEditingContext ec,EODepart depart, boolean refresh) {
		try {
			return fetchAll(ec, CocktailFinder.getQualifierEqual(DEPART_KEY, depart), null);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	/**
	 * 	ferme tout ce qui se rattache a la carriere d'un individu valide a cette date et supprime tout ce qui commence au-dela
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @param depart
	 */
	public static void fermerCarrieres(EOEditingContext edc, EOIndividu individu, NSTimestamp date, EODepart depart) {

		NSArray<EOCarriere> objets = rechercherCarrieresSurPeriode(edc, individu, date, date);

		for (EOCarriere carriere : objets) {
			carriere.fermerCarriereADate(date);
			if (carriere.depart() != null) {
				if (carriere.depart() != depart) {
					carriere.depart().invalider();
					carriere.setDepartRelationship(depart);
				}
			} else {
				carriere.setDepartRelationship(depart);
			}
		}

		// Fermer les carrieres superieures a la date de reference
		NSArray<EOCarriere> carrieresPosterieures = carrieresPosterieuresADate(edc, individu, date, true);

		for (EOCarriere carriere : carrieresPosterieures) {
			boolean invalider = true;
			NSArray<EOElementCarriere> elements = EOElementCarriere.findForCarriere(edc, carriere);
			for (EOElementCarriere element :  elements) {
				if (element.estGereParEtablissement()) {
					invalider = false;
					break;
				}
			}
			if (invalider) {
				invaliderCarriere(edc, carriere); // invalide aussi les elements, les stages, les changements de position,...
			} else {
				carriere.fermerCarriereADate(date);
			}
		}
	}

	/**
	 * 
	 * @param individu
	 * @return
	 */
	private int obtenirNumeroSeqCarriere(EOIndividu individu) {
		NSArray<EOCarriere> carrieres = rechercherTouteCarrierePourIndividu(editingContext(),individu);
		if (carrieres.size() == 0) {
			// Premier segment de carrière
			return 1;
		} else {
			NSArray<EOCarriere> temp = EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, new NSArray(EOSortOrdering.sortOrderingWithKey("noSeqCarriere",EOSortOrdering.CompareDescending)));
			EOCarriere carriere = temp.get(0);
			return carriere.noSeqCarriere().intValue() + 1;
		}
	}
}

// _EOContratHeberges.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOContratHeberges.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import java.util.NoSuchElementException;

import org.cocktail.mangue.modele.PeriodePourIndividu;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOContratHeberges extends  PeriodePourIndividu {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "ContratHeberges";
	public static final String ENTITY_TABLE_NAME = "MANGUE.CONTRAT_HEBERGES";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ctrhOrdre";

	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String CTRH_HEURE_KEY = "ctrhHeure";
	public static final String CTRH_INM_KEY = "ctrhInm";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DETAIL_ORIGINE_KEY = "detailOrigine";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String CTRH_ORDRE_KEY = "ctrhOrdre";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String C_STRUCTURE_HEBERGEMENT_KEY = "cStructureHebergement";
	public static final String C_STRUCTURE_ORIGINE_KEY = "cStructureOrigine";

//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String CTRH_HEURE_COLKEY = "CTRH_HEURE";
	public static final String CTRH_INM_COLKEY = "CTRH_INM";
	public static final String DATE_DEBUT_COLKEY = "D_DEB_CONTRAT_INV";
	public static final String DATE_FIN_COLKEY = "D_FIN_CONTRAT_INV";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DETAIL_ORIGINE_COLKEY = "DETAIL_ORIGINE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_TYPE_CONTRAT_TRAV_COLKEY = "C_TYPE_CONTRAT_TRAV";
	public static final String CTRH_ORDRE_COLKEY = "CTRH_ORDRE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String C_STRUCTURE_HEBERGEMENT_COLKEY = "C_STRUCTURE";
	public static final String C_STRUCTURE_ORIGINE_COLKEY = "C_STRUCTURE_ORIGINE";


	// Relationships
	public static final String ADRESSE_STRUCTURE_KEY = "adresseStructure";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_STRUCTURE_HEBERGEMENT_KEY = "toStructureHebergement";
	public static final String TO_STRUCTURE_ORIGINE_KEY = "toStructureOrigine";
	public static final String TO_UAI_KEY = "toUai";
	public static final String TYPE_CONTRAT_TRAVAIL_KEY = "typeContratTravail";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String commentaire() {
    return (String) storedValueForKey(COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    takeStoredValueForKey(value, COMMENTAIRE_KEY);
  }

  public Integer ctrhHeure() {
    return (Integer) storedValueForKey(CTRH_HEURE_KEY);
  }

  public void setCtrhHeure(Integer value) {
    takeStoredValueForKey(value, CTRH_HEURE_KEY);
  }

  public Integer ctrhInm() {
    return (Integer) storedValueForKey(CTRH_INM_KEY);
  }

  public void setCtrhInm(Integer value) {
    takeStoredValueForKey(value, CTRH_INM_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public String detailOrigine() {
    return (String) storedValueForKey(DETAIL_ORIGINE_KEY);
  }

  public void setDetailOrigine(String value) {
    takeStoredValueForKey(value, DETAIL_ORIGINE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOAdresse adresseStructure() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOAdresse)storedValueForKey(ADRESSE_STRUCTURE_KEY);
  }

  public void setAdresseStructureRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOAdresse value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOAdresse oldValue = adresseStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_STRUCTURE_KEY);
    }
  }
    
  public org.cocktail.mangue.modele.grhum.EOCorps toCorps() {
    return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey(TO_CORPS_KEY);
  }

  public void setToCorpsRelationship(org.cocktail.mangue.modele.grhum.EOCorps value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CORPS_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOGrade toGrade() {
    return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey(TO_GRADE_KEY);
  }

  public void setToGradeRelationship(org.cocktail.mangue.modele.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GRADE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOStructure toStructureHebergement() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(TO_STRUCTURE_HEBERGEMENT_KEY);
  }

  public void setToStructureHebergementRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = toStructureHebergement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_HEBERGEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_HEBERGEMENT_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOStructure toStructureOrigine() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(TO_STRUCTURE_ORIGINE_KEY);
  }

  public void setToStructureOrigineRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = toStructureOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_ORIGINE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EORne toUai() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EORne)storedValueForKey(TO_UAI_KEY);
  }

  public void setToUaiRelationship(org.cocktail.mangue.common.modele.nomenclatures.EORne value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EORne oldValue = toUai();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UAI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_UAI_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail typeContratTravail() {
    return (org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail)storedValueForKey(TYPE_CONTRAT_TRAVAIL_KEY);
  }

  public void setTypeContratTravailRelationship(org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail oldValue = typeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CONTRAT_TRAVAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CONTRAT_TRAVAIL_KEY);
    }
  }
  

/**
 * Créer une instance de EOContratHeberges avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOContratHeberges createEOContratHeberges(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail typeContratTravail			) {
    EOContratHeberges eo = (EOContratHeberges) createAndInsertInstance(editingContext, _EOContratHeberges.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setTypeContratTravailRelationship(typeContratTravail);
    return eo;
  }

  
	  public EOContratHeberges localInstanceIn(EOEditingContext editingContext) {
	  		return (EOContratHeberges)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOContratHeberges creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOContratHeberges creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOContratHeberges object = (EOContratHeberges)createAndInsertInstance(editingContext, _EOContratHeberges.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOContratHeberges localInstanceIn(EOEditingContext editingContext, EOContratHeberges eo) {
    EOContratHeberges localInstance = (eo == null) ? null : (EOContratHeberges)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOContratHeberges#localInstanceIn a la place.
   */
	public static EOContratHeberges localInstanceOf(EOEditingContext editingContext, EOContratHeberges eo) {
		return EOContratHeberges.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOContratHeberges fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOContratHeberges fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOContratHeberges eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOContratHeberges)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOContratHeberges fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOContratHeberges fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOContratHeberges eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOContratHeberges)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOContratHeberges fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOContratHeberges eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOContratHeberges ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOContratHeberges fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

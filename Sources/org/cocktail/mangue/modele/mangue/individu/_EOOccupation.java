/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOccupation.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOccupation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Occupation";
	public static final String ENTITY_TABLE_NAME = "MANGUE.OCCUPATION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noOccupation";

	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String MOTIF_FIN_KEY = "motifFin";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String OBSERVATIONS_KEY = "observations";
	public static final String QUOTITE_KEY = "quotite";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String NO_OCCUPATION_KEY = "noOccupation";
	public static final String ID_EMPLOI_KEY = "idEmploi";
	public static final String NO_SEQ_CONTRAT_KEY = "noSeqContrat";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_COLKEY = "D_DEB_OCCUPATION";
	public static final String DATE_FIN_COLKEY = "D_FIN_OCCUPATION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String MOTIF_FIN_COLKEY = "MOTIF_FIN";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String NO_SEQ_CARRIERE_COLKEY = "NO_SEQ_CARRIERE";
	public static final String OBSERVATIONS_COLKEY = "OBSERVATIONS";
	public static final String QUOTITE_COLKEY = "NUM_MOYEN_UTILISE";
	public static final String TEM_TITULAIRE_COLKEY = "TEM_TITULAIRE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String NO_OCCUPATION_COLKEY = "NO_OCCUPATION";
	public static final String ID_EMPLOI_COLKEY = "ID_EMPLOI";
	public static final String NO_SEQ_CONTRAT_COLKEY = "NO_SEQ_CONTRAT";


	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CARRIERE_KEY = "toCarriere";
	public static final String TO_CONTRAT_KEY = "toContrat";
	public static final String TO_EMPLOI_KEY = "toEmploi";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String motifFin() {
    return (String) storedValueForKey(MOTIF_FIN_KEY);
  }

  public void setMotifFin(String value) {
    takeStoredValueForKey(value, MOTIF_FIN_KEY);
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey(NO_DOSSIER_PERS_KEY);
  }

  public void setNoDossierPers(Integer value) {
    takeStoredValueForKey(value, NO_DOSSIER_PERS_KEY);
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey(NO_SEQ_CARRIERE_KEY);
  }

  public void setNoSeqCarriere(Integer value) {
    takeStoredValueForKey(value, NO_SEQ_CARRIERE_KEY);
  }

  public String observations() {
    return (String) storedValueForKey(OBSERVATIONS_KEY);
  }

  public void setObservations(String value) {
    takeStoredValueForKey(value, OBSERVATIONS_KEY);
  }

  public java.math.BigDecimal quotite() {
    return (java.math.BigDecimal) storedValueForKey(QUOTITE_KEY);
  }

  public void setQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, QUOTITE_KEY);
  }

  public String temTitulaire() {
    return (String) storedValueForKey(TEM_TITULAIRE_KEY);
  }

  public void setTemTitulaire(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOCarriere toCarriere() {
    return (org.cocktail.mangue.modele.mangue.individu.EOCarriere)storedValueForKey(TO_CARRIERE_KEY);
  }

  public void setToCarriereRelationship(org.cocktail.mangue.modele.mangue.individu.EOCarriere value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOCarriere oldValue = toCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CARRIERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CARRIERE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOContrat toContrat() {
    return (org.cocktail.mangue.modele.mangue.individu.EOContrat)storedValueForKey(TO_CONTRAT_KEY);
  }

  public void setToContratRelationship(org.cocktail.mangue.modele.mangue.individu.EOContrat value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOContrat oldValue = toContrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.gpeec.EOEmploi toEmploi() {
    return (org.cocktail.mangue.common.modele.gpeec.EOEmploi)storedValueForKey(TO_EMPLOI_KEY);
  }

  public void setToEmploiRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploi value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.gpeec.EOEmploi oldValue = toEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EMPLOI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EMPLOI_KEY);
    }
  }
  

/**
 * Créer une instance de EOOccupation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOOccupation createEOOccupation(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, java.math.BigDecimal quotite
, String temTitulaire
, String temValide
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu, org.cocktail.mangue.common.modele.gpeec.EOEmploi toEmploi			) {
    EOOccupation eo = (EOOccupation) createAndInsertInstance(editingContext, _EOOccupation.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setQuotite(quotite);
		eo.setTemTitulaire(temTitulaire);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setToEmploiRelationship(toEmploi);
    return eo;
  }

  
	  public EOOccupation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOccupation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOccupation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOccupation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOOccupation object = (EOOccupation)createAndInsertInstance(editingContext, _EOOccupation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOOccupation localInstanceIn(EOEditingContext editingContext, EOOccupation eo) {
    EOOccupation localInstance = (eo == null) ? null : (EOOccupation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOOccupation#localInstanceIn a la place.
   */
	public static EOOccupation localInstanceOf(EOEditingContext editingContext, EOOccupation eo) {
		return EOOccupation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOOccupation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOccupation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOccupation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOccupation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOccupation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOccupation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOccupation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOccupation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOccupation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOccupation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOccupation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOccupation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOReliquatsAnciennete extends _EOReliquatsAnciennete {

	public static final EOSortOrdering SORT_ANNEE_DESC = new EOSortOrdering(ANC_ANNEE_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray<EOSortOrdering> SORT_ARRAY_ANNEE_DESC = new NSArray(SORT_ANNEE_DESC);
	
	public EOReliquatsAnciennete() {
		super();
	}
	public boolean estUtilisee() {
		return temUtilise().equals("O");
	}
	public void setEstUtilisee(boolean yn) {
		if (yn)
			setTemUtilise("O");
		else
			setTemUtilise("N");
	}


	public static EOReliquatsAnciennete creer(EOEditingContext ec, EOElementCarriere element) {

		EOReliquatsAnciennete newObject = (EOReliquatsAnciennete) createAndInsertInstance(ec, EOReliquatsAnciennete.ENTITY_NAME);    

		newObject.setElementCarriereRelationship(element);
		
		newObject.setNoDossierPers(element.noDossierPers());
		newObject.setNoSeqElement(element.noSeqElement());
		newObject.setNoSeqCarriere(element.noSeqCarriere());

		newObject.setTemValide(CocktailConstantes.VRAI);
		newObject.setTemUtilise(CocktailConstantes.FAUX);
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		return newObject;
	}

	/**
	 * 
	 * @param ec
	 * @param elementCarriere
	 * @return
	 */
	public static NSArray<EOReliquatsAnciennete> rechercherReliquatsNonUtilisesPourElementCarriere(EOEditingContext ec,EOElementCarriere elementCarriere) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(getQualifierElement(elementCarriere));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(getQualifierUtilise(false));

		return fetchAll(ec, new EOAndQualifier(qualifiers), null);

	}

	/** recherche les reliquats d'anciennetes associees a un element de carriere 
	 * @param editingContext
	 * @param elementCarriere element de carriere
	 * @return tableau des anciennetes
	 */
	public static NSArray rechercherReliquatsPourElementCarriere(EOEditingContext ec,EOElementCarriere elementCarriere) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(getQualifierElement(elementCarriere));
		qualifiers.addObject(getQualifierValide());

		return fetchAll(ec, new EOAndQualifier(qualifiers), null);

	}

	/**
	 * 
	 * @return
	 */
	public static EOQualifier getQualifierUtilise(boolean isUtilise) {
		return (isUtilise)?EOQualifier.qualifierWithQualifierFormat(TEM_UTILISE_KEY + "=%@", new NSArray<String>("O"))
				:EOQualifier.qualifierWithQualifierFormat(TEM_UTILISE_KEY + "=%@", new NSArray<String>("N"));
					
	}	
	/**
	 * 
	 * @return
	 */
	public static EOQualifier getQualifierValide() {
		return EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray<String>("O"));
	}	
	/**
	 * 
	 * @return
	 */
	public static EOQualifier getQualifierElement(EOElementCarriere element) {
		return EOQualifier.qualifierWithQualifierFormat(ELEMENT_CARRIERE_KEY + "=%@", new NSArray<EOElementCarriere>(element));
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		if (ancAnnee() == null) {
			throw new NSValidation.ValidationException("L'année est obligatoire !");
		}
		if (ancNbAnnees() == null && ancNbMois() == null && ancNbJours() == null) {
			throw new NSValidation.ValidationException("Veuillez entrer une durée de reduction !");
		}
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	}
}

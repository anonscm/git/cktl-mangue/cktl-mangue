//EOPeriodeHandicap.java
//Created on Thu Mar 22 15:19:51 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation:<BR>
 * La date debut doit etre fournie, elle ne doit pas &ecirc;tre anterieure a la date de naissance et posterieure a la date de deces si elle est fournie<BR>
 * La date de fin doit etre posterieure a la date de fin, elle ne doit pas etre posterieure a la date de deces si elle est fournie<BR>
 * L'individu ou l'enfant doivent etre fournis<BR>
 * Le taux doit etre compris entre 0 et 100%<BR>
 * La somme des taux des periodes qui se chevauchent doit etre inferieure ou egale a 100%
 * @author christine
 *
 */
// 26/06/2010 - modification pour le Cir
public class EOPeriodeHandicap extends _EOPeriodeHandicap {

	public EOPeriodeHandicap() {
		super();
	}

	public static EOPeriodeHandicap creer(EOEditingContext ec, EOIndividu individu) {
		EOPeriodeHandicap newObject = (EOPeriodeHandicap) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setTemValide(CocktailConstantes.VRAI);
		return newObject;		
	}

	public static EOPeriodeHandicap creer(EOEditingContext ec, EOEnfant enfant) {
		EOPeriodeHandicap newObject = (EOPeriodeHandicap) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setEnfantRelationship(enfant);
		newObject.setTemValide(CocktailConstantes.VRAI);

		return newObject;		
	}


	public void initAvecEnfant(EOEnfant enfant) {
		init();
		setEnfantRelationship(enfant);
	}
	public String dateAvisFormatee() {
		return SuperFinder.dateFormatee(this,DATE_AVIS_MEDECIN_KEY);
	}
	public void setDateAvisFormatee(String uneDate) {
		if (uneDate == null) {
			setDateAvisMedecin(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_AVIS_MEDECIN_KEY,uneDate);
		}
	}


	public static NSArray findForIndividu(EOEditingContext ec, EOIndividu individu) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu));
			return fetchAll(ec, qualifier, SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}


	public static NSArray rechercherPourEnfant(EOEditingContext ec, EOEnfant enfant) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(ENFANT_KEY + " = %@", new NSArray(enfant));
		return fetchAll(ec, qualifier, SORT_ARRAY_DATE_DEBUT);
	}

	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (situationHandicap() == null && enfant() == null)
			throw new NSValidation.ValidationException("La situation est obligatoire !");

		NSTimestamp dateNaissance = null,dateDeces = null;

		if (individu() != null) {
			dateNaissance = individu().dNaissance();
			dateDeces = individu().dDeces();
		} else if (enfant() != null) {
			dateNaissance = enfant().dNaissance();
			dateDeces = enfant().dDeces();
		}
		// La date de fin ne doit pas etre inferieure a la date de debut
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début doit être définie !");
		} else if (dateNaissance != null && DateCtrl.isBefore(dateDebut(), dateNaissance)) {
			throw new NSValidation.ValidationException("La date de début doit être postérieure à la date de naissance !");
		} else if (dateDeces != null && DateCtrl.isAfter(dateDebut(), dateDeces)) {
			throw new NSValidation.ValidationException("La date de début doit être antérieure à la date de décès !");
		} else if (dateFin() != null) {
			if (dateDebut().after(dateFin())) {
				throw new NSValidation.ValidationException("HANDICAP\nLa date de fin doit être postérieure à la date de début !");
			} else if (dateDeces != null && DateCtrl.isAfter(dateFin(), dateDeces)) {
				throw new NSValidation.ValidationException("La date de fin doit être antérieure à la date de décès !");
			} 
		}
		if (individu() == null && enfant() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu ou un enfant");
		}
		if (individu() != null && enfant() != null) {
			throw new NSValidation.ValidationException("Vous ne pouvez pas fournir un individu et un enfant");
		}
		if (tauxHandicap() != null) {
			double taux = tauxHandicap().doubleValue();
			if (taux < 0 || taux > 100) {
				throw new NSValidation.ValidationException("Le taux doit être compris entre 0 et 100%");
			}
			NSArray periodesHandicap = null;
			if (individu() != null) {
				// Vérifier que  la somme des taux sur la période est inférieure ou égale à 100%
				periodesHandicap = PeriodePourIndividu.rechercherDureesPourIndividuEtPeriode(editingContext(), "PeriodeHandicap", individu(), dateDebut(), dateFin());
			} else {
				NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("enfant = %@", new NSArray(enfant())));
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateDebut(),DATE_FIN_KEY,dateFin()));
				periodesHandicap = EOPeriodeHandicap.rechercherDureePourEntiteAvecCriteres(editingContext(), "PeriodeHandicap",new EOAndQualifier(qualifiers));
			}
			for (java.util.Enumeration<EOPeriodeHandicap> e = periodesHandicap.objectEnumerator();e.hasMoreElements();) {
				EOPeriodeHandicap periode = e.nextElement();
				if (periode.tauxHandicap() != null && periode != this) {
					taux += periode.tauxHandicap().doubleValue();
				}
			}
			if ( taux > 100) {
				throw new NSValidation.ValidationException("La somme des taux de handicap sur cette période ne doit pas dépasser 100%");
			}
		}

	}
	// CIR
	/** Retourne le taux de handicap comme une valeur positive <= 100 ou null encodee sur 3 caracteres **/
	public String tauxInvalidite() {
		if (tauxHandicap() == null) {
			return null;
		} else {
			int taux = tauxHandicap().intValue();
			if (taux == 0) {
				return null;
			} else {
				String tauxInvalidite = new Integer(taux).toString();
				while (tauxInvalidite.length() < 3) {
					tauxInvalidite = "0" + tauxInvalidite;
				}
				return tauxInvalidite;
			}
		}
	}
	// méthodes protégées
	protected void init() {
		// pas d'initialisation spécifique
	}

}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOChangementPosition.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOChangementPosition extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ChangementPosition";
	public static final String ENTITY_TABLE_NAME = "MANGUE.CHANGEMENT_POSITION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "changementOrdre";

	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_POSITION_KEY = "lieuPosition";
	public static final String LIEU_POSITION_ORIG_KEY = "lieuPositionOrig";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String QUOTITE_KEY = "quotite";
	public static final String TEMOIN_POSITION_PREV_KEY = "temoinPositionPrev";
	public static final String TEM_PC_ACQUITEE_KEY = "temPcAcquitee";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String TXT_POSITION_KEY = "txtPosition";
	public static final String TXT_POSITION_ORIG_KEY = "txtPositionOrig";

// Attributs non visibles
	public static final String C_MOTIF_POSITION_KEY = "cMotifPosition";
	public static final String C_PAYS_KEY = "cPays";
	public static final String C_POSITION_KEY = "cPosition";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_RNE_ORIG_KEY = "cRneOrig";
	public static final String CARRIERE_ACCUEIL_KEY = "carriereAccueil";
	public static final String CARRIERE_ORIGINE_KEY = "carriereOrigine";
	public static final String CHANGEMENT_ORDRE_KEY = "changementOrdre";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_ENFANT_KEY = "noEnfant";
	public static final String ABS_NUMERO_KEY = "absNumero";
	public static final String PAS_ORDRE_KEY = "pasOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_ARRETE_COLKEY = "D_ARRETE_POSITION";
	public static final String DATE_DEBUT_COLKEY = "D_DEB_POSITION";
	public static final String DATE_FIN_COLKEY = "D_FIN_POSITION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LIEU_POSITION_COLKEY = "LIEU_POSITION";
	public static final String LIEU_POSITION_ORIG_COLKEY = "LIEU_POSITION_ORIG";
	public static final String NO_ARRETE_COLKEY = "NO_ARRETE_POSITION";
	public static final String QUOTITE_COLKEY = "QUOTITE_POSITION";
	public static final String TEMOIN_POSITION_PREV_COLKEY = "TEMOIN_POSITION_PREV";
	public static final String TEM_PC_ACQUITEE_COLKEY = "TEM_PC_ACQUITEE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";
	public static final String TXT_POSITION_COLKEY = "TXT_POSITION";
	public static final String TXT_POSITION_ORIG_COLKEY = "TXT_POSITION_ORIG";

	public static final String C_MOTIF_POSITION_COLKEY = "C_MOTIF_POSITION";
	public static final String C_PAYS_COLKEY = "C_PAYS";
	public static final String C_POSITION_COLKEY = "C_POSITION";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_RNE_ORIG_COLKEY = "C_RNE_ORIG";
	public static final String CARRIERE_ACCUEIL_COLKEY = "CARRIERE_ACCUEIL";
	public static final String CARRIERE_ORIGINE_COLKEY = "CARRIERE_ORIGINE";
	public static final String CHANGEMENT_ORDRE_COLKEY = "CPOS_ORDRE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String NO_ENFANT_COLKEY = "NO_ENFANT";
	public static final String ABS_NUMERO_COLKEY = "ABS_NUMERO";
	public static final String PAS_ORDRE_COLKEY = "PAS_ORDRE";


	// Relationships
	public static final String ABSENCE_KEY = "absence";
	public static final String CARRIERE_KEY = "carriere";
	public static final String ENFANT_KEY = "enfant";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String PAYS_KEY = "pays";
	public static final String TO_CARRIERE_ORIGINE_KEY = "toCarriereOrigine";
	public static final String TO_MOTIF_POSITION_KEY = "toMotifPosition";
	public static final String TO_PASSE_KEY = "toPasse";
	public static final String TO_POSITION_KEY = "toPosition";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_RNE_ORIGINE_KEY = "toRneOrigine";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey(DATE_ARRETE_KEY);
  }

  public void setDateArrete(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ARRETE_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String lieuPosition() {
    return (String) storedValueForKey(LIEU_POSITION_KEY);
  }

  public void setLieuPosition(String value) {
    takeStoredValueForKey(value, LIEU_POSITION_KEY);
  }

  public String lieuPositionOrig() {
    return (String) storedValueForKey(LIEU_POSITION_ORIG_KEY);
  }

  public void setLieuPositionOrig(String value) {
    takeStoredValueForKey(value, LIEU_POSITION_ORIG_KEY);
  }

  public String noArrete() {
    return (String) storedValueForKey(NO_ARRETE_KEY);
  }

  public void setNoArrete(String value) {
    takeStoredValueForKey(value, NO_ARRETE_KEY);
  }

  public Integer quotite() {
    return (Integer) storedValueForKey(QUOTITE_KEY);
  }

  public void setQuotite(Integer value) {
    takeStoredValueForKey(value, QUOTITE_KEY);
  }

  public String temoinPositionPrev() {
    return (String) storedValueForKey(TEMOIN_POSITION_PREV_KEY);
  }

  public void setTemoinPositionPrev(String value) {
    takeStoredValueForKey(value, TEMOIN_POSITION_PREV_KEY);
  }

  public String temPcAcquitee() {
    return (String) storedValueForKey(TEM_PC_ACQUITEE_KEY);
  }

  public void setTemPcAcquitee(String value) {
    takeStoredValueForKey(value, TEM_PC_ACQUITEE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public String txtPosition() {
    return (String) storedValueForKey(TXT_POSITION_KEY);
  }

  public void setTxtPosition(String value) {
    takeStoredValueForKey(value, TXT_POSITION_KEY);
  }

  public String txtPositionOrig() {
    return (String) storedValueForKey(TXT_POSITION_ORIG_KEY);
  }

  public void setTxtPositionOrig(String value) {
    takeStoredValueForKey(value, TXT_POSITION_ORIG_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOAbsences absence() {
    return (org.cocktail.mangue.modele.mangue.individu.EOAbsences)storedValueForKey(ABSENCE_KEY);
  }

  public void setAbsenceRelationship(org.cocktail.mangue.modele.mangue.individu.EOAbsences value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOAbsences oldValue = absence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ABSENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ABSENCE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOCarriere carriere() {
    return (org.cocktail.mangue.modele.mangue.individu.EOCarriere)storedValueForKey(CARRIERE_KEY);
  }

  public void setCarriereRelationship(org.cocktail.mangue.modele.mangue.individu.EOCarriere value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOCarriere oldValue = carriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CARRIERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CARRIERE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOEnfant enfant() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOEnfant)storedValueForKey(ENFANT_KEY);
  }

  public void setEnfantRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOEnfant value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOEnfant oldValue = enfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENFANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENFANT_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOPays pays() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOPays)storedValueForKey(PAYS_KEY);
  }

  public void setPaysRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOPays value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOPays oldValue = pays();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAYS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAYS_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOCarriere toCarriereOrigine() {
    return (org.cocktail.mangue.modele.mangue.individu.EOCarriere)storedValueForKey(TO_CARRIERE_ORIGINE_KEY);
  }

  public void setToCarriereOrigineRelationship(org.cocktail.mangue.modele.mangue.individu.EOCarriere value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOCarriere oldValue = toCarriereOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CARRIERE_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CARRIERE_ORIGINE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.carriere.EOMotifPosition toMotifPosition() {
    return (org.cocktail.mangue.common.modele.nomenclatures.carriere.EOMotifPosition)storedValueForKey(TO_MOTIF_POSITION_KEY);
  }

  public void setToMotifPositionRelationship(org.cocktail.mangue.common.modele.nomenclatures.carriere.EOMotifPosition value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.carriere.EOMotifPosition oldValue = toMotifPosition();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MOTIF_POSITION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MOTIF_POSITION_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOPasse toPasse() {
    return (org.cocktail.mangue.modele.mangue.individu.EOPasse)storedValueForKey(TO_PASSE_KEY);
  }

  public void setToPasseRelationship(org.cocktail.mangue.modele.mangue.individu.EOPasse value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOPasse oldValue = toPasse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PASSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PASSE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition toPosition() {
    return (org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition)storedValueForKey(TO_POSITION_KEY);
  }

  public void setToPositionRelationship(org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition oldValue = toPosition();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_POSITION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_POSITION_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EORne toRne() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EORne)storedValueForKey(TO_RNE_KEY);
  }

  public void setToRneRelationship(org.cocktail.mangue.common.modele.nomenclatures.EORne value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EORne toRneOrigine() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EORne)storedValueForKey(TO_RNE_ORIGINE_KEY);
  }

  public void setToRneOrigineRelationship(org.cocktail.mangue.common.modele.nomenclatures.EORne value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EORne oldValue = toRneOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_ORIGINE_KEY);
    }
  }
  

/**
 * Créer une instance de EOChangementPosition avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOChangementPosition createEOChangementPosition(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer quotite
, String temoinPositionPrev
, String temPcAcquitee
, String temValide
			) {
    EOChangementPosition eo = (EOChangementPosition) createAndInsertInstance(editingContext, _EOChangementPosition.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setQuotite(quotite);
		eo.setTemoinPositionPrev(temoinPositionPrev);
		eo.setTemPcAcquitee(temPcAcquitee);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOChangementPosition localInstanceIn(EOEditingContext editingContext) {
	  		return (EOChangementPosition)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOChangementPosition creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOChangementPosition creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOChangementPosition object = (EOChangementPosition)createAndInsertInstance(editingContext, _EOChangementPosition.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOChangementPosition localInstanceIn(EOEditingContext editingContext, EOChangementPosition eo) {
    EOChangementPosition localInstance = (eo == null) ? null : (EOChangementPosition)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOChangementPosition#localInstanceIn a la place.
   */
	public static EOChangementPosition localInstanceOf(EOEditingContext editingContext, EOChangementPosition eo) {
		return EOChangementPosition.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOChangementPosition fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOChangementPosition fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOChangementPosition eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOChangementPosition)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOChangementPosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOChangementPosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOChangementPosition eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOChangementPosition)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOChangementPosition fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOChangementPosition eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOChangementPosition ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOChangementPosition fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

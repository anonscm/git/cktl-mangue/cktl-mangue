// EOSituation.java
// Created on Tue Feb 17 16:28:59 Europe/Paris 2009 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

/** Permet d'affecter la localisation geographique d'un individu.<BR>
 * C'est une structure de type "Service", "Laboratoire" ou "Ecole Doctorale".<BR>
 * La quotite totale pendant une meme periode ne peut depasser 100%.<BR>
 * Les dates doivent etre valides, la longueur des champs aussi
 */
import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOSituationGeographique extends _EOSituationGeographique {

	public static final EOSortOrdering SORT_AFF_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_AFF_DEBUT_DESC = new EOSortOrdering(DATE_FIN_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_DEBUT_ASC = new NSArray(SORT_AFF_DEBUT_ASC);
	public static final NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(SORT_AFF_DEBUT_DESC);

	public final static String[] TYPES_POUR_SITUATION_GEO = {"EN","ED","LA","S"};

	public EOSituationGeographique() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOSituationGeographique creer(EOEditingContext edc, EOIndividu individu, EOAffectation affectation) {

		EOSituationGeographique newObject = new EOSituationGeographique();
		edc.insertObject(newObject);

		newObject.setIndividuRelationship(individu);
		newObject.setToAffectationRelationship(affectation);
		newObject.init();
		return newObject;
	}
	public static EOSituationGeographique creer(EOEditingContext ec, EOAffectation affectation) {

		EOSituationGeographique newObject = new EOSituationGeographique();
		ec.insertObject(newObject);

		newObject.setIndividuRelationship(affectation.individu());
		newObject.setDateDebut(affectation.dateDebut());
		newObject.setDateFin(affectation.dateFin());
		newObject.init();
		return newObject;
	}
	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		super.validateForSave();

		if (estValide()) {
			if (structure() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir une structure !");
			}
			if (quotite() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir une quotité !");
			}
			if (situCommentaire() != null && situCommentaire().length() > 1000) {
				throw new NSValidation.ValidationException("Le commentaire ne peut dépasser 1000 caractères");
			}

			if (toAffectation() == null) {
				throw new NSValidation.ValidationException("SITUATIO GEO - Pas d'affectation associée ! ");
			}

			if (DateCtrl.isBefore(dateDebut(), toAffectation().dateDebut())) {
				throw new NSValidation.ValidationException("Le début de la situation géographique doit être postérieur à la date de début d'affectation !");			 
			}
			if (dateFin() == null && toAffectation().dateFin() != null)
				throw new NSValidation.ValidationException("La fin de la situation géographique ne peut pas être postérieure à celle de l'affectation !");			 
			else {
				if (dateFin() != null ) {
					if (DateCtrl.isAfter(dateFin() , toAffectation().dateFin())) {
						throw new NSValidation.ValidationException("La fin de la situation géographique doit être antérieure à la date de fin d'affectation !");			 				
					}
				}
			}
		}

	}

	/** Recherche les situations valides pour un individu 
	 * @param editingContext
	 * @param individu */
	public static NSArray<EOSituationGeographique> findForIndividu(EOEditingContext editingContext, EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			qualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
			return fetchAll(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOSituationGeographique>();
		}
	}
	/** Recherche les situations valides pour un individu 
	 * @param editingContext
	 * @param individu */
	public static NSArray<EOSituationGeographique> findSituationsInvalides(EOEditingContext editingContext, EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			qualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_AFFECTATION_KEY + "=nil", null));
			return fetchAll(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOSituationGeographique>();
		}
	}
	public static NSArray<EOSituationGeographique> findForAffectation(EOEditingContext editingContext, EOAffectation affectation) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			qualifiers.addObject(CocktailFinder.getQualifierEqual(TO_AFFECTATION_KEY, affectation));
			return fetchAll(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOSituationGeographique>();
		}
	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub
		setTemValide(CocktailConstantes.VRAI);
		setQuotite(new Integer(100));
	}
}

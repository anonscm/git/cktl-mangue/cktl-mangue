/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOArrivee.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOArrivee extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Arrivee";
	public static final String ENTITY_TABLE_NAME = "MANGUE.ARRIVEE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noArrivee";

	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D1_AFFECTATION_KEY = "d1Affectation";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INM_KEY = "inm";
	public static final String LIEU_ARRIVEE_KEY = "lieuArrivee";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_PRIVE_KEY = "temPrive";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_RNE_PROVENANCE_KEY = "cRneProvenance";
	public static final String C_TYPE_ACCES_KEY = "cTypeAcces";
	public static final String NO_ARRIVEE_KEY = "noArrivee";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";

//Colonnes dans la base de donnees
	public static final String C_ECHELON_COLKEY = "C_ECHELON";
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String D1_AFFECTATION_COLKEY = "D_1_AFFECTATION";
	public static final String DATE_ARRETE_COLKEY = "D_ARRETE_ARRIVEE";
	public static final String DATE_DEBUT_COLKEY = "D_EFFET_ARRIVEE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String INM_COLKEY = "INM";
	public static final String LIEU_ARRIVEE_COLKEY = "LIEU_ARRIVEE";
	public static final String NO_ARRETE_COLKEY = "NO_ARRETE_ARRIVEE";
	public static final String TEM_PRIVE_COLKEY = "TEM_PRIVE";
	public static final String TEM_TITULAIRE_COLKEY = "TEM_TITULAIRE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String C_RNE_PROVENANCE_COLKEY = "C_RNE_PROVENANCE";
	public static final String C_TYPE_ACCES_COLKEY = "C_TYPE_ACCES";
	public static final String NO_ARRIVEE_COLKEY = "NO_ARRIVEE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";


	// Relationships
	public static final String CORPS_KEY = "corps";
	public static final String GRADE_KEY = "grade";
	public static final String INDIVIDU_KEY = "individu";
	public static final String RNE_PROVENANCE_KEY = "rneProvenance";
	public static final String TYPE_ACCES_KEY = "typeAcces";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cEchelon() {
    return (String) storedValueForKey(C_ECHELON_KEY);
  }

  public void setCEchelon(String value) {
    takeStoredValueForKey(value, C_ECHELON_KEY);
  }

  public String commentaire() {
    return (String) storedValueForKey(COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    takeStoredValueForKey(value, COMMENTAIRE_KEY);
  }

  public NSTimestamp d1Affectation() {
    return (NSTimestamp) storedValueForKey(D1_AFFECTATION_KEY);
  }

  public void setD1Affectation(NSTimestamp value) {
    takeStoredValueForKey(value, D1_AFFECTATION_KEY);
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey(DATE_ARRETE_KEY);
  }

  public void setDateArrete(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ARRETE_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer inm() {
    return (Integer) storedValueForKey(INM_KEY);
  }

  public void setInm(Integer value) {
    takeStoredValueForKey(value, INM_KEY);
  }

  public String lieuArrivee() {
    return (String) storedValueForKey(LIEU_ARRIVEE_KEY);
  }

  public void setLieuArrivee(String value) {
    takeStoredValueForKey(value, LIEU_ARRIVEE_KEY);
  }

  public String noArrete() {
    return (String) storedValueForKey(NO_ARRETE_KEY);
  }

  public void setNoArrete(String value) {
    takeStoredValueForKey(value, NO_ARRETE_KEY);
  }

  public String temPrive() {
    return (String) storedValueForKey(TEM_PRIVE_KEY);
  }

  public void setTemPrive(String value) {
    takeStoredValueForKey(value, TEM_PRIVE_KEY);
  }

  public String temTitulaire() {
    return (String) storedValueForKey(TEM_TITULAIRE_KEY);
  }

  public void setTemTitulaire(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.EOCorps corps() {
    return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey(CORPS_KEY);
  }

  public void setCorpsRelationship(org.cocktail.mangue.modele.grhum.EOCorps value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOCorps oldValue = corps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CORPS_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOGrade grade() {
    return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey(GRADE_KEY);
  }

  public void setGradeRelationship(org.cocktail.mangue.modele.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOGrade oldValue = grade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GRADE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EORne rneProvenance() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EORne)storedValueForKey(RNE_PROVENANCE_KEY);
  }

  public void setRneProvenanceRelationship(org.cocktail.mangue.common.modele.nomenclatures.EORne value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EORne oldValue = rneProvenance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RNE_PROVENANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RNE_PROVENANCE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces typeAcces() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces)storedValueForKey(TYPE_ACCES_KEY);
  }

  public void setTypeAccesRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces oldValue = typeAcces();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACCES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACCES_KEY);
    }
  }
  

/**
 * Créer une instance de EOArrivee avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOArrivee createEOArrivee(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temPrive
			) {
    EOArrivee eo = (EOArrivee) createAndInsertInstance(editingContext, _EOArrivee.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemPrive(temPrive);
    return eo;
  }

  
	  public EOArrivee localInstanceIn(EOEditingContext editingContext) {
	  		return (EOArrivee)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOArrivee creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOArrivee creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOArrivee object = (EOArrivee)createAndInsertInstance(editingContext, _EOArrivee.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOArrivee localInstanceIn(EOEditingContext editingContext, EOArrivee eo) {
    EOArrivee localInstance = (eo == null) ? null : (EOArrivee)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOArrivee#localInstanceIn a la place.
   */
	public static EOArrivee localInstanceOf(EOEditingContext editingContext, EOArrivee eo) {
		return EOArrivee.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOArrivee fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOArrivee fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOArrivee eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOArrivee)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOArrivee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOArrivee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOArrivee eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOArrivee)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOArrivee fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOArrivee eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOArrivee ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOArrivee fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

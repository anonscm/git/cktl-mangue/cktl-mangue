// EOIndividuTypeElectionHu.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Gestion des types d'election pour les personnels hospitalo-universitaires : ces types ne sont a definir
 * que pour les personnels des corps : <BR>
 * Regles de gestion :<BR>
 * le type de gestion et la	date de d&eacute;but sont obligatoires<BR>
 * la date de debut est anterieure a la date de fin<BR>
 * l'individu doit avoir un contrat ou un segment HU pendant la periode<BR>
 * ces donnees ne peuvent se chevaucher<BR>
 * @author christine
 *
 */
public class EOIndividuTypeElectionHu extends PeriodePourIndividu {

    public EOIndividuTypeElectionHu() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOIndividuTypeElectionHu(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeElectionHu typeElection() {
        return (org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeElectionHu)storedValueForKey("typeElection");
    }

    public void setTypeElection(org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeElectionHu value) {
        takeStoredValueForKey(value, "typeElection");
    }
    // Méthodes ajoutées
    public void supprimerRelations() {
    	super.supprimerRelations();
    	removeObjectFromBothSidesOfRelationshipWithKey(typeElection(), "typeElection");
    }
    public void validateForSave() throws NSValidation.ValidationException {
    	super.validateForSave();
    	if (typeElection() == null) {
    		throw new NSValidation.ValidationException("Le type d'élection est obligatoire");
    	}
    	// Vérifier si l'individu a un segment ou un contrat HU pendant la p&eacute;riode
    	if (individu().aSegmentHospitalierSurPeriode(dateDebut(), dateFin()) == false && individu().aContratHospitalierSurPeriode(dateDebut(), dateFin()) == false) {
    		throw new NSValidation.ValidationException("Cet individu n'a pas de carrière ou de contrat hospitalo-univ pendant la période");
    	}
    	// Vérifier si il y a un chevauchement des périodes
    	NSArray types = rechercherTypesPourIndividuEtDates(editingContext(), individu(), dateDebut(), dateFin(), false);
    	if (types.count() > 0) {
    		if (types.objectAtIndex(0) != this) {
    			throw new NSValidation.ValidationException("Les périodes ne peuvent se chevaucher");
    		}
    	}
    }
    // Méthodes protégées
	protected void init() {		
	}
	// Méthodes statiques
	/** Recherche les types d'un individu pour la periode passee en parametre
	 * @param individu individu concerne
	 * @param dateDebut debut de la periode (peut etre nulle)
	 * @param dateFin fin de la periode (peut etre nulle)
	 * @param shouldRefresh true si il faut raffraichir les donnees
	 */
	public static NSArray rechercherTypesPourIndividuEtDates(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean shouldRefresh) {
		return PeriodePourIndividu.rechercherDureesPourIndividuEtPeriode(editingContext, "IndividuTypeElectionHu", individu, debutPeriode, finPeriode,shouldRefresh);
	}
}

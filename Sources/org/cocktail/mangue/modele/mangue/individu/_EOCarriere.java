/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCarriere.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCarriere extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Carriere";
	public static final String ENTITY_TABLE_NAME = "MANGUE.CARRIERE";



	// Attributes


	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MIN_ELEM_CAR_KEY = "dMinElemCar";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String TEM_CIR_KEY = "temCir";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String C_TYPE_POPULATION_KEY = "cTypePopulation";
	public static final String NO_DEPART_KEY = "noDepart";
	public static final String TRHU_ORDRE_KEY = "trhuOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_COLKEY = "D_DEB_CARRIERE";
	public static final String DATE_FIN_COLKEY = "D_FIN_CARRIERE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MIN_ELEM_CAR_COLKEY = "D_MIN_ELEM_CAR";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String NO_SEQ_CARRIERE_COLKEY = "NO_SEQ_CARRIERE";
	public static final String TEM_CIR_COLKEY = "TEM_CIR";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String C_TYPE_POPULATION_COLKEY = "C_TYPE_POPULATION";
	public static final String NO_DEPART_COLKEY = "NO_DEPART";
	public static final String TRHU_ORDRE_COLKEY = "TRHU_ORDRE";


	// Relationships
	public static final String CHANGEMENTS_POSITION_KEY = "changementsPosition";
	public static final String DEPART_KEY = "depart";
	public static final String ELEMENTS_KEY = "elements";
	public static final String SPECIALISATIONS_KEY = "specialisations";
	public static final String STAGES_KEY = "stages";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_TYPE_POPULATION_KEY = "toTypePopulation";
	public static final String TYPE_RECRUTEMENT_KEY = "typeRecrutement";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dMinElemCar() {
    return (NSTimestamp) storedValueForKey(D_MIN_ELEM_CAR_KEY);
  }

  public void setDMinElemCar(NSTimestamp value) {
    takeStoredValueForKey(value, D_MIN_ELEM_CAR_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey(NO_DOSSIER_PERS_KEY);
  }

  public void setNoDossierPers(Integer value) {
    takeStoredValueForKey(value, NO_DOSSIER_PERS_KEY);
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey(NO_SEQ_CARRIERE_KEY);
  }

  public void setNoSeqCarriere(Integer value) {
    takeStoredValueForKey(value, NO_SEQ_CARRIERE_KEY);
  }

  public String temCir() {
    return (String) storedValueForKey(TEM_CIR_KEY);
  }

  public void setTemCir(String value) {
    takeStoredValueForKey(value, TEM_CIR_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EODepart depart() {
    return (org.cocktail.mangue.modele.mangue.individu.EODepart)storedValueForKey(DEPART_KEY);
  }

  public void setDepartRelationship(org.cocktail.mangue.modele.mangue.individu.EODepart value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EODepart oldValue = depart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPART_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPART_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation toTypePopulation() {
    return (org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation)storedValueForKey(TO_TYPE_POPULATION_KEY);
  }

  public void setToTypePopulationRelationship(org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation oldValue = toTypePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_POPULATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_POPULATION_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu typeRecrutement() {
    return (org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu)storedValueForKey(TYPE_RECRUTEMENT_KEY);
  }

  public void setTypeRecrutementRelationship(org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.hospitalo.EOTypeRecrutementHu oldValue = typeRecrutement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_RECRUTEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_RECRUTEMENT_KEY);
    }
  }
  
  public NSArray changementsPosition() {
    return (NSArray)storedValueForKey(CHANGEMENTS_POSITION_KEY);
  }

  public NSArray changementsPosition(EOQualifier qualifier) {
    return changementsPosition(qualifier, null, false);
  }

  public NSArray changementsPosition(EOQualifier qualifier, boolean fetch) {
    return changementsPosition(qualifier, null, fetch);
  }

  public NSArray changementsPosition(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.EOChangementPosition.CARRIERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.EOChangementPosition.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = changementsPosition();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToChangementsPositionRelationship(org.cocktail.mangue.modele.mangue.individu.EOChangementPosition object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CHANGEMENTS_POSITION_KEY);
  }

  public void removeFromChangementsPositionRelationship(org.cocktail.mangue.modele.mangue.individu.EOChangementPosition object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CHANGEMENTS_POSITION_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOChangementPosition createChangementsPositionRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ChangementPosition");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CHANGEMENTS_POSITION_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.EOChangementPosition) eo;
  }

  public void deleteChangementsPositionRelationship(org.cocktail.mangue.modele.mangue.individu.EOChangementPosition object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CHANGEMENTS_POSITION_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllChangementsPositionRelationships() {
    Enumeration objects = changementsPosition().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteChangementsPositionRelationship((org.cocktail.mangue.modele.mangue.individu.EOChangementPosition)objects.nextElement());
    }
  }

  public NSArray elements() {
    return (NSArray)storedValueForKey(ELEMENTS_KEY);
  }

  public NSArray elements(EOQualifier qualifier) {
    return elements(qualifier, null, false);
  }

  public NSArray elements(EOQualifier qualifier, boolean fetch) {
    return elements(qualifier, null, fetch);
  }

  public NSArray elements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.EOElementCarriere.TO_CARRIERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.EOElementCarriere.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = elements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToElementsRelationship(org.cocktail.mangue.modele.mangue.individu.EOElementCarriere object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ELEMENTS_KEY);
  }

  public void removeFromElementsRelationship(org.cocktail.mangue.modele.mangue.individu.EOElementCarriere object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ELEMENTS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOElementCarriere createElementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ElementCarriere");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ELEMENTS_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.EOElementCarriere) eo;
  }

  public void deleteElementsRelationship(org.cocktail.mangue.modele.mangue.individu.EOElementCarriere object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ELEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllElementsRelationships() {
    Enumeration objects = elements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteElementsRelationship((org.cocktail.mangue.modele.mangue.individu.EOElementCarriere)objects.nextElement());
    }
  }

  public NSArray specialisations() {
    return (NSArray)storedValueForKey(SPECIALISATIONS_KEY);
  }

  public NSArray specialisations(EOQualifier qualifier) {
    return specialisations(qualifier, null, false);
  }

  public NSArray specialisations(EOQualifier qualifier, boolean fetch) {
    return specialisations(qualifier, null, fetch);
  }

  public NSArray specialisations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations.TO_CARRIERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = specialisations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSpecialisationsRelationship(org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations object) {
    addObjectToBothSidesOfRelationshipWithKey(object, SPECIALISATIONS_KEY);
  }

  public void removeFromSpecialisationsRelationship(org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SPECIALISATIONS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations createSpecialisationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CarriereSpecialisations");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, SPECIALISATIONS_KEY);
    return (org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations) eo;
  }

  public void deleteSpecialisationsRelationship(org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SPECIALISATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSpecialisationsRelationships() {
    Enumeration objects = specialisations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSpecialisationsRelationship((org.cocktail.mangue.modele.mangue.EOCarriereSpecialisations)objects.nextElement());
    }
  }

  public NSArray stages() {
    return (NSArray)storedValueForKey(STAGES_KEY);
  }

  public NSArray stages(EOQualifier qualifier) {
    return stages(qualifier, null, false);
  }

  public NSArray stages(EOQualifier qualifier, boolean fetch) {
    return stages(qualifier, null, fetch);
  }

  public NSArray stages(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.EOStage.CARRIERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.EOStage.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = stages();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToStagesRelationship(org.cocktail.mangue.modele.mangue.individu.EOStage object) {
    addObjectToBothSidesOfRelationshipWithKey(object, STAGES_KEY);
  }

  public void removeFromStagesRelationship(org.cocktail.mangue.modele.mangue.individu.EOStage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, STAGES_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOStage createStagesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Stage");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, STAGES_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.EOStage) eo;
  }

  public void deleteStagesRelationship(org.cocktail.mangue.modele.mangue.individu.EOStage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, STAGES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllStagesRelationships() {
    Enumeration objects = stages().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteStagesRelationship((org.cocktail.mangue.modele.mangue.individu.EOStage)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCarriere avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCarriere createEOCarriere(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, String temCir
, String temValide
			) {
    EOCarriere eo = (EOCarriere) createAndInsertInstance(editingContext, _EOCarriere.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setTemCir(temCir);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOCarriere localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCarriere)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCarriere creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCarriere creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCarriere object = (EOCarriere)createAndInsertInstance(editingContext, _EOCarriere.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCarriere localInstanceIn(EOEditingContext editingContext, EOCarriere eo) {
    EOCarriere localInstance = (eo == null) ? null : (EOCarriere)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCarriere#localInstanceIn a la place.
   */
	public static EOCarriere localInstanceOf(EOEditingContext editingContext, EOCarriere eo) {
		return EOCarriere.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCarriere fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCarriere fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCarriere eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCarriere)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCarriere fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCarriere fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCarriere eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCarriere)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCarriere fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCarriere eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCarriere ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCarriere fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVacataires.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVacataires extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Vacataires";
	public static final String ENTITY_TABLE_NAME = "MANGUE.Vacataires";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "vacId";

	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ENSEIGNEMENT_KEY = "enseignement";
	public static final String NBR_HEURES_KEY = "nbrHeures";
	public static final String NBR_HEURES_REALISEES_KEY = "nbrHeuresRealisees";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String OBSERVATIONS_KEY = "observations";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TAUX_HORAIRE_KEY = "tauxHoraire";
	public static final String TAUX_HORAIRE_REALISE_KEY = "tauxHoraireRealise";
	public static final String TEM_AUTORISATION_CUMUL_KEY = "temAutorisationCumul";
	public static final String TEM_DEPASSEMENT_AUTO_KEY = "temDepassementAuto";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_PAIEMENT_PONCTUEL_KEY = "temPaiementPonctuel";
	public static final String TEM_PERS_ETAB_KEY = "temPersEtab";
	public static final String TEM_SIGNE_KEY = "temSigne";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String C_TYPE_CONTRAT_TRAV_KEY = "cTypeContratTrav";
	public static final String C_UAI_KEY = "cUai";
	public static final String NO_CNU_KEY = "noCnu";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String PRO_CODE_KEY = "proCode";
	public static final String SIT_ID_KEY = "sitId";
	public static final String VAC_ID_KEY = "vacId";

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String DATE_ARRETE_COLKEY = "DATE_ARRETE";
	public static final String DATE_DEBUT_COLKEY = "D_DEB_VACATION";
	public static final String DATE_FIN_COLKEY = "D_FIN_VACATION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ENSEIGNEMENT_COLKEY = "ENSEIGNEMENT";
	public static final String NBR_HEURES_COLKEY = "NBR_HEURES";
	public static final String NBR_HEURES_REALISEES_COLKEY = "NBR_HEURES_REALISEES";
	public static final String NO_ARRETE_COLKEY = "NO_ARRETE";
	public static final String OBSERVATIONS_COLKEY = "OBSERVATIONS";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String TAUX_HORAIRE_COLKEY = "TAUX_HORAIRE";
	public static final String TAUX_HORAIRE_REALISE_COLKEY = "TAUX_HORAIRE_REALISE";
	public static final String TEM_AUTORISATION_CUMUL_COLKEY = "TEM_AUTORISATION_CUMUL";
	public static final String TEM_DEPASSEMENT_AUTO_COLKEY = "TEM_DEPASSEMENT_AUTO";
	public static final String TEM_ENSEIGNANT_COLKEY = "TEM_ENSEIGNANT";
	public static final String TEM_PAIEMENT_PONCTUEL_COLKEY = "TEM_PAYE";
	public static final String TEM_PERS_ETAB_COLKEY = "TEM_PERS_ETAB";
	public static final String TEM_SIGNE_COLKEY = "TEM_SIGNE";
	public static final String TEM_TITULAIRE_COLKEY = "TEM_TITULAIRE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String C_TYPE_CONTRAT_TRAV_COLKEY = "C_TYPE_CONTRAT_TRAV";
	public static final String C_UAI_COLKEY = "C_UAI";
	public static final String NO_CNU_COLKEY = "NO_CNU";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String PRO_CODE_COLKEY = "PRO_CODE";
	public static final String SIT_ID_COLKEY = "SIT_ID";
	public static final String VAC_ID_COLKEY = "VAC_ID";


	// Relationships
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_AFFECTATIONS_KEY = "toAffectations";
	public static final String TO_CNU_KEY = "toCnu";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_GRADE_KEY = "toGrade";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_INDIVIDU_IDENTITE_KEY = "toIndividuIdentite";
	public static final String TO_PROFESSION_KEY = "toProfession";
	public static final String TO_STRUCTURE_KEY = "toStructure";
	public static final String TO_TYPE_CONTRAT_TRAVAIL_KEY = "toTypeContratTravail";
	public static final String TO_UAI_KEY = "toUai";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey(DATE_ARRETE_KEY);
  }

  public void setDateArrete(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ARRETE_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String enseignement() {
    return (String) storedValueForKey(ENSEIGNEMENT_KEY);
  }

  public void setEnseignement(String value) {
    takeStoredValueForKey(value, ENSEIGNEMENT_KEY);
  }

  public java.math.BigDecimal nbrHeures() {
    return (java.math.BigDecimal) storedValueForKey(NBR_HEURES_KEY);
  }

  public void setNbrHeures(java.math.BigDecimal value) {
    takeStoredValueForKey(value, NBR_HEURES_KEY);
  }

  public java.math.BigDecimal nbrHeuresRealisees() {
    return (java.math.BigDecimal) storedValueForKey(NBR_HEURES_REALISEES_KEY);
  }

  public void setNbrHeuresRealisees(java.math.BigDecimal value) {
    takeStoredValueForKey(value, NBR_HEURES_REALISEES_KEY);
  }

  public String noArrete() {
    return (String) storedValueForKey(NO_ARRETE_KEY);
  }

  public void setNoArrete(String value) {
    takeStoredValueForKey(value, NO_ARRETE_KEY);
  }

  public String observations() {
    return (String) storedValueForKey(OBSERVATIONS_KEY);
  }

  public void setObservations(String value) {
    takeStoredValueForKey(value, OBSERVATIONS_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public java.math.BigDecimal tauxHoraire() {
    return (java.math.BigDecimal) storedValueForKey(TAUX_HORAIRE_KEY);
  }

  public void setTauxHoraire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TAUX_HORAIRE_KEY);
  }

  public java.math.BigDecimal tauxHoraireRealise() {
    return (java.math.BigDecimal) storedValueForKey(TAUX_HORAIRE_REALISE_KEY);
  }

  public void setTauxHoraireRealise(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TAUX_HORAIRE_REALISE_KEY);
  }

  public String temAutorisationCumul() {
    return (String) storedValueForKey(TEM_AUTORISATION_CUMUL_KEY);
  }

  public void setTemAutorisationCumul(String value) {
    takeStoredValueForKey(value, TEM_AUTORISATION_CUMUL_KEY);
  }

  public String temDepassementAuto() {
    return (String) storedValueForKey(TEM_DEPASSEMENT_AUTO_KEY);
  }

  public void setTemDepassementAuto(String value) {
    takeStoredValueForKey(value, TEM_DEPASSEMENT_AUTO_KEY);
  }

  public String temEnseignant() {
    return (String) storedValueForKey(TEM_ENSEIGNANT_KEY);
  }

  public void setTemEnseignant(String value) {
    takeStoredValueForKey(value, TEM_ENSEIGNANT_KEY);
  }

  public String temPaiementPonctuel() {
    return (String) storedValueForKey(TEM_PAIEMENT_PONCTUEL_KEY);
  }

  public void setTemPaiementPonctuel(String value) {
    takeStoredValueForKey(value, TEM_PAIEMENT_PONCTUEL_KEY);
  }

  public String temPersEtab() {
    return (String) storedValueForKey(TEM_PERS_ETAB_KEY);
  }

  public void setTemPersEtab(String value) {
    takeStoredValueForKey(value, TEM_PERS_ETAB_KEY);
  }

  public String temSigne() {
    return (String) storedValueForKey(TEM_SIGNE_KEY);
  }

  public void setTemSigne(String value) {
    takeStoredValueForKey(value, TEM_SIGNE_KEY);
  }

  public String temTitulaire() {
    return (String) storedValueForKey(TEM_TITULAIRE_KEY);
  }

  public void setTemTitulaire(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOAdresse toAdresse() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOAdresse)storedValueForKey(TO_ADRESSE_KEY);
  }
  
  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
      return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
    }

  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
        EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
        fetchSpec.setIsDeep(true);
        fetchSpec.setUsesDistinct(distinct);
        if (prefetchs != null)
            fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
        NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
        return eoObjects;
      }


  public void setToAdresseRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOAdresse value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOAdresse oldValue = toAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ADRESSE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu toCnu() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu)storedValueForKey(TO_CNU_KEY);
  }

  public void setToCnuRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu oldValue = toCnu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CNU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CNU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOCorps toCorps() {
    return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey(TO_CORPS_KEY);
  }

  public void setToCorpsRelationship(org.cocktail.mangue.modele.grhum.EOCorps value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CORPS_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOGrade toGrade() {
    return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey(TO_GRADE_KEY);
  }

  public void setToGradeRelationship(org.cocktail.mangue.modele.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOGrade oldValue = toGrade();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GRADE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GRADE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite toIndividuIdentite() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite)storedValueForKey(TO_INDIVIDU_IDENTITE_KEY);
  }

  public void setToIndividuIdentiteRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite oldValue = toIndividuIdentite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_IDENTITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_IDENTITE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOProfession toProfession() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOProfession)storedValueForKey(TO_PROFESSION_KEY);
  }

  public void setToProfessionRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOProfession value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOProfession oldValue = toProfession();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PROFESSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PROFESSION_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOStructure toStructure() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail toTypeContratTravail() {
    return (org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail)storedValueForKey(TO_TYPE_CONTRAT_TRAVAIL_KEY);
  }

  public void setToTypeContratTravailRelationship(org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail oldValue = toTypeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CONTRAT_TRAVAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CONTRAT_TRAVAIL_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EORne toUai() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EORne)storedValueForKey(TO_UAI_KEY);
  }

  public void setToUaiRelationship(org.cocktail.mangue.common.modele.nomenclatures.EORne value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EORne oldValue = toUai();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_UAI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_UAI_KEY);
    }
  }
  
  public NSArray toAffectations() {
    return (NSArray)storedValueForKey(TO_AFFECTATIONS_KEY);
  }

  public NSArray toAffectations(EOQualifier qualifier) {
    return toAffectations(qualifier, null, false);
  }

  public NSArray toAffectations(EOQualifier qualifier, boolean fetch) {
    return toAffectations(qualifier, null, fetch);
  }

  public NSArray toAffectations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation.TO_VACATAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toAffectations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToAffectationsRelationship(org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_AFFECTATIONS_KEY);
  }

  public void removeFromToAffectationsRelationship(org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_AFFECTATIONS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation createToAffectationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("VacatairesAffectation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_AFFECTATIONS_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation) eo;
  }

  public void deleteToAffectationsRelationship(org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_AFFECTATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToAffectationsRelationships() {
    Enumeration objects = toAffectations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToAffectationsRelationship((org.cocktail.mangue.modele.mangue.individu.EOVacatairesAffectation)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOVacataires avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVacataires createEOVacataires(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temEnseignant
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu			) {
    EOVacataires eo = (EOVacataires) createAndInsertInstance(editingContext, _EOVacataires.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemEnseignant(temEnseignant);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  
	  public EOVacataires localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVacataires)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVacataires creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVacataires creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVacataires object = (EOVacataires)createAndInsertInstance(editingContext, _EOVacataires.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVacataires localInstanceIn(EOEditingContext editingContext, EOVacataires eo) {
    EOVacataires localInstance = (eo == null) ? null : (EOVacataires)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVacataires#localInstanceIn a la place.
   */
	public static EOVacataires localInstanceOf(EOEditingContext editingContext, EOVacataires eo) {
		return EOVacataires.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVacataires fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVacataires fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVacataires eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVacataires)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVacataires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVacataires fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVacataires eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVacataires)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVacataires fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVacataires eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVacataires ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVacataires fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

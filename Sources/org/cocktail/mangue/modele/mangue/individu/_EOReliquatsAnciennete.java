/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReliquatsAnciennete.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOReliquatsAnciennete extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ReliquatsAnciennete";
	public static final String ENTITY_TABLE_NAME = "MANGUE.RELIQUATS_ANCIENNETE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "relOrdre";

	public static final String ANC_ANNEE_KEY = "ancAnnee";
	public static final String ANC_NB_ANNEES_KEY = "ancNbAnnees";
	public static final String ANC_NB_JOURS_KEY = "ancNbJours";
	public static final String ANC_NB_MOIS_KEY = "ancNbMois";
	public static final String D_ARRETE_KEY = "dArrete";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_SEQ_CARRIERE_KEY = "noSeqCarriere";
	public static final String NO_SEQ_ELEMENT_KEY = "noSeqElement";
	public static final String TEM_UTILISE_KEY = "temUtilise";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String REL_ORDRE_KEY = "relOrdre";

//Colonnes dans la base de donnees
	public static final String ANC_ANNEE_COLKEY = "REL_ANC_ANNEE";
	public static final String ANC_NB_ANNEES_COLKEY = "REL_ANC_NB_ANNEES";
	public static final String ANC_NB_JOURS_COLKEY = "REL_ANC_NB_JOURS";
	public static final String ANC_NB_MOIS_COLKEY = "REL_ANC_NB_MOIS";
	public static final String D_ARRETE_COLKEY = "D_ARRETE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NO_ARRETE_COLKEY = "NO_ARRETE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String NO_SEQ_CARRIERE_COLKEY = "NO_SEQ_CARRIERE";
	public static final String NO_SEQ_ELEMENT_COLKEY = "NO_SEQ_ELEMENT";
	public static final String TEM_UTILISE_COLKEY = "REL_UTILISE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String REL_ORDRE_COLKEY = "REL_ORDRE";


	// Relationships
	public static final String ELEMENT_CARRIERE_KEY = "elementCarriere";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer ancAnnee() {
    return (Integer) storedValueForKey(ANC_ANNEE_KEY);
  }

  public void setAncAnnee(Integer value) {
    takeStoredValueForKey(value, ANC_ANNEE_KEY);
  }

  public Integer ancNbAnnees() {
    return (Integer) storedValueForKey(ANC_NB_ANNEES_KEY);
  }

  public void setAncNbAnnees(Integer value) {
    takeStoredValueForKey(value, ANC_NB_ANNEES_KEY);
  }

  public Integer ancNbJours() {
    return (Integer) storedValueForKey(ANC_NB_JOURS_KEY);
  }

  public void setAncNbJours(Integer value) {
    takeStoredValueForKey(value, ANC_NB_JOURS_KEY);
  }

  public Integer ancNbMois() {
    return (Integer) storedValueForKey(ANC_NB_MOIS_KEY);
  }

  public void setAncNbMois(Integer value) {
    takeStoredValueForKey(value, ANC_NB_MOIS_KEY);
  }

  public NSTimestamp dArrete() {
    return (NSTimestamp) storedValueForKey(D_ARRETE_KEY);
  }

  public void setDArrete(NSTimestamp value) {
    takeStoredValueForKey(value, D_ARRETE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String noArrete() {
    return (String) storedValueForKey(NO_ARRETE_KEY);
  }

  public void setNoArrete(String value) {
    takeStoredValueForKey(value, NO_ARRETE_KEY);
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey(NO_DOSSIER_PERS_KEY);
  }

  public void setNoDossierPers(Integer value) {
    takeStoredValueForKey(value, NO_DOSSIER_PERS_KEY);
  }

  public Integer noSeqCarriere() {
    return (Integer) storedValueForKey(NO_SEQ_CARRIERE_KEY);
  }

  public void setNoSeqCarriere(Integer value) {
    takeStoredValueForKey(value, NO_SEQ_CARRIERE_KEY);
  }

  public Integer noSeqElement() {
    return (Integer) storedValueForKey(NO_SEQ_ELEMENT_KEY);
  }

  public void setNoSeqElement(Integer value) {
    takeStoredValueForKey(value, NO_SEQ_ELEMENT_KEY);
  }

  public String temUtilise() {
    return (String) storedValueForKey(TEM_UTILISE_KEY);
  }

  public void setTemUtilise(String value) {
    takeStoredValueForKey(value, TEM_UTILISE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOElementCarriere elementCarriere() {
    return (org.cocktail.mangue.modele.mangue.individu.EOElementCarriere)storedValueForKey(ELEMENT_CARRIERE_KEY);
  }

  public void setElementCarriereRelationship(org.cocktail.mangue.modele.mangue.individu.EOElementCarriere value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOElementCarriere oldValue = elementCarriere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ELEMENT_CARRIERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ELEMENT_CARRIERE_KEY);
    }
  }
  

/**
 * Créer une instance de EOReliquatsAnciennete avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOReliquatsAnciennete createEOReliquatsAnciennete(EOEditingContext editingContext, Integer ancAnnee
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noDossierPers
, Integer noSeqCarriere
, Integer noSeqElement
, String temValide
, org.cocktail.mangue.modele.mangue.individu.EOElementCarriere elementCarriere			) {
    EOReliquatsAnciennete eo = (EOReliquatsAnciennete) createAndInsertInstance(editingContext, _EOReliquatsAnciennete.ENTITY_NAME);    
		eo.setAncAnnee(ancAnnee);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoDossierPers(noDossierPers);
		eo.setNoSeqCarriere(noSeqCarriere);
		eo.setNoSeqElement(noSeqElement);
		eo.setTemValide(temValide);
    eo.setElementCarriereRelationship(elementCarriere);
    return eo;
  }

  
	  public EOReliquatsAnciennete localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReliquatsAnciennete)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReliquatsAnciennete creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReliquatsAnciennete creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOReliquatsAnciennete object = (EOReliquatsAnciennete)createAndInsertInstance(editingContext, _EOReliquatsAnciennete.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOReliquatsAnciennete localInstanceIn(EOEditingContext editingContext, EOReliquatsAnciennete eo) {
    EOReliquatsAnciennete localInstance = (eo == null) ? null : (EOReliquatsAnciennete)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOReliquatsAnciennete#localInstanceIn a la place.
   */
	public static EOReliquatsAnciennete localInstanceOf(EOEditingContext editingContext, EOReliquatsAnciennete eo) {
		return EOReliquatsAnciennete.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReliquatsAnciennete fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReliquatsAnciennete fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReliquatsAnciennete eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReliquatsAnciennete)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReliquatsAnciennete fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReliquatsAnciennete fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReliquatsAnciennete eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReliquatsAnciennete)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReliquatsAnciennete fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReliquatsAnciennete eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReliquatsAnciennete ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReliquatsAnciennete fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

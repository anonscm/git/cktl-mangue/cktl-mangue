//EOArrivee.java
//Created on Fri Mar 23 11:04:45 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** G&egrave;re les donees d'arrivee d'un individu dans l'etablissement<BR>
 * Regles de validation:<BR>
 * L'individu, le type d'acces et la date d'effet doivent etre fournis<BR>
 * La date d'effet ne peut etre posterieure a la date de premiere affectation<BR>
 * Les longueurs des strings sont verifiees<BR>
 * @author christine
 *
 */
public class EOArrivee extends _EOArrivee {
	
	public static final EOSortOrdering SORT_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_DEBUT_ASC = new NSArray(SORT_DEBUT_ASC);
			
	private static final int LONGUEUR_LIEU_ARRIVEE = 80;
	
	public EOArrivee() {
		super();
	}

    public static EOArrivee findForKey( EOEditingContext edc, Number key) {
		return fetchFirstByQualifier(edc, CocktailFinder.getQualifierEqual(NO_ARRIVEE_KEY, key));
	}
    
    /**
     * 
     * @param ec
     * @param individu
     * @return
     */
    public static EOArrivee creer(EOEditingContext edc, EOIndividu individu) {

		EOArrivee newObject = (EOArrivee) createAndInsertInstance(edc, ENTITY_NAME);
		newObject.setIndividuRelationship(individu);
		newObject.setTemValide("O");
		newObject.setTemTitulaire(CocktailConstantes.FAUX);
		newObject.setTemPrive(CocktailConstantes.FAUX);

		return newObject;
	}

	public void validateForSave() throws NSValidation.ValidationException {
		// On ne fait pas appel à super.validateForSave() car pas de date de fin
		if (individu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu");
		}
		if (noArrete() != null && noArrete().length() > 20) {
			throw new NSValidation.ValidationException("Un numéro d'arrêté comporte au plus 20 caractères");
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date d'arrivée");
		}
		if (typeAcces() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le type d'accès");
		}
		if (lieuArrivee() != null && lieuArrivee().length() > LONGUEUR_LIEU_ARRIVEE) {
			throw new NSValidation.ValidationException("Le lieu d'arrivée comporte au plus " + LONGUEUR_LIEU_ARRIVEE + " caractères");
		}
		if (d1Affectation() != null && DateCtrl.isAfter(dateDebut(),d1Affectation())) {
			throw new NSValidation.ValidationException("La date d'arrivée ne peut être postérieure à la date de première affectation");
		}

	}
	public boolean estTitulaire() {
		return temTitulaire() != null && temTitulaire().equals(CocktailConstantes.VRAI);
	}
	public void setEstTitulaire(boolean aBool) {
		setTemTitulaire((aBool)?CocktailConstantes.VRAI : CocktailConstantes.FAUX);
	}
	public boolean vientDuPrive() {
		return temPrive() != null && temPrive().equals(CocktailConstantes.VRAI);
	}
	public void setVientDuPrive(boolean aBool) {
		setTemPrive((aBool)?CocktailConstantes.VRAI : CocktailConstantes.FAUX);
	}
	public NSTimestamp dateFin() {
		return null;
	}
	public void setDateFin(NSTimestamp date) {}
	/** pas de type absence associe a cette duree
	 */
	public String typeEvenement() {
		return null;
	}

	/** pas plusieurs types d'absence */
	public boolean supportePlusieursTypesEvenement() {
		return false;
	}
	public String date1AffectationFormatee() {
		return SuperFinder.dateFormatee(this,D1_AFFECTATION_KEY);
	}
	public void setDate1AffectationFormatee(String uneDate) {
		if (uneDate == null || uneDate.equals("")) {
			setD1Affectation(null);
		} else {
			SuperFinder.setDateFormatee(this, D1_AFFECTATION_KEY,uneDate);
		}
	}

	/**
	 * 
	 * @param edc
	 * @param individu
	 * @return
	 */
	public static EOArrivee findForIndividu(EOEditingContext edc,EOIndividu individu) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(CocktailFinder.getQualifierEqual(INDIVIDU_KEY, individu));
			qualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));
			return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DEBUT_ASC);
		} catch (Exception e) {
			return null;
		}
	}
}

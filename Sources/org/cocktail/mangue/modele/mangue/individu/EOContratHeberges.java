//EOContratHeberges.java
//Created on Wed Sep 16 15:49:09 Europe/Paris 2009 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import java.util.Enumeration;

import org.cocktail.mangue.common.modele.nomenclatures.EOMotifDepart;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Verifie que la date de debut et l'individu sont fournis, que les dates de debut et de fin sont coherentes
 * et que le commentaire a une longueur correcte.  Verifie aussi que l'individu n'a pas de contrat ou carriere sur la periode
 * et que les contrats d'heberges ne se chevauchent pas */

public class EOContratHeberges extends _EOContratHeberges {

	public static final EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);

	public static final String HEBERGE_A_VALIDER = "N";

	public static String LL_ORIGINE_KEY = "libelleOrigine";

	public EOContratHeberges() {
		super();
	}

	public static EOContratHeberges creer(EOEditingContext ec, EOIndividu individu) {

		EOContratHeberges newObject = (EOContratHeberges) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setTemValide("O");
		return newObject;

	}

	public String libelleOrigine() {

		if (toUai() != null)
			return toUai().libelleLong();
		else
			if (toStructureOrigine() != null)
				return toStructureOrigine().llStructure();

		return detailOrigine();

	}
	
	// interface RecordAvecLibelle
	public String libelle() {
		return "HEBERGE du " + DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dateFin());
	}

	/**
	 * 
	 * @param ec
	 * @param ancienContrat
	 * @return
	 */
	public static EOContratHeberges renouveler(EOEditingContext ec, EOContratHeberges ancienContrat) {

		EOContratHeberges newObject = (EOContratHeberges) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.setIndividuRelationship(ancienContrat.individu());
		newObject.setToStructureHebergementRelationship(ancienContrat.toStructureHebergement());
		newObject.setToStructureOrigineRelationship(ancienContrat.toStructureOrigine());
		newObject.setAdresseStructureRelationship(ancienContrat.adresseStructure());
		newObject.setToCorpsRelationship(ancienContrat.toCorps());
		newObject.setToGradeRelationship(ancienContrat.toGrade());
		newObject.setToUaiRelationship(ancienContrat.toUai());
		newObject.setTypeContratTravailRelationship(ancienContrat.typeContratTravail());
		newObject.setCtrhHeure(ancienContrat.ctrhHeure());
		newObject.setCtrhInm(ancienContrat.ctrhInm());
		newObject.setEstValide(true);
		
		newObject.setDateDebut(DateCtrl.jourSuivant(ancienContrat.dateFin()));
		newObject.setDateFin(null);

		return newObject;

	}

	public void awakeFromInsertion(EOEditingContext editingContext) {
		setTemValide(CocktailConstantes.VRAI);
	}
	public boolean estValide() {
		return temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}
	public boolean estAvalider() {
		return temValide() != null && temValide().equals(HEBERGE_A_VALIDER);
	}


	/**
	 * 
	 * @param editingContext
	 * @param debut
	 * @param fin
	 * @return
	 */
	public static NSArray<EOContratHeberges> findForPeriode(EOEditingContext editingContext, NSTimestamp debut, NSTimestamp fin) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
			if (debut != null)
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debut, DATE_FIN_KEY, fin));

			EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), SORT_ARRAY_NOM_ASC);
			fetchSpec.setIsDeep(true);
			fetchSpec.setPrefetchingRelationshipKeyPaths(new NSArray(INDIVIDU_KEY));
			NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
			return eoObjects;

		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}

	}


	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		// La date de fin ne doit pas etre inferieure a la date de debut
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début doit etre définie !");
		} else if (dateFin() != null && dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException("HEBERGE\nLa date de fin doit être postérieure à la date de début (FIN : " + dateFinFormatee() + " , DEBUT : " + dateDebutFormatee() + ")");
		}
		if (individu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu");
		}
		if (commentaire() != null && commentaire().length() > 2000) {
			throw new NSValidation.ValidationException("Le commentaire ne peut dépasser 2000 caractères");
		}
		if (ctrhHeure() != null) {
			if (ctrhHeure().intValue() < 0)
				throw new NSValidation.ValidationException("Le nombre d'heures du contrat ne peut être nul");
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		//individu().setDModification(new NSTimestamp());		
	}

	/**
	 * 
	 */
	public void invalider() {
		setEstValide(false);
	}
	/**
	 * 
	 */
	protected void init() {
		setTemValide(CocktailConstantes.VRAI);		
		setDateDebut(new NSTimestamp());
	}


	/** Retourne les contrats heberges valides ou a valider d'un individu classes par ordre de date decroissante */
	public static NSArray<EOContratHeberges> rechercherContratsEnCoursPourIndividu(EOEditingContext ec,EOIndividu individu) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);

	}


	/**
	 * 
	 */
	public static EOContratHeberges findForIndividuAndDate(EOEditingContext edc, EOIndividu individu, NSTimestamp dateReference) {

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));
			if (dateReference != null) {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateReference,DATE_FIN_KEY,dateReference));
			}		

			return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));

		}
		catch (Exception e) {
			return null;
		}

	}

	/** Retourne les contrats heberges d'un individu pour la periode passee en parametre */
	public static NSArray<EOContratHeberges> findForIndividuAndPeriode(EOEditingContext ec,EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		try {
			
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));
			if (dateDebut != null) {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateDebut,DATE_FIN_KEY,dateFin));
			}		
			return fetchAll(ec,new EOAndQualifier(qualifiers),SORT_ARRAY_DATE_DEBUT_DESC );
		}
		catch (Exception e) {
			return new NSArray();
		}
	}	

	public static boolean aContratHeberge(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return findForIndividuAndPeriode(editingContext, individu,debutPeriode,finPeriode).count() > 0;
	}


	/** Retourne le contrats heberge lie a une affectation */
	public static EOContratHeberges rechercherContratPourAffectation(EOEditingContext editingContext,EOAffectation affectation) {
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,EOQualifier.qualifierWithQualifierFormat("affectation = %@ AND temValide = 'O'", new NSArray(affectation)),new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareDescending)));
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (EOContratHeberges)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Retourne les contrats heberges d'un individu a valider classes par ordre de date decroissante */
	public static NSArray<EOContratHeberges> rechercherContratsAValider(EOEditingContext ec) {		
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = 'N'", null);
		return fetchAll(ec, qualifier, SORT_ARRAY_DATE_DEBUT_DESC );		
	}

	/** Recherche les contrats heberge valides a la date et pour le type de personnel passe en parametre 
	 * apres dateFin
	 * @param editingContext
	 * @param date peut etre nulle
	 * @param typeEnseignant
	 * @return contrats heberges trouves
	 */
	public static NSArray<EOContratHeberges> rechercherContratsHebergesPourDateEtTypePersonnel(EOEditingContext editingContext,NSTimestamp date,int typeEnseignant) {

		NSMutableArray qualifiers = new NSMutableArray();
		if (date != null) {
			NSMutableArray args = new NSMutableArray();
			args.addObject(date);
			args.addObject(date);
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@ AND (dateFin >= %@ OR dateFin = nil) AND temValide = 'O' AND individu.temValide = 'O'", args));
		}

		qualifiers.addObject(qualifierPourTypeEnseignant(typeEnseignant));

		EOQualifier qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier,new NSArray(EOSortOrdering.sortOrderingWithKey("noDossierPers", EOSortOrdering.CompareAscending)));
		fs.setRefreshesRefetchedObjects(true);
		NSMutableArray prefetches = new NSMutableArray(INDIVIDU_KEY);
		prefetches.addObject(TO_GRADE_KEY);
		fs.setPrefetchingRelationshipKeyPaths(prefetches);
		fs.setUsesDistinct(true);

		return editingContext.objectsWithFetchSpecification(fs);
	}

	/** recherche les individus avec des contrats heberge valides commencant avant dateDebut et terminant 
	 * apres dateFin
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateDebut peut etre nulle
	 * @param dateFin	 peut etre nulle
	 * @param supprimerDoublons true si supprimer les doublons
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @return individus trouves (type IndividuIdentite)
	 */
	public static NSArray rechercherIndividusAvecContratsHebergesPourPeriode(EOEditingContext editingContext,String nom,String prenom,NSTimestamp dateDebut,NSTimestamp dateFin,boolean supprimerDoublons,boolean estNomPatronymique) {
		EOQualifier qualifierDate = null;
		if (dateDebut != null && dateFin != null) { 
			NSMutableArray args = new NSMutableArray();
			String stringQualifier = "";
			args.addObject(dateDebut);
			args.addObject(dateFin);
			stringQualifier = "contratsHeberges.dateDebut <= %@ AND (contratsHeberges.dateFin >= %@ OR contratsHeberges.dateFin = nil)";
			qualifierDate = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		} 
		return rechercherIndividusAvecQualifierDate(editingContext, qualifierDate, nom, prenom,supprimerDoublons, estNomPatronymique);
	}

	/** recherche les individus ayant des contrats heberges valides se terminant avant la date fournie
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateReference peut &ecirc;tre nulle (date du jour)
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants, vacataires
	 * @param estNomPatronymique true si la recherche doit etre faite sur le nom patronymique
	 * @return individus trouves (type IndividuIdentite)
	 */
	public static NSArray rechercherIndividusPourContratsHebergesAnterieureDate(EOEditingContext editingContext,String nom,String prenom,NSTimestamp dateReference,boolean estNomPatronymique) {
		// pour ramener à une date sans prendre en compte les heures
		if (dateReference == null)
			dateReference =  DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		EOQualifier qualifierDate = EOQualifier.qualifierWithQualifierFormat("contratsHeberges.dateFin < %@", new NSArray(dateReference));
		return rechercherIndividusAvecQualifierDate(editingContext, qualifierDate, nom, prenom, true, estNomPatronymique);
	} 
	/** recherche les individus avec des contrats heberge valides commencant apres la date de reference
	 * @param editingContext
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param dateReference date apres laquelle le contrat commence (si nulle date du jour)
	 * @param typePersonnel type de personnel recherche : tous, enseignants, non-enseignants, vacataires
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @return individus trouves (type IndividuIdentite)
	 */
	public static NSArray rechercherIndividusAvecContratsHebergesFuturs(EOEditingContext editingContext,String nom,String prenom,NSTimestamp dateReference,boolean estNomPatronymique) {
		if (dateReference == null) {
			// pour ramener à une date sans prendre en compte les heures
			dateReference =  DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		}
		EOQualifier qualifierDate = EOQualifier.qualifierWithQualifierFormat("contratsHeberges.dateDebut > %@", new NSArray(dateReference));
		return rechercherIndividusAvecQualifierDate(editingContext, qualifierDate, nom, prenom, true, estNomPatronymique);
	}
	/** Ferme les contrats heberges valides a la date et supprime les contrats futurs */
	public static void fermerContrats(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date, EOMotifDepart motif) {
		NSArray objets = findForIndividuAndPeriode(editingContext,individu,date,date);

		for (java.util.Enumeration<EOContratHeberges> e = objets.objectEnumerator();e.hasMoreElements();) {
			EOContratHeberges contrat = e.nextElement();
			contrat.setDateFin(date);
			// Pour les normaliens, il n'y a pas de contrat donc ne pas prendre en compte ce type de motif si par hasard il y avait un contrat
			if (motif != null) {
				String commentaire = "Fermeture pour motif de départ : " + motif.libelleCourt();
				if (contrat.commentaire() == null) {
					contrat.setCommentaire(commentaire);
				} else if (contrat.commentaire().length() < (2000 - commentaire.length())) {
					contrat.setCommentaire(contrat.commentaire() + "\n" + commentaire);
				}
			}

		}
		// invalider les informations posterieures à cette date
		// annuler le contrat
		for (Enumeration<EOContratHeberges> e1 = rechercherDureesPourEntitePosterieuresADate(editingContext, ENTITY_NAME, individu, date).objectEnumerator();e1.hasMoreElements();) {
			EOContratHeberges contrat = e1.nextElement();
			if (contrat.estValide())
				contrat.invalider();
		}
	}

	private static EOQualifier qualifierPourNom(String relation,String nom,String prenom,boolean estNomPatronymique) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (relation.length() > 0)
			relation = relation + ".";

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "temValide = 'O'", null));
		if (nom != null) {
			if (estNomPatronymique) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "nomUsuel like '*'",null));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "nomPatronymique caseInsensitiveLike %@", new NSArray(nom + "*")));
			} else
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "nomUsuel caseInsensitiveLike %@",new NSArray(nom + "*")));
		} else
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "nomUsuel like '*'",null));
		if (prenom != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(relation + "prenom caseInsensitiveLike %@",new NSArray(prenom + "*")));
		}
		return new EOAndQualifier(qualifiers);
	}

	private static NSArray rechercherIndividusAvecQualifierDate(EOEditingContext editingContext,EOQualifier qualifierDate,String nom,String prenom,boolean supprimerDoublons,boolean estNomPatronymique) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (qualifierDate != null)
			qualifiers.addObject(qualifierDate);

		// tous les contrats heberges valides
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOIndividuIdentite.CONTRATS_HEBERGES_KEY + "." + EOContratHeberges.TEM_VALIDE_KEY + " = 'O'",null));
		// avec les noms et prenoms recherches
		qualifiers.addObject(qualifierPourNom("",nom, prenom, estNomPatronymique));

		EOQualifier qualifier = new EOAndQualifier(qualifiers);
		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(EOIndividuIdentite.NO_INDIVIDU_KEY, EOSortOrdering.CompareAscending));
		EOFetchSpecification myFetch = new EOFetchSpecification(EOIndividuIdentite.ENTITY_NAME, qualifier,sorts);
		myFetch.setUsesDistinct(supprimerDoublons);
		myFetch.setRefreshesRefetchedObjects(true);
		return new NSMutableArray(editingContext.objectsWithFetchSpecification(myFetch));
	}
	
	/**
	 * 
	 * @param typeEnseignant
	 * @return
	 */
	private static EOQualifier qualifierPourTypeEnseignant(int typeEnseignant) {
		NSMutableArray qualifiers = new NSMutableArray();
		//05/02/2010 - Modification du critère de recherche des contrats non-vacataires, on se base sur le témoin temRemunerationPrincipale
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_CONTRAT_TRAVAIL_KEY + "." + EOTypeContratTravail.TEM_POUR_TITULAIRE_KEY + " != 'O'", null));
		if (typeEnseignant == ManGUEConstantes.ENSEIGNANT) {
			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_CONTRAT_TRAVAIL_KEY + "." + EOTypeContratTravail.TEM_ENSEIGNANT_KEY + " = 'O'",null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_GRADE_KEY + " != NIL  AND toGrade.cGrade != '" + EOGrade.CODE_SANS_GRADE + "' AND toGrade.toCorps.toTypePopulation.temEnseignant = 'O'",null));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		} else if (typeEnseignant == ManGUEConstantes.NON_ENSEIGNANT) {
			NSMutableArray orQualifiers = new NSMutableArray();
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_CONTRAT_TRAVAIL_KEY + "." + EOTypeContratTravail.TEM_ENSEIGNANT_KEY + " = 'N'",null));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_GRADE_KEY + " != NIL  AND " + TO_GRADE_KEY + ".cGrade != '" + EOGrade.CODE_SANS_GRADE + "' AND toGrade.toCorps.toTypePopulation.temEnseignant = 'N'",null));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		EOQualifier qualifier = new EOAndQualifier(qualifiers);
		return qualifier;
	}
}

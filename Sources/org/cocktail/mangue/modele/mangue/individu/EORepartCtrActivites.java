// EORepartCtrActivites.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
/** R&grave;gles de validation :<BR>
 * L'avenant et l'activit&eacute; sont obligatoires<BR>
 * L'activit&eacute; et l'avenant correspondent au m&ecirc;me type de contrat de travail<BR>
 * L'activit&eacute; ne doit pas &ecirc;tre d&eacute;j&agrave; associ&eacute;e &agrave; cet avenant
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.modele.grhum.EOActivitesTypeContrat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EORepartCtrActivites extends _EORepartCtrActivites {

	public EORepartCtrActivites() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param avenant
	 * @param activite
	 * @return
	 */
	public static EORepartCtrActivites creer(EOEditingContext ec, EOContratAvenant avenant, EOActivitesTypeContrat activite) {

		EORepartCtrActivites newObject = (EORepartCtrActivites) createAndInsertInstance(ec, EORepartCtrActivites.ENTITY_NAME);    

		newObject.setAvenantRelationship(avenant);
		newObject.setActiviteTypeContratRelationship(activite);

		return newObject;


	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		// Vérifier que les relations obligatoires sont bien fournies
		if (avenant() == null) {
			throw new NSValidation.ValidationException("L'avenant doit être fourni");
		}
		if (activiteTypeContrat() == null) {
			throw new NSValidation.ValidationException("L'activité doit être fournie !");
		}
		// Vérifier que l'activité correspond au type de contrat de l'avenant
		if (avenant().contrat().toTypeContratTravail() != activiteTypeContrat().toTypeContratTravail()) {
			throw new NSValidation.ValidationException("L'activité n'est pas valide pour cet avenant, elle correspond à un contrat de travail différent");
		}
		// Vérifier qu'elle n'est pas déjà affectée à l'avenant
		NSArray<EORepartCtrActivites> reparts = rechercherRepartsActivitesPourAvenantEtActivite(avenant().editingContext(),avenant(), activiteTypeContrat());
		if (reparts.count() > 0 && reparts.objectAtIndex(0) != this) {
			throw new NSValidation.ValidationException("L'activité est déjà associée à cet avenant");
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	/** Retourne les reparts activites liees a un avenant et/ou a une activite
	 * @param avenant (peut etre nul)
	 * @param activite recherchee (peut etre nulle)
	 */
	public static NSArray<EORepartCtrActivites> rechercherRepartsActivitesPourAvenantEtActivite(EOEditingContext edc, EOContratAvenant avenant,EOActivitesTypeContrat activite) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (avenant != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(AVENANT_KEY + "=%@", new NSArray(avenant)));
		}
		if (activite != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ACTIVITE_TYPE_CONTRAT_KEY + "=%@", new NSArray(activite)));
		}

		return fetchAll(edc, new EOAndQualifier(qualifiers));
	}
	/** Retourne les reparts activites liees a un avenant (retourne nul si l'avenant est nul)
	 * 
	 */
	public static NSArray<EORepartCtrActivites> rechercherRepartsActivitesPourAvenant(EOContratAvenant avenant, boolean shouldRefresh) {
		return rechercherRepartsActivitesPourAvenantEtActivite(avenant.editingContext(),avenant,null);
	}

}

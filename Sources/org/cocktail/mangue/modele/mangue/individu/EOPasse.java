// EOPasse.java
// Created on Wed Nov 30 16:41:45 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import java.math.BigDecimal;

import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeService;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl.IntRef;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOPasse extends _EOPasse {

	public static final String TYPE_TEMPS_PARTIEL = "P";
	public static final String TYPE_TEMPS_INCOMPLET = "I";
	public static final String TYPE_TEMPS_COMPLET = "C";

	public EOPasse() {
		super();
	}
	
	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}


	public boolean estTitulaire() {
		return temTitulaire() != null && temTitulaire().equals(CocktailConstantes.VRAI);
	}
	public void setEstTitulaire(boolean aBool) {
		if (aBool) {
			setTemTitulaire(CocktailConstantes.VRAI);
		} else {
			setTemTitulaire(CocktailConstantes.FAUX);
		}
	}

	public boolean estPcAquitees() {
		return pasPcAcquitee() != null && pasPcAcquitee().equals(CocktailConstantes.VRAI);
	}
	public void setEstPcAquitees(boolean aBool) {
		if (aBool)
			setPasPcAcquitee(CocktailConstantes.VRAI);
		else
			setPasPcAcquitee(CocktailConstantes.FAUX);
	}


	public boolean estSecteurPublic() {
		return secteurPublic().equals(CocktailConstantes.VRAI);
	}
	public void setEstSecteurPublic(boolean aBool) {
		if (aBool)
			setSecteurPublic(CocktailConstantes.VRAI);
		else
			setSecteurPublic(CocktailConstantes.FAUX);
	}


	public boolean estTempsComplet() {
		return pasTypeTemps().equals(TYPE_TEMPS_COMPLET);
	}
	public boolean estTempsIncomplet() {
		return pasTypeTemps().equals(TYPE_TEMPS_INCOMPLET);
	}
	public boolean estTempsPartiel() {
		return pasTypeTemps().equals(TYPE_TEMPS_PARTIEL);
	}

	public boolean estPaiementCompletCotisations() {
		return pasPcAcquitee().equals(CocktailConstantes.VRAI);
	}
	public void setEstPaiementCompletCotisations(boolean aBool) {
		if (aBool)
			setPasPcAcquitee(CocktailConstantes.VRAI);
		else
			setPasPcAcquitee(CocktailConstantes.FAUX);
	}

	public String dateValidationServiceFormatee() {
		return SuperFinder.dateFormatee(this, D_VALIDATION_SERVICE_KEY);
	}
	public void setDateValidationServiceFormatee(String uneDate) {
		if (uneDate == null)
			setDValidationService(null);
		else
			SuperFinder.setDateFormatee(this, D_VALIDATION_SERVICE_KEY, uneDate);
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOPasse creer(EOEditingContext ec, EOIndividu individu) {

		EOPasse newObject = new EOPasse();
		ec.insertObject(newObject);

		newObject.setIndividuRelationship(individu);
		newObject.setToTypeFonctionPubliqueRelationship((EOTypeFonctionPublique)NomenclatureFinder.findForCode(ec, EOTypeFonctionPublique.ENTITY_NAME, EOTypeFonctionPublique.TYPE_FCT_PUBLIQUE_ETAT));

		newObject.setPasTypeTemps(TYPE_TEMPS_COMPLET);
		newObject.setPasPcAcquitee(CocktailConstantes.VRAI);
		newObject.setSecteurPublic(CocktailConstantes.VRAI);
		newObject.setTemTitulaire(CocktailConstantes.VRAI);
		newObject.setEstValide(true);

		return newObject;

	}

	/**
	 * 
	 * @param ec
	 * @param avenant
	 * @return
	 */
	public static EOPasse creerFromContratAvenant(EOEditingContext edc, EOContratAvenant avenant) {

		EOPasse newObject = (EOPasse) createAndInsertInstance(edc, ENTITY_NAME);    

		newObject.setIndividuRelationship(avenant.contrat().toIndividu());
		newObject.setToTypeServiceRelationship(EOTypeService.defaultTypeService(edc));
		newObject.setToTypeFonctionPubliqueRelationship(EOTypeFonctionPublique.defaultTypeFonctionPublique(edc));

		newObject.setDateDebut(avenant.dateDebut());
		newObject.setDateFin(avenant.dateFin());

		if (avenant.contrat().toRne() != null && avenant.contrat().toRne().libelleLong() != null) {
			newObject.setEtablissementPasse(avenant.contrat().toRne().libelleLong());
		}

		newObject.setPasMinistere("MESR");
		newObject.setSecteurPublic("O");
		newObject.setTemTitulaire("O");
		newObject.setTemValide("O");

		newObject.setDureeValideeAnnees(avenant.ctraDureeValideeAnnees());
		newObject.setDureeValideeMois(avenant.ctraDureeValideeMois());
		newObject.setDureeValideeJours(avenant.ctraDureeValideeJours());
		newObject.setPasPcAcquitee(avenant.ctraPcAcquitees());
		newObject.setPasTypeTemps(avenant.ctraTypeTemps());
		newObject.setPasQuotiteCotisation(new BigDecimal(avenant.ctraQuotiteCotisation()));

		newObject.setDValidationService(avenant.dValContratAv());

		newObject.setContratAvenantRelationship(avenant);
		return newObject;
	}

	/** la sous-classe doit absolument invoquer cette methode */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (temValide().equals("O")) {

			if (individu() == null) {
				throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_INDIVIDU_NON_RENSEIGNE, "PASSE"));
			}
			if (dateDebut() == null) {
				throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "PASSE"));
			}
			if (dateFin() == null) {
				throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_NON_RENSEIGNE, "PASSE"));
			}
			if (dateDebut().after(dateFin())) {
				throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "PASSE", dateFinFormatee(), dateDebutFormatee()));
			}

			if (etablissementPasse() == null  && pasMinistere() == null && toMinistere() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir soit un ministère soit un établissement !");
			}
			if (etablissementPasse() != null && etablissementPasse().length() > 65) {
				throw new NSValidation.ValidationException("Le nom de l'établissement comporte au maximum 65 caractères");
			}
			if (toTypeFonctionPublique() == null && estSecteurPublic())
				throw new NSValidation.ValidationException("Le type de fonction publique est obligatoire !");

			if (toTypeService() == null)
				throw new NSValidation.ValidationException("Le type de service est obligatoire !");

			if (fonctionPasse() != null && fonctionPasse().length() > 200)
				throw new NSValidation.ValidationException("La fonction comporte au maximum 200 caractères");

			//		if (estSecteurPublic() && categorie() == null)
			//			throw new NSValidation.ValidationException("Pour le secteur public, vous devez fournir la catégorie");			

			// Quotite de cotisation > 50 et < 100
			if (estTempsPartiel()) {
				if (pasQuotiteCotisation() != null && (pasQuotiteCotisation().floatValue() < 50 || pasQuotiteCotisation().floatValue() > 99 ) ) {
					throw new NSValidation.ValidationException("Pour un temps partiel la quotité de cotisation doit être comprise entre 50 et 99% !");				
				}
			}

			String message = validationsCir();
			if (message != null && message.length() > 0)
				throw new NSValidation.ValidationException(message);
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}	

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOPasse> findForPeriode(EOEditingContext ec, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		try {

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(getQualifierValide());

			if (individu != null) {
				qualifiers.addObject(getQualifierIndividu(individu));
			}

			if (dateDebut != null && dateFin != null) {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,dateDebut, DATE_FIN_KEY, dateFin));
			}

			return fetchAll(ec, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/** Les validations Cir ne s'appliquent qu'aux non-titulaires du service public : <BR>
	 * La date de validation des services doit etre fournie<BR>
	 * La quotite de service doit etre fournie<BR>
	 * Les durees validees ne doivent etre fournies que pour une quotite de service et de cotisation de 100%<BR>
	 * La quotite de service et celle de cotisation doivent valoir 0 ou etre comprise entre 50% et 100%<BR>
	 * Pas de chevauchement des periodes pour les temps plein et pour les temps partiels le cumul de quotite
	 * doit etre inferieur ou egal a 100% et pour les temps incomplets, cumul des durees 
	 * inferieur a la duree de la periode<BR>
	 * On verifie aussi qu'il n'y a pas de chevauchements des passes avec les carrieres/contrats
	 * 
	 * @return
	 */
	public String validationsCir() {

		if (pasMinistere() != null && pasMinistere().length() > 25)
			return "PASSE - Le libellé du ministère ne doit pas dépasser 25 caractères !";

		// Validation des services valides
		if ((dureeValideeAnnees() != null && dureeValideeAnnees().intValue() > 0) ||
				(dureeValideeMois() != null && dureeValideeMois().intValue() > 0) ||
				(dureeValideeJours() != null && dureeValideeJours().intValue() > 0)) {

			if (toTypeService() != null && toTypeService().estTypeServiceNonValide()) {
				return "PASSE - Pour un service non validé pour la retraite, vous ne devez pas saisir de durée !";
			}

			if (pasQuotiteCotisation() == null || pasQuotiteCotisation().floatValue() == 0) 
				return "PASSE - La quotité de cotisation ne peut être égale à 0 !";

			if (dureeValideeMois() != null && (dureeValideeMois().intValue() < 0 || dureeValideeMois().intValue() > 11)) {
				return "PASSE - Les mois validés doivent être compris entre 0 et 11 !";
			}
			if (dureeValideeJours() != null && (dureeValideeJours().intValue() < 0 || dureeValideeJours().intValue() > 29) ) {
				return "PASSE - Les jours validés doivent être compris entre 0 et 29 !";
			}
			if (dValidationService() == null)
				return "PASSE - Vous devez fournir une date de validation des services (SV du " + DateCtrl.dateToString(dateDebut()) + " au " + DateCtrl.dateToString(dateFin()) + ") !";

			int annees = 0,mois = 0, jours = 0;
			if (dureeValideeAnnees() != null && dureeValideeAnnees().intValue() > 0) {
				annees = dureeValideeAnnees().intValue();
			}
			if (dureeValideeMois() != null && dureeValideeMois().intValue() > 0) {
				mois = dureeValideeMois().intValue();
			}
			if (dureeValideeJours() != null && dureeValideeJours().intValue() > 0) {
				jours = dureeValideeJours().intValue();
			}

			int nbJoursValides = (annees * 360) + (mois * 30) + jours;

			int nbJoursCalcules = DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true, false);

			if (DateCtrl.dateToString(dateDebut(), "%d/%m").equals("01/02") && 
					DateCtrl.dateToString(dateFin(), "%d/%m").equals("28/02"))
				nbJoursCalcules += 2;

			if (DateCtrl.dateToString(dateDebut(), "%d/%m").equals("01/02") && 
					DateCtrl.dateToString(dateFin(), "%d/%m").equals("29/02")) {
				nbJoursCalcules ++;
			}

			// Test de l'année bisextile
			int annee = DateCtrl.getYear(dateDebut());
			boolean anneeBisextile = (annee % 4) == 0;
			if (anneeBisextile) {
				if (DateCtrl.isBefore(dateDebut(), DateCtrl.stringToDate("29/02/"+annee))
						&& DateCtrl.isAfter(dateFin(), DateCtrl.stringToDate("29/02/"+annee)) ) {
					nbJoursCalcules ++;
				}
			}
			else {
				if (DateCtrl.isBefore(dateDebut(), DateCtrl.stringToDate("28/02/"+annee))
						&& DateCtrl.isAfterEq(dateFin(), DateCtrl.stringToDate("28/02/"+annee)) ) {
					nbJoursCalcules +=2;
				}
			}

			if (nbJoursValides > nbJoursCalcules) {
				return "PASSE - Le nombre de jours validés ("+nbJoursValides + " Jours) est supérieur au nombre d'années, mois, jours écoulés entre la date début et la date fin ("+nbJoursCalcules+" Jours) !";
			}	
		}
		else {
			if (toTypeService() != null && toTypeService().estTypeServiceValide()) {
				return "PASSE - Pour un service validé pour la retraite une durée validée est obligatoire !";
			}
		}

		// Vérifier qu'il n'y a pas de chevauchements avec des positions d'activite

		NSArray<EOChangementPosition> positions = EOChangementPosition.rechercherChangementsActivitePourPeriode(editingContext(), individu(), dateDebut(), dateFin());
		if (positions.size() > 0) {
			return "Les périodes de PASSE ne peuvent pas chevaucher des positions d'ACTIVITE !";
		}

		if (toTypeService() != null && toTypeService().estTypeServiceValide()) {

			if (dValidationService() == null) {
				//				return "Pour les services validés, vous devez fournir la date de validation";
			} else if (DateCtrl.isBefore(dValidationService(), dateFin())) {
				return "PASSE - La date de validation ne peut être antérieure à la date de fin de service";
			}
			if (pasQuotiteCotisation() == null && estTempsPartiel()) {
				return "PASSE - La quotité de service doit être fournie pour un service à temps partiel";
			}
			if (pasPcAcquitee() == null) {
				return "PASSE - Le témoin 'PC Acquitées' doit être fourni";
			}
			double quotite = pasQuotiteCotisation().doubleValue();
			if ((quotite != 0 && (quotite < 50.00 || quotite  > 100.00)) || quotite < 0) {
				return "La quotité de service doit être égale à zéro ou comprise entre 50% et 99%";
			}

			if (pasQuotiteCotisation() != null) {
				quotite = pasQuotiteCotisation().doubleValue();
				if ((quotite != 0 && (quotite < 50.00 || quotite  > 100.00)) || quotite < 0) {
					return "la quotité  de cotisation doit être égale à zéro ou comprise entre 50% et 100%";
				}
			}

		} 		

		return null;
	}
	// méthodes protégées
	protected void init() {
		setTemTitulaire(CocktailConstantes.FAUX);
		setSecteurPublic(CocktailConstantes.FAUX);
	}


	//	/**
	//	 * 
	//	 * La quotite cumulee de plusieurs périodes de passe qui se chevauchent ne peut depasser 100%
	//	 * 
	//	 * @return
	//	 */
	//	private String verifierChevauchements() {
	//		// Vérification des chevauchements
	//
	//		NSMutableArray<PasseAvecDureesCalculees> passesAvecDurees = new NSMutableArray<PasseAvecDureesCalculees>();
	//
	//		NSArray<EOPasse> passes = EOPasse.rechercherPassesPourIndividuAvecServicesValidesEtPeriode(editingContext(), individu(), dateDebut(), dateFin());
	//		for (EOPasse passe : passes) {
	//			if (passe != this) {
	//				passesAvecDurees.addObject(new PasseAvecDureesCalculees(passe, estTempsIncomplet()));
	//			}
	//		}
	//
	//		// Pour les temps pleins et partiels, on vérifie la quotité
	//		double quotiteTotale = pasQuotiteCotisation().doubleValue();
	//		NSMutableArray<PasseAvecDureesCalculees> periodesAConsiderer = new NSMutableArray<PasseAvecDureesCalculees>();
	//
	//		for (PasseAvecDureesCalculees passe : passesAvecDurees) {
	//
	//			if (passe.quotite() == null) {
	//				return "La quotité de service doit être fournie pour le passé débutant le " + DateCtrl.dateToString(passe.dateDebut());
	//			}
	//			if (estTempsIncomplet() == false) {
	//				quotiteTotale = quotiteTotale + passe.quotite().doubleValue();
	//			}
	//			
	//			// Vérifier si le passé courant est postérieur aux passés déjà pris en compte
	//			// en quel cas il ne faut plus prendre en compte ces derniers en compte et retirer leur quotité et nombre de jours, mois, années
	//			if (periodesAConsiderer.count() > 0) {
	//				for (PasseAvecDureesCalculees passeASupprimer : periodesAConsiderer) {
	//
	//					if (DateCtrl.isBefore(passeASupprimer.dateFin(), passe.dateDebut())) {
	//						
	//						// Le passé n'a plus court sur le passé en cours d'analyse
	//						// Enlever sa quotité et le supprimer des passés pris en compte, les passés étant classés
	//						// par date de début croissant, on trouvera ensuite toujours des passés postérieurs
	//
	//						if (estTempsIncomplet() == false) {
	//							quotiteTotale = quotiteTotale - passeASupprimer.quotite().doubleValue();
	//						}
	//					}
	//					periodesAConsiderer.removeObject(passeASupprimer);
	//				}
	//			}
	//			periodesAConsiderer.addObject(passe);
	//		}
	//
	//		if (quotiteTotale > 100.00) {
	//			return "La quotité de service cumulée des passés qui se chevauchent dépasse 100%";
	//		}
	//		return null;
	//	}


	/**
	 * 
	 * @param ec
	 * @param avenant
	 * @return
	 */
	public static EOPasse rechercherPassePourAvenant(EOEditingContext ec, EOContratAvenant avenant) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONTRAT_AVENANT_KEY + " = %@ ", new NSArray(avenant)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@ ", new NSArray(CocktailConstantes.VRAI)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));

	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOPasse> rechercherPassesPourIndividuAvecServicesValidesAnterieursADate(EOEditingContext ec,EOIndividu individu,NSTimestamp dateReference) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@ ", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@ ", new NSArray(dateReference)));

		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DUREE_VALIDEE_ANNEES_KEY + " >= %@ ", new NSArray(new Integer(0))));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DUREE_VALIDEE_MOIS_KEY + " >= %@ ", new NSArray(new Integer(0))));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DUREE_VALIDEE_JOURS_KEY + " >= %@ ", new NSArray(new Integer(0))));

		qualifiers.addObject(new EOOrQualifier(orQualifiers));

		return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);	

	}

	/**
	 * Recupere les periodes de PASSE pour un individu comptant dans la remontee CIR (EAS, ENGAGE ou ELEVE)
	 * @param ec
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOPasse> rechercherPassesPourRemonteeCIR(EOEditingContext ec,EOIndividu individu,NSTimestamp dateReference) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@ ", new NSArray(CocktailConstantes.VRAI)));

		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_SERVICE_KEY + " = %@ ", new NSArray(EOTypeService.findForCode(ec, EOTypeService.TYPE_SERVICE_EAS))));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_SERVICE_KEY + " = %@ ", new NSArray(EOTypeService.findForCode(ec, EOTypeService.TYPE_SERVICE_ENGAGE))));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_SERVICE_KEY + " = %@ ", new NSArray(EOTypeService.findForCode(ec, EOTypeService.TYPE_SERVICE_ELEVE))));

		qualifiers.addObject(new EOOrQualifier(orQualifiers));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@ ", new NSArray(dateReference)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);	

	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOPasse> rechercherPassesEASPourPeriode(EOEditingContext ec,EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_SERVICE_KEY + " = %@ ", new NSArray(EOTypeService.findForCode(ec, EOTypeService.TYPE_SERVICE_EAS))));
		if (dateDebut != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin));
		}

		return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);	

	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOPasse> rechercherPassesAnterieursADate(EOEditingContext ec,EOIndividu individu,NSTimestamp dateReference) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@ ", new NSArray(dateReference)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);	

	}


	/**
	 * Renvoie toutes les periodes de passe d'un agent, non associees a un contrat et du secteur public
	 * 
	 * @param ec
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOPasse> rechercherPassesPourSyntheseCarriere(EOEditingContext ec,EOIndividu individu,NSTimestamp dateReference) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONTRAT_AVENANT_KEY + " = nil ", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SECTEUR_PUBLIC_KEY + " = %@ ", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@ ", new NSArray(dateReference)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
	}


	/** Retourne la liste des passes hors EN non-titulaires concernant la fonction publique pour la periode
	 *  tries par ordre de date croissante 
	 * @param debutPeriode peut etre nul
	 */
	public static NSArray<EOPasse> rechercherPassesPourIndividuAvecServicesValidesEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(getQualifierValide());

		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_VALIDATION_SERVICE_KEY + " != nil", null));		
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DUREE_VALIDEE_ANNEES_KEY + " > 0", null));		
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DUREE_VALIDEE_MOIS_KEY + " > 0", null));		
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DUREE_VALIDEE_JOURS_KEY + " > 0", null));		

		qualifiers.addObject(new EOOrQualifier(orQualifiers));

		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
		}

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
	}


	/** Retourne la liste des passes hors EN non-titulaires concernant la fonction publique pour la periode
	 *  tries par ordre de date croissante 
	 * @param debutPeriode peut etre nul
	 */
	public static NSArray<EOPasse> findForIndividuAndPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(getQualifierValide());
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOPasse> findForIndividu(EOEditingContext ec, EOIndividu individu) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(getQualifierIndividu(individu));
			qualifiers.addObject(getQualifierValide());

			return fetchAll(ec, new EOAndQualifier(qualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOPasse>();
		}
	}

	/** Retourne la liste des passes hors EN non-titulaires concernant la fonction publique */
	public static NSArray<EOPasse> rechercherPassesPourIndividuAvecServicesValides(EOEditingContext editingContext,EOIndividu individu) {
		return rechercherPassesPourIndividuAvecServicesValidesEtPeriode(editingContext, individu,null,null);
	}
	

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierIndividu(EOIndividu individu) {
		return  EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray<EOIndividu>(individu));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierValide() {
		return  EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray<String>("O"));
	}
}
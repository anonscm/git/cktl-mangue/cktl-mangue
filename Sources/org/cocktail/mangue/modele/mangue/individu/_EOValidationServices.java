/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOValidationServices.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOValidationServices extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ValidationServices";
	public static final String ENTITY_TABLE_NAME = "MANGUE.VALIDATION_SERVICES";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "valId";

	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String VAL_ANNEES_KEY = "valAnnees";
	public static final String DATE_VALIDATION_KEY = "dateValidation";
	public static final String VAL_ETABLISSEMENT_KEY = "valEtablissement";
	public static final String VAL_JOURS_KEY = "valJours";
	public static final String VAL_MINISTERE_KEY = "valMinistere";
	public static final String VAL_MOIS_KEY = "valMois";
	public static final String VAL_PC_ACQUITEE_KEY = "valPcAcquitee";
	public static final String VAL_QUOTITE_KEY = "valQuotite";
	public static final String VAL_TYPE_TEMPS_KEY = "valTypeTemps";

// Attributs non visibles
	public static final String CTRA_ORDRE_KEY = "ctraOrdre";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String PAS_ORDRE_KEY = "pasOrdre";
	public static final String VAL_ID_KEY = "valId";
	public static final String VAL_TYPE_FCT_PUBLIQUE_KEY = "valTypeFctPublique";
	public static final String VAL_TYPE_SERVICE_KEY = "valTypeService";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_COLKEY = "D_DEB_VALIDATION";
	public static final String DATE_FIN_COLKEY = "D_FIN_VALIDATION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";
	public static final String VAL_ANNEES_COLKEY = "VAL_ANNEES";
	public static final String DATE_VALIDATION_COLKEY = "VAL_D_VALIDATION";
	public static final String VAL_ETABLISSEMENT_COLKEY = "VAL_ETABLISSEMENT";
	public static final String VAL_JOURS_COLKEY = "VAL_JOURS";
	public static final String VAL_MINISTERE_COLKEY = "VAL_MINISTERE";
	public static final String VAL_MOIS_COLKEY = "VAL_MOIS";
	public static final String VAL_PC_ACQUITEE_COLKEY = "VAL_PC_ACQUITEE";
	public static final String VAL_QUOTITE_COLKEY = "VAL_QUOTITE";
	public static final String VAL_TYPE_TEMPS_COLKEY = "VAL_TYPE_TEMPS";

	public static final String CTRA_ORDRE_COLKEY = "CTRA_ORDRE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String PAS_ORDRE_COLKEY = "PAS_ORDRE";
	public static final String VAL_ID_COLKEY = "VAL_ID";
	public static final String VAL_TYPE_FCT_PUBLIQUE_COLKEY = "VAL_TYPE_FCT_PUBLIQUE";
	public static final String VAL_TYPE_SERVICE_COLKEY = "VAL_TYPE_SERVICE";


	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_CONTRAT_AVENANT_KEY = "toContratAvenant";
	public static final String TO_PASSE_KEY = "toPasse";
	public static final String TO_TYPE_FONCTION_PUBLIQUE_KEY = "toTypeFonctionPublique";
	public static final String TO_TYPE_SERVICE_KEY = "toTypeService";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public Integer valAnnees() {
    return (Integer) storedValueForKey(VAL_ANNEES_KEY);
  }

  public void setValAnnees(Integer value) {
    takeStoredValueForKey(value, VAL_ANNEES_KEY);
  }

  public NSTimestamp dateValidation() {
    return (NSTimestamp) storedValueForKey(DATE_VALIDATION_KEY);
  }

  public void setDateValidation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_VALIDATION_KEY);
  }

  public String valEtablissement() {
    return (String) storedValueForKey(VAL_ETABLISSEMENT_KEY);
  }

  public void setValEtablissement(String value) {
    takeStoredValueForKey(value, VAL_ETABLISSEMENT_KEY);
  }

  public Integer valJours() {
    return (Integer) storedValueForKey(VAL_JOURS_KEY);
  }

  public void setValJours(Integer value) {
    takeStoredValueForKey(value, VAL_JOURS_KEY);
  }

  public String valMinistere() {
    return (String) storedValueForKey(VAL_MINISTERE_KEY);
  }

  public void setValMinistere(String value) {
    takeStoredValueForKey(value, VAL_MINISTERE_KEY);
  }

  public Integer valMois() {
    return (Integer) storedValueForKey(VAL_MOIS_KEY);
  }

  public void setValMois(Integer value) {
    takeStoredValueForKey(value, VAL_MOIS_KEY);
  }

  public String valPcAcquitee() {
    return (String) storedValueForKey(VAL_PC_ACQUITEE_KEY);
  }

  public void setValPcAcquitee(String value) {
    takeStoredValueForKey(value, VAL_PC_ACQUITEE_KEY);
  }

  public java.math.BigDecimal valQuotite() {
    return (java.math.BigDecimal) storedValueForKey(VAL_QUOTITE_KEY);
  }

  public void setValQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, VAL_QUOTITE_KEY);
  }

  public String valTypeTemps() {
    return (String) storedValueForKey(VAL_TYPE_TEMPS_KEY);
  }

  public void setValTypeTemps(String value) {
    takeStoredValueForKey(value, VAL_TYPE_TEMPS_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOContratAvenant toContratAvenant() {
    return (org.cocktail.mangue.modele.mangue.individu.EOContratAvenant)storedValueForKey(TO_CONTRAT_AVENANT_KEY);
  }

  public void setToContratAvenantRelationship(org.cocktail.mangue.modele.mangue.individu.EOContratAvenant value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOContratAvenant oldValue = toContratAvenant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CONTRAT_AVENANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CONTRAT_AVENANT_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.individu.EOPasse toPasse() {
    return (org.cocktail.mangue.modele.mangue.individu.EOPasse)storedValueForKey(TO_PASSE_KEY);
  }

  public void setToPasseRelationship(org.cocktail.mangue.modele.mangue.individu.EOPasse value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOPasse oldValue = toPasse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PASSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PASSE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique toTypeFonctionPublique() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique)storedValueForKey(TO_TYPE_FONCTION_PUBLIQUE_KEY);
  }

  public void setToTypeFonctionPubliqueRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique oldValue = toTypeFonctionPublique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_FONCTION_PUBLIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_FONCTION_PUBLIQUE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeService toTypeService() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeService)storedValueForKey(TO_TYPE_SERVICE_KEY);
  }

  public void setToTypeServiceRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeService value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeService oldValue = toTypeService();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_SERVICE_KEY);
    }
  }
  

/**
 * Créer une instance de EOValidationServices avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOValidationServices createEOValidationServices(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dateFin
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu, org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique toTypeFonctionPublique, org.cocktail.mangue.common.modele.nomenclatures.EOTypeService toTypeService			) {
    EOValidationServices eo = (EOValidationServices) createAndInsertInstance(editingContext, _EOValidationServices.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    eo.setToTypeFonctionPubliqueRelationship(toTypeFonctionPublique);
    eo.setToTypeServiceRelationship(toTypeService);
    return eo;
  }

  
	  public EOValidationServices localInstanceIn(EOEditingContext editingContext) {
	  		return (EOValidationServices)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOValidationServices creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOValidationServices creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOValidationServices object = (EOValidationServices)createAndInsertInstance(editingContext, _EOValidationServices.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOValidationServices localInstanceIn(EOEditingContext editingContext, EOValidationServices eo) {
    EOValidationServices localInstance = (eo == null) ? null : (EOValidationServices)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOValidationServices#localInstanceIn a la place.
   */
	public static EOValidationServices localInstanceOf(EOEditingContext editingContext, EOValidationServices eo) {
		return EOValidationServices.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOValidationServices fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOValidationServices fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOValidationServices eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOValidationServices)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOValidationServices fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOValidationServices fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOValidationServices eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOValidationServices)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOValidationServices fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOValidationServices eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOValidationServices ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOValidationServices fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

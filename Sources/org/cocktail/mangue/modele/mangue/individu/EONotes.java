// EONotes.java
// Created on Fri Apr 04 08:36:17  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;


import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOGenericRecord;
public class EONotes extends EOGenericRecord {

    public EONotes() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EONotes(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public Number anneeNotation() {
        return (Number)storedValueForKey("anneeNotation");
    }

    public void setAnneeNotation(Number value) {
        takeStoredValueForKey(value, "anneeNotation");
    }

    public Number noteProposee() {
        return (Number)storedValueForKey("noteProposee");
    }

    public void setNoteProposee(Number value) {
        takeStoredValueForKey(value, "noteProposee");
    }

    public Number noteDefinitive() {
        return (Number)storedValueForKey("noteDefinitive");
    }

    public void setNoteDefinitive(Number value) {
        takeStoredValueForKey(value, "noteDefinitive");
    }

    public String poncAss() {
        return (String)storedValueForKey("poncAss");
    }

    public void setPoncAss(String value) {
        takeStoredValueForKey(value, "poncAss");
    }

    public String activEffic() {
        return (String)storedValueForKey("activEffic");
    }

    public void setActivEffic(String value) {
        takeStoredValueForKey(value, "activEffic");
    }

    public String autoSExec() {
        return (String)storedValueForKey("autoSExec");
    }

    public void setAutoSExec(String value) {
        takeStoredValueForKey(value, "autoSExec");
    }

    public String appGen() {
        return (String)storedValueForKey("appGen");
    }

    public void setAppGen(String value) {
        takeStoredValueForKey(value, "appGen");
    }

    public Number notePrecedent() {
        return (Number)storedValueForKey("notePrecedent");
    }

    public void setNotePrecedent(Number value) {
        takeStoredValueForKey(value, "notePrecedent");
    }

    public String propReduction() {
        return (String)storedValueForKey("propReduction");
    }

    public void setPropReduction(String value) {
        takeStoredValueForKey(value, "propReduction");
    }

    public String noDossierMinistere() {
        return (String)storedValueForKey("noDossierMinistere");
    }

    public void setNoDossierMinistere(String value) {
        takeStoredValueForKey(value, "noDossierMinistere");
    }

    public EOGrade grade() {
        return (EOGrade)storedValueForKey("grade");
    }

    public void setGrade(EOGrade value) {
        takeStoredValueForKey(value, "grade");
    }

    public EOIndividu individu() {
        return (EOIndividu)storedValueForKey("individu");
    }

    public void setIndividu(EOIndividu value) {
        takeStoredValueForKey(value, "individu");
    }
    // méthodes ajoutées
    public void initAvecIndividuEtGrade(EOIndividu individu,EOGrade grade) {
		addObjectToBothSidesOfRelationshipWithKey(individu,"individu");
		if (grade != null) {
			addObjectToBothSidesOfRelationshipWithKey(grade,"grade");
		}
    }
    public void supprimerRelations() {
    		removeObjectFromBothSidesOfRelationshipWithKey(individu(),"individu");
    		removeObjectFromBothSidesOfRelationshipWithKey(grade(),"grade");
    }
    /** Tests de coherence pour l'enregistrement d'une note */
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		// vérifier les longueurs des chaînes
		if (anneeNotation() == null || anneeNotation().toString().length() < 4) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("L'année doit comporter 4 chiffres");
		}
		// vérifier les notes mais je ne sais pas les valeurs
		if (noDossierMinistere() != null && noDossierMinistere().length() > 13) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le numéro de dossier ministère doit comporter 13 caractères au maximum");
		}
	}
}

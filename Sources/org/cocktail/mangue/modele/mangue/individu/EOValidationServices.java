/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.individu;

import java.util.Enumeration;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeFonctionPublique;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeService;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl.IntRef;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOValidationServices extends _EOValidationServices {

	public static final EOSortOrdering SORT_DATE_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_DATE_DESC = new NSArray(SORT_DATE_DESC);

	private static final String TYPE_TEMPS_PARTIEL = "P";
	private static final String TYPE_TEMPS_INCOMPLET = "I";
	private static final String TYPE_TEMPS_COMPLET = "C";

	public EOValidationServices() {
		super();
	}

	public static EOValidationServices creer(EOEditingContext ec, EOIndividu individu) {

		EOValidationServices newObject = (EOValidationServices) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setToTypeServiceRelationship((EOTypeService)NomenclatureFinder.findForCode(ec, EOTypeService.ENTITY_NAME, EOTypeService.TYPE_SERVICE_VALIDES));
		newObject.setToTypeFonctionPubliqueRelationship((EOTypeFonctionPublique)NomenclatureFinder.findForCode(ec, EOTypeFonctionPublique.ENTITY_NAME, EOTypeFonctionPublique.TYPE_FCT_PUBLIQUE_ETAT));
		newObject.setIndividuRelationship(individu);
		newObject.setValTypeTemps(TYPE_TEMPS_COMPLET);
		newObject.setValPcAcquitee(CocktailConstantes.VRAI);
		newObject.setTemValide(CocktailConstantes.VRAI);
		newObject.setValQuotite(ManGUEConstantes.QUOTITE_100);

		return newObject;

	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOValidationServices> findForIndividu(EOEditingContext ec, EOIndividu individu) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray("O")));

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DESC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param avenant
	 * @return
	 */
	public static EOValidationServices findForAvenant(EOEditingContext ec, EOContratAvenant avenant) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CONTRAT_AVENANT_KEY + " = %@", new NSArray(avenant)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI)));

			return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray<EOValidationServices> findForIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));		
		if (debutPeriode != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));
		}
		return fetchAll(editingContext, new EOAndQualifier(qualifiers), new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending)));
	}


	public boolean isPcAcquitees() {
		return valPcAcquitee() != null && valPcAcquitee().equals(CocktailConstantes.VRAI);
	}
	public void setPcAquitees(boolean aBool) {
		if (aBool)
			setValPcAcquitee(CocktailConstantes.VRAI);
		else
			setValPcAcquitee(CocktailConstantes.FAUX);
	}

	public boolean estTempsComplet() {
		return valTypeTemps().equals(TYPE_TEMPS_COMPLET);
	}
	public boolean estTempsIncomplet() {
		return valTypeTemps().equals(TYPE_TEMPS_INCOMPLET);
	}
	public boolean estTempsPartiel() {
		return valTypeTemps().equals(TYPE_TEMPS_PARTIEL);
	}

	public void setTempsComplet() {
		setValTypeTemps(TYPE_TEMPS_COMPLET);
	}
	public void setTempsIncomplet() {
		setValTypeTemps(TYPE_TEMPS_INCOMPLET);
	}
	public void setTempsPartiel() {
		setValTypeTemps(TYPE_TEMPS_PARTIEL);
	}

	public boolean isValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean yn) {
		if (yn)
			setTemValide(CocktailConstantes.VRAI);
		else
			setTemValide(CocktailConstantes.FAUX);
	}

	public boolean pcAcquittees() {
		return valPcAcquitee() != null && valPcAcquitee().equals(CocktailConstantes.VRAI);
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		if (temValide().equals("O")) {

			if (dateDebut() == null)
				throw new NSValidation.ValidationException("Vous devez fournir une date de début");

			if (dateFin() == null)
				throw new NSValidation.ValidationException("Vous devez fournir une date de fin");

			if (DateCtrl.isAfter(dateDebut(), dateFin()))
				throw new NSValidation.ValidationException("VALIDATION - La date de fin doit être supérieure à la date de début !");

			if (valEtablissement() == null  && valMinistere() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir soit un ministère soit un établissement !");
			}

			if (valEtablissement() != null && valEtablissement().length() > 65) {
				throw new NSValidation.ValidationException("Le nom de l'établissement comporte au maximum 65 caractères");
			}

			if (toTypeFonctionPublique() == null)
				throw new NSValidation.ValidationException("Le type de fonction publique est obligatoire !");

			if (toTypeService() == null)
				throw new NSValidation.ValidationException("Le type de service est obligatoire !");

			// Quotite de cotisation > 50 et < 100
			if (estTempsPartiel()) {
				if (valQuotite() != null && (valQuotite().floatValue() < 50 || valQuotite().floatValue() > 99 ) ) {
					throw new NSValidation.ValidationException("Pour un temps partiel la quotité de cotisation doit être comprise entre 50 et 99% !");				
				}
			}

			String message = validationsCir();
			if (message != null && message.length() > 0)
				throw new NSValidation.ValidationException(message);
		}
		if (dCreation() == null)
			setDCreation(new NSTimestamp());

		setDModification(new NSTimestamp());

	}


	/** Les validations Cir ne s'appliquent qu'aux non-titulaires du service public : <BR>
	 * La date de validation des services doit etre fournie<BR>
	 * La quotite de service doit etre fournie<BR>
	 * Les durees validees ne doivent etre fournies que pour une quotite de service et de cotisation de 100%<BR>
	 * La quotite de service et celle de cotisation doivent valoir 0 ou etre comprise entre 50% et 100%<BR>
	 * Pas de chevauchement des periodes pour les temps plein et pour les temps partiels le cumul de quotite
	 * doit etre inferieur ou egal a 100% et pour les temps incomplets, cumul des durees 
	 * inferieur a la duree de la periode<BR>
	 * On verifie aussi qu'il n'y a pas de chevauchements des passes avec les carrieres/contrats
	 * 
	 * @return
	 */
	public String validationsCir() {

		if (valMinistere() != null && valMinistere().length() > 25)
			return "Services validés -Le libellé du ministère ne doit pas dépasser 25 caractères !";
		else
			if (valMinistere() == null && valEtablissement() != null && valEtablissement().length() > 25)
				return "Services validés -Le libellé de l'établissement ne doit pas dépasser 25 caractères !";

		// Validation des services valides
		if ((valAnnees() != null && valAnnees().intValue() > 0) ||
				(valMois() != null && valMois().intValue() > 0) ||
				(valJours() != null && valJours().intValue() > 0)) {

			if (valQuotite() == null || valQuotite().floatValue() == 0) 
				return "Services validés - La quotité de cotisation ne peut être égale à 0 !";

			if (valMois() != null && (valMois().intValue() < 0 || valMois().intValue() > 11)) {
				return "Services validés - Les mois validés doivent être compris entre 0 et 11 !";
			}
			if (valJours() != null && (valJours().intValue() < 0 || valJours().intValue() > 29) ) {
				return "Services validés - Les jours validés doivent être compris entre 0 et 29 !";
			}

			if (dateValidation() == null)
				throw new NSValidation.ValidationException("Vous devez fournir une date de validation des services !");

			int annees = 0,mois = 0, jours = 0;
			if (valAnnees() != null && valAnnees().intValue() > 0) {
				annees = valAnnees().intValue();
			}
			if (valMois() != null && valMois().intValue() > 0) {
				mois = valMois().intValue();
			}
			if (valJours() != null && valJours().intValue() > 0) {
				jours = valJours().intValue();
			}

			int nbJoursValides = (annees * 360) + (mois * 30) + jours;

			int nbJoursCalcules = DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true);

			if (DateCtrl.dateToString(dateDebut(), "%d/%m").equals("01/02") && 
					DateCtrl.dateToString(dateFin(), "%d/%m").equals("28/02"))
				nbJoursCalcules += 2;

			if (DateCtrl.dateToString(dateDebut(), "%d/%m").equals("01/02") && 
					DateCtrl.dateToString(dateFin(), "%d/%m").equals("29/02")) {
				nbJoursCalcules ++;
			}

			// Test de l'année bisextile
			int annee = DateCtrl.getYear(dateDebut());
			boolean anneeBisextile = (annee % 4) == 0;
			if (anneeBisextile) {
				if (DateCtrl.isBefore(dateDebut(), DateCtrl.stringToDate("29/02/"+annee))
						&& DateCtrl.isAfter(dateFin(), DateCtrl.stringToDate("29/02/"+annee)) ) {
					nbJoursCalcules ++;
				}				
			}
			else {
				if (DateCtrl.isBefore(dateDebut(), DateCtrl.stringToDate("28/02/"+annee))
						&& DateCtrl.isAfter(dateFin(), DateCtrl.stringToDate("28/02/"+annee)) ) {
					nbJoursCalcules +=2;
				}								
			}

			if (nbJoursValides > nbJoursCalcules) {
				throw new NSValidation.ValidationException("Services validés - Le nombre de jours validés ("+nbJoursValides + " Jours) est supérieur au nombre d'années, mois, jours écoulés entre la date début et la date fin ("+nbJoursCalcules+" Jours entre le " + DateCtrl.dateToString(dateDebut())+" et le " + DateCtrl.dateToString(dateFin()) + " ) !");
			}	
		}


		//		// Vérifier qu'il n'y a pas de chevauchements avec les carrières/contrats
		//		NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(), individu(), dateDebut(), dateFin(), false);
		//		if (contratAvenant() != null && carrieres.count() > 0) {
		//			return "Les périodes de PASSE ne peuvent pas chevaucher des segments de carrière dans l'établissement";
		//		}

		if (toTypeService() != null && toTypeService().estTypeServiceValide()) {

			if (dateValidation() == null) {
				//				return "Pour les services validés, vous devez fournir la date de validation";
			} else if (DateCtrl.isBefore(dateValidation(), dateFin())) {
				return "Services validés - La date de validation " + DateCtrl.dateToString(dateValidation()) + " ne peut être antérieure à la date de fin de service (" + DateCtrl.dateToString(dateFin())+ " ";
			}
			if (valQuotite() == null && estTempsPartiel()) {
				return "Services validés - La quotité de service doit être fournie pour un service à temps partiel";
			}
			if (valPcAcquitee() == null) {
				return "Services validés - Le témoin 'PC Acquitées' doit être fourni";
			}
			double quotite = valQuotite().doubleValue();
			/*if ((dureeValideeAnnees() != null || dureeValideeMois() != null || dureeValideeJours() != null)) {
				if (quotite != 100) {
					return "Pour les services validés dont la durée en années, mois, jours est fournie, la quotité de service doit être 100%";
				}

				if (pasQuotiteCotisation() != null && pasQuotiteCotisation().doubleValue() != 100) {
					return "Pour les services validés dont la durée en années, mois, jours est fournie, la quotité de cotisation doit être 100%";
				}
			} */
			if ((quotite != 0 && (quotite < 50.00 || quotite  > 100.00)) || quotite < 0) {
				return "La quotité de service doit être égale à zéro ou comprise entre 50% et 99%";
			}

			if (valQuotite() != null) {
				quotite = valQuotite().doubleValue();
				if ((quotite != 0 && (quotite < 50.00 || quotite  > 100.00)) || quotite < 0) {
					return "la quotité  de cotisation doit être égale à zéro ou comprise entre 50% et 100%";
				}
			}
			return verifierChevauchements();
		} 		

		return null;
	}


	// Méthodes privées
	private String verifierChevauchements() {
		// Vérification des chevauchements
		NSArray passes = EOValidationServices.findForIndividuEtPeriode(editingContext(), individu(), dateDebut(), dateFin());
		// Commencer par construire des passesAvecDureesCalculees

		NSMutableArray passesAvecDurees = new NSMutableArray();
		java.util.Enumeration<EOValidationServices> e = passes.objectEnumerator();
		while (e.hasMoreElements()) {
			EOValidationServices validation = (EOValidationServices)e.nextElement();
			if (validation != this) {	// on ne le garde pas car on prendra en compte sa quotité ou ses nombres de jours au démarrage
				passesAvecDurees.addObject(new PasseAvecDureesCalculees(validation, estTempsIncomplet()));
			}
		}
		// Pour les temps incomplets, vérifier que les durées validées totales ne dépassent pas la durée sur la période
		int nbAnneesTotal = 0, nbMoisTotal = 0, nbJoursTotal = 0;
		if (estTempsIncomplet()) {
			if (valAnnees() != null) {
				nbAnneesTotal = valAnnees().intValue();
			}
			if (valMois() != null) {
				nbMoisTotal = valMois().intValue();
			}
			if (valJours() != null) {
				nbJoursTotal = valJours().intValue();
			}
		}
		// Pour les temps pleins et partiels, on vérifie la quotité
		double quotiteTotale = valQuotite().doubleValue();
		NSMutableArray periodesAConsiderer = new NSMutableArray();

		for (Enumeration<PasseAvecDureesCalculees>e1 = passesAvecDurees.objectEnumerator();e1.hasMoreElements();) {
			PasseAvecDureesCalculees passe = (PasseAvecDureesCalculees)e1.nextElement();
			if (passe.quotite() == null) {
				return "La quotité de service doit être fournie pour le passé débutant le " + DateCtrl.dateToString(passe.dateDebut());
			}
			if (estTempsIncomplet()) {
				nbAnneesTotal += passe.nbAnnees();
				nbMoisTotal += passe.nbMois();
				nbJoursTotal += passe.nbJours();
			} else {
				quotiteTotale = quotiteTotale + passe.quotite().doubleValue();
			}
			// Vérifier si le passé courant est postérieur aux passés déjà pris en compte
			// en quel cas il ne faut plus prendre en compte ces derniers en compte et retirer leur quotité et nombre de jours, mois, années
			if (periodesAConsiderer.count() > 0) {
				for (java.util.Enumeration<PasseAvecDureesCalculees> e2 = periodesAConsiderer.objectEnumerator();e2.hasMoreElements();) {
					PasseAvecDureesCalculees passeASupprimer = (PasseAvecDureesCalculees)e2.nextElement();
					if (DateCtrl.isBefore(passeASupprimer.dateFin(), passe.dateDebut())) {
						// Le passé n'a plus court sur le passé en cours d'analyse
						// Enlever sa quotité et le supprimer des passés pris en compte, les passés étant classés
						// par date de début croissant, on trouvera ensuite toujours des passés postérieurs
						if (estTempsIncomplet()) {
							LogManager.logDetail("Suppression des durées de la periode du " + DateCtrl.dateToString(passeASupprimer.dateDebut()));
							nbAnneesTotal -= passeASupprimer.nbAnnees();
							nbMoisTotal -= passeASupprimer.nbMois();
							nbJoursTotal = passeASupprimer.nbJours();
						} else {
							LogManager.logDetail("Suppression de la quotité de la periode du " + DateCtrl.dateToString(passeASupprimer.dateDebut()));
							quotiteTotale = quotiteTotale - passeASupprimer.quotite().doubleValue();
							LogManager.logDetail("quotité totale après suppression " + quotiteTotale);
						}
					}
					periodesAConsiderer.removeObject(passeASupprimer);
				}
			}
			periodesAConsiderer.addObject(passe);
		}

		//		if (estTempsIncomplet()) {
		//
		//			// On ramène tout en jours comptables
		//			// Total de la duree validee saisie
		//			int total = (nbAnneesTotal * 360) + (nbMoisTotal * 30) + nbJoursTotal;
		//
		//			// Duree calculee de la periode
		//			int totalPeriode = DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true, true);
		//			//			IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
		//			//			DateCtrl.joursMoisAnneesEntre(dateDebut(), dateFin(), anneeRef, moisRef, jourRef, false,true);
		//			//			int totalPeriode = (anneeRef.value * 360) + (moisRef.value * 30) + jourRef.value;
		//
		//			if (total > totalPeriode) {
		//				return "Le cumul des durées validées saisies des passés (" + total + " Jours) dépasse la durée du passé (" + totalPeriode + " Jours) !";
		//			}			
		//		} else 
		if (quotiteTotale > 100.00) {
			return "La quotité de service cumulée des passés qui se chevauchent dépasse 100%";
		}
		return null;
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}
	
	/**
	 * 
	 * @author cpinsard
	 *
	 */
	private class PasseAvecDureesCalculees {
		private EOValidationServices validation;
		private int nbAnnees;
		private int nbMois;
		private int nbJours;
		public PasseAvecDureesCalculees(EOValidationServices validation, boolean estPourTempsIncomplet) {
			this.validation = validation;
			if (estPourTempsIncomplet) {
				if (validation.valAnnees() != null || validation.valMois() != null || validation.valJours() != null) {
					if (validation.valAnnees() != null) {
						nbAnnees = valAnnees().intValue();
					}
					if (valMois() != null) {
						nbMois = valMois().intValue();
					}
					if (valJours() != null) {
						nbJours = valJours().intValue();
					}
				} else {
					IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
					DateCtrl.joursMoisAnneesEntre(validation.dateDebut(), validation.dateFin(), anneeRef, moisRef, jourRef, false,true);
					nbAnnees = anneeRef.value;
					nbMois = moisRef.value;
					nbJours = jourRef.value;
				}
			}

		}
		public int nbAnnees() {
			return nbAnnees;
		}
		public int nbMois() {
			return nbMois;
		}
		public int nbJours() {
			return nbJours;
		}
		public Number quotite() {
			return validation.valQuotite();
		}
		public NSTimestamp dateDebut() {
			return validation.dateDebut();
		}	
		public NSTimestamp dateFin() {
			return validation.dateFin();
		}
	}

}

//EOMad.java
//Created on Tue Mar 11 10:41:12  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** 	
 * Regles de validation :<BR>
 * Pas de chevauchement des Mad<BR>
 * Le type de Mad, la date debut, la date de fin et la rne ou le lieu d'accueil doivent etre fournis<BR>
 * la date de debut doit etre anterieure a celle de fin<BR> 
 * Verifie la compatibilite avec les conges et les modalites de service <BR>
 * Verifie que l'individu peut beneficier d'une Mad (fonctionnaire titulaire non stagiaire).<BR>
 * La Mad doit reposer sur une position d'activite ou de detachement dans l'etablissement.<BR>
 * On doit obligatoirement saisir un R.N.E. d'accueil ou bien un lieu hors R.N.E. Si on saisit un RNE alors celui-ci doit etre different 
 * du RNE de l'&eacute;tablissement.<BR>
 * On ne peut pas saisir une mise e disposition si l'agent est affecte a 100% sur cette periode.<BR>
 * Verification de la quotite (0 < quotite < 100)<BR>
 * Verification par rapport a la quotite d'affectation<BR>
 * @author christine
 */
// 25/03/2011 - ajout de la quotité de MAD
public class EOMad extends _EOMad {
	public static int DUREE_MAX_CHERCHEUR = 60;	// mois
	public EOMad() {
		super();
	}


	public String typeEvenement() {
		return "MAD";
	}
	public void validateForSave() throws NSValidation.ValidationException {
		
		super.validateForSave();
		
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de fin");
		}
		if (toTypeMad() == null) {
			throw new NSValidation.ValidationException("Vous devez sélectionner le type de Mise à disposition");
		}
		if (toRne() == null && (lieuMad() == null || lieuMad().length() == 0)) {
			throw new NSValidation.ValidationException("Vous devez saisir une UAI ou un lieu d'accueil");
		}
		if (lieuMad() != null && lieuMad().length() > 80) {
			throw new NSValidation.ValidationException("Le lieu d'accueil comporte au maximum 80 caractères");
		}

		if (toRne() != null) {
			NSArray rneStructures = EOStructure.rneStructuresEtablissements(editingContext());
			for (java.util.Enumeration e = rneStructures.objectEnumerator();e.hasMoreElements();) {
				Object rne = e.nextElement();
				if (rne != NSKeyValueCoding.NullValue && rne.equals(toRne().code())) {
					throw new NSValidation.ValidationException("L'UAI d'accueil ne doit pas faire partie de l'établissement");
				}
			}
		}
		
		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),individu(),dateDebut(),dateFin());

		// vérifie si fonctionnaire titulaire, non en stage
		if (carrieres == null || carrieres.count() == 0) {
			throw new NSValidation.ValidationException("Il doit y avoir au moins un segment de carrière défini pour cette période !");
		} else {
			
			EOCarriere carriereTrouvee = null;
			boolean enActivite = false;
			for (EOCarriere carriere : carrieres) {

				NSArray<EOStage> stages = carriere.stagesPourPeriode(dateDebut(),dateFin());
				if (carriere.toTypePopulation().estFonctionnaire() && individu().estTitulaire()) {
					
					if (stages != null && stages.count() > 0)
						throw new NSValidation.ValidationException("Les MAD ne sont valides que pour les fonctionnaires titulaires non stagiaires !");

					NSArray<EOChangementPosition> changements = carriere.changementsPositionPourPeriode(dateDebut(),dateFin());
					if (changements != null && changements.count() > 0) {
						carriereTrouvee = carriere;
						for (EOChangementPosition changement : changements) {
							// La période de maintien en fonction doit reposer sur une position d'activité ou de détachement avec carrière d'accueil
							if (!changement.toPosition().estEnActiviteDansEtablissement() && (!changement.toPosition().estUnDetachement() || (changement.estDetachementSortant()))) {
								enActivite = false;
							} else {
								enActivite = true;
							}
						}
					}
					if (enActivite) {
						break;
					}
				}
			}
			
			if (carriereTrouvee == null) {
				throw new NSValidation.ValidationException("Pas de changement de position défini pendant toute la durée de la MAD !");
			}
			if (!enActivite) {
				throw new NSValidation.ValidationException("Une MAD doit reposer sur une position d'activité ou de détachement avec carrière d'accueil !");
			}
			
			// contrôle non blocant => effectué dans l'interface
			/*int nbMois = DateCtrl.calculerDureeEnMois(dateDebut(),dateFin()).intValue();
        		EOCorps corpsAgrege = null;
        		if (carriereTrouvee.toTypePopulation().est2Degre()) {
        			NSArray elementsCarriere = carriereTrouvee.elementsPourPeriode(dateDebut(),dateFin());
        			e = elementsCarriere.objectEnumerator();
        			while (e.hasMoreElements()) {
        				EOElementCarriere element = (EOElementCarriere)e.nextElement();
        				if (element.toCorps().cCorps().equals(EOCorps.CORPS_POUR_AGREFGE_2DEGRE)) {
        					corpsAgrege = element.toCorps();
        					break;
        				}
        			}
        		}
        		if (carriereTrouvee.toTypePopulation().cTypePopulation().equals(EOTypePopulation.ENSEIGNANT_CHERCHEUR) ||
        			corpsAgrege != null) {
        			if (nbMois > 60) {
        				throw new NSValidation.ValidationException("Pour les enseignants-chercheurs, la durée maximum d'une période de mise à disposition est de 5 ans");
        			}
        		} else {
        			if (nbMois > 36) {
        				throw new NSValidation.ValidationException("La durée maximum d'une période de mise à disposition est de 3 ans");
        			}
        		}
		 */

			// 25/03/2011 - Vérification de la quotité
			double quotiteMad = 0;
			if (madQuotite() != null) {
				quotiteMad = madQuotite().doubleValue();
				if (quotiteMad == 0) {
    				throw new NSValidation.ValidationException("La quotité ne peut être nulle");
				} else if (quotiteMad > 100.00) {
    				throw new NSValidation.ValidationException("La quotité ne peut pas être supérieure à 100%");
				}
				// vérifier la quotité des affectations : la somme des quotités d'affectation et de Mad ne peut dépasser 100%
				verifierQuotiteAffectations();

			} else
				throw new NSValidation.ValidationException("La quotité ne peut être nulle !");
			
		}
	}
	
	public String affichageLieu() {
		
		if (toRne() != null)
			return toRne().libelleLong();
		
		return lieuMad();
		
	}
	
	// méthodes protégées
	protected void init() {
		setTemValide("O");
		setMadQuotite(new Double(100.00));
	}
	// Méthodes privées
	private void verifierQuotiteAffectations() {
		NSArray affectations = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin());
		affectations = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		double quotiteTotaleAffectation = 0;
		NSMutableArray affectationsAConsiderer = new NSMutableArray();
		for (java.util.Enumeration<EOAffectation> e = affectations.objectEnumerator();e.hasMoreElements();) {
			EOAffectation affectation = e.nextElement();
			if (affectationsAConsiderer.containsObject(affectation) == false) {
				String texte = "mad du " + affectation.dateDebut();
				if (affectation.dateFin() != null) {
					texte += " au " + affectation.dateFin();
				}
				texte += ", quotité : " + affectation.quotite();
				LogManager.logDetail(texte);
				quotiteTotaleAffectation = quotiteTotaleAffectation + affectation.quotite().doubleValue();
				LogManager.logDetail("quotité totale " + quotiteTotaleAffectation);
				//	 Vérifier si l'affectation courante est postérieure aux affectations prises en compte, en quel cas il ne faut
				// plus prendre en compte leur quotité
				if (affectationsAConsiderer.count() > 0) {
					NSMutableArray affectationsGardees = new NSMutableArray();
					for (java.util.Enumeration<EOAffectation> e1 = affectationsAConsiderer.objectEnumerator();e1.hasMoreElements();) {
						EOAffectation affectationASupprimer = e1.nextElement();
						if (affectationASupprimer.dateFin() != null && DateCtrl.isBefore(affectationASupprimer.dateFin(), affectation.dateDebut())) {
							// L'affectation n'a plus court sur la période en cours d'analyse
							// Enlever sa quotité et le supprimer des affectations prises en compte, les affectations étant classées
							// par date début croissant, on trouvera ensuite toujours des affectations postérieures
							quotiteTotaleAffectation = quotiteTotaleAffectation - affectationASupprimer.quotite().doubleValue();
						} else {
							affectationsGardees.addObject(affectationASupprimer);
						}
					}
					// 26/03/2010
					affectationsAConsiderer = new NSMutableArray(affectationsGardees);
				}
				affectationsAConsiderer.addObject(affectation);
			}
		}
//		double quotiteMad = madQuotite().doubleValue();
//		if (quotiteTotaleAffectation > (100 - quotiteMad)) {
//			throw new NSValidation.ValidationException("L'agent est affecté à " + quotiteTotaleAffectation + "% pendant cette période. Les affectations ou la quotité de mise à disposition doivent être modifiées");
//		}
	}
	// méthodes statiques
	/** recherche les mad d'un individu
	 * @param editingContext
	 * @param individu
	 * @return contrats trouves
	 */
	public static NSArray rechercherMadsPourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu));
		return fetchAll(editingContext, myQualifier);
	}
	/** recherche les mad d'un individu anterieurs a la date fournie
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @return contrats trouves
	 */
	public static NSArray rechercherMadsAnterieursADate(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) {
	
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",new NSArray(individu)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@",new NSArray(date)));
		return fetchAll(editingContext, new EOAndQualifier(qualifiers));
	}
}

//EOHistoPromotion.java
//Created on Mon Jun 29 10:26:02 Europe/Paris 2009 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl.IntRef;
import org.cocktail.mangue.modele.FicheAnciennete;
import org.cocktail.mangue.modele.IndividuPromouvable;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOEquivAncGrade;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOParamPromotion;
import org.cocktail.mangue.modele.mangue.modalites.EOModalitesService;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Stocke les informations relatives a la promotion d'un individu.<BR>
 * Comporte des methodes statiques pour determiner/verifier la promouvabilite d'un ou plusieurs individus en prenant
 * en compte les conditions decrites dans les parametres de promotion (EOParamPromotion)<BR>
 * Une promotion a un statut provisoire si elle remplit les conditions de corps depart ou grade depart ou categorie<BR>
 * Elle a un statut automatique (manuel) si elle remplit toutes les conditions et qu'elle est evaluee automatiquement (manuellement)<BR>
 * Une promotion qui a un statut automatique ou manuel peut avoir le dossier de l'individu a destination du Ministere rempli. Tant que
 * le dossier n'est pas rempli, il est possible de revenir en arriere et de la rendre provisoire. Une fois le dossier rempli, la promotion 
 * peut &ecirc;tre transmise au Ministere. Apres avis du Ministere, la promotion a le statut PROMU (acceptee) ou 
 * NEGATIF (refusee) Une fois que la promotion est transmise au Ministere, elle ne pourra pas etre reevaluee dans
 * l'annee de promotion. Une fois que la promotion est acceptee ou refusee, il n'est plus possible de revenir sur la 
 * promotion et de modifier son statut.
 * Verification des conditions :<BR>
 * Si grade de depart et duree de service effectif sans categorie de service effectif et sans type de population => duree effective dans
 * grade (i.e prise en compte des positions permettant l'avancement de grade et de la quotite de position))<BR>
 * Si type de population et duree de service public et pas de duree de service effectif et pas de duree de categorie de service public =>
 * calcul de la duree de service public en prenant en compte le type de population sinon calcul de la duree de service public<BR>
 * Si duree de service effectif et (type de population ou categorie de service effectif) => calcul de la duree effective pour ce type de
 * population ou cette categorie (on prend celle du corps)<BR>
 * Si duree de categorie de service public et ((type de population et pas de duree de service effectif) ou categorie de service public) => 
 * calcul de la dur&eacute; pour ce type de population ou cette categorie (on prend celle du corps)<BR>
 *
 */

public class EOHistoPromotion extends _EOHistoPromotion {
	
	public static final EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(EOHistoPromotion.INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);
	
	/** Evaluation automatique de la promotion : remplit tous les criteres definis dans le parametre associe */
	public final static String STATUT_AUTOMATIQUE = "A";
	/** Evaluation manuelle (demandee par l'utilisateur) de la promotion :  : remplit tous les crites definis dans le parametre associe*/
	public final static String STATUT_MANUEL = "M";
	/** Remplit les conditions de corps depart, grade depart ou categorie mais pas les autres conditions */
	public final static String STATUT_PROVISOIRE = "P";
	/** Promotion avec dossier rempli et transmise au Ministere */
	public final static String STATUT_TRANSMIS_MINISTERE = "T";
	/** Promotion acceptee par le Ministere */
	public final static String STATUT_PROMU = "V";
	/** Promotion refusee par le Ministere */
	public final static String STATUT_NEGATIF = "N";
	/** Promotion supprimee */
	public final static String STATUT_SUPPRIME = "S";

	public EOHistoPromotion()  {
		super();
	}


	public void indiquerManuelle() {
		setHproStatut(STATUT_MANUEL);
	}
	public void indiquerAutomatique() {
		setHproStatut(STATUT_AUTOMATIQUE);
	}
	public void transmettreMinistere() {
		setHproStatut(STATUT_TRANSMIS_MINISTERE);
	}
	/** Indiquer que la promotion est acceptee par le Ministere */
	public void promouvoir() {
		setHproStatut(STATUT_PROMU);
	}
	/** Indiquer que la promotion est refusee par le Ministere */
	public void refuserPromotion() {
		setHproStatut(STATUT_NEGATIF);
	}
	public boolean estProvisoire() {
		return hproStatut() != null && hproStatut().equals(STATUT_PROVISOIRE);
	}
	public void rendreProvisoire() {
		setHproStatut(STATUT_PROVISOIRE);
	}
	public boolean estAjouteeManuellement() {
		return hproStatut() != null && hproStatut().equals(STATUT_MANUEL);
	}
	public boolean estEvalueeAutomatiquement() {
		return hproStatut() != null && hproStatut().equals(STATUT_AUTOMATIQUE);
	}
	public boolean estTransmiseMinistere() {
		return hproStatut() != null && hproStatut().equals(STATUT_TRANSMIS_MINISTERE);
	}
	public boolean individuPromu() {
		return hproStatut() != null && hproStatut().equals(STATUT_PROMU);
	}
	public boolean promotionRefusee() {
		return hproStatut() != null && hproStatut().equals(STATUT_NEGATIF);
	}
	public boolean aRempliDossier() {
		return hproRempli() != null && hproRempli().equals(CocktailConstantes.VRAI);
	}
	public void setARempliDossier(boolean aBool) {
		if (aBool) {
			setHproRempli(CocktailConstantes.VRAI);
		} else {
			setHproRempli(CocktailConstantes.FAUX);
		}
	}	
	public void init() {
		setHproStatut(STATUT_PROVISOIRE);
		setHproRempli(CocktailConstantes.FAUX);
		NSTimestamp today = new NSTimestamp();
		setDCreation(today);
		setDModification(today);
	}
	public void initAvecIndividu(EOIndividu individu) {
		init();
		setIndividuRelationship(individu);
	}
	public void initAvecIndividuParametreEtAnnee(EOIndividu individu, EOParamPromotion paramPromotion, Integer annee) {
		initAvecIndividu(individu);
		setHproAnnee(annee);
		setParamPromotionRelationship(paramPromotion);
	}
	public void invalider() {
		setDModification(new NSTimestamp());
		setHproStatut(STATUT_SUPPRIME);
	}
	public void modifierAvecResultatPromotion(ResultatEvaluationAnciennete resultat) {
		setHproStatut(resultat.statutPromotion());
		setHproEchelon(resultat.echelon());
		setHproDureeGrade(resultat.ancienneteGrade());
		setHproDureeServPublic(resultat.ancienneteService());
		setHproDureeCorps(resultat.ancienneteCorps());
		setHproDureeEchelon(resultat.ancienneteEchelon());
		setHproDureeCategServPublics(resultat.ancienneteCategorieSvcPublic());
		setHproDureeServEffectifs(resultat.ancienneteServiceEffectif());
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (hproAnnee() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une année");
		}
		if (individu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu");
		}
		if (paramPromotion() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un paramètre de promotion");
		}
	}
	/** Retourne l'age (en annees) de l'individu a la date de promotion : "" si la date de naissance est inconnue */
	public String ageADatePromotion() {
		if (individu().dNaissance() == null)
			return "";

		IntRef anneeRef = new DateCtrl.IntRef(),moisRef = new DateCtrl.IntRef(), jourRef = new DateCtrl.IntRef();
		DateCtrl.joursMoisAnneesEntre(individu().dNaissance(), datePromotionPourParametreEtAnnee(paramPromotion().parpType(), hproAnnee()), anneeRef, moisRef, jourRef, true);
		return new Integer(anneeRef.value).toString();
	}
	/** Retourne le libelle du type de population, corps ou grade d'origine */
	public String libelleCorpsGradeDepart() {
		if (paramPromotion() == null) {
			return "";
		} else {
			if (paramPromotion().gradeDepart() != null) {
				return paramPromotion().gradeDepart().lcGrade();
			} else if (paramPromotion().corpsDepart() != null) {
				return paramPromotion().corpsDepart().lcCorps();
			} else if (paramPromotion().typePopulationDepart() != null) {
				return paramPromotion().typePopulationDepart().libelleCourt();
			} else {
				return "";
			}
		}
	}
	
	/** Restreint les fiches d'anciennete aux criteres de promotion */
	public NSArray restreindreAnciennete(NSArray fichesAnciennete) {
		String categorie = null;
		EOTypePopulation typePopulationSvcPublic = null;
		boolean peutUtiliserParametre = paramPromotion().cCategorieServPublics() != null || (paramPromotion().typePopulationServPublics() != null && paramPromotion().parpDureeServEffectifs() == null);
		if (paramPromotion().parpDureeCategServPublics() != null && peutUtiliserParametre)  {
			categorie = paramPromotion().cCategorieServPublics();
			typePopulationSvcPublic = paramPromotion().typePopulationServPublics();
		}
		if (paramPromotion().parpDureeServEffectifs() != null && (paramPromotion().cCategorieServEffectifs() != null || paramPromotion().typePopulationServPublics() != null))  {
			categorie = paramPromotion().cCategorieServEffectifs();
			typePopulationSvcPublic = paramPromotion().typePopulationServPublics();
		}
		return EOHistoPromotion.fichesAnciennetePourCritere(fichesAnciennete, categorie, typePopulationSvcPublic, paramPromotion().parpDureeServPublics() != null);
	}

	public static NSArray rechercherPromotionsReellesPourParametresEtAnnee(EOEditingContext edc,NSArray parametresPromotion,Integer anneePromotion) {
		NSMutableArray statuts = new NSMutableArray(STATUT_PROVISOIRE);
		statuts.addObject(STATUT_SUPPRIME);
		return rechercherPromotionsPourQualifierStatutParametresEtAnnee(edc,EOQualifier.qualifierWithQualifierFormat(HPRO_STATUT_KEY + " <> %@ AND " + HPRO_STATUT_KEY + " <> %@", statuts), parametresPromotion, anneePromotion);
	}
	public static NSArray rechercherPromotionsPotentiellesPourParametreEtAnnee(EOEditingContext edc,NSArray parametresPromotion, Integer anneePromotion) {
		return rechercherPromotionsPourQualifierStatutParametresEtAnnee(edc, EOQualifier.qualifierWithQualifierFormat(HPRO_STATUT_KEY + " = %@", new NSArray(STATUT_PROVISOIRE)), parametresPromotion, anneePromotion);
	}
	public static NSArray rechercherPromotionsPourIndividu(EOEditingContext edc,EOIndividu individu) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@ AND "+HPRO_STATUT_KEY+" <> '" + STATUT_SUPPRIME + "'", new NSArray(individu));
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
		fs.setRefreshesRefetchedObjects(true);
		return edc.objectsWithFetchSpecification(fs);
	}

	/**
	 * 
	 * @param editingContext
	 * @param parametresPromotion
	 * @param anneePromotion
	 * @param rechercherPromotionsEchelon
	 * @return
	 */
	public static String preparerPromotionsPourParametresEtAnnee(EOEditingContext edc,
			NSArray<EOParamPromotion> parametresPromotion, 
			Integer anneePromotion, 
			boolean rechercherPromotionsEchelon) {

		try {

			edc.lock();

			NSArray promotionsExistantes = rechercherPromotionsPourQualifierStatutParametresEtAnnee(edc, 
					EOQualifier.qualifierWithQualifierFormat(HPRO_STATUT_KEY + " != %@", new NSArray(STATUT_SUPPRIME)), parametresPromotion, anneePromotion);

			for (EOParamPromotion parametrePromotion : parametresPromotion) {

				NSTimestamp datePromotion = datePromotionPourParametreEtAnnee(parametrePromotion.parpType(), anneePromotion);
				// 09/02/2010 - Rechercher eventuellement les promotions d'échelon
				NSArray individusPromouvables = null;
				if (rechercherPromotionsEchelon && parametrePromotion.gradeDepart() != null && parametrePromotion.cEchelon() != null) {
					NSTimestamp debutPeriode = DateCtrl.stringToDate("01/01/" + anneePromotion);
					NSMutableArray	qualifiers = new NSMutableArray();
					// On recherche les éléments de carrière valides sur la période
					qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOElementCarriere.DATE_DEBUT_KEY , debutPeriode, EOElementCarriere.DATE_FIN_KEY, datePromotion));
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGrade.C_GRADE_KEY + " = %@", new NSArray(parametrePromotion.gradeDepart().cGrade())));
					EOQualifier qualifier = new EOAndQualifier(qualifiers);
					individusPromouvables = IndividuPromouvable.evaluerIndividusPromouvables(edc, qualifier, debutPeriode, datePromotion, true);
				}

				// Rechercher tous les éléments de carrière valides à la date de promotion qui respectent les critères définis pas le paramètre
				// en terme de corps départ, grade départ ou catégorie de corps (Et echelon d'origine ?)
				EOIndividu individu = EOIndividu.rechercherIndividuAvecID(edc, new Integer(300428), true);
				NSArray<EOElementCarriere> elementsCarriere = rechercherElementsCarrierePourIndividuParametreEtDatePromotion(individu, parametrePromotion, datePromotion);
				
				NSMutableArray nouveauxPromus = new NSMutableArray();
				for (EOElementCarriere element : elementsCarriere) {

					// vérifier si les conditions sont remplies
					ResultatEvaluationAnciennete resultat = verifierConditionsPourElementParametreEtDatePromotion(edc, element, parametrePromotion, datePromotion, individusPromouvables,true);
					EOHistoPromotion currentPromotion = rechercherPromotionPourIndividu(promotionsExistantes, element.toIndividu());
					
					if (resultat != null) {	// Le resultat est null si un individu n'est pas promouvable (disponibilité, hors cadre ou congé parental)
						if (currentPromotion != null) {
							nouveauxPromus.addObject(currentPromotion);
							// Si la promotion a été transmise au ministère, refusée ou acceptée par le ministère alors on ne permet pas la création d'une nouvelle
							// promotion pour l'année en cours et on ne modifie rien dans la promotion courante => currentPromotion = null
							if (currentPromotion.estTransmiseMinistere() || currentPromotion.individuPromu() || currentPromotion.promotionRefusee()) {
								currentPromotion = null;
							} else {
								currentPromotion.setHproStatut(resultat.statutPromotion());
							}
						} else	if (((NSArray)nouveauxPromus.valueForKey(INDIVIDU_KEY)).containsObject(element.toIndividu()) == false) {	// ne devrait pas être true car il n'y a pas de chevauchement des éléments de carrière de chaque individu
							// Créer une nouvelle promotion et l'insérer dans l'editing context
							currentPromotion = new EOHistoPromotion();
							currentPromotion.initAvecIndividuParametreEtAnnee(element.toIndividu(), parametrePromotion, anneePromotion);
							edc.insertObject(currentPromotion);
							nouveauxPromus.addObject(currentPromotion);
						}
						// Modifier ou ajouter les informations calculées pour cette promotion
						if (currentPromotion != null) {
							currentPromotion.modifierAvecResultatPromotion(resultat);
						}
					} else {
						// Si l'individu avait déjà une promotion, il faut l'invalider
						if (currentPromotion != null) {
							currentPromotion.setHproStatut(STATUT_SUPPRIME);
						}
					}
				}
			}
			if (edc.insertedObjects().size() > 0 || edc.updatedObjects().size() > 0) {
				edc.saveChanges();
				return null;
			} else {
				return "Pas d'individu promouvable !";
			}
		} catch (Exception exc) {
			exc.printStackTrace();
			return exc.getMessage();
		} finally {
			edc.unlock();
		}
	}
	
	/** Retourne le resultat de la verification avec STATUT_SUPPRIME comme statut si l'individu ne peut beneficier de cette promotion,
	 * null si l'individu a une position qui ne permet pas la promotion */
	public static ResultatEvaluationAnciennete verifierPromotionPourIndividuAnneeEtParametre(EOIndividu individu, Integer anneePromotion, EOParamPromotion parametrePromotion,boolean rechercherPromotionEchelon) {
		NSTimestamp datePromotion = datePromotionPourParametreEtAnnee(parametrePromotion.parpType(), anneePromotion);
		NSArray<EOElementCarriere> elementsCarriere = rechercherElementsCarrierePourIndividuParametreEtDatePromotion(individu, parametrePromotion,datePromotion);
		if (elementsCarriere.size() == 0) {
			ResultatEvaluationAnciennete resultat = new ResultatEvaluationAnciennete(null);
			resultat.setStatutPromotion(STATUT_SUPPRIME);
			return resultat;
		}
		// On ne peut trouver qu'un seul élément de carrière valide à une date donnée, pas de chevauchement des éléments de carrière
		EOElementCarriere element = elementsCarriere.get(0);
		// 09/02/2010 - on vérifie si l'individu peut avoir une promotion d'échelon
		NSArray promouvables = null;
		if (rechercherPromotionEchelon) {
			NSTimestamp debutPeriode = DateCtrl.debutAnnee(anneePromotion);
			IndividuPromouvable individuP = IndividuPromouvable.individuPromouvablePourElementEtPeriode(individu.editingContext(), element, debutPeriode, datePromotion, null, true);
			if (individuP != null) {
				promouvables = new NSArray(individuP);
			}
		}
		return verifierConditionsPourElementParametreEtDatePromotion(element.editingContext(),element,parametrePromotion,datePromotion,promouvables,false);
	}
	/** Prepare la date de promotion en fonction du type de parametre de promotion : 01/01/AAAA pour liste d'aptitude, 31/12/AAAA pour les autres */
	public static NSTimestamp datePromotionPourParametreEtAnnee(String typeParametre, Integer annee) {
		if (typeParametre.equals(EOParamPromotion.LISTE_APTITUDE)) {
			return DateCtrl.debutAnnee(annee);
		} else {
			return DateCtrl.finAnnee(annee);
		}
	}
	/** Retourne les fiches d'anciennete correspondant aux criteres retenus 
	 * @param categorie categorie de population
	 * @param populationServicePublic type de population service public recherchee 
	 * si les deux sont nuls, on ne recherche que sur la carriere */
	public static NSArray<FicheAnciennete> fichesAnciennetePourCritere(NSArray<FicheAnciennete> fiches,String categorie,EOTypePopulation typePopulationServicePublic,boolean prendreEnCompteDureeServicePublic) {
		if (categorie == null && typePopulationServicePublic == null && prendreEnCompteDureeServicePublic) {
			return fiches;
		}
		NSMutableArray<FicheAnciennete> fichesAPrendreEnCompte = new NSMutableArray<FicheAnciennete>();
		String typePop = null;
		if (typePopulationServicePublic != null) {
			typePop = typePopulationServicePublic.code();
		}
		for (FicheAnciennete fiche : fiches) {
			if (categorie == null && typePopulationServicePublic == null) {
				// Ne garder que les fiches comportant des positions autre que CDD, MILITAIRE et passé hors EN
				if (fiche.position().equals(FicheAnciennete.CDD) == false && fiche.position().equals(FicheAnciennete.PASSE_HORS_EN) == false &&
						fiche.position().equals(FicheAnciennete.MILITAIRE) == false) {
					fichesAPrendreEnCompte.addObject(fiche);
				}
			} else {
				// garder toutes les fiches qui ont la même population ou qui ont une catégorie = à la catégorie attendue
				if ((typePop != null && fiche.typePopulation() != null && fiche.typePopulation().equals(typePop)) || 
						(categorie != null && fiche.categorie() != null && fiche.categorie().equals(categorie))){ 
					fichesAPrendreEnCompte.addObject(fiche);
				}
			}
		}
		return fichesAPrendreEnCompte;
	}

	// Recherche tous les éléments de carrière valides à la date de promotion qui respectent les conditions de corps départ ou grade départ
	// ou catégorie du corps
	private static NSArray<EOElementCarriere> rechercherElementsCarrierePourIndividuParametreEtDatePromotion(EOIndividu individu,EOParamPromotion parametrePromotion,NSTimestamp datePromotion) {

		EOQualifier qualifier = preparerQualifierPourParametreEtIndividu(parametrePromotion, individu, datePromotion);
		// Rechercher tous les éléments de carrière valides à cette date selon les critères retenus
		return EOElementCarriere.rechercherElementsAvecCriteres(parametrePromotion.editingContext(), qualifier, true, true);
	}

	/**
	 * 
	 * @param editingContext
	 * @param qualifierStatut
	 * @param parametresPromotion
	 * @param anneePromotion
	 * @return
	 */
	private static NSArray<EOHistoPromotion> rechercherPromotionsPourQualifierStatutParametresEtAnnee(EOEditingContext editingContext,EOQualifier qualifierStatut,NSArray parametresPromotion,Integer anneePromotion) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		if (qualifierStatut != null)
			qualifiers.addObject(qualifierStatut);
		else
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(HPRO_STATUT_KEY + " <> %@",new NSArray(STATUT_SUPPRIME)));

		if (parametresPromotion != null && parametresPromotion.count() > 0) {
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			for (java.util.Enumeration<EOParamPromotion> e1 = parametresPromotion.objectEnumerator();e1.hasMoreElements();) {
				EOParamPromotion parametre = e1.nextElement();
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PARAM_PROMOTION_KEY + " = %@", new NSArray(parametre)));
			}
			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		if (anneePromotion != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(HPRO_ANNEE_KEY + " = %@", new NSArray(anneePromotion)));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), SORT_ARRAY_NOM_ASC, new NSArray<String>(INDIVIDU_KEY));

	}
	
	/**
	 * 
	 * @param parametrePromotion
	 * @param individu
	 * @param datePromotion
	 * @return
	 */
	private static EOQualifier preparerQualifierPourParametreEtIndividu(EOParamPromotion parametrePromotion,EOIndividu individu,NSTimestamp datePromotion) {

		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TEM_VALIDE_KEY +"=%@", new NSArray(CocktailConstantes.VRAI)));

		if (individu != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TEM_PROVISOIRE_KEY + "=%@", new NSArray("N")));

		qualifiers.addObject(SuperFinder.qualifierPourPeriode(EOElementCarriere.DATE_DEBUT_KEY, datePromotion, EOElementCarriere.DATE_FIN_KEY, datePromotion));
		// On ne fait une recherche sur le type de population de départ que si le corps et le grade de départ sont nuls
		if (parametrePromotion.typePopulationDepart() != null && parametrePromotion.corpsDepart() == null && parametrePromotion.gradeDepart() == null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_CARRIERE_KEY + "." + EOCarriere.TO_TYPE_POPULATION_KEY + "=%@", new NSArray(parametrePromotion.typePopulationDepart())));
		}
		// On ne fait une recherche sur le corps de départ que si le grade de départ est nul
		if (parametrePromotion.corpsDepart() != null && parametrePromotion.gradeDepart() == null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_CORPS_KEY + "=%@", new NSArray(parametrePromotion.corpsDepart())));
		}

		if (parametrePromotion.gradeDepart() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_GRADE_KEY +"=%@", new NSArray(parametrePromotion.gradeDepart())));
		}

		if (parametrePromotion.cEchelon() != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.C_ECHELON_KEY +">=%@", new NSArray(parametrePromotion.cEchelon())));
		}

		if (parametrePromotion.cCategorieServPublics() != null || parametrePromotion.cCategorieServEffectifs() != null) {
			String categorie = parametrePromotion.cCategorieServPublics();
			if (categorie == null) {
				categorie = parametrePromotion.cCategorieServEffectifs();
			}
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_CORPS_KEY+"."+EOCorps.C_CATEGORIE_KEY + "=%@", new NSArray(categorie)));

		}
		return new EOAndQualifier(qualifiers);
	}

	/**
	 * 
	 * @param corps
	 * @param dateReference
	 * @return
	 */
	private static EOQualifier preparerQualifierPourCorps(EOCorps corps,NSTimestamp dateReference) {
		return  EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_CORPS_KEY + "=%@", new NSArray(corps));
	}


	/**
	 * 
	 * @param grade
	 * @param dateReference
	 * @return
	 */
	private static EOQualifier preparerQualifierPourGrade(EOGrade grade,NSTimestamp dateReference) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray gradesEquivalents = new NSMutableArray();
		rechercherGradesEquivalentsPourGrade(gradesEquivalents, grade);
		for (java.util.Enumeration<EOGrade> e = gradesEquivalents.objectEnumerator();e.hasMoreElements();) {
			EOGrade gradeCourant = e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.TO_GRADE_KEY + "=%@", new NSArray(gradeCourant)));
		}
		return new EOOrQualifier(qualifiers);
	}

	/**
	 * 
	 * @param gradesVus
	 * @param grade
	 */
	private static void rechercherGradesEquivalentsPourGrade(NSMutableArray gradesVus,EOGrade grade) {
		gradesVus.addObject(grade);
		NSArray<EOEquivAncGrade> gradesEquivalents = EOEquivAncGrade.equivalencesGradePourGrade(grade.editingContext(), grade, null);
		for (EOEquivAncGrade equivalence : gradesEquivalents) {
			if (equivalence.gradeEquivalent() != null && gradesVus.containsObject(equivalence.gradeEquivalent()) == false) {
				rechercherGradesEquivalentsPourGrade(gradesVus,equivalence.gradeEquivalent());
			}
		}
	}
	
	/**
	 * 
	 * @param promotions
	 * @param individu
	 * @return
	 */
	private static EOHistoPromotion rechercherPromotionPourIndividu(NSArray promotions,EOIndividu individu) {
		for (java.util.Enumeration<EOHistoPromotion> e = promotions.objectEnumerator();e.hasMoreElements();) {
			EOHistoPromotion promotion = e.nextElement();
			if (promotion.individu() == individu) {
				return promotion;
			}
		}
		return null;
	}

	
	/**
	 * 
	 * @param editingContext
	 * @param element
	 * @param parametrePromotion
	 * @param datePromotion
	 * @param individusPromouvables
	 * @param estModeAutomatique
	 * @return
	 */
	private static ResultatEvaluationAnciennete verifierConditionsPourElementParametreEtDatePromotion(EOEditingContext edc, EOElementCarriere element, 
			EOParamPromotion parametrePromotion, NSTimestamp datePromotion, NSArray individusPromouvables, boolean estModeAutomatique) {
		
		EOIndividu individu = element.toIndividu();
		LogManager.logDetail("individu : " + individu.identite());
		NSArray<EOChangementPosition> changementsPosition = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(edc, individu, datePromotion, datePromotion);
		// il n'y a qu'un seul changement de position car ils ne se chevauchent pas
		if (changementsPosition.count() == 0) {
			return null;
		} 
		Number droitAvancementGrade  = changementsPosition.get(0).toPosition().prctDroitAvctGrad();
		if (droitAvancementGrade == null || droitAvancementGrade.intValue() == 0) {
			return null;	// Pas de promotion possible pour ces positions
		}
		
		ResultatEvaluationAnciennete resultat = new ResultatEvaluationAnciennete(element.cEchelon());
		boolean peutChangerStatut = true;
		
		// Vérifier la durée dans le grade
		if (parametrePromotion.parpDureeCorps() != null) {
			// On calcule un service effectif
			String ancienneteCorps = calculerDureeCorpsPourIndividu(edc, individu, datePromotion, parametrePromotion.corpsDepart());
			resultat.setAncienneteCorps(ancienneteCorps);
			LogManager.logDetail("Vérification durée corps >= " + parametrePromotion.parpDureeCorps());
			LogManager.logDetail("Ancienneté corps : " + ancienneteCorps);
			int nbAnnees = extraireNbAnneesAnciennete(ancienneteCorps);
			if (nbAnnees < parametrePromotion.parpDureeCorps().intValue()) {
				LogManager.logDetail("durée réelle >= " + nbAnnees);
				peutChangerStatut = false;
			}		
		}
		String ancienneteGrade = calculerDureeCorpsGradeEchelonPourIndividu(edc, individu, datePromotion, null, parametrePromotion.gradeDepart(), null, false);
		if (parametrePromotion.gradeDepart() == null) {
			// On mettra dans l'ancienneté du grade, l'ancienneté du dernier grade
			LogManager.logDetail("Anciennete pour dernier grade : " + ancienneteGrade);
		} else {
			LogManager.logDetail("Anciennete pour grade : " + ancienneteGrade);
		}
		resultat.setAncienneteGrade(ancienneteGrade);
		// le statut devient automatique si tous les critères sont remplis sinon il est provisoire. On a identifié que l'individu
		// respectait les conditions de corps, grade ou catégorie départ
		// Vérifier durée grade ou corps
		if (parametrePromotion.parpDureeGrade() != null) {
			// Calculer la durée d'ancienneté dans le grade ou le corps
			LogManager.logDetail("Vérification durée grade >= " + parametrePromotion.parpDureeGrade());
			int nbAnnees = extraireNbAnneesAnciennete(ancienneteGrade);
			if (nbAnnees < parametrePromotion.parpDureeGrade().intValue()) {
				LogManager.logDetail("durée réelle >= " + nbAnnees);
				peutChangerStatut = false;
			}
		}
		// Si durée de service effectif sans catégorie de service effectif et sans type de population => il s'agit d'une durée de service effectif dans le grade
		if (peutChangerStatut && parametrePromotion.cCategorieServEffectifs() == null 
				&& parametrePromotion.typePopulationServPublics() == null 
				&& parametrePromotion.parpDureeServEffectifs() != null) {
			LogManager.logDetail("Vérification durée service effectif pour le grade " + parametrePromotion.parpDureeServEffectifs());
			String ancienneteSvcEffectif = calculerDureeCorpsGradeEchelonPourIndividu(edc, individu, datePromotion, null, parametrePromotion.gradeDepart(), null, true);
			LogManager.logDetail("Ancienneté service effectif pour grade : " + ancienneteSvcEffectif);
			resultat.setAncienneteServiceEffectif(ancienneteSvcEffectif);
			int nbAnnees = extraireNbAnneesAnciennete(ancienneteSvcEffectif);
			if (nbAnnees < parametrePromotion.parpDureeServEffectifs().intValue()) {
				LogManager.logDetail("durée réelle >= " + nbAnnees);
				peutChangerStatut = false;
			}
		}
		// Vérifier échelon
		if (parametrePromotion.cEchelon() != null && element.cEchelon() != null) {
			try {
				int echelonIndividu = new Integer(element.cEchelon()).intValue();
				int echelonMinimum = new Integer(parametrePromotion.cEchelon()).intValue();
				if (peutChangerStatut) {
					LogManager.logDetail("Vérification échelon : " + echelonMinimum);
					if (echelonIndividu < echelonMinimum) {
						// 09/02/2010 - Vérifier si il existe des promotions d'échelon et si l'idividu en fait partie
						if (individusPromouvables != null && individusPromouvables.count() > 0) {
							LogManager.logDetail("Vérification des promotions d'échelon");
							boolean found = false;
							for (java.util.Enumeration<IndividuPromouvable> e = individusPromouvables.objectEnumerator();e.hasMoreElements();) {
								IndividuPromouvable individuP = e.nextElement();
								if (individuP.individu() == element.toIndividu()) {
									int echelonSuivant = new Integer(individuP.echelonSuivant()).intValue();
									LogManager.logDetail("échelon après promotion : " + echelonSuivant);
									if (echelonSuivant < echelonMinimum) {
										LogManager.logDetail("individu non promouvable");
										peutChangerStatut = false;
									}
									found = true;
									break;
								}
							}
							if (!found) {
								LogManager.logDetail("Individu sans promotion d'échelon");
								peutChangerStatut = false;
							}
						} else {
							LogManager.logDetail("échelon réelle : " + echelonIndividu);
							peutChangerStatut = false;
						}
					}
				}
				if (parametrePromotion.parpDureeEchelon() != null) {
					String ancienneteEchelon = calculerDureeGradeEchelonPourIndividu(edc,individu,datePromotion,parametrePromotion.gradeDepart(),parametrePromotion.cEchelon());
					resultat.setAncienneteEchelon(ancienneteEchelon);
					LogManager.logDetail("Ancienneté échelon : " + ancienneteEchelon);
					if (peutChangerStatut && echelonIndividu == echelonMinimum) {
						// On ne vérifie la durée dans l'échelon que si c'est le même échelon, pour les échelons supérieurs la durée n'a pas de sens
						LogManager.logDetail("Vérification durée échelon : " + parametrePromotion.parpDureeEchelon());
						int nbAnnees = extraireNbAnneesAnciennete(ancienneteEchelon);
						if (nbAnnees < parametrePromotion.parpDureeEchelon().intValue()) {
							LogManager.logDetail("durée réelle >= " + nbAnnees);
							peutChangerStatut = false;
						}
					}
				} 
			} catch (Exception exc) {
				exc.printStackTrace();
				peutChangerStatut = false;
			}
		} 

		// Préparer les fiches d'ancienneté de l'individu, on les exploitera ensuite selon le contexte
		NSArray fichesAnciennete = FicheAnciennete.calculerAnciennete(edc, individu, datePromotion, true,true);
		String ancienneteService = calculerDureeServicesDansCategorieAvecFiches(fichesAnciennete,null,null,false);	// Durée générale de service public
		LogManager.logDetail("Anciennete pour service public : " + ancienneteService);
		resultat.setAncienneteService(ancienneteService);
		if (peutChangerStatut && parametrePromotion.parpDureeServPublics() != null)  {
			int nbAnnees = extraireNbAnneesAnciennete(ancienneteService);
			LogManager.logDetail("Vérification durée service public : " + parametrePromotion.parpDureeServPublics());
			if (nbAnnees < parametrePromotion.parpDureeServPublics().intValue()) {
				LogManager.logDetail("durée réelle >= " + nbAnnees);
				peutChangerStatut = false;
			}
		}
		// La durée de catégorie de service public utilise la catégorie de service public ou le type de population 
		// si cette dernière n'est pas utilisée pour les services effectifs
		boolean peutUtiliserParametre = parametrePromotion.cCategorieServPublics() != null || (parametrePromotion.typePopulationServPublics() != null && parametrePromotion.parpDureeServEffectifs() == null);
		if (parametrePromotion.parpDureeCategServPublics() != null && peutUtiliserParametre)  {
			LogManager.logDetail("Vérification durée catégorie service public : " + parametrePromotion.parpDureeCategServPublics());
			String anciennetePourCategorie = calculerDureeServicesDansCategorieAvecFiches(fichesAnciennete, parametrePromotion.cCategorieServPublics(),parametrePromotion.typePopulationServPublics(),false);
			resultat.setAncienneteCategorieSvcPublic(anciennetePourCategorie);
			if (peutChangerStatut) {
				LogManager.logDetail("Anciennete pour catégorie service public : " + anciennetePourCategorie);
				int nbAnnees = extraireNbAnneesAnciennete(anciennetePourCategorie);
				if (nbAnnees < parametrePromotion.parpDureeCategServPublics().intValue()) {
					LogManager.logDetail("durée réelle >= " + nbAnnees);
					peutChangerStatut = false;
				}
			}
		}
		// La durée de service effectif est calculée pour la catégorie de service effectif ou la population
		if (parametrePromotion.parpDureeServEffectifs() != null && (parametrePromotion.cCategorieServEffectifs() != null || parametrePromotion.typePopulationServPublics() != null))  {
			LogManager.logDetail("Vérification durée service effectif : " + parametrePromotion.parpDureeServEffectifs() + " pour le corps de catégorie " + parametrePromotion.cCategorieServEffectifs());
			String ancienneteSvcEffectif = calculerDureeServicesDansCategorieAvecFiches(fichesAnciennete,parametrePromotion.cCategorieServEffectifs(),parametrePromotion.typePopulationServPublics(),true);
			resultat.setAncienneteServiceEffectif(ancienneteSvcEffectif);
			LogManager.logDetail("Anciennete service effectif : " + ancienneteSvcEffectif);
			if (peutChangerStatut) {
				int nbAnnees = extraireNbAnneesAnciennete(ancienneteSvcEffectif);
				if (nbAnnees < parametrePromotion.parpDureeServEffectifs().intValue()) {
					LogManager.logDetail("durée réelle >= " + nbAnnees);
					peutChangerStatut = false;
				}
			}
		}
		String statut = "";
		if (peutChangerStatut) {
			if (estModeAutomatique) {
				statut = STATUT_AUTOMATIQUE;
			} else {
				statut = STATUT_MANUEL;
			}
		} else {
			statut = STATUT_PROVISOIRE;
		}
		resultat.setStatutPromotion(statut);
		return resultat;
	}
	
	private static String calculerDureeCorpsPourIndividu(EOEditingContext edc, EOIndividu individu, NSTimestamp datePromotion, EOCorps corps) {
		return calculerDureeCorpsGradeEchelonPourIndividu(edc, individu, datePromotion, corps, null, null, true);
	}
	
	private static String calculerDureeGradeEchelonPourIndividu(EOEditingContext edc, EOIndividu individu, NSTimestamp datePromotion, EOGrade grade, String echelon) {
		return calculerDureeCorpsGradeEchelonPourIndividu(edc, individu, datePromotion, null, grade, echelon, false);
	}

	//FIXME
	// Revoir cette methode de calcul a partir de la quotite des modalites de service et non de la quotite de position
	// Retirer les periodes de CLD pour lesquelles la duree est null
	// Pourquoi pas a partir de la table Absences ?
	private static String calculerDureeCorpsGradeEchelonPourIndividu(EOEditingContext edc, EOIndividu individu, 
			NSTimestamp datePromotion, EOCorps corps, EOGrade grade, String echelon, boolean estServiceEffectif) {
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>(EOElementCarriere.getQualifierValide());
		qualifiers.addObject(EOElementCarriere.getQualifierNonProvisoire());
		qualifiers.addObject(EOElementCarriere.getQualifierIndividu(individu));
		
		// On prend jour jour suivant pour être sûr d'avoir ceux qui se terminent à la date de promotion
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOElementCarriere.DATE_DEBUT_KEY + "<%@", 
				new NSArray(DateCtrl.jourSuivant(datePromotion))));
		if (corps != null) {
			// Préparer le qualifier en prenant en compte les équivalences de corps
			qualifiers.addObject(preparerQualifierPourCorps(corps,datePromotion));
		} 
		if (grade != null) {
			// Préparer le qualifier en prenant en compte les équivalences de grade
			qualifiers.addObject(preparerQualifierPourGrade(grade, datePromotion));
		}
		NSArray<EOElementCarriere> elements = EOElementCarriere.rechercherElementsAvecCriteres(edc, new EOAndQualifier(qualifiers), true, true);
		if (elements.size() > 0 && corps == null && grade == null) {
			// On ne retient que les éléments relevant du dernier grade
			NSMutableArray<EOElementCarriere> elementsRetenus = new NSMutableArray();
			// les éléments de carrière sont rangés par ordre décroissant
			EOElementCarriere element = elements.get(0);
			elementsRetenus.addObject(elements.get(0));
			// 04/03/2010 - on recherche les équivalences de grade
			NSMutableArray gradesEquivalents = new NSMutableArray();

			rechercherGradesEquivalentsPourGrade(gradesEquivalents, element.toGrade());

			for (int i = 1; i < elements.count();i++) {
				element = elements.get(i);
				if (gradesEquivalents.containsObject(element.toGrade())) {
					elementsRetenus.addObject(element);
				}
			}
			elements = new NSArray(elementsRetenus);
		}

		// Comme les élements de carrière ne se chevauchent pas, on peut additionner toutes les périodes
		int nbJours = 0;
		for (EOElementCarriere elementCarriere : elements) {

			NSTimestamp dateFin = datePromotion;
			if (elementCarriere.dateFin() != null && DateCtrl.isBefore(elementCarriere.dateFin(), datePromotion)) {
				dateFin = elementCarriere.dateFin();
			}
			// Vérifier si il faut prendre en compte l'échelon,que l'échelon de l'élément de carrière est bien >= à la valeur minimum
			boolean prendreEnCompteElement = (echelon == null);
			if (echelon != null && elementCarriere.cEchelon() != null) {
				try {
					int echelonNum = new Integer(echelon).intValue();
					int echelonReel = new Integer(elementCarriere.cEchelon()).intValue();
					prendreEnCompteElement = (echelonReel >= echelonNum);
				} catch (Exception exc) {}
			}
			if (prendreEnCompteElement) {
				if (estServiceEffectif) {	
					// Rechercher les changements de position pendant la période (ils sont classés par ordre croissant)
					NSArray<EOChangementPosition> changements = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(edc, individu, elementCarriere.dateDebut(), dateFin);
					//FIXME
					// On recherche les differentes modalites de service saisies pour enlever un nombre de jours à temps partiel (TP ou TPT)
					NSArray<EOModalitesService> modalites = EOModalitesService.rechercherPourIndividuEtPeriode(edc, individu, elementCarriere.dateDebut(), dateFin);

					float nbJoursChangement = 0;
					float nbJoursModalites = 0;
					for (EOChangementPosition changement : changements) {

						if (changement.carriere() != null && changement.carriere() == elementCarriere.toCarriere()) {
							// On ne prend en compte que les changements de position qui donnent droit à avancement
							Number droitAvancementGrade  = changement.toPosition().prctDroitAvctGrad();
							if (droitAvancementGrade != null && droitAvancementGrade.intValue() > 0) {
								NSTimestamp dateDebutChangement = changement.dateDebut();
								if (DateCtrl.isBefore(dateDebutChangement, elementCarriere.dateDebut())) {
									dateDebutChangement = elementCarriere.dateDebut();
								}
								NSTimestamp dateFinChangement = dateFin;
								if (changement.dateFin() != null && DateCtrl.isBefore(changement.dateFin(), dateFin)) {
									dateFinChangement = changement.dateFin();
								}
								float quotite = droitAvancementGrade.floatValue() / 100;
								// Prendre en compte la quotité de la position et la quotité d'avancement
								if (changement.quotite() != null) {
									quotite = quotite * (changement.quotite().floatValue() / 100);
								}
								nbJoursChangement += ((float)DateCtrl.nbJoursEntre(dateDebutChangement, dateFinChangement, true, true)) * quotite;
							}
						}
					}
					LogManager.logDetail("nbJoursChangement " + nbJoursChangement);

					nbJours += (int)nbJoursChangement;
				} else {
					int nbJoursElement = DateCtrl.nbJoursEntre(elementCarriere.dateDebut(), dateFin, true,true);
					LogManager.logDetail("debut " + elementCarriere.dateDebut() + "fin " + DateCtrl.dateToString(dateFin));
					LogManager.logDetail("nbJoursElement " + nbJoursElement);
					nbJours += nbJoursElement;
				}
				
				
			} 
		}

		if (nbJours > 0) {	
			// On calcule la date que cela représente dans le passé pour savoir ensuite le nombre d'années que cela représente
			NSTimestamp dateDebut = DateCtrl.dateAvecAjoutJours(datePromotion, -nbJours + 1);	// On veut que la date de fin soit incluse donc il faut supprimer 1 jour
			LogManager.logDetail(DateCtrl.dateToString(dateDebut));
			IntRef anneeRef = new DateCtrl.IntRef(),moisRef = new DateCtrl.IntRef(), jourRef = new DateCtrl.IntRef();
			DateCtrl.joursMoisAnneesEntre(dateDebut, datePromotion, anneeRef, moisRef, jourRef, true,true);

			// Préparer l'ancienneté courante pour stocker les valeurs

			return "" + anneeRef.value + " an(s) " + moisRef.value + " mois " + jourRef.value + " jour(s)";
		} else {
			return "";
		}
	}
	
	/**
	 * 
	 * @param fiches
	 * @param categorie
	 * @param typePopulation
	 * @param estServiceEffectif
	 * @return
	 */
	private static String calculerDureeServicesDansCategorieAvecFiches(NSArray fiches,String categorie,EOTypePopulation typePopulation,boolean estServiceEffectif) {
		NSArray fichesAPrendreEnCompte;
		if (categorie == null && typePopulation == null) {
			fichesAPrendreEnCompte = fiches;
		} else {
			fichesAPrendreEnCompte = fichesAnciennetePourCritere(fiches, categorie, typePopulation, false);
		}
		int typeAnciennete = FicheAnciennete.TOUTE_ANCIENNETE;
		if (estServiceEffectif) {
			typeAnciennete = FicheAnciennete.ANCIENNETE_TITULAIRE;
		}
		String texte = "Fiches prises en compte pour ";
		if (typePopulation != null) {
			texte += "type population : " + typePopulation + ", ";
		}
		if (categorie != null) {
			texte += "categorie : " + categorie + ", ";
		}
		if (estServiceEffectif) {
			texte += " service effectif";
		} else {
			texte += " tout type anciennete";
		}
		texte += "\n";
		LogManager.logDetail(texte + FicheAnciennete.formaterFichesAnciennetes(fichesAPrendreEnCompte));
		return FicheAnciennete.calculerTotal(fichesAPrendreEnCompte, typeAnciennete);	// String au format : X an(s), Y mois...
	}

	/**
	 * 
	 * @param anciennete
	 * @return
	 */
	private static int extraireNbAnneesAnciennete(String anciennete) {
		try {
			int index = anciennete.indexOf(" an");
			anciennete = anciennete.substring(0,index);
			return new Integer(anciennete).intValue();
		} catch (Exception e) {
			System.out.println("EOHistoPromotion.extraireNbAnneesAnciennete() " + anciennete );
			//e.printStackTrace();
			return 0;
		}
	}

	public static class ResultatEvaluationAnciennete {
		private String echelon;
		private String ancienneteCorps;
		private String ancienneteGrade;
		private String ancienneteEchelon;
		private String ancienneteService;
		private String ancienneteCategorieSvcPublic;
		private String ancienneteServiceEffectif;
		private String statutPromotion;

		public ResultatEvaluationAnciennete(String echelon) {
			this.echelon = echelon;
		}
		public String echelon() {
			return echelon;
		}
		public String statutPromotion() {
			return statutPromotion;
		}
		public void setStatutPromotion(String statutPromotion) {
			this.statutPromotion = statutPromotion;
		}
		public String ancienneteCorps() {
			return ancienneteCorps;
		}
		public void setAncienneteCorps(String ancienneteCorps) {
			this.ancienneteCorps = ancienneteCorps;
		}
		public String ancienneteGrade() {
			return ancienneteGrade;
		}
		public void setAncienneteGrade(String ancienneteGrade) {
			this.ancienneteGrade = ancienneteGrade;
		}
		public String ancienneteService() {
			return ancienneteService;
		}
		public void setAncienneteService(String ancienneteService) {
			this.ancienneteService = ancienneteService;
		}
		public String ancienneteEchelon() {
			return ancienneteEchelon;
		}
		public void setAncienneteEchelon(String ancienneteEchelon) {
			this.ancienneteEchelon = ancienneteEchelon;
		}
		public String ancienneteCategorieSvcPublic() {
			return ancienneteCategorieSvcPublic;
		}
		public void setAncienneteCategorieSvcPublic(String ancienneteCategorieSvcPublic) {
			this.ancienneteCategorieSvcPublic = ancienneteCategorieSvcPublic;
		}
		public String ancienneteServiceEffectif() {
			return ancienneteServiceEffectif;
		}
		public void setAncienneteServiceEffectif(String ancienneteServiceEffectif) {
			this.ancienneteServiceEffectif = ancienneteServiceEffectif;
		}
		
		/**
		 * 
		 */
		public String toString() {
			String temp =  "statut promotion " + statutPromotion();
			if (echelon() != null) {
				temp += ", echelon : " + echelon();
			}
			if (ancienneteCorps() != null) {
				temp += ", anc. corps : " + ancienneteCorps();
			}
			if (ancienneteGrade() != null) {
				temp += ", anc. grade : " + ancienneteGrade();
			}
			if (ancienneteEchelon() != null) {
				temp += ", anc. echelon : " + ancienneteEchelon();
			}
			if (ancienneteService() != null) {
				temp += ", anc. service : " + ancienneteService();
			}
			if (ancienneteCategorieSvcPublic() != null) {
				temp += ", anc. categ svc public : " + ancienneteCategorieSvcPublic();
			}
			if (ancienneteServiceEffectif() != null) {
				temp += ", anc. categ svc effectif : " + ancienneteServiceEffectif();
			}
			return temp;
		}
	}
}


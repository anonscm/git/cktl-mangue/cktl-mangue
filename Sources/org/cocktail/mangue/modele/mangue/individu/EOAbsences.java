//EOAbsences.java
//Created on Thu Mar 13 14:24:57  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.IDureePourIndividu;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.conges.EOCongeMaternite;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Modelise un evenement lie a un individu : conge, modalite de service, changement de position…
 * Correspond a une table denormalisee utilisee pour optimiser certaines fonctionnalites : gestion des conges,
 * requetes, gestion des listes electorales …
 */
public class EOAbsences extends _EOAbsences implements IDureePourIndividu {

	public static final String C_TYPE_EXCLUSION_DETA = "DETA";
	public static final String C_TYPE_EXCLUSION_DISP = "DISP";
	public static final String C_TYPE_EXCLUSION_SNAT = "SNAT";
	public static final String C_TYPE_EXCLUSION_CGPA = "CGPA";

	public static String MATIN = "am";
	public static String APRES_MIDI = "pm";

	public EOAbsences() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public String libelleTypeAbsence() {

		if ( toTypeAbsence().estCongeMaternite()) {

			EOCongeMaternite conge = EOCongeMaternite.rechercherCongePourAbsence(editingContext(), this);

			if ( conge != null )
				return conge.toTypeCgMatern().libelleLong();
		}

		return toTypeAbsence().libelleLong();

	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param typeAbsence
	 * @return
	 */
	public static EOAbsences creer(EOEditingContext ec, EOIndividu individu, EOTypeAbsence typeAbsence) {

		EOAbsences newObject = (EOAbsences) createAndInsertInstance(ec, EOAbsences.ENTITY_NAME);    
		newObject.initAvecTypeEtIndividu(typeAbsence, individu);

		if (typeAbsence.estDetachement())
			newObject.setCTypeExclusion(C_TYPE_EXCLUSION_DETA);
		if (typeAbsence.estDisponibilite())
			newObject.setCTypeExclusion(C_TYPE_EXCLUSION_DISP);
		if (typeAbsence.estServiceNational())
			newObject.setCTypeExclusion(C_TYPE_EXCLUSION_SNAT);
		if (typeAbsence.estCongeParental())
			newObject.setCTypeExclusion(C_TYPE_EXCLUSION_CGPA);

		return newObject;
	}

	/**
	 * 
	 * @param typeAbsence
	 * @param individu
	 */
	public void initAvecTypeEtIndividu(EOTypeAbsence typeAbsence,EOIndividu individu) {

		setToTypeAbsenceRelationship(typeAbsence);
		setToIndividuRelationship(individu);
		setAbsAmpmDebut(MATIN);
		setAbsAmpmFin(APRES_MIDI);
		setTemValide(CocktailConstantes.VRAI);
		
	}

	public void supprimerRelations() {
		setToTypeAbsenceRelationship(null);
		setToIndividuRelationship(null);
		setTypeExclusionRelationship(null);
	}
	
	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	
	public void setEstValide(boolean aBool) {
		setTemValide((aBool)?CocktailConstantes.VRAI : CocktailConstantes.FAUX);
	}
	
	public float calculerDureeAbsence() {
		if (toTypeAbsence().code().equals(EOTypeAbsence.TYPE_CONGE_GARDE_ENFANT)) {
			// seul type d'absence où on prend en compte les demi-journées
			return Duree.nbJoursEntre(dateDebut(),absAmpmDebut(),dateFin(),absAmpmFin());
		} else {
			return (float)DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true);
		}
	}
	
	/**
	 * 
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public float calculerDureeAbsencePourPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		// Restreindre les dates du congé aux dates de la période si elles les dépassent
		NSTimestamp dateDebutPourPeriode = null,dateFinPourPeriode = null;
		if (DateCtrl.isBefore(dateDebut(), debutPeriode)) {	// l'événement commence avant le début de la période
			dateDebutPourPeriode = debutPeriode;
		} else {
			dateDebutPourPeriode = dateDebut();
		}
		if (finPeriode == null) {
			if (dateFin() != null) {
				dateFinPourPeriode = dateFin();
			}
		} else { // Fin période non nulle
			if (dateFin() == null) {
				dateFinPourPeriode = finPeriode;
			} else if (DateCtrl.isAfter(dateFin(), finPeriode)) {	// l'événement se termine après la fin de la période
				dateFinPourPeriode = finPeriode;
			} else {
				dateFinPourPeriode = dateFin();
			}
		}
		if (toTypeAbsence().code().equals(EOTypeAbsence.TYPE_CONGE_GARDE_ENFANT)) {
			// seul type d'absence où on prend en compte les demi-journées
			return Duree.nbJoursEntre(dateDebutPourPeriode,absAmpmDebut(),dateFinPourPeriode,absAmpmFin());
		} else {
			return (float)DateCtrl.nbJoursEntre(dateDebutPourPeriode,dateFinPourPeriode,true);
		}
	}

	/**
	 * 
	 */
	public void modifierDureeAbsence() {
		float nbJours = calculerDureeAbsence();
		if (nbJours == 0) {
			setAbsDureeTotale("");
		} else {
			setAbsDureeTotale("" + nbJours);
		}
	}
	/** modifie la duree d'une absence */
	public void modifierDureeAbsence(float nbJours) {
		setAbsDureeTotale("" + nbJours);
	}
	public EOIndividu individu() {
		return toIndividu();
	}
	public boolean supportePlusieursTypesEvenement() {
		return false;
	}
	public String typeEvenement() {
		if (toTypeAbsence() != null) {
			return toTypeAbsence().code();
		} else {
			return "";
		}
	}

	/** Trouve les absences legales pour un individu entre deux periodes
	 * @param editingContext	editing context dans lequel effectuer le fetch
	 * @param individu
	 * @param debutPeriode debut periode
	 * @param finPeriode fin Periode
	 * @param toutTypeAbsence true si on veut les absences valides et invalides
	 * @return liste des absences trouvees
	 */
	public static NSArray<EOAbsences> rechercherAbsencesLegalesPourIndividuEtDates(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode,boolean toutTypeAbsence) {		

		try {

			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));			
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_TYPE_ABSENCE_KEY+"."+EOTypeAbsence.CONGE_LEGAL_KEY, "O"));

			if (finPeriode != null)
				andQualifiers.addObject(CocktailFinder.getQualifierBeforeEq(DATE_DEBUT_KEY, finPeriode));
			if (debutPeriode != null) 
				andQualifiers.addObject(CocktailFinder.getQualifierAfterEq(DATE_FIN_KEY, debutPeriode));

			return fetchAll(editingContext, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOAbsences>();
		}
	}
	
	/** Trouve les absences valides a declarer au Cir pour un individu entre deux periodes
	 * @param editingContext	editing context dans lequel effectuer le fetch
	 * @param individu
	 * @param debutPeriode debut periode
	 * @param finPeriode fin Periode
	 * @return liste des absences trouvees
	 */
	public static NSArray<EOAbsences> findAbsencesCirPourIndividuEtDates(EOEditingContext ec,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {		
		try {
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));			
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));			

			andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_ABSENCE_KEY+"."+EOTypeAbsence.TEM_CIR_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			if (finPeriode != null) {
				andQualifiers.addObject(CocktailFinder.getQualifierBeforeEq(DATE_DEBUT_KEY, finPeriode));
			}
			if (debutPeriode != null) {	
				andQualifiers.addObject(CocktailFinder.getQualifierAfterEq(DATE_FIN_KEY, debutPeriode));
			}

			return fetchAll(ec, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return null;
		}
	}

	/** Trouve les absences lesgales pour un individu entre deux periodes
	 * @param editingContext	editing context dans lequel effectuer le fetch
	 * @param individu
	 * @param debutPeriode debut periode
	 * @param finPeriode fin Periode 
	 * @return liste des absences trouvees
	 */
	public static NSArray<EOAbsences> rechercherAbsencesLegalesPourIndividuEtDates(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {		
		return rechercherAbsencesLegalesPourIndividuEtDates(editingContext,individu,debutPeriode,finPeriode,true);
	}

	/** Trouve les absences correspondant a aux types de conges fournis pour un individu entre deux periodes, valides ou toutes
	 * @param editingContext	editing context dans lequel effectuer le fetch
	 * @param individu
	 * @param debutPeriode debut periode
	 * @param finPeriode fin Periode
	 * @param typesAbsence tableau de String contenant les types d'absences - peut etre nul ou vide
	 * @param toutTypeAbsence true si on veut les absences valides et invalides
	 * @return liste des absences trouvees
	 */
	public static NSArray<EOAbsences> rechercherAbsencesPourIndividuDatesEtTypesAbsence(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode,NSArray typesAbsence) {

		try {
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));			
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));			

			EOQualifier qualifier = CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode);
			if (qualifier != null)
				andQualifiers.addObject(qualifier);

			if (typesAbsence != null && typesAbsence.size() > 0)
				andQualifiers.addObject(SuperFinder.construireORQualifier(TO_TYPE_ABSENCE_KEY + "." + EOTypeAbsence.CODE_KEY, typesAbsence));

			return fetchAll(edc, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOAbsences>();
		}
	}

	/** Trouve les absences correspondant a un certain type de conge pour un individu entre deux periodes, valides ou toutes
	 * @param editingContext	editing context dans lequel effectuer le fetch
	 * @param individu
	 * @param debutPeriode debut periode
	 * @param finPeriode fin Periode
	 * @param typeAbsence String
	 * @param toutTypeAbsence true si on veut les absences valides et invalides
	 * @return liste des absences trouvees
	 */
	public static NSArray<EOAbsences> rechercherAbsencesPourIndividuDatesEtTypeAbsence(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode, String typeAbsence) {
		return rechercherAbsencesPourIndividuDatesEtTypesAbsence(edc, individu, debutPeriode, finPeriode, new NSArray(typeAbsence));
	}
	
	/** Trouve l'absence d'un certain type pour un individu avec les dates identiques a celles passees en parametre
	 * @param editingContext	editing context dans lequel effectuer le fetch
	 * @param individu
	 * @param dateDebut 
	 * @param dateFin peut etre nulle. On recupere alors la premiere absence trouvee
	 * @return liste des absences trouvees
	 */
	public static EOAbsences rechercherAbsencePourIndividuDateEtTypeAbsence(EOEditingContext edc, EOIndividu individu, NSTimestamp dateDebut, NSTimestamp dateFin, EOTypeAbsence typeAbsence) {

		try {
			if (typeAbsence == null || individu == null || dateDebut == null)
				return null;

			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));			
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));			
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_TYPE_ABSENCE_KEY, typeAbsence));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(DATE_DEBUT_KEY, dateDebut));
			if (dateFin != null) {
				andQualifiers.addObject(CocktailFinder.getQualifierEqual(DATE_FIN_KEY, dateFin));
			}

			return fetchFirstByQualifier(edc, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);

		} catch (Exception e) {
			return null;
		}
	}

	/** Trouve l'absence avec un certain type d'exclusion pour un individu a la date passee en parametre
	 * @param editingContext	editing context dans lequel effectuer le fetch
	 * @param individu
	 * @param dateReference 
	 * @param typeExclusion
	 * @return liste des absences trouvees
	 */
	public static NSArray<EOAbsences> rechercherAbsencePourIndividuDateEtTypeExclusion(EOEditingContext edc, EOIndividu individu, NSTimestamp dateReference, EOTypeExclusion typeExclusion) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));			
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));			
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TYPE_EXCLUSION_KEY, typeExclusion));
		andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY, dateReference, DATE_FIN_KEY, dateReference));

		return fetchAll(edc, new EOAndQualifier(andQualifiers), PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT_DESC);
	}

	/** Recherche des absences valides d'un individu pendant une periode donnee 
	 * @param individu
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 */
	public static NSArray<EOAbsences> rechercherAbsencesPourIndividuEtPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));			
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));			
		andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));

		return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
	}
	
	/** Recherche des evenements valides d'un individu pendant une periode donnee 
	 * @param individu
	 * @param debutPeriode debut periode
	 * @param finPeriode fin periode (peut etre nulle) 
	 */
	public static NSArray<EOAbsences> rechercherEvenementsPourIndividuEtPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));			
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));			
		andQualifiers.addObject(CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));

		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_ABSENCE_KEY+"."+EOTypeAbsence.CODE_HARPEGE_KEY + " != nil", null));

		return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
	}
	
	/**
	 * Recherche les absences valides ayant un des types passes en parametre
	 * @param editingContext
	 * @param debutPeriode
	 * @param finPeriode	peut etre nulle
	 * @param types			peut etre nul
	 * @return
	 */
	public static NSArray<EOAbsences> findForPeriodeEtTypes(EOEditingContext editingContext, NSTimestamp debutPeriode, NSTimestamp finPeriode, NSArray types) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));			
		andQualifiers.addObject( SuperFinder.construireORQualifier(TO_TYPE_ABSENCE_KEY + "." + EOTypeAbsence.CODE_KEY, types));
		andQualifiers.addObject( CocktailFinder.getQualifierForPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));

		return fetchAll(editingContext, new EOAndQualifier(andQualifiers), null);
	}

	/** recherche les absences d'un individu commencant apres la date fournie en parametre */
	public static NSArray<EOAbsences> absencesPosterieuresADate(EOEditingContext edc, EOIndividu individu, NSTimestamp date) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));			
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O"));			
		andQualifiers.addObject(CocktailFinder.getQualifierAfter(DATE_DEBUT_KEY, date));
		
		return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
	}

	/** ferme les affectations d'un agent valides a la date passee en parametre  */
	public static void fermerAbsencesSaufAbsencesDepart(EOEditingContext edc, EOIndividu individu, NSTimestamp date) {

		NSArray<EOAbsences> objets = rechercherAbsencesPourIndividuEtPeriode(edc, individu, date, date);

		// fermer l'absence
		for (EOAbsences absence : objets) {
			if (absence.typeEvenement().equals(EOTypeAbsence.TYPE_DEPART) == false) {
				absence.setDateFin(date);
				absence.modifierDureeAbsence();
			}
		}
		// supprimer les absences postérieures à cette date qui ne sont pas liées à des événements
		// comme les stages ou des changements de position car en ce cas il faut les invalider
		objets = absencesPosterieuresADate(edc, individu, date);
		for (EOAbsences absence : objets) {
			if (absence.typeEvenement().equals(EOTypeAbsence.TYPE_DEPART) == false) {
				boolean invaliderAbsence = absence.typeEvenement().equals("STAGE");
				if (absence.typeEvenement().equals("") == false && !invaliderAbsence) {
					// Vérifier si il ne s'agirait pas d'une position
					EOGenericRecord position = SuperFinder.rechercherObjetAvecAttributEtValeurEgale(edc, EOPosition.ENTITY_NAME, INomenclature.CODE_KEY, absence.typeEvenement());
					if (position != null)
						invaliderAbsence = true;
				}
				if (invaliderAbsence)
					absence.setEstValide(false);
				else {
					absence.supprimerRelations(); 
					edc.deleteObject(absence);
				}
			}
		}
	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (toTypeAbsence() == null)
			throw new NSValidation.ValidationException("Le type d'absence n'est pas défini !");
		if (dateDebut() == null)
			throw new NSValidation.ValidationException("Une absence (Evènement) de type " + toTypeAbsence().libelleCourt() + " doit avoir une date de début !");

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

}
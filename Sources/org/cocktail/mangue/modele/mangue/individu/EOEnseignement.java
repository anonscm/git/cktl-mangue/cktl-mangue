// EOEnseignement.java
// Created on Wed Dec 22 14:03:23 Europe/Paris 2010 by Apple EOModeler Version 5.2

package org.cocktail.mangue.modele.mangue.individu;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.IntervalleTempsComparator;
import org.cocktail.mangue.common.utilities.Utilitaires;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

/** Heures d'enseignement complementaire pour les Biatss ou les contractuels enseignants du 1er ou 2nd degre.<BR>
 * Regles de validation : l'individu doit etre fourni et etre un BIATSS ou un enseignant pendant toute la periode */
public class EOEnseignement extends _EOEnseignement {
	
	public static NSArray SORT_DEBUT_DESC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));
	public static NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(SORT_DEBUT_DESC);
	
	/** Type population d'un enseignant du 1er degre */
	public final static String ENSEIGNANT_DEGRE_1 = "DC";
	/** Type population d'un enseignant du 2nd degre */
	public final static String ENSEIGNANT_DEGRE_2 = "DA";
	public EOEnseignement() {
		super();
	}

	public static EOEnseignement creer(EOEditingContext ec, EOIndividu individu) {
		EOEnseignement newObject = (EOEnseignement) createAndInsertInstance(ec,ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		newObject.setNbHEnseignement(new BigDecimal(0));
		return newObject;
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		if (structure() != null) {
			setStructureRelationship(null);
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {

		super.validateForSave();
		
		if (dateFin() == null)
			throw new NSValidation.ValidationException("La date de fin doit être définie !");

		// Vérifier si le nombre d'heures complémentaires est positif ou null
		if (nbHEnseignement() != null) {
			if (nbHEnseignement().doubleValue() < 0) {
				throw new NSValidation.ValidationException("Le nombre d'heures doit être un nombre positif !");
			}
		}
		
		// Vérifier si l'individu est un biatoss sur toute la période
		// On recherche les segments de carrière et avenants valides sur la période et on construit des intervalles temps
		// pour tous les segments/avenants de Iatoss
		NSMutableArray intervallesTemps = new NSMutableArray();
		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(), individu(), dateDebut(), dateFin());
		carrieres = EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		for (java.util.Enumeration<EOCarriere> e  = carrieres.objectEnumerator();e.hasMoreElements();) {
			EOCarriere carriere = e.nextElement();
			if (carriere.toTypePopulation().estAtos() || carriere.toTypePopulation().estItarf() ) {
				intervallesTemps.addObject(new Utilitaires.IntervalleTemps(carriere.dateDebut(), carriere.dateFin()));
			} 
		}
		NSArray<EOContratAvenant> avenants = EOContratAvenant.rechercherAvenantsRemunerationPrincipalePourIndividuEtDates(editingContext(), individu(), dateDebut(), dateFin());
		avenants = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenants, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		for (Enumeration<EOContratAvenant> e1  = avenants.objectEnumerator();e1.hasMoreElements();) {
			EOContratAvenant avenant = e1.nextElement();
			if (avenant.toGrade() != null && avenant.toGrade().toCorps() != null && avenant.toGrade().toCorps().toTypePopulation() != null && 
					(	avenant.toGrade().toCorps().toTypePopulation().estAtos() 
							|| avenant.toGrade().toCorps().toTypePopulation().estItarf() 
							|| avenant.toGrade().toCorps().toTypePopulation().code().equals(ENSEIGNANT_DEGRE_1) ||
							avenant.toGrade().toCorps().toTypePopulation().code().equals(ENSEIGNANT_DEGRE_2))) {
				// L'individu est IATOSS ou contractuel enseignant
				intervallesTemps.addObject(new Utilitaires.IntervalleTemps(avenant.dateDebut(),avenant.dateFin()));
			}
		}
		// Trier les intervalles de temps
		try {
			intervallesTemps.sortedArrayUsingComparator(new IntervalleTempsComparator());
			// Vérifier si toute la période est couverte
			NSTimestamp dateDebut = dateDebut();
			boolean estPeriodeCouverte = false;
			for (Enumeration<Utilitaires.IntervalleTemps> e2 = intervallesTemps.objectEnumerator();e2.hasMoreElements();) {
				Utilitaires.IntervalleTemps intervalle = e2.nextElement();
				if (DateCtrl.isAfter(intervalle.debutPeriode(), dateDebut)) {
					// Toute la période n'est pas couverte, on peut interrompre la vérification
					break;
				} else {
					if (intervalle.finPeriode() != null) {
						dateDebut = DateCtrl.jourSuivant(intervalle.finPeriode());
					} else {
						dateDebut = null;
					}
				}
				if (dateDebut == null || (dateFin() != null && DateCtrl.isAfterEq(dateDebut, dateFin()))) {
					// On a couvert toute la période
					estPeriodeCouverte = true;
					break;
				}
				if (intervalle.finPeriode() == null) {
					// On a couvert toute la période
					estPeriodeCouverte = true;
					break;
				}
			}
			if (estPeriodeCouverte == false)  {
				throw new NSValidation.ValidationException("L'individu n'est pas ITRF ou contractuel enseignant 1er ou 2nd degré sur toute la période");
			}
		} catch (Exception exc) {
			if (exc instanceof NSValidation.ValidationException == false) {
				LogManager.logException(exc);
			} else {
				throw (NSValidation.ValidationException)exc;
			}
		}
		
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		
			
	}
	// Méthodes protégées
	protected void init() {
	}
	// Méthodes statiques
	public static NSArray rechercherPourIndividu(EOEditingContext ec,EOIndividu individu) {
		try {
			NSMutableArray args = new NSMutableArray(individu);
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@",args);
			return fetchAll(ec,myQualifier, SORT_ARRAY_DEBUT_DESC );
		}
		catch (Exception e) {
			return new NSArray();
		}
	}
}

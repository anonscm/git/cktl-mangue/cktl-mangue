// EOCgFinActivite.java
// Created on Fri Mar 17 13:10:06 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOPosition;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Un cong&eacute; de fin d'activit&eacute; ne peut se prendre que si le changement de position pour les m&ecirc;mes dates est un CFA */
// 18/11/2010 - ajout d'une méthode statique pour déterminer un CFA
public class EOCgFinActivite extends _EOCgFinActivite {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public EOCgFinActivite() {
        super();
    }
    // méthodes ajoutées
    public String typeEvenement() {
    		return EOPosition.CODE_POSITION_EN_ACTIVITE;
    }
    public void validateForSave() throws NSValidation.ValidationException {
    	super.validateForSave();
    	NSArray changements = EOChangementPosition.rechercherChangementsPourIndividuEtPeriode(editingContext(), individu(), dateDebut(), dateFin());
    	// les changements sont classés par ordre de début croissante
    	if (changements.count() == 0) {
    		throw new NSValidation.ValidationException("Pas de changement de position pour cette période");
    	}
    	NSTimestamp dateDebut = null, dateFin = null;
    	// Vérifier que les dates coïncident avec une posittion d'activite
    	for (java.util.Enumeration<EOChangementPosition> e = changements.objectEnumerator();e.hasMoreElements();) {
    		EOChangementPosition changement = e.nextElement();
    		if (changement.toPosition().code().equals(typeEvenement()) == false) {
    			throw new NSValidation.ValidationException("Un CFA s'appuie sur un changement de position \"" + typeEvenement() + "\"");
    		}
    		if (dateDebut == null) {
    			dateDebut = changement.dateDebut();
    		}
    		if (changement == changements.lastObject()) {
    			dateFin = changement.dateFin();
    		}
    	}
//    	if (DateCtrl.isSameDay(dateDebut(), dateDebut) == false) {
//    		throw new NSValidation.ValidationException("La date de début de ce congé ne correspond pas à la date de début du changement de position en CFA");
//    	}
//    	if ((dateFin == null && dateFin() != null) || (dateFin != null && dateFin() == null) || DateCtrl.isSameDay(dateFin(), dateFin) == false) {
//    		throw new NSValidation.ValidationException("La date de fin de ce congé ne correspond pas à la date de fin du changement de position en CFA");
//    	}
    }
    
    //  méthodes protégées
	protected void init() {
		// pas d'initialisation spécifique
	}
	// Méthodes statiques
	// 18/11/2010 - recherche d'un CFA
	public static EOCgFinActivite rechercherCongePourIndividuEtDates(EOEditingContext editingContext, EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("individu = %@", new NSArray(individu)));
		// On recherche les dates sur < jour précédent ou > jour suivant pour éviter les problèmes de GMT
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateDebut > %@", new NSArray(DateCtrl.jourPrecedent(dateDebut))));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateDebut < %@", new NSArray(DateCtrl.jourSuivant(dateDebut))));
		if (dateFin != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateFin > %@", new NSArray(DateCtrl.jourPrecedent(dateFin))));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateFin < %@", new NSArray(DateCtrl.jourSuivant(dateFin))));
		}
		EOFetchSpecification fs = new EOFetchSpecification("CgFinActivite",new EOAndQualifier(qualifiers),null);
		try {
			return (EOCgFinActivite)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return  null;
		}
	}
}

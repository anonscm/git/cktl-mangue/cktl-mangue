/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.IValidite;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOStage extends _EOStage implements IValidite  {

	public static final EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static final EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_DATE_DEBUT_DESC = new NSArray(SORT_DATE_DEBUT_DESC);
	public static final NSArray SORT_ARRAY_DATE_DEBUT_ASC = new NSArray(SORT_DATE_DEBUT_ASC);

	private final static String TYPE_EVENEMENT = "STAGE";

	public EOStage() {
		super();
	}

	public String typeEvenement() {
		return TYPE_EVENEMENT;
	}
	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}  
	public String dateTitularisationFormatee() {
		return SuperFinder.dateFormatee(this,DATE_TITULARISATION_KEY);
	}
	public void setDateTitularisationFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,DATE_TITULARISATION_KEY,uneDate);
	}
	public boolean estUnRenouvellement() {
		return temRenouvellement() != null && temRenouvellement().equals(CocktailConstantes.VRAI);
	}
	public void setEstUnRenouvellement(boolean aBool) {
		if (aBool) {
			setTemRenouvellement(CocktailConstantes.VRAI);
		} else {
			setTemRenouvellement(CocktailConstantes.FAUX);
		}
	}

	/** retourne true si le stage est renouvele */
	public boolean estRenouvele() {

		if (carriere().stages().count() == 0)
			return false;

		if (dateDebut() == null)
			return false;

		// trier les stages par date début croissantes
		NSArray<EOStage> stages = EOSortOrdering.sortedArrayUsingKeyOrderArray(carriere().stages(), SORT_ARRAY_DATE_DEBUT_ASC);
		boolean takeNext = false,estRenouvele = false;
		for (EOStage myStage : stages) {
			if (myStage.estValide()) {
				if (takeNext) {
					if (myStage.corps() == corps() && myStage.estUnRenouvellement()); {
						estRenouvele = true;
						break;
					}
				} else 
					if (myStage == this)
						takeNext = true;

			}			
		}
		return estRenouvele;
	}

	/** retourne le stage &agrave; l'origine du renouvellement */
	public EOStage stageOrigineRenouvellement() {
		if (carriere().stages().count() == 0) {
			return null;
		}
		// trier les stages par date début décroissante
		NSArray<EOStage> stages = EOSortOrdering.sortedArrayUsingKeyOrderArray(carriere().stages(),SORT_ARRAY_DATE_DEBUT_DESC);
		boolean takeNext = false;
		for (EOStage myStage : stages) {
			if (myStage.estValide()) {
				if (takeNext) {
					if (myStage.corps() == corps() && myStage.estUnRenouvellement())
						return myStage;
				} else if (myStage == this)
					takeNext = true;

			}
		}
		return null;
	}

	public void awakeFromInsertion(EOEditingContext ed) {
		setEstValide(true);
	}

	/** methode a invoquer en cas de creation */
	public void initAvecCarriere(EOCarriere carriere) {
		initAvecCarriereCorpsEtDate(carriere,null,null);
	}
	/** methode a invoquer en cas de renouvellement */
	public void initAvecCarriereCorpsEtDate(EOCarriere carriere,EOCorps corps,NSTimestamp dateDebut) {

		setEstValide(true);

		if (carriere != null) {
			setCarriereRelationship(carriere);
			if (carriere.toIndividu() != null)
				setIndividuRelationship(carriere.toIndividu());

		}
		if (corps != null)
			setCorpsRelationship(corps);

		if (dateDebut != null)
			setDateDebut(dateDebut);

		if (carriere != null && corps != null && dateDebut != null)
			setTemRenouvellement(CocktailConstantes.VRAI);
		else
			setTemRenouvellement(CocktailConstantes.FAUX);
	}

	/** invalide le stage et l'evenement associe */
	public void invalider() {		
		setEstValide(false);
		if (absence() != null)
			absence().setEstValide(false);
	}

	public void validateForSave() throws NSValidation.ValidationException {
		// Pour les stages invalides, ne pas effectuer les verifications, elles ont ete faites lors de la precedente modification
		if (!estValide())
			return;

		super.validateForSave();	// pour une partie de la gestion des dates début et date fin
		if (corps() == null)
			throw new NSValidation.ValidationException("Le corps est obligatoire");

		if (DateCtrl.isBefore(dateDebut(),carriere().dateDebut()))			
			throw new NSValidation.ValidationException("La date de début doit être postérieure à celle du segment de carrière");

		if (dateFin() == null) {
			if (carriere().dateFin() != null) {
				throw new NSValidation.ValidationException("Vous devez fournir une date de fin car le segment de carrière en a une");
			}
			if (dateTitularisation() != null) {
				throw new NSValidation.ValidationException("Vous ne pouvez pas fournir une date de titularisation sans date de fin de stage");
			}
		} else {
			if (DateCtrl.isBefore(dateFin(),dateDebut())) {
				throw new NSValidation.ValidationException("STAGE\nLa date de fin doit être postérieure à la date de début");
			}
			// vérifier que la date de fin est antérieure à la date de fin du segment
			if (carriere().dateFin() != null && DateCtrl.isAfter(dateFin(),carriere().dateFin())) {
				throw new NSValidation.ValidationException("Pour un stage, la date de fin doit être antérieure à la date du segment de carrière");
			}
		}

		// verifier si la position pour la periode autorise l'acces au stage
		NSArray changements = carriere().changementsPositionPourPeriode(dateDebut(),dateFin());
		if (changements == null || changements.count() == 0) {
			throw new NSValidation.ValidationException("Pas de changement de position pour cette période");
		}
		// Verifier si la position et le motif position sont compatibles avec un stage
		for (java.util.Enumeration<EOChangementPosition> e = changements.objectEnumerator();e.hasMoreElements();) {
			EOChangementPosition changement = e.nextElement();
			if (changement.peutSuivreStage() == false)
				throw new NSValidation.ValidationException("Pas de stage possible pendant ces dates car l'agent est en " + changement.toPosition().libelleCourt());

			if (changement.toPosition().nonValidePourStagiaire())
				throw new NSValidation.ValidationException("La position " + changement.toPosition().libelleLong() + " pendant cette période, ne permet pas d'effectuer des stages");

		}

		if (dateTitularisation() != null) {

			//			if (estRenouvele())
			//				throw new NSValidation.ValidationException("Le stage est renouvelé, vous ne pouvez pas fournir une date de titularisation");

			if (DateCtrl.isBefore(dateTitularisation(),dateFin()))
				throw new NSValidation.ValidationException("La date de titularisation ne peut pas être antérieure à la date de fin");

		}

		// pas de chevauchement de stages
		NSArray stages = carriere().stagesPourPeriode(dateDebut(),dateFin());
		if (stages != null && stages.count() > 1)
			throw new NSValidation.ValidationException("Il existe déjà au moins un stage pendant cette période");

	}

	protected void init() {
		setEstValide(true);
	}


	/**
	 * 
	 * @param editingContext
	 * @param carriere
	 * @return
	 */
	public  static NSArray<EOStage> findForCarriere(EOEditingContext editingContext, EOCarriere carriere) {
		
		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(carriere.toIndividu())));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CARRIERE_KEY + "=%@", new NSArray(carriere)));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * Stages effectues par un individu sur une periode donnee.
	 * 
	 * @param editingContext
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @param forceRefresh
	 * @return
	 */
	public  static NSArray<EOStage> findForIndividuEtPeriode(EOEditingContext editingContext, EOIndividu individu,NSTimestamp debutPeriode, NSTimestamp finPeriode, boolean forceRefresh) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));

			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode,DATE_FIN_KEY,finPeriode));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
	}


	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

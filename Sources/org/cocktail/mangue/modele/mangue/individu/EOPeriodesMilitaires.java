// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   EOPeriodesMilitaires.java

package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypePeriodeMilit;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOPeriodesMilitaires extends _EOPeriodesMilitaires {
	
	public static NSArray SORT_DEBUT_ASC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));
	public static NSArray SORT_DEBUT_DESC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));

	public EOPeriodesMilitaires()
	{
	}

	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,DATE_DEBUT_KEY);
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,DATE_FIN_KEY);
	}
	public String typePeriodeOnp() {
		return toTypePeriode().codeOnp();
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOPeriodesMilitaires creer(EOEditingContext ec, EOIndividu individu) {
		EOPeriodesMilitaires newObject = (EOPeriodesMilitaires) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.setIndividuRelationship(individu);
		newObject.setTemValide("O");
		return newObject;		
	}

	/**
	 * 
	 */
	protected void init() {
		setTemValide("O");
	}

	/** Recherche les periodes militaires pour un individu  commencant avant une date donnee */
	public static NSArray<EOPeriodesMilitaires> findForIndividu(EOEditingContext ec, EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray("O")));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu)));

			return fetchAll(ec, new EOAndQualifier(qualifiers), null);
		}
		catch (Exception e) {
			return new NSArray<EOPeriodesMilitaires>();			
		}
	}


	/** Recherche les periodes militaires pour un individu  commencant avant une date donnee */
	public static NSArray<EOPeriodesMilitaires> findForIndividuAnterieuresADate(EOEditingContext ec, EOIndividu individu, NSTimestamp date) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " <= %@", new NSArray(date)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray("O")));

			// On 
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_PERIODE_KEY+"."+EOTypePeriodeMilit.CODE_ONP_KEY + " != nil", null));

			return fetchAll(ec, new EOAndQualifier(qualifiers), null);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();			
		}

	}
	
    public static NSArray<EOPeriodesMilitaires> rechercherPeriodesPourIndividuEtDates(EOEditingContext editingContext, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode)
    {
        try
        {
            NSMutableArray qualifiers = new NSMutableArray();
            
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " = %@", new NSArray(individu)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray("O")));

			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, debutPeriode, DATE_FIN_KEY, finPeriode));

            return fetchAll(editingContext, new EOAndQualifier(qualifiers), new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
        }
        catch(Exception e)
        {
            return new NSArray<EOPeriodesMilitaires>();
        }
    }

    

	public void validateForInsert()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
		super.validateForDelete();
	}

	public void validateObjectMetier()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
		
		if (dateFin() == null)
			throw new ValidationException("Veuillez saisir une date de fin pour cette période !");

		if (toTypePeriode() == null)
			throw new ValidationException("Veuillez entrer un type de période militaire !");

	}

	public void validateBeforeTransactionSave()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
	}
}

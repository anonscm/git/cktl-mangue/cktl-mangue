/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.individu;

import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOVacatairesAffectation extends _EOVacatairesAffectation {

	public EOVacatairesAffectation() {
		super();
	}

	public boolean estPrincipale() {
		return temPrincipale() != null && temPrincipale().equals(CocktailConstantes.VRAI);
	}
	public void setEstPrincipale(boolean aBool) {
		if (aBool) {
			setTemPrincipale(CocktailConstantes.VRAI);
		} else {
			setTemPrincipale(CocktailConstantes.FAUX);
		}
	}

	/**
	 * 
	 * @param ec
	 * @param vacataire
	 * @return
	 */
	public static EOVacatairesAffectation creer(EOEditingContext ec, EOVacataires vacataire) {

		EOVacatairesAffectation newObject = (EOVacatairesAffectation) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setToVacataireRelationship(vacataire);
		
		return newObject;

	}

	/**
	 * 
	 * @param ec
	 * @param vacation
	 * @return
	 */
	public static NSArray<EOVacatairesAffectation> fetchForVacation(EOEditingContext ec, EOVacataires vacation) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_VACATAIRE_KEY +" = %@", new NSArray<EOVacataires>(vacation)));		
		return fetchAll(ec, new EOAndQualifier(qualifiers), null);

	}

	public void validateForSave() throws NSValidation.ValidationException {

		if (toStructure() == null)
			throw new NSValidation.ValidationException("La structure d'affectation est obligatoire !");

		if (nbrHeures() == null || nbrHeures().floatValue() == 0)			
			throw new NSValidation.ValidationException("Le nombre d'heures de vacations doit être renseigné !");
	}

}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.individu.budget;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/** Modelise les objets metier liees a un persBudget.<BR>
 * Regles de validation :<BR>
 * le budget pour l'individu doit etre fourni<BR>
 * 0 < pourcentage <= 100<BR>
 * le pourcentage de toutes les actions liees au budget doit etre inferieur <= 100%
 * @author christine
 *
 */
public abstract class DonneePourBudget extends EOGenericRecord {
	public Number pourcentage() {
		return (Number)storedValueForKey("pourcentage");
	}

	public void setPourcentage(Number value) {
		takeStoredValueForKey(value, "pourcentage");
	}

	public org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudget persBudget() {
		return (org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudget)storedValueForKey(EOPersBudgetAction.PERS_BUDGET_KEY);
	}

	public void setPersBudget(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudget value) {
		takeStoredValueForKey(value, EOPersBudgetAction.PERS_BUDGET_KEY);
	}
	// Méthodes ajoutées
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(persBudget(),EOPersBudgetAction.PERS_BUDGET_KEY);
	}
	public void validateForSave() {
		if (persBudget() == null) {
			throw new NSValidation.ValidationException("Le budget individu est obligatoire");
		}
		if (pourcentage() == null) {
			throw new NSValidation.ValidationException("Le pourcentage est obligatoire");
		}
		double pourcentage = pourcentage().doubleValue();
		if (pourcentage == 0 || pourcentage > 100.00) {
			throw new NSValidation.ValidationException("Le pourcentage doit être positif et inférieur ou égal à 100%");
		}
		// Rechercher les persBudgets pour la période
		NSArray donnees = rechercherDonneesPourPersBudget(editingContext(), entityName(), persBudget(),false);
		if (donnees != null && donnees.count() > 0) {
			double pourcentTotal = 0;
			for (java.util.Enumeration<DonneePourBudget> e = donnees.objectEnumerator();e.hasMoreElements();) {
				DonneePourBudget donneesPourBudget = (DonneePourBudget)e.nextElement();
				if (donneesPourBudget != this) {
					pourcentTotal += donneesPourBudget.pourcentage().doubleValue();
				}
			}
			// Ajouter le pourcentage de l'objet courant
			pourcentTotal += pourcentage().doubleValue();
			if (pourcentTotal > 100.00) {
				throw new NSValidation.ValidationException(texteExceptionPourQuotite());
			}
		}
	}
	public void initAvecPersBudgetEtPourcentage(EOPersBudget persBudget, double pourcentage) {
		addObjectToBothSidesOfRelationshipWithKey(persBudget, "persBudget");
		setPourcentage(new Double(pourcentage));
	}
	public abstract String texteExceptionPourQuotite();
	// Méthodes privées
	/* Retourne les données de type nomEntite liées à persBudget en parcourant la relation
	 * nomEntite : "PersBudgetAction", "PersBudgetAnalytique", "PersBudgetConvention"
	 */
	private static NSArray rechercherDonneesPourPersBudget(EOEditingContext editingContext,String nomEntite,EOPersBudget persBudget,boolean shouldRefresh) {
		if (nomEntite.equals(EOPersBudgetAction.ENTITY_NAME)) {
			return persBudget.toListeActions();
		} else if (nomEntite.equals(EOPersBudgetAnalytique.ENTITY_NAME)) {
			return persBudget.toListeAnalytiques();
		} else if (nomEntite.equals(EOPersBudgetConvention.ENTITY_NAME)) {
			return persBudget.toListeConventions();
		} else {
			return new NSArray();
		}
	}

}

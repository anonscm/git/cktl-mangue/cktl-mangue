/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.individu.budget;

import java.math.BigDecimal;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.paye.EOKxElement;
import org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.PeriodeAvecQuotite;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.budget.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Ligne budgétaire des personnels de l'établissement
 */
public class EOPersBudget extends _EOPersBudget {

	public static final EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_DATE_DEBUT_ASC = new NSArray(SORT_DATE_DEBUT_ASC);
	public static final NSArray SORT_ARRAY_DATE_DEBUT_DESC = new NSArray(SORT_DATE_DEBUT_DESC);

	public static String PERIODE_DEBUT_KEY = "periodeDebut";
	public static String PERIODE_FIN_KEY = "periodeFin";
	
	public EOPersBudget() {
		super();
	}

	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,DATE_DEBUT_KEY);
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,DATE_FIN_KEY);
	}
	
	public String periodeDebut() {
		if (toMoisDebut() != null && toElementPaye() != null) {
			return toMoisDebut().moisComplet();
		}
		return dateDebutFormatee();
	}
	public String periodeFin() {
		if (toMoisFin() != null && toElementPaye() != null) {
			return toMoisFin().moisComplet();
		}
		return dateFinFormatee();
	}

	/**
	 * @param edc : editing context
	 * @param element : element de paye Kx
	 * @param pourcentage : quotité
	 * @return la ligne budgétaire sur lequel l'élement Kx est affecté
	 */
	public static EOPersBudget creer(EOEditingContext edc, EOKxElement element, BigDecimal pourcentage) {

		EOPersBudget newObject = new EOPersBudget();    

		newObject.setToElementPayeRelationship(element);
		newObject.setPbudPourcentage(pourcentage);
		newObject.setTemValide(CocktailConstantes.VRAI);

		edc.insertObject(newObject);
		return newObject;
	}
	
	/**
	 * @param edc : editing context
	 * @param individu : un individu
	 * @param pourcentage : quotité
	 * @return la ligne budgétaire sur lequel l'agent est affecté
	 */
	public static EOPersBudget creer(EOEditingContext edc, EOIndividu individu, BigDecimal pourcentage) {

		EOPersBudget newObject = new EOPersBudget();    

		newObject.setToIndividuRelationship(individu);
		newObject.setPbudPourcentage(pourcentage);
		newObject.setTemValide(CocktailConstantes.VRAI);

		edc.insertObject(newObject);
		return newObject;
	}
	
	/**
	 * @param edc : editing context
	 * @param emploi : un emploi
	 * @param pourcentage : quotité
	 * @return la ligne budgétaire sur lequel l'emploi est affecté
	 */
	public static EOPersBudget creer(EOEditingContext edc, IEmploi emploi, BigDecimal pourcentage) {
		EOPersBudget newObject = new EOPersBudget();    

		newObject.setToEmploiRelationship((EOEmploi) emploi);
		newObject.setPbudPourcentage(pourcentage);
		newObject.setTemValide(CocktailConstantes.VRAI);

		edc.insertObject(newObject);
		return newObject;
	}
	
	/**
	 * 
	 * @param edc
	 * @param individu
	 * @param pourcentage
	 * @return
	 */
	public static void creerPourEmploi(EOEditingContext edc, EOIndividu individu, NSArray<EOPersBudget> budgets,
			NSTimestamp debut, NSTimestamp fin) {

		for (EOPersBudget budget : budgets) {

			EOPersBudget newObject = creer(edc, individu, budget.pbudPourcentage());
			newObject.setToOrganRelationship(budget.toOrgan());
			newObject.setToTypeCreditRelationship(budget.toTypeCredit());

			newObject.setDateDebut(debut);
			newObject.setDateFin(fin);

			if (budget.toListeActions().size() > 0) {
				NSArray<EOPersBudgetAction> actions = EOPersBudgetAction.findForBudget(edc, budget);
				for (EOPersBudgetAction action : actions) {
					EOPersBudgetAction.creer(edc, newObject, action.lolfNomenclatureDepense(), action.pourcentage());
				}
			}

			for (EOPersBudgetAnalytique analytique : (NSArray<EOPersBudgetAnalytique>)budget.toListeAnalytiques()) {
				EOPersBudgetAnalytique.creer(edc, newObject, analytique.codeAnalytique(), analytique.pourcentage());				
			}

			edc.insertObject(newObject);
		}
	}


	/**
	 * @param edc : editing context
	 * @param oldBudget : budget à renouveler
	 * @param individu : un individu
	 * @return la ligne budgétaire dupliquée pour un individu
	 */
	public static EOPersBudget renouveler(EOEditingContext edc, EOPersBudget oldBudget, EOIndividu individu) {
		EOPersBudget budget = new EOPersBudget();

		budget.setToIndividuRelationship(individu);
		budget.setToOrganRelationship(oldBudget.toOrgan());
		budget.setToTypeCreditRelationship(oldBudget.toTypeCredit());
		budget.setPbudPourcentage(ManGUEConstantes.QUOTITE_100);
		budget.setTemValide(CocktailConstantes.VRAI);

		if (oldBudget.dateFin()  != null) {
			budget.setDateDebut(DateCtrl.dateAvecAjoutJours(oldBudget.dateFin(), 1));
		}
		edc.insertObject(budget);

		return budget;

	}
	
	/**
	 * @param edc : editing context
	 * @param oldBudget : budget à renouveler
	 * @param unEmploi : un emploi
	 * @return la ligne budgétaire dupliquée pour l'emploi
	 */
	public static EOPersBudget renouveler(EOEditingContext edc, EOPersBudget oldBudget, IEmploi unEmploi) {
		EOPersBudget budget = new EOPersBudget();

		budget.setToEmploiRelationship((EOEmploi) unEmploi);
		budget.setToOrganRelationship(oldBudget.toOrgan());
		budget.setToTypeCreditRelationship(oldBudget.toTypeCredit());
		budget.setPbudPourcentage(ManGUEConstantes.QUOTITE_100);
		budget.setTemValide(CocktailConstantes.VRAI);

		if (oldBudget.dateFin()  != null) {
			budget.setDateDebut(DateCtrl.dateAvecAjoutJours(oldBudget.dateFin(), 1));
		}
		edc.insertObject(budget);

		return budget;
	}

	/**
	 * Recherche les persBudgets d'un individu pour une periode donnee
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOPersBudget> findForIndividuAndPeriode(EOEditingContext edc, EOIndividu individu, NSTimestamp debutPeriode, NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode));
		qualifiers.addObject(getQualifierIndividu(individu));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_ELEMENT_PAYE_KEY + " = nil", null));
		qualifiers.addObject(getQualifierValide());

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);

	}	/**
	 * Recherche les persBudgets d'un individu pour une periode donnee
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOPersBudget> findForEmploiAndPeriode(EOEditingContext edc, IEmploi emploi, NSTimestamp debutPeriode, NSTimestamp finPeriode) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,debutPeriode, DATE_FIN_KEY,finPeriode));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_EMPLOI_KEY + "=%@", new NSArray(emploi)));
		qualifiers.addObject(getQualifierValide());

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);

	}


	/** 
	 * Recherche les persBudgets d'un individu 
	 * @param edc : editing Context
	 * @param individu : un individu
	 * @return liste des persBudgets d'un individu
	 */
	public static NSArray<EOPersBudget> findForIndividu(EOEditingContext edc, EOIndividu individu) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(getQualifierIndividu(individu));
			qualifiers.addObject(getQualifierValide());
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_ELEMENT_PAYE_KEY + " = nil", null));
			return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOPersBudget>();
		}
	}
	/** 
	 * Recherche les persBudgets d'un individu 
	 * @param edc : editing Context
	 * @param individu : un individu
	 * @return liste des persBudgets d'un individu
	 */
	public static NSArray<EOPersBudget> findForIndividuEtElements(EOEditingContext edc, EOIndividu individu) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(getQualifierIndividu(individu));
			qualifiers.addObject(getQualifierValide());
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_ELEMENT_PAYE_KEY + " != nil", null));
			return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOPersBudget>();
		}
	}
	/** Recherche les persBudgets d'un emploi 
	 * @param individu
	 * @param shouldPrefetch si true  prefetch des relations to-many
	 * @param shouldRefresh pour raffraichir les donnees */
	public static NSArray<EOPersBudget> findForEmploi(EOEditingContext edc, IEmploi emploi) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_EMPLOI_KEY + "=%@", new NSArray(emploi)));
			qualifiers.addObject(getQualifierValide());
			return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOPersBudget>();
		}
	}

	/**
	 * 
	 */
	public void validateForSave() {

		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("BUDGET - La date de début doit être définie !");
		} else if (dateFin() != null && dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException("BUDGET - La date de fin doit être postérieure à la date de début !");
		}
		if (toIndividu() == null && toEmploi() == null) {
			throw new NSValidation.ValidationException("BUDGET - Vous devez fournir un individu ou un emploi !");
		}
		if (toOrgan() == null) {
			throw new NSValidation.ValidationException("La ligne budgétaire est obligatoire");
		}
		if (pbudPourcentage() == null) {
			throw new NSValidation.ValidationException("Le pourcentage est obligatoire");
		}

		// Controle de la quotite <= 100%

		NSArray<EOPersBudget> persBudgets = null;

		if (toIndividu() != null) {
			if (toElementPaye() == null) {
				persBudgets = findForIndividuAndPeriode(editingContext(), toIndividu(), dateDebut(), dateFin());
			}
		}
		else {
			if (toEmploi() != null) {
				persBudgets = findForEmploiAndPeriode(editingContext(), toEmploi(), dateDebut(), dateFin());			
			}
		}

		if (persBudgets != null && persBudgets.size() > 0) {

			NSMutableArray<PeriodeAvecQuotite> periodesAvecQuotite = new NSMutableArray(new PeriodeAvecQuotite(dateDebut(), dateFin(), pbudPourcentage()));
			EOOrgan organ = toOrgan();
			for (EOPersBudget budget : persBudgets) {
				if (budget != this) {
					periodesAvecQuotite.addObject(new PeriodeAvecQuotite(budget.dateDebut(), budget.dateFin(), budget.pbudPourcentage()));
					if (budget.toOrgan() == organ) {
						throw new NSValidation.ValidationException("Cette ligne budgétaire est déjà présente sur cette période !");						
					}
				}
			}

			BigDecimal pourcentTotal = new BigDecimal(PeriodeAvecQuotite.calculerQuotiteTotale(periodesAvecQuotite).floatValue());
			pourcentTotal = pourcentTotal.setScale(2, BigDecimal.ROUND_HALF_UP);

			if (pourcentTotal != null && pourcentTotal.compareTo(ManGUEConstantes.QUOTITE_100) > 0 ) {
				throw new NSValidation.ValidationException("Le pourcentage de toutes les lignes budgétaires sur la période (" + pourcentTotal.floatValue() + ") est supérieur à 100%");
			}
		}
	}
	
	public static EOQualifier getQualifierValide() {
		return CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, "O");
	}
	public static EOQualifier getQualifierIndividu(EOIndividu individu) {
		return CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu);
	}
	public static EOQualifier getQualifierElement(EOKxElement element) {
		return CocktailFinder.getQualifierEqual(TO_ELEMENT_PAYE_KEY, element);
	}
	public static EOQualifier getQualifierDate(NSTimestamp dateReference) {
		
		Number moisCode = new Integer(DateCtrl.getYear(dateReference) + StringCtrl.stringCompletion(String.valueOf(DateCtrl.getMonth(dateReference)), 2, "0", "G"));
		
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_MOIS_DEBUT_KEY + "." + EOMois.MOIS_CODE_KEY + " <=%@", new NSArray(moisCode)));
		
		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_MOIS_FIN_KEY + " = nil", null));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_MOIS_FIN_KEY + "." + EOMois.MOIS_CODE_KEY + " >=%@", new NSArray(moisCode)));
		
		andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		
		return new EOAndQualifier(andQualifiers);
		
	}


	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

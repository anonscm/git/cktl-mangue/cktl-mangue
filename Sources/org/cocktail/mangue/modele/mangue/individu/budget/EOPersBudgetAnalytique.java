/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.individu.budget;

import java.math.BigDecimal;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.mangue.budget.EOCodeAnalytique;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOPersBudgetAnalytique extends _EOPersBudgetAnalytique {

    public EOPersBudgetAnalytique() {
        super();
    }

	public static EOPersBudgetAnalytique creer(EOEditingContext ec, EOPersBudget budget, EOCodeAnalytique code , BigDecimal pourcentage) {
		
		EOPersBudgetAnalytique newObject = (EOPersBudgetAnalytique) createAndInsertInstance(ec, EOPersBudgetAnalytique.ENTITY_NAME);    

		newObject.setPersBudgetRelationship(budget);
		newObject.setCodeAnalytiqueRelationship(code);
		newObject.setPourcentage(new Double(pourcentage.floatValue()));

		return newObject;
	}
	
	/**
	 * 
	 * @param edc
	 * @param budget
	 * @return
	 */
	public static NSArray<EOPersBudgetAnalytique> findForBudget(EOEditingContext edc, EOPersBudget budget) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(PERS_BUDGET_KEY + " = %@", new NSArray(budget));
		return fetchAll(edc, qualifier, null);
	}

	/**
	 * @param ec : editingContext
	 * @param emploi : un emploi
	 * @return liste des codes analytiques pour un emploi
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOPersBudgetAnalytique> listeAnalytiquesParEmploi(EOEditingContext ec, IEmploi emploi) {
		NSArray<EOPersBudgetAnalytique> listeCodesAnalytiques = new NSMutableArray<EOPersBudgetAnalytique>();
		NSArray<EOPersBudget> budgets = EOPersBudget.findForEmploi(ec, emploi);
		
		if ((budgets != null) && (budgets.size() > 0)) {
			for (EOPersBudget unBudget : budgets) {
				listeCodesAnalytiques.addAll(unBudget.toListeAnalytiques());
			}
		}
		
		return listeCodesAnalytiques;
	}

	/**
	 * @param ec : editingContext
	 * @param emploi : un emploi
	 * @return liste des codes analytiques courants pour un emploi
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOPersBudgetAnalytique> listeAnalytiquesCourantesParEmploi(EOEditingContext ec, IEmploi emploi) {
		NSArray<EOPersBudgetAnalytique> listeCodesAnalytiques = new NSMutableArray<EOPersBudgetAnalytique>();
		NSArray<EOPersBudget> budgets = EOPersBudget.findForEmploiAndPeriode(ec, emploi, DateCtrl.now(), DateCtrl.now());
		
		if ((budgets != null) && (budgets.size() > 0)) {
			for (EOPersBudget unBudget : budgets) {
				listeCodesAnalytiques.addAll(unBudget.toListeAnalytiques());
			}
		}
		return listeCodesAnalytiques;
	}

	/**
	 * @return la quotité réelle du code analytique
	 */
	public BigDecimal quotiteAnalytiqueParBudget() {
		BigDecimal result = (pourcentage().multiply(persBudget().pbudPourcentage())).divide(ManGUEConstantes.QUOTITE_100);
		return result.setScale(2);
	}
	
	/**
	 * Méthode créée pour l'édition des emplois sinon NPE
	 * @return code et libellé du code analytique
	 */
	public String codeEtLibelleAnalytique() {
		return codeAnalytique().codeEtLibelle();
	}

	public String texteExceptionPourQuotite() {
		return "Le pourcentage de tous les codes analytiques est supérieur à 100%";
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		if (codeAnalytique() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(codeAnalytique(), CODE_ANALYTIQUE_KEY);
		}
	}
	public void validateForSave() {
		if (codeAnalytique() == null) {
			throw new NSValidation.ValidationException("Le code analytique est obligatoire");
		}
		super.validateForSave();
	}

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

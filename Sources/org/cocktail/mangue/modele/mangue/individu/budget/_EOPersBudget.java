/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersBudget.java instead.
package org.cocktail.mangue.modele.mangue.individu.budget;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPersBudget extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PersBudget";
	public static final String ENTITY_TABLE_NAME = "MANGUE.PERS_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pbudId";

	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String PBUD_POURCENTAGE_KEY = "pbudPourcentage";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String ID_EMPLOI_KEY = "idEmploi";
	public static final String KELM_ID_KEY = "kelmId";
	public static final String MOIS_CODE_DEBUT_KEY = "moisCodeDebut";
	public static final String MOIS_CODE_FIN_KEY = "moisCodeFin";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PBUD_ID_KEY = "pbudId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_COLKEY = "PBUD_DEBUT";
	public static final String DATE_FIN_COLKEY = "PBUD_FIN";
	public static final String PBUD_POURCENTAGE_COLKEY = "PBUD_POURCENTAGE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String ID_EMPLOI_COLKEY = "ID_EMPLOI";
	public static final String KELM_ID_COLKEY = "KELM_ID";
	public static final String MOIS_CODE_DEBUT_COLKEY = "MOIS_CODE_DEBUT";
	public static final String MOIS_CODE_FIN_COLKEY = "MOIS_CODE_FIN";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PBUD_ID_COLKEY = "PBUD_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String TO_ELEMENT_PAYE_KEY = "toElementPaye";
	public static final String TO_EMPLOI_KEY = "toEmploi";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_LISTE_ACTIONS_KEY = "toListeActions";
	public static final String TO_LISTE_ANALYTIQUES_KEY = "toListeAnalytiques";
	public static final String TO_LISTE_CONVENTIONS_KEY = "toListeConventions";
	public static final String TO_MOIS_DEBUT_KEY = "toMoisDebut";
	public static final String TO_MOIS_FIN_KEY = "toMoisFin";
	public static final String TO_ORGAN_KEY = "toOrgan";
	public static final String TO_TYPE_CREDIT_KEY = "toTypeCredit";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public java.math.BigDecimal pbudPourcentage() {
    return (java.math.BigDecimal) storedValueForKey(PBUD_POURCENTAGE_KEY);
  }

  public void setPbudPourcentage(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PBUD_POURCENTAGE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.paye.EOKxElement toElementPaye() {
    return (org.cocktail.mangue.common.modele.nomenclatures.paye.EOKxElement)storedValueForKey(TO_ELEMENT_PAYE_KEY);
  }

  public void setToElementPayeRelationship(org.cocktail.mangue.common.modele.nomenclatures.paye.EOKxElement value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.paye.EOKxElement oldValue = toElementPaye();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ELEMENT_PAYE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ELEMENT_PAYE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.gpeec.EOEmploi toEmploi() {
    return (org.cocktail.mangue.common.modele.gpeec.EOEmploi)storedValueForKey(TO_EMPLOI_KEY);
  }

  public void setToEmploiRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploi value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.gpeec.EOEmploi oldValue = toEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EMPLOI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EMPLOI_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois toMoisDebut() {
    return (org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois)storedValueForKey(TO_MOIS_DEBUT_KEY);
  }

  public void setToMoisDebutRelationship(org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois oldValue = toMoisDebut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MOIS_DEBUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MOIS_DEBUT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois toMoisFin() {
    return (org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois)storedValueForKey(TO_MOIS_FIN_KEY);
  }

  public void setToMoisFinRelationship(org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois oldValue = toMoisFin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MOIS_FIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MOIS_FIN_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.budget.EOOrgan toOrgan() {
    return (org.cocktail.mangue.modele.mangue.budget.EOOrgan)storedValueForKey(TO_ORGAN_KEY);
  }

  public void setToOrganRelationship(org.cocktail.mangue.modele.mangue.budget.EOOrgan value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.budget.EOOrgan oldValue = toOrgan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORGAN_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.budget.EOTypeCredit toTypeCredit() {
    return (org.cocktail.mangue.modele.mangue.budget.EOTypeCredit)storedValueForKey(TO_TYPE_CREDIT_KEY);
  }

  public void setToTypeCreditRelationship(org.cocktail.mangue.modele.mangue.budget.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.budget.EOTypeCredit oldValue = toTypeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CREDIT_KEY);
    }
  }
  
  public NSArray toListeActions() {
    return (NSArray)storedValueForKey(TO_LISTE_ACTIONS_KEY);
  }

  public NSArray toListeActions(EOQualifier qualifier) {
    return toListeActions(qualifier, null, false);
  }

  public NSArray toListeActions(EOQualifier qualifier, boolean fetch) {
    return toListeActions(qualifier, null, fetch);
  }

  public NSArray toListeActions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction.PERS_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeActionsRelationship(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_LISTE_ACTIONS_KEY);
  }

  public void removeFromToListeActionsRelationship(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LISTE_ACTIONS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction createToListeActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PersBudgetAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_LISTE_ACTIONS_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction) eo;
  }

  public void deleteToListeActionsRelationship(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LISTE_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeActionsRelationships() {
    Enumeration objects = toListeActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeActionsRelationship((org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAction)objects.nextElement());
    }
  }

  public NSArray toListeAnalytiques() {
    return (NSArray)storedValueForKey(TO_LISTE_ANALYTIQUES_KEY);
  }

  public NSArray toListeAnalytiques(EOQualifier qualifier) {
    return toListeAnalytiques(qualifier, null, false);
  }

  public NSArray toListeAnalytiques(EOQualifier qualifier, boolean fetch) {
    return toListeAnalytiques(qualifier, null, fetch);
  }

  public NSArray toListeAnalytiques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique.PERS_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeAnalytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeAnalytiquesRelationship(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_LISTE_ANALYTIQUES_KEY);
  }

  public void removeFromToListeAnalytiquesRelationship(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LISTE_ANALYTIQUES_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique createToListeAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PersBudgetAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_LISTE_ANALYTIQUES_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique) eo;
  }

  public void deleteToListeAnalytiquesRelationship(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LISTE_ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeAnalytiquesRelationships() {
    Enumeration objects = toListeAnalytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeAnalytiquesRelationship((org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetAnalytique)objects.nextElement());
    }
  }

  public NSArray toListeConventions() {
    return (NSArray)storedValueForKey(TO_LISTE_CONVENTIONS_KEY);
  }

  public NSArray toListeConventions(EOQualifier qualifier) {
    return toListeConventions(qualifier, null, false);
  }

  public NSArray toListeConventions(EOQualifier qualifier, boolean fetch) {
    return toListeConventions(qualifier, null, fetch);
  }

  public NSArray toListeConventions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention.PERS_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeConventionsRelationship(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_LISTE_CONVENTIONS_KEY);
  }

  public void removeFromToListeConventionsRelationship(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LISTE_CONVENTIONS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention createToListeConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PersBudgetConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_LISTE_CONVENTIONS_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention) eo;
  }

  public void deleteToListeConventionsRelationship(org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LISTE_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeConventionsRelationships() {
    Enumeration objects = toListeConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeConventionsRelationship((org.cocktail.mangue.modele.mangue.individu.budget.EOPersBudgetConvention)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPersBudget avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersBudget createEOPersBudget(EOEditingContext editingContext, NSTimestamp dateDebut
, java.math.BigDecimal pbudPourcentage
, String temValide
			) {
    EOPersBudget eo = (EOPersBudget) createAndInsertInstance(editingContext, _EOPersBudget.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setPbudPourcentage(pbudPourcentage);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOPersBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersBudget)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersBudget creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersBudget creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPersBudget object = (EOPersBudget)createAndInsertInstance(editingContext, _EOPersBudget.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPersBudget localInstanceIn(EOEditingContext editingContext, EOPersBudget eo) {
    EOPersBudget localInstance = (eo == null) ? null : (EOPersBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersBudget#localInstanceIn a la place.
   */
	public static EOPersBudget localInstanceOf(EOEditingContext editingContext, EOPersBudget eo) {
		return EOPersBudget.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

// _EOIndividuDiplomes.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIndividuDiplomes.java instead.
package org.cocktail.mangue.modele.mangue.individu;

import java.util.NoSuchElementException;

import org.cocktail.mangue.common.modele.nomenclatures.EOTitulaireDiplome;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOIndividuDiplomes extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "IndividuDiplomes";
	public static final String ENTITY_TABLE_NAME = "MANGUE.INDIVIDU_DIPLOMES";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noSeqIndividuDiplomes";

	public static final String C_UAI_OBTENTION_KEY = "cUaiObtention";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DIPLOME_KEY = "dDiplome";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIEU_DIPLOME_KEY = "lieuDiplome";
	public static final String SPECIALITE_KEY = "specialite";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String NO_SEQ_INDIVIDU_DIPLOMES_KEY = "noSeqIndividuDiplomes";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String C_TITULAIRE_DIPLOME_KEY = "cTitulaireDiplome";
	public static final String C_DIPLOME_KEY = "cDiplome";

//Colonnes dans la base de donnees
	public static final String C_UAI_OBTENTION_COLKEY = "C_UAI_OBTENTION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DIPLOME_COLKEY = "D_DIPLOME";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LIEU_DIPLOME_COLKEY = "LIEU_DIPLOME";
	public static final String SPECIALITE_COLKEY = "SPECIALITE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String NO_SEQ_INDIVIDU_DIPLOMES_COLKEY = "NO_SEQ_INDIVIDU_DIPLOMES";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String C_TITULAIRE_DIPLOME_COLKEY = "C_TITULAIRE_DIPLOME";
	public static final String C_DIPLOME_COLKEY = "C_DIPLOME";


	// Relationships
	public static final String DIPLOME_KEY = "diplome";
	public static final String INDIVIDU_KEY = "individu";
	public static final String TO_TITULAIRE_KEY = "toTitulaire";
	public static final String UAI_OBTENTION_KEY = "uaiObtention";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cUaiObtention() {
    return (String) storedValueForKey(C_UAI_OBTENTION_KEY);
  }

  public void setCUaiObtention(String value) {
    takeStoredValueForKey(value, C_UAI_OBTENTION_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDiplome() {
    return (NSTimestamp) storedValueForKey(D_DIPLOME_KEY);
  }

  public void setDDiplome(NSTimestamp value) {
    takeStoredValueForKey(value, D_DIPLOME_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String lieuDiplome() {
    return (String) storedValueForKey(LIEU_DIPLOME_KEY);
  }

  public void setLieuDiplome(String value) {
    takeStoredValueForKey(value, LIEU_DIPLOME_KEY);
  }

  public String specialite() {
    return (String) storedValueForKey(SPECIALITE_KEY);
  }

  public void setSpecialite(String value) {
    takeStoredValueForKey(value, SPECIALITE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.EODiplomes diplome() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EODiplomes)storedValueForKey(DIPLOME_KEY);
  }

  public void setDiplomeRelationship(org.cocktail.mangue.common.modele.nomenclatures.EODiplomes value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EODiplomes oldValue = diplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DIPLOME_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public EOTitulaireDiplome toTitulaire() {
    return (EOTitulaireDiplome)storedValueForKey(TO_TITULAIRE_KEY);
  }

  public void setToTitulaireRelationship(EOTitulaireDiplome value) {
    if (value == null) {
    	EOTitulaireDiplome oldValue = toTitulaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TITULAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TITULAIRE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EORne uaiObtention() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EORne)storedValueForKey(UAI_OBTENTION_KEY);
  }

  public void setUaiObtentionRelationship(org.cocktail.mangue.common.modele.nomenclatures.EORne value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EORne oldValue = uaiObtention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UAI_OBTENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UAI_OBTENTION_KEY);
    }
  }
  

/**
 * Créer une instance de EOIndividuDiplomes avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOIndividuDiplomes createEOIndividuDiplomes(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
, org.cocktail.mangue.common.modele.nomenclatures.EODiplomes diplome, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu			) {
    EOIndividuDiplomes eo = (EOIndividuDiplomes) createAndInsertInstance(editingContext, _EOIndividuDiplomes.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    eo.setDiplomeRelationship(diplome);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  
	  public EOIndividuDiplomes localInstanceIn(EOEditingContext editingContext) {
	  		return (EOIndividuDiplomes)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividuDiplomes creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividuDiplomes creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOIndividuDiplomes object = (EOIndividuDiplomes)createAndInsertInstance(editingContext, _EOIndividuDiplomes.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOIndividuDiplomes localInstanceIn(EOEditingContext editingContext, EOIndividuDiplomes eo) {
    EOIndividuDiplomes localInstance = (eo == null) ? null : (EOIndividuDiplomes)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOIndividuDiplomes#localInstanceIn a la place.
   */
	public static EOIndividuDiplomes localInstanceOf(EOEditingContext editingContext, EOIndividuDiplomes eo) {
		return EOIndividuDiplomes.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOIndividuDiplomes fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOIndividuDiplomes fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOIndividuDiplomes eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOIndividuDiplomes)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOIndividuDiplomes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndividuDiplomes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndividuDiplomes eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndividuDiplomes)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOIndividuDiplomes fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOIndividuDiplomes eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOIndividuDiplomes ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOIndividuDiplomes fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

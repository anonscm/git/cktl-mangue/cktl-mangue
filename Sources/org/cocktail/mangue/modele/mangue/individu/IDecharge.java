package org.cocktail.mangue.modele.mangue.individu;

public interface IDecharge {

	public abstract Integer anneeUniversitaire();

	public abstract void setAnneeUniversitaire(Integer annee);

}
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue;

import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOParamPromotion extends _EOParamPromotion implements RecordAvecLibelleEtCode {

	public static final String[] typesPromotion = {"LA", "TA", "EP","EC"};
	public static final String[] typesPromotionLong = {"Liste d'Aptitude", "Tableau d'Avancement", "Examen Professionnel","Enseignant-Chercheur"};
	public static final String LISTE_APTITUDE = "LA";
	private static final String SANS_CATEGORIE = "Z";

    public EOParamPromotion() {
        super();
    }

    public String toString() {
    	return parpCode();
    }


	public boolean existeCondition() {
		return parpTemCondition() != null && parpTemCondition().equals(CocktailConstantes.VRAI);
	}
	public void setExisteCondition(boolean aBool) {
		if (aBool) {
			setParpTemCondition(CocktailConstantes.VRAI);
		} else {
			setParpTemCondition(CocktailConstantes.FAUX);
		}
	}
	public boolean utiliserCategorie() {
		return parpTemCategorie() != null && parpTemCategorie().equals(CocktailConstantes.VRAI);
	}
	public void setUtiliserCategorie(boolean aBool) {
		if (aBool) {
			setParpTemCategorie(CocktailConstantes.VRAI);
		} else {
			setParpTemCategorie(CocktailConstantes.FAUX);
		}
	}
	public boolean utiliserCorps() {
		return parpTemCorps() != null && parpTemCorps().equals(CocktailConstantes.VRAI);
	}
	public void setUtiliserCorps(boolean aBool) {
		if (aBool) {
			setParpTemCorps(CocktailConstantes.VRAI);
		} else {
			setParpTemCorps(CocktailConstantes.FAUX);
		}
	}
	public boolean utiliserGrade() {
		return parpTemGrade() != null && parpTemGrade().equals(CocktailConstantes.VRAI);
	}
	public void setUtiliserGrade(boolean aBool) {
		if (aBool) {
			setParpTemGrade(CocktailConstantes.VRAI);
		} else {
			setParpTemGrade(CocktailConstantes.FAUX);
		}
	}
	public boolean utiliserEchelon() {
		return parpTemEchelon() != null && parpTemEchelon().equals(CocktailConstantes.VRAI);
	}
	public void setUtiliserEchelon(boolean aBool) {
		if (aBool) {
			setParpTemEchelon(CocktailConstantes.VRAI);
		} else {
			setParpTemEchelon(CocktailConstantes.FAUX);
		}
	}
	public boolean aConditionServicePublic() {
		return parpTemServPublics() != null && parpTemServPublics().equals(CocktailConstantes.VRAI);
	}
	public void setAConditionServicePublic(boolean aBool) {
		if (aBool) {
			setParpTemServPublics(CocktailConstantes.VRAI);
		} else {
			setParpTemServPublics(CocktailConstantes.FAUX);
		}
	}
	public boolean aConditionServiceEffectif() {
		return parpTemServEffectifs() != null && parpTemServEffectifs().equals(CocktailConstantes.VRAI);
	}
	public void setAConditionServiceEffectif(boolean aBool) {
		if (aBool) {
			setParpTemServEffectifs(CocktailConstantes.VRAI);
		} else {
			setParpTemServEffectifs(CocktailConstantes.FAUX);
		}
	}
	public boolean estValide() {
		return parpTemValide() != null && parpTemValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setParpTemValide(CocktailConstantes.VRAI);
		} else {
			setParpTemValide(CocktailConstantes.FAUX);
		}
	}
	public String dateOuvertureFormatee() {
		return SuperFinder.dateFormatee(this,PARP_D_OUVERTURE_KEY);
	}
	public void setDateOuvertureFormatee(String uneDate) {
		if (uneDate == null) {
			setParpDOuverture(null);
		} else {
			SuperFinder.setDateFormatee(this,PARP_D_OUVERTURE_KEY,uneDate);
		}
	}
	public String dateFermetureFormatee() {
		return SuperFinder.dateFormatee(this,PARP_D_FERMETURE_KEY);
	}
	public void setDateFermetureFormatee(String uneDate) {
		if (uneDate == null) {
			setParpDFermeture(null);
		} else {
			SuperFinder.setDateFormatee(this,PARP_D_FERMETURE_KEY,uneDate);
		}
	}
	public boolean estListeAptitude() {
		return parpType() != null && parpType().equals("LA");
	}
	public void init() {
		setEstValide(true);
		setExisteCondition(false);
		setUtiliserCategorie(false);
		setUtiliserCorps(false);
		setUtiliserGrade(false);
		setUtiliserEchelon(false);
		setAConditionServiceEffectif(false);
		setAConditionServicePublic(false);
	}
	public void initAvecType(String type) {
		init();
		setParpType(type);
	}
	public void invalider() {
		setEstValide(false);
	}
	public String ancienneteRequise() {
		String temp = "";
		if (parpDureeGrade() != null || parpDureeMoisGrade() != null) {
			temp = "Grade : ";
			String duree = "";
			if (parpDureeGrade() != null) {
				duree += parpDureeGrade() + " année(s)";
			}
			if (parpDureeMoisGrade() != null) {
				if (duree.length() > 0) {
					duree += " ";
				}
				duree += parpDureeMoisGrade() + " mois";
			}
			temp += duree;
		}
		return temp;
	}
	public String ancienneteConservee() {
		if (parpDureeMaxAncienneteCons() != null) {
			return "si < " + parpDureeMaxAncienneteCons() + " ans";
		} else {
			return "";
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (parpDOuverture() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de début de validité");
		}
		if (parpDOuverture() != null && parpDFermeture() != null && DateCtrl.isAfter(parpDOuverture(), parpDFermeture())) {
			throw new NSValidation.ValidationException("La date d'ouverture ne peut pas êtr postérieure à la date de fermeture");
		}
		if (parpType() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un type");
		}
		if (parpCode() != null && parpCode().length() > 40) {
			throw new NSValidation.ValidationException("Le code comporte au plus 40 caractères");

		}
		boolean found = false;
		for (int i = 0; i < typesPromotion.length;i++) {
			if (parpType().equals(typesPromotion[i])) {
				found = true;
				break;
			}
		}
		if (!found) {
			throw new NSValidation.ValidationException("Type de paramètre inconnu");
		}
		if (gradeArrivee() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le grade d'arrivée");
		}
		if ((cCategorieServEffectifs() != null && cCategorieServEffectifs().length() > 1) || (cCategorieServPublics() != null && cCategorieServPublics().length() > 1)) {
			throw new NSValidation.ValidationException("La catégorie comporte 1 seul caractère");
		}
		if (cEchelon() != null) {
			if (cEchelon().length() > 2) {
				throw new NSValidation.ValidationException("L'échelon comporte au plus deux caractères");
			}
			if (gradeDepart() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir avec un échelon, un grade de départ");
			}
			NSArray passages = EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(editingContext(), gradeDepart().cGrade(), cEchelon(), parpDOuverture(), true);
			if (passages.count() == 0) {
				throw new NSValidation.ValidationException("Cet échelon n'existe pas pour le grade de départ fourni");
			}
		} else {
			if (cChevronDepart() != null) {
				throw new NSValidation.ValidationException("Vous fournissez le chevron de départ, l'échelon doit être fourni");
			}
			// vérifier si la durée d'échelon est fournie que l'échelon est fourni
			if (parpDureeEchelon() != null) {
				throw new NSValidation.ValidationException("Vous devez fournir l'échelon si vous fournissez la durée dans l'échelon");
			}
			if (parpDureeMaxAncienneteCons() != null) {
				throw new NSValidation.ValidationException("Vous devez fournir l'échelon si vous fournissez la durée maximum pour bénéficier de la conservation d'ancienneté");
			}
		}
		if (cChevronArrivee() != null && cEchelonArrivee() == null) {
			throw new NSValidation.ValidationException("Vous fournissez le chevron d'arrivée, l'échelon d'arrivée doit être fourni");
		}
		if (corpsDepart() != null && typePopulationDepart() != null && corpsDepart().toTypePopulation() != typePopulationDepart()) {
			throw new NSValidation.ValidationException("Le corps et le type de population de départ ne sont pas cohérents");
		}
		if (corpsDepart() != null && gradeDepart() != null && gradeDepart().toCorps() != corpsDepart()) {
			throw new NSValidation.ValidationException("Le corps et le grade de départ ne sont pas cohérents");
		}
		if (corpsArrivee() != null && gradeArrivee() != null && gradeArrivee().toCorps() != corpsArrivee()) {
			throw new NSValidation.ValidationException("Le corps et le grade d'arrivée ne sont pas cohérents");
		}
		if (gradeDepart() == null && cCategorieServEffectifs() == null && typePopulationServPublics() == null && parpDureeServEffectifs() != null) {
			throw new NSValidation.ValidationException("Avec une durée de service effectif, vous devez fournir un grade de départ ou un type de population ou une catégorie de service effectif");
		}
		if (parpDureeCategServPublics() != null && cCategorieServPublics() == null && typePopulationServPublics() == null) {
			throw new NSValidation.ValidationException("Avec une durée de catégorie de service public, vous devez fournir un type de population ou une catégorie de service public");
		}
		if (corpsArrivee() == null && gradeArrivee() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le corps ou le grade d'arrivée");
		}
		// Vérifier si les durées sont des nombres positifs
		verifierDuree(parpDureeCorps(),"de corps");
		verifierDuree(parpDureeGrade(),"de grade");
		verifierDuree(parpDureeMoisGrade(),"en mois du grade");
		verifierDuree(parpDureeEchelon(),"d'echelon");
		verifierDuree(parpDureeCategServPublics()," de catégorie de services publics");
		verifierDuree(parpDureeServPublics()," de services publics");
		verifierDuree(parpDureeServEffectifs()," de services effectifs");

		if (parpRefReglementaire() != null && parpRefReglementaire().length() > 2000) {
			throw new NSValidation.ValidationException("La référence réglementaire comporte au plus 2000 caractères");
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public String description() {
		
		String result = "";

		if (parpDureeCorps() != null) {
			result += "Avoir au moins " + parpDureeCorps() + " ans de svc effectifs dans le corps, ";
		}
		if (parpDureeGrade() != null) {
			if (result.length() == 0) {
				result += "Avoir ";
			} else {
				result += "avoir ";
			}
			result += "au moins " + parpDureeGrade() + " ans d'anc. dans le grade, ";
		}
		if (cEchelon() != null) {
			if (result.length() == 0) {
				result += "Avoir ";
			} else {
				result += "avoir ";
			}
			result += "atteint l'échelon " + cEchelon(); 
			if (parpDureeEchelon() != null) {
				result += " avec au moins " + parpDureeEchelon() +  " ans d'anc.";
			} 
			result += ", ";
		}
		if (parpDureeServPublics() != null) {
			if (result.length() == 0) {
				result += "Au moins ";
			} else {
				result += "au moins ";
			}
			result += parpDureeServPublics() + " ans de svc publics";
			// Si il y a un type de population et pas de durée de catégorie et pas de durée de service effectif, on considère que la condition veut dire durée service effectif comme population
			// Sinon le type de population sera rattaché à une des deux durées
			if (typePopulationServPublics() != null && parpDureeCategServPublics() == null && parpDureeServEffectifs() == null) {
				result += " comme " + typePopulationServPublics().libelleCourt();
			}
			result +=  ", ";
		}
		if (parpDureeCategServPublics() != null) {
			if (result.length() == 0) {
				result += "Au moins ";
			} else {
				result += "au moins ";
			}
			result += parpDureeCategServPublics() + " ans de svc publics";
			if (cCategorieServPublics() != null) {
				result += " dans un corps de catég. " + cCategorieServPublics(); 	
			} else if (typePopulationServPublics() != null) {
				result += " comme " + typePopulationServPublics().libelleCourt();
			}
			result +=  ", ";
		}
		if (parpDureeServEffectifs() != null) {
			if (result.length() == 0) {
				result += "Au moins ";
			} else {
				result += "au moins ";
			}
			result += parpDureeServEffectifs() + " ans  de svc effectifs";
			if (cCategorieServEffectifs() != null) {
				result += " dans un corps de catég. " + cCategorieServEffectifs() + ", "; 	
			} else if (gradeDepart() != null ) { 
				result += " dans le grade, ";
			} else if (typePopulationServPublics() != null) {
				result += " comme  " + typePopulationServPublics().libelleCourt() + ", ";

			}
		}
		if (result.length() == 0) {
			return "Aucune condition";
		} else {
			// Supprimer ", " en fin de texte
			return result.substring(0,result.length() - 2);
		}
	}
	// Interface RecordAvecLibelleEtCode
	public String code() {
		return parpType();
	}
	public String libelle() {
		return parpCode();
	}
	// Méthodes privées
	private void verifierDuree(Number duree,String type) throws NSValidation.ValidationException {
		if (duree != null)
			if (duree.intValue() <= 0) {
				throw new NSValidation.ValidationException("La durée " + type + " est un nombre positif");
			} else if (type.indexOf("mois") >= 0) {
				if (duree.intValue() > 12) {
					throw new NSValidation.ValidationException("La durée " + type + " est inférieure ou égale à 12");
				}
			} else if (duree.intValue() > 100) {
				throw new NSValidation.ValidationException("La durée " + type + " est inférieure à 100");
			}
	}
	// Méthodes statiques
	
	public static NSArray rechercherParametresPourCode(EOEditingContext ec,String code) {

		try {
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PARP_TEM_VALIDE_KEY + " = %@", new NSArray("O")));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PARP_CODE_KEY + " = %@", new NSArray(code)));

		return fetchAll(ec, new EOAndQualifier(qualifiers));
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
		
		
	}
	
	public static NSArray rechercherParametresPourType(EOEditingContext editingContext,String type) {
		if (type == null) {
			return null;
		}
		NSMutableArray args = new NSMutableArray(type);
		args.addObject(CocktailConstantes.VRAI);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,EOQualifier.qualifierWithQualifierFormat(PARP_TYPE_KEY + " = %@ AND parpTemValide = %@", args),null);
		return editingContext.objectsWithFetchSpecification(fs);
	}


	/** Rercherche tous les parametres valides dans la periode */
	public static NSArray<EOParamPromotion> rechercherParametresPourDateEtType(EOEditingContext editingContext,NSTimestamp date,String type) {

		try {

			if (type == null)
				return null;

			NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(PARP_TEM_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PARP_TYPE_KEY + " = %@", new NSArray(type)));
			if (date != null)
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(PARP_D_OUVERTURE_KEY, date, PARP_D_FERMETURE_KEY, date));

			EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers),null);
			fs.setRefreshesRefetchedObjects(true);

			return editingContext.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}

	}
	public static String typePromotionCoursPourTypeLong(String typePromotionLong) {
		if (typePromotionLong == null) {
			return null;
		}
		for (int i = 0; i < typesPromotionLong.length;i++) {
			if (typePromotionLong.equals(typesPromotionLong[i])) {
				return EOParamPromotion.typesPromotion[i];
			}
		}
		return null;
	}
	public static String typePromotionLongPourTypeCourt(String typePromotionCourt) {
		if (typePromotionCourt == null) {
			return null;
		}
		for (int i = 0; i < EOParamPromotion.typesPromotion.length;i++) {
			if (typePromotionCourt.equals(EOParamPromotion.typesPromotion[i])) {
				return typesPromotionLong[i];
			}
		}
		return null;
	}
	/** Retourne l'ordre de tri des parametres de promotion : on trie sur l'echelon et le chevron cible puis sur l'echelon et le chevron de d&eacute;part par ordre d&eacute;croissant */
	public static NSArray triParametres(String relation) {
		if (relation.length() > 0) {
			relation += ".";
		}
		// On trie les paramètres par échelon décroissant et chevron décroissant pour commencer par l'échelon d'arrivée et le chevron le plus haut car dans certains cas, on n'atteint l'échelon le plus haut
		// que si les conditions d'ancienneté sont remplies, sinon on est promouvable au chevron ou à l'échelon précédent
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(relation + C_ECHELON_ARRIVEE_KEY, EOSortOrdering.CompareDescending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(relation + C_CHEVRON_ARRIVEE_KEY, EOSortOrdering.CompareDescending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(relation + C_ECHELON_KEY, EOSortOrdering.CompareDescending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey(relation + C_CHEVRON_DEPART_KEY, EOSortOrdering.CompareDescending));
		return sorts;
	}

    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

//EOAgentPersonnel.java
//Created on Tue Mar 04 08:32:26  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue;


import org.cocktail.common.Constantes;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.referentiel.EOCompte;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOAgentPersonnel extends _EOAgentPersonnel {
	
	public final static String TYPE_POPULATION_ENSEIGNANT = "E";
	public final static String TYPE_POPULATION_NON_ENSEIGNANT = "N";
	public final static String TYPE_POPULATION_VACATAIRE = "V";
	public final static String TYPE_POPULATION_HEBERGE = "H";

	public EOAgentPersonnel() {
		super();
	}

	public String agtLogin() {
		return compte().cptLogin();
	}
	public boolean gereEnseignants() {
		return agtTypePopulation() != null && agtTypePopulation().indexOf(TYPE_POPULATION_ENSEIGNANT) >= 0;
	}
	public void setGereEnseignants(boolean aBool) {
		modifierTypePopulation(TYPE_POPULATION_ENSEIGNANT,aBool);
	}
	public boolean gereNonEnseignants() {
		return agtTypePopulation() != null && agtTypePopulation().indexOf(TYPE_POPULATION_NON_ENSEIGNANT) >= 0;
	}
	public void setGereNonEnseignants(boolean aBool) {
		modifierTypePopulation(TYPE_POPULATION_NON_ENSEIGNANT,aBool);
	}
	public boolean gereVacataires() {
		return agtTypePopulation() != null && agtTypePopulation().indexOf(TYPE_POPULATION_VACATAIRE) >= 0;
	}
	public void setGereVacataires(boolean aBool) {
		modifierTypePopulation(TYPE_POPULATION_VACATAIRE,aBool);
	}
	public boolean gereHeberges() {
		return agtTypePopulation() != null && agtTypePopulation().indexOf(TYPE_POPULATION_HEBERGE) >= 0;
	}
	public void setGereHeberges(boolean aBool) {
		modifierTypePopulation(TYPE_POPULATION_HEBERGE,aBool);
	}
	public String agtGereHeberge() {
		if (gereHeberges()) {
			return CocktailConstantes.VRAI;
		} else {
			return CocktailConstantes.FAUX;
		}
	}
	public boolean gereTousAgents() {
		return gereNonEnseignants() && gereEnseignants() && gereVacataires() && gereHeberges();
	}
	public boolean gereTouteStructure() {
		return agtTout() != null && agtTout().equals(CocktailConstantes.VRAI);
	}
	public void setGereTouteStructure(boolean aBool) {
		if (aBool) {
			setAgtTout(CocktailConstantes.VRAI);
		} else {
			setAgtTout(CocktailConstantes.FAUX);
		}
	}
	/** Retourne la liste des services geres par l'utilisateur avec les structures filles */
	public NSArray<EOStructure> structuresGereesEtFilles() {
		NSMutableArray<EOStructure> structuresCompletes = new NSMutableArray<EOStructure>();
		for (EOStructure structure : structuresGerees()) {
			if (structuresCompletes.containsObject(structure) == false) {
				structuresCompletes.addObject(structure);
				for (EOStructure fille : structure.structuresFils()) {
					if (structuresCompletes.containsObject(fille) == false) {
						structuresCompletes.addObject(fille);
					}
				}
			}
		}
		return structuresCompletes.immutableClone();
	}
	/** Retourne la liste des services geres par l'utilisateur */
	public NSArray<EOStructure> structuresGerees() {
		return filtrerStructures(true);
	}
	/** Retourne la liste des groupes geres par l'utilisateur */
	public NSArray<EOStructure> groupesGeres() {
		return filtrerStructures(false);
	}
	public boolean peutConsulterDossier() {
		return agtConsDossier() != null && agtConsDossier().equals(CocktailConstantes.VRAI);
	}
	public void setPeutConsulterDossier(boolean aBool) {
		if (aBool) {
			setAgtConsDossier(CocktailConstantes.VRAI);
		} else {
			setAgtConsDossier(CocktailConstantes.FAUX);
		}
	}

	public boolean peutAfficherEmplois() {
		return agtConsEmplois() != null && agtConsEmplois().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherEmplois(boolean aBool) {
		if (aBool) {
			setAgtConsEmplois(CocktailConstantes.VRAI);
		} else {
			setAgtConsEmplois(CocktailConstantes.FAUX);
		}
	}
	public boolean peutGererEmplois() {
		return agtUpdEmplois() != null && agtUpdEmplois().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererEmplois(boolean aBool) {
		if (aBool) {
			setAgtUpdEmplois(CocktailConstantes.VRAI);
		} else {
			setAgtUpdEmplois(CocktailConstantes.FAUX);
		}
	}
	public boolean peutAfficherPostes() {
		return agtConsPostes() != null && agtConsPostes().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherPostes(boolean aBool) {
		if (aBool) {
			setAgtConsPostes(CocktailConstantes.VRAI);
		} else {
			setAgtConsPostes(CocktailConstantes.FAUX);
		}
	}
	public boolean peutGererPostes() {
		return agtUpdPostes() != null && agtUpdPostes().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererPostes(boolean aBool) {
		if (aBool) {
			setAgtUpdPostes(CocktailConstantes.VRAI);
		} else {
			setAgtUpdPostes(CocktailConstantes.FAUX);
		}
	}
	public boolean peutAfficherIndividu() {
		return agtConsIdentite() != null && agtConsIdentite().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherIndividu(boolean aBool) {
		if (aBool) {
			setAgtConsIdentite(CocktailConstantes.VRAI);
		} else {
			setAgtConsIdentite(CocktailConstantes.FAUX);
		}
	}
	public boolean peutGererBudget() {
		return agtBudget() != null && agtBudget().equals(CocktailConstantes.VRAI);
	}
	public boolean peutGererCir() {
		return agtCir() != null && agtCir().equals(CocktailConstantes.VRAI);
	}
	public boolean peutGererNomenclatures() {
		return agtNomenclatures() != null && agtNomenclatures().equals(CocktailConstantes.VRAI);
	}
	public boolean peutGererVacations() {
		return agtVacations() != null && agtVacations().equals(CocktailConstantes.VRAI);
	}

	
	
	public boolean isModuleVacation() {
		return agtModVacation() != null && agtModVacation().equals(CocktailConstantes.VRAI);
	}
	public void setIsModuleVacation(boolean value) {
		if (value)
			setAgtModVacation(CocktailConstantes.VRAI);
		else
			setAgtModVacation(CocktailConstantes.FAUX);
	}
	
	public boolean peutGererHeberges() {
		return agtGereHeberge() != null && agtGereHeberge().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererNomenclatures(boolean aBool) {
		if (aBool) {
			setAgtNomenclatures(CocktailConstantes.VRAI);
		} else {
			setAgtNomenclatures(CocktailConstantes.FAUX);
		}
	}
	public void setPeutGererBudget(boolean aBool) {
		if (aBool) {
			setAgtBudget(CocktailConstantes.VRAI);
		} else {
			setAgtBudget(CocktailConstantes.FAUX);
		}
	}
	public void setPeutGererCir(boolean aBool) {
		if (aBool) {
			setAgtCir(CocktailConstantes.VRAI);
		} else {
			setAgtCir(CocktailConstantes.FAUX);
		}
	}

	public void setPeutGererVacations(boolean aBool) {
		if (aBool) {
			setAgtVacations(CocktailConstantes.VRAI);
		} else {
			setAgtVacations(CocktailConstantes.FAUX);
		}
	}

	public void setPeutGererHeberges(boolean aBool) {
		if (aBool) {
			setAgtHeberges(CocktailConstantes.VRAI);
		} else {
			setAgtHeberges(CocktailConstantes.FAUX);
		}
	}

	public boolean peutGererIndividu() {
		return agtUpdIdentite() != null && agtUpdIdentite().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererIndividu(boolean aBool) {
		if (aBool) {
			setAgtUpdIdentite(CocktailConstantes.VRAI);
		} else {
			setAgtUpdIdentite(CocktailConstantes.FAUX);
		}
	}
	public boolean peutAfficherInfosPerso() {
		return agtInfosPerso() != null && agtInfosPerso().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherInfosPerso(boolean aBool) {
		if (aBool) {
			setAgtInfosPerso(CocktailConstantes.VRAI);
		} else {
			setAgtInfosPerso(CocktailConstantes.FAUX);
		}
	}
	public boolean peutAfficherContrats() {
		return agtConsContrat() != null && agtConsContrat().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherContrats(boolean aBool) {
		if (aBool) {
			setAgtConsContrat(CocktailConstantes.VRAI);
		} else {
			setAgtConsContrat(CocktailConstantes.FAUX);
		}
	}
	public boolean peutGererContrats() {
		return agtUpdContrat() != null && agtUpdContrat().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererContrats(boolean aBool) {
		if (aBool) {
			setAgtUpdContrat(CocktailConstantes.VRAI);
		} else {
			setAgtUpdContrat(CocktailConstantes.FAUX);
		}
	}
	public boolean peutAfficherCarrieres() {
		return agtConsCarriere() != null && agtConsCarriere().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherCarrieres(boolean aBool) {
		if (aBool) {
			setAgtConsCarriere(CocktailConstantes.VRAI);
		} else {
			setAgtConsCarriere(CocktailConstantes.FAUX);
		}
	}
	public boolean peutGererCarrieres() {
		return agtUpdCarriere() != null && agtUpdCarriere().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererCarrieres(boolean aBool) {
		if (aBool) {
			setAgtUpdCarriere(CocktailConstantes.VRAI);
		} else {
			setAgtUpdCarriere(CocktailConstantes.FAUX);
		}
	}
	public boolean peutAfficherOccupation() {
		return agtConsOccupation() != null && agtConsOccupation().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherOccupation(boolean aBool) {
		if (aBool) {
			setAgtConsOccupation(CocktailConstantes.VRAI);
		} else {
			setAgtConsOccupation(CocktailConstantes.FAUX);
		}
	}
	public boolean peutGererOccupation() {
		return agtUpdOccupation() != null && agtUpdOccupation().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererOccupation(boolean aBool) {
		if (aBool) {
			setAgtUpdOccupation(CocktailConstantes.VRAI);
		} else {
			setAgtUpdOccupation(CocktailConstantes.FAUX);
		}
	}
	public boolean peutAfficherConges() {
		return agtConsConges() != null && agtConsConges().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherConges(boolean aBool) {
		if (aBool) {
			setAgtConsConges(CocktailConstantes.VRAI);
		} else {
			setAgtConsConges(CocktailConstantes.FAUX);
		}
	}  
	public boolean peutGererConges() {
		return agtUpdConges() != null && agtUpdConges().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererConges(boolean aBool) {
		if (aBool) {
			setAgtUpdConges(CocktailConstantes.VRAI);
		} else {
			setAgtUpdConges(CocktailConstantes.FAUX);
		}
	}
	public boolean peutUtiliserOutils() {
		return agtOutils() != null && agtOutils().equals(CocktailConstantes.VRAI);
	}
	public void setPeutUtiliserOutils(boolean aBool) {
		if (aBool) {
			setAgtOutils(CocktailConstantes.VRAI);
		} else {
			setAgtOutils(CocktailConstantes.FAUX);
		}
	}
	public boolean peutAdministrer() {
		return agtAdministration() != null && agtAdministration().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAdministrer(boolean aBool) {
		if (aBool) {
			setAgtAdministration(CocktailConstantes.VRAI);
		} else {
			setAgtAdministration(CocktailConstantes.FAUX);
		}
	}
	public boolean peutUtiliserRequetes() {
		return agtEditionRequetes() != null && agtEditionRequetes().equals(CocktailConstantes.VRAI);
	}
	public void setPeutUtiliserRequetes(boolean aBool) {
		if (aBool) {
			setAgtEditionRequetes(CocktailConstantes.VRAI);
		} else {
			setAgtEditionRequetes(CocktailConstantes.FAUX);
		}
	}

	public boolean peutAfficherAgents() {
		return agtConsAgents() != null && agtConsAgents().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherAgents(boolean aBool) {
		if (aBool) {
			setAgtConsAgents(CocktailConstantes.VRAI);
		} else {
			setAgtConsAgents(CocktailConstantes.FAUX);
		}
	}
	public boolean peutGererAgents() {
		return agtUpdAgents() != null && agtUpdAgents().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererAgents(boolean aBool) {
		if (aBool) {
			setAgtUpdAgents(CocktailConstantes.VRAI);
		} else {
			setAgtUpdAgents(CocktailConstantes.FAUX);
		}
	}
	public boolean peutAfficherAccidentTravail() {
		return agtConsAccident() != null && agtConsAccident().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherAccidentTravail(boolean aBool) {
		if (aBool) {
			setAgtConsAccident(CocktailConstantes.VRAI);
		} else {
			setAgtConsAccident(CocktailConstantes.FAUX);
		}
	}
	public boolean peutGererAccidentTravail() {
		return agtUpdAccident() != null && agtUpdAccident().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererAccidentTravail(boolean aBool) {
		if (aBool) {
			setAgtUpdAccident(CocktailConstantes.VRAI);
		} else {
			setAgtUpdAccident(CocktailConstantes.FAUX);
		}
	}
	
	public boolean peutCreerListesElectorales() {
		return agtElection() != null && agtElection().equals(CocktailConstantes.VRAI);
	}
	public void setPeutCreerListesElectorales(boolean aBool) {
		if (aBool) {
			setAgtElection(CocktailConstantes.VRAI);
		} else {
			setAgtElection(CocktailConstantes.FAUX);
		}
	}
	
	public boolean peutGererPromouvabilites() {
		return agtPromouvable() != null && agtPromouvable().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererPromouvabilites(boolean aBool) {
		if (aBool) {
			setAgtPromouvable(CocktailConstantes.VRAI);
		} else {
			setAgtPromouvable(CocktailConstantes.FAUX);
		}
	}
	public boolean peutGererPrimes() {
		return agtPrime() != null && agtPrime().equals(CocktailConstantes.VRAI);
	}
	public void setPeutGererPrimes(boolean aBool) {
		if (aBool) {
			setAgtPrime(CocktailConstantes.VRAI);
		} else {
			setAgtPrime(CocktailConstantes.FAUX);
		}
	}
	public boolean gerePlusieursCategoriesPersonnel() {
		return agtTypePopulation() != null && agtTypePopulation().length() > 1;
	}
	
	/**
	 * 
	 */
	public void init() {
		setAgtConsAgents(CocktailConstantes.FAUX);
		setAgtConsCarriere(CocktailConstantes.FAUX);
		setAgtConsConges(CocktailConstantes.FAUX);
		setAgtConsContrat(CocktailConstantes.FAUX);
		setAgtConsDossier(CocktailConstantes.FAUX);
		setAgtConsEmplois(CocktailConstantes.FAUX);
		setAgtConsPostes(CocktailConstantes.FAUX);
		setAgtConsIdentite(CocktailConstantes.FAUX);
		setAgtConsOccupation(CocktailConstantes.FAUX);
		setAgtEditionFos(CocktailConstantes.FAUX);
		setAgtEditionRequetes(CocktailConstantes.FAUX);
		setAgtInfosPerso(CocktailConstantes.FAUX);
		setAgtOutils(CocktailConstantes.FAUX);
		setAgtTout(CocktailConstantes.FAUX);
		setAgtUpdAgents(CocktailConstantes.FAUX);
		setAgtUpdCarriere(CocktailConstantes.FAUX);
		setAgtUpdConges(CocktailConstantes.FAUX);
		setAgtUpdContrat(CocktailConstantes.FAUX);
		setAgtUpdEmplois(CocktailConstantes.FAUX);
		setAgtUpdPostes(CocktailConstantes.FAUX);
		setAgtUpdIdentite(CocktailConstantes.FAUX);
		setAgtUpdAccident(CocktailConstantes.FAUX);
		setAgtUpdOccupation(CocktailConstantes.FAUX);
		setAgtElection(CocktailConstantes.FAUX);
		setAgtAdministration(CocktailConstantes.FAUX);
		setAgtPromouvable(CocktailConstantes.FAUX);
		setAgtPrime(CocktailConstantes.FAUX);
		setAgtNomenclatures(CocktailConstantes.FAUX);
		setAgtBudget(CocktailConstantes.FAUX);
		setAgtCir(CocktailConstantes.FAUX);
		setAgtVacations(CocktailConstantes.FAUX);
		setAgtHeberges(CocktailConstantes.FAUX);
		setAgtModVacation(CocktailConstantes.FAUX);
	}
	public void initPourPersonnel() {
		init();
		setAgtConsCarriere(CocktailConstantes.VRAI);
		setAgtConsConges(CocktailConstantes.VRAI);
		setAgtConsContrat(CocktailConstantes.VRAI);
		setAgtConsDossier(CocktailConstantes.VRAI);
		setAgtConsOccupation(CocktailConstantes.VRAI);
		setAgtInfosPerso(CocktailConstantes.VRAI);
		setAgtConsAccident(CocktailConstantes.VRAI);
		setAgtTypePopulation(TYPE_POPULATION_ENSEIGNANT + TYPE_POPULATION_NON_ENSEIGNANT);
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (agtTypePopulation() != null && agtTypePopulation().length() > 4) {
			throw new NSValidation.ValidationException("le type de population géré ne peut dépasser 4 caractères, type actuel " + agtTypePopulation());
		}
	}
	public void transformerEnPersonnel() {
		init();
		supprimerRelations();	// Supprime les relations sur les services
		initPourPersonnel();
	}
	
	/**
	 * 
	 */
	public void supprimerRelations() {
		NSArray<EOAgentsDroitsServices> droitsServices = new NSArray<EOAgentsDroitsServices>(toAgentsDroitsServices());
		for (EOAgentsDroitsServices droit : droitsServices) {
			removeObjectFromBothSidesOfRelationshipWithKey(droit,TO_AGENTS_DROITS_SERVICES_KEY);
			editingContext().deleteObject(droit);
		}
	}

	/**
	 * 
	 * @param uneStr
	 * @param aBool
	 */
	private void modifierTypePopulation(String uneStr, boolean aBool) {
		if (aBool) {
			if (agtTypePopulation() == null) {
				setAgtTypePopulation(uneStr);
			} else if (agtTypePopulation().indexOf(uneStr) < 0) {
				// type de population non encore pris en compte
				setAgtTypePopulation(agtTypePopulation() + uneStr);
			}
		} else {
			if (agtTypePopulation() != null && agtTypePopulation().indexOf(uneStr) >= 0) {
				// supprimer du type de population
				setAgtTypePopulation(agtTypePopulation().replaceAll(uneStr,""));
			}
		}
	}
	
	/**
	 * 
	 * @param estService
	 * @return
	 */
	private NSArray<EOStructure> filtrerStructures(boolean estService) {
		if (toAgentsDroitsServices() == null) {
			return null;
		}
		
		NSMutableArray<EOStructure> structures = new NSMutableArray<EOStructure>();
		
		for (EOAgentsDroitsServices droit : (NSArray<EOAgentsDroitsServices>)toAgentsDroitsServices()) {
			if (estService) {
				if (droit.toStructure().isService()) {
					structures.addObject(droit.toStructure());
				}
			} else {
				if (!droit.toStructure().isService()) {
					structures.addObject(droit.toStructure());
				}
			}
		}
		return structures;
	}

	/** retourne l'agent ayant le login fourni en parametre
	 * @param editingContext
	 * @param login
	 * @return null si pas d'agent trouve
	 */
	public static EOAgentPersonnel rechercherAgentAvecLogin(EOEditingContext editingContext,String login) {
		try {
			
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COMPTE_KEY + "." + EOCompte.CPT_LOGIN_KEY + " = %@", new NSArray(login.toLowerCase())));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COMPTE_KEY + "." + EOCompte.CPT_LOGIN_KEY + " = %@", new NSArray(login.toUpperCase())));			
			
			return fetchFirstByQualifier(editingContext, new EOOrQualifier(orQualifiers));
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static NSArray<EOAgentPersonnel> rechercherGestionnairesMangue(EOEditingContext editingContext) {
		return rechercherAgents(editingContext,AGT_CONS_DOSSIER_KEY,Constantes.FAUX);
	}
	public static NSArray<EOAgentPersonnel> rechercherPersonnels(EOEditingContext editingContext) {
		return rechercherAgents(editingContext,AGT_CONS_DOSSIER_KEY,Constantes.VRAI);
	}
	private static NSArray<EOAgentPersonnel> rechercherAgents(EOEditingContext editingContext,String nomAttribut,String valeurAttribut) {
		if (nomAttribut == null || valeurAttribut == null) {
			return new NSArray<EOAgentPersonnel>();
		}
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(nomAttribut + " = %@", new NSArray(valeurAttribut));		
		return fetchAll(editingContext, myQualifier);
	}
}

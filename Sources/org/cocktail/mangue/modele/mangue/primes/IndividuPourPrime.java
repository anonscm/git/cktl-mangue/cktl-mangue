/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.primes;

import org.cocktail.mangue.common.modele.nomenclatures.primes.EOPrime;
import org.cocktail.mangue.modele.FonctionPourIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

/** Comporte toutes les donn&eacute;es n&eacute;cessaires pour v&eacute;rifier si un individu peut b&eacute;n&eacute;ficier d'une prime
 * et pour &eacute;valuer le montant de la prime */
public class IndividuPourPrime implements NSKeyValueCoding {
	private EOIndividu individu;
	private EOPrime prime;
	private String cCorps,cGrade, cEchelon,cTypeContratTrav,indice;
	private FonctionPourIndividu fonction;
	private NSTimestamp dateDebut,dateFin;		// Dates pour lesquelles la prime est évaluée
	
	public IndividuPourPrime(EOIndividu individu,EOPrime prime,NSTimestamp dateDebut, NSTimestamp dateFin) {
		this.individu = individu;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.prime = prime;
	}
	// Accesseurs
	public EOIndividu individu() {
		return individu;
	}
	public EOPrime prime() {
		return prime;
	}
	public String corps() {
		return cCorps;
	}
	public void setCorps(String corps) {
		this.cCorps = corps;
	}
	public String echelon() {
		return cEchelon;
	}
	public void setCEchelon(String echelon) {
		this.cEchelon = echelon;
	}
	public void setElementCarriere(EOElementCarriere element) {
		this.cCorps = element.toCorps().cCorps();
		this.cGrade = element.toGrade().cGrade();
		this.cEchelon = element.cEchelon();
	}
	public String grade() {
		return cGrade;
	}
	public void setGrade(String grade) {
		cGrade = grade;
	}
	public String typeContratTrav() {
		return cTypeContratTrav;
	}
	public void setTypeContratTrav(String typeContratTrav) {
		cTypeContratTrav = typeContratTrav;
	}
	public String indice() {
		return indice;
	}
	public void setIndice(String indice) {
		this.indice = indice;
	}
	public FonctionPourIndividu fonction() {
		return fonction;
	}
	public void setFonction(FonctionPourIndividu fonction) {
		this.fonction = fonction;
	}
	public NSTimestamp dateDebut() {
		return dateDebut;
	}
	public void setDateDebut(NSTimestamp dateDebut) {
		this.dateDebut = dateDebut;
	}
	public NSTimestamp getDateFin() {
		return dateFin;
	}
	public void dateFin(NSTimestamp dateFin) {
		this.dateFin = dateFin;
	}
	// Interface keyValue coding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}

	
}

// _EONbi.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONbi.java instead.
package org.cocktail.mangue.modele.mangue.nbi;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EONbi extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Nbi";
	public static final String ENTITY_TABLE_NAME = "MANGUE.NBI";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cNbi";

	public static final String C_NBI_KEY = "cNbi";
	public static final String C_TYPE_NBI_KEY = "cTypeNbi";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_NBI_KEY = "dDebNbi";
	public static final String D_FIN_NBI_KEY = "dFinNbi";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NB_POINTS_KEY = "nbPoints";
	public static final String NO_TRANCHE_KEY = "noTranche";
	public static final String TEM_TYPE_NBI_KEY = "temTypeNbi";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_NBI_COLKEY = "C_NBI";
	public static final String C_TYPE_NBI_COLKEY = "C_NBI_TYPE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_NBI_COLKEY = "D_DEB_NBI";
	public static final String D_FIN_NBI_COLKEY = "D_FIN_NBI";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NB_POINTS_COLKEY = "NB_POINTS";
	public static final String NO_TRANCHE_COLKEY = "NO_TRANCHE";
	public static final String TEM_TYPE_NBI_COLKEY = "TEM_TYPE_NBI";



	// Relationships
	public static final String NBI_IMPRESSIONS_KEY = "nbiImpressions";
	public static final String NBI_OCCUPATIONS_KEY = "nbiOccupations";
	public static final String NBI_REPART_FONCTIONS_KEY = "nbiRepartFonctions";
	public static final String TO_NBI_TYPES_KEY = "toNbiTypes";
	public static final String TRANCHE_KEY = "tranche";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cNbi() {
    return (String) storedValueForKey(C_NBI_KEY);
  }

  public void setCNbi(String value) {
    takeStoredValueForKey(value, C_NBI_KEY);
  }

  public String cTypeNbi() {
    return (String) storedValueForKey(C_TYPE_NBI_KEY);
  }

  public void setCTypeNbi(String value) {
    takeStoredValueForKey(value, C_TYPE_NBI_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebNbi() {
    return (NSTimestamp) storedValueForKey(D_DEB_NBI_KEY);
  }

  public void setDDebNbi(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_NBI_KEY);
  }

  public NSTimestamp dFinNbi() {
    return (NSTimestamp) storedValueForKey(D_FIN_NBI_KEY);
  }

  public void setDFinNbi(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_NBI_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer nbPoints() {
    return (Integer) storedValueForKey(NB_POINTS_KEY);
  }

  public void setNbPoints(Integer value) {
    takeStoredValueForKey(value, NB_POINTS_KEY);
  }

  public Integer noTranche() {
    return (Integer) storedValueForKey(NO_TRANCHE_KEY);
  }

  public void setNoTranche(Integer value) {
    takeStoredValueForKey(value, NO_TRANCHE_KEY);
  }

  public String temTypeNbi() {
    return (String) storedValueForKey(TEM_TYPE_NBI_KEY);
  }

  public void setTemTypeNbi(String value) {
    takeStoredValueForKey(value, TEM_TYPE_NBI_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTypes toNbiTypes() {
    return (org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTypes)storedValueForKey(TO_NBI_TYPES_KEY);
  }

  public void setToNbiTypesRelationship(org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTypes value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTypes oldValue = toNbiTypes();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_NBI_TYPES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_NBI_TYPES_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTranches tranche() {
    return (org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTranches)storedValueForKey(TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTranches value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTranches oldValue = tranche();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TRANCHE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TRANCHE_KEY);
    }
  }
  
  public NSArray nbiImpressions() {
    return (NSArray)storedValueForKey(NBI_IMPRESSIONS_KEY);
  }

  public NSArray nbiImpressions(EOQualifier qualifier) {
    return nbiImpressions(qualifier, null);
  }

  public NSArray nbiImpressions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = nbiImpressions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToNbiImpressionsRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbiImpressions object) {
    addObjectToBothSidesOfRelationshipWithKey(object, NBI_IMPRESSIONS_KEY);
  }

  public void removeFromNbiImpressionsRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbiImpressions object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, NBI_IMPRESSIONS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.nbi.EONbiImpressions createNbiImpressionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("NbiImpressions");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, NBI_IMPRESSIONS_KEY);
    return (org.cocktail.mangue.modele.mangue.nbi.EONbiImpressions) eo;
  }

  public void deleteNbiImpressionsRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbiImpressions object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, NBI_IMPRESSIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllNbiImpressionsRelationships() {
    Enumeration objects = nbiImpressions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteNbiImpressionsRelationship((org.cocktail.mangue.modele.mangue.nbi.EONbiImpressions)objects.nextElement());
    }
  }

  public NSArray nbiOccupations() {
    return (NSArray)storedValueForKey(NBI_OCCUPATIONS_KEY);
  }

  public NSArray nbiOccupations(EOQualifier qualifier) {
    return nbiOccupations(qualifier, null, false);
  }

  public NSArray nbiOccupations(EOQualifier qualifier, boolean fetch) {
    return nbiOccupations(qualifier, null, fetch);
  }

  public NSArray nbiOccupations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation.TO_NBI_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = nbiOccupations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToNbiOccupationsRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, NBI_OCCUPATIONS_KEY);
  }

  public void removeFromNbiOccupationsRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, NBI_OCCUPATIONS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation createNbiOccupationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("NbiOccupation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, NBI_OCCUPATIONS_KEY);
    return (org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation) eo;
  }

  public void deleteNbiOccupationsRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, NBI_OCCUPATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllNbiOccupationsRelationships() {
    Enumeration objects = nbiOccupations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteNbiOccupationsRelationship((org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation)objects.nextElement());
    }
  }

  public NSArray nbiRepartFonctions() {
    return (NSArray)storedValueForKey(NBI_REPART_FONCTIONS_KEY);
  }

  public NSArray nbiRepartFonctions(EOQualifier qualifier) {
    return nbiRepartFonctions(qualifier, null, false);
  }

  public NSArray nbiRepartFonctions(EOQualifier qualifier, boolean fetch) {
    return nbiRepartFonctions(qualifier, null, fetch);
  }

  public NSArray nbiRepartFonctions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions.TO_NBI_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = nbiRepartFonctions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToNbiRepartFonctionsRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions object) {
    addObjectToBothSidesOfRelationshipWithKey(object, NBI_REPART_FONCTIONS_KEY);
  }

  public void removeFromNbiRepartFonctionsRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, NBI_REPART_FONCTIONS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions createNbiRepartFonctionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("NbiRepartFonctions");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, NBI_REPART_FONCTIONS_KEY);
    return (org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions) eo;
  }

  public void deleteNbiRepartFonctionsRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, NBI_REPART_FONCTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllNbiRepartFonctionsRelationships() {
    Enumeration objects = nbiRepartFonctions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteNbiRepartFonctionsRelationship((org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions)objects.nextElement());
    }
  }


/**
 * Créer une instance de EONbi avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EONbi createEONbi(EOEditingContext editingContext, String cNbi
, String cTypeNbi
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer nbPoints
, String temTypeNbi
, org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiTypes toNbiTypes			) {
    EONbi eo = (EONbi) createAndInsertInstance(editingContext, _EONbi.ENTITY_NAME);    
		eo.setCNbi(cNbi);
		eo.setCTypeNbi(cTypeNbi);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNbPoints(nbPoints);
		eo.setTemTypeNbi(temTypeNbi);
    eo.setToNbiTypesRelationship(toNbiTypes);
    return eo;
  }

  
	  public EONbi localInstanceIn(EOEditingContext editingContext) {
	  		return (EONbi)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONbi creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONbi creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EONbi object = (EONbi)createAndInsertInstance(editingContext, _EONbi.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EONbi localInstanceIn(EOEditingContext editingContext, EONbi eo) {
    EONbi localInstance = (eo == null) ? null : (EONbi)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EONbi#localInstanceIn a la place.
   */
	public static EONbi localInstanceOf(EOEditingContext editingContext, EONbi eo) {
		return EONbi.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EONbi fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EONbi fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EONbi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONbi)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONbi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONbi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONbi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONbi)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EONbi fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONbi eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONbi ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONbi fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

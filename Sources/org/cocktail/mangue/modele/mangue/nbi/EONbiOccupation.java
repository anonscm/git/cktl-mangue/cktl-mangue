// EONbiOccupation.java
// Created on Fri Feb 07 08:15:30  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.nbi;


import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
public class EONbiOccupation extends _EONbiOccupation {

	public static final EOSortOrdering SORT_DEBUT_DESC = new EOSortOrdering(D_DEB_NBI_OCC_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(SORT_DEBUT_DESC);
	
	/** Regles de validation<BR>
	 *  La date de debut, le nombre de points, la nbi et l'individu doivent etre fournis<BR>
	 *  Le nombre de points affectes ne doit pas etre superieur au nombre de points de la nbi<BR>
	 *  La date de debut ne peut etre anterieure a la date de debut de la nbi<BR>
	 *  La date de debut ne peut etre posterieure a la date de fin de la nbi si cette derniere est non nulle<BR>
	 *  La date de fin ne peut etre posterieure a la date de debut<BR>
	 *  La date de fin ne etre posterieure a la date de fin de la nbi<BR>
	 *  L'agent doit etre en position d'activite (ou d&eacute;tachement avec une carriere d'accueil) sur toute la duree de la bonification indiciaire<BR>
	 *  La nouvelle bonification indiciaire ne peut chevaucher le conge de formation professionnelle.<BR>
	 *  Pas de NBI si agent en CLD<BR>
	 */
    public EONbiOccupation() {
        super();
    }

	public static EONbiOccupation creer(EOEditingContext ec, EONbi nbi) {

		EONbiOccupation newObject = (EONbiOccupation) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setToNbiRelationship(nbi);
		
		return newObject;
		
	}


    public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,D_DEB_NBI_OCC_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setDDebNbiOcc(null);
		} else {
			SuperFinder.setDateFormatee(this,D_DEB_NBI_OCC_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,D_FIN_NBI_OCC_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setDFinNbiOcc(null);
		} else {
			SuperFinder.setDateFormatee(this,D_FIN_NBI_OCC_KEY,uneDate);
		}
	}
	public String jourSuivantFin() {
		if (dFinNbiOcc() == null) {
			return null;
		} else {
			return DateCtrl.dateToString(DateCtrl.jourSuivant(dFinNbiOcc()));
		}
	}
    public void initAvecNbi(EONbi nbi) {
    		setToNbiRelationship(nbi);
    		setNbPoints(nbi.nbPoints());
    }
    public void supprimerRelations() {
    		removeObjectFromBothSidesOfRelationshipWithKey(toNbi(),"toNbi");
    		removeObjectFromBothSidesOfRelationshipWithKey(toIndividu(),"toIndividu");
    }
    public void validateForSave() throws NSValidation.ValidationException {
		if (dDebNbiOcc() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date de début");
		}
		if (toNbi() == null) {
			throw new NSValidation.ValidationException("La Nbi est obligatoire"); 
		}
		if (toIndividu() == null) {
			throw new NSValidation.ValidationException("Vous devez choisir un individu"); 
		}
		if (nbPoints() == null) {
			throw new NSValidation.ValidationException("Le nombre de points est obligatoire");
		}
		if (nbPoints().intValue() > toNbi().nbPoints().intValue()) {
			throw new NSValidation.ValidationException("Le nombre de points affectés ne peut être supérieur au nombre de points maximum de la nbi");
		}
		if (dFinNbiOcc() == null && toNbi().dFinNbi() != null) {
			throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de fin de la nbi");
		}
		if (DateCtrl.isBefore(dDebNbiOcc(),toNbi().dDebNbi())) {
			throw new NSValidation.ValidationException("La date de début ne peut être antérieure à la date de début de la nbi");
		}
		if (toNbi().dFinNbi() != null && DateCtrl.isAfter(dDebNbiOcc(),toNbi().dFinNbi())) {
			throw new NSValidation.ValidationException("La date de début ne peut être postérieure à la date de fin de la nbi");
		}
		if (dFinNbiOcc() != null) {
			if (DateCtrl.isBefore(dFinNbiOcc(),dDebNbiOcc())) {
				throw new NSValidation.ValidationException("NbiOccupation - La date de début ne peut être postérieure à la date de fin");
			}
			if (toNbi().dFinNbi() != null && DateCtrl.isAfter(dFinNbiOcc(),toNbi().dFinNbi())) {
				throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de fin de la nbi");
			}
		}
		if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext(),toIndividu(),dDebNbiOcc(),dFinNbiOcc())) {
	 		if (EOChangementPosition.fonctionnaireEnActivitePendantPeriode(editingContext(),toIndividu(),dDebNbiOcc(),dFinNbiOcc()) == false)
	 		throw new NSValidation.ValidationException("L'affectation de Nbi ne concerne qu'un agent fonctionnaire titulaire ou stagiaire");
	 	} else {
	 		throw new NSValidation.ValidationException("L'affectation de Nbi doit reposer sur une position d'activité ou de détachement avec gestion de la carrière d’accueil dans l'établissement");
	 	}
		NSArray conges = Duree.rechercherDureesPourIndividuEtPeriode(editingContext(),"CongeFormation",toIndividu(),dDebNbiOcc(),dFinNbiOcc());
		if (conges != null && conges.count() > 0) {
			throw new NSValidation.ValidationException("La personne ne peut pas être en congé formation pendant la période");
		}
		conges = Duree.rechercherDureesPourIndividuEtPeriode(editingContext(),"Cld",toIndividu(),dDebNbiOcc(),dFinNbiOcc());
		if (conges != null && conges.count() > 0) {
			throw new NSValidation.ValidationException("La personne ne peut pas être en congé longue durée pendant la période");
		}
    }
    public String currentFonction() {
    		try {
    			return currentRepartFonction().toNbiFonctions().llNbiFonction();
    		} catch (Exception e) {
    			return null;
    		}
    }
    public String currentComposante() {
		try {
			return EOStructure.getComposante(editingContext(), currentRepartFonction().toStructure().cStructure()).llStructure();
		} catch (Exception e) {
			return null;
		}
    }
    // méthodes privées
    private EONbiRepartFonctions currentRepartFonction() {
	  	if (dDebNbiOcc() == null || toNbi() == null) {
	  		return null;
	  	} else {
			NSArray results = EONbiRepartFonctions.rechercherNbiRepartFonctionsPourPeriode(editingContext(),toNbi().cNbi(),dDebNbiOcc(),dDebNbiOcc()); 
			try {
				return (EONbiRepartFonctions)results.objectAtIndex(0);
			} catch(Exception e) {
				return null;
			}
	  	}
    }
    // méthodes statiques
    /** Recherche les occupations nbi avec le code nbi passe en parametre
     * @param editingContext
     * @param codeNbi code nbi
     */
    public static NSArray rechercherNbiOccupations(EOEditingContext ec,String codeNbi) {
	    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_NBI_KEY + "=%@",new NSArray(codeNbi));	    	
	    	return fetchAll(ec, qualifier, SORT_ARRAY_DEBUT_DESC);	    	
    }
    public static NSArray rechercherNbiOccupations(EOEditingContext ec,EONbi nbi) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_NBI_KEY + "=%@",new NSArray(nbi));	    	
    	return fetchAll(ec, qualifier, SORT_ARRAY_DEBUT_DESC);	    	
}
    /** Recherche les nbi occupations avec le code nbi passe en parametre
     * @param editingContext
     * @param codeNbi code nbi (peut etre nul)
     * @param debutPeriode debut periode
     * @param finPeriode fin periode (peut etre nulle)
     */
    public static NSArray rechercherNbiOccupationsPourPeriode(EOEditingContext editingContext,EONbi nbi,NSTimestamp debutPeriode,NSTimestamp finPeriode) { 
    		return rechercherNbiOccupationsPourIndividuEtPeriode(editingContext, nbi,null,debutPeriode,finPeriode);
    }
    /** Recherche les nbi occupations avec le code nbi passe en parametre
     * @param editingContext
     * @param codeNbi code nbi (peut etre nul)
     * @param individu (peut etre nul)
     * @param debutPeriode debut periode
     * @param finPeriode fin periode (peut etre nulle)
     */
    public static NSArray rechercherNbiOccupationsPourIndividuEtPeriode(EOEditingContext ec,EONbi  nbi,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    	
   		NSMutableArray qualifiers = new NSMutableArray();
    		
		if (nbi != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_NBI_KEY + " =%@", new NSArray(nbi)));
    
		if (individu != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " =%@", new NSArray(individu)));

		qualifiers.addObject(SuperFinder.qualifierPourPeriode(D_DEB_NBI_OCC_KEY,debutPeriode,D_FIN_NBI_OCC_KEY,finPeriode));

    	return fetchAll(ec, new EOAndQualifier(qualifiers), null);
    }
    /** Recherche les nbi occupations d'un individu pour une periode
     * @param editingContext
     * @param individu (peut etre nul)
     * @param debutPeriode debut periode
     * @param finPeriode fin periode (peut etre nulle)
     */
    public static NSArray rechercherNbiOccupationsPourIndividuEtPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
    		return rechercherNbiOccupationsPourIndividuEtPeriode(editingContext,null,individu,debutPeriode,finPeriode);
    }
    /** Recherche les nbi occupations avec le code nbi passe en parametre
     * @param editingContext
     * @param codeNbi code nbi
     * @param date
     */
    public static NSArray rechercherAutresOccupationsNbiEnCoursPourIndividu(EOEditingContext ec, EONbi nbi,NSTimestamp debutPeriode,NSTimestamp finPeriode,EOIndividu individu) {

    	NSMutableArray qualifiers = new NSMutableArray();    	
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_NBI_KEY + " != %@ ", new NSArray(nbi)));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@ ", new NSArray(individu)));
    	qualifiers.addObject(SuperFinder.qualifierPourPeriode(D_DEB_NBI_OCC_KEY,debutPeriode, D_FIN_NBI_OCC_KEY,finPeriode));

    	return fetchAll(ec, new EOAndQualifier(qualifiers), null);
    }
}

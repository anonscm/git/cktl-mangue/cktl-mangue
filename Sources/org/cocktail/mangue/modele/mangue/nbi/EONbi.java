// EONbi.java
// Created on Wed Feb 26 15:27:11  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.mangue.nbi;


import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EONbi extends _EONbi {
	
	public static final EOSortOrdering SORT_CODE_ASC = new EOSortOrdering(C_NBI_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_CODE_ASC = new NSArray(SORT_CODE_ASC);
	
	public static String TYPE_ADMINISTRATIVE = "A";
	public static String TYPE_TECHNIQUE = "T";

	public EONbi() {
		super();
	}


	public boolean estAdministrative() {
		return temTypeNbi().equals(TYPE_ADMINISTRATIVE);
	}
	public boolean estTechnique() {
		return temTypeNbi().equals(TYPE_TECHNIQUE);
	}


	public static EONbi creer(EOEditingContext ec) {
		EONbi newObject = new EONbi();	
		newObject.setTemTypeNbi("A");
		ec.insertObject(newObject);
		return newObject;
	}



	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,D_DEB_NBI_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setDDebNbi(null);
		} else {
			SuperFinder.setDateFormatee(this,D_DEB_NBI_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,D_FIN_NBI_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setDFinNbi(null);
		} else {
			SuperFinder.setDateFormatee(this,D_FIN_NBI_KEY,uneDate);
		}
	}
	public String noTrancheAsString() {
		if (noTranche() == null) {
			return null;
		} else {
			return noTranche().toString();
		}
	}

	public String typeNbi() {
		if (temTypeNbi().equals(TYPE_ADMINISTRATIVE))
			return "Administrative";

		return "Technique";
	}

	public void setNoTrancheAsString(String aString) {
		if (aString == null || aString.equals("")) {
			return;
		}
		setNoTranche(new Integer(aString));
	}

	public void validateForSave() throws NSValidation.ValidationException {
		if (noTranche() == null) {
			throw new NSValidation.ValidationException("Vous devez sélectionner une tranche");
		}
		if (nbPoints() == null) {
			throw new NSValidation.ValidationException("Le nombre de points est obligatoire");
		}
		if (dFinNbi() != null && DateCtrl.isBefore(dFinNbi(),dDebNbi())) {
			throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de début");
		}
	}

	/** recherche les nbi
	 * @param editingContext
	 */
	public static NSArray<EONbi> rechercherNBI(EOEditingContext ec,NSTimestamp date) {
		return fetchAll(ec, null, SORT_ARRAY_CODE_ASC);
	}
	/** recherche les nbi non terminees &agrave; la date fournie
	 * @param editingContext
	 * @param date
	 */
	public static NSArray<EONbi> rechercherNBINonFiniesADate(EOEditingContext ec,NSTimestamp date) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(D_FIN_NBI_KEY + " >= %@ or " + D_FIN_NBI_KEY + " = nil",new NSArray(date));
		return fetchAll(ec, myQualifier, SORT_ARRAY_CODE_ASC);		
	}
	/** recherche les nbi valables pour la periode passe en parametre
	 * @param editingContext
	 * @param date
	 */
	public static NSArray<EONbi> rechercherNBIPourPeriode(EOEditingContext ec,NSTimestamp dateDebut,NSTimestamp dateFin) {
		EOQualifier myQualifier = SuperFinder.qualifierPourPeriode(D_DEB_NBI_KEY, dateDebut, D_FIN_NBI_KEY, dateFin);
		return fetchAll(ec, myQualifier, SORT_ARRAY_CODE_ASC);		
	}
}

// EONbiImpressions.java
// Created on Wed Feb 26 13:46:52  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.nbi;


import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
public class EONbiImpressions extends _EONbiImpressions {

	public EONbiImpressions() {
		super();
	}

	public static Number nouveauNumeroImpressionPourAnnee(EOEditingContext editingContext,Integer annee) {
		NSMutableArray mySort = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(IMP_NUMERO_KEY, EOSortOrdering.CompareDescending));
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat( IMP_ANNEE_KEY + " = " + annee,null);
		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,mySort);
		myFetch.setFetchLimit(1);
		myFetch.setRefreshesRefetchedObjects(true);
		NSArray results = editingContext.objectsWithFetchSpecification(myFetch);
		if (results.count() > 0) {
			EONbiImpressions recordImpression = (EONbiImpressions)results.objectAtIndex(0);
			int dernierNumero = recordImpression.impNumero().intValue();
			return (Number)new Integer(dernierNumero + 1);
		}
		return 	(Number)new Integer(1);
	}

	public static boolean arretePriseDeFonctionImprimePourNbiEtPersonne(EOEditingContext editingContext,EONbi nbi,EOIndividu individu) {
		return arreteImprimePourNbiEtPersonne(editingContext,nbi,individu,ManGUEConstantes.TYPE_ARRETE_NBI_PRISE_DE_FONCTION);
	}

	/**
	 * 
	 * @param editingContext
	 * @param nbi
	 * @param individu
	 * @return
	 */
	public static boolean arreteCessationImprimePourNbiEtPersonne(EOEditingContext editingContext,EONbi nbi,EOIndividu individu) {
		return arreteImprimePourNbiEtPersonne(editingContext,nbi,individu,ManGUEConstantes.TYPE_ARRETE_NBI_CESSATION);
	}

	/**
	 * 
	 * @param ec
	 * @param nbi
	 * @param individu
	 * @param typeArrete
	 * @return
	 */
	private static boolean arreteImprimePourNbiEtPersonne(EOEditingContext ec,EONbi nbi,EOIndividu individu,String typeArrete) {

		if (nbi != null) {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_NBI_KEY + "=%@", new NSArray(nbi.cNbi())));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_DOSSIER_PERS_KEY + "=%@", new NSArray(individu.noIndividu())));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(IMP_TYPE_KEY + "=%@", new NSArray(typeArrete)));

			return (fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers))) != null;    	
		}
		
		return false;
	}
}

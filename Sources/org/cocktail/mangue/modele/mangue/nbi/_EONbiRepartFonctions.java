// _EONbiRepartFonctions.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONbiRepartFonctions.java instead.
package org.cocktail.mangue.modele.mangue.nbi;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EONbiRepartFonctions extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "NbiRepartFonctions";
	public static final String ENTITY_TABLE_NAME = "MANGUE.NBI_REPART_FONCTIONS";



	// Attributes


	public static final String C_NBI_KEY = "cNbi";
	public static final String C_NBI_FONCTION_KEY = "cNbiFonction";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_NBI_FONC_KEY = "dDebNbiFonc";
	public static final String D_FIN_NBI_FONC_KEY = "dFinNbiFonc";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARR_DEB_NBI_FONC_KEY = "noArrDebNbiFonc";
	public static final String NO_ARR_FIN_NBI_FONC_KEY = "noArrFinNbiFonc";

// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";

//Colonnes dans la base de donnees
	public static final String C_NBI_COLKEY = "C_NBI";
	public static final String C_NBI_FONCTION_COLKEY = "C_NBI_FONCTION";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_NBI_FONC_COLKEY = "D_DEB_NBI_FONC";
	public static final String D_FIN_NBI_FONC_COLKEY = "D_FIN_NBI_FONC";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NO_ARR_DEB_NBI_FONC_COLKEY = "NO_ARR_DEB_NBI_FONC";
	public static final String NO_ARR_FIN_NBI_FONC_COLKEY = "NO_ARR_FIN_NBI_FONC";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";


	// Relationships
	public static final String TO_NBI_KEY = "toNbi";
	public static final String TO_NBI_FONCTIONS_KEY = "toNbiFonctions";
	public static final String TO_STRUCTURE_KEY = "toStructure";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cNbi() {
    return (String) storedValueForKey(C_NBI_KEY);
  }

  public void setCNbi(String value) {
    takeStoredValueForKey(value, C_NBI_KEY);
  }

  public String cNbiFonction() {
    return (String) storedValueForKey(C_NBI_FONCTION_KEY);
  }

  public void setCNbiFonction(String value) {
    takeStoredValueForKey(value, C_NBI_FONCTION_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebNbiFonc() {
    return (NSTimestamp) storedValueForKey(D_DEB_NBI_FONC_KEY);
  }

  public void setDDebNbiFonc(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_NBI_FONC_KEY);
  }

  public NSTimestamp dFinNbiFonc() {
    return (NSTimestamp) storedValueForKey(D_FIN_NBI_FONC_KEY);
  }

  public void setDFinNbiFonc(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_NBI_FONC_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String noArrDebNbiFonc() {
    return (String) storedValueForKey(NO_ARR_DEB_NBI_FONC_KEY);
  }

  public void setNoArrDebNbiFonc(String value) {
    takeStoredValueForKey(value, NO_ARR_DEB_NBI_FONC_KEY);
  }

  public String noArrFinNbiFonc() {
    return (String) storedValueForKey(NO_ARR_FIN_NBI_FONC_KEY);
  }

  public void setNoArrFinNbiFonc(String value) {
    takeStoredValueForKey(value, NO_ARR_FIN_NBI_FONC_KEY);
  }

  public org.cocktail.mangue.modele.mangue.nbi.EONbi toNbi() {
    return (org.cocktail.mangue.modele.mangue.nbi.EONbi)storedValueForKey(TO_NBI_KEY);
  }

  public void setToNbiRelationship(org.cocktail.mangue.modele.mangue.nbi.EONbi value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.nbi.EONbi oldValue = toNbi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_NBI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_NBI_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiFonctions toNbiFonctions() {
    return (org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiFonctions)storedValueForKey(TO_NBI_FONCTIONS_KEY);
  }

  public void setToNbiFonctionsRelationship(org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiFonctions value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiFonctions oldValue = toNbiFonctions();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_NBI_FONCTIONS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_NBI_FONCTIONS_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOStructure toStructure() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_KEY);
    }
  }
  

/**
 * Créer une instance de EONbiRepartFonctions avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EONbiRepartFonctions createEONbiRepartFonctions(EOEditingContext editingContext, String cNbi
, String cNbiFonction
, NSTimestamp dCreation
, NSTimestamp dDebNbiFonc
, NSTimestamp dModification
, org.cocktail.mangue.modele.mangue.nbi.EONbi toNbi			) {
    EONbiRepartFonctions eo = (EONbiRepartFonctions) createAndInsertInstance(editingContext, _EONbiRepartFonctions.ENTITY_NAME);    
		eo.setCNbi(cNbi);
		eo.setCNbiFonction(cNbiFonction);
		eo.setDCreation(dCreation);
		eo.setDDebNbiFonc(dDebNbiFonc);
		eo.setDModification(dModification);
    eo.setToNbiRelationship(toNbi);
    return eo;
  }

  
	  public EONbiRepartFonctions localInstanceIn(EOEditingContext editingContext) {
	  		return (EONbiRepartFonctions)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONbiRepartFonctions creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONbiRepartFonctions creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EONbiRepartFonctions object = (EONbiRepartFonctions)createAndInsertInstance(editingContext, _EONbiRepartFonctions.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EONbiRepartFonctions localInstanceIn(EOEditingContext editingContext, EONbiRepartFonctions eo) {
    EONbiRepartFonctions localInstance = (eo == null) ? null : (EONbiRepartFonctions)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EONbiRepartFonctions#localInstanceIn a la place.
   */
	public static EONbiRepartFonctions localInstanceOf(EOEditingContext editingContext, EONbiRepartFonctions eo) {
		return EONbiRepartFonctions.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EONbiRepartFonctions fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EONbiRepartFonctions fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EONbiRepartFonctions eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONbiRepartFonctions)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONbiRepartFonctions fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONbiRepartFonctions fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONbiRepartFonctions eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONbiRepartFonctions)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EONbiRepartFonctions fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONbiRepartFonctions eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONbiRepartFonctions ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONbiRepartFonctions fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

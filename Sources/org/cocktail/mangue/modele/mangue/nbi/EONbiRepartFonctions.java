//EONbiRepartFonctions.java
//Created on Fri Feb 07 08:15:23  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.nbi;


import org.cocktail.mangue.common.modele.nomenclatures.nbi.EONbiFonctions;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
public class EONbiRepartFonctions extends _EONbiRepartFonctions {

	public static final EOSortOrdering SORT_DEBUT_DESC = new EOSortOrdering(D_DEB_NBI_FONC_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(SORT_DEBUT_DESC);

	public EONbiRepartFonctions() {
		super();
	}

	public static EONbiRepartFonctions creer(EOEditingContext ec, EONbi nbi) {

		EONbiRepartFonctions newObject = (EONbiRepartFonctions) createAndInsertInstance(ec, ENTITY_NAME);    
		
		newObject.setCNbi(nbi.cNbi());
		newObject.setToNbiRelationship(nbi);
		
		return newObject;
		
	}


	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,"dDebNbiFonc");
	}
	public void setDateDebutFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dDebNbiFonc",uneDate);
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,"dFinNbiFonc");
	}
	public void setDateFinFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dFinNbiFonc",uneDate);
	}
	public void initAvecNbi(EONbi nbi) {
		addObjectToBothSidesOfRelationshipWithKey(nbi,"toNbi");
		setCNbi(nbi.cNbi());
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (dDebNbiFonc() == null) {
			throw new NSValidation.ValidationException("La date de début est obligatoire");
		}
		if (toStructure() == null) {
			throw new NSValidation.ValidationException("Vous devez sélectionner une structure");
		}
	}
	public String composante() {
		EOStructure structure = EOStructure.getComposante(editingContext(), toStructure().cStructure());
		if (structure != null) {
			return structure.lcStructure();
		} else {
			return null;
		}
	}
	/** Recherche les nbi repart fonction avec le code nbi passe en parametre
	 * @param editingContext
	 * @param nbi
	 */
	public static NSArray rechercherNbiRepartFonctions(EOEditingContext ec,EONbi nbi) {

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_NBI_KEY + "=%@",new NSArray(nbi));		
		return fetchAll(ec, qualifier, SORT_ARRAY_DEBUT_DESC);

	}
	
	public static NSArray rechercherNbiRepartFonctions(EOEditingContext ec,EONbiFonctions fonction) {

		try {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_NBI_FONCTIONS_KEY + "=%@",new NSArray(fonction));
		return fetchAll(ec, qualifier, SORT_ARRAY_DEBUT_DESC);
		}
		catch (Exception e) {
			return new NSArray();
		}

	}

	/** Recherche les nbi repart fonction avec le code nbi pass&eacute; en parametre
	 * @param editingContext
	 * @param codeNbi code nbi
	 * @param dateDebut debut periode
	 * @param dateFin fin periode
	 */
	public static NSArray rechercherNbiRepartFonctionsPourPeriode(EOEditingContext editingContext,String codeNbi,NSTimestamp dateDebut,NSTimestamp dateFin) {
		NSMutableArray args = new NSMutableArray(codeNbi);
		args.addObject(dateDebut);
		args.addObject(dateFin);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("cNbi = %@ and dDebNbiFonc <= %@ and (dFinNbiFonc >= %@ or dFinNbiFonc = nil)",args);
		EOFetchSpecification fs = new EOFetchSpecification("NbiRepartFonctions",qualifier,null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
}

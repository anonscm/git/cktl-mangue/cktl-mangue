// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   EOCirData.java

package org.cocktail.mangue.modele.mangue.cir;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.server.metier.mangue.cir:
//            _EOCirData, EOCirFichierImport

public class EOCirCarriereData extends _EOCirCarriereData
{

	public static NSArray SORT_CLASSEMENT_DESC = new NSArray(new EOSortOrdering(CIRCD_CLASSEMENT_KEY, EOSortOrdering.CompareDescending));
	public static NSArray SORT_CLASSEMENT_ASC = new NSArray(new EOSortOrdering(CIRCD_CLASSEMENT_KEY, EOSortOrdering.CompareAscending));
	
	public static final String RUBRIQUE_IDENTITE = "IdentiteAgent";

    public EOCirCarriereData()
    {
    }
    
    public static EOCirCarriereData findForCarriereEtClassement(EOEditingContext ec, EOCirCarriere carriere, Integer classement )  {
    	
    	NSMutableArray qualifiers = new NSMutableArray();
    	
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_CARRIERE_KEY + "=%@", new NSArray(carriere)));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIRCD_CLASSEMENT_KEY + "=%@", new NSArray(classement)));
    	
    	return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
    	
    	
    }

    /**
     * 
     * @param editingContext
     * @param carriere
     * @return
     */
    public static NSArray recordsPourCirCarriere(EOEditingContext editingContext, EOCirCarriere carriere) {
        NSMutableArray mesQualifiers = new NSMutableArray();
        mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_CARRIERE_KEY + " = %@", new NSArray(carriere)));

        return fetchAll(editingContext, new EOAndQualifier(mesQualifiers), SORT_CLASSEMENT_ASC);
    }

    public static NSArray recordsPourFichierEtIndividu(EOEditingContext ec, EOCirFichierImport fichierCir, EOIndividu individu ) {
    	
    	NSMutableArray qualifiers = new NSMutableArray();
    	
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_CARRIERE_KEY + "."+ EOCirCarriere.CIR_FICHIER_IMPORT_KEY + "=%@", new NSArray(fichierCir)));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_CARRIERE_KEY + "."+ EOCirCarriere.TO_INDIVIDU_KEY + "=%@", new NSArray(individu)));

    	return fetchAll(ec, new EOAndQualifier(qualifiers),SORT_CLASSEMENT_ASC);	
    	
    }
    
    /**
     * 
     * @param quotite
     * @return
     */
    public static String formaterQuotite(BigDecimal quotite) {    	
        String result = ((quotite.multiply(ManGUEConstantes.QUOTITE_100)).setScale(0)).toString();
        result = StringCtrl.replace(result, ".", "");
        return StringCtrl.stringCompletion(result, 5, "0", "G");
    }

    /**
     * 
     * @param nbAnnees
     * @param nbMois
     * @param nbJours
     * @return
     */
    public static String formaterDuree(int nbAnnees, int nbMois, int nbJours) {
    	if (nbJours == 30) {
    		nbJours = 0;
    		nbMois++;
    	}
    	if (nbMois == 12) {
    		nbMois = 0;
    		nbAnnees++;
    	}
        return dureeSur2Caracteres(new Integer(nbAnnees)) + dureeSur2Caracteres(new Integer(nbMois)) + dureeSur2Caracteres(new Integer(nbJours));
    }

    /**
     * 
     * @param duree
     * @return
     */
    private static String dureeSur2Caracteres(Number duree)
    {
        if(duree == null)
            return "00";
        int entier = duree.intValue();
        if(entier <= 9)
            return (new StringBuilder("0")).append(entier).toString();
        else
            return (new StringBuilder()).append(entier).toString();
    }

    public void validateForInsert()
        throws com.webobjects.foundation.NSValidation.ValidationException
    {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate()
        throws com.webobjects.foundation.NSValidation.ValidationException
    {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete()
        throws com.webobjects.foundation.NSValidation.ValidationException
    {
        super.validateForDelete();
    }

    public void validateObjectMetier()
        throws com.webobjects.foundation.NSValidation.ValidationException
    {
    	
    	if (dCreation() == null)
    		setDCreation(new NSTimestamp());
    	
    	setDModification(new NSTimestamp());
    	
    }

    public void validateBeforeTransactionSave()
        throws com.webobjects.foundation.NSValidation.ValidationException
    {
    }

}

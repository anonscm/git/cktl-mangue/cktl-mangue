/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.cir;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOCirCarriere extends _EOCirCarriere {

	private static final long serialVersionUID = 1L;
	public static final NSArray SORT_IND_NOM_USUEL = new NSArray(new EOSortOrdering(TO_INDIVIDU_KEY+"."+EOIndividu.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
	public static final NSArray SORT_ANNEE_FICHIER_DESC = new NSArray(new EOSortOrdering(CIR_FICHIER_IMPORT_KEY+"."+EOCirFichierImport.CFIM_ANNEE_KEY, EOSortOrdering.CompareDescending));

	public EOCirCarriere() {
		super();
	}

	public boolean estValide() {
		return temValide().equals("O");
	}

	public static EOCirCarriere creer(EOEditingContext ec, EOCirFichierImport fichier, EOIndividu individu) {

		EOCirCarriere newObject = new EOCirCarriere();
		newObject.setToIndividuRelationship(individu);
		newObject.setCirFichierImportRelationship(fichier);

		newObject.setTemValide("O");

		ec.insertObject(newObject);

		return newObject;

	}
	
	public static EOCirCarriere carrierePourFichierEtIndividu(EOEditingContext edc, EOCirFichierImport fichier, EOIndividu individu) {

		try {
			NSMutableArray andQualifiers = new NSMutableArray();

			if (fichier != null)
				andQualifiers.addObject(CocktailFinder.getQualifierEqual(CIR_FICHIER_IMPORT_KEY, fichier));
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(TO_INDIVIDU_KEY, individu));

			return fetchFirstByQualifier(edc, new EOAndQualifier(andQualifiers), SORT_ANNEE_FICHIER_DESC);
		}
		catch (Exception e){
			e.printStackTrace();
			return null;
		}

	}

	public void initAvecFichierImportRubriqueEtIndividu(EOCirFichierImport cirFichierImport, EOIndividu individu)
	{
		setCirFichierImportRelationship(cirFichierImport);
		setToIndividuRelationship(individu);

		NSTimestamp today = new NSTimestamp();
	}

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
		    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		    fetchSpec.setIsDeep(true);
		    fetchSpec.setUsesDistinct(distinct);
		    if (prefetchs != null)
		    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
		    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
		    return eoObjects;
		  }

	public static NSArray<EOCirCarriere> individusPourFichierImport(EOEditingContext editingContext, EOCirFichierImport fichier)
	{
		try {
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_FICHIER_IMPORT_KEY + " = %@ ", new NSArray(fichier)));
			
			return fetchAll(editingContext, new EOAndQualifier(mesQualifiers), SORT_IND_NOM_USUEL, false, new NSArray(TO_INDIVIDU_KEY));
		}
		catch(Exception ex)
		{
			return new NSArray();
		}
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}


	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

        if(cirFichierImport() == null)
            throw new com.webobjects.foundation.NSValidation.ValidationException("Le fichier import est obligatoire");

        if(toIndividu() == null)
            throw new com.webobjects.foundation.NSValidation.ValidationException("L'individu est obligatoire");
        
		if (dCreation() == null)
			setDCreation(new NSTimestamp());

		setDModification(new NSTimestamp());

	}

	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

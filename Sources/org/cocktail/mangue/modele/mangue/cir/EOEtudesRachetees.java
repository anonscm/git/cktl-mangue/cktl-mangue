/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.cir;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOEtudesRachetees extends _EOEtudesRachetees {

	public static NSArray SORT_DATE_DEBUT_ASC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));

    public EOEtudesRachetees() {
        super();
    }
    
	public static EOEtudesRachetees creer(EOEditingContext ec, EOIndividu individu) {

		EOEtudesRachetees newObject = (EOEtudesRachetees) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.setIndividuRelationship(individu);

		newObject.setTemValide(CocktailConstantes.VRAI);
		newObject.setTemPcAcquitees(CocktailConstantes.VRAI);
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		return newObject;
	}


	public boolean estPcAcquittees() {
		return temPcAcquitees().equals(CocktailConstantes.VRAI);
	}
	public void setEstPcAcquittees(boolean yn) {

		if (yn)
			setTemPcAcquitees(CocktailConstantes.VRAI);
		else
			setTemPcAcquitees(CocktailConstantes.FAUX);

	}

    public String dureeLiquidable() {
    	if (erDureeLiquidable() != null)
    		return StringCtrl.stringCompletion(erDureeLiquidable().toString(), 4, "0", "G");
    	return null;
    }
    public String dureeAssurance() {
    	if (erDureeAssurance() != null)
    		return StringCtrl.stringCompletion(erDureeAssurance().toString(), 4, "0", "G");
    	return null;
    }
    public String dureeConstitutive() {
    	if (erDureeConstitutive() != null)
    		return StringCtrl.stringCompletion(erDureeConstitutive().toString(), 4, "0", "G");
    	return null;
    }
    public String retenuesAcquittees() {
    	if (temPcAcquitees().equals("O"))
    		return "1";
    	
    	return "0";
    }
    
    
	public static NSArray findForIndividu(EOEditingContext ec, EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_DATE_DEBUT_ASC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}


	/**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {


		if (individu() == null) {
			throw new NSValidation.ValidationException("Aucun individu associé");
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de début");
		}
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de fin");
		}
		if (DateCtrl.isBefore(dateFin(), dateDebut())) {
			throw new NSValidation.ValidationException("La date de début doit être antérieure à la date de fin");
		}
		if (dateRachat() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de rachat");
		}
		if (DateCtrl.isBefore(dateRachat(), dateFin())) {
			throw new NSValidation.ValidationException("La date de rachat (" + DateCtrl.dateToString(dateRachat())+ ") doit être postérieure à la date de fin (" + DateCtrl.dateToString(dateFin())+ ")");
		}

		if (erDureeAssurance() == null && erDureeConstitutive() == null && erDureeLiquidable() == null) {
			throw new NSValidation.ValidationException("Aucune durée n'a été renseignée !");
		}
	
    }
    
	public String validationsCir() {

		String temp = "";
		
		if (erDureeAssurance() == null)
			temp = "La durée d'assurance doit être renseignée !";
		if (erDureeLiquidable() == null)
			temp = "La durée liquidable doit être renseignée !";
		if (erDureeConstitutive() == null)
			temp = "La durée constitutive doit être renseignée !";

		return temp;
	}
    
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.cir;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOBonifications extends _EOBonifications {

	public static NSArray SORT_DATE_DEBUT_ASC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));

	public EOBonifications() {
		super();
	}
	public static NSArray<EOBonifications> findForIndividu(EOEditingContext ec, EOIndividu individu) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_DATE_DEBUT_ASC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOBonifications> findForIndividuAnterieuresADate(EOEditingContext ec, EOIndividu individu, NSTimestamp dateReference) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + "<=%@", new NSArray(dateReference)));

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_DATE_DEBUT_ASC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	public static EOBonifications creer(EOEditingContext ec, EOIndividu individu) {

		EOBonifications newObject = (EOBonifications) createAndInsertInstance(ec, EOBonifications.ENTITY_NAME);    

		newObject.setIndividuRelationship(individu);
		newObject.setTemValide("O");
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		return newObject;
	}

	/*
		CIR
	 */
	public String natureBonificationCir() {		
		return natureBonification().nboCode();		
	}
	public String tauxBonificationCir() {	

		if (natureBonification() != null && territoire() != null && 
				(natureBonification().estBeneficeCampagne() || natureBonification().estHorsEurope()) )
			return territoire().toTaux().nbtCode();

		return null;
	}
	public String territoireBonificationCir() {		
		if (territoire() != null) {
			return territoire().nbotCode();		
		}		
		return null;
	}

	public String positionAgent() {
		return null;
	}


	public String dureeCongeBonification() {
		return null;
	}
	public String dateDebutCongeBonification() {
		return null;
	}
	public String dateFinCongeBonification() {
		return null;
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}


	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de début !");
		}
		
		String message = validationsCir();
		if (message != null && message.length() > 0)
			throw new NSValidation.ValidationException(message);
		
	}

	/**
	 * 
	 * @return
	 */
	public String validationsCir() {
		
		if ( natureBonification().estHorsEurope() && territoire() == null)
			return "BONIF - Pour une bonification hors d'europe, le territoire est obligatoire !";

		return null;
		
	}
	
	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}


package org.cocktail.mangue.modele.mangue.cir;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCivilite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EOCirIdentite extends _EOCirIdentite {

	public static final EOSortOrdering SORT_ANNEE_DESC = new EOSortOrdering(CIR_ANNEE_KEY, EOSortOrdering.CompareDescending);
	public static final EOSortOrdering SORT_ANNEE_ASC = new EOSortOrdering(CIR_ANNEE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_ANNEE_DESC = new NSArray(SORT_ANNEE_DESC);
	public static final NSArray SORT_ARRAY_ANNEE_ASC = new NSArray(SORT_ANNEE_ASC);

	
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOCirIdentite> findForQualifier(EOEditingContext edc, EOQualifier qualifier)	{
		try	{
			return fetchAll(edc, qualifier, null, true, new NSArray(TO_INDIVIDU_KEY));
		}
		catch(Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}
	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOCirIdentite> findForAnnee(EOEditingContext edc, Integer exercice)	{
		try	{
			NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_ANNEE_KEY + " = %@", new NSArray(exercice)));

			return fetchAll(edc, new EOAndQualifier(mesQualifiers), null, true, new NSArray(TO_INDIVIDU_KEY));
		}
		catch(Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOCirIdentite findCompteCertifiePourIndividu(EOEditingContext ec, EOIndividu individu)	{
		try	{
			NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY + " = %@", new NSArray(individu)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_VALIDE_KEY + " = %@", new NSArray("O")));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY+"."+EOIndividu.PERSONNELS_KEY+"."+EOPersonnel.CIR_D_CERTIFICATION_KEY + " != nil", null));
			return fetchFirstByQualifier(ec, new EOAndQualifier(mesQualifiers), SORT_ARRAY_ANNEE_ASC);
		}
		catch(Exception e)	{
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * 
	 * @param editingContext
	 * @param annee
	 * @param individu
	 * @return
	 */
	public static EOCirIdentite rechercherComptePourIndividu(EOEditingContext editingContext, Integer annee, EOIndividu individu)	{

		try {

			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_ANNEE_KEY  + " = %@", new NSArray(annee)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY  + " = %@", new NSArray(individu)));

			return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers), null);

		}
		catch(Exception e) {
			return null;
		}
	}


	public boolean estCertifie()
	{
		return estValide() && toIndividu().personnel().cirDCertification() != null;
	}

	public boolean estValide()
	{
		return cirValide() != null && cirValide().equals("O");
	}

	public void setEstValide(boolean aBool)
	{
		if(aBool)
			setCirValide("O");
		else
			setCirValide("N");
	}

	
	/**
	 * 
	 * @return
	 */
	private boolean controleFormatIdentite(String nom) {

		// Pas de caracteres speciaux dans les noms
		if (StringCtrl.containsIgnoreCase(nom, "_")
				|| StringCtrl.containsIgnoreCase(nom, "*")) {
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @param individu
	 * @param annee
	 */
	public void reinitAvecIndividu(EOIndividu individu, Integer annee) {

		setEstValide(true);

		setCirAnnee(annee);
		setCirDateRef(DateCtrl.today());

		if(individu != null) {

			setNomUsuel(individu.nomUsuel());

			if (individu.nomPatronymique() != null) {
				
				if (controleFormatIdentite(individu.nomUsuel()) == false || controleFormatIdentite(individu.prenom()) == false) {
					setEstValide(false);
				}
				
				setNomPatronymique(individu.nomPatronymique());
			}
			else
				setNomPatronymique(nomUsuel());

			setPrenomUsuel(individu.prenom());
			String prenoms = individu.prenom();
			if(individu.prenom2() != null && individu.prenom2().length() < 50 - individu.prenom().length() - 1)
				prenoms = (new StringBuilder(String.valueOf(individu.prenom()))).append(",").append(individu.prenom2()).toString();
			setPrenoms(prenoms);
			EOGenericRecord civilite = SuperFinder.rechercherObjetAvecAttributEtValeurEgale(individu.editingContext(), EOCivilite.ENTITY_NAME, EOCivilite.C_CIVILITE_KEY, individu.cCivilite());
			setSexe((String)civilite.valueForKey("sexeOnp"));

			if(individu.prendreEnCompteNumeroProvisoire())
				setIndNoInsee(individu.indNoInseeProv());
			else
				setIndNoInsee(individu.indNoInsee());

			if (indNoInsee() != null && indNoInsee().length() != 13) {
				setEstValide(false);
			}

			if(individu.neEnFrance()) {
				String villeDeNaissance = individu.villeDeNaissance();

				if (villeDeNaissance != null && villeDeNaissance.length() > 30) {
					setVilleDeNaissance(villeDeNaissance.substring(0,30));
				}
			}

			if(individu.toDepartement() != null)
				setDepartementRelationship(individu.toDepartement());
			else
				if(departement() != null)
					removeObjectFromBothSidesOfRelationshipWithKey(departement(), DEPARTEMENT_KEY);

			if(individu.toPaysNaissance() != null)
				setPaysNaissanceRelationship(individu.toPaysNaissance());
			else
				if(paysNaissance() != null)
					removeObjectFromBothSidesOfRelationshipWithKey(paysNaissance(), PAYS_NAISSANCE_KEY);

			setDNaissance(individu.dateNaissanceFormatee());

			if(individu.personnel() != null)
				setNumen(individu.personnel().numen());
			else
				setNumen(null);
		}
		setDModification(DateCtrl.today());
	}

	/**
	 * 
	 * @param individu
	 * @param annee
	 */
	public void initAvecIndividu(EOIndividu individu, Integer annee) {
		setDCreation(new NSTimestamp());
		setEstValide(true);
		setToIndividuRelationship(individu);
		reinitAvecIndividu(individu, annee);
	}

	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if(dNaissance() == null)
			throw new com.webobjects.foundation.NSValidation.ValidationException("La date de naissance doit être fournie");

		return;
	}

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static NSArray<EOCirIdentite> comptesValidesPourAnnee(EOEditingContext edc, Integer exercice)	{
		try	{
			NSMutableArray mesQualifiers = new NSMutableArray();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_ANNEE_KEY + " = %@", new NSArray(exercice)));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_VALIDE_KEY + " = %@", new NSArray("O")));

			return fetchAll(edc, new EOAndQualifier(mesQualifiers), null, true, new NSArray(TO_INDIVIDU_KEY));
		}
		catch(Exception e)	{
			e.printStackTrace();
			return new NSArray();
		}
	}


	/**
	 * 
	 * @param editingContext
	 * @param noInsee
	 * @return
	 */
	public static NSArray<EOCirIdentite> rechercherComptesPourInsee(EOEditingContext editingContext, String noInsee) {

		try {

			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(IND_NO_INSEE_KEY + " = %@", new NSArray(noInsee)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CIR_VALIDE_KEY + " = %@", new NSArray("O")));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers), SORT_ARRAY_ANNEE_DESC);

		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}

	}



	public void validateForInsert() throws com.webobjects.foundation.NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate()
			throws com.webobjects.foundation.NSValidation.ValidationException
			{
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
			}

	public void validateForDelete()
			throws com.webobjects.foundation.NSValidation.ValidationException
			{
		super.validateForDelete();
			}

	public void validateObjectMetier()
			throws com.webobjects.foundation.NSValidation.ValidationException
			{
			}

	public void validateBeforeTransactionSave()
			throws com.webobjects.foundation.NSValidation.ValidationException
			{
			}
}

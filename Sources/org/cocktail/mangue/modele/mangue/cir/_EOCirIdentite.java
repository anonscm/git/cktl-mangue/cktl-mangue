/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCirIdentite.java instead.
package org.cocktail.mangue.modele.mangue.cir;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCirIdentite extends  EOGenericRecord {
	public static final String ENTITY_NAME = "CirIdentite";
	public static final String ENTITY_TABLE_NAME = "MANGUE.CIR_IDENTITE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cirOrdre";

	public static final String CIR_ANNEE_KEY = "cirAnnee";
	public static final String CIR_COMMENTAIRES_KEY = "cirCommentaires";
	public static final String CIR_DATE_CERTIF_KEY = "cirDateCertif";
	public static final String CIR_DATE_COMPLETUDE_KEY = "cirDateCompletude";
	public static final String CIR_DATE_REF_KEY = "cirDateRef";
	public static final String CIR_DATE_VERIFICATION_KEY = "cirDateVerification";
	public static final String CIR_VALIDE_KEY = "cirValide";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String NUMEN_KEY = "numen";
	public static final String PRENOMS_KEY = "prenoms";
	public static final String PRENOM_USUEL_KEY = "prenomUsuel";
	public static final String SEXE_KEY = "sexe";
	public static final String VILLE_DE_NAISSANCE_KEY = "villeDeNaissance";

// Attributs non visibles
	public static final String C_DEPT_NAISSANCE_KEY = "cDeptNaissance";
	public static final String C_PAYS_NAISSANCE_KEY = "cPaysNaissance";
	public static final String CIR_ORDRE_KEY = "cirOrdre";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";

//Colonnes dans la base de donnees
	public static final String CIR_ANNEE_COLKEY = "CIR_ANNEE";
	public static final String CIR_COMMENTAIRES_COLKEY = "CIR_COMMENTAIRES";
	public static final String CIR_DATE_CERTIF_COLKEY = "CIR_DATE_CERTIF";
	public static final String CIR_DATE_COMPLETUDE_COLKEY = "CIR_DATE_COMPLETUDE";
	public static final String CIR_DATE_REF_COLKEY = "CIR_DATE_REF";
	public static final String CIR_DATE_VERIFICATION_COLKEY = "CIR_DATE_VERIFICATION";
	public static final String CIR_VALIDE_COLKEY = "CIR_VALIDE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
	public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String NUMEN_COLKEY = "NUMEN";
	public static final String PRENOMS_COLKEY = "PRENOMS";
	public static final String PRENOM_USUEL_COLKEY = "PRENOM_USUEL";
	public static final String SEXE_COLKEY = "SEXE";
	public static final String VILLE_DE_NAISSANCE_COLKEY = "VILLE_DE_NAISSANCE";

	public static final String C_DEPT_NAISSANCE_COLKEY = "C_DEPT_NAISSANCE";
	public static final String C_PAYS_NAISSANCE_COLKEY = "C_PAYS_NAISSANCE";
	public static final String CIR_ORDRE_COLKEY = "CIR_ORDRE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";


	// Relationships
	public static final String DEPARTEMENT_KEY = "departement";
	public static final String PAYS_NAISSANCE_KEY = "paysNaissance";
	public static final String TO_INDIVIDU_KEY = "toIndividu";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer cirAnnee() {
    return (Integer) storedValueForKey(CIR_ANNEE_KEY);
  }

  public void setCirAnnee(Integer value) {
    takeStoredValueForKey(value, CIR_ANNEE_KEY);
  }

  public String cirCommentaires() {
    return (String) storedValueForKey(CIR_COMMENTAIRES_KEY);
  }

  public void setCirCommentaires(String value) {
    takeStoredValueForKey(value, CIR_COMMENTAIRES_KEY);
  }

  public NSTimestamp cirDateCertif() {
    return (NSTimestamp) storedValueForKey(CIR_DATE_CERTIF_KEY);
  }

  public void setCirDateCertif(NSTimestamp value) {
    takeStoredValueForKey(value, CIR_DATE_CERTIF_KEY);
  }

  public NSTimestamp cirDateCompletude() {
    return (NSTimestamp) storedValueForKey(CIR_DATE_COMPLETUDE_KEY);
  }

  public void setCirDateCompletude(NSTimestamp value) {
    takeStoredValueForKey(value, CIR_DATE_COMPLETUDE_KEY);
  }

  public NSTimestamp cirDateRef() {
    return (NSTimestamp) storedValueForKey(CIR_DATE_REF_KEY);
  }

  public void setCirDateRef(NSTimestamp value) {
    takeStoredValueForKey(value, CIR_DATE_REF_KEY);
  }

  public NSTimestamp cirDateVerification() {
    return (NSTimestamp) storedValueForKey(CIR_DATE_VERIFICATION_KEY);
  }

  public void setCirDateVerification(NSTimestamp value) {
    takeStoredValueForKey(value, CIR_DATE_VERIFICATION_KEY);
  }

  public String cirValide() {
    return (String) storedValueForKey(CIR_VALIDE_KEY);
  }

  public void setCirValide(String value) {
    takeStoredValueForKey(value, CIR_VALIDE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String dNaissance() {
    return (String) storedValueForKey(D_NAISSANCE_KEY);
  }

  public void setDNaissance(String value) {
    takeStoredValueForKey(value, D_NAISSANCE_KEY);
  }

  public String indNoInsee() {
    return (String) storedValueForKey(IND_NO_INSEE_KEY);
  }

  public void setIndNoInsee(String value) {
    takeStoredValueForKey(value, IND_NO_INSEE_KEY);
  }

  public String nomPatronymique() {
    return (String) storedValueForKey(NOM_PATRONYMIQUE_KEY);
  }

  public void setNomPatronymique(String value) {
    takeStoredValueForKey(value, NOM_PATRONYMIQUE_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public String numen() {
    return (String) storedValueForKey(NUMEN_KEY);
  }

  public void setNumen(String value) {
    takeStoredValueForKey(value, NUMEN_KEY);
  }

  public String prenoms() {
    return (String) storedValueForKey(PRENOMS_KEY);
  }

  public void setPrenoms(String value) {
    takeStoredValueForKey(value, PRENOMS_KEY);
  }

  public String prenomUsuel() {
    return (String) storedValueForKey(PRENOM_USUEL_KEY);
  }

  public void setPrenomUsuel(String value) {
    takeStoredValueForKey(value, PRENOM_USUEL_KEY);
  }

  public String sexe() {
    return (String) storedValueForKey(SEXE_KEY);
  }

  public void setSexe(String value) {
    takeStoredValueForKey(value, SEXE_KEY);
  }

  public String villeDeNaissance() {
    return (String) storedValueForKey(VILLE_DE_NAISSANCE_KEY);
  }

  public void setVilleDeNaissance(String value) {
    takeStoredValueForKey(value, VILLE_DE_NAISSANCE_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.EODepartement departement() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EODepartement)storedValueForKey(DEPARTEMENT_KEY);
  }

  public void setDepartementRelationship(org.cocktail.mangue.common.modele.nomenclatures.EODepartement value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EODepartement oldValue = departement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPARTEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPARTEMENT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOPays paysNaissance() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOPays)storedValueForKey(PAYS_NAISSANCE_KEY);
  }

  public void setPaysNaissanceRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOPays value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOPays oldValue = paysNaissance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAYS_NAISSANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAYS_NAISSANCE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
    }
  }
  

/**
 * Créer une instance de EOCirIdentite avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCirIdentite createEOCirIdentite(EOEditingContext editingContext, Integer cirAnnee
, NSTimestamp cirDateRef
, NSTimestamp dCreation
, NSTimestamp dModification
, String indNoInsee
, String nomUsuel
, String prenoms
, String sexe
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu			) {
    EOCirIdentite eo = (EOCirIdentite) createAndInsertInstance(editingContext, _EOCirIdentite.ENTITY_NAME);    
		eo.setCirAnnee(cirAnnee);
		eo.setCirDateRef(cirDateRef);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setIndNoInsee(indNoInsee);
		eo.setNomUsuel(nomUsuel);
		eo.setPrenoms(prenoms);
		eo.setSexe(sexe);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  
	  public EOCirIdentite localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCirIdentite)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCirIdentite creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCirIdentite creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCirIdentite object = (EOCirIdentite)createAndInsertInstance(editingContext, _EOCirIdentite.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCirIdentite localInstanceIn(EOEditingContext editingContext, EOCirIdentite eo) {
    EOCirIdentite localInstance = (eo == null) ? null : (EOCirIdentite)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCirIdentite#localInstanceIn a la place.
   */
	public static EOCirIdentite localInstanceOf(EOEditingContext editingContext, EOCirIdentite eo) {
		return EOCirIdentite.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }
      public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
        EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
        fetchSpec.setIsDeep(true);
        fetchSpec.setUsesDistinct(distinct);
        if (prefetchs != null)
            fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
        NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
        return eoObjects;
      }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCirIdentite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCirIdentite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCirIdentite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCirIdentite)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCirIdentite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCirIdentite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCirIdentite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCirIdentite)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCirIdentite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCirIdentite eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCirIdentite ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCirIdentite fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

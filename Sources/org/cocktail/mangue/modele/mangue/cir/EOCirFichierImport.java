// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   EOCirFichierImport.java

package org.cocktail.mangue.modele.mangue.cir;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// Referenced classes of package org.cocktail.mangue.server.mestier.mangue.cir:
//            _EOCirFichierImport

public class EOCirFichierImport extends _EOCirFichierImport
{
	public static final EOSortOrdering SORT_ANNEE_DESC = new EOSortOrdering( CFIM_ANNEE_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_ANNEE_DESC = new NSArray<EOSortOrdering>(SORT_ANNEE_DESC);

	public static final EOSortOrdering SORT_DATE_DESC = new EOSortOrdering( CFIM_DATE_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_DATE_DESC = new NSArray<EOSortOrdering>(SORT_DATE_DESC);

	private static final long serialVersionUID = 1L;
	public static final String EXTENSION_POUR_DECLARATION_IDENTITE = "IDE";
	public static final String EXTENSION_POUR_CARRIERE = "CAR";
	public static final String TYPE_FICHIER_IDENTITE = "D";
	public static final String TYPE_FICHIER_CAMPAGNE = "C";
	public static final String TYPE_FICHIER_COMPLEMENTAIRE = "I";
	public static final String TYPE_FICHIER_RECTIFICATIF = "U";
	public static final String LL_TYPE_FICHIER_CAMPAGNE = "CAMPAGNE";
	public static final String LL_TYPE_FICHIER_COMPLEMENTAIRE = "COMPLEMENTAIRE";
	public static final String LL_TYPE_FICHIER_RECTIFICATIF = "RECTIFICATIF";

	public EOCirFichierImport()
	{
	}

	/**
	 * 
	 * @return
	 */
	public boolean isProgramme() {
		return cfimHeureCalcul() != null;
	}
	
	/**
	 * 
	 * @param ec
	 * @param annee
	 * @return
	 */
	public static EOCirFichierImport creer(EOEditingContext ec, Integer annee) {

		EOCirFichierImport newObject = new EOCirFichierImport();    

		newObject.setCfimAnnee(annee);
		newObject.setCfimDate(DateCtrl.today());

		newObject.setCfimType(TYPE_FICHIER_CAMPAGNE);

		newObject.setCfimExpedition(CocktailConstantes.FAUX);
		newObject.setCfimGeneration(CocktailConstantes.FAUX);

		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		ec.insertObject(newObject);
		
		return newObject;
	}

	public String typeFichierLong() {
		if(cfimType().equals(TYPE_FICHIER_CAMPAGNE))
			return LL_TYPE_FICHIER_CAMPAGNE;

		if(cfimType().equals(TYPE_FICHIER_COMPLEMENTAIRE))
			return LL_TYPE_FICHIER_COMPLEMENTAIRE;

		return LL_TYPE_FICHIER_RECTIFICATIF;
	}

	public static EOCirFichierImport findFichierProgramme(EOEditingContext edc) {

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(CFIM_HEURE_CALCUL_KEY + " != nil", null);
		return fetchFirstByQualifier(edc, myQualifier, SORT_ARRAY_ANNEE_DESC);
	}

	public static EOCirFichierImport getLastFichier(EOEditingContext edc) {
		return fetchFirstByQualifier(edc, null, SORT_ARRAY_ANNEE_DESC);
	}

	public boolean estProgramme() {
		return cfimHeureCalcul() != null;
	}

	public String dateGenerationFormatee()
	{
		return SuperFinder.dateFormatee(this, "cfimDate");
	}

	public void setDateGenerationFormatee(String uneDate)
	{
		SuperFinder.setDateFormatee(this, "cfimDate", uneDate);
	}

	public boolean estGenere()
	{
		return cfimGeneration() != null && cfimGeneration().equals("O");
	}

	public void setEstGenere(boolean aBool)
	{
		if(aBool)
			setCfimGeneration("O");
		else
			setCfimGeneration("N");
	}

	public boolean estExpedie()
	{
		return cfimExpedition() != null && cfimExpedition().equals("O");
	}

	public void setEstExpedie(boolean aBool)
	{
		if(aBool)
			setCfimExpedition("O");
		else
			setCfimExpedition("N");
	}

	/**
	 * 
	 * @param edc
	 * @param annee
	 * @return
	 */
	public static NSArray<EOCirFichierImport> findForAnnee(EOEditingContext edc, Number annee)    {
		NSMutableArray qualifiers = new NSMutableArray();        
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CFIM_ANNEE_KEY + " =%@",new NSArray(annee)));

		return fetchAll(edc, new EOAndQualifier(qualifiers), null);
	}

	/**
	 * 
	 * @param editingContext
	 * @param fichierCir
	 * @return
	 */
	public static String nomFichierEnvoi(EOEditingContext editingContext, EOCirFichierImport fichierCir)
	{
		String nomBase = EOGrhumParametres.getUniteGestionCir();
		if(nomBase == null || !nomBase.startsWith("EM"))
			nomBase = "EM000";
		String typeFichier = "IDE";
		NSTimestamp date = new NSTimestamp();
		if(fichierCir != null) {
			typeFichier = "CAR";
			if(fichierCir.cfimDate() != null)
				date = fichierCir.cfimDate();
		}
		String dateString = DateCtrl.dateToString(date, "%Y%m%d%H%M%S");
		return nomBase + "." + typeFichier + "-" + dateString;
	}

	/**
	 * 
	 */
	public void validateForInsert() throws com.webobjects.foundation.NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
		super.validateForDelete();
	}

	public void validateForSave()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
		if(cfimAnnee() == null)
			throw new com.webobjects.foundation.NSValidation.ValidationException("L'année est obligatoire");
		if(cfimFichier() == null)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le nom de fichier est obligatoire");
		if(cfimFichier().length() > 200)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le nom de fichier comporte au plus 200 caractères");
		if(cfimType() == null)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le type de fichier est obligatoire");
		
		if(!cfimType().equals(TYPE_FICHIER_CAMPAGNE) 
				&& !cfimType().equals(TYPE_FICHIER_COMPLEMENTAIRE) 
				&& !cfimType().equals(TYPE_FICHIER_RECTIFICATIF))
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le type de fichier est inconnu");

		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	public void validateObjectMetier()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
	}

	public void validateBeforeTransactionSave()
	throws com.webobjects.foundation.NSValidation.ValidationException
	{
	}

}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.cir;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.NumberCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOBeneficeEtudes extends _EOBeneficeEtudes {

	public static NSArray SORT_DATE_DEBUT_ASC = new NSArray(new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending));

	public EOBeneficeEtudes() {
		super();
	}

	public static EOBeneficeEtudes creer(EOEditingContext ec, EOIndividu individu) {

		EOBeneficeEtudes newObject = (EOBeneficeEtudes) createAndInsertInstance(ec, EOBeneficeEtudes.ENTITY_NAME);    

		newObject.setIndividuRelationship(individu);

		newObject.setTemValide("O");
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		return newObject;
	}


	public static NSArray findForIndividu(EOEditingContext ec, EOIndividu individu) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_DATE_DEBUT_ASC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}


	public String codeEcole() {
		
		if (ecoleMilitaire() != null)
			return ecoleMilitaire().emCode();
		
		return null;
	}
	
	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		if (individu() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_INDIVIDU_NON_RENSEIGNE, "ETUDES"));
		}
		if (dateDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "ETUDES"));
		} 
		if (dateFin() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_NON_RENSEIGNE, "ETUDES"));
		}
		if (dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "ETUDES", DateCtrl.dateToString(dateFin()), DateCtrl.dateToString(dateDebut())));
		}

		if (ecoleMilitaire() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une école !");
		}

		if (betuDuree() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une durée !");
		}

		if (!NumberCtrl.isANumber(betuDuree()))
			throw new NSValidation.ValidationException("La durée ne doit comporter que des chiffres !");
		if (betuDuree().length() != 6)
			throw new NSValidation.ValidationException("La durée doit être saisie sous le format AAMMJJ !");

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

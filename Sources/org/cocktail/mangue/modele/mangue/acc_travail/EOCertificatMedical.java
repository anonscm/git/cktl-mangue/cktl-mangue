//EOCertificatMedical.java
//Created on Tue Oct 23 08:34:33 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.acc_travail;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation :<BR>
 * Verification des longueurs des chaines et des formats d'heure<BR>
 * Verifie que les heures sont coherentes<BR>
 * Verifie que cmedType a des valeurs legales : I(nitial), P(rolongation), R(echute), F(inal)<BR>
 * Verifie que si les indicateurs sont vrais (estArretTravail, estSoins, estAutorisationDeSortie, estReprise, les dates ou infos
 * associees sont fournies) et que si ils sont faux, elles ne sont pas fournies<BR>
 * Verifie que les certificats sont coherents i.e qu'il n'y a pas plusieurs certificats initiaux ou finaux<BR>
 * Verifie qu'il existe un certificat initial avant un certificat final<BR>
 * Verifie que la conclusion est fournie dans un certificat final et qu'un certificat d'un autre type ne comporte pas de 
 * conclusion.<BR>
 * Verifie qu'un certificat final ne comporte pas de date de soins et d'information de sortie<BR>
 * Verifie que la date de reprise est fournie dans un certificat final<BR>
 * Verifie que les dates ont un sens i.e que les date du certificat, d'arret, de soins et de reprise ne sont pas 
 * anterieures a la date de l'accident et que la date de reprise est posterieure a la date d'arret
 *
 * 
 * @author christine
 *
 */
public class EOCertificatMedical extends _EOCertificatMedical {

	public final static EOSortOrdering SORT_DATE_DESC = new EOSortOrdering(CMED_DATE_KEY, EOSortOrdering.CompareDescending);
	public final static NSArray SORT_ARRAY_DATE_DESC = new NSArray(SORT_DATE_DESC);

	public static final String TYPE_FINAL = "F";
	private static final String TYPE_INITIAL = "I";
	private static final String TYPE_PROLONGATION = "P";
	private static final String TYPE_RECHUTE = "R";

	public final static String TYPE_CERTIFICAT[] = {TYPE_INITIAL, TYPE_PROLONGATION, TYPE_RECHUTE, TYPE_FINAL};
	private final static String TYPE_CERTIFICAT_LIBELLE[] = {"Initial", "Prolongation", "Rechute", "Final"};

	public EOCertificatMedical() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param declaration
	 * @return
	 */
	public static EOCertificatMedical creer(EOEditingContext ec, EODeclarationAccident declaration) {
		
		EOCertificatMedical newObject = new EOCertificatMedical(); 		
		ec.insertObject(newObject);

		newObject.setAAllocationTemporaireInvalidite(false);
		newObject.setDeclarationAccidentRelationship(declaration);
		return newObject;
	}


	// Méthodes ajoutées
	public String typeLibelle() {
		if (cmedType() == null) {
			return null;
		} else {
			for (int i = 0; i < TYPE_CERTIFICAT.length;i++) {
				if (cmedType().equals(TYPE_CERTIFICAT[i])) {
					return TYPE_CERTIFICAT_LIBELLE[i];
				}
			}
			return null;
		}
	}
	public String cmedArretDateFormatee() {
		return SuperFinder.dateFormatee(this, CMED_ARRET_DATE_KEY);
	}
	public void setCmedArretDateFormatee(String uneDate) {
		if (uneDate == null) {
			setCmedArretDate(null);
		} else {
			SuperFinder.setDateFormatee(this, CMED_ARRET_DATE_KEY, uneDate);
		}
	}
	public String cmedConclusionDateFormatee() {
		return SuperFinder.dateFormatee(this, CMED_CONCLUSION_DATE_KEY);
	}
	public void setCmedConclusionDateFormatee(String uneDate) {
		if (uneDate == null) {
			setCmedArretDate(null);
		} else {
			SuperFinder.setDateFormatee(this, CMED_CONCLUSION_DATE_KEY,uneDate);
		}
	}
	public String cmedDateFormatee() {
		return SuperFinder.dateFormatee(this,CMED_DATE_KEY);
	}
	public void setCmedDateFormatee(String uneDate) {
		if (uneDate == null) {
			setCmedDate(null);
		} else {
			SuperFinder.setDateFormatee(this,CMED_DATE_KEY,uneDate);
		}
	}
	public String cmedRepriseDateFormatee() {
		return SuperFinder.dateFormatee(this,CMED_REPRISE_DATE_KEY);
	}
	public void setCmedRepriseDateFormatee(String uneDate) {
		if (uneDate == null) {
			setCmedRepriseDate(null);
		} else {
			SuperFinder.setDateFormatee(this,CMED_REPRISE_DATE_KEY,uneDate);
		}
	}
	public String cmedSoinsDateFormatee() {
		return SuperFinder.dateFormatee(this,CMED_SOINS_DATE_KEY);
	}
	public void setCmedSoinsDateFormatee(String uneDate) {
		if (uneDate == null) {
			setCmedSoinsDate(null);
		} else {
			SuperFinder.setDateFormatee(this,CMED_SOINS_DATE_KEY,uneDate);
		}
	}
	public boolean estInitial() {
		return cmedType() != null && cmedType().equals(TYPE_INITIAL);
	}
	public boolean estProlongation() {
		return cmedType() != null && cmedType().equals(TYPE_PROLONGATION);
	}
	public boolean estFinal() {
		return cmedType() != null && cmedType().equals(TYPE_FINAL);
	}
	public boolean estRechute() {
		return cmedType() != null && cmedType().equals(TYPE_RECHUTE);
	}


	public void setEstFinal() {
		setCmedType(TYPE_FINAL);
	}
	public void setEstInitial() {
		setCmedType(TYPE_INITIAL);
	}
	public void setEstRechute() {
		setCmedType(TYPE_RECHUTE);
	}
	public void setEstProlongation() {
		setCmedType(TYPE_PROLONGATION);
	}

	public boolean estArretTravail() {
		return cmedArret() != null && cmedArret().equals(CocktailConstantes.VRAI);
	}
	public void setEstArretTravail(boolean aBool) {
		if (aBool) {
			setCmedArret(CocktailConstantes.VRAI);
		} else {
			setCmedArret(CocktailConstantes.FAUX);
		}
	}
	public boolean estReprise() {
		return cmedReprise() != null && cmedReprise().equals(CocktailConstantes.VRAI);
	}
	public void setEstReprise(boolean aBool) {
		if (aBool) {
			setCmedReprise(CocktailConstantes.VRAI);
		} else {
			setCmedReprise(CocktailConstantes.FAUX);
		}
	}
	public boolean estSoins() {
		return cmedSoins() != null && cmedSoins().equals(CocktailConstantes.VRAI);
	}
	public void setEstSoins(boolean aBool) {
		if (aBool) {
			setCmedSoins(CocktailConstantes.VRAI);
		} else {
			setCmedSoins(CocktailConstantes.FAUX);
		}
	}
	public boolean estAutorisationDeSortie() {
		return cmedSortie() != null && cmedSortie().equals(CocktailConstantes.VRAI);
	}
	public void setEstAutorisationDeSortie(boolean aBool) {
		if (aBool) {
			setCmedSortie(CocktailConstantes.VRAI);
		} else {
			setCmedSortie(CocktailConstantes.FAUX);
		}
	}
	public boolean aAllocationTemporaireInvalidite() {
		return cmedAti() != null && cmedAti().equals(CocktailConstantes.VRAI);
	}
	public void setAAllocationTemporaireInvalidite(boolean aBool) {
		if (aBool) {
			setCmedAti(CocktailConstantes.VRAI);
		} else {
			setCmedAti(CocktailConstantes.FAUX);
		}
	}


	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (declarationAccident() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une déclaration d'accident");
		}
		if (cmedType() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le type de certificat médical");
		}
		if (cmedConstatation() != null && cmedConstatation().length() > 2000) {
			throw new NSValidation.ValidationException("La constatation comporte au plus 2000 caractères");
		}

		// Vérifier que les dates sont cohérentes et que les certificats sont cohérents entre eux
		if (cmedDate() != null) {
			if (DateCtrl.isBefore(cmedDate(), declarationAccident().daccDate())) {
				throw new NSValidation.ValidationException("La date du certificat ne peut être antérieure à la date d'accident");
			}
		}
		if (cmedSoinsDate() != null) {
			if (DateCtrl.isBefore(cmedSoinsDate(), declarationAccident().daccDate())) {
				throw new NSValidation.ValidationException("La date de soins ne peut être antérieure à la date d'accident");
			}
		}
		if (estArretTravail()) {
			if (cmedArretDate() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir la date d'arrêt de travail");
			}
		} else {
			if (cmedArretDate() != null) {
				throw new NSValidation.ValidationException("Vous ne devez pas fournir la date d'arrêt de travail");
			}
		}
		if (estSoins()) {
			if (cmedSoinsDate() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir la date de soins");
			}
		} else {
			if (cmedSoinsDate() != null) {
				throw new NSValidation.ValidationException("Vous ne devez pas fournir la date de soins");
			}
		}
		if (estAutorisationDeSortie()) {
			if (cmedSortieDebut() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir l'heure de début de sortie");
			}
			if (cmedSortieFin() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir l'heure de fin de sortie");
			}
			if (DateCtrl.estHeureValide(cmedSortieDebut(),false) == false) {
				throw new NSValidation.ValidationException("L'heure de début de sortie n'a pas un format valide (HH:MM)");
			}
			if (DateCtrl.estHeureValide(cmedSortieFin(),false) == false) {
				throw new NSValidation.ValidationException("L'heure de fin de sortie n'a pas un format valide (HH:MM)");
			}
			if (heureDebutAnterieureHeureFin() == false) {
				throw new NSValidation.ValidationException("L'heure de fin de sortie ne peut pas être avant l'heure de début de sortie");
			}
		} else {
			if (cmedSortieDebut() != null) {
				throw new NSValidation.ValidationException("Vous ne devez pas fournir l'heure de début de sortie");
			}
			if (cmedSortieFin() != null) {
				throw new NSValidation.ValidationException("Vous ne devez pas fournir l'heure de début de fin");
			}
		}
		if (estReprise()) {
			if (cmedRepriseDate() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir la date de reprise");
			}
		} else {
			if (cmedRepriseDate() != null) {
				throw new NSValidation.ValidationException("Vous ne devez pas fournir la date de reprise");
			}
		}
		NSArray<EOCertificatMedical> certificats = EOCertificatMedical.findForDeclaration(editingContext(), declarationAccident());
		if (estFinal()) {
			// Vérifier qu'il existe un certificat initial déjà saisi
			boolean aCertificatInitial = false;
			for (EOCertificatMedical certificat : certificats) {
				if (certificat.estInitial()) {
					aCertificatInitial = true;
					break;
				}
			}
			if (!aCertificatInitial) {
				throw new NSValidation.ValidationException("Avant de saisir un certificat final, vous devez saisir au moins un certificat initial");
			}
			if (cmedSoinsDate() != null || (cmedSoins() != null && cmedSoins().equals(CocktailConstantes.VRAI))) {
				throw new NSValidation.ValidationException("Pas d'information de soins dans un certificat final");
			}
			if (cmedSortieDebut() != null || cmedSortieFin() != null || (cmedSortie() != null && cmedSortie().equals(CocktailConstantes.VRAI))) {
				throw new NSValidation.ValidationException("Pas d'information de sortie dans un certificat final");
			}
		}

		// Vérifier qu'il n'y a qu'un seul certificat de type initial ou final
		if ((estInitial()) && estCertificatUnique(certificats,cmedType()) == false) {
			throw new NSValidation.ValidationException("Il ne peut y avoir deux certificats de type " + typeLibelle());
		}
		//  18/08/09 DT 1787 - Vérifier si il existe un certificat de rechute en quel cas le contrôle sur les dates ne se fait pas
		boolean existeRechute = false;
		if (estRechute()) {
			existeRechute = true;
		} else {
			for (java.util.Enumeration<EOCertificatMedical> e = certificats.objectEnumerator();e.hasMoreElements();) {
				EOCertificatMedical currentCertificat = e.nextElement();
				if (currentCertificat.estRechute()) {
					existeRechute = true;
					break;
				}
			}
		}
		if (cmedArretDate() != null) {
			if (DateCtrl.isBefore(cmedArretDate(), declarationAccident().daccDate())) {
				throw new NSValidation.ValidationException("La date d'arrêt ne peut être antérieure à la date d'accident");
			}

			if (cmedRepriseDate() != null) {
				if (DateCtrl.isAfter(cmedArretDate(), cmedRepriseDate())) {
					throw new NSValidation.ValidationException("La date de reprise ne peut être antérieure à la date d'arrêt");
				}
			} else if (!existeRechute) {
				// Vérifier si il existe un certificat médical avec une date de reprise
				for (java.util.Enumeration<EOCertificatMedical> e = certificats.objectEnumerator();e.hasMoreElements();) {
					EOCertificatMedical currentCertificat = e.nextElement();
					if (currentCertificat.cmedRepriseDate() != null) {
						if (currentCertificat.cmedRepriseDate() != null && DateCtrl.isAfter(cmedArretDate(), currentCertificat.cmedRepriseDate())) {
							throw new NSValidation.ValidationException("La date d'arrêt ne peut être postérieure à la date de reprise");
						}
						break;
					}
				}
			}
		} else if (!existeRechute) {
			if (cmedRepriseDate() != null) {
				// Vérifier si il existe un certificat médical avec la date d'arrêt
				for (java.util.Enumeration<EOCertificatMedical> e = certificats.objectEnumerator();e.hasMoreElements();) {
					EOCertificatMedical currentCertificat = e.nextElement();
					if (currentCertificat.cmedArretDate() != null) {
						if (DateCtrl.isAfter(currentCertificat.cmedArretDate(), cmedRepriseDate())) {
							throw new NSValidation.ValidationException("La date de reprise ne peut être antérieure à la date d'arrêt");
						} 
					}
					break;
				}
			}
		}
		if (cmedType().equals(TYPE_FINAL)) {
			if (conclusion() == null) {
				throw new NSValidation.ValidationException("Un certificat médical final comporte nécessairement une conclusion");
			}
			if (cmedConclusionDate() == null) {
				throw new NSValidation.ValidationException("Un certificat médical final comporte nécessairement une date de conclusion");
			}
		} else {
			if (conclusion() != null) {
				throw new NSValidation.ValidationException("Un certificat médical autre que final ne comporte pas de conclusion");
			}
			if (cmedConclusionDate() != null) {
				throw new NSValidation.ValidationException("Un certificat médical autre que final ne comporte pas de date de conclusion");
			}
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
	}

	/**
	 * 
	 * @param certificats
	 * @param typeCertificat
	 * @return
	 */
	private boolean estCertificatUnique(NSArray<EOCertificatMedical> certificats,String typeCertificat) {
		
		for (EOCertificatMedical certificat : certificats) {
			if (certificat != this && certificat.cmedType().equals(typeCertificat)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param ec
	 * @param declaration
	 * @return
	 */
	public static NSArray<EOCertificatMedical> findForDeclaration(EOEditingContext ec,EODeclarationAccident declaration) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(DECLARATION_ACCIDENT_KEY + "=%@", new NSArray(declaration));
			return fetchAll(ec, qualifier, SORT_ARRAY_DATE_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOCertificatMedical>();
		}
	}

	/**
	 * 
	 * @return
	 */
	private boolean heureDebutAnterieureHeureFin() {
		int heureDebut = new Integer(cmedSortieDebut().substring(0,2)).intValue();
		int heureFin = new Integer(cmedSortieFin().substring(0,2)).intValue();
		if (heureDebut > heureFin) {
			return false;
		} else if (heureDebut == heureFin) {
			int minutesDebut = new Integer(cmedSortieDebut().substring(3,5)).intValue();
			int minutesFin = new Integer(cmedSortieFin().substring(3,5)).intValue();
			if (minutesDebut > minutesFin) {
				return false;
			}
		}
		return true;
	}
}

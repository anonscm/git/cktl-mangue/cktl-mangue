// EODeclarationAccident.java
// Created on Tue Oct 23 08:33:06 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.acc_travail;

import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation :<BR>
 * Verification des longueurs des chaines et des formats d'heure<BR>
 * Verifie que les heures sont coherentes<BR>
 * Verifie que le type d'accident est conforme au statut de l'individu a la date de l'accident (Accident de service, accident de travail)
 * Verifie que daccSuite a des valeurs legales : S(ans arret de travail), A(vec arret de travail > 24 H), Deces immediat)<BR>
 * @author christine
 *
 */
public class EODeclarationAccident extends _EODeclarationAccident implements RecordAvecLibelle {

	public final static String SUITE_SANS_ARRET = "S";
	public final static String SUITE_ARRET = "A";
	public final static String SUITE_DECES = "D";

	public final static EOSortOrdering SORT_DATE_DESC = new EOSortOrdering(DACC_DATE_KEY, EOSortOrdering.CompareDescending);
	public final static NSArray SORT_ARRAY_DATE_DESC = new NSArray(SORT_DATE_DESC);

	private final static String TYPE_SUITE[] = {"S","A","D"};
	public EODeclarationAccident() {
		super();
	}

	public static EODeclarationAccident creer(EOEditingContext ec, EOIndividu individu) {
		//		EODeclarationAccident newObject = (EODeclarationAccident) createAndInsertInstance(ec, ENTITY_NAME);    	
		EODeclarationAccident newObject = new EODeclarationAccident();
		newObject.initAvecIndividu(individu);
		ec.insertObject(newObject);
		return newObject;
	}

	public void setSuiteSansArret() {
		setDaccSuite(SUITE_SANS_ARRET);
	}
	public void setSuiteArret() {
		setDaccSuite(SUITE_ARRET);
	}
	public void setSuiteDeces() {
		setDaccSuite(SUITE_DECES);
	}


	public boolean suiteSansArret() {
		return daccSuite() != null && daccSuite().equals(SUITE_SANS_ARRET); 
	}
	public boolean suiteAvecArret() {
		return daccSuite() != null && daccSuite().equals(SUITE_ARRET); 
	}
	public boolean suiteAvecDeces() {
		return daccSuite() != null && daccSuite().equals(SUITE_DECES); 
	}


	public String daccDateFormatee() {
		return SuperFinder.dateFormatee(this,DACC_DATE_KEY);
	}
	public void setDaccDateFormatee(String uneDate) {
		if (uneDate == null) {
			setDaccDate(null);
		} else {
			SuperFinder.setDateFormatee(this,DACC_DATE_KEY,uneDate);
		}
	}
	public void initAvecIndividu(EOIndividu individu) {
		setIndividuRelationship(individu);
	}
	public void supprimerRelations() {
		setIndividuRelationship(null);
		setTypeAccidentRelationship(null);
		setLieuAccidentRelationship(null);
	}
	public void validateForSave() throws NSValidation.ValidationException {

		if (individu() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un individu");
		}    	
		if (typeAccident() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un type d'accident");
		}
		if (daccLocalite() != null && daccLocalite().length() > 200) {
			throw new NSValidation.ValidationException("La localité comporte au plus 200 caractères");
		}
		if (daccCirconstance() != null && daccCirconstance().length() > 2000) {
			throw new NSValidation.ValidationException("La circonstance comporte au plus 2000 caractères");
		}
		if (daccTransport() != null && daccTransport().length() > 2000) {
			throw new NSValidation.ValidationException("Le lieu où a été transporté la victime, comporte au plus 2000 caractères");
		}
		if (DateCtrl.estHeureValide(daccHeure(),false) == false) {
			throw new NSValidation.ValidationException("L'horaire de l'accident de travail n'a pas un format valide (HH:MM)");
		}
		if (DateCtrl.estHeureValide(daccHdebAm(),false) == false) {
			throw new NSValidation.ValidationException("L'horaire de début de travail le matin n'a pas un format valide (HH:MM)");
		}
		if (DateCtrl.estHeureValide(daccHfinAm(),false) == false) {
			throw new NSValidation.ValidationException("L'horaire de fin de travail le matin n'a pas un format valide (HH:MM)");
		}
		if (DateCtrl.estHeureValide(daccHdebPm(),false) == false) {
			throw new NSValidation.ValidationException("L'horaire de début de travail l'après-midi n'a pas un format valide (HH:MM)");
		}
		if (DateCtrl.estHeureValide(daccHfinPm(),false) == false) {
			throw new NSValidation.ValidationException("L'horaire de fin de travail l'après-midi n'a pas un format valide (HH:MM)");
		}
		if (daccHdebAm() != null && daccHfinAm() != null && heureDebutAnterieureHeureFin(daccHdebAm(), daccHfinAm()) == false) {
			throw new NSValidation.ValidationException("L'horaire de début du matin ne peut être postérieure à l'heure de fin du matin");
		}
		if (daccHdebPm() != null && daccHfinPm() != null && heureDebutAnterieureHeureFin(daccHdebPm(), daccHfinPm()) == false) {
			throw new NSValidation.ValidationException("L'heure de début de l'après-midi ne peut être postérieure à l'heure de fin de l'après-midi");
		}
		if (daccHdebPm() != null && daccHfinAm() != null && heureDebutAnterieureHeureFin(daccHfinAm(), daccHdebPm()) == false) {
			throw new NSValidation.ValidationException("L'heure de fin du matin ne peut être postérieure à l'heure de début de l'après-midi");
		}

		if (daccDate() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date d'accident");
		}
		// Vérifier la cohérence du type d'accident selon le statut
		if (estPourTitulaire()) {
			if (typeAccident().estPourContractuelUniquement()) {
				throw new NSValidation.ValidationException("Ce type d'accident ne s'applique qu'aux contractuels");
			}	
		} else {
			if (typeAccident().estPourTitulaireUniquement()) {
				throw new NSValidation.ValidationException("Ce type d'accident ne s'applique qu'aux fonctionnaires");
			}
		}

		if (daccSuite() != null) {
			if (daccSuite().length() == 0 || daccSuite().length() > 1) {
				throw new NSValidation.ValidationException("La suite probable comporte un caractère");
			} else {
				boolean found = false;
				for (int i = 0; i < TYPE_SUITE.length;i++) {
					if (daccSuite().equals(TYPE_SUITE[i])) {
						found = true;
						break;
					}
				}
				if (!found) {
					throw new NSValidation.ValidationException("La suite fournie n'est pas valide");
				}	
			}
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());


	}
	public boolean estPourTitulaire() {
		if (daccDate() != null) {
			return individu().estTitulaireSurPeriodeComplete(daccDate(), daccDate());
		} else {
			return individu().estTitulaire();
		}
	}
	// Interface RecordAvecLibelle
	public String libelle() {
		return "Accident du " + daccDateFormatee();
	}

	// Méthodes privées
	private boolean heureDebutAnterieureHeureFin(String heureDebutStr,String heureFinStr) {
		int heureDebut = new Integer(heureDebutStr.substring(0,2)).intValue();
		int heureFin = new Integer(heureFinStr.substring(0,2)).intValue();
		if (heureDebut > heureFin) {
			return false;
		} else if (heureDebut == heureFin) {
			int minutesDebut = new Integer(heureDebutStr.substring(3,5)).intValue();
			int minutesFin = new Integer(heureFinStr.substring(3,5)).intValue();
			if (minutesDebut > minutesFin) {
				return false;
			}
		}
		return true;
	}


	/** Recherche les declarations d'accident du travail pour un individu
	 * Retourne le resultat par ordre de date decroissant */
	public static NSArray<EODeclarationAccident> findForIndividu(EOEditingContext ec,EOIndividu individu) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu));
			return fetchAll(ec, qualifier, SORT_ARRAY_DATE_DESC);
		}
		catch (Exception e) {
			return new NSArray<EODeclarationAccident>();
		}
	}
}

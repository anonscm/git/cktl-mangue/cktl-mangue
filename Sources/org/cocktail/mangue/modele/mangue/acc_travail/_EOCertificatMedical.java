// _EOCertificatMedical.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCertificatMedical.java instead.
package org.cocktail.mangue.modele.mangue.acc_travail;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOCertificatMedical extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "CertificatMedical";
	public static final String ENTITY_TABLE_NAME = "MANGUE.CERTIFICAT_MEDICAL";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cmedOrdre";

	public static final String CMED_ARRET_KEY = "cmedArret";
	public static final String CMED_ARRET_DATE_KEY = "cmedArretDate";
	public static final String CMED_ATI_KEY = "cmedAti";
	public static final String CMED_CONCLUSION_DATE_KEY = "cmedConclusionDate";
	public static final String CMED_CONSTATATION_KEY = "cmedConstatation";
	public static final String CMED_DATE_KEY = "cmedDate";
	public static final String CMED_IPP_KEY = "cmedIpp";
	public static final String CMED_REPRISE_KEY = "cmedReprise";
	public static final String CMED_REPRISE_DATE_KEY = "cmedRepriseDate";
	public static final String CMED_SOINS_KEY = "cmedSoins";
	public static final String CMED_SOINS_DATE_KEY = "cmedSoinsDate";
	public static final String CMED_SORTIE_KEY = "cmedSortie";
	public static final String CMED_SORTIE_DEBUT_KEY = "cmedSortieDebut";
	public static final String CMED_SORTIE_FIN_KEY = "cmedSortieFin";
	public static final String CMED_TYPE_KEY = "cmedType";
	public static final String DACC_ORDRE_KEY = "daccOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

// Attributs non visibles
	public static final String CMED_ORDRE_KEY = "cmedOrdre";
	public static final String CMED_CONCLUSION_KEY = "cmedConclusion";

//Colonnes dans la base de donnees
	public static final String CMED_ARRET_COLKEY = "CMED_ARRET";
	public static final String CMED_ARRET_DATE_COLKEY = "CMED_ARRET_DATE";
	public static final String CMED_ATI_COLKEY = "CMED_ATI";
	public static final String CMED_CONCLUSION_DATE_COLKEY = "CMED_CONCLUSION_DATE";
	public static final String CMED_CONSTATATION_COLKEY = "CMED_CONSTATATION";
	public static final String CMED_DATE_COLKEY = "CMED_DATE";
	public static final String CMED_IPP_COLKEY = "CMED_IPP";
	public static final String CMED_REPRISE_COLKEY = "CMED_REPRISE";
	public static final String CMED_REPRISE_DATE_COLKEY = "CMED_REPRISE_DATE";
	public static final String CMED_SOINS_COLKEY = "CMED_SOINS";
	public static final String CMED_SOINS_DATE_COLKEY = "CMED_SOINS_DATE";
	public static final String CMED_SORTIE_COLKEY = "CMED_SORTIE";
	public static final String CMED_SORTIE_DEBUT_COLKEY = "CMED_SORTIE_DEBUT";
	public static final String CMED_SORTIE_FIN_COLKEY = "CMED_SORTIE_FIN";
	public static final String CMED_TYPE_COLKEY = "CMED_TYPE";
	public static final String DACC_ORDRE_COLKEY = "DACC_ORDRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";

	public static final String CMED_ORDRE_COLKEY = "CMED_ORDRE";
	public static final String CMED_CONCLUSION_COLKEY = "CMED_CONCLUSION";


	// Relationships
	public static final String CONCLUSION_KEY = "conclusion";
	public static final String DECLARATION_ACCIDENT_KEY = "declarationAccident";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cmedArret() {
    return (String) storedValueForKey(CMED_ARRET_KEY);
  }

  public void setCmedArret(String value) {
    takeStoredValueForKey(value, CMED_ARRET_KEY);
  }

  public NSTimestamp cmedArretDate() {
    return (NSTimestamp) storedValueForKey(CMED_ARRET_DATE_KEY);
  }

  public void setCmedArretDate(NSTimestamp value) {
    takeStoredValueForKey(value, CMED_ARRET_DATE_KEY);
  }

  public String cmedAti() {
    return (String) storedValueForKey(CMED_ATI_KEY);
  }

  public void setCmedAti(String value) {
    takeStoredValueForKey(value, CMED_ATI_KEY);
  }

  public NSTimestamp cmedConclusionDate() {
    return (NSTimestamp) storedValueForKey(CMED_CONCLUSION_DATE_KEY);
  }

  public void setCmedConclusionDate(NSTimestamp value) {
    takeStoredValueForKey(value, CMED_CONCLUSION_DATE_KEY);
  }

  public String cmedConstatation() {
    return (String) storedValueForKey(CMED_CONSTATATION_KEY);
  }

  public void setCmedConstatation(String value) {
    takeStoredValueForKey(value, CMED_CONSTATATION_KEY);
  }

  public NSTimestamp cmedDate() {
    return (NSTimestamp) storedValueForKey(CMED_DATE_KEY);
  }

  public void setCmedDate(NSTimestamp value) {
    takeStoredValueForKey(value, CMED_DATE_KEY);
  }

  public java.math.BigDecimal cmedIpp() {
    return (java.math.BigDecimal) storedValueForKey(CMED_IPP_KEY);
  }

  public void setCmedIpp(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CMED_IPP_KEY);
  }

  public String cmedReprise() {
    return (String) storedValueForKey(CMED_REPRISE_KEY);
  }

  public void setCmedReprise(String value) {
    takeStoredValueForKey(value, CMED_REPRISE_KEY);
  }

  public NSTimestamp cmedRepriseDate() {
    return (NSTimestamp) storedValueForKey(CMED_REPRISE_DATE_KEY);
  }

  public void setCmedRepriseDate(NSTimestamp value) {
    takeStoredValueForKey(value, CMED_REPRISE_DATE_KEY);
  }

  public String cmedSoins() {
    return (String) storedValueForKey(CMED_SOINS_KEY);
  }

  public void setCmedSoins(String value) {
    takeStoredValueForKey(value, CMED_SOINS_KEY);
  }

  public NSTimestamp cmedSoinsDate() {
    return (NSTimestamp) storedValueForKey(CMED_SOINS_DATE_KEY);
  }

  public void setCmedSoinsDate(NSTimestamp value) {
    takeStoredValueForKey(value, CMED_SOINS_DATE_KEY);
  }

  public String cmedSortie() {
    return (String) storedValueForKey(CMED_SORTIE_KEY);
  }

  public void setCmedSortie(String value) {
    takeStoredValueForKey(value, CMED_SORTIE_KEY);
  }

  public String cmedSortieDebut() {
    return (String) storedValueForKey(CMED_SORTIE_DEBUT_KEY);
  }

  public void setCmedSortieDebut(String value) {
    takeStoredValueForKey(value, CMED_SORTIE_DEBUT_KEY);
  }

  public String cmedSortieFin() {
    return (String) storedValueForKey(CMED_SORTIE_FIN_KEY);
  }

  public void setCmedSortieFin(String value) {
    takeStoredValueForKey(value, CMED_SORTIE_FIN_KEY);
  }

  public String cmedType() {
    return (String) storedValueForKey(CMED_TYPE_KEY);
  }

  public void setCmedType(String value) {
    takeStoredValueForKey(value, CMED_TYPE_KEY);
  }

  public Integer daccOrdre() {
    return (Integer) storedValueForKey(DACC_ORDRE_KEY);
  }

  public void setDaccOrdre(Integer value) {
    takeStoredValueForKey(value, DACC_ORDRE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.medical.EOConclusionMedicale conclusion() {
    return (org.cocktail.mangue.common.modele.nomenclatures.medical.EOConclusionMedicale)storedValueForKey(CONCLUSION_KEY);
  }

  public void setConclusionRelationship(org.cocktail.mangue.common.modele.nomenclatures.medical.EOConclusionMedicale value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.medical.EOConclusionMedicale oldValue = conclusion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONCLUSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONCLUSION_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident declarationAccident() {
    return (org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident)storedValueForKey(DECLARATION_ACCIDENT_KEY);
  }

  public void setDeclarationAccidentRelationship(org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident oldValue = declarationAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DECLARATION_ACCIDENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DECLARATION_ACCIDENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOCertificatMedical avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCertificatMedical createEOCertificatMedical(EOEditingContext editingContext, String cmedType
, Integer daccOrdre
, NSTimestamp dCreation
, NSTimestamp dModification
			) {
    EOCertificatMedical eo = (EOCertificatMedical) createAndInsertInstance(editingContext, _EOCertificatMedical.ENTITY_NAME);    
		eo.setCmedType(cmedType);
		eo.setDaccOrdre(daccOrdre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  
	  public EOCertificatMedical localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCertificatMedical)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCertificatMedical creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCertificatMedical creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCertificatMedical object = (EOCertificatMedical)createAndInsertInstance(editingContext, _EOCertificatMedical.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCertificatMedical localInstanceIn(EOEditingContext editingContext, EOCertificatMedical eo) {
    EOCertificatMedical localInstance = (eo == null) ? null : (EOCertificatMedical)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCertificatMedical#localInstanceIn a la place.
   */
	public static EOCertificatMedical localInstanceOf(EOEditingContext editingContext, EOCertificatMedical eo) {
		return EOCertificatMedical.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCertificatMedical fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCertificatMedical fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCertificatMedical eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCertificatMedical)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCertificatMedical fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCertificatMedical fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCertificatMedical eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCertificatMedical)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCertificatMedical fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCertificatMedical eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCertificatMedical ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCertificatMedical fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

// _EODeclarationAccident.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODeclarationAccident.java instead.
package org.cocktail.mangue.modele.mangue.acc_travail;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EODeclarationAccident extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "DeclarationAccident";
	public static final String ENTITY_TABLE_NAME = "MANGUE.DECLARATION_ACCIDENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "daccOrdre";

	public static final String DACC_CIRCONSTANCE_KEY = "daccCirconstance";
	public static final String DACC_DATE_KEY = "daccDate";
	public static final String DACC_HDEB_AM_KEY = "daccHdebAm";
	public static final String DACC_HDEB_PM_KEY = "daccHdebPm";
	public static final String DACC_HEURE_KEY = "daccHeure";
	public static final String DACC_HFIN_AM_KEY = "daccHfinAm";
	public static final String DACC_HFIN_PM_KEY = "daccHfinPm";
	public static final String DACC_JOUR_KEY = "daccJour";
	public static final String DACC_LOCALITE_KEY = "daccLocalite";
	public static final String DACC_SUITE_KEY = "daccSuite";
	public static final String DACC_TRANSPORT_KEY = "daccTransport";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

// Attributs non visibles
	public static final String DACC_ORDRE_KEY = "daccOrdre";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String LACC_ORDRE_KEY = "laccOrdre";
	public static final String C_TYPE_ACCIDENT_KEY = "cTypeAccident";

//Colonnes dans la base de donnees
	public static final String DACC_CIRCONSTANCE_COLKEY = "DACC_CIRCONSTANCE";
	public static final String DACC_DATE_COLKEY = "DACC_DATE";
	public static final String DACC_HDEB_AM_COLKEY = "DACC_HDEB_AM";
	public static final String DACC_HDEB_PM_COLKEY = "DACC_HDEB_PM";
	public static final String DACC_HEURE_COLKEY = "DACC_HEURE";
	public static final String DACC_HFIN_AM_COLKEY = "DACC_HFIN_AM";
	public static final String DACC_HFIN_PM_COLKEY = "DACC_HFIN_PM";
	public static final String DACC_JOUR_COLKEY = "DACC_JOUR";
	public static final String DACC_LOCALITE_COLKEY = "DACC_LOCALITE";
	public static final String DACC_SUITE_COLKEY = "DACC_SUITE";
	public static final String DACC_TRANSPORT_COLKEY = "DACC_TRANSPORT";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";

	public static final String DACC_ORDRE_COLKEY = "DACC_ORDRE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String LACC_ORDRE_COLKEY = "LACC_ORDRE";
	public static final String C_TYPE_ACCIDENT_COLKEY = "C_TYPE_ACCIDENT";


	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String LIEU_ACCIDENT_KEY = "lieuAccident";
	public static final String TYPE_ACCIDENT_KEY = "typeAccident";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String daccCirconstance() {
    return (String) storedValueForKey(DACC_CIRCONSTANCE_KEY);
  }

  public void setDaccCirconstance(String value) {
    takeStoredValueForKey(value, DACC_CIRCONSTANCE_KEY);
  }

  public NSTimestamp daccDate() {
    return (NSTimestamp) storedValueForKey(DACC_DATE_KEY);
  }

  public void setDaccDate(NSTimestamp value) {
    takeStoredValueForKey(value, DACC_DATE_KEY);
  }

  public String daccHdebAm() {
    return (String) storedValueForKey(DACC_HDEB_AM_KEY);
  }

  public void setDaccHdebAm(String value) {
    takeStoredValueForKey(value, DACC_HDEB_AM_KEY);
  }

  public String daccHdebPm() {
    return (String) storedValueForKey(DACC_HDEB_PM_KEY);
  }

  public void setDaccHdebPm(String value) {
    takeStoredValueForKey(value, DACC_HDEB_PM_KEY);
  }

  public String daccHeure() {
    return (String) storedValueForKey(DACC_HEURE_KEY);
  }

  public void setDaccHeure(String value) {
    takeStoredValueForKey(value, DACC_HEURE_KEY);
  }

  public String daccHfinAm() {
    return (String) storedValueForKey(DACC_HFIN_AM_KEY);
  }

  public void setDaccHfinAm(String value) {
    takeStoredValueForKey(value, DACC_HFIN_AM_KEY);
  }

  public String daccHfinPm() {
    return (String) storedValueForKey(DACC_HFIN_PM_KEY);
  }

  public void setDaccHfinPm(String value) {
    takeStoredValueForKey(value, DACC_HFIN_PM_KEY);
  }

  public String daccJour() {
    return (String) storedValueForKey(DACC_JOUR_KEY);
  }

  public void setDaccJour(String value) {
    takeStoredValueForKey(value, DACC_JOUR_KEY);
  }

  public String daccLocalite() {
    return (String) storedValueForKey(DACC_LOCALITE_KEY);
  }

  public void setDaccLocalite(String value) {
    takeStoredValueForKey(value, DACC_LOCALITE_KEY);
  }

  public String daccSuite() {
    return (String) storedValueForKey(DACC_SUITE_KEY);
  }

  public void setDaccSuite(String value) {
    takeStoredValueForKey(value, DACC_SUITE_KEY);
  }

  public String daccTransport() {
    return (String) storedValueForKey(DACC_TRANSPORT_KEY);
  }

  public void setDaccTransport(String value) {
    takeStoredValueForKey(value, DACC_TRANSPORT_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.medical.EOLieuAccident lieuAccident() {
    return (org.cocktail.mangue.common.modele.nomenclatures.medical.EOLieuAccident)storedValueForKey(LIEU_ACCIDENT_KEY);
  }

  public void setLieuAccidentRelationship(org.cocktail.mangue.common.modele.nomenclatures.medical.EOLieuAccident value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.medical.EOLieuAccident oldValue = lieuAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LIEU_ACCIDENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LIEU_ACCIDENT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeAccidentTrav typeAccident() {
    return (org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeAccidentTrav)storedValueForKey(TYPE_ACCIDENT_KEY);
  }

  public void setTypeAccidentRelationship(org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeAccidentTrav value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeAccidentTrav oldValue = typeAccident();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACCIDENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACCIDENT_KEY);
    }
  }
  

/**
 * Créer une instance de EODeclarationAccident avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODeclarationAccident createEODeclarationAccident(EOEditingContext editingContext, NSTimestamp daccDate
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu, org.cocktail.mangue.common.modele.nomenclatures.medical.EOTypeAccidentTrav typeAccident			) {
    EODeclarationAccident eo = (EODeclarationAccident) createAndInsertInstance(editingContext, _EODeclarationAccident.ENTITY_NAME);    
		eo.setDaccDate(daccDate);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setIndividuRelationship(individu);
    eo.setTypeAccidentRelationship(typeAccident);
    return eo;
  }

  
	  public EODeclarationAccident localInstanceIn(EOEditingContext editingContext) {
	  		return (EODeclarationAccident)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODeclarationAccident creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODeclarationAccident creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODeclarationAccident object = (EODeclarationAccident)createAndInsertInstance(editingContext, _EODeclarationAccident.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODeclarationAccident localInstanceIn(EOEditingContext editingContext, EODeclarationAccident eo) {
    EODeclarationAccident localInstance = (eo == null) ? null : (EODeclarationAccident)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODeclarationAccident#localInstanceIn a la place.
   */
	public static EODeclarationAccident localInstanceOf(EOEditingContext editingContext, EODeclarationAccident eo) {
		return EODeclarationAccident.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODeclarationAccident fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODeclarationAccident fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODeclarationAccident eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODeclarationAccident)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODeclarationAccident fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODeclarationAccident fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODeclarationAccident eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODeclarationAccident)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODeclarationAccident fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODeclarationAccident eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODeclarationAccident ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODeclarationAccident fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

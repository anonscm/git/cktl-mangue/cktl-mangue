/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue;

import org.cocktail.common.modele.SuperFinder;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOCarriereSpecialisations extends _EOCarriereSpecialisations {

	public static EOSortOrdering SORT_DATE_DEBUT_ASC = new EOSortOrdering(SPEC_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(SPEC_DEBUT_KEY, EOSortOrdering.CompareDescending);

	public static NSArray SORT_ARRAY_DATE_DEBUT_ASC = new NSArray(SORT_DATE_DEBUT_ASC);
	public static NSArray SORT_ARRAY_DATE_DEBUT_DESC = new NSArray(SORT_DATE_DEBUT_DESC);

	public static final String LIBELLE_SPEC_KEY = "libelleSpecialisation";

	public EOCarriereSpecialisations() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param carriere
	 * @param utilisateur
	 * @return
	 */
	public static EOCarriereSpecialisations creer(EOEditingContext ec, EOCarriere carriere, EOAgentPersonnel utilisateur) {
		EOCarriereSpecialisations newObject = new EOCarriereSpecialisations();

		newObject.setToIndividuRelationship(carriere.toIndividu());
		newObject.setToCarriereRelationship(carriere);
		newObject.setPersIdCreation(utilisateur.toIndividu().persId());
		newObject.setPersIdModification(utilisateur.toIndividu().persId());

		ec.insertObject(newObject);
		return newObject;
	}

	/**
	 * 
	 * @param ec
	 * @param carriere
	 * @return
	 */
	public static NSArray<EOCarriereSpecialisations> findForCarriere(EOEditingContext ec,EOCarriere carriere) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_CARRIERE_KEY +" = %@", new NSArray(carriere));
			return fetchAll(ec, qualifier, SORT_ARRAY_DATE_DEBUT_DESC);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param dateReference
	 * @return
	 */
	public static EOCarriereSpecialisations findForIndividuAndDate(EOEditingContext ec, EOIndividu individu, NSTimestamp dateReference) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_INDIVIDU_KEY +" = %@", new NSArray(individu)));
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(SPEC_DEBUT_KEY, dateReference, SPEC_FIN_KEY, dateReference));

			return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String libelleSpecialisation() {

		String libelle = "";

		if (toReferensEmploi() != null)
			libelle = "REFERENS : " + toReferensEmploi().code()+ " - " + toReferensEmploi().libelleLong();
		if (toDiscSecondDegre() != null)
			libelle = "DISC : " + toDiscSecondDegre().code()+ " - " + toDiscSecondDegre().libelleLong();
		if (toCnu() != null)
			libelle = "CNU : " + toCnu().code()+ " - " + toCnu().libelleLong();
		if (toSpecialiteItarf() != null)
			libelle = "ITARF : " + toSpecialiteItarf().code()+ " - " + toSpecialiteItarf().libelleLong();
		if (toSpecialiteAtos() != null)
			libelle = "ATOS : " + toSpecialiteAtos().code()+ " - " + toSpecialiteAtos().libelleLong();
		if (toCneca() != null)
			libelle = "CNECA : " + toCneca().libelleCourt()+ " - " + toCneca().libelleLong();

		return libelle;

	}

	/**
	 * 	ferme tout ce qui se rattache a la carriere d'un individu valide a cette date et supprime tout ce qui commence au-dela
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @param depart
	 */
	public static void fermerSpecialisationDate(EOEditingContext edc,EOIndividu individu, NSTimestamp date) {
		EOCarriereSpecialisations spec = findForIndividuAndDate(edc,individu,date);
		if (spec != null) {
			spec.setSpecFin(date);
		}
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		if (specDebut() == null) {
			throw new NSValidation.ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_DEBUT_NON_RENSEIGNE, "SPECIALISATIONS"));
		}
	
		if (specFin() != null && specDebut().after(specFin())) {
			throw new ValidationException(String.format(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT, "SPECIALISATIONS"));
		}
	
		if (toCarriere() == null)
			throw new NSValidation.ValidationException("Pas de segment de carrière associé !");

		if (toBap() == null && toReferensEmploi() == null && 
				toSpecialiteAtos() == null && toCnu() == null && toCneca() == null 
				&& toSpecialiteItarf() == null && toDiscSecondDegre() == null)
			throw new NSValidation.ValidationException("Veuillez sélectionner une spécialisation !");

		if (specFin() != null && DateCtrl.isBefore(specFin(),specDebut())) {
			throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de début");
		}

		if (DateCtrl.isBetween(specDebut(), toCarriere().dateDebut(), toCarriere().dateFin()) == false )
			throw new NSValidation.ValidationException("La période de spécialisation doit être inclue dans le segment de carrière (" + DateCtrl.dateToString(toCarriere().dateDebut()) + " - " + DateCtrl.dateToString(toCarriere().dateFin()) + ") !");

		if (specFin() == null && toCarriere().dateFin() != null)
			throw new NSValidation.ValidationException("La période de spécialisation doit être inclue dans le segment de carrière (" + DateCtrl.dateToString(toCarriere().dateDebut()) + " - " + DateCtrl.dateToString(toCarriere().dateFin()) + ") !");

		if (specFin() != null && DateCtrl.isBetween(specFin(), toCarriere().dateDebut(), toCarriere().dateFin()) == false)
			throw new NSValidation.ValidationException("La période de spécialisation doit être inclue dans le segment de carrière (" + DateCtrl.dateToString(toCarriere().dateDebut()) + " - " + DateCtrl.dateToString(toCarriere().dateFin()) + ") !");

		if (dCreation() == null)
			setDCreation(DateCtrl.today());
		setDModification(DateCtrl.today());

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

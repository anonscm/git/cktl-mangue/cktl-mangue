// EOCorpsPromouvable.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue;

import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

// 28/02/2011 - Modification de la méthode de recherche des corps promouvables pour ajouter un qualifier
public class EOCorpsPromouvable extends _EOCorpsPromouvable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String PROMOTION_ECHELON = "E";
	public static final String PROMOTION_CHEVRON = "C";
	
    public EOCorpsPromouvable() {
        super();
    }

	public static EOCorpsPromouvable creer(EOEditingContext ec, String typePromotion) {
		
		EOCorpsPromouvable newObject = (EOCorpsPromouvable) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.setCoprPromotion(typePromotion);
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		return newObject;
	}
    
    public String dateDebut() {
		return SuperFinder.dateFormatee(this,D_DEB_VAL_KEY);
	}
	public void setDateDebut(String uneDate) {
		if (uneDate == null) {
			setDDebVal(null);
		} else {
			SuperFinder.setDateFormatee(this,D_DEB_VAL_KEY,uneDate);
		}
	}
	public String dateFin() {
		return SuperFinder.dateFormatee(this,D_FIN_VAL_KEY);
	}
	public void setDateFin(String uneDate) {
		if (uneDate == null) {
			setDFinVal(null);
		} else {
			SuperFinder.setDateFormatee(this,D_FIN_VAL_KEY,uneDate);
		}
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(corps(), CORPS_KEY);
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (corps() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un corps");
		}
		if (dDebVal() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de début de validité");
		}
	}

	
	public static NSArray rechercherCorpsPromouvablesPourPeriodeEtType(EOEditingContext ec,NSTimestamp dateDebut,NSTimestamp dateFin,String type) {
    	NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COPR_PROMOTION_KEY + " = %@",new NSArray(type)));

		if (dateDebut == null)
    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FIN_VAL_KEY + " = nil)",null));
    	else
    		qualifiers.addObject(SuperFinder.qualifierPourPeriode(D_DEB_VAL_KEY, dateDebut, D_FIN_VAL_KEY, dateFin));

    	return fetchAll(ec, new EOAndQualifier(qualifiers));
      }

	
	 /** Recherche les corps promouvables valides pendant la periode passee en parametre
	  * @param editingContext  
	  * @param dateDebut peut etre nulle
	  * @param dateFin peut etre nulle
	  * @param qualifier peut etre nul
	  */
	public static NSArray rechercherCorpsPromouvablesValidesPourPeriodeAvecQualifier(EOEditingContext ec,NSTimestamp dateDebut,NSTimestamp dateFin,EOQualifier qualifier) {
    	NSMutableArray qualifiers = new NSMutableArray();
    	if (dateDebut == null)
    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FIN_VAL_KEY + " = nil)",null));
    	else
    		qualifiers.addObject(SuperFinder.qualifierPourPeriode(D_DEB_VAL_KEY, dateDebut, D_FIN_VAL_KEY, dateFin));

    	if (qualifier != null)
    		qualifiers.addObject(qualifier);

    	return fetchAll(ec, new EOAndQualifier(qualifiers));
      }
	
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

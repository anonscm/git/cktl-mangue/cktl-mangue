/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrefsPersonnel.java instead.
package org.cocktail.mangue.modele.mangue;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPrefsPersonnel extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PrefsPersonnel";
	public static final String ENTITY_TABLE_NAME = "MANGUE.PREF_PERSONNEL";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prefOrdre";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String DIRECTORY_EXPORTS_KEY = "directoryExports";
	public static final String DIRECTORY_IMPRESSION_KEY = "directoryImpression";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ONGLET_PAR_DEFAUT_KEY = "ongletParDefaut";
	public static final String PHOTO_KEY = "photo";
	public static final String POSITION_LISTE_AGENTS_KEY = "positionListeAgents";
	public static final String TEM_AFFICHER_AU_DEMARRAGE_KEY = "temAfficherAuDemarrage";
	public static final String TEM_AFFICHER_EMPLOIS_KEY = "temAfficherEmplois";
	public static final String TEM_AFFICHER_POSTES_KEY = "temAfficherPostes";
	public static final String TEM_CONGE_ANNEE_UNIV_KEY = "temCongeAnneeUniv";
	public static final String TEM_EXCLU_HEBERGES_KEY = "temExcluHeberges";
	public static final String TEM_FENETRE_KEY = "temFenetre";
	public static final String TEM_LISTE_AGENTS_NEW_KEY = "temListeAgentsNew";
	public static final String TYPE_EMPLOI_AU_DEMARRAGE_KEY = "typeEmploiAuDemarrage";
	public static final String TYPE_PERSONNEL_AU_DEMARRAGE_KEY = "typePersonnelAuDemarrage";
	public static final String TYPE_POSTE_AU_DEMARRAGE_KEY = "typePosteAuDemarrage";
	public static final String TYPES_POPULATION_KEY = "typesPopulation";

// Attributs non visibles
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_TYPE_ABSENCE_KEY = "cTypeAbsence";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String PREF_ORDRE_KEY = "prefOrdre";
	public static final String TADR_CODE_KEY = "tadrCode";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DIRECTORY_EXPORTS_COLKEY = "DIRECTORY_Exports";
	public static final String DIRECTORY_IMPRESSION_COLKEY = "DIRECTORY_IMPRESSION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ONGLET_PAR_DEFAUT_COLKEY = "ONGLET_PAR_DEFAUT";
	public static final String PHOTO_COLKEY = "PHOTO";
	public static final String POSITION_LISTE_AGENTS_COLKEY = "POSITION_LISTE_AGENTS";
	public static final String TEM_AFFICHER_AU_DEMARRAGE_COLKEY = "TEM_AFFICHER_AU_DEMARRAGE";
	public static final String TEM_AFFICHER_EMPLOIS_COLKEY = "TEM_AFFICHER_EMPLOIS";
	public static final String TEM_AFFICHER_POSTES_COLKEY = "TEM_AFFICHER_POSTES";
	public static final String TEM_CONGE_ANNEE_UNIV_COLKEY = "TEM_CONGE_ANNEE_UNIV";
	public static final String TEM_EXCLU_HEBERGES_COLKEY = "TEM_EXCLU_HEBERGES";
	public static final String TEM_FENETRE_COLKEY = "TEM_FENETRE";
	public static final String TEM_LISTE_AGENTS_NEW_COLKEY = "TEM_LISTE_AGENTS_NEW";
	public static final String TYPE_EMPLOI_AU_DEMARRAGE_COLKEY = "TYPE_EMPLOI_DEMARRAGE";
	public static final String TYPE_PERSONNEL_AU_DEMARRAGE_COLKEY = "TYPE_PERSONNEL_DEMARRAGE";
	public static final String TYPE_POSTE_AU_DEMARRAGE_COLKEY = "TYPE_POSTE_DEMARRAGE";
	public static final String TYPES_POPULATION_COLKEY = "TYPES_POPULATION";

	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String C_TYPE_ABSENCE_COLKEY = "C_TYPE_ABSENCE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String PREF_ORDRE_COLKEY = "PREF_ORDRE";
	public static final String TADR_CODE_COLKEY = "TADR_CODE";


	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String STRUCTURE_KEY = "structure";
	public static final String TYPE_ABSENCE_KEY = "typeAbsence";
	public static final String TYPE_ADRESSE_KEY = "typeAdresse";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public String directoryExports() {
    return (String) storedValueForKey(DIRECTORY_EXPORTS_KEY);
  }

  public void setDirectoryExports(String value) {
    takeStoredValueForKey(value, DIRECTORY_EXPORTS_KEY);
  }

  public String directoryImpression() {
    return (String) storedValueForKey(DIRECTORY_IMPRESSION_KEY);
  }

  public void setDirectoryImpression(String value) {
    takeStoredValueForKey(value, DIRECTORY_IMPRESSION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String ongletParDefaut() {
    return (String) storedValueForKey(ONGLET_PAR_DEFAUT_KEY);
  }

  public void setOngletParDefaut(String value) {
    takeStoredValueForKey(value, ONGLET_PAR_DEFAUT_KEY);
  }

  public String photo() {
    return (String) storedValueForKey(PHOTO_KEY);
  }

  public void setPhoto(String value) {
    takeStoredValueForKey(value, PHOTO_KEY);
  }

  public String positionListeAgents() {
    return (String) storedValueForKey(POSITION_LISTE_AGENTS_KEY);
  }

  public void setPositionListeAgents(String value) {
    takeStoredValueForKey(value, POSITION_LISTE_AGENTS_KEY);
  }

  public String temAfficherAuDemarrage() {
    return (String) storedValueForKey(TEM_AFFICHER_AU_DEMARRAGE_KEY);
  }

  public void setTemAfficherAuDemarrage(String value) {
    takeStoredValueForKey(value, TEM_AFFICHER_AU_DEMARRAGE_KEY);
  }

  public String temAfficherEmplois() {
    return (String) storedValueForKey(TEM_AFFICHER_EMPLOIS_KEY);
  }

  public void setTemAfficherEmplois(String value) {
    takeStoredValueForKey(value, TEM_AFFICHER_EMPLOIS_KEY);
  }

  public String temAfficherPostes() {
    return (String) storedValueForKey(TEM_AFFICHER_POSTES_KEY);
  }

  public void setTemAfficherPostes(String value) {
    takeStoredValueForKey(value, TEM_AFFICHER_POSTES_KEY);
  }

  public String temCongeAnneeUniv() {
    return (String) storedValueForKey(TEM_CONGE_ANNEE_UNIV_KEY);
  }

  public void setTemCongeAnneeUniv(String value) {
    takeStoredValueForKey(value, TEM_CONGE_ANNEE_UNIV_KEY);
  }

  public String temExcluHeberges() {
    return (String) storedValueForKey(TEM_EXCLU_HEBERGES_KEY);
  }

  public void setTemExcluHeberges(String value) {
    takeStoredValueForKey(value, TEM_EXCLU_HEBERGES_KEY);
  }

  public String temFenetre() {
    return (String) storedValueForKey(TEM_FENETRE_KEY);
  }

  public void setTemFenetre(String value) {
    takeStoredValueForKey(value, TEM_FENETRE_KEY);
  }

  public String temListeAgentsNew() {
    return (String) storedValueForKey(TEM_LISTE_AGENTS_NEW_KEY);
  }

  public void setTemListeAgentsNew(String value) {
    takeStoredValueForKey(value, TEM_LISTE_AGENTS_NEW_KEY);
  }

  public Integer typeEmploiAuDemarrage() {
    return (Integer) storedValueForKey(TYPE_EMPLOI_AU_DEMARRAGE_KEY);
  }

  public void setTypeEmploiAuDemarrage(Integer value) {
    takeStoredValueForKey(value, TYPE_EMPLOI_AU_DEMARRAGE_KEY);
  }

  public Integer typePersonnelAuDemarrage() {
    return (Integer) storedValueForKey(TYPE_PERSONNEL_AU_DEMARRAGE_KEY);
  }

  public void setTypePersonnelAuDemarrage(Integer value) {
    takeStoredValueForKey(value, TYPE_PERSONNEL_AU_DEMARRAGE_KEY);
  }

  public Integer typePosteAuDemarrage() {
    return (Integer) storedValueForKey(TYPE_POSTE_AU_DEMARRAGE_KEY);
  }

  public void setTypePosteAuDemarrage(Integer value) {
    takeStoredValueForKey(value, TYPE_POSTE_AU_DEMARRAGE_KEY);
  }

  public String typesPopulation() {
    return (String) storedValueForKey(TYPES_POPULATION_KEY);
  }

  public void setTypesPopulation(String value) {
    takeStoredValueForKey(value, TYPES_POPULATION_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOStructure structure() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(STRUCTURE_KEY);
  }

  public void setStructureRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence typeAbsence() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence)storedValueForKey(TYPE_ABSENCE_KEY);
  }

  public void setTypeAbsenceRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence oldValue = typeAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ABSENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ABSENCE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse typeAdresse() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse)storedValueForKey(TYPE_ADRESSE_KEY);
  }

  public void setTypeAdresseRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse oldValue = typeAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ADRESSE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPrefsPersonnel avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrefsPersonnel createEOPrefsPersonnel(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temAfficherAuDemarrage
, String temAfficherEmplois
, String temAfficherPostes
, String temCongeAnneeUniv
, String temExcluHeberges
, String temFenetre
, String temListeAgentsNew
, Integer typeEmploiAuDemarrage
, Integer typePersonnelAuDemarrage
, Integer typePosteAuDemarrage
			) {
    EOPrefsPersonnel eo = (EOPrefsPersonnel) createAndInsertInstance(editingContext, _EOPrefsPersonnel.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemAfficherAuDemarrage(temAfficherAuDemarrage);
		eo.setTemAfficherEmplois(temAfficherEmplois);
		eo.setTemAfficherPostes(temAfficherPostes);
		eo.setTemCongeAnneeUniv(temCongeAnneeUniv);
		eo.setTemExcluHeberges(temExcluHeberges);
		eo.setTemFenetre(temFenetre);
		eo.setTemListeAgentsNew(temListeAgentsNew);
		eo.setTypeEmploiAuDemarrage(typeEmploiAuDemarrage);
		eo.setTypePersonnelAuDemarrage(typePersonnelAuDemarrage);
		eo.setTypePosteAuDemarrage(typePosteAuDemarrage);
    return eo;
  }

  
	  public EOPrefsPersonnel localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrefsPersonnel)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrefsPersonnel creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrefsPersonnel creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPrefsPersonnel object = (EOPrefsPersonnel)createAndInsertInstance(editingContext, _EOPrefsPersonnel.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrefsPersonnel localInstanceIn(EOEditingContext editingContext, EOPrefsPersonnel eo) {
    EOPrefsPersonnel localInstance = (eo == null) ? null : (EOPrefsPersonnel)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrefsPersonnel#localInstanceIn a la place.
   */
	public static EOPrefsPersonnel localInstanceOf(EOEditingContext editingContext, EOPrefsPersonnel eo) {
		return EOPrefsPersonnel.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrefsPersonnel fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrefsPersonnel fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrefsPersonnel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrefsPersonnel)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrefsPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrefsPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrefsPersonnel eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrefsPersonnel)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrefsPersonnel fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrefsPersonnel eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrefsPersonnel ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrefsPersonnel fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

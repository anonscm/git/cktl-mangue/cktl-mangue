package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.modele.conges.interfaces.IConge;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSTimestamp;

public abstract class EOConge extends EOGenericRecord implements IConge{

	// Definition des attributs communs a tous les conges
	
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final String INDIVIDU_KEY = "individu";
	
	// A VERIFIER ! Les conges sont ils tous lies a une absence - Il faudrait ! ?
	public static final String ABSENCE_KEY = "absence";

	// A TRAITER ! Les conges avec Arretes - Verifier s'ils ont tous cet attribut
	
	
	/* (non-Javadoc)
	 * @see org.cocktail.mangue.modele.mangue.conges.IConge#dateDebut()
	 */
	public NSTimestamp dateDebut() {
		return (NSTimestamp)storedValueForKey(DATE_DEBUT_KEY);
	}

	public void setDateDebut(NSTimestamp value) {
		takeStoredValueForKey(value, DATE_DEBUT_KEY);
	}

	/* (non-Javadoc)
	 * @see org.cocktail.mangue.modele.mangue.conges.IConge#dateFin()
	 */
	public NSTimestamp dateFin() {
		return (NSTimestamp)storedValueForKey(DATE_FIN_KEY);
	}

	public void setDateFin(NSTimestamp value) {
		takeStoredValueForKey(value, DATE_FIN_KEY);
	}

	/* (non-Javadoc)
	 * @see org.cocktail.mangue.modele.mangue.conges.IConge#dCreation()
	 */
	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, D_CREATION_KEY);
	}

	/* (non-Javadoc)
	 * @see org.cocktail.mangue.modele.mangue.conges.IConge#dModification()
	 */
	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	/* (non-Javadoc)
	 * @see org.cocktail.mangue.modele.mangue.conges.IConge#commentaire()
	 */
	public String commentaire() {
		return (String)storedValueForKey(COMMENTAIRE_KEY);
	}
	public void setCommentaire(String value) {
		takeStoredValueForKey(value, COMMENTAIRE_KEY);
	}
	public String temValide() {
		return (String)storedValueForKey(TEM_VALIDE_KEY);
	}
	public void setTemValide(String value) {
		takeStoredValueForKey(value, TEM_VALIDE_KEY);
	}	

	/* (non-Javadoc)
	 * @see org.cocktail.mangue.modele.mangue.conges.IConge#individu()
	 */
	public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
		return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
	}

	public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
		if (value == null) {
			org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
		}
	}	

	/* (non-Javadoc)
	 * @see org.cocktail.mangue.modele.mangue.conges.IConge#absence()
	 */
	public org.cocktail.mangue.modele.mangue.individu.EOAbsences absence() {
		return (org.cocktail.mangue.modele.mangue.individu.EOAbsences)storedValueForKey(ABSENCE_KEY);
	}

	public void setAbsenceRelationship(org.cocktail.mangue.modele.mangue.individu.EOAbsences value) {
		if (value == null) {
			org.cocktail.mangue.modele.mangue.individu.EOAbsences oldValue = absence();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ABSENCE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, ABSENCE_KEY);
		}
	}
	
	
}

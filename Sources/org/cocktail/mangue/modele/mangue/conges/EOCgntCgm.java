// EOCgntCgm.java
// Created on Tue Feb 14 09:17:51 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Le conge de grave maladie ne concerne que les agents contractuels ou les contractuels assimiles qui ont une carriere (TEM_fonctionnaire = N dans type_population).<BR>
 * L'agent doit avoir un contrat de travail ou une carriere de non fonctionnaire en activite ou detachement avec+carriere 
 * d'accueil pendant tout le conge.<BR>
 * La duree maximale du conge est de 6 mois<BR>
 * */
public class EOCgntCgm extends _EOCgntCgm {

    public EOCgntCgm() {
        super();
    }

   public String typeEvenement() {
		return EOTypeAbsence.TYPE_CONGE_MALADIE_CGM;
	}
	/** utilise pour la communication avec GRhum */
	public String parametreOccupation() {
			return "CL_CGM";
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOCgntCgm creer(EOEditingContext ec, EOIndividu individu) {
		return creer(ec, individu, null);
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOCgntCgm creer(EOEditingContext edc, EOIndividu individu, EOAbsences absence) {

		EOCgntCgm newObject = new EOCgntCgm();    

		newObject.setIndividuRelationship(individu);
        newObject.setAbsenceRelationship(absence);        
		newObject.setEstValide(true);
		newObject.setTemConfirme("O");
		newObject.setTemEnCause("N");
		newObject.setTemGestEtab("O");
		edc.insertObject(newObject);
		return newObject;
	}

	/**
	 * 
	 * @param ec
	 * @param absences
	 * @param dateDebut
	 * @param dateFin
	 * @param dateComite
	 * @throws Exception
	 */
	public static void requalifier(EOEditingContext edc, NSArray<EOAbsences> absences, NSTimestamp dateDebut, NSTimestamp dateFin, NSTimestamp dateComite) throws Exception {

		try {
			
			EOAbsences premiereAbsence = absences.get(0);

			int indexAbsences = 0;
			EOAbsences absencePrecedente = null;		
			for (EOAbsences absence : absences) {

				if (!absence.toTypeAbsence().estCongeMaladieContractuel())
					throw new ValidationException("Un des congés sélectionnés n'est pas un congé maladie de Non Titulaire !");

				if (absences.size() > 1) {
					if (absencePrecedente != null && DateCtrl.nbJoursEntre(absencePrecedente.dateFin(), absence.dateDebut(), false) != 1)
						throw new ValidationException("Les congés maladie à requalifier ne sont pas continus !");
				}

				absencePrecedente = absence;

				// On traite la premiere absence selectionnee
				if (indexAbsences == 0) {

					// Si la date de debut est superieure à la date de debut de la premiere absence, 
					// on conserve la premiere absence en modifiant la date de fin.
					if (DateCtrl.isAfter(dateDebut, absence.dateDebut())) {
						
						absence.setDateFin(DateCtrl.jourPrecedent(dateDebut));
						EOCgntMaladie congeMaladie = (EOCgntMaladie)rechercherCongeAvecAbsence(edc, EOCgntMaladie.ENTITY_NAME, absence);
						if (congeMaladie != null) {
							congeMaladie.setDateFin(DateCtrl.jourPrecedent(dateDebut));
						}
					}
					else {
						supprimerConge(edc, absence);
					}
				}
				else {
					supprimerConge(edc, absence);
				}

				indexAbsences++;
			}

			// Creation du CGM et de l'Absence
			EOAbsences absence = EOAbsences.creer(edc, premiereAbsence.individu(), EOTypeAbsence.findForCode(edc, EOTypeAbsence.TYPE_CONGE_MALADIE_CGM));
			absence.setDateDebut(dateDebut);
			absence.setDateFin(dateFin);

			EOCgntCgm cgm = creer(edc, premiereAbsence.individu(), absence);			
			cgm.initAvecDates(dateDebut, dateFin);
			cgm.setDComMedCgm(dateComite);
			cgm.setCommentaire("Requalification Congé Maladie NT");						
		}
		catch (Exception e) {  
			e.printStackTrace();
			throw e;
		}
	}
	
	
	
	/**
	 * Suppression d'un conge a partir d'une absence donnee (CLM ou CMO).
	 * 
	 * @param ec
	 * @param absence
	 */
	private static void supprimerConge(EOEditingContext ec, EOAbsences absence) {
		
		if (absence.toTypeAbsence().estCongePourContractuel()) {
			EOCgntMaladie congeMaladie = (EOCgntMaladie)rechercherCongeAvecAbsence(ec, EOCgntMaladie.ENTITY_NAME, absence);
			if (congeMaladie != null) {
				congeMaladie.setTemValide("N");
			}
		}

		absence.setEstValide(false);
		
	}


	/**
	 * 
	 */
	public void validateForSave() {
		
		super.validateForSave();
		
		if (!individu().estContractuelSurPeriodeComplete(dateDebut(), dateFin())) {
			throw new NSValidation.ValidationException("L'agent doit avoir un contrat de travail ou une carrière de non fonctionnaire en activité ou détaché dans l'établissement pendant tout le congé");
		}
		int nbMois = DateCtrl.calculerDureeEnMois(dateDebut(), dateFin(), true).intValue();
		
		// on ne vérifie pas les durées minimum car ce n'est pas bloquant
		int nbMoisMax = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_DUREE_MAX);
		if (nbMois > nbMoisMax) {
			throw new NSValidation.ValidationException("La durée maximum d'un congé de grave maladie est de " + nbMoisMax + " mois");
		}
		// vérifier si il existe des congés continus pour une période de 3 ans et qu'il existe plus d'une année écoulée avant la reprise d'un nouveau Clm
		NSArray autresCgm = Duree.rechercherDureesPourEntiteAnterieuresADate(editingContext(), entityName(), individu(), dateDebut());
		if (autresCgm.count() > 0) {
			// triés par ordre décroissant
			autresCgm = EOSortOrdering.sortedArrayUsingKeyOrderArray(autresCgm, SORT_ARRAY_DATE_FIN_DESC);
			EOCgntCgm dernierCgm = (EOCgntCgm)autresCgm.get(0);
			int dureePeriodeRef = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_DELAI);
			if (DateCtrl.calculerDureeEnMois(dernierCgm.dateFin(), dateDebut(), true).intValue() < dureePeriodeRef * 12) {
				// moins d'un an écoulé
				nbMoisMax = EOCgntCgm.nbMoisMaxCongesContinus(editingContext());
				if (dernierCgm.dureeEnMoisCongesContinusAssocies() == nbMoisMax) {	// 3 ans de congés continus
					throw new NSValidation.ValidationException("Suite à " + nbMoisMax / 12 + " ans de congés de grave maladie continus, l'agent doit avoir repris son travail pendant au moins " + dureePeriodeRef + " an pour pouvoir reprendre un congé de grave maladie");
				}
			}
		}
		
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static int nbMoisMaxCongesContinus(EOEditingContext editingContext) {
		return EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_PLEIN_TRAITEMENT).intValue() 
				+ EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_DEMI_TRAITEMENT).intValue();
	}

	/**
	 * 
	 */
    public NSArray details() {
    		return DetailConge.preparerDetailsCgm(editingContext(),individu(), dateDebut(), dateFin());
    }
	/** qualifier de date a pour la periode de remise en cause : pour les conges hospitalo-univ c'est le conge
	 * commencant le jour suivant: l'agent recupere la totalite de ses droits a conge des 
	 * le premier jour de reprise de travail*/
	protected  EOQualifier qualifierPeriodeRemiseEnCause() {
		return EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + " = %@", new NSArray(DateCtrl.jourSuivant(dateFin())));
	}
}

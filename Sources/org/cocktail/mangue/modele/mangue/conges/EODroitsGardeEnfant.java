// EODroitsGardeEnfant.java
// Created on Fri Mar 14 13:50:49  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EODroitsGardeEnfant extends _EODroitsGardeEnfant {
	
	public static int NB_DEMI_JOURNEES_GARDE = 10;
	public static int NB_DEMI_JOURNEES_SUPPLEMENTAIRE = 2;

	public EODroitsGardeEnfant() {
		super();
	}
	public void initAvecIndividu(EOIndividu individu) {
		setIndividuRelationship(individu);
	}
	public void supprimerRelations() {
		setIndividuRelationship(null);
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (annee() == null)
			throw new NSValidation.ValidationException("L'année est obligatoire");
		if (nbDemiJournees() == null)
			throw new NSValidation.ValidationException("Le nombre de demi-journées est obligatoire");
	}
	
	/** Recherche les droits de garde enfant d'un individu pour une annee
	 * @param editingContext
	 * @param individu
	 * @param annee (peut etre nulle)
	 * @return objets trouves
	 */
	public static EODroitsGardeEnfant findForIndividuAndDate(EOEditingContext edc, EOIndividu individu, NSTimestamp dateReference)	{
		try {
			return  rechercherDroitsGardePourIndividuEtAnnee(edc, individu, DateCtrl.getYear(dateReference));	
		}
		catch (Exception e) {
			return null;
		}
	}
	
	/** Recherche les droits de garde enfant d'un individu pour une annee
	 * @param editingContext
	 * @param individu
	 * @param annee (peut etre nulle)
	 * @return objets trouves
	 */
	public static EODroitsGardeEnfant rechercherDroitsGardePourIndividuEtAnnee(EOEditingContext edc, EOIndividu individu,Integer annee)	{

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray<EOIndividu>(individu)));			
			if (annee != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ANNEE_KEY + "=%@", new NSArray<Integer>(annee)));
			}
			return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return null;
		}
	}
	/** Recherche les droits de garde enfant d'un individu
	 * @param editingContext
	 * @param individu
	 * @return objets troues
	 */
	public static NSArray<EODroitsGardeEnfant> rechercherDroitsGardePourIndividu(EOEditingContext edc, EOIndividu individu) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray<EOIndividu>(individu));			
		return fetchAll(edc, qualifier, null);
	}
}

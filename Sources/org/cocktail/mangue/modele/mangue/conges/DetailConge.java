//DetailConge.java
//Created on Tue Jan 31 13:03:11 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import java.util.Enumeration;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Mod&eacute;lise un detail de conge. 
 * Comporte les regles de validation concernant les dates de debut et de fin
 * Tous les details de traitement sont calcules en jours comptables : 30 jours par mois pour tous les mois 
 */
public class DetailConge extends _DetailConge {
	private static NSTimestamp debutPeriodeGlissante;	// DT 60271 - pour afficher le début de la période glissante

	public static final String DUREE_CALENDAIRE_KEY = "duree";
	public static final String DUREE_COMPTABLE_KEY = "dureeComptable";
	
	public DetailConge() {
		super();
	}

	public String temJourCarence() {
		return (String) storedValueForKey("temJourCarence");
	}

	public void setTemJourCarence(String value) {
		takeStoredValueForKey(value, "temJourCarence");
	}


	public void initAvecConge(Conge conge,NSTimestamp dateDebut,NSTimestamp dateFin) {
		initAvecConge(conge);
		setDateDebut(dateDebut);
		setDateFin(dateFin);
	}
	public void initAvecConge(Conge conge) {
		setNumTaux(new Integer(100));
		setDenTaux(new Integer(100));
		addObjectToBothSidesOfRelationshipWithKey(conge,"conge");
	}
	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,"dateDebut");
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setDateDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,"dateDebut",uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,"dateFin");
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setDateFin(null);
		} else {
			SuperFinder.setDateFormatee(this,"dateFin",uneDate);
		}
	}

	public boolean estSansTraitement() {
		return numTaux() != null && numTaux().doubleValue() == 0;
	}
	public boolean estPleinTraitement() {
		return numTaux() != null && numTaux().doubleValue() == 100.0;
	}
	
	public void setEstPleinTraitement(boolean aBool) {
		if (aBool) {
			setNumTaux(new Integer(100));
		} else {
			setNumTaux(new Integer(50));
		}
	}
	public Number duree() {
		return new Integer(DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true));
	}
	public Number dureeComptable() {
		try {
			return new Integer(DateCtrl.dureeComptable(dateDebut(), dateFin(), true));
		} catch (Exception e) {
			return new Float(0);
		}
	}
	public void supprimerRelations() {
		setCongeRelationship(conge());
	}
	public void validateForSave() {

		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début du détail de rémunération doit etre définie !");
		} else if (dateFin() != null && dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException("La date de fin du détail de rémunération doit être postérieure à la date de début !");
		}
		if (conge() == null) {
			throw new NSValidation.ValidationException("Un congé doit être associé au détail de rémunération");
		}
		if (DateCtrl.isBefore(conge().dateFin(),dateDebut())) {
			throw new NSValidation.ValidationException("Le détail de rémunération ne peut commencer après la fin du congé");
		}
		if (DateCtrl.isBefore(dateDebut(),conge().dateDebut())) {
			throw new NSValidation.ValidationException("Le détail de rémunération ne peut commencer avant le début du congé");
		}
		if (DateCtrl.isBefore(dateFin(),conge().dateDebut())) {
			throw new NSValidation.ValidationException("Le détail de rémunération ne peut se terminer avant le début du congé");
		}
		if (DateCtrl.isBefore(conge().dateFin(),dateFin())) {
			throw new NSValidation.ValidationException("Le détail de rémunération ne peut se terminer après la fin du congé");
		}
	}

	// méthodes statiques
	/** retourne les details de conge longue duree d'un individu pour la periode passee en parametre,
	 * tries par date de debut croissant : ce sont les conges contigus au conge selectionne */
	public static NSArray preparerDetailsCld(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin) {
		System.out.println("DetailConge.preparerDetailsCld()");
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(dateDebut);
		args.addObject(dateFin);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("conge.individu = %@ AND dateDebut >= %@ AND dateFin <= %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(DetailConge.ENTITY_NAME,qual,new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));
		return editingContext.objectsWithFetchSpecification(fs);
	}
	
	/** retourne les details de conge de grave maladie pour la periode passee en parametre : ,
	 * tries par date de debut croissant */
	public static NSArray preparerDetailsCgm(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSArray conges = CongeAvecArreteAnnulation.rechercherCongesValidesPourIndividu(editingContext,"CgntCgm",individu);
		
		if (conges.count() == 0) {
			return null;
		}
		NSMutableArray periodesDetail = new NSMutableArray();
		Periode periode = periodeCongesContigus(conges,debutPeriode,finPeriode);

		int nbMoisPleinTraitement = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_PLEIN_TRAITEMENT);
		int nbMoisDemiTraitement = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CGM_DEMI_TRAITEMENT);

		NSTimestamp dateFinPleinTraitement = DateCtrl.dateAvecAjoutMois(periode.debut(),nbMoisPleinTraitement);
		// il faut inclure les bornes
		dateFinPleinTraitement = DateCtrl.jourPrecedent(dateFinPleinTraitement);

		if (DateCtrl.isBeforeEq(periode.fin(),dateFinPleinTraitement)) {
			periodesDetail.addObject(nouvellePeriode(periode.debut(),periode.fin(),new Integer(100)));
		} else {
			periodesDetail.addObject(nouvellePeriode(periode.debut(),dateFinPleinTraitement,new Integer(100)));
			NSTimestamp dateDebutDemiTraitement = DateCtrl.jourSuivant(dateFinPleinTraitement);
			NSTimestamp dateFinDemiTraitement = DateCtrl.dateAvecAjoutMois(dateDebutDemiTraitement,nbMoisDemiTraitement);
			System.out.println("DetailConge.preparerDetailsCgm() DEBUT DEMI " + dateDebutDemiTraitement);
			System.out.println("DetailConge.preparerDetailsCgm() FIN DEMI " + dateFinDemiTraitement);
			//	il faut inclure les bornes
			dateFinDemiTraitement = DateCtrl.jourPrecedent(dateFinDemiTraitement);
			if (DateCtrl.isBeforeEq(periode.fin(),dateFinDemiTraitement)) {
				System.out.println("DetailConge.preparerDetailsCgm()");
				periodesDetail.addObject(nouvellePeriode(dateDebutDemiTraitement,periode.fin(),new Integer(50)));
			} else {
				// le dépassement ne devrait pas se produire car limité par les règles de validation
				periodesDetail.addObject(nouvellePeriode(dateDebutDemiTraitement,dateFinDemiTraitement,new Integer(50)));
				periodesDetail.addObject(nouvellePeriode(DateCtrl.jourSuivant(dateFinDemiTraitement),periode.fin(),new Integer(0)));
			}
		}

		System.out.println("DetailConge.preparerDetailsCgm() >>>>> PERIODES DETAILS CGM");
		for (Enumeration<DetailConge> e=periodesDetail.objectEnumerator();e.hasMoreElements();) {
			
			System.out.println("DetailConge.preparerDetailsCgm() " + e.nextElement());
			
		}
		
		return periodesDetail;

	}
	/** retourne les details de conge hospitalo-univ d'un individu pour la periode passee en parametre,
	 * tries par date de debut croissant  : ce sont les conges contigus au conge selectionne */
	public static NSArray preparerDetailsCongeAl(EOEditingContext editingContext,String nomEntite,EOIndividu individuCourant,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSDictionary dict;
		if (nomEntite.equals("CongeAl3")) {
			dict = EOCongeAl3.dureeEnMoisPourTaux();
		} else if (nomEntite.equals("CongeAl4")) {
			dict = EOCongeAl4.dureeEnMoisPourTaux();
		} else if (nomEntite.equals("CongeAl5")) {
			dict = EOCongeAl5.dureeEnMoisPourTaux();
		} else {
			dict = EOCongeAl6.dureeEnMoisPourTaux();
		}
		// Pour les congés Al, on recherche tous les congés valides et ensuite on ne retient que les congés contigüs au congé courant
		NSArray conges = CongeAvecArreteAnnulation.rechercherCongesValidesPourIndividu(editingContext,nomEntite,individuCourant);
		if (conges.count() > 0) {
			Periode periode = periodeCongesContigus(conges,debutPeriode,finPeriode);
			NSMutableArray periodesDetail = new NSMutableArray();
			preparerDetailPourDates(periode.debut(),periode.fin(),dict,periodesDetail);	
			return periodesDetail;
		} else {
			return new NSArray();
		}
	}
	/** retourne les details de conge avec periode glissante d'un individu tries par date de debut croissant */
	public static NSArray preparerDetailsGlissantsPourPeriode(EOEditingContext editingContext,String nomEntite,EOIndividu individuCourant,NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		LogManager.logDetail(" ********* PREPARER DETAILS GLISSANTS ********** ");

		// rechercher les congés commençant avant la date de fin
		NSArray conges = CongeAvecArreteAnnulation.rechercherCongesValidesPourIndividuAnterieurs(editingContext,nomEntite,individuCourant,finPeriode);
		conges = EOSortOrdering.sortedArrayUsingKeyOrderArray(conges, new NSArray(EOSortOrdering.sortOrderingWithKey(DATE_DEBUT_KEY,EOSortOrdering.CompareAscending)));
		if (conges.count() == 0) {
			return null;
		}
		// pour les contractuels avec des contrats non continus, la période de référence est exprimée en jours dans le cas des congés de maladie
		// dans tous les autres cas, elle est exprimée en année
		int nbAnneeGlissement = 0,nbJoursGlissement = 0;
		if (nomEntite.equals("CgntMaladie")) {
			NSTimestamp dateDebut = ((Conge)conges.objectAtIndex(0)).dateDebut(),dateFin = ((Conge)conges.lastObject()).dateFin();
			if (individuCourant.estContractuelSurPeriodeComplete(dateDebut,dateFin) == false) {
				conges = EOSortOrdering.sortedArrayUsingKeyOrderArray(conges, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
				nbJoursGlissement = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CMNT_PERIODE_REFERENCE_D).intValue();
			}
		}
		if (nbJoursGlissement == 0) {
			nbAnneeGlissement = evaluerAnneeGlissante(editingContext,nomEntite);
		}

		NSArray periodesDetail = preparerDetailsGlissants(editingContext,conges,nomEntite,nbJoursGlissement,nbAnneeGlissement);
		// limiter les détails à la période passée en paramètre
		NSMutableArray results = new NSMutableArray();
		if (nbJoursGlissement != 0) {
			debutPeriodeGlissante = DateCtrl.dateAvecAjoutJours(debutPeriode,-nbJoursGlissement);
		} else {
			debutPeriodeGlissante = DateCtrl.dateAvecAjoutAnnees(debutPeriode,-nbAnneeGlissement);
		}
		LogManager.logDetail(" Début période glissante : " + debutPeriodeGlissante);

		for (java.util.Enumeration<DetailConge> e = periodesDetail.objectEnumerator();e.hasMoreElements();) {
			DetailConge detail = e.nextElement();
			if (DateCtrl.isAfterEq(detail.dateFin(),debutPeriodeGlissante) && DateCtrl.isBeforeEq(detail.dateFin(),finPeriode))
				results.addObject(detail);
		}
		return results;	
	}

	/** retourne la duree de la periode de glissement en annee */
	public static int evaluerAnneeGlissante(EOEditingContext editingContext,String nomEntite) {
		if (nomEntite.equals(EOClm.ENTITY_NAME)) {
			return 4;
		} else if (nomEntite.equals(EOCongeMaladie.ENTITY_NAME)) {
			return EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CMO_PERIODE_REFERENCE).intValue() / 12;
		} else if (nomEntite.equals(EOCgntMaladie.ENTITY_NAME)) {
			return EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CMNT_PERIODE_REFERENCE_C).intValue() / 12;
		} else {
			return 0;
		}
	}

	public static void calculerDebutPeriodeGlissante(EOEditingContext ec, NSTimestamp debutPeriode, String nomEntite) {

		int nbAnneeGlissement = evaluerAnneeGlissante(ec,nomEntite);
		debutPeriodeGlissante = DateCtrl.dateAvecAjoutAnnees(debutPeriode,-nbAnneeGlissement);

	}


	/** retourne la date debut de la periode glissante */
	public static NSTimestamp debutPeriodeGlissante() {
		return debutPeriodeGlissante;
	}




	// méthodes privées
	// Prépare les détails glissants pour les Clm, CongeMaladie et CgntMaladie. 
	// Modifiée le 25/08/09 pour corriger la gestion des congés de maladie non titulaires et transformer la 
	// gestion de durée comptable en durée calendaire et inversement la durée des congés de maladie titulaire en durée comptable
	// Pour les congés de maladie non titulaires, les calculs sont faits en durée calendaire
	// Pour les congés maladie titulaires et clms en durée comptable
	private static NSArray preparerDetailsGlissants(EOEditingContext editingContext,NSArray conges,String nomEntite, int nbJoursGlissement,int nbAnneeGlissement) {

		NSMutableArray periodesDetail = new NSMutableArray();
		LogManager.logDetail("preparerDetailsGlissants - nbJoursGlissement " + nbJoursGlissement + " nbAnneeGlissement " + nbAnneeGlissement);
		int nbJoursPleinTraitement = evaluerNbJours(editingContext,nomEntite,true);
		int nbJoursDemiTraitement = evaluerNbJours(editingContext,nomEntite,false);
		int nbJoursTotal = 0;	// pour stocker le nombre de jours total payé à plein traitement et demi-traitement
		int nbJoursComparaisonTotal = 0; // pour stocker le nombre de jours de comparaison payé à plein traitement et demi-traitement
		for (java.util.Enumeration<Conge> e = conges.objectEnumerator();e.hasMoreElements();) {
			Conge conge = e.nextElement();
			boolean estCongeNonTitulaire = conge instanceof EOCgntMaladie;
			
			// On génère un tableau avec les periodes de traitement à taux plein ou taux partiel
			NSTimestamp dateATraiter = conge.dateDebut();
			NSTimestamp dateFin = conge.dateFin();
			NSMutableArray	tableDonneesJours = new NSMutableArray();
			// Utilisé pour indiquer le nombre de jours supplémentaires de plein traitement ajouté
			int nbJoursSupPleinTraitement = 1;	// on part de 1 pour indiquer que le jour courant est à prendre en compte
			if (estCongeNonTitulaire) {
				// pour les congés de maladie contractuels, le nombre de jours de plein traitement est fonction
				// de l'ancienneté
				nbJoursPleinTraitement = ((EOCgntMaladie)conge).anciennete().dureePleinTrait().intValue() * 30;	// durée exprimée en mois
				nbJoursDemiTraitement = ((EOCgntMaladie)conge).anciennete().dureeDemiTrait().intValue() * 30;		// durée exprimée en mois
				// 24/08/09 - DT Insa Lyon sur les congés maladie pour une ancienneté < 4 mois
				if (nbJoursPleinTraitement == 0 && nbJoursDemiTraitement == 0) {
					nbJoursSupPleinTraitement = 0;
				}
			}
			LogManager.logDetail("preparerDetailsGlissants - nbJoursPleinTraitement revu " + nbJoursPleinTraitement + " nbJoursDemiTraitement revu " + nbJoursDemiTraitement);
			// Pour chaque jour du congé, on calcule le droit à congé en tant que plein traitement, demi-traitement ou sans rémunération
			// en utilisant des dates glissantes
			while (DateCtrl.isBeforeEq(dateATraiter,dateFin)) {			
				// On enregistre le jour, le nbre de jours de congés en partant depuis le nb d'année de glissements
				// ainsi que le traitement : demi ou plein, jour par jour
				NSMutableDictionary donneesJours = new NSMutableDictionary();
				donneesJours.setObjectForKey(dateATraiter,"jour");
				nbJoursTotal++;
				NSTimestamp	borne1;
				// déterminer la borne inférieure en retranchant selon les congés le nombre de jours ou le nombre d'années de glissement
				if (nbJoursGlissement > 0) {
					borne1 = DateCtrl.dateAvecAjoutJours(dateATraiter,-nbJoursGlissement);		// Date a traiter moins nbJours
				} else {
					borne1 = DateCtrl.dateAvecAjoutAnnees(dateATraiter,-nbAnneeGlissement);		// Date a traiter moins nb années glissement.
					borne1 = DateCtrl.dateAvecAjoutJours(borne1, 1);							// il faut partir du jour j+1
				}
				NSTimestamp borne2 = DateCtrl.jourPrecedent(dateATraiter);
				// Calculer le nombre de jours comptables de plein traitement pendant les congés précédents sauf pour les congés
				// non titulaires où on calcule sur la base du nombre de jours
				int joursPleinTraitement = nbJoursPleinTraitement(periodesDetail,borne1,borne2,!estCongeNonTitulaire);

				// Pour comparer par rapport au nombre autorisé de jours plein traitement, on ramène le nombre de
				// jours supplémentaires à plein traitement à la durée comptable entre la date de début de congé
				// et la date lorsqu'on ajoute le nombre de jours de plein traitement sauf pour les congés ordinaires de maladie
				int dureeSupplementairePleinTraitement = 0;
				// 25/08/09- if (nomEntite.equals("CongeMaladie")) {
				if (estCongeNonTitulaire) {
					dureeSupplementairePleinTraitement = nbJoursSupPleinTraitement;
				} else {
					try {
						NSTimestamp dateComparaison = DateCtrl.dateAvecAjoutJours(conge.dateDebut(),nbJoursSupPleinTraitement -1); 	// on enlève 1 car nbJoursSupPleinTraitement est initialisé à 1
						dureeSupplementairePleinTraitement = DateCtrl.dureeComptable(conge.dateDebut(), dateComparaison,true);
						LogManager.logDetail("preparerDetailsGlissants - date deb : " + DateCtrl.dateToString(conge.dateDebut()) + ", nbJoursSupPleinTraitement - 1 " + (nbJoursSupPleinTraitement - 1) + " date avec ajout : " + DateCtrl.dateToString(dateComparaison) + " duree supp " + dureeSupplementairePleinTraitement);
					} catch(Exception exc) {
						// ne doit pas arriver dateDebut et dateFin sont définies
					}
				}

				int nbJoursComparaison = joursPleinTraitement + dureeSupplementairePleinTraitement;
				LogManager.logDetail("Jours PT : " + joursPleinTraitement + ", nbJoursComparaison : " + nbJoursComparaison);

				// Correction du 12/11/09 suite commentaire Mulhouse
				// if (nbJoursComparaison > 0 && nbJoursComparaison <= nbJoursPleinTraitement) { 
				// pose un problème lorsque le congé commence le 31 d'un mois
				if (nbJoursComparaison >= 0 && nbJoursComparaison <= nbJoursPleinTraitement) {
					nbJoursSupPleinTraitement++;	// on vient d'ajouter 1 journée à plein traitement
					donneesJours.setObjectForKey((Number)new Integer(100),"taux");
					if (nbJoursComparaison == nbJoursPleinTraitement) {
						nbJoursTotal = nbJoursComparaison;
						nbJoursComparaisonTotal = nbJoursComparaison;
					}
				} else {
					// Pour comparer par rapport au nombre autorisé de jours plein traitement, on ramène le nombre de
					// jours total à la durée comptable entre la date de début de congé
					// et la date lorsqu'on ajoute le nombre de jours total sauf pour les congés de maladie de titulaire
					//int nbJoursComparaisonTotal = 0;
					// 25/08/09 - if (nomEntite.equals("CongeMaladie")) {
					if (estCongeNonTitulaire) {
						nbJoursComparaisonTotal = nbJoursTotal;
					} else {
						try {
							// 25/08/09 - corrigé pour supprimer la prise en compte du 31 d'un mois lors de l'évaluation des durées comptables
							nbJoursComparaisonTotal += DateCtrl.dureeComptable(dateATraiter, dateATraiter, true);
						} catch(Exception exc) {
							// ne doit pas arriver dateATraiter est définie
						}
					}
					if (nbJoursComparaisonTotal <= nbJoursPleinTraitement + nbJoursDemiTraitement) {
						donneesJours.setObjectForKey((Number)new Integer(50),"taux");
					} else { 
						// si on depasse le nombre de jours à plein traitement et le nombre de jours à demi-traitement
						// pas de rémunération => taux nul
						donneesJours.setObjectForKey((Number)new Integer(0),"taux");
					}
				}
				tableDonneesJours.addObject(donneesJours);
				dateATraiter = DateCtrl.jourSuivant(dateATraiter);
			}
			// regrouper tous les jours au même taux
			Number taux = (Number)new Integer(-1);	// 25/09/08 - pour afficher les taux 0 des non titulaires
			NSMutableArray periodesTraitement = new NSMutableArray();
			NSTimestamp lastDate = null;
			LogManager.logDetail("    COUNT TABLE DONNEES JOURS : " + tableDonneesJours.count());
			for (java.util.Enumeration<NSDictionary> e1 = tableDonneesJours.objectEnumerator();e1.hasMoreElements();) {
				NSDictionary dicoTemp = e1.nextElement();
				if (taux.intValue() != ((Number)dicoTemp.objectForKey("taux")).intValue()) {
					// Si on change d'etat, on ajoute la date et l'etat
					if (periodesTraitement.count() > 0) {
						NSMutableDictionary periodePrecedente = (NSMutableDictionary)periodesTraitement.objectAtIndex(periodesTraitement.count() - 1);
						periodePrecedente.setObjectForKey(DateCtrl.jourPrecedent((NSTimestamp)dicoTemp.objectForKey("jour")),"fin");
					}
					NSMutableDictionary dicoTraitement = new NSMutableDictionary();
					dicoTraitement.setObjectForKey((NSTimestamp)dicoTemp.objectForKey("jour"),"debut");
					dicoTraitement.setObjectForKey(dicoTemp.objectForKey("taux"),"taux");
					taux = (Number)dicoTemp.objectForKey("taux");
					periodesTraitement.addObject(dicoTraitement);
				}
				lastDate = (NSTimestamp)dicoTemp.objectForKey("jour");
			}
			// Pour la dernière période
			if (periodesTraitement.count() > 0) {
				NSMutableDictionary periodePrecedente = (NSMutableDictionary)periodesTraitement.objectAtIndex(periodesTraitement.count() - 1);
				periodePrecedente.setObjectForKey(lastDate,"fin");
			}

			LogManager.logDetail("    COUNT PERIODES TRAITEMENT : " + periodesTraitement.count());
			// Créer les détails
			for (Enumeration<NSDictionary> e2 = periodesTraitement.objectEnumerator();e2.hasMoreElements();) {
				NSDictionary dicoTemp = e2.nextElement();
				DetailConge currentDetail = nouvellePeriode((NSTimestamp)dicoTemp.objectForKey("debut"),(NSTimestamp)dicoTemp.objectForKey("fin"),(Number)dicoTemp.objectForKey("taux"));
				periodesDetail.addObject(currentDetail);
			}
		}
		return periodesDetail;
	}

	// NB JOURS PLEIN TRAITEMENT : Renvoie le nombre de jours de maladie à plein traitement entre deux dates
	private static int nbJoursPleinTraitement(NSArray periodesDetail, NSTimestamp date1, NSTimestamp date2,boolean estDureeComptable) {
		NSTimestamp dateDebut = null, dateFin = null;
		float nbJours = (float)0.0;
		for (java.util.Enumeration<DetailConge> e = periodesDetail.objectEnumerator();e.hasMoreElements();) {
			DetailConge detail = e.nextElement();
			// si le détail de congé démarre avant la date de fin et se termine après la date de début
			// ils ont une période commune : c'est la période comprise entre le max des dates début et le min des dates fin
			if (DateCtrl.isBeforeEq(detail.dateDebut(),date2) && DateCtrl.isAfterEq(detail.dateFin(),date1) && detail.numTaux().intValue() == 100) {
				if (DateCtrl.isBeforeEq(detail.dateDebut(),date1)) {
					//dateDebut = DateCtrl.jourSuivant(date1.timestampByAddingGregorianUnits);
					dateDebut = date1;	// on prend en compte les bornes dans les calculs
				} else {
					dateDebut = detail.dateDebut();
					//dateDebut = DateCtrl.jourPrecedent(detail.dateDebut());
				}
				if (DateCtrl.isAfterEq(detail.dateFin(),date2)) {
					dateFin = date2;
				} else {
					dateFin = detail.dateFin();
				}
				if (DateCtrl.isBeforeEq(dateDebut,dateFin)) {
					// cas aux limites ou la date de début est égale ou passe après la date de fin
					// Pour chaque conge maladie , on calcule le nombre de jours comptables de l'absence
					try {
						nbJours = nbJours + DateCtrl.nbJoursEntre(dateDebut,dateFin,true,estDureeComptable);	
					} catch (Exception e1) {}
				}
			}
		}
		return (int)nbJours;
	}
	private static void preparerDetailPourDates(NSTimestamp dateDebut,NSTimestamp dateFin,NSDictionary dict,NSMutableArray periodesDetail) {
		LogManager.logDetail("periode : " + DateCtrl.dateToString(dateDebut) + " - " + DateCtrl.dateToString(dateFin));
		try {
			NSArray cles = dict.allKeys().sortedArrayUsingComparator(NSComparator.DescendingNumberComparator);
			for (int j = 0; j < cles.count();j++) {
				LogManager.logDetail("periode : " + DateCtrl.dateToString(dateDebut) + " - " + DateCtrl.dateToString(dateFin));
				int nbJoursCongeComptable = DateCtrl.dureeComptable(dateDebut,dateFin,true);
				LogManager.logDetail("duree comptable periode : " + nbJoursCongeComptable);
				Integer taux = (Integer)cles.objectAtIndex(j);
				int nbMois = ((Integer)dict.objectForKey(taux)).intValue();
				int nbJoursPourTaux = nbMois * DateCtrl.NB_JOURS_COMPTABLES_MENSUELS;		// durée fournie en mois
				LogManager.logDetail("taux a appliquer : " + taux + " pendant une duree maximum de " + nbJoursPourTaux + " jours");
				// Si le nombre de jours de congé comptable pour cette période est inférieur au nombre de jours maximum
				// toute la période est payée à ce taux
				if (nbJoursCongeComptable <= nbJoursPourTaux) {
					periodesDetail.addObject(nouvellePeriode(dateDebut,dateFin,taux));
					break;
				} else {
					// Sinon, on a une période qui va de date debut à date debut + nombre de mois à ajouter
					NSTimestamp finPeriode = DateCtrl.dateAvecAjoutMois(dateDebut,nbMois);
					finPeriode = DateCtrl.jourPrecedent(finPeriode);	// on inclut les bornes dans les calculs de durée
					if (j == (cles.count() - 1) && taux.intValue() == 0)	{
						// normalement ne devrait pas se produire car il y a un contrôle au niveau des congés
						periodesDetail.addObject(nouvellePeriode(dateDebut,dateFin,taux));
					} else {
						periodesDetail.addObject(nouvellePeriode(dateDebut,finPeriode,taux));
						dateDebut = DateCtrl.jourSuivant(finPeriode);
					}
				}
			}
		} catch (Exception e) {
			// ne devrait pas arriver
		}
	}
	public static DetailConge nouvellePeriode(NSTimestamp dateDebut,NSTimestamp dateFin,Number taux) {
		DetailConge detail = new DetailConge();
		detail.setDateDebut(dateDebut);
		detail.setDateFin(dateFin);
		detail.setNumTaux(new Integer(taux.intValue()));
		detail.setTemJourCarence("N");
		return detail;
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param nomEntite
	 * @param estPleinTraitement
	 * @return
	 */
	private static int evaluerNbJours(EOEditingContext editingContext,String nomEntite,boolean estPleinTraitement) {

		if (nomEntite.equals(EOClm.ENTITY_NAME)) {
			if (estPleinTraitement) {
				return EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_PLEIN_TRAITEMENT).intValue() * DateCtrl.NB_JOURS_COMPTABLES_MENSUELS;
			} else {
				return EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_DEMI_TRAITEMENT).intValue() * DateCtrl.NB_JOURS_COMPTABLES_MENSUELS;
			}
			// Durée fournie en mois. A ramener en jours comptables
		} else if (nomEntite.equals(EOCongeMaladie.ENTITY_NAME)) {
			if (estPleinTraitement) {
				return EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CMO_PLEIN_TRAITEMENT).intValue();
			} else {
				return EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CMO_DEMI_TRAITEMENT).intValue();
			}
		} 
		return 0;
	}

	/**
	 * 
	 * @param conges
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	private static Periode periodeCongesContigus(NSArray conges,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		// identifier le congé recherché, on devrait forcément le trouver
		int indexCongePourDetail = -1;
		for (int i = 0; i < conges.count();i++) {
			Conge conge = (Conge)conges.objectAtIndex(i);
			if (DateCtrl.isSameDay(conge.dateDebut(),debutPeriode) && DateCtrl.isSameDay(conge.dateFin(),finPeriode)) {
				indexCongePourDetail = i;
				break;
			}
		}
		// Ne garder que les congés contigüs à ce congé i.e ceux se terminant le jour précédent en prenant en compte
		// la contiguïté des congés précédents
		if (indexCongePourDetail >= 0) {
			NSTimestamp dateDebut = debutPeriode, dateFin = finPeriode;
			NSTimestamp dateComparaison = DateCtrl.jourPrecedent(debutPeriode);
			// chercher les congés contigüs précédents
			for (int i = indexCongePourDetail - 1; i >= 0;i--) {
				Conge conge = (Conge)conges.objectAtIndex(i);
				if (DateCtrl.isSameDay(conge.dateFin(),dateComparaison)) {
					dateDebut = conge.dateDebut();
					dateComparaison = DateCtrl.jourPrecedent(conge.dateDebut());
				} else {
					break;
				}
			}
			return new Periode(dateDebut,dateFin);
		} else {
			return null;
		}
	}
	private  static class Periode {
		private NSTimestamp debut;
		private NSTimestamp fin;
		// constructeur
		Periode(NSTimestamp dateDebut,NSTimestamp dateFin) {
			debut = dateDebut;
			fin = dateFin;
		}
		// accesseurs
		public NSTimestamp debut() {
			return debut;
		}
		public NSTimestamp fin() {
			return fin;
		}
	}




}

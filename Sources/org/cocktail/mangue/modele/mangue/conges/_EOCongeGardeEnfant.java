// _EOCongeGardeEnfant.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCongeGardeEnfant.java instead.
package org.cocktail.mangue.modele.mangue.conges;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOCongeGardeEnfant extends  CongeAvecArrete {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "CongeGardeEnfant";
	public static final String ENTITY_TABLE_NAME = "MANGUE.CONGE_GARDE_ENFANT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "congeOrdre";

	public static final String CG_AM_PM_DEBUT_KEY = "cgAmPmDebut";
	public static final String CG_AM_PM_FIN_KEY = "cgAmPmFin";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_ARRETE_KEY = "dateArrete";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_JUSTIFICATIF_KEY = "dJustificatif";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String NO_ARRETE_KEY = "noArrete";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String CONGE_ORDRE_KEY = "congeOrdre";
	public static final String NO_ENFANT_KEY = "noEnfant";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String ABS_NUMERO_KEY = "absNumero";

//Colonnes dans la base de donnees
	public static final String CG_AM_PM_DEBUT_COLKEY = "CG_AM_PM_DEBUT";
	public static final String CG_AM_PM_FIN_COLKEY = "CG_AM_PM_FIN";
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String DATE_ARRETE_COLKEY = "D_ARRETE_CG_GARDE";
	public static final String DATE_DEBUT_COLKEY = "D_DEB_CG_GARDE";
	public static final String DATE_FIN_COLKEY = "D_FIN_CG_GARDE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_JUSTIFICATIF_COLKEY = "D_JUSTIFICATIF";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NO_ARRETE_COLKEY = "NO_ARRETE_CG_GARDE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String CONGE_ORDRE_COLKEY = "NO_SEQ_ARRETE";
	public static final String NO_ENFANT_COLKEY = "NO_ENFANT";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String ABS_NUMERO_COLKEY = "ABS_NUMERO";


	// Relationships
	public static final String ABSENCE_KEY = "absence";
	public static final String ENFANT_KEY = "enfant";
	public static final String INDIVIDU_KEY = "individu";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cgAmPmDebut() {
    return (String) storedValueForKey(CG_AM_PM_DEBUT_KEY);
  }

  public void setCgAmPmDebut(String value) {
    takeStoredValueForKey(value, CG_AM_PM_DEBUT_KEY);
  }

  public String cgAmPmFin() {
    return (String) storedValueForKey(CG_AM_PM_FIN_KEY);
  }

  public void setCgAmPmFin(String value) {
    takeStoredValueForKey(value, CG_AM_PM_FIN_KEY);
  }

  public String commentaire() {
    return (String) storedValueForKey(COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    takeStoredValueForKey(value, COMMENTAIRE_KEY);
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey(DATE_ARRETE_KEY);
  }

  public void setDateArrete(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ARRETE_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dJustificatif() {
    return (NSTimestamp) storedValueForKey(D_JUSTIFICATIF_KEY);
  }

  public void setDJustificatif(NSTimestamp value) {
    takeStoredValueForKey(value, D_JUSTIFICATIF_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String noArrete() {
    return (String) storedValueForKey(NO_ARRETE_KEY);
  }

  public void setNoArrete(String value) {
    takeStoredValueForKey(value, NO_ARRETE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOAbsences absence() {
    return (org.cocktail.mangue.modele.mangue.individu.EOAbsences)storedValueForKey(ABSENCE_KEY);
  }

  public void setAbsenceRelationship(org.cocktail.mangue.modele.mangue.individu.EOAbsences value) {
    if (value == null) {
    	org.cocktail.mangue.modele.mangue.individu.EOAbsences oldValue = absence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ABSENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ABSENCE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOEnfant enfant() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOEnfant)storedValueForKey(ENFANT_KEY);
  }

  public void setEnfantRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOEnfant value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOEnfant oldValue = enfant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENFANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENFANT_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  

/**
 * Créer une instance de EOCongeGardeEnfant avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCongeGardeEnfant createEOCongeGardeEnfant(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temValide
			) {
    EOCongeGardeEnfant eo = (EOCongeGardeEnfant) createAndInsertInstance(editingContext, _EOCongeGardeEnfant.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOCongeGardeEnfant localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCongeGardeEnfant)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCongeGardeEnfant creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCongeGardeEnfant creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCongeGardeEnfant object = (EOCongeGardeEnfant)createAndInsertInstance(editingContext, _EOCongeGardeEnfant.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCongeGardeEnfant localInstanceIn(EOEditingContext editingContext, EOCongeGardeEnfant eo) {
    EOCongeGardeEnfant localInstance = (eo == null) ? null : (EOCongeGardeEnfant)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCongeGardeEnfant#localInstanceIn a la place.
   */
	public static EOCongeGardeEnfant localInstanceOf(EOEditingContext editingContext, EOCongeGardeEnfant eo) {
		return EOCongeGardeEnfant.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCongeGardeEnfant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCongeGardeEnfant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCongeGardeEnfant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCongeGardeEnfant)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCongeGardeEnfant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCongeGardeEnfant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCongeGardeEnfant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCongeGardeEnfant)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCongeGardeEnfant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCongeGardeEnfant eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCongeGardeEnfant ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCongeGardeEnfant fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

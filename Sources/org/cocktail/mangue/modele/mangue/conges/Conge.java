/*
 * Created on 25 janv. 2006
 *
 * Durée avec une relation sur la classe EOAbscences
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

/**
 *   Classe abstraite pour modéliser les congees legaux. Les conges ont une relation sur la classe EOAbscences 
 *   
 */
public abstract class Conge extends Duree {

	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String TEM_VALIDE_KEY = "temValide";

	public static final String ABSENCE_KEY = "absence";

	public Conge() {
		super();
	}

	public String commentaire() {
		return (String)storedValueForKey(COMMENTAIRE_KEY);
	}
	public void setCommentaire(String value) {
		takeStoredValueForKey(value, COMMENTAIRE_KEY);
	}
	public String temValide() {
		return (String)storedValueForKey(TEM_VALIDE_KEY);
	}
	public void setTemValide(String value) {
		takeStoredValueForKey(value, TEM_VALIDE_KEY);
	}	

	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean value) {
		setTemValide((value)?CocktailConstantes.VRAI:CocktailConstantes.FAUX);
	}

	public org.cocktail.mangue.modele.mangue.individu.EOAbsences absence() {
		return (org.cocktail.mangue.modele.mangue.individu.EOAbsences)storedValueForKey(ABSENCE_KEY);
	}

	public void setAbsenceRelationship(org.cocktail.mangue.modele.mangue.individu.EOAbsences value) {
		if (value == null) {
			org.cocktail.mangue.modele.mangue.individu.EOAbsences oldValue = absence();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ABSENCE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, ABSENCE_KEY);
		}
	}


	public boolean supportePlusieursTypesEvenement() {
		return false;
	}

	// méthodes ajoutées
	protected void init() {
		setEstValide(true);
	}

	public void supprimerRelations() {
		super.supprimerRelations();
		setAbsenceRelationship(null);
	}

	/**
	 * 
	 */
	public void validateForSave() {

		super.validateForSave();
		// Le conge doit reposer soit sur un contrat, soit sur une position d'activite ou de detachement avec gestion de la carriere d'accueil dans l'établissement
		boolean estEnActivite = (EOChangementPosition.individuEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin()));

		NSArray<EOPasse> passesEAS = EOPasse.rechercherPassesEASPourPeriode(editingContext(), individu(), dateDebut(), dateFin());

		if (!estEnActivite && passesEAS.size() == 0 && EOContrat.aContratEnCoursSurPeriode(editingContext(),individu(),dateDebut(),dateFin()) == false) {
			throw new NSValidation.ValidationException("Le congé doit être en activité dans l'établissement sur cette période (Contrat, carrière ou passé type EAS)");
		}
		if (commentaire() != null && commentaire().length() > 2000) {
			throw new NSValidation.ValidationException("Le commentaire ne peut dépasser 2000 caractères");
		}
	}


	/**
	 * 
	 * @param editingContext
	 * @param absence
	 * @return
	 */
	public static Conge findForAbsence(EOEditingContext edc, String nomEntite, EOAbsences absence) {

		try {
			EOFetchSpecification fs = new EOFetchSpecification(nomEntite, getQualifierAbsence(absence), null);		
			return (Conge)edc.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param ec
	 * @param nomEntite
	 * @param absence
	 * @return
	 */
	public static Conge rechercherCongeAvecAbsence(EOEditingContext ec, String nomEntite, EOAbsences absence) {

		try {

			EOFetchSpecification fs = new EOFetchSpecification(nomEntite, getQualifierAbsence(absence), null);		
			NSArray<Conge> results = ec.objectsWithFetchSpecification(fs);

			if (results.size() > 0) {
				return results.get(0);
			} else {

				NSMutableArray qualifiers = new NSMutableArray();

				qualifiers.addObject(getQualifierIndividu(absence.toIndividu()));
				qualifiers.addObject(getQualifierValide(true));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_DEBUT_KEY + "=%@", new NSArray(absence.dateDebut())));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FIN_KEY + "=%@", new NSArray(absence.dateFin())));

				fs = new EOFetchSpecification(nomEntite,new EOAndQualifier(qualifiers),null);
				return (Conge)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			}
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierIndividu(EOIndividu individu) {
		return  EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray<EOIndividu>(individu));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierValide(boolean value) {
		return  EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray<String>((value)?"O":"N"));
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierAbsence(EOAbsences absence) {
		return  EOQualifier.qualifierWithQualifierFormat(ABSENCE_KEY + "=%@", new NSArray<EOAbsences>(absence));
	}

}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.mangue.conges;

import java.util.Enumeration;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeCgMatern;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EODeclarationMaternite extends _EODeclarationMaternite {

    public EODeclarationMaternite() {
        super();
    }

    
	public static EODeclarationMaternite creer(EOEditingContext ec, EOIndividu individu) {

		EODeclarationMaternite newObject = (EODeclarationMaternite) createAndInsertInstance(ec, EODeclarationMaternite.ENTITY_NAME);    

		newObject.setDCreation(new NSTimestamp());
		newObject.initAvecIndividu(individu);
		
		return newObject;
	}

	
   	public void initAvecIndividu(EOIndividu individu) {
		setIndividuRelationship(individu);
		setNbEnfantsDecl(new Integer(0));
		setTemGrossesseGemellaire(CocktailConstantes.FAUX);
		setTemGrossesseTriple(CocktailConstantes.FAUX);
		setTemAnnule(CocktailConstantes.FAUX);
		setTemAnnuleBis(CocktailConstantes.FAUX);
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(individu(),INDIVIDU_KEY);
	}
	public String dateConstatFormatee() {
		return SuperFinder.dateFormatee(this,D_CONSTAT_MATERN_KEY);
	}
	public void setDateConstatFormatee(String uneDate) {
		if (uneDate == null) {
			setDConstatMatern(null);
		} else {
			SuperFinder.setDateFormatee(this,D_CONSTAT_MATERN_KEY,uneDate);
		}
	}
	public boolean estGrossesseGemellaire() {
		return temGrossesseGemellaire() != null && temGrossesseGemellaire().equals(CocktailConstantes.VRAI);
	}
	public void setEstGrossesseGemellaire(boolean aBool) {
		if (aBool) {
			setTemGrossesseGemellaire(CocktailConstantes.VRAI);
		} else {
			setTemGrossesseGemellaire(CocktailConstantes.FAUX);	
		}
	}
	public boolean estGrossesseTriple() {
		return temGrossesseTriple() != null && temGrossesseTriple().equals(CocktailConstantes.VRAI);
	}
	public void setEstGrossesseTriple(boolean aBool) {
		if (aBool) {
			setTemGrossesseTriple(CocktailConstantes.VRAI);
		} else {
			setTemGrossesseTriple(CocktailConstantes.FAUX);	
		}
	}
	public boolean estGrossesseInterrompue() {
		return temAnnule() != null && temAnnule().equals(CocktailConstantes.VRAI);
	}
	public void setEstGrossesseInterrompue(boolean aBool) {
		if (aBool) {
			setTemAnnule(CocktailConstantes.VRAI);
		} else {
			setTemAnnule(CocktailConstantes.FAUX);
		}
	}
	public boolean estDeclarationAnnulee() {
		return temAnnuleBis() != null && temAnnuleBis().equals(CocktailConstantes.VRAI);
	}
	public void setEstDeclarationAnnulee(boolean aBool) {
		if (aBool) {
			setTemAnnuleBis(CocktailConstantes.VRAI);
		} else {
			setTemAnnuleBis(CocktailConstantes.FAUX);
		}
	}
	public String dateAccouchementFormatee() {
		return SuperFinder.dateFormatee(this,D_ACCOUCHEMENT_KEY);
	}
	public void setDateAccouchementFormatee(String uneDate) {
		if (uneDate == null) {
			setDAccouchement(null);
		} else {
			SuperFinder.setDateFormatee(this,D_ACCOUCHEMENT_KEY,uneDate);
		}
	}
	public String dateNaissPrevFormatee() {
		return SuperFinder.dateFormatee(this,D_NAIS_PREV_KEY);
	}
	public void setDateNaissPrevFormatee(String uneDate) {
		if (uneDate == null) {
			setDNaisPrev(null);
		} else {
			SuperFinder.setDateFormatee(this,D_NAIS_PREV_KEY,uneDate);
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (individu().estHomme()) {
			if (dAccouchement() == null) {
				throw new NSValidation.ValidationException("Pour un homme, vous devez fournir la date d'accouchement");
			}
			return;
		}
		if (dConstatMatern() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date de constatation");
		}
		if (dNaisPrev() != null && DateCtrl.isBefore(dNaisPrev(),dConstatMatern())) {
			throw new NSValidation.ValidationException("La date de naissance ne peut être antérieure à la date de constatation");
		}

		if (dAccouchement() != null && DateCtrl.isBefore(dAccouchement(),dConstatMatern())) {
			throw new NSValidation.ValidationException("La date d'accouchement ne peut être antérieure à la date de constatation");
		}
		
		setDModification(new NSTimestamp());
	}
	/** retourne le conge valide qui comporte la periode prenatale pour une femme ou qui demarre le
	 * jour de la date d'accouchement pour un homme
	 */
	public EOCongeMaternite congePrincipal() {
		for (java.util.Enumeration<EOCongeMaternite> e = congesMaternite().objectEnumerator();e.hasMoreElements();) {
			EOCongeMaternite conge = e.nextElement();
			// prendre en compte les conges de maternite qui ne sont pas annulés par un arrêté d'annulation ou remplacés
			// sinon c'est celui qui a la periode prenatale
			if (conge.estTypeMaternite() && conge.estAnnule() == false && conge.dateDebut() != null) {
				if ((individu().estHomme() && dAccouchement() != null && DateCtrl.isSameDay(conge.dateDebut(),dAccouchement())) ||
						(!individu().estHomme() && dNaisPrev() != null && DateCtrl.isBefore(conge.dateDebut(),dNaisPrev()))) {
					return conge;
				}
			}
		}
		return null;
	}
	/** retourne les conges  qui n'ont pas ete annules */
	public NSArray congesValides() {
		NSMutableArray result = new NSMutableArray();
		for (Enumeration<EOCongeMaternite> e = congesMaternite().objectEnumerator();e.hasMoreElements();) {
			EOCongeMaternite conge = e.nextElement();
			// prendre en compte les conges qui ne sont pas annulés par un arrêté d'annulation
			if (conge.toTypeCgMatern() != null && !conge.estAnnule()) {
				result.addObject(conge);
			}
		}
		return result;
	}
	/** retourne les conges de type Maternite qui n'ont pas ete annues ordonnes par date de debut decroissante */
	public NSArray congesValidesDeTypeMaternite() {
		NSMutableArray result = new NSMutableArray();
		for (Enumeration<EOCongeMaternite> e = congesMaternite().objectEnumerator();e.hasMoreElements();) {
			EOCongeMaternite conge = e.nextElement();
			// prendre en compte les conges de maternite qui ne sont pas annules par un arrete d'annulation
			if (conge.estTypeMaternite() && !conge.estAnnule()) {
				result.addObject(conge);
			}
		}
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(result,new NSArray(EOSortOrdering.sortOrderingWithKey(EOCongeMaternite.DATE_DEBUT_KEY,EOSortOrdering.CompareDescending)));
	}
	/** retourne les conges rattaches a cette declaration qui sont de meme type */
	public NSArray congesValidesDeType(EOTypeCgMatern typeConge) {
		NSMutableArray result = new NSMutableArray();
		
		for (Enumeration<EOCongeMaternite> e = congesMaternite().objectEnumerator();e.hasMoreElements();) {
			EOCongeMaternite conge = e.nextElement();
			// prendre en compte les conges de maternite qui ne sont pas annules par un arrete d'annulation
			if (conge.toTypeCgMatern() == typeConge && !conge.estAnnule()) {
				result.addObject(conge);
			}
		}
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(result,new NSArray(EOSortOrdering.sortOrderingWithKey(EOCongeMaternite.DATE_DEBUT_KEY,EOSortOrdering.CompareDescending)));
	}
	public boolean estCongeFractionne() {
		return congesValidesDeTypeMaternite().count() > 1;
	}

	/** recherche les declarations de maternite valides pour un individu triee par ordre de creation decroissant */
	public static NSArray rechercherDeclarationsValidesPourIndividu(EOEditingContext editingContext, EOIndividu individu) {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + " =%@", new NSArray(individu)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_ANNULE_BIS_KEY + " =%@", new NSArray(CocktailConstantes.FAUX)));

		NSArray mySort = new NSArray(new EOSortOrdering(D_MAT_ORDRE_KEY,EOSortOrdering.CompareDescending));
		return fetchAll(editingContext, new EOAndQualifier(qualifiers), mySort);

	}

    
    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

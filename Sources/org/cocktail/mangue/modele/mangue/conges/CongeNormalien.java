/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Mod&eacute;lise les cong&eacute;s sp&eacute;cifiques aux normaliens */
public abstract class CongeNormalien extends CongeAvecArrete {
	public CongeNormalien() {
		super();
	}
	public String motif() {
		return (String)storedValueForKey("motif");
	}

	public void setMotif(String value) {
		takeStoredValueForKey(value, "motif");
	}

	public NSTimestamp dateDecision() {
		return (NSTimestamp)storedValueForKey("dateDecision");
	}

	public void setDateDecision(NSTimestamp value) {
		takeStoredValueForKey(value, "dateDecision");
	}
	public String dateDecisionFormatee() {
		return SuperFinder.dateFormatee(this,"dateDecision");
	}
	public void setDateDecisionFormatee(String uneDate) {
		if (uneDate == null) {
			setDateDecision(null);
		} else {
			SuperFinder.setDateFormatee(this,"dateDecision",uneDate);
		}
	}
	/** V&eacute;rifie que la dur&eacute;e du cong&eacute; est 12 mois */
	public void validateForSave() {
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date de fin");
		}
		NSTimestamp dFin = DateCtrl.dateAvecAjoutAnnees(dateDebut(), 1);
		if (DateCtrl.isSameDay(DateCtrl.jourPrecedent(dFin), dateFin()) == false) {
			throw new NSValidation.ValidationException("Ce congé dure 12 mois");
		}
		super.validateForSave();

	}

}

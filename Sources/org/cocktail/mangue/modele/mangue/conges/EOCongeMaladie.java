// EOCongeMaladie.java
// Created on Wed Mar 12 15:04:02  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;



import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl.IntRef;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
/** Regles de validation<BR>
 * 1. Le conge de maladie ordinaire ne concerne qu'un agent fonctionnaire titulaire ou stagiaire.<BR>
 * 2. Le conge doit reposer sur une position d'activite ou de detachement avec gestion de la carriere d'accueil dans l'etablissement.<BR>
 * 3. Un conge ordinaire de maladie ne peut etre requalifie qu'en presence d'un conge de longue maladie ou de longue duree<BR>
 * @author christine
 */

public class EOCongeMaladie extends CongeMaladie {
	
	public static final String ENTITY_NAME = "CongeMaladie";
	
	public static int NB_MOIS_CONSECUTIFS_MAX = 12;
	public static int NB_MOIS_CONSECUTIFS_AVEC_AVIS = 6;
	public static int NB_MOIS_CONSECUTIFS_AVEC_INFO = 4;
	private NSMutableArray congesMaladieAssocies;	// Sera évalué pendant la méthode dureeEnMoisCongesMaladieAssocies
	
    public EOCongeMaladie() {
        super();
    }
    public String temAccServ() {
        return (String)storedValueForKey("temAccServ");
    }

    public void setTemAccServ(String value) {
        takeStoredValueForKey(value, "temAccServ");
    }
    public NSTimestamp dComMedCom() {
        return (NSTimestamp)storedValueForKey("dComMedCom");
    }

    public void setDcomMedCom(NSTimestamp value) {
        takeStoredValueForKey(value, "dComMedCom");
    }
    
    // méthodes ajoutées
    public String typeEvenement() {
    		return "MAL";
    }
    /** utilise pour la communication avec GRhum */
    public String parametreOccupation() {
    		return "CL_MO";
    }
    public String dateComMedFormatee() {
		return SuperFinder.dateFormatee(this,"dComMedCom");
	}
	public void setDateComMedFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dComMedCom",uneDate);
	}
	public boolean estAccidentService() {
		return temAccServ() != null && temAccServ().equals(CocktailConstantes.VRAI);
	}
	public void setEstAccidentService(boolean aBool) {
		if (aBool) {
			setTemAccServ(CocktailConstantes.VRAI);
		} else {
			setTemAccServ(CocktailConstantes.FAUX);
		}
	}
	
	public void validateForSave() {
		super.validateForSave();
		if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin())) {
	 		if (EOChangementPosition.fonctionnaireEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin()) == false)
	 		throw new NSValidation.ValidationException("Le congé de maladie ordinaire ne concerne qu'un agent fonctionnaire titulaire ou stagiaire");
	 	} else {
	 		throw new NSValidation.ValidationException("Le congé de maladie ordinaire doit reposer sur une position d'activité ou de détachement avec gestion de la carrière d’accueil dans l'établissement");
	 	}
	}
	/** recherche recursivement tous les conges maladie valides qui sont associes au conge courant 
	 * (tous ceux tels que les dates de debut et de fin soient contigues) et retourne la duree totale du conge en mois
	 */
	public NSDictionary dureeEnMoisCongesMaladieAssocies() {
		
		boolean finished = false;
		NSMutableDictionary dicoRetour = new NSMutableDictionary();
		
		EOCongeMaladie congeCourant = this;
		congesMaladieAssocies = new NSMutableArray();
		while (!finished) {
			NSArray conges = congeCourant.congesMaladieAssocies(true);	// rechercher les congés antérieurs
			if (conges.count() == 0) {
				finished = true;
			} else {
				congesMaladieAssocies.addObjectsFromArray(conges);
				if ((EOCongeMaladie)conges.objectAtIndex(0) == this) {
					// cas où on vient de modifier le congé et que l'ancienne date de fin coïncide avec le jour précédent la date de début
					finished = true;
				} else {
					// il ne peut y en avoir qu'un, pas de superposition des congés
					congeCourant = (EOCongeMaladie)conges.objectAtIndex(0);
				}
			}
		}
		NSTimestamp dateDebut = congeCourant.dateDebut();
		finished = false;
		congeCourant = this;
		while (!finished) {
			NSArray conges = congeCourant.congesMaladieAssocies(false);	// rechercher les congés postérieurs
			congesMaladieAssocies.addObjectsFromArray(conges);
			if (conges.count() == 0) {
				finished = true;
			} else {	
				if ((EOCongeMaladie)conges.objectAtIndex(0) == this) {
					// cas où on vient de modifier le congé et que l'ancienne date de début coïncide avec le jour suivant la date de fin
					finished = true;
				} else {
					// il ne peut y en avoir qu'un, pas de superposition des congés
					congeCourant = (EOCongeMaladie)conges.objectAtIndex(0);
				}
			}
		}
		NSTimestamp dateFin = congeCourant.dateFin();
		IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
		DateCtrl.joursMoisAnneesEntre(dateDebut,dateFin,anneeRef,moisRef,jourRef,true);	// inclure les bornes
		
		int nbMois = (12 * anneeRef.value) + moisRef.value;
		int nbJours = jourRef.value;
		
		dicoRetour.setObjectForKey(new Integer(nbMois), "nbMois");
		dicoRetour.setObjectForKey(new Integer(nbJours), "nbJours");
		
		return dicoRetour.immutableClone();
	}
	/** Verifie que pour un cong&eacute; maladie de fonctionnaire la date d'avis de la commission est fournie */
	public boolean verifierAvisComitePourDelaiSuperieur6Mois() {
		if (congesMaladieAssocies == null || congesMaladieAssocies.count() == 0) {
			return false;
		} else {
			for (java.util.Enumeration<CongeMaladie> e = congesMaladieAssocies.objectEnumerator();e.hasMoreElements();) {
				CongeMaladie conge = e.nextElement();
				if (conge instanceof EOCongeMaladie == false) {
					return true;
				} else {
					if (((EOCongeMaladie)conge).dComMedCom() != null) {	// Si on en trouve une, c'est OK
						return true;
					}
				}
			}
			return false;
		}
	}
	// méthodes protégées
	protected void init() {
		super.init();
		setTemAccServ(CocktailConstantes.FAUX);
	}
	// méthodes privées
	private NSArray congesMaladieAssocies(boolean congesAnterieurs) {
		NSMutableArray args = new NSMutableArray(individu());
		EOQualifier qual;
		// ne prendre en compte que les congés non annulés
		if (congesAnterieurs) {
			args.addObject(DateCtrl.jourPrecedent(dateDebut()));
			qual = EOQualifier.qualifierWithQualifierFormat("temValide = 'O' AND individu = %@ AND dateFin = %@ AND noArreteAnnulation = nil AND dAnnulation = nil AND congeDeRemplacement = nil",args);
		} else {
			args.addObject(DateCtrl.jourSuivant(dateFin()));
			qual = EOQualifier.qualifierWithQualifierFormat("temValide = 'O' AND individu = %@ AND dateDebut = %@ AND noArreteAnnulation = nil AND dAnnulation = nil AND congeDeRemplacement = nil",args);
		}
		EOFetchSpecification fs = new EOFetchSpecification("CongeMaladie", qual,null);
		return editingContext().objectsWithFetchSpecification(fs);
	}
}

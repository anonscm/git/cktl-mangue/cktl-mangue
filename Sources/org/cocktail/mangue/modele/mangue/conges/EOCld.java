//EOCld.java
//Created on Fri Sep 05 09:16:31  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;


import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation :<BR>
 * La date debut et la date de fin doivent etre fournies<BR>
 * Verification de dates :<BR>
 * Un Cld dure au minimum 3 mois et au maximum 12 mois<BR>
 * Verifie que l'agent est titulaire <BR>
 * Ferme les occupations<BR>
 * */

public class EOCld extends _EOCld {

	public EOCld() {
		super();
	}

	public static EOCld creer(EOEditingContext ec, EOAbsences absence) {

		EOCld newObject = (EOCld) createAndInsertInstance(ec, EOCld.ENTITY_NAME);    

		newObject.setAbsenceRelationship(absence);		
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());
		newObject.init();

		return newObject;
	}


	public static void requalifier(EOEditingContext ec, NSArray absences, NSTimestamp dateDebut, NSTimestamp dateFin, NSTimestamp dateComite) throws Exception {

		try {

			EOAbsences premiereAbsence = (EOAbsences)absences.objectAtIndex(0);

			int indexAbsences = 0;
			EOAbsences absencePrecedente = null;		
			for (java.util.Enumeration<EOAbsences> e = absences.objectEnumerator();e.hasMoreElements();) {

				EOAbsences absence  = e.nextElement();				
				if (!absence.toTypeAbsence().estCongeLongueMaladie() && !absence.toTypeAbsence().estCongeMaladie())
					throw new ValidationException("Un des congés sélectionnés n'est pas un congé maladie ou longue maladie !");

				if (absences.count() > 1) {
					if (absencePrecedente != null && DateCtrl.nbJoursEntre(absencePrecedente.dateFin(), absence.dateDebut(), false) != 1)
						throw new ValidationException("Les congés maladie à requalifier ne sont pas continus !");
				}

				absencePrecedente = absence;

				// On traite la premiere absence selectionnee
				if (indexAbsences == 0) {

					// Si la date de debut est superieure à la date de debut de la premiere absence, 
					// on conserve la premiere absence en modifiant la date de fin.
					if (DateCtrl.isAfter(dateDebut, absence.dateDebut())) {

						absence.setDateFin(DateCtrl.jourPrecedent(dateDebut));
						if (absence.toTypeAbsence().estCongeLongueMaladie()) {
							EOClm congeMaladie = (EOClm)rechercherCongeAvecAbsence(ec, EOClm.ENTITY_NAME, absence);
							if (congeMaladie != null) {
								congeMaladie.setDateFin(DateCtrl.jourPrecedent(dateDebut));
							}
						}
						else {
							EOCongeMaladie congeMaladie = (EOCongeMaladie)rechercherCongeAvecAbsence(ec, "CongeMaladie", absence);
							if (congeMaladie != null) {
								congeMaladie.setDateFin(DateCtrl.jourPrecedent(dateDebut));
							}
						}
					}
					else {
						supprimerConge(ec, absence);
					}
				}
				else {
					supprimerConge(ec, absence);
				}

				indexAbsences++;
			}

			// Creation du CLD et de l'Absence
			EOAbsences absence = EOAbsences.creer(ec, premiereAbsence.individu(), EOTypeAbsence.findForCode(ec, EOTypeAbsence.TYPE_CLD));
			absence.setDateDebut(dateDebut);
			absence.setDateFin(dateFin);

			EOCld cld = creer(ec, absence);			
			cld.initAvecDates(dateDebut, dateFin);
			cld.initAvecIndividu(premiereAbsence.individu());
			cld.setDComMedCld(dateComite);
			cld.setTemProlongCld("N");
			cld.setCommentaire("Requalification Congé Longue Maladie");						
		}
		catch (Exception e) {  
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * Suppression d'un conge a partir d'une absence donnee (CLM ou CMO).
	 * 
	 * @param ec
	 * @param absence
	 */
	private static void supprimerConge(EOEditingContext ec, EOAbsences absence) {

		if (absence.toTypeAbsence().estCongeLongueMaladie()) {
			EOClm congeMaladie = (EOClm)rechercherCongeAvecAbsence(ec, EOClm.ENTITY_NAME, absence);
			if (congeMaladie != null) {
				congeMaladie.setTemValide("N");
			}
		}
		else {
			EOCongeMaladie congeMaladie = (EOCongeMaladie)rechercherCongeAvecAbsence(ec, "CongeMaladie", absence);
			if (congeMaladie != null) {
				congeMaladie.setTemValide("N");
			}
		}
		absence.setTemValide("N");

	}

	public String typeEvenement() {
		return "CLD";
	}
	/** utilis&eacute; pour la communication avec GRhum */
	public String parametreOccupation() {
		return "CL_CDLD";
	}
	public String dateComMedFormatee() {
		return SuperFinder.dateFormatee(this,D_COM_MED_CLD_KEY);
	}
	public void setDateComMedFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,D_COM_MED_CLD_KEY,uneDate);
	}
	public boolean estProlonge() {
		return temProlongCld() != null && temProlongCld().equals(CocktailConstantes.VRAI);
	}
	public void setEstProlonge(boolean aBool) {
		if (aBool) {
			setTemProlongCld(CocktailConstantes.VRAI);
		} else {
			setTemProlongCld(CocktailConstantes.FAUX);
		}
	}
	public void supprimerRelations() {
		NSArray details = details();
		int total = details.count();
		for (int i = 0; i < total; i++) {
			DetailConge detail = (DetailConge)details().objectAtIndex(0);
			detail.supprimerRelations();
			editingContext().deleteObject(detail);
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (estValide()) {
			
			int nbMoisMin = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLD_DUREE_MIN);
			int nbMoisMax = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLD_DUREE_MAX);

			
			if (dateDebut() != null && dateFin() != null && !estProlonge()) {
				NSTimestamp limite = DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutMois(dateDebut(),nbMoisMin));	// DT1636 - pour prendre en compte les bornes, on prend le jour précédent
				if (DateCtrl.isBefore(dateFin(),limite)) {
					throw new NSValidation.ValidationException("Un CLD ( " + dateDebutFormatee() + " - " + dateFinFormatee() + " ) dure au minimum " + nbMoisMin + " mois !");
				}
				limite = DateCtrl.dateAvecAjoutMois(dateDebut(), nbMoisMax);
				if (DateCtrl.isAfter(dateFin(),limite)) {
					throw new NSValidation.ValidationException("Un CLD ( " + dateDebutFormatee() + " - " + dateFinFormatee() + " ) dure au maximum " + nbMoisMax + " mois !");
				}
				// Le congé doit reposer sur une carrière. Les autres contrôles sont effectués par la superclasse
				if (EOCarriere.aCarriereEnCoursSurPeriode(editingContext(),individu(),dateDebut(),dateFin()) == false) {
					throw new NSValidation.ValidationException("Ce type de congé ne s'applique qu'aux titulaires !");
				} 
			}

			if (dComMedCld() == null) {
				throw new NSValidation.ValidationException("La date d'avis du comité médical est obligatoire !");
			} 
		}
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

		super.validateForSave();
	}
	/** retourne les details de traitement pour un cld */
	public NSArray detailsTraitement() {
		return DetailConge.preparerDetailsCld(editingContext(),individu(),dateDebut(),dateFin());
	}
	// méthodes protégées
	protected void init() {

		setTemProlongCld(CocktailConstantes.FAUX);
		setTemConfirme(CocktailConstantes.FAUX);
		setTemValide(CocktailConstantes.VRAI);
		setTemGestEtab(CocktailConstantes.VRAI);
		setTemEnCause(CocktailConstantes.FAUX);
	}

	@Override
	protected EOQualifier qualifierPeriodeRemiseEnCause() {
		// TODO Auto-generated method stub
		return null;
	}

}

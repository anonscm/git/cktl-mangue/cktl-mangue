//EOCongeGardeEnfant.java
//Created on Tue Mar 04 07:32:03  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

/** Conge pour enfant de moins de 16 ans : verifie que l'enfant est age de moins de 16 ans */
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.IEvenementAvecEnfant;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOPeriodeHandicap;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOCongeGardeEnfant extends _EOCongeGardeEnfant implements IEvenementAvecEnfant {
	
	public final static String ANNEE_CIVILE = "C";
	private String typeAnnee;
	private final static int NB_ANNEES_MAX = 16;

	public EOCongeGardeEnfant() {
		super();
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOCongeGardeEnfant creer(EOEditingContext ec, EOIndividu individu) {
		return creer(ec, individu, null);
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOCongeGardeEnfant creer(EOEditingContext edc, EOIndividu individu, EOAbsences absence) {

		EOCongeGardeEnfant newObject = new EOCongeGardeEnfant();    

		newObject.setIndividuRelationship(individu);
        newObject.setAbsenceRelationship(absence);
        newObject.setCgAmPmDebut("am");
        newObject.setCgAmPmFin("pm");
		newObject.setEstValide(true);
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());
		edc.insertObject(newObject);
		return newObject;
	}

	/**
	 * 
	 * @param edc
	 * @param individu
	 * @param annee
	 * @return
	 */
	public static NSArray<EOCongeGardeEnfant> findForAnnee(EOEditingContext edc, EOIndividu individu, Integer annee) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray("O")));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu)));
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, DateCtrl.debutAnnee(annee), DATE_FIN_KEY, DateCtrl.finAnnee(annee)));
		return fetchAll(edc, new EOAndQualifier(qualifiers), null);
	}

	public String typeEvenement() {
		return EOTypeAbsence.TYPE_CONGE_GARDE_ENFANT;
	}
	/** utilise pour la communication avec GRhum */
	public String parametreOccupation() {
		return "CL_SEM";
	}
	
	public String dateJustificatifFormatee() {
		return SuperFinder.dateFormatee(this,D_JUSTIFICATIF_KEY);
	}
	public void setDateJustificatifFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,D_JUSTIFICATIF_KEY,uneDate);
	}
	public String typeAnnee() {
		if (typeAnnee == null) {
			typeAnnee = EOGrhumParametres.getAnneeRefGardeEnfant();
		}
		return typeAnnee;
	}
	
	/** retourne le nombre de jours de garde pendant l'annee de debut */
	public float nbJoursPourAnnee() {
		if (absence() != null) {
			int annee = DateCtrl.getYear(dateDebut());
			NSTimestamp date = dateFin();
			NSTimestamp finAnnee = DateCtrl.finAnnee(dateDebut());
			if (typeAnnee().equals(ANNEE_CIVILE) == false) {
				int mois = DateCtrl.getMonth(dateDebut());
				// Pour les années universitaires, si la date de début est entre septembre et décembre
				// la date de fin d'année est l'année suivante sinon c'est l'année de début
				if (mois >= 8) {
					annee++;
				}
				finAnnee = DateCtrl.stringToDate("31/08/" + annee);
			}

			if (DateCtrl.isBefore(finAnnee,dateFin())) {
				date = finAnnee;
			}

			return Duree.nbJoursEntre(dateDebut(),absence().absAmpmDebut(),date,absence().absAmpmFin());
		} else {
			return new Float(nbJours()).floatValue();
		}
	}
	
	/** retourne le nombre de demi-journées de garde pendant l'annee de debut */
	public String nbDemiJournees() {
		return StringCtrl.replace(String.valueOf(nbJoursPourAnnee() * 2), ".0", "");
	}
	
	/**
	 * Droits en demi journees accordees pour l'annee
	 * @return
	 */
	public String nbDemiJourneesAnnuelles() {
		int annee = DateCtrl.getYear(dateDebut());

		if (typeAnnee().equals(ANNEE_CIVILE) == false) {	
			// L'année universitaire commence au 01/09 de l'année précédente, il faut donc prendre l'année précédente
			// pour les mois antérieurs à Septembre pour trouver les droits de garde
			int month = DateCtrl.getMonth(dateDebut());
			if (month < 8) {
				annee--;
			}
		}
		EODroitsGardeEnfant droit = EODroitsGardeEnfant.rechercherDroitsGardePourIndividuEtAnnee(editingContext(),individu(),new Integer(annee));
		if (droit != null) {
			return droit.nbDemiJournees().toString();
		}
			
		return String.valueOf(0);
	}
		
	/** retourne le nombre de demi-journees deja consomme pour l'annee */
	public String totalGardesPourAnnee() {
		float nbDemiJournees = 0;
		for (EOCongeGardeEnfant conge : (NSArray<EOCongeGardeEnfant>)absencesPourAnnee()) {
			if (conge != this) {
				nbDemiJournees += new Float(conge.nbDemiJournees()).floatValue();
			}
		}
		int result = (int)nbDemiJournees;
		return new Integer(result).toString();
	}
	
	/** retourne le nombre de demi-journees deja consomme jusqu'a ce conge */
	public String totalGardesPourAnneeAnterieuresConge() {
		float nbDemiJournees = 0;
		NSArray<EOCongeGardeEnfant> absences = EOSortOrdering.sortedArrayUsingKeyOrderArray(absencesPourAnneeAnterieuresConge(), SORT_ARRAY_DATE_DEBUT);
		for (EOCongeGardeEnfant conge : absences) {
			nbDemiJournees += new Float(conge.nbDemiJournees()).floatValue();
			if (conge == this) {	// congé courant, on s'arrête
				break;
			}
		}
		int result = (int)nbDemiJournees;
		return new Integer(result).toString();
	}
	
	/** retourne les absences pour conge de garde d'enfant pris dans l'annee */
	public NSArray<EOCongeGardeEnfant> absencesPourAnnee() {
		
		int annee = DateCtrl.getYear(dateDebut());
		String debutAnnee = "01/01/" + annee;
		String finAnnee = "31/12/" + annee;
		if (typeAnnee().equals(ANNEE_CIVILE) == false) {
			int mois = DateCtrl.getMonth(dateDebut());
			if (mois >= 8)	{ // les mois commencent à zéro
				debutAnnee = "01/09/" + (annee);
				finAnnee = "31/08/" + (annee + 1);
			} else {
				debutAnnee = "01/09/" + (annee - 1);
				finAnnee = "31/08/" + annee;
			}
		}
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(getQualifierValide(true));
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,DateCtrl.stringToDate(debutAnnee), DATE_FIN_KEY,DateCtrl.stringToDate(finAnnee)));

		return Duree.rechercherDureesPourIndividuEtQualifier(editingContext(),ENTITY_NAME, individu(), new EOAndQualifier(qualifiers));
	}
	
	/** retourne les absences pour conge de garde d'enfant pris dans l'annee */
	public NSArray<EOCongeGardeEnfant> absencesPourAnneeAnterieuresConge() {
		int annee = DateCtrl.getYear(dateDebut());
		String debutAnnee = "01/01/" + annee;
		if (typeAnnee().equals(ANNEE_CIVILE) == false) {
			int mois = DateCtrl.getMonth(dateDebut());
			if (mois >= 8)	{ // les mois commencent à zéro
				debutAnnee = "01/09/" + annee;
			} else {
				debutAnnee = "01/09/" + (annee - 1);
			}
		}
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(getQualifierValide(true));
		qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY,DateCtrl.stringToDate(debutAnnee), DATE_FIN_KEY,dateFin()));

		return Duree.rechercherDureesPourIndividuEtQualifier(editingContext(),ENTITY_NAME,individu(), new EOAndQualifier(qualifiers));
	}
	
	/** retourne le ampmDebut de l'absence */
	public String ampmDebut() {
		return (absence() != null)?absence().absAmpmDebut() : "am";
	}
	/** retourne le ampmFin de l'absence */
	public String ampmFin() {
		return (absence() != null)?absence().absAmpmFin() : "pm";
	}
	
	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (temValide().equals(CocktailConstantes.VRAI)) {

			super.validateForSave();

			// 09/06/2010 - il faut dorénavant fournir l'enfant
			if (enfant() == null)
				throw new NSValidation.ValidationException("Vous devez signaler pour quel enfant vous demandez ce congé !");


			// On vérifie que l'enfant a moins de 16 ans
			if (enfant().dSeizeAns() != null && DateCtrl.isBefore(dateDebut(), enfant().dSeizeAns()))
				return;

			// 
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ENFANT_KEY + "=%@", new NSArray(enfant())));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateDebut <= %@", new NSArray(dateDebut())));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dateFin = nil or dateFin >= %@", new NSArray(dateDebut())));

			NSArray handicaps = EOPeriodeHandicap.rechercherDureePourEntiteAvecCriteres(
					editingContext(), 
					EOPeriodeHandicap.ENTITY_NAME, 
					new EOAndQualifier(qualifiers));

			NSTimestamp dSeizeAns = DateCtrl.dateAvecAjoutAnnees(enfant().dNaissance(), NB_ANNEES_MAX);
			if (handicaps.count() > 0 || DateCtrl.isBefore(dateDebut(), dSeizeAns)) {
				return;
			} else {
				throw new NSValidation.ValidationException("Pour bénéficier d'un congé de garde pour enfant malade, il faut au moins avoir un enfant de moins de "+ NB_ANNEES_MAX + " ans");
			}
		}
	}
	// méthodes protégées
	protected void init() {
		// pas d'initialisation spécifique
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.client.GestionnaireEvenementIndividu;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/** Le conge de solidarite est commun aux fonctionnaires et contractuels.<BR>
 *  Le conge de solidarite peut etre renouvele une fois sur une periode de 3 mois.<BR>
 *  Regles de validation : <BR>
 *  Un conge non fractionne et non a temps partiel dure au plus 3 mois (90 jours en duree comptable). Pas de controle des durees sur les conges fractionnes<BR>
 *  La date de certificat medical, la date de debut et de fin sont obligatoires<BR>
 *  La date de fin anticipee est posterieure a la date de debut et antrieure ou egale a la date de fin<BR>
 */
// Un congé peut être renouvelé. Dans ce cas, le congé de renouvellement a temRenouvele à 'O' et le congé pour lequel on a fait
// le renouvellement a congeRenouvele avec la valeur du congé de renouvellement
public class EOCongeSolidariteFamiliale extends _EOCongeSolidariteFamiliale {
	private static Number limiteMaxMois;
	private static int LIMITE_MAX_MOIS = 3;



	public String typeEvenement() {
		return "CSF";
	}
	/** Retourne true si il s'agit d'un conge de renouvellement */
	public boolean estRenouvele() {
		return temRenouvele() != null && temRenouvele().equals(CocktailConstantes.VRAI);
	}
	public void setEstRenouvele(boolean aBool) {
		if (aBool) {
			setTemRenouvele(CocktailConstantes.VRAI);
		} else {
			setTemRenouvele(CocktailConstantes.FAUX);
		}
	}
	public boolean estSigne() {
		return temConfirme() != null && temConfirme().equals(CocktailConstantes.VRAI);
	}
	public void setEstSigne(boolean aBool) {
		if (aBool) {
			setTemConfirme(CocktailConstantes.VRAI);
		} else {
			setTemConfirme(CocktailConstantes.FAUX);
		}
	}
	/** retourne true si l'arrete est gere par l'etablissement */
	public boolean estGereParEtablissement() {
		return temGestEtab() != null && temGestEtab().equals(CocktailConstantes.VRAI);
	}
	public String dateCertificatFormatee() {
		return SuperFinder.dateFormatee(this,"dCertificatMedical");
	}
	public void setDateCertificatFormatee(String uneDate) {
		if (uneDate == null) {
			setDCertificatMedical(null);
		} else {
			SuperFinder.setDateFormatee(this,"dCertificatMedical",uneDate);
		}
	}
	public String dateFinAnticipeeFormatee() {
		return SuperFinder.dateFormatee(this,"dFinAnticipee");
	}
	public void setDateFinAnticipeeFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dFinAnticipee",uneDate);
	}
	public String libelleCongePourEdition() {
		if (typeCsf() == null) {
			return "";
		} else {
			return typeCsf().tcsfLibelle().toLowerCase();
		}
	}
	/** Retourne le conge pour lequel le renouvellement a ete demande, null si le conge n'est pas un conge renouvele */
	public EOCongeSolidariteFamiliale congeOrigine() {
		if (estRenouvele() == false) {
			return null;
		} else {
			EOFetchSpecification fs = new EOFetchSpecification("CongeSolidariteFamiliale",EOQualifier.qualifierWithQualifierFormat("congeRenouvele = %@", new NSArray(this)),null);
			try {
				return (EOCongeSolidariteFamiliale)editingContext().objectsWithFetchSpecification(fs).objectAtIndex(0);
			} catch (Exception e) {
				// Ne devrait pas se produire
				return null;
			}
		}
	}
	public void init() {
		super.init();
		setEstRenouvele(false);
		setEstSigne(false);
		setTemGestEtab(CocktailConstantes.VRAI);
	}
	public void initAvecConge(EOCongeSolidariteFamiliale congeSolidarite) {
		super.init();
		addObjectToBothSidesOfRelationshipWithKey(congeSolidarite.typeCsf(), "typeCsf");
		setDCertificatMedical(congeSolidarite.dCertificatMedical());
		setEstRenouvele(true);
		congeSolidarite.addObjectToBothSidesOfRelationshipWithKey(this, "congeRenouvele");
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		removeObjectFromBothSidesOfRelationshipWithKey(typeCsf(), "typeCsf");
		if (congeRenouvele() != null) {
			EOCongeSolidariteFamiliale congeRenouvele = congeRenouvele();	// pour ne pas qu'après la suppression de la relation, on n'est plus de référence sur l'objet
			// Supprimer ce congé et l'absence associée
			EOAbsences absence = congeRenouvele.absence(); // pour ne pas qu'après la suppression de la relation, on n'est plus de référence sur l'objet
			congeRenouvele.supprimerRelations();
			removeObjectFromBothSidesOfRelationshipWithKey(congeRenouvele, "congeRenouvele");
			editingContext().deleteObject(congeRenouvele);
			try {
				GestionnaireEvenementIndividu.supprimerEvenement(absence,true);
			} catch (Exception exc) {
				exc.printStackTrace();
				LogManager.logException(exc);
			}
		}
	}

	public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("La date de fin est obligatoire");
		}
		if (dCertificatMedical() == null) {
			throw new NSValidation.ValidationException("La date de certificat médical est obligatoire");
		}
		if (dFinAnticipee() != null) {
			if (DateCtrl.isBefore(dFinAnticipee(), dateDebut())) {
				throw new NSValidation.ValidationException("La date de fin anticipée ne peut être antérieure à la date de début");
			} else if (DateCtrl.isAfter(dFinAnticipee(), dateFin())) {
				throw new NSValidation.ValidationException("La date de fin anticipée ne peut être postérieure à la date de fin");
			}
		}
		if (typeCsf() == null) {
			throw new NSValidation.ValidationException("Le type de congé est obligatoire");
		} else if (typeCsf().estCsfNormal()) {
			if (limiteMaxMois == null) {
				try {
					EOGenericRecord param = (EOGenericRecord)SuperFinder.rechercherEntite(editingContext(), "ParamCongesAbsences").objectAtIndex(0);
					limiteMaxMois = (Number)param.valueForKey("csfDureeMaxi");
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (limiteMaxMois == null) {
					limiteMaxMois = new Integer(LIMITE_MAX_MOIS);
				}
			}
			// Vérifier la durée du csf
			int nbJours = DateCtrl.nbJoursEntre(dateDebut(), dateFin(), true, true);
			if (nbJours > limiteMaxMois.intValue() * DateCtrl.NB_JOURS_COMPTABLES_MENSUELS) {
				throw new NSValidation.ValidationException("Un congé non fractionné ne peut durer plus de " + limiteMaxMois + " mois");
			}
		}
	}

}

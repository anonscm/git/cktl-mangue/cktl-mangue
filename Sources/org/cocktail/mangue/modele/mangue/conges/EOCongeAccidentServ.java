// EOCongeAccidentServ.java
// Created on Tue Sep 07 14:02:16  2004 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Les conges d'accident de service supportent l'annulation d'arrete<BR>
 * Regles de validation<BR>
 * 1. Le conge d'accident de service ne concerne qu'un agent fonctionnaire titulaire ou stagiaire.<BR>
 * 2. Le conge doit reposer sur une position d'activite ou de detachement avec gestion de la carriere d'accueil dans l'etablissement.<BR>
 * 3. Il est impossible de produire un projet d'arrete d'accident de service si une date d'avis du comite medical superieur est renseignee<BR>
 *
 * @author christine
 */
public class EOCongeAccidentServ extends _EOCongeAccidentServ {

    public EOCongeAccidentServ() {
        super();
    }

	public static EOCongeAccidentServ creer(EOEditingContext ec, EODeclarationAccident declaration) {

		EOCongeAccidentServ newObject = (EOCongeAccidentServ) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(declaration.individu());
		newObject.setDeclarationRelationship(declaration);
		newObject.setDateAccident(declaration.daccDate());
		newObject.setTemConfirme(CocktailConstantes.FAUX);
		newObject.setTemGestEtab(CocktailConstantes.VRAI);
		newObject.setTemValide(CocktailConstantes.VRAI);
	
		return newObject;
	}

	public static NSArray rechercherTousCongesPourDeclaration(EOEditingContext ec, EODeclarationAccident declaration) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DECLARATION_KEY + "=%@", new NSArray(declaration)));
			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		} catch (Exception e) {
			return null;
		}
	}
	public static NSArray rechercherPourDeclaration(EOEditingContext ec, EODeclarationAccident declaration) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DECLARATION_KEY + "=%@", new NSArray(declaration)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		} catch (Exception e) {
			return null;
		}
	}

    
	public boolean estAnnule() {
		return dAnnulation() != null || noArreteAnnulation() != null || congeDeRemplacement() != null;
	}
   
	public String typeEvenement() {
    		return "ACCSERV";
    }
    /** utilis&eacute; pour la communication avec GRhum */
    public String parametreOccupation() {
    		return "CL_CPADT";
    }
    public String dateComMedFormatee() {
		return SuperFinder.dateFormatee(this,"dateComMed");
	}
	public void setDateComMedFormatee(String uneDate) {
		if (uneDate == null) {
			setDateComMed(null);
		} else {
			SuperFinder.setDateFormatee(this,"dateComMed",uneDate);
		}
	}
	 public String dateComReformeFormatee() {
			return SuperFinder.dateFormatee(this,"dComReforme");
		}
		public void setDateComReformeFormatee(String uneDate) {
			if (uneDate == null) {
				setDateComMed(null);
			} else {
				SuperFinder.setDateFormatee(this,"dComReforme",uneDate);
			}
		}
	 public void validateForSave() {
	 	super.validateForSave();
	 	if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin())) {
	 		if (EOChangementPosition.fonctionnaireEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin()) == false)
	 		throw new NSValidation.ValidationException("Le congé d’accident de service ne concerne qu'un agent fonctionnaire titulaire ou stagiaire");
	 	} else {
	 		throw new NSValidation.ValidationException("Le congé doit reposer sur une position d'activité ou de détachement avec gestion de la carrière d’accueil dans l'établissement");
	 	}
	 	if (dateComMed() != null && (dateArrete() != null || noArrete() != null)) {
	 		throw new NSValidation.ValidationException("Il est impossible de produire un projet d'arrêté d'accident de service si une date d'avis du comité médical supérieur est renseignée");
	 	}
	 	
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	 }
    // méthodes protégées
    protected void init() {
    		super.init();
    		setTemConfirme(CocktailConstantes.FAUX);
    		setTemGestEtab(CocktailConstantes.FAUX);
    }
}

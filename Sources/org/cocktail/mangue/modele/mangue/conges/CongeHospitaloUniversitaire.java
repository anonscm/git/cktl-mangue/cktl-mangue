/*
 * Created on 23 févr. 2006
 *
 * Modélise les congés hospitalo-universitaire
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Mod&eacute;lise les cong&eacute;s hospitalo-universitaire<BR>
 * Regrave;gles de validation:<BR>
 * 1. Ce cong&eacute; ne concerne que les chefs de clinique des universit&eacute;s assistant des h&ocirc;pitaux et les assistants hospitaliers
 * universitaires donc les agents ayant un contrat de type AH CU AO<BR>
 * 2. La totalit&eacute; du cong&eacute; de maladie doit reposer sur un contrat.<BR>
 * 
 * @author christine
 *
 */
public abstract class CongeHospitaloUniversitaire extends CongeAvecDetails implements ICongeAvecRequalification {
	
	public CongeHospitaloUniversitaire() {
		super();
	}
    public NSTimestamp dComMed() {
        return (NSTimestamp)storedValueForKey("dComMed");
    }

    public void setDComMed(NSTimestamp value) {
        takeStoredValueForKey(value, "dComMed");
    }

	public String temRequalifMaladie() {
        return (String)storedValueForKey("temRequalifMaladie");
    }

    public void setTemRequalifMaladie(String value) {
        takeStoredValueForKey(value, "temRequalifMaladie");
    }

    public String temProlongMaladie() {
        return (String)storedValueForKey("temProlongMaladie");
    }

    public void setTemProlongMaladie(String value) {
        takeStoredValueForKey(value, "temProlongMaladie");
    }
    // méthodes ajoutées
    public String dateComMedFormatee() {
		return SuperFinder.dateFormatee(this,"dComMed");
	}
	public void setDateComMedFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dComMed",uneDate);
	}
    
	public boolean estProlonge() {
		return temProlongMaladie() != null && temProlongMaladie().equals(CocktailConstantes.VRAI);
	}
	public void setEstProlonge(boolean aBool) {
		if (aBool) {
			setTemProlongMaladie(CocktailConstantes.VRAI);
		} else {
			setTemProlongMaladie(CocktailConstantes.FAUX);
		}
	}
	public boolean estRequalifie() {
		return temRequalifMaladie() != null && temRequalifMaladie().equals(CocktailConstantes.VRAI);
	}
	public void setEstRequalifie(boolean aBool) {
		if (aBool) {
			setTemRequalifMaladie(CocktailConstantes.VRAI);
		} else {
			setTemRequalifMaladie(CocktailConstantes.FAUX);
		}
	}
	public int dureeMaximumConge() {
		int total = 0;
		for (int i= 0; i < tableMoisRemuneration().length;i++) {
			total += tableMoisRemuneration()[i];
		}
		return total;
	}
	public void validateForSave() {
		super.validateForSave();
		// vérifier que l'agent a bien un contrat de type hospitalo-universitaire
		NSArray contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin(),false);
		if (contrats.count() == 0) {
			throw new NSValidation.ValidationException("Pas de contrat défini à ces dates pour cet agent");
		} else if (individu().aContratHospitalierSurPeriode(dateDebut(),dateFin()) == false) {
				throw new NSValidation.ValidationException("Ce type de congé ne concerne que les personnels hospitalo-universitaires");
		} else if (individu().estContractuelSurPeriodeComplete(dateDebut(),dateFin()) == false) {
			throw new NSValidation.ValidationException("L'agent n'a pas de contrats continus sur cette période");
		} 
	}
	 /** retourne les d&eacute;tails de traitement pour un cong&eacute; hospitalo-universitaire */
    public NSArray details() {
    	return DetailConge.preparerDetailsCongeAl(editingContext(),entityName(),individu(),dateDebut(),dateFin());
    }
	// méthodes à surcharger par les sous-classes
	public abstract boolean dateAvisObligatoire();
	/** retourner 0 si pas de dur&eacute; maximum pour le cong&eacute; */
    public abstract int dureeMaxiConge();
    /** retourner 0 si pas de dur&eacute; l&eacute;gale pour le cong&eacute; */
    public abstract int dureeLegale();
	public abstract int[] tableMoisRemuneration();
	public abstract int[] tableTauxRemuneration();
	// méthodes protégées
	protected void init() {
		super.init();
		setTemRequalifMaladie(CocktailConstantes.FAUX);
		setTemProlongMaladie(CocktailConstantes.FAUX);
	}
	/** qualifier de date &agrave; pour la p&eacute;riode de remise en cause : pour les cong&eacute;s hospitalo-univ c'est le cong&eacute;
	 * commencant le jour suivant: l'agent r&eacute;cup&egrave;re la totalit&eacute; de ses droits &agrave; cong&eacute; d&egrave;s 
	 * le premier jour de reprise de travail*/
	protected  EOQualifier qualifierPeriodeRemiseEnCause() {
		NSArray args = new NSArray(DateCtrl.jourSuivant(dateFin()));
		return EOQualifier.qualifierWithQualifierFormat("dateDebut = %@",args);
	}
}

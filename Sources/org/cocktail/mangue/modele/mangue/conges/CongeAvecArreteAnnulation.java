/*
 * Created on 26 janv. 2006
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe abstraite pour les cong&eacute;s comportant des arr&ecirc;t&eacute;s et des arr&ecirc;t&eacute;s d'annulation
 */
public abstract class CongeAvecArreteAnnulation extends CongeAvecArrete {
	public CongeAvecArreteAnnulation() {
		super();
	}
	// signature
	 public String temConfirme() {
        return (String)storedValueForKey("temConfirme");
    }

    public void setTemConfirme(String value) {
        takeStoredValueForKey(value, "temConfirme");
    }

    public String temGestEtab() {
        return (String)storedValueForKey("temGestEtab");
    }

    public void setTemGestEtab(String value) {
        takeStoredValueForKey(value, "temGestEtab");
    }
   
	// arrêté d'annulation
    public NSTimestamp dAnnulation() {
        return (NSTimestamp)storedValueForKey("dAnnulation");
    }

    public void setDAnnulation(NSTimestamp value) {
        takeStoredValueForKey(value, "dAnnulation");
    }

    public String noArreteAnnulation() {
        return (String)storedValueForKey("noArreteAnnulation");
    }

    public void setNoArreteAnnulation(String value) {
        takeStoredValueForKey(value, "noArreteAnnulation");
    }
    public CongeAvecArrete congeDeRemplacement() {
        return (CongeAvecArrete)storedValueForKey("congeDeRemplacement");
    }

    public void setCongeDeRemplacement(CongeAvecArrete value) {
        takeStoredValueForKey(value, "congeDeRemplacement");
    }
    // méthodes ajoutées
	public boolean estSigne() {
		return temConfirme() != null && temConfirme().equals(CocktailConstantes.VRAI);
	}
	public void setEstSigne(boolean aBool) {
		if (aBool) {
			setTemConfirme(CocktailConstantes.VRAI);
		} else {
			setTemConfirme(CocktailConstantes.FAUX);
		}
	}
	
	/** retourne true si l'arrete est gere par l'etablissement */
	public boolean estGereParEtablissement() {
		return temGestEtab() != null && temGestEtab().equals(CocktailConstantes.VRAI);
	}
	/** retourne true si il y a un arrete d'annulation */
	public boolean estAnnule() {
		return dAnnulation() != null || noArreteAnnulation() != null || congeDeRemplacement() != null;
	}
	 /** retourne true si ce conge annule d'autres conges */
	 public boolean aCongesRemplaces() {
	 	// pour blinder en cas de création (il ne faut pas que l'objet nouvellement créé soit transféré sur le serveur)
	 	if (editingContext().insertedObjects().containsObject(this)) {
	 		return false;
	 	}
   		String nomEntite = this.classDescription().entityName();
   		NSMutableArray args = new NSMutableArray(this);
   		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("congeDeRemplacement = %@",args);
   		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier,null);
   		return editingContext().objectsWithFetchSpecification(fs).count() > 0;
	 }
	/** recherche recursivement tous les conges de meme type valides qui sont associes au conge courant 
	 * (tous ceux tels que les dates de debut et de fin soient contigues) et retourne la duretotale du conge en mois
	 */
	public int dureeEnMoisCongesContinusAssocies() {
		boolean finished = false;
		CongeAvecArreteAnnulation congeCourant = this;
		while (!finished) {
			NSArray conges = congeCourant.congesAssocies(true);	// rechercher les congés antérieurs
			if (conges.count() == 0) {
				finished = true;
			} else {	
				if ((CongeAvecArreteAnnulation)conges.objectAtIndex(0) == this) {
					// cas où on vient de modifier le congé et que l'ancienne date de fin coïncide avec le jour précedent la date de début
					finished = true;
				} else {
					// il ne peut y en avoir qu'un, pas de superposition des congés
					congeCourant = (CongeAvecArreteAnnulation)conges.objectAtIndex(0);
				}
			}
		}
		NSTimestamp dateDebut = congeCourant.dateDebut();
		finished = false;
		congeCourant = this;
		while (!finished) {
			NSArray conges = congeCourant.congesAssocies(false);	// rechercher les congés postérieurs
			if (conges.count() == 0) {
				finished = true;
			} else {		
				// il ne peut y en avoir qu'un, pas de superposition des congés
				if ((Conge)conges.objectAtIndex(0) == this) {
					// cas où on vient de modifier le congé et que l'ancienne date de début coïncide avec le jour suivant la date de fin
					finished = true;
				} else {
					// il ne peut y en avoir qu'un, pas de superposition des congés
					congeCourant = (CongeAvecArreteAnnulation)conges.objectAtIndex(0);
				}
			}
		}
		NSTimestamp dateFin = congeCourant.dateFin();
		return DateCtrl.calculerDureeEnMois(dateDebut,dateFin,true).intValue();
	}
	public NSArray congesAssocies(boolean congesAnterieurs) {
		NSMutableArray args = new NSMutableArray(individu());
		EOQualifier qual;
		// ne prendre en compte que les congés non annulés
		if (congesAnterieurs) {
			args.addObject(DateCtrl.jourPrecedent(dateDebut()));
			qual = EOQualifier.qualifierWithQualifierFormat("temValide = 'O' AND individu = %@ AND dateFin = %@ AND noArreteAnnulation = nil AND dAnnulation = nil",args);
		} else {
			args.addObject(DateCtrl.jourSuivant(dateFin()));
			qual = EOQualifier.qualifierWithQualifierFormat("temValide = 'O' AND individu = %@ AND dateDebut = %@ AND noArreteAnnulation = nil AND dAnnulation = nil",args);
		}
		EOFetchSpecification fs = new EOFetchSpecification(entityName(), qual,null);
		return editingContext().objectsWithFetchSpecification(fs);
	}
	// méthodes protégées
	protected void init() {
		super.init();
		setTemConfirme(CocktailConstantes.FAUX);
		setTemGestEtab(CocktailConstantes.FAUX);
	}
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		super.validateForSave();
		if (noArreteAnnulation() != null && noArreteAnnulation().length() > 20) {
			throw new NSValidation.ValidationException("Un numéro d'arrêté comporte au plus 20 caractères");
		}
	}
	// méthodes statiques
	/** retourne tous les conges valides d'un certain type pour un individu
	 * @param editingContext
	 * @param entite nom de l'entite recherchee
	 * @param individu individu pour lequel on recherche des conges
	 * @return conges trouves tries par ordre de date debut croissant
	 * */
	 public static NSArray rechercherCongesValidesPourIndividu(EOEditingContext editingContext,String entite,EOIndividu individu) {
		return rechercherCongesValidesPourIndividuAnterieurs(editingContext,entite,individu,null);
	 }
	 /** retourne tous les conges valides d'un certain type pour un individu commeneant avant la date fournie
		 * @param editingContext
		 * @param entite nom de l'entite recherchee
		 * @param individu individu pour lequel on recherche des conges
		 * @param dateDebut date debut maximum
		 * @return conges trouves tries par ordre de date d&eacute;but croissant
		 * */
		 public static NSArray rechercherCongesValidesPourIndividuAnterieurs(EOEditingContext editingContext,String entite,EOIndividu individu,NSTimestamp dateDebut) {

			NSMutableArray args = new NSMutableArray(individu);
			String stringQualifier = "temValide = 'O' AND individu = %@ AND noArreteAnnulation = nil AND dAnnulation = nil AND congeDeRemplacement = nil";
			if (dateDebut != null) {
				args.addObject(dateDebut);
				stringQualifier = stringQualifier + " AND dateDebut <= %@";
			}
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(rechercherDureePourEntiteAvecCriteres(editingContext,entite,qualifier), new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));
		 }
	/** retourne tous les conges valides d'un certain type pour une periode 
	 * @param editingContext
	 * @param entite nom de l'entite recherchee
	 * @param individu individu pour lequel on recherche des conges
	 * @param debutPeriode
	 * @param finPeriode
	 * @return conges trouves tries par ordre de date debut croissant
	 * */
	 public static NSArray rechercherCongesValidesPourIndividuEtPeriode(EOEditingContext editingContext,String entite,EOIndividu individu,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray args = new NSMutableArray(individu);
		String stringQualifier = "temValide = 'O' AND individu = %@ AND noArreteAnnulation = nil AND dAnnulation = nil AND congeDeRemplacement = nil";
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		qualifiers.addObject(qualifier);
		qualifier = SuperFinder.qualifierPourPeriode("dateDebut",debutPeriode,"dateFin",finPeriode);
		qualifiers.addObject(qualifier);
		qualifier = new EOAndQualifier(qualifiers);
	
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(rechercherDureePourEntiteAvecCriteres(editingContext,entite,qualifier), new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut",EOSortOrdering.CompareAscending)));
	 }
}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/** R&egrave;gles de validation :<BR>
 * Le type de cong&eacute;, la date d&eacute;but et la date fin doivent &ecir;tre fournis<BR>
 * Verification de dates<BR>
 * <UL>conge de type Etat pathologique de a la grossesse : la date de constat et date de naissance previsionnelle doivent &ecirc;tre fournies</UL>
 * <UL>conge de type Etat pathologique suite a l'accouchement : la date d'accouchement doit etre fournie</UL>
 * <UL>conge de type Maternit&eacute; : la date de naissance pr&eacute;visionnelle doit &ecirc;tre fournie</UL>
1. Le cong&eacute; de maternit&eacute; de type ACCOU et GROSS ne concerne qu'un personnel feminin.<BR>
2. La d&eacute;claration de maternit&eacute; ne concerne qu'un personnel feminin. Sa saisie dans ce cas est obligatoire pour pouvoir saisir des cong&eacute;s.<BR>
3. Le conge de type GROSS Etat pathologique d&ucirc; &agrave; la grossesse est de 2 semaines et peut &ecirc;tre pris entre la date de constatation et la date pr&eacute;sum&eacute;e de l'accouchement. Il peut &ecirc;tre fractionn&eacute;.<BR>
4. Le conge de type ACCOU Etat pathologique suite a l'accouchement est de 4 semaines apres le conge de maternit&eacute;. Il peut &ecirc;tre fractionn&eacute;.<BR>
Les regles ci-apres concernent le conge de maternite de type MATER<BR>
5. Le conge de maternite peut etre fractionne, en cas d'hospitalisation de l'enfant au-del&agrave; de 6 semaines.<BR>
6. Le conge de maternite ne peut &ecirc;tre d'une dur&eacute;e totale inf&eacute;rieure &agrave; 8 semaines avant et apr&egrave;s l'accouchement dont 6 semaines apr&egrave;s l'accouchement.<BR>
7. Pour la naissance du 1er ou du 2eme enfant, le cong&eacute; de maternit&eacute; est de 16 semaines maximum, 6 semaines avant la date pr&eacute;sum&eacute;e de l'accouchement et 10 semaines apr&egrave;s.<BR>
8.Il est possible de reporter une partie de la p&eacute;riode pr&eacute;natale sur la p&eacute;riode postnatale<BR>
9. Pour la naissance du 3eme enfant et plus, le cong&eacute; de maternit&eacute; est de 26 semaines maximum, 8 semaines avant la date pr&eacute;sum&eacute;e de l'accouchement et 18 semaines apr&egrave;s.<BR>
10.Il est possible d'anticiper la p&eacute;riode pr&eacute;natale, la p&eacute;riode postnatale &eacute;tant r&eacute;duite d'autant.<BR>
11. Pour la naissance de jumeaux, le cong&eacute; de maternit&eacute; est de 34 semaines, 12 semaines avant la date pr&eacute;sum&eacute;e de l'accouchement et 22 semaines apr&egrave;s.<BR>
12.Il est possible d'anticiper la p&eacute;riode pr&eacute;natale, la p&eacute;riode postnatale &eacute;tant r&eacute;duite d'autant.<BR>
13. Pour la naissance de tripl&eacute;s ou plus, le cong&eacute; de maternit&eacute; est de 46 semaines maximum, 24 semaines avant la date pr&eacute;sum&eacute;e de l'accouchement et 22 semaines apr&egrave;s.<BR>
14.Il ne devrait pas &ecirc;tre possible de d'anticiper la p&eacute;riode pr&eacute;natale.<BR>
15. En cas d'accouchement apr&egrave;s la date pr&eacute;sum&eacute;e, la p&eacute;riode pr&eacute;natale se trouve prolong&eacute;e et la p&eacute;riode postnatale n'est pas r&eacute;duite pour autant.<BR>
16. En cas d'accouchement pr&eacute;matur&eacute;, la dur&eacute;e totale du cong&eacute; de maternit&eacute; n'est pas modifi&eacute;e.<BR>
17. En cas de grossesse non mene a terme, le conge de maternite est pour la duree du repos observe.<BR>
18. Le conge de maternite pour un agent masculin, en cas de deces de la mere debute apres la date de naissance de l'enfant.<BR>
19.Le cong&eacute; de maternit&eacute; pour un agent masculin est de 10 semaines en cas de de naissance d'un seul enfant et si le p&egrave;re a moins de 2 enfants &agrave; charge, 
 18 semaines si le pere a au moins duex enfants a charge, 22 semaines en cas de naissances multiples.<BR>
20. La case Conge sans traitement ne concerne que les agents non titulaires, les fonctionnaires perçoivent un plein traitement.<BR>
21. Le conge doit reposer soit sur un contrat, soit sur une position d'activit&eacute; ou de d&eacute;tachement avec gestion de la carri&egrave;re d'accueil 
dans l'&eacute;tablissement.<BR>
**/

package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeCgMatern;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOCongeMaternite extends _EOCongeMaternite {

	public static int NB_JOURS_NORMAL_FEMME = 112;
	public static int NB_JOURS_2ENFANTS_OU_PLUS_FEMME = 182;
	public static int NB_JOURS_JUMEAUX_FEMME = 238;
	public static int NB_JOURS_TRIPLES_FEMME = 322;
	public static int NB_JOURS_NORMAL_PRENATAL = 42;
	public static int NB_JOURS_2ENFANTS_OU_PLUS_PRENATAL = 56;
	public static int NB_JOURS_JUMEAUX_PRENATAL = 84;
	public static int NB_JOURS_TRIPLES_PRENATAL = 168;
	public static int NB_JOURS_NORMAL_POSTNATAL = 70;
	public static int NB_JOURS_2ENFANTS_OU_PLUS_POSTNATAL = 126;
	public static int NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL = 154;

    public EOCongeMaternite() {
        super();
    }
    
    
	public static EOCongeMaternite creer(EOEditingContext ec, EODeclarationMaternite declaration, EOTypeCgMatern type) {

		EOCongeMaternite newObject = (EOCongeMaternite) createAndInsertInstance(ec, EOCongeMaternite.ENTITY_NAME);    

		newObject.setDCreation(new NSTimestamp());

		newObject.setDeclarationMaterniteRelationship(declaration);
		newObject.setIndividuRelationship(declaration.individu());
		newObject.setToTypeCgMaternRelationship(type);

		newObject.setTemConfirme("N");
		newObject.setTemGestEtab("N");
		newObject.setTemCgSansTrait("N");
		newObject.setTemValide("O");

		return newObject;
	}

    
    
	public String typeEvenement() {
		return "CMATER";
	}
	/** utilis&eacute; pour la communication avec GRhum */
	public String parametreOccupation() {
		return "CL_CDM";
	}
	/** retourne true si un conge est de type maternite **/
	public boolean estTypeMaternite() {
		return toTypeCgMatern() != null && toTypeCgMatern().estMaternite();
	}
	/** retourne true si un un conge est de type pathologie grossesse */
	public boolean estTypePathologieGrossesse() {
		return toTypeCgMatern() != null && toTypeCgMatern().estPathologieGrossesse();
	}
	/** retourne true si un un conge est de type pathologie accouchement */
	public boolean estTypePathologieAccouchement() {
		return toTypeCgMatern() != null && toTypeCgMatern().estPathologieAccouchement();
	}
	public int nbJoursLegaux() {
		if (!estTypeMaternite()) {
			return toTypeCgMatern().dureeCgMatern().intValue() * 7;
		} else {
			return nbJoursLegauxCongeTypeMaternite();
		}
	}
	/** calcule la duree du conge maternite en semaine avec arrondi inferieur et le retourne sous forme de string */
	public String nbSemainesConge() {
		int nbSemaines = 0;
		if (dateDebut() != null && dateFin() != null) {
			nbSemaines = DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true) / 7;
		}
		if (nbSemaines == 0) {
			return "";
		} else {
			return new Integer(nbSemaines).toString();
		}
	}
	/** calcule la duree totale des conges de meme type */
	public int dureeTotaleCongeMemeType(boolean inclureCongeCourant) {
		int dureeTotale = 0;
		NSArray conges = declarationMaternite().congesValidesDeType(toTypeCgMatern());
		for (java.util.Enumeration<EOCongeMaternite> e = conges.objectEnumerator();e.hasMoreElements();) {
			EOCongeMaternite conge = e.nextElement();
			if (inclureCongeCourant || conge != this) {
				dureeTotale += DateCtrl.nbJoursEntre(conge.dateDebut(), conge.dateFin(), true);
			}
		}
		return dureeTotale;
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		setDeclarationMaterniteRelationship(null);
		setToTypeCgMaternRelationship(null);
	}
	
	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		
		if (temValide().equals(CocktailConstantes.FAUX)) 
			return;
		
		super.validateForSave();	// vérification des dates début, date fin

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
			
		if (individu().estHomme() == false && declarationMaternite() == null) {
			throw new NSValidation.ValidationException("Vous devez saisir la déclaration de maternité");
		}
		if (estSigne() || estAnnule() || declarationMaternite().estDeclarationAnnulee()) {
			// les champs ne peuvent plus être modifiés
			return;
		}
		
		if (declarationMaternite() != null && declarationMaternite().nbEnfantsDecl() == null)
			throw new NSValidation.ValidationException("Vous devez saisir le nombre d'enfants à charge !");
		
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date de fin");
		}
		if (toTypeCgMatern() == null) {
			throw new NSValidation.ValidationException("Vous devez sélectionner le type de congé maternité");
		}

		// vérifier que pour un homme, c'est un congé de type maternité et qu'il débute bien le jour de l'accouchement
		if (individu().estHomme()) {
			if (!estTypeMaternite()) {
				throw new NSValidation.ValidationException("Un homme ne peut prendre qu'un congé maternité de type \"" + toTypeCgMatern().libelleLong() + "\"");
			}
			// le congé de maternité débute le jour de l'accouchement
			if (declarationMaternite().dAccouchement() == null) {
				throw new NSValidation.ValidationException("Pour un homme, vous devez fournir la date d'accouchement");
			}
			if (declarationMaternite().estCongeFractionne() == false && DateCtrl.isSameDay(declarationMaternite().dAccouchement(),dateDebut()) == false) {
				throw new NSValidation.ValidationException("Pour un homme, le congé de maternité débute le jour de l'accouchement");
			}
			verifierDurees();
			return;
		} 
		// cas d'une femme
		if (declarationMaternite().dNaisPrev() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date d'accouchement prévisionnel");
		}
		if (estTypeMaternite()) {
			verifierDurees();
		} else {
			int nbJoursLegaux = dureeLegale();
			if (estTypePathologieGrossesse()) {
				if (DateCtrl.isBefore(dateDebut(),declarationMaternite().dConstatMatern())) {
					throw new NSValidation.ValidationException("Un congé de type \"" + toTypeCgMatern().libelleLong() + "\" ne peut pas commencer avant la date de constatation !!");
				}
				EOCongeMaternite congePrincipal = declarationMaternite().congePrincipal();
				if (congePrincipal == null) {
					throw new NSValidation.ValidationException("Vous devez d'abord définir un congé de type maternité");
				}
				if (DateCtrl.isAfterEq(dateDebut(),congePrincipal.dateDebut())) {
					throw new NSValidation.ValidationException("Un congé de type \"" + toTypeCgMatern().libelleLong() + "\" ne peut pas commencer après le début du congé de maternité !!");
				}
				if (DateCtrl.isAfterEq(dateFin(),congePrincipal.dateDebut())) {
					throw new NSValidation.ValidationException("Un congé de type \"" + toTypeCgMatern().libelleLong() + "\" ne peut pas se terminer après après le début du congé de maternité !!");
				}	
				if (DateCtrl.isAfter(dateFin(),declarationMaternite().dNaisPrev())) {
					throw new NSValidation.ValidationException("Un congé de type \"" + toTypeCgMatern().libelleLong() + "\" ne peut pas se terminer après la date d'accouchement prévisionnel !!");
				}	
			} else if (estTypePathologieAccouchement()) {
				if (declarationMaternite().dAccouchement() == null) {
					throw new NSValidation.ValidationException("Pour congé de type \""  + toTypeCgMatern().libelleLong() + "\", vous devez fournir la date d'accouchement");
				}
				EOCongeMaternite conge = autreCongeDeTypeMaternite();
				if (conge == null) {
					throw new NSValidation.ValidationException("Le congé de type \""  + toTypeCgMatern().libelleLong() + "\" requiert l'existence d'un congé de type Maternité");
				} else if (DateCtrl.isBeforeEq(dateDebut(),conge.dateFin())) {
					throw new NSValidation.ValidationException("Un congé de type \""  + toTypeCgMatern().libelleLong() + "\" ne peut commencer avant la fin d'un congé de type Maternité");
				}
			}
			int nbJours = dureeTotaleCongeMemeType(true);
			if (nbJours > nbJoursLegaux) {
				throw new NSValidation.ValidationException("Le congé de type \""  + toTypeCgMatern().libelleLong() + "\" est de " + nbJoursLegaux + " jours au total");
			}
		}    
		
		setDModification(new NSTimestamp());
		
	}
	public int dureeLegale() {
		if (toTypeCgMatern().dureeCgMatern() != null) {
			return toTypeCgMatern().dureeCgMatern().intValue() * 7;	// la valeur fournie est en semaines
		} else {
			return 0;
		}
	}
	/**	 calcule la duree prenatale ou postnatale de tous les conges maternite 
	 * de type maternite attaches à la déclaration de maternite
	 * @param conges
	 * @param estPrenatale
	 * @return
	 */
	public int calculerDuree(NSArray conges,boolean estPrenatale) {
		if (conges.count() == 1) {
			// un seul congé, c'est forcément celui-ci, se baser sur la date de naissance prévisionnel
			if (estPrenatale) {
				return DateCtrl.nbJoursEntre(dateDebut(),declarationMaternite().dNaisPrev(),true);
			} else {
				return DateCtrl.nbJoursEntre(declarationMaternite().dNaisPrev(),dateFin(),true);
			}
		}
		// plusieurs conges (fractionnement pour enfant malade, la date d'accouchement doit être fournie). Il n'y en a qu'un
		// qui commence avant la date de naissance prévisionnelle
		int nbJours = 0;
		for (java.util.Enumeration<EOCongeMaternite> e = conges.objectEnumerator();e.hasMoreElements();) {
			EOCongeMaternite conge = (EOCongeMaternite)e.nextElement();
			if (estPrenatale) {
				if (DateCtrl.isBefore(conge.dateDebut(),declarationMaternite().dNaisPrev())) {
					return DateCtrl.nbJoursEntre(conge.dateDebut(),declarationMaternite().dNaisPrev(),true);
				}
			} else {
				// congé post natale
				if (DateCtrl.isBefore(conge.dateDebut(),declarationMaternite().dAccouchement())) {
					// congé qui comportait la durée prénatale
					nbJours += DateCtrl.nbJoursEntre(declarationMaternite().dAccouchement(),conge.dateFin(),true);
				} else {
					nbJours += DateCtrl.nbJoursEntre(conge.dateDebut(),conge.dateFin(),true);
				}
			}
		}
		return nbJours;
	}
	// méthodes privées

	public static EOCongeMaternite rechercherCongePourAbsence(EOEditingContext editingContext, EOAbsences absence) {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ABSENCE_KEY+"=%@", new NSArray(absence)));

		return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));

	}

	
	private EOCongeMaternite autreCongeDeTypeMaternite() {
		try {
			return (EOCongeMaternite)declarationMaternite().congesValidesDeTypeMaternite().objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	private void verifierDurees() {
		if (individu().estHomme() == false) {
			if (declarationMaternite().dConstatMatern() != null && DateCtrl.isBefore(dateDebut(),declarationMaternite().dConstatMatern())) {
				throw new NSValidation.ValidationException("Un congé de maternité ne peut pas commencer avant la date de constatation !!");
			}
			if (declarationMaternite().estGrossesseInterrompue()) {
				// on vérifie que la date de début et de fin sont bien antérieures à la date de naissance prévisionnelle si elle est fournie
				if (declarationMaternite().dNaisPrev() != null) {
					if (DateCtrl.isAfter(dateDebut(),declarationMaternite().dNaisPrev())) {
						throw new NSValidation.ValidationException("Une interruption de grossesse ne peut commencer après la date de naissance prévisionnelle");
					}
					if (DateCtrl.isAfter(dateFin(),declarationMaternite().dNaisPrev())) {
						throw new NSValidation.ValidationException("Une interruption de grossesse ne peut se terminer après la date de naissance prévisionnelle");
					}
					// on vérifier qu'il n'y a pas de date d'accouchement
					if (declarationMaternite().dAccouchement() != null) {
						throw new NSValidation.ValidationException("La grossesse est interrompue, il ne peut pas y avoir de date d'accouchement");
					}
				}
				return;
			}
			if (DateCtrl.isBefore(dateFin(),declarationMaternite().dNaisPrev())) {
				throw new NSValidation.ValidationException("Un congé de type \"Maternité\" ne peut pas se terminer avant la date de naissance prévisionnelle");
			}
		}
		if (declarationMaternite().dAccouchement() != null &&DateCtrl.isBefore(dateFin(),declarationMaternite().dAccouchement())) {
			throw new NSValidation.ValidationException("Un congé de type \"Maternité\" ne peut pas se terminer avant la date d'accouchement");
		}
		int nbEnfants = 0;
		if (declarationMaternite().nbEnfantsDecl() != null) {
			nbEnfants = declarationMaternite().nbEnfantsDecl().intValue();
		}
		// le congé de maternité peut être fractionné en cas d'hospitalisation de l'enfant. En ce cas,
		// il faut que la date d'accouchement soit fournie pour pouvoir calculer la durée totale postMaternité
		NSArray congesTypeMaternite = declarationMaternite().congesValidesDeTypeMaternite();
		if (congesTypeMaternite.count() > 1 && declarationMaternite().dAccouchement() == null) {	// il y a plusieurs congés
			throw new NSValidation.ValidationException("Vous devez fournir la date d'accouchement");
		}
		int nbJoursTotal = calculerDureeTotale(congesTypeMaternite);
		int deltaSupplementaire = 0;
		if (declarationMaternite().dNaisPrev() != null &&declarationMaternite().dAccouchement() != null && 
				DateCtrl.isBefore(declarationMaternite().dNaisPrev(),declarationMaternite().dAccouchement())) {
			deltaSupplementaire = DateCtrl.nbJoursEntre(declarationMaternite().dNaisPrev(),declarationMaternite().dAccouchement(),false);
		}
		LogManager.logDetail("CongeMaternite - validateForSave, nb Jours " + nbJoursTotal + " delta " + deltaSupplementaire);
		if (nbEnfants <= 1 
				&& declarationMaternite().estGrossesseGemellaire() == false 
				&& declarationMaternite().estGrossesseTriple() == false
				&& (declarationMaternite().dAccouchement() == null ||
				   !DateCtrl.isBefore(declarationMaternite().dAccouchement(), declarationMaternite().dNaisPrev())) ) {

			if (individu().estHomme()) {
				if (nbJoursTotal != NB_JOURS_NORMAL_POSTNATAL) {
					throw new NSValidation.ValidationException("Pour un homme, le congé paternité doit durer " + NB_JOURS_NORMAL_POSTNATAL + " semaines");
				}
			} else {
				if (nbJoursTotal < 56) {	// minimum 8 semaines
					throw new NSValidation.ValidationException("Le congé de maternité ne peut pas durer moins de 8 semaines !");
				}
				if (nbJoursTotal - deltaSupplementaire > NB_JOURS_NORMAL_FEMME) {
					throw new NSValidation.ValidationException("Le congé de maternité dure 16 semaines maximum !");
				}
			}
		} else {
			int nbJoursPostnatal = calculerDuree(congesTypeMaternite, false);
			int dureeTotale = NB_JOURS_2ENFANTS_OU_PLUS_FEMME; 							// 26 semaines
			int dureePostnatale = EOCongeMaternite.NB_JOURS_2ENFANTS_OU_PLUS_POSTNATAL;	// 18 semaines
			String message = "A partir du 3ème enfant, ";
			if (declarationMaternite().estGrossesseGemellaire()) {
				dureeTotale = NB_JOURS_JUMEAUX_FEMME;										// 34 semaines
				dureePostnatale = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;	// 22 semaines
				message = "Pour des jumeaux, ";
			} else if (declarationMaternite().estGrossesseTriple()) {
				dureeTotale = NB_JOURS_TRIPLES_FEMME;										// 46 semaines
				dureePostnatale = EOCongeMaternite.NB_JOURS_NAISSANCE_MULTIPLE_POSTNATAL;	// 22 semaines
				message = "Pour des triplés ou plus, ";
			} 
			// pour un homme, le congé de maternité a la durée postnatale
			if (individu().estHomme()) {
				if (nbJoursPostnatal != dureePostnatale) {
					throw new NSValidation.ValidationException("Pour un homme,le congé de maternité doit durer au maximum " + (dureePostnatale/7) + " semaines");
				}
			} else {		// pour une femme
				if (nbJoursPostnatal > dureeTotale + deltaSupplementaire) {	
					throw new NSValidation.ValidationException(message + "le congé de maternité doit durer au maximum " + (dureeTotale/7) + " semaines");
				}
			}
		}
	}
	// calcule la durée totale de tous les congés maternité de type maternité attachés à la déclaration de maternité
	private int calculerDureeTotale(NSArray conges) {
		int nbJours = 0;
		for (java.util.Enumeration<EOCongeMaternite> e = conges.objectEnumerator();e.hasMoreElements();) {
			EOCongeMaternite conge = (EOCongeMaternite)e.nextElement();
			nbJours += DateCtrl.nbJoursEntre(conge.dateDebut(),conge.dateFin(),true);
		}
		return nbJours;
	}

	private int nbJoursLegauxCongeTypeMaternite() {
		// A MODIFIER pour retourner un paramètre de types congé maternité
		return 0;
	}

	
    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

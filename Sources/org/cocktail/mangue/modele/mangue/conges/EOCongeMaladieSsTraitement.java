// EOCongeMaladieSsTraitement.java
// Created on Wed Mar 10 08:51:34 Europe/Paris 2010 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation<BR>
 * 1. Le conge de maladie sans traitement concerne les fonctionnaires en activite et les contractuels.<BR>
 * 2. Le conge doit reposer sur une position d'activite ou de detachement avec gestion de la carriere d'accueil dans l'etablissement.<BR>
 * 3. Un conge ordinaire de maladie ne peut etre requalifie qu'en presence d'un conge de longue maladie ou de longue duree<BR>
 * @author christine
 */

public class EOCongeMaladieSsTraitement extends CongeAvecArreteAnnulation implements ICongeAvecRequalification {

    public EOCongeMaladieSsTraitement() {
        super();
    }
    
    public static String ENTITY_NAME = "CongeMaladieSsTrt";

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOCongeMaladieSsTraitement(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
    public String temRequalifMaladie() {
        return (String)storedValueForKey("temRequalifMaladie");
    }

    public void setTemRequalifMaladie(String value) {
        takeStoredValueForKey(value, "temRequalifMaladie");
    }

    public String temProlongMaladie() {
        return (String)storedValueForKey("temProlongMaladie");
    }

    public void setTemProlongMaladie(String value) {
        takeStoredValueForKey(value, "temProlongMaladie");
    }

    public NSTimestamp dArretTravail() {
        return (NSTimestamp)storedValueForKey("dArretTravail");
    }

    public void setDArretTravail(NSTimestamp value) {
        takeStoredValueForKey(value, "dArretTravail");
    }

    public String temAccServ() {
        return (String)storedValueForKey("temAccServ");
    }

    public void setTemAccServ(String value) {
        takeStoredValueForKey(value, "temAccServ");
    }

    public NSTimestamp dComMedCom() {
        return (NSTimestamp)storedValueForKey("dComMedCom");
    }

    public void setDComMedCom(NSTimestamp value) {
        takeStoredValueForKey(value, "dComMedCom");
    }
    // Méthodes ajoutées
    public String typeEvenement() {
		return "MALSSTRT";
	}
    public String dateComMedFormatee() {
		return SuperFinder.dateFormatee(this,"dComMedCom");
	}
	public void setDateComMedFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dComMedCom",uneDate);
	}
	public boolean estAccidentService() {
		return temAccServ() != null && temAccServ().equals(CocktailConstantes.VRAI);
	}
	public void setEstAccidentService(boolean aBool) {
		if (aBool) {
			setTemAccServ(CocktailConstantes.VRAI);
		} else {
			setTemAccServ(CocktailConstantes.FAUX);
		}
	}
	public String dateArretFormatee() {
		return SuperFinder.dateFormatee(this,"dArretTravail");
	}
	public void setDateArretFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dArretTravail",uneDate);
	}

	public boolean estProlonge() {
		return temProlongMaladie() != null && temProlongMaladie().equals(CocktailConstantes.VRAI);
	}
	public void setEstProlonge(boolean aBool) {
		if (aBool) {
			setTemProlongMaladie(CocktailConstantes.VRAI);
		} else {
			setTemProlongMaladie(CocktailConstantes.FAUX);
		}
	}
	public boolean estRequalifie() {
		return temRequalifMaladie() != null && temRequalifMaladie().equals(CocktailConstantes.VRAI);
	}
	public void setEstRequalifie(boolean aBool) {
		if (aBool) {
			setTemRequalifMaladie(CocktailConstantes.VRAI);
		} else {
			setTemRequalifMaladie(CocktailConstantes.FAUX);
		}
	}
	public void validateForSave() {
		super.validateForSave();
		if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin())) {
	 		if (EOChangementPosition.fonctionnaireEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin()) == false)
	 		throw new NSValidation.ValidationException("Le congé de maladie sans traitement ne concerne qu'un agent fonctionnaire titulaire ou stagiaire");
	 	} else {
	 		throw new NSValidation.ValidationException("Le congé de maladie sans traitement doit reposer sur une position d'activité ou de détachement avec gestion de la carrière daccueil dans l'établissement");
	 	}
	}
	// méthodes protégées
	protected void init() {
		super.init();
		setEstAccidentService(false);
		setEstProlonge(false);
		setEstRequalifie(false);
		setEstSigne(false);
	}
	
}

// EOCongeAl4.java
// Created on Wed Feb 22 17:47:56 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

/** Regles de validation :<BR>
 * La date d'avis du comite medical doit etre fournie.<BR>
 * 1. L'agent beneficie d'un conge de 12 mois apres avis du comite medical.<BR>
 * 2. L’agent beneficie  d'une prolongation de conge de six mois apres un nouvel avis du comite medical.<BR>>
 * 3. L'agent beneficie pendant les dix-huit premiers mois du maintien des deux tiers (66%) de sa remuneration.<BR>
 * 4. L'agent beneficie ensuite apres ces dix-huit mois consecutifs d’un conge sans remuneration pendant
 * dix-huit mois.<BR>
*/
public class EOCongeAl4 extends CongeHospitaloUniversitaire {
	/** duree de la premiere periode */
	public static int NB_MOIS_1ERE_PERIODE = 12;
	/** duree de la deuxieme  periode */
	public static int NB_MOIS_2EME_PERIODE = 6;
	/** nombre de mois remunere au deux-tiers du traitement */
	public static int NB_MOIS_66 = 18;
	/** nombre de mois sans traitement */
	public static int NB_MOIS_SANS_TRAITEMENT = 18;
	private static int[] MOIS_REMUNERATION = {NB_MOIS_66,NB_MOIS_SANS_TRAITEMENT};
	private static int[] TAUX_REMUNERATION = {66,0};
	
    public EOCongeAl4() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOCongeAl4(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
    
    // méthodes ajoutées
    public String typeEvenement() {
		return "AHCUAO4";
	}
	/** utilise pour la communication avec GRhum */
	public String parametreOccupation() {
			return "CL_HU4";
	}
	public void validateForSave() {
		super.validateForSave();
		NSArray congesAssociesAnterieurs = congesAssocies(true);
		// la date d'avis du comité médical doit être fournie
		if (congesAssociesAnterieurs.count() <= 1 &&  dComMed() == null) {
			throw new NSValidation.ValidationException("La date d'avis du comité médical doit être fournie");
		}
		int dureeConge = dureeLegale();
		
		try {
			if (dureeConge > 0 && DateCtrl.dureeComptable(dateDebut(),dateFin(),true) > dureeConge) {
				throw new NSValidation.ValidationException("La durée légale de ce congé est " + dureeConge);
			}
		} catch (Exception e) {}	// ne devrait pas se produire
		// vérifier si la durée du congé est supérieure à la durée maximale
		int nbMoisMaxi = dureeMaximumConge();
		int dureeTotale = dureeEnMoisCongesContinusAssocies();
		if (dureeTotale > nbMoisMaxi) {
			throw new NSValidation.ValidationException("Le congé hospitalo-universitaire Alinéa 4 ne peut dépasser " + nbMoisMaxi + " mois");
		}
	}
	public boolean dateAvisObligatoire() {
		return true;
	}
	public int dureeMaxiConge() {
		return 0;
	}
	/** retourne la duree legale du conge */
	public int dureeLegale() {
		NSArray congesAssociesAnterieurs = congesAssocies(true);
		if (congesAssociesAnterieurs.count() == 0) {
			return NB_MOIS_1ERE_PERIODE;
		} else if (congesAssociesAnterieurs.count() == 1) {
			return NB_MOIS_2EME_PERIODE;
		} else {
			return 0;
		}
	}
	public int[] tableMoisRemuneration() {
		return MOIS_REMUNERATION; 
	}
	public int[] tableTauxRemuneration() {
		return TAUX_REMUNERATION;
	}
	public static NSDictionary dureeEnMoisPourTaux() {
		NSMutableDictionary dict = new NSMutableDictionary();
		for (int i = 0;i < MOIS_REMUNERATION.length;i++) {
			dict.setObjectForKey(new Integer(MOIS_REMUNERATION[i]),new Integer(TAUX_REMUNERATION[i]));
		}
		return dict;
	}
}
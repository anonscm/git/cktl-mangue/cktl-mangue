// _EODeclarationMaternite.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODeclarationMaternite.java instead.
package org.cocktail.mangue.modele.mangue.conges;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EODeclarationMaternite extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "DeclarationMaternite";
	public static final String ENTITY_TABLE_NAME = "DECLARATION_MATERNITE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dMatOrdre";

	public static final String D_ACCOUCHEMENT_KEY = "dAccouchement";
	public static final String D_CONSTAT_MATERN_KEY = "dConstatMatern";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAIS_PREV_KEY = "dNaisPrev";
	public static final String NB_ENFANTS_DECL_KEY = "nbEnfantsDecl";
	public static final String TEM_ANNULE_KEY = "temAnnule";
	public static final String TEM_ANNULE_BIS_KEY = "temAnnuleBis";
	public static final String TEM_GROSSESSE_GEMELLAIRE_KEY = "temGrossesseGemellaire";
	public static final String TEM_GROSSESSE_TRIPLE_KEY = "temGrossesseTriple";

// Attributs non visibles
	public static final String D_MAT_ORDRE_KEY = "dMatOrdre";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";

//Colonnes dans la base de donnees
	public static final String D_ACCOUCHEMENT_COLKEY = "D_ACCOUCHEMENT";
	public static final String D_CONSTAT_MATERN_COLKEY = "D_CONSTAT_MATERN";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_NAIS_PREV_COLKEY = "D_NAIS_PREV";
	public static final String NB_ENFANTS_DECL_COLKEY = "NB_ENFANTS_DECL";
	public static final String TEM_ANNULE_COLKEY = "TEM_ANNULE";
	public static final String TEM_ANNULE_BIS_COLKEY = "TEM_ANNULE_BIS";
	public static final String TEM_GROSSESSE_GEMELLAIRE_COLKEY = "TEM_GROSSESSE_GEMELLAIRE";
	public static final String TEM_GROSSESSE_TRIPLE_COLKEY = "TEM_GROSSESSE_TRIPLE";

	public static final String D_MAT_ORDRE_COLKEY = "DMAT_ORDRE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";


	// Relationships
	public static final String CONGES_MATERNITE_KEY = "congesMaternite";
	public static final String INDIVIDU_KEY = "individu";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dAccouchement() {
    return (NSTimestamp) storedValueForKey(D_ACCOUCHEMENT_KEY);
  }

  public void setDAccouchement(NSTimestamp value) {
    takeStoredValueForKey(value, D_ACCOUCHEMENT_KEY);
  }

  public NSTimestamp dConstatMatern() {
    return (NSTimestamp) storedValueForKey(D_CONSTAT_MATERN_KEY);
  }

  public void setDConstatMatern(NSTimestamp value) {
    takeStoredValueForKey(value, D_CONSTAT_MATERN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dNaisPrev() {
    return (NSTimestamp) storedValueForKey(D_NAIS_PREV_KEY);
  }

  public void setDNaisPrev(NSTimestamp value) {
    takeStoredValueForKey(value, D_NAIS_PREV_KEY);
  }

  public Integer nbEnfantsDecl() {
    return (Integer) storedValueForKey(NB_ENFANTS_DECL_KEY);
  }

  public void setNbEnfantsDecl(Integer value) {
    takeStoredValueForKey(value, NB_ENFANTS_DECL_KEY);
  }

  public String temAnnule() {
    return (String) storedValueForKey(TEM_ANNULE_KEY);
  }

  public void setTemAnnule(String value) {
    takeStoredValueForKey(value, TEM_ANNULE_KEY);
  }

  public String temAnnuleBis() {
    return (String) storedValueForKey(TEM_ANNULE_BIS_KEY);
  }

  public void setTemAnnuleBis(String value) {
    takeStoredValueForKey(value, TEM_ANNULE_BIS_KEY);
  }

  public String temGrossesseGemellaire() {
    return (String) storedValueForKey(TEM_GROSSESSE_GEMELLAIRE_KEY);
  }

  public void setTemGrossesseGemellaire(String value) {
    takeStoredValueForKey(value, TEM_GROSSESSE_GEMELLAIRE_KEY);
  }

  public String temGrossesseTriple() {
    return (String) storedValueForKey(TEM_GROSSESSE_TRIPLE_KEY);
  }

  public void setTemGrossesseTriple(String value) {
    takeStoredValueForKey(value, TEM_GROSSESSE_TRIPLE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public NSArray congesMaternite() {
    return (NSArray)storedValueForKey(CONGES_MATERNITE_KEY);
  }

  public NSArray congesMaternite(EOQualifier qualifier) {
    return congesMaternite(qualifier, null, false);
  }

  public NSArray congesMaternite(EOQualifier qualifier, boolean fetch) {
    return congesMaternite(qualifier, null, fetch);
  }

  public NSArray congesMaternite(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(EOCongeMaternite.DECLARATION_MATERNITE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results =EOCongeMaternite.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = congesMaternite();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCongesMaterniteRelationship(EOCongeMaternite object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONGES_MATERNITE_KEY);
  }

  public void removeFromCongesMaterniteRelationship(EOCongeMaternite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONGES_MATERNITE_KEY);
  }

  public EOCongeMaternite createCongesMaterniteRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CongeMaternite");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONGES_MATERNITE_KEY);
    return (EOCongeMaternite) eo;
  }

  public void deleteCongesMaterniteRelationship(EOCongeMaternite object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONGES_MATERNITE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCongesMaterniteRelationships() {
    Enumeration objects = congesMaternite().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCongesMaterniteRelationship((EOCongeMaternite)objects.nextElement());
    }
  }


/**
 * Créer une instance de EODeclarationMaternite avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODeclarationMaternite createEODeclarationMaternite(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temAnnule
, String temGrossesseGemellaire
, String temGrossesseTriple
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu			) {
    EODeclarationMaternite eo = (EODeclarationMaternite) createAndInsertInstance(editingContext, _EODeclarationMaternite.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemAnnule(temAnnule);
		eo.setTemGrossesseGemellaire(temGrossesseGemellaire);
		eo.setTemGrossesseTriple(temGrossesseTriple);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  
	  public EODeclarationMaternite localInstanceIn(EOEditingContext editingContext) {
	  		return (EODeclarationMaternite)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODeclarationMaternite creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODeclarationMaternite creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODeclarationMaternite object = (EODeclarationMaternite)createAndInsertInstance(editingContext, _EODeclarationMaternite.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODeclarationMaternite localInstanceIn(EOEditingContext editingContext, EODeclarationMaternite eo) {
    EODeclarationMaternite localInstance = (eo == null) ? null : (EODeclarationMaternite)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODeclarationMaternite#localInstanceIn a la place.
   */
	public static EODeclarationMaternite localInstanceOf(EOEditingContext editingContext, EODeclarationMaternite eo) {
		return EODeclarationMaternite.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODeclarationMaternite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODeclarationMaternite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODeclarationMaternite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODeclarationMaternite)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODeclarationMaternite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODeclarationMaternite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODeclarationMaternite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODeclarationMaternite)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODeclarationMaternite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODeclarationMaternite eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODeclarationMaternite ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODeclarationMaternite fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

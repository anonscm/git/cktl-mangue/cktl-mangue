// EOClm.java
// Created on Wed Mar 19 10:06:21  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.Duree;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation<BR>
 * 1. Le conge de longue maladie  ne concerne qu'un agent fonctionnaire titulaire ou stagiaire.<BR>
 * 2. Le conge doit reposer sur une position d'activite ou de detachement avec gestion de la carriere d'accueil dans 
 * l'etablissement.<BR>
 * 3. Un conge de longue maladie ne peut etre requalifie qu'en presence d'un conge de longue duree<BR>
 * 4. Il est impossible de produire un projet d'arrete de conge longue maladie si une date d'avis du comite medical s
 * uperieur est renseignee<BR>
 * 5. La duree maximale du conge est de 6 mois<BR>
 * 6. Dans le cas de conges de longue maladie continus pendant 3 ans, l'agent doit reprendre son travail pendant 1 an pour beneficier 
 * d'un nouveau conge de longue maladie.<BR>
 */
public class EOClm extends _EOClm implements ICongeAvecRequalification {
	
	public EOClm() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOClm creer(EOEditingContext ec, EOIndividu individu) {
		return creer(ec, individu, null);
	}
	
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static EOClm creer(EOEditingContext edc, EOIndividu individu, EOAbsences absence) {

		EOClm newObject = new EOClm();    

		newObject.setIndividuRelationship(individu);
        newObject.setAbsenceRelationship(absence);        
		newObject.setEstValide(true);
		newObject.setTemConfirme("O");
		newObject.setTemEnCause("N");
		newObject.setTemGestEtab("O");
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());
		edc.insertObject(newObject);
		return newObject;
	}


	/**
	 * 
	 * Requalification de conges maladie en CLM.
	 * 
	 * 
	 * @param ec
	 * @param absences
	 * @param dateDebut
	 * @param dateFin
	 * @throws Exception
	 */
	public static void requalifier(EOEditingContext ec, NSArray<EOAbsences> absences, NSTimestamp dateDebut, NSTimestamp dateFin, NSTimestamp dateComite) throws Exception {

		try {

			EOAbsences premiereAbsence = absences.get(0);
			EOAbsences derniereAbsence = absences.get(absences.count() - 1);

			EOAbsences absencePrecedente = null;		
			for (EOAbsences absence : absences) {

				if (!absence.toTypeAbsence().estCongeMaladie())
					throw new ValidationException("Un des congés sélectionnés n'est pas un congé maladie !");

				if (absences.size() > 1) {
					if (absencePrecedente != null && DateCtrl.nbJoursEntre(absencePrecedente.dateFin(), absence.dateDebut(), false) != 1)
						throw new ValidationException("Les congés maladie à requalifier ne sont pas continus !");
				}

				absencePrecedente = absence;

				// Suppression du CMO associe
				EOCongeMaladie congeMaladie = (EOCongeMaladie)rechercherCongeAvecAbsence(ec, EOCongeMaladie.ENTITY_NAME, absence);
				if (congeMaladie != null) {
					congeMaladie.setTemValide("N");
					absence.setTemValide("N");
				}
				else {
					EOCgntMaladie congeMaladiteNonTitulaire = (EOCgntMaladie)rechercherCongeAvecAbsence(ec, EOCgntMaladie.ENTITY_NAME, absence);					
					if (congeMaladiteNonTitulaire != null) {
						throw new ValidationException("La requalification n'est pas possible pour un agent non titulaire !");
					}
				}
			}

			// Creation du CLM et de l'Absence
			EOAbsences absence = EOAbsences.creer(ec, premiereAbsence.individu(), EOTypeAbsence.findForCode(ec, EOTypeAbsence.TYPE_CLM));
			absence.setDateDebut(premiereAbsence.dateDebut());
			absence.setDateFin(derniereAbsence.dateFin());

			EOClm clm = creer(ec, premiereAbsence.individu(), absence);			
			clm.initAvecDates(premiereAbsence.dateDebut(), derniereAbsence.dateFin());
			clm.setTemRequalifClm(CocktailConstantes.VRAI);
			clm.setDComMedClm(dateComite);
			clm.setCommentaire("Requalification de congés maladie");			

		}
		catch (Exception e) {  
			e.printStackTrace();
			throw e;
		}

	}

	public String typeEvenement() {
		return EOTypeAbsence.TYPE_CLM;
	}

	public String parametreOccupation() {
		return "CL_CDLM";
	}
	public boolean estProlonge() {
		return temProlongClm() != null && temProlongClm().equals(CocktailConstantes.VRAI);
	}
	public void setEstProlonge(boolean aBool) {
		if (aBool)
			setTemProlongClm(CocktailConstantes.VRAI);
		else
			setTemProlongClm(CocktailConstantes.FAUX);
	}
	public boolean estRequalifie() {
		return temRequalifClm() != null && temRequalifClm().equals(CocktailConstantes.VRAI);
	}
	public void setEstRequalifie(boolean aBool) {
		if (aBool) {
			setTemRequalifClm(CocktailConstantes.VRAI);
		} else {
			setTemRequalifClm(CocktailConstantes.FAUX);
		}
	}
	public String dateComMedFormatee() {
		return SuperFinder.dateFormatee(this,D_COM_MED_CLM_KEY);
	}
	public void setDateComMedFormatee(String uneDate) {
		if (uneDate == null) {
			setDComMedClm(null);
		} else {
			SuperFinder.setDateFormatee(this,D_COM_MED_CLM_KEY,uneDate);
		}
	}
	public String dateComMedSupFormatee() {
		return SuperFinder.dateFormatee(this,D_COM_MED_CLM_SUP_KEY);
	}
	
	public void setDateComMedSupFormatee(String uneDate) {
		if (uneDate == null) {
			setDComMedClmSup(null);
		} else {
			SuperFinder.setDateFormatee(this,D_COM_MED_CLM_SUP_KEY,uneDate);
		}
	}
	
	/**
	 * 
	 */
	public void validateForSave() {
		
		super.validateForSave();
		if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin())) {
			if (EOChangementPosition.fonctionnaireEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin()) == false)
				throw new NSValidation.ValidationException("Le congé de longue maladie ne concerne qu'un agent fonctionnaire titulaire ou stagiaire");
		} else {
			throw new NSValidation.ValidationException("Le congé de longue maladie doit reposer sur une position d'activité ou de détachement avec gestion de la carrière d’accueil dans l'établissement");
		}
		if (dComMedClmSup() != null && (dateArrete() != null || noArrete() != null)) {
			throw new NSValidation.ValidationException("Il est impossible de produire un projet d'arrêté de congé longue maladie si une date d'avis du comité médical supérieur est renseignée");
		}
		int nbMois = DateCtrl.calculerDureeEnMois(dateDebut(),dateFin(),true).intValue();
		int nbMoisMax = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_DUREE_BLOQUANTE).intValue();    
		
		if (nbMois > nbMoisMax) {
			throw new NSValidation.ValidationException("La durée maximum d'un congé de longue maladie est de " + nbMoisMax + " mois");
		}

		// vérifier, si il existe des congés continus pour une période de 3 ans, qu'il existe plus d'une année écoulée avant la reprise d'un nouveau Clm
		NSArray autresClm = Duree.rechercherDureesPourEntiteAnterieuresADate(editingContext(),"Clm",individu(),dateDebut());
		if (autresClm.count() > 0) {
			// triés par ordre décroissant
			autresClm = EOSortOrdering.sortedArrayUsingKeyOrderArray(autresClm,new NSArray(EOSortOrdering.sortOrderingWithKey("dateFin",EOSortOrdering.CompareDescending)));
			EOClm dernierClm = (EOClm)autresClm.objectAtIndex(0);
			
			int dureePeriodeRef = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_PERIODE_REFERENCE).intValue();    
			
			if (DateCtrl.calculerDureeEnMois(dernierClm.dateFin(),dateDebut(),true).intValue() < dureePeriodeRef * 12) {
				// moins d'un an écoulé
				nbMoisMax = EOClm.nbMoisMaxCongesContinus(editingContext());
				if (dernierClm.dureeEnMoisCongesContinusAssocies() == nbMoisMax) {	// 3 ans de congés continus
					throw new NSValidation.ValidationException("Suite à " + nbMoisMax / 12 + " ans de congés longue maladie continus,l'agent doit avoir repris son travail pendant au moins " + dureePeriodeRef + " an pour pouvoir reprendre un congé de longue maladie");
				}
			}
		}
		
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	// méthodes protégées
	protected void init() {
		super.init();
		setTemRequalifClm(CocktailConstantes.FAUX);
		setTemProlongClm(CocktailConstantes.FAUX);
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static int nbMoisMaxCongesContinus(EOEditingContext editingContext) {
		
		int nbMoisPleinTraitement = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_PLEIN_TRAITEMENT).intValue();  
		int nbMoisDemiTraitement = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CLM_DEMI_TRAITEMENT).intValue();  
		
		return nbMoisPleinTraitement + nbMoisDemiTraitement;
	}
	
	// méthodes protégées statiques
	/** qualifier de date a pour la periode de remise en cause : pour les conges de longue maladie, ce sont tous les
	 * conges de longue maladie dans la periode glissante (4 ans) */
	protected EOQualifier qualifierPeriodeRemiseEnCause() {
		// tous les congés qui se commencent ou se terminent dans la période glissante : date à date + durée glissante
		int dureeGlissement = DetailConge.evaluerAnneeGlissante(editingContext(),entityName());
		// dans les congés la date de fin ne peut être nulle
		NSTimestamp finPeriode = DateCtrl.dateAvecAjoutAnnees(dateFin(),dureeGlissement);
		NSMutableArray args = new NSMutableArray(dateFin());
		args.addObject(finPeriode);
		args.addObject(dateFin());
		args.addObject(dateFin());
		args.addObject(finPeriode);
		return EOQualifier.qualifierWithQualifierFormat("(dateDebut >= %@ AND dateDebut <= %@) OR (dateDebut < %@ AND dateFin = NIL) OR (dateFin >= %@ AND dateFin <= %@)",args);
	}

	public NSArray details() {
		return DetailConge.preparerDetailsGlissantsPourPeriode(editingContext(),entityName(),individu(),dateDebut(),dateFin());
	}
}

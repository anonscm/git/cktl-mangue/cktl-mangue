/*
 * Created on 3 févr. 2006
 *
 * Modélise les  accidents du travail
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.acc_travail.EOCertificatMedical;
import org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Classe abstraite : modelise les  accidents du travail
 * Regles de validation : verifie que la date de l'accident et la d&eacute;claration sont fournies<BR>
 *
 */
// 23/03/2011 - Ajout de méthodes pour l'impression
public abstract class CongeAccidentTravail extends CongeAvecArreteAnnulation {
	private NSArray congesAssocies;	// Sera évalué pendant la méthode validateDorSave

	public CongeAccidentTravail() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOCgntAccidentTrav(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */

	public NSTimestamp dateAccident() {
		return (NSTimestamp)storedValueForKey("dateAccident");
	}

	public void setDateAccident(NSTimestamp value) {
		takeStoredValueForKey(value, "dateAccident");
	}

	public EODeclarationAccident declaration() {
		return (EODeclarationAccident)storedValueForKey("declaration");
	}

	public void setDeclaration(EODeclarationAccident value) {
		takeStoredValueForKey(value, "declaration");
	}

	// Méthodes ajoutées
	public String dateAccidentFormatee() {
		return SuperFinder.dateFormatee(this,"dateAccident");
	}
	public void setDateAccidentFormatee(String uneDate) {
		if (uneDate == null) {
			setDateAccident(null);
		} else {
			SuperFinder.setDateFormatee(this,"dateAccident",uneDate);
		}
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		if (declaration() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(declaration(),"declaration");
		}
	}
	public void validateForSave() {
		super.validateForSave();
		if (dateAccident() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date de l'accident du travail");
		}

		if (declaration() != null && DateCtrl.isSameDay(declaration().daccDate(),dateAccident()) == false) {
			throw new NSValidation.ValidationException("La date de la déclaration n'est pas celle de l'accident");
		}
		if (DateCtrl.isBefore(dateDebut(),dateAccident())) {
			throw new NSValidation.ValidationException("Le congé ne peut pas commencer avant la date de l'accident");
		}
		if (entityName().equals("CgntAccidentTrav")) {
			// Recalculer la date de début et de fin de traitement pour tous les congés qui 

		}
	}
	/** Retourne la liste des conges associes a la declaration
	 * @param congesAnterieurs si true : retourne la liste des conges associes a la declaration anterieurs au conge courant
	 */
	public NSArray congesAssocies(boolean congesAnterieurs) {
		try{
			NSMutableArray args = new NSMutableArray(declaration());
			EOQualifier qual;
			// ne prendre en compte que les congés non annulés
			if (congesAnterieurs) {
				args.addObject(DateCtrl.jourPrecedent(dateDebut()));
				qual = EOQualifier.qualifierWithQualifierFormat("declaration = %@ AND dateFin <= %@ AND absence.temValide = 'O'",args);
			} else {
				qual = EOQualifier.qualifierWithQualifierFormat("declaration = %@ AND absence.temValide = 'O'",args);
			}
			EOFetchSpecification fs = new EOFetchSpecification(entityName(), qual,null);
			return editingContext().objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	} 
	// Méthodes pour l'impression
	/** Retourne la date du certificat medical dont la date est anterieure a ce conge et la
	 * plus proche de la date de debut du conge. Retourne null si pas de certificat
	 */
	public NSTimestamp dateCertificatMedical() {
		NSArray certificats = EOCertificatMedical.findForDeclaration(editingContext(), declaration());
		if (certificats == null || certificats.count() == 0) {
			return null;
		}
		certificats = EOSortOrdering.sortedArrayUsingKeyOrderArray(certificats, new NSArray(EOSortOrdering.sortOrderingWithKey("cmedDate", EOSortOrdering.CompareDescending)));
		java.util.Enumeration e = certificats.objectEnumerator();
		while (e.hasMoreElements()) {
			EOCertificatMedical certificat = (EOCertificatMedical)e.nextElement();
			if (DateCtrl.isBeforeEq(certificat.cmedDate(), dateDebut())) {
				return certificat.cmedDate();
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean aAutresConges() {
		if (congesAssocies == null) {
			congesAssocies = congesAssocies(true);
		}
		return new Boolean(congesAssocies != null && congesAssocies.count() > 0);
	}
	/** Retourne les cong&eacute;s pr&eacute;c&eacute;dents tri&eacute;s par ordre de date croissant */
	public NSArray congesPrecedents() {
		if (congesAssocies == null) {
			congesAssocies = congesAssocies(true);
		}
		congesAssocies = EOSortOrdering.sortedArrayUsingKeyOrderArray(congesAssocies, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
		return congesAssocies;
	}
	// Méthodes statiques
	/** Retourne le cong&eacute; d'accident du travail ("CgntAccidentTrav" ou "CongeAccidentServ" selon le nom de l'entit&eacute;)
	 * pour un individu &agrave; la date de l'accident
	 */
	public static CongeAccidentTravail congePourIndividuEtDateAccident(EOEditingContext editingContext,String nomEntite,EOIndividu individu,NSTimestamp dateAccident) {
		NSMutableArray args = new NSMutableArray(individu);
		args.addObject(dateAccident);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("individu = %@ AND dateAccident = %@", args);
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier,null);
		try {
			return (CongeAccidentTravail)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
}
//EOCgntAccidentTrav.java
//Created on Fri Feb 03 10:08:18 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOParamAnciennete;
import org.cocktail.mangue.modele.mangue.acc_travail.EODeclarationAccident;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** L'accident de travail ne concerne que les agents contractuels ou les contractuels assimiles qui ont une carriere (TEM_fonctionnaire = N dans type_population).<BR>
 * L'agent doit avoir un contrat de travail ou une carriere de non fonctionnaire en activite ou detachement avec carriere 
 * d'accueil le jour de l'accident de travail.<BR>
 * L'agent doit avoir un contrat de travail ou une carriere de non fonctionnaire en activite ou detachement avec carriere 
 * d'accueil pendant tout le conge.<BR>
 * La date de debut de perception a plein traitement doit etre posterieure a la date de debut du conge<BR>
 * La date de fin de perception a plein traitement doit etre anterieure  a la date de fin du conge accident de travail
 * Tous les conges associes a une meme declaration doivent avoir la meme anciennete<BR>
 * 
 */
// 23/03/2011 - Rajout de règles de validation sur les congés associés + méthodes pour l'impression
// 24/03/2011 - Suppression des contraintes sur les dates de plein traitement : elles sont nulles si le temps de plein traitement est déjà couvert
public class EOCgntAccidentTrav extends _EOCgntAccidentTrav {

	public EOCgntAccidentTrav() {
		super();
	}

	
	public static EOCgntAccidentTrav creer(EOEditingContext ec, EODeclarationAccident declaration) {

		EOCgntAccidentTrav newObject = (EOCgntAccidentTrav) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(declaration.individu());
		newObject.setDeclarationRelationship(declaration);
		newObject.setDateAccident(declaration.daccDate());
		newObject.setDateDebut(declaration.daccDate());
		newObject.setTemConfirme(CocktailConstantes.FAUX);
		newObject.setTemGestEtab(CocktailConstantes.VRAI);
		newObject.setTemValide(CocktailConstantes.VRAI);
		
		return newObject;
	}

	public static NSArray rechercherTousCongesPourDeclaration(EOEditingContext ec, EODeclarationAccident declaration) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DECLARATION_KEY + "=%@", new NSArray(declaration)));
			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		} catch (Exception e) {
			return null;
		}
	}
	public static NSArray rechercherPourDeclaration(EOEditingContext ec, EODeclarationAccident declaration) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DECLARATION_KEY + "=%@", new NSArray(declaration)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DEBUT_DESC);
		} catch (Exception e) {
			return null;
		}
	}


	public String temConfirme() {
		return null;
	}
	public void setTemConfirme(String value) {
	}

	public String temGestEtab() {
		return null;
	}
	public void setTemGestEtab(String value) {
	}
	public NSTimestamp dAnnulation() {
		return null;
	}
	public void setDAnnulation(NSTimestamp value) {
	}
	public String noArreteAnnulation() {
		return null;
	}
	public void setNoArreteAnnulation(String value) {
	}
	public CongeAvecArrete congeDeRemplacement() {
		return null;
	}
	public void setCongeDeRemplacement(CongeAvecArrete value) {

	}
	public boolean estAnnule() {
		return false;
	}
	public boolean aCongesRemplaces() {
		return false;
	}
	// autres
	public String typeEvenement() {
		return "ACCTRAV";
	}
	public void ajouterAnciennete(EOParamAnciennete anciennete) {
		addObjectToBothSidesOfRelationshipWithKey(anciennete,"anciennete");
		int duree = anciennete.dureePleinTrait().intValue();
		if (duree != 0) {
			setDDebPleinTrait(dateDebut());
			NSTimestamp dateFinPleinTraitement = DateCtrl.dateAvecAjoutMois(dateDebut(),duree);
			if (DateCtrl.isBefore(dateFinPleinTraitement,dateFin())) {
				setDFinPleinTrait(dateFinPleinTraitement);
			}
		}
	}
	public void supprimerRelations() {
		super.supprimerRelations();
		removeObjectFromBothSidesOfRelationshipWithKey(anciennete(),"anciennete");
	}
	public void validateForSave() {
		super.validateForSave();
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de fin !");
		}
		if (anciennete() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir l'ancienneté");
		}
		if (!individu().estContractuelSurPeriodeComplete(dateAccident(),dateAccident()))  {
			throw new NSValidation.ValidationException("L'agent doit avoir un contrat de travail ou une carrière de non fonctionnaire en activité ou détaché dans l'établissement le jour de l'accident de travail");
		}
		if (!individu().estContractuelSurPeriodeComplete(dateDebut(),dateFin())) {
			throw new NSValidation.ValidationException("L'agent doit avoir un contrat de travail ou une carrière de non fonctionnaire en activité ou détaché dans l'établissement pendant tout le congé");
		}
		/* 24/03/2011 - Suppression de la contrainte
		if (dDebPleinTrait() == null) {
			throw new NSValidation.ValidationException("La date de début de plein traitement doit être fournie");
		}
		if (dFinPleinTrait() == null) {
			throw new NSValidation.ValidationException("La date de fin de plein traitement doit être fournie");
		} */
		if (dDebPleinTrait() != null && dFinPleinTrait() != null) {
			if (DateCtrl.isBefore(dFinPleinTrait(),dDebPleinTrait())) {
				throw new NSValidation.ValidationException("La date de fin de plein traitement doit être antérieure à la date de début de plein traitement");
			}
			if (DateCtrl.isBefore(dDebPleinTrait(),dateDebut())) {
				throw new NSValidation.ValidationException("La date de début de plein traitement doit être postérieure à la date de début du congé");
			}
			if (DateCtrl.isBefore(dateFin(),dFinPleinTrait())) {
				throw new NSValidation.ValidationException("La date de fin de plein traitement doit être antérieure à la date de fin du congé");
			}
		}
		// 23/03/2011 - vérifier l'ancienneté dans les congés associés
		NSArray congesAssocies = congesAssocies(false);
		for (java.util.Enumeration<EOCgntAccidentTrav> e = congesAssocies.objectEnumerator();e.hasMoreElements();) {
			EOCgntAccidentTrav conge = e.nextElement();
			if (conge != this && conge.anciennete() != null && conge.anciennete() != anciennete()) {
				throw new NSValidation.ValidationException("Vous devez fournir la même ancienneté ( " + conge.anciennete().lAnciennete() + ") que le congé du " + conge.dateDebutFormatee() + " au " + conge.dateFinFormatee());
			}
		}
	}
	// 23/03/2011 - méthodes pour l'impression
	/** Retourne une string comportant les dates du conge, sa duree, sa duree comptable et
	 * le nb de jours a plein traitement. Ex : du 01/01/2011 au 30/03/2011 : 8j - 6j compt. à PT
	 */
	public String descriptionCompleteConge() {
		String temp = "du " + dateDebutFormatee() + " au " + dateFinFormatee() + " : " + nbJours() + "j";
		if (dDebPleinTrait() != null) {
			try {
				int nbJoursPT = DateCtrl.dureeComptable(dDebPleinTrait(), dFinPleinTrait(), true);
				temp += " - " + nbJoursPT + "j compt. à PT";
			} catch (Exception e) {}
		}
		return temp;
	}
	/** Retourne  la somme des jours &agrave; plein traitement accorde pour ce conge et tous les conges
	 * anterieurs lies a la meme declaration
	 */
	public int nbJoursComptablesAPleinTraitementTousConges() {
		NSArray congesPrecedents = congesPrecedents();
		int nbJoursTotal = 0;
		if (congesPrecedents != null && congesPrecedents.count() > 0) {
			for (java.util.Enumeration<EOCgntAccidentTrav> e = congesPrecedents.objectEnumerator();e.hasMoreElements();) {
				EOCgntAccidentTrav conge = e.nextElement();
				if (conge.dDebPleinTrait() != null) {
					try {
						int nbJoursComptables = DateCtrl.dureeComptable(conge.dDebPleinTrait(), conge.dFinPleinTrait(), true);
						nbJoursTotal += nbJoursComptables;
					} catch (Exception exc) {}
				}
			}
		}
		// Ajouter la durée à plein traitement de ce congé
		if (dDebPleinTrait() != null) {
			try {
				int nbJoursComptables = DateCtrl.dureeComptable(dDebPleinTrait(), dFinPleinTrait(), true);
				nbJoursTotal += nbJoursComptables;
			} catch (Exception exc) {}	
		}
		return nbJoursTotal;
	}

}

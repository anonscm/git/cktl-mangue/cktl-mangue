/*
 * Created on 13 févr. 2006
 *
 * Modélisation des congés de maladie fonctionnaires et contractuels
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.conges.CalculDetailsTraitementsCLM;
import org.cocktail.mangue.common.conges.CalculDetailsTraitementsCMNT;
import org.cocktail.mangue.common.conges.CalculDetailsTraitementsCMO;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;


/** Classe abstraite : modelisation des conges de maladie fonctionnaires et contractuels
 *
 */
public abstract class CongeMaladie extends CongeAvecDetails implements ICongeAvecRequalification {
	
	private static final long serialVersionUID = -5420860406891749413L;
	private NSArray<DetailConge> details = new NSMutableArray<DetailConge>();	// DT 60271 - pour pouvoir calculer le nombre de jours plein traitement
	private NSTimestamp debutPeriodeGlissante;
	
	public CongeMaladie() {
		super();
	}

	public String temRequalifMaladie() {
		return (String)storedValueForKey("temRequalifMaladie");
	}

	public void setTemRequalifMaladie(String value) {
		takeStoredValueForKey(value, "temRequalifMaladie");
	}

	public String temJourCarence() {
		return (String)storedValueForKey("temJourCarence");
	}
	public void setTemJourCarence(String value) {
		takeStoredValueForKey(value, "temJourCarence");
	}
	public String temProlongMaladie() {
		return (String)storedValueForKey("temProlongMaladie");
	}

	public void setTemProlongMaladie(String value) {
		takeStoredValueForKey(value, "temProlongMaladie");
	}
	public NSTimestamp dArretTravail() {
		return (NSTimestamp)storedValueForKey("dArretTravail");
	}

	public void setDArretTravail(NSTimestamp value) {
		takeStoredValueForKey(value, "dArretTravail");
	}
	
	
	public Integer nbJoursComptables() {
		return (Integer)storedValueForKey("nbJoursComptables");
	}
	public void setNbJoursComptables(Integer value) {
		takeStoredValueForKey(value, "nbJoursComptables");
	}

	
	public String dateArretFormatee() {
		return SuperFinder.dateFormatee(this,"dArretTravail");
	}
	public void setDateArretFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dArretTravail",uneDate);
	}
	
	public boolean jourDeCarence() {
		return temJourCarence() != null && temJourCarence().equals(CocktailConstantes.VRAI);
	}
	public void setJourDeCarence(boolean aBool) {
		if (aBool)
			setTemJourCarence(CocktailConstantes.VRAI);
		else
			setTemJourCarence(CocktailConstantes.FAUX);
	}

	public boolean estProlonge() {
		return temProlongMaladie() != null && temProlongMaladie().equals(CocktailConstantes.VRAI);
	}
	public void setEstProlonge(boolean aBool) {
		if (aBool)
			setTemProlongMaladie(CocktailConstantes.VRAI);
		else
			setTemProlongMaladie(CocktailConstantes.FAUX);
	}
	
	
	public boolean estJourDeCarence() {
		return temJourCarence() != null && temJourCarence().equals(CocktailConstantes.VRAI);
	}
	public void setEstJourDeCarence(boolean aBool) {
		if (aBool)
			setTemJourCarence(CocktailConstantes.VRAI);
		else
			setTemJourCarence(CocktailConstantes.FAUX);
	}

	public boolean estRequalifie() {
		return temRequalifMaladie() != null && temRequalifMaladie().equals(CocktailConstantes.VRAI);
	}
	public void setEstRequalifie(boolean aBool) {
		if (aBool) {
			setTemRequalifMaladie(CocktailConstantes.VRAI);
		} else {
			setTemRequalifMaladie(CocktailConstantes.FAUX);
		}
	}
	/** retourne les details de traitement pour un conge de maladie depuis la date de reference */
	public NSArray details() {
		return details;
	}
	
	/** 
	 * @return retourne le nombre de jours a plein traitement sur la periode glissante 
	 */
	public int nbJoursPleinTraitement() {
		if (details.size() == 0) {
			return 0;
		}
		
		int nbJours = 0;
		for (DetailConge myDetail : details) {
			if (myDetail.estPleinTraitement() || myDetail.estSansTraitement()) {
				nbJours += myDetail.duree().intValue();
			}
		}
		
		return nbJours;
	}
	
	/** 
	 * @return le nombre de jours a plein traitement sur la periode glissante 
	 */
	public int nbJoursComptablePleinTraitement() {
		int nbJours = 0;
		
		if (details.size() == 0) {
			return nbJours;
		}
		
		for (DetailConge myDetail : details) {
			if (myDetail.estPleinTraitement() || myDetail.estSansTraitement()) {
				nbJours += myDetail.dureeComptable().intValue();
			}
		}
		
		return nbJours;
	}
	
	/** 
	 * @return le nombre de jours comptables a plein traitement sur la periode glissante 
	 */
	public int nbJoursComptablesPleinTraitement() {
		//return nbJoursComptablePleinTraitement();
		return nbJoursPleinTraitement();
	}
	
	/** 
	 * @return le nombre de jours a plein traitement imprimable sur la periode glissante 
	 */
	public String nbJoursPleinTraitementImprimable() {
		return new Integer(nbJoursPleinTraitement()).toString();
	}
	
	/** 
	 * @return le nombre de jours comptables a plein traitement imprimable sur la periode glissantee 
	 */
	public String nbJoursComptablePleinTraitementImprimable() {
		return new Integer(nbJoursComptablesPleinTraitement()).toString();
	}
	
	/** 
	 * @return le debut de la periode glissante 
	 */
	public String debutPeriodeGlissante() {
		preparerDetails();
		return DateCtrl.dateToString(debutPeriodeGlissante);
	}
	// méthodes protégées
	protected void init() {
		super.init();
		setNbJoursComptables(new Integer(0));
		setTemRequalifMaladie(CocktailConstantes.FAUX);
		setTemProlongMaladie(CocktailConstantes.FAUX);
		setTemJourCarence(CocktailConstantes.FAUX);
	}

	/** qualifier de date a pour la periode de remise en cause : pour les conges de maladie, ce sont tous les
	 * conges de maladie dans la periode glissante (1 an) */
	protected EOQualifier qualifierPeriodeRemiseEnCause() {
		// tous les congés qui se commencent ou se terminent dans la période glissante : date à date + durée glissante
		int dureeGlissement = DetailConge.evaluerAnneeGlissante(editingContext(),entityName());
		// dans les congés la date de fin ne peut être nulle
		NSTimestamp finPeriode = DateCtrl.dateAvecAjoutAnnees(dateFin(),dureeGlissement);
		NSMutableArray args = new NSMutableArray(dateFin());
		args.addObject(finPeriode);
		args.addObject(dateFin());
		args.addObject(dateFin());
		args.addObject(finPeriode);
		return EOQualifier.qualifierWithQualifierFormat("(dateDebut >= %@ AND dateDebut <= %@) OR (dateDebut < %@ AND dateFin = NIL) OR (dateFin >= %@ AND dateFin <= %@)",args);
	}
	
	// Méthodes privées
	/**
	 * Affiche les détails de traitement du congé traité
	 */
	private void preparerDetails() {
		
		if (details.size() == 0) {
			if (entityName().equals("CongeMaladie")) {
				NSArray<DetailConge> detailsConge = CalculDetailsTraitementsCMO.sharedInstance().calculerDetails(editingContext(), individu(), dateDebut(), dateFin());

				//Période de glissement sur un an - 1 jour
				debutPeriodeGlissante = DateCtrl.dateAvecAjoutAnnees(dateDebut(), -1);
				debutPeriodeGlissante = DateCtrl.dateAvecAjoutJours(debutPeriodeGlissante, 1);
				
				//On affiche les congés de moins d'un an à partir de la date de début du congé traité
				for (DetailConge unDetail : detailsConge) {
					if (DateCtrl.isAfter(unDetail.dateFin(), debutPeriodeGlissante)) {
						if (DateCtrl.isBeforeEq(unDetail.dateDebut(), debutPeriodeGlissante)) {
							unDetail.setDateDebut(debutPeriodeGlissante);
							details.add(unDetail);
						} else {
							details.add(unDetail);
						}
					}
				}
			} else if (entityName().equals("CgntMaladie")) {
				CalculDetailsTraitementsCMNT calculCMNT = new CalculDetailsTraitementsCMNT();
				NSArray<DetailConge> detailsConge = calculCMNT.calculerDetails(editingContext(), individu(), dateDebut(), dateFin());

				//Période de glissement sur un an - 1 jour
				debutPeriodeGlissante = DateCtrl.dateAvecAjoutAnnees(dateDebut(), -1);
				debutPeriodeGlissante = DateCtrl.dateAvecAjoutJours(debutPeriodeGlissante, 1);
				
				//On affiche les congés de moins d'un an à partir de la date de début du congé traité
				for (DetailConge unDetail : detailsConge) {
					if (DateCtrl.isAfter(unDetail.dateFin(), debutPeriodeGlissante)) {
						if (DateCtrl.isBeforeEq(unDetail.dateDebut(), debutPeriodeGlissante)) {
							unDetail.setDateDebut(debutPeriodeGlissante);
							details.add(unDetail);
						} else {
							details.add(unDetail);
						}
					}
				}
			} else {
				if (entityName().equals("Clm")) {
					CalculDetailsTraitementsCLM calculCLM = new CalculDetailsTraitementsCLM();
					NSArray<DetailConge> detailsConge = calculCLM.calculerDetails(editingContext(), individu(), dateDebut(), dateFin());

					//Période de glissement sur un an - 1 jour
					debutPeriodeGlissante = DateCtrl.dateAvecAjoutAnnees(dateDebut(), -2);
					debutPeriodeGlissante = DateCtrl.dateAvecAjoutJours(debutPeriodeGlissante, 1);
					
					//On affiche les congés de moins d'un an à partir de la date de début du congé traité
					for (DetailConge unDetail : detailsConge) {
						if (DateCtrl.isAfter(unDetail.dateFin(), debutPeriodeGlissante)) {
							if (DateCtrl.isBeforeEq(unDetail.dateDebut(), debutPeriodeGlissante)) {
								unDetail.setDateDebut(debutPeriodeGlissante);
								details.add(unDetail);
							} else {
								details.add(unDetail);
							}
						}
					}
				}
				details = DetailConge.preparerDetailsGlissantsPourPeriode(editingContext(), entityName(), individu(), dateDebut(), dateFin());
			}
			
			
		}
	}
	

	/**
	 * Regroupement de periodes de conges ayant le meme taux de traitement
	 * Un conge peut donc etre fractionne en deux periodes à 100% et 50%
	 * @param arrayJours
	 * @return
	 */
	public static NSMutableArray regrouperPeriodesTaux(NSMutableArray arrayJours) {
		// Regroupement de toutes les periodes au même taux
		Number taux = (Number)new Integer(-1);	// 25/09/08 - pour afficher les taux 0 des non titulaires
		NSMutableArray periodesTraitement = new NSMutableArray();
		NSTimestamp lastDate = null;
		for (java.util.Enumeration<NSDictionary> e1 = arrayJours.objectEnumerator();e1.hasMoreElements();) {
			NSDictionary dicoTemp = e1.nextElement();

			// Si on change d'etat, on ajoute la date et l'etat
			if (taux.intValue() != ((Number)dicoTemp.objectForKey("taux")).intValue()) {
				if (periodesTraitement.count() > 0) {
					NSMutableDictionary periodePrecedente = (NSMutableDictionary)periodesTraitement.objectAtIndex(periodesTraitement.count() - 1);
					periodePrecedente.setObjectForKey(DateCtrl.jourPrecedent((NSTimestamp)dicoTemp.objectForKey("jour")),"fin");
				}
				NSMutableDictionary dicoTraitement = new NSMutableDictionary();
				dicoTraitement.setObjectForKey((NSTimestamp)dicoTemp.objectForKey("jour"),"debut");
				dicoTraitement.setObjectForKey(dicoTemp.objectForKey("taux"),"taux");
				taux = (Number)dicoTemp.objectForKey("taux");
				dicoTraitement.setObjectForKey(dicoTemp.objectForKey("carence"), "carence");
				periodesTraitement.addObject(dicoTraitement);
			}

			lastDate = (NSTimestamp)dicoTemp.objectForKey("jour");
		}
		// Pour la dernière période
		if (periodesTraitement.count() > 0) {
			NSMutableDictionary periodePrecedente = (NSMutableDictionary)periodesTraitement.objectAtIndex(periodesTraitement.count() - 1);
			periodePrecedente.setObjectForKey(lastDate,"fin");
		}

		return periodesTraitement;
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAbsences;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/** Classe pour cr&eacute;er des cong&eacute;s d&eacute;taill&eacute;s comportant les d&eacute;tails de traitement */
public class CongeDetaillePourHistorique implements NSKeyValueCoding {
	private NSMutableDictionary informationsConge;

	/** Constructeur par d&eacute;faut */
	public CongeDetaillePourHistorique() {
		informationsConge = new NSMutableDictionary();
	}
	/** Constructeur avec un cong&eacute; et l'absence associ&eacute;e */
	public CongeDetaillePourHistorique(EOAbsences absence, Conge conge) {
		this();
		initAvecConge(absence,conge);
		// Ajouter les détails de traitement
		if (conge instanceof CongeAvecDetails) {
			informationsConge.setObjectForKey(((CongeAvecDetails)conge).details(), "details");
		}
		if (conge instanceof EOCld) {
			informationsConge.setObjectForKey(((EOCld)conge).detailsTraitement(), "details");
		}
	}
	// Accesseurs
	public EOIndividu individu() {
		return (EOIndividu)informationsConge.objectForKey("individu");
	}
	public String typeConge() {
		return (String)informationsConge.objectForKey("typeConge");
	}
	public String dateDebut() {
		return (String)informationsConge.objectForKey("dateDebut");
	}
	public String dateFin() {
		return (String)informationsConge.objectForKey("dateFin");
	}
	public String nbJours() {
		return (String)informationsConge.objectForKey("nbJours");
	}
	public String dateArrete() {
		return (String)informationsConge.objectForKey("dateArrete");
	}
	public String noArrete() {
		return (String)informationsConge.objectForKey("noArrete");
	}
	public String signature() {
		return (String)informationsConge.objectForKey("signature");
	}
	public String requalification() {
		return (String)informationsConge.objectForKey("requalification");
	}
	public String valide() {
		return (String)informationsConge.objectForKey("valide");
	}
	public NSArray details() {
		return (NSArray)informationsConge.objectForKey("details");
	}
	//	 interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Méthodes statiques
	public static NSArray preparerCongesDetaillesPourAbsences(NSArray absences) {
		NSMutableArray congesDetailles = new NSMutableArray(absences.count());
		java.util.Enumeration e = absences.objectEnumerator();
		while (e.hasMoreElements()) {
			EOAbsences absence = (EOAbsences)e.nextElement();
			String nomEntite = EOEntity.nameForExternalName(absence.toTypeAbsence().codeHarpege(),"_",true);
			Conge conge = Conge.rechercherCongeAvecAbsence(absence.editingContext(), nomEntite, absence);
			if (conge != null) {	
				CongeDetaillePourHistorique congeDetaille = new CongeDetaillePourHistorique(absence, conge);
				congesDetailles.addObject(congeDetaille);
			}
		}
		return congesDetailles;
	}
	// Méthodes privées
	private void initAvecConge(EOAbsences absence, Conge conge) {
		informationsConge.setObjectForKey(absence.individu(),"individu");
		informationsConge.setObjectForKey(conge.dateDebutFormatee(),"dateDebut");
		if (conge.dateFin() != null) {
			informationsConge.setObjectForKey(conge.dateFinFormatee(), "dateFin");
		}
		informationsConge.setObjectForKey(absence.absDureeTotale(),"nbJours");
		if (conge instanceof CongeAvecArrete) {
			CongeAvecArrete congeA = (CongeAvecArrete)conge;
			if (congeA.dateArrete() != null) {
				informationsConge.setObjectForKey(congeA.dateArreteFormatee(), "dateArrete");
			}
			if (congeA.noArrete() != null) {
				informationsConge.setObjectForKey(congeA.noArrete(), "noArrete");
			}
		}
		informationsConge.setObjectForKey(absence.toTypeAbsence().libelleLong(), "typeConge");
		if (absence.temValide() != null) {
			informationsConge.setObjectForKey(conge.absence().temValide(), "valide");
		}
		if (conge instanceof CongeAvecArreteAnnulation) {
			CongeAvecArreteAnnulation congeAnnul = (CongeAvecArreteAnnulation)conge;
			if (congeAnnul.temConfirme() != null) {
				informationsConge.setObjectForKey(congeAnnul.temConfirme(), "signature");
			}
		}
		if (conge instanceof ICongeAvecRequalification) {
			ICongeAvecRequalification congeReq = (ICongeAvecRequalification)conge;
			if (congeReq.estRequalifie()) {
				informationsConge.setObjectForKey(new String("O"), "requalification");
			}
		}
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class CongeAvecDetails extends CongeAvecRemiseEnCause {
	private NSArray details;
	public CongeAvecDetails() {
		super();
	}

	public NSArray detailsTraitement() {
		details = details();
		return detailsPour(true);
	}
	public NSArray detailsPrecedents() {
		return detailsPour(false);
	}
	// Méthodes protégées
	protected abstract NSArray details();

	// Méthodes privées
	private NSArray detailsPour(boolean estPourConge) {

		try {
			if (this instanceof CongeHospitaloUniversitaire) {
				return detailsPourCongeHospitaloUniversitaire(estPourConge);
			} else {
				NSMutableArray nouveauxDetails = new NSMutableArray();
				for (java.util.Enumeration<DetailConge> e = details.objectEnumerator();e.hasMoreElements();) {
					DetailConge detail = e.nextElement();
					if ((estPourConge && DateCtrl.isAfterEq(detail.dateDebut(),dateDebut())) ||
							(!estPourConge && DateCtrl.isBefore(detail.dateFin(), dateDebut()))) {
						nouveauxDetails.addObject(detail);
					}
				}
				return nouveauxDetails;
			}
		}
		catch (Exception e) {
			return new NSArray();
		}
	}
	
	/**
	 * 
	 * @param estPourConge
	 * @return
	 */
	private NSArray detailsPourCongeHospitaloUniversitaire(boolean estPourConge) {
		NSMutableArray nouveauxDetails = new NSMutableArray();
		for (java.util.Enumeration<DetailConge> e = details.objectEnumerator();e.hasMoreElements();) {
			DetailConge detail = e.nextElement();
			// les détails des congés hospitalo universitaires peuvent se répartir sur plusieurs congés
			// lorsque les congés sont consécutifs
			if (estPourConge) {
				// Vérifier si il y a une intersection avec le congé courant
				if (DateCtrl.isBefore(detail.dateDebut(),dateDebut()) && DateCtrl.isAfterEq(detail.dateFin(),dateDebut()) &&
						DateCtrl.isBeforeEq(detail.dateFin(),dateFin())) {
					// détail à cheval, rajouter un détail et prendre comme date début le début du congé
					DetailConge nouveauDetail = new DetailConge();
					nouveauDetail.setDateDebut(dateDebut());
					nouveauDetail.setDateFin(detail.dateFin());
					nouveauDetail.setNumTaux(detail.numTaux());
					nouveauxDetails.addObject(nouveauDetail);
				}
				if (DateCtrl.isAfterEq(detail.dateDebut(),dateDebut()) && DateCtrl.isBefore(detail.dateDebut(),dateFin()) &&
						DateCtrl.isAfter(detail.dateFin(),dateFin())) {
					// détail à cheval, rajouter un détail et prendre comme date fin la fin du congé
					DetailConge nouveauDetail = new DetailConge();
					nouveauDetail.setDateDebut(detail.dateDebut());
					nouveauDetail.setDateFin(dateFin());
					nouveauDetail.setNumTaux(detail.numTaux());
					nouveauxDetails.addObject(nouveauDetail);
				}
				if (DateCtrl.isAfterEq(detail.dateDebut(),dateDebut()) && DateCtrl.isBeforeEq(detail.dateFin(),dateFin())) {
					nouveauxDetails.addObject(detail);
				}
			}
			if (!estPourConge) {
				// Vérifier si il y a une intersection avec le congé courant
				if (DateCtrl.isBefore(detail.dateDebut(),dateDebut()) && DateCtrl.isAfterEq(detail.dateFin(),dateDebut())) {
					// détail à cheval, rajouter un détail et prendre comme date fin le jour précédent la date début
					DetailConge nouveauDetail = new DetailConge();
					nouveauDetail.setDateDebut(detail.dateDebut());
					nouveauDetail.setDateFin(DateCtrl.jourPrecedent(dateDebut()));
					nouveauDetail.setNumTaux(detail.numTaux());
					nouveauxDetails.addObject(nouveauDetail);
				}
				if (DateCtrl.isBefore(detail.dateFin(),dateDebut())) {
					nouveauxDetails.addObject(detail);
				}
			}
		}
		return nouveauxDetails;
	}
}

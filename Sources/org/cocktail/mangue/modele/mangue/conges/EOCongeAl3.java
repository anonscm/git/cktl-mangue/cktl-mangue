// EOCongeAl3.java
// Created on Wed Feb 22 17:47:42 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;


import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;


/** Regles de validation :<BR>
 * La duree de conges consecutifs ne peut exceder 24 mois.<BR>
 * Au-dela de 9 mois, l'avis du comite medical doit etre fourni.<BR>
 * 1. L'agent beneficie pendant les trois premiers mois du maintien des deux tiers (66%) de sa remuneration.<BR>
 * 2. L'agent beneficie pendant six mois suivants de la moitie de sa remuneration.<BR>
 * 3. L'agent beneficie ensuite apres ces neuf mois consecutifs d’un conge sans remuneration pendant
 * douze mois apr&egrave;s avis du comite medical.<BR>
*/
// 07/11/2010 - correction du nombre de mois à 50%
public class EOCongeAl3 extends CongeHospitaloUniversitaire {
	/** nombre de mois r&eacute;mun&eacute;r&eacute; au deux-tiers du traitement */
	public static int NB_MOIS_66 = 3;
	/** nombre de mois r&eacute;mun&eacute;r&eacute; &agrave; 50% deux-tiers du traitement */
	public static int NB_MOIS_50 = 6;
	/** nombre de mois sans traitement */
	public static int NB_MOIS_SANS_TRAITEMENT = 12;
	private static int[] MOIS_REMUNERATION = {NB_MOIS_66,NB_MOIS_50,NB_MOIS_SANS_TRAITEMENT};
	private static int[] TAUX_REMUNERATION = {66,50,0};
	
    public EOCongeAl3() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOCongeAl3(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
    	public String typeEvenement() {
		return "AHCUAO3";
	}
	/** utilise pour la communication avec GRhum */
	public String parametreOccupation() {
			return "CL_HU3";
	}
    // méthodes ajoutées
	public void validateForSave() {
		super.validateForSave();
		// vérifier si la durée du congé est supérieure à la durée maximale
		int nbMoisMaxi = dureeMaximumConge();
		int dureeTotale = dureeEnMoisCongesContinusAssocies();
		if (dureeTotale > nbMoisMaxi) {
			throw new NSValidation.ValidationException("Le congé hospitalo-universitaire Alinéa 3 ne peut dépasser " + nbMoisMaxi + " mois");
		} else if (dureeTotale > NB_MOIS_SANS_TRAITEMENT && dComMed() == null) {
			throw new NSValidation.ValidationException("Au-delà de " + NB_MOIS_SANS_TRAITEMENT + " mois, vous devez fournir la date d'avis du comité médical");
		}
	}
	public boolean dateAvisObligatoire() {
		return false;
	}
	public int dureeMaxiConge() {
		return 0;
	}
	public int dureeLegale() {
		return 0;
	}
	public int[] tableMoisRemuneration() {
		return MOIS_REMUNERATION; 
	}
	public int[] tableTauxRemuneration() {
		return TAUX_REMUNERATION;
	}
	public static NSDictionary dureeEnMoisPourTaux() {
		NSMutableDictionary dict = new NSMutableDictionary();
		for (int i = 0;i < MOIS_REMUNERATION.length;i++) {
			dict.setObjectForKey(new Integer(MOIS_REMUNERATION[i]),new Integer(TAUX_REMUNERATION[i]));
		}
		return dict;
	}
}

// EOCongeAdoption.java
// Created on Wed Mar 19 10:41:33  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import java.util.Enumeration;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.IEvenementAvecEnfant;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartEnfant;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/** Regles de gestion
1. Le conge d'adoption si un seul parent en beneficie est de 10 semaines en cas d'adoption simple.<BR>
2. Le conge d'adoption si un seul parent en beneficie est de 18 semaines lorsque l'adoption simple porte le nombre d'enfants au moins &agrave; 3.<BR>
3. Le conge d'adoption si un seul parent en beneficie est de 22 semaines en cas d'adoptions multiples.<BR>
4. En cas de partage du conge d'adoption entre les deux parents :
<UL>Le conge peut être pris simultanement par les deux parents et doit être reparti en deux p&eacute;riodes.</UL>
<UL>Le conge d'adoption ne peut etre inferieur a 11 jours.</UL>
<UL>La duree maximale du conge est de 10 semaines et 11 jours pour une adoption simple, de 18 semaines et 11 jours lorsque l'adoption porte au moins a 3 le nombre d'enfants a charge, de 22 semaines et 18 jours en cas d'adoptions multiples.</UL>
5.  La date de debut du conge d'adoption ne peut preceder de plus de 7 jours la date d'arrivee au foyer de l'enfant.<BR>
6.  Pour un conge posterieur au 01/01/2004, l'enfant adopte doit &ecirc;tre fourni
 *

 */
// 02/07/2010 - ajout de la date minimum pour saisir obligatoire enfant + relation enfant
// 14/07/2010 - ajout d'une méthode de validation du Cir
public class EOCongeAdoption extends _EOCongeAdoption implements IEvenementAvecEnfant {
	public static int NB_JOURS_MIN_CONGE_PARTAGE = 11;
	public static int NB_JOURS_SUPP_CONGE_PARTAGE_ADOPTION_MULTIPLE = 18;
	public static int NB_JOURS_SUPP_CONGE_PARTAGE_AUTRE = 11;
	private static int NB_JOURS_AVANT_ARRIVEE = 7;
	public static final String DATE_LIMITE_POUR_DECLARATION_ENFANT ="01/01/2004";

	public EOCongeAdoption() {
		super();
	}

	public String typeEvenement() {
		return  EOTypeAbsence.TYPE_CONGE_ADOPTION;
	}
	/** utilis&eacute; pour la communication avec GRhum */
	public String parametreOccupation() {
		return "CL_CPA";
	}
	public String dateDemandeFormatee() {
		return SuperFinder.dateFormatee(this,"dDemande");
	}
	public void setDateDemandeFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this,"dDemande",uneDate);
	}
	public String dateArriveeFormatee() {
		return SuperFinder.dateFormatee(this,"dArriveeFoyer");
	}
	public void setDateArriveeFormatee(String uneDate) {
		if (uneDate == null) {
			setDArriveeFoyer(null);
		} else {
			SuperFinder.setDateFormatee(this,"dArriveeFoyer",uneDate);
		}
	}
	public boolean estPartage() {
		return temCgPartage() != null && temCgPartage().equals(CocktailConstantes.VRAI);
	}
	public void setEstPartage(boolean aBool) {
		if (aBool) {
			setTemCgPartage(CocktailConstantes.VRAI);
		} else {
			setTemCgPartage(CocktailConstantes.FAUX);
		}
	}

	public boolean estAdoptionMultiple() {
		return temAdoptionMultiple() != null && temAdoptionMultiple().equals(CocktailConstantes.VRAI);
	}
	public void setEstAdoptionMultiple(boolean aBool) {
		if (aBool) {
			setTemAdoptionMultiple(CocktailConstantes.VRAI);
		} else {
			setTemAdoptionMultiple(CocktailConstantes.FAUX);	
		}
	}
	
	/** retourne le nombre de jours legaux pour le conge */
	public int nbJoursLegaux(Integer nbEnfantsACharge) {
		
		int nbJoursAdoption = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_ADOPT_DUREE) * 7;
		int nbJoursAdoption2 = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_ADOPT_DUREE_2) * 7;
		int nbJoursAdoption3 = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_ADOPT_DUREE_3) * 7;

		int nbJoursAutorises = nbJoursAdoption;
		if (estAdoptionMultiple()) {
			nbJoursAutorises = nbJoursAdoption2;
		} else if (nbEnfantsACharge != null && nbEnfantsACharge.intValue() >= 2) {	// Les enfants à charge sont les enfants à charge avant adoption
			nbJoursAutorises = nbJoursAdoption3;
		}
		return nbJoursAutorises;
		
	}
	
	/**
	 * 
	 */
	public void validateForSave() {
		super.validateForSave();
		if (dArriveeFoyer() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date d'arrivée au foyer");
		}
		if (enfant() != null) {
			NSArray reparts = EORepartEnfant.rechercherRepartsPourEnfant(editingContext(), enfant());
			for (Enumeration<EORepartEnfant> e = reparts.objectEnumerator();e.hasMoreElements();) {
				EORepartEnfant repart = e.nextElement();
				if (repart.lienFiliation() != null && repart.lienFiliation().estAdopte() == false) {
					throw new NSValidation.ValidationException("L'enfant n'est pas un enfant adopté !");
				}
			}
		}

		int nbJours = DateCtrl.nbJoursEntre(dateDebut(),dArriveeFoyer(),true);
		if (nbJours > NB_JOURS_AVANT_ARRIVEE) {
			throw new NSValidation.ValidationException("La date de début du congé d'adoption ne peut précéder de plus de " + NB_JOURS_AVANT_ARRIVEE + " jours la date d'arrivée au foyer");
		}
		nbJours = DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true);
		LogManager.logDetail("CongeAdoption - durée congé adoption "+ nbJours);
		int nbJoursAutorises = nbJoursLegaux(nbEnfantACharge());
		if (!estPartage()) {
			if (nbJours != nbJoursAutorises) {
				throw new NSValidation.ValidationException("La durée légale du congé est de " + nbJoursAutorises + " jours");
			}
		} else {
			if (nbJours < NB_JOURS_MIN_CONGE_PARTAGE) {
				throw new NSValidation.ValidationException("Pour un congé partagé, la durée minimum est de " + NB_JOURS_MIN_CONGE_PARTAGE + " jours");
			} else {
				if (estAdoptionMultiple()) {
					nbJoursAutorises = nbJoursAutorises + NB_JOURS_SUPP_CONGE_PARTAGE_ADOPTION_MULTIPLE;
				} else {
					nbJoursAutorises = nbJoursAutorises + NB_JOURS_SUPP_CONGE_PARTAGE_AUTRE;
				}
				if (nbJours > nbJoursAutorises) {
					throw new NSValidation.ValidationException("Pour un congé partagé, la durée maximum est de " + nbJoursAutorises + " jours");
				} 
			}
		}
	}
	public String validationsCir() {

		if (DateCtrl.isAfterEq(dateDebut(),  DateCtrl.stringToDate(DATE_LIMITE_POUR_DECLARATION_ENFANT)) && enfant() == null) {
			return "Pour un congé postérieur au " + DATE_LIMITE_POUR_DECLARATION_ENFANT + ", vous devez fournir l'enfant concerné";
		}
		return null;

	}
	// méthodes protégées
	protected void init() {
		super.init();
		setTemCgPartage(CocktailConstantes.FAUX);
		setTemAdoptionMultiple(CocktailConstantes.FAUX);
	}

}

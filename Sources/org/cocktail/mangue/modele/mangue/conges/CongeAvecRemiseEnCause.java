/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/** Modelise des conges pouvant etre remis en cause par la saisie de conges anterieurs. Les sous-classes
 * doivent implementer la methode qualifierPeriodeRemiseEnCause()
 *
 */
public abstract class CongeAvecRemiseEnCause extends CongeAvecArreteAnnulation {
	public CongeAvecRemiseEnCause() {
		super();
	}
	
	public String temEnCause() {
		return (String)storedValueForKey("temEnCause");
	 }

	public void setTemEnCause(String value) {
		takeStoredValueForKey(value, "temEnCause");
	}
	public boolean estRemisEnCause() {
		return temEnCause() != null && temEnCause().equals(CocktailConstantes.VRAI);
	}
	public void setEstRemisEnCause(boolean aBool) {
		if (aBool) {
			setTemEnCause(CocktailConstantes.VRAI);
		} else {
			setTemEnCause(CocktailConstantes.FAUX);
		}
	}
	/** retourne la liste des conges de meme type valides pour cet individu qui sont a remettre en cause 
	 * @param nom d'entite des conges recherches
	 * @param date a partir de laquelle rechercher les conges */
	public NSArray congesARemettreEnCause() {
		// un congé est à remettre en cause si il est valide pendant la période passée en paramètre (i.e non annulé) et si il y a un arrêté 
		NSArray args = new NSArray(individu());
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("temValide = 'O' and individu = %@ AND (noArrete <> NIL OR dateArrete <> NIL) AND dAnnulation = NIL AND noArreteAnnulation = NIL and congeDeRemplacement = NIL",args));
		EOQualifier qualifier = qualifierPeriodeRemiseEnCause();
		if (qualifier != null) {
			qualifiers.addObject(qualifier);
		}
		qualifier = new EOAndQualifier(qualifiers);
		EOFetchSpecification fs = new EOFetchSpecification(entityName(),qualifier,null);
		return editingContext().objectsWithFetchSpecification(fs);
	}
	// méthodes protégées
	protected void init() {
		super.init();
		setTemEnCause(CocktailConstantes.FAUX);
	}
	/** qualifier de date a fournir pour qualifier une periode de remise en cause */
	protected abstract EOQualifier qualifierPeriodeRemiseEnCause() ;
	
}

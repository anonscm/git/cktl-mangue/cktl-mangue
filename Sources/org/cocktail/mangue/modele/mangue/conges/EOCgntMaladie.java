// EOCgntMaladie.java
// Created on Wed Feb 08 17:27:59 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.EOParamAncienneteMaladie;

import com.webobjects.foundation.NSValidation;

/** Classe metier pour les conges de maladie des contractuels<BR>
 * 1. Le conge de maladie non titulaire ne concerne que les agents contractuels ou les contractuels assimiles qui ont une carriere<BR>
 * 2. Le conge de maladie non titulaire doit etre recouvert entierement par un ou des contrats avenants ou une carriere d’assimile.<BR>
 */
public class EOCgntMaladie extends CongeMaladie {
	
	public static final String ENTITY_NAME = "CgntMaladie";
	
    public EOCgntMaladie() {
        super();
    }
 
	/** retourne le nombre de jours comptables a plein traitement sur la periode glissante */
	public int nbJoursComptablesPleinTraitement() {
		return nbJoursPleinTraitement();
	}
		
    public String temDureeContinue() {
        return (String)storedValueForKey("temDureeContinue");
    }

    public void setTemDureeContinue(String value) {
        takeStoredValueForKey(value, "temDureeContinue");
    }
    
    public EOParamAncienneteMaladie anciennete() {
        return (EOParamAncienneteMaladie)storedValueForKey("anciennete");
    }

    public void setAnciennete(EOParamAncienneteMaladie value) {
        takeStoredValueForKey(value, "anciennete");
    }
    public Boolean peutBeneficierTraitement() {
    	return new Boolean(anciennete() != null && (anciennete().dureePleinTrait().intValue() > 0 || anciennete().dureeDemiTrait().intValue() > 0));
    }
    // méthodes ajoutées
    public String typeEvenement() {
    		return EOTypeAbsence.TYPE_CONGE_MALADIE_NT;
    }
    /** utilise pour la communication avec GRhum */
    public String parametreOccupation() {
    		return "CL_MO";
    }
    public boolean estContratContinu() {
		return temDureeContinue() != null && temDureeContinue().equals(CocktailConstantes.VRAI);
	}
	public void setEstContratContinu(boolean aBool) {
		if (aBool) {
			setTemDureeContinue(CocktailConstantes.VRAI);
		} else {
			setTemDureeContinue(CocktailConstantes.FAUX);
		}
	}	
	
	public void supprimerRelations() {
		super.supprimerRelations();
		removeObjectFromBothSidesOfRelationshipWithKey(anciennete(),"anciennete");
	}	
	public void validateForSave() {
		super.validateForSave();
		if (anciennete() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir l'ancienneté");
		}
		if (!individu().estContractuelSurPeriodeComplete(dateDebut(),dateFin())) {
			throw new NSValidation.ValidationException("L'agent doit avoir un contrat de travail ou une carrière de non fonctionnaire en activité ou détaché dans l'établissement pendant tout le congé");
		}
	}
	public int jourDebutDemiTraitement() {
		if (anciennete() == null) {
			return 0;
		} else {
			return (anciennete().dureePleinTrait().intValue() * 30) + 1;
		}
	}
	public String jourDebutDemiTraitementImprimable() {
		return new Integer(jourDebutDemiTraitement()).toString();
	}
	// méthodes protégées
	protected void init() {
		super.init();
		setTemDureeContinue(CocktailConstantes.FAUX);
	}
	
}

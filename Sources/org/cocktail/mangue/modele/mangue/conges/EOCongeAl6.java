// EOCongeAl6.java
// Created on Wed Feb 22 17:48:21 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

/** Regles de validation :<BR>
 * La duree de conges consecutifs ne peut exceder 24 mois apres avis du comite medical.<BR>
 * Chaque periode de conge ne peut exceder 6 mois pendant ces 24 mois<BR>
 * 1. L'agent beneficie d'un conge d'une dure maximum de 12 mois.<BR>
 * 2. L’agent beneficie  d'une prolongation de conge de 24 mois apres avis du comite medical.<BR>>
 * 3. L'agent beneficie pendant les douze premiers mois du maintien de sa remuneration.<BR>
 * 4. L'agent beneficie pendant les 24 mois suivants du maintien des deux tiers (66%) de sa remuneration.<BR>
*/
// 07/11/2010 - 
public class EOCongeAl6 extends CongeHospitaloUniversitaire {
	/** duree de la premiere periode */
	public static int NB_MOIS_1ERE_PERIODE = 12;
	/** nombre de mois remunere a 100% du traitement */
	public static int NB_MOIS_PLEIN_TRAITEMENT = 12;
	/** nombre de mois remunere au deux-tiers du traitement */
	public static int NB_MOIS_66 = 24;
	/** nombre de mois sans traitement */
	private static int DUREE_MAXI_CONGE = 6;		// 6 mois
	private static int[] MOIS_REMUNERATION = {NB_MOIS_PLEIN_TRAITEMENT,NB_MOIS_66};
	private static int[] TAUX_REMUNERATION = {100,66};
	
    public EOCongeAl6() {
        super();
    }
    // méthodes ajoutées
   public String typeEvenement() {
		return "AHCUAO6";
	}
	/** utilise pour la communication avec GRhum */
	public String parametreOccupation() {
			return "CL_HU6";
	}
	public void validateForSave() {
		super.validateForSave();
		if (DateCtrl.calculerDureeEnMois(dateDebut(),dateFin(),true).intValue() > DUREE_MAXI_CONGE) {
			throw new NSValidation.ValidationException("La durée de ce congé ne doit pas excéder " + DUREE_MAXI_CONGE + " mois");
		}
		// vérifier si la durée du congé est supérieure à la durée maximale
		int nbMoisMaxi = dureeMaximumConge();
		int dureeTotale = dureeEnMoisCongesContinusAssocies();
		if (dureeTotale > nbMoisMaxi) {
			throw new NSValidation.ValidationException("Le congé hospitalo-universitaire Alinéa 6 ne peut dépasser " + nbMoisMaxi + " mois");
		} else if (dureeTotale > NB_MOIS_1ERE_PERIODE && dComMed() == null) {
			throw new NSValidation.ValidationException("Au-delà de " + NB_MOIS_1ERE_PERIODE + " mois, vous devez fournir la date d'avis du comité médical");
		}
	}
	public boolean dateAvisObligatoire() {
		return false;
	}
	/** la duree maximum d'un conge pendant la seconde periode est limitee a 6 mois */
	public int dureeMaxiConge() {
		// 07/11/2010 - la durée maximum d'un congé pendant la seconde période est limitée à 6 mois
		int dureeCongesPrecedents = dureeEnMoisCongesContinusAssocies();
		if (dureeCongesPrecedents < NB_MOIS_1ERE_PERIODE) {
			return 0;
		} else {
			return DUREE_MAXI_CONGE;
		}
	}
	public int dureeLegale() {
		return 0;
	}
	public int[] tableMoisRemuneration() {
		return MOIS_REMUNERATION; 
	}
	public int[] tableTauxRemuneration() {
		return TAUX_REMUNERATION;
	}
	public static NSDictionary dureeEnMoisPourTaux() {
		NSMutableDictionary dict = new NSMutableDictionary();
		for (int i = 0;i < MOIS_REMUNERATION.length;i++) {
			dict.setObjectForKey(new Integer(MOIS_REMUNERATION[i]),new Integer(TAUX_REMUNERATION[i]));
		}
		return dict;
	}
}

// EOCongePaternite.java
// Created on Thu Mar 13 10:48:03  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation :<BR>
 * La date de naissance, la date debut et la date fin doivent etre fournies<BR>
 * Le conge de paternite ne s'applique qu'aux hommes<BR>
 * Verifie qu'il n'y a pas de conge paternite ou de conge d'adoption defini pour cette periode<BR>
 * Verification de dates<BR>
1. Le conge de paternite doit etre pris apres la naissance de l'enfant<BR>
2. Le conge de paternite doit etre pris dans un delai de 4 mois<BR>
3. Le conge est de 11 jours pour une naissance simple, 18 pour une naissance multiple et il est non fractionnable<BR>
4. Le conge doit commencer apres le 01/01/02<BR>
5. La case Conge sans traitement ne concerne que les agents non titulaires, les fonctionnaires perçoivent un plein traitement.<BR>
6. Le conge doit reposer soit sur un contrat, soit sur une position d'activite ou de detachement avec gestion de la carri&egrave;re d'accueil 
dans l'etablissement.<BR>
 *
 */
public class EOCongePaternite extends _EOCongePaternite {
	
	private static int LIMITE_MAX_MOIS = 12;
	public static int NB_JOURS_SIMPLE = 11;
	public static int NB_JOURS_MULTIPLE = 18;
	
    public EOCongePaternite() {
        super();
    }

    public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
		if (dateFin() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date de fin !");
		}
		if (dNaisPrev() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la date d'accouchement prévisionnel");
		}
		if (DateCtrl.isBefore(dNaisPrev(),DateCtrl.stringToDate("01/01/2002"))) {
			throw new NSValidation.ValidationException("Les congés de paternité n'existaient pas à cette date");
		}
//		if (DateCtrl.isBefore(dateDebut(),dNaisPrev())) {
//			throw new NSValidation.ValidationException("Un congé de paternité ne peut pas commencer avant la date de naissance prévisionnelle !!");
//		}
//		if (DateCtrl.isBefore(dateFin(),dNaisPrev())) {
//			throw new NSValidation.ValidationException("Un congé de paternité ne peut pas se terminer avant la date de naissance prévisionnelle !!");
//		}
		int nbJours = DateCtrl.nbJoursEntre(dateDebut(),dateFin(),true);
		if (estGrossesseMultiple()) {
			if (nbJours > NB_JOURS_MULTIPLE) {
			throw new NSValidation.ValidationException("En cas de grossesse multiple, le congé de paternité est au maximum de "+ NB_JOURS_MULTIPLE + " jours");
			}
		} else if (nbJours > NB_JOURS_SIMPLE) {
			throw new NSValidation.ValidationException("En cas de grossesse simple, le congé de paternité est au maximum de "+ NB_JOURS_SIMPLE + " jours");
		}
		NSTimestamp limiteMax = DateCtrl.dateAvecAjoutMois(dNaisPrev(),LIMITE_MAX_MOIS);
		if (DateCtrl.isAfter(dateDebut(),limiteMax)) {
			throw new NSValidation.ValidationException("Le congé de paternité doit se prendre dans les "+ LIMITE_MAX_MOIS + " mois suivant la naissance");
		}
    }
    public String typeEvenement() {
    		return EOTypeAbsence.TYPE_CONGE_PATERNITE;
    }
    /** utilise pour la communication avec GRhum */
    public String parametreOccupation() {
    		return "CL_CDP";
    }
    public String dateNaissPrevFormatee() {
		return SuperFinder.dateFormatee(this,D_NAIS_PREV_KEY);
	}
	public void setDateNaissPrevFormatee(String uneDate) {
		if (uneDate == null) {
			setDNaisPrev(null);
		} else {
			SuperFinder.setDateFormatee(this, D_NAIS_PREV_KEY, uneDate);
		}
	}
	public String dateDemandeFormatee() {
		return SuperFinder.dateFormatee(this, D_DEMANDE_KEY);
	}
	public void setDateDemandeFormatee(String uneDate) {
		SuperFinder.setDateFormatee(this, D_DEMANDE_KEY, uneDate);
	}
	public boolean estGrossesseMultiple() {
		return temGrossesseMultiple().equals(CocktailConstantes.VRAI);
	}
	public void setEstGrossesseMultiple(boolean aBool) {
		if (aBool) {
			setTemGrossesseMultiple(CocktailConstantes.VRAI);
		} else {
			setTemGrossesseMultiple(CocktailConstantes.FAUX);	
		}
	}
	
	protected void init() {
		super.init();
		setTemGrossesseMultiple(CocktailConstantes.FAUX);
    }
}

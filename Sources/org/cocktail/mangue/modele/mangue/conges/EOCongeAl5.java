// EOCongeAl5.java
// Created on Wed Feb 22 17:48:08 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

/** Regles de validation :<BR>
 * La date d'avis du comite medical doit etre fournie.<BR>
 * La duree de conges consecutifs ne peut exceder 30 mois apres avis du comite medical.<BR>
 * Chaque periode de conge ne peut exceder 6 mois<BR>
 * 1. L'agent beneficie pendant les six premiers mois du maintien des deux tiers (66%) de sa remuneration.<BR>
 * 2. L'agent beneficie pendant les 24 mois suivants du tiers de sa remuneration.<BR>
*/
public class EOCongeAl5 extends CongeHospitaloUniversitaire {
	/** nombre de mois remunere au deux-tiers du traitement */
	public static int NB_MOIS_66 = 6;
	/** nombre de mois remunere a 33% deux-tiers du traitement */
	public static int NB_MOIS_33 = 24;
	private static int DUREE_MAXI_CONGE = 6;		// 6 mois
	private static int[] MOIS_REMUNERATION = {NB_MOIS_66,NB_MOIS_33};
	private static int[] TAUX_REMUNERATION = {66,33};
	
    public EOCongeAl5() {
        super();
    }
    public String typeEvenement() {
		return "AHCUAO5";
	}
	/** utilisepour la communication avec GRhum */
	public String parametreOccupation() {
			return "CL_HU5";
	}
    public void validateForSave() {
		super.validateForSave();
		if (DateCtrl.calculerDureeEnMois(dateDebut(),dateFin(),true).intValue() > DUREE_MAXI_CONGE) {
			throw new NSValidation.ValidationException("La durée de ce congé ne doit pas excéder " + DUREE_MAXI_CONGE + " mois");
		}
		if (dComMed() == null) {
			throw new NSValidation.ValidationException("La date d'avis du comité médical doit être fournie");
		}
		// vérifier si la durée du congé est supérieure à la durée maximale
		int nbMoisMaxi = dureeMaximumConge();
		int dureeTotale = dureeEnMoisCongesContinusAssocies();
		if (dureeTotale > nbMoisMaxi) {
			throw new NSValidation.ValidationException("Le congé hospitalo-universitaire Alinéa 5 ne peut dépasser " + nbMoisMaxi + " mois");
		}
	}
    public boolean dateAvisObligatoire() {
		return true;
	}
    public int dureeMaxiConge() {
		return DUREE_MAXI_CONGE;
	}
	public int dureeLegale() {
		return 0;
	}
	public int[] tableMoisRemuneration() {
		return MOIS_REMUNERATION; 
	}
	public int[] tableTauxRemuneration() {
		return TAUX_REMUNERATION;
	}
	//	 méthodes statiques
	public static NSDictionary dureeEnMoisPourTaux() {
		NSMutableDictionary dict = new NSMutableDictionary();
		for (int i = 0;i < MOIS_REMUNERATION.length;i++) {
			dict.setObjectForKey(new Integer(MOIS_REMUNERATION[i]),new Integer(TAUX_REMUNERATION[i]));
		}
		return dict;
	}
}

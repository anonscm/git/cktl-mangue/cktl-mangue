// _EOCld.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCld.java instead.
package org.cocktail.mangue.modele.mangue.conges;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOCld extends  CongeAvecDetails {

	private static final long serialVersionUID = -3258666968396815005L;
	
	public static final String ENTITY_NAME = "Cld";
	public static final String ENTITY_TABLE_NAME = "CLD";

// Attributes
	public static final String ENTITY_PRIMARY_KEY = "congeOrdre";

	public static final String D_COM_MED_CLD_KEY = "dComMedCld";
	public static final String TEM_PROLONG_CLD_KEY = "temProlongCld";

// Attributs non visibles
	public static final String ABS_NUMERO_KEY = "absNumero";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String CONGE_ORDRE_KEY = "congeOrdre";
	public static final String TEM_CONFIRME_KEY = "temConfirme";
	public static final String TEM_EN_CAUSE_KEY = "temEnCause";
	public static final String TEM_GEST_ETAB_KEY = "temGestEtab";

//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String DATE_ARRETE_COLKEY = "D_ARRETE_CLD";
	public static final String DATE_DEBUT_COLKEY = "D_DEB_CLD";
	public static final String DATE_FIN_COLKEY = "D_FIN_CLD";
	public static final String D_COM_MED_CLD_COLKEY = "D_COM_MED_CLD";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String NO_ARRETE_COLKEY = "NO_ARRETE_CLD";
	public static final String TEM_PROLONG_CLD_COLKEY = "TEM_PROLONG_CLD";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";
	public static final String TEM_CONFIRME_COLKEY = "TEM_CONFIRME";
	public static final String TEM_EN_CAUSE_COLKEY = "TEM_EN_CAUSE";
	public static final String TEM_GEST_ETAB_COLKEY = "TEM_GEST_ETAB";

	public static final String ABS_NUMERO_COLKEY = "ABS_NUMERO";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String CONGE_ORDRE_COLKEY = "NO_SEQ_ARRETE";


	// Relationships
	public static final String DETAILS_KEY = "details";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dComMedCld() {
    return (NSTimestamp) storedValueForKey(D_COM_MED_CLD_KEY);
  }

  public void setDComMedCld(NSTimestamp value) {
    takeStoredValueForKey(value, D_COM_MED_CLD_KEY);
  }

  public String temProlongCld() {
    return (String) storedValueForKey(TEM_PROLONG_CLD_KEY);
  }

  public void setTemProlongCld(String value) {
    takeStoredValueForKey(value, TEM_PROLONG_CLD_KEY);
  }
  
  
  public String temConfirme() {
	    return (String) storedValueForKey(TEM_CONFIRME_KEY);
	  }

	  public void setTemConfirme(String value) {
	    takeStoredValueForKey(value, TEM_CONFIRME_KEY);
	  }

	  public String temEnCause() {
	    return (String) storedValueForKey(TEM_EN_CAUSE_KEY);
	  }

	  public void setTemEnCause(String value) {
	    takeStoredValueForKey(value, TEM_EN_CAUSE_KEY);
	  }

	  public String temGestEtab() {
	    return (String) storedValueForKey(TEM_GEST_ETAB_KEY);
	  }

	  public void setTemGestEtab(String value) {
	    takeStoredValueForKey(value, TEM_GEST_ETAB_KEY);
	  }

  
  public NSArray details() {
    return (NSArray)storedValueForKey(DETAILS_KEY);
  }

  public NSArray details(EOQualifier qualifier) {
    return details(qualifier, null, false);
  }

  public NSArray details(EOQualifier qualifier, boolean fetch) {
    return details(qualifier, null, fetch);
  }

  public NSArray details(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.conges.DetailConge.CONGE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.conges.DetailConge.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = details();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDetailsRelationship(org.cocktail.mangue.modele.mangue.conges.DetailConge object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DETAILS_KEY);
  }

  public void removeFromDetailsRelationship(org.cocktail.mangue.modele.mangue.conges.DetailConge object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DETAILS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.conges.DetailConge createDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("DetailConge");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DETAILS_KEY);
    return (org.cocktail.mangue.modele.mangue.conges.DetailConge) eo;
  }

  public void deleteDetailsRelationship(org.cocktail.mangue.modele.mangue.conges.DetailConge object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDetailsRelationships() {
    Enumeration objects = details().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDetailsRelationship((org.cocktail.mangue.modele.mangue.conges.DetailConge)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCld avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCld createEOCld(EOEditingContext editingContext, NSTimestamp dateDebut
, NSTimestamp dCreation
, NSTimestamp dModification
, String temProlongCld
, String temValide
, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu			) {
    EOCld eo = (EOCld) createAndInsertInstance(editingContext, _EOCld.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemProlongCld(temProlongCld);
		eo.setTemValide(temValide);
    eo.setIndividuRelationship(individu);
    return eo;
  }

  
	  public EOCld localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCld)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCld creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCld creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCld object = (EOCld)createAndInsertInstance(editingContext, _EOCld.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCld localInstanceIn(EOEditingContext editingContext, EOCld eo) {
    EOCld localInstance = (eo == null) ? null : (EOCld)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCld#localInstanceIn a la place.
   */
	public static EOCld localInstanceOf(EOEditingContext editingContext, EOCld eo) {
		return EOCld.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCld fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCld fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCld eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCld)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCld fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCld fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCld eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCld)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCld fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCld eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCld ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCld fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

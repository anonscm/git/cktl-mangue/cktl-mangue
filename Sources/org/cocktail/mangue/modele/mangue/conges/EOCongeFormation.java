// EOCongeFormation.java
// Created on Mon Oct 11 15:26:23  2004 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue.conges;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.DateCtrl.IntRef;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Regles de validation<BR>
1. Pendant tout le conge, l'agent doit etre dans un corps ouvrant droit au conge de formation<BR>
2. Le conge doit reposer sur une position d'activite ou de detachement avec gestion de la 
	carriere d'accueil dans l'etablissement.<BR>
3. La date de debut de perception de l'indemnite doit etre posterieure ou egale a la date de debut du 
	conge formation.<BR>
4. La date de fin de perception de l'indemnite doit etre posterieure ou egale à la date de de début de perception de l'indemnite
5. La date de fin de perception de l'indemnite doit etre anterieure ou egale a la date de fin du conge formation.<BR>
6. Le nombre de jours de conge formation doit etre un nombre entier compris entre 0 et 31.<BR>
7. La duree en mois jours ne peut pas etre plus grande que le conge en date de debut et fin.
   Si cette duree est inferieure, on parle de conge de formation fractionn&eacute;. 
   Par ex. conge de formation du 01/01/2006 au 30/06/2006 avec comme duree 3 mois (l'agent est en conge tous les matins et travaille les apm).<BR>
8. L'agent a droit a 36 mois de conge de formation sur toute sa carriere.<BR>
9. La duree minimum d'un conge de formation est d'un mois.<BR>
10. Pendant un conge de formation non fractionne, l'agent ne doit pas occuper un emploi.<BR>
11. Pendant un conge de formation fractionne, l'agent occupe un emploi a moins de 100%.<BR>
12. Pendant un conge de formation, l'agent ne peut pas benéficier d'une bonification indiciaire.<BR>

 */
public class EOCongeFormation extends _EOCongeFormation {

    public EOCongeFormation() {
        super();
    }


   public String typeEvenement() {
    		return EOTypeAbsence.TYPE_CFP;
    }
    /** utilise pour la communication avec GRhum */
    public String parametreOccupation() {
    		return "CL_CDFP";
    } 
    
    
	public boolean estValide() {
		return temValide().equals(CocktailConstantes.VRAI);
	}

    public boolean estFractionne() {
    		return temFractionne() != null && temFractionne().equals(CocktailConstantes.VRAI);
    }
    public void setEstFractionne(boolean aBool) {
    		if (aBool) {
    			setTemFractionne(CocktailConstantes.VRAI);
    		} else {
    			setTemFractionne(CocktailConstantes.FAUX);
    		}
    }
    public String dateDebutIndemnite() {
		return SuperFinder.dateFormatee(this,D_DEB_INDEMNITE_CGF_KEY);
	}
	public void setDateDebutIndemnite(String uneDate) {
		if (uneDate == null) {
			setDateFin(null);
		} else {
			SuperFinder.setDateFormatee(this,D_DEB_INDEMNITE_CGF_KEY,uneDate);
		}
	}
	  public String dateFinIndemnite() {
		return SuperFinder.dateFormatee(this,D_FIN_INDEMNITE_CGF_KEY);
	}
	public void setDateFinIndemnite(String uneDate) {
		if (uneDate == null) {
			setDateFin(null);
		} else {
			SuperFinder.setDateFormatee(this,D_FIN_INDEMNITE_CGF_KEY,uneDate);
		}
	}
	
	/**
	 * 
	 */
    public void validateForSave() {
    	
    		super.validateForSave();
    		if (individu().peutBeneficierCongeFormation(dateDebut(),dateFin()) == false) {
    			throw new NSValidation.ValidationException("CONGE FORMATION\nL'agent pendant ces dates n'est pas dans un corps ouvrant droit au congé formation");
    		}
    		if (EOChangementPosition.individuEnActivitePendantPeriode(editingContext(),individu(),dateDebut(),dateFin()) == false) {
    	 		throw new NSValidation.ValidationException("CONGE FORMATION\nLe congé de formation doit reposer sur une position d'activité ou de détachement avec gestion de la carrière d'accueil dans l'établissement");
    	 	}
    		if (dDebIndemniteCgf() != null) {
    			if (DateCtrl.isBefore(dDebIndemniteCgf(),dateDebut())) {
    				throw new NSValidation.ValidationException("CONGE FORMATION\nLa date de début de perception de l'indemnité doit être postérieure ou égale à la date de début du congé formation");
    			}
    			if (dFinIndemniteCgf() == null) {
    				throw new NSValidation.ValidationException("CONGE FORMATION\nVous avez fourni la date de début d'indemnité, vous devez fournir la date de fin d'indemnité");
    			} else {
    				if (DateCtrl.isBefore(dFinIndemniteCgf(),dDebIndemniteCgf())) {
    					throw new NSValidation.ValidationException("CONGE FORMATION\nLa date de fin de perception de l'indemnité doit être postérieure ou égale à la date de de début de perception de l'indemnité");
    				}
    				if (DateCtrl.isAfter(dFinIndemniteCgf(),dateFin())) {
    					throw new NSValidation.ValidationException("CONGE FORMATION\nLa date de fin de perception de l'indemnité doit être antérieure ou égale à la date de fin du congé formation");
    				}
    			}
    		}
    		if (dureeJjCgf() == null) {
    			throw new NSValidation.ValidationException("CONGE FORMATION\nVous devez fournir au minimum la durée en jours du congé formation");
    		} else {
    			verifierDurees();
    		}
    		
    		NSArray<EOOccupation> occupations = EOOccupation.rechercherOccupationsPourIndividuEtPeriode(editingContext(),individu(),dateDebut(),dateFin());
    		if (estFractionne() == false) {
    			if (occupations.count() > 0) {
    				throw new NSValidation.ValidationException("CONGE FORMATION\nPendant un congé de formation non fractionné, l'agent ne doit pas occuper un emploi");
    			}
    		} else {
    			for (EOOccupation occupation : occupations) {
    				if (occupation.quotite() != null && occupation.quotite().floatValue() == 100.0) {
    					throw new NSValidation.ValidationException("CONGE FORMATION\nPendant un congé de formation fractionné, l'agent doit occuper un emploi à moins de 100%");
    				}
    			}
    		}
    }
    
    
    /** retourne la date de fin calculee en fonction de la duree */
    public NSTimestamp dateFinPourDuree() {
    		NSTimestamp dateFinCalculee = dateDebut();
		if (dureeMmCgf() != null) {
			dateFinCalculee = DateCtrl.dateAvecAjoutMois(dateFinCalculee,dureeMmCgf().intValue());
		}
		dateFinCalculee = DateCtrl.dateAvecAjoutJours(dateFinCalculee,dureeJjCgf().intValue() - 1);	// -1 pour que la borne de fin soit inclus
		return dateFinCalculee;
    }
    
    /** calcule la duree des autres conges formation de l'annee. Prend en compte les anciens conges dont les
     * champs dureeJjCfp et dureeMmCfp sont nuls
     * @return un dictionnaire contenant le nombre de mois (cle : mois) et le nombre de jours (cle : jours)
     */
    public NSDictionary dureeEnJoursAutresCongesFormation() {
		int nbMois = 0, nbJours = 0;
   		for (java.util.Enumeration<EOCongeFormation> e = Conge.rechercherDureesPourIndividu(editingContext(),entityName(),individu()).objectEnumerator();e.hasMoreElements();) {
			EOCongeFormation conge = e.nextElement();
			if (conge != this) {
				if (conge.dureeMmCgf() == null && conge.dureeJjCgf() == null) {
					IntRef anneeRef = new IntRef(), moisRef = new IntRef(), jourRef = new IntRef();
					DateCtrl.joursMoisAnneesEntre(conge.dateDebut(),conge.dateFin(),anneeRef,moisRef,jourRef,true);	// inclure les bornes
					nbMois += anneeRef.value * 12 + moisRef.value;
					nbJours += jourRef.value;
				} else {
					if (conge.dureeMmCgf() != null) {
						nbMois += conge.dureeMmCgf().intValue();
					}
					nbJours += conge.dureeJjCgf().intValue();
				}
			}
		}
		if (nbJours > 30) {
			// on ramène à des mois de trente jours
			nbMois += nbJours / DateCtrl.NB_JOURS_COMPTABLES_MENSUELS;
			nbJours = nbJours % DateCtrl.NB_JOURS_COMPTABLES_MENSUELS;
		}
    		NSMutableDictionary dict = new NSMutableDictionary();
    		dict.setObjectForKey(new Integer(nbMois),"mois");
    		dict.setObjectForKey(new Integer(nbJours),"jours");
    		return dict;
    }

    /**
     * 
     */
	protected void init() {
		setTemFractionne(CocktailConstantes.FAUX);
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void verifierDurees() throws NSValidation.ValidationException {
		int nbJours = dureeJjCgf().intValue();
		if (nbJours < 0 || nbJours > 31) {
			throw new NSValidation.ValidationException("CONGE FORMATION\nLe nombre de jours de congé formation doit être un nombre entier compris entre 0 et 31");
		}
		NSTimestamp nouvelleDate = dateFinPourDuree();

		if (DateCtrl.isAfter(nouvelleDate,dateFin())) {
			throw new NSValidation.ValidationException("CONGE FORMATION\nLa durée en mois jours ne peut pas être plus grande que le congé en date de début et fin");
		}
		if (DateCtrl.isBefore(nouvelleDate,dateFin()) && estFractionne() == false) {
			throw new NSValidation.ValidationException("CONGE FORMATION\nLa durée en mois et jours du congé est inférieure à la durée du congé, celui-ci devrait être fractionné");
		}
		int nbMois = 0;
		if (dureeMmCgf() != null) {
			nbMois = dureeMmCgf().intValue();
		}
		
		int nbMoisMini = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CFP_DUREE_MIN);
		if (nbMois < (nbMoisMini - 1) || (nbMois == nbMoisMini - 1 && nbJours < 31)) {
			throw new NSValidation.ValidationException("CONGE FORMATION\nLa durée minimum d'un congé de formation est "+ nbMoisMini + " mois");
		}
		
		int nbMoisMaxi = EOGrhumParametres.getValueParamInteger(ManGUEConstantes.ABS_PARAM_KEY_CFP_DUREE_MAX) * 12;
		if (nbMois > nbMoisMaxi || (nbMois == nbMoisMaxi && nbJours > 0)) {
			throw new NSValidation.ValidationException("CONGE FORMATION\nL'agent a droit à " + nbMoisMaxi + " mois de congé de formation sur toute sa carrière");
		}
		NSDictionary dict = dureeEnJoursAutresCongesFormation();
		nbMois += ((Integer)dict.objectForKey("mois")).intValue();
		nbJours += ((Integer)dict.objectForKey("jours")).intValue();
		if (nbMois > nbMoisMaxi || (nbMois == nbMoisMaxi && nbJours > 0)) {
			throw new NSValidation.ValidationException("CONGE FORMATION\nL'agent a droit à " + nbMoisMaxi + " mois de congé de formation sur toute sa carrière");
		}
	
	}
}

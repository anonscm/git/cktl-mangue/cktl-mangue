package org.cocktail.mangue.modele.mangue.conges;
//Created on Sun Feb 23 18:41:29  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/** Cong&eacute; de Naissance : classe abstraite pour les cong&eacute;s de maternit&eacute; paternit&eacute;, adoption
 * qui sont sans traitement pour les contractuels dont la date d'embauche est inf&eacute;rieure &agrave; 6 mois
 * @author christine
 *
 */
public abstract class CongeNaissance extends CongeAvecArreteAnnulation {

	public String temCgSansTrait() {
		return (String)storedValueForKey("temCgSansTrait");
	}

	public void setTemCgSansTrait(String value) {
		takeStoredValueForKey(value, "temCgSansTrait");
	}
	public boolean estCongeSansTraitement() {
		return temCgSansTrait() != null && temCgSansTrait().equals(CocktailConstantes.VRAI);
	}
	public void setEstCongeSansTraitement(boolean aBool) {
		if (aBool) {
			setTemCgSansTrait(CocktailConstantes.VRAI);
		} else {
			setTemCgSansTrait(CocktailConstantes.FAUX);	
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
		// Le congé sans traitement n'est valable que pour les contractuels => il ne doit pas y avoir de segment de carrière
		NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),individu(),dateDebut(),dateFin());
		if (carrieres.count() > 0 && estCongeSansTraitement()) {
			throw new NSValidation.ValidationException("Le congé sans traitement ne s'applique qu'aux contractuels");
		}
	}
	// méthodes protégées
	protected void init() {
		super.init();
		setTemCgSansTrait(CocktailConstantes.FAUX);
	}
}

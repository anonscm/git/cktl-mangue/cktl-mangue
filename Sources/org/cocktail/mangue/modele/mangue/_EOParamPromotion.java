// _EOParamPromotion.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOParamPromotion.java instead.
package org.cocktail.mangue.modele.mangue;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOParamPromotion extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "ParamPromotion";
	public static final String ENTITY_TABLE_NAME = "PARAM_PROMOTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "parpOrdre";

	public static final String C_CATEGORIE_SERV_EFFECTIFS_KEY = "cCategorieServEffectifs";
	public static final String C_CATEGORIE_SERV_PUBLICS_KEY = "cCategorieServPublics";
	public static final String C_CHEVRON_ARRIVEE_KEY = "cChevronArrivee";
	public static final String C_CHEVRON_DEPART_KEY = "cChevronDepart";
	public static final String C_ECHELON_KEY = "cEchelon";
	public static final String C_ECHELON_ARRIVEE_KEY = "cEchelonArrivee";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PARP_CODE_KEY = "parpCode";
	public static final String PARP_D_FERMETURE_KEY = "parpDFermeture";
	public static final String PARP_D_OUVERTURE_KEY = "parpDOuverture";
	public static final String PARP_DUREE_CATEG_SERV_PUBLICS_KEY = "parpDureeCategServPublics";
	public static final String PARP_DUREE_CORPS_KEY = "parpDureeCorps";
	public static final String PARP_DUREE_ECHELON_KEY = "parpDureeEchelon";
	public static final String PARP_DUREE_GRADE_KEY = "parpDureeGrade";
	public static final String PARP_DUREE_MAX_ANCIENNETE_CONS_KEY = "parpDureeMaxAncienneteCons";
	public static final String PARP_DUREE_MOIS_GRADE_KEY = "parpDureeMoisGrade";
	public static final String PARP_DUREE_SERV_EFFECTIFS_KEY = "parpDureeServEffectifs";
	public static final String PARP_DUREE_SERV_PUBLICS_KEY = "parpDureeServPublics";
	public static final String PARP_REF_REGLEMENTAIRE_KEY = "parpRefReglementaire";
	public static final String PARP_TEM_CATEGORIE_KEY = "parpTemCategorie";
	public static final String PARP_TEM_CONDITION_KEY = "parpTemCondition";
	public static final String PARP_TEM_CORPS_KEY = "parpTemCorps";
	public static final String PARP_TEM_ECHELON_KEY = "parpTemEchelon";
	public static final String PARP_TEM_GRADE_KEY = "parpTemGrade";
	public static final String PARP_TEM_SERV_EFFECTIFS_KEY = "parpTemServEffectifs";
	public static final String PARP_TEM_SERV_PUBLICS_KEY = "parpTemServPublics";
	public static final String PARP_TEM_VALIDE_KEY = "parpTemValide";
	public static final String PARP_TYPE_KEY = "parpType";

// Attributs non visibles
	public static final String C_GRADE_ARRIVEE_KEY = "cGradeArrivee";
	public static final String PARP_TYPE_POPU_SERV_PUBLICS_KEY = "parpTypePopuServPublics";
	public static final String C_CORPS_ARRIVEE_KEY = "cCorpsArrivee";
	public static final String PARP_ORDRE_KEY = "parpOrdre";
	public static final String C_TYPE_POPULATION_DEPART_KEY = "cTypePopulationDepart";
	public static final String C_GRADE_DEPART_KEY = "cGradeDepart";
	public static final String C_CORPS_DEPART_KEY = "cCorpsDepart";

//Colonnes dans la base de donnees
	public static final String C_CATEGORIE_SERV_EFFECTIFS_COLKEY = "C_CATEGORIE_SERV_EFFECTIFS";
	public static final String C_CATEGORIE_SERV_PUBLICS_COLKEY = "C_CATEGORIE_SERV_PUBLICS";
	public static final String C_CHEVRON_ARRIVEE_COLKEY = "C_CHEVRON_ARRIVEE";
	public static final String C_CHEVRON_DEPART_COLKEY = "C_CHEVRON_DEPART";
	public static final String C_ECHELON_COLKEY = "C_ECHELON";
	public static final String C_ECHELON_ARRIVEE_COLKEY = "C_ECHELON_ARRIVEE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PARP_CODE_COLKEY = "PARP_CODE";
	public static final String PARP_D_FERMETURE_COLKEY = "PARP_D_FERMETURE";
	public static final String PARP_D_OUVERTURE_COLKEY = "PARP_D_OUVERTURE";
	public static final String PARP_DUREE_CATEG_SERV_PUBLICS_COLKEY = "PARP_DUREE_CATEG_SERV_PUBLICS";
	public static final String PARP_DUREE_CORPS_COLKEY = "PARP_DUREE_CORPS";
	public static final String PARP_DUREE_ECHELON_COLKEY = "PARP_DUREE_ECHELON";
	public static final String PARP_DUREE_GRADE_COLKEY = "PARP_DUREE_GRADE";
	public static final String PARP_DUREE_MAX_ANCIENNETE_CONS_COLKEY = "PARP_DUREE_MAX_ANCIENNETE_CONS";
	public static final String PARP_DUREE_MOIS_GRADE_COLKEY = "PARP_DUREE_MOIS_GRADE";
	public static final String PARP_DUREE_SERV_EFFECTIFS_COLKEY = "PARP_DUREE_SERV_EFFECTIFS";
	public static final String PARP_DUREE_SERV_PUBLICS_COLKEY = "PARP_DUREE_SERV_PUBLICS";
	public static final String PARP_REF_REGLEMENTAIRE_COLKEY = "PARP_REF_REGLEMENTAIRE";
	public static final String PARP_TEM_CATEGORIE_COLKEY = "PARP_TEM_CATEGORIE";
	public static final String PARP_TEM_CONDITION_COLKEY = "PARP_TEM_CONDITION";
	public static final String PARP_TEM_CORPS_COLKEY = "PARP_TEM_CORPS";
	public static final String PARP_TEM_ECHELON_COLKEY = "PARP_TEM_ECHELON";
	public static final String PARP_TEM_GRADE_COLKEY = "PARP_TEM_GRADE";
	public static final String PARP_TEM_SERV_EFFECTIFS_COLKEY = "PARP_TEM_SERV_EFFECTIFS";
	public static final String PARP_TEM_SERV_PUBLICS_COLKEY = "PARP_TEM_SERV_PUBLICS";
	public static final String PARP_TEM_VALIDE_COLKEY = "PARP_TEM_VALIDE";
	public static final String PARP_TYPE_COLKEY = "PARP_TYPE";

	public static final String C_GRADE_ARRIVEE_COLKEY = "C_GRADE_ARRIVEE";
	public static final String PARP_TYPE_POPU_SERV_PUBLICS_COLKEY = "PARP_TYPE_POPU_SERV_PUBLICS";
	public static final String C_CORPS_ARRIVEE_COLKEY = "C_CORPS_ARRIVEE";
	public static final String PARP_ORDRE_COLKEY = "PARP_ORDRE";
	public static final String C_TYPE_POPULATION_DEPART_COLKEY = "C_TYPE_POPULATION_DEPART";
	public static final String C_GRADE_DEPART_COLKEY = "C_GRADE_DEPART";
	public static final String C_CORPS_DEPART_COLKEY = "C_CORPS_DEPART";


	// Relationships
	public static final String CORPS_ARRIVEE_KEY = "corpsArrivee";
	public static final String CORPS_DEPART_KEY = "corpsDepart";
	public static final String GRADE_ARRIVEE_KEY = "gradeArrivee";
	public static final String GRADE_DEPART_KEY = "gradeDepart";
	public static final String TYPE_POPULATION_DEPART_KEY = "typePopulationDepart";
	public static final String TYPE_POPULATION_SERV_PUBLICS_KEY = "typePopulationServPublics";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cCategorieServEffectifs() {
    return (String) storedValueForKey(C_CATEGORIE_SERV_EFFECTIFS_KEY);
  }

  public void setCCategorieServEffectifs(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_SERV_EFFECTIFS_KEY);
  }

  public String cCategorieServPublics() {
    return (String) storedValueForKey(C_CATEGORIE_SERV_PUBLICS_KEY);
  }

  public void setCCategorieServPublics(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_SERV_PUBLICS_KEY);
  }

  public String cChevronArrivee() {
    return (String) storedValueForKey(C_CHEVRON_ARRIVEE_KEY);
  }

  public void setCChevronArrivee(String value) {
    takeStoredValueForKey(value, C_CHEVRON_ARRIVEE_KEY);
  }

  public String cChevronDepart() {
    return (String) storedValueForKey(C_CHEVRON_DEPART_KEY);
  }

  public void setCChevronDepart(String value) {
    takeStoredValueForKey(value, C_CHEVRON_DEPART_KEY);
  }

  public String cEchelon() {
    return (String) storedValueForKey(C_ECHELON_KEY);
  }

  public void setCEchelon(String value) {
    takeStoredValueForKey(value, C_ECHELON_KEY);
  }

  public String cEchelonArrivee() {
    return (String) storedValueForKey(C_ECHELON_ARRIVEE_KEY);
  }

  public void setCEchelonArrivee(String value) {
    takeStoredValueForKey(value, C_ECHELON_ARRIVEE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String parpCode() {
    return (String) storedValueForKey(PARP_CODE_KEY);
  }

  public void setParpCode(String value) {
    takeStoredValueForKey(value, PARP_CODE_KEY);
  }

  public NSTimestamp parpDFermeture() {
    return (NSTimestamp) storedValueForKey(PARP_D_FERMETURE_KEY);
  }

  public void setParpDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, PARP_D_FERMETURE_KEY);
  }

  public NSTimestamp parpDOuverture() {
    return (NSTimestamp) storedValueForKey(PARP_D_OUVERTURE_KEY);
  }

  public void setParpDOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, PARP_D_OUVERTURE_KEY);
  }

  public Integer parpDureeCategServPublics() {
    return (Integer) storedValueForKey(PARP_DUREE_CATEG_SERV_PUBLICS_KEY);
  }

  public void setParpDureeCategServPublics(Integer value) {
    takeStoredValueForKey(value, PARP_DUREE_CATEG_SERV_PUBLICS_KEY);
  }

  public Integer parpDureeCorps() {
    return (Integer) storedValueForKey(PARP_DUREE_CORPS_KEY);
  }

  public void setParpDureeCorps(Integer value) {
    takeStoredValueForKey(value, PARP_DUREE_CORPS_KEY);
  }

  public Integer parpDureeEchelon() {
    return (Integer) storedValueForKey(PARP_DUREE_ECHELON_KEY);
  }

  public void setParpDureeEchelon(Integer value) {
    takeStoredValueForKey(value, PARP_DUREE_ECHELON_KEY);
  }

  public Integer parpDureeGrade() {
    return (Integer) storedValueForKey(PARP_DUREE_GRADE_KEY);
  }

  public void setParpDureeGrade(Integer value) {
    takeStoredValueForKey(value, PARP_DUREE_GRADE_KEY);
  }

  public Integer parpDureeMaxAncienneteCons() {
    return (Integer) storedValueForKey(PARP_DUREE_MAX_ANCIENNETE_CONS_KEY);
  }

  public void setParpDureeMaxAncienneteCons(Long value) {
    takeStoredValueForKey(value, PARP_DUREE_MAX_ANCIENNETE_CONS_KEY);
  }

  public Integer parpDureeMoisGrade() {
    return (Integer) storedValueForKey(PARP_DUREE_MOIS_GRADE_KEY);
  }

  public void setParpDureeMoisGrade(Long value) {
    takeStoredValueForKey(value, PARP_DUREE_MOIS_GRADE_KEY);
  }

  public Integer parpDureeServEffectifs() {
    return (Integer) storedValueForKey(PARP_DUREE_SERV_EFFECTIFS_KEY);
  }

  public void setParpDureeServEffectifs(Integer value) {
    takeStoredValueForKey(value, PARP_DUREE_SERV_EFFECTIFS_KEY);
  }

  public Integer parpDureeServPublics() {
    return (Integer) storedValueForKey(PARP_DUREE_SERV_PUBLICS_KEY);
  }

  public void setParpDureeServPublics(Integer value) {
    takeStoredValueForKey(value, PARP_DUREE_SERV_PUBLICS_KEY);
  }

  public String parpRefReglementaire() {
    return (String) storedValueForKey(PARP_REF_REGLEMENTAIRE_KEY);
  }

  public void setParpRefReglementaire(String value) {
    takeStoredValueForKey(value, PARP_REF_REGLEMENTAIRE_KEY);
  }

  public String parpTemCategorie() {
    return (String) storedValueForKey(PARP_TEM_CATEGORIE_KEY);
  }

  public void setParpTemCategorie(String value) {
    takeStoredValueForKey(value, PARP_TEM_CATEGORIE_KEY);
  }

  public String parpTemCondition() {
    return (String) storedValueForKey(PARP_TEM_CONDITION_KEY);
  }

  public void setParpTemCondition(String value) {
    takeStoredValueForKey(value, PARP_TEM_CONDITION_KEY);
  }

  public String parpTemCorps() {
    return (String) storedValueForKey(PARP_TEM_CORPS_KEY);
  }

  public void setParpTemCorps(String value) {
    takeStoredValueForKey(value, PARP_TEM_CORPS_KEY);
  }

  public String parpTemEchelon() {
    return (String) storedValueForKey(PARP_TEM_ECHELON_KEY);
  }

  public void setParpTemEchelon(String value) {
    takeStoredValueForKey(value, PARP_TEM_ECHELON_KEY);
  }

  public String parpTemGrade() {
    return (String) storedValueForKey(PARP_TEM_GRADE_KEY);
  }

  public void setParpTemGrade(String value) {
    takeStoredValueForKey(value, PARP_TEM_GRADE_KEY);
  }

  public String parpTemServEffectifs() {
    return (String) storedValueForKey(PARP_TEM_SERV_EFFECTIFS_KEY);
  }

  public void setParpTemServEffectifs(String value) {
    takeStoredValueForKey(value, PARP_TEM_SERV_EFFECTIFS_KEY);
  }

  public String parpTemServPublics() {
    return (String) storedValueForKey(PARP_TEM_SERV_PUBLICS_KEY);
  }

  public void setParpTemServPublics(String value) {
    takeStoredValueForKey(value, PARP_TEM_SERV_PUBLICS_KEY);
  }

  public String parpTemValide() {
    return (String) storedValueForKey(PARP_TEM_VALIDE_KEY);
  }

  public void setParpTemValide(String value) {
    takeStoredValueForKey(value, PARP_TEM_VALIDE_KEY);
  }

  public String parpType() {
    return (String) storedValueForKey(PARP_TYPE_KEY);
  }

  public void setParpType(String value) {
    takeStoredValueForKey(value, PARP_TYPE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.EOCorps corpsArrivee() {
    return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey(CORPS_ARRIVEE_KEY);
  }

  public void setCorpsArriveeRelationship(org.cocktail.mangue.modele.grhum.EOCorps value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOCorps oldValue = corpsArrivee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CORPS_ARRIVEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CORPS_ARRIVEE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOCorps corpsDepart() {
    return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey(CORPS_DEPART_KEY);
  }

  public void setCorpsDepartRelationship(org.cocktail.mangue.modele.grhum.EOCorps value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOCorps oldValue = corpsDepart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CORPS_DEPART_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CORPS_DEPART_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOGrade gradeArrivee() {
    return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey(GRADE_ARRIVEE_KEY);
  }

  public void setGradeArriveeRelationship(org.cocktail.mangue.modele.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOGrade oldValue = gradeArrivee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GRADE_ARRIVEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GRADE_ARRIVEE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOGrade gradeDepart() {
    return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey(GRADE_DEPART_KEY);
  }

  public void setGradeDepartRelationship(org.cocktail.mangue.modele.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOGrade oldValue = gradeDepart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GRADE_DEPART_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GRADE_DEPART_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation typePopulationDepart() {
    return (org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation)storedValueForKey(TYPE_POPULATION_DEPART_KEY);
  }

  public void setTypePopulationDepartRelationship(org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation oldValue = typePopulationDepart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_POPULATION_DEPART_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_POPULATION_DEPART_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation typePopulationServPublics() {
    return (org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation)storedValueForKey(TYPE_POPULATION_SERV_PUBLICS_KEY);
  }

  public void setTypePopulationServPublicsRelationship(org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation oldValue = typePopulationServPublics();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_POPULATION_SERV_PUBLICS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_POPULATION_SERV_PUBLICS_KEY);
    }
  }
  

/**
 * Créer une instance de EOParamPromotion avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOParamPromotion createEOParamPromotion(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String parpCode
, NSTimestamp parpDOuverture
, String parpTemCategorie
, String parpTemCondition
, String parpTemCorps
, String parpTemEchelon
, String parpTemGrade
, String parpTemServEffectifs
, String parpTemServPublics
, String parpTemValide
, String parpType
			) {
    EOParamPromotion eo = (EOParamPromotion) createAndInsertInstance(editingContext, _EOParamPromotion.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setParpCode(parpCode);
		eo.setParpDOuverture(parpDOuverture);
		eo.setParpTemCategorie(parpTemCategorie);
		eo.setParpTemCondition(parpTemCondition);
		eo.setParpTemCorps(parpTemCorps);
		eo.setParpTemEchelon(parpTemEchelon);
		eo.setParpTemGrade(parpTemGrade);
		eo.setParpTemServEffectifs(parpTemServEffectifs);
		eo.setParpTemServPublics(parpTemServPublics);
		eo.setParpTemValide(parpTemValide);
		eo.setParpType(parpType);
    return eo;
  }

  
	  public EOParamPromotion localInstanceIn(EOEditingContext editingContext) {
	  		return (EOParamPromotion)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOParamPromotion creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOParamPromotion creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOParamPromotion object = (EOParamPromotion)createAndInsertInstance(editingContext, _EOParamPromotion.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOParamPromotion localInstanceIn(EOEditingContext editingContext, EOParamPromotion eo) {
    EOParamPromotion localInstance = (eo == null) ? null : (EOParamPromotion)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOParamPromotion#localInstanceIn a la place.
   */
	public static EOParamPromotion localInstanceOf(EOEditingContext editingContext, EOParamPromotion eo) {
		return EOParamPromotion.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOParamPromotion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOParamPromotion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOParamPromotion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOParamPromotion)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOParamPromotion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOParamPromotion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOParamPromotion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOParamPromotion)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOParamPromotion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOParamPromotion eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOParamPromotion ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOParamPromotion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

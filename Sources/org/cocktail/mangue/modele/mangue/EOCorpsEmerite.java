// EOCorpsEmerite.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOCorps;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOCorpsEmerite extends _EOCorpsEmerite {

	public EOCorpsEmerite() {
		super();
	}

	public static EOCorpsEmerite creer(EOEditingContext ec) {
		EOCorpsEmerite newObject = new EOCorpsEmerite();
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());
		ec.insertObject(newObject);
		return newObject;
	}

	public void validateForSave() throws NSValidation.ValidationException {
		if (dDebVal() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une date de début de validité");
		}	
		if (dFinVal() != null && DateCtrl.isAfter(dDebVal(),dFinVal())) {
			throw new NSValidation.ValidationException("La date de début de validité ne peut être postérieure à la date de fin de validité");
		}
		if (corps() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un corps");
		}
		// Vérifier que les dates de début et fin sont postérieures à celle du corps
		if (corps().dOuvertureCorps() != null && DateCtrl.isBefore(dDebVal(),corps().dOuvertureCorps())) {
			throw new NSValidation.ValidationException("La date de début ne peut être antérieure à la date d'ouverture du corps (" + DateCtrl.dateToString(corps().dOuvertureCorps()) + ")");
		}
		if (corps().dFermetureCorps() != null && (dFinVal() == null || DateCtrl.isAfter(dFinVal(), corps().dFermetureCorps()))) {
			throw new NSValidation.ValidationException("La date de fin ne peut être postérieure à la date de fermeture du corps du corps (" + DateCtrl.dateToString(corps().dFermetureCorps()) + ")");

		}
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param corps
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static EOCorpsEmerite rechercherCorpsEmeritePourCorpsEtPeriode(EOEditingContext editingContext, EOCorps corps, NSTimestamp dateDebut, NSTimestamp dateFin) {
		try {    	
			NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(CORPS_KEY+"=%@",new NSArray(corps)));
			if (dateDebut == null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FIN_VAL_KEY + " = nil",null));
			} else {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(dFinVal  = nil OR dFinVal >= %@)",new NSArray(dateDebut)));
				if (dateFin != null) {
					// champDateDebut = nil or champDateDebut <= finPeriode
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(dDebVal = nil OR dDebVal <= %@)",new NSArray(dateFin)));
				}
			}
			return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
		} catch (Exception e) {
			return null;
		}
	}
}

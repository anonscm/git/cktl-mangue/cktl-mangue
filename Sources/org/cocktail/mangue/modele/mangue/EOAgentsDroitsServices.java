//EOAgentsDroitsServices.java
//Created on Thu Feb 20 09:47:07  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue;


import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOGenericRecord;

public class EOAgentsDroitsServices extends EOGenericRecord implements RecordAvecLibelle {

	public EOAgentsDroitsServices() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOAgentsDroitsServices(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */

	public Number noIndividu() {
		return (Number)storedValueForKey("noIndividu");
	}

	public void setNoIndividu(Number value) {
		takeStoredValueForKey(value, "noIndividu");
	}

	public String cStructure() {
		return (String)storedValueForKey("cStructure");
	}

	public void setCStructure(String value) {
		takeStoredValueForKey(value, "cStructure");
	}

	public EOStructure toStructure() {
		return (EOStructure)storedValueForKey("toStructure");
	}

	public void setToStructure(EOStructure value) {
		takeStoredValueForKey(value, "toStructure");
	}

	public EOIndividu toIndividu() {
		return (EOIndividu)storedValueForKey("toIndividu");
	}

	public void setToIndividu(EOIndividu value) {
		takeStoredValueForKey(value, "toIndividu");
	}
	// méthodes ajoutées
	public void initAvecAgentEtStructure(EOAgentPersonnel agent,EOStructure structure) {
		setNoIndividu(agent.toIndividu().noIndividu());
		addObjectToBothSidesOfRelationshipWithKey(agent.toIndividu(),"toIndividu");
		setCStructure(structure.cStructure());
		addObjectToBothSidesOfRelationshipWithKey(structure,"toStructure");
		agent.addObjectToBothSidesOfRelationshipWithKey(this,"toAgentsDroitsServices");

	}
	// Interface RecordAvecLibelle
	public String libelle() {
		return toStructure().llStructure();
	}
}

//EOPostePfr.java
//Created on Fri Nov 06 12:57:21 Europe/Paris 2009 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.mangue;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.mangue.lolf.EOPoste;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


/** V&eacute;rifie la coh&eacute;rence des dates et qu'il n'y a pas de chevauchement de dates<BR>
 * V&eacute;rifie que le coefficient est dans les limites impos&eacute;es pour le coefficient de r&eacute;sultat de la PFR */
public class EOPostePFR extends EOGenericRecord {
	private final static String COEFFICIENT_MINIMUM_PART_RESULTAT = "COMIPFRR";
	private final static String COEFFICIENT_MAXIMUM_PART_RESULTAT = "COMXPFRR";
	public EOPostePFR() {
		super();
	}

	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOPostePfr(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */
	public NSTimestamp ppfrDDebut() {
		return (NSTimestamp)storedValueForKey("ppfrDDebut");
	}

	public void setPpfrDDebut(NSTimestamp value) {
		takeStoredValueForKey(value, "ppfrDDebut");
	}

	public NSTimestamp ppfrDFin() {
		return (NSTimestamp)storedValueForKey("ppfrDFin");
	}

	public void setPpfrDFin(NSTimestamp value) {
		takeStoredValueForKey(value, "ppfrDFin");
	}

	public BigDecimal ppfrCoef() {
		return (BigDecimal)storedValueForKey("ppfrCoef");
	}

	public void setPpfrCoef(BigDecimal value) {
		takeStoredValueForKey(value, "ppfrCoef");
	}

	public org.cocktail.mangue.modele.mangue.lolf.EOPoste poste() {
		return (org.cocktail.mangue.modele.mangue.lolf.EOPoste)storedValueForKey("poste");
	}

	public void setPoste(org.cocktail.mangue.modele.mangue.lolf.EOPoste value) {
		takeStoredValueForKey(value, "poste");
	}
	// Méthodes ajoutées
	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,"ppfrDDebut");
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setPpfrDDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,"ppfrDDebut",uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,"ppfrDFin");
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setPpfrDFin(null);
		} else {
			SuperFinder.setDateFormatee(this,"ppfrDFin",uneDate);
		}
	}
	public void initAvecPoste(EOPoste poste) {
		setPpfrDDebut(poste.posDDebut());
		addObjectToBothSidesOfRelationshipWithKey(poste,"poste");
	}
	public void validateForSave() throws  NSValidation.ValidationException {
		if (ppfrCoef() == null) {
			throw new NSValidation.ValidationException("Le coefficient de PFR doit être saisi");
		}
		if (poste() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un poste");
		}
		// Vérifier la cohérence des dates et qu'il n'y a pas de chevauchement de dates
		if (ppfrDDebut() == null) {
			throw new NSValidation.ValidationException("La date de début de validité doit être fournie");
		}
		if (ppfrDFin() != null && DateCtrl.isBefore(ppfrDFin(), ppfrDDebut())) {
			throw new NSValidation.ValidationException("La date de fin de validité ne peut être postérieure à la date de début de validité");
		}
		// Vérifier que les dates sont cohérentes avec celles du poste
		if (DateCtrl.isBefore(ppfrDDebut(),poste().posDDebut()) || 
				(poste().posDFin() != null && DateCtrl.isAfter(ppfrDDebut(), poste().posDFin()))) {
			throw new NSValidation.ValidationException("La date de début de validité doit être comprise dans les dates de validité du poste");
		}
		if ((ppfrDFin() == null && poste().posDFin() != null) || 
				(ppfrDFin() != null && 
						(DateCtrl.isBefore(ppfrDFin(), poste().posDDebut()) || 
						(poste().posDFin() != null && DateCtrl.isAfter(ppfrDFin(), poste().posDFin()))))) {
			throw new NSValidation.ValidationException("La date de fin de validité doit être comprise dans les dates de validité du poste");
		}
		NSArray postePFRs = postesPFRPourPoste(editingContext(), poste());
		postePFRs = EOSortOrdering.sortedArrayUsingKeyOrderArray(postePFRs, new NSArray(EOSortOrdering.sortOrderingWithKey("ppfrDDebut", EOSortOrdering.CompareDescending)));
		java.util.Enumeration e = postePFRs.objectEnumerator();
		while (e.hasMoreElements()) {
			EOPostePFR postePFR = (EOPostePFR)e.nextElement();
			if (postePFR != this) {
				if (ppfrDFin() == null) {
					if (DateCtrl.isAfterEq(postePFR.ppfrDDebut(),ppfrDDebut()) || postePFR.ppfrDFin() == null || 
							DateCtrl.isAfterEq(postePFR.ppfrDFin(),ppfrDDebut())) {
						throw new NSValidation.ValidationException("Un coefficient de PFR est déjà défini pour cette période");
					}
				} else {
					// tous ceux qui ne se terminent pas avant la date de début ou qui ne commencent pas avant la date de fin
					if (!((postePFR.ppfrDFin() != null && DateCtrl.isBeforeEq(postePFR.ppfrDFin(),ppfrDDebut())) || DateCtrl.isAfterEq(postePFR.ppfrDDebut(),ppfrDFin()))) {
						throw new NSValidation.ValidationException("Un coefficient de PFR est déjà défini pour cette période");
					}
				} 
			}
		}
		NSArray coefficientsMini = EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(editingContext(), COEFFICIENT_MINIMUM_PART_RESULTAT, ppfrDDebut(), ppfrDFin(), null);
		NSArray coefficientsMaxi = EOPrimeParam.rechercherParametresValidesPourCodePeriodeEtQualifier(editingContext(), COEFFICIENT_MAXIMUM_PART_RESULTAT, ppfrDDebut(), ppfrDFin(), null);
		if (coefficientsMini.count() == 0) {
			throw new NSValidation.ValidationException("Contactez votre administrateur des primes, les coefficients de résultat minimum ne sont pas définis");
		}
		if (coefficientsMaxi.count() == 0) {
			throw new NSValidation.ValidationException("Contactez votre administrateur des primes, les coefficients de résultat maximum ne sont pas définis");
		}
		// Le coefficient doit être compris entre le max des coefficients mini et le min des coefficients maxi
		coefficientsMini = EOSortOrdering.sortedArrayUsingKeyOrderArray(coefficientsMini, new NSArray(EOSortOrdering.sortOrderingWithKey("pparEntier", EOSortOrdering.CompareDescending)));
		coefficientsMaxi = EOSortOrdering.sortedArrayUsingKeyOrderArray(coefficientsMaxi, new NSArray(EOSortOrdering.sortOrderingWithKey("pparEntier", EOSortOrdering.CompareAscending)));
		EOPrimeParam coefficientMini = (EOPrimeParam)coefficientsMini.objectAtIndex(0);
		if (coefficientMini.pparEntier() == null) {
			throw new NSValidation.ValidationException("Contactez votre administrateur des primes, le coefficient de résultat minimum n'est pas défini dans le paramètre de prime");
		}
		EOPrimeParam coefficientMaxi = (EOPrimeParam)coefficientsMaxi.objectAtIndex(0);
		if (coefficientMaxi.pparEntier() == null) {
			throw new NSValidation.ValidationException("Contactez votre administrateur des primes, le coefficient de résultat maximum n'est pas défini dans le paramètre de prime");
		}
		double coeff = ppfrCoef().doubleValue();
		double coeffMini = coefficientMini.pparEntier().doubleValue();
		if (coeff < coeffMini) {
			throw new NSValidation.ValidationException("Le coefficient doit être supérieur ou égal à " + coeffMini);
		}
		double coeffMaxi = coefficientMaxi.pparEntier().doubleValue();
		if (coeff > coeffMaxi) {
			throw new NSValidation.ValidationException("Le coefficient doit être inférieur ou égal à " + coeffMaxi);
		}
	}
	// Méthodes statiques
	public static NSArray postesPFRPourPoste(EOEditingContext editingContext, EOPoste poste) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("poste = %@", new NSArray(poste));
		EOFetchSpecification fs = new EOFetchSpecification("PostePFR",qualifier,null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	public static NSArray postesPFRPourPosteEtPeriode(EOEditingContext editingContext, EOPoste poste,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat("poste = %@", new NSArray(poste)));
		qualifiers.addObject(SuperFinder.qualifierPourPeriode("ppfrDDebut", debutPeriode, "ppfrDFin", finPeriode));
		EOFetchSpecification fs = new EOFetchSpecification("PostePFR",new EOAndQualifier(qualifiers),null);
		fs.setRefreshesRefetchedObjects(true);
		return editingContext.objectsWithFetchSpecification(fs);
	}
}

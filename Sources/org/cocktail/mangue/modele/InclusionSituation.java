/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.grhum.elections.EOGroupeExclusion;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class InclusionSituation extends Inclusion {

	public InclusionSituation() {
		super();
	}
	public org.cocktail.mangue.common.modele.nomenclatures.EOCategorie categorie() {
		return (org.cocktail.mangue.common.modele.nomenclatures.EOCategorie)storedValueForKey("categorie");
	}

	public void setCategorie(org.cocktail.mangue.common.modele.nomenclatures.EOCategorie value) {
		takeStoredValueForKey(value, "categorie");
	}

	public org.cocktail.mangue.modele.grhum.elections.EOGroupeExclusion groupeExclusion() {
		return (org.cocktail.mangue.modele.grhum.elections.EOGroupeExclusion)storedValueForKey("groupeExclusion");
	}

	public void setGroupeExclusion(org.cocktail.mangue.modele.grhum.elections.EOGroupeExclusion value) {
		takeStoredValueForKey(value, "groupeExclusion");
	}
	// méthodes ajoutées
	public void initAvecCollegeGroupeExclusionEtCategorie(EOCollege college,EOGroupeExclusion groupeExclusion,EOCategorie categorie) {
		super.initAvecCollege(college);
		if (groupeExclusion != null) {
			addObjectToBothSidesOfRelationshipWithKey(groupeExclusion,"groupeExclusion");
		}
		if (categorie != null) {
			addObjectToBothSidesOfRelationshipWithKey(categorie,"categorie");
		}
	}
	public void supprimerRelations() {
		supprimerRelations(true);
	}
	// 31/03/2010
	public void supprimerRelations(boolean modifierInstances) {
		super.supprimerRelations(modifierInstances);
		if (groupeExclusion() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(groupeExclusion(),"groupeExclusion");
		}
		if (categorie() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(categorie(),"categorie");
		}
	}
	// méthodes statiques
	/** Recherche toutes les inclusions d'un certain type liees a un groupe d'exclusion
	 * @param nomEntite nom de l'entite associee au groupe d'exclusion
	 * @param groupeExclusion (peut etre nul)
	 */
	public static NSArray rechercherInclusionsPourGroupeExclusion(EOEditingContext editingContext,String nomEntite,EOGroupeExclusion groupeExclusion) {
		EOQualifier qualifier = null;
		if (groupeExclusion != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat("groupeExclusion = %@",new NSArray(groupeExclusion));
		}
		EOFetchSpecification myFetch = new EOFetchSpecification(nomEntite,qualifier,null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}
}

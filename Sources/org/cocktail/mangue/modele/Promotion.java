/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOHierarchieCorps;
import org.cocktail.mangue.modele.grhum.EOHierarchieGrade;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class Promotion {
	private EOCorps corps;
	private EOGrade grade;
	private String echelon;
	private String chevron;

	public Promotion(EOCorps corps, EOGrade grade,String echelon,String chevron) {
		this.corps = corps;
		this.grade = grade;
		this.echelon = echelon;
		this.chevron = chevron;
	}
	public Promotion(EOCorps corps, EOGrade grade,String echelon) {
		this(corps,grade,echelon,null);
	}
	public EOCorps corps() {
		return corps;
	}
	public void setCorps(EOCorps corps) {
		this.corps = corps;
	}
	public EOGrade grade() {
		return grade;
	}
	public void setGrade(EOGrade grade) {
		this.grade = grade;
	}
	public String echelon() {
		return echelon;
	}
	public void setEchelon(String echelon) {
		this.echelon = echelon;
	}
	public String chevron() {
		return chevron;
	}
	public void setChevron(String chevron) {
		this.chevron = chevron;
	}
	// Méthodes statiques pour évaluer les promotions de corps, grade, échelon, chevron
	/** Retourne un objet de type Promotion correspondant a une promotion de corps si celle-ci est possible pour l'element
	 * de carriere fourni ou null si pas de promotion
	 * Si pas de corps suivant dans la hierarchie des corps ou pas de grade pour ce corps : pas de promotion
	 * Dans une promotion de corps : le corps, le grade et eventuellement l'echelon sont fournis
	 * @param editingContext
	 * @param elementCarriere element pour lequel on veut effectuer la promotion
	 */
	public static Promotion promotionCorpsPourElementCarriere(EOEditingContext editingContext,EOElementCarriere elementCarriere) {
		NSArray<EOHierarchieCorps> hierarchiesCorps = SuperFinder.rechercherAvecAttributEtValeurEgale(editingContext, EOHierarchieCorps.ENTITY_NAME, EOHierarchieCorps.C_CORPS_INIT_KEY, elementCarriere.toCorps().cCorps());
		// Si on trouve 0 enregistrement, on est au niveau le plus eleve du corps ==> pas de promotion de corps possible
		if (hierarchiesCorps.count() > 0)	{
			EOHierarchieCorps hierarchie = hierarchiesCorps.get(0);
			EOCorps nouveauCorps = hierarchie.toCorpsSuivant();
			if (nouveauCorps != null) {
				NSArray grades = SuperFinder.rechercherAvecAttributEtValeurEgale(editingContext,EOGrade.ENTITY_NAME,EOGrade.C_CORPS_KEY,nouveauCorps.cCorps());
				if (grades.count() > 0) {
					// On recupere le grade le plus bas du nouveau corps - On trie par code grade sur le corps en question
					grades = EOSortOrdering.sortedArrayUsingKeyOrderArray(grades,new NSArray(EOSortOrdering.sortOrderingWithKey(EOGrade.C_GRADE_KEY, EOSortOrdering.CompareAscending)));
					EOGrade nouveauGrade = (EOGrade)grades.objectAtIndex(0);
					EOPassageEchelon nouvelEchelon = null;
					// déterminer l'échelon en fonction de l'indice du corps actuel
					Number ancienIndice = elementCarriere.indiceMajore();
					NSArray passagesEchelon = EOPassageEchelon.rechercherPassagesEchelonPourGradeAvecTri(editingContext, nouveauGrade.cGrade(), elementCarriere.dateFin(),false);
					LogManager.logDetail(" \t : Passages Echelon grade " + nouveauGrade.cGrade() + " : " + passagesEchelon.count());
					for (java.util.Enumeration<EOPassageEchelon> e = passagesEchelon.objectEnumerator();e.hasMoreElements();) {
						EOPassageEchelon passage = (EOPassageEchelon)e.nextElement();
						if (passage != null && passage.cIndiceBrut() != null) {
							EOIndice indice = EOIndice.indiceMajorePourIndiceBrutEtDate(editingContext, passage.cIndiceBrut(), elementCarriere.dateDebut());
							if (indice != null && ancienIndice != null && indice.cIndiceMajore().intValue() >= ancienIndice.intValue()) {
								nouvelEchelon = passage;
								break;
							}
						}
					}
					if (nouvelEchelon == null) {
						LogManager.logDetail("Promotion Corps - nouveauCorps : " + nouveauCorps.llCorps() + " nouveauGrade :" + nouveauGrade.llGrade());
						return new Promotion(nouveauCorps,nouveauGrade,null);
					} else {
						LogManager.logDetail("Promotion Corps - nouveauCorps : " + nouveauCorps.llCorps() + " nouveauGrade :" + nouveauGrade.llGrade() + " nouvelEchelon :" + nouvelEchelon.cEchelon());
						return new Promotion(nouveauCorps, nouveauGrade,nouvelEchelon.cEchelon());
					}
				}
			}
		}
		return null;
	}
	/** Retourne un objet de type Promotion correspondant &agrave; une promotion de grade si celle-ci est possible pour l'element
	 * de carriere fourni ou null si pas de promotion
	 * Si pas de grade suivant dans la hierarchie des grades : pas de promotion
	 * Dans une promotion de grade : le corps et le grade sont fournis
	 * @param editingContext
	 * @param elementCarriere element pour lequel on veut effectuer la promotion
	 */
	public static Promotion promotionGradePourElementCarriere(EOEditingContext editingContext,EOElementCarriere elementCarriere) {
		NSArray<EOHierarchieGrade> hierarchiesGrades = SuperFinder.rechercherAvecAttributEtValeurEgale(editingContext, EOHierarchieGrade.ENTITY_NAME, EOHierarchieGrade.C_GRADE_INIT_KEY, elementCarriere.toGrade().cGrade());		
		// Si on trouve 0 enregistrement, on est au niveau le plus eleve du grade ==> pas de promotion de grade possible
		if (hierarchiesGrades.count() > 0) {
			EOHierarchieGrade hierarchie = hierarchiesGrades.get(0);
			EOGrade nouveauGrade = hierarchie.toGradeSuivant();
			if (nouveauGrade != null) {
				LogManager.logDetail("Promotion Grade : nouveau grade : " + nouveauGrade.llGrade());
				return new Promotion(nouveauGrade.toCorps(),nouveauGrade,null);
			}
		} 
		return null;
	}

	/** Retourne un objet de type Promotion correspondant a une promotion d'echelon si celle-ci est possible pour l'element
	 * de carriere fourni ou null si pas de promotion
	 * Si il n'y a pas d'entree dans la table PassageEchelon : pas de promotion possible
	 * Dans une promotion d'echelon : l'echelon de promotion est fourni
	 * @param editingContext
	 * @param elementCarriere element pour lequel on veut effectuer la promotion
	 */
	public static Promotion promotionEchelonPourElementCarriere(EOEditingContext editingContext,EOElementCarriere elementCarriere) {
		try {
			NSArray<EOPassageEchelon> echelons = EOPassageEchelon.rechercherPassagesEchelonPourGradeAvecTri(editingContext,elementCarriere.toGrade().cGrade(), elementCarriere.dateFin(), false);
			EOPassageEchelon nouvelEchelon = null;
			if (echelons != null) {
				for (int i = 0; i < echelons.count();i++) {
					EOPassageEchelon echelon = echelons.get(i);
					if (elementCarriere.cEchelon().equals(echelon.cEchelon()) && i < echelons.count() - 1) {
						nouvelEchelon = echelons.get(i+1);
						break;
					}
				}
			}
			if (nouvelEchelon != null) {
				LogManager.logDetail("GestionCarrieres promouvoirEchelon nouvelEchelon : " + nouvelEchelon.cEchelon());
				return new Promotion(elementCarriere.toCorps(),elementCarriere.toGrade(),nouvelEchelon.cEchelon(), elementCarriere.cChevron());
			} else {
				return null;
			}
		}
		catch (Exception e) {
			return null;
		}
	}
	/** Retourne un objet de type Promotion correspondant a une promotion de cehvron si celle-ci est possible pour l'el&eacute;ment
	 * de carriere fourni ou null si pas de promotion
	 * Si un element de carriere n'a pas d'echelon ou de chevron ou bien qu'il n'y a pas 
	 * d'entree dans la table PassageChevron : pas de promotion possible
	 * Dans une promotion de chevron : le chevron de promotion est fourni
	 * @param editingContext
	 * @param elementCarriere element pour lequel on veut effectuer la promotion
	 */
	public static Promotion promotionChevronPourElementCarriere(EOEditingContext editingContext,EOElementCarriere elementCarriere) {
		if (elementCarriere.cEchelon() == null || elementCarriere.cChevron() == null) {
			return null;
		}
		// rechercher quel est le nouvel chevron pour le grade et l'échelon actuels. C'est celui qui suit le chevron actuel si il y en a un
		NSArray chevrons = SuperFinder.rechercherChevronsOuvertsPourGradeEchelonAvecTri(editingContext,elementCarriere.toGrade().cGrade(),elementCarriere.cEchelon(),elementCarriere.dateFin());
		EOGenericRecord nouveauChevron = null;
		for (int i = 0; i < chevrons.count();i++) {
			EOGenericRecord chevron = (EOGenericRecord)chevrons.objectAtIndex(i);
			if (elementCarriere.cChevron().equals(chevron.valueForKey("cChevron")) && i < chevrons.count() - 1) {
				nouveauChevron = (EOGenericRecord)chevrons.objectAtIndex(i+1);
				break;
			}
		}
		if (nouveauChevron != null) {
			LogManager.logDetail("GestionCarrieres promouvoirChevron nouveauChevron : " + nouveauChevron.valueForKey("cChevron"));
			return new Promotion(elementCarriere.toCorps(),elementCarriere.toGrade(),elementCarriere.cEchelon(),(String)nouveauChevron.valueForKey("cChevron"));
		} else {
			return null;
		}
	}
}

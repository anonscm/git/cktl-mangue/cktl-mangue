// DetailNbiIndividu.java
// Created on Wed May 31 14:10:10 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.nbi.EONbi;
import org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation;
import org.cocktail.mangue.modele.mangue.nbi.EONbiRepartFonctions;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class DetailNbi extends Object implements NSKeyValueCoding {
	private String libelleCorps;
	private EONbi nbi;
	private EONbiOccupation nbiOccupation;
	private EONbiRepartFonctions nbiRepartFonction;

	public DetailNbi(EONbi nbi,EONbiRepartFonctions nbiFonction,EONbiOccupation nbiOccupation,String libelleCorps) {
		super();
		this.nbi = nbi;
		this.nbiOccupation = nbiOccupation;
		this.nbiRepartFonction = nbiFonction;
		this.libelleCorps = libelleCorps;
	}
	/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public DetailNbiIndividu(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
	 */
	public String libelleCorps() {
		return libelleCorps;
	}
	public void setLibelleCorps(String value) {
		libelleCorps = value;
	}
	public EONbi nbi() {
		return nbi;
	}
	public void setNbi(EONbi value) {
		nbi = value;
	}

	public EONbiOccupation nbiOccupation() {
		return (EONbiOccupation)nbiOccupation;
	}
	public void setNbiOccupation(EONbiOccupation value) {
		nbiOccupation = value;
	}
	public EONbiRepartFonctions nbiRepartFonction() {
		return nbiRepartFonction;
	}
	public void setNbiRepartFonction(EONbiRepartFonctions value) {
		nbiRepartFonction = value;
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// méthodes statiques
	/** prepare les details de nbi a la date de reference */
	public static NSArray<DetailNbi> preparerDetailsNbi(EOEditingContext edc,NSTimestamp dateReference) {
		try {
			NSMutableArray<DetailNbi>details = new NSMutableArray<DetailNbi>();
			NSArray<EONbi> nbis = EONbi.rechercherNBINonFiniesADate(edc,dateReference);

			for (EONbi currentNbi : nbis) {

				EONbiRepartFonctions currentFonction = null;
				EONbiOccupation currentOccupation = null;
				String libelle = null;
				NSArray<EONbiRepartFonctions> fonctions = EONbiRepartFonctions.rechercherNbiRepartFonctionsPourPeriode(edc,currentNbi.cNbi(),dateReference,dateReference);
				if (fonctions.count() == 1) {
					currentFonction = (EONbiRepartFonctions)fonctions.objectAtIndex(0);
				}
				NSArray<EONbiOccupation> occupations = EONbiOccupation.rechercherNbiOccupationsPourPeriode(edc,currentNbi,dateReference,dateReference);
				if (occupations.count() == 1)	{
					currentOccupation = (EONbiOccupation)occupations.objectAtIndex(0);
					NSArray<EOElementCarriere> elements = EOElementCarriere.findForPeriode(edc,currentOccupation.toIndividu(), currentOccupation.dDebNbiOcc(),currentOccupation.dFinNbiOcc());
					if (elements.count() > 0) {
						libelle = ((EOElementCarriere)elements.objectAtIndex(0)).toCorps().lcCorps();
					}
				}
				DetailNbi detail = new DetailNbi(currentNbi, currentFonction, currentOccupation,libelle);
				details.addObject(detail);
			}
			return details;
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSArray<DetailNbi>();
		}
	}

	/**
	 * Prepare les details de nbi a pour la periode passee en parametre
	 * @param editingContext
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<DetailNbi> preparerDetailsNbiHistorique(EOEditingContext editingContext,NSTimestamp dateDebut,NSTimestamp dateFin) {
		try {
			NSMutableArray<DetailNbi> details = new NSMutableArray<DetailNbi>();
			NSMutableArray<EOSortOrdering> mySort = new NSMutableArray<EOSortOrdering>();
			mySort.addObject(EOSortOrdering.sortOrderingWithKey(EONbiOccupation.C_NBI_KEY, EOSortOrdering.CompareAscending));
			mySort.addObject(EOSortOrdering.sortOrderingWithKey(EONbiOccupation.TO_NBI_KEY + ".noTranche", EOSortOrdering.CompareAscending));

			EOFetchSpecification fs = new EOFetchSpecification(EONbiOccupation.ENTITY_NAME,
					SuperFinder.qualifierPourPeriode(EONbiOccupation.D_DEB_NBI_OCC_KEY,dateDebut,EONbiOccupation.D_FIN_NBI_OCC_KEY,dateFin),mySort);
			
			NSArray<EONbiOccupation> nbisOccupation = editingContext.objectsWithFetchSpecification(fs);
			
			EONbi nbiCourante = null;
			for (EONbiOccupation currentOccupation : nbisOccupation) {
			
				if (currentOccupation.toNbi() != nbiCourante) {
					nbiCourante = currentOccupation.toNbi();
				}  else {
					nbiCourante = null;		// pour éviter dans l'impression la répétition de la Nbi
				}
				// Rechercher les fonctions pour cette période
				NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>(EOQualifier.qualifierWithQualifierFormat(EONbiRepartFonctions.C_NBI_KEY + " = %@", new NSArray(currentOccupation.toNbi().cNbi())));
				qualifiers.addObject(SuperFinder.qualifierPourPeriode("dDebNbiFonc", currentOccupation.dDebNbiOcc(), "dFinNbiFonc", currentOccupation.dFinNbiOcc()));
				fs = new EOFetchSpecification(EONbiRepartFonctions.ENTITY_NAME,new EOAndQualifier(qualifiers),null);
				NSArray<EONbiRepartFonctions> fonctions = editingContext.objectsWithFetchSpecification(fs);

				EONbiRepartFonctions currentFonction = null;
				if (fonctions.count() == 1) {
					currentFonction = fonctions.get(0);
				}
				String libelleCorps = "";
				NSArray<EOElementCarriere> elements = EOElementCarriere.findForPeriode(editingContext, currentOccupation.toIndividu(), currentOccupation.dDebNbiOcc(),currentOccupation.dFinNbiOcc());
				if (elements.count() > 0) {
					libelleCorps = elements.get(0).toCorps().lcCorps();
				}
				DetailNbi detail = new DetailNbi(nbiCourante, currentFonction, currentOccupation, libelleCorps);
				details.addObject(detail);

			}
			return details;
		} catch (Exception e) {
			LogManager.logException(e);
			return new NSArray();
		}
	}
}

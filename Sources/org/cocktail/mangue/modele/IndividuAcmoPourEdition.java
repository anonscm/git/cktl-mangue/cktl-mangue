/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.grhum.referentiel.EOPersonneTelephone;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartAssociation;
import org.cocktail.mangue.modele.grhum.referentiel.EORepartPersonneAdresse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class IndividuAcmoPourEdition implements NSKeyValueCoding, NSCoding {
	private EORepartAssociation repartAssociation;
	private NSArray emails,telephonesProfessionnels;
	private EOEditingContext editingContext;

	public IndividuAcmoPourEdition(EORepartAssociation repartAssociation) {
		this.repartAssociation = repartAssociation;
		EOIndividu individu = repartAssociation.individu();
		if (individu != null) {
			NSArray repartsAdresses = EORepartPersonneAdresse.adressesProValides(individu.editingContext(), individu);
			NSMutableArray temp = new NSMutableArray(repartsAdresses.count());
			java.util.Enumeration e = repartsAdresses.objectEnumerator();
			while (e.hasMoreElements()) {
				EORepartPersonneAdresse repart = (EORepartPersonneAdresse)e.nextElement();
				if (repart.eMail() != null && repart.eMail().length() > 0) {
					temp.addObject(repart.eMail());
				}
			}
			this.emails = new NSArray(temp);
			NSArray telephonesProf = EOPersonneTelephone.rechercherTelProfessionnels(individu.editingContext(), individu);
			telephonesProfessionnels = (NSArray)telephonesProf.valueForKey("noTelephone");
		}
	}
	public IndividuAcmoPourEdition(NSDictionary dict) {
		editingContext = new EOEditingContext();
		EOGlobalID globalID = (EOGlobalID)dict.objectForKey("repartAssociation");
		this.repartAssociation = (EORepartAssociation)SuperFinder.objetForGlobalIDDansEditingContext(globalID, editingContext);
		emails = (NSArray)dict.objectForKey("emails");
		telephonesProfessionnels = (NSArray)dict.objectForKey("telephones");

	}
	// Accesseurs
	public EORepartAssociation repartAssociation() {
		return repartAssociation;
	}
	public void setRepartAssociation(EORepartAssociation repartAssociation) {
		this.repartAssociation = repartAssociation;
	}
	public String debutHabilitation() {
		if (repartAssociation() == null) {
			return null;
		} else {
			return repartAssociation().dateDebutFormatee();
		}
	}
	public String finHabilitation() {
		if (repartAssociation() == null) {
			return null;
		} else {
			return repartAssociation().dateFinFormatee();
		}
	}
	public EOIndividu individu() {
		if (repartAssociation() == null) {
			return null;
		} else {
			return repartAssociation().individu();
		}
	}
	public String emailsFormates() {
		return construireTexte(emails());
	}
	public String telephonesFormates() {
		return construireTexte(telephonesProfessionnels());
	}
	public NSArray emails() {
		return emails;
	}
	public void setEmails(NSArray emails) {
		this.emails = emails;
	}
	public NSArray telephonesProfessionnels() {
		return telephonesProfessionnels;
	}
	public void setTelephonesProfessionnels(NSArray telephonesProfessionnels) {
		this.telephonesProfessionnels = telephonesProfessionnels;
	}
	public EOEditingContext editingContext() {
		return editingContext;
	}
	// Interface key-value Coding
	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}
	// Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	public Class classForCoder() {
		return IndividuAcmoPourEdition.class;
	}
	public static Class decodeClass() {
		return IndividuAcmoPourEdition.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new IndividuAcmoPourEdition((NSDictionary)coder.decodeObject());
	}
	public void encodeWithCoder(NSCoder coder) {
		NSMutableDictionary dict = new NSMutableDictionary();
		dict.setObjectForKey(repartAssociation.editingContext().globalIDForObject(repartAssociation), "repartAssociation");
		dict.setObjectForKey(emails,"emails");
		dict.setObjectForKey(telephonesProfessionnels,"telephones");
		coder.encodeObject(dict);
	}
	// Méthodes privées
	private String construireTexte(NSArray arrayOfStrings) {
		if (arrayOfStrings == null) {
			return "";
		}
		String texte = "";
		java.util.Enumeration e = arrayOfStrings.objectEnumerator();
		while (e.hasMoreElements()) {
			String item = (String)e.nextElement();
			texte += item + "\n";
		}
		if (texte.length() > 0) {
			// Enlever le dernier caractère
			texte = texte.substring(0,texte.length() - 1);
		}
		return texte;
	}
}

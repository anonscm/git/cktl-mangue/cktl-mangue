// _EOQuotite.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOQuotite.java instead.
package org.cocktail.mangue.modele.grhum;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOQuotite extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Quotite";
	public static final String ENTITY_TABLE_NAME = "GRHUM.QUOTITE";



	// Attributes


	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEN_QUOTITE_KEY = "denQuotite";
	public static final String DEN_QUOTITE_REMUN_R_KEY = "denQuotiteRemunR";
	public static final String DEN_QUOTITE_REMUN_TP_KEY = "denQuotiteRemunTp";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INDIC_QUOTITE_RECRUT_KEY = "indicQuotiteRecrut";
	public static final String INDIC_QUOTITE_TP_KEY = "indicQuotiteTp";
	public static final String NUM_QUOTITE_KEY = "numQuotite";
	public static final String NUM_QUOTITE_REMUN_R_KEY = "numQuotiteRemunR";
	public static final String NUM_QUOTITE_REMUN_TP_KEY = "numQuotiteRemunTp";
	public static final String QUOTITE_CONGE_ANNUEL_KEY = "quotiteCongeAnnuel";
	public static final String QUOTITE_FINANCIERE_KEY = "quotiteFinanciere";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DEN_QUOTITE_COLKEY = "DEN_QUOTITE";
	public static final String DEN_QUOTITE_REMUN_R_COLKEY = "DEN_QUOTITE_REMUN_R";
	public static final String DEN_QUOTITE_REMUN_TP_COLKEY = "DEN_QUOTITE_REMUN_TP";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String INDIC_QUOTITE_RECRUT_COLKEY = "INDIC_QUOTITE_RECRUT";
	public static final String INDIC_QUOTITE_TP_COLKEY = "INDIC_QUOTITE_TP";
	public static final String NUM_QUOTITE_COLKEY = "NUM_QUOTITE";
	public static final String NUM_QUOTITE_REMUN_R_COLKEY = "NUM_QUOTITE_REMUN_R";
	public static final String NUM_QUOTITE_REMUN_TP_COLKEY = "NUM_QUOTITE_REMUN_TP";
	public static final String QUOTITE_CONGE_ANNUEL_COLKEY = "QUOTITE_CONGE_ANNUEL";
	public static final String QUOTITE_FINANCIERE_COLKEY = "QUOTITE_FINANCIERE";



	// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public Integer denQuotite() {
    return (Integer) storedValueForKey(DEN_QUOTITE_KEY);
  }

  public void setDenQuotite(Integer value) {
    takeStoredValueForKey(value, DEN_QUOTITE_KEY);
  }

  public Integer denQuotiteRemunR() {
    return (Integer) storedValueForKey(DEN_QUOTITE_REMUN_R_KEY);
  }

  public void setDenQuotiteRemunR(Integer value) {
    takeStoredValueForKey(value, DEN_QUOTITE_REMUN_R_KEY);
  }

  public Integer denQuotiteRemunTp() {
    return (Integer) storedValueForKey(DEN_QUOTITE_REMUN_TP_KEY);
  }

  public void setDenQuotiteRemunTp(Integer value) {
    takeStoredValueForKey(value, DEN_QUOTITE_REMUN_TP_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String indicQuotiteRecrut() {
    return (String) storedValueForKey(INDIC_QUOTITE_RECRUT_KEY);
  }

  public void setIndicQuotiteRecrut(String value) {
    takeStoredValueForKey(value, INDIC_QUOTITE_RECRUT_KEY);
  }

  public String indicQuotiteTp() {
    return (String) storedValueForKey(INDIC_QUOTITE_TP_KEY);
  }

  public void setIndicQuotiteTp(String value) {
    takeStoredValueForKey(value, INDIC_QUOTITE_TP_KEY);
  }

  public Integer numQuotite() {
    return (Integer) storedValueForKey(NUM_QUOTITE_KEY);
  }

  public void setNumQuotite(Integer value) {
    takeStoredValueForKey(value, NUM_QUOTITE_KEY);
  }

  public Integer numQuotiteRemunR() {
    return (Integer) storedValueForKey(NUM_QUOTITE_REMUN_R_KEY);
  }

  public void setNumQuotiteRemunR(Integer value) {
    takeStoredValueForKey(value, NUM_QUOTITE_REMUN_R_KEY);
  }

  public Integer numQuotiteRemunTp() {
    return (Integer) storedValueForKey(NUM_QUOTITE_REMUN_TP_KEY);
  }

  public void setNumQuotiteRemunTp(Integer value) {
    takeStoredValueForKey(value, NUM_QUOTITE_REMUN_TP_KEY);
  }

  public java.math.BigDecimal quotiteCongeAnnuel() {
    return (java.math.BigDecimal) storedValueForKey(QUOTITE_CONGE_ANNUEL_KEY);
  }

  public void setQuotiteCongeAnnuel(java.math.BigDecimal value) {
    takeStoredValueForKey(value, QUOTITE_CONGE_ANNUEL_KEY);
  }

  public java.math.BigDecimal quotiteFinanciere() {
    return (java.math.BigDecimal) storedValueForKey(QUOTITE_FINANCIERE_KEY);
  }

  public void setQuotiteFinanciere(java.math.BigDecimal value) {
    takeStoredValueForKey(value, QUOTITE_FINANCIERE_KEY);
  }


/**
 * Créer une instance de EOQuotite avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOQuotite createEOQuotite(EOEditingContext editingContext, NSTimestamp dCreation
, Integer denQuotite
, NSTimestamp dModification
, Integer numQuotite
			) {
    EOQuotite eo = (EOQuotite) createAndInsertInstance(editingContext, _EOQuotite.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDenQuotite(denQuotite);
		eo.setDModification(dModification);
		eo.setNumQuotite(numQuotite);
    return eo;
  }

  
	  public EOQuotite localInstanceIn(EOEditingContext editingContext) {
	  		return (EOQuotite)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOQuotite creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOQuotite creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOQuotite object = (EOQuotite)createAndInsertInstance(editingContext, _EOQuotite.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOQuotite localInstanceIn(EOEditingContext editingContext, EOQuotite eo) {
    EOQuotite localInstance = (eo == null) ? null : (EOQuotite)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOQuotite#localInstanceIn a la place.
   */
	public static EOQuotite localInstanceOf(EOEditingContext editingContext, EOQuotite eo) {
		return EOQuotite.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOQuotite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOQuotite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOQuotite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOQuotite)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOQuotite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOQuotite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOQuotite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOQuotite)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOQuotite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOQuotite eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOQuotite ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOQuotite fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

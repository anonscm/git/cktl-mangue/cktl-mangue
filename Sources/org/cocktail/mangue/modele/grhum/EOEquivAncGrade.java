// EquivAncGrade.java
// Created on Mon Jul 20 17:03:36 Europe/Paris 2009 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum;

import java.util.Enumeration;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

// 25/02/2010 - on recherche sur le grade de départ, les grades équivalents
// 08/03/2010 - gestion de la nomenclature
/** Regles de validation : le grade equivalent, la date de debut de validite doivent etre fournis<BR>
 * La date de d&eacute;but de validite ne doit pas &ecirc;tre post&eacute;rieure &agrave; la date de fin
 */
public class EOEquivAncGrade extends _EOEquivAncGrade {

    public EOEquivAncGrade() {
        super();
    }


	public static EOEquivAncGrade creer(EOEditingContext ec) {
		
		EOEquivAncGrade newObject = (EOEquivAncGrade) createAndInsertInstance(ec, EOEquivAncGrade.ENTITY_NAME);    

		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());

		return newObject;
	}


	
	
    public static NSArray rechercherGradesDepart(EOEditingContext ec) {

    	NSMutableArray qualifiers = new NSMutableArray();
    	
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FIN_VAL_KEY  + " = nil", new NSArray()));
    	    	
    	NSArray equivalences = fetchAll(ec, new EOAndQualifier(qualifiers), null);
    	NSMutableArray gradesDeparts = new NSMutableArray();
    	
    	for (Enumeration<EOEquivAncGrade> e = equivalences.objectEnumerator();e.hasMoreElements();) {
    		EOEquivAncGrade equiv = e.nextElement();
    		if (!gradesDeparts.containsObject(equiv.gradeDepart()))
    			gradesDeparts.addObject(equiv.gradeDepart());    		
    	}
    
    	return gradesDeparts.immutableClone();    	
    }

    
	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this, D_DEB_VAL_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null || uneDate.equals("")) {
			setDDebVal(null);
		} else {
			SuperFinder.setDateFormatee(this, D_DEB_VAL_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this, D_FIN_VAL_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null || uneDate.equals("")) {
			setDFinVal(null);
		} else {
			SuperFinder.setDateFormatee(this, D_FIN_VAL_KEY,uneDate);
		}
	}
	public boolean uniquementPourPromouvabilite() {
		return temPromouvabilite() != null && temPromouvabilite().equals(CocktailConstantes.VRAI);
	}
	public void setUniquementPourPromouvabilite(boolean aBool) {
		if (aBool) {
			setTemPromouvabilite(CocktailConstantes.VRAI);
		} else {
			setTemPromouvabilite(CocktailConstantes.FAUX);
		}
	}
	public boolean prendreEnCompteTemoinTypeAcces() {
		return temTtTypeAcces() != null && temTtTypeAcces().equals(CocktailConstantes.VRAI);
	}
	public void setPrendreEnCompteTemoinTypeAcces(boolean aBool) {
		if (aBool) {
			setTemTtTypeAcces(CocktailConstantes.VRAI);
		} else {
			setTemTtTypeAcces(CocktailConstantes.FAUX);
		}
	}
	public void initAvecGrade(EOGrade grade) {
		setUniquementPourPromouvabilite(false);
		setPrendreEnCompteTemoinTypeAcces(false);
		setGradeDepartRelationship(grade);
	}
	public void supprimerRelations() {
		if (gradeEquivalent() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(gradeEquivalent(), GRADE_EQUIVALENT_KEY);
		}
		if (typeAcces() != null) {
			removeObjectFromBothSidesOfRelationshipWithKey(gradeEquivalent(), TYPE_ACCES_KEY);
		}
		removeObjectFromBothSidesOfRelationshipWithKey(gradeDepart(), GRADE_DEPART_KEY);
	}
	public void validateForSave() {
		if (gradeEquivalent() == null) {
			throw new NSValidation.ValidationException("Le grade équivalent doit être fourni");
		}
		if (dDebVal() == null) {
			throw new NSValidation.ValidationException("La date de début de validité doit être fournie");
		} else if (dFinVal() != null && DateCtrl.isBefore(dFinVal(), dDebVal())) {
			throw new NSValidation.ValidationException("La date de début de validité ne peut être postérieure à la date de fin de validité");
		}
	}
    // Méthodes statiques
    /** Retourne le tableau des &eacute;quivalences de grades pour le grade fourni */
    // 25/02/2010 - on recherche sur le grade de départ, les grades équivalents
    public static NSArray equivalencesGradePourGrade(EOEditingContext editingContext, EOGrade grade, NSTimestamp dateReference) {
		NSMutableArray args = new NSMutableArray(grade);
		EOQualifier qualifier = null;
		if (dateReference != null) {
			args.addObject(dateReference);
			args.addObject(dateReference);
			qualifier = EOQualifier.qualifierWithQualifierFormat("gradeDepart = %@ AND gradeEquivalent <> NIL AND (dDebVal <= %@ OR dDebVal  = NIL) AND (dFinVal >= %@ OR dFinVal = NIL)", args);
		} else {
			qualifier = EOQualifier.qualifierWithQualifierFormat("gradeDepart = %@ AND gradeEquivalent <> NIL",args);
		}
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier, null);
		return editingContext.objectsWithFetchSpecification(fs);
    }
}

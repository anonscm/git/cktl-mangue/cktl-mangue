/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.grhum;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOBanque extends _EOBanque {

    public EOBanque() {
        super();
    }

    
    public boolean estLocale() {
    	return temLocal().equals(CocktailConstantes.VRAI);
    }
    
	public static NSArray find(EOEditingContext ec, String banque, String guichet, String domiciliation, String bic)	{

		try {

			NSMutableArray qualifiers = new NSMutableArray();

			if (banque != null && banque.length() > 0)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_BANQUE_KEY + " caseInsensitiveLike %@", 
						new NSArray("*"+banque+"*")));

			if (guichet != null && guichet.length() > 0)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GUICHET_KEY + " caseInsensitiveLike %@", 
						new NSArray("*"+guichet+"*")));

			if (domiciliation != null && domiciliation.length() > 0)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DOMICILIATION_KEY + " caseInsensitiveLike %@", 
						new NSArray("*"+domiciliation+"*")));

			if (bic != null && bic.length() > 0)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(BIC_KEY + " caseInsensitiveLike %@", 
						new NSArray("*"+bic+"*")));

			NSArray sort = new NSArray(new EOSortOrdering(C_BANQUE_KEY, EOSortOrdering.CompareAscending));
						
			return fetchAll(ec, new EOAndQualifier(qualifiers), sort);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
    
	/** Initialisation d'un object de type EOBanque */
	public void initBanque(String banque, String guichet, String domiciliation)
	{
		setCBanque(banque);
		setCGuichet(guichet);
		setDomiciliation(domiciliation);

	}
	public void validateForSave() {
		if (cBanque() == null && cGuichet() == null && bic() == null) {
			throw new NSValidation.ValidationException("Le code banque et le guichet ou le bic doivent être fournis");
		}
		if (cBanque() != null && cBanque().length() != 5) {
			throw new NSValidation.ValidationException("Le code banque comporte 5 caractères");
		}
		if (cGuichet() != null && cGuichet().length() != 5) {
			throw new NSValidation.ValidationException("Le code guichet comporte 5 caractères");
		}
		if (bic() != null && bic().length() > 11) {
			throw new NSValidation.ValidationException("Le bic comporte au plus 11 caractères");
		}
		if (domiciliation() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir la domiciliation");
		} else if (domiciliation().length() > 200) {
			throw new NSValidation.ValidationException("La domiciliation comporte au plus 200 caractères");
		}
		setDomiciliation(StringCtrl.chaineClaire(domiciliation(), true).toUpperCase());
	}
	
	
	public static EOBanque rechercherBanque(EOEditingContext ec, String cBanque, String cGuichet) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_BANQUE_KEY + " = %@", new NSArray(cBanque)));		
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GUICHET_KEY + " = %@", new NSArray(cGuichet)));		
		
		NSArray banques = fetchAll(ec, new EOAndQualifier(qualifiers));
		if (banques.count() == 1)
			return (EOBanque)banques.objectAtIndex(0);

		return null;
	}
	
	
	
	/** recherche toutes les banques selon les crit&grave;res pass&eacute;s en param&grave;tre. Retourne la liste
	 * tri&eacute;e par code banque et guichet croissants */
	public static NSArray rechercherBanques(EOEditingContext ec, String banque,String guichet,String bic, String domiciliation,boolean shouldRefresh)	{
		NSMutableArray qualifiers = new NSMutableArray();
		if (banque != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("cBanque like '*" + banque + "*'",null));
		}
		if (guichet != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("cGuichet like '*" + guichet + "*'",null));
		}
		if (bic != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("bic caseinsensitivelike '*" + bic + "*'",null));
		}
		if (domiciliation != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("domiciliation caseinsensitivelike '*" + domiciliation + "*'",null));
		}
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey("cBanque", EOSortOrdering.CompareAscending));
		sorts.addObject(EOSortOrdering.sortOrderingWithKey("cGuichet", EOSortOrdering.CompareAscending));
		EOFetchSpecification fs = new EOFetchSpecification("Banque",new EOAndQualifier(qualifiers),sorts);
		fs.setRefreshesRefetchedObjects(shouldRefresh);
		return ec.objectsWithFetchSpecification(fs);
	}

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

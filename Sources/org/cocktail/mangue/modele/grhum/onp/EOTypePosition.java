/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.grhum.onp;

import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOTypePosition extends _EOTypePosition {

	public static NSArray SORT_ARRAY_CODE_ASC = new NSArray(new EOSortOrdering(C_POSITION_KEY, EOSortOrdering.CompareAscending));
	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(new EOSortOrdering(LL_POSITION_KEY, EOSortOrdering.CompareAscending));

	public static final String C_POSITION_ACTIVITE = "ACI00";
	public static final String C_POSITION_DETACHEMENT = "DET00";
	public static final String C_POSITION_DISPONIBILITE = "DSP00";
	public static final String C_POSITION_CONGE_PARENTAL = "CGP00";
	
    public EOTypePosition() {
        super();
    }

    public String toString() {
    	return llPosition();
    }
    
    public boolean estPourEnfant() {
    	return temEnfant().equals(CocktailConstantes.VRAI);
    }
    public boolean estTitulaire() {
    	return temTitulaire().equals(CocktailConstantes.VRAI);
    }
    
    public boolean estActivite() {
    	return cPosition().equals(C_POSITION_ACTIVITE);
    }
    public boolean estDetachement() {
    	return cPosition().equals(C_POSITION_DETACHEMENT);
    }
    public boolean estDispo() {
    	return cPosition().equals(C_POSITION_DISPONIBILITE);
    }
    public boolean estCongeParental() {
    	return cPosition().equals(C_POSITION_CONGE_PARENTAL);
    }

    public boolean estEnActivite() {
    	return estActivite() || estDetachement();
    }
    
    public static EOTypePosition findPosition(EOEditingContext ec, String code) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_POSITION_KEY + "=%@", new NSArray(code));
    	return fetchFirstByQualifier(ec, qualifier);
    }
    public static NSArray findPositions(EOEditingContext ec, NSTimestamp dateRef) {
    	//EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat( + "=%@", new NSArray(code));
    	return fetchAll(ec, null, SORT_ARRAY_LIBELLE_ASC);
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

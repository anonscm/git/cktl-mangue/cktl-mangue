// _EOTypePositionDetail.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypePositionDetail.java instead.
package org.cocktail.mangue.modele.grhum.onp;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOTypePositionDetail extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "TypePositionDetail";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TYPE_POSITION_DETAIL";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tpdId";

	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String TEM_VISIBLE_KEY = "temVisible";
	public static final String TPD_CODE_KEY = "tpdCode";
	public static final String TPD_LIBELLE_COURT_KEY = "tpdLibelleCourt";
	public static final String TPD_LIBELLE_LONG_KEY = "tpdLibelleLong";
	public static final String TPD_NIVEAU_KEY = "tpdNiveau";

// Attributs non visibles
	public static final String TPD_ID_KEY = "tpdId";
	public static final String TPD_PERE_ID_KEY = "tpdPereId";
	public static final String TYP_POS_ID_KEY = "typPosId";

//Colonnes dans la base de donnees
	public static final String D_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String D_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String TEM_VISIBLE_COLKEY = "TEM_VISIBLE";
	public static final String TPD_CODE_COLKEY = "TPD_CODE";
	public static final String TPD_LIBELLE_COURT_COLKEY = "TPD_LIBELLE_COURT";
	public static final String TPD_LIBELLE_LONG_COLKEY = "TPD_LIBELLE_LONG";
	public static final String TPD_NIVEAU_COLKEY = "TPD_NIVEAU";

	public static final String TPD_ID_COLKEY = "TPD_ID";
	public static final String TPD_PERE_ID_COLKEY = "TPD_PERE_ID";
	public static final String TYP_POS_ID_COLKEY = "TYP_POS_ID";


	// Relationships
	public static final String PERE_KEY = "pere";
	public static final String TO_POSITION_KEY = "toPosition";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey(D_FERMETURE_KEY);
  }

  public void setDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, D_FERMETURE_KEY);
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey(D_OUVERTURE_KEY);
  }

  public void setDOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, D_OUVERTURE_KEY);
  }

  public String temVisible() {
    return (String) storedValueForKey(TEM_VISIBLE_KEY);
  }

  public void setTemVisible(String value) {
    takeStoredValueForKey(value, TEM_VISIBLE_KEY);
  }

  public String tpdCode() {
    return (String) storedValueForKey(TPD_CODE_KEY);
  }

  public void setTpdCode(String value) {
    takeStoredValueForKey(value, TPD_CODE_KEY);
  }

  public String tpdLibelleCourt() {
    return (String) storedValueForKey(TPD_LIBELLE_COURT_KEY);
  }

  public void setTpdLibelleCourt(String value) {
    takeStoredValueForKey(value, TPD_LIBELLE_COURT_KEY);
  }

  public String tpdLibelleLong() {
    return (String) storedValueForKey(TPD_LIBELLE_LONG_KEY);
  }

  public void setTpdLibelleLong(String value) {
    takeStoredValueForKey(value, TPD_LIBELLE_LONG_KEY);
  }

  public Integer tpdNiveau() {
    return (Integer) storedValueForKey(TPD_NIVEAU_KEY);
  }

  public void setTpdNiveau(Integer value) {
    takeStoredValueForKey(value, TPD_NIVEAU_KEY);
  }

  public org.cocktail.mangue.modele.grhum.onp.EOTypePositionDetail pere() {
    return (org.cocktail.mangue.modele.grhum.onp.EOTypePositionDetail)storedValueForKey(PERE_KEY);
  }

  public void setPereRelationship(org.cocktail.mangue.modele.grhum.onp.EOTypePositionDetail value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.onp.EOTypePositionDetail oldValue = pere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.onp.EOTypePosition toPosition() {
    return (org.cocktail.mangue.modele.grhum.onp.EOTypePosition)storedValueForKey(TO_POSITION_KEY);
  }

  public void setToPositionRelationship(org.cocktail.mangue.modele.grhum.onp.EOTypePosition value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.onp.EOTypePosition oldValue = toPosition();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_POSITION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_POSITION_KEY);
    }
  }
  

/**
 * Créer une instance de EOTypePositionDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypePositionDetail createEOTypePositionDetail(EOEditingContext editingContext, NSTimestamp dOuverture
, String temVisible
, String tpdCode
, String tpdLibelleCourt
, String tpdLibelleLong
, Integer tpdNiveau
, org.cocktail.mangue.modele.grhum.onp.EOTypePosition toPosition			) {
    EOTypePositionDetail eo = (EOTypePositionDetail) createAndInsertInstance(editingContext, _EOTypePositionDetail.ENTITY_NAME);    
		eo.setDOuverture(dOuverture);
		eo.setTemVisible(temVisible);
		eo.setTpdCode(tpdCode);
		eo.setTpdLibelleCourt(tpdLibelleCourt);
		eo.setTpdLibelleLong(tpdLibelleLong);
		eo.setTpdNiveau(tpdNiveau);
    eo.setToPositionRelationship(toPosition);
    return eo;
  }

  
	  public EOTypePositionDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypePositionDetail)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypePositionDetail creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypePositionDetail creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypePositionDetail object = (EOTypePositionDetail)createAndInsertInstance(editingContext, _EOTypePositionDetail.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypePositionDetail localInstanceIn(EOEditingContext editingContext, EOTypePositionDetail eo) {
    EOTypePositionDetail localInstance = (eo == null) ? null : (EOTypePositionDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypePositionDetail#localInstanceIn a la place.
   */
	public static EOTypePositionDetail localInstanceOf(EOEditingContext editingContext, EOTypePositionDetail eo) {
		return EOTypePositionDetail.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypePositionDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypePositionDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypePositionDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypePositionDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypePositionDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypePositionDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypePositionDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypePositionDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypePositionDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypePositionDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypePositionDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypePositionDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

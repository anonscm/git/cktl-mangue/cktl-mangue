/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.grhum.onp;

import org.cocktail.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOTypePositionDetail extends _EOTypePositionDetail {

	public static NSArray SORT_ARRAY_CODE_ASC = new NSArray(new EOSortOrdering(TPD_CODE_KEY, EOSortOrdering.CompareAscending));
	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(new EOSortOrdering(TPD_LIBELLE_LONG_KEY, EOSortOrdering.CompareAscending));

	public static final String C_ELEVER_ENFANT= "DSP40";

	public static final String C_DETACHEMENT_ENTRANT = "DEE";
	public static final String C_DETACHEMENT_SORTANT = "DET";
	public static final String C_DETACHEMENT_ENTRANT_SORTANT = "DES";

    public EOTypePositionDetail() {
        super();
    }

    public boolean estPourEnfant() {
    	return tpdCode().equals(C_ELEVER_ENFANT);
    }
    
    public boolean estDetachementEntrant() {
    	return StringCtrl.containsIgnoreCase(tpdCode(), C_DETACHEMENT_ENTRANT);
    }
    public boolean estDetachementSortant() {
    	return StringCtrl.containsIgnoreCase(tpdCode(), C_DETACHEMENT_SORTANT);
    }
    public boolean estDetachementEntrantSortant() {
    	return StringCtrl.containsIgnoreCase(tpdCode(), C_DETACHEMENT_ENTRANT_SORTANT);
    }

    
	public static NSArray findForTypePosition(EOEditingContext ec, EOTypePosition typePosition) {

		NSMutableArray qualifiers = new NSMutableArray();				
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_POSITION_KEY + "=%@", new NSArray(typePosition)));
				
		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_CODE_ASC);
	}

	
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

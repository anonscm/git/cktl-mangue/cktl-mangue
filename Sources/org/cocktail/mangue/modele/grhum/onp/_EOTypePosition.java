// _EOTypePosition.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypePosition.java instead.
package org.cocktail.mangue.modele.grhum.onp;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public abstract class _EOTypePosition extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "TypePosition";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TYPE_POSITION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "typPosId";

	public static final String C_POSITION_KEY = "cPosition";
	public static final String DUREE_MAX_PERIOD_POS_KEY = "dureeMaxPeriodPos";
	public static final String LC_POSITION_KEY = "lcPosition";
	public static final String LL_POSITION_KEY = "llPosition";
	public static final String PRCT_DROIT_AVCT_ECH_KEY = "prctDroitAvctEch";
	public static final String PRCT_DROIT_AVCT_GRAD_KEY = "prctDroitAvctGrad";
	public static final String TEM_CONTRAT_KEY = "temContrat";
	public static final String TEM_D_FIN_OBLIG_KEY = "temDFinOblig";
	public static final String TEM_NON_TITULAIRE_KEY = "temNonTitulaire";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TEM_ENFANT_KEY = "temEnfant";

// Attributs non visibles
	public static final String TYP_POS_ID_KEY = "typPosId";

//Colonnes dans la base de donnees
	public static final String C_POSITION_COLKEY = "C_POSITION";
	public static final String DUREE_MAX_PERIOD_POS_COLKEY = "DUREE_MAX_PERIOD_POS";
	public static final String LC_POSITION_COLKEY = "LC_POSITION";
	public static final String LL_POSITION_COLKEY = "LL_POSITION";
	public static final String PRCT_DROIT_AVCT_ECH_COLKEY = "PRCT_DROIT_AVCT_ECH";
	public static final String PRCT_DROIT_AVCT_GRAD_COLKEY = "PRCT_DROIT_AVCT_GRAD";
	public static final String TEM_CONTRAT_COLKEY = "TEM_CONTRAT";
	public static final String TEM_D_FIN_OBLIG_COLKEY = "TEM_D_FIN_OBLIG";
	public static final String TEM_NON_TITULAIRE_COLKEY = "TEM_NON_TITULAIRE";
	public static final String TEM_ENFANT_COLKEY = "TEM_ENFANT";

	public static final String TYP_POS_ID_COLKEY = "TYP_POS_ID";


	// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cPosition() {
    return (String) storedValueForKey(C_POSITION_KEY);
  }

  public void setCPosition(String value) {
    takeStoredValueForKey(value, C_POSITION_KEY);
  }

  public Integer dureeMaxPeriodPos() {
    return (Integer) storedValueForKey(DUREE_MAX_PERIOD_POS_KEY);
  }

  public void setDureeMaxPeriodPos(Integer value) {
    takeStoredValueForKey(value, DUREE_MAX_PERIOD_POS_KEY);
  }

  public String lcPosition() {
    return (String) storedValueForKey(LC_POSITION_KEY);
  }

  public void setLcPosition(String value) {
    takeStoredValueForKey(value, LC_POSITION_KEY);
  }

  public String llPosition() {
    return (String) storedValueForKey(LL_POSITION_KEY);
  }

  public void setLlPosition(String value) {
    takeStoredValueForKey(value, LL_POSITION_KEY);
  }

  public Integer prctDroitAvctEch() {
    return (Integer) storedValueForKey(PRCT_DROIT_AVCT_ECH_KEY);
  }

  public void setPrctDroitAvctEch(Integer value) {
    takeStoredValueForKey(value, PRCT_DROIT_AVCT_ECH_KEY);
  }

  public Integer prctDroitAvctGrad() {
    return (Integer) storedValueForKey(PRCT_DROIT_AVCT_GRAD_KEY);
  }

  public void setPrctDroitAvctGrad(Integer value) {
    takeStoredValueForKey(value, PRCT_DROIT_AVCT_GRAD_KEY);
  }

  public String temContrat() {
    return (String) storedValueForKey(TEM_CONTRAT_KEY);
  }

  public void setTemContrat(String value) {
    takeStoredValueForKey(value, TEM_CONTRAT_KEY);
  }

  public String temDFinOblig() {
    return (String) storedValueForKey(TEM_D_FIN_OBLIG_KEY);
  }

  public void setTemDFinOblig(String value) {
    takeStoredValueForKey(value, TEM_D_FIN_OBLIG_KEY);
  }

  public String temNonTitulaire() {
    return (String) storedValueForKey(TEM_NON_TITULAIRE_KEY);
  }

  public void setTemNonTitulaire(String value) {
    takeStoredValueForKey(value, TEM_NON_TITULAIRE_KEY);
  }

  public String temTitulaire() {
	    return (String) storedValueForKey(TEM_TITULAIRE_KEY);
	  }
  public void setTemTitulaire(String value) {
    takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
  }
  public String temEnfant() {
	    return (String) storedValueForKey(TEM_ENFANT_KEY);
	  }
public void setTemEnfant(String value) {
	    takeStoredValueForKey(value, TEM_ENFANT_KEY);
	  }


/**
 * Créer une instance de EOTypePosition avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypePosition createEOTypePosition(EOEditingContext editingContext, String cPosition
, String temContrat
, String temDFinOblig
, String temNonTitulaire
, String temTitulaire
			) {
    EOTypePosition eo = (EOTypePosition) createAndInsertInstance(editingContext, _EOTypePosition.ENTITY_NAME);    
		eo.setCPosition(cPosition);
		eo.setTemContrat(temContrat);
		eo.setTemDFinOblig(temDFinOblig);
		eo.setTemNonTitulaire(temNonTitulaire);
		eo.setTemTitulaire(temTitulaire);
    return eo;
  }

  
	  public EOTypePosition localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypePosition)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypePosition creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypePosition creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypePosition object = (EOTypePosition)createAndInsertInstance(editingContext, _EOTypePosition.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypePosition localInstanceIn(EOEditingContext editingContext, EOTypePosition eo) {
    EOTypePosition localInstance = (eo == null) ? null : (EOTypePosition)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypePosition#localInstanceIn a la place.
   */
	public static EOTypePosition localInstanceOf(EOEditingContext editingContext, EOTypePosition eo) {
		return EOTypePosition.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypePosition fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypePosition fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypePosition eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypePosition)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypePosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypePosition fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypePosition eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypePosition)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypePosition fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypePosition eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypePosition ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypePosition fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

//EOGroupeExclusion.java
//Created on Tue Sep 19 09:04:34 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.InclusionSituation;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class EOGroupeExclusion extends _EOGroupeExclusion implements RecordAvecLibelleEtCode {

	private static String TYPE_FONCTIONNAIRE = "Fonctionnaire";
	private static String TYPE_CONTRACTUEL = "Contractuel";

	public static EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LL_GROUPE_EXCLUSION_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);

	/** Un groupe d'exclusion gere un ensemble d'exclusions. Les inclusions de contrat et de corps sont aussi liees au groupe d'exclusion.<BR>
	 * Regles de validation : les longueurs des strings doivent etre respectees
	 */
	public EOGroupeExclusion() {
		super();
	}


	public void init() {
		setTypeGroupe(TYPE_FONCTIONNAIRE);
		setTemLocal(CocktailConstantes.VRAI);
	}
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (cGroupeExclusion() == null || cGroupeExclusion().length() > 3) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le code est obligatoire et ne doit pas comporter plus de 3 caractères !");
		}
		if (lcGroupeExclusion() == null || lcGroupeExclusion().length() > 20) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé court est obligatoire et ne doit pas comporter plus de 20 caractères !");
		}
		if (llGroupeExclusion() == null || llGroupeExclusion().length() > 40) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé long est obligatoire et ne doit pas comporter plus de 40 caractères !");
		}
		if (cTypeGroupeExclusion() == null || cTypeGroupeExclusion().length() > 1) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le type de groupe est obligatoire et ne doit pas comporter plus de 1s caractères !");
		}
	}
	public String typeGroupe() {
		if (cTypeGroupeExclusion() != null && cTypeGroupeExclusion().equals(TYPE_FONCTIONNAIRE.substring(0,1))) {
			return TYPE_FONCTIONNAIRE;
		} else {
			return TYPE_CONTRACTUEL;
		}
	}
	public void setTypeGroupe(String aString) {
		if (aString != null) {
			setCTypeGroupeExclusion(aString.substring(0,1));
		} else {
			setCTypeGroupeExclusion(null);
		}
	}
	public boolean estLocal() {
		return temLocal() != null && temLocal().equals(CocktailConstantes.VRAI);
	}
	public void setEstLocal(boolean aBool) {
		if (aBool) {
			setTemLocal(CocktailConstantes.VRAI);
		} else {
			setTemLocal(CocktailConstantes.FAUX);
		}
	}
	
	/**
	 * 
	 * @param exclusions
	 */
	public void supprimerExclusions(NSArray<EOExclusion> exclusions) {

		if (exclusions != null && exclusions.count() > 0) {
			for (EOExclusion exclusion : exclusions) {
				exclusion.supprimerRelations();
				editingContext().deleteObject(exclusion);
			}
		}
	}
	
	public NSArray exclusions() {
		return EOExclusion.findForGroupe(editingContext(), this);
	}
	// interface RecordAvecLibelleEtCode
	public String code() {
		return cGroupeExclusion();
	}
	public String libelle() {
		return llGroupeExclusion();
	}
	// méthodes privées
	private void supprimerInclusions(String nomEntite) {
		NSArray inclusions = InclusionSituation.rechercherInclusionsPourGroupeExclusion(editingContext(),nomEntite, this);
		if (inclusions != null && inclusions.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des " + nomEntite);
			java.util.Enumeration e = inclusions.objectEnumerator();
			while (e.hasMoreElements()) {
				InclusionSituation inclusion = (InclusionSituation)e.nextElement();
				inclusion.supprimerRelations();
				editingContext().deleteObject(inclusion);
			}
		}
	}
}

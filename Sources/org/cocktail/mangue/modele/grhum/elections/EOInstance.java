//EOInstance.java
//Created on Tue Sep 12 09:41:09 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.ParametrageElection;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamRepartElec;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamStrBv;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;


/** Une instance (election) est associe a des parametrages d'instance (ParamXX),
 * des parametrages finaux et une liste d'electeurs. Lors de la suppression d'une instance, 
 * on supprime tous parametrages finaux lies a cette instance<BR>
 * Regles de validation : les longueurs des strings doivent etre respectees., le type d'instance doit etre fourni<BR>
 */
public class EOInstance extends _EOInstance implements RecordAvecLibelleEtCode {

	public static EOSortOrdering SORT_SCRUTIN_DESC = new EOSortOrdering(D_SCRUTIN_KEY, EOSortOrdering.CompareDescending);
	public static NSArray SORT_ARRAY_SCRUTIN_DESC = new NSArray(SORT_SCRUTIN_DESC);
	public static EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LL_INSTANCE_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);

	public EOInstance() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param instance
	 * @param college
	 * @return
	 */
	public static EOInstance creer(EOEditingContext ec, EOTypeInstance typeInstance) {

		EOInstance newObject = new EOInstance();
		newObject.setTypeInstanceRelationship(typeInstance);
		
		ec.insertObject(newObject);
		return newObject;
	}

	public void initAvecTypeInstance(EOTypeInstance type) {
		setTemEditionCoherente(CocktailConstantes.VRAI);
		addObjectToBothSidesOfRelationshipWithKey(type,"typeInstance");
	}
	public void supprimerRelations() {
		// supprimer tous les params bv relatifs à cette instance
		supprimerParametrages();
		removeObjectFromBothSidesOfRelationshipWithKey(typeInstance(),"typeInstance");
	}
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (cInstance() == null || cInstance().length() > 3) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le code est obligatoire et ne doit pas comporter plus de 3 caractères !");
		}
		if (lcInstance() == null || lcInstance().length() > 20) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé court est obligatoire et ne doit pas comporter plus de 20 caractères !");
		}
		if (llInstance() == null || llInstance().length() > 40) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé long est obligatoire et ne doit pas comporter plus de 40 caractères !");
		}
		if (typeInstance() == null) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez fournir le type d'instance");
		}
	}
	public String dateScrutinFormatee() {
		return SuperFinder.dateFormatee(this,"dScrutin");
	}
	public void setDateScrutinFormatee(String uneDate) {
		// on change les dates, les éditions risquent d'être incohérentes
		setEstEditionCoherente(false);
		if (uneDate == null) {
			setDScrutin(null);
		} else {
			SuperFinder.setDateFormatee(this,"dScrutin",uneDate);
		}
	}  
	public String dateReferenceFormatee() {
		return SuperFinder.dateFormatee(this,"dReference");
	}
	public void setDateReferenceFormatee(String uneDate) {
		// on change les dates, les éditions risquent d'être incohérentes
		setEstEditionCoherente(false);
		if (uneDate == null) {
			setDScrutin(null);
		} else {
			SuperFinder.setDateFormatee(this,"dReference",uneDate);
		}
	}
	public boolean estEditionCoherente() {
		return temEditionCoherente() != null && temEditionCoherente().equals(CocktailConstantes.VRAI);
	}
	public void setEstEditionCoherente(boolean aBool) {
		if (aBool) {
			setTemEditionCoherente(CocktailConstantes.VRAI);
		} else {
			setTemEditionCoherente(CocktailConstantes.FAUX);
		}
	}
	public void supprimerParametrages() {
		supprimerParametrage("ParamBv");
		supprimerParametrage("ParamCnu");
		supprimerParametrage("ParamCollege");
		supprimerParametrage("ParamCompElec");
		supprimerParametrage("ParamCorps");
		supprimerParametrage("ParamSecteur");
		supprimerParametrage("ParamSectionElective");
		supprimerParametrageBvStr();
		supprimerParametrageFinal();
		supprimerElecteursPourInstance();
	}
	/** Supprime les parametrages finaux lies a cette instance */
	public void supprimerParametrageFinal() {
		NSArray params = EOParamRepartElec.rechercherParametragesPourInstance(editingContext(), this);
		if (params != null && params.count() > 0) {
			LogManager.logDetail("Suppression du paramétrage final de l'instance");
			java.util.Enumeration e = params.objectEnumerator();
			while (e.hasMoreElements()) {
				EOParamRepartElec repart = (EOParamRepartElec)e.nextElement();
				repart.supprimerRelations();
				editingContext().deleteObject(repart);
			}
		}
	}
	/** Supprime les &eacute;lecteurs li&eacute;s &agrave; cette instance */
	public void supprimerElecteursPourInstance() {
		NSArray electeurs = EOListeElecteur.rechercherElecteursPourInstance(this);
		if (electeurs != null && electeurs.count() > 0) {
			LogManager.logDetail("Suppression des électeurs de l'instance");
			java.util.Enumeration e = electeurs.objectEnumerator();
			while (e.hasMoreElements()) {
				EOListeElecteur electeur = (EOListeElecteur)e.nextElement();
				electeur.supprimerRelations();
				editingContext().deleteObject(electeur);
			}
		}
	}
	// interface RecordAvecLibelle
	public String libelle() {
		return llInstance();
	}
	public String code() {
		return cInstance();
	}
	// Méthodes privées
	private void supprimerParametrage(String nomEntite) {
		NSArray params = ParametrageElection.rechercherParametragesPourInstance(editingContext(), nomEntite, this);
		if (params != null && params.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression de " + nomEntite);
			java.util.Enumeration e = params.objectEnumerator();
			while (e.hasMoreElements()) {
				ParametrageElection param = (ParametrageElection)e.nextElement();
				param.supprimerRelations();
				editingContext().deleteObject(param);
			}
		}
	}
	private void supprimerParametrageBvStr() {
		NSArray params = EOParamStrBv.rechercherParamsPourInstanceEtBureau(editingContext(), this, null);
		if (params != null && params.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + "Suppression des paramStrBv");
			java.util.Enumeration e = params.objectEnumerator();
			while (e.hasMoreElements()) {
				EOParamStrBv param = (EOParamStrBv)e.nextElement();
				param.supprimerRelations();
				editingContext().deleteObject(param);
			}
		}
	}
	// Méthodes statiques
	public static NSArray<EOInstance> findForTypeInstance(EOEditingContext edc,EOTypeInstance typeInstance) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TYPE_INSTANCE_KEY + " = %@", new NSArray(typeInstance));
		return fetchAll(edc, qualifier, SORT_ARRAY_SCRUTIN_DESC);
	}
	

}

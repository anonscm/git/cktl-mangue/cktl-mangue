//EOInclusionCorps.java
//Created on Tue Sep 19 09:19:33 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.modele.grhum.EOCorps;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOInclusionCorps extends _EOInclusionCorps {

	public static final EOSortOrdering SORT_CORPS = new EOSortOrdering(CORPS_KEY+"."+EOCorps.LL_CORPS_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_EXCLUSION = new EOSortOrdering(GROUPE_EXCLUSION_KEY+"."+EOGroupeExclusion.LL_GROUPE_EXCLUSION_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_CATEGORIE = new EOSortOrdering(CATEGORIE_KEY+"."+EOCategorie.CODE_KEY, EOSortOrdering.CompareAscending);

	public static final NSArray SORT_ARRAY_CORPS = new NSArray(SORT_CORPS);
	public static final NSArray SORT_ARRAY_EXCLUSION = new NSArray(SORT_EXCLUSION);
	public static final NSArray SORT_ARRAY_CATEGORIE = new NSArray(SORT_CATEGORIE);

	public EOInclusionCorps() {
		super();
	}

	public void supprimerRelations() {
		supprimerRelations(true);
	}

	public void supprimerRelations(boolean modifierInstance) {
		super.supprimerRelations(modifierInstance);
		setCorpsRelationship(null);

	}

	/**
	 * 
	 * @param ec
	 * @param instance
	 * @param college
	 * @return
	 */
	public static EOInclusionCorps creer(EOEditingContext ec, EOCollege college, EOCorps corps, EOGroupeExclusion exclusion, EOCategorie categorie) {

		EOInclusionCorps newObject = (EOInclusionCorps) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.setCollegeRelationship(college);
		newObject.setCorpsRelationship(corps);
		newObject.setGroupeExclusionRelationship(exclusion);
		newObject.setCategorieRelationship(categorie);
		return newObject;
	}

	/**
	 * 
	 * @param edc
	 * @param college
	 * @param corps
	 * @param exclusion
	 * @param categorie
	 * @return
	 */
	public static NSArray<EOInclusionCorps> find(EOEditingContext edc, EOCollege college, EOCorps corps, EOGroupeExclusion exclusion, EOCategorie categorie) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		if (college != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " =%@", new NSArray(college)));

		if (corps != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CORPS_KEY + " =%@", new NSArray(corps)));

		if (exclusion != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(GROUPE_EXCLUSION_KEY + " =%@", new NSArray(exclusion)));

		if (categorie != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CATEGORIE_KEY + " =%@", new NSArray(categorie)));

		return fetchAll(edc, new EOAndQualifier(qualifiers), null);
	}

	/**
	 * 
	 * @param edc
	 * @param college
	 * @return
	 */
	public static NSArray<EOInclusionCorps> findCorps(EOEditingContext edc, EOCollege college) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " =%@", new NSArray(college)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(GROUPE_EXCLUSION_KEY + " = nil", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CATEGORIE_KEY + " = nil", null));

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_CORPS);
	}
	
	/**
	 * 
	 * @param edc
	 * @param college
	 * @return
	 */
	public static NSArray<EOInclusionCorps> findExclusions(EOEditingContext edc, EOCollege college, EOCorps corps) {

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " =%@", new NSArray(college)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CORPS_KEY + " =%@", new NSArray(corps)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CATEGORIE_KEY + " = nil", null));

			return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_EXCLUSION);
		}
		catch (Exception e) {
			return new NSArray<EOInclusionCorps>();
		}
	}
	/**
	 * 
	 * @param edc
	 * @param college
	 * @return
	 */
	public static NSArray<EOInclusionCorps> findCategories(EOEditingContext edc, EOCollege college, EOCorps corps, EOGroupeExclusion groupe) {

		try {

			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " =%@", new NSArray(college)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CORPS_KEY + " =%@", new NSArray(corps)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(GROUPE_EXCLUSION_KEY + " =%@", new NSArray(groupe)));

			return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_CATEGORIE);
		}
		catch (Exception e) {
			return new NSArray<EOInclusionCorps>();
		}
	}

	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		super.validateForSave();
		if (corps() == null) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez fournir un corps !");
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param typeInstance
	 * @param corps
	 * @return
	 */
	public static NSArray<EOInclusionCorps> rechercherInclusionsPourTypeInstanceEtCorps(EOEditingContext editingContext,EOTypeInstance typeInstance,EOCorps corps) {

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY+"."+EOCollege.TYPE_INSTANCE_KEY+" = %@", new NSArray(typeInstance)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CORPS_KEY + " = %@", new NSArray(corps)));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

}
//EOComposanteElective.java
//Created on Wed Sep 13 11:55:32 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCompElec;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamRepartElec;

import com.webobjects.foundation.NSArray;

/** Une composante elective est associee a des regroupements de composantes et de 
 * secteurs et a des parametrages d'instance (ParamCompElec) et des parametrages finaux.
 * Lors de la suppression d'une composante elective, on invalide toutes les instances des 
 * parametrages finaux lies a cette composante elective<BR>
 * Regles de validation : les longueurs des strings doivent etre respectees.<BR>
 */

public class EOComposanteElective extends _EOComposanteElective implements RecordAvecLibelleEtCode {
	public EOComposanteElective() {
		super();
	}

	public String code() {
		return cComposanteElective();
	}
	public String libelle() {
		return llComposanteElective();
	}
	
	/**
	 * 
	 */
	public void supprimerParametragesEtModifierInstance() {
		NSArray result = EORegroupementComposante.rechercherRegroupementsPourComposanteElective(editingContext(), this);
		if (result != null && result.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des regroupements de composante");
			java.util.Enumeration e = result.objectEnumerator();
			while (e.hasMoreElements()) {
				EORegroupementComposante regroupement = (EORegroupementComposante)e.nextElement();
				regroupement.supprimerRelations();
				editingContext().deleteObject(regroupement);
			}
		}
		result = EORegroupementSecteur.rechercherRegroupementsPourComposanteElective(editingContext(), this);
		if (result != null && result.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des regroupements de secteur");
			java.util.Enumeration e = result.objectEnumerator();
			while (e.hasMoreElements()) {
				EORegroupementSecteur regroupement = (EORegroupementSecteur)e.nextElement();
				regroupement.supprimerRelations();
				editingContext().deleteObject(regroupement);
			}
		}
		result = EOParamCompElec.rechercherParametragesPourComposante(editingContext(), this);
		if (result != null && result.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des paramétrages de composantes");
			java.util.Enumeration e = result.objectEnumerator();while (e.hasMoreElements()) {
				EOParamCompElec param = (EOParamCompElec)e.nextElement();
				param.supprimerRelations();
				editingContext().deleteObject(param);
			}
		}

		EOParamRepartElec.invaliderInstancesEtSupprimerParams(editingContext(), "composanteElective", this);
		EOListeElecteur.invaliderInstancesEtSuppressionElecteurs(editingContext(), "composanteElective", this);

	}
	
	/**
	 * 
	 */
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (cComposanteElective() == null || cComposanteElective().length() > 5) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le code est obligatoire et ne doit pas comporter plus de 5 caractères !");
		}
		if (lcComposanteElective() == null || lcComposanteElective().length() > 20) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé court est obligatoire et ne doit pas comporter plus de 20 caractères !");
		}
		if (llComposanteElective() == null || llComposanteElective().length() > 40) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé long est obligatoire et ne doit pas comporter plus de 40 caractères !");
		}
	}
	/** Retourne les regroupements de composante pour cette composante */
	public NSArray regroupements() {
		return EORegroupementComposante.rechercherRegroupementsPourComposanteElective(editingContext(), this);
	}

}

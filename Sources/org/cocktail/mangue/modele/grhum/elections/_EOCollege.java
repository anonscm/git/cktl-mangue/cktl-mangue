// _EOCollege.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCollege.java instead.
package org.cocktail.mangue.modele.grhum.elections;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOCollege extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "College";
	public static final String ENTITY_TABLE_NAME = "GRHUM.COLLEGE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noSeqCollege";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_CONTRAT_KEY = "dureeContrat";
	public static final String DUREE_INTERRUPTION_CONTRAT_KEY = "dureeInterruptionContrat";
	public static final String DUREE_MIN_CONTRAT_KEY = "dureeMinContrat";
	public static final String LC_COLLEGE_KEY = "lcCollege";
	public static final String LL_COLLEGE_KEY = "llCollege";
	public static final String NUM_ELC_ORDRE_KEY = "numElcOrdre";

// Attributs non visibles
	public static final String C_TYPE_INSTANCE_KEY = "cTypeInstance";
	public static final String NO_SEQ_COLLEGE_KEY = "noSeqCollege";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String DUREE_CONTRAT_COLKEY = "DUREE_CONTRAT";
	public static final String DUREE_INTERRUPTION_CONTRAT_COLKEY = "DUREE_INTERRUPTION_CTR";
	public static final String DUREE_MIN_CONTRAT_COLKEY = "DUREE_MIN_CONTRAT";
	public static final String LC_COLLEGE_COLKEY = "LC_COLLEGE";
	public static final String LL_COLLEGE_COLKEY = "LL_COLLEGE";
	public static final String NUM_ELC_ORDRE_COLKEY = "NUM_ELC_ORDRE";

	public static final String C_TYPE_INSTANCE_COLKEY = "C_TYPE_INSTANCE";
	public static final String NO_SEQ_COLLEGE_COLKEY = "NO_SEQ_COLLEGE";


	// Relationships
	public static final String TYPE_INSTANCE_KEY = "typeInstance";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer dureeContrat() {
    return (Integer) storedValueForKey(DUREE_CONTRAT_KEY);
  }

  public void setDureeContrat(Integer value) {
    takeStoredValueForKey(value, DUREE_CONTRAT_KEY);
  }

  public Integer dureeInterruptionContrat() {
    return (Integer) storedValueForKey(DUREE_INTERRUPTION_CONTRAT_KEY);
  }

  public void setDureeInterruptionContrat(Integer value) {
    takeStoredValueForKey(value, DUREE_INTERRUPTION_CONTRAT_KEY);
  }

  public Integer dureeMinContrat() {
    return (Integer) storedValueForKey(DUREE_MIN_CONTRAT_KEY);
  }

  public void setDureeMinContrat(Integer value) {
    takeStoredValueForKey(value, DUREE_MIN_CONTRAT_KEY);
  }

  public String lcCollege() {
    return (String) storedValueForKey(LC_COLLEGE_KEY);
  }

  public void setLcCollege(String value) {
    takeStoredValueForKey(value, LC_COLLEGE_KEY);
  }

  public String llCollege() {
    return (String) storedValueForKey(LL_COLLEGE_KEY);
  }

  public void setLlCollege(String value) {
    takeStoredValueForKey(value, LL_COLLEGE_KEY);
  }

  public Integer numElcOrdre() {
    return (Integer) storedValueForKey(NUM_ELC_ORDRE_KEY);
  }

  public void setNumElcOrdre(Integer value) {
    takeStoredValueForKey(value, NUM_ELC_ORDRE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.elections.EOTypeInstance typeInstance() {
    return (org.cocktail.mangue.modele.grhum.elections.EOTypeInstance)storedValueForKey(TYPE_INSTANCE_KEY);
  }

  public void setTypeInstanceRelationship(org.cocktail.mangue.modele.grhum.elections.EOTypeInstance value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.elections.EOTypeInstance oldValue = typeInstance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_INSTANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_INSTANCE_KEY);
    }
  }
  

/**
 * Créer une instance de EOCollege avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCollege createEOCollege(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String lcCollege
, String llCollege
, Integer numElcOrdre
, org.cocktail.mangue.modele.grhum.elections.EOTypeInstance typeInstance			) {
    EOCollege eo = (EOCollege) createAndInsertInstance(editingContext, _EOCollege.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcCollege(lcCollege);
		eo.setLlCollege(llCollege);
		eo.setNumElcOrdre(numElcOrdre);
    eo.setTypeInstanceRelationship(typeInstance);
    return eo;
  }

  
	  public EOCollege localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCollege)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCollege creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCollege creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCollege object = (EOCollege)createAndInsertInstance(editingContext, _EOCollege.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCollege localInstanceIn(EOEditingContext editingContext, EOCollege eo) {
    EOCollege localInstance = (eo == null) ? null : (EOCollege)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCollege#localInstanceIn a la place.
   */
	public static EOCollege localInstanceOf(EOEditingContext editingContext, EOCollege eo) {
		return EOCollege.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCollege fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCollege fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCollege eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCollege)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCollege fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCollege fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCollege eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCollege)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCollege fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCollege eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCollege ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCollege fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

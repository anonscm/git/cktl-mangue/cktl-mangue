//EOInclusionContrat.java
//Created on Tue Sep 19 09:17:27 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOInclusionContrat extends _EOInclusionContrat {

	public static final EOSortOrdering SORT_CONTRAT = new EOSortOrdering(TYPE_CONTRAT_TRAVAIL_KEY+"."+EOTypeContratTravail.LIBELLE_LONG_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_EXCLUSION = new EOSortOrdering(GROUPE_EXCLUSION_KEY+"."+EOGroupeExclusion.LL_GROUPE_EXCLUSION_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_CATEGORIE = new EOSortOrdering(CATEGORIE_KEY+"."+EOCategorie.CODE_KEY, EOSortOrdering.CompareAscending);

	public static final NSArray SORT_ARRAY_CORPS = new NSArray(SORT_CONTRAT);
	public static final NSArray SORT_ARRAY_EXCLUSION = new NSArray(SORT_EXCLUSION);
	public static final NSArray SORT_ARRAY_CATEGORIE = new NSArray(SORT_CATEGORIE);

	public EOInclusionContrat() {
		super();
	}
	
	
	/**
	 * 
	 * @param ec
	 * @param instance
	 * @param college
	 * @return
	 */
	public static EOInclusionContrat creer(EOEditingContext ec, EOCollege college, EOTypeContratTravail typeContrat, EOGroupeExclusion exclusion, EOCategorie categorie) {

		EOInclusionContrat newObject = (EOInclusionContrat) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.setCollegeRelationship(college);
		newObject.setTypeContratTravailRelationship(typeContrat);
		newObject.setGroupeExclusionRelationship(exclusion);
		newObject.setCategorieRelationship(categorie);
		return newObject;
	}

	/**
	 * 
	 * @param edc
	 * @param type
	 * @return
	 */
	public static NSArray<EOContrat> findForType(EOEditingContext edc, EOTypeContratTravail type) {
		try {			
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_CONTRAT_TRAVAIL_KEY + " = %@", new NSArray(type)));
			return fetchAll(edc, new EOAndQualifier(qualifiers), null);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOContrat>();
		}
	}
	/**
	 * 
	 * @param edc
	 * @param college
	 * @return
	 */
	public static NSArray<EOInclusionContrat> findContrats(EOEditingContext edc, EOCollege college) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " =%@", new NSArray(college)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(GROUPE_EXCLUSION_KEY + " = nil", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CATEGORIE_KEY + " = nil", null));

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_CORPS);
	}
	/**
	 * 
	 * @param edc
	 * @param college
	 * @return
	 */
	public static NSArray<EOInclusionContrat> findExclusions(EOEditingContext edc, EOCollege college, EOTypeContratTravail contrat) {

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " =%@", new NSArray(college)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_CONTRAT_TRAVAIL_KEY + " =%@", new NSArray(contrat)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CATEGORIE_KEY + " = nil", null));

			return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_EXCLUSION);
		}
		catch (Exception e) {
			return new NSArray<EOInclusionContrat>();
		}
	}
	/**
	 * 
	 * @param edc
	 * @param college
	 * @return
	 */
	public static NSArray<EOInclusionContrat> findCategories(EOEditingContext edc, EOCollege college, EOTypeContratTravail contrat, EOGroupeExclusion groupe) {

		try {

			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " =%@", new NSArray(college)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_CONTRAT_TRAVAIL_KEY + " =%@", new NSArray(contrat)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(GROUPE_EXCLUSION_KEY + " =%@", new NSArray(groupe)));

			return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_CATEGORIE);
		}
		catch (Exception e) {
			return new NSArray<EOInclusionContrat>();
		}
	}


	public void supprimerRelations() {
		supprimerRelations(true);
	}

	public void supprimerRelations(boolean modifierInstances) {
		super.supprimerRelations(modifierInstances);
		setTypeContratTravailRelationship(null);
	}
	
	/**
	 * 
	 */
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		super.validateForSave();
		if (typeContratTravail() == null) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez fournir un contrat de travail !");
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param typeInstance
	 * @param typeContrat
	 * @return
	 */
	public static NSArray<EOInclusionContrat> rechercherInclusionsPourTypeInstanceEtTypeContrat(EOEditingContext edc, EOTypeInstance typeInstance, EOTypeContratTravail typeContrat) {
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY+"."+EOCollege.TYPE_INSTANCE_KEY + " = %@", new NSArray(typeInstance)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_CONTRAT_TRAVAIL_KEY + " = %@", new NSArray(typeContrat)));
		
		return fetchAll(edc, new EOAndQualifier(qualifiers));
	}
}

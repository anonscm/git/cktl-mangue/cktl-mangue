//EOSecteur.java
//Created on Mon Sep 18 15:47:26 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSecteur;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
/** Une secteur est associe a des regroupements de secteurs et a des parametrages d'instance (ParamSecteur).<BR>
 * Lors de la suppression d'un secteur, on invalide toutes les instances des listes electorales lies a ce secteur<BR>
 * Regles de validation : les longueurs des strings doivent etre respectees.<BR>
 */

// 30/03/2010 - lors des suppressions de secteurs,... on supprime les électeurs
public class EOSecteur extends _EOSecteur implements RecordAvecLibelle {

	public static EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LL_SECTEUR_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);

	public EOSecteur() {
		super();
	}


	public void supprimerParametragesEtElecteurs() {
		NSArray result = EORegroupementSecteur.rechercherRegroupementsPourSecteur(editingContext(), this);
		if (result != null && result.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des regroupements de secteur");
			java.util.Enumeration e = result.objectEnumerator();
			while (e.hasMoreElements()) {
				EORegroupementSecteur regroupement = (EORegroupementSecteur)e.nextElement();
				regroupement.supprimerRelations();
				editingContext().deleteObject(regroupement);
			}
		}
		result = EOParamSecteur.rechercherParametragesPourCollegeEtSecteur(editingContext(), null,this);
		if (result != null && result.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des paramétrages de secteur");
			java.util.Enumeration e = result.objectEnumerator();
			while (e.hasMoreElements()) {
				EOParamSecteur param = (EOParamSecteur)e.nextElement();
				param.supprimerRelations();
				editingContext().deleteObject(param);
			}
		}
		EOListeElecteur.invaliderInstancesEtSuppressionElecteurs(editingContext(), "secteur", this);
	}
	
	/**
	 * 
	 */
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (cSecteur() == null || cSecteur().length() > 3) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le code est obligatoire et ne doit pas comporter plus de 3 caractères !");
		}
		if (lcSecteur() == null || lcSecteur().length() > 20) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé court est obligatoire et ne doit pas comporter plus de 20 caractères !");
		}
		if (llSecteur() == null || llSecteur().length() > 40) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé long est obligatoire et ne doit pas comporter plus de 40 caractères !");
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public NSArray composantesElectives() {
		return (NSArray)EORegroupementSecteur.rechercherRegroupementsPourSecteur(editingContext(), this).valueForKey("composante");
	}
	// Interface RecordAvecLibelle
	public String libelle() {
		return llSecteur();
	}
	
}


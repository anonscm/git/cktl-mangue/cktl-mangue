//EOTypeInstance.java
//Created on Mon Sep 11 13:40:19 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/** Une type d'instance est associ&eacute; &agrave; des instances, des coll&egrave;ges 
 * param&eacute;trages d'instance (ParamBv, ParamStrBv).<BR>
 * R&egrave;gles de validation : les longueurs des strings doivent &ecirc;tre respect&eacute;es. et des
 * param&eacute;trages d'instance<BR>
 */
// 23/02/2011 - Modification pour rajouter une méthode indiquant si il s'agit du type instance JDNPHU ou JDNPEH
public class EOTypeInstance extends _EOTypeInstance {
	private static final String TYPE_INSTANCE_JDNPHU = "JDNPHU";
	private static final String TYPE_INSTANCE_JDNPEH = "JDNPEH";
	
	public EOTypeInstance() {
		super();
	}

	public void init() {
		setTemElcCap(CocktailConstantes.FAUX);
		setTemElcCnu(CocktailConstantes.FAUX);
		setTemElcCs(CocktailConstantes.FAUX);
		setTemElcCses(CocktailConstantes.FAUX);
		setTemElcCufr(CocktailConstantes.FAUX);
		setTemSecteur(CocktailConstantes.FAUX);
		setTemLocal(CocktailConstantes.VRAI);
		setTemDoubleCarriere(CocktailConstantes.FAUX);
		setTemInsertionIndividus(CocktailConstantes.FAUX);
		setTemStrPrincipale(CocktailConstantes.VRAI);
	}
	/** Supprime toutes les param&eacute;trages des instances associ&eacute;es */
	public void supprimerParametragesInstances() {
		// Supprimer les paramétrages des instances
		if (instances() != null && instances().count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des des paramétrages d'instances");
			java.util.Enumeration e = instances().objectEnumerator();
			while (e.hasMoreElements()) {
				EOInstance instance = (EOInstance)e.nextElement();
				instance.supprimerParametrages();
			}
		}
	}
	/** Supprime tos les param&eacute;trages des coll&egrave;ges associ&eacute;s et retourne la liste des coll&egrave;ges
	*/
	public NSArray supprimerParametragesColleges() {
		NSArray result = EOCollege.findForTypeInstance(editingContext(), this);
		if (result != null && result.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des paramétrages de collèges");
			java.util.Enumeration e = result.objectEnumerator();
			while (e.hasMoreElements()) {
				EOCollege college = (EOCollege)e.nextElement();
				college.supprimerParametrages();
			}
		}
		return result;
	}
	/** Supprime toutes les instances et tous les coll&egrave;ges associ&eacute;s et */
	public void supprimerInstancesEtColleges(NSArray colleges) {
		// Supprimer les paramétrages des instances
		if (instances() != null && instances().count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des instances");
			java.util.Enumeration e = instances().objectEnumerator();
			while (e.hasMoreElements()) {
				EOInstance instance = (EOInstance)e.nextElement();
				editingContext().deleteObject(instance);
			}
		}
		if (colleges != null && colleges.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression des collèges");
			java.util.Enumeration e = colleges.objectEnumerator();
			while (e.hasMoreElements()) {
				EOCollege college = (EOCollege)e.nextElement();
				college.supprimerRelations();
				editingContext().deleteObject(college);
			}
		}
	}
	public void supprimerAutresDonneesEtEnregistrer() throws Exception {
		try {
			supprimerParametragesInstances();
			editingContext().saveChanges();
			NSArray colleges = supprimerParametragesColleges();
			editingContext().saveChanges();
			supprimerInstancesEtColleges(colleges);
			editingContext().saveChanges();
		} catch (Exception e) {
			throw e;
		}
	}
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (cTypeInstance() == null || cTypeInstance().length() > 7) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le code est obligatoire et ne doit pas comporter plus de 7 caractères !");
		}
		if (lcTypeInstance() == null || lcTypeInstance().length() > 20) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé court est obligatoire et ne doit pas comporter plus de 20 caractères !");
		}
		if (llTypeInstance() == null || llTypeInstance().length() > 40) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé long est obligatoire et ne doit pas comporter plus de 40 caractères !");
		}
	}
	public boolean estElcCap() {
		return temElcCap() != null && temElcCap().equals(CocktailConstantes.VRAI);
	}	
	public void setEstElcCap(boolean aBool) {
		if (aBool) {
			setTemElcCap(CocktailConstantes.VRAI);
		} else {
			setTemElcCap(CocktailConstantes.FAUX);
		}
	}
	public boolean estElcCnu() {
		return temElcCnu() != null && temElcCnu().equals(CocktailConstantes.VRAI);
	}	
	public void setEstElcCnu(boolean aBool) {
		if (aBool) {
			setTemElcCnu(CocktailConstantes.VRAI);
		} else {
			setTemElcCnu(CocktailConstantes.FAUX);
		}
	}
	public boolean estElcCs() {
		return temElcCs() != null && temElcCs().equals(CocktailConstantes.VRAI);
	}	
	public void setEstElcCs(boolean aBool) {
		if (aBool) {
			setTemElcCs(CocktailConstantes.VRAI);
		} else {
			setTemElcCs(CocktailConstantes.FAUX);
		}
	}
	public boolean estElcCses() {
		return temElcCses() != null && temElcCses().equals(CocktailConstantes.VRAI);
	}	
	public void setEstElcCses(boolean aBool) {
		if (aBool) {
			setTemElcCses(CocktailConstantes.VRAI);
		} else {
			setTemElcCses(CocktailConstantes.FAUX);
		}
	}
	public boolean estElcCufr() {
		return temElcCufr() != null && temElcCufr().equals(CocktailConstantes.VRAI);
	}	
	public void setEstElcCufr(boolean aBool) {
		if (aBool) {
			setTemElcCufr(CocktailConstantes.VRAI);
		} else {
			setTemElcCufr(CocktailConstantes.FAUX);
		}
	}
	public boolean estLocal() {
		return temLocal() != null && temLocal().equals(CocktailConstantes.VRAI);
	}	
	public void setEstLocal(boolean aBool) {
		if (aBool) {
			setTemLocal(CocktailConstantes.VRAI);
		} else {
			setTemLocal(CocktailConstantes.FAUX);
		}
	}
	public boolean estDoubleCarriere() {
		return temDoubleCarriere() != null && temDoubleCarriere().equals(CocktailConstantes.VRAI);
	}	
	public void setEstDoubleCarriere(boolean aBool) {
		if (aBool) {
			setTemDoubleCarriere(CocktailConstantes.VRAI);
		} else {
			setTemDoubleCarriere(CocktailConstantes.FAUX);
		}
	}
	public boolean estSectorise() {
		return temSecteur() != null && temSecteur().equals(CocktailConstantes.VRAI);
	}	
	public void setEstSectorise(boolean aBool) {
		if (aBool) {
			setTemSecteur(CocktailConstantes.VRAI);
		} else {
			setTemSecteur(CocktailConstantes.FAUX);
		}
	}
	public boolean estRattachementAffectationPrincipale() {
		return temStrPrincipale() != null && temStrPrincipale().equals(CocktailConstantes.VRAI);
	}	
	public void setEstRattachementAffectationPrincipale(boolean aBool) {
		if (aBool) {
			setTemStrPrincipale(CocktailConstantes.VRAI);
		} else {
			setTemStrPrincipale(CocktailConstantes.FAUX);
		}
	}
	public boolean peutInsererIndividus() {
		return temInsertionIndividus() != null && temInsertionIndividus().equals(CocktailConstantes.VRAI);
	}	
	public void setPeutInsererIndividus(boolean aBool) {
		if (aBool) {
			setTemInsertionIndividus(CocktailConstantes.VRAI);
		} else {
			setTemInsertionIndividus(CocktailConstantes.FAUX);
		}
	}
	public boolean estTypeJuridDiscHu() {
		return cTypeInstance() != null && cTypeInstance().equals(TYPE_INSTANCE_JDNPHU);
	}
	public boolean estTypeJuridDiscHuOd() {
		return cTypeInstance() != null && cTypeInstance().equals(TYPE_INSTANCE_JDNPEH);
	}
	public NSArray colleges() {
		return EOCollege.findForTypeInstance(editingContext(), this);
	}
	public NSArray secteurs() {
		if (estSectorise()) {
			return SuperFinder.rechercherEntite(editingContext(), "Secteur");
		} else {
			return null;
		}
	}
	public NSArray sectionsElectives() {
		if (estElcCses()) {
			return SuperFinder.rechercherEntite(editingContext(), "SectionElective");
		} else {
			return null;
		}
	}
	// Méthodes statiques
	public static EOTypeInstance typeInstanceJDNPHU(EOEditingContext editingContext) {
		return (EOTypeInstance)SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext, "TypeInstance", "cTypeInstance", TYPE_INSTANCE_JDNPHU);
	}
}

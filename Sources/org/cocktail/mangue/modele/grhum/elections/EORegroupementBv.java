//EORegroupementBv.java
//Created on Fri Feb 23 09:58:00 Europe/Paris 2007 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EORegroupementBv extends _EORegroupementBv {


	public void initAvecBureauVoteEtStructure(EOBureauVote bureau,EOStructure structure) {
		setTemStructuresFilles(CocktailConstantes.FAUX);
		setBureauVoteRelationship(bureau);
		setStructureRelationship(structure);
	}

	public void supprimerRelations() {
		setBureauVoteRelationship(null);
		setStructureRelationship(null);
	}

	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (bureauVote() == null) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez fournir un bureau de vote");
		}
		if (structure() == null) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez fournir une structure");
		}
	}
	public boolean selectionnerStructuresFilles() {
		return temStructuresFilles() != null && temStructuresFilles().equals(CocktailConstantes.VRAI);
	}	
	public void setSelectionnerStructuresFilles(boolean aBool) {
		if (aBool) {
			setTemStructuresFilles(CocktailConstantes.VRAI);
		} else {
			setTemStructuresFilles(CocktailConstantes.FAUX);
		}
	}

	/**
	 * 
	 * @param edc
	 * @param bureauVote
	 * @return
	 */
	public static NSArray<EORegroupementBv> rechercherRegroupementsPourBureauDeVote(EOEditingContext edc,EOBureauVote bureauVote) {
		return fetchAll(edc, getQualifierBureauVote(bureauVote), null);
	}
	/**
	 * 
	 * @param edc
	 * @param structure
	 * @return
	 */
	public static NSArray rechercherRegroupementsPourStructure(EOEditingContext edc,EOStructure structure) {
		return fetchAll(edc, getQualifierStructure(structure), null);
	}
	/**
	 * 
	 * @param edc
	 * @param bureauVote
	 * @param structure
	 * @return
	 */
	public static NSArray rechercherRegroupementsPourBureauVoteEtStructure(EOEditingContext edc,EOBureauVote bureauVote,EOStructure structure) {
		NSMutableArray qualifiers = new NSMutableArray(bureauVote);

		qualifiers.addObject(getQualifierBureauVote(bureauVote));
		qualifiers.addObject(getQualifierStructure(structure));

		return fetchAll(edc, new EOAndQualifier(qualifiers), null);
	}

	public static EOQualifier getQualifierBureauVote(EOBureauVote bureau) {
		return EOQualifier.qualifierWithQualifierFormat(BUREAU_VOTE_KEY + "=%@", new NSArray(bureau));
	}
	public static EOQualifier getQualifierStructure(EOStructure structure) {
		return EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY + "=%@", new NSArray(structure));
	}

}
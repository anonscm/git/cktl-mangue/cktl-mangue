//EOInclusionDiplome.java
//Created on Tue Sep 19 09:20:54 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

//31/03/2010 - modifié pour ajouter un booléen signalant si on modifie les instances ou non
public class EOInclusionDiplome extends _EOInclusionDiplome {

	public static final EOSortOrdering SORT_DIPLOME = new EOSortOrdering(DIPLOME_KEY+"."+EODiplomes.LIBELLE_LONG_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_DIPLOME = new NSArray(SORT_DIPLOME);

	public EOInclusionDiplome() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param college
	 * @param diplome
	 * @return
	 */
	public static EOInclusionDiplome creer(EOEditingContext ec, EOCollege college, EODiplomes diplome) {

		EOInclusionDiplome newObject = (EOInclusionDiplome) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.setCollegeRelationship(college);
		newObject.setDiplomeRelationship(diplome);
		return newObject;
	}
	


	/**
	 * 
	 * @param edc
	 * @param college
	 * @return
	 */
	public static NSArray<EOInclusionDiplome> find(EOEditingContext edc, EOCollege college) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " =%@", new NSArray(college)));

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_DIPLOME);
	}
	public void initAvecCollegeEtDiplome(EOCollege college,EODiplomes diplome) {
		super.initAvecCollege(college);
		addObjectToBothSidesOfRelationshipWithKey(diplome,"diplome");
	}
	public void supprimerRelations() {
		supprimerRelations(true);
	}
	public void supprimerRelations(boolean modifierInstances) {
		super.supprimerRelations(modifierInstances);
		removeObjectFromBothSidesOfRelationshipWithKey(diplome(),"diplome");
	}
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		super.validateForSave();
		if (diplome() == null) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez fournir un diplôme");
		}
	}
}

// _EOTypeInstance.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeInstance.java instead.
package org.cocktail.mangue.modele.grhum.elections;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOTypeInstance extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "TypeInstance";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TYPE_INSTANCE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cTypeInstance";

	public static final String C_TYPE_INSTANCE_KEY = "cTypeInstance";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_TYPE_INSTANCE_KEY = "lcTypeInstance";
	public static final String LL_TYPE_INSTANCE_KEY = "llTypeInstance";
	public static final String TEM_DOUBLE_CARRIERE_KEY = "temDoubleCarriere";
	public static final String TEM_ELC_CAP_KEY = "temElcCap";
	public static final String TEM_ELC_CNU_KEY = "temElcCnu";
	public static final String TEM_ELC_CS_KEY = "temElcCs";
	public static final String TEM_ELC_CSES_KEY = "temElcCses";
	public static final String TEM_ELC_CUFR_KEY = "temElcCufr";
	public static final String TEM_INSERTION_INDIVIDUS_KEY = "temInsertionIndividus";
	public static final String TEM_LOCAL_KEY = "temLocal";
	public static final String TEM_SECTEUR_KEY = "temSecteur";
	public static final String TEM_STR_PRINCIPALE_KEY = "temStrPrincipale";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_TYPE_INSTANCE_COLKEY = "C_TYPE_INSTANCE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LC_TYPE_INSTANCE_COLKEY = "LC_TYPE_INSTANCE";
	public static final String LL_TYPE_INSTANCE_COLKEY = "LL_TYPE_INSTANCE";
	public static final String TEM_DOUBLE_CARRIERE_COLKEY = "TEM_DOUBLE_CARRIERE";
	public static final String TEM_ELC_CAP_COLKEY = "TEM_ELC_CAP";
	public static final String TEM_ELC_CNU_COLKEY = "TEM_ELC_CNU";
	public static final String TEM_ELC_CS_COLKEY = "TEM_ELC_CS";
	public static final String TEM_ELC_CSES_COLKEY = "TEM_ELC_CSES";
	public static final String TEM_ELC_CUFR_COLKEY = "TEM_ELC_CUFR";
	public static final String TEM_INSERTION_INDIVIDUS_COLKEY = "TEM_INSERTION_INDIVIDUS";
	public static final String TEM_LOCAL_COLKEY = "TEM_LOCAL";
	public static final String TEM_SECTEUR_COLKEY = "TEM_SECTEUR";
	public static final String TEM_STR_PRINCIPALE_COLKEY = "TEM_STR_PRINCIPALE";



	// Relationships
	public static final String INSTANCES_KEY = "instances";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cTypeInstance() {
    return (String) storedValueForKey(C_TYPE_INSTANCE_KEY);
  }

  public void setCTypeInstance(String value) {
    takeStoredValueForKey(value, C_TYPE_INSTANCE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String lcTypeInstance() {
    return (String) storedValueForKey(LC_TYPE_INSTANCE_KEY);
  }

  public void setLcTypeInstance(String value) {
    takeStoredValueForKey(value, LC_TYPE_INSTANCE_KEY);
  }

  public String llTypeInstance() {
    return (String) storedValueForKey(LL_TYPE_INSTANCE_KEY);
  }

  public void setLlTypeInstance(String value) {
    takeStoredValueForKey(value, LL_TYPE_INSTANCE_KEY);
  }

  public String temDoubleCarriere() {
    return (String) storedValueForKey(TEM_DOUBLE_CARRIERE_KEY);
  }

  public void setTemDoubleCarriere(String value) {
    takeStoredValueForKey(value, TEM_DOUBLE_CARRIERE_KEY);
  }

  public String temElcCap() {
    return (String) storedValueForKey(TEM_ELC_CAP_KEY);
  }

  public void setTemElcCap(String value) {
    takeStoredValueForKey(value, TEM_ELC_CAP_KEY);
  }

  public String temElcCnu() {
    return (String) storedValueForKey(TEM_ELC_CNU_KEY);
  }

  public void setTemElcCnu(String value) {
    takeStoredValueForKey(value, TEM_ELC_CNU_KEY);
  }

  public String temElcCs() {
    return (String) storedValueForKey(TEM_ELC_CS_KEY);
  }

  public void setTemElcCs(String value) {
    takeStoredValueForKey(value, TEM_ELC_CS_KEY);
  }

  public String temElcCses() {
    return (String) storedValueForKey(TEM_ELC_CSES_KEY);
  }

  public void setTemElcCses(String value) {
    takeStoredValueForKey(value, TEM_ELC_CSES_KEY);
  }

  public String temElcCufr() {
    return (String) storedValueForKey(TEM_ELC_CUFR_KEY);
  }

  public void setTemElcCufr(String value) {
    takeStoredValueForKey(value, TEM_ELC_CUFR_KEY);
  }

  public String temInsertionIndividus() {
    return (String) storedValueForKey(TEM_INSERTION_INDIVIDUS_KEY);
  }

  public void setTemInsertionIndividus(String value) {
    takeStoredValueForKey(value, TEM_INSERTION_INDIVIDUS_KEY);
  }

  public String temLocal() {
    return (String) storedValueForKey(TEM_LOCAL_KEY);
  }

  public void setTemLocal(String value) {
    takeStoredValueForKey(value, TEM_LOCAL_KEY);
  }

  public String temSecteur() {
    return (String) storedValueForKey(TEM_SECTEUR_KEY);
  }

  public void setTemSecteur(String value) {
    takeStoredValueForKey(value, TEM_SECTEUR_KEY);
  }

  public String temStrPrincipale() {
    return (String) storedValueForKey(TEM_STR_PRINCIPALE_KEY);
  }

  public void setTemStrPrincipale(String value) {
    takeStoredValueForKey(value, TEM_STR_PRINCIPALE_KEY);
  }

  public NSArray instances() {
    return (NSArray)storedValueForKey(INSTANCES_KEY);
  }

  public NSArray instances(EOQualifier qualifier) {
    return instances(qualifier, null, false);
  }

  public NSArray instances(EOQualifier qualifier, boolean fetch) {
    return instances(qualifier, null, fetch);
  }

  public NSArray instances(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.grhum.elections.EOInstance.TYPE_INSTANCE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.grhum.elections.EOInstance.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = instances();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInstancesRelationship(org.cocktail.mangue.modele.grhum.elections.EOInstance object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INSTANCES_KEY);
  }

  public void removeFromInstancesRelationship(org.cocktail.mangue.modele.grhum.elections.EOInstance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INSTANCES_KEY);
  }

  public org.cocktail.mangue.modele.grhum.elections.EOInstance createInstancesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Instance");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INSTANCES_KEY);
    return (org.cocktail.mangue.modele.grhum.elections.EOInstance) eo;
  }

  public void deleteInstancesRelationship(org.cocktail.mangue.modele.grhum.elections.EOInstance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INSTANCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInstancesRelationships() {
    Enumeration objects = instances().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInstancesRelationship((org.cocktail.mangue.modele.grhum.elections.EOInstance)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOTypeInstance avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeInstance createEOTypeInstance(EOEditingContext editingContext, String cTypeInstance
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcTypeInstance
, String llTypeInstance
, String temDoubleCarriere
, String temElcCap
, String temElcCnu
, String temElcCs
, String temElcCses
, String temElcCufr
, String temInsertionIndividus
, String temLocal
, String temSecteur
, String temStrPrincipale
			) {
    EOTypeInstance eo = (EOTypeInstance) createAndInsertInstance(editingContext, _EOTypeInstance.ENTITY_NAME);    
		eo.setCTypeInstance(cTypeInstance);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcTypeInstance(lcTypeInstance);
		eo.setLlTypeInstance(llTypeInstance);
		eo.setTemDoubleCarriere(temDoubleCarriere);
		eo.setTemElcCap(temElcCap);
		eo.setTemElcCnu(temElcCnu);
		eo.setTemElcCs(temElcCs);
		eo.setTemElcCses(temElcCses);
		eo.setTemElcCufr(temElcCufr);
		eo.setTemInsertionIndividus(temInsertionIndividus);
		eo.setTemLocal(temLocal);
		eo.setTemSecteur(temSecteur);
		eo.setTemStrPrincipale(temStrPrincipale);
    return eo;
  }

  
	  public EOTypeInstance localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeInstance)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeInstance creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeInstance creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypeInstance object = (EOTypeInstance)createAndInsertInstance(editingContext, _EOTypeInstance.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeInstance localInstanceIn(EOEditingContext editingContext, EOTypeInstance eo) {
    EOTypeInstance localInstance = (eo == null) ? null : (EOTypeInstance)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeInstance#localInstanceIn a la place.
   */
	public static EOTypeInstance localInstanceOf(EOEditingContext editingContext, EOTypeInstance eo) {
		return EOTypeInstance.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeInstance fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeInstance fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeInstance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeInstance)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeInstance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeInstance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeInstance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeInstance)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeInstance fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeInstance eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeInstance ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeInstance fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

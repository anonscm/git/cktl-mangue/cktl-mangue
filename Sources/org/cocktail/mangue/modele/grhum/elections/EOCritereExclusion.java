// EOCritereExclusion.java
// Created on Thu Oct 05 13:52:46 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAbsence;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOCritereExclusion extends EOGenericRecord {
	public static String TYPE_ENTIER = "i";
	public static String TYPE_FLOAT = "f";
	public static String TYPE_STRING = "S";
    public EOCritereExclusion() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOCritereExclusion(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String llCritere() {
        return (String)storedValueForKey("llCritere");
    }

    public void setLlCritere(String value) {
        takeStoredValueForKey(value, "llCritere");
    }

    public String critereAttribut() {
        return (String)storedValueForKey("critereAttribut");
    }

    public void setCritereAttribut(String value) {
        takeStoredValueForKey(value, "critereAttribut");
    }

    public String critereOperateur() {
        return (String)storedValueForKey("critereOperateur");
    }

    public void setCritereOperateur(String value) {
        takeStoredValueForKey(value, "critereOperateur");
    }

    public String critereValeur() {
        return (String)storedValueForKey("critereValeur");
    }

    public void setCritereValeur(String value) {
        takeStoredValueForKey(value, "critereValeur");
    }

    public String critereTypeValeur() {
        return (String)storedValueForKey("critereTypeValeur");
    }

    public void setCritereTypeValeur(String value) {
        takeStoredValueForKey(value, "critereTypeValeur");
    }

    public org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion typeExclusion() {
        return (org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion)storedValueForKey("typeExclusion");
    }

    public void setTypeExclusion(org.cocktail.mangue.modele.grhum.elections.EOTypeExclusion value) {
        takeStoredValueForKey(value, "typeExclusion");
    }
    // méthodes ajoutées
    public boolean concerneToutAgent() {
    		return critereAttribut() == null;
    }
    /** retourne la liste des crit&egrave;res associ&eacute;s &agrave; un type d'absence, tri&eacute; par type d'exclusion,
     * le crit&egrave;re pour tous les agents, si il existe, &eacute;tant plac&eacute; en dernier
     */
    public static NSArray rechercherCriteresPourTypeAbsence(EOEditingContext editingContext,EOTypeAbsence typeAbsence) {
    		NSArray typesExclusion = SuperFinder.rechercherAvecAttributEtValeurEgale(editingContext,"TypeExclusion","typeAbsence",typeAbsence);
    		if (typesExclusion.count() == 0) {
    			return new NSArray();
    		}
    		EOQualifier qualifier = SuperFinder.construireORQualifier("cTypeExclusion",(NSArray)typesExclusion.valueForKey("cTypeExclusion"));
    		EOFetchSpecification fs = new EOFetchSpecification("CritereExclusion",qualifier,new NSArray(EOSortOrdering.sortOrderingWithKey("cTypeExclusion",EOSortOrdering.CompareAscending)));
    		NSArray criteres = editingContext.objectsWithFetchSpecification(fs);
    		EOCritereExclusion lastCritere = null;
    		NSMutableArray result = new NSMutableArray(criteres.count());
    		java.util.Enumeration e = criteres.objectEnumerator();
    		while (e.hasMoreElements()) {
    			EOCritereExclusion critere = (EOCritereExclusion)e.nextElement();
    			if (critere.concerneToutAgent()) {
    				lastCritere = critere;
    			} else {
    				result.addObject(critere);
    			}
    		}
    		if (lastCritere != null) {
    			result.addObject(lastCritere);
    		}
    		return result;
    }
}

// EOCollegeSectionElective.java
// Created on Wed Nov 17 09:17:59 Europe/Paris 2010 by Apple EOModeler Version 5.2

package org.cocktail.mangue.modele.grhum.elections;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/** Cette classe est utilisee pour saisir les relations entre les colleges et les sections electives. Ces
 * relations sont utilisees uniquement dans le cas des elections des membres de la juridiction disciplinaire
 * competente a l'egard des personnels enseignants des CHU (type d'instance : 'JDNPHU')
 *
 */
public class EOCollegeSectionElective extends _EOCollegeSectionElective {

	public final static EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(SECTION_ELECTIVE_KEY+"."+EOSectionElective.LL_SECTION_ELECTIVE_KEY, EOSortOrdering.CompareAscending);
	public final static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);
	
    public EOCollegeSectionElective() {
        super();
    }

    /**
     * 
     * @param college
     * @param section
     */
    public void initAvecCollegeEtSection(EOCollege college,EOSectionElective section) {
    	setCollegeRelationship(college);
    	setSectionElectiveRelationship(section);
    }
    
    /**
     * 
     */
    public void supprimerRelations() {
    	setCollegeRelationship(null);
    	setSectionElectiveRelationship(null);
    	setTypeRecrutementRelationship(null);
    }

    /**
     * 
     */
    public void validateForSave() {
    	if (college() == null) {
    		throw new NSValidation.ValidationException("Vous devez fournir un collège !");
    	}
    	if (sectionElective() == null) {
    		throw new NSValidation.ValidationException("Vous devez fournir une sectionElective !");
    	}
    }
    // Méthodes statiques
    public static NSArray<EOCollegeSectionElective> findForCollege(EOEditingContext edc, EOCollege college,boolean shouldRefresh) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(COLLEGE_KEY + " = %@", new NSArray(college));
    	return fetchAll(edc, qualifier, SORT_ARRAY_LIBELLE_ASC);
    }
    public static NSArray<EOCollegeSectionElective> findForSection(EOEditingContext edc, EOSectionElective section,boolean shouldRefresh) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(SECTION_ELECTIVE_KEY + " = %@", new NSArray(section));
    	return fetchAll(edc, qualifier, SORT_ARRAY_LIBELLE_ASC);
    }
    public static NSArray<EOCollegeSectionElective> findSansTypeRecrutement(EOEditingContext edc,boolean shouldRefresh) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TYPE_RECRUTEMENT_KEY + " = nil",null);
    	return fetchAll(edc, qualifier, SORT_ARRAY_LIBELLE_ASC);
    }
}

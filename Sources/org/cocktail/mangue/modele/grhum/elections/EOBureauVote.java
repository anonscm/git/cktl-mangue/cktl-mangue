//EOBureauVote.java
//Created on Thu Sep 21 15:44:02 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamBv;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamRepartElec;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamStrBv;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/** Une bureau de vote est associ&e a des regroupements de bureaux de vote et a des 
 * parametrages d'instance (ParamBv, ParamStrBv) et des parametrages finaux. Lors de la suppression
 * d'un bureau de vote, on invalide toutes les instances des parametrages finaux lies a ce bureau de vote<BR>
 * Regles de validation : les longueurs des strings doivent etre respectees. et des
 * parametrages d'instance<BR>
 */

public class EOBureauVote extends _EOBureauVote implements RecordAvecLibelle{

	public static EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LL_BUREAU_VOTE_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);
	
	public static final String ADRESSE_COMPLETE_KEY = "adresseBureau";

	public EOBureauVote() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public String adresseBureau() {
		if (adresse() != null) {
			
			return adresse().adrAdresse1() + " " + adresse().adrAdresse2() + " " + adresse().codePostal() + " " + adresse().ville();
			
		}
		return null;
	}
	
	/**
	 * 
	 * @param adresse
	 */
	public void initAvecAdresse(EOAdresse adresse) {
		setAdresseRelationship(adresse);
	}

	/**
	 * 
	 */
	public void supprimerParametragesEtModifierInstance() {
		setAdresseRelationship(null);

		for (EORegroupementBv myRegroupement : regroupementsBv()) {
			myRegroupement.supprimerRelations();
			editingContext().deleteObject(myRegroupement);
		}
		supprimerParametrages();
		EOParamRepartElec.invaliderInstancesEtSupprimerParams(editingContext(), "bureauVote", this);
		EOListeElecteur.invaliderInstancesEtSuppressionElecteurs(editingContext(), "bureauVote", this);
	}
	
	/**
	 * 
	 */
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (cBureauVote() == null || cBureauVote().length() > 3) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le code est obligatoire et ne doit pas comporter plus de 3 caractères !");
		}
		if (lcBureauVote() == null || lcBureauVote().length() > 20) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé court est obligatoire et ne doit pas comporter plus de 20 caractères !");
		}
		if (llBureauVote() == null || llBureauVote().length() > 40) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé long est obligatoire et ne doit pas comporter plus de 40 caractères !");
		}
		if (adresse() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une adresse");
		}
	}
	
	/** Retourne les regroupements de bureaux de vote pour ce bureau de vote */
	public NSArray<EORegroupementBv> regroupementsBv() {
		return EORegroupementBv.rechercherRegroupementsPourBureauDeVote(editingContext(), this);
	}

	public String libelle() {
		return llBureauVote();
	}
	
	//  méthodes privées
	private void supprimerParametrages() {
		LogManager.logDetail(this.getClass().getName() + " - Suppression des ParamBv");
		NSArray params = EOParamBv.rechercherParametragePourBureauVote(editingContext(), this);
		java.util.Enumeration e = params.objectEnumerator();
		if (params != null && params.count() > 0) {
			while (e.hasMoreElements()) {
				EOParamBv param = (EOParamBv)e.nextElement();
				param.supprimerRelations();
				editingContext().deleteObject(param);
			}
		}
		LogManager.logDetail(this.getClass().getName() + " - Suppression des ParamStrBv");
		params = EOParamStrBv.rechercherParamsPourInstanceEtBureau(editingContext(), null,this);
		if (params != null && params.count() > 0) {
			e = params.objectEnumerator();
			while (e.hasMoreElements()) {
				EOParamStrBv param = (EOParamStrBv)e.nextElement();
				param.supprimerRelations();
				editingContext().deleteObject(param);
			}
		}
	}
}

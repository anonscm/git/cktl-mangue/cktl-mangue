//EOCollege.java
//Created on Tue Sep 12 16:33:34 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.elections;

import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.modele.Inclusion;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamCollege;
import org.cocktail.mangue.modele.mangue.elections.parametrage.EOParamSecteur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;


/** Un college est associe a des inclusions et des parametrages d'instance
 * sont aussi liees au groupe d'exclusion.<BR>
 * Lors de la suppression d'un college, on invalide toutes les instances des 
 * listes electorales lies a ce college<BR>
 * Regles de validation : les longueurs des strings doivent etre respectees.<BR>
 * Le numero d'ordre et le type d'instance sont obligatoires.
 */
public class EOCollege extends _EOCollege implements RecordAvecLibelle {

	public static EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LL_COLLEGE_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);

	public EOCollege() {
		super();
	}
	
	/**
	 * 
	 * @param ec
	 * @param instance
	 * @param college
	 * @return
	 */
	public static EOCollege creer(EOEditingContext ec, EOTypeInstance typeInstance) {

		EOCollege newObject = new EOCollege();
		newObject.setTypeInstanceRelationship(typeInstance);
		
		ec.insertObject(newObject);
		return newObject;
	}


	public void initAvecTypeInstance(EOTypeInstance type,Number numeroOrdre) {
		setNumElcOrdre(new Integer(numeroOrdre.intValue()));
		addObjectToBothSidesOfRelationshipWithKey(type,"typeInstance");
	}
	/** Supprime les paramCollege, les paramSecteur et toutes les inclusions */
	public void supprimerParametrages() {
		NSArray params = EOParamCollege.rechercherParametragesPourCollege(editingContext(), this);
		if (params != null && params.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression de ParamCollege");
			java.util.Enumeration e = params.objectEnumerator();
			while (e.hasMoreElements()) {
				EOParamCollege param = (EOParamCollege)e.nextElement();
				param.supprimerRelations();
				editingContext().deleteObject(param);
			}
		}
		params = EOParamSecteur.rechercherParametragesPourCollegeEtSecteur(editingContext(), this,null);
		if (params != null && params.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression de ParamSecteur");
			java.util.Enumeration e = params.objectEnumerator();
			while (e.hasMoreElements()) {
				EOParamSecteur param = (EOParamSecteur)e.nextElement();
				param.supprimerRelations();
				editingContext().deleteObject(param);
			}
		}
		supprimerInclusions("InclusionCorps");
		supprimerInclusions("InclusionDiplome");
		supprimerInclusions("InclusionContrat");
		// 31/03/2010
		EOListeElecteur.invaliderInstancesEtSuppressionElecteurs(editingContext(), "college", this);
	}
	public void supprimerRelations() {
		setTypeInstanceRelationship(null);
	}
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (numElcOrdre() == null) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le numéro d'ordre du collège est obligatoire");
		}
		if (lcCollege() == null || lcCollege().length() > 20) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé court est obligatoire et ne doit pas comporter plus de 20 caractères !");
		}
		if (llCollege() == null || llCollege().length() > 40) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le libellé long est obligatoire et ne doit pas comporter plus de 40 caractères !");
		}
		if (typeInstance() == null) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez fournir le type d'instance");
		}
	}
	public NSArray inclusionsCorps() {
		return EOInclusionCorps.rechercherInclusionsPourEntiteEtCollege(editingContext(), "InclusionCorps", this);
	}
	public NSArray inclusionsContrat() {
		return EOInclusionContrat.rechercherInclusionsPourEntiteEtCollege(editingContext(), "InclusionContrat", this);
	}
	public NSArray inclusionsDiplome() {
		if (typeInstance().estElcCs()) {
			return EOInclusionDiplome.rechercherInclusionsPourEntiteEtCollege(editingContext(), "InclusionDiplome", this);
		} else {
			return null;
		}
	}
	// Interface RecordAvecLibelle
	public String libelle() {
		return llCollege();
	}
	// Méthodes privées
	
	private void supprimerInclusions(String nomEntite) {
		NSArray inclusions = Inclusion.rechercherInclusionsPourEntiteEtCollege(editingContext(), nomEntite, this);
		if (inclusions != null && inclusions.count() > 0) {
			LogManager.logDetail(this.getClass().getName() + " - Suppression de " + nomEntite);
			java.util.Enumeration e = inclusions.objectEnumerator();
			while (e.hasMoreElements()) {
				Inclusion inclusion = (Inclusion)e.nextElement();
				// 31/03/2010 - on n'appelle supprimerRelations sans l'invalidation des instances qui sera faite globalement pour le collège
				inclusion.supprimerRelations(false);
				editingContext().deleteObject(inclusion);
			}
		}
	}

	// Méthodes statiques
	public static NSArray<EOInstance> findForTypeInstance(EOEditingContext edc,EOTypeInstance typeInstance) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TYPE_INSTANCE_KEY + " = %@", new NSArray(typeInstance));
		return fetchAll(edc, qualifier, SORT_ARRAY_LIBELLE_ASC);
	}
}
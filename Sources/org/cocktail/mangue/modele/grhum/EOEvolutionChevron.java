// EOEvolutionChevron.java
// Created on Tue Jan 28 15:16:38  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum;


import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
public class EOEvolutionChevron extends EOGenericRecord {

    public EOEvolutionChevron() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOEvolutionChevron(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public Number noDossierPers() {
        return (Number)storedValueForKey("noDossierPers");
    }

    public void setNoDossierPers(Number value) {
        takeStoredValueForKey(value, "noDossierPers");
    }

    public Number noSeqCarriere() {
        return (Number)storedValueForKey("noSeqCarriere");
    }

    public void setNoSeqCarriere(Number value) {
        takeStoredValueForKey(value, "noSeqCarriere");
    }

    public Number noSeqElement() {
        return (Number)storedValueForKey("noSeqElement");
    }

    public void setNoSeqElement(Number value) {
        takeStoredValueForKey(value, "noSeqElement");
    }

    public String cChevron() {
        return (String)storedValueForKey("cChevron");
    }

    public void setCChevron(String value) {
        takeStoredValueForKey(value, "cChevron");
    }

    public NSTimestamp dEffetChevron() {
        return (NSTimestamp)storedValueForKey("dEffetChevron");
    }

    public void setDEffetChevron(NSTimestamp value) {
        takeStoredValueForKey(value, "dEffetChevron");
    }

    public NSTimestamp dFinChevron() {
        return (NSTimestamp)storedValueForKey("dFinChevron");
    }

    public void setDFinChevron(NSTimestamp value) {
        takeStoredValueForKey(value, "dFinChevron");
    }

    public String noArreteChevron() {
        return (String)storedValueForKey("noArreteChevron");
    }

    public void setNoArreteChevron(String value) {
        takeStoredValueForKey(value, "noArreteChevron");
    }

    public NSTimestamp dArreteChevron() {
        return (NSTimestamp)storedValueForKey("dArreteChevron");
    }

    public void setDArreteChevron(NSTimestamp value) {
        takeStoredValueForKey(value, "dArreteChevron");
    }

    public String statut() {
        return (String)storedValueForKey("statut");
    }

    public void setStatut(String value) {
        takeStoredValueForKey(value, "statut");
    }
    
    // Méthodes ajoutées
    /** recherche les &eacute;volutions chevrons associ&eacute;es &agrave; un &eacute;l&eacute;ment de carri&egrave;re 
     * @param editingContext editing context
     * @param elementCarriere &eacute;l&eacute;ment de carri&egrave;re
     * @return tableau des anciennet&eacute;s
     */
    public static NSArray rechercherEvolutionsChevron(EOEditingContext editingContext,EOElementCarriere elementCarriere) {
		String stringQualifier = "noDossierPers = " + elementCarriere.noDossierPers() + " and noSeqCarriere = " + elementCarriere.noSeqCarriere()
							+ " and noSeqElement = " + elementCarriere.noSeqElement();
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,null);
		EOFetchSpecification myFetch = new EOFetchSpecification("EvolutionChevron",myQualifier,null);
		return editingContext.objectsWithFetchSpecification(myFetch);
    }
}

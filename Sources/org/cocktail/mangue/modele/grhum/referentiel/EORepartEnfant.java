// EORepartEnfant.java
// Created on Wed Mar 12 16:06:12 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;

import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOLienFiliationEnfant;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

// 09/06/2010 - Modification pour supporter l'interface RecordAvecLibelle
// 23/09/2010 - Modification pour gérer correctement le nombre d'enfants dans personnel suite à DT 2706
public class EORepartEnfant extends _EORepartEnfant implements RecordAvecLibelle {

	public static EOSortOrdering SORT_NAISSANCE_DESC = new EOSortOrdering(ENFANT_KEY+"."+EOEnfant.D_NAISSANCE_KEY, EOSortOrdering.CompareDescending);
	public static NSArray<EOSortOrdering> SORT_ARRAY_NAISSANCE_DESC = new NSArray<EOSortOrdering>(SORT_NAISSANCE_DESC);

	public static EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(ENFANT_KEY+"."+EOEnfant.NOM_KEY, EOSortOrdering.CompareAscending);
	public static NSArray<EOSortOrdering> SORT_ARRAY_NOM_ASC = new NSArray<EOSortOrdering>(SORT_NOM_ASC);
	public NSTimestamp dateFinPriseEnCharge;

	public EORepartEnfant() {
		super();
	}

	/**
	 * Creation d'un objet REPART ENFANT a partir d'un enfant et d'un parent
	 * @param ec
	 * @param parent
	 * @param enfant
	 * @return
	 */
	public static EORepartEnfant creer(EOEditingContext ec, EOIndividu parent, EOEnfant enfant) {

		EORepartEnfant newObject = (EORepartEnfant) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.setLienFiliationRelationship(EOLienFiliationEnfant.getDefault(ec));

		newObject.setParentRelationship(parent);
		newObject.setEstACharge(true);
		newObject.setEnfantRelationship(enfant);
		newObject.setTemSft(CocktailConstantes.VRAI);

		return newObject;		
	}

	public boolean estACharge() {
		return temACharge() != null && temACharge().equals(CocktailConstantes.VRAI);
	}
	public void setEstACharge(boolean aBool) {
		if (aBool) {
			setTemACharge(CocktailConstantes.VRAI);
		} else {
			setTemACharge(CocktailConstantes.FAUX);
		}
	}

	public boolean procureSFT() {
		return temSft() != null && temSft().equals(CocktailConstantes.VRAI);
	}
	public void setProcureSFT(boolean aBool) {
		if (aBool) {
			setTemSft(CocktailConstantes.VRAI);
		} else {
			setTemSft(CocktailConstantes.FAUX);
		}
	}

	public String typeLienFiliation() {
		if (lienFiliation() == null) {
			return EOLienFiliationEnfant.LIEN_FILIATION_INCONNU;
		} else {
			return lienFiliation().lfenCode();
		}
	}
	/** Retourne true si l'enfant est adopte, legitime ou naturel et que la date de naissance
	 * ou d'adoption est anterieure au 01/01/2004, retourne null sinon
	 */
	public Boolean ouvreDroitBonification() {
		//				if (lienFiliation() != null) {
		//					NSTimestamp dateComparaison = null;
		//					if (lienFiliation().estAdopte()) {
		//						dateComparaison = enfant().dAdoption();
		//					} else if (lienFiliation().estEnfantLegitime() || lienFiliation().estEnfantNaturel()) {
		//						dateComparaison = enfant().dNaissance();
		//					}
		//					if (dateComparaison != null) {	// 23/11/2010 - modification pour renvoyer vrai ou false. Avant cette date l'indicateur retourne true
		//						return new Boolean(DateCtrl.isBefore(dateComparaison, DateCtrl.stringToDate(DATE_POUR_BONIFICATION)));
		//					}
		//				}
		return null;
	}
	/** Retourne true si l'enfant est legitime ou naturel et que la date de naissance est anterieure au 01/01/2004
	 * Retourne false pour un enfant adopte	 */
	public Boolean ouvreDroitBonificationPourEtude() {

		//		if (lienFiliation() != null) {
		//			if (lienFiliation().estAdopte()) {
		//				return new Boolean(false);
		//			} else if ((lienFiliation().estEnfantLegitime() || lienFiliation().estEnfantNaturel()) && dNaissance() != null) {	
		//				return new Boolean(DateCtrl.isBefore(dNaissance(), DateCtrl.stringToDate(DATE_POUR_BONIFICATION)));// 23/11/2010 - modification pour renvoyer vrai ou false. Avant cette date l'indicateur retourne true
		//			}
		//		}

		return null;

	}


	/** Pour un enfant legitime, la date de debut de prise en charge est la date de naissance,
	 * pour les autres, c'est la date d'arrivee au foyer
	 * @return
	 */
	public NSTimestamp dateDebutPriseEnCharge() {
		if (lienFiliation() == null) {
			return enfant().dNaissance();
		} else if (lienFiliation().estEnfantLegitime()) {
			return enfant().dNaissance();
		} else {
			return enfant().dArriveeFoyer();
		}
	}

	/** Pour un enfant legitime ou naturel, la date de debut de lien de filiation est la date de naissance
	 * null sinon
	 */
	public NSTimestamp dateDebutFiliation() {
		if (lienFiliation() != null && (lienFiliation().estEnfantLegitime() || lienFiliation().estEnfantNaturel())) {
			return enfant().dNaissance();
		} else {
			return null;
		}
	}

	/** Retourne la date max de prise en charge ou evalue cette date en fonction du temoin a charge
	 * et de la date de declaration du Cir
	 * @return
	 */
	public NSTimestamp dateFinPriseEnCharge() {

		if (dateDebutPriseEnCharge() != null && DateCtrl.isBefore(dateFinPriseEnCharge, dateDebutPriseEnCharge()))
			return dateDebutPriseEnCharge();

		return dateFinPriseEnCharge;
	}
	public String dureePriseEnCharge() {
		return null;
	}
	/** Toujours nulle */
	public NSTimestamp dateFinFiliation() {
		return null;
	}
	
	// Pour le CIR
	/** Evalue la date de fin de prise en charge si elle n'est pas fournie et retourne un message en cas d'ambiguite.
	 * Si la date de deces de l'individu est fourni, la date de fin de prise en charge doit etre inferieure 
	 * ou egale : on la force a la date de deces */
	// 23/11/2010 - modifiée pour prendre en compte la date de décès du parent
	public String preparerPourCir(NSTimestamp dateDuJour, NSTimestamp dateDecesParent) {
		String message = null;
		if (enfant().dMaxACharge() != null) {
			dateFinPriseEnCharge = enfant().dMaxACharge();
		} else {
			int nbMois = DateCtrl.calculerDureeEnMois(enfant().dNaissance(), dateDuJour).intValue();
			int nbAnnees = nbMois / 12;
			int nbAnneesPriseEnCharge = 0;
			// Si moins de 16 ans, on prends 16 ans
			if (nbAnnees < 16) {
				nbAnneesPriseEnCharge = 16;
			} else if (nbAnnees <= 20) {
				message = "Erreur possible dans l'évaluation de la date de fin de prise en charge, veuillez la saisir pour " + enfant().identite();
				if (estACharge()) {
					nbAnneesPriseEnCharge = 20;
				} else {
					nbAnneesPriseEnCharge = 16;
				}
			} else {
				if (nbAnnees == 21 && estACharge()) {
					nbAnneesPriseEnCharge = 21;
				} else {
					// On met vingt ans mais cela ne pourrait être que 16
					nbAnneesPriseEnCharge = 20;
				}
			}
			dateFinPriseEnCharge =  DateCtrl.jourPrecedent(DateCtrl.dateAvecAjoutAnnees(enfant().dNaissance(), nbAnneesPriseEnCharge));
		}	

		if (dateDecesParent != null && dateFinPriseEnCharge != null && DateCtrl.isAfter(dateFinPriseEnCharge, dateDecesParent)) {
			dateFinPriseEnCharge = dateDecesParent;
		}
		return message;
	}


	// Interface RecordAvecLibelle
	public String libelle() {
		return enfant().nom() + " " + enfant().prenom();
	}



	/**
	 * 
	 * @param ec
	 * @param parent
	 * @return
	 */
	public static NSArray<EORepartEnfant> rechercherPourParent(EOEditingContext ec, EOIndividu parent) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();    	
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PARENT_KEY + "=%@", new NSArray(parent)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ENFANT_KEY+"."+EOEnfant.TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_NAISSANCE_DESC);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}
	
	/**
	 * 
	 * @param edc
	 * @param enfant
	 * @return
	 */
	public static NSArray<EORepartEnfant> rechercherRepartsPourEnfant(EOEditingContext edc, EOEnfant enfant) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(ENFANT_KEY + "=%@", new NSArray(enfant));
		return fetchAll(edc, qualifier, null);
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param enfant
	 * @param parent
	 * @return
	 */
	public static NSArray<EORepartEnfant> rechercherRepartsPourEnfantEtParent(EOEditingContext edc, EOEnfant enfant,EOIndividu parent) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ENFANT_KEY + " = %@", new NSArray(enfant)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PARENT_KEY + " = %@", new NSArray(parent)));
		
		return fetchAll(edc, new EOAndQualifier(qualifiers), null);
	}

	/**
	 * 
	 * @param ec
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public static NSArray<EORepartEnfant> rechercherEnfantPourCreation(EOEditingContext ec,String nom,String prenom)	{

		try {

			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ENFANT_KEY+"."+EOEnfant.TEM_VALIDE_KEY+ " = %@",new NSArray(CocktailConstantes.VRAI)));

			String stringQualifier = "";

			nom = StringCtrl.replace(nom, " ", "");
			for (int i=0;i<nom.length();i++)	{
				if( i == 0)
					stringQualifier = ENFANT_KEY+"."+EOEnfant.NOM_KEY + " like '" + nom.toUpperCase().charAt(i) + "*";
				else
					stringQualifier = stringQualifier + nom.toUpperCase().charAt(i) + "*";
			}
			stringQualifier = stringQualifier +  "'";
			stringQualifier = (NSArray.componentsSeparatedByString(stringQualifier,"*'*")).componentsJoinedByString("*''*");		
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(stringQualifier,null));

			// Filtre sur le prenom
			if (prenom != null && prenom.length() > 0)	{
				stringQualifier = "";			
				for (int i=0;i<prenom.length();i++)	{
					if( i == 0)
						stringQualifier = ENFANT_KEY+"."+EOEnfant.PRENOM_KEY + " like '" + prenom.toUpperCase().charAt(i) + "*";
					else
						stringQualifier = stringQualifier + prenom.toUpperCase().charAt(i) + "*";
				}		
				stringQualifier = stringQualifier +  "'";
				stringQualifier = (NSArray.componentsSeparatedByString(stringQualifier,"*'*")).componentsJoinedByString("*''*");

				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(stringQualifier,null));
			}

			return fetchAll(ec,new EOAndQualifier(qualifiers), null );
		}
		catch (Exception e)	{
			return null;
		}
	}


	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (enfant() == null || parent() == null) {
			throw new NSValidation.ValidationException("ENFANTS - Veuillez associer un enfant au parent !");
		}

		if (lienFiliation() == null) {
			throw new NSValidation.ValidationException("ENFANTS - Le lien de filiation est obligatoire !");
		}
		
		if (lienFiliation().estInconnu()) {
			throw new NSValidation.ValidationException("ENFANTS - Veuillez sélectionner un autre lien de filiation que 'Non Précisé' !");			
		}
		
		String message = validationsCir();
		if (message != null && message.length() > 0)
			throw new NSValidation.ValidationException(message);

	}

	/**
	 * 
	 * @return
	 */
	public String validationsCir() {


		// vérifier que la date de naissance n'est pas postérieure à la date de décès !!
		if (enfant().dNaissance() != null && DateCtrl.isBefore(enfant().dNaissance(), parent().dNaissance())) {
			return " ENFANTS - La date de naissance de l'enfant doit être posterieure à celle du parent !";
		}

		if (lienFiliation() != null) {
			if (lienFiliation().estEnfantLegitime() == false 
					&& !lienFiliation().estInconnu() && enfant().dArriveeFoyer() == null) {
				return " ENFANTS - Vous devez fournir la date d'arrivée au foyer";
			}

			if (lienFiliation().estAdopte()) {
				if (enfant().dAdoption() == null) {
					return " ENFANTS - Pour un enfant adopté, vous devez fournir la date de dépôt de requête d'adoption";
				} else if (DateCtrl.isBeforeEq(enfant().dAdoption(),enfant().dNaissance())) {
					return " ENFANTS - La date de dépôt de requête d'adoption ne peut être antérieure à la date de naissance !!";
				}
			} else if (enfant().dAdoption() != null) {
				return " ENFANTS - Pour un enfant autre qu'adopté, vous ne devez pas fournir la date de dépôt de requête d'adoption";
			}
		}


		return null;

	}
}

// EOIndividuAutrFonct.java
// Created on Mon Oct 23 15:20:19 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOIndividuAutrFonct extends _EOIndividuAutrFonct {

	public static final EOSortOrdering SORT_DATE_DEBUT_DESC = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray<EOSortOrdering> SORT_ARRAY_DATE_DEBUT_DESC = new NSArray<EOSortOrdering>(SORT_DATE_DEBUT_DESC);

	public static EOIndividuAutrFonct findForKey( EOEditingContext ec, Number key) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(NO_EXERCICE_FONCTION_KEY + "=%@", new NSArray<Number>(key));
		return fetchFirstByQualifier(ec, qualifier);
	}
	public static NSArray<EOIndividuAutrFonct> findForIndividu(EOEditingContext ec, EOIndividu individu) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INDIVIDU_KEY + "=%@", new NSArray(individu));
		return fetchAll(ec,  qualifier, SORT_ARRAY_DATE_DEBUT_DESC);
	}
	
	public static EOIndividuAutrFonct creer(EOEditingContext ec, EOIndividu individu) {
		EOIndividuAutrFonct newObject = (EOIndividuAutrFonct) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setIndividuRelationship(individu);
		return newObject;
	}

	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (dateDebut() == null) {
			throw new NSValidation.ValidationException("La date de début est obligatoire !");
		}
		if (fonction() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une fonction");
		}
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
	}

}

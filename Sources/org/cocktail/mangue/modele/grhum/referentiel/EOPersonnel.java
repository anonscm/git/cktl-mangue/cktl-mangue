// EOPersonnel.java
// Created on Wed Feb 26 13:34:56  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPersonnel extends _EOPersonnel {
	
	public static EOSortOrdering SORT_MATRICULE_DESC = new EOSortOrdering(NO_MATRICULE_KEY, EOSortOrdering.CompareDescending);
	public static NSArray SORT_ARRAY_MATRICULE_DESC = new NSArray(SORT_MATRICULE_DESC);
	
	public static String PARAM_MATRICULE_SEQUENCE = "S";
	public static String PARAM_MATRICULE_ANNEE = "A";
	public static String PARAM_MATRICULE_NO_INDIVIDU = "N";
	public static String PARAM_MATRICULE_MANUEL = "M";
	
	public static int LG_NUMEN = 13;
	public static int LG_EPICEA = 6;
	public static int LG_MATRICULE = 8;
	public static int LG_NPC = 2;
	public static int LG_TXT_LIBRE = 100;
	
    public EOPersonnel() {
        super();
    }

    public boolean estMESR() {
    	return toMinistere() == null || toMinistere().estMESR();
    }
    public boolean estMAAF() {
    	return toMinistere() != null && toMinistere().estMAAF();
    }

    public boolean estSauvadet() {
    	return temSauvadet() != null && temSauvadet().equals("O");
    }
    public void setEstSauvadet(boolean value) {
    	if (value)
    		setTemSauvadet("O");
    	else
    		setTemSauvadet("N");
    }
    
    public boolean isIdentiteCertifiee() {
    	return cirDCertification() != null;
    }

	/** Initialisation d'un nouvel objet de type EOPersonnel */
	public void initPersonnel(Integer numero) {
		setAffecteDefense("N");
		setTemImposable("O");
		setTemPaieSecu("O");
		setTemTitulaire("N");
		setTemSauvadet("N");
		setNpc("00");
		setTemBudgetEtat("N");
		setNbEnfants(new Integer(0));
		setNbEnfantsACharge(new Integer(0));
		setNoDossierPers(numero);
	}
	
	/** Tests de coherence pour l'enregistrement d'un individu */
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {

		// vérifier le numen (format : "99A9999999AAA")
		// vérifier le numen NOUVEAU FORMAT (format : "99AAA99999AAA")
		if (numen() != null) {
			if (numen().length() != LG_NUMEN) {
				throw new NSValidation.ValidationException("Le NUMEN doit comporter " + LG_NUMEN + " caractères");
			}
			for (int i = 0; i < numen().length();i++) {
				char c = numen().charAt(i);
				if (i == 2  || i >= 10) {		// || i == 3 || i == 4
					if (StringCtrl.isBasicLetter(c) == false) {
						throw new NSValidation.ValidationException("Le NUMEN doit comporter une lettre en position " + (i + 1));
					} 
				} else {
					if ( i != 3 && i != 4 && StringCtrl.isBasicDigit(c) == false) {
						throw new NSValidation.ValidationException("Le NUMEN doit comporter un chiffre en position " + (i + 1));
					}
				}
			}
			// 12/10/2010 - Vérifier l'unicité du numen
			EOPersonnel personnel = EOPersonnel.rechercherPersonnelAvecNumen(editingContext(), numen());
			if (personnel != null && personnel != this) {
				throw new NSValidation.ValidationException(" Ce NUMEN est déjà utilisé par " + personnel.toIndividu().identitePrenomFirst() + " !");
			}
		}
		// Vérifier l'unicité du matricule
		if (noMatricule() != null) {
			EOPersonnel personnel = EOPersonnel.rechercherPersonnelAvecMatricule(editingContext(),noMatricule());
			if (personnel != null && personnel != this) {
				throw new NSValidation.ValidationException("Le numéro de matricule n'est pas unique");
			}
		}
		if (txtLibre() != null && txtLibre().length() > LG_TXT_LIBRE)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le texte libre comporte au plus " + LG_TXT_LIBRE + " caractères !");
	}

/**
 * Age limite de mise a la retraite pour les remontees CIR. 
 * Pour le moment seuls les ages de 6000 et 6500 sont pris en compte. 
 * 
 * Ensuite le cLimiteAge pourra etre retourne.
 * 
 * @return
 */
	public String ageMiseOfficeRetraite() {
		if (limiteAge() != null) {
			
			Integer limiteAge = new Integer(limiteAge().code().substring(0,2));
			
			if (limiteAge.intValue() > 65)
				return "6500";

			if (limiteAge.intValue() < 65 && limiteAge.intValue() > 60)
				return "6000";
		}
	
		return "6500";

	}
	
	/**
	 * 
	 * @param ec
	 * @param matricule
	 * @return
	 */
	public static EOPersonnel findLastMatriculeForYear(EOEditingContext edc, Integer annee) {
		
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_MATRICULE_KEY + " caseInsensitiveLike %@", new NSArray(String.valueOf(annee) + "*")));
		
		return fetchFirstByQualifier(edc, new EOAndQualifier(andQualifiers), SORT_ARRAY_MATRICULE_DESC);
	}

	/**
	 * 
	 * @param ec
	 * @param matricule
	 * @return
	 */
	public static EOPersonnel rechercherPersonnelAvecMatricule(EOEditingContext edc,String matricule) {
		return fetchFirstByQualifier(edc, CocktailFinder.getQualifierEqual(NO_MATRICULE_KEY, matricule));
	}
	
	/**
	 * 
	 * @param ec
	 * @param numen
	 * @return
	 */
	public static EOPersonnel rechercherPersonnelAvecNumen(EOEditingContext edc,String numen) {
		return fetchFirstByQualifier(edc, CocktailFinder.getQualifierEqual(NUMEN_KEY, numen));
	}


}

// EOIndividuSimple.java
// Created on Tue Mar 28 15:45:39 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;

import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

// 10/12/2010 - ajout du témoin listeRouge
public class EOIndividuSimple extends _EOIndividuSimple implements RecordAvecLibelle {

	public static NSArray SORT_NOM_ASC = new NSArray(new EOSortOrdering(NOM_USUEL_KEY, EOSortOrdering.CompareAscending));


	public static String INSEE_INDIVIDU_KEY = "noInsee";
	
	public EOIndividuSimple() {
		super();
	}

	public String identite() {
		return nomUsuel() + " " + prenom();
	}
	public String identitePrenomFirst() {
		return prenom() + " " + nomUsuel();
	}

	public String noInsee() {
		
		if (indNoInsee() != null) {
			return indNoInsee();
		}
		else if (indNoInseeProv() != null) {
			return indNoInseeProv();
		}
		
		return null;
	}

	public boolean estListeRouge() {
		return listeRouge() != null && listeRouge().equals(CocktailConstantes.VRAI);
	}
	public void setEstListeRouge(boolean aBool) {
		if (aBool) {
			setListeRouge(CocktailConstantes.VRAI);
		} else {
			setListeRouge(CocktailConstantes.FAUX);
		}
	}

	public static EOIndividuSimple creer(EOEditingContext ec) {

		EOIndividuSimple individu = (EOIndividuSimple)CocktailUtilities.instanceForEntity(ec, EOIndividuSimple.ENTITY_NAME);
		individu.init();

		ec.insertObject(individu);
		return individu;
	}

	public void init() {
		setTemValide(CocktailConstantes.VRAI);
		setListeRouge(CocktailConstantes.FAUX);
	}

	public static NSArray rechercherIndividuPourCreation(EOEditingContext ec,String nom,String prenom)	{

		try {

			if ((nom == null || nom.length() == 0) && (prenom == null && prenom.length() == 0) )
				return new NSArray();
			
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+ " = %@",new NSArray(CocktailConstantes.VRAI)));

			String stringQualifier = "";

			// Filtre sur le nom
			if (nom != null && nom.length() > 0)	{

				nom = StringCtrl.replace(nom, " ", "");
				for (int i=0;i<nom.length();i++)	{
					if( i == 0)
						stringQualifier = NOM_USUEL_KEY + " like '" + nom.toUpperCase().charAt(i) + "*";
					else
						stringQualifier = stringQualifier + nom.toUpperCase().charAt(i) + "*";
				}
				stringQualifier = stringQualifier +  "'";
				stringQualifier = (NSArray.componentsSeparatedByString(stringQualifier,"*'*")).componentsJoinedByString("*''*");		
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(stringQualifier,null));
			}
			// Filtre sur le prenom
			if (prenom != null && prenom.length() > 0)	{
				stringQualifier = "";			
				for (int i=0;i<prenom.length();i++)	{
					if( i == 0)
						stringQualifier = PRENOM_KEY + " like '" + prenom.toUpperCase().charAt(i) + "*";
					else
						stringQualifier = stringQualifier + prenom.toUpperCase().charAt(i) + "*";
				}		
				stringQualifier = stringQualifier +  "'";
				stringQualifier = (NSArray.componentsSeparatedByString(stringQualifier,"*'*")).componentsJoinedByString("*''*");

				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(stringQualifier,null));
			}

			return fetchAll(ec,new EOAndQualifier(qualifiers), null );
		}
		catch (Exception e)	{
			e.printStackTrace();
			return null;
		}
	}




	/** Tests de coherence pour l'enregistrement d'un individu */
	public void validateForSave () throws ValidationException {		
		if (nomUsuel() == null || nomUsuel().length() > 80)
			throw new ValidationException("Le nom est obligatoire et ne doit pas comporter plus de 80 caractères !");
		setNomUsuel(nomUsuel().toUpperCase());
		if (prenom() == null || prenom().length() > 30)
			throw new ValidationException("Le prénom est obligatoire et ne doit pas comporter plus de 30 caractères !");
		setPrenom(prenom().toUpperCase());

		if (cCivilite() == null || cCivilite().length() == 0) {
			throw new ValidationException("La civilité est obligatoire !");
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
	// interface RecordAvecLibelle
	public String libelle() {
		return nomUsuel() + " " + prenom();
	}
	// Méthodes statiques
	public static EOIndividuSimple rechercherIndividuAvecID(EOEditingContext editingContext,Number individuID) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_INDIVIDU_KEY+"=%@", new NSArray(individuID)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));

			return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e)	{
			return null;
		}
	}
	public static EOIndividuSimple rechercherPourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_INDIVIDU_KEY+"=%@", new NSArray(individu.noIndividu())));
			return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e)	{
			return null;
		}
	}

}

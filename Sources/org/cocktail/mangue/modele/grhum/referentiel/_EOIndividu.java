/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIndividu.java instead.
package org.cocktail.mangue.modele.grhum.referentiel;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOIndividu extends  EOGenericRecord {
	public static final String ENTITY_NAME = "IndividuUlr";
	public static final String ENTITY_TABLE_NAME = "GRHUM.INDIVIDU_ULR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noIndividu";

	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DECES_KEY = "dDeces";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String IND_ACTIVITE_KEY = "indActivite";
	public static final String IND_CLE_INSEE_KEY = "indCleInsee";
	public static final String IND_CLE_INSEE_PROV_KEY = "indCleInseeProv";
	public static final String IND_C_SITUATION_FAMILLE_KEY = "indCSituationFamille";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String IND_NO_INSEE_PROV_KEY = "indNoInseeProv";
	public static final String IND_PHOTO_KEY = "indPhoto";
	public static final String IND_QUALITE_KEY = "indQualite";
	public static final String LIEU_DECES_KEY = "lieuDeces";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_AFFICHAGE_KEY = "nomAffichage";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_PATRONYMIQUE_AFFICHAGE_KEY = "nomPatronymiqueAffichage";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PRENOM_KEY = "prenom";
	public static final String PRENOM2_KEY = "prenom2";
	public static final String PRENOM_AFFICHAGE_KEY = "prenomAffichage";
	public static final String PRISE_CPT_INSEE_KEY = "priseCptInsee";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String VILLE_DE_NAISSANCE_KEY = "villeDeNaissance";

// Attributs non visibles
	public static final String C_DEPT_NAISSANCE_KEY = "cDeptNaissance";
	public static final String C_PAYS_NAISSANCE_KEY = "cPaysNaissance";
	public static final String C_PAYS_NATIONALITE_KEY = "cPaysNationalite";
	public static final String D_NATURALISATION_KEY = "dNaturalisation";
	public static final String IND_C_SIT_MILITAIRE_KEY = "indCSitMilitaire";
	public static final String IND_ORIGINE_KEY = "indOrigine";
	public static final String TAILLE_NOM_KEY = "tailleNom";
	public static final String TEM_SS_DIPLOME_KEY = "temSsDiplome";

//Colonnes dans la base de donnees
	public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DECES_COLKEY = "D_DECES";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
	public static final String IND_ACTIVITE_COLKEY = "IND_ACTIVITE";
	public static final String IND_CLE_INSEE_COLKEY = "IND_CLE_INSEE";
	public static final String IND_CLE_INSEE_PROV_COLKEY = "IND_CLE_INSEE_PROV";
	public static final String IND_C_SITUATION_FAMILLE_COLKEY = "IND_C_SITUATION_FAMILLE";
	public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
	public static final String IND_NO_INSEE_PROV_COLKEY = "IND_NO_INSEE_PROV";
	public static final String IND_PHOTO_COLKEY = "IND_PHOTO";
	public static final String IND_QUALITE_COLKEY = "IND_QUALITE";
	public static final String LIEU_DECES_COLKEY = "LIEU_DECES";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NOM_AFFICHAGE_COLKEY = "NOM_AFFICHAGE";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String NOM_PATRONYMIQUE_AFFICHAGE_COLKEY = "NOM_PATRONYMIQUE_AFFICHAGE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String PRENOM2_COLKEY = "PRENOM2";
	public static final String PRENOM_AFFICHAGE_COLKEY = "PRENOM_AFFICHAGE";
	public static final String PRISE_CPT_INSEE_COLKEY = "PRISE_CPT_INSEE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";
	public static final String VILLE_DE_NAISSANCE_COLKEY = "VILLE_DE_NAISSANCE";

	public static final String C_DEPT_NAISSANCE_COLKEY = "C_DEPT_NAISSANCE";
	public static final String C_PAYS_NAISSANCE_COLKEY = "C_PAYS_NAISSANCE";
	public static final String C_PAYS_NATIONALITE_COLKEY = "C_PAYS_NATIONALITE";
	public static final String D_NATURALISATION_COLKEY = "D_NATURALISATION";
	public static final String IND_C_SIT_MILITAIRE_COLKEY = "IND_C_SIT_MILITAIRE";
	public static final String IND_ORIGINE_COLKEY = "IND_ORIGINE";
	public static final String TAILLE_NOM_COLKEY = "$attribute.columnName";
	public static final String TEM_SS_DIPLOME_COLKEY = "TEM_SS_DIPLOME";


	// Relationships
	public static final String DIPLOMES_KEY = "diplomes";
	public static final String PERSONNELS_KEY = "personnels";
	public static final String TO_CIVILITE_KEY = "toCivilite";
	public static final String TO_DEPARTEMENT_KEY = "toDepartement";
	public static final String TO_PAYS_NAISSANCE_KEY = "toPaysNaissance";
	public static final String TO_PAYS_NATIONALITE_KEY = "toPaysNationalite";
	public static final String TO_SITUATION_FAMILIALE_KEY = "toSituationFamiliale";
	public static final String TO_SITUATION_MILITAIRE_KEY = "toSituationMilitaire";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cCivilite() {
    return (String) storedValueForKey(C_CIVILITE_KEY);
  }

  public void setCCivilite(String value) {
    takeStoredValueForKey(value, C_CIVILITE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDeces() {
    return (NSTimestamp) storedValueForKey(D_DECES_KEY);
  }

  public void setDDeces(NSTimestamp value) {
    takeStoredValueForKey(value, D_DECES_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dNaissance() {
    return (NSTimestamp) storedValueForKey(D_NAISSANCE_KEY);
  }

  public void setDNaissance(NSTimestamp value) {
    takeStoredValueForKey(value, D_NAISSANCE_KEY);
  }

  public String indActivite() {
    return (String) storedValueForKey(IND_ACTIVITE_KEY);
  }

  public void setIndActivite(String value) {
    takeStoredValueForKey(value, IND_ACTIVITE_KEY);
  }

  public Integer indCleInsee() {
    return (Integer) storedValueForKey(IND_CLE_INSEE_KEY);
  }

  public void setIndCleInsee(Integer value) {
    takeStoredValueForKey(value, IND_CLE_INSEE_KEY);
  }

  public Integer indCleInseeProv() {
    return (Integer) storedValueForKey(IND_CLE_INSEE_PROV_KEY);
  }

  public void setIndCleInseeProv(Integer value) {
    takeStoredValueForKey(value, IND_CLE_INSEE_PROV_KEY);
  }

  public String indCSituationFamille() {
    return (String) storedValueForKey(IND_C_SITUATION_FAMILLE_KEY);
  }

  public void setIndCSituationFamille(String value) {
    takeStoredValueForKey(value, IND_C_SITUATION_FAMILLE_KEY);
  }

  public String indNoInsee() {
    return (String) storedValueForKey(IND_NO_INSEE_KEY);
  }

  public void setIndNoInsee(String value) {
    takeStoredValueForKey(value, IND_NO_INSEE_KEY);
  }

  public String indNoInseeProv() {
    return (String) storedValueForKey(IND_NO_INSEE_PROV_KEY);
  }

  public void setIndNoInseeProv(String value) {
    takeStoredValueForKey(value, IND_NO_INSEE_PROV_KEY);
  }

  public String indPhoto() {
    return (String) storedValueForKey(IND_PHOTO_KEY);
  }

  public void setIndPhoto(String value) {
    takeStoredValueForKey(value, IND_PHOTO_KEY);
  }

  public String indQualite() {
    return (String) storedValueForKey(IND_QUALITE_KEY);
  }

  public void setIndQualite(String value) {
    takeStoredValueForKey(value, IND_QUALITE_KEY);
  }

  public String lieuDeces() {
    return (String) storedValueForKey(LIEU_DECES_KEY);
  }

  public void setLieuDeces(String value) {
    takeStoredValueForKey(value, LIEU_DECES_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public String nomAffichage() {
    return (String) storedValueForKey(NOM_AFFICHAGE_KEY);
  }

  public void setNomAffichage(String value) {
    takeStoredValueForKey(value, NOM_AFFICHAGE_KEY);
  }

  public String nomPatronymique() {
    return (String) storedValueForKey(NOM_PATRONYMIQUE_KEY);
  }

  public void setNomPatronymique(String value) {
    takeStoredValueForKey(value, NOM_PATRONYMIQUE_KEY);
  }

  public String nomPatronymiqueAffichage() {
    return (String) storedValueForKey(NOM_PATRONYMIQUE_AFFICHAGE_KEY);
  }

  public void setNomPatronymiqueAffichage(String value) {
    takeStoredValueForKey(value, NOM_PATRONYMIQUE_AFFICHAGE_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public String prenom2() {
    return (String) storedValueForKey(PRENOM2_KEY);
  }

  public void setPrenom2(String value) {
    takeStoredValueForKey(value, PRENOM2_KEY);
  }

  public String prenomAffichage() {
    return (String) storedValueForKey(PRENOM_AFFICHAGE_KEY);
  }

  public void setPrenomAffichage(String value) {
    takeStoredValueForKey(value, PRENOM_AFFICHAGE_KEY);
  }

  public String priseCptInsee() {
    return (String) storedValueForKey(PRISE_CPT_INSEE_KEY);
  }

  public void setPriseCptInsee(String value) {
    takeStoredValueForKey(value, PRISE_CPT_INSEE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public String villeDeNaissance() {
    return (String) storedValueForKey(VILLE_DE_NAISSANCE_KEY);
  }

  public void setVilleDeNaissance(String value) {
    takeStoredValueForKey(value, VILLE_DE_NAISSANCE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.EOCivilite toCivilite() {
    return (org.cocktail.mangue.modele.grhum.EOCivilite)storedValueForKey(TO_CIVILITE_KEY);
  }

  public void setToCiviliteRelationship(org.cocktail.mangue.modele.grhum.EOCivilite value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOCivilite oldValue = toCivilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CIVILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CIVILITE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EODepartement toDepartement() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EODepartement)storedValueForKey(TO_DEPARTEMENT_KEY);
  }

  public void setToDepartementRelationship(org.cocktail.mangue.common.modele.nomenclatures.EODepartement value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EODepartement oldValue = toDepartement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPARTEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPARTEMENT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOPays toPaysNaissance() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOPays)storedValueForKey(TO_PAYS_NAISSANCE_KEY);
  }

  public void setToPaysNaissanceRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOPays value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOPays oldValue = toPaysNaissance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_NAISSANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_NAISSANCE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOPays toPaysNationalite() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOPays)storedValueForKey(TO_PAYS_NATIONALITE_KEY);
  }

  public void setToPaysNationaliteRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOPays value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOPays oldValue = toPaysNationalite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_NATIONALITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_NATIONALITE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale toSituationFamiliale() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale)storedValueForKey(TO_SITUATION_FAMILIALE_KEY);
  }

  public void setToSituationFamilialeRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOSituationFamiliale oldValue = toSituationFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SITUATION_FAMILIALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SITUATION_FAMILIALE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOSituationMilitaire toSituationMilitaire() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOSituationMilitaire)storedValueForKey(TO_SITUATION_MILITAIRE_KEY);
  }

  public void setToSituationMilitaireRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOSituationMilitaire value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOSituationMilitaire oldValue = toSituationMilitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SITUATION_MILITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SITUATION_MILITAIRE_KEY);
    }
  }
  
  public NSArray diplomes() {
    return (NSArray)storedValueForKey(DIPLOMES_KEY);
  }

  public NSArray diplomes(EOQualifier qualifier) {
    return diplomes(qualifier, null, false);
  }

  public NSArray diplomes(EOQualifier qualifier, boolean fetch) {
    return diplomes(qualifier, null, fetch);
  }

  public NSArray diplomes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes.INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = diplomes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDiplomesRelationship(org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DIPLOMES_KEY);
  }

  public void removeFromDiplomesRelationship(org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DIPLOMES_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes createDiplomesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("IndividuDiplomes");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DIPLOMES_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes) eo;
  }

  public void deleteDiplomesRelationship(org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DIPLOMES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDiplomesRelationships() {
    Enumeration objects = diplomes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDiplomesRelationship((org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes)objects.nextElement());
    }
  }

  public NSArray personnels() {
    return (NSArray)storedValueForKey(PERSONNELS_KEY);
  }

  public NSArray personnels(EOQualifier qualifier) {
    return personnels(qualifier, null, false);
  }

  public NSArray personnels(EOQualifier qualifier, boolean fetch) {
    return personnels(qualifier, null, fetch);
  }

  public NSArray personnels(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel.TO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = personnels();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersonnelsRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSONNELS_KEY);
  }

  public void removeFromPersonnelsRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNELS_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel createPersonnelsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Personnel");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSONNELS_KEY);
    return (org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel) eo;
  }

  public void deletePersonnelsRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNELS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersonnelsRelationships() {
    Enumeration objects = personnels().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersonnelsRelationship((org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOIndividu avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOIndividu createEOIndividu(EOEditingContext editingContext, String cCivilite
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer noIndividu
, String nomUsuel
, Integer persId
, String prenom
			) {
    EOIndividu eo = (EOIndividu) createAndInsertInstance(editingContext, _EOIndividu.ENTITY_NAME);    
		eo.setCCivilite(cCivilite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setNoIndividu(noIndividu);
		eo.setNomUsuel(nomUsuel);
		eo.setPersId(persId);
		eo.setPrenom(prenom);
    return eo;
  }

  
	  public EOIndividu localInstanceIn(EOEditingContext editingContext) {
	  		return (EOIndividu)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividu creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividu creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOIndividu object = (EOIndividu)createAndInsertInstance(editingContext, _EOIndividu.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOIndividu localInstanceIn(EOEditingContext editingContext, EOIndividu eo) {
    EOIndividu localInstance = (eo == null) ? null : (EOIndividu)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOIndividu#localInstanceIn a la place.
   */
	public static EOIndividu localInstanceOf(EOEditingContext editingContext, EOIndividu eo) {
		return EOIndividu.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOIndividu fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOIndividu fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOIndividu eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOIndividu)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOIndividu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndividu fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndividu eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndividu)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOIndividu fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOIndividu eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOIndividu ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOIndividu fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

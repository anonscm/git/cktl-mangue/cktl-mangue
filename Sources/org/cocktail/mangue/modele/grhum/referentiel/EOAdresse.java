//EOAdresse.java
//Created on Wed Feb 26 09:32:42  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;


import org.cocktail.common.utilities.StringCtrl;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.server.cir.ConstantesCir;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOAdresse extends _EOAdresse {

	public static String CP_AFFICHAGE_KEY = "codePostalAffichage";
	
	public EOAdresse() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param agent
	 * @return
	 */
	public static EOAdresse creer(EOEditingContext ec, EOAgentPersonnel agent) {

		EOAdresse adresse = (EOAdresse)CocktailUtilities.instanceForEntity(ec, ENTITY_NAME);

		adresse.setToPaysRelationship(EOPays.getDefault(ec));
		adresse.setAdrOrdre(new Integer(construireAdresseOrdre(ec).intValue()));
		adresse.setPersIdCreation(agent.toIndividu().persId());
		adresse.setPersIdModification(agent.toIndividu().persId());
		adresse.setTemPayeUtil(CocktailConstantes.FAUX);
		adresse.setAdrListeRouge(CocktailConstantes.FAUX);
		adresse.setDDebVal(new NSTimestamp());

		ec.insertObject(adresse);
		return adresse;
	}

	public String codePostalAffichage() {
		if (codePostal() != null) {
			return codePostal();
		}
		if (cpEtranger() != null) {
			return cpEtranger();
		}
		return null;
	}
	
	/**
	 * 
	 * @param code
	 * @return
	 */
	public String getExtensionVoie(String code) {

		if (code.equals("B"))
			return "BIS";
		if (code.equals("T"))
			return "TER";
		if (code.equals("Q"))
			return "QUATER";

		return "";

	}

	/**
	 * 
	 */
	public void initAdrAdresse1() {

		String libelle = "";

		if (noVoie() != null)
			libelle += noVoie().toString() + " ";
		if (bisTer() != null) {
			libelle += getExtensionVoie(bisTer()) + " ";
		}
		if (toVoie() != null)
			libelle += toVoie().libelleLong() + " ";
		if (nomVoie() != null)
			libelle += nomVoie();

		setAdrAdresse1(libelle.trim());

	}

	/**
	 * 
	 * @return
	 */
	public String adrAdresse1Cir() {

		String retour = adrAdresse1();
		if (adrAdresse1() != null && adrAdresse1().length() > 38) {
			NSArray<String> details = NSArray.componentsSeparatedByString(adrAdresse1(), " ");
			retour = adrAdresse1().replace(details.lastObject(), "");
		}

		return retour;
	}
	/**
	 * 
	 * @return
	 */
	public String adrAdresse3Cir() {

		String retour = adrAdresse2();

		if (adrAdresse2() != null && adrAdresse2().length() > 38) {
			NSArray<String> details = NSArray.componentsSeparatedByString(adrAdresse2(), " ");
			retour = adrAdresse2().replace(details.lastObject(), "");
		}

		return retour;
	}


	/**
	 * 
	 * @return
	 */
	public String adrAdresse2Cir() {

		String retour = null;

		if (adrAdresse1() != null && adrAdresse1().length() > 38) {
			NSArray<String> details = NSArray.componentsSeparatedByString(adrAdresse1(), " ");
			retour = details.lastObject();
		}

		return retour;
	}

	/**
	 * 
	 * @return
	 */
	public String adrAdresse4Cir() {

		String retour = null;

		if (adrAdresse2() != null && adrAdresse2().length() > 38) {
			NSArray<String> details = NSArray.componentsSeparatedByString(adrAdresse2(), " ");
			retour = details.lastObject();
		}

		return retour;
	}
	/**
	 * 
	 * @return
	 */
	public String adresseComplete() {

		String chaineAdresse = "";

		if (adrAdresse1() != null)
			chaineAdresse += adrAdresse1();
		if (adrAdresse2() != null)
			chaineAdresse += " " + adrAdresse2();
		if (codePostal() != null)
			chaineAdresse += " " + codePostal();
		if (ville() != null)
			chaineAdresse += " " + ville();

		return chaineAdresse;

	}


	public void init() {
		setTemPayeUtil(CocktailConstantes.FAUX);
		setAdrListeRouge(CocktailConstantes.FAUX);
		setDDebVal(new NSTimestamp());
	}
	
	/**
	 * 
	 */
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		
		String message = validationsCir();
		if (message != null && message.length() > 0)
			throw new NSValidation.ValidationException(message);

		if (adrAdresse1() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le champ d'adresse principale");
		}
		if (codePostal() != null && codePostal().length() > 5 ) {
			throw new NSValidation.ValidationException("Le code postal ne doit pas dépasser 5 caractères");
		}
		if (ville() != null && ville().length() > 60) {
			throw new NSValidation.ValidationException("La ville ne doit pas dépasser 60 caractères");
		}
		if (adrAdresse1() != null && adrAdresse1().length() > 60) {
			throw new NSValidation.ValidationException("L'adresse ne doit pas dépasser 60 caractères");
		}
		if (adrAdresse2() != null && adrAdresse2().length() > 38 ) {
			throw new NSValidation.ValidationException("Le complément d'adresse ne doit pas dépasser 38 caractères");
		}
		if (adrBp() != null && adrBp().length() > 7) {
			throw new NSValidation.ValidationException("La boite postale ne doit pas dépasser 7 caractères");
		}
		if (cpEtranger() != null && cpEtranger().length() > 10) {
			throw new NSValidation.ValidationException("Le code postal étranger ne doit pas dépasser 10 caractères");
		}


		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	/**
	 * 
	 * @return
	 */
	public String validationsCir() {
		String temp = "";
		
		if (toPays() == null) {
			return "Vous devez fournir le pays !";
		}
		if (toPays().code().equals(EOPays.CODE_PAYS_ETRANGER_INCONNU)) {
			return "Pour une adresse à l'étranger, vous devez préciser le pays !";		
		}

		if (toPays().code().equals(EOPays.CODE_PAYS_FRANCE)) {
			if (codePostal() == null) {
				return "Pour une adresse en France, vous devez fournir le code postal !";				
			}
			try {
				Integer testCP = new Integer(codePostal());
			}
			catch (Exception e){
				return " Pour une adresse en France, le code postal ne doit contenir que des chiffres !";
			}
		}

		if (ville() == null || ville().length() == 0) {
			return "La ville est obligatoire !";
		}

		if (adrAdresse1() != null && StringCtrl.containsIgnoreCase(adrAdresse1(), "@")) {
			temp = "Le caractère '@' n'est pas autorisé dans l'adresse de l'agent";
		}
		if (adrAdresse2() != null && StringCtrl.containsIgnoreCase(adrAdresse2(), "@")) {
			temp = "Le caractère '@' n'est pas autorisé dans le complément d'adresse de l'agent";
		}

		return temp;
	}
	/** retourne le pays de l'adresse.
	 * @throws Exception
	 */
	public String codePaysCir() throws Exception {
		String pays = null;
		if (codePostal() != null || toPays() != null && toPays().code().equals(EOPays.CODE_PAYS_FRANCE)) {
			pays = ConstantesCir.CODE_PAYS_DEBUT + EOPays.CODE_PAYS_FRANCE;
		} else {
			if (toPays() == null) {
				throw new Exception("Dans une adresse, vous devez fournir un code postal (France) ou un pays");
			} else if (toPays().code().equals(EOPays.CODE_PAYS_ETRANGER_INCONNU)) {
				throw new Exception("Pour une adresse à l'étranger, vous devez fournir le pays");
			}
			pays = ConstantesCir.CODE_PAYS_DEBUT + toPays().code();
		}
		return pays;
	}
	/** Retourne le code postal ou le code postal etranger */
	public String codePostalCir() throws Exception {
		String cp = null;
		if (codePostal() != null) {
			cp = codePostal();
		} else {
			// Vérifier si il s'agit d'une adresse en France
			if (toPays() != null && toPays().code().equals(EOPays.CODE_PAYS_FRANCE)) {
				throw new Exception("En France, le code postal est obligatoire");
			}
			if (cpEtranger() != null && cpEtranger().length() != 5) {
				return null;	// On ne sait pas comment tronquer
			} else {
				cp = cpEtranger();
			}
		}
		return cp;
	}
	// méthodes statiques
	/** Creation manuelle du noIndividu d'un individu
	@param editingContext
	@return num du persID */
	public static Number construireAdresseOrdre(EOEditingContext editingContext) {
		return SuperFinder.clePrimairePour(editingContext,"Adresse","adrOrdre","SeqAdresse",true);
	}
}

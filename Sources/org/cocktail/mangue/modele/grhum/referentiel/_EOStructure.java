// _EOStructure.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStructure.java instead.
package org.cocktail.mangue.modele.grhum.referentiel;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOStructure extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "StructureUlr";
	public static final String ENTITY_TABLE_NAME = "GRHUM.STRUCTURE_ULR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cStructure";

	public static final String C_STATUT_JURIDIQUE_KEY = "cStatutJuridique";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_STRUCTURE_PERE_KEY = "cStructurePere";
	public static final String C_TYPE_DECISION_STR_KEY = "cTypeDecisionStr";
	public static final String C_TYPE_ETABLISSEMEN_KEY = "cTypeEtablissemen";
	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String DATE_DECISION_KEY = "dateDecision";
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String GRP_ACCES_KEY = "grpAcces";
	public static final String GRP_ALIAS_KEY = "grpAlias";
	public static final String GRP_APE_CODE_KEY = "grpApeCode";
	public static final String GRP_APE_CODE_BIS_KEY = "grpApeCodeBis";
	public static final String GRP_APE_CODE_COMP_KEY = "grpApeCodeComp";
	public static final String GRP_CA_KEY = "grpCa";
	public static final String GRP_CAPITAL_KEY = "grpCapital";
	public static final String GRP_CENTRE_DECISION_KEY = "grpCentreDecision";
	public static final String GRP_EFFECTIFS_KEY = "grpEffectifs";
	public static final String GRP_FONCTION1_KEY = "grpFonction1";
	public static final String GRP_FONCTION2_KEY = "grpFonction2";
	public static final String GRP_FORME_JURIDIQUE_KEY = "grpFormeJuridique";
	public static final String GRP_MOTS_CLEFS_KEY = "grpMotsClefs";
	public static final String GRP_RESPONSABILITE_KEY = "grpResponsabilite";
	public static final String GRP_TRADEMARQUE_KEY = "grpTrademarque";
	public static final String GRP_WEBMESTRE_KEY = "grpWebmestre";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String REF_DECISION_KEY = "refDecision";
	public static final String REF_EXT_COMP_KEY = "refExtComp";
	public static final String REF_EXT_CR_KEY = "refExtCr";
	public static final String REF_EXT_ETAB_KEY = "refExtEtab";
	public static final String SIREN_KEY = "siren";
	public static final String SIRET_KEY = "siret";
	public static final String STR_ACTIVITE_KEY = "strActivite";
	public static final String STR_AFFICHAGE_KEY = "strAffichage";
	public static final String STR_ORIGINE_KEY = "strOrigine";
	public static final String STR_PHOTO_KEY = "strPhoto";
	public static final String TEM_COTIS_ASSEDIC_KEY = "temCotisAssedic";
	public static final String TEM_DADS_KEY = "temDads";
	public static final String TEM_SECTORISE_KEY = "temSectorise";
	public static final String TEM_SOUMIS_TVA_KEY = "temSoumisTva";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String GRP_OWNER_KEY = "grpOwner";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_NAF_KEY = "cNaf";
	public static final String C_ACADEMIE_KEY = "cAcademie";
	public static final String GRP_RESPONSABLE_KEY = "grpResponsable";

//Colonnes dans la base de donnees
	public static final String C_STATUT_JURIDIQUE_COLKEY = "C_STATUT_JURIDIQUE";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String C_STRUCTURE_PERE_COLKEY = "C_STRUCTURE_PERE";
	public static final String C_TYPE_DECISION_STR_COLKEY = "C_TYPE_DECISION_STR";
	public static final String C_TYPE_ETABLISSEMEN_COLKEY = "C_TYPE_ETABLISSEMEN";
	public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
	public static final String DATE_DECISION_COLKEY = "DATE_DECISION";
	public static final String DATE_FERMETURE_COLKEY = "DATE_FERMETURE";
	public static final String DATE_OUVERTURE_COLKEY = "DATE_OUVERTURE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String GRP_ACCES_COLKEY = "GRP_ACCES";
	public static final String GRP_ALIAS_COLKEY = "GRP_ALIAS";
	public static final String GRP_APE_CODE_COLKEY = "GRP_APE_CODE";
	public static final String GRP_APE_CODE_BIS_COLKEY = "GRP_APE_CODE_BIS";
	public static final String GRP_APE_CODE_COMP_COLKEY = "GRP_APE_CODE_COMP";
	public static final String GRP_CA_COLKEY = "GRP_CA";
	public static final String GRP_CAPITAL_COLKEY = "GRP_CAPITAL";
	public static final String GRP_CENTRE_DECISION_COLKEY = "GRP_CENTRE_DECISION";
	public static final String GRP_EFFECTIFS_COLKEY = "GRP_EFFECTIFS";
	public static final String GRP_FONCTION1_COLKEY = "GRP_FONCTION1";
	public static final String GRP_FONCTION2_COLKEY = "GRP_FONCTION2";
	public static final String GRP_FORME_JURIDIQUE_COLKEY = "GRP_FORME_JURIDIQUE";
	public static final String GRP_MOTS_CLEFS_COLKEY = "GRP_MOTS_CLEFS";
	public static final String GRP_RESPONSABILITE_COLKEY = "GRP_RESPONSABILITE";
	public static final String GRP_TRADEMARQUE_COLKEY = "GRP_TRADEMARQUE";
	public static final String GRP_WEBMESTRE_COLKEY = "GRP_WEBMESTRE";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LL_STRUCTURE_COLKEY = "LL_STRUCTURE";
	public static final String ORG_ORDRE_COLKEY = "ORG_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String REF_DECISION_COLKEY = "REF_DECISION";
	public static final String REF_EXT_COMP_COLKEY = "REF_EXT_COMP";
	public static final String REF_EXT_CR_COLKEY = "REF_EXT_CR";
	public static final String REF_EXT_ETAB_COLKEY = "REF_EXT_ETAB";
	public static final String SIREN_COLKEY = "SIREN";
	public static final String SIRET_COLKEY = "SIRET";
	public static final String STR_ACTIVITE_COLKEY = "STR_ACTIVITE";
	public static final String STR_AFFICHAGE_COLKEY = "STR_AFFICHAGE";
	public static final String STR_ORIGINE_COLKEY = "STR_ORIGINE";
	public static final String STR_PHOTO_COLKEY = "STR_PHOTO";
	public static final String TEM_COTIS_ASSEDIC_COLKEY = "TEM_COTIS_ASSEDIC";
	public static final String TEM_DADS_COLKEY = "TEM_DADS";
	public static final String TEM_SECTORISE_COLKEY = "TEM_SECTORISE";
	public static final String TEM_SOUMIS_TVA_COLKEY = "TEM_SOUMIS_TVA";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String GRP_OWNER_COLKEY = "GRP_OWNER";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_NAF_COLKEY = "C_NAF";
	public static final String C_ACADEMIE_COLKEY = "C_ACADEMIE";
	public static final String GRP_RESPONSABLE_COLKEY = "GRP_RESPONSABLE";


	// Relationships
	public static final String ACADEMIE_KEY = "academie";
	public static final String NAF_KEY = "naf";
	public static final String OWNER_KEY = "owner";
	public static final String RESPONSABLE_KEY = "responsable";
	public static final String RNE_KEY = "rne";
	public static final String TO_REPART_TYPE_GROUPE_KEY = "toRepartTypeGroupe";
	public static final String TO_STRUCTURE_PERE_KEY = "toStructurePere";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cStatutJuridique() {
    return (String) storedValueForKey(C_STATUT_JURIDIQUE_KEY);
  }

  public void setCStatutJuridique(String value) {
    takeStoredValueForKey(value, C_STATUT_JURIDIQUE_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public String cStructurePere() {
    return (String) storedValueForKey(C_STRUCTURE_PERE_KEY);
  }

  public void setCStructurePere(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_PERE_KEY);
  }

  public String cTypeDecisionStr() {
    return (String) storedValueForKey(C_TYPE_DECISION_STR_KEY);
  }

  public void setCTypeDecisionStr(String value) {
    takeStoredValueForKey(value, C_TYPE_DECISION_STR_KEY);
  }

  public String cTypeEtablissemen() {
    return (String) storedValueForKey(C_TYPE_ETABLISSEMEN_KEY);
  }

  public void setCTypeEtablissemen(String value) {
    takeStoredValueForKey(value, C_TYPE_ETABLISSEMEN_KEY);
  }

  public String cTypeStructure() {
    return (String) storedValueForKey(C_TYPE_STRUCTURE_KEY);
  }

  public void setCTypeStructure(String value) {
    takeStoredValueForKey(value, C_TYPE_STRUCTURE_KEY);
  }

  public NSTimestamp dateDecision() {
    return (NSTimestamp) storedValueForKey(DATE_DECISION_KEY);
  }

  public void setDateDecision(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DECISION_KEY);
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey(DATE_FERMETURE_KEY);
  }

  public void setDateFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FERMETURE_KEY);
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey(DATE_OUVERTURE_KEY);
  }

  public void setDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_OUVERTURE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String grpAcces() {
    return (String) storedValueForKey(GRP_ACCES_KEY);
  }

  public void setGrpAcces(String value) {
    takeStoredValueForKey(value, GRP_ACCES_KEY);
  }

  public String grpAlias() {
    return (String) storedValueForKey(GRP_ALIAS_KEY);
  }

  public void setGrpAlias(String value) {
    takeStoredValueForKey(value, GRP_ALIAS_KEY);
  }

  public String grpApeCode() {
    return (String) storedValueForKey(GRP_APE_CODE_KEY);
  }

  public void setGrpApeCode(String value) {
    takeStoredValueForKey(value, GRP_APE_CODE_KEY);
  }

  public String grpApeCodeBis() {
    return (String) storedValueForKey(GRP_APE_CODE_BIS_KEY);
  }

  public void setGrpApeCodeBis(String value) {
    takeStoredValueForKey(value, GRP_APE_CODE_BIS_KEY);
  }

  public String grpApeCodeComp() {
    return (String) storedValueForKey(GRP_APE_CODE_COMP_KEY);
  }

  public void setGrpApeCodeComp(String value) {
    takeStoredValueForKey(value, GRP_APE_CODE_COMP_KEY);
  }

  public Integer grpCa() {
    return (Integer) storedValueForKey(GRP_CA_KEY);
  }

  public void setGrpCa(Integer value) {
    takeStoredValueForKey(value, GRP_CA_KEY);
  }

  public Integer grpCapital() {
    return (Integer) storedValueForKey(GRP_CAPITAL_KEY);
  }

  public void setGrpCapital(Integer value) {
    takeStoredValueForKey(value, GRP_CAPITAL_KEY);
  }

  public String grpCentreDecision() {
    return (String) storedValueForKey(GRP_CENTRE_DECISION_KEY);
  }

  public void setGrpCentreDecision(String value) {
    takeStoredValueForKey(value, GRP_CENTRE_DECISION_KEY);
  }

  public Integer grpEffectifs() {
    return (Integer) storedValueForKey(GRP_EFFECTIFS_KEY);
  }

  public void setGrpEffectifs(Integer value) {
    takeStoredValueForKey(value, GRP_EFFECTIFS_KEY);
  }

  public String grpFonction1() {
    return (String) storedValueForKey(GRP_FONCTION1_KEY);
  }

  public void setGrpFonction1(String value) {
    takeStoredValueForKey(value, GRP_FONCTION1_KEY);
  }

  public String grpFonction2() {
    return (String) storedValueForKey(GRP_FONCTION2_KEY);
  }

  public void setGrpFonction2(String value) {
    takeStoredValueForKey(value, GRP_FONCTION2_KEY);
  }

  public String grpFormeJuridique() {
    return (String) storedValueForKey(GRP_FORME_JURIDIQUE_KEY);
  }

  public void setGrpFormeJuridique(String value) {
    takeStoredValueForKey(value, GRP_FORME_JURIDIQUE_KEY);
  }

  public String grpMotsClefs() {
    return (String) storedValueForKey(GRP_MOTS_CLEFS_KEY);
  }

  public void setGrpMotsClefs(String value) {
    takeStoredValueForKey(value, GRP_MOTS_CLEFS_KEY);
  }

  public String grpResponsabilite() {
    return (String) storedValueForKey(GRP_RESPONSABILITE_KEY);
  }

  public void setGrpResponsabilite(String value) {
    takeStoredValueForKey(value, GRP_RESPONSABILITE_KEY);
  }

  public String grpTrademarque() {
    return (String) storedValueForKey(GRP_TRADEMARQUE_KEY);
  }

  public void setGrpTrademarque(String value) {
    takeStoredValueForKey(value, GRP_TRADEMARQUE_KEY);
  }

  public String grpWebmestre() {
    return (String) storedValueForKey(GRP_WEBMESTRE_KEY);
  }

  public void setGrpWebmestre(String value) {
    takeStoredValueForKey(value, GRP_WEBMESTRE_KEY);
  }

  public String lcStructure() {
    return (String) storedValueForKey(LC_STRUCTURE_KEY);
  }

  public void setLcStructure(String value) {
    takeStoredValueForKey(value, LC_STRUCTURE_KEY);
  }

  public String llStructure() {
    return (String) storedValueForKey(LL_STRUCTURE_KEY);
  }

  public void setLlStructure(String value) {
    takeStoredValueForKey(value, LL_STRUCTURE_KEY);
  }

  public Integer orgOrdre() {
    return (Integer) storedValueForKey(ORG_ORDRE_KEY);
  }

  public void setOrgOrdre(Integer value) {
    takeStoredValueForKey(value, ORG_ORDRE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String refDecision() {
    return (String) storedValueForKey(REF_DECISION_KEY);
  }

  public void setRefDecision(String value) {
    takeStoredValueForKey(value, REF_DECISION_KEY);
  }

  public String refExtComp() {
    return (String) storedValueForKey(REF_EXT_COMP_KEY);
  }

  public void setRefExtComp(String value) {
    takeStoredValueForKey(value, REF_EXT_COMP_KEY);
  }

  public String refExtCr() {
    return (String) storedValueForKey(REF_EXT_CR_KEY);
  }

  public void setRefExtCr(String value) {
    takeStoredValueForKey(value, REF_EXT_CR_KEY);
  }

  public String refExtEtab() {
    return (String) storedValueForKey(REF_EXT_ETAB_KEY);
  }

  public void setRefExtEtab(String value) {
    takeStoredValueForKey(value, REF_EXT_ETAB_KEY);
  }

  public String siren() {
    return (String) storedValueForKey(SIREN_KEY);
  }

  public void setSiren(String value) {
    takeStoredValueForKey(value, SIREN_KEY);
  }

  public String siret() {
    return (String) storedValueForKey(SIRET_KEY);
  }

  public void setSiret(String value) {
    takeStoredValueForKey(value, SIRET_KEY);
  }

  public String strActivite() {
    return (String) storedValueForKey(STR_ACTIVITE_KEY);
  }

  public void setStrActivite(String value) {
    takeStoredValueForKey(value, STR_ACTIVITE_KEY);
  }

  public String strAffichage() {
    return (String) storedValueForKey(STR_AFFICHAGE_KEY);
  }

  public void setStrAffichage(String value) {
    takeStoredValueForKey(value, STR_AFFICHAGE_KEY);
  }

  public String strOrigine() {
    return (String) storedValueForKey(STR_ORIGINE_KEY);
  }

  public void setStrOrigine(String value) {
    takeStoredValueForKey(value, STR_ORIGINE_KEY);
  }

  public String strPhoto() {
    return (String) storedValueForKey(STR_PHOTO_KEY);
  }

  public void setStrPhoto(String value) {
    takeStoredValueForKey(value, STR_PHOTO_KEY);
  }

  public String temCotisAssedic() {
    return (String) storedValueForKey(TEM_COTIS_ASSEDIC_KEY);
  }

  public void setTemCotisAssedic(String value) {
    takeStoredValueForKey(value, TEM_COTIS_ASSEDIC_KEY);
  }

  public String temDads() {
    return (String) storedValueForKey(TEM_DADS_KEY);
  }

  public void setTemDads(String value) {
    takeStoredValueForKey(value, TEM_DADS_KEY);
  }

  public String temSectorise() {
    return (String) storedValueForKey(TEM_SECTORISE_KEY);
  }

  public void setTemSectorise(String value) {
    takeStoredValueForKey(value, TEM_SECTORISE_KEY);
  }

  public String temSoumisTva() {
    return (String) storedValueForKey(TEM_SOUMIS_TVA_KEY);
  }

  public void setTemSoumisTva(String value) {
    takeStoredValueForKey(value, TEM_SOUMIS_TVA_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.EOAcademie academie() {
    return (org.cocktail.mangue.modele.grhum.EOAcademie)storedValueForKey(ACADEMIE_KEY);
  }

  public void setAcademieRelationship(org.cocktail.mangue.modele.grhum.EOAcademie value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOAcademie oldValue = academie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ACADEMIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ACADEMIE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.structures.EONaf naf() {
    return (org.cocktail.mangue.common.modele.nomenclatures.structures.EONaf)storedValueForKey(NAF_KEY);
  }

  public void setNafRelationship(org.cocktail.mangue.common.modele.nomenclatures.structures.EONaf value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.structures.EONaf oldValue = naf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, NAF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, NAF_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu owner() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(OWNER_KEY);
  }

  public void setOwnerRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = owner();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, OWNER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, OWNER_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu responsable() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(RESPONSABLE_KEY);
  }

  public void setResponsableRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = responsable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RESPONSABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RESPONSABLE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EORne rne() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EORne)storedValueForKey(RNE_KEY);
  }

  public void setRneRelationship(org.cocktail.mangue.common.modele.nomenclatures.EORne value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EORne oldValue = rne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RNE_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.referentiel.EOStructure toStructurePere() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(TO_STRUCTURE_PERE_KEY);
  }

  public void setToStructurePereRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = toStructurePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_PERE_KEY);
    }
  }
  
  public NSArray toRepartTypeGroupe() {
    return (NSArray)storedValueForKey(TO_REPART_TYPE_GROUPE_KEY);
  }

  public NSArray toRepartTypeGroupe(EOQualifier qualifier) {
    return toRepartTypeGroupe(qualifier, null);
  }

  public NSArray toRepartTypeGroupe(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = toRepartTypeGroupe();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToRepartTypeGroupeRelationship(org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_REPART_TYPE_GROUPE_KEY);
  }

  public void removeFromToRepartTypeGroupeRelationship(org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_TYPE_GROUPE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe createToRepartTypeGroupeRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartTypeGroupe");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_REPART_TYPE_GROUPE_KEY);
    return (org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe) eo;
  }

  public void deleteToRepartTypeGroupeRelationship(org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_REPART_TYPE_GROUPE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartTypeGroupeRelationships() {
    Enumeration objects = toRepartTypeGroupe().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartTypeGroupeRelationship((org.cocktail.mangue.modele.grhum.referentiel.EORepartTypeGroupe)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOStructure avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOStructure createEOStructure(EOEditingContext editingContext, String cStructure
, String cTypeStructure
, Integer persId
, String temCotisAssedic
, String temDads
, String temSectorise
, String temSoumisTva
, String temValide
			) {
    EOStructure eo = (EOStructure) createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setCTypeStructure(cTypeStructure);
		eo.setPersId(persId);
		eo.setTemCotisAssedic(temCotisAssedic);
		eo.setTemDads(temDads);
		eo.setTemSectorise(temSectorise);
		eo.setTemSoumisTva(temSoumisTva);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOStructure localInstanceIn(EOEditingContext editingContext) {
	  		return (EOStructure)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructure creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructure creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOStructure object = (EOStructure)createAndInsertInstance(editingContext, _EOStructure.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOStructure localInstanceIn(EOEditingContext editingContext, EOStructure eo) {
    EOStructure localInstance = (eo == null) ? null : (EOStructure)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOStructure#localInstanceIn a la place.
   */
	public static EOStructure localInstanceOf(EOEditingContext editingContext, EOStructure eo) {
		return EOStructure.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOStructure fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStructure fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOStructure fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOStructure eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOStructure)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOStructure fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOStructure eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOStructure ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOStructure fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

//EORepartTypeGroupe.java
//Created on Mon Nov 27 17:45:40 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;

import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EOTypeGroupe;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class EORepartTypeGroupe extends _EORepartTypeGroupe {

	
	public static String TYPE_SERVICE = "S";
	public static String TYPE_LABO = "LA";
	public static String TYPE_ECOLE_DOCT = "ED";
	public static String TYPE_EQUIPE_RECHERCHE = "ER";
	public static String TYPE_FEDERATION_RECHERCHE = "FR";
	public static String TYPE_STRUCTURE_RECHERCHE = "SR";
	public static String TYPE_ENSEIGNEMENT = "DE";
	public static String TYPE_VALORISATION = "CV";
	public static String TYPE_SERVICE_GESTIONNAIRE = "SG";
	
	public static String[] TYPES_GROUPES_ROLES = new String[]{TYPE_SERVICE, TYPE_LABO, TYPE_ECOLE_DOCT, TYPE_EQUIPE_RECHERCHE,
		TYPE_FEDERATION_RECHERCHE, TYPE_STRUCTURE_RECHERCHE, TYPE_ENSEIGNEMENT, TYPE_VALORISATION};
	
	public EORepartTypeGroupe() {
		super();
	}


	public static EORepartTypeGroupe creer(EOEditingContext ec, EOStructure structure, EOTypeGroupe type, EOAgentPersonnel agent) {

		// On teste que l'on a pas de doublons
		EORepartTypeGroupe repart = rechercherPourTypeEtStructure(ec, type, structure);
		if (repart == null) {

			EORepartTypeGroupe newObject = new EORepartTypeGroupe();
			newObject.initAvecStructureEtType(structure, type);
			ec.insertObject(newObject);

			return newObject;
		}

		return null;
	}

	
	/**
	 * 
	 * @param ec
	 * @param structure
	 * @return
	 */
	public static NSArray<EORepartTypeGroupe> findForStructure(EOEditingContext ec, EOStructure structure) {

		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_STRUCTURE_KEY +"=%@", new NSArray(structure.cStructure())));

		return fetchAll(ec, new EOAndQualifier(qualifiers));

	}

	/**
	 * 
	 * @param ec
	 * @param type
	 * @param structure
	 * @return
	 */
	public static EORepartTypeGroupe rechercherPourTypeEtStructure(EOEditingContext ec, EOTypeGroupe type, EOStructure structure) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_GROUPE_KEY + "=%@", new NSArray(type)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_STRUCTURE_KEY +"=%@", new NSArray(structure.cStructure())));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));

	}

	/**
	 * 
	 * @param structure
	 * @param typeGroupe
	 */
	public void initAvecStructureEtType(EOStructure structure, EOTypeGroupe typeGroupe) {
		
		setCStructure(structure.cStructure());
		setTgrpCode(typeGroupe.code());
		setTypeGroupeRelationship(typeGroupe);
		
	}
	
	/**
	 * 
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (cStructure() == null)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez renseigner une structure !");
		
		if (tgrpCode() == null)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez renseigner un type de groupe !");

		EOTypeGroupe typeGroupe = (EOTypeGroupe)NomenclatureFinder.findForCode(editingContext(), EOTypeGroupe.ENTITY_NAME, tgrpCode());
		if (typeGroupe == null)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le type de groupe " + tgrpCode() + " n'est pas reconnu !");
		
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		
	}
	
}

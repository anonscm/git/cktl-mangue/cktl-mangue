// _EOPersonnel.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersonnel.java instead.
package org.cocktail.mangue.modele.grhum.referentiel;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOPersonnel extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;


	public static final String ENTITY_NAME = "Personnel";
	public static final String ENTITY_TABLE_NAME = "GRHUM.PERSONNEL_ULR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noDossierPers";

	public static final String AFFECTE_DEFENSE_KEY = "affecteDefense";
	public static final String CIR_D_CERTIFICATION_KEY = "cirDCertification";
	public static final String CIR_D_COMPLETUDE_KEY = "cirDCompletude";
	public static final String CIR_D_VERIFICATION_KEY = "cirDVerification";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_SITUATION_FAMILLE_KEY = "dSituationFamille";
	public static final String ID_MINISTERE_KEY = "idMinistere";
	public static final String NB_ENFANTS_KEY = "nbEnfants";
	public static final String NB_ENFANTS_A_CHARGE_KEY = "nbEnfantsACharge";
	public static final String NO_DOSSIER_PERS_KEY = "noDossierPers";
	public static final String NO_EPICEA_KEY = "noEpicea";
	public static final String NO_MATRICULE_KEY = "noMatricule";
	public static final String NPC_KEY = "npc";
	public static final String NUMEN_KEY = "numen";
	public static final String TEM_BUDGET_ETAT_KEY = "temBudgetEtat";
	public static final String TEM_IMPOSABLE_KEY = "temImposable";
	public static final String TEM_PAIE_SECU_KEY = "temPaieSecu";
	public static final String TEM_SAUVADET_KEY = "temSauvadet";
	public static final String TEM_TITULAIRE_KEY = "temTitulaire";
	public static final String TXT_LIBRE_KEY = "txtLibre";

	// Attributs non visibles
	public static final String C_LIMITE_AGE_KEY = "cLimiteAge";
	public static final String C_LOGE_KEY = "cLoge";

	//Colonnes dans la base de donnees
	public static final String AFFECTE_DEFENSE_COLKEY = "AFFECTE_DEFENSE";
	public static final String CIR_D_CERTIFICATION_COLKEY = "CIR_D_CERTIFICATION";
	public static final String CIR_D_COMPLETUDE_COLKEY = "CIR_D_COMPLETUDE";
	public static final String CIR_D_VERIFICATION_COLKEY = "CIR_D_VERIFICATION";
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_SITUATION_FAMILLE_COLKEY = "D_SITUATION_FAMILLE";
	public static final String ID_MINISTERE_COLKEY = "ID_MINISTERE";
	public static final String NB_ENFANTS_COLKEY = "NB_ENFANTS";
	public static final String NB_ENFANTS_A_CHARGE_COLKEY = "NB_ENFANTS_A_CHARGE";
	public static final String NO_DOSSIER_PERS_COLKEY = "NO_DOSSIER_PERS";
	public static final String NO_EPICEA_COLKEY = "NO_EPICEA";
	public static final String NO_MATRICULE_COLKEY = "NO_MATRICULE";
	public static final String NPC_COLKEY = "NPC";
	public static final String NUMEN_COLKEY = "NUMEN";
	public static final String TEM_BUDGET_ETAT_COLKEY = "TEM_BUDGET_ETAT";
	public static final String TEM_IMPOSABLE_COLKEY = "TEM_IMPOSABLE";
	public static final String TEM_PAIE_SECU_COLKEY = "TEM_PAIE_SECU";
	public static final String TEM_SAUVADET_COLKEY = "TEM_SAUVADET";
	public static final String TEM_TITULAIRE_COLKEY = "TEM_TITULAIRE";
	public static final String TXT_LIBRE_COLKEY = "TXT_LIBRE";

	public static final String C_LIMITE_AGE_COLKEY = "C_LIMITE_AGE";
	public static final String C_LOGE_COLKEY = "C_LOGE";


	// Relationships
	public static final String IDENTITE_INDIVIDU_KEY = "identiteIndividu";
	public static final String LIMITE_AGE_KEY = "limiteAge";
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_LOGE_KEY = "toLoge";
	public static final String TO_MINISTERE_KEY = "toMinistere";
	public static final String TO_STRUCTURE_KEY = "toStructure";



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
	public String affecteDefense() {
		return (String) storedValueForKey(AFFECTE_DEFENSE_KEY);
	}

	public void setAffecteDefense(String value) {
		takeStoredValueForKey(value, AFFECTE_DEFENSE_KEY);
	}

	public NSTimestamp cirDCertification() {
		return (NSTimestamp) storedValueForKey(CIR_D_CERTIFICATION_KEY);
	}

	public void setCirDCertification(NSTimestamp value) {
		takeStoredValueForKey(value, CIR_D_CERTIFICATION_KEY);
	}

	public NSTimestamp cirDCompletude() {
		return (NSTimestamp) storedValueForKey(CIR_D_COMPLETUDE_KEY);
	}

	public void setCirDCompletude(NSTimestamp value) {
		takeStoredValueForKey(value, CIR_D_COMPLETUDE_KEY);
	}

	public NSTimestamp cirDVerification() {
		return (NSTimestamp) storedValueForKey(CIR_D_VERIFICATION_KEY);
	}

	public void setCirDVerification(NSTimestamp value) {
		takeStoredValueForKey(value, CIR_D_VERIFICATION_KEY);
	}

	public String cStructure() {
		return (String) storedValueForKey(C_STRUCTURE_KEY);
	}

	public void setCStructure(String value) {
		takeStoredValueForKey(value, C_STRUCTURE_KEY);
	}

	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public NSTimestamp dSituationFamille() {
		return (NSTimestamp) storedValueForKey(D_SITUATION_FAMILLE_KEY);
	}

	public void setDSituationFamille(NSTimestamp value) {
		takeStoredValueForKey(value, D_SITUATION_FAMILLE_KEY);
	}

	public Integer idMinistere() {
		return (Integer) storedValueForKey(ID_MINISTERE_KEY);
	}

	public void setIdMinistere(Integer value) {
		takeStoredValueForKey(value, ID_MINISTERE_KEY);
	}

	public Integer nbEnfants() {
		return (Integer) storedValueForKey(NB_ENFANTS_KEY);
	}

	public void setNbEnfants(Integer value) {
		takeStoredValueForKey(value, NB_ENFANTS_KEY);
	}

	public Integer nbEnfantsACharge() {
		return (Integer) storedValueForKey(NB_ENFANTS_A_CHARGE_KEY);
	}

	public void setNbEnfantsACharge(Integer value) {
		takeStoredValueForKey(value, NB_ENFANTS_A_CHARGE_KEY);
	}

	public Integer noDossierPers() {
		return (Integer) storedValueForKey(NO_DOSSIER_PERS_KEY);
	}

	public void setNoDossierPers(Integer value) {
		takeStoredValueForKey(value, NO_DOSSIER_PERS_KEY);
	}

	public String noEpicea() {
		return (String) storedValueForKey(NO_EPICEA_KEY);
	}

	public void setNoEpicea(String value) {
		takeStoredValueForKey(value, NO_EPICEA_KEY);
	}

	public String noMatricule() {
		return (String) storedValueForKey(NO_MATRICULE_KEY);
	}

	public void setNoMatricule(String value) {
		takeStoredValueForKey(value, NO_MATRICULE_KEY);
	}

	public String npc() {
		return (String) storedValueForKey(NPC_KEY);
	}

	public void setNpc(String value) {
		takeStoredValueForKey(value, NPC_KEY);
	}

	public String numen() {
		return (String) storedValueForKey(NUMEN_KEY);
	}

	public void setNumen(String value) {
		takeStoredValueForKey(value, NUMEN_KEY);
	}

	public String temBudgetEtat() {
		return (String) storedValueForKey(TEM_BUDGET_ETAT_KEY);
	}

	public void setTemBudgetEtat(String value) {
		takeStoredValueForKey(value, TEM_BUDGET_ETAT_KEY);
	}

	public String temSauvadet() {
		return (String) storedValueForKey(TEM_SAUVADET_KEY);
	}

	public void setTemSauvadet(String value) {
		takeStoredValueForKey(value, TEM_SAUVADET_KEY);
	}

	public String temImposable() {
		return (String) storedValueForKey(TEM_IMPOSABLE_KEY);
	}

	public void setTemImposable(String value) {
		takeStoredValueForKey(value, TEM_IMPOSABLE_KEY);
	}

	public String temPaieSecu() {
		return (String) storedValueForKey(TEM_PAIE_SECU_KEY);
	}

	public void setTemPaieSecu(String value) {
		takeStoredValueForKey(value, TEM_PAIE_SECU_KEY);
	}

	public String temTitulaire() {
		return (String) storedValueForKey(TEM_TITULAIRE_KEY);
	}

	public void setTemTitulaire(String value) {
		takeStoredValueForKey(value, TEM_TITULAIRE_KEY);
	}

	public String txtLibre() {
		return (String) storedValueForKey(TXT_LIBRE_KEY);
	}

	public void setTxtLibre(String value) {
		takeStoredValueForKey(value, TXT_LIBRE_KEY);
	}

	public org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite identiteIndividu() {
		return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite)storedValueForKey(IDENTITE_INDIVIDU_KEY);
	}

	public void setIdentiteIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite value) {
		if (value == null) {
			org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite oldValue = identiteIndividu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, IDENTITE_INDIVIDU_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, IDENTITE_INDIVIDU_KEY);
		}
	}

	public org.cocktail.mangue.common.modele.nomenclatures.EOLimiteAge limiteAge() {
		return (org.cocktail.mangue.common.modele.nomenclatures.EOLimiteAge)storedValueForKey(LIMITE_AGE_KEY);
	}

	public void setLimiteAgeRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOLimiteAge value) {
		if (value == null) {
			org.cocktail.mangue.common.modele.nomenclatures.EOLimiteAge oldValue = limiteAge();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LIMITE_AGE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, LIMITE_AGE_KEY);
		}
	}

	public org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu() {
		return (org.cocktail.mangue.modele.grhum.referentiel.EOIndividu)storedValueForKey(TO_INDIVIDU_KEY);
	}

	public void setToIndividuRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOIndividu value) {
		if (value == null) {
			org.cocktail.mangue.modele.grhum.referentiel.EOIndividu oldValue = toIndividu();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_KEY);
		}
	}

	public org.cocktail.mangue.common.modele.nomenclatures.EOLoge toLoge() {
		return (org.cocktail.mangue.common.modele.nomenclatures.EOLoge)storedValueForKey(TO_LOGE_KEY);
	}

	public void setToLogeRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOLoge value) {
		if (value == null) {
			org.cocktail.mangue.common.modele.nomenclatures.EOLoge oldValue = toLoge();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_LOGE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_LOGE_KEY);
		}
	}

	public org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres toMinistere() {
		return (org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres)storedValueForKey(TO_MINISTERE_KEY);
	}

	public void setToMinistereRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres value) {
		if (value == null) {
			org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres oldValue = toMinistere();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MINISTERE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_MINISTERE_KEY);
		}
	}

	public org.cocktail.mangue.modele.grhum.referentiel.EOStructure toStructure() {
		return (org.cocktail.mangue.modele.grhum.referentiel.EOStructure)storedValueForKey(TO_STRUCTURE_KEY);
	}

	public void setToStructureRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOStructure value) {
		if (value == null) {
			org.cocktail.mangue.modele.grhum.referentiel.EOStructure oldValue = toStructure();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_STRUCTURE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_STRUCTURE_KEY);
		}
	}


	/**
	 * Créer une instance de EOPersonnel avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	 public static  EOPersonnel createEOPersonnel(EOEditingContext editingContext, NSTimestamp dCreation
			 , NSTimestamp dModification
			 , Integer noDossierPers
			 , String temImposable
			 , String temPaieSecu
			 , String temTitulaire
			 , org.cocktail.mangue.modele.grhum.referentiel.EOIndividuIdentite identiteIndividu, org.cocktail.mangue.modele.grhum.referentiel.EOIndividu toIndividu			) {
		 EOPersonnel eo = (EOPersonnel) createAndInsertInstance(editingContext, _EOPersonnel.ENTITY_NAME);    
		 eo.setDCreation(dCreation);
		 eo.setDModification(dModification);
		 eo.setNoDossierPers(noDossierPers);
		 eo.setTemImposable(temImposable);
		 eo.setTemPaieSecu(temPaieSecu);
		 eo.setTemTitulaire(temTitulaire);
		 eo.setIdentiteIndividuRelationship(identiteIndividu);
		 eo.setToIndividuRelationship(toIndividu);
		 return eo;
	 }


	 public EOPersonnel localInstanceIn(EOEditingContext editingContext) {
		 return (EOPersonnel)localInstanceOfObject(editingContext, this);
	 }


	 /**
	  * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	  * @param editingContext
	  * 
	  * @return L'objet insere dans l'editing context.
	  */
	 public static EOPersonnel creerInstance(EOEditingContext editingContext) {
		 return creerInstance(editingContext, null);
	 }

	 /**
	  * Cree une instance de l'objet et l'insere dans l'editing context.
	  * @param editingContext
	  * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	  * @return L'objet insere dans l'editing context.
	  */
	 public static EOPersonnel creerInstance(EOEditingContext editingContext, NSArray specificites) {
		 EOPersonnel object = (EOPersonnel)createAndInsertInstance(editingContext, _EOPersonnel.ENTITY_NAME, specificites);
		 return object;
	 }



	 public static EOPersonnel localInstanceIn(EOEditingContext editingContext, EOPersonnel eo) {
		 EOPersonnel localInstance = (eo == null) ? null : (EOPersonnel)localInstanceOfObject(editingContext, eo);
		 if (localInstance == null && eo != null) {
			 throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		 }
		 return localInstance;
	 }

	 /**
	  * 
	  * @param editingContext
	  * @param eo
	  * @return L'objet eo dans l'editingContext
	  * @deprecated Utilisez EOPersonnel#localInstanceIn a la place.
	  */
	 public static EOPersonnel localInstanceOf(EOEditingContext editingContext, EOPersonnel eo) {
		 return EOPersonnel.localInstanceIn(editingContext, eo);
	 }







	 /* Finders */

	 public static NSArray fetchAll(EOEditingContext editingContext) {
		 return fetchAll(editingContext, (EOQualifier)null);
	 }

	 public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		 return fetchAll(editingContext, null, sortOrderings);
	 }

	 public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		 return fetchAll(editingContext, qualifier, null, false, null);
	 }

	 public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	 }

	 public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
		 return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
	 }

	 public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		 return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	 }

	 public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
		 EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		 fetchSpec.setIsDeep(true);
		 fetchSpec.setUsesDistinct(distinct);
		 if (prefetchs != null)
			 fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
		 NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
		 return eoObjects;
	 }

	 /**
	  * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	  * Une exception est declenchee si plusieurs objets sont trouves.
	  * 
	  * @return Renvoie l'objet correspondant a la paire cle/valeur
	  * @throws IllegalStateException  
	  */
	 public static EOPersonnel fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		 return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	 }


	 /**
	  * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	  * Une exception est declenchee si plusieurs objets sont trouves.
	  * 
	  * @param editingContext
	  * @param qualifier
	  * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	  * @throws IllegalStateException
	  */
	 public static EOPersonnel fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		 NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		 EOPersonnel eoObject;
		 int count = eoObjects.count();
		 if (count == 0) {
			 eoObject = null;
		 }
		 else if (count == 1) {
			 eoObject = (EOPersonnel)eoObjects.objectAtIndex(0);
		 }
		 else {
			 throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		 }
		 return eoObject;
	 }




	 public static EOPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		 return fetchFirstByQualifier(editingContext, qualifier, null);
	 }

	 public static EOPersonnel fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		 NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		 EOPersonnel eoObject;
		 int count = eoObjects.count();
		 if (count == 0) {
			 eoObject = null;
		 }
		 else {
			 eoObject = (EOPersonnel)eoObjects.objectAtIndex(0);
		 }
		 return eoObject;
	 }  


	 /**
	  * Une exception est declenchee si aucun objet est trouve.
	  * 
	  * @param editingContext
	  * @param qualifier Le filtre
	  * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	  * @throws NoSuchElementException si aucun objet est trouve
	  */
	 public static EOPersonnel fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		 EOPersonnel eoObject = fetchFirstByQualifier(editingContext, qualifier);
		 if (eoObject == null) {
			 throw new NoSuchElementException("Aucun objet EOPersonnel ne correspond au qualifier '" + qualifier + "'.");
		 }
		 return eoObject;
	 }	


	 public static EOPersonnel fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		 return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	 }





}

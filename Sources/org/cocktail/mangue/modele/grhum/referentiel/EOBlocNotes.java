// EOBlocNotes.java

// Created on Wed Mar 05 15:06:14  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.grhum.referentiel;

import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOBlocNotes extends _EOBlocNotes {

	private final static EOSortOrdering SORT_DATE_DESC = new EOSortOrdering(D_BLOC_NOTES_KEY, EOSortOrdering.CompareDescending);
	private final static NSArray SORT_ARRAY_DATE_DESC = new NSArray(SORT_DATE_DESC);

	public EOBlocNotes() {
		super();
	}

	public static EOBlocNotes creer(EOEditingContext ec, EOIndividu individu) {

		EOBlocNotes object = (EOBlocNotes)CocktailUtilities.instanceForEntity(ec, ENTITY_NAME);

		object.setDBlocNotes(new NSTimestamp());
		object.setPersId(individu.persId());

		ec.insertObject(object);
		return object;
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray rechercherPourIndividu(EOEditingContext ec,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = %@", new NSArray(individu.persId())));
			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DESC);		
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	public void initAvecIndividu(EOIndividu individu) {
		setDBlocNotes(new NSTimestamp());
		setPersId(individu.persId());
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (llBlocNotes() != null && llBlocNotes().length() > 500) {
			throw new NSValidation.ValidationException("Le texte comporte au maximum 500 caractères");
		}
	}
	public String lcBlocNotes() {
		if (llBlocNotes() == null) {
			return null;
		} else if (llBlocNotes().length() < 100) {
			return llBlocNotes();
		} else {
			return llBlocNotes().substring(0,100) + "...";
		}
	}
	public String dateBlocNotes() {
		if (dBlocNotes() == null) {
			return "";
		} else {
			return DateCtrl.dateToString(dBlocNotes());
		}
	}
	// Méthodes statiques
	public static NSArray rechercherNotesPourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		return rechercherNotesPourIndividuEtPeriode(editingContext, individu,null,null);
	}
	public static NSArray rechercherNotesPourIndividuEtPeriode(EOEditingContext ec,EOIndividu individu,NSTimestamp dateDebut, NSTimestamp dateFin) {
		if (individu == null) {
			return null;
		}
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + "=%@", new NSArray(individu.persId())));
		if (dateDebut != null || dateFin != null) {
			if (dateDebut == null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_BLOC_NOTES_KEY + " <= %@", new NSArray(dateFin)));
			} else {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(D_BLOC_NOTES_KEY, dateDebut, D_BLOC_NOTES_KEY, dateFin));
			}
		}
		
		return fetchAll(ec,new EOAndQualifier(qualifiers), SORT_ARRAY_DATE_DESC );

	}
}

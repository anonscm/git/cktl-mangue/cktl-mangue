//EOEnfant.java
//Created on Fri Feb 21 09:21:13  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;


import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
/** Regles de validation<BR>
  	Verifie la longueur des chaines de caracteres<BR>
	Verifie que la date et le sexe sont fournis<BR>
	Verifie que la date de naissance n'est pas anterieure a celle du pere ou de la mere<BR>
	Verifie que si la mere est decedee, la date de naissance n'est pas posterieure<BR>
	Verifie que la date de naissance n'est pas posterieure a la date de deces<BR>
	Verifie que la date d'arrivee au foyer n'est pas anterieure a la date de naissance<BR>
	Verifie que les dates de naissance, deces et d'arrivee au foyer ne sont pas posterieures a la date du jour<BR>
	Verifie que si l'enfant est mort pour la France, la date de deces est fournie<BR>
	Verifie que pour un enfant autre que legitime, la date d'arrivee au foyer est fournie<BR>
	Verifie que pour un enfant adopte, la date de depot de requete d'adoption est fournie et qu'elle
	est posterieure a la date de naissance et	qu'elle n'est pas fournie dans les autres cas<BR>
	Verifie que  la date maximum de prise en charge posterieure a la date de naissance et qu'elle est 
	inferieure ou egale a la fin de l'annee scolaire qui comprend son 21eme anniversaire 
	(cas des enfants malades pendant les etudes)<BR>
	La date de fin de prise en charge ne peut etre posterieure a la date de deces<BR>
 * @author christine
 */

public class EOEnfant extends _EOEnfant {

	private static final long serialVersionUID = 3575830282921432473L;
	public static EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(NOM_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);

	private final static String DATE_POUR_BONIFICATION = "01/01/2004";

	public EOEnfant() {
		super();
	}

	public String toString() {
		return prenom() + " " + nom();
	}
	
	public static EOEnfant creer(EOEditingContext ec, EOIndividu individu) {

		EOEnfant newObject = (EOEnfant) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.setEstMortPourLaFrance(false);
		newObject.setTemValide(CocktailConstantes.VRAI);
		newObject.setNom(individu.nomUsuel());
		int numeroOrdre = 1;
		if (individu.enfants() != null) {
			numeroOrdre = individu.enfants().count() + 1;
		}
		newObject.setNoOrdreNaissance(new Integer(numeroOrdre));
		if (individu.personnel() != null) {
			if (individu.personnel().nbEnfants() != null) {
				individu.personnel().setNbEnfants(new Integer(individu.personnel().nbEnfants().intValue() + 1));
			} else {
				individu.personnel().setNbEnfants(new Integer(1));
			}
		}

		return newObject;		
	}


	public String sexeCir() {

		if (sexe() == null)
			return null;

		if (sexe().equals("M"))
			return "1";

		return "2";
	}

	/** Tests de coherence pour l'enregistrement d'un enfant */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if (temValide().equals(CocktailConstantes.VRAI)) {

			if (nom() == null || nom().length() > 40) {
				throw new NSValidation.ValidationException("Le nom est obligatoire et ne doit pas comporter plus de 40 caractères !");
			}
			if ((prenom() == null) || prenom().length() > 20) {
				throw new NSValidation.ValidationException("Le prénom est obligatoire et ne doit pas comporter plus de 20 caractères !");
			}
			if (lieuNaissance() != null && lieuNaissance().length() > 60) {
				throw new NSValidation.ValidationException("Le lieu de naissance ne doit pas comporter plus de 60 caractères !");
			}

			String message = validationsCir();
			if (message != null && message.length() > 0)
				throw new NSValidation.ValidationException(message);

			// vérifier que la date de naissance n'est pas postérieure à la date de décès !!
			if (dDeces() != null && DateCtrl.isBefore(dDeces(),dNaissance())) {
				throw new NSValidation.ValidationException("La date de naissance de l'enfant ne peut être postérieure à la date de décès !!");
			}

			//	vérifier que la date de naissance n'est pas postérieure à la date d'arrivée au foyer !!
			if (dArriveeFoyer() != null && DateCtrl.isBefore(dArriveeFoyer(),dNaissance())) {
				throw new NSValidation.ValidationException("La date d'arrivée au foyer ne peut être antérieure à la date de naissance !!");
			}
			// vérifier que la date de décès n'est pas antérieure à la date d'arrivée au foyer !!
			if (dDeces() != null && dArriveeFoyer() != null && DateCtrl.isBefore(dDeces(),dArriveeFoyer())) {
				throw new NSValidation.ValidationException("La date d'arrivée au foyer de l'enfant ne peut être postérieure à la date de décès !!");
			}

			// vérifier la cohérence des dates par rapport à la date du jour
			NSTimestamp today = new NSTimestamp();
			if (DateCtrl.isAfter(dNaissance(),today)) {
				throw new NSValidation.ValidationException("La date de naissance ne peut être postérieure à aujourd'hui");
			}
			if (dDeces() != null) {
				if (DateCtrl.isAfter(dDeces(),today)) {
					throw new NSValidation.ValidationException("La date de décès ne peut être postérieure à aujourd'hui");
				}
				// 15/07/2010 - la date de fin de prise en charge ne peut être postérieure à la date de décès
				if (dMaxACharge() == null || DateCtrl.isAfter(dMaxACharge(), dDeces())) {
					setDMaxACharge(dDeces());
				}
			}
			if (dArriveeFoyer() != null && DateCtrl.isAfter(dArriveeFoyer(),today)) {
				throw new NSValidation.ValidationException("La date d'arrivée au foyer ne peut être postérieure à aujourd'hui");
			}
			// La date de naissance est valide : on enregistre dSeizeAns et dVingtAns
			setDSeizeAns(DateCtrl.timestampByAddingGregorianUnits(dNaissance(),16,0,0,2,0,0));	
			setDVingtAns(DateCtrl.timestampByAddingGregorianUnits(dNaissance(),20,0,0,2,0,0));	

			if (dMaxACharge() == null) {
				throw new NSValidation.ValidationException("Vous devez fournir la date maximum de prise en charge");
			} else {
				
				if (DateCtrl.isBefore(dMaxACharge(), dNaissance()))
					throw new NSValidation.ValidationException("La date maximale de fin de prise en charge ne peut être antérieure à la date de naissance");
				
				// Si l'enfant doit arrêter ses études pour cause de maladie, l'âge peut être repoussé jusqu'à la fin de l'année scolaire
				// qui comprend son 21ème anniversaire. L'année scolaire commence le 1er octobre et finit le 30 septembre suivant.
				if (estDateMaxPriseEnChargeValide() == false) {
					throw new NSValidation.ValidationException("La date maximale de fin de prise en charge ne peut être postérieure à la fin de l'année scolaire comprenant le 21ème anniversaire (si l'enfant a dû interrompre ses études pour maladie)");
				}
			}
		}
		
	}


	/**
	 * 
	 * @return
	 */
	public String validationsCir() {

		if (estMortPourLaFrance() && dDeces() == null) {
			return "ENFANTS - Pour un enfant mort pour la France, vous devez fournir la date de décès";
		}

		if (dNaissance() == null)
			return "ENFANTS - La date de naissance est obligatoire !";

		if (DateCtrl.isBefore(dNaissance(), DateCtrl.stringToDate("01/01/1950")))
			return "ENFANTS - Veuillez vérifier l'année de la date de naissance !";

		if (DateCtrl.isBefore(dMaxACharge(), dNaissance()))
			return "ENFANTS - La date de fin de prise en charge ne peut être antérieure à la date de naissance !";

		if(sexe() == null)
			return "ENFANTS - Veuillez renseigner le sexe de l'enfant ";

		return null;

	}



	public String identite() {
		return prenom() + " " + nom();
	}
	public boolean estMortPourLaFrance() {
		return mortPourLaFrance() != null && mortPourLaFrance().equals(CocktailConstantes.VRAI);
	}
	public void setEstMortPourLaFrance(boolean aBool) {
		if (aBool) {
			setMortPourLaFrance(CocktailConstantes.VRAI);
		} else {
			setMortPourLaFrance(CocktailConstantes.FAUX);
		}
	}
	public String dateNaissanceFormatee() {
		return SuperFinder.dateFormatee(this,D_NAISSANCE_KEY);
	}
	public void setDateNaissanceFormatee(String uneDate) {
		if (uneDate == null) {
			setDNaissance(null);
		} else {
			SuperFinder.setDateFormatee(this,D_NAISSANCE_KEY,uneDate);
		}	
	}
	public String dateAdoptionFormatee() {
		return SuperFinder.dateFormatee(this,D_ADOPTION_KEY);
	}

	public void setDateAdoptionFormatee(String uneDate) {
		if (uneDate == null) {
			setDAdoption(null);
		} else {
			SuperFinder.setDateFormatee(this,D_ADOPTION_KEY,uneDate);
		}	}
	public String dateDecesFormatee() {
		return SuperFinder.dateFormatee(this,D_DECES_KEY);
	}

	public void setDateDecesFormatee(String uneDate) {
		if (uneDate == null) {
			setDDeces(null);
		} else {
			SuperFinder.setDateFormatee(this,D_DECES_KEY,uneDate);
		}
	}
	public String dateArriveeFoyerFormatee() {
		return SuperFinder.dateFormatee(this,D_ARRIVEE_FOYER_KEY);
	}

	public void setDateArriveeFoyerFormatee(String uneDate) {
		if (uneDate == null) {
			setDArriveeFoyer(null);
		} else {
			SuperFinder.setDateFormatee(this,D_ARRIVEE_FOYER_KEY,uneDate);
		}
	}
	public String dateMaxPriseEnChargeFormatee() {
		return SuperFinder.dateFormatee(this,D_MAX_A_CHARGE_KEY);
	}

	public void setDateMaxPriseEnChargeFormatee(String uneDate) {
		if (uneDate == null) {
			setDMaxACharge(null);
		} else {
			SuperFinder.setDateFormatee(this,D_MAX_A_CHARGE_KEY,uneDate);
		}
	}
	/** Retourne true si la date maximum de prise en charge est posterieure a la date de naissance et inferieure ou egale a
	 * la fin de l'annee scolaire du 21eme anniversaire */
	public boolean estDateMaxPriseEnChargeValide() {
		if (dNaissance() == null || dMaxACharge() == null) {	// au cas où
			return true;
		}
		// La date de prise en charge doit être postérieure à la date de naissance
		if (DateCtrl.isBefore(dMaxACharge(), dNaissance())) {
			return false;
		}
		// Si l'enfant doit arrêter ses études pour cause de maladie, l'âge peut être repoussé jusqu'à la fin de l'année scolaire
		// qui comprend son 21ème anniversaire. L'année scolaire commence le 1er octobre et finit le 30 septembre suivant.
		NSTimestamp date21 = DateCtrl.dateAvecAjoutAnnees(dNaissance(), 21);
		int annee = DateCtrl.getYear(date21);
		NSTimestamp dateMax = DateCtrl.stringToDate("30/09/" + annee);
		return (DateCtrl.isBeforeEq(dMaxACharge(), dateMax));
	}

	public String noInsee() {
		return null;
	}
	public String dptNaissance() {
		return null;
	}
	public String paysNaissance() {
		return null;
	}

	public String nbTrimestresBonification() {
		return null;
	}
	public String dureeMajorationPourInfirmite() {
		return null;
	}
	public static NSArray rechercherEnfantsPourQualifier(EOEditingContext ec,EOQualifier qualifier) {
		try {
			NSArray reparts = EORepartEnfant.fetchAll(ec, qualifier, null);
			return (NSArray)reparts.valueForKey(EORepartEnfant.ENFANT_KEY);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec : editing context
	 * @param nom : nom de l'enfant
	 * @param prenom : prénom de l'enfant
	 * @return la liste des enfants correspondant au filtre
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOEnfant> rechercherEnfantsPourCreation(EOEditingContext ec, String nom, String prenom)	{
		try {

			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray<String>(CocktailConstantes.VRAI)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEnfant.NOM_KEY + " caseInsensitiveLike %@", 
					new NSArray<String>("*" + nom.trim() + "*")));
			
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEnfant.PRENOM_KEY + " caseInsensitiveLike %@", 
					new NSArray<String>("*" + prenom.trim() + "*")));

			return fetchAll(ec,new EOAndQualifier(qualifiers), null );
		}
		catch (Exception e)	{
			return null;
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @return
	 */
	public static NSArray rechercherEnfantsPourIndividu(EOEditingContext editingContext,EOIndividu individu) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+EOEnfant.TEM_VALIDE_KEY + " =%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.PARENT_KEY+ " =%@", new NSArray(individu)));

			NSArray reparts = EORepartEnfant.fetchAll(editingContext, new EOAndQualifier(qualifiers), null);
			return (NSArray)reparts.valueForKey(EORepartEnfant.ENFANT_KEY);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}
	public static NSArray rechercherEnfantsAvecLimiteAgePourIndividu(EOEditingContext editingContext,EOIndividu individu, NSTimestamp dateReference, Integer limiteAge) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+EOEnfant.TEM_VALIDE_KEY + " =%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.PARENT_KEY+ " =%@", new NSArray(individu)));

			int ageControleDebut = new Integer(limiteAge).intValue();
			NSTimestamp dateControleDebut = dateReference.timestampByAddingGregorianUnits(-ageControleDebut,0,0,0,0,0);
			NSTimestamp dateControleFin = dateReference;

			System.out
			.println("EOEnfant.rechercherEnfantsAvecLimiteAgePourIndividu() CONTROLE DEBUT : " + dateControleDebut);
			System.out
			.println("EOEnfant.rechercherEnfantsAvecLimiteAgePourIndividu() CONTROLE FIN : " + dateControleFin);

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+EOEnfant.D_NAISSANCE_KEY + " >=%@", new NSArray(dateControleDebut)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+EOEnfant.D_NAISSANCE_KEY + " <=%@", new NSArray(dateControleFin)));

			NSArray reparts = EORepartEnfant.fetchAll(editingContext, new EOAndQualifier(qualifiers), null);			
			return (NSArray)reparts.valueForKey(EORepartEnfant.ENFANT_KEY);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}
	public static NSArray rechercherEnfantsAdoptesPourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+EOEnfant.TEM_VALIDE_KEY + " =%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.PARENT_KEY+ " =%@", new NSArray(individu)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+EOEnfant.D_ADOPTION_KEY+ " != nil", null));

			NSArray reparts = EORepartEnfant.fetchAll(editingContext, new EOAndQualifier(qualifiers), null);
			return (NSArray)reparts.valueForKey(EORepartEnfant.ENFANT_KEY);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}


	public static NSArray rechercherEnfantsduMemeJourDeParentDeSexe(EOEditingContext editingContext,NSTimestamp dateNaissance,String sexe,boolean estPere) {
		// comme la comparaison avec les heures ne va pas marcher, on prend compris entre le jour précédent et le jour suivant
		NSTimestamp jourPrecedent = DateCtrl.jourPrecedent(dateNaissance);
		NSTimestamp jourSuivant = DateCtrl.jourSuivant(dateNaissance);

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_NAISSANCE_KEY + ">%@", new NSArray(jourPrecedent)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_NAISSANCE_KEY + "<%@", new NSArray(jourSuivant)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SEXE_KEY + "=%@", new NSArray(sexe)));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), null);

		//		NSMutableArray args = new NSMutableArray(jourPrecedent);
		//		args.addObject(jourSuivant);
		//		args.addObject(sexe);
		//		String stringQualifier = "dNaissance > %@ AND dNaissance < %@ AND sexe = %@ AND temValide = 'O'";
		//
		//		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		//		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier, null);
		//		// 18/06/2010 - suppression de refresh dans les fetchs qui créaient des plantages
		//		//fs.setRefreshesRefetchedObjects(true);
		//		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** recherche les enfants d'un individu nes ou arrives au foyer (adoption) a une certaine date */
	public static NSArray rechercherEnfantsPourIndividuDate(EOEditingContext editingContext,EOIndividu individu, NSTimestamp date,boolean estAdoption) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.PARENT_KEY+ "=%@", new NSArray(individu)));

		if (date != null) {
			if (estAdoption) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+D_ARRIVEE_FOYER_KEY + ">=%@", new NSArray(date)));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+D_ARRIVEE_FOYER_KEY + "<%@", new NSArray(DateCtrl.jourSuivant(date))));
			}
			else {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+D_NAISSANCE_KEY + ">=%@", new NSArray(date)));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartEnfant.ENFANT_KEY+"."+D_NAISSANCE_KEY + "<%@", new NSArray(DateCtrl.jourSuivant(date))));
			}
		}
		NSArray reparts = EORepartEnfant.fetchAll(editingContext, new EOAndQualifier(qualifiers), null);

		return (NSArray)reparts.valueForKey(EORepartEnfant.ENFANT_KEY);

		//		NSMutableArray args = new NSMutableArray(individu);
		//		String stringQualifier = "enfant.temValide = 'O' AND parent = %@";
		//		if (date != null) {
		//			args.addObject(date);
		//			args.addObject(DateCtrl.jourSuivant(date));
		//
		//			if (estAdoption) {
		//				stringQualifier = stringQualifier + " AND enfant.dArriveeFoyer >= %@ AND enfant.dArriveeFoyer < %@";
		//			} else {
		//				stringQualifier = stringQualifier + " AND enfant.dNaissance >= %@ AND enfant.dNaissance < %@";
		//			}
		//		}
		//		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		//		EOFetchSpecification fs = new EOFetchSpecification(EORepartEnfant.ENTITY_NAME,qualifier, null);
		//		fs.setRefreshesRefetchedObjects(true);
		//		return (NSArray)editingContext.objectsWithFetchSpecification(fs).valueForKey(EORepartEnfant.ENFANT_KEY);
	}
}

//EORepartPersonneAdresse.java
//Created on Thu Mar 06 13:11:14  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;


import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
public class EORepartPersonneAdresse extends _EORepartPersonneAdresse {

	public static final NSArray SORT_RPA_VALIDE_DESC = new NSArray(new EOSortOrdering(RPA_VALIDE_KEY, EOSortOrdering.CompareDescending));
	public static final NSArray SORT_RPA_VALIDE_AS = new NSArray(new EOSortOrdering(RPA_VALIDE_KEY, EOSortOrdering.CompareAscending));

	public EORepartPersonneAdresse() {
		super();
	}

	public static void supprimer(EOEditingContext ec, EORepartPersonneAdresse rpa, String type) {

		EORepartPersonneAdresse rpaExistante = findForAdresseEtType(ec, rpa.persId(), rpa.toAdresse(), type);
		if (rpaExistante != null)
			ec.deleteObject(rpaExistante);

	}


	public static EORepartPersonneAdresse creer(EOEditingContext ec, Integer persId, EOAdresse adresse, String typeAdresse) {

		EORepartPersonneAdresse rpa = (EORepartPersonneAdresse)CocktailUtilities.instanceForEntity(ec, EORepartPersonneAdresse.ENTITY_NAME);
		rpa.setTadrCode(typeAdresse);
		rpa.setAdrOrdre(adresse.adrOrdre());
		rpa.setPersId(persId);
		rpa.setToAdresseRelationship(adresse);		
		rpa.setRpaValide(CocktailConstantes.VRAI);
		rpa.setRpaPrincipal(CocktailConstantes.FAUX);
		rpa.setDCreation(new NSTimestamp());
		rpa.setDModification(new NSTimestamp());

		ec.insertObject(rpa);

		return rpa;

	}


	public boolean estValide() {
		return rpaValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setRpaValide(CocktailConstantes.VRAI);
		} else {
			setRpaValide(CocktailConstantes.FAUX);
		}
	}

	public boolean estPersonnelle() {
		return tadrCode().equals(EOTypeAdresse.TYPE_PERSONNELLE);
	}
	public boolean estProfessionnelle() {
		return tadrCode().equals(EOTypeAdresse.TYPE_PROFESSIONNELLE);
	}
	public boolean estFacturation() {
		return tadrCode().equals(EOTypeAdresse.TYPE_FACTURATION);
	}
	public boolean estEtudiant() {
		return tadrCode().equals(EOTypeAdresse.TYPE_ETUDIANT);
	}
	public boolean estParent() {
		return tadrCode().equals(EOTypeAdresse.TYPE_PARENT);
	}

	public void init(EOIndividu individu) {
		setRpaPrincipal(CocktailConstantes.FAUX);
		setRpaValide(CocktailConstantes.VRAI);
		setPersId(individu.persId());
	}
	public void initAdresseEntreprise(Number persId,EOAdresse adresse) {
		setRpaValide(CocktailConstantes.VRAI);
		setPersId(new Integer(persId.intValue()));
		setRpaPrincipal(CocktailConstantes.VRAI);
		setTadrCode(EOTypeAdresse.TYPE_PROFESSIONNELLE);
		setAdrOrdre(adresse.adrOrdre());
		setToAdresseRelationship(adresse);
	}
	
	/** Initialise les champs de la repart courante avec ceux de la repart passee en parametre sans prendre
	 * en compte l'adresse de cette derniere */
	public void initAvecIdEtRepartAdresse(Number persId,EORepartPersonneAdresse repart) {
		setRpaValide(repart.rpaValide());
		setRpaPrincipal(repart.rpaPrincipal());
		setTadrCode(repart.tadrCode());
		setEMail(repart.eMail());
		setPersId(new Integer(persId.intValue()));
	}
	
	/** Initialise une repartPersonneAdresse de facturation */
	public void initRepartFournisseurPourIndividuEtAdresse(EOIndividu individu,EOAdresse adresse) {
		init(individu);
		setTadrCode(EOTypeAdresse.TYPE_FACTURATION);
		setAdrOrdre(adresse.adrOrdre());
		setToAdresseRelationship(adresse);
	}
	
	/**
	 * 
	 */
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		
		// Controle de l'email
		if (eMail() != null && StringCtrl.replace(eMail(), " ", "").length() > 0) {
			if (eMail().length() > 120) {
				throw new com.webobjects.foundation.NSValidation.ValidationException("Une adresse email comporte au plus 120 caractères !");
			} else {
				int index = eMail().indexOf("@");
				if (index == -1) {
					throw new com.webobjects.foundation.NSValidation.ValidationException("L'adresse email n'est pas valide !");
				}
			}
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	public boolean estAdressePrincipale() {
		return rpaPrincipal().equals(CocktailConstantes.VRAI);
	}
	public void setEstAdressePrincipale(boolean aBool) {
		if (aBool) {
			setRpaPrincipal(CocktailConstantes.VRAI);
		} else {
			setRpaPrincipal(CocktailConstantes.FAUX);
		}
	}
	public boolean estUtiliseePaye() {
		return toAdresse() != null && toAdresse().temPayeUtil() != null && toAdresse().temPayeUtil().equals(CocktailConstantes.VRAI);
	}
	
	/**
	 * 
	 * @param ec
	 * @param structure
	 * @param type
	 * @return
	 */
	public static EOAdresse rechercherAdresse(EOEditingContext ec, EOStructure structure, String type) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.PERS_ID_KEY + "=%@", new NSArray(structure.persId())));
			//qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_PRINCIPAL_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY + "=%@", new NSArray(type)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			EORepartPersonneAdresse rpa = fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));

			if (rpa != null)
				return rpa.toAdresse();

			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}



	// méthodes statiques
	/** recherche les adresses de l'individu
	 * @param editingContext
	 * @param individu
	 * @return repartPersoAdresses
	 */
	public static NSArray<EORepartPersonneAdresse> adresses(EOEditingContext editingContext, EOIndividu individu) {
		return adressesDeType(editingContext,individu,null,false);
	}
	/** recherche les adresses de l'individu
	 * @param editingContext
	 * @param individu
	 * @param typeAdresse type adresse
	 * @return repartPersoAdresses
	 */
	public static NSArray adressesValidesDeType(EOEditingContext editingContext, EOIndividu individu,String typeAdresse) {
		return adressesDeType(editingContext,individu,typeAdresse,true);
	}
	/** recherche les adresses de facturation de l'individu
	 * @param editingContext
	 * @param individu
	 * @return repartPersoAdresses
	 */
	public static NSArray<EORepartPersonneAdresse> adressesFacturationsValides(EOEditingContext editingContext, EOIndividu individu) {
		return adressesDeType(editingContext,individu,EOTypeAdresse.TYPE_FACTURATION,true);
	}
	/** recherche les adresses personnelles de l'individu
	 * @param editingContext
	 * @param individu
	 * @return repartPersoAdresses
	 */
	public static NSArray<EORepartPersonneAdresse> adressesPersoValides(EOEditingContext editingContext, EOIndividu individu) {
		return adressesDeType(editingContext,individu,EOTypeAdresse.TYPE_PERSONNELLE,true);
	}
	public static NSArray<EORepartPersonneAdresse> adressesProValides(EOEditingContext editingContext, EOIndividu individu) {
		return adressesDeType(editingContext,individu,EOTypeAdresse.TYPE_PROFESSIONNELLE,true);
	}

	/** recherche les adresses personnelles et professionnelles de l'individu
	 * @param editingContext
	 * @param individu
	 * @return repartPersoAdresses
	 */
	public static NSArray<EORepartPersonneAdresse> adressesPersoEtProfValides(EOEditingContext editingContext, EOIndividu individu) {
		NSArray args = new NSArray(individu.persId());
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = %@ AND (tadrCode = 'PERSO' OR tadrCode = 'PRO') AND rpaValide = 'O'",args);
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(TADR_CODE_KEY,EOSortOrdering.CompareAscending);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier, new NSArray(sort));

		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** recherche les adresses non personnelles de l'individu
	 * @param editingContext
	 * @param individu
	 * @return repartPersoAdresses
	 */
	public static NSArray autresAdressesValides(EOEditingContext editingContext, EOIndividu individu) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = " + individu.persId() + " and tadrCode != 'PERSO' and tadrCode != 'FACT' and rpaValide = '" + CocktailConstantes.VRAI + "'",null);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier, null);

		return editingContext.objectsWithFetchSpecification(fs);
	}

	/** recherche  la premiere adresse personnelle valide de l'individu
	 * @param editingContext
	 * @param individu
	 * @return adresses*/
	public static EOAdresse rechercherAdresseCourante(EOEditingContext editingContext, EOIndividu individu) {
		try {return ((EORepartPersonneAdresse)adressesPersoValides(editingContext,individu).objectAtIndex(0)).toAdresse();} 
		catch (Exception e) { return null; }
	}

	/** recherche les repart valides associees a une structure
	 * @param editingContext
	 * @param structure
	 * @return  */
	public static NSArray rechercherAdressesProfessionnellesValidesStructure(EOEditingContext editingContext, EOStructure structure) {
		NSMutableArray args = new NSMutableArray(structure.persId());
		args.addObject(EOTypeAdresse.TYPE_PROFESSIONNELLE);
		args.addObject(CocktailConstantes.VRAI);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = %@ AND tadrCode = %@ AND rpaValide = %@" ,args);

		return fetchAll(editingContext, qualifier);
	}

	// méthodes privées
	private static NSArray adressesDeType(EOEditingContext editingContext, EOIndividu individu,String typeAdresse,boolean uniquementValide) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + "=%@", new NSArray(individu.persId())));

		if (typeAdresse != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TADR_CODE_KEY + "=%@", new NSArray(typeAdresse)));

		if (uniquementValide)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(RPA_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers));
	}

	public static NSArray findForQualifier(EOEditingContext ec, EOQualifier qualifier) {		
		return fetchAll(ec, qualifier);
	}

	public static NSArray findForAdresse(EOEditingContext ec, EOIndividu individu, EOAdresse adresse) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + "=%@", new NSArray(individu.persId())));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_ADRESSE_KEY + "=%@", new NSArray(adresse)));

		return fetchAll(ec, new EOAndQualifier(qualifiers));
	}

	public static EORepartPersonneAdresse findForAdresseEtType(EOEditingContext ec, Integer persId, EOAdresse adresse, String type) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + "=%@", new NSArray(persId)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_ADRESSE_KEY + "=%@", new NSArray(adresse)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TADR_CODE_KEY + "=%@", new NSArray(type)));

		//qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(RPA_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}

	/**
	 * Recherche de l'adresse principale valide d'un invididu ou d'une structure
	 * @param ec
	 * @param persId Identifiant de l'individu ou de la structure
	 * @return
	 */
	public static EORepartPersonneAdresse adressePrincipale(EOEditingContext ec, Integer persId) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + "=%@", new NSArray(persId)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(RPA_PRINCIPAL_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(RPA_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	}


}

/*
 * Created on 23 mai 2005
 *
 * Modélise l'identité d'un individu. Construite sur la table EOIndividu. Ne peut être utilisée qu'en lecture
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;

import java.util.Enumeration;
import java.util.GregorianCalendar;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 *
 * Modelise l'identite d'un individu. Construite sur la table EOIndividu. Ne peut etre utilisee qu'en lecture
 */

public class EOIndividuIdentite extends _EOIndividuIdentite {
	
	public static String CONTRATS_KEY = "contrats";
	public static String CONTRATS_HEBERGES_KEY = "contratsHeberges";
	public static String CARRIERES_KEY = "carrieres";

	public EOIndividuIdentite() {
		super();
	}

	public String identite() {
		return nomUsuel() + " " + prenom();
	}
	public String identiteInverse() {
		return prenom() + " " + nomUsuel();
	}

/**
 * 
 * @param editingContext
 * @param individuID
 * @return
 */
	public static EOIndividuIdentite findForId(EOEditingContext edc,Number noIndividu) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(CocktailFinder.getQualifierEqual(NO_INDIVIDU_KEY, noIndividu));
		qualifiers.addObject(CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, CocktailConstantes.VRAI));

		return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));	
	}
	
	/** retourne la liste des employes en se limitant sur le nom ou prenom
	 * param editing context
	 * @param nom sur lequel restreindre la recherche (peut etre nul)
	 * @param prenom prenom sur lequel restreindre la recherche (peut etre nul)
	 * @param estNomPatronymique true sil faut faire la recherche sur le nom patronymique */
	public static NSArray rechercherPersonnels(EOEditingContext edc,String nom,String prenom,boolean estNomPatronymique, int typePersonnel) {
		
		LogManager.logDetail("rechercherPersonnels");

		NSMutableArray qualifiers = new NSMutableArray();
		NSMutableArray orQualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonnel.IDENTITE_INDIVIDU_KEY+"."+TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		
		if (nom != null) {
			if (estNomPatronymique) {
				
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonnel.IDENTITE_INDIVIDU_KEY+"."+NOM_PATRONYMIQUE_KEY  + " caseInsensitiveLike %@", new NSArray("*" + nom + "*")));
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonnel.IDENTITE_INDIVIDU_KEY+"."+NOM_USUEL_KEY  + " caseInsensitiveLike %@", new NSArray("*" + nom + "*")));
				
				qualifiers.addObject(new EOOrQualifier(orQualifiers));
				
			}
			else
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonnel.IDENTITE_INDIVIDU_KEY+"."+NOM_USUEL_KEY  + " caseInsensitiveLike %@", new NSArray("*" + nom + "*")));
		} 

		if (prenom != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPersonnel.IDENTITE_INDIVIDU_KEY+"."+PRENOM_KEY  + " caseInsensitiveLike %@", new NSArray("*" + prenom + "*")));
		}

		NSArray sorts = new NSArray(EOSortOrdering.sortOrderingWithKey(EOPersonnel.NO_DOSSIER_PERS_KEY,EOSortOrdering.CompareAscending));
		EOFetchSpecification fs = new EOFetchSpecification(EOPersonnel.ENTITY_NAME, new EOAndQualifier(qualifiers),sorts);
		fs.setRefreshesRefetchedObjects(true);

		if ((nom != null || prenom != null))
			fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOPersonnel.IDENTITE_INDIVIDU_KEY));

		NSArray personnels = edc.objectsWithFetchSpecification(fs);
		GregorianCalendar debut = new GregorianCalendar();

		NSArray	result = (NSArray)personnels.valueForKey(EOPersonnel.IDENTITE_INDIVIDU_KEY);

		GregorianCalendar fin = new GregorianCalendar();
		LogManager.logDetail("duree : " + (fin.getTimeInMillis() - debut.getTimeInMillis()) + " millisecondes");
		return result;
	}
	
	
	/** retourne la liste des employes en se limitant sur le nom ou prenom et en supprimant les heberges si necessaire
	 * param editing context
	 * @param nom sur lequel restreindre la recherche (peut etre nul)
	 * @param prenom prenom sur lequel restreindre la recherche (peut etre nul)
	 * @param estNomPatronymique true sil faut faire la recherche sur le nom patronymique
	 * @param exclureHeberges true si il faut exclure les heberges */
	public static NSArray rechercherPersonnels(EOEditingContext editingContext,String nom,String prenom,boolean estNomPatronymique,boolean exclureHeberges, int typePersonnel) {

		NSMutableArray individus = new NSMutableArray();

		NSArray result = rechercherPersonnels(editingContext, nom, prenom, estNomPatronymique, typePersonnel);

		if ( ! exclureHeberges)
			return result;

		// On ne garde que les individus qui ne sont pas heberges
		NSArray heberges = rechercherPersonnelsDeTypes(editingContext, ManGUEConstantes.HEBERGE, ManGUEConstantes.TOUS, nom, prenom,estNomPatronymique);
		for (java.util.Enumeration<EOIndividuIdentite> e = result.objectEnumerator();e.hasMoreElements();) {
			EOIndividuIdentite individu = e.nextElement();
			if ( ! heberges.containsObject(individu))
				individus.addObject(individu);
		}
		return new NSArray(individus);
	}
	/** Recherche les personnels d'un certain type sans exclure les heberges */
	public static NSArray rechercherPersonnelsDeTypes(EOEditingContext editingContext,int typePersonnel,int typeTemps,String nom,String prenom,boolean estNomPatronymique) {
		return rechercherPersonnelsDeTypes(editingContext, typePersonnel, typeTemps, nom, prenom, estNomPatronymique, false);
	}
	/** Recherche les personnels d'un certain type dont le contrat ou les elements de carriere
	 * sont valides au regard des criteres fournis en parametre
	 * Les reles de gestion sont les suivantes:<BR>
	 * On ne prend pas en compte les contrats annules et les elements de carriere avec
	 * un arrete d'annulation<BR>
	 * Dans le cas des vacataires, on ne s'interesse qu'aux contrats de vacation<BR>
	 * Pour les agents actuels, on retourne tous les agents ayant un contrat ou un element de
	 * carriere a la date du jour, restreints au type de personnel.<BR>
	 * Pour les agents passes, on retourne tous les agents ayant un contrat ou un element
	 * de carriere termines a la date du jour,restreints au type de personnel et n'ayant pas de contrat ou element
	 * de carriere en cours <BR>
	 * Pour les agents futurs, on retourne tous les agents ayant un contrat ou un element
	 * de carriere commencant avant la date du jour, restreints au type de personnel et n'ayant pas de contrat ou element
	 * de carriere en cours.<BR>
	 * Pour tous les agents, on retourne tous les agents passes, presents, futurs (sans doublons)
	 * de carriere en cours ou , restreints au type de personnel.<BR>
	 * @param editingContext
	 * @param typePersonnel	(tous/enseignants/non-enseignants/vacataires/heberges)
	 * @param typeTemps		(presents/anciens/futurs/tous);
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param estNomPatronymique true si on fait la recherche sur le nom patronymique
	 * @param exclureHeberge (dans le cas ou on recherche tout le monde
	 * @return personnels trouves (tableau de IndividuIdentite)
	 */
	public static NSArray rechercherPersonnelsDeTypes(EOEditingContext editingContext,int typePersonnel,int typeTemps,String nom,String prenom,boolean estNomPatronymique,boolean exclureHeberges) {
		GregorianCalendar debut = new GregorianCalendar();
		NSTimestamp dateReference = new NSTimestamp();
		// pour ramener à une date sans prendre en compte les heures
		dateReference = DateCtrl.stringToDate(DateCtrl.dateToString(dateReference));
		boolean supprimerDoublons = false;
		if (typeTemps == ManGUEConstantes.TOUT_EMPLOYE) {
			supprimerDoublons = true;
		} 
		if (nom != null) {
			nom = "*" + nom;
		}
		if (prenom != null) {
			prenom = "*" + prenom;
		}
		
		// Pour optimiser la recherche, on stocke dans individus les noDossierPers
		// on fera un fetch global de IdentiteIndividu à partir des noDossiersPers de individus
		NSMutableArray individus = new NSMutableArray();
		NSArray individusAAjouter;
		// on recherche tous les contrats actuels
		if (typeTemps == ManGUEConstantes.EMPLOYE_ACTUEL || typeTemps == ManGUEConstantes.TOUT_EMPLOYE) {
			
			if (typePersonnel != ManGUEConstantes.HEBERGE) {
				individusAAjouter = EOContrat.rechercherIndividusAvecContratsPourPeriodeEtTypePersonnel(editingContext,nom,prenom,dateReference,dateReference,typePersonnel,supprimerDoublons,estNomPatronymique);
				ajouterIndividus(individus,individusAAjouter);
			}
			if (typePersonnel != ManGUEConstantes.VACATAIRE && typePersonnel != ManGUEConstantes.HEBERGE) {
				individusAAjouter = EOElementCarriere.rechercherIndividusAvecElementsPourPeriodeEtTypePersonnel(editingContext,nom,prenom,dateReference,dateReference,typePersonnel,supprimerDoublons,estNomPatronymique);
				ajouterIndividus(individus,individusAAjouter);
			} 
			if (!exclureHeberges || typePersonnel == ManGUEConstantes.HEBERGE) {
				individusAAjouter = EOContratHeberges.rechercherIndividusAvecContratsHebergesPourPeriode(editingContext, nom, prenom, dateReference, dateReference, supprimerDoublons, estNomPatronymique);
				ajouterIndividus(individus,individusAAjouter);
			}
			
		}

		if (typeTemps == ManGUEConstantes.EMPLOYE_ANCIEN || typeTemps == ManGUEConstantes.TOUT_EMPLOYE) {
			// On recherche les agents qui ont des contrats passés
			if (typePersonnel != ManGUEConstantes.HEBERGE) {
				individusAAjouter = EOContrat.rechercherIndividusPourTypePersonnelEtContratsAnterieureDate(editingContext, nom, prenom, dateReference, typePersonnel, estNomPatronymique);
				ajouterIndividus(individus,individusAAjouter);
			}
			if (typePersonnel != ManGUEConstantes.VACATAIRE  && typePersonnel != ManGUEConstantes.HEBERGE) {
				// On ajoute les agents qui ont des éléments passés
				individusAAjouter = EOElementCarriere.rechercherIndividusAvecElementsAnterieursDatePourTypePersonnel(editingContext, nom, prenom, dateReference, typePersonnel, estNomPatronymique);
				ajouterIndividus(individus,individusAAjouter);
			}
			if (!exclureHeberges || typePersonnel == ManGUEConstantes.HEBERGE) {
				individusAAjouter = EOContratHeberges.rechercherIndividusPourContratsHebergesAnterieureDate(editingContext, nom, prenom, dateReference, estNomPatronymique);
				ajouterIndividus(individus,individusAAjouter);
			}
		}
		if (typeTemps == ManGUEConstantes.EMPLOYE_FUTUR || typeTemps == ManGUEConstantes.TOUT_EMPLOYE) {
			// ajouter à la liste des agents futur, toux ceux ayant un contrat dans le futur
			if (typePersonnel != ManGUEConstantes.HEBERGE) {
				individusAAjouter = EOContrat.rechercherIndividusAvecContratsFuturPourTypePersonnel(editingContext,nom,prenom,dateReference,typePersonnel,estNomPatronymique);
				ajouterIndividus(individus,individusAAjouter);
			}
			if (typePersonnel != ManGUEConstantes.VACATAIRE  && typePersonnel != ManGUEConstantes.HEBERGE) {
				individusAAjouter = EOElementCarriere.rechercherIndividusAvecElementsFutursPourTypePersonnel(editingContext,nom,prenom,dateReference,typePersonnel,estNomPatronymique);
				ajouterIndividus(individus,individusAAjouter);
			}
			if (!exclureHeberges || typePersonnel == ManGUEConstantes.HEBERGE) {
				individusAAjouter = EOContratHeberges.rechercherIndividusAvecContratsHebergesFuturs(editingContext, nom, prenom, dateReference, estNomPatronymique);
				ajouterIndividus(individus,individusAAjouter);
			}
		}
		if (typeTemps == ManGUEConstantes.EMPLOYE_ANCIEN || typeTemps == ManGUEConstantes.EMPLOYE_FUTUR) {
			// retirer tous les agents qui ont un contrat ou un segment de carrière quelque soit le type de personnel
			NSArray individusASupprimer = EOContrat.rechercherIndividusAvecContratsPourPeriodeEtTypePersonnel(editingContext,nom,prenom,dateReference,dateReference,ManGUEConstantes.TOUT_PERSONNEL,supprimerDoublons,estNomPatronymique);
			supprimerIndividus(individus,individusASupprimer);
			// retirer tous les agents qui ont des éléments de carrières actuels
			individusASupprimer = EOElementCarriere.rechercherIndividusAvecElementsPourPeriodeEtTypePersonnel(editingContext,nom,prenom,dateReference,dateReference,ManGUEConstantes.TOUT_PERSONNEL,supprimerDoublons,estNomPatronymique);
			supprimerIndividus(individus,individusASupprimer);
			// Pour les heberges, on ne le fait pas car il n'y a qu'un contrat d'heberge actif à la fois par individu
		}
		GregorianCalendar fin = new GregorianCalendar();
		LogManager.logDetail("duree : " + (fin.getTimeInMillis() - debut.getTimeInMillis()) + " millisecondes");
		return individus;
	}

	/** Recherche les personnels d'un certain type dont le contrat ou la carriere est valide a une certaine date
	 * @param editingContext
	 * @param typePersonnel	(tous/enseignants/non-enseignants)
	 * @param typeTemps		(presents, anciens,futurs);
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @return personnels trouves (tableau de IndividuIdentite)
	 */
	public static NSArray rechercherPersonnelsDeTypes(EOEditingContext editingContext,int typePersonnel,int typeTemps,String nom,String prenom) {
		return rechercherPersonnelsDeTypes(editingContext,typePersonnel,typeTemps,nom,prenom,false);
	}
	/** Recherche les personnels geres par un agent, d'un certain type 
	 * dont le contrat ou la carriere est valide a une certaine date
	 * restreints aux structures passees en parametre<BR>
	 * Dans le cas ou l'agent a demande tous les types de personnels mais il ne peut en
	 * voir que certains, on lui retourne uniquement ceux qu'il peut voir
	 * @param editingContext
	 * @param typePersonnel	(enseignants/non-enseignants/heberges/heberges a valider/vacataires/tous)
	 * @param typeTemps		(presents, anciens,futurs);
	 * @param nom sur lequel restreindre la recherche - peut etre nul
	 * @param prenom sur lequel restreindre la recherche - peut etre nul
	 * @param structure sstructures pour lesquels on restreint la recherche (peut etre nulle)
	 * @param estNomPatronymique true si la recherche est faite sur le nom patronymique
	 * @param exclureHeberges true si la recherche ne retourne pas les heberges
	 * @return personnels trouves (tableau de IndividuIdentite)
	 */
	public static NSArray rechercherPourAgentPersonnelsDeTypes(EOEditingContext editingContext,EOAgentPersonnel agent,int typePersonnel, int typeTemps,String nom,String prenom,NSArray structures,boolean estNomPatronymique,boolean exclureHeberges) {
		NSArray identites;
		if (agent.gereTousAgents() || typePersonnel != ManGUEConstantes.TOUT_PERSONNEL) {
			identites = rechercherPersonnelsDeTypes(editingContext,typePersonnel,typeTemps,nom,prenom,estNomPatronymique,exclureHeberges);

		} else {
			// On ne retient que les catégories
			NSMutableArray individus = new NSMutableArray();	// contiendra des EOIndividus
			if (agent.gereEnseignants()) {
				individus.addObjectsFromArray(rechercherPersonnelsDeTypes(editingContext,ManGUEConstantes.ENSEIGNANT,typeTemps,nom,prenom,estNomPatronymique));
			}
			if (agent.gereNonEnseignants()) {
				individus.addObjectsFromArray(rechercherPersonnelsDeTypes(editingContext,ManGUEConstantes.NON_ENSEIGNANT,typeTemps,nom,prenom,estNomPatronymique));
			}
			if (agent.gereVacataires()) {
				individus.addObjectsFromArray(rechercherPersonnelsDeTypes(editingContext,ManGUEConstantes.VACATAIRE,typeTemps,nom,prenom,estNomPatronymique));
			}
			if (agent.gereHeberges()) {
				individus.addObjectsFromArray(rechercherPersonnelsDeTypes(editingContext,ManGUEConstantes.HEBERGE,typeTemps,nom,prenom,estNomPatronymique));
			}
			if ((structures == null || structures.count() == 0) && typeTemps == ManGUEConstantes.TOUT_EMPLOYE) {

				int total = 0;
				// On veut tous les personnels, il faut donc ajouter les personnels sans carrière et contrat
				NSArray tousIndividus = rechercherPersonnels(editingContext, nom, prenom, estNomPatronymique, typePersonnel);
				NSArray individusAvecCarrieresContrats = rechercherPersonnelsDeTypes(editingContext,ManGUEConstantes.TOUT_PERSONNEL,typeTemps,nom,prenom,estNomPatronymique);
				for (java.util.Enumeration<EOIndividuIdentite> e = tousIndividus.objectEnumerator();e.hasMoreElements();) {
					EOIndividuIdentite individu = e.nextElement();
					if (individusAvecCarrieresContrats.containsObject(individu) == false && individus.containsObject(individu) == false) {
						// Individu pas encore retenu, sans carriere ni contrat
						individus.addObject(individu);
						total++;
					}
				}
				EOSortOrdering.sortedArrayUsingKeyOrderArray(individus, new NSArray(EOSortOrdering.sortOrderingWithKey(NOM_USUEL_KEY, EOSortOrdering.CompareAscending)));
			}
			identites = new NSArray(individus);
		}
		if (structures != null && structures.count() > 0) {
			return restreindrePersonnels(editingContext,identites,structures, typeTemps);
		} else {
			return identites;
		}
	}

	public static NSArray restreindrePersonnelsNonAffectesOuAvecStructure(EOEditingContext editingContext,String nom,String prenom,boolean estNomPatronymique,NSArray identites,NSArray structures) {
		// Plus performant de rechercher toutes les affectations d'un coup meme si à chaque fois
		// on a ensuite un fetch pour la personne
		// si nom et prenom sont nuls, on recherche toutes les affectations0
		// sinon on se limite à ce nom ou prenom
		EOQualifier qualifier = qualifierAffectation(nom,prenom,estNomPatronymique);
		System.out
				.println("EOIndividuIdentite.restreindrePersonnelsNonAffectesOuAvecStructure() " + qualifier);
		NSArray affectations = EOAffectation.rechercherAffectationsAvecCriteres(editingContext,qualifier);
		NSArray personnes = (NSArray)affectations.valueForKey(EOAffectation.PERSONNE_KEY);
		affectations = EOSortOrdering.sortedArrayUsingKeyOrderArray(affectations, new NSArray(EOSortOrdering.sortOrderingWithKey(EOAffectation.PERSONNE_KEY+"."+NOM_USUEL_KEY, EOSortOrdering.CompareAscending)));
		NSMutableArray retour = new NSMutableArray();
		for (java.util.Enumeration<EOIndividuIdentite> e = identites.objectEnumerator();e.hasMoreElements();) {
			EOIndividuIdentite personne = e.nextElement();
			if (retour.containsObject(personne) == false) {
				if (personnes.containsObject(personne) == false) {
					// pas d'affectation, on l'ajoute
					retour.addObject(personne);
				} else if (structures != null) {	
					// structures = null on recherche uniquement les personnels non affectes
					// Rechercher pour chaque agent ses affectations et si on trouve dans les affectations
					// une des structures de la liste passée en parametre
					for (java.util.Enumeration<EOAffectation> e1 = affectations.objectEnumerator();e1.hasMoreElements();) {
						EOAffectation affectation = e1.nextElement();
						if (affectation.personne() == personne) {
							if (structures.containsObject(affectation.toStructureUlr())) {
								retour.addObject(personne);
								break;
							}
						}
					}
				}		
			}
		}
		return retour;
	}
	/** Restreint une liste d'individus (IdentiteIndividu) aux non-affectes
	 * @return liste d'IdentiteIndividu */
	public static NSArray restreindreIndividusNonAffectes(EOEditingContext edc,String nom,String prenom,boolean estNomPatronymique,NSArray identitesIndividus) {
		// Plus performant de rechercher toutes les affectations d'un coup même si à chaque fois
		// on a ensuite un fetch pour la personne
		// si nom et prénom sont nuls, on recherche toutes les affectations
		// sinon on se limite à ce nom ou prénom
		LogManager.logDetail("\t--> Restriction individus non affectes");
		EOQualifier qualifier = qualifierAffectation(nom,prenom,estNomPatronymique);
		NSArray affectations = EOAffectation.rechercherAffectationsAvecCriteres(edc,qualifier);
		NSArray individusAffectes = (NSArray)affectations.valueForKey(EOAffectation.PERSONNE_KEY);
		NSMutableArray nonAffectes = new NSMutableArray();

		// pour chaque personne de la liste initiale, si elle n'est pas dans la liste des personnes affectées
		// et qu'on ne l'a pas encore gardée, alors l'ajouter au résultat
		for (java.util.Enumeration<EOIndividuIdentite> e = identitesIndividus.objectEnumerator();e.hasMoreElements();) {
			EOIndividuIdentite personne = e.nextElement();
			// pas d'affectation, on l'ajoute
			if (nonAffectes.containsObject(personne) == false && individusAffectes.containsObject(personne) == false)
				nonAffectes.addObject(personne);	
		}
		return new NSArray(nonAffectes);
	}
	/** Restreint une liste d'individus (IdentiteIndividu) aux non-vacataires
	 * @return liste d'IdentiteIndividu */
	public static NSArray restreindreNonVacataires(EOEditingContext editingContext,String nom,String prenom,boolean estNomPatronymique,NSArray identitesIndividus) {
		LogManager.logDetail("\t--> Restriction non vacataires");
		// Plus performant de rechercher tous les vacataires d'un coup même si à chaque fois
		// on a ensuite un fetch pour la personne
		NSArray vacataires = EOContrat.rechercherContratsPourPeriodeEtTypePersonnel(editingContext, nom, prenom, null, null, ManGUEConstantes.VACATAIRE, true, estNomPatronymique);
		NSArray personnes = (NSArray)vacataires.valueForKey("identiteIndividu");
		NSMutableArray individus = new NSMutableArray();
		// pour chaque personne de la liste initiale, si elle n'est pas dans la liste des vacataires
		// et qu'on ne l'a pas encore gardée, alors l'ajouter au résultat
		for (java.util.Enumeration<EOIndividuIdentite> e = identitesIndividus.objectEnumerator();e.hasMoreElements();) {
			EOIndividuIdentite personne = e.nextElement();
			// n'est pas vacataire, on l'ajoute
			if (individus.containsObject(personne) == false && personnes.containsObject(personne) == false)
				individus.addObject(personne);	
		}
		return new NSArray(individus);
	}
	//	méthodes privées
	private static NSArray restreindrePersonnels(EOEditingContext editingContext,NSArray personnels,NSArray structures, int categorie) {
				
		NSMutableArray args = new NSMutableArray();
		//	pour ramener à une date sans prendre en compte les heures
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		args.addObject(today);
		String qualifier = "";
		if ( categorie != ManGUEConstantes.EMPLOYE_ANCIEN) {
			args.addObject(today);
			qualifier = "dateDebut <= %@ AND (dateFin >=%@ OR dateFin = nil)";
		}
		else
			qualifier = "dateFin <=%@";
		
		if (structures.count() > 0) {
			NSMutableArray listeStructures = new NSMutableArray();
			String newQualifier = "";
			for (java.util.Enumeration<EOStructure> e = structures.objectEnumerator();e.hasMoreElements();) {
				EOStructure structure = e.nextElement();
				if (newQualifier.equals(""))
					newQualifier = "toStructureUlr = %@";
				else
					newQualifier = newQualifier + " OR toStructureUlr = %@";

				listeStructures.addObject(structure);
				for (java.util.Enumeration<EOStructure> e1 = structure.toutesStructuresFils().objectEnumerator();e1.hasMoreElements();) {
					newQualifier = newQualifier + " OR toStructureUlr = %@";
					listeStructures.addObject(e1.nextElement());
				}
			}
			args.addObjectsFromArray(listeStructures);
			qualifier = qualifier + " AND (" + newQualifier + ")";
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(qualifier,args);
		NSArray affectations = EOAffectation.rechercherAffectationsAvecCriteres(editingContext,qual);
		NSArray restrictions = (NSArray)affectations.valueForKey(EOAffectation.PERSONNE_KEY);
		NSMutableArray retour = new NSMutableArray();
		for (java.util.Enumeration<EOIndividuIdentite> e = personnels.objectEnumerator();e.hasMoreElements();) {
			EOIndividuIdentite personnel = e.nextElement();
			for (Enumeration e1 = restrictions.objectEnumerator();e1.hasMoreElements();) {
				// 22/09/09 - lorsqu'on vient de supprimer une affectation, on peut avoir NSKeyValueCoding$Null dans les restrictions
				Object restrict = e1.nextElement();
				if (restrict instanceof EOIndividuIdentite) {
					EOIndividuIdentite restrictionPersonnel = (EOIndividuIdentite)restrict;
					if (editingContext.globalIDForObject(personnel).equals(editingContext.globalIDForObject(restrictionPersonnel)) &&
							retour.containsObject(personnel) == false) {
						retour.addObject(personnel);
						break;
					}
				}
			}	
		}

		return (NSArray)retour;
	}
	//	méthodes privées
	//	ajoute ou supprime des agents d'un tableau et retourne le résultat. 
	private static void modifierIndividus(NSMutableArray individus,NSArray nouveauxIndividus,boolean ajouter) {
		if (nouveauxIndividus != null && nouveauxIndividus.count() > 0) {
			for (Enumeration<EOIndividuIdentite> e = nouveauxIndividus.objectEnumerator();e.hasMoreElements();) {
				try {
					EOIndividuIdentite individu = e.nextElement();
					if (ajouter && !individus.containsObject(individu)) 
						individus.addObject(individu);

					if (!ajouter && individus.containsObject(individu))
						individus.removeObject(individu);

				} catch (Exception exc) {
					exc.printStackTrace();
				}
			}
		}
	}

	private static void ajouterIndividus(NSMutableArray individus,NSArray nouveauxIndividus) {
		modifierIndividus(individus,nouveauxIndividus,true);
	}
	private static void supprimerIndividus(NSMutableArray individus,NSArray nouveauxIndividus) {
		modifierIndividus(individus,nouveauxIndividus,false);
	}

	
	// retourne le qualifier d'affectation en fonction des critères de recherche
	private static EOQualifier qualifierAffectation(String nom,String prenom,boolean estNomPatronymique) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		if (nom != null || prenom != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAffectation.PERSONNE_KEY+"."+EOIndividu.TEM_VALIDE_KEY + " caseInsensitiveLike %@", new NSArray(CocktailConstantes.VRAI)));
		}
		
		if (nom != null) {
			if (estNomPatronymique) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAffectation.PERSONNE_KEY+"."+NOM_PATRONYMIQUE_KEY  + " caseInsensitiveLike %@", new NSArray(nom + "*")));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAffectation.PERSONNE_KEY+"."+NOM_USUEL_KEY  + " caseInsensitiveLike %@", new NSArray(nom + "*")));
			} else {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAffectation.PERSONNE_KEY+"."+NOM_USUEL_KEY  + " caseInsensitiveLike %@", new NSArray(nom + "*")));
			}
		}
		if (prenom != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOAffectation.PERSONNE_KEY+"."+PRENOM_KEY  + " caseInsensitiveLike %@", new NSArray(prenom + "*")));

		LogManager.logDetail("");
		
		return new EOAndQualifier(qualifiers);
	}
}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.grhum.referentiel;

import java.math.BigInteger;

import org.cocktail.mangue.common.controles.rib.ControleIban;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOBanque;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EORib extends _EORib {

	public static final EOSortOrdering SORT_VALIDE_DESC = new EOSortOrdering(RIB_VALIDE_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_VALIDE_DESC = new NSArray(SORT_VALIDE_DESC);
	public static final String DEFAULT_MODE_PAIEMENT = "30"; 

	public static String ANNULATION = "A";

	public EORib() {
		super();
	}


	/**
	 * 
	 * @param ec
	 * @param fournis
	 * @param titulaire
	 * @return
	 */
	public static EORib creer(EOEditingContext ec, EOFournis fournis, String titulaire) {

		EORib newObject = (EORib)CocktailUtilities.instanceForEntity(ec, ENTITY_NAME);
		newObject.initRib(fournis, titulaire);
		ec.insertObject(newObject);
		return newObject;
	}

	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EORib> findForIndividu(EOEditingContext ec, EOIndividu individu) {
		try {

			NSMutableArray qualifiers = new NSMutableArray();
			NSMutableArray orQualifiers = new NSMutableArray();

			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(RIB_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_FOURNIS_KEY+"=%@", new NSArray(individu.fournis())));

			return fetchAll(ec,new EOAndQualifier(qualifiers), SORT_ARRAY_VALIDE_DESC);
		}
		catch (Exception e) {return null;}
	}


	public static EORib rechercherRibAvecNoCompteExceptIndividu(EOEditingContext ec, String compte, EOIndividu individu) {
		try {

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(RIB_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_COMPTE_KEY+"=%@", new NSArray(compte)));

			EOFournis fournis = individu.fournis();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_FOURNIS_KEY+".fouCode =%@", new NSArray(fournis.fouCode())));

			return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {return null;}
	}

	/**
	 *
	 */
	public static EORib rechercherRibAvecNoCompte(EOEditingContext ec, String compte) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(RIB_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_COMPTE_KEY+"=%@", new NSArray(compte)));

			return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {return null;}
	}

	/**
	 * 
	 * @param fournis
	 * @param titulaire
	 */
	public void initRib( EOFournis fournis, String titulaire) {

		setToFournisRelationship(fournis);		
		setModCode(DEFAULT_MODE_PAIEMENT);
		setRibTitco(titulaire);
		setRibValide("O");
		setTemPayeUtil("N");

	}

	/**
	 * Validation de l'objet Metier.
	 * Privilegiant le format SEPA, on regarde deja si le code IBAN est choisi.
	 */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		if ("N".equals(ribValide()))	return;		// Si on a annule le rib, on ne verifie rien.

		// Saisie BANQUE GUICHET COMPTE CLE

		if (iban() == null) {

			// L association de la banque reste obligatoire , en mode SEPA ou non.
			if (toBanque() == null && iban() != null && StringCtrl.containsIgnoreCase(iban(), "FR") )
				throw new com.webobjects.foundation.NSValidation.ValidationException("Veuillez sélectionner une banque !");

			if ( noCompte() == null || cleRib() == null)
				throw new com.webobjects.foundation.NSValidation.ValidationException("Tous les champs du RIB ne sont pas renseignés (IBAN ou Compte/Clé) !");

			setNoCompte(noCompte().toUpperCase());

			if (toBanque() != null) {
				setNoCompte(StringCtrl.stringCompletion(noCompte(), 11, "0", "G"));
				setCBanque(toBanque().cBanque());
				setCGuichet(toBanque().cGuichet());

				if ((noCompte().length() + cleRib().length() + cBanque().length() + cGuichet().length()) != 23)
					throw new com.webobjects.foundation.NSValidation.ValidationException("Les données du RIB sont incomplètes !");
				if (!donneesRibValides()) {
					throw new com.webobjects.foundation.NSValidation.ValidationException("Les données du RIB sont erronées !");
				}
			}
		}
		else {	// SAISIE IBAN - Code BIC obligatoire + code IBAN de 27 à 34 caractères.

			ControleIban.verifierFormatIban(iban());

			setIban(iban().toUpperCase());

			// L association de la banque reste obligatoire , en mode SEPA ou non.
			if (toBanque() == null) {

				EOBanque banque = EOBanque.rechercherBanque(editingContext(), iban().substring(4,9), iban().substring(9,14));

				if (banque == null)
					throw new com.webobjects.foundation.NSValidation.ValidationException("Veuillez sélectionner une banque !");
				else {
					setToBanqueRelationship(banque);
					setCBanque(banque.cBanque());
					setCGuichet(banque.cGuichet());
				}
			}
			else {
				// Les code banque, code guichet sont issus des donnees de la table BANQUE
				setCBanque(toBanque().cBanque());
				setCGuichet(toBanque().cGuichet());
				setBic(toBanque().bic());

				setNoCompte(iban().substring(14, 25));
				setCleRib(iban().substring(25, 27));

			}
		}
	}


	public boolean estValide() {
		return ribValide() != null && ribValide().equals(CocktailConstantes.VRAI);

	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setRibValide(CocktailConstantes.VRAI);
		} else {
			setRibValide(ANNULATION);
		}
	}
	public boolean estUtilisePourPaye() {
		return temPayeUtil() != null && temPayeUtil().equals("O");
	}
	public boolean estBanqueLocale() {
		return toBanque() != null && toBanque().temLocal().equals("O");
	}

	/** */
	public boolean donneesRibValides(){
		String chiffres = "";
		for (int i=0;i<noCompte().length();i++)
		{
			String car =noCompte().substring(i,i+1);
			if ( ("A".equals(car)) || ("J".equals(car) ) )
				chiffres = chiffres + "1";
			else
				if ( ("B".equals(car)) || ("K".equals(car) ) || ("S".equals(car) ) )
					chiffres = chiffres + "2";
				else
					if ( ("C".equals(car)) || ("L".equals(car) ) || ("T".equals(car) ) )
						chiffres = chiffres + "3";
					else
						if ( ("D".equals(car)) || ("M".equals(car) ) || ("U".equals(car) ) )
							chiffres = chiffres + "4";
						else
							if ( ("E".equals(car)) || ("N".equals(car) ) || ("V".equals(car) ) )
								chiffres = chiffres + "5";
							else
								if ( ("F".equals(car)) || ("O".equals(car) ) || ("W".equals(car) ) )
									chiffres = chiffres + "6";
								else
									if ( ("G".equals(car)) || ("P".equals(car) ) || ("X".equals(car) ) )
										chiffres = chiffres + "7";
									else
										if ( ("H".equals(car)) || ("Q".equals(car) ) || ("Y".equals(car) ) )
											chiffres = chiffres + "8";
										else
											if ( ("I".equals(car)) || ("R".equals(car) ) || ("Z".equals(car) ) )
												chiffres = chiffres + "9";
											else
												chiffres = chiffres + car;
		}

		String s = toBanque().cBanque()+cGuichet()+chiffres+cleRib();
		BigInteger bigInt = new BigInteger(s);

		if ((bigInt.mod(new BigInteger("97"))).intValue() != 0)
			return false;

		return true;
	}
	
	/** retourne true si le rib existe deja
	 * @param editingContext
	 * @param banque
	 * @param guichet
	 * @param noCompte
	 * @param cle
	 * @return boolean
	 */
	public static EORib ribExiste(EOEditingContext ec, String iban, String banque,String guichet,String noCompte,String cle) {

		try {

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(RIB_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_FOURNIS_KEY+"."+EOFournis.FOU_VALIDE_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));

			if (iban != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(IBAN_KEY+" caseInsensitiveLike %@", new NSArray(iban)));
			else {
				if (banque != null && guichet != null && noCompte != null && cle != null) {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_BANQUE_KEY+"=%@", new NSArray(banque)));
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GUICHET_KEY+"=%@", new NSArray(guichet)));
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NO_COMPTE_KEY+" caseInsensitiveLike %@", new NSArray(noCompte)));
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CLE_RIB_KEY+" = %@", new NSArray(cle)));
				}
			}

			return fetchFirstByQualifier(ec,new EOAndQualifier(qualifiers));
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}


	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

//EOIndividu.java
//Created on Wed Mar 19 08:28:41  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;


import java.math.BigInteger;

import org.cocktail.application.client.eof.EORibfourUlr;
import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.modele.nomenclatures.EODiplomes;
import org.cocktail.mangue.common.modele.nomenclatures.EOPays;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeTel;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOCivilite;
import org.cocktail.mangue.modele.grhum.EOGestCgmodCon;
import org.cocktail.mangue.modele.grhum.EOGestCgmodPop;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOAutorisationTravail;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EODepart;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOIndividuDiplomes;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;
import org.cocktail.mangue.modele.mangue.nbi.EONbi;
import org.cocktail.mangue.modele.mangue.nbi.EONbiOccupation;
import org.cocktail.mangue.server.cir.ConstantesCir;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Verifications effectuees sur l'individu : <BR>
 * Longueur des champs et presence obligatoire<BR>
 * Coherence des dates<BR>
 * Presence du departement si pays de naissance = France<BR>
 * Coherence du departement et du pays de naissance<BR>
 * Validite du pays de naissance et du pays de nationalite<BR>
 * Validite du numero Insee si verification parametre dans Grhum
 *  
 */

public class EOIndividu extends _EOIndividu implements RecordAvecLibelle {

	private static final String CODE_DEP_INSEE_CORSE = "20";
	public static final String IND_QUALITE_HEBERGE = "HEBERGE";
	public static int ANNEE_BASE_POUR_NAISSANCE = 1900;
	public static int LONGUEUR_NO_INSEE = 13;
	public static final String NO_INSEE_PROVISOIRE = "999999999999";
	public static final String INDIVIDU_A_VALIDER = "N";
	private NSTimestamp datePourImpression; // Utilisée pour rechercher l'affectation et la situation d'un individu à une certaine période
	private final static String PRISE_EN_COMPTE_NO_INSEE_PROVISOIRE = "P";
	private final static String PRISE_EN_COMPTE_NO_INSEE = "R";
	//private EOGenericRecord civilite; // Pour le Cir

	public static final String IDENTITE_KEY = "identite";
	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String C_DEPT_NAISSANCE_KEY = "cDeptNaissance";
	public static final String C_PAYS_NAISSANCE_KEY = "cPaysNaissance";
	public static final String C_PAYS_NATIONALITE_KEY = "cPaysNationalite";
	public static final String C_SITUATION_FAMILLE_KEY = "cSituationFamille";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String IND_ACTIVITE_KEY = "indActivite";
	public static final String IND_CLE_INSEE_KEY = "indCleInsee";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String IND_QUALITE_KEY = "indQualite";
	public static final String LIEU_NAISSANCE_KEY = "lieuNaissance";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PERS_ID_KEY = "persId";
	public static final String PRENOM_KEY = "prenom";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String PERSONNEL_KEY = "personnel";
	public static final String DATE_COMPLETUDE_KEY = "dateCompletude";
	public static final String DATE_CERTIFICATION_KEY = "dateCertification";

	public static EOSortOrdering SORT_NOM_ASC = new EOSortOrdering(NOM_USUEL_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_NOM_ASC = new NSArray(SORT_NOM_ASC);

	public EOIndividu() {
		super();
	}


	public void setIndNoInsee(String value) {
		if (value != null) {
			// supprimer les espaces si il y en a
			value = (NSArray.componentsSeparatedByString(value," ")).componentsJoinedByString("");
		}
		takeStoredValueForKey(value, IND_NO_INSEE_KEY);
	}

	public void setIndNoInseeProv(String value) {
		if (value != null) {
			// supprimer les espaces si il y en a
			value = (NSArray.componentsSeparatedByString(value," ")).componentsJoinedByString("");
		}
		takeStoredValueForKey(value, IND_NO_INSEE_PROV_KEY);
	}


	public EOPersonnel personnel() {
		try {
			return (EOPersonnel)personnels().get(0);	
		} catch (Exception e) {
			return null;
		}
	}

	public NSTimestamp dateCertification() {
		try {
			return personnel().cirDCertification();	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public NSTimestamp dateCompletude() {
		try {
			return personnel().cirDCompletude();	
		} catch (Exception e) {
			return null;
		}
	}


	//*******  METHODES AJOUTEES *************/

	public String nomPatronymiqueCir() {

		return nomPatronymique();

	}

	public String nomUsuelCir() {
		if (estHomme())
			return "";
		return nomUsuel();
	}

	public void init() {
		setIndPhoto(CocktailConstantes.FAUX);
		setTemValide(CocktailConstantes.VRAI);
		setPrendreEnCompteNumeroProvisoire(false);
	}

	public boolean estValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean aBool) {
		if (aBool) {
			setTemValide(CocktailConstantes.VRAI);
		} else {
			setTemValide(CocktailConstantes.FAUX);
		}
	}
	public  NSTimestamp datePourImpression() {
		return datePourImpression;
	}
	public void setDatePourImpression(NSTimestamp datePourImpression) {
		this.datePourImpression = datePourImpression;
	}
	/** Tests de coherence pour l'enregistrement d'un individu */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {
		if (nomUsuel() == null || nomUsuel().length() > 80)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le nom est obligatoire et ne doit pas comporter plus de 80 caractères !");
		setNomUsuel(nomUsuel().toUpperCase());
		if (nomPatronymique()  != null && nomPatronymique ().length() > 80) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le nom de jeune fille ne doit pas comporter plus de 80 caractères !");
		}
		if (nomPatronymique() != null) {
			setNomPatronymique(nomPatronymique().toUpperCase());
		}
		if (prenom() == null || prenom().length() > 30)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le prénom est obligatoire et ne doit pas comporter plus de 30 caractères !");
		setPrenom(prenom().toUpperCase());
		if (prenom2() != null) {
			if (prenom2().length() > 40) {
				throw new com.webobjects.foundation.NSValidation.ValidationException("Le deuxième prénom comporte au plus 40 caractères !");
			}
			setPrenom2(prenom2().toUpperCase());
			if (prenom2().equals(prenom())) {
				throw new com.webobjects.foundation.NSValidation.ValidationException("Le deuxième prénom ("+prenom2()+") doit être différent du prénom principal (" + prenom() + ")");
			}
		}
		if (cCivilite() == null)
			throw new com.webobjects.foundation.NSValidation.ValidationException("La civilité est obligatoire !");

		if ( dNaissance() == null && !estHeberge() && !estVacataire()) {
			throw new NSValidation.ValidationException("La date de naissance est obligatoire !");
		}

		if (lieuDeces() != null && lieuDeces().length() > 100)
			throw new com.webobjects.foundation.NSValidation.ValidationException("Le lieu de décès comporte au plus 100 caractères !");

		//	vérifier la cohérence des dates par rapport à la date du jour
		NSTimestamp today = new NSTimestamp();
		if (dNaissance() != null && DateCtrl.isAfter(dNaissance(),today)) {
			throw new NSValidation.ValidationException("La date de naissance ne peut être postérieure à aujourd'hui");
		}

		// vérifier que la date de décès n'est pas antérieure à la date de naissance
		if (dDeces() != null && DateCtrl.isBefore(dDeces(),dNaissance())) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("La date de décès ne peut être antérieure à la date de naissance");
		}

		if (toPaysNaissance() != null && !estHeberge() && !estVacataire()) {
			if (toPaysNaissance().code().equals(EOPays.CODE_PAYS_FRANCE) && toDepartement() == null && indNoInseeProv() == null) {
				throw new NSValidation.ValidationException("Pour une personne née en France, vous devez fournir le département");
			}
			NSTimestamp date = today;
			if (dNaissance() != null) {
				date = dNaissance();
			}
			if (EOPays.estPaysValide(editingContext(), toPaysNaissance().code()) == false) { 
				throw new NSValidation.ValidationException("Le pays de naissance n'est pas valide !");
			}
		}
		if (toPaysNationalite() != null && EOPays.estPaysValide(editingContext(), toPaysNationalite().code()) == false) {
			throw new NSValidation.ValidationException("Le pays de nationalité n'est pas valide");
		}
		if (toDepartement() != null && toDepartement().code().equals("000") == false && (toPaysNaissance() == null || toPaysNaissance().code().equals(EOPays.CODE_PAYS_FRANCE) == false)) {
			throw new NSValidation.ValidationException("Le pays de naissance est incohérent avec le département de naissance. Soit la personne est née en France, soit vous ne devez pas fournir de département");
		}

		String doitValiderNoSS = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_CONTROLE_INSEE);
		if ( !estHeberge() && !estVacataire() && doitValiderNoSS.substring(0,1).equals(CocktailConstantes.VRAI) || doitValiderNoSS.substring(0,1).equals("Y")) {
			verifierNoSS();
		} else {
			// Vérifier juste les longueurs des n° SS
			if (indNoInseeProv() != null) {
				if (indNoInseeProv().length() > LONGUEUR_NO_INSEE) {
					throw new NSValidation.ValidationException("Le numéro SS comporte au plus treize chiffres");
				}
			}
			if (indNoInsee() != null) {
				if (indNoInsee().length() > LONGUEUR_NO_INSEE) {
					throw new NSValidation.ValidationException("Le numéro SS comporte au plus treize chiffres");
				}
			}
		}
		if (villeDeNaissance() != null) {
			if (villeDeNaissance().length() > 60) {
				throw new com.webobjects.foundation.NSValidation.ValidationException("La ville de naissance doit pas comporter plus de 60 caractères !");
			} else {
				setVilleDeNaissance(villeDeNaissance().toUpperCase());
			}
		}

		if (dCreation() == null) 
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}

	public String dateNaissanceFormatee() {
		return SuperFinder.dateFormatee(this,D_NAISSANCE_KEY);
	}
	public void setDateNaissanceFormatee(String uneDate) {
		if (uneDate == null) {
			setDNaissance(null);
		} else {
			SuperFinder.setDateFormatee(this,D_NAISSANCE_KEY,uneDate);
		}
	}
	public boolean estHomme() {
		return cCivilite() != null && cCivilite().equals(ManGUEConstantes.MONSIEUR);
	}

	public boolean estHeberge() {
		return indQualite() != null && indQualite().equals(IND_QUALITE_HEBERGE);
	}
	public boolean estVacataire() {
		return indActivite() != null && indActivite().equals(ManGUEConstantes.IND_ACTIVITE_VACATION);
	}

	public boolean estTitulaire() {
		return personnel() != null && personnel().temTitulaire() != null && personnel().temTitulaire().equals(CocktailConstantes.VRAI);
	}
	public boolean prendreEnCompteNumeroProvisoire() {
		return (priseCptInsee() != null && priseCptInsee().equals(PRISE_EN_COMPTE_NO_INSEE_PROVISOIRE));
	}
	public void setPrendreEnCompteNumeroProvisoire(boolean aBool) {
		if (aBool) {
			setPriseCptInsee(PRISE_EN_COMPTE_NO_INSEE_PROVISOIRE);
		} else {
			setPriseCptInsee(PRISE_EN_COMPTE_NO_INSEE);
		}
	}
	public String identite() {
		return nomUsuel() + " " + prenom();
	}
	public String identitePrenomFirst() {
		return prenom() + " " + nomUsuel();
	}
	public String dateDecesFormatee() {
		return SuperFinder.dateFormatee(this,D_DECES_KEY);
	}

	public void setDateDecesFormatee(String uneDate) {
		if (uneDate == null) {
			setDDeces(null);
		} else {
			SuperFinder.setDateFormatee(this,D_DECES_KEY,uneDate);
		}	
	}

	public void peutAvoirConjoint() throws ValidationException {

		if (toSituationFamiliale() == null)
			throw new ValidationException("Pour la saisie d'un conjoint, l'agent sélectionné doit avoir une situation familiale !");

		if (toSituationFamiliale().estCelibataire())
			throw new ValidationException("L'agent sélectionné est célibataire ... Pas de saisie de conjoint possible !");

	}


	public boolean peutAfficherPhoto() {
		return indPhoto() != null && indPhoto().equals(CocktailConstantes.VRAI);
	}
	public void setPeutAfficherPhoto(boolean aBool) {
		if (aBool) {
			setIndPhoto(CocktailConstantes.VRAI);
		} else {
			setIndPhoto(CocktailConstantes.FAUX);
		}
	}

	public String ministere() {
		if (personnel() != null && personnel().toMinistere() != null)
			return personnel().toMinistere().code();

		return null;
	}
	public String civilite() {
		EOGenericRecord civilite = SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(),"Civilite","cCivilite",cCivilite());
		return (String)civilite.valueForKey("lCivilite");
	}

	/** retourne l'image associee a cet individu 
	 * EOImageView attend un NSData */
	public NSData image() {		
		try {			
			return (NSData) (EOPhotosEmployes.findForIndividu(editingContext(), this)).datasPhoto();
		}
		catch (Exception e) {
			return null;
		}
	}

	public Integer age() {
		return DateCtrl.calculerDureeEnAnnees(dNaissance(), new NSTimestamp(), true);
	}

	/** retourne la liste des enfants d'un individu */
	public NSArray<EOEnfant> enfants() {
		NSMutableArray args = new NSMutableArray(this);
		String stringQualifier = EORepartEnfant.PARENT_KEY + " = %@";
		stringQualifier = stringQualifier + " AND enfant.temValide = 'O'";

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		EOFetchSpecification fs = new EOFetchSpecification(EORepartEnfant.ENTITY_NAME, qualifier, null);
		// 18/06/2010 - suppression de refresh dans les fetchs qui créaient des plantages
		//fs.setRefreshesRefetchedObjects(true);
		return (NSArray)editingContext().objectsWithFetchSpecification(fs).valueForKey(EORepartEnfant.ENFANT_KEY);
	}


	/**
	 * Liste des diplomes de l individu
	 * @return
	 */
	public NSArray<EODiplomes> listeDiplomes() {
		return (NSArray<EODiplomes>)(EOIndividuDiplomes.fetchAll(editingContext(),  CocktailFinder.getQualifierEqual(EOIndividuDiplomes.INDIVIDU_KEY, this))).valueForKey(EOIndividuDiplomes.DIPLOME_KEY);		
	}

	/**
	 * Adresse de facturation valide
	 * @return
	 */
	public EORepartPersonneAdresse adresseFacturationValide()	{
		NSArray<EORepartPersonneAdresse> reparts = EORepartPersonneAdresse.adressesFacturationsValides(editingContext(),this);
		if (reparts.size() > 0) {
			return reparts.get(0);
		} else {
			return null;
		}
	}
	/** retoure l'adresse principale d'un individu
	 * @return adresse ou null si non trouvee
	 */
	public EORepartPersonneAdresse repartAdressePrincipale() {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject( CocktailFinder.getQualifierEqual(EORepartPersonneAdresse.PERS_ID_KEY, persId()));
			qualifiers.addObject( CocktailFinder.getQualifierEqual(EORepartPersonneAdresse.RPA_PRINCIPAL_KEY, CocktailConstantes.VRAI));
			qualifiers.addObject( CocktailFinder.getQualifierEqual(EORepartPersonneAdresse.RPA_VALIDE_KEY, CocktailConstantes.VRAI));

			return EORepartPersonneAdresse.fetchFirstByQualifier(editingContext(), new EOAndQualifier(qualifiers));
		} catch (Exception e) {
			return null;
		}
	}

	/** retourne le fournisseur valide associe a un individu */
	public EOFournis fournis() {
		return fournisseur("O");
	}
	/** retourne le fournisseur  associe a un individu (valide ou non) */
	public EOFournis fournisValideOuNon() {
		return fournisseur(null);
	}
	/** renvoie true si l'individu est un fournisseur */
	public boolean isFournis() {
		return fournis() != null;
	}
	public boolean neEnFrance() {
		String departement = departementNaissance();
		return departement != null && departement.equals("99") == false && departement.equals("96") == false;
	}
	// Renvoie la cle insee en fonction du numero
	public Number getCleInsee(String numero) {
		try {
			String nouveauNumero = numero.toUpperCase();
			String nombreASoustraire = null;	// Pour la Corse, si c'est 2A ou 2B, il faut retirer des valeurs
			if (nouveauNumero.indexOf("A") > 0) {
				nombreASoustraire = "100";
			} else if (nouveauNumero.indexOf("B") > 0) {
				nombreASoustraire = "200";
			}
			nouveauNumero = nouveauNumero.replaceAll("B","0");
			nouveauNumero = nouveauNumero.replaceAll("A","0");
			BigInteger bigInt1 = new BigInteger(nouveauNumero.substring(0,9));
			if (nombreASoustraire != null) {
				bigInt1 = bigInt1.subtract(new BigInteger(nombreASoustraire));
			}
			String s = "" + ((bigInt1.mod(new BigInteger("97"))).intValue()) + (nouveauNumero.substring(9,13));
			BigInteger bigInt2 = new BigInteger(s);

			return new Integer(97 - ( (bigInt2.mod(new BigInteger("97"))).intValue()));
		}
		catch (Exception e) {
			return new Integer(00);
		}
	}

	/** Retourne les ribs d'un individu tries sur la validite  */
	public NSArray ribs() {
		EOGenericRecord fournis = this.fournis();
		if (fournis != null) {
			// id fournisseur
			NSMutableArray values = new NSMutableArray(fournis);
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EORib.TO_FOURNIS_KEY + "= %@",values);
			EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(EORib.RIB_VALIDE_KEY, EOSortOrdering.CompareDescending);
			EOFetchSpecification fs = new EOFetchSpecification(EORib.ENTITY_NAME,qualifier, new NSArray(sort));
			fs.setRefreshesRefetchedObjects(true);
			return editingContext().objectsWithFetchSpecification(fs);
		}
		return null;
	}
	/** Retourne les ribs valides d'un individu  */
	public NSArray ribsValides() {
		EOGenericRecord fournis = this.fournis();
		if (fournis != null) {
			// id fournisseur
			NSMutableArray values = new NSMutableArray(fournis);
			values.addObject(CocktailConstantes.VRAI);
			// rib valide
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EORib.TO_FOURNIS_KEY + "=%@ AND ribValide = %@",values);
			EOFetchSpecification fs = new EOFetchSpecification(EORib.ENTITY_NAME,qualifier, null);
			fs.setRefreshesRefetchedObjects(true);
			return editingContext().objectsWithFetchSpecification(fs);
		}
		return null;
	}
	/** retourne true si l'individu fait partie du service passe en parametre */
	public boolean faitPartieService(EOStructure structure) {
		return EORepartStructure.rechercherRepartStructuresPourIndividuEtStructure(editingContext(),this,structure).count() > 0;
	}
	/** retourne true si un individu a d'autres occupations NBI que celle passee en en parametre */
	public boolean aAutreOccupationNbiEnCours(EONbi nbi,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		return EONbiOccupation.rechercherAutresOccupationsNbiEnCoursPourIndividu(editingContext(), nbi, debutPeriode,finPeriode,this).count() > 0;
	}
	// Méthodes pour impression
	public String prenomPourEdition() {
		if (prenom() == null) {
			return null;
		} else {
			return StringCtrl.capitalizedString(prenom());
		}
	}
	public String situationProfessionnelle() {
		// pour ramener à une date sans prendre en compte les heures
		NSTimestamp date = null;
		if (datePourImpression() != null) {
			date = DateCtrl.stringToDate(DateCtrl.dateToString(datePourImpression()));
		} else {
			date = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		}
		NSArray<EOElementCarriere> elements = recupereElementsCarriere(date, date);
		if (elements.size() > 0) {
			String libelle = "";
			EOElementCarriere element = elements.get(0);
			if (element.toGrade() != null) {
				libelle = element.toGrade().llGrade();
			}
			if (element.cEchelon() != null) {
				libelle = libelle + " Echelon : " + element.cEchelon();
			}
			if (element.cChevron() != null) {
				libelle = libelle + " Chevron : " + element.cChevron();
			}
			return libelle;
		} else {
			NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),this,date,date,true);
			if (contrats.size() > 0) {
				EOContrat contrat = contrats.get(0);
				String libelle = "";
				if (estHomme()) {
					libelle = "Contractuel (";
				} else {
					libelle = "Contractuelle (";
				}
				libelle = libelle + contrat.toTypeContratTravail().libelleLong() + ") :";
				EOContratAvenant avenant = contrat.avenantCourant();
				if (avenant != null) {
					if (avenant.toGrade() != null) {
						libelle = libelle + " Equivalence grade " + avenant.toGrade().llGrade();
					}
					if (avenant.cEchelon() != null) {
						libelle = libelle + " Echelon " + avenant.cEchelon();
					}
					if (avenant.toCategorie() != null) {
						libelle = libelle.concat( ", Catég. " + avenant.toCategorie().code());
					}
				} 

				return libelle;

			} else {
				return "Aucun contrat ni carrière en ce moment...";
			}
		} 
	}

	/**
	 * 
	 */
	public String corpsActuel() {
		// pour ramener à une date sans prendre en compte les heures
		NSTimestamp today = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		NSArray<EOElementCarriere> elements = recupereElementsCarriere(today, today);
		if (elements.size() > 0) {
			EOElementCarriere element = elements.get(0);
			if (element.toCorps() != null) {
				return element.toCorps().llCorps();
			}
			return "";
		} else {
			// Que faut-il retourner pour un contractuel ??
			return "";
		}
	}

	/**
	 * 
	 * @return
	 */
	public String affectationActuelle() {
		EOStructure structureAffectation = structureAffectation();
		if (structureAffectation != null) {
			return structureAffectation.llStructure();
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String etablissementActuel() {
		EOStructure structureAffectation = structureAffectation();
		if (structureAffectation != null) {
			return structureAffectation.etablissement().llStructure();
		} else {
			return null;
		}	
	}

	/**
	 * 
	 * @return
	 */
	public String composanteActuelle() {
		EOStructure structureAffectation = structureAffectation();
		if (structureAffectation != null) {
			return structureAffectation.toComposante().llStructure();
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String signaturePourEtablissement() {
		EOStructure structureAffectation = structureAffectation();
		if (structureAffectation != null) {
			EOStructure etablissement = structureAffectation.etablissement();
			if (etablissement != null) {
				if (etablissement.cTypeStructure().equals(ManGUEConstantes.C_TYPE_STRUCTURE_ETABLISSEMENT)) {
					return EOGrhumParametres.getValueParam(ManGUEConstantes.PARAM_KEY_ORDO_UNIV);
				} else if (etablissement.cTypeStructure().equals(ManGUEConstantes.C_TYPE_STRUCTURE_ETAB_SECONDAIRE)) {
					return EOGrhumParametres.getValueParam(ManGUEConstantes.PARAM_KEY_ORDO_IUT);
				} else {
					return null;
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	/** Recherche le type de gestion d'arrete a la date de la periode passee en parametre 
	 * en determinant le segment de carriere ou le contrat associe
	 * @param nomTableCongeContractuel : correspond au nom de la table de conge contractuel tel qu'il est defini dans Harpege
	 * @param nomTableCongeFonctionnaire : correspond au nom de la table de conge fonctionnaire tel qu'il est defini dans Harpege
	 * @return typeGestion trouve ou null
	 */
	public EOTypeGestionArrete rechercherTypeGestionArretePourPeriode(String nomTableCongeContractuel,String nomTableCongeFonctionnaire,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		if (debutPeriode == null) {
			debutPeriode = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		}
		if (finPeriode == null) {
			finPeriode = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		}
		// rechercher d'abord si il y a des segments de carrière
		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),this,debutPeriode,finPeriode);
		if (carrieres != null && carrieres.size() > 0) {
			if (nomTableCongeFonctionnaire == null) {
				return null;
			}
			// normalement il n'y en a qu'un car pas de chevauchement de carrière
			EOCarriere carriere = carrieres.get(0);
			NSMutableArray args = new NSMutableArray(carriere.toTypePopulation());
			args.addObject(nomTableCongeFonctionnaire);
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("typePopulation = %@ AND nomTableCgmod = %@",args);
			EOFetchSpecification fs = new EOFetchSpecification(EOGestCgmodPop.ENTITY_NAME,qualifier,null);
			fs.setPrefetchingRelationshipKeyPaths(new NSArray("typeGestionArrete"));
			NSArray results = editingContext().objectsWithFetchSpecification(fs);

			try {
				return (EOTypeGestionArrete)((EOGenericRecord)results.get(0)).valueForKey("typeGestionArrete");
			} catch(Exception e) {
				return null;
			}
		} else {
			NSArray contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),this,debutPeriode,finPeriode,false);
			if (contrats != null && contrats.count() > 0) {
				if (nomTableCongeContractuel == null) {
					return null;
				}
				// il peut y avoir plusieurs contrats actifs en même temps
				for (java.util.Enumeration<EOContrat> e = contrats.objectEnumerator();e.hasMoreElements();) {
					EOContrat contrat = e.nextElement();
					NSMutableArray args = new NSMutableArray(contrat.toTypeContratTravail());
					args.addObject(nomTableCongeContractuel);
					EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("typeContratTrav = %@ AND nomTableCgmod = %@",args);
					EOFetchSpecification fs = new EOFetchSpecification(EOGestCgmodCon.ENTITY_NAME,qualifier,null);
					fs.setPrefetchingRelationshipKeyPaths(new NSArray("typeGestionArrete"));
					NSArray results = editingContext().objectsWithFetchSpecification(fs);
					try {
						return (EOTypeGestionArrete)((EOGenericRecord)results.get(0)).valueForKey("typeGestionArrete");
					} catch(Exception exc) {
					}
				}
				return null;		// on n'a rien trouvé
			} else {
				return null;
			}
		}	
	}
	/** return true si l'individu est titulaire sur toute la periode */
	public boolean estTitulaireSurPeriodeComplete(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),this,debutPeriode,finPeriode);
		carrieres =  EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		// vérifier qu'il y a bien continuité sur toute la période
		NSTimestamp dateDebut = null,dateFin = null;
		if (finPeriode != null && DateCtrl.isSameDay(debutPeriode,finPeriode)) {
			return carrieres.count() > 0;
		} else {
			// vérifier si pour ces éléments les dates sont contigües
			for (java.util.Enumeration<EOCarriere> e = carrieres.objectEnumerator();e.hasMoreElements();) {
				EOCarriere carriere = (EOCarriere)e.nextElement();
				if (dateDebut == null) {
					if (DateCtrl.isBeforeEq(carriere.dateDebut(),debutPeriode)) {
						dateDebut = debutPeriode;
						dateFin = carriere.dateFin();
						if (dateFin == null || (finPeriode != null && DateCtrl.isAfterEq(dateFin,finPeriode))) {
							return true;		// on a trouvé un segment qui couvre la période
						}
					} else {
						return false;		// le segment commence après, il ne couvre même pas la période de début
					}
				} else {
					// nouveau segment, vérifier qu'il commence le jour suivant la date de fin (continuité des segments)
					if (DateCtrl.isSameDay(DateCtrl.jourSuivant(dateFin), carriere.dateDebut())) {	// date fin est forcément non nul, sinon on sort avant
						dateFin = carriere.dateFin();	// on prend comme fin la date de fin du nouveau segment
						if (dateFin == null || (finPeriode != null && DateCtrl.isAfterEq(dateFin,finPeriode))) {
							return true;		// on a trouvé une suite de segments qui couvrent la période
						}
					} else {
						return false;	// pas de continuité des segments
					}

				}
			}
		}
		// on a bien trouvé des segments continus mais qui se terminent avant la fin
		if (dateFin != null && (finPeriode == null || DateCtrl.isBefore(dateFin, finPeriode))) {
			return false;
		}
		// normalement tous les autres cas ont été traités avant
		return true;
	}
	public boolean estNormalienSurPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(), this, debutPeriode, finPeriode);
		carrieres =  EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		for (EOCarriere carriere : carrieres) {
			if (carriere.toTypePopulation().estNormalien()) {
				return true;
			}
		}
		return false;
	}
	/** return true si l'individu peut bene des conges de contractuels sur la periode */
	public boolean beneficieCongeContractuelSurPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		if (debutPeriode == null) {
			NSArray<EOContrat> contrats = EOContrat.rechercherContratsSupportantCongePourIndividu(editingContext(), this);
			return contrats.size() > 0;
		} else {
			NSArray<EOContrat> contrats = EOContrat.rechercherTousContratsPourIndividuEtPeriode(editingContext(),this,debutPeriode,finPeriode);
			for (EOContrat contrat : contrats) {
				if (contrat.toTypeContratTravail().peutBeneficierConges()) {
					return true;
				}
			}
			return false;
		}
	}

	/** return true si le no insee est provisoire */
	public boolean estNoInseeProvisoire() {
		return prendreEnCompteNumeroProvisoire() || (indNoInsee() != null && indNoInsee().substring(1).equals(NO_INSEE_PROVISOIRE));
	}
	public void setEstNoInseeProvisoire(boolean yn) {
		if (yn)
			setPriseCptInsee(PRISE_EN_COMPTE_NO_INSEE_PROVISOIRE);
		else
			setPriseCptInsee(PRISE_EN_COMPTE_NO_INSEE);	
	}


	/**
	 * Test de l'unicite du numéro INSEE
	 * @return
	 */
	public boolean aNumeroInseeUnique() {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY  +"=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(IND_NO_INSEE_KEY  +"=%@", new NSArray(indNoInsee())));
		NSArray<EOIndividu> result = fetchAll(editingContext(), new EOAndQualifier(qualifiers));
		if (result.count() > 1) {
			return false; 
		} else if (result.count() == 1) {
			return result.get(0) == this;
		}

		return true;

	}

	/** return true si l'individu a dans ses diplomes un DEA */
	public boolean aDea() {
		for (EODiplomes diplome : listeDiplomes()) {
			if (diplome.libelleCourt().equals(EODiplomes.LIBELLE_DEA)) {
				return true;
			}
		}
		return false;
	}

	/** return true si l'individu a des contrats avec des avenants ou des vacations continues sur toute la periode */
	public boolean estContractuelSurPeriodeComplete(NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		// rechercher tous les contrats sur cette période
		NSArray<EOContrat> contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),this,debutPeriode,finPeriode,true);
		contrats =  EOSortOrdering.sortedArrayUsingKeyOrderArray(contrats, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		NSTimestamp dateDebut = null,dateFin = null;
		if (DateCtrl.isSameDay(debutPeriode,finPeriode)) {
			if (contrats.count() > 0) {
				return true;
			}
		} else {
			// vérifier si pour ces contrats, les dates d'avenants ou de validation sont contigües
			for (EOContrat contrat : contrats) {

				NSArray<EOContratAvenant> avenantsValides = EOContratAvenant.findForContrat(editingContext(), contrat);					
				boolean aAvenantsValides = avenantsValides.size() > 0;

				if (contrat.toTypeContratTravail().estVacataire() || !aAvenantsValides) {
					if (dateDebut == null) {
						dateDebut = contrat.dateDebut();
					}
					if (dateFin == null) {
						dateFin = contrat.dateFin();
						if (contrat.dateFinAnticipee() != null) {
							dateFin = contrat.dateFinAnticipee();
						}
					} else if  (DateCtrl.isSameDay(DateCtrl.jourSuivant(dateFin),contrat.dateDebut()) == false) {
						// non continuité des contrats de vacation
						return false;
					} else {
						dateFin = contrat.dateFin();
						if (contrat.dateFinAnticipee() != null) {
							dateFin = contrat.dateFinAnticipee();
						}
					}
				} else {
					NSArray<EOContratAvenant> avenants = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenantsValides, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
					for (EOContratAvenant avenant : avenants) {
						if (dateDebut == null) {
							if (DateCtrl.isBeforeEq(avenant.dateDebut(),debutPeriode)) {
								dateDebut = avenant.dateDebut();
								dateFin = avenant.dateFin();
								if (avenant.contrat().dateFinAnticipee() != null) {
									dateFin = avenant.contrat().dateFinAnticipee();
								}
								if (dateFin == null || DateCtrl.isAfterEq(dateFin,finPeriode)) {
									return true;		// on a trouvé un avenant qui couvre la période (cdi ou cdd après période)
								}
							} else {
								break;		// l'avenant commence après, il ne couvre même pas la période de début
							}
						} else {
							// nouvel avenant, vérifier qu'il commence le jour suivant la date (continuité des avenants)
							if (DateCtrl.isSameDay(DateCtrl.jourSuivant(dateFin),avenant.dateDebut())) {	// date fin est forcément non nul, sinon on sort avant
								dateFin = avenant.dateFin();	// on prend comme fin la date de fin du nouvel avenant	
								if (avenant.contrat().dateFinAnticipee() != null) {
									dateFin = avenant.contrat().dateFinAnticipee();
								}
								if (dateFin == null || DateCtrl.isAfterEq(dateFin,finPeriode)) {
									return true;		// on a trouvé un avenant qui couvre la période (cdi ou cdd après période)
								}
							} else {
								break;	// pas de continuité des contrats
							}
						}
					}
				}

			}
			if (estInclusDansPeriode(debutPeriode, finPeriode, dateDebut, dateFin)) {
				return true;
			}
		}

		return dateDebut != null && DateCtrl.isBeforeEq(dateDebut,debutPeriode) && (dateFin == null || DateCtrl.isAfterEq(dateFin,finPeriode));	// dans tous les autres cas on est sorti avant		
	}

	/**
	 * 
	 * @param debutPeriode
	 * @param finPeriode
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	private boolean estInclusDansPeriode(NSTimestamp debutPeriode,
			NSTimestamp finPeriode, NSTimestamp dateDebut, NSTimestamp dateFin) {
		return dateDebut != null && dateFin != null 
				&& DateCtrl.isBeforeEq(dateDebut,debutPeriode) && DateCtrl.isAfterEq(dateFin,finPeriode);
	}


	/** retourne true si l'individu peut beneficier d'un conge formation pendant la periode */
	public boolean peutBeneficierCongeFormation(NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSArray<EOPasse> passesEAS = EOPasse.rechercherPassesEASPourPeriode(editingContext(), this, debutPeriode, finPeriode);
		if (passesEAS.size() > 0) {
			return true;
		}

		NSArray<EOElementCarriere> elements = recupereElementsCarriere(debutPeriode, finPeriode);
		if (elements == null || elements.count() == 0) {
			return false;
		} else {		
			for (EOElementCarriere element : elements) {
				if (element.toCorps().beneficieCfp() == false) {
					return false;
				}
			}
		}
		return true;
	}


	protected NSArray recupereElementsCarriere(NSTimestamp debutPeriode,
			NSTimestamp finPeriode) {
		return EOElementCarriere.findForPeriode(editingContext(),this,debutPeriode,finPeriode);
	}


	/** retourne true si un individu a un contrat d'hospitalo-universitaire sur une periode
	 * @param debutPeriode
	 * @param finPeriode
	 */
	public boolean aContratHospitalierSurPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray contrats = null;
		if (debutPeriode == null) {
			contrats = EOContrat.rechercherContratsPourIndividu(editingContext(), this, false);
		} else {
			contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),this,debutPeriode,finPeriode,false);
		}
		if (contrats.count() == 0) {
			return false;
		} 
		contrats =  EOSortOrdering.sortedArrayUsingKeyOrderArray(contrats, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		java.util.Enumeration<EOContrat> e = contrats.objectEnumerator();
		while (e.hasMoreElements()) {
			EOContrat contrat = e.nextElement();
			if (contrat.toTypeContratTravail().estHospitalier()) {
				return true;
			}
		}
		return false;
	}
	
	/** retourne true si un individu beneficie des conges contractuels hospitalo-universitaire sur une periode
	 * @param debutPeriode
	 * @param finPeriode
	 */
	public boolean beneficieCongeHospitalierSurPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray contrats = null;
		if (debutPeriode == null) {
			contrats = EOContrat.rechercherContratsPourIndividu(editingContext(), this, false);
		} else {
			contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext(),this,debutPeriode,finPeriode,false);
		}
		if (contrats.count() == 0) {
			return false;
		} 
		contrats =  EOSortOrdering.sortedArrayUsingKeyOrderArray(contrats, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		java.util.Enumeration<EOContrat> e = contrats.objectEnumerator();
		while (e.hasMoreElements()) {
			EOContrat contrat = e.nextElement();
			if (contrat.toTypeContratTravail().estHospitalier()) {
				if (contrat.toTypeContratTravail().code().equals("AH") || contrat.toTypeContratTravail().code().equals("AO") ||
						contrat.toTypeContratTravail().code().equals("CU") || contrat.toTypeContratTravail().code().equals("CMG")) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public boolean aSegmentHospitalierSurPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext(),this,debutPeriode,finPeriode);
		carrieres =  EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieres, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
		for (java.util.Enumeration<EOCarriere> e = carrieres.objectEnumerator();e.hasMoreElements();) {
			EOCarriere carriere = e.nextElement();
			if (carriere.toTypePopulation().estHospitalier()) {
				return true;
			}
		}
		return false;
	}
	/** return true si l'individu peut beneficier d'un Crct sur la periode */
	public boolean peutBeneficierCrctPourPeriode(NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSArray elementsCarriere = recupereElementsCarriere(debutPeriode, finPeriode);
		for (java.util.Enumeration<EOElementCarriere> e = elementsCarriere.objectEnumerator();e.hasMoreElements();) {
			EOElementCarriere element = e.nextElement();
			if (element.toCorps().beneficieCrct()) {
				return true;
			}
		}
		return false;
	}

	/** return true si l'individu peut beneficier d'un Crct sur la periode complete */
	public boolean peutBeneficierCrctPourPeriodeComplete(NSTimestamp debutPeriode,NSTimestamp finPeriode) {

		NSArray<EOElementCarriere> elementsCarriere = recupereElementsCarriere(debutPeriode, finPeriode);
		elementsCarriere = EOSortOrdering.sortedArrayUsingKeyOrderArray(elementsCarriere, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);

		if (DateCtrl.isSameDay(debutPeriode,finPeriode)) {
			return elementsCarriere.count() > 0;
		}

		NSTimestamp dateDebut = null, dateFin = null;

		for (java.util.Enumeration<EOElementCarriere> e = elementsCarriere.objectEnumerator();e.hasMoreElements();) {

			EOElementCarriere element = e.nextElement();

			if (!element.toCorps().beneficieCrct()) {
				return false;	// il existe un segment de carriere de fonctionnaire
			}
			if (dateDebut == null) {
				if (DateCtrl.isBeforeEq(element.dateDebut(),debutPeriode)) {
					dateDebut = element.dateDebut();
					dateFin = element.dateFin();
					if (dateFin == null || DateCtrl.isAfterEq(dateFin,finPeriode)) {
						return true;		// on a trouve un segment qui couvre la période
					}
				} else {
					break;		// le segment ne couvre meme pas la période de début
				}
			} else {
				// nouveau segment, vérifier qu'il commence le jour suivant la date (continuité)
				if (DateCtrl.isSameDay(DateCtrl.jourSuivant(dateFin),element.dateDebut())) {	
					dateFin = element.dateFin();	// on prend comme fin la date de fin de l'élément
					if (dateFin == null || DateCtrl.isAfterEq(dateFin,finPeriode)) {
						return true;		// on a trouvé un élément qui couvre la période
					}
				} else {
					break;	// pas de continuite des éléments
				}
			}
		}
		return dateDebut != null && DateCtrl.isBeforeEq(dateDebut,debutPeriode) && (dateFin == null || DateCtrl.isAfterEq(dateFin,finPeriode));	// dans tous les autres cas on est sorti avant		
	}

	/** Pour une personne nee a l'etranger, le pays de naissance est obligatoire<BR>
	 * Pas de caracteres speciaux dans l'identite
	 * 
	 * @return
	 */
	public String validationsCir() {

		if (personnel().cirDCertification() == null)
			return "IDENTITE - Identité non certifiée !";

		if (nomPatronymique() == null)
			return "IDENTITE - Vous devez fournir le nom de famille !";

		if (indNoInsee() == null)
			return "IDENTITE - Vous devez fournir un numéro INSEE !";

		if (indNoInsee() != null && indNoInsee().length() != LONGUEUR_NO_INSEE)
			return "IDENTITE - Le numéro INSEE doit comporter 13 caractères !";

		if (nomPatronymique() == null)
			return "IDENTITE - Vous devez fournir le nom de famille !";

		// Pas de caracteres speciaux dans les noms
		if (StringCtrl.containsIgnoreCase(nomUsuel(), "_")
				|| StringCtrl.containsIgnoreCase(nomPatronymique(), "_")
				|| StringCtrl.containsIgnoreCase(prenom(), "_")
				|| StringCtrl.containsIgnoreCase(nomUsuel(), "*")
				|| StringCtrl.containsIgnoreCase(nomPatronymique(), "*")
				|| StringCtrl.containsIgnoreCase(prenom(), "*"))
			return "IDENTITE - Pas de caractères spéciaux dans l'identité (Noms et prénoms) !";

		if (dDeces() != null) {
			EODepart departPourDeces = EODepart.departDecesValidePourIndividu(editingContext(), this);
			if (departPourDeces == null)
				return "DECES - Aucun départ pour décès n'a été renseigné !";

		}

		if (neEnFrance() == false && (toPaysNaissance() == null || toPaysNaissance().code().equals(EOPays.CODE_PAYS_ETRANGER_INCONNU))) {
			return "L'individu est né à l'étranger, vous devez fournir un pays de naissance";
		}

		return null;	
	}


	public String nir() {
		if (estNoInseeProvisoire()) {
			return indNoInseeProv();
		} else {
			return indNoInsee();
		}
	}
	public String prenoms() {
		String temp = prenom();
		if (prenom2() != null) {
			temp +="," + prenom2();
		}
		return temp;
	}

	/**
	 * 
	 * @return
	 */
	public String civiliteCir() {
		return toCivilite().sexe();
	}
	/** Departement de naissance ou "099" pour les individus nes a l'etranger */
	public String departementNaissanceCir() {
		if (toDepartement() != null) {
			return toDepartement().code();
		} else {
			return ConstantesCir.DEPT_ETRANGER;
		}
	}
	/** Commune de naissance ou null pour les individus nes a l'etranger */
	public String communeNaissanceCir() {
		if (neEnFrance()) {
			return villeDeNaissance();
		} else {
			return null;
		}
	}
	/** retourne null ou pays de naissance (sur 5 caracteres) pour les individus nes a l'etranger.
	 */
	public String paysNaissanceCir() {
		if (neEnFrance()) {
			return null;
		} else if (toPaysNaissance() != null && toPaysNaissance().code().equals(EOPays.CODE_PAYS_ETRANGER_INCONNU) == false) {
			return ConstantesCir.CODE_PAYS_DEBUT + toPaysNaissance().code();
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String periodeAutorisationTravail() {
		NSArray<EOAutorisationTravail> periodesAutorisations = EOAutorisationTravail.findForIndividu(editingContext(), this);		
		if (periodesAutorisations.size() > 0) {
			return DateCtrl.dateToString(periodesAutorisations.get(0).dateFin());
		}
		return "";
	}

	public String libelle() {
		return identite();
	}
	public String numeroInseeImpression() {
		if (indNoInsee() != null) {
			return indNoInsee();
		} else if (indNoInseeProv() != null) {
			return indNoInseeProv();
		} else {
			return "";
		}
	}

	public String cleInseeFormatee() {
		if (indCleInsee() != null)
			return StringCtrl.stringCompletion(indCleInsee().toString(), 2, "0", "G");
		return "";
	}
	public String cleInseeProvisoireFormatee() {
		if (indCleInseeProv() != null)
			return StringCtrl.stringCompletion(indCleInseeProv().toString(), 2, "0", "G");

		return "";
	}

	public String cleInseeImpression() {
		if (indCleInsee() != null) {
			return indCleInsee().toString();
		} else if (indCleInseeProv() != null) {
			return indCleInseeProv().toString();
		} else {
			return "";
		}
	}


	/**
	 * 
	 * @param edc
	 * @param persId
	 * @return
	 */
	public static EOIndividu rechercherIndividuPersId(EOEditingContext edc, Number persId) {
		try {
			return fetchFirstByQualifier(edc, CocktailFinder.getQualifierEqual(PERS_ID_KEY, persId));
		}
		catch (Exception e) { 
			return null; 
		}
	}

	public static EOIndividu rechercherIndividuNoIndividu(EOEditingContext ec, Number noIndividu) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(NO_INDIVIDU_KEY + "  = %@" , new NSArray(noIndividu));
			return fetchFirstByQualifier(ec, qualifier);
		} 
		catch (Exception e) { return null; }
	}

	/** recherche un individu a partir de son nom et prenom
	 * @param editingContext
	 * @param individuID de l'individu
	 * @param prefetch true si faire les prefetchs
	 * @return individu
	 */
	public static EOIndividu rechercherIndividuAvecID(EOEditingContext edc,Number individuID,boolean prefetch) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(getQualifierValide());
			qualifiers.addObject( CocktailFinder.getQualifierEqual(NO_INDIVIDU_KEY, individuID));

			EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers), null);
			if (prefetch) {
				NSMutableArray relations = new NSMutableArray();
				relations.addObject(PERSONNELS_KEY);
				relations.addObject(TO_PAYS_NAISSANCE_KEY);
				relations.addObject(TO_PAYS_NAISSANCE_KEY);
				relations.addObject(TO_SITUATION_FAMILIALE_KEY);
				relations.addObject(TO_SITUATION_MILITAIRE_KEY);
				fs.setPrefetchingRelationshipKeyPaths(relations);
			}
			fs.setRefreshesRefetchedObjects(true);
			return (EOIndividu)edc.objectsWithFetchSpecification(fs).get(0);
		}
		catch (Exception e)	{return null;}
	}

	/** Renvoie la cle formattee sur 2 caracteres */
	public static String formatteCleInsee(Number cle) {
		if (cle.intValue() < 10)			
			return "0" + cle;

		return cle.toString();
	}

	/** recherche des homonymes sur les noms ou les noms patronymiques et le prenom si il est fourni.
	 * Pour les prenoms, on recherche avec et sans caracteres accentues
	 * @param editingContext
	 * @param nom
	 * @param prenom	
	 * @return individus
	 */
	public static NSArray rechercherHomonymes(EOEditingContext editingContext,String nom,String prenom) {
		NSMutableArray args = new NSMutableArray();
		String nomUsuel = nom.toUpperCase();
		if (nomUsuel.startsWith("DE ")) {
			nomUsuel = nomUsuel.replaceFirst("DE ", "*");
		}
		// 23/09/2010 - modification du critère de recherche des homonymes, on prend en compte aussi le début du nom pour retrouver les
		// noms composés dont une partie serait supprimée (DT 2812). Dans le cas où le nom comporte un délimiteur (espace ou tiret) on sépare
		// la recherche en deux morceaux.
		nomUsuel = nomUsuel.trim();	// Supprimer tous les espaces en tête et fin
		String stringQualifier = "";
		int indexSeparateur = rechercherSeparateur(nomUsuel);
		if (indexSeparateur > 0) {
			String nom1 = nomUsuel.substring(0,indexSeparateur);
			nom1 = formaterPourRechercheComplete(nom1);
			String nom2 = nomUsuel.substring(indexSeparateur);
			nom2 = formaterPourRechercheComplete(nom2);
			args.addObject(nom1);
			args.addObject(nom2);
			args.addObject(nom1);
			args.addObject(nom2);
			stringQualifier = "(nomUsuel caseInsensitiveLike %@ OR nomUsuel caseInsensitiveLike %@ OR nomPatronymique caseInsensitiveLike %@ OR nomPatronymique caseInsensitiveLike %@)";
		} else {
			nomUsuel = formaterPourRechercheComplete(nomUsuel);
			args.addObject(nomUsuel);
			args.addObject(nomUsuel);
			stringQualifier = "(nomUsuel caseInsensitiveLike %@ OR nomPatronymique caseInsensitiveLike %@)";
		}
		if (prenom != null) {
			String prenomPropre = StringCtrl.chaineClaire(prenom, true).toUpperCase();
			String prenomMajuscule = prenom.toUpperCase();
			args.addObject(formaterPourRechercheComplete(prenomMajuscule));
			if (prenomMajuscule.equals(prenomPropre) == false) {
				args.addObject(formaterPourRechercheComplete(prenomPropre));
				stringQualifier = stringQualifier + " AND (prenom caseInsensitiveLike %@ OR prenom caseInsensitiveLike %@)";
			} else {
				stringQualifier = stringQualifier + " AND prenom caseInsensitiveLike %@";
			}
		}
		stringQualifier = stringQualifier + " AND temValide = 'O' AND dDeces = nil";
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, args);
		LogManager.logDetail("qualifier de recherche des homonymes " + myQualifier);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, myQualifier, null);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray(PERSONNELS_KEY));
		return editingContext.objectsWithFetchSpecification(fs);

	}

	/** Creation manuelle du noIndividu d'un individu
	@param editingContext
	@return num du persID */
	public static Integer construireNoIndividu(EOEditingContext editingContext) {
		return SuperFinder.clePrimairePour(editingContext,ENTITY_NAME,NO_INDIVIDU_KEY, "SeqIndividu", true);
	}

	public void construireNoInseeProvisoire() {

		String noInsee = EOIndividu.NO_INSEE_PROVISOIRE;
		if (cCivilite().equals(ManGUEConstantes.MONSIEUR)){
			noInsee = EOCivilite.CODE_SEXE_MONSIEUR + noInsee;
		} else {
			noInsee = EOCivilite.CODE_SEXE_MADAME + noInsee;
		}
		setIndNoInseeProv(noInsee);
		setPrendreEnCompteNumeroProvisoire(true);

	}


	/** Restreint une liste d'individus (EOIndividu) aux non-affectes
	 * @return liste d'EOIndividu */
	public static NSArray restreindrePersonnelsNonAffectes(EOEditingContext editingContext,NSArray personnels) {
		// Plus performant de rechercher toutes les affectations d'un coup même si à chaque fois
		// on a ensuite un fetch pour l'individu
		NSArray affectations = EOAffectation.rechercherAffectationsAvecCriteres(editingContext,null);
		NSArray individusAffectes = (NSArray)affectations.valueForKey(EOAffectation.INDIVIDU_KEY);
		NSMutableArray nonAffectes = new NSMutableArray();
		for (java.util.Enumeration<EOIndividu> e = personnels.objectEnumerator();e.hasMoreElements();) {
			// pour chaque individu de la liste initiale, si elle n'est pas dans la liste des individus affectées
			// et qu'on ne l'a pas encore gardée, alors l'ajouter au résultat
			EOIndividu individu = e.nextElement();
			if (nonAffectes.containsObject(individu) == false && individusAffectes.containsObject(individu) == false) {
				// pas d'affectation, on l'ajoute
				nonAffectes.addObject(individu);	
			}
		}
		return new NSArray(nonAffectes);
	}
	/** Restreint une liste d'individus (EOIndividu) aux non-vacataires
	 * @return liste d'EOIndividu */
	public static NSArray restreindreNonVacataires(EOEditingContext editingContext,NSArray individus) {
		// Plus performant de rechercher tous les vacataires d'un coup même si à chaque fois
		// on a ensuite un fetch pour la personne
		NSArray vacataires = EOContrat.rechercherContratsPourPeriodeEtTypePersonnel(editingContext, null, null, null, null, ManGUEConstantes.VACATAIRE, true, false);
		NSArray individusVacataires = (NSArray)vacataires.valueForKey("toIndividu");
		return restreindreNonVacataires(editingContext,individus,individusVacataires);
	}
	/** Restreint une liste d'individus (EOIndividu) aux non-vacataires
	 * @param individus liste d'individus
	 * @param liste d'individus vacataires
	 * @return liste d'EOIndividu */
	public static NSArray restreindreNonVacataires(EOEditingContext editingContext,NSArray individus,NSArray individusVacataires) {
		NSMutableArray individusRetenus = new NSMutableArray();
		for (java.util.Enumeration<EOIndividu> e = individus.objectEnumerator();e.hasMoreElements();) {
			// pour chaque personne de la liste initiale, si elle n'est pas dans la liste des vacataires
			// et qu'on ne l'a pas encore gardée, alors l'ajouter au résultat
			EOIndividu individu = e.nextElement();
			if (individusRetenus.containsObject(individu) == false && individusVacataires.containsObject(individu) == false) {
				// n'est pas vacataire, on l'ajoute
				individusRetenus.addObject(individu);	
			}
		}
		return new NSArray(individusRetenus);
	}
	// Méthodes privées
	private EOStructure structureAffectation() {
		// pour ramener à une date sans prendre en compte les heures
		NSTimestamp date = null;
		if (datePourImpression() != null) {
			date = DateCtrl.stringToDate(DateCtrl.dateToString(datePourImpression()));
		} else {
			date = DateCtrl.stringToDate(DateCtrl.dateToString(new NSTimestamp()));
		}
		NSMutableArray<EOAffectation> affectations = new NSMutableArray(EOAffectation.findForIndividu(editingContext(),this,date));
		EOSortOrdering.sortArrayUsingKeyOrderArray(affectations, EOAffectation.SORT_ARRAY_AFF_PRINCIPALE_DESC);
		try {
			return affectations.get(0).toStructureUlr();
		} catch (Exception e) {
			return null;
		}
	}
	/** retourne le departement de naissance extrait du numero SS ou exception si numeroSS = null */
	private String departementNaissance() {
		try {
			String departement = indNoInsee().substring(5,7);
			return departement;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 
	 */
	private void verifierNoSS() {

		if (indNoInsee() == null && indNoInseeProv() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le numéro Insee ou le numéro provisoire");
		}
		if (indNoInseeProv() != null) {
			if (indNoInseeProv().length() > LONGUEUR_NO_INSEE) {
				throw new NSValidation.ValidationException("Le numéro SS comporte au plus treize chiffres");
			}
		}
		if (indNoInsee() != null) {
			if (indNoInsee().length() != LONGUEUR_NO_INSEE) {
				throw new NSValidation.ValidationException("Le numéro SS (" + indNoInsee() + ") comporte treize chiffres");
			}
			// vérifier si le numéro Insee est cohérent avec la civilité
			if (estHomme()) {
				if (indNoInsee().substring(0,1).equals("1") == false) {
					throw new NSValidation.ValidationException("Pour un homme, le numéro Insee commence par 1");
				}
			} else {
				if (indNoInsee().substring(0,1).equals("2") == false && indNoInsee().substring(0,1).equals("8") == false) {
					throw new NSValidation.ValidationException("Pour une femme, le numéro Insee commence par 2");
				}
			}
			if (!estNoInseeProvisoire()) {		// NoInsee provisoire
				String temp = indNoInsee().substring(1,3);
				int anneeNaissance = new Integer(temp).intValue();
				anneeNaissance = ANNEE_BASE_POUR_NAISSANCE + anneeNaissance;
				if (DateCtrl.getYear(dNaissance()) != anneeNaissance) {
					throw new NSValidation.ValidationException("L'année de naissance du numéro Insee (" + anneeNaissance+ ") ne correspond pas à l'année de naissance (" + DateCtrl.getYear(dNaissance()) + ") !");
				}
				temp = indNoInsee().substring(3,5);
				if (temp.equals("99") == false) {		// NoSS provisoire
					int mois = new Integer(temp).intValue();
					// 21/06/2011 - Correction sur les n° Insee dont le mois est >= 20
					if (mois < 20  && DateCtrl.getMonth(dNaissance()) + 1 != mois) {
						throw new NSValidation.ValidationException("Le mois de naissance du numéro Insee ne correspond pas au mois de naissance");
					}
				}
				// vérifier la cohérence du département avec le département de l'individu et avec le pays de naissance
				temp = indNoInsee().substring(5,7);
				if (temp.equals("99") || temp.equals("96")) {	// étranger ou ancien protectorat
					if (toPaysNaissance() == null) {
						throw new NSValidation.ValidationException("Pour une personne née à l'étranger, vous devez fournir le pays de naissance");
					}
					if (toPaysNaissance().code().equals(EOPays.CODE_PAYS_FRANCE)) {
						throw new NSValidation.ValidationException("Le numéro Insee indique que la personne est née à l'étranger");
					} else {
						String temp1 = indNoInsee().substring(7,10);
						try {
							int valeur = new Integer(temp1).intValue();
							if (valeur == 0) {
								throw new NSValidation.ValidationException("Le code pays du numéro Insee pour les personnes nées à l'étranger doit être compris entre 000 et 999");
							}
						} catch(Exception e) {
							throw new NSValidation.ValidationException("Le code pays du numéro Insee pour les personnes nées à l'étranger ne doit comporter que des chiffres");
						}
					}
					if (temp.equals("96") && anneeNaissance > 1967) {
						throw new NSValidation.ValidationException("A partir de 1967, Le code département du numéro Insee ne peut être 96, ce doit être 99");
					} 
				} else {
					String departement = "";
					boolean ancienneColonie = false;	// 01/10/2010 - Pour les anciennes colonies
					if (temp.equals("97") || temp.equals("98")) {	// DOM - TOM
						if (toDepartement() != null) {
							departement = toDepartement().code();
						} else if (temp.equals("98")) {	// Les anciennes colonies ont des codes à cheval sur Mayotte, on se base donc sur le fait que le département n'est pas fourni pour vérifier si il s'agit d'anciennes colonies
							try {
								String temp1 = indNoInsee().substring(7,10);
								int val = new Integer(temp1).intValue();
								ancienneColonie = (val >= 201 && val <= 506);
							} catch (Exception e) {e.printStackTrace();}
						}
						temp = indNoInsee().substring(5,8);		// le département est codé sur 3 caractères;
						ancienneColonie = true;		// PERMET DE NE PAS CONTROLER LES DEPARTEMENTS OUTREMER.
					} else if (toDepartement() != null) {
						departement = toDepartement().code().substring(1);		// les numéros de département commencent par 0 pour les départements hors dom-tom
					}

					if (!ancienneColonie && toDepartement() != null) {
						if (departement.equals(temp) == false) {
							// Vérification des gens nés sous protectorat 
							if (anneeNaissance <= 1962 && (temp.equals("91") || temp.equals("92") || temp.equals("93") || temp.equals("94") || temp.equals("95"))) {
								// Vérifier que le département n'est pas fourni
								if (toDepartement() != null) {
									throw new NSValidation.ValidationException("Pas de département pour les personnes nées en Algérie ou sous un protectorat");
								}
							} else {
								LogManager.logDetail("\t  Annee naissance : " + anneeNaissance + " , Code Dep INSEE : " + temp);
								// vérification île de france pour les gens nés avant 1968
								if (anneeNaissance <= 1968 && (temp.equals("75") || temp.equals("78"))) {

									LogManager.logDetail("\t Verif Ile de France");

									// le code département 75 peut avoir un département de naissance égal à 75 ou 92 ou 93 ou 94
									if (temp.equals("75") && departement.equals("75") == false && departement.equals("92") == false && departement.equals("93") == false && departement.equals("94") == false) {
										throw new NSValidation.ValidationException("Une personne née avant 1968 avec comme département du numéro Insee 75, ne peut avoir comme département de naissance que : 75, 92, 93 ou 94");
									}
									// 09/03/2011- le code département 78 peut avoir un département de naissance égal à  78 ou 91 ou 92 ou 93 ou 94 ou 95
									if (temp.equals("78") && departement.equals("78") == false && departement.equals("91") == false && departement.equals("92") == false && departement.equals("93") == false && departement.equals("94") == false && departement.equals("95") == false) {
										throw new NSValidation.ValidationException("Une personne née avant 1968 avec comme département du numéro Insee 78, ne peut avoir comme département de naissance que : 78, 91, 92, 93, 94 ou 95");
									}
								} else if (temp.equals(CODE_DEP_INSEE_CORSE)) {

									LogManager.logDetail("\t -- Vérification INSEE corse : " + anneeNaissance + " ,  Dep INSEE : " + temp + ", Dep saisi : " + departement);

									if (anneeNaissance < 1976) {			// 20 avant 1976
										if (departement.equals(CODE_DEP_INSEE_CORSE) == false) {
											throw new NSValidation.ValidationException("Le département de naissance sélectionné ne correspond pas à la Corse (20 pour une naissance avant 1976)");
										}																							
									}
									else
										if (anneeNaissance > 1976) {		// 2A ou 2B apres 1976
											if (departement.equals("2A") == false && departement.equals("2B") == false) {
												throw new NSValidation.ValidationException("Le département de naissance sélectionné ne correspond pas à la Corse (2A ou 2B pour une naissance après 1976)");
											}												
										}
								}
								else {
									LogManager.logDetail("\t Erreur ");
									throw new NSValidation.ValidationException("Le département du numéro Insee ("+temp + ") ne correspond pas avec le département fourni (" + toDepartement().code()+") !");
								}
							}
						}
						if (anneeNaissance <= 1962 && (temp.equals("91") || temp.equals("92") || temp.equals("93") || temp.equals("94") || temp.equals("95"))) {
							// Vérifier que le pays de naissance est correct
							if (temp.equals("91") || temp.equals("92") || temp.equals("93") || temp.equals("94")) {
								if (toPaysNaissance() == null || (toPaysNaissance().code().equals("352") == false && toPaysNaissance().code().equals("999") == false)) {
									throw new NSValidation.ValidationException("Le pays de naissance de cette personne est l'Algérie");
								}
							} else if (temp.equals("95")) {
								if (toPaysNaissance() == null || (toPaysNaissance().code().equals("350") == false && toPaysNaissance().code().equals("999") == false)) {
									throw new NSValidation.ValidationException("Le pays de naissance de cette personne est le Maroc");
								}
							}
						} else	if (toPaysNaissance().code().equals(EOPays.CODE_PAYS_FRANCE) == false) {
							throw new NSValidation.ValidationException("Le numéro Insee indique que la personne est née en France");
						}
						if ((temp.equals("2A") || temp.equals("2B")) && anneeNaissance < 1976) {
							LogManager.logDetail("\t controle CORSE 2A ou 2 B > 1976 ");
							throw new NSValidation.ValidationException("Pour un corse né avant 1976, le code département du numéro Insee ne peut être 2A ou 2B");
						}
						//						if (temp.equals(CODE_DEP_INSEE_CORSE) && anneeNaissance >= 1976) {
						//							LogManager.logDetail("\t controle CORSE 20 < 1976 ");
						//							throw new NSValidation.ValidationException("Pour un corse né après 1976, le code département du numéro Insee doit être 2A ou 2B");
						//						}
					}
				}
				// vérifier la clé Insee
				if (indCleInsee() == null) {
					throw new NSValidation.ValidationException("Vous devez fournir la clé Insee");
				}
				int cleInsee = getCleInsee(indNoInsee()).intValue();
				if (cleInsee > 99) {
					throw new NSValidation.ValidationException("La clé INSEE doit etre inférieure à 100");
				}
				if (cleInsee != indCleInsee().intValue()) {
					throw new NSValidation.ValidationException("La clé Insee a une valeur erronée");
				}
			}
		}
	}

	/**
	 * 
	 * @param temValide
	 * @return
	 */
	private EOFournis fournisseur(String temValide) {

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject( CocktailFinder.getQualifierEqual(EOFournis.PERS_ID_KEY, persId()));

			if (temValide != null) {
				qualifiers.addObject( CocktailFinder.getQualifierEqual(EOFournis.FOU_VALIDE_KEY, temValide));
			}

			return EOFournis.fetchFirstByQualifier(editingContext(), new EOAndQualifier(qualifiers), EOFournis.SORT_ARRAY_CODE_DESC);
		} 
		catch (Exception e) { 
			e.printStackTrace();
			return null; 
		}

	}
	// Méthodes statiques privées
	// Créer une string avec des étoiles par tout
	private static String formaterPourRechercheComplete(String aStr) {
		// Modif. le 22/04/2005 :spécif. UNC pour les noms du style N'GUYEN, le car. ' est remplacé par rien
		// Modif. le 17/07/08 : pour les noms composés avec -, le car. - est remplacé par rien
		aStr = aStr.replaceAll("'","");
		aStr = aStr.replaceAll("-","");
		String newStr = "*";
		for (int i = 0; i < aStr.length();i++) {
			newStr = newStr + aStr.substring(i,i + 1) + "*";
		}
		return newStr;
	}
	private static int rechercherSeparateur(String aStr) {
		int index = aStr.indexOf(" ");
		if (index < 0) {
			index = aStr.indexOf("-");
		}
		return index;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierValide() {
		return  EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray<String>("O"));
	}

}

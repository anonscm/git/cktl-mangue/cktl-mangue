// _EOCompte.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCompte.java instead.
package org.cocktail.mangue.modele.grhum.referentiel;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOCompte extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Compte";
	public static final String ENTITY_TABLE_NAME = "GRHUM.COMPTE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cptOrdre";

	public static final String CPT_CHARTE_KEY = "cptCharte";
	public static final String CPT_CONNEXION_KEY = "cptConnexion";
	public static final String CPT_CRYPTE_KEY = "cptCrypte";
	public static final String CPT_DEBUT_VALIDE_KEY = "cptDebutValide";
	public static final String CPT_DOMAINE_KEY = "cptDomaine";
	public static final String CPT_EMAIL_KEY = "cptEmail";
	public static final String CPT_FIN_VALIDE_KEY = "cptFinValide";
	public static final String CPT_GID_KEY = "cptGid";
	public static final String CPT_HOME_KEY = "cptHome";
	public static final String CPT_LOGIN_KEY = "cptLogin";
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String CPT_PASSWD_KEY = "cptPasswd";
	public static final String CPT_PASSWD_CLAIR_KEY = "cptPasswdClair";
	public static final String CPT_SHELL_KEY = "cptShell";
	public static final String CPT_UID_KEY = "cptUid";
	public static final String CPT_VALIDE_KEY = "cptValide";
	public static final String CPT_VLAN_KEY = "cptVlan";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String TCRY_ORDRE_KEY = "tcryOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String CPT_CHARTE_COLKEY = "CPT_CHARTE";
	public static final String CPT_CONNEXION_COLKEY = "CPT_CONNEXION";
	public static final String CPT_CRYPTE_COLKEY = "CPT_CRYPTE";
	public static final String CPT_DEBUT_VALIDE_COLKEY = "CPT_DEBUT_VALIDE";
	public static final String CPT_DOMAINE_COLKEY = "CPT_DOMAINE";
	public static final String CPT_EMAIL_COLKEY = "CPT_EMAIL";
	public static final String CPT_FIN_VALIDE_COLKEY = "CPT_FIN_VALIDE";
	public static final String CPT_GID_COLKEY = "CPT_GID";
	public static final String CPT_HOME_COLKEY = "CPT_HOME";
	public static final String CPT_LOGIN_COLKEY = "CPT_LOGIN";
	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String CPT_PASSWD_COLKEY = "CPT_PASSWD";
	public static final String CPT_PASSWD_CLAIR_COLKEY = "CPT_PASSWD_CLAIR";
	public static final String CPT_SHELL_COLKEY = "CPT_SHELL";
	public static final String CPT_UID_COLKEY = "CPT_UID";
	public static final String CPT_VALIDE_COLKEY = "CPT_VALIDE";
	public static final String CPT_VLAN_COLKEY = "CPT_VLAN";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_ID_CREATION_COLKEY = "CPT_CREATEUR";
	public static final String PERS_ID_MODIFICATION_COLKEY = "CPT_MODIFICATEUR";
	public static final String TCRY_ORDRE_COLKEY = "TCRY_ORDRE";



	// Relationships
	public static final String TO_VLAN_KEY = "toVlan";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cptCharte() {
    return (String) storedValueForKey(CPT_CHARTE_KEY);
  }

  public void setCptCharte(String value) {
    takeStoredValueForKey(value, CPT_CHARTE_KEY);
  }

  public String cptConnexion() {
    return (String) storedValueForKey(CPT_CONNEXION_KEY);
  }

  public void setCptConnexion(String value) {
    takeStoredValueForKey(value, CPT_CONNEXION_KEY);
  }

  public String cptCrypte() {
    return (String) storedValueForKey(CPT_CRYPTE_KEY);
  }

  public void setCptCrypte(String value) {
    takeStoredValueForKey(value, CPT_CRYPTE_KEY);
  }

  public NSTimestamp cptDebutValide() {
    return (NSTimestamp) storedValueForKey(CPT_DEBUT_VALIDE_KEY);
  }

  public void setCptDebutValide(NSTimestamp value) {
    takeStoredValueForKey(value, CPT_DEBUT_VALIDE_KEY);
  }

  public String cptDomaine() {
    return (String) storedValueForKey(CPT_DOMAINE_KEY);
  }

  public void setCptDomaine(String value) {
    takeStoredValueForKey(value, CPT_DOMAINE_KEY);
  }

  public String cptEmail() {
    return (String) storedValueForKey(CPT_EMAIL_KEY);
  }

  public void setCptEmail(String value) {
    takeStoredValueForKey(value, CPT_EMAIL_KEY);
  }

  public NSTimestamp cptFinValide() {
    return (NSTimestamp) storedValueForKey(CPT_FIN_VALIDE_KEY);
  }

  public void setCptFinValide(NSTimestamp value) {
    takeStoredValueForKey(value, CPT_FIN_VALIDE_KEY);
  }

  public Integer cptGid() {
    return (Integer) storedValueForKey(CPT_GID_KEY);
  }

  public void setCptGid(Integer value) {
    takeStoredValueForKey(value, CPT_GID_KEY);
  }

  public String cptHome() {
    return (String) storedValueForKey(CPT_HOME_KEY);
  }

  public void setCptHome(String value) {
    takeStoredValueForKey(value, CPT_HOME_KEY);
  }

  public String cptLogin() {
    return (String) storedValueForKey(CPT_LOGIN_KEY);
  }

  public void setCptLogin(String value) {
    takeStoredValueForKey(value, CPT_LOGIN_KEY);
  }

  public Integer cptOrdre() {
    return (Integer) storedValueForKey(CPT_ORDRE_KEY);
  }

  public void setCptOrdre(Integer value) {
    takeStoredValueForKey(value, CPT_ORDRE_KEY);
  }

  public String cptPasswd() {
    return (String) storedValueForKey(CPT_PASSWD_KEY);
  }

  public void setCptPasswd(String value) {
    takeStoredValueForKey(value, CPT_PASSWD_KEY);
  }

  public String cptPasswdClair() {
    return (String) storedValueForKey(CPT_PASSWD_CLAIR_KEY);
  }

  public void setCptPasswdClair(String value) {
    takeStoredValueForKey(value, CPT_PASSWD_CLAIR_KEY);
  }

  public String cptShell() {
    return (String) storedValueForKey(CPT_SHELL_KEY);
  }

  public void setCptShell(String value) {
    takeStoredValueForKey(value, CPT_SHELL_KEY);
  }

  public Integer cptUid() {
    return (Integer) storedValueForKey(CPT_UID_KEY);
  }

  public void setCptUid(Integer value) {
    takeStoredValueForKey(value, CPT_UID_KEY);
  }

  public String cptValide() {
    return (String) storedValueForKey(CPT_VALIDE_KEY);
  }

  public void setCptValide(String value) {
    takeStoredValueForKey(value, CPT_VALIDE_KEY);
  }

  public String cptVlan() {
    return (String) storedValueForKey(CPT_VLAN_KEY);
  }

  public void setCptVlan(String value) {
    takeStoredValueForKey(value, CPT_VLAN_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public Integer tcryOrdre() {
    return (Integer) storedValueForKey(TCRY_ORDRE_KEY);
  }

  public void setTcryOrdre(Integer value) {
    takeStoredValueForKey(value, TCRY_ORDRE_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOVlans toVlan() {
    return (org.cocktail.mangue.modele.grhum.referentiel.EOVlans)storedValueForKey(TO_VLAN_KEY);
  }

  public void setToVlanRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOVlans value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.referentiel.EOVlans oldValue = toVlan();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_VLAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_VLAN_KEY);
    }
  }
  

/**
 * Créer une instance de EOCompte avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCompte createEOCompte(EOEditingContext editingContext, String cptCharte
, String cptConnexion
, Integer cptGid
, Integer cptOrdre
, Integer cptUid
, String cptValide
, String cptVlan
, NSTimestamp dCreation
, NSTimestamp dModification
, Integer persId
, Integer tcryOrdre
, org.cocktail.mangue.modele.grhum.referentiel.EOVlans toVlan			) {
    EOCompte eo = (EOCompte) createAndInsertInstance(editingContext, _EOCompte.ENTITY_NAME);    
		eo.setCptCharte(cptCharte);
		eo.setCptConnexion(cptConnexion);
		eo.setCptGid(cptGid);
		eo.setCptOrdre(cptOrdre);
		eo.setCptUid(cptUid);
		eo.setCptValide(cptValide);
		eo.setCptVlan(cptVlan);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setPersId(persId);
		eo.setTcryOrdre(tcryOrdre);
    eo.setToVlanRelationship(toVlan);
    return eo;
  }

  
	  public EOCompte localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCompte)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompte creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompte creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCompte object = (EOCompte)createAndInsertInstance(editingContext, _EOCompte.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCompte localInstanceIn(EOEditingContext editingContext, EOCompte eo) {
    EOCompte localInstance = (eo == null) ? null : (EOCompte)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCompte#localInstanceIn a la place.
   */
	public static EOCompte localInstanceOf(EOEditingContext editingContext, EOCompte eo) {
		return EOCompte.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCompte fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCompte fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCompte eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCompte)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCompte fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCompte fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCompte eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCompte)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCompte fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCompte eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCompte ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCompte fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

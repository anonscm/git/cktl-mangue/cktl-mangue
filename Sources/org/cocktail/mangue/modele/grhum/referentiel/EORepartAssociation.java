// EORepartAssociation.java
// Created on Wed Jun 11 12:25:29 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.modele.grhum.referentiel;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOAssociation;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.IIndividuAvecDuree;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EORepartAssociation extends _EORepartAssociation implements IIndividuAvecDuree {

	public static NSArray SORT_LIBELLE_ASS_ASC = new NSArray(new EOSortOrdering(ASSOCIATION_KEY+"."+EOAssociation.LIBELLE_LONG_KEY, EOSortOrdering.CompareAscending));
	
	public static String CODE_ASSOCIATION_LOCAL = "LOCAL";
	public static String STRUCTURE_ASSOCIEE_KEY = "structureAssociee";

	public EORepartAssociation() {
		super();
	}


	public boolean estPrincipal() {
		return rasPrincipale() != null && rasPrincipale().equals("O");
	}
	
	/**
	 * 
	 * @param ec
	 * @param structure
	 * @param individu
	 * @return
	 */
	public static EORepartAssociation creer(EOEditingContext ec, EOStructure structure, EOIndividu individu, EOAgentPersonnel agent) {

		EORepartAssociation newObject = (EORepartAssociation)CocktailUtilities.instanceForEntity(ec, EORepartAssociation.ENTITY_NAME);

		newObject.setPersId(individu.persId());
		newObject.setStructureRelationship(structure);
		newObject.setRasRang(new Integer(1));
		newObject.setPersIdCreation(agent.toIndividu().persId());
		newObject.setPersIdModification(agent.toIndividu().persId());

		ec.insertObject(newObject);

		return newObject;
	}

	/**
	 * 
	 * @param ec
	 * @param structure
	 * @param individu
	 * @param agent
	 * @return
	 */
	public static EORepartAssociation creerAssociationAffectation(EOEditingContext ec, EOStructure structure, EOIndividu individu, EOAgentPersonnel agent) {

		EORepartAssociation newObject = (EORepartAssociation)CocktailUtilities.instanceForEntity(ec, EORepartAssociation.ENTITY_NAME);

		newObject.setAssociationRelationship(EOAssociation.associationPourAffectation(ec));
		newObject.setPersId(individu.persId());
		newObject.setStructureRelationship(structure);
		newObject.setRasRang(new Integer(1));
		newObject.setPersIdCreation(agent.toIndividu().persId());
		newObject.setPersIdModification(agent.toIndividu().persId());

		ec.insertObject(newObject);

		return newObject;
	}
	public static EORepartAssociation creerAssociationHeberge(EOEditingContext ec, EOStructure structure, EOIndividu individu, EOAgentPersonnel agent) {

		EORepartAssociation newObject = (EORepartAssociation)CocktailUtilities.instanceForEntity(ec, EORepartAssociation.ENTITY_NAME);

		newObject.setAssociationRelationship(EOAssociation.associationPourHeberge(ec));
		newObject.setPersId(individu.persId());
		newObject.setPersIdCreation(agent.toIndividu().persId());
		newObject.setPersIdModification(agent.toIndividu().persId());
		newObject.setStructureRelationship(structure);
		newObject.setRasRang(new Integer(1));

		ec.insertObject(newObject);

		return newObject;
	}

	/**
	 * @param ec : editing Context
	 * @param structure : structure de l'individu
	 * @param individu : individu
	 * @return Une répartition entre l'individu et la structure avec le role Enseignant
	 */
	public static EORepartAssociation creerAssociationEnseignant(EOEditingContext ec, EOStructure structure, EOIndividu individu, EOAgentPersonnel agent) {

		EORepartAssociation newObject = new EORepartAssociation();

		newObject.setAssociationRelationship(EOAssociation.associationPourEnseignant(ec));
		newObject.setPersId(individu.persId());
		newObject.setPersIdCreation(agent.toIndividu().persId());
		newObject.setPersIdModification(agent.toIndividu().persId());
		newObject.setStructureRelationship(structure);
		newObject.setRasRang(new Integer(1));

		ec.insertObject(newObject);
		return newObject;
	}

	/**
	 * 
	 * @param edc
	 * @param service
	 * @return
	 */
	public static NSArray<EORepartAssociation> findLocaux(EOEditingContext edc, EOStructure service) {
		
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(STRUCTURE_KEY, service));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(ASSOCIATION_KEY+"."+EOAssociation.CODE_KEY, CODE_ASSOCIATION_LOCAL ));
		
		return fetchAll(edc, new EOAndQualifier(andQualifiers));
		
	}
	
	/**
	 * 
	 * @param edc
	 * @param service
	 * @return
	 */
	public static EORepartAssociation findLocalPrincipal(EOEditingContext edc, EOStructure service) {
		
		NSArray<EORepartAssociation> locaux = findLocaux(edc, service);
		for (EORepartAssociation local : locaux) {
			if (local.estPrincipal()) {
				return local;
			}
		}
		return null;		
	}

	
	/**
	 * 
	 * @param edc
	 * @param service
	 * @return
	 */
	public static NSArray<EORepartAssociation> findLocauxForService(EOEditingContext edc, EOStructure service) {
		
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(STRUCTURE_KEY, service));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(ASSOCIATION_KEY+"."+EOAssociation.CODE_KEY, CODE_ASSOCIATION_LOCAL ));
		
		return fetchAll(edc, new EOAndQualifier(andQualifiers));
		
	}
	
	public EOStructure structureAssociee() {
		try{
			return EOStructure.findForPersId(editingContext(), persId());
		}
		catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 
	 * @param edc
	 * @param individu
	 * @param dateFin
	 */
	public static void fermerAssociationsADate(EOEditingContext edc, EOIndividu individu, NSTimestamp dateFin) {
		
		NSArray<EORepartAssociation> reparts = EORepartAssociation.rechercherAssociationsPourIndividuEtGroupe(edc, individu, null); 
		for (EORepartAssociation repart : reparts) {

			if (repart.dateDebut() != null && DateCtrl.isBefore(repart.dateDebut(), dateFin) && 
					(repart.dateFin() == null || DateCtrl.isAfter(repart.dateFin(), dateFin)))
				repart.fermerADate(dateFin);

			if (repart.dateDebut() != null && DateCtrl.isAfter(repart.dateDebut(), dateFin) )
				edc.deleteObject(repart);
		}
	}

	public void fermerADate(NSTimestamp dateFermeture) {
		if (dateDebut() != null)
			setDateFin(dateFermeture);
	}

	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,DATE_DEBUT_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null) {
			setDateDebut(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_DEBUT_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,DATE_FIN_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null) {
			setDateFin(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_FIN_KEY,uneDate);
		}
	}
	// Interface IndividuAvecDuree
	public NSTimestamp debutPeriode() {
		return dateDebut();
	}
	public NSTimestamp finPeriode() {
		return dateFin();
	}
	/** Retourne l'individu lie a une association */
	public EOIndividu individu() {
		if (persId() != null) {
			return EOIndividu.rechercherIndividuPersId(editingContext(), persId());
		} else {
			return null;
		}
	}
	/** Initialise une repart association avec une repart structure et un agent responsable de la creation */
	public void initAvecRepartStructureEtAgent(EORepartStructure repartStructure,Integer agtPersId) {
		setStructureRelationship(repartStructure.structure());
		setPersId(repartStructure.persId());
		setRasRang(new Integer(1));
		setPersIdCreation(agtPersId);
	}
	/** Initialise une repart association un individu, une structure et un agent responsable de la creation */
	public void initAvecIndividuStructureEtAgent(EOIndividu individu, EOStructure structure,Integer agtPersId) {
		setStructureRelationship(structure);
		setPersId(individu.persId());
		setRasRang(new Integer(1));
		setPersIdCreation(agtPersId);
	}
	public void initAvecRepartStructure(EORepartStructure repartStructure) {
		initAvecRepartStructureEtAgent(repartStructure, null);
	}
	public void supprimerRelations() {
		setAssociationRelationship(null);
	}
	/** Verifie que la date de fin n'est pas posterieure a la date de debut et 
	 * que toutes les donnees sont fournies */
	public void validateForSave() {

		// La date de fin ne doit pas etre inferieure a la date de debut
		if (dateDebut() != null && dateFin() != null && dateDebut().after(dateFin())) {
			throw new NSValidation.ValidationException("ASSOCIATION\nLa date de fin (" + dateFinFormatee() + ") doit être postérieure à la date de début (" + dateDebutFormatee() + ") !");
		} else if (dateDebut() == null && dateFin() != null) {
			throw new NSValidation.ValidationException("La date de début n'est pas renseignée (Rôle))!");
		}
		if (structure() == null || persId() == null) {
			throw new NSValidation.ValidationException("Une repartAssociation est associée à une structure");
		}
		if (association() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une association");
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());

		setDModification(new NSTimestamp());

	}
	// Méthodes statiques
	public static NSArray rechercherRepartAssociationsPourIndividuEtAssociation(EOEditingContext editingContext,EOIndividu individu, String nomAssociation) {
		return rechercherRepartAssociationsPourIndividuAssociationEtPeriode(editingContext, individu, nomAssociation,null,null);
	}
	public static NSArray rechercherRepartAssociationsPourIndividuAssociationEtPeriode(EOEditingContext editingContext,EOIndividu individu, String nomAssociation, NSTimestamp dateDebut,NSTimestamp dateFin) {
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + "=%@", new NSArray(individu.persId())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ASSOCIATION_KEY+"."+EOAssociation.LIBELLE_LONG_KEY + "= %@", new NSArray(nomAssociation)));
		if (dateDebut != null) {
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin));
		}

		return fetchAll(editingContext,new EOAndQualifier(qualifiers),null);    	
	}

	public static EORepartAssociation rechercherPourIndividuStructureAssociationEtPeriode(EOEditingContext editingContext,EOIndividu individu, EOStructure structure, String nomAssociation, NSTimestamp dateDebut,NSTimestamp dateFin) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + "=%@", new NSArray(individu.persId())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY+"= %@", new NSArray(structure)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(ASSOCIATION_KEY+"."+EOAssociation.LIBELLE_LONG_KEY + "= %@", new NSArray(nomAssociation)));
		if (dateDebut != null)
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin));

		return fetchFirstByQualifier(editingContext,new EOAndQualifier(qualifiers));   	
	}


	public static NSArray rechercherAssociationsPourIndividuEtGroupe(EOEditingContext ec, EOIndividu individu, EOStructure structure) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + "=%@", new NSArray(individu.persId())));

		if (structure != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY + "=%@", new NSArray(structure)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_LIBELLE_ASS_ASC);

	}


	/** Recherche des associations selon des criteres
	 * 
	 * @param editingContext
	 * @param qualifier qualifier contenant les criteres
	 * @param refresh true si faire un refresh
	 * @return tableau de RepartAssociation
	 */
	public static NSArray rechercherRepartAssociationsPourCriteres(EOEditingContext editingContext,EOQualifier qualifier,boolean refresh) {   
		return fetchAll(editingContext, qualifier);
	}
	/** Recherche toutes les associations d'un certain type valides pour la periode passee en parametre
	 * 
	 * @param editingContext
	 * @param nomAssociation libelle de l'association
	 * @param dateDebut peut etre nulle
	 * @param dateFin	peut etre nulle
	 * @return tableau de RepartAssociation
	 */
	public static NSArray rechercherRepartAssociationsPourAssociationEtPeriode(EOEditingContext editingContext,String nomAssociation,NSTimestamp dateDebut,NSTimestamp dateFin) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(
				EOQualifier.qualifierWithQualifierFormat(ASSOCIATION_KEY + "." + EOAssociation.LIBELLE_LONG_KEY + "=%@", new NSArray(nomAssociation)));
		if (dateDebut != null) {
			qualifiers.addObject(qualifierPourPeriode(DATE_DEBUT_KEY, dateDebut, DATE_FIN_KEY, dateFin));
		}
		return rechercherRepartAssociationsPourCriteres(editingContext,new EOAndQualifier(qualifiers),false);
	}
	// Méthodes privées
	private static EOQualifier qualifierPourPeriode(String champDateDebut,NSTimestamp debutPeriode,String champDateFin,NSTimestamp finPeriode) {
		if (debutPeriode == null) {
			return null;
		}
		String stringQualifier = "";
		NSMutableArray args = new NSMutableArray(debutPeriode);

		// champDateFin = nil or champDateFin >= debutPeriode
		stringQualifier = "(" + champDateFin + " = nil OR " + champDateFin + " >= %@)";
		if (finPeriode != null) {
			args.addObject(finPeriode);
			// champDateDebut = nil or champDateDebut <= finPeriode
			stringQualifier = stringQualifier + " AND (" + champDateDebut + " = nil OR " + champDateDebut + " <= %@)";
		}
		return EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
	}


}

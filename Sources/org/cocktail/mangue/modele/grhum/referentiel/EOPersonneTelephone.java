//EOPersonneTelephone.java
//Created on Tue Mar 18 13:36:50  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;

// 16/06/2010 - Rajout de constantes et recherche des numéros de fax et de mobile ajoutée
import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeNoTel;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeTel;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.application.client.tools.CocktailUtilities;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
public class EOPersonneTelephone extends _EOPersonneTelephone {

	public static final Integer INDICATIF_FRANCE = new Integer(33);

	public static final EOSortOrdering SORT_TYPE_NO_DESC = new EOSortOrdering(TYPE_NO_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray SORT_ARRAY_TYPE_NO_DESC = new NSArray(SORT_TYPE_NO_DESC);


	public static String[] OUTIL_TEL = {EOTypeNoTel.TYPE_FIXE,EOTypeNoTel.TYPE_FAX,EOTypeNoTel.TYPE_MOBILE};

	public EOPersonneTelephone() {
		super();
	}

	public static EOPersonneTelephone creer(EOEditingContext ec, EOIndividu individu) {

		EOPersonneTelephone newObject = (EOPersonneTelephone)CocktailUtilities.instanceForEntity(ec, EOPersonneTelephone.ENTITY_NAME);

		newObject.setPersId(individu.persId());

		// Tel PRO par defaut
		newObject.setToTypeNoRelationship(EOTypeNoTel.getDefault(ec));
		newObject.setToTypeTelRelationship(EOTypeTel.getDefault(ec));

		newObject.setTelPrincipal(CocktailConstantes.VRAI);
		newObject.setListeRouge(CocktailConstantes.FAUX);
		newObject.setIndicatif(INDICATIF_FRANCE);
		newObject.setDDebVal(new NSTimestamp());

		ec.insertObject(newObject);
		return newObject;
	}

	public String noTelephoneAffichage() {

		if (noTelephone() == null)
			return "";

		String numero = nettoyerTelephone(noTelephone());
		String tempo = "";
		int nbChiffres = 0;

		for (int i = 0;i < numero.length();i++)	{
			if ((nbChiffres == 2) && (tempo.length() < 14)) {
				tempo = tempo + ".";
				nbChiffres = 0;
			}
			if (StringCtrl.isBasicDigit(numero.charAt(i))) {
				tempo = tempo + numero.charAt(i);
				nbChiffres = nbChiffres + 1;
			}
		}

		return tempo;

	}

	public void initAvecIndividu(EOIndividu individu) {
		setDDebVal(new NSTimestamp());
		setListeRouge(CocktailConstantes.FAUX);
		setPersId(individu.persId());
		setIndicatif(INDICATIF_FRANCE);
	}
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {

		if (noTelephone() == null || noTelephone().length() == 0) {
			throw new NSValidation.ValidationException("Vous devez fournir le numero de telephone");
		}   	

		if (typeNo() == null || typeNo().length() == 0) {
			throw new NSValidation.ValidationException("Vous devez fournir l'outil");
		}   
		boolean typeExiste = false;
		for (int i = 0;i < OUTIL_TEL.length;i++) {
			if (typeNo().equals(OUTIL_TEL[i])) {
				typeExiste = true;
				break;
			}
		}
		if (!typeExiste) {
			throw new NSValidation.ValidationException("L'outil choisi est invalide");
		}   
		if (typeTel() == null || typeTel().length() == 0) {
			throw new NSValidation.ValidationException("Vous devez fournir le type");
		}   

		if (toTypeTel() == null) {
			throw new NSValidation.ValidationException("Veuillez sélectionner un type de téléphone !");
		}

		int myMin = 0, myMax = 0;
		try {
			String min = EOGrhumParametres.getValueParam("GRHUM_" + typeTel() + "_MIN");
			myMin = new Integer(min).intValue();
		} catch (Exception e) {
			throw new NSValidation.ValidationException("Mauvais paramétrage de GRHUM pour GRHUM_" + typeTel() + "_MIN");
		}
		try {
			String max = EOGrhumParametres.getValueParam("GRHUM_" + typeTel() + "_MAX");
			myMax = new Integer(max).intValue();
		} catch (Exception e) {
			throw new NSValidation.ValidationException("Mauvais paramétrage de GRHUM pour GRHUM_" + typeTel() + "_MAX");
		}	
		String tel = EOPersonneTelephone.nettoyerTelephone(noTelephone());
		// On ne formatte le numero de telephone que s'il s'agit d'un numero en france.
		if (indicatif() == null || indicatif().intValue() == INDICATIF_FRANCE) {
			if (tel.length() < myMin || tel.length() > myMax) {
				throw new NSValidation.ValidationException("Numéro de téléphone " + noTelephone() + " invalide.Il doit comporter de " + myMin + " à " + myMax + " chiffres");
			}
			formatterTelephone();
		}

		if (indicatif() != null)
			formatterIndicatif();

		if (!estTelephoneInterne() && indicatif() == null)
			throw new NSValidation.ValidationException("Veuillez renseigner l'indicatif pour un numéro non INTERNE !");

		if (noTelephone().length() > 20) {
			throw new NSValidation.ValidationException("Numéro de téléphone " + noTelephone() + " invalide.Il doit comporter de " + myMin + " à " + myMax + " chiffres");
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
	public boolean estTelPrincipal() {
		return telPrincipal() != null && telPrincipal().equals(CocktailConstantes.VRAI);
	}
	public void setEstTelPrincipal(boolean aBool) {
		if (aBool) {
			setTelPrincipal(CocktailConstantes.VRAI);
		} else {
			setTelPrincipal(CocktailConstantes.FAUX);
		}
	}

	public boolean estSurListeRouge() {
		return listeRouge() != null && listeRouge().equals(CocktailConstantes.VRAI);
	}
	public void setEstSurListeRouge(boolean aBool) {
		if (aBool) {
			setListeRouge(CocktailConstantes.VRAI);
		} else {
			setListeRouge(CocktailConstantes.FAUX);
		}
	}

	public void formatterIndicatif() {
		if (noTelephone() != null) {
			setIndicatif(new Integer(EOPersonneTelephone.nettoyerEtFormater(indicatif().toString())));
		}
	}
	public void formatterTelephone() {
		if (noTelephone() != null) {
			setNoTelephone(EOPersonneTelephone.nettoyerEtFormater(noTelephone()));
		}
	}

	public String typeTelephone() {
		if (typeTel() != null) {
			EOTypeTel type = EOTypeTel.rechercherTypeTelPourType(editingContext(), typeTel());
			if (type == null) {
				return null;
			} else {
				return type.libelleLong();
			}
		}
		return null;
	}

	public boolean estTelephoneInterne() {
		return typeTel().equals(EOTypeTel.TYPE_INTERNE);
	}

	// méthodes statiques
	/** supprime tous les caracteres non numeriques du numero */
	public static String nettoyerEtFormater(String unNumero) {
		return formatterTelephone(nettoyerTelephone(unNumero));
	}
	public static String nettoyerTelephone(String unNumero) {
		String tempo = "";
		for (int i = 0;i < unNumero.length();i++) 	{
			if (StringCtrl.isBasicDigit(unNumero.charAt(i)))
				tempo = tempo + unNumero.charAt(i);
		}
		return tempo;
	}

	//  FORMATTER TELEPHONE : On formate le numero en mettant des '.' entre chaque chiffre. Supprime les autres caracteres
	private static String formatterTelephone(String unNumero) {

		StringBuffer sb = new StringBuffer();
		String s = StringCtrl.normalize(unNumero);

		for (int i = 0; i < s.length(); i++) {
			if (StringCtrl.isBasicDigit(s.charAt(i))) {
				sb.append(s.charAt(i));
			}
		}
		return sb.toString();
	}

	// TEL PERSONNELS

	/**
	 * 
	 * @param edc
	 * @param individu
	 * @return
	 */
	public static NSArray<EOPersonneTelephone> rechercherTelPersonnels(EOEditingContext edc,EOIndividu individu) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();		
			qualifiers.addObject(getQualifierPersId(individu.persId()));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_TEL_KEY + "=%@", new NSArray(EOTypeTel.TYPE_NO_PRIVE)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_NO_KEY + "=%@", new NSArray(EOTypeNoTel.TYPE_FIXE)));

			return fetchAll(edc, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOPersonneTelephone>();
		}
	}
	/**
	 * 
	 * @param edc
	 * @param individu
	 * @return
	 */
	public static NSArray<EOPersonneTelephone> rechercherTelEtMobilePersonnels(EOEditingContext edc,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();		
			qualifiers.addObject(getQualifierPersId(individu.persId()));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_TEL_KEY + "=%@", new NSArray(EOTypeTel.TYPE_NO_PRIVE)));

			NSMutableArray orQualifiers = new NSMutableArray();		
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_NO_KEY + "=%@", new NSArray(EOTypeNoTel.TYPE_FIXE)));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_NO_KEY + "=%@", new NSArray(EOTypeNoTel.TYPE_MOBILE)));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));

			return fetchAll(edc, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOPersonneTelephone>();
		}
	}
	/**
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 */
	public static NSArray<EOPersonneTelephone> findForIndividu(EOEditingContext ec,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();		
			qualifiers.addObject(getQualifierPersId(individu.persId()));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_TEL_KEY+"."+EOTypeTel.CODE_KEY + " !=%@ ", new NSArray(EOTypeTel.TYPE_NO_ETUD)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_TYPE_TEL_KEY+"."+EOTypeTel.CODE_KEY + " !=%@ ", new NSArray(EOTypeTel.TYPE_NO_PAR)));

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_TYPE_NO_DESC);
		}
		catch (Exception e) {
			return new NSArray<EOPersonneTelephone>();
		}
	}

	/**
	 * 
	 * @param edc
	 * @param individu
	 * @return
	 */
	public static EOPersonneTelephone rechercheTelPrincipal(EOEditingContext edc,EOIndividu individu) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();		
			qualifiers.addObject(getQualifierPersId(individu.persId()));
			qualifiers.addObject(CocktailFinder.getQualifierEqual(TEL_PRINCIPAL_KEY, CocktailConstantes.VRAI));
			return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));
		}
		catch (Exception e){
			return null;
		}
	}

	// TEL PROFESSIONNELS
	public static NSArray<EOPersonneTelephone> rechercherTelProfessionnels(EOEditingContext editingContext,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();		
			qualifiers.addObject(getQualifierPersId(individu.persId()));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_TEL_KEY + "=%@", new NSArray(EOTypeTel.TYPE_NO_PRO)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_NO_KEY + "=%@", new NSArray(EOTypeNoTel.TYPE_FIXE)));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOPersonneTelephone>();
		}
	}
	public static NSArray<EOPersonneTelephone> rechercherTelEtMobileProfessionnels(EOEditingContext editingContext,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();		
			qualifiers.addObject(getQualifierPersId(individu.persId()));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_TEL_KEY + "=%@", new NSArray(EOTypeTel.TYPE_NO_PRO)));

			NSMutableArray orQualifiers = new NSMutableArray();		
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_NO_KEY + "=%@", new NSArray(EOTypeNoTel.TYPE_FIXE)));
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_NO_KEY + "=%@", new NSArray(EOTypeNoTel.TYPE_MOBILE)));
			qualifiers.addObject(new EOOrQualifier(orQualifiers));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOPersonneTelephone>();
		}
	}
	public static NSArray<EOPersonneTelephone> rechercherFaxProfessionnels(EOEditingContext editingContext,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(getQualifierPersId(individu.persId()));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_TEL_KEY + "=%@", new NSArray(EOTypeTel.TYPE_NO_PRO)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_NO_KEY + "=%@", new NSArray(EOTypeNoTel.TYPE_FAX)));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return new NSArray<EOPersonneTelephone>();
		}
	}
	
	public static EOQualifier getQualifierPersId(Integer persId) {
		return CocktailFinder.getQualifierEqual(PERS_ID_KEY, persId);
	}
}

//EOStructure.java
//Created on Wed Feb 26 13:48:26  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;


import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.EOTypeAdresse;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EOTypeGroupe;
import org.cocktail.mangue.common.modele.nomenclatures.structures.EOTypeStructure;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;
import org.cocktail.mangue.modele.sequences.EOSeqStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** Validation des donnees d'une structure.<BR>
 * Verifie la longueur des chaines de caracteres et la presence des champs obligatoires<BR>
 *  Verifie que le le type de structure est correct<BR>
 *  Verifie que la date d'ouverture n'est pas posterieure a la date de fermeture<BR>
 *  Verifie si le rne est fournit qu'il n'existe pas d'autres structures avec ce rne<BR>
 *  Verifie si c'est paramete dans Grhum que le siret est valide et qu'il n'existe pas d'autres structures avec ce siret<BR>
 *  Verifie la coherence des donnees selon le type de structure :
 *  structure pere obligatoire sauf pour les personnes morales et l'etablissement<BR>
 * La creation de structures ne fonctionne que pour les bases Oracle */
public class EOStructure extends _EOStructure implements RecordAvecLibelleEtCode {

	public static final NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(new EOSortOrdering(LL_STRUCTURE_KEY, EOSortOrdering.CompareAscending));
	private static String TYPE_GROUPE_SERVICE = "S";

	public EOStructure() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @param structurePere
	 * @param agent
	 * @param ajout
	 * @return
	 */
	public static EOStructure creer(EOEditingContext ec, EOStructure structurePere, EOAgentPersonnel agent, boolean ajout) {

		EOStructure nouvelleStructure = new EOStructure();
		if (ajout)
			ec.insertObject(nouvelleStructure);

		nouvelleStructure.setPersIdCreation(agent.toIndividu().persId());
		nouvelleStructure.setPersIdModification(agent.toIndividu().persId());

		nouvelleStructure.setToStructurePereRelationship(structurePere);
		nouvelleStructure.setCTypeStructure(ManGUEConstantes.C_TYPE_STRUCTURE_AUTRE);

		nouvelleStructure.init();

		return nouvelleStructure;
	}


	/**
	 * 
	 */
	public void init() {
		setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());
		setCStructure(SuperFinder.clePrimairePour(editingContext(), ENTITY_NAME, C_STRUCTURE_KEY, EOSeqStructure.ENTITY_NAME, true).toString());
		setPersId(SuperFinder.construirePersId(editingContext()));
		setTemCotisAssedic(CocktailConstantes.FAUX);
		setTemDads(CocktailConstantes.FAUX);
		setTemSectorise(CocktailConstantes.FAUX);
		setTemSoumisTva(CocktailConstantes.FAUX);
		setTemValide(CocktailConstantes.VRAI);
		setGrpAcces("+");	
	}

	/** Ajoute le parent comme structure pere, duplique l'adresse principale du parent et
	 * cree la repartPersonneAdresse principale et les secretariats 
	 * en utilisant ceux du parent */
	public void initAvecParent(EOStructure structurePere) {
		init();
		if (structurePere != null) {	
			setCStructurePere(structurePere.cStructure());
			setToStructurePereRelationship(structurePere);
		}
	}
	/** Cree la rpa principale + duplique l'adresse du pere si elle n'existe pas et l'insere dans l'editing context.
	 * Retourne true si elle est creee */
	public boolean preparerRepartPersonneAdresse() {
		EORepartPersonneAdresse repartPrincipale = EORepartPersonneAdresse.adressePrincipale(editingContext(), this.persId());
		if (repartPrincipale == null) {
			// Créer la repartAdresse en se basant sur celle du père
			repartPrincipale = EORepartPersonneAdresse.adressePrincipale(editingContext(), toStructurePere().persId());
			if (repartPrincipale != null)	{
				EOAdresse adresse = new EOAdresse();
				adresse.takeValuesFromDictionary(repartPrincipale.toAdresse().snapshot());
				// créer la clé primaire
				adresse.setAdrOrdre(new Integer(EOAdresse.construireAdresseOrdre(editingContext()).intValue()));
				editingContext().insertObject(adresse);
				EORepartPersonneAdresse newRepart = new EORepartPersonneAdresse();
				newRepart.initAvecIdEtRepartAdresse(persId(),repartPrincipale);
				newRepart.setAdrOrdre(adresse.adrOrdre());
				newRepart.setToAdresseRelationship(adresse);
				editingContext().insertObject(newRepart);

				return true;
			} else {
				return false;	// Pas d'adresse principale pour le père
			}
		} else {
			return false;	// L'adresse principale existe déjà
		}
	}

	/**
	 * Prepare les repartTypeGroupes associes aux services et aux groupes si necessaire. Retourne true si crees
	 * @return
	 */
	public boolean preparerRepartTypeGroupes() {

		// RepartTypeGroupe : S(Service) et G(roupe annuaire) sont à créer pour les structures autres que les personnes morales
		if (cTypeStructure() != null && cTypeStructure().equals(ManGUEConstantes.C_TYPE_STRUCTURE_AUTRE) == false) {
			// Vérifier si il existe des repart type groupes pour cette structure de type groupe et service
			boolean creerGroupe = true, creerService = true;
			NSArray<EORepartTypeGroupe> repartsTg = EORepartTypeGroupe.findForStructure(editingContext(), this);
			if (repartsTg != null && repartsTg.size() > 0) {
				NSArray<String> types = (NSArray<String>)repartsTg.valueForKey(EORepartTypeGroupe.TGRP_CODE_KEY);
				creerGroupe = types.containsObject(EOTypeGroupe.TYPE_GROUPE_GROUPE) == false;
				creerService = types.containsObject(EOTypeGroupe.TYPE_GROUPE_SERVICE) == false;
			}
			if (creerGroupe) {
				EORepartTypeGroupe typeGroupe = new EORepartTypeGroupe();
				typeGroupe.initAvecStructureEtType(this,EOTypeGroupe.getTypeGroupeGroupe(editingContext()));
				editingContext().insertObject(typeGroupe);
			}

			if (creerService) {
				EORepartTypeGroupe typeGroupe = new EORepartTypeGroupe();
				typeGroupe.initAvecStructureEtType(this,EOTypeGroupe.getTypeGroupeService(editingContext()));
				editingContext().insertObject(typeGroupe);
			}
			return creerGroupe || creerService;	// true si un des deux créés
		} else {
			return false;
		}
	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (llStructure() == null || llStructure().length() == 0) {
			throw new NSValidation.ValidationException("Vous devez fournir le libellé long");
		} else if (llStructure().length() > 200) {
			throw new NSValidation.ValidationException("Le libellé long comporte au plus 200 caractères");
		}
		if (lcStructure() != null && lcStructure().length() > 30) {
			throw new NSValidation.ValidationException("Le libellé court comporte au plus 30 caractères");
		}
		if (cTypeStructure() == null || cTypeStructure().length() == 0) {
			throw new NSValidation.ValidationException("Le type de structure est obligatoire");
		} else if (cTypeStructure() != null && cTypeStructure().length() > 2) {
			throw new NSValidation.ValidationException("Le type de structure comporte au plus 2 caractères");
		} else if (NomenclatureFinder.findForCode(editingContext(), EOTypeStructure.ENTITY_NAME, cTypeStructure()) == null) {
			throw new NSValidation.ValidationException("Type structure invalide");
		} else if (cTypeStructure().equals("A") == false && cTypeStructure().equals("E") == false) {
			if (cStructurePere() == null) {
				throw new NSValidation.ValidationException("Une structure de l'établissement doit avoir une structure parent");
			}
			if (owner() == null) {
				throw new NSValidation.ValidationException("Pour une structure de l'établissement, le propriétaire doit être fourni");
			}
			if (responsable() == null) {
				throw new NSValidation.ValidationException("Pour une structure de l'établissement, le responsable doit être fourni");
			}
		}
		if (grpAcces() != null) {
			if (grpAcces().length() > 1) {
				throw new NSValidation.ValidationException("L'accès ne comporte qu'un caractère");
			} else if (grpAcces().equals("+") == false && grpAcces().equals("-") == false) {
				throw new NSValidation.ValidationException("L'accès ne prend que les valeurs + ou -");
			}
		}
		// Vérifier que le rne n'est pas utilisé pour une autre structure
		if (rne() != null) {
			// Vérifier qu'il n'y a pas d'autres structures avec le même numéro de Rne
			NSArray structures = EOStructure.rechercherStructuresAvecRne(editingContext(),rne().code());
			if (structures.count() > 1 || (structures.count() == 1 && structures.objectAtIndex(0) != this)) {
				throw new NSValidation.ValidationException("Une autre structure a déjà ce rne");
			}
		}
		// Vérifier le n° de Siret si c'est un paramétrage de Grhum
		if (siret() != null) {
			if (siret().length() > 14) {
				throw new NSValidation.ValidationException("Le n° de siret comporte au plus 14 caractères");
			}

			if (EOGrhumParametres.findParametre(editingContext(), ManGUEConstantes.GRHUM_PARAM_KEY_CONTROLE_SIRET).isParametreVrai()) {
				if (estSiretValide(siret()) == false) {
					throw new NSValidation.ValidationException( "Le numéro de Siret est invalide !");
				}
			}
			// Vérifier qu'il n'y ait pas d'autres structures avec le même numéro de Siret
			NSArray<EOStructure> structures = EOStructure.rechercherStructuresAvecSiret(editingContext(),siret());
			if (structures.count() > 1 || (structures.count() == 1 && structures.get(0) != this)) {
				throw new NSValidation.ValidationException("Une autre structure a déjà ce numéro de siret.\n" + structures.get(0).llStructure());
			}
		}
		if (dateOuverture() != null && dateFermeture() != null && DateCtrl.isAfterEq(dateOuverture(), dateFermeture())) {
			throw new NSValidation.ValidationException("La date d'ouverture ne peut être postérieure à la date de fermeture");
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
	/** Interdit la suppression pour les structures qui ne sont pas des feuilles */
	public void validateForDelete() throws NSValidation.ValidationException {
		if (structuresFils().count() > 0){
			throw new NSValidation.ValidationException("On ne peut supprimer que des structures dans fils");
		}
		super.validateForDelete();
	}
	public String dateDebut() {
		return SuperFinder.dateFormatee(this,DATE_OUVERTURE_KEY);
	}
	public void setDateDebut(String uneDate) {
		if (uneDate == null) {
			setDateOuverture(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_OUVERTURE_KEY,uneDate);
		}
	}
	public String dateFin() {
		return SuperFinder.dateFormatee(this,DATE_FERMETURE_KEY);
	}
	public void setDateFin(String uneDate) {
		if (uneDate == null) {
			setDateFermeture(null);
		} else {
			SuperFinder.setDateFormatee(this,DATE_FERMETURE_KEY,uneDate);
		}
	}

	/**
	 * la structure est-elle une composante de l universite ?
	 * @return
	 */
	public boolean isService() {
		return ((NSArray) valueForKeyPath(TO_REPART_TYPE_GROUPE_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY)).containsObject("S");
	}



	/**
	 * la structure est-elle l'etablissement ?
	 * @return
	 */
	public boolean estEtablissement() {
		return (cTypeStructure().equals("E") || cTypeStructure().equals("ES"));
	}

	/**
	 * la structure est-elle une composante de l'universite ?
	 * @return
	 */
	public boolean estComposante() {
		return "C".equals(cTypeStructure()) || "E".equals(cTypeStructure()) || "ES".equals(cTypeStructure()) || "CS".equals(cTypeStructure());
	}


	/**
	 * la structure est-elle l'etablissement principal ?
	 * @return
	 */
	public boolean estEtablissementPrincipal() {
		return cTypeStructure().equals("E");
	}
	/**
	 * trouver la composante dont depend un service
	 * @return
	 */
	public EOStructure toComposante() {
		EOStructure composante = null;
		if (estComposante()) {
			composante = (EOStructure) this;
		} else {
			composante = toStructurePere().toComposante();
		}
		return composante;
	}

	// méthodes ajoutées
	public String toString() {
		return llStructure();
	}


	public NSArray<EOStructure> structuresFils() {

		NSMutableArray qualifiers = new NSMutableArray();		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_STRUCTURE_KEY + "!=%@", new NSArray(cStructure())));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_STRUCTURE_PERE_KEY + "=%@", new NSArray(this)));

		qualifiers.addObject(qualifierTypeService());

		return fetchAll(editingContext(), new EOAndQualifier(qualifiers));		
	}

	/**
	 * 
	 * @return
	 */
	public NSArray<EOStructure> toutesStructuresFils() {
		
		NSMutableArray tousFils = new NSMutableArray();
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_STRUCTURE_KEY + "!=%@", new NSArray(cStructure())));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_STRUCTURE_PERE_KEY + "=%@", new NSArray(this)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_REPART_TYPE_GROUPE_KEY+"."+EORepartTypeGroupe.TGRP_CODE_KEY + "=%@", new NSArray(EOTypeGroupe.TYPE_GROUPE_SERVICE)));
		
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(qualifiers), null);
		NSArray<EOStructure> fils = editingContext().objectsWithFetchSpecification(fs);
		for (EOStructure structure : fils) {
			tousFils.addObject(structure);
			tousFils.addObjectsFromArray(structure.toutesStructuresFils());
		}
		
		return tousFils;

	}
	public String stringAdresseProfessionnelle() {

		EOAdresse adresse = adresseProfessionnelle();
		if (adresse == null) {
			adresse = adresseFacturation();			
			if (adresse == null)
				return "";
		}

		String retour = "";

		if (adresse.adrAdresse1() != null)
			retour = retour.concat(adresse.adrAdresse1() + " ");

		if (adresse.adrAdresse2() != null)
			retour = retour.concat(adresse.adrAdresse2() + " ");

		if (adresse.codePostal() != null)
			retour = retour.concat(adresse.codePostal() + " ");

		if (adresse.ville() != null)
			retour = retour.concat(adresse.ville());

		return retour;
	}


	/** retourne l'adresse valide principale d'une structure de type 'PRO', null si il n'y en a pas */
	public EOAdresse adresseProfessionnelle() {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.PERS_ID_KEY + "=%@", new NSArray(persId())));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_PRINCIPAL_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY + "=%@", new NSArray(EOTypeAdresse.TYPE_PROFESSIONNELLE)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			return (EORepartPersonneAdresse.fetchFirstByQualifier(editingContext(), new EOAndQualifier(qualifiers))).toAdresse();
		}
		catch (Exception e) {
			return null;
		}
	}
	/** retourne l'adresse valide principale d'une structure de type 'FACT', null si il n'y en a pas */
	public EOAdresse adresseFacturation() {

		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.PERS_ID_KEY + "=%@", new NSArray(persId())));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_PRINCIPAL_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.TADR_CODE_KEY + "=%@", new NSArray(EOTypeAdresse.TYPE_FACTURATION)));

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.RPA_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

			return (EORepartPersonneAdresse.fetchFirstByQualifier(editingContext(), new EOAndQualifier(qualifiers))).toAdresse();
		}
		catch (Exception e) {
			return null;
		}
	}


	public int niveauStructure() {
		if (cTypeStructure().equals("E")) {
			return 1;
		} else {
			EOStructure etablissement = rechercherEtablissement(editingContext());
			if (etablissement != null) {
				if (cStructurePere() != null && estComposante()) {
					if (cStructurePere().equals(etablissement.cStructure())) {
						return 2;
					}  else {
						return 3;
					}
				} else {
					return 0;
				}
			} else {
				return 0;
			}
		}
	}
	/** Retourne l'etablissement associe a cette structure */
	public EOStructure etablissement() {
		if (estEtablissement())
			return this;
		else {
			if (cStructure().equals(cStructurePere()))
				return null;

			return toStructurePere().etablissement();
		}
	}
	/** Retourne l'etablissement principal associe a cette structure */
	public EOStructure etablissementPrincipal() {
		if (estEtablissementPrincipal()) {
			return this;
		} else {
			if (cStructure().equals(cStructurePere())) {
				return null;
			}
			return toStructurePere().etablissementPrincipal();
		}
	}
	/** Retourne le siret de l'etablissement principal */
	public String siretEtablissementPrincipal() {
		return etablissementPrincipal().siret();
	}
	// interface RecordAvecLibelleEtCode
	public String code() {
		return lcStructure();
	}
	public String libelle() {
		return llStructure();
	}

	/**
	 * Libelle Complet d'une structure. Chemin complet jusqu'à la composante, voire l'établissement.
	 * @return
	 */
	public String libelleComplet() {

		EOStructure currentStructure = this;
		NSMutableArray composants = new NSMutableArray();
		composants.addObject(currentStructure);

		while (!currentStructure.estEtablissementPrincipal() && !currentStructure.estComposante()) {

			if (currentStructure.toStructurePere() == null) {
				return "PAS DE STRUCTURE PERE !!!!!";
			}

			currentStructure = currentStructure.toStructurePere();
			composants.addObject(currentStructure);
		}

		String libelle = ((EOStructure)composants.objectAtIndex(composants.count()-1)).lcStructure();
		for (int i=(composants.count()-2);i>=0;i--) {
			EOStructure structure = (EOStructure)composants.objectAtIndex(i);
			libelle = libelle + " / " + StringCtrl.capitalizedString(structure.llStructure());			
		}

		return libelle;
	}

	// METHODES STATIQUES

	public static NSArray<EOStructure> rechercherStructuresAvecLibelle(EOEditingContext editingContext, String libelle, String pere, EOTypeGroupe typeGroupe) {

		NSMutableArray qualifiers = new NSMutableArray();		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI)));

		if (libelle != null && libelle.length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LL_STRUCTURE_KEY + " caseInsensitiveLike %@", new NSArray("*"+libelle+"*")));

		if (pere != null && pere.length() > 0)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_STRUCTURE_PERE_KEY+"."+ LL_STRUCTURE_KEY + " caseInsensitiveLike %@", new NSArray("*"+pere+"*")));

		if (typeGroupe != null)		
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_REPART_TYPE_GROUPE_KEY+"."+EORepartTypeGroupe.TGRP_CODE_KEY + " = %@", new NSArray(typeGroupe.code())));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers), SORT_ARRAY_LIBELLE_ASC);

	}

	/** Retourne les structures liees a un type groupe. Pour le type groupe "EN", on restreindra aux structures
	 * avec un libelle long, le type structure "A" */
	public static NSArray<EOStructure> findForTypeGroupe(EOEditingContext editingContext,String typeCode,EOQualifier qualifierSupplementaire) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_REPART_TYPE_GROUPE_KEY+"."+EORepartTypeGroupe.TGRP_CODE_KEY + "=%@",new NSArray(typeCode)));

		if (typeCode.equals(EOTypeGroupe.TYPE_GROUPE_ENTREPRISE)) {

			String codeStructurePere = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_EMPLOYEURS);

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_STRUCTURE_PERE_KEY + "=%@",new NSArray(codeStructurePere)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LL_STRUCTURE_KEY + "!= nil",null));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_REPART_TYPE_GROUPE_KEY+"."+EORepartTypeGroupe.TGRP_CODE_KEY + "=%@",new NSArray(EOTypeGroupe.TYPE_GROUPE_ENTREPRISE)));
		}

		if (qualifierSupplementaire != null) {
			qualifiers.addObject(qualifierSupplementaire);
		}

		return fetchAll(editingContext, new EOAndQualifier(qualifiers));
	}

	/**
	 * 
	 * @param editingContext
	 * @param libelle
	 * @param type
	 * @return
	 */
	public static EOStructure rechercherStructureAvecLibelleEtType(EOEditingContext editingContext, String libelle,String type) {

		NSMutableArray qualifiers = new NSMutableArray();		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LL_STRUCTURE_KEY + "=%@", new NSArray(libelle.toUpperCase())));
		
		if (type != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_STRUCTURE_KEY + "=%@", new NSArray(type)));
		}

		return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EOStructure rechercherEtablissement(EOEditingContext editingContext) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(C_TYPE_STRUCTURE_KEY + " = %@",new NSArray("E"));		
		return fetchFirstByQualifier(editingContext, qual);
	}

	/**
	 * 
	 * @param editingContext
	 * @param key
	 * @return
	 */
	public static EOStructure findForKey(EOEditingContext editingContext, String key) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(C_STRUCTURE_KEY + " = %@",new NSArray(key));		
		return fetchFirstByQualifier(editingContext, qual);
	}

	/**
	 * 
	 * @param editingContext
	 * @param key
	 * @return
	 */
	public static EOStructure findForPersId(EOEditingContext editingContext, Integer persId) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = %@",new NSArray(persId));		
		return fetchFirstByQualifier(editingContext, qual);
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EOStructure rechercherEtablissementPrincipal(EOEditingContext edc) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();		

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_STRUCTURE_KEY + "=%@", new NSArray(ManGUEConstantes.C_TYPE_STRUCTURE_ETABLISSEMENT)));

		return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));		
	}


	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static NSArray rechercherEtablissements(EOEditingContext editingContext) {
		NSMutableArray qualifiers = new NSMutableArray();		

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));

		NSMutableArray orQualifiers = new NSMutableArray();		

		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_STRUCTURE_KEY + "=%@", new NSArray(ManGUEConstantes.C_TYPE_STRUCTURE_ETABLISSEMENT)));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_STRUCTURE_KEY + "=%@", new NSArray(ManGUEConstantes.C_TYPE_STRUCTURE_ETAB_SECONDAIRE)));

		qualifiers.addObject(new EOOrQualifier(orQualifiers));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers));		
	}


	/**
	 * 
	 * Liste des services de  l etablissement tries par ordre croissant
	 * 
	 * @param editingContext
	 * @return
	 */
	public static NSArray<EOStructure> rechercherStructuresEtablissements(EOEditingContext edc) {

		NSMutableArray qualifiers = new NSMutableArray();		
		NSMutableArray orQualifiers = new NSMutableArray();		

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FERMETURE_KEY + " = nil or " + DATE_FERMETURE_KEY +  " >= %@", new NSArray(DateCtrl.today())));

		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_STRUCTURE_KEY + "=%@", new NSArray(ManGUEConstantes.C_TYPE_STRUCTURE_ETABLISSEMENT)));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_TYPE_STRUCTURE_KEY + "=%@", new NSArray(ManGUEConstantes.C_TYPE_STRUCTURE_ETAB_SECONDAIRE)));
		orQualifiers.addObject(qualifierTypeService());

		qualifiers.addObject(new EOOrQualifier(orQualifiers));

		return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_LIBELLE_ASC, true);		
	}

	/**
	 * 
	 * @param editingContext
	 * @param rne
	 * @return
	 */
	public static NSArray rechercherStructuresAvecRne(EOEditingContext editingContext,String rne) {
		NSMutableArray qualifiers = new NSMutableArray();		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_RNE_KEY + "=%@", new NSArray(rne)));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers));		
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param siret
	 * @return
	 */
	public static NSArray rechercherStructuresAvecSiret(EOEditingContext editingContext,String siret) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SIRET_KEY + "=%@", new NSArray(siret)));
		return fetchAll(editingContext, new EOAndQualifier(qualifiers));		
	}

	public static NSArray rechercherPourCreation(EOEditingContext ec,String libelle,String siret)	{

		try {			
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY+ " = %@",new NSArray(CocktailConstantes.VRAI)));

			if (libelle != null) {
				NSMutableArray orQualifiers = new NSMutableArray();
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LL_STRUCTURE_KEY+ " caseInsensitiveLike %@",new NSArray("*"+libelle+"*")));
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LC_STRUCTURE_KEY+ " caseInsensitiveLike %@",new NSArray("*"+libelle+"*")));

				qualifiers.addObject(new EOOrQualifier(orQualifiers));
			}

			if (siret != null && siret.length() > 0)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(SIRET_KEY+ " caseInsensitiveLike %@",new NSArray("*"+siret+"*")));

			return fetchAll(ec,new EOAndQualifier(qualifiers));
		}
		catch (Exception e)	{
			return null;
		}
	}


	// GET COMPOSANTE : Recupere le libelle de la composante associee a la structure
	public static EORne getUAIEtablissement(EOEditingContext ec, EOStructure structure) {

		try {

			if (structure == null)
				return null;

			if ( structure.cTypeStructure().equals("E") || structure.cTypeStructure().equals("ES")) {
				if (structure.rne() != null)
					return structure.rne();
			}

			return getUAIEtablissement(ec, structure.toStructurePere());
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// GET COMPOSANTE : Recupere le libelle de la composante associee a la structure
	public static EOStructure getComposante(EOEditingContext ec, String cStructure) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(C_STRUCTURE_KEY + " = '" + cStructure +"'",null);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qual,null);
		NSArray structures = ec.objectsWithFetchSpecification(fs);		

		EOStructure structure = (EOStructure)structures.objectAtIndex(0);

		// 07/01/04 : ajout de cTypeStructure = "CS" et de cTypeStructure = "ES"
		if ( (structure.cTypeStructure().equals("C")) || (structure.cTypeStructure().equals("E"))  
				|| (structure.cTypeStructure().equals("CS")) || (structure.cTypeStructure().equals("ES")) )
			return structure;

		return getComposante(ec, structure.toStructurePere().cStructure());
	}

	/** retourne le libelle des structures considerees comme etablissement */
	public static NSArray<EOStructure> libelleCourtEtablissements(EOEditingContext editingContext) {
		NSMutableArray libelles = (NSMutableArray)rechercherEtablissements(editingContext).valueForKey(LC_STRUCTURE_KEY);
		libelles.insertObjectAtIndex(new String("*"),0);
		return (NSArray)libelles;
	}

	/** retourne le code rne des structures considerees comme etablissement */
	public static NSArray<String> rneEtablissements(EOEditingContext editingContext) {

		NSArray<EOStructure> etablissements = rechercherEtablissements(editingContext);
		NSMutableArray rnes = new NSMutableArray();	
		for (EOStructure etab : etablissements) {
			if (etab.rne() != null)
				rnes.addObject(etab.rne().code());
		}		
		return rnes.immutableClone();

	}

	/** retourne le code rne des structures de l'etablissement */
	public static NSArray<EOStructure> rneStructuresEtablissements(EOEditingContext editingContext) {
		NSMutableArray rnes = (NSMutableArray)rechercherStructuresEtablissements(editingContext).valueForKey(EOStructure.RNE_KEY+"."+EORne.CODE_KEY);
		return rnes.immutableClone();
	}
	/** retourne la structure liee a un code structure */
	public static EOStructure structurePourCode(EOEditingContext editingContext,String cStructure) {
		NSMutableArray qualifiers = new NSMutableArray();		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_STRUCTURE_KEY+"=%@",new NSArray(cStructure)));
		return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
	}



	// Méthodes privées
	public static boolean estSiretValide(String siret) {
		try {
			new Long(siret);
		} catch (Exception e) {
			return false;	// Ne contient pas que des caractères numériques
		}
		// completion à droite par des 0 pour avoir un numero SIRET de 14 caractères
		siret = StringCtrl.stringCompletion(siret, 14, "0", "D");

		int somme = 0;
		for (int i = 0;i < siret.length(); i++) {
			int nombre = new Integer(siret.substring(i,i + 1)).intValue();
			// pour les chiffres impairs, on multiplie par 2. Comme on commence à zéro pour le premier caractère, on regarde le modulo 2
			if (i % 2 == 0) {
				try {
					nombre = nombre * 2;
					// si le nombre est supérieur à 9 on retranche 9 
					if (nombre > 9) {
						nombre = nombre - 9;
					}
				} catch (Exception e) {
					return false;
				}
			} 
			somme += nombre;
		}
		if (somme % 10 == 0) {
			return true;
		} else {
			return false;
		}

	}
	
	/**
	 * @param edc : editing context
	 * @return la liste des structures de type service avec un RNE renseigné
	 */
	public static NSArray<EOStructure> rechercherListeServicesAvecRneNonNull(EOEditingContext edc) {
		NSMutableArray qualifiers = new NSMutableArray(qualifierTypeService());
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_RNE_KEY + "!= nil", null));
		try {
			return fetchAll(edc, new EOAndQualifier(qualifiers));	
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOStructure>();
		}
	}
	
	/**
	 * @return qualifier pour le type de groupe Service
	 */
	public static EOQualifier qualifierTypeService() {
		return EOQualifier.qualifierWithQualifierFormat(TO_REPART_TYPE_GROUPE_KEY + "." + EORepartTypeGroupe.TGRP_CODE_KEY + "=%@", 
				new NSArray(TYPE_GROUPE_SERVICE));
	}
}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIndividuIdentite.java instead.
package org.cocktail.mangue.modele.grhum.referentiel;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOIndividuIdentite extends  EOGenericRecord {
	public static final String ENTITY_NAME = "IndividuIdentite";
	public static final String ENTITY_TABLE_NAME = "GRHUM.INDIVIDU_ULR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noIndividu";

	public static final String IDENTITE_KEY = "identite";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_AFFICHAGE_KEY = "nomAffichage";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PRENOM_KEY = "prenom";
	public static final String PRENOM_AFFICHAGE_KEY = "prenomAffichage";

// Attributs non visibles
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String TEM_VALIDE_KEY = "temValide";

//Colonnes dans la base de donnees
	public static final String IDENTITE_COLKEY = "$attribute.columnName";
	public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NOM_AFFICHAGE_COLKEY = "NOM_AFFICHAGE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String PRENOM_AFFICHAGE_COLKEY = "PRENOM_AFFICHAGE";

	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";


	// Relationships
	public static final String PERSONNELS_KEY = "personnels";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String identite() {
    return (String) storedValueForKey(IDENTITE_KEY);
  }

  public void setIdentite(String value) {
    takeStoredValueForKey(value, IDENTITE_KEY);
  }

  public String indNoInsee() {
    return (String) storedValueForKey(IND_NO_INSEE_KEY);
  }

  public void setIndNoInsee(String value) {
    takeStoredValueForKey(value, IND_NO_INSEE_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public String nomAffichage() {
    return (String) storedValueForKey(NOM_AFFICHAGE_KEY);
  }

  public void setNomAffichage(String value) {
    takeStoredValueForKey(value, NOM_AFFICHAGE_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public String prenomAffichage() {
    return (String) storedValueForKey(PRENOM_AFFICHAGE_KEY);
  }

  public void setPrenomAffichage(String value) {
    takeStoredValueForKey(value, PRENOM_AFFICHAGE_KEY);
  }

  public NSArray personnels() {
    return (NSArray)storedValueForKey(PERSONNELS_KEY);
  }

  public NSArray personnels(EOQualifier qualifier) {
    return personnels(qualifier, null, false);
  }

  public NSArray personnels(EOQualifier qualifier, boolean fetch) {
    return personnels(qualifier, null, fetch);
  }

  public NSArray personnels(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel.IDENTITE_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = personnels();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersonnelsRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSONNELS_KEY);
  }

  public void removeFromPersonnelsRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNELS_KEY);
  }

  public org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel createPersonnelsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Personnel");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSONNELS_KEY);
    return (org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel) eo;
  }

  public void deletePersonnelsRelationship(org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSONNELS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersonnelsRelationships() {
    Enumeration objects = personnels().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersonnelsRelationship((org.cocktail.mangue.modele.grhum.referentiel.EOPersonnel)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOIndividuIdentite avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOIndividuIdentite createEOIndividuIdentite(EOEditingContext editingContext, Integer noIndividu
			) {
    EOIndividuIdentite eo = (EOIndividuIdentite) createAndInsertInstance(editingContext, _EOIndividuIdentite.ENTITY_NAME);    
		eo.setNoIndividu(noIndividu);
    return eo;
  }

  
	  public EOIndividuIdentite localInstanceIn(EOEditingContext editingContext) {
	  		return (EOIndividuIdentite)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividuIdentite creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividuIdentite creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOIndividuIdentite object = (EOIndividuIdentite)createAndInsertInstance(editingContext, _EOIndividuIdentite.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOIndividuIdentite localInstanceIn(EOEditingContext editingContext, EOIndividuIdentite eo) {
    EOIndividuIdentite localInstance = (eo == null) ? null : (EOIndividuIdentite)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOIndividuIdentite#localInstanceIn a la place.
   */
	public static EOIndividuIdentite localInstanceOf(EOEditingContext editingContext, EOIndividuIdentite eo) {
		return EOIndividuIdentite.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOIndividuIdentite fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOIndividuIdentite fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOIndividuIdentite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOIndividuIdentite)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOIndividuIdentite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndividuIdentite fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndividuIdentite eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndividuIdentite)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOIndividuIdentite fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOIndividuIdentite eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOIndividuIdentite ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOIndividuIdentite fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

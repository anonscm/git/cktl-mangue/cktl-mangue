// _EOIndividuSimple.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIndividuSimple.java instead.
package org.cocktail.mangue.modele.grhum.referentiel;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOIndividuSimple extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;


	public static final String ENTITY_NAME = "IndividuSimple";
	public static final String ENTITY_TABLE_NAME = "GRHUM.INDIVIDU_ULR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noIndividu";

	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String IND_NO_INSEE_PROV_KEY = "indNoInseeProv";
	public static final String IND_QUALITE_KEY = "indQualite";
	public static final String LISTE_ROUGE_KEY = "listeRouge";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PERS_ID_KEY = "persId";
	public static final String PRENOM_KEY = "prenom";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

	// Attributs non visibles

	//Colonnes dans la base de donnees
	public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
	public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
	public static final String IND_QUALITE_COLKEY = "IND_QUALITE";
	public static final String LISTE_ROUGE_COLKEY = "LISTE_ROUGE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";

	// Relationships



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
	public String cCivilite() {
		return (String) storedValueForKey(C_CIVILITE_KEY);
	}

	public void setCCivilite(String value) {
		takeStoredValueForKey(value, C_CIVILITE_KEY);
	}

	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public NSTimestamp dNaissance() {
		return (NSTimestamp) storedValueForKey(D_NAISSANCE_KEY);
	}

	public void setDNaissance(NSTimestamp value) {
		takeStoredValueForKey(value, D_NAISSANCE_KEY);
	}

	public String indNoInsee() {
		return (String) storedValueForKey(IND_NO_INSEE_KEY);
	}
	public String indNoInseeProv() {
		return (String) storedValueForKey(IND_NO_INSEE_PROV_KEY);
	}

	public void setIndNoInsee(String value) {
		takeStoredValueForKey(value, IND_NO_INSEE_KEY);
	}

	public String indQualite() {
		return (String) storedValueForKey(IND_QUALITE_KEY);
	}

	public void setIndQualite(String value) {
		takeStoredValueForKey(value, IND_QUALITE_KEY);
	}

	public String listeRouge() {
		return (String) storedValueForKey(LISTE_ROUGE_KEY);
	}

	public void setListeRouge(String value) {
		takeStoredValueForKey(value, LISTE_ROUGE_KEY);
	}

	public Integer noIndividu() {
		return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
	}

	public void setNoIndividu(Integer value) {
		takeStoredValueForKey(value, NO_INDIVIDU_KEY);
	}

	public String nomPatronymique() {
		return (String) storedValueForKey(NOM_PATRONYMIQUE_KEY);
	}

	public void setNomPatronymique(String value) {
		takeStoredValueForKey(value, NOM_PATRONYMIQUE_KEY);
	}

	public String nomUsuel() {
		return (String) storedValueForKey(NOM_USUEL_KEY);
	}

	public void setNomUsuel(String value) {
		takeStoredValueForKey(value, NOM_USUEL_KEY);
	}

	public Integer persId() {
		return (Integer) storedValueForKey(PERS_ID_KEY);
	}

	public void setPersId(Integer value) {
		takeStoredValueForKey(value, PERS_ID_KEY);
	}
	public Integer persIdCreation() {
		return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
	}

	public void setPersIdCreation(Integer value) {
		takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
	}

	public Integer persIdModification() {
		return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
	}

	public void setPersIdModification(Integer value) {
		takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
	}

	public String prenom() {
		return (String) storedValueForKey(PRENOM_KEY);
	}

	public void setPrenom(String value) {
		takeStoredValueForKey(value, PRENOM_KEY);
	}

	public String temValide() {
		return (String) storedValueForKey(TEM_VALIDE_KEY);
	}

	public void setTemValide(String value) {
		takeStoredValueForKey(value, TEM_VALIDE_KEY);
	}


	/**
	 * Créer une instance de EOIndividuSimple avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOIndividuSimple createEOIndividuSimple(EOEditingContext editingContext, String cCivilite
			, NSTimestamp dCreation
			, NSTimestamp dModification
			, String listeRouge
			, Integer noIndividu
			, String nomUsuel
			, Integer persId
			, String prenom
			, String temValide
	) {
		EOIndividuSimple eo = (EOIndividuSimple) createAndInsertInstance(editingContext, _EOIndividuSimple.ENTITY_NAME);    
		eo.setCCivilite(cCivilite);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setListeRouge(listeRouge);
		eo.setNoIndividu(noIndividu);
		eo.setNomUsuel(nomUsuel);
		eo.setPersId(persId);
		eo.setPrenom(prenom);
		eo.setTemValide(temValide);
		return eo;
	}


	public EOIndividuSimple localInstanceIn(EOEditingContext editingContext) {
		return (EOIndividuSimple)localInstanceOfObject(editingContext, this);
	}


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOIndividuSimple creerInstance(EOEditingContext editingContext) {
		return creerInstance(editingContext, null);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOIndividuSimple creerInstance(EOEditingContext editingContext, NSArray specificites) {
		EOIndividuSimple object = (EOIndividuSimple)createAndInsertInstance(editingContext, _EOIndividuSimple.ENTITY_NAME, specificites);
		return object;
	}



	public static EOIndividuSimple localInstanceIn(EOEditingContext editingContext, EOIndividuSimple eo) {
		EOIndividuSimple localInstance = (eo == null) ? null : (EOIndividuSimple)localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOIndividuSimple#localInstanceIn a la place.
	 */
	public static EOIndividuSimple localInstanceOf(EOEditingContext editingContext, EOIndividuSimple eo) {
		return EOIndividuSimple.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	public static NSArray fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier)null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOIndividuSimple fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOIndividuSimple fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOIndividuSimple eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOIndividuSimple)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}




	public static EOIndividuSimple fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOIndividuSimple fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOIndividuSimple eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOIndividuSimple)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOIndividuSimple fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOIndividuSimple eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOIndividuSimple ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	


	public static EOIndividuSimple fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}

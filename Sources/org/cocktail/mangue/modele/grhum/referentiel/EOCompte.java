//EOCompte.java
//Created on Wed Feb 26 13:45:24  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;


import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

/** Compte Grhum :
 * La cr&eacute;ation du login et du password est bas&eacute;e sur des proc&eacute;dures stock&eacute;es de Grhum.
 * C'est la responsabilit&eacute; des classes qui cr&eacute;ent les comptes de faire appel &agrave; la m&eacute;thode
 * distante de la session qui retourne un login-mot de passe pour un individu
 * @author christine
 *
 */
// 16/06/2010 - ajout de constantes statiques et d'une méthode pour déteminer si c'est un compte administratif
public class EOCompte extends _EOCompte {

	private final static NSArray SORT_ARRAY_DEBUT_DESC = new NSArray(new EOSortOrdering(CPT_DEBUT_VALIDE_KEY, EOSortOrdering.CompareDescending));

	private static final String VLAN_PERSONNEL = "P";
	private static final String VLAN_EXTERIEUR = "X";
	public EOCompte() {
		super();
	}

	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @return
	 */
	public static boolean existeCompteExternePourIndividu(EOEditingContext editingContext,EOIndividu individu) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = %@",new NSArray(individu.persId()));
		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, myQualifier,null);
		NSArray results = editingContext.objectsWithFetchSpecification(myFetch);
		switch (results.count()) {
		case 0 : return false;
		default : return true;
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param login
	 * @return
	 */
	public static EOCompte rechercherComptePourLogin(EOEditingContext editingContext, String login) {
		try {
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(CPT_LOGIN_KEY + " =%@" ,new NSArray(login));
			return fetchFirstByQualifier(editingContext, myQualifier);
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			return null;
		}
	}

	/**
	 * @param editingContext
	 * @param individu
	 * @return compte trouve
	 * @throws Exception si l'individu a plusieurs comptes
	 * @return null si pas de compte trouve
	 */
	public static EOCompte comptePourIndividu(EOEditingContext editingContext,EOIndividu individu) throws Exception {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI)));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = %@", new NSArray(individu.persId())));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_EMAIL_KEY + " != nil", null));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_DOMAINE_KEY + " != nil", null));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_VLAN_KEY + " != nil", null));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_VLAN_KEY + " != %@", new NSArray("E")));

		NSArray results = fetchAll(editingContext, new EOAndQualifier(qualifiers));
		switch (results.count()) {
		case 0 : return null;
		case 1 : return (EOCompte)results.objectAtIndex(0);
		default : throw new Exception("Plusieurs comptes");
		}
	}

	/** Initialise un compte et un compte suite qui est insere dans l'editing context */
	public void init(String login,boolean estPersonnel) {

		Integer clePrimaire = SuperFinder.clePrimairePour(editingContext(), ENTITY_NAME, CPT_ORDRE_KEY, "SeqCompte", true);
		setCptOrdre(clePrimaire);
		
		EOVlans vlan = null;
		if (estPersonnel)			
			vlan = EOVlans.findForCode(editingContext(), VLAN_PERSONNEL);			
		else
			vlan = EOVlans.findForCode(editingContext(), VLAN_EXTERIEUR);

		setToVlanRelationship(vlan);
		setCptVlan(vlan.cVlan());
		setTcryOrdre(vlan.tcryOrdre());

		if (estPersonnel) {
			setCptShell("/bin/");
			if (login != null) {
				setCptHome("/Utilisateurs/" + login);
			} else {
				setCptHome("/Utilisateurs/");
			}

			// Recuperation du GID par defaut. 
			String defaultGid = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_GID);
			if (defaultGid != null)
				setCptGid(new Integer(defaultGid));
			else
				setCptGid(new Integer(1000));
		}

		setCptConnexion(CocktailConstantes.FAUX);
		setCptCharte(CocktailConstantes.FAUX);
		setCptValide(CocktailConstantes.VRAI);

	}

	/**
	 * 
	 * @return
	 */
	public String email() {
		String email = null;
		
		if (cptEmail() != null && cptDomaine() != null) {
			email = cptEmail() + "@" + cptDomaine();
		}
		
		return email;
	}
	/** Retourne true si il s'agit d'un compte utilis&eacute; pour l'&eacute;tablissement */
	public boolean estCompteEtablissement() {
		return cptVlan() != null && cptVlan().equals(VLAN_PERSONNEL);
	}
	// Méthodes statiques
	
	/** Cree un compte pour un individu si le parametrage de Mangue propose la creation
	 * automatique de compte et que l'individu n'a pas de compte externe et l'insere dans l'editing context ainsi
	 * que les donnees associees<BR>
	 * @param estPersonnel true si il s'agit d'un personnel
	 * Retourne null si pas de creation automatique ou que l'individu a deja un compte
	 */
	public static EOCompte creerComptePourIndividu(EOEditingContext ec, EOAgentPersonnel agent, EOIndividu individu, boolean estPersonnel) {

		NSDictionary dict = preparerInfosCompte(ec, individu);
		String login = (String)dict.valueForKey("login");
	
		if (!EOGrhumParametres.isCompteAuto())
			return null;

		if (individu.persId() == null)
			return null;

		if (EOCompte.existeCompteExternePourIndividu(individu.editingContext(), individu))
			return null;

		EOCompte compte = new EOCompte();
		ec.insertObject(compte);

		compte.init(login,estPersonnel);
		compte.setPersId(individu.persId());

		compte.setPersIdCreation(agent.toIndividu().persId());
		compte.setPersIdModification(agent.toIndividu().persId());

		modifierCompteAvecDictionnaire(ec, compte, dict);
		
		EOCompteEmail.creer(ec, compte);
		
		return compte;
	}
	
	
	/**
	 * 
	 * @param ec
	 * @param compte
	 * @param agent
	 * @param individu
	 */
	public static void modifierComptePourIndividu(EOEditingContext ec, EOCompte compte, EOIndividu individu) {

		NSDictionary dict = preparerInfosCompte(ec, individu);
		compte.setCptLogin((String)dict.valueForKey("login"));

		if (EOGrhumParametres.isComptePasswordClair())
			compte.setCptPasswdClair((String)dict.valueForKey("password"));

		compte.setCptCrypte("O");
		compte.setCptPasswd(getCryptedPassword(ec, (String)dict.valueForKey("password")));

		compte.setCptEmail((String)dict.valueForKey("email"));
		compte.setCptDomaine((String)dict.valueForKey("domaine"));
		compte.setCptUid(new Integer( ((Number)dict.valueForKey("uid")).intValue()) );
	}
	private static void modifierCompteAvecDictionnaire(EOEditingContext ec, EOCompte compte, NSDictionary dict) {

		compte.setCptLogin((String)dict.valueForKey("login"));
		if (EOGrhumParametres.isComptePasswordClair())
			compte.setCptPasswdClair((String)dict.valueForKey("password"));

		compte.setCptCrypte("O");		
		compte.setCptPasswd(getCryptedPassword(ec, (String)dict.valueForKey("password")));

		compte.setCptEmail((String)dict.valueForKey("email"));
		compte.setCptDomaine((String)dict.valueForKey("domaine"));
		compte.setCptUid(new Integer( ((Number)dict.valueForKey("uid")).intValue()) );
	}
	private static NSDictionary preparerInfosCompte(EOEditingContext ec, EOIndividu individu) {
		// preparer login, password, email, domaine et uid
		EODistributedObjectStore store = (EODistributedObjectStore)ec.parentObjectStore();
		Class[] classeParametres = new Class[] {String.class,String.class};
		Object[] parametres = new Object[] {individu.nomUsuel(),individu.prenom()};
		return (NSDictionary)store.invokeRemoteMethodWithKeyPath(ec,"session","clientSideRequestEvaluerInformationsCompte",classeParametres,parametres,false);
	}

	// méthodes privées
	private static String getCryptedPassword(EOEditingContext ec, String uncrypted) {
		String retour = "";
		EODistributedObjectStore objectStore = (EODistributedObjectStore)ec.parentObjectStore();
		retour = (String)objectStore.invokeRemoteMethodWithKeyPath(ec, "session","clientSideRequestGetCryptPassword", new Class[]{String.class},
				new Object[]{uncrypted}, false);
		return retour;
	}



	
	
	

	/** Retourne tous les comptes d'un individu */
	public static NSArray rechercherPourIndividu(EOEditingContext ec,EOIndividu individu) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CPT_VALIDE_KEY + " = %@", new NSArray(CocktailConstantes.VRAI)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PERS_ID_KEY + " = %@", new NSArray(individu.persId())));

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_DEBUT_DESC);		
		}
		catch (Exception e) {
			return new NSArray();
		}
	}


	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


}

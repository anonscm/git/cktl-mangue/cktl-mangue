// EOFournis.java
// Created on Thu Mar 06 13:13:56  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;


import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EOFournis extends _EOFournis {

	public static EOSortOrdering SORT_CODE = new EOSortOrdering(FOU_CODE_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_CODE_DESC = new EOSortOrdering(FOU_CODE_KEY, EOSortOrdering.CompareDescending);
	public static NSArray<EOSortOrdering> SORT_ARRAY_CODE = new NSArray<EOSortOrdering>(SORT_CODE);
	public static NSArray<EOSortOrdering> SORT_ARRAY_CODE_DESC = new NSArray<EOSortOrdering>(SORT_CODE_DESC);


	public EOFournis() {
		super();
	}

	/** cree un fournisseur et toutes les informations adHoc 
	 * @param editingContext
	 * @param adresse du fournisseur
	 * @param individu concerne
	 * @param agent jefy 
	 * @return le fournisseur cree */
	public static EOFournis creeFournisDansEditingContext(EOEditingContext edc,EOAdresse adresse, EOIndividu individu, String code, EOAgent agent) {

		String fouCode;

		// On ajoute un fournisseur
		EOClassDescription descriptionClass = EOClassDescription.classDescriptionForEntityName(ENTITY_NAME);
		EOFournis newFournis = (EOFournis)descriptionClass.createInstanceWithEditingContext(edc,null);
		fouCode = EOFournis.getFouCode(edc,individu.nomUsuel());
		newFournis.initFournis( adresse, individu, fouCode , agent );
		edc.insertObject(newFournis);

		EOValideFournis newValideFournis = newFournis.valideFournis();
		newValideFournis.initValideFournis(newFournis, agent, agent);
		edc.insertObject(newValideFournis);
		return newFournis;

	}
	public boolean estValide() {
		return fouValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean yn) {
		if (yn)
			setFouValide(CocktailConstantes.VRAI);
		else
			setFouValide(CocktailConstantes.FAUX);
	}

	/** initialise le fournisseur et cree le validFournis qui est insere dans l'editing context
	 * @param adresse
	 * @param individu
	 * @param code
	 * @param agent agent responsable de la creation
	 */
	public void initFournis(EOAdresse adresse, EOIndividu individu, String code, EOAgent agent) {

		setAgentRelationship(agent);

		if (adresse != null) {
			setAdresseRelationship(adresse);  
		}

		setEstValide(true);
		setPersId(individu.persId());  
		setFouCode(code);
		setFouDate(new NSTimestamp ());
		setFouMarche("0");
		setFouType("F");
		setFouEtranger("N");
		
		// Créer le fournisseur valide
		EOValideFournis valideFournis = new EOValideFournis();
		valideFournis.initValideFournis(this, agent, agent);
		agent.editingContext().insertObject(valideFournis);
	} 

	/**
	 * Construction du code du fournisseur
	 * @param edc
	 * @param nom
	 * @return
	 */
	public static String getFouCode(EOEditingContext edc, String nom) {
		String retour = null;

		retour = (NSArray.componentsSeparatedByString(nom," ")).componentsJoinedByString("");
		retour = (NSArray.componentsSeparatedByString(retour,"'")).componentsJoinedByString("");
		retour = (NSArray.componentsSeparatedByString(retour,",")).componentsJoinedByString("");
		retour = (NSArray.componentsSeparatedByString(retour,"-")).componentsJoinedByString("");
		retour = (NSArray.componentsSeparatedByString(retour,"_")).componentsJoinedByString("");

		if (retour.length() >= 3) {
			retour = retour.substring(0,3);
		}
		else {
			String chaine = "";
			for (int i=0;i< (3 - (retour.length()));i++)
				chaine = chaine + "0";
			retour = retour + chaine;
		}

		EOQualifier qual  = EOQualifier.qualifierWithQualifierFormat(FOU_CODE_KEY + " like '" + retour + "*'",null);
		NSArray<EOFournis> fournis = fetchAll(edc, qual, SORT_ARRAY_CODE_DESC);

		int numero = 0;
		if (fournis.size() > 0) {
			EOFournis myFournis = fournis.get(0);
			numero = ((Number)new Integer(myFournis.fouCode().substring(3,8))).intValue();
		}

		numero ++;

		String chaine = "";
		for (int i=0;i< (5 - ((""+ numero).length()));i++)		    
			chaine = chaine + "0";

		return (retour + chaine + numero);
	}
}

//EORepartStructure.java
//Created on Thu Mar 06 08:58:46  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum.referentiel;


import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EORepartStructure extends _EORepartStructure {

	public static NSArray<EOSortOrdering> SORT_LIBELLE_STRUCTURE_ASC = new NSArray(new EOSortOrdering(STRUCTURE_KEY+"."+EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareAscending));

	public EORepartStructure() {
		super();
	}


	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param structure
	 * @return
	 */
	public static EORepartStructure creer (EOEditingContext edc, Integer persId, EOStructure structure) {

		EORepartStructure newObject = new EORepartStructure();
		edc.insertObject(newObject);

		newObject.setPersId(persId);
		newObject.setStructureRelationship(structure);
		newObject.setCStructure(structure.cStructure());

		return newObject;

	}

	/**
	 * Recherche un enregistrement dans RepartStructure pour un Individu et un Groupe
	 * 
	 * @param editingContext
	 * @param persId
	 * @param cStructure
	 * @return
	 */
	public static EORepartStructure rechercherPourPersIdEtStructure(EOEditingContext ec, Integer persId, EOStructure structure) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(PERS_ID_KEY, persId));		
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(STRUCTURE_KEY, structure));

		return fetchFirstByQualifier(ec, new EOAndQualifier(andQualifiers));
	}

	public void init(EOIndividu individu,EOStructure structure) {
		setPersId(individu.persId());
		setCStructure(structure.cStructure());
		setStructureRelationship(structure);

	}

	public void validateForSave() {
		if (persId() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir le persId d'un individu ou d'une structure");
		}
		if (structure() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir une structure");
		}

		if (dCreation() == null)
			setDCreation(new NSTimestamp());

		setDModification(new NSTimestamp());

	}
	public void supprimerRepartAssociations() {
		if (repartsAssociation() == null || repartsAssociation().count() == 0) {
			return;
		}
		int total = repartsAssociation().count();
		for (int i = 0; i < total;i++) {
			EORepartAssociation repartAssociation = (EORepartAssociation)repartsAssociation().get(0);
			repartAssociation.supprimerRelations();
			removeObjectFromBothSidesOfRelationshipWithKey(repartAssociation, REPARTS_ASSOCIATION_KEY);
			editingContext().deleteObject(repartAssociation);
		}
	}

	
	/** recherche les repart structures lies a une structure
	 * @param editingContext
	 * @param cStructure (ne peut etre nul) : code de la structure
	 * @return reparts structures ou null si cStructure = null
	 */
	public static NSArray<EORepartStructure> rechercherRepartStructuresPourCodeStructure(EOEditingContext ec, String cStructure) {

		if (cStructure == null)
			return null;

		return fetchAll(ec, CocktailFinder.getQualifierEqual(C_STRUCTURE_KEY, cStructure));		
	}


	/**
	 * 
	 * @param ec
	 * @param individu
	 * @param services
	 * @return
	 */
	public static NSArray<EORepartStructure> rechercherRepartStructuresRolesPourIndividu(EOEditingContext ec, EOIndividu individu, boolean services) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray();		

		andQualifiers.addObject(CocktailFinder.getQualifierEqual(PERS_ID_KEY, individu.persId()));
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPE_KEY+"."+EORepartTypeGroupe.TGRP_CODE_KEY +"!=%@", new NSArray("FO")));
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPE_KEY+"."+EORepartTypeGroupe.TGRP_CODE_KEY +"!=%@", new NSArray("PN")));
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPE_KEY+"."+EORepartTypeGroupe.TGRP_CODE_KEY +"!=%@", new NSArray("U")));
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY + "." + EOStructure.TO_REPART_TYPE_GROUPE_KEY+"."+EORepartTypeGroupe.TGRP_CODE_KEY +"!=%@", new NSArray("RE")));
		andQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY+"."+EOStructure.GRP_ACCES_KEY + "!=%@", new NSArray("-")));			

		if (services) {
			NSMutableArray orQualifiers = new NSMutableArray();
			for (int i=0;i<EORepartTypeGroupe.TYPES_GROUPES_ROLES.length;i++) {
				orQualifiers.addObject(
						EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY+"."+
								EOStructure.TO_REPART_TYPE_GROUPE_KEY+"."+
								EORepartTypeGroupe.TGRP_CODE_KEY + "=%@", 
								new NSArray(EORepartTypeGroupe.TYPES_GROUPES_ROLES[i])));				
			}			
			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}	

		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, new EOAndQualifier(andQualifiers), null);
		fs.setUsesDistinct(true);

		return ec.objectsWithFetchSpecification(fs);		
	}



	/** recherche les repart structures liés a un individu et une structure
	 * @param editingContext
	 * @param individu
	 * @param cStructure (peut &ecirc;tre nul) : code de la structure
	 * @return repart structure
	 */
	public static NSArray rechercherRepartStructuresPourIndividuEtCodeStructure(EOEditingContext ec, EOIndividu individu,String cStructure) {

		NSMutableArray qualifiers = new NSMutableArray();		

		qualifiers.addObject(CocktailFinder.getQualifierEqual(PERS_ID_KEY, individu.persId()));

		if (cStructure != null)
			qualifiers.addObject(CocktailFinder.getQualifierEqual(C_STRUCTURE_KEY, cStructure));
		else {
			NSMutableArray orQualifiers = new NSMutableArray();
			for (int i=0;i<EORepartTypeGroupe.TYPES_GROUPES_ROLES.length;i++) {
				orQualifiers.addObject(
						EOQualifier.qualifierWithQualifierFormat(STRUCTURE_KEY+"."+
								EOStructure.TO_REPART_TYPE_GROUPE_KEY+"."+
								EORepartTypeGroupe.TGRP_CODE_KEY + "=%@", 
								new NSArray(EORepartTypeGroupe.TYPES_GROUPES_ROLES[i])));				
			}

			qualifiers.addObject(new EOOrQualifier(orQualifiers));
		}

		return fetchAll(ec, new EOAndQualifier(qualifiers));		
	}
	/** recherche les repart structures lies a un individu et une structure
	 * @param editingContext
	 * @param individu
	 * @param structure (peut etre nul)
	 * @return repart structure
	 */
	public static NSArray<EORepartStructure> rechercherRepartStructuresPourIndividuEtStructure(EOEditingContext edc, EOIndividu individu,EOStructure structure) {
		if (structure != null)
			return rechercherRepartStructuresPourIndividuEtCodeStructure(edc, individu, structure.cStructure());
		else
			return rechercherRepartStructuresPourIndividuEtCodeStructure(edc, individu, null);
	}
	/** supprime les repart structure pour un individu qui n'est plus present dans l'etablissement */
	public static void supprimerRepartPourIndividu(EOEditingContext ec,EOIndividu individu) {
		supprimerSiNecessaire(ec, fetchAll(ec, CocktailFinder.getQualifierEqual(PERS_ID_KEY, individu.persId()), null));
	}
	/** supprime une repart structure pour un individu et une structure */
	public static void supprimerRepartPourIndividuEtStructure(EOEditingContext ec,EOIndividu individu,EOStructure structure) {
		supprimerSiNecessaire(ec, new NSArray<EORepartStructure>(rechercherPourPersIdEtStructure(ec, individu.persId(), structure)));
	}


	/**
	 * 
	 */
	private static void supprimerSiNecessaire(EOEditingContext editingContext,NSArray<EORepartStructure> reparts) {

		try {

			String structureFouArchives = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_ANNUAIRE_FOUR_ARCHIVE);
			String structureFouEnCours = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_ANNUAIRE_FOUR_ENCOURS_VALIDE);
			String structureFouValide = EOGrhumParametres.getValueParam(ManGUEConstantes.GRHUM_PARAM_KEY_ANNUAIRE_FOU_VALIDE_PHYSIQUE);

			for (EORepartStructure repart : reparts) {

				boolean supprimer = true;

				if (repart.structure() == null) 
					supprimer = false;
				else
					if ((structureFouArchives != null && repart.structure().cStructure().equals(structureFouArchives)) ||
							(structureFouEnCours != null && repart.structure().cStructure().equals(structureFouEnCours)) ||
							(structureFouValide != null && repart.structure().cStructure().equals(structureFouValide))) {
						supprimer = false;
					}

				if (supprimer) {
					repart.setStructureRelationship(null);
					editingContext.deleteObject(repart);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}
}


// EOLienFiliationEnfant.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum;

import org.cocktail.common.utilities.RecordAvecLibelle;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

// 02/02/2010 - Ajout de constantes et de méthodes pour le CIR renvoyant des booléens
// 14/07/2010 - Constantes rendues publiques
public class EOLienFiliationEnfant extends _EOLienFiliationEnfant implements RecordAvecLibelle {

	public final static String LIEN_FILIATION_INCONNU = "LF00";
	public final static String LIEN_FILIATION_LEGITIME = "LF10";
	public final static String LIEN_FILIATION_ADOPTION = "LF20";
	private final static String LIEN_FILIATION_NATUREL = "LF30";
    public EOLienFiliationEnfant() {
        super();
    }
    
    public String toString() {
    	return lfenCode()+" - " + lfenEdition();
    }

    public static EOLienFiliationEnfant getDefault(EOEditingContext ec) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(LFEN_CODE_KEY+"=%@", new NSArray(LIEN_FILIATION_LEGITIME));
    	return fetchFirstByQualifier(ec, qualifier);
   	}
    
    public boolean estInconnu() {
    	return lfenCode().equals(LIEN_FILIATION_INCONNU);
    }
    public boolean estEnfantLegitime() {
    	return lfenCode().equals(LIEN_FILIATION_LEGITIME);
    }
    public boolean estAdopte() {
    	return lfenCode().equals(LIEN_FILIATION_ADOPTION);
    }
    public boolean estEnfantNaturel() {
    	return lfenCode().equals(LIEN_FILIATION_NATUREL);
    }
    // Interface RecordAvecLibelle
	public String libelle() {
		return lfenEdition();
	}

 
}

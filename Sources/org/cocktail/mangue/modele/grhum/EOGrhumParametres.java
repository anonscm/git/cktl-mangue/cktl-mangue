// GrhumParametres.java
// Created on Fri Sep 05 08:43:38  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOGrhumParametres extends _EOGrhumParametres {

	public static final String TYPE_PARAMETRE_MANGUE = "org.cocktail.mangue.";
	public static final String TYPE_PARAMETRE_MANGUE_CONGES = "org.cocktail.mangue.conges.";
	public static final String TYPE_PARAMETRE_MANGUE_MODALITES = "org.cocktail.mangue.modalites.";
	public static final String CLE_PARAM_KEY = "cleParam";
	public static final String LIBELLE_AFFICHAGE_KEY = "libelleAffichage";

	public static final String[] LISTE_PARAMS_TYPES_CONTRAT = new String[] {"A", "L"};
	public static final String[] LISTE_PARAMS_NO_MATRICULE = new String[] {"S", "A", "M", "N"};

	private static NSMutableDictionary dicoParametres = new NSMutableDictionary();

	public EOGrhumParametres() {
		super();
	}

	public String libelleAffichage() {
		return StringCtrl.replace(paramKey(), TYPE_PARAMETRE_MANGUE, "");
	}

	/**
	 * 
	 * @return
	 */
	public static EOGrhumParametres creer(EOEditingContext edc) {
		EOGrhumParametres newObject = new EOGrhumParametres();

		edc.insertObject(newObject);
		return newObject;
	}

	public static NSArray<EOGrhumParametres> findParametresMangue(EOEditingContext edc) {
		NSArray mySort = new NSArray(new EOSortOrdering(PARAM_KEY_KEY, EOSortOrdering.CompareAscending));
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(PARAM_KEY_KEY + " caseInsensitiveLike %@", new NSArray(TYPE_PARAMETRE_MANGUE + "*"));
		return fetchAll(edc, qualifier, mySort);
	}
	/**
	 * 
	 * @param edc
	 * @param cle
	 * @return
	 */
	public static EOGrhumParametres findParametre(EOEditingContext edc,String cle) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(PARAM_KEY_KEY + " = %@",new NSArray(cle));
			return fetchFirstByQualifier(edc, qualifier);
		} catch(Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param paramKey
	 * @return
	 */
	public static Integer getValueParamInteger(String paramKey) {
		String value = getValueParam(paramKey);
		if (value != null) {
			return new Integer(value);
		}
		return null;
	}

	/**
	 * 
	 * @param paramKey
	 * @return
	 */
	public static String getValueParam(String paramKey) {

		EOGrhumParametres param = (EOGrhumParametres)dicoParametres.objectForKey(paramKey);
		if (param != null) {
			return param.paramValue();
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isParametreVrai() {
		return paramValue().equals("VRAI") || paramValue().equals("1") 
				|| paramValue().equals("O") || paramValue().equals("OUI") 
				|| paramValue().equals("Y") || paramValue().equals("YES");
	}


	public static NSMutableDictionary getDicoParametres() {
		return dicoParametres;
	}

	public static void setDicoParametres(NSMutableDictionary dicoParametres) {
		EOGrhumParametres.dicoParametres = dicoParametres;
	}

	/**
	 * 
	 * @return
	 */
	public String cleParam() {
		return StringCtrl.replace(paramKey(), TYPE_PARAMETRE_MANGUE, "");
	}

	public static Boolean isCompteAuto() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_COMPTE_AUTO) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_COMPTE_AUTO)).isParametreVrai();
	}
	public static Boolean isComptePasswordClair() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_COMPTE_PASSWORD_CLAIR) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_COMPTE_PASSWORD_CLAIR)).isParametreVrai();
	}

	public static Boolean isAffectationStructure() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_INS_AFF_STRUCTURE) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_INS_AFF_STRUCTURE)).isParametreVrai();
	}

	public static Boolean isAffectationStructureVacataires() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_INS_AFF_VAC_STRUCTURE) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_INS_AFF_VAC_STRUCTURE)).isParametreVrai();
	}

	public static Boolean isUseSifac() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_USE_SIFAC) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_USE_SIFAC)).isParametreVrai();
	}

	public static Boolean isUseModulePrimes() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_MODULE_PRIMES) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_MODULE_PRIMES)).isParametreVrai();
	}

	public static Boolean isUseGestionnaire() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_GESTIONNAIRES) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_GESTIONNAIRES)).isParametreVrai();
	}

	public static Boolean isNoEmploiFormatte() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_GESTION_NUMERO_EMPLOI) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_GESTION_NUMERO_EMPLOI)).isParametreVrai();
	}

	public static boolean isGestionHu() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_GESTION_HU) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_GESTION_HU)).isParametreVrai();
	}

	public static boolean isSiretObligatoire() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_SIRET_OBLIGATOIRE) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_SIRET_OBLIGATOIRE)).isParametreVrai();
	}

	public static boolean isGestionLocal() {
		try {
			return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_GESTION_LOCAUX) != null
					&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_GESTION_LOCAUX)).isParametreVrai();
		}
		catch (Exception e) {
			return false;
		}
	}

	public static boolean isGestionEns() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_CONGES_ENS) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_CONGES_ENS)).isParametreVrai();
	}

	public static boolean isUtilisationListeElectorale() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_LISTE_ELECTORALE) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_LISTE_ELECTORALE)).isParametreVrai();
	}

	public static boolean isPhotos() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_PHOTOS) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_PHOTOS)).isParametreVrai();
	}

	public static boolean isNoArreteAuto() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_NO_ARRETE_AUTO) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_NO_ARRETE_AUTO)).isParametreVrai();
	}
	public static boolean isCtrlTptMal() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_CTRL_MAL_MTT) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_CTRL_MAL_MTT)).isParametreVrai();
	}

	public static boolean isTypeContratVacataire() {
		return dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_TYPES_CONTRAT_VAC) != null
				&&	((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_TYPES_CONTRAT_VAC)).isParametreVrai();
	}

	public static String getDefaultRne() {
		return ((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.GRHUM_PARAM_KEY_DEFAULT_RNE)).paramValue();
	}
	public static String getOrdonnateurUniv() {
		try {
			return ((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_ORDO_UNIV)).paramValue();
		}
		catch (Exception e) {
			System.err.println(" Le paramètre " + ManGUEConstantes.PARAM_KEY_ORDO_UNIV + " n'est pas renseigné !");
			return "";
		}
	}
	public static String getOrdonnateurIut() {
		try {
			return ((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_ORDO_IUT)).paramValue();
		}
		catch (Exception e) {
			System.err.println(" Le paramètre " + ManGUEConstantes.PARAM_KEY_ORDO_UNIV + " n'est pas renseigné !");
			return "";
		}
	}
	public static String getSignaturePresident() {
		return ((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_SIGNATURE_PRES)).paramValue();
	}
	public static String getUniteGestionCir() {
		return ((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_UNITE_GESTION)).paramValue();
	}
	public static String getComplementVisiteMedicale() {
		return ((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_COMPLEMENT_VISITE_MEDICALE)).paramValue();
	}
	public static String getAnneeRefGardeEnfant() {
		return ((EOGrhumParametres)dicoParametres.objectForKey(ManGUEConstantes.PARAM_KEY_ANNEE_GARDE_ENFANT)).paramValue();
	}

	public EOGrhumParametresType type() {
		return toTypeParametre();
	}

	/**
	 * 
	 */
	public static void initParametres(EOEditingContext edc) {
		dicoParametres = new NSMutableDictionary();
		NSArray<EOGrhumParametres> parametres = fetchAll(edc);

		for (EOGrhumParametres parametre : parametres) {
			if (parametre.paramKey() != null && parametre.paramValue() != null)	{
				getDicoParametres().setObjectForKey(parametre, parametre.paramKey());
			}
		}    	

	}

}

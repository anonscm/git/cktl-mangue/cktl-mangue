// EOActivitesTypeContrat.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum;

import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOActivitesTypeContrat extends _EOActivitesTypeContrat {
	private final static int LG_LIBELLE = 100;
	
    public EOActivitesTypeContrat() {
        super();
    }

    /**
     * 
     * @param edc
     * @param typeContrat
     */
    public static EOActivitesTypeContrat creer(EOEditingContext edc, EOTypeContratTravail typeContrat) {
    	
    	EOActivitesTypeContrat newActivite = new EOActivitesTypeContrat();
    	newActivite.setToTypeContratTravailRelationship(typeContrat);
    	newActivite.setDCreation(DateCtrl.today());
    	edc.insertObject(newActivite);
    	
    	return newActivite;
    }
    
    /**
     * 
     */
    public void validateForSave() throws NSValidation.ValidationException {
    	if (toTypeContratTravail() == null) {
    		throw new NSValidation.ValidationException("Le type de contrat de travail est obligatoire");
    	}
    	if (atcoLibelle() == null || atcoLibelle().length() == 0) {
    		throw new NSValidation.ValidationException("Le libellé est obligatoire");
    	} else if (atcoLibelle().length() > LG_LIBELLE) {
    		throw new NSValidation.ValidationException("Le libellé comporte au plus " + LG_LIBELLE + " caractères");
    	}
    }

    /**
     * 
     * @param typeContrat
     * @return
     */
    public static NSArray<EOActivitesTypeContrat> rechercherActivitesPourTypeContrat(EOTypeContratTravail typeContrat) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TO_TYPE_CONTRAT_TRAVAIL_KEY + "=%@", new NSArray(typeContrat));
    	return fetchAll(typeContrat.editingContext(), qualifier);
    }
}

// _EOCorps.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCorps.java instead.
package org.cocktail.mangue.modele.grhum;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOCorps extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Corps";
	public static final String ENTITY_TABLE_NAME = "GRHUM.CORPS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cCorps";

	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_CORPS_KEY = "cCorps";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_CORPS_KEY = "dFermetureCorps";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_CORPS_KEY = "dOuvertureCorps";
	public static final String ID_MINISTERE_KEY = "idMinistere";
	public static final String LC_CORPS_KEY = "lcCorps";
	public static final String LL_CORPS_KEY = "llCorps";
	public static final String POTENTIEL_BRUT_KEY = "potentielBrut";
	public static final String TEM_CFP_KEY = "temCfp";
	public static final String TEM_CRCT_KEY = "temCrct";
	public static final String TEM_DELEGATION_KEY = "temDelegation";
	public static final String TEM_LOCAL_KEY = "temLocal";
	public static final String TEM_SURNOMBRE_KEY = "temSurnombre";

// Attributs non visibles
	public static final String C_BUREAU_GESTION_KEY = "cBureauGestion";
	public static final String C_FILIERE_KEY = "cFiliere";
	public static final String C_TYPE_CORPS_KEY = "cTypeCorps";
	public static final String MASSE_INDICIAIRE_KEY = "masseIndiciaire";
	public static final String TEM_MISDEP_KEY = "temMisdep";

//Colonnes dans la base de donnees
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_CORPS_COLKEY = "C_CORPS";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_FERMETURE_CORPS_COLKEY = "D_FERMETURE_CORPS";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_OUVERTURE_CORPS_COLKEY = "D_OUVERTURE_CORPS";
	public static final String ID_MINISTERE_COLKEY = "ID_MINISTERE";
	public static final String LC_CORPS_COLKEY = "LC_CORPS";
	public static final String LL_CORPS_COLKEY = "LL_CORPS";
	public static final String POTENTIEL_BRUT_COLKEY = "POTENTIEL_BRUT";
	public static final String TEM_CFP_COLKEY = "TEM_CFP";
	public static final String TEM_CRCT_COLKEY = "TEM_CRCT";
	public static final String TEM_DELEGATION_COLKEY = "TEM_DELEGATION";
	public static final String TEM_LOCAL_COLKEY = "TEM_LOCAL";
	public static final String TEM_SURNOMBRE_COLKEY = "TEM_SURNOMBRE";

	public static final String C_BUREAU_GESTION_COLKEY = "C_BUREAU_GESTION";
	public static final String C_FILIERE_COLKEY = "C_FILIERE";
	public static final String C_TYPE_CORPS_COLKEY = "C_TYPE_CORPS";
	public static final String MASSE_INDICIAIRE_COLKEY = "MASSE_INDICIAIRE";
	public static final String TEM_MISDEP_COLKEY = "TEM_MISDEP";


	// Relationships
	public static final String TO_CATEGORIE_KEY = "toCategorie";
	public static final String TO_MINISTERE_KEY = "toMinistere";
	public static final String TO_TYPE_POPULATION_KEY = "toTypePopulation";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cCategorie() {
    return (String) storedValueForKey(C_CATEGORIE_KEY);
  }

  public void setCCategorie(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_KEY);
  }

  public String cCorps() {
    return (String) storedValueForKey(C_CORPS_KEY);
  }

  public void setCCorps(String value) {
    takeStoredValueForKey(value, C_CORPS_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dFermetureCorps() {
    return (NSTimestamp) storedValueForKey(D_FERMETURE_CORPS_KEY);
  }

  public void setDFermetureCorps(NSTimestamp value) {
    takeStoredValueForKey(value, D_FERMETURE_CORPS_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dOuvertureCorps() {
    return (NSTimestamp) storedValueForKey(D_OUVERTURE_CORPS_KEY);
  }

  public void setDOuvertureCorps(NSTimestamp value) {
    takeStoredValueForKey(value, D_OUVERTURE_CORPS_KEY);
  }

  public Integer idMinistere() {
    return (Integer) storedValueForKey(ID_MINISTERE_KEY);
  }

  public void setIdMinistere(Integer value) {
    takeStoredValueForKey(value, ID_MINISTERE_KEY);
  }

  public String lcCorps() {
    return (String) storedValueForKey(LC_CORPS_KEY);
  }

  public void setLcCorps(String value) {
    takeStoredValueForKey(value, LC_CORPS_KEY);
  }

  public String llCorps() {
    return (String) storedValueForKey(LL_CORPS_KEY);
  }

  public void setLlCorps(String value) {
    takeStoredValueForKey(value, LL_CORPS_KEY);
  }

  public Integer potentielBrut() {
    return (Integer) storedValueForKey(POTENTIEL_BRUT_KEY);
  }

  public void setPotentielBrut(Integer value) {
    takeStoredValueForKey(value, POTENTIEL_BRUT_KEY);
  }

  public String temCfp() {
    return (String) storedValueForKey(TEM_CFP_KEY);
  }

  public void setTemCfp(String value) {
    takeStoredValueForKey(value, TEM_CFP_KEY);
  }

  public String temCrct() {
    return (String) storedValueForKey(TEM_CRCT_KEY);
  }

  public void setTemCrct(String value) {
    takeStoredValueForKey(value, TEM_CRCT_KEY);
  }

  public String temDelegation() {
    return (String) storedValueForKey(TEM_DELEGATION_KEY);
  }

  public void setTemDelegation(String value) {
    takeStoredValueForKey(value, TEM_DELEGATION_KEY);
  }

  public String temLocal() {
    return (String) storedValueForKey(TEM_LOCAL_KEY);
  }

  public void setTemLocal(String value) {
    takeStoredValueForKey(value, TEM_LOCAL_KEY);
  }

  public String temSurnombre() {
    return (String) storedValueForKey(TEM_SURNOMBRE_KEY);
  }

  public void setTemSurnombre(String value) {
    takeStoredValueForKey(value, TEM_SURNOMBRE_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.EOCategorie toCategorie() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOCategorie)storedValueForKey(TO_CATEGORIE_KEY);
  }

  public void setToCategorieRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOCategorie value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOCategorie oldValue = toCategorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CATEGORIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CATEGORIE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres toMinistere() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres)storedValueForKey(TO_MINISTERE_KEY);
  }

  public void setToMinistereRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOMinisteres oldValue = toMinistere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MINISTERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MINISTERE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation toTypePopulation() {
    return (org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation)storedValueForKey(TO_TYPE_POPULATION_KEY);
  }

  public void setToTypePopulationRelationship(org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation oldValue = toTypePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_POPULATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_POPULATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOCorps avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCorps createEOCorps(EOEditingContext editingContext, String cCorps
, NSTimestamp dCreation
, NSTimestamp dModification
			) {
    EOCorps eo = (EOCorps) createAndInsertInstance(editingContext, _EOCorps.ENTITY_NAME);    
		eo.setCCorps(cCorps);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  
	  public EOCorps localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCorps)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCorps creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCorps creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCorps object = (EOCorps)createAndInsertInstance(editingContext, _EOCorps.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCorps localInstanceIn(EOEditingContext editingContext, EOCorps eo) {
    EOCorps localInstance = (eo == null) ? null : (EOCorps)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCorps#localInstanceIn a la place.
   */
	public static EOCorps localInstanceOf(EOEditingContext editingContext, EOCorps eo) {
		return EOCorps.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCorps fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCorps fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCorps)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCorps)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCorps fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCorps eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCorps ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCorps fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

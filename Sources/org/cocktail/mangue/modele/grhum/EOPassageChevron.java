/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.grhum;

import java.util.Enumeration;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOPassageChevron extends _EOPassageChevron {

	public static NSArray SORT_C_CHEVRON_ASC = new NSArray(new EOSortOrdering(C_CHEVRON_KEY, EOSortOrdering.CompareAscending));
	public static NSArray SORT_ARRAY_OUVERTURE_DESC = new NSArray(new EOSortOrdering(D_OUVERTURE_KEY, EOSortOrdering.CompareDescending));

    public EOPassageChevron() {
        super();
    }
    
	public static EOPassageChevron creer(EOEditingContext ec, EOPassageEchelon passageEchelon) {
		EOPassageChevron newObject = (EOPassageChevron) createAndInsertInstance(ec, ENTITY_NAME); 		
		newObject.setDOuverture(passageEchelon.dOuverture());
		newObject.setDFermeture(passageEchelon.dFermeture());
		newObject.setCEchelon(passageEchelon.cEchelon());
		newObject.setCGrade(passageEchelon.cGrade());

		return newObject;
	}

	public Boolean isLocal() {
		return true;
	}


	public String libellePassage() {    	

		String libelle = "";
		if (dureeChevronAnnees() != null)
			libelle += dureeChevronAnnees() + " An" + ((dureeChevronAnnees().intValue()>1)?"s":"");
		if ( dureeChevronMois() != null)
			libelle += " " + dureeChevronMois() + " Mois";

		return libelle;
	}
	
	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,D_OUVERTURE_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null || uneDate.equals("")) {
			setDOuverture(null);
		} else {
			SuperFinder.setDateFormatee(this,D_OUVERTURE_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,D_FERMETURE_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null || uneDate.equals("")) {
			setDFermeture(null);
		} else {
			SuperFinder.setDateFormatee(this,D_FERMETURE_KEY,uneDate);
		}
	}
	public void initAvecGrade(EOGrade grade) {
		setCGrade(grade.cGrade());
	}


	
	public void validateForSave() throws NSValidation.ValidationException {
		
		if (cChevron() == null)
			throw new NSValidation.ValidationException("Le chevron doit être fourni");
		
		if (dOuverture() == null)
			throw new NSValidation.ValidationException("La date d'ouverture doit être fournie");
		else 
			if (dFermeture() != null && DateCtrl.isBefore(dFermeture(), dOuverture())) 
				throw new NSValidation.ValidationException("La date d'ouverture ne peut être postérieure à la date de fermeture");
		
		// Rechercher si il existe un passage echelon pour cet echelon
		NSArray passagesEchelon = EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(editingContext(), cGrade(), cEchelon(), dOuverture(), true);
		if (passagesEchelon == null || passagesEchelon.count() == 0) 
			throw new NSValidation.ValidationException("Il n'y a pas de passage échelon défini pour ce grade, cet échelon et cette date d'ouverture !");

		if (dCreation() != null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
	
	
	public static EOPassageChevron findForChevron(EOEditingContext ec, String chevron) {
		
		NSMutableArray qualifiers = new NSMutableArray();				

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_CHEVRON_KEY + "=%@", new NSArray(chevron)));

		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_OUVERTURE_DESC);
	}

	
	public static EOPassageChevron chevronSuivant(EOEditingContext ec, EOGrade grade, String chevron) {
		
		NSMutableArray qualifiers = new NSMutableArray();
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GRADE_KEY + "=%@", new NSArray(grade.cGrade())));
		NSArray chevrons = fetchAll(ec, new EOAndQualifier(qualifiers), SORT_C_CHEVRON_ASC);
		
		for (Enumeration<EOPassageChevron> e = chevrons.objectEnumerator();e.hasMoreElements();) {
			if (e.nextElement().cChevron().equals(chevron))
				return e.nextElement();			
		}
		
		return null;		
	}
	
	
	public static NSArray rechercherPourGradeEtEchelon(EOEditingContext ec,EOGrade grade, String echelon) {

		NSMutableArray qualifiers = new NSMutableArray();				

		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(new EOSortOrdering(D_FERMETURE_KEY, EOSortOrdering.CompareDescending));
		sorts.addObject(new EOSortOrdering(C_CHEVRON_KEY, EOSortOrdering.CompareAscending));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GRADE_KEY + "=%@", new NSArray(grade.cGrade())));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_ECHELON_KEY + "=%@", new NSArray(echelon)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), sorts);

	}

	
	
	/** Recherche les passages chevron pour un grade et un echelon donnes */
	public static NSArray rechercherPassageChevronOuvertPourGradeEtEchelon(EOEditingContext editingContext,String grade,String echelon,NSTimestamp dateReference,boolean enOrdreDecroissant) {

		NSMutableArray args = new NSMutableArray(grade);
		String qualifier = C_GRADE_KEY + " = %@";
		if (echelon != null) {
			args.addObject(echelon);
			qualifier = qualifier + " AND cEchelon = %@";
		}
		if (dateReference != null) {
			args.addObject(dateReference);
			qualifier = qualifier + " AND (dFermeture = nil OR dFermeture >= %@)";
		} else {
			qualifier = qualifier + " AND dFermeture = nil";
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(qualifier ,args);
		// on ne fait pas le tri par la base de données car il ne renvoie pas les classements selon l'ordre alphabétique
		// avec les numériques en tête correctement (du moins avec Oracle)
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qual,null);
		NSArray results = editingContext.objectsWithFetchSpecification(myFetch);
		NSArray sorts = null;
		if (enOrdreDecroissant) {
			sorts = new NSArray (EOSortOrdering.sortOrderingWithKey(C_CHEVRON_KEY,EOSortOrdering.CompareDescending));
		} else {
			sorts = new NSArray (EOSortOrdering.sortOrderingWithKey(C_CHEVRON_KEY,EOSortOrdering.CompareAscending));
		}

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sorts);
	}
	
	
	/** Recherche les passages chevron pour un grade et un echelon donnes */
	public static NSArray rechercherPassageChevronPourGradeEtChevron(EOEditingContext editingContext,String grade, String chevron,NSTimestamp dateReference, boolean enOrdreDecroissant) {

		NSMutableArray args = new NSMutableArray(grade);
		String qualifier = C_GRADE_KEY + " = %@";
		if (chevron != null) {
			args.addObject(chevron);
			qualifier = qualifier + " AND " + C_CHEVRON_KEY + " = %@";
		}
		if (dateReference != null) {
			args.addObject(dateReference);
			qualifier = qualifier + " AND (dFermeture = nil OR dFermeture >= %@)";
		} else {
			qualifier = qualifier + " AND dFermeture = nil";
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(qualifier ,args);
		// on ne fait pas le tri par la base de données car il ne renvoie pas les classements selon l'ordre alphabétique
		// avec les numériques en tête correctement (du moins avec Oracle)
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qual,null);
		NSArray results = editingContext.objectsWithFetchSpecification(myFetch);
		NSArray sorts = null;
		if (enOrdreDecroissant) {
			sorts = new NSArray (EOSortOrdering.sortOrderingWithKey(C_CHEVRON_KEY,EOSortOrdering.CompareDescending));
		} else {
			sorts = new NSArray (EOSortOrdering.sortOrderingWithKey(C_CHEVRON_KEY,EOSortOrdering.CompareAscending));
		}

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sorts);
	}

	
	
	/** recherche les passages chevron associes a une liste de grades
	 * @param editingContext
	 * @param grades tableau de EOGrade
	 * @param shouldRefresh pour raffraichir les donnees
	 */
	public static NSArray rechercherPassagesChevronPourGrade(EOEditingContext ec,NSArray grades, boolean shouldRefresh) {
		NSMutableArray qualifiers = new NSMutableArray();
		for (Enumeration<EOGrade> e = grades.objectEnumerator();e.hasMoreElements();){
			EOGrade grade = e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat( C_GRADE_KEY + " = '" + grade.cGrade() + "'",null));
		}
		return fetchAll(ec, new EOOrQualifier(qualifiers));
	}
	/** Recherche les passages chevron valides lies a un grade valide pendant la periode
	 * @param editingContext
	 * @param grade
	 * @param dateReference peut etre nulle
	 */
	public static NSArray rechercherPassagesChevronPourGradesValidesPourPeriode(EOEditingContext editingContext,EOGrade grade,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GRADE_KEY + " = %@",new NSArray(grade.cGrade())));
		if (debutPeriode == null) {
			if (finPeriode == null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY + " = nil)",null));
			} else {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("dFermeture = nil OR dFermeture >= %@)",new NSArray(finPeriode)));
			}
		} else
			qualifiers.addObject(SuperFinder.qualifierPourPeriode(D_OUVERTURE_KEY, debutPeriode, D_FERMETURE_KEY, finPeriode));
		
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers),null);
		NSArray result = editingContext.objectsWithFetchSpecification(fs);
		return result;
	}
	/** Recherche les passages &eacute;chelon  pour un grade donn&eacute; */
	public static NSArray rechercherPassagesChevronPourGradeAvecTri(EOEditingContext ec,String grade,NSTimestamp dateReference,boolean enOrdreDecroissant) {
		return rechercherPassageChevronOuvertPourGradeEtEchelon(ec,grade,null,dateReference,enOrdreDecroissant);
	}

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.grhum;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.modele.finder.GradeFinder;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOGrade extends _EOGrade {

	public static final EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LL_GRADE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);
	
	public static String CODE_SANS_GRADE = "9990";
	public static String CODE_GRADE_DOC_ENS = "6904";		// Grade associe aux doctorants enseignants

	String cGradeNNE;

	public static final String[] LISTE_CATEGORIES = new String[]{"A", "B", "C", "D", "E"};

    public EOGrade() {
        super();
    }
    
    public String codeLibelle() {
    	return cGrade() + " - " + llGrade();
    }
    
	public Boolean isLocal() {
		return true;
	}

	/**
	 * 
	 * @return
	 */
	public boolean estNonRenseigne() {
		return cGrade().equals(CODE_SANS_GRADE);
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param corps
	 * @param cGrade
	 * @param dCreation
	 * @param dModification
	 * @return
	 */
    public static  EOGrade createEOGrade(EOEditingContext editingContext, EOCorps corps, String cGrade
    		, NSTimestamp dCreation
    		, NSTimestamp dModification
    					) {
    		    EOGrade eo = (EOGrade) createAndInsertInstance(editingContext, _EOGrade.ENTITY_NAME);    
    				eo.setCGrade(cGrade);
    				eo.setToCorpsRelationship(corps);
    				eo.setDCreation(dCreation);
    				eo.setDModification(dModification);
    		    return eo;
    		  }

    
	/** Recherche le grade TG dans la table LienGradeMenTG et retourne le premier trouv&eacute;, on ne prend pas en compte
	 * les dates d'ouverture et de fermeture */
	public String cGradeNNE() {
		if (cGradeNNE != null)
			return cGradeNNE;

		NSArray<EOLienGradeMenTg> grades = EOLienGradeMenTg.fetchAll(editingContext(), CocktailFinder.getQualifierEqual(EOLienGradeMenTg.C_GRADE_KEY, cGrade()));
		if (grades.size() == 0) {
			return null;
		} else {
			return grades.get(0).cGradeTg();
		}
	}
	
	/** Retourne null */
	public String cGradeAdage() {
		return null;
	}
	// Interface RecordAvecLibelleEtCode
	public String libelle() {
		String libelle = llGrade();
		if (dOuverture() != null) {
			if (dFermeture() == null) {
				libelle += " ouvert à partir du " + DateCtrl.dateToString(dOuverture());
			} else {
				libelle += " ouvert du " + DateCtrl.dateToString(dOuverture()) + " au " + DateCtrl.dateToString(dFermeture());
			}
		} else if (dFermeture() != null) {
			libelle += " fermé à partir du " + DateCtrl.dateToString(dFermeture());
		}
		return libelle;
	}
	public String code() {
		return cGrade();
	}
	public boolean estSansGrade() {
		return cGrade() != null && cGrade().equals(CODE_SANS_GRADE);
	}
	public boolean estDoctorantEns() {
		return cGrade() != null && cGrade().equals(CODE_GRADE_DOC_ENS);
	}
		
	/** Recherche les grades valides lies a un corps pendant la periode passee en parametre
	 * @param editingContext
	 * @param corps peut etre nul
	 * @param dateDebut peutetre nulle
	 * @param dateFin peut etre nulle
	 */
	public static NSArray rechercherGradesPourCorpsValidesEtPeriode(EOEditingContext editingContext,EOCorps corps,NSTimestamp dateDebut,NSTimestamp dateFin) {
		NSMutableArray qualifiers = new NSMutableArray();
		if (corps != null) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CORPS_KEY + " = %@",new NSArray(corps)));
		}
		if (dateDebut == null) {
			qualifiers.addObject(CocktailFinder.getQualifierNullValue(D_FERMETURE_KEY));
		} else {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(dFermeture  = nil OR dFermeture >= %@)",new NSArray(dateDebut)));
			if (dateFin != null) {
				// champDateDebut = nil or champDateDebut <= finPeriode
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("(dOuverture = nil OR dOuverture <= %@)",new NSArray(dateFin)));
			}
		}
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,new EOAndQualifier(qualifiers),null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
	
	/**
	 * 
	 * @param ec
	 * @param corps
	 * @return
	 */
	public static NSArray<EOGrade> findForCorps(EOEditingContext ec,EOCorps corps) {

		NSMutableArray<EOSortOrdering> sorts = new NSMutableArray<EOSortOrdering>();
		sorts.addObject(new EOSortOrdering(D_FERMETURE_KEY, EOSortOrdering.CompareDescending));
		sorts.addObject(new EOSortOrdering(LC_GRADE_KEY, EOSortOrdering.CompareAscending));

		return fetchAll(ec, CocktailFinder.getQualifierEqual(TO_CORPS_KEY, corps), sorts);
	}
	
	/** Recherche les grades avec le code corps passe en parametre
	 * @param editingContext
	 * @param codeCorps code corps
	 * @param avecTri booleen : vrai si il faut trier les grades par ordre croissant
	 */
	public static NSArray<EOGrade> rechercherGradesPourCorps(EOEditingContext edc, String codeCorps, NSTimestamp dateValidite, boolean avecTri) {

		NSArray<EOSortOrdering> sorts = null;
		if (avecTri)
			sorts = new NSArray<EOSortOrdering>(new EOSortOrdering(C_GRADE_KEY, EOSortOrdering.CompareAscending));
		
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(C_CORPS_KEY, codeCorps));		
		andQualifiers.addObject(GradeFinder.getQualifierValiditeForDate(dateValidite));

		return fetchAll(edc, new EOAndQualifier(andQualifiers), sorts);

	}
	
	
	public static NSArray rechercherGradePourQualifier(EOEditingContext ec, EOQualifier qualifier) {		
		return fetchAll(ec, qualifier, null);	
	}


	/** Recherche les grades avec le code corps passe en parametre
	 * @param editingContext
	 * @param codeCorps code corps
	 */
	public static EOGrade rechercherGradeMinimumPourCorps(EOEditingContext editingContext,String codeCorps,NSTimestamp dateValidite) {
		NSArray grades = rechercherGradesPourCorps(editingContext,codeCorps,dateValidite,true);
		try {
			return (EOGrade)grades.objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	
	public String llGradeCir() {
		
		if (cGradeNNE() != null)
			return llGrade();
		
		return null;
	}

	
	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (cGrade() == null) {
			throw new NSValidation.ValidationException("Le code grade est obligatoire !");			
		}
		
		if (cGrade().indexOf(toCorps().cCorps()) == -1) {
			throw new NSValidation.ValidationException("Le code grade doit commencer par le code du corps !");
		}  
			
		if (lcGrade() == null || lcGrade().length() >= 20) {
			throw new NSValidation.ValidationException("Le libellé court du grade doit être renseigné et ne doit pas excéder 20 caractères !");
		}  

		if (llGrade() == null || llGrade().length() >= 200) {
			throw new NSValidation.ValidationException("Le libellé du grade doit être renseigné et ne doit pas excéder 200 caractères !");
		}  

		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}
    
    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

// EOParamAncienneteMaladie.java
// Created on Thu Feb 09 09:45:46 Europe/Paris 2006 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public class EOParamAncienneteMaladie extends EOParamAnciennete {

	public EOParamAncienneteMaladie() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public static EOParamAncienneteMaladie getAncienneteForIndividu(EOEditingContext edc, EOIndividu individu, NSTimestamp dateReference) {

		// Calcul de l'anciennete de l'agent
		NSArray<EOContrat> contrats = EOContrat.rechercherContratsIndividuAnterieursADate(edc, individu, dateReference);

		int nbJours = 0;
		for (EOContrat contrat : contrats) {

			if (DateCtrl.isAfter(contrat.dateFin(), dateReference))
				nbJours += DateCtrl.nbJoursEntre(contrat.dateDebut(), dateReference, false);
			else
				nbJours += DateCtrl.nbJoursEntre(contrat.dateDebut(), contrat.dateFin(), false);

		}

		int nbMois = nbJours / 30;

		EOParamAncienneteMaladie anciennete = null;
		if (nbMois < 4)
			anciennete = (EOParamAncienneteMaladie)getParamForAnciennete(edc, ANC_INF_4_MOIS);
		else
			if (nbMois <= 24)
				anciennete =  (EOParamAncienneteMaladie)getParamForAnciennete(edc, ANC_4_MOIS_2_ANS);
			else
				if (nbMois <= 36)
					anciennete =  (EOParamAncienneteMaladie)getParamForAnciennete(edc, ANC_2_3_ANS);
				else
					if (nbMois > 36)
						anciennete =  (EOParamAncienneteMaladie)getParamForAnciennete(edc, ANC_SUP_3_ANS);

		return anciennete;
	}

	/**
	 * 
	 * @param edc
	 * @param code
	 * @return
	 */
	public static EOParamAncienneteMaladie getParamForAnciennete(EOEditingContext edc, String code) {   			

		try {
			EOFetchSpecification fetchSpec = new EOFetchSpecification("ParamCgntMal", CocktailFinder.getQualifierEqual(C_ANCIENNETE_KEY, code), null);
			return ((NSArray<EOParamAncienneteMaladie>)edc.objectsWithFetchSpecification(fetchSpec)).get(0);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public Number dureeDemiTrait() {
		return (Number)storedValueForKey("dureeDemiTrait");
	}

	public void setDureeDemiTrait(Number value) {
		takeStoredValueForKey(value, "dureeDemiTrait");
	}
}

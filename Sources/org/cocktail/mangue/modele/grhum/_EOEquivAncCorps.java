// _EOEquivAncCorps.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEquivAncCorps.java instead.
package org.cocktail.mangue.modele.grhum;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOEquivAncCorps extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "EquivAncCorps";
	public static final String ENTITY_TABLE_NAME = "GRHUM.EQUIV_ANC_CORPS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noSeqEquivAncCorps";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TEM_PROMOUVABILITE_KEY = "temPromouvabilite";
	public static final String TEM_TT_TYPE_ACCES_KEY = "temTtTypeAcces";

// Attributs non visibles
	public static final String NO_SEQ_EQUIV_ANC_CORPS_KEY = "noSeqEquivAncCorps";
	public static final String C_GRADE_EQUIV_KEY = "cGradeEquiv";
	public static final String C_CORPS_DEPART_KEY = "cCorpsDepart";
	public static final String C_TYPE_ACCES_KEY = "cTypeAcces";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String TEM_PROMOUVABILITE_COLKEY = "TEM_PROMOUVABILITE";
	public static final String TEM_TT_TYPE_ACCES_COLKEY = "TEM_TT_TYPE_ACCES";

	public static final String NO_SEQ_EQUIV_ANC_CORPS_COLKEY = "NO_SEQ_EQUIV_ANC_CORPS";
	public static final String C_GRADE_EQUIV_COLKEY = "C_GRADE_EQUIV";
	public static final String C_CORPS_DEPART_COLKEY = "C_CORPS_DEPART";
	public static final String C_TYPE_ACCES_COLKEY = "C_TYPE_ACCES";


	// Relationships
	public static final String CORPS_DEPART_KEY = "corpsDepart";
	public static final String GRADE_EQUIVALENT_KEY = "gradeEquivalent";
	public static final String TYPE_ACCES_KEY = "typeAcces";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey(D_DEB_VAL_KEY);
  }

  public void setDDebVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_VAL_KEY);
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey(D_FIN_VAL_KEY);
  }

  public void setDFinVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VAL_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String temPromouvabilite() {
    return (String) storedValueForKey(TEM_PROMOUVABILITE_KEY);
  }

  public void setTemPromouvabilite(String value) {
    takeStoredValueForKey(value, TEM_PROMOUVABILITE_KEY);
  }

  public String temTtTypeAcces() {
    return (String) storedValueForKey(TEM_TT_TYPE_ACCES_KEY);
  }

  public void setTemTtTypeAcces(String value) {
    takeStoredValueForKey(value, TEM_TT_TYPE_ACCES_KEY);
  }

  public org.cocktail.mangue.modele.grhum.EOCorps corpsDepart() {
    return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey(CORPS_DEPART_KEY);
  }

  public void setCorpsDepartRelationship(org.cocktail.mangue.modele.grhum.EOCorps value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOCorps oldValue = corpsDepart();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CORPS_DEPART_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CORPS_DEPART_KEY);
    }
  }
  
  public org.cocktail.mangue.modele.grhum.EOGrade gradeEquivalent() {
    return (org.cocktail.mangue.modele.grhum.EOGrade)storedValueForKey(GRADE_EQUIVALENT_KEY);
  }

  public void setGradeEquivalentRelationship(org.cocktail.mangue.modele.grhum.EOGrade value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOGrade oldValue = gradeEquivalent();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GRADE_EQUIVALENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GRADE_EQUIVALENT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces typeAcces() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces)storedValueForKey(TYPE_ACCES_KEY);
  }

  public void setTypeAccesRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeAcces oldValue = typeAcces();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACCES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACCES_KEY);
    }
  }
  

/**
 * Créer une instance de EOEquivAncCorps avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEquivAncCorps createEOEquivAncCorps(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String temPromouvabilite
, String temTtTypeAcces
, org.cocktail.mangue.modele.grhum.EOCorps corpsDepart			) {
    EOEquivAncCorps eo = (EOEquivAncCorps) createAndInsertInstance(editingContext, _EOEquivAncCorps.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemPromouvabilite(temPromouvabilite);
		eo.setTemTtTypeAcces(temTtTypeAcces);
    eo.setCorpsDepartRelationship(corpsDepart);
    return eo;
  }

  
	  public EOEquivAncCorps localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEquivAncCorps)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEquivAncCorps creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEquivAncCorps creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOEquivAncCorps object = (EOEquivAncCorps)createAndInsertInstance(editingContext, _EOEquivAncCorps.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEquivAncCorps localInstanceIn(EOEditingContext editingContext, EOEquivAncCorps eo) {
    EOEquivAncCorps localInstance = (eo == null) ? null : (EOEquivAncCorps)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEquivAncCorps#localInstanceIn a la place.
   */
	public static EOEquivAncCorps localInstanceOf(EOEditingContext editingContext, EOEquivAncCorps eo) {
		return EOEquivAncCorps.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEquivAncCorps fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEquivAncCorps fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEquivAncCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEquivAncCorps)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEquivAncCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEquivAncCorps fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEquivAncCorps eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEquivAncCorps)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEquivAncCorps fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEquivAncCorps eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEquivAncCorps ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEquivAncCorps fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

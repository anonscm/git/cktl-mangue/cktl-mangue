// EOTypeGestionArrete.java
// Created on Fri Nov 04 09:52:37 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele.grhum;

import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOTypeGestionArrete extends _EOTypeGestionArrete {
	
	public static String 	TYPE_ARRETE_AVANCEMENT = "AVANC_ECHE";
	private static String 	TYPE_COMPETENCE_ETABLISSEMENT = "E";
	
	public EOTypeGestionArrete() {
		super();
	}

	public boolean peutImprimerArrete() {
		return temProdEtab() != null && temProdEtab().equals(CocktailConstantes.VRAI);
	}
	public boolean gereSignature() {
		return temSignEtab() != null && temSignEtab().equals(CocktailConstantes.VRAI);
	}

	/**
	 * recherche le type de gestion d'arrete de conge selon le nom de la table
	 * 
	 * @param editingContext
	 * @param nomTableConge
	 * @param typePopulation
	 * @return
	 */
	public static EOTypeGestionArrete rechercherTypeGestionArretePourTypePopulation(EOEditingContext edc,String nomTableConge,EOTypePopulation typePopulation) {

		try {

			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGestCgmodPop.TYPE_POPULATION_KEY + " = %@", new NSArray(typePopulation)));
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOGestCgmodPop.NOM_TABLE_CGMOD_KEY + " = %@", new NSArray(nomTableConge)));

			return EOGestCgmodPop.fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers), null).typeGestionArrete();
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 *     
	 * @param editingContext
	 * @return
	 */
	public static EOTypeGestionArrete competenceEtablissement(EOEditingContext edc) {

		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOTypeGestionArrete.C_GESTION_ARRETE_KEY + " = %@", new NSArray(TYPE_COMPETENCE_ETABLISSEMENT));
			return fetchFirstByQualifier(edc, qualifier, null);
		}
		catch (Exception e) {
			return null;
		}
	}

}

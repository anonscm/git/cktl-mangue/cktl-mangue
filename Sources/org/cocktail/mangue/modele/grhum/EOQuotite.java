/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.grhum;

import java.math.BigDecimal;

import org.cocktail.mangue.common.utilities.ManGUEConstantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOQuotite extends _EOQuotite {

	public static final EOSortOrdering SORT_QUOT_R_ASC = new EOSortOrdering(NUM_QUOTITE_KEY, EOSortOrdering.CompareAscending);


	public EOQuotite() {
		super();
	}

	public String toString() {
		return numQuotiteRemunR().toString();
	}

	/**
	 * 
	 * @param quotite
	 * @return
	 */
	public static BigDecimal getQuotiteFinanciere(BigDecimal quotite) {

		if (quotite.compareTo(ManGUEConstantes.QUOTITE_TRAVAIL_90) == 0)
			return ManGUEConstantes.QUOTITE_FINANCIERE_90;
		else if (quotite.compareTo(ManGUEConstantes.QUOTITE_TRAVAIL_80) == 0)
			return ManGUEConstantes.QUOTITE_FINANCIERE_80;

		return quotite;
	}

	/**
	 * 
	 * @param ec
	 * @param quotite
	 * @return
	 */
	public static BigDecimal quotiteFinanciere(EOEditingContext ec, BigDecimal quotite) {
		BigDecimal quotiteFinanciere = null;
		try {
			quotiteFinanciere = (findForQuotite(ec, quotite.intValue())).quotiteFinanciere();
			if (quotiteFinanciere == null)
				quotiteFinanciere = quotite;
		}
		catch (Exception e) {
			quotiteFinanciere = quotite;
		}

		return quotiteFinanciere;

	}


	/**
	 * 
	 * @param ec
	 * @param quotite
	 * @return
	 */
	public static EOQuotite findForQuotite(EOEditingContext ec, Integer quotite) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NUM_QUOTITE_KEY +" = %@", new NSArray(quotite)));
		return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));    	
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray quotitesTempsPartiel(EOEditingContext ec) {

		NSMutableArray qualifiers = new NSMutableArray();

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NUM_QUOTITE_REMUN_R_KEY + " !=nil", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(NUM_QUOTITE_REMUN_R_KEY + " < %@",new NSArray(new Integer(100))));

		return fetchAll(ec, new EOAndQualifier(qualifiers), new NSArray(SORT_QUOT_R_ASC));

	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

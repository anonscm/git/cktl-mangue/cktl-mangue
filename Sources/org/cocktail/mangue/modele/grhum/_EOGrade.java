// _EOGrade.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGrade.java instead.
package org.cocktail.mangue.modele.grhum;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public abstract class _EOGrade extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Grade";
	public static final String ENTITY_TABLE_NAME = "GRHUM.GRADE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cGrade";

	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_GRADE_KEY = "cGrade";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_FERMETURE_KEY = "dFermeture";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_OUVERTURE_KEY = "dOuverture";
	public static final String ECHELLE_KEY = "echelle";
	public static final String IRM_GRADE_KEY = "irmGrade";
	public static final String LC_GRADE_KEY = "lcGrade";
	public static final String LL_GRADE_KEY = "llGrade";
	public static final String TEM_LOCAL_KEY = "temLocal";

// Attributs non visibles
	public static final String TEMOIN_NOTE_KEY = "temoinNote";
	public static final String C_CORPS_KEY = "cCorps";

//Colonnes dans la base de donnees
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_GRADE_COLKEY = "C_GRADE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_FERMETURE_COLKEY = "D_FERMETURE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_OUVERTURE_COLKEY = "D_OUVERTURE";
	public static final String ECHELLE_COLKEY = "ECHELLE";
	public static final String IRM_GRADE_COLKEY = "IRM_GRADE";
	public static final String LC_GRADE_COLKEY = "LC_GRADE";
	public static final String LL_GRADE_COLKEY = "LL_GRADE";
	public static final String TEM_LOCAL_COLKEY = "TEM_LOCAL";

	public static final String TEMOIN_NOTE_COLKEY = "TEMOIN_NOTE";
	public static final String C_CORPS_COLKEY = "C_CORPS";


	// Relationships
//	public static final String PASSAGE_ECHELONS1_KEY = "passageEchelons1";
//	public static final String PASSAGE_ECHELONS2_KEY = "passageEchelons2";
	public static final String TO_CORPS_KEY = "toCorps";
	public static final String TO_CATEGORIE_KEY = "toCategorie";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cCategorie() {
    return (String) storedValueForKey(C_CATEGORIE_KEY);
  }

  public void setCCategorie(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_KEY);
  }

  public String cGrade() {
    return (String) storedValueForKey(C_GRADE_KEY);
  }

  public void setCGrade(String value) {
    takeStoredValueForKey(value, C_GRADE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dFermeture() {
    return (NSTimestamp) storedValueForKey(D_FERMETURE_KEY);
  }

  public void setDFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, D_FERMETURE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dOuverture() {
    return (NSTimestamp) storedValueForKey(D_OUVERTURE_KEY);
  }

  public void setDOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, D_OUVERTURE_KEY);
  }

  public String echelle() {
    return (String) storedValueForKey(ECHELLE_KEY);
  }

  public void setEchelle(String value) {
    takeStoredValueForKey(value, ECHELLE_KEY);
  }

  public Integer irmGrade() {
    return (Integer) storedValueForKey(IRM_GRADE_KEY);
  }

  public void setIrmGrade(Integer value) {
    takeStoredValueForKey(value, IRM_GRADE_KEY);
  }

  public String lcGrade() {
    return (String) storedValueForKey(LC_GRADE_KEY);
  }

  public void setLcGrade(String value) {
    takeStoredValueForKey(value, LC_GRADE_KEY);
  }

  public String llGrade() {
    return (String) storedValueForKey(LL_GRADE_KEY);
  }

  public void setLlGrade(String value) {
    takeStoredValueForKey(value, LL_GRADE_KEY);
  }

  public String temLocal() {
    return (String) storedValueForKey(TEM_LOCAL_KEY);
  }

  public void setTemLocal(String value) {
    takeStoredValueForKey(value, TEM_LOCAL_KEY);
  }

  public org.cocktail.mangue.modele.grhum.EOCorps toCorps() {
    return (org.cocktail.mangue.modele.grhum.EOCorps)storedValueForKey(TO_CORPS_KEY);
  }

  public void setToCorpsRelationship(org.cocktail.mangue.modele.grhum.EOCorps value) {
    if (value == null) {
    	org.cocktail.mangue.modele.grhum.EOCorps oldValue = toCorps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CORPS_KEY);
    }
  }
	public org.cocktail.mangue.common.modele.nomenclatures.EOCategorie toCategorie() {
		return (org.cocktail.mangue.common.modele.nomenclatures.EOCategorie)storedValueForKey(TO_CATEGORIE_KEY);
	}

	public void setToCategorieRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOCategorie value) {
		if (value == null) {
			org.cocktail.mangue.common.modele.nomenclatures.EOCategorie oldValue = toCategorie();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CATEGORIE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_CATEGORIE_KEY);
		}
	}

//  public NSArray passageEchelons1() {
//    return (NSArray)storedValueForKey(PASSAGE_ECHELONS1_KEY);
//  }
//
//  public NSArray passageEchelons1(EOQualifier qualifier) {
//    return passageEchelons1(qualifier, null, false);
//  }
//
//  public NSArray passageEchelons1(EOQualifier qualifier, boolean fetch) {
//    return passageEchelons1(qualifier, null, fetch);
//  }
//
//  public NSArray passageEchelons1(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
//    NSArray results;
//    if (fetch) {
//      EOQualifier fullQualifier;
//      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.grhum.EOPassageEchelon.GRADE_KEY, EOQualifier.QualifierOperatorEqual, this);
//    	
//      if (qualifier == null) {
//        fullQualifier = inverseQualifier;
//      }
//      else {
//        NSMutableArray qualifiers = new NSMutableArray();
//        qualifiers.addObject(qualifier);
//        qualifiers.addObject(inverseQualifier);
//        fullQualifier = new EOAndQualifier(qualifiers);
//      }
//
//      results = org.cocktail.mangue.modele.grhum.EOPassageEchelon.fetchAll(editingContext(), fullQualifier, sortOrderings);
//    }
//    else {
//      results = passageEchelons1();
//      if (qualifier != null) {
//        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
//      }
//      if (sortOrderings != null) {
//        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
//      }
//    }
//    return results;
//  }
//  
//  public void addToPassageEchelons1Relationship(org.cocktail.mangue.modele.grhum.EOPassageEchelon object) {
//    addObjectToBothSidesOfRelationshipWithKey(object, PASSAGE_ECHELONS1_KEY);
//  }
//
//  public void removeFromPassageEchelons1Relationship(org.cocktail.mangue.modele.grhum.EOPassageEchelon object) {
//    removeObjectFromBothSidesOfRelationshipWithKey(object, PASSAGE_ECHELONS1_KEY);
//  }
//
//  public org.cocktail.mangue.modele.grhum.EOPassageEchelon createPassageEchelons1Relationship() {
//    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PassageEchelon");
//    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
//    editingContext().insertObject(eo);
//    addObjectToBothSidesOfRelationshipWithKey(eo, PASSAGE_ECHELONS1_KEY);
//    return (org.cocktail.mangue.modele.grhum.EOPassageEchelon) eo;
//  }
//
//  public void deletePassageEchelons1Relationship(org.cocktail.mangue.modele.grhum.EOPassageEchelon object) {
//    removeObjectFromBothSidesOfRelationshipWithKey(object, PASSAGE_ECHELONS1_KEY);
//    editingContext().deleteObject(object);
//  }
//
//  public void deleteAllPassageEchelons1Relationships() {
//    Enumeration objects = passageEchelons1().immutableClone().objectEnumerator();
//    while (objects.hasMoreElements()) {
//      deletePassageEchelons1Relationship((org.cocktail.mangue.modele.grhum.EOPassageEchelon)objects.nextElement());
//    }
//  }
//
//  public NSArray passageEchelons2() {
//    return (NSArray)storedValueForKey(PASSAGE_ECHELONS2_KEY);
//  }
//
//  public NSArray passageEchelons2(EOQualifier qualifier) {
//    return passageEchelons2(qualifier, null, false);
//  }
//
//  public NSArray passageEchelons2(EOQualifier qualifier, boolean fetch) {
//    return passageEchelons2(qualifier, null, fetch);
//  }
//
//  public NSArray passageEchelons2(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
//    NSArray results;
//    if (fetch) {
//      EOQualifier fullQualifier;
//      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.grhum.EOPassageEchelon.GRADE_KEY, EOQualifier.QualifierOperatorEqual, this);
//    	
//      if (qualifier == null) {
//        fullQualifier = inverseQualifier;
//      }
//      else {
//        NSMutableArray qualifiers = new NSMutableArray();
//        qualifiers.addObject(qualifier);
//        qualifiers.addObject(inverseQualifier);
//        fullQualifier = new EOAndQualifier(qualifiers);
//      }
//
//      results = org.cocktail.mangue.modele.grhum.EOPassageEchelon.fetchAll(editingContext(), fullQualifier, sortOrderings);
//    }
//    else {
//      results = passageEchelons2();
//      if (qualifier != null) {
//        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
//      }
//      if (sortOrderings != null) {
//        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
//      }
//    }
//    return results;
//  }
//  
//  public void addToPassageEchelons2Relationship(org.cocktail.mangue.modele.grhum.EOPassageEchelon object) {
//    addObjectToBothSidesOfRelationshipWithKey(object, PASSAGE_ECHELONS2_KEY);
//  }
//
//  public void removeFromPassageEchelons2Relationship(org.cocktail.mangue.modele.grhum.EOPassageEchelon object) {
//    removeObjectFromBothSidesOfRelationshipWithKey(object, PASSAGE_ECHELONS2_KEY);
//  }
//
//  public org.cocktail.mangue.modele.grhum.EOPassageEchelon createPassageEchelons2Relationship() {
//    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PassageEchelon");
//    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
//    editingContext().insertObject(eo);
//    addObjectToBothSidesOfRelationshipWithKey(eo, PASSAGE_ECHELONS2_KEY);
//    return (org.cocktail.mangue.modele.grhum.EOPassageEchelon) eo;
//  }
//
//  public void deletePassageEchelons2Relationship(org.cocktail.mangue.modele.grhum.EOPassageEchelon object) {
//    removeObjectFromBothSidesOfRelationshipWithKey(object, PASSAGE_ECHELONS2_KEY);
//    editingContext().deleteObject(object);
//  }
//
//  public void deleteAllPassageEchelons2Relationships() {
//    Enumeration objects = passageEchelons2().immutableClone().objectEnumerator();
//    while (objects.hasMoreElements()) {
//      deletePassageEchelons2Relationship((org.cocktail.mangue.modele.grhum.EOPassageEchelon)objects.nextElement());
//    }
//  }


/**
 * Créer une instance de EOGrade avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOGrade createEOGrade(EOEditingContext editingContext, String cGrade
, NSTimestamp dCreation
, NSTimestamp dModification
, String lcGrade
, String llGrade
			) {
    EOGrade eo = (EOGrade) createAndInsertInstance(editingContext, _EOGrade.ENTITY_NAME);    
		eo.setCGrade(cGrade);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLcGrade(lcGrade);
		eo.setLlGrade(llGrade);
    return eo;
  }

  
	  public EOGrade localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGrade)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGrade creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOGrade creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOGrade object = (EOGrade)createAndInsertInstance(editingContext, _EOGrade.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOGrade localInstanceIn(EOEditingContext editingContext, EOGrade eo) {
    EOGrade localInstance = (eo == null) ? null : (EOGrade)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOGrade#localInstanceIn a la place.
   */
	public static EOGrade localInstanceOf(EOEditingContext editingContext, EOGrade eo) {
		return EOGrade.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOGrade fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOGrade fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGrade eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGrade)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGrade fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGrade fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGrade eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGrade)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOGrade fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGrade eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGrade ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGrade fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

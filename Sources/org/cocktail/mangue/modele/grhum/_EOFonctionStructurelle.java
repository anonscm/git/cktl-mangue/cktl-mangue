// _EOFonctionStructurelle.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFonctionStructurelle.java instead.
package org.cocktail.mangue.modele.grhum;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public abstract class _EOFonctionStructurelle extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "FonctionStructurelle";
	public static final String ENTITY_TABLE_NAME = "GRHUM.FONCTION_STRUCTURELLE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cFonction";

	public static final String LC_FONCTION_KEY = "lcFonction";
	public static final String LL_FONCTION_KEY = "llFonction";
	public static final String TEM_AVANCEMENT_SPEC_KEY = "temAvancementSpec";
	public static final String TEM_LOCAL_KEY = "temLocal";
	public static final String TEM_RESP_PRINC_KEY = "temRespPrinc";
	public static final String TITRE_KEY = "titre";

// Attributs non visibles
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String C_TYPE_PRIME_KEY = "cTypePrime";
	public static final String C_TYPE_DECHARGE_KEY = "cTypeDecharge";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_FONCTION_KEY = "cFonction";

//Colonnes dans la base de donnees
	public static final String LC_FONCTION_COLKEY = "LC_FONCTION";
	public static final String LL_FONCTION_COLKEY = "LL_FONCTION";
	public static final String TEM_AVANCEMENT_SPEC_COLKEY = "TEM_AVANCEMENT_SPEC";
	public static final String TEM_LOCAL_COLKEY = "TEM_LOCAL";
	public static final String TEM_RESP_PRINC_COLKEY = "TEM_RESP_PRINC";
	public static final String TITRE_COLKEY = "TITRE";

	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String C_TYPE_PRIME_COLKEY = "C_TYPE_PRIME";
	public static final String C_TYPE_DECHARGE_COLKEY = "C_TYPE_DECHARGE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_FONCTION_COLKEY = "C_FONCTION";


	// Relationships
	public static final String CATEGORIE_KEY = "categorie";
	public static final String TYPE_DECHARGE_KEY = "typeDecharge";
	public static final String TYPE_PRIME_KEY = "typePrime";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String lcFonction() {
    return (String) storedValueForKey(LC_FONCTION_KEY);
  }

  public void setLcFonction(String value) {
    takeStoredValueForKey(value, LC_FONCTION_KEY);
  }

  public String llFonction() {
    return (String) storedValueForKey(LL_FONCTION_KEY);
  }

  public void setLlFonction(String value) {
    takeStoredValueForKey(value, LL_FONCTION_KEY);
  }

  public String temAvancementSpec() {
    return (String) storedValueForKey(TEM_AVANCEMENT_SPEC_KEY);
  }

  public void setTemAvancementSpec(String value) {
    takeStoredValueForKey(value, TEM_AVANCEMENT_SPEC_KEY);
  }

  public String temLocal() {
    return (String) storedValueForKey(TEM_LOCAL_KEY);
  }

  public void setTemLocal(String value) {
    takeStoredValueForKey(value, TEM_LOCAL_KEY);
  }

  public String temRespPrinc() {
    return (String) storedValueForKey(TEM_RESP_PRINC_KEY);
  }

  public void setTemRespPrinc(String value) {
    takeStoredValueForKey(value, TEM_RESP_PRINC_KEY);
  }

  public String titre() {
    return (String) storedValueForKey(TITRE_KEY);
  }

  public void setTitre(String value) {
    takeStoredValueForKey(value, TITRE_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.EOCategorieFonction categorie() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOCategorieFonction)storedValueForKey(CATEGORIE_KEY);
  }

  public void setCategorieRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOCategorieFonction value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOCategorieFonction oldValue = categorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATEGORIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATEGORIE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypeDechargeService typeDecharge() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypeDechargeService)storedValueForKey(TYPE_DECHARGE_KEY);
  }

  public void setTypeDechargeRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypeDechargeService value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypeDechargeService oldValue = typeDecharge();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_DECHARGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_DECHARGE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EOTypePrime typePrime() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EOTypePrime)storedValueForKey(TYPE_PRIME_KEY);
  }

  public void setTypePrimeRelationship(org.cocktail.mangue.common.modele.nomenclatures.EOTypePrime value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EOTypePrime oldValue = typePrime();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PRIME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PRIME_KEY);
    }
  }
  

/**
 * Créer une instance de EOFonctionStructurelle avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFonctionStructurelle createEOFonctionStructurelle(EOEditingContext editingContext, String lcFonction
, String llFonction
, String temAvancementSpec
, String temLocal
			) {
    EOFonctionStructurelle eo = (EOFonctionStructurelle) createAndInsertInstance(editingContext, _EOFonctionStructurelle.ENTITY_NAME);    
		eo.setLcFonction(lcFonction);
		eo.setLlFonction(llFonction);
		eo.setTemAvancementSpec(temAvancementSpec);
		eo.setTemLocal(temLocal);
    return eo;
  }

  
	  public EOFonctionStructurelle localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFonctionStructurelle)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFonctionStructurelle creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFonctionStructurelle creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOFonctionStructurelle object = (EOFonctionStructurelle)createAndInsertInstance(editingContext, _EOFonctionStructurelle.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOFonctionStructurelle localInstanceIn(EOEditingContext editingContext, EOFonctionStructurelle eo) {
    EOFonctionStructurelle localInstance = (eo == null) ? null : (EOFonctionStructurelle)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFonctionStructurelle#localInstanceIn a la place.
   */
	public static EOFonctionStructurelle localInstanceOf(EOEditingContext editingContext, EOFonctionStructurelle eo) {
		return EOFonctionStructurelle.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFonctionStructurelle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFonctionStructurelle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFonctionStructurelle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFonctionStructurelle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFonctionStructurelle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFonctionStructurelle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFonctionStructurelle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFonctionStructurelle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFonctionStructurelle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFonctionStructurelle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFonctionStructurelle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFonctionStructurelle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

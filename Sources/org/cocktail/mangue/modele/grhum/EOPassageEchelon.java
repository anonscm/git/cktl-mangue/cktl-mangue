/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.grhum;

import java.util.Enumeration;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOPassageEchelon extends _EOPassageEchelon {

	public static EOSortOrdering SORT_C_ECHELON_ASC = new EOSortOrdering(C_ECHELON_KEY, EOSortOrdering.CompareAscending);
	public static NSArray SORT_ARRAY_C_ECHELON_ASC = new NSArray(SORT_C_ECHELON_ASC);

	public static final Integer[] LISTE_PASSAGE_ANNEES = new Integer[]{new Integer(1), new Integer(2), new Integer(3), new Integer(4), new Integer(5),new Integer(6)};
	public static final Integer[] LISTE_PT_CHOIX_ANNEES = new Integer[]{new Integer(1), new Integer(2), new Integer(3), new Integer(4), new Integer(5),new Integer(6)};
	public static final Integer[] LISTE_GD_CHOIX_ANNEES = new Integer[]{new Integer(1), new Integer(2), new Integer(3), new Integer(4), new Integer(5),new Integer(6)};

	public static final Integer[] LISTE_PASSAGE_MOIS = new Integer[]{new Integer(3), new Integer(4),new Integer(5), new Integer(6), new Integer(7),new Integer(8),new Integer(9),new Integer(10)};
	public static final Integer[] LISTE_PT_CHOIX_MOIS = new Integer[]{new Integer(1),new Integer(3), new Integer(4),new Integer(6), new Integer(7),new Integer(9),new Integer(10)};
	public static final Integer[] LISTE_GD_CHOIX_MOIS = new Integer[]{new Integer(1),new Integer(3), new Integer(4),new Integer(6), new Integer(7),new Integer(9),new Integer(10)};

	public static String INDICE_MAJORE_KEY = "indiceMajore";

	public EOPassageEchelon() {
		super();
	}

	
	public static EOPassageEchelon creer(EOEditingContext ec, EOGrade grade) {

		EOPassageEchelon newObject = (EOPassageEchelon) createAndInsertInstance(ec, ENTITY_NAME);    
		newObject.setGradeRelationship(grade);
		newObject.setCGrade(grade.cGrade());
		newObject.setTemLocal(CocktailConstantes.VRAI);
		return newObject;
	}

	public Number indiceMajore() {
		return EOIndice.indiceMajorePourIndiceBrutEtDate(editingContext(), cIndiceBrut(), dOuverture()).cIndiceMajore();
	}
	public Boolean isLocal() {
		return true;//"O".equals(temLocal());
	}

	public String libellePassage() {    	

		String libelle = "";
		if (dureePassageAnnees() != null)
			libelle += dureePassageAnnees() + " An" + ((dureePassageAnnees().intValue()>1)?"s":"");
		if ( dureePassageMois() != null)
			libelle += " " + dureePassageMois() + " Mois";

		return libelle;
	}
	public String libellePetitChoix() {    	

		String libelle = "";
		if (dureePtChoixAnnees() != null)
			libelle += dureePtChoixAnnees() + " An" + ((dureePtChoixAnnees().intValue()>1)?"s":"");
		if ( dureePtChoixMois() != null)
			libelle += " " + dureePtChoixMois() + " Mois";

		return libelle;
	}
	public String libelleGrandChoix() {    	
		String libelle = "";
		if (dureeGrChoixAnnees() != null)
			libelle += dureeGrChoixAnnees() + " An" + ((dureeGrChoixAnnees().intValue()>1)?"s":"");
		if ( dureeGrChoixMois() != null)
			libelle += " " + dureeGrChoixMois() + " Mois";

		return libelle;
	}

	public String dateDebutFormatee() {
		return SuperFinder.dateFormatee(this,D_OUVERTURE_KEY);
	}
	public void setDateDebutFormatee(String uneDate) {
		if (uneDate == null || uneDate.equals("")) {
			setDOuverture(null);
		} else {
			SuperFinder.setDateFormatee(this,D_OUVERTURE_KEY,uneDate);
		}
	}
	public String dateFinFormatee() {
		return SuperFinder.dateFormatee(this,D_FERMETURE_KEY);
	}
	public void setDateFinFormatee(String uneDate) {
		if (uneDate == null || uneDate.equals("")) {
			setDFermeture(null);
		} else {
			SuperFinder.setDateFormatee(this,D_FERMETURE_KEY,uneDate);
		}
	}
	public void initAvecGrade(EOGrade grade) {
		setCGrade(grade.cGrade());
	}
	public void validateForSave() throws NSValidation.ValidationException {

		if (grade() == null) {
			throw new NSValidation.ValidationException("Le grade doit être fourni");
		}
		if (cEchelon() == null) {
			throw new NSValidation.ValidationException("L'échelon doit être fourni");
		}
		if (dOuverture() == null) {
			throw new NSValidation.ValidationException("La date d'ouverture doit être fournie");
		} else if (dFermeture() != null && DateCtrl.isBefore(dFermeture(), dOuverture())) {
			throw new NSValidation.ValidationException("La date d'ouverture ne peut être postérieure à la date de fermeture");
		}
		
		if (dCreation() == null)
			setDCreation(new NSTimestamp());
		setDModification(new NSTimestamp());

	}


	/*	public boolean estDernierEchelon() {
		EOPassageEchelon dernierEchelon = (EOPassageEchelon)rechercherPassagesEchelonPourGradeAvecTri(editingContext(),cGrade(),true).objectAtIndex(0);
		return (dernierEchelon.cEchelon().equals(cEchelon()));
	}*/

	public static NSArray<EOPassageEchelon> rechercherPourGrade(EOEditingContext ec,EOGrade grade) {

		NSMutableArray qualifiers = new NSMutableArray();				

		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(new EOSortOrdering(D_FERMETURE_KEY, EOSortOrdering.CompareDescending));
		sorts.addObject(new EOSortOrdering(C_ECHELON_KEY, EOSortOrdering.CompareAscending));

		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(GRADE_KEY + "=%@", new NSArray(grade)));

		return fetchAll(ec, new EOAndQualifier(qualifiers), sorts);

	}


	/** recherche les passages echelon associes a une liste de grades
	 * @param editingContext
	 * @param grades tableau de EOGrade
	 * @param shouldRefresh pour rafraichir les donnees
	 */
	public static NSArray<EOPassageEchelon> rechercherPassagesEchelonPourGrade(EOEditingContext editingContext,NSArray grades, boolean shouldRefresh) {

		Enumeration e = grades.objectEnumerator();
		NSMutableArray qualifiers = new NSMutableArray();
		while (e.hasMoreElements()){
			EOGrade grade = (EOGrade)e.nextElement();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat( C_GRADE_KEY  + "= '" + grade.cGrade() + "'",null));
		}

		EOFetchSpecification myFetch = new EOFetchSpecification (ENTITY_NAME, new EOOrQualifier(qualifiers),null);
		myFetch.setRefreshesRefetchedObjects(shouldRefresh);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}


	/** Recherche les passages echelons valides lies a un grade valides pendant la periode
	 * @param editingContext
	 * @param grade
	 * @param dateReference peut etre nulle
	 */
	public static NSArray<EOPassageEchelon> rechercherPassagesEchelonPourGradesValidesPourPeriode(EOEditingContext ec, EOGrade grade,NSTimestamp debutPeriode,NSTimestamp finPeriode) {
		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_GRADE_KEY + " = %@",new NSArray(grade.cGrade())));
			if (debutPeriode == null) {
				if (finPeriode == null) {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY + " = nil)",null));
				} else {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY + " = nil OR dFermeture >= %@)",new NSArray(finPeriode)));
				}
			} else {
				qualifiers.addObject(SuperFinder.qualifierPourPeriode(D_OUVERTURE_KEY, debutPeriode, D_FERMETURE_KEY, finPeriode));
			}
			
			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_C_ECHELON_ASC );		
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	/** Recherche les passages echelon  pour un grade donne */
	public static NSArray<EOPassageEchelon> rechercherPassagesEchelonPourGradeAvecTri(EOEditingContext ec,String grade,NSTimestamp dateReference,boolean enOrdreDecroissant) {
		return rechercherPassageEchelonOuvertPourGradeEtEchelon(ec,grade,null,dateReference,enOrdreDecroissant);
	}

	/**
	 * 
	 * @param edc
	 * @param dateDebut
	 * @param dateFin
	 * @return
	 */
	public static NSArray<EOPassageEchelon> findForGradeEchelonAndPeriode(EOEditingContext edc, EOGrade grade, String echelon, NSTimestamp dateDebut, NSTimestamp dateFin) {
		
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(GRADE_KEY, grade));
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(C_ECHELON_KEY, echelon));
		
		andQualifiers.addObject(SuperFinder.qualifierPourPeriode(D_OUVERTURE_KEY, dateDebut, D_FERMETURE_KEY, dateFin));
		
		return fetchAll(edc, new EOAndQualifier(andQualifiers), null);
	}
	
	/** Recherche les passages echelon pour un grade et un echelon donnes */
	public static NSArray<EOPassageEchelon> rechercherPassageEchelonOuvertPourGradeEtEchelon(EOEditingContext editingContext,String grade,String echelon,NSTimestamp dateReference,boolean enOrdreDecroissant) {
		NSMutableArray args = new NSMutableArray(grade);
		String qualifier = C_GRADE_KEY + "= %@";
		if (echelon != null) {
			args.addObject(echelon);
			qualifier = qualifier + " AND cEchelon = %@";
		}
		if (dateReference != null) {
			args.addObject(dateReference);
			qualifier = qualifier + " AND (dFermeture = nil OR dFermeture >= %@)";
		} else {
			qualifier = qualifier + " AND dFermeture = nil";
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(qualifier ,args);
		// on ne fait pas le tri par la base de données car il ne renvoie pas les classements selon l'ordre alphabétique
		// avec les numériques en tête correctement (du moins avec Oracle)
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qual,null);
		NSArray results = editingContext.objectsWithFetchSpecification(myFetch);
		NSArray sorts = null;
		if (enOrdreDecroissant) {
			sorts = new NSArray (new EOSortOrdering(C_ECHELON_KEY,EOSortOrdering.CompareDescending));
		} else {
			sorts = new NSArray (new EOSortOrdering(C_ECHELON_KEY,EOSortOrdering.CompareAscending));
		}
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sorts);
	}
	/** Recherche le passage echelon pour un grade et un echelon donnes 
	 * @param editingContext
	 * @param grade recherche
	 * @param indiceBrut indice brut recherche
	 */
	public static EOPassageEchelon echelonPourIndice(EOEditingContext editingContext,String grade,String indiceBrut) {
		NSMutableArray args = new NSMutableArray(grade);
		args.addObject(indiceBrut);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("cGrade =   %@ AND dFermeture = nil AND cIndiceBrut = %@" ,args);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qual,null);
		try {
			return (EOPassageEchelon)editingContext.objectsWithFetchSpecification(myFetch).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	/** Recherche les passage echelon pour un indice donne
	 * @param editingContext
	 * @param indice recherch&eacute;
	 * @param estMajore true si rechercher un indice major&eacute; */
	public static NSArray rechercherPassageEchelonPourIndice(EOEditingContext editingContext,NSTimestamp date,String indice,boolean estMajore) {
		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY + " = nil", null));
		if (estMajore) {
			// Rechercher les indices
			NSArray indices = EOIndice.indicesPourIndiceMajoreEtDate(editingContext, new Integer(indice), date);
			if (indices != null && indices.count() > 0) {
				qualifiers.addObject(SuperFinder.construireORQualifier(C_INDICE_BRUT_KEY, (NSArray)indices.valueForKey("cIndiceBrut")));
			}
		} else 
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_INDICE_BRUT_KEY + " = %@", new NSArray(indice)));

		EOQualifier qual = new EOAndQualifier(qualifiers);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qual,null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

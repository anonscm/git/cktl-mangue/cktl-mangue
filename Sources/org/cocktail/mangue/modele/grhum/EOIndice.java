/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.modele.grhum;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.SuperFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOIndice extends _EOIndice {

	public static final EOSortOrdering SORT_KEY_BRUT_ASC = new EOSortOrdering(C_INDICE_BRUT_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_KEY_BRUT_DESC = new EOSortOrdering(C_INDICE_BRUT_KEY, EOSortOrdering.CompareDescending);
	public static final EOSortOrdering SORT_KEY_MAJORE_ASC = new EOSortOrdering(C_INDICE_MAJORE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_KEY_MAJORE_DESC = new EOSortOrdering(C_INDICE_MAJORE_KEY, EOSortOrdering.CompareDescending);

	public static final NSArray SORT_ARRAY_BRUT_ASC = new NSArray(SORT_KEY_BRUT_ASC);
	public static final NSArray SORT_ARRAY_BRUT_DESC = new NSArray(SORT_KEY_BRUT_DESC);

	public EOIndice() {
		super();
	}

	public boolean isLocal() {
		return temLocal().equals("O");
	}

	public static EOIndice creer(EOEditingContext ec) {

		EOIndice newObject = (EOIndice) createAndInsertInstance(ec, ENTITY_NAME);    

		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());
		newObject.setTemLocal("O");

		return newObject;
	}

	public String dateMajorationFormatee() {
		return SuperFinder.dateFormatee(this,D_MAJORATION_KEY);
	}
	public void setDateMajorationFormatee(String uneDate) {
		if (uneDate == null) {
			setDMajoration(null);
		} else {
			SuperFinder.setDateFormatee(this,D_MAJORATION_KEY,uneDate);
		}
	}
	public String dateFermetureFormatee() {
		return SuperFinder.dateFormatee(this,D_MAJORATION_KEY);
	}
	public void setDateFermetureFormatee(String uneDate) {
		if (uneDate == null) {
			setDFermeture(null);
		} else {
			SuperFinder.setDateFormatee(this,D_FERMETURE_KEY,uneDate);
		}
	}
	public void initAvecIndice(EOIndice indice) {
		setCIndiceBrut(indice.cIndiceBrut());
		if (indice.dFermeture() != null) {
			setDMajoration(DateCtrl.jourSuivant(indice.dFermeture()));
		}
	}
	public void validateForSave() throws NSValidation.ValidationException {
		if (cIndiceBrut() == null) {
			throw new NSValidation.ValidationException("L'indice brut est obligatoire");
		} else if (cIndiceBrut().length() > 4) {
			throw new NSValidation.ValidationException("L'indice brut comporte au maximum 4 caractères");
		}
		if (cIndiceMajore() == null) {
			throw new NSValidation.ValidationException("L'indice majoré est obligatoire");
		}
		if (dMajoration() == null) {
			throw new NSValidation.ValidationException("La date de majoration est obligatoire");
		}
		if (dFermeture() != null && DateCtrl.isBefore(dFermeture(),dMajoration())) {
			throw new NSValidation.ValidationException("La date de majoration ne peut être postérieure à la date de fermeture");
		}
		// Pas de chevauchement des indices
		EOIndice autreIndice = indiceMajorePourIndiceBrutEtDate(editingContext(), cIndiceBrut(), dMajoration());
		if (autreIndice != null && autreIndice != this) {
			// Dans le cas d'un renouvellement, le fetch peut retourner l'indice renouvellé dont la date a été modifié.
			// On vérifie donc la date de fermeture
			if (autreIndice.dFermeture() == null) {
				throw new NSValidation.ValidationException("Il existe une autre valeur de cet indice brut valide à cette date, modifiez sa date de fermeture");
			} else if (DateCtrl.isAfterEq(autreIndice.dFermeture(), dMajoration())) {
				throw new NSValidation.ValidationException("Il existe une autre valeur de cet indice brut valide à cette date");
			}
		}
		// Vérifier la contiguité des dates
		NSArray indices = rechercherIndicesAvecMemeIndiceBrut(editingContext(), cIndiceBrut());
		indices = EOSortOrdering.sortedArrayUsingKeyOrderArray(indices, new NSArray(EOSortOrdering.sortOrderingWithKey("dMajoration", EOSortOrdering.CompareDescending)));
		// On prend le premier indice qu'on rencontre qui a une date de début antérieure à celle de l'indice courant
		java.util.Enumeration e = indices.objectEnumerator();
		while (e.hasMoreElements()) {
			EOIndice indice = (EOIndice)e.nextElement();
			if (DateCtrl.isBefore(indice.dMajoration(), dMajoration())) {
				// Sa date de fin est nécessairement non nulle sinon on aurait eu l'exception sur les validations
				if (indice.dFermeture() != null && DateCtrl.isSameDay(DateCtrl.jourPrecedent(dMajoration()),indice.dFermeture()) == false) {
					throw new NSValidation.ValidationException("Les dates des indices associés à cet indice brut ne sont pas contigues");
				}
				break;	// on a terminé quoiqu'il arrive
			}
		}
	}


	/**
	 * 
	 * @param edc
	 * @param grade
	 * @param echelon
	 * @param date
	 * @return
	 */
	public static EOIndice indiceMajorePourGradeEtEchelon(EOEditingContext edc, EOGrade grade, String echelon, NSTimestamp dateReference) {
		if (grade != null && echelon  != null) {

			NSArray<EOPassageEchelon> passagesEchelon = EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(edc, grade.cGrade(), echelon, dateReference, false);
			if (passagesEchelon.size() > 0) {
				EOPassageEchelon passage = (EOPassageEchelon)passagesEchelon.objectAtIndex(0);
				return indiceMajorePourIndiceBrutEtDate(edc, passage.cIndiceBrut(), dateReference);
			}
		}
		return null;
	}

	/**
	 * 
	 * Retourne la valeur de l'indice majore pour un indice brut et une date donnee.
	 * 
	 * @param editingContext
	 * @param indiceBrut
	 * @param date
	 * @return
	 */
	public static EOIndice indiceMajorePourIndiceBrutEtDate(EOEditingContext editingContext,String indiceBrut,NSTimestamp date) {

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_INDICE_BRUT_KEY + " =%@", new NSArray(indiceBrut)));

			if (date == null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY+"= nil", null));
			else {
				NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_MAJORATION_KEY+"=nil", null));
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_MAJORATION_KEY+"<=%@", new NSArray(date)));

				qualifiers.addObject(new EOOrQualifier(orQualifiers));

				orQualifiers = new NSMutableArray<EOQualifier>();
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY+"=nil", null));
				orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(D_FERMETURE_KEY+">=%@", new NSArray(date)));

				qualifiers.addObject(new EOOrQualifier(orQualifiers));
			}    		

			return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));

		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 
	 */
	public static NSArray<EOIndice> findForMajoreAndPeriode(EOEditingContext edc, Number indiceMajore, NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		
		andQualifiers.addObject(CocktailFinder.getQualifierEqual(C_INDICE_MAJORE_KEY, indiceMajore));
		andQualifiers.addObject(SuperFinder.qualifierPourPeriode(D_MAJORATION_KEY, dateDebut, D_FERMETURE_KEY, dateFin));

		return fetchAll(edc, new EOAndQualifier(andQualifiers), SORT_ARRAY_BRUT_DESC);
	}
	

	/** Retourne les indices associes a l'indice majore valables a la date fournie. Retourne null si date est nulle ou si non trouve */
	public static NSArray<EOIndice> indicesPourIndiceMajoreEtDate(EOEditingContext editingContext,Number indiceMajore,NSTimestamp date) {
		if (date == null)
			return new NSArray();

		NSMutableArray args = new NSMutableArray(indiceMajore);
		args.addObject(date);
		args.addObject(date);
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(" cIndiceMajore = %@ AND (dMajoration = nil OR dMajoration <= %@) AND (dFermeture = nil OR dFermeture >= %@)", args);

		return fetchAll(editingContext, qualifier);

	}

	/** Recherche des indices pour un indice brut ou un indice major&eacute;.<BR>
	 * @param indiceBrut valeur de l'indice brut : peut &circ;tre nul
	 * @param indiceMajore valeur de l'indice majore : peut etre nul
	 * Retourne null si les deux valeurs sont nulles en m&ecirc;me temps
	 */
	public static NSArray rechercherIndices(EOEditingContext editingContext, String indiceBrut, Number indiceMajore) {
		if (indiceBrut == null && indiceMajore == null) {
			return null;
		}
		return rechercherIndices(editingContext, indiceBrut, indiceMajore,false);
	}
	/** Recherche des indices pour un indice brut identique.<BR>
	 * @param indiceBrut valeur de l'indice brut : ne peut pas  &circ;tre nul
	 * @param indiceMajore valeur de l'indice majore : peut etre nul
	 * Retourne null si l'indice brut est nul
	 */
	public static NSArray rechercherIndicesAvecMemeIndiceBrut(EOEditingContext editingContext, String indiceBrut) {
		if (indiceBrut == null) {
			return null;
		}
		return rechercherIndices(editingContext, indiceBrut, null,true);
	}

	/** Recherche des indices pour un indice brut identique<BR>
	 * @param indiceBrut valeur de l'indice brut : ne peut pas &circ;tre nul
	 */
	private static NSArray rechercherIndices(EOEditingContext editingContext, String indiceBrut, Number indiceMajore, boolean memeIndiceBrut) {
		if (indiceBrut == null && indiceMajore == null) {
			return null;
		}
		NSMutableArray qualifiers = new NSMutableArray();
		if (indiceBrut != null) {
			if (memeIndiceBrut) 
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_INDICE_BRUT_KEY + "= %@",new NSArray(indiceBrut)));
			else
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_INDICE_BRUT_KEY +" caseinsensitivelike %@",new NSArray(indiceBrut + "*")));
		}
		if (indiceMajore != null)
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_INDICE_MAJORE_KEY + " = %@",new NSArray(indiceMajore)));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers),null);

	}


	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele à partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}

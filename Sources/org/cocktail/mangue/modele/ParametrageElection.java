/*
 * Created on 20 mai 2005
 *
 * Classe Application client
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.elections.EOInstance;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public abstract class ParametrageElection extends EOGenericRecord {
	public ParametrageElection() {
		super();
	}
	public String temSelection() {
		return (String)storedValueForKey("temSelection");
	}
	public void setTemSelection(String value) {
		takeStoredValueForKey(value, "temSelection");
	}
	public EOInstance instance() {
		return (EOInstance)storedValueForKey("instance");
	}
	public void setInstance(EOInstance value) {
		takeStoredValueForKey(value, "instance");
	}
	// méthodes ajoutées
	public void initAvecInstance(EOInstance instance) {
		setTemSelection(CocktailConstantes.VRAI);
		addObjectToBothSidesOfRelationshipWithKey(instance,"instance");
	}
	public void supprimerRelations() {
		removeObjectFromBothSidesOfRelationshipWithKey(instance(), "instance");
	}
	public boolean estActif() {
		return temSelection() != null && temSelection().equals(CocktailConstantes.VRAI);
	}
	public void setEstActif(boolean aBool) {
		if (aBool) {
			setTemSelection(CocktailConstantes.VRAI);
		} else {
			setTemSelection(CocktailConstantes.FAUX);
		}
	}
	public void changerSelection() {
		if (temSelection() != null && temSelection().equals(CocktailConstantes.VRAI)) {
			setTemSelection(CocktailConstantes.FAUX);
		} else {
			setTemSelection(CocktailConstantes.VRAI);
		}
	}

	/** retourne les parametrages associes a l'instance
	 * @param editingContext
	 * @param nom de l'entite pour le parametrage
	 * @param instance
	 */
	public static NSArray rechercherParametragesPourInstance(EOEditingContext edc,String nomEntite,EOInstance instance) {
		if (instance == null) {
			return null;
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("instance = %@", new NSArray(instance));
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite, qualifier, null);
		return edc.objectsWithFetchSpecification(fs);
	}
	/** retourne les param&eacute;trages actifs associ&eacute;s &agrave; l'instance
	 * @param editingContext
	 * @param nom de l'entit&eacute; pour le param&eacute;trage
	 * @param instance
	 */
	public static NSArray rechercherParametragesActifsPourInstance(EOEditingContext editingContext,String nomEntite,EOInstance instance) {
		if (instance == null) {
			return null;
		}
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("instance = %@ AND temSelection = '" + CocktailConstantes.VRAI +"'", new NSArray(instance));
		EOFetchSpecification fs = new EOFetchSpecification(nomEntite,qualifier,null);
		return editingContext.objectsWithFetchSpecification(fs);
	}
}

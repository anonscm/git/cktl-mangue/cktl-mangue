/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;


/** Contient toutes les donnees pour l'edition des conges sous la forme de valeurs simples.<BR>
 * Supporte l'interface NSKeyValueCoding pour l'acces aux attributs et NSCoding pour le transfert des donnees entre le client et le serveur */
public class EvenementPourEdition extends Object implements NSKeyValueCoding, NSCoding {
	private NSMutableDictionary donnees;

	public EvenementPourEdition() {
		super();
		donnees = new NSMutableDictionary();
		// pour ne pas avoir de souci à l'export
		setPrenom("");
	}
	/** construit un conge avec un individu, un type de conge et une dure en jours */
	public EvenementPourEdition(EOEditingContext ec, EOIndividu individu,String typeEvenement,String motif, float duree,float dureePeriode,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		this();
		
		setToIndividu(individu);
		setNom(individu.nomUsuel());
				
		if (individu.prenom() != null)
			setPrenom(StringCtrl.capitalizedString(individu.prenom()));
		
		setType(typeEvenement);		
		setMotif(motif);		
		
		if (duree != 0) {
			setDuree(new Float(duree));
		}
		if (dureePeriode != 0) {
			setDureePeriode(new Float(dureePeriode));
		}
		if (debutPeriode != null) {
			setDateDebut(DateCtrl.dateToString(debutPeriode));
		}
		if (finPeriode != null) {
			setDateFin(DateCtrl.dateToString(finPeriode));
		}
				
	}
	/** constructeur pour encoding/decoding */
	public EvenementPourEdition(NSDictionary aDict) {
		donnees = new NSMutableDictionary(aDict);
	}
	
	public String nom() {
		return (String)donnees.objectForKey("nom");
	}
	public void setNom(String value) {
		if (value == null) {
			donnees.setObjectForKey("", "nom");
		} else {
			donnees.setObjectForKey(value, "nom");
		}
	}
	public String prenom() {
		return (String)donnees.objectForKey("prenom");
	}
	public void setPrenom(String value) {
		donnees.setObjectForKey(value, "prenom");
	}
	public String dateDebut() {
		return (String)donnees.objectForKey("dateDebut");
	}
	public void setDateDebut(String value) {
		donnees.setObjectForKey(value, "dateDebut");
	}
	public String dateFin() {
		return (String)donnees.objectForKey("dateFin");
	}
	public void setDateFin(String value) {
		donnees.setObjectForKey(value, "dateFin");
	}
	public String type() {
		return (String)donnees.objectForKey("type");
	}
	public void setType(String value) {
		donnees.setObjectForKey(value, "type");
	}
	public String motif() {
		return (String)donnees.objectForKey("motif");
	}
	public void setMotif(String value) {
		donnees.setObjectForKey(value, "motif");
	}
	/** dure des conges en jours (non entiers si demi-journees) */
	public Float duree() {
		return (Float)donnees.objectForKey("duree");
	}
	public void setDuree(Float value) {
		donnees.setObjectForKey(value, "duree");
	}
	/** duree des conges en jours sur la periode (non entiers si demi-journ&eacute;es) */
	public Float dureePeriode() {
		return (Float)donnees.objectForKey("dureePeriode");
	}
	public void setDureePeriode(Float value) {
		donnees.setObjectForKey(value, "dureePeriode");
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		donnees.setObjectForKey(valeur,cle);
	}
	public Object valueForKey(String cle) {
		return donnees.objectForKey(cle);
	}

	public EOIndividu toIndividu() {
		return (EOIndividu)donnees.objectForKey("toIndividu");
	}
	public void setToIndividu(EOIndividu value) {
		donnees.setObjectForKey(value, "toIndividu");
	}

	public EOGrade toGrade() {
		return (EOGrade)donnees.objectForKey("toGrade");
	}
	public void setToGrade(EOGrade value) {
		donnees.setObjectForKey(value, "toGrade");
	}

	// Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	public Class classForCoder() {
		return EvenementPourEdition.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		coder.encodeObject(donnees);
	}
	public static Class decodeClass() {
		return EvenementPourEdition.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new EvenementPourEdition((NSDictionary)coder.decodeObject());
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import java.util.Enumeration;

import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class InfoCarriereContrat implements NSKeyValueCoding {

	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String LIBELLE_KEY = "libelle";

	private EOContrat currentContrat;
	private EOCarriere currentCarriere;
	
	private NSTimestamp dateDebut;
	private NSTimestamp dateFin;
	private EOGlobalID globalIDObjet;
	private boolean estCarriere;
	private boolean estHeberge;

	// constructeurs
	public InfoCarriereContrat(EOCarriere carriere) {
		setCurrentContrat(null);
		setCurrentCarriere(carriere);
		this.dateDebut = carriere.dateDebut();
		this.dateFin = carriere.dateFin();
		this.globalIDObjet = carriere.editingContext().globalIDForObject(carriere);
		this.estCarriere = true;
		this.estHeberge = false;
	}
	public InfoCarriereContrat(EOContrat contrat) {
		setCurrentContrat(contrat);
		setCurrentCarriere(null);
		this.dateDebut = contrat.dateDebut();
		this.dateFin = contrat.dateFin();
		if (contrat.dateFinAnticipee() != null) {
			this.dateFin = contrat.dateFinAnticipee();
		}
		this.globalIDObjet = contrat.editingContext().globalIDForObject(contrat);
		this.estCarriere = false;
		this.estHeberge = false;
	}
	public InfoCarriereContrat(EOContratHeberges contrat) {
		this.dateDebut = contrat.dateDebut();
		this.dateFin = contrat.dateFin();
		this.globalIDObjet = contrat.editingContext().globalIDForObject(contrat);
		this.estCarriere = false;
		this.estHeberge = true;
	}
	
	
	
	public EOContrat getCurrentContrat() {
		return currentContrat;
	}
	public void setCurrentContrat(EOContrat currentContrat) {
		this.currentContrat = currentContrat;
	}
	public EOCarriere getCurrentCarriere() {
		return currentCarriere;
	}
	public void setCurrentCarriere(EOCarriere currentCarriere) {
		this.currentCarriere = currentCarriere;
	}
	// accesseurs
	public NSTimestamp dateDebut() {
		return dateDebut;
	}
	public NSTimestamp dateFin() {
		return dateFin;
	}
	public boolean estCarriere() {
		return estCarriere;
	}
	public boolean estHeberge() {
		return estHeberge;
	}
	public EOGlobalID globalIDObjet() {
		return globalIDObjet;
	}
	public String libelle() {
		if (estCarriere()) {
			return "Carriere";
		} else if (estHeberge()) {
			return "Contrat Hébergé";
		} else {
			return "Contrat";
		}
	}
	public String toString() {
		String texte = libelle() + " du " + DateCtrl.dateToString(dateDebut());
		if (dateFin() != null) {
			texte = texte + " au " + DateCtrl.dateToString(dateFin());
		}
		return texte;
	}

	public void takeValueForKey(Object valeur,String cle) {
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	}
	public Object valueForKey(String cle) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	}


	/** retourne la liste des carrieres/contrats (objets de type InfoCarriereContrat) d'un individu pour la periode
	 * ordonnes par ordre decroissant de date debut
	 */
	public static NSArray carrieresContratsPourPeriode(EOEditingContext editingContext,EOIndividu individu,NSTimestamp dateDebut,NSTimestamp dateFin,boolean estAffectation) {
		NSArray<EOCarriere> carrieres = EOCarriere.rechercherCarrieresSurPeriode(editingContext,individu, dateDebut, dateFin);
		NSMutableArray carrieresContrats = new NSMutableArray();
		for (EOCarriere myCarriere : carrieres) {
			InfoCarriereContrat cc = new InfoCarriereContrat(myCarriere);
			carrieresContrats.addObject(cc);
		}

		NSArray contrats = null;
		if (carrieresContrats.count() > 0) {
			//	Pour les titulaires, on n'indique pas les contrats de vacation
			contrats = EOContrat.rechercherContratsPourIndividuEtPeriode(editingContext, individu, dateDebut, dateFin, false);
		} else {
			contrats = EOContrat.rechercherTousContratsPourIndividuEtPeriode(editingContext,individu, dateDebut, dateFin);
		}
		for (Enumeration<EOContrat> e1= contrats.objectEnumerator();e1.hasMoreElements();) {
			EOContrat contrat = e1.nextElement();
			InfoCarriereContrat cc = new InfoCarriereContrat(contrat);
			carrieresContrats.addObject(cc);
		}
		if (estAffectation) {
			// Rechercher les contrats d'hébergés
			NSArray<EOContratHeberges> contratsHeberges = EOContratHeberges.findForIndividuAndPeriode(editingContext, individu,dateDebut,dateFin);
			for ( EOContratHeberges myContratHeberge : contratsHeberges) {
				InfoCarriereContrat cc = new InfoCarriereContrat(myContratHeberge);
				carrieresContrats.addObject(cc);
			}
		}
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(carrieresContrats,new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareDescending)));
	}
}

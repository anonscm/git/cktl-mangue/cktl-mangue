/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.modele.grhum.elections.EOCollege;
import org.cocktail.mangue.modele.mangue.elections.EOListeElecteur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/** En cas de suppression d'une inclusion, invalide les param&eacute;trages finaux des instances associ&eacute;es dans les listes des &eacute;lecteurs au
 * coll&egrave;
 */
//31/03/2010 - modifié pour ajouter un booléen signalant si on modifie les instances ou non
public class Inclusion extends EOGenericRecord {

	public Inclusion() {
		super();
	}
	public org.cocktail.mangue.modele.grhum.elections.EOCollege college() {
		return (org.cocktail.mangue.modele.grhum.elections.EOCollege)storedValueForKey("college");
	}

	public void setCollege(org.cocktail.mangue.modele.grhum.elections.EOCollege value) {
		takeStoredValueForKey(value, "college");
	}
	// méthodes ajoutées
	public void initAvecCollege(EOCollege college) {
		addObjectToBothSidesOfRelationshipWithKey(college,"college");
	}
	public void supprimerRelations() {
		supprimerRelations(true);
	}
	public void supprimerRelations(boolean modifierInstances) {
		// Invalider toutes les instances associées au collège de cette inclusion
		if (modifierInstances) {	// 31/03/2010
			EOListeElecteur.invaliderInstances(editingContext(), "college", college());
		}
		removeObjectFromBothSidesOfRelationshipWithKey(college(),"college");
	}
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		if (college() == null) {
			throw new com.webobjects.foundation.NSValidation.ValidationException("Vous devez fournir un collège");
		}
	}
	// méthodes statiques
	/** fetche toutes les inclusions d'un coll&egrave;ge pour un certain type d'inclusion */
	public static NSArray rechercherInclusionsPourEntiteEtCollege(EOEditingContext editingContext,String nomEntite,EOCollege college) {
		LogManager.logDetail("rechercherInclusionsPourEntiteEtCollege " + nomEntite);
		NSArray args = new NSArray(college);

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("college = %@",args);
		EOFetchSpecification myFetch = new EOFetchSpecification(nomEntite,qualifier,null);
		return (NSArray)editingContext.objectsWithFetchSpecification(myFetch);
	}
}

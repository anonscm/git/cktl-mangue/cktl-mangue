/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

//Editions.java
//Created on Thu Jul 27 08:49:35 Europe/Paris 2006 by Apple EOModeler Version 5.2

import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.referentiel.EOAdresse;
import org.cocktail.mangue.modele.grhum.referentiel.EOEnfant;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOConservationAnciennete;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOReliquatsAnciennete;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/** contient toutes les donn&eacute;es pour l'&eacute;dition des individus sous la forme de valeurs simples.<BR>
 * Supporte l'interface NSKeyValueCoding pour l'acc&eagrave;s aux attributs et NSCoding pour le transfert des donn&eacute;es entre le client et le serveur */
// 07/01/2010 - modifié pour prendre en compte l'adresse de l'employeur stockée dans la vacation
public class IndividuPourEdition extends InfoPourEdition implements NSKeyValueCoding, NSCoding {

	public IndividuPourEdition() {
		super();
		// pour ne pas avoir de souci à l'export
		setCPosition(" ");
		setAdressePerso("");
		setCodePostal("");
		setNomEnfant("");
		setEnseignant("");
		setEmployeur("");
		setInseeComplet("");
		setLlGrade("");
		setNumen("");
		setNoIndividu("");
		setPersId("");
		setOrgComp("");
		setProCode("");
		setProLibelle("");
		setVille("");
		setLieuNaissance("");
		setDptNaissance("");
		setPaysNaissance("");
		setPaysNationalite("");
		setConservationNbAnnees("");
		setConservationNbMois("");
		setConservationNbJours("");
		setConservationAnnee("");
		setAncienneteNbAnnees("");
		setAncienneteNbMois("");
		setAncienneteNBJours("");
		setAncienneteAnnee("");
		// 04/11/09 - DT 1924 ajout de l'ancienneté générale et de services
		setAncienneteGenerale("");
		setAncienneteService("");

	}
	// constructeur pour encoding/decoding
	public IndividuPourEdition(NSDictionary aDict) {
		super(aDict);
	}
	public NSTimestamp dNaissance() {
		return (NSTimestamp)objectForKey("dNaissance");
	}
	public void setDNaissance(NSTimestamp value) {
		setObjectForKey(value, "dNaissance");
	}
	public String lieuNaissance() {
		return (String)objectForKey("lieuNaissance");
	}
	public void setLieuNaissance(String value) {
		setObjectForKey(value, "lieuNaissance");
	}
	public String dptNaissance() {
		return (String)objectForKey("dptNaissance");
	}
	public void setDptNaissance(String value) {
		setObjectForKey(value, "dptNaissance");
	}
	public String paysNaissance() {
		return (String)objectForKey("paysNaissance");
	}
	public void setPaysNaissance(String value) {
		setObjectForKey(value, "paysNaissance");
	}
	public String paysNationalite() {
		return (String)objectForKey("paysNationalite");
	}
	public void setPaysNationalite(String value) {
		setObjectForKey(value, "paysNationalite");
	}
	public String numen() {
		return (String)objectForKey("numen");
	}
	public void setNumen(String value) {
		setObjectForKey(value, "numen");
	}
	public String noIndividu() {
		return (String)objectForKey("noIndividu");
	}
	public void setNoIndividu(String value) {
		setObjectForKey(value, "noIndividu");
	}
	public String persId() {
		return (String)objectForKey("persId");
	}
	public void setPersId(String value) {
		setObjectForKey(value, "persId");
	}
	public String adressePerso() {
		return (String)objectForKey("adressePerso");
	}
	public void setAdressePerso(String value) {
		setObjectForKey(value, "adressePerso");
	}
	public String ville() {
		return (String)objectForKey("ville");
	}
	public void setVille(String value) {
		setObjectForKey(value, "ville");
	} 
	public String nomEnfant() {
		return (String)objectForKey("nomEnfant");
	}
	public void setNomEnfant(String value) {
		setObjectForKey(value, "nomEnfant");
	}
	public NSTimestamp dateNaissanceEnfant() {
		return (NSTimestamp)objectForKey("dateNaissanceEnfant");
	}
	public void setDateNaissanceEnfant(NSTimestamp value) {
		setObjectForKey(value, "dateNaissanceEnfant");
	}
	public String enseignant() {
		return (String)objectForKey("enseignant");
	}
	public void setEnseignant(String value) {
		setObjectForKey(value, "enseignant");
	}
	public String codePostal() {
		return (String)objectForKey("codePostal");
	}
	public void setCodePostal(String value) {
		setObjectForKey(value, "codePostal");
	}
	public String llGrade() {
		return (String)objectForKey("llGrade");
	}
	public void setLlGrade(String value) {
		setObjectForKey(value, "llGrade");
	}
	public String cPosition() {
		return (String)objectForKey("cPosition");
	}
	public void setCPosition(String value) {
		setObjectForKey(value, "cPosition");
	}
	public String conservationAnnee() {
		return (String)objectForKey("conservationAnnee");
	}
	public void setConservationAnnee(String value) {
		setObjectForKey(value, "conservationAnnee");
	}
	public String conservationNbAnnees() {
		return (String)objectForKey("conservationNbAnnees");
	}
	public void setConservationNbAnnees(String value) {
		setObjectForKey(value, "conservationNbAnnees");
	}
	public String conservationNbMois() {
		return (String)objectForKey("conservationNbMois");
	}
	public void setConservationNbMois(String value) {
		setObjectForKey(value, "conservationNbMois");
	}
	public String conservationNbJours() {
		return (String)objectForKey("conservationNbJours");
	}
	public void setConservationNbJours(String value) {
		setObjectForKey(value, "conservationNbJours");
	}
	public String ancienneteAnnee() {
		return (String)objectForKey("ancienneteAnnee");
	}
	public void setAncienneteAnnee(String value) {
		setObjectForKey(value, "ancienneteAnnee");
	}
	public String ancienneteNbAnnees() {
		return (String)objectForKey("ancienneteNbAnnees");
	}
	public void setAncienneteNbAnnees(String value) {
		setObjectForKey(value, "ancienneteNbAnnees");
	}
	public String ancienneteNbMois() {
		return (String)objectForKey("ancienneteNbMois");
	}
	public void setAncienneteNbMois(String value) {
		setObjectForKey(value, "ancienneteNbMois");
	}
	public String ancienneteNBJours() {
		return (String)objectForKey("ancienneteNBJours");
	}
	public void setAncienneteNBJours(String value) {
		setObjectForKey(value, "ancienneteNBJours");
	}
	// 04/11/2009 - DT 1924
	public String ancienneteGenerale() {
		return (String)objectForKey("ancienneteGenerale");
	}
	public void setAncienneteGenerale(String value) {
		setObjectForKey(value, "ancienneteGenerale");
	}
	public String ancienneteService() {
		return (String)objectForKey("ancienneteService");
	}
	public void setAncienneteService(String value) {
		setObjectForKey(value, "ancienneteService");
	}
	public String employeur() {
		return (String)objectForKey("employeur");
	}
	public void setEmployeur(String value) {
		setObjectForKey(value, "employeur");
	}
	public String orgComp() {
		return (String)objectForKey("orgComp");
	}
	public void setOrgComp(String value) {
		setObjectForKey(value, "orgComp");
	}
	public String inseeComplet() {
		return (String)objectForKey("inseeComplet");
	}
	public void setInseeComplet(String value) {
		setObjectForKey(value, "inseeComplet");
	}
	public String proCode() {
		return (String)objectForKey("proCode");
	}
	public void setProCode(String value) {
		setObjectForKey(value, "proCode");
	}
	public String proLibelle() {
		return (String)objectForKey("proLibelle");
	}
	public void setProLibelle(String value) {
		setObjectForKey(value, "proLibelle");
	}
	// autres

	public void setIndividu(EOIndividu individu) {
		super.setIndividu(individu);
		if (individu.dNaissance() != null) {
			setDNaissance(individu.dNaissance());
		}
		if (individu.villeDeNaissance() != null) {
			setLieuNaissance(individu.villeDeNaissance());
		}
		// département de naissance
		if (individu.toDepartement() != null) {
			if (individu.toDepartement().code().equals("000") == false) {
				setDptNaissance(individu.toDepartement().libelleLong());
			}
		}
		// pays de naissance
		if (individu.toPaysNaissance() != null) {
			if (individu.toPaysNaissance().libelleLong() != null) {
				setPaysNaissance(individu.toPaysNaissance().libelleLong());
			}
		}
		if (individu.toPaysNationalite() != null) {
			if (individu.toPaysNationalite().libelleLong() != null) {
				setPaysNationalite(individu.toPaysNationalite().libelleLong());
			}
		}
		
		// numéro complet de sécurité sociale
		String valeur = "";
		if (individu.indNoInsee() != null) {
			valeur = valeur + individu.indNoInsee();
		}
		if (individu.indCleInsee() != null) {
			valeur = valeur + " " + StringCtrl.stringCompletion(individu.indCleInsee().toString(), 2, "0", "G");
		}
		setInseeComplet(valeur);
		
		setNoIndividu(individu.noIndividu().toString());
		setPersId(individu.persId().toString());
		// numen
		if (individu.personnel() != null) {
			if (individu.personnel().numen() != null) {
				setNumen(individu.personnel().numen());
			}
		}
	}
	public void setAdresse(EOAdresse adresse) {
		String valeur = "";
		if (adresse.adrAdresse1() != null) {
			valeur = valeur + adresse.adrAdresse1();
		}
		if (adresse.adrAdresse2() != null) {
			valeur = valeur + " - " + adresse.adrAdresse2();
		}
		if (adresse.adrBp() != null) {
			valeur = valeur + "  BP " + adresse.adrBp();
		}
		valeur.replaceAll("\t"," ");
		setAdressePerso(valeur);

		if (adresse.codePostal() != null) {
			setCodePostal(adresse.codePostal()); 
		}
		if (adresse.ville() != null) {
			setVille(adresse.ville().replaceAll("\t"," ")); 
		} 
	}
	public void setEnfant(EOEnfant enfant) {
		String nom = "";
		if (enfant.nom() != null) {
			nom += enfant.nom();
		}
		if (enfant.prenom() != null) {
			if (nom.length() > 0) {
				nom +=  " ";
			}
			nom += enfant.prenom();
		}
		setNomEnfant(nom);
		setDateNaissanceEnfant(enfant.dNaissance());
	}
	public void setDuree(Duree duree,String code) {
		setDebut(duree.dateDebut());
		if (duree.dateFin() != null) {
			setFin(duree.dateFin());
		}
		setStatut("TIT");
		if (code != null) {
			setCodeSpec(code);
		}
	}
	public void setContratAvenant(EOContratAvenant avenant,boolean recupererSpecialisation) {
		super.setContratAvenant(avenant,recupererSpecialisation);
	}
	
	/** Prepare tous les reliquats et conservations d'anciennete de l'element de carriere.
	 * Si il y a plus d'un reliquat et d'une conservation, cree des individusPourEdition supplementaires avec ces
	 * informations
	 * @param elementCarriere
	 * @return tableau des individus ajoutes ou tableau vide si aucun
	 */
	public NSArray setAnciennetePourElement(EOElementCarriere elementCarriere) {

		EOReliquatsAnciennete reliquat = null;
		EOConservationAnciennete conservation = null;
		
		NSArray anciennetes =  EOReliquatsAnciennete.rechercherReliquatsNonUtilisesPourElementCarriere(elementCarriere.editingContext(), elementCarriere);
		anciennetes = EOSortOrdering.sortedArrayUsingKeyOrderArray(anciennetes, EOReliquatsAnciennete.SORT_ARRAY_ANNEE_DESC);
		
		NSMutableArray reliquatsAAjouter = new NSMutableArray(anciennetes);
		if (anciennetes.size() > 0) {
			reliquat = (EOReliquatsAnciennete)reliquatsAAjouter.get(0);
			reliquatsAAjouter.removeObjectAtIndex(0);
		}
		anciennetes = EOConservationAnciennete.rechercherAnciennetesNonUtiliseesPourElementCarriere(elementCarriere.editingContext(), elementCarriere);
		anciennetes = EOSortOrdering.sortedArrayUsingKeyOrderArray(anciennetes, new NSArray(EOSortOrdering.sortOrderingWithKey("ancAnnee", EOSortOrdering.CompareDescending)));
		NSMutableArray conservationsAAjouter = new NSMutableArray(anciennetes);
		if (anciennetes.count() > 0) {
			conservation = (EOConservationAnciennete)conservationsAAjouter.objectAtIndex(0);
			conservationsAAjouter.removeObjectAtIndex(0);
		}
		if (reliquat == null && conservation == null) {
			// pas d'ancienneté
			return new NSArray();
		}
		// ajouter l'ancienneté à cet individu
		setAnciennete(reliquat,conservation,false);	
		int maxAgents = 0;
		if (reliquatsAAjouter.size() > 0) {
			maxAgents = reliquatsAAjouter.size();
		}
		if (conservationsAAjouter.count() > maxAgents) {
			maxAgents = conservationsAAjouter.size();
		}
		if (maxAgents == 0) {
			return new NSArray();
		}
		NSMutableArray agentsAjoutes = new NSMutableArray();
		for (int i = 0; i < maxAgents;i++) {
			reliquat = null;
			conservation = null;
			// Commencer par dupliquer cet individu
			IndividuPourEdition nouvelIndividu = (IndividuPourEdition)this.duplicate();
			if (nouvelIndividu != null) {	// Suite à une erreur
				if (i < reliquatsAAjouter.count()) {
					reliquat = (EOReliquatsAnciennete)reliquatsAAjouter.objectAtIndex(i);
				}
				if (i < conservationsAAjouter.count()) {
					conservation = (EOConservationAnciennete)conservationsAAjouter.objectAtIndex(i);
				}
				nouvelIndividu.setAnciennete(reliquat,conservation,true);
				agentsAjoutes.addObject(nouvelIndividu);
			}
		}
		return agentsAjoutes;
	}

	/** Ajoute a l'individu le reliquat et la conservation d'anciennete */
	public void setAnciennete(EOReliquatsAnciennete reliquat,EOConservationAnciennete conservation,boolean estDuplicationIndividu) {
		// Commencer par tout supprimer
		if (estDuplicationIndividu) {
			setAncienneteAnnee("");
			setAncienneteNbAnnees("");
			setAncienneteNbMois("");
			setAncienneteNBJours("");
			setConservationAnnee("");
			setConservationNbAnnees("");
			setConservationNbMois("");
			setConservationNbJours("");
		}
		if (reliquat != null) {
			if (reliquat.ancAnnee() != null) {
				setAncienneteAnnee(reliquat.ancAnnee().toString());
			}
			if (reliquat.ancNbAnnees() != null) {
				setAncienneteNbAnnees(reliquat.ancNbAnnees().toString());
			}
			if (reliquat.ancNbMois() != null) {
				setAncienneteNbMois(reliquat.ancNbMois().toString());
			}
			if (reliquat.ancNbJours() != null) {
				setAncienneteNBJours(reliquat.ancNbJours().toString());
			}
		} 
		if (conservation != null) {
			if (conservation.ancNbAnnees() != null) {
				setConservationNbAnnees(conservation.ancNbAnnees().toString());
			}
			if (conservation.ancNbMois() != null) {
				setConservationNbMois(conservation.ancNbMois().toString());
			}
			if (conservation.ancNbJours() != null) {
				setConservationNbJours(conservation.ancNbJours().toString());
			}
		}
	}
	/** ajoute les informations de carriere
	 * @param carriere
	 * @param uniquementCarriere true si les informations sur les elements de carriere ne sont pas fournis
	 */
	public void setCarriere(EOCarriere carriere,boolean uniquementCarriere,boolean recupererSpecialisation) {
		super.setCarriere(carriere,uniquementCarriere,recupererSpecialisation);
		try {
			EOChangementPosition changementPosition = carriere.dernierePosition();
			setCPosition(changementPosition.toPosition().code());
		} catch (Exception exc) {
			// pas de changement de position
		}
	}
	/** 10/02/09 - pour rajouter le libelle long du grade */
	public void setElementCarriere(EOElementCarriere element,boolean recupererSpecialisation) {
		setLlGrade(element.toGrade().llGrade());
		super.setElementCarriere(element, recupererSpecialisation);
	}

	// Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	public Class classForCoder() {
		return IndividuPourEdition.class;
	}
	public static Class decodeClass() {
		return IndividuPourEdition.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new IndividuPourEdition((NSDictionary)coder.decodeObject());
	}

}

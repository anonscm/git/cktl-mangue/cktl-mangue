/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import java.lang.reflect.Constructor;

import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOQuotite;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;


/** classe abstraite : contient toutes les donn&eacute;es communes &agrave; toutes les classes d'&eacute;dition des emplois sous la forme de valeurs simples.<BR>
 * Supporte l'interface NSKeyValueCoding pour l'acc&eagrave;s aux attributs et NSCoding pour le transfert des donn&eacute;es entre le client et le serveur */
// 27/01/2010 - Modification pour ajouter l'indice dans les données éditables
public abstract class InfoPourEdition extends Object implements NSKeyValueCoding, NSCoding {
	private NSMutableDictionary donnees;

	public InfoPourEdition() {
		super();
		donnees = new NSMutableDictionary();
		// pour ne pas avoir de souci à l'export
		setLcGrade(" ");
		setLcCorps(" ");
		setImplantation("");
		setNom("");
		setNomPatronymique("");
		setPrenom("");
		setCCivilite("");
		setSexe("");
		setCEchelon("");
		setIndiceMajore("");
		setChapitre("");
		setCodeSpec("");
		setLcTypeContratTrav("");
		setLlComposante("");
		setLlStructure("");
		setLlStructureComplet("");
		setStatut("");
		setTemEnseignant("");
		setNumeroEmploi("");
		setFonction("");
		setSpecialisation("");
	}
	// constructeur pour encoding/decoding
	public InfoPourEdition(NSDictionary aDict) {
		donnees = new NSMutableDictionary(aDict);
	}
	public String nom() {
		return (String)objectForKey("nom");
	}
	public void setNom(String value) {
		setObjectForKey(value, "nom");
	}
	public String prenom() {
		return (String)objectForKey("prenom");
	}
	public void setPrenom(String value) {
		setObjectForKey(value, "prenom");
	}
	public String nomPatronymique() {
		return (String)objectForKey("nomPatronymique");
	}
	public void setNomPatronymique(String value) {
		setObjectForKey(value, "nomPatronymique");
	}
	public String cCivilite() {
		return (String)objectForKey("cCivilite");
	}
	public void setCCivilite(String value) {
		setObjectForKey(value, "cCivilite");
	}
	public String sexe() {
		return (String)objectForKey("sexe");
	}
	public void setSexe(String value) {
		setObjectForKey(value, "sexe");
	}
	public String lcCorps() {
		return (String)objectForKey("lcCorps");
	}
	public void setLcCorps(String value) {
		setObjectForKey(value, "lcCorps");
	}
	public String lcGrade() {
		return (String)objectForKey("lcGrade");
	}
	public void setLcGrade(String value) {
		setObjectForKey(value, "lcGrade");
	}
	public String cEchelon() {
		return (String)objectForKey("cEchelon");
	}
	public void setCEchelon(String value) {
		setObjectForKey(value, "cEchelon");
	}
	public String indiceMajore() {
		return (String)objectForKey("indiceMajore");
	}
	public void setIndiceMajore(String value) {
		setObjectForKey(value, "indiceMajore");
	}
	public NSTimestamp debut() {
		return (NSTimestamp)objectForKey("debut");
	}
	public void setDebut(NSTimestamp value) {
		setObjectForKey(value, "debut");
	}
	public NSTimestamp fin() {
		return (NSTimestamp)objectForKey("fin");
	}
	public void setFin(NSTimestamp value) {
		setObjectForKey(value, "fin");
	}

	public String llStructure() {
		return (String)objectForKey("llStructure");
	}
	public void setLlStructure(String value) {
		setObjectForKey(value, "llStructure");
	}
	public String llStructureComplet() {
		return (String)objectForKey("llStructureComplet");
	}
	public void setLlStructureComplet(String value) {
		setObjectForKey(value, "llStructureComplet");
	}
	public String statut() {
		return (String)objectForKey("statut");
	}
	public void setStatut(String value) {
		setObjectForKey(value, "statut");
	}
	
	public Number quotite() {
		return (Number)objectForKey("quotite");
	}
	public void setQuotite(Number value) {
		setObjectForKey(value, "quotite");
	}

	public Number quotiteTravail() {
		return (Number)objectForKey("quotiteTravail");
	}
	public void setQuotiteTravail(Number value) {
		setObjectForKey(value, "quotiteTravail");
	}
	
	public Number quotiteOccupation() {
		return (Number)objectForKey("quotiteOccupation");
	}
	public void setQuotiteOccupation(Number value) {
		setObjectForKey(value, "quotiteOccupation");
	}

	public Number quotiteFinanciere() {
		return (Number)objectForKey("quotiteFinanciere");
	}
	public void setQuotiteFinanciere(Number value) {
		setObjectForKey(value, "quotiteFinanciere");
	}

	public String numeroEmploi() {
		return (String)objectForKey("numeroEmploi");
	}
	public void setNumeroEmploi(String value) {
		setObjectForKey(value, "numeroEmploi");
	}
	public String fonction() {
		return (String)objectForKey("fonction");
	}
	public void setFonction(String value) {
		setObjectForKey(value, "fonction");
	}
	public String llComposante() {
		return (String)objectForKey("llComposante");
	}
	public void setLlComposante(String value) {
		setObjectForKey(value, "llComposante");
	}
	public NSTimestamp debutAffectation() {
		return (NSTimestamp)objectForKey("debutAffectation");
	}
	public void setDebutAffectation(NSTimestamp value) {
		setObjectForKey(value, "debutAffectation");
	}
	public NSTimestamp finAffectation() {
		return (NSTimestamp)objectForKey("finAffectation");
	}
	public void setFinAffectation(NSTimestamp value) {
		setObjectForKey(value, "finAffectation");
	}
	public String implantation() {
		return (String)objectForKey("implantation");
	}
	public void setImplantation(String value) {
		setObjectForKey(value, "implantation");
	}
	public String chapitre() {
		return (String)objectForKey("chapitre");
	}
	public void setChapitre(String value) {
		setObjectForKey(value, "chapitre");
	}
	public NSTimestamp debutOccupation() {
		return (NSTimestamp)objectForKey("debutOccupation");
	}
	public void setDebutOccupation(NSTimestamp value) {
		setObjectForKey(value, "debutOccupation");
	}
	public NSTimestamp finOccupation() {
		return (NSTimestamp)objectForKey("finOccupation");
	}
	public void setFinOccupation(NSTimestamp value) {
		setObjectForKey(value, "finOccupation");
	}
	public String lcTypeContratTrav() {
		return (String)objectForKey("lcTypeContratTrav");
	}
	public void setLcTypeContratTrav(String value) {
		setObjectForKey(value, "lcTypeContratTrav");
	}
	public String specialisation() {
		return (String)objectForKey("specialisation");
	}
	public void setSpecialisation(String value) {
		setObjectForKey(value, "specialisation");
	}
	public String codeSpec() {
		return (String)objectForKey("codeSpec");
	}
	public void setCodeSpec(String value) {
		setObjectForKey(value, "codeSpec");
	}
	public String temEnseignant() {
		return (String)objectForKey("temEnseignant");
	}
	public void setTemEnseignant(String value) {
		setObjectForKey(value, "temEnseignant");
	}
	// interface keyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		setObjectForKey(valeur,cle);
	}
	public Object valueForKey(String cle) {
		return objectForKey(cle);
	}
	public void encodeWithCoder(NSCoder coder) {
		coder.encodeObject(donnees);
	}
	// Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	public Class classForCoder() {
		return InfoPourEdition.class;
	}
	public static Class decodeClass() {
		return InfoPourEdition.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new IndividuPourEdition((NSDictionary)coder.decodeObject());
	}
	// Autres
	public InfoPourEdition duplicate() {
		try {
			Constructor constructeur = this.getClass().getConstructor(new Class[] {NSDictionary.class});
			InfoPourEdition newObject = (InfoPourEdition)constructeur.newInstance(new Object[] {donnees});
			return newObject;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public void setIndividu(EOIndividu individu) {	
		if (individu.cCivilite() != null) {
			setCCivilite(individu.cCivilite());
			EOGenericRecord civilite = SuperFinder.rechercherObjetAvecAttributEtValeurEgale(individu.editingContext(), "Civilite", "cCivilite", individu.cCivilite());
			if (civilite != null) {
				setSexe((String)civilite.valueForKey("sexe"));
			}
		}
		setNom(individu.nomUsuel());
		if (individu.nomPatronymique() != null) {
			setNomPatronymique(individu.nomPatronymique());
		}
		if (individu.prenom() != null) {
			setPrenom(StringCtrl.capitalizedString(individu.prenom()));
		}	
	}
	public void setContratAvenant(EOContratAvenant avenant,boolean recupererSpecialisation) {
		if (avenant == null) {
			setStatut("CDD");
			return;
		}
		EOContrat contrat = avenant.contrat();
		setDebut(contrat.dateDebut());
		if (contrat.dateFin() != null) {
			setFin(contrat.dateFin());
		}
		EOGrade grade = avenant.toGrade();
		if (grade != null) {
			setLcGrade(grade.lcGrade());
			if (grade.toCorps() != null) {
				setLcCorps(grade.toCorps().lcCorps());
			}
		}
		if (avenant.indiceContrat() != null) {
			setIndiceMajore(avenant.indiceContrat());
		}
		setStatut("CDD");
		setLcTypeContratTrav(contrat.toTypeContratTravail().libelleCourt());
		if (avenant.cEchelon() != null) {
			setCEchelon(avenant.cEchelon());
		}
		setTemEnseignant(contrat.toTypeContratTravail().temEnseignant());
		if (recupererSpecialisation) {
			if (avenant.toCnu() != null)  {
				setCodeSpec(avenant.toCnu().code());
				setSpecialisation(avenant.toCnu().libelleCourt());
			} else if (avenant.toSpecialiteAtos() != null) {
				setCodeSpec(avenant.toSpecialiteAtos().code());
				setSpecialisation(avenant.toSpecialiteAtos().libelleCourt());
			} else if (avenant.toReferensEmploi() != null) {
				setCodeSpec(avenant.toReferensEmploi().code());
				setSpecialisation(avenant.toReferensEmploi().libelleLong());
			} else if (avenant.toDiscSecondDegre() != null) {
				setCodeSpec(avenant.toDiscSecondDegre().code());
				setSpecialisation(avenant.toDiscSecondDegre().libelleCourt());
			} else if (avenant.toBap() != null) {
				setCodeSpec(avenant.toBap().code());
				setSpecialisation(avenant.toBap().libelleLong());
			}
		}
	}
	/** ajoute les informations de carriere
	 * @param carriere
	 * @param uniquementCarriere true si les informations sur les &eacute;l&eacute;ments de carri&egrave;re ne sont pas fournis
	 */
	public void setCarriere(EOCarriere carriere,boolean uniquementCarriere,boolean recupererSpecialisation) {

		if (uniquementCarriere) {
			setDebut(carriere.dateDebut());
			if (carriere.dateFin() != null) {
				setFin(carriere.dateFin());
			}
			if (recupererSpecialisation && carriere.toBap() != null) {
				setCodeSpec(carriere.toBap().code());
				setSpecialisation(carriere.toBap().libelleLong());
			}
		}
		setStatut("TIT");
		setTemEnseignant(carriere.toTypePopulation().temEnseignant());
		if (recupererSpecialisation) {
			if (carriere.toCnu() != null)  {
				setCodeSpec(carriere.toCnu().code());
				setSpecialisation(carriere.toCnu().libelleCourt());
			}
			if (carriere.toSpecialiteAtos() != null) {
				setCodeSpec(carriere.toSpecialiteAtos().code());
				setSpecialisation(carriere.toSpecialiteAtos().libelleCourt());
			}

			if (carriere.toDiscSecondDegre() != null) {
				setCodeSpec(carriere.toDiscSecondDegre().code());
				setSpecialisation(carriere.toDiscSecondDegre().libelleCourt());
			}
		}
		
		
		
	}
	
	/**
	 * 
	 * @param element
	 * @param recupererSpecialisation
	 */
	public void setElementCarriere(EOElementCarriere element,boolean recupererSpecialisation) {
		setCarriere(element.toCarriere(),false,recupererSpecialisation);
		setDebut(element.dateDebut());
		if (element.dateFin() != null) {
			setFin(element.dateFin());
		}
		setLcGrade(element.toGrade().lcGrade());
		setLcCorps(element.toCorps().lcCorps());
		setCEchelon(element.cEchelon());
		if (element.inmEffectif() != null) {
			setIndiceMajore(element.inmEffectif().toString());
		} else if (element.indiceMajore() != null) {
			setIndiceMajore(element.indiceMajore().toString());
		}
		if (element.toCarriere().toReferensEmploi() != null) {
			setCodeSpec(element.toCarriere().toReferensEmploi().code());
			setSpecialisation(element.toCarriere().toReferensEmploi().libelleLong());
		}
				
	}
	
	/**
	 * 
	 * @param affectation
	 */
	public void setAffectation(EOAffectation affectation) {
		
		setLlStructure(affectation.toStructureUlr().llStructure());
		setLlStructureComplet(affectation.toStructureUlr().libelleComplet());
		setLlComposante(affectation.toStructureUlr().toComposante().llStructure());
		setQuotite(affectation.quotite());

		if (quotiteFinanciere() == null) {
			setQuotiteFinanciere(affectation.quotite());
		}
		
		setDebutAffectation(affectation.dateDebut());
		if (affectation.dateFin() != null) {
			setFinAffectation(affectation.dateFin());
		}
	}
	
	/**
	 * @param emploi : un emploi
	 */
	public void setEmploi(IEmploi emploi) {
		if (emploi.getNoEmploi() != null) {
			setNumeroEmploi(emploi.getNoEmploiAffichage());
		}
		if (emploi.getToRne() != null) {
			setImplantation(emploi.getToRne().libelleCourt());
		} 
		if (emploi.getToChapitre() != null && emploi.getToChapitre().cChapitre() != null) {
			setChapitre(emploi.getToChapitre().cChapitre().toString());
		}
	}
	
	/**
	 * 
	 * @param occupation
	 */
	public void setOccupation(EOOccupation occupation) {
		
		setEmploi(occupation.toEmploi());
		setDebutOccupation(occupation.dateDebut());
		
		if (occupation.dateFin() != null) {
			setFinOccupation(occupation.dateFin());
		}		
		if (occupation.observations() != null) {
			setFonction(occupation.observations());
		}
		
		setQuotiteOccupation(occupation.quotite());
		
		NSArray<EOQuotite> quotites = SuperFinder.rechercherAvecAttributEtValeurEgale(occupation.editingContext(),EOQuotite.ENTITY_NAME, EOQuotite.NUM_QUOTITE_KEY, occupation.quotite());
		try {
			setQuotiteFinanciere(quotites.objectAtIndex(0).quotiteFinanciere());
		} catch (Exception exc1) {
			setQuotiteFinanciere(occupation.quotite());
		}
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	protected Object objectForKey(String value) {
		return donnees.objectForKey(value);
	}
	
	/**
	 * 
	 * @param value
	 * @param cle
	 */
	protected void setObjectForKey(Object value,String cle) {
		if (value != null) {
			donnees.setObjectForKey(value, cle);
		}
	}
}

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;

/** Pour d&eacute;finir les infos d'impression (nomMethodeImpression, nomFichierImpression, nomFichierJasper, type : LISTE, CONVOCATION */

public class InfoPourEditionMedicale implements NSCoding {
	public static  final int TYPE_LISTE = 0;
	public static final int TYPE_CONVOCATION = 1;
	private int typeEdition;
	private String methodeImpression,nomFichierImpression, nomFichierJasper;
	
	/** Constructeur
	 * 
	 * @param typeEdition : LISTE ou CONVOCATION Individuelle
	 * @param methodeImpression nom de m&eacute;thode d'impression
	 * @param nomFichierImpression nom du fichier d'impression
	 * @param nomFichierJasper nom du fichier Jasper
	 */
	public InfoPourEditionMedicale(int typeEdition,String methodeImpression,String nomFichierImpression,String nomFichierJasper) {
		this.typeEdition = typeEdition;
		this.methodeImpression = methodeImpression;
		this.nomFichierImpression = nomFichierImpression;
		this.nomFichierJasper = nomFichierJasper;
	}
	public int typeEdition() {
		return typeEdition;
	}
	public String methodeImpression() {
		return methodeImpression;
	}
	public String nomFichierImpression() {
		return nomFichierImpression;
	}
	public String nomFichierJasper() {
		return nomFichierJasper;
	}
	public void setNomFichierImpression(String unNom) {
		this.nomFichierImpression = unNom;
	}
 	public void encodeWithCoder(NSCoder coder) {
		coder.encodeObject(new Integer(typeEdition()));
		coder.encodeObject(methodeImpression());
		coder.encodeObject(nomFichierImpression());
		coder.encodeObject(nomFichierJasper());
	}
	public static Class decodeClass() {
		return InfoPourEditionMedicale.class;
	}
	public static Object decodeObject(NSCoder coder) {
		int typeEdition = ((Integer)coder.decodeObject()).intValue();
	    String methodeImpression = (String)coder.decodeObject();
	    String nomFichierImpression = (String)coder.decodeObject();
	    String nomFichierJasper = (String)coder.decodeObject();
	 
	    return new InfoPourEditionMedicale(typeEdition,methodeImpression,nomFichierImpression,nomFichierJasper);
	    }
	public Class classForCoder() {
		return InfoPourEditionMedicale.class;
	}
}

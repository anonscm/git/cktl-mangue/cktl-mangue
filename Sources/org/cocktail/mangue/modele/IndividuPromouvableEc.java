/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */package org.cocktail.mangue.modele;

 /** Cette classe est utilis&eacute;e pour imprimer les arr&ecirc;t&eacute;s de promotion des Enseignants-Chercheurs
  * 
  */
 import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.EOIndice;
import org.cocktail.mangue.modele.grhum.EOPassageEchelon;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.EOSupInfoData;
import org.cocktail.mangue.modele.mangue.visa.EOVisa;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

 public class IndividuPromouvableEc  implements NSKeyValueCoding,NSCoding  {
	 private EOIndividu individu;
	 private String echelon, chevron;
	 private EOGrade grade;
	 private String datePromotion,noArrete,dateArrete;
	 private EOEditingContext editingContext;
	 private EOIndice indice;
	 private NSArray visas;
	 private final static String TYPE_ARRETE = "PROMO_EC";
	 
	 public IndividuPromouvableEc(EOSupInfoData recordElectra) {
		 editingContext = recordElectra.editingContext();
		 individu = recordElectra.toIndividu();
		 grade = recordElectra.toParamPromotion().gradeArrivee();
		 echelon = recordElectra.toParamPromotion().cEchelonArrivee();
		 chevron = recordElectra.toParamPromotion().cChevronArrivee();
		 datePromotion = recordElectra.datePromotionFormatee();
		 noArrete = recordElectra.eficNoArrete();
		 dateArrete = recordElectra.dateArreteFormatee();
	 }
	 public IndividuPromouvableEc(NSDictionary aDict) {
		 editingContext = new EOEditingContext();
		 EOGlobalID globalID = (EOGlobalID)aDict.objectForKey("individu");
		 individu = (EOIndividu)SuperFinder.objetForGlobalIDDansEditingContext(globalID, editingContext);
		 globalID = (EOGlobalID)aDict.objectForKey("grade");
		 grade = (EOGrade)SuperFinder.objetForGlobalIDDansEditingContext(globalID, editingContext);
		 echelon = (String)aDict.objectForKey("echelon");
		 chevron = (String)aDict.objectForKey("chevron");
		 noArrete = (String)aDict.objectForKey("noArrete");
		 dateArrete = (String)aDict.objectForKey("dateArrete");
		 datePromotion = (String)aDict.objectForKey("datePromotion");
		 preparerIndice(editingContext);
		 preparerVisas();
	 }
	 public EOEditingContext editingContext() {
		 return editingContext;
	 }
	 public EOIndividu individu() {
		 return individu;
	 }
	 public EOGrade grade() {
		return grade;
	 }
	 public NSArray visas() {
		 return visas;
	 }
	 public String noArrete() {
		 return noArrete;
	 }
	 public String dateArrete() {
		 return dateArrete;
	 }
	 public String echelon() {
		 return echelon;
	 }
	 public String chevron() {
		 return chevron;
	 }
	 public String indiceMajore() {
		 if (indice != null) {
			 return indice.cIndiceMajore().toString();
		 } else {
			 return null;
		 }
	 }
	 public String datePromotion() {
		 return datePromotion;
	 }

	 // Interface key-value Coding
	 public void takeValueForKey(Object valeur,String cle) {
		 NSKeyValueCoding.DefaultImplementation.takeValueForKey(this,valeur,cle);
	 }
	 public Object valueForKey(String cle) {
		 return NSKeyValueCoding.DefaultImplementation.valueForKey(this,cle);
	 }
	 // Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	 public Class classForCoder() {
		 return IndividuPromouvableEc.class;
	 }
	 public void encodeWithCoder(NSCoder coder) {
		 NSMutableDictionary dict = new NSMutableDictionary();
		 dict.setObjectForKey(individu.editingContext().globalIDForObject(individu), "individu");
		 dict.setObjectForKey(grade.editingContext().globalIDForObject(grade), "grade");
		 if (echelon != null) {
			 dict.setObjectForKey(echelon, "echelon");
		 }
		 if (chevron != null) {
			 dict.setObjectForKey(chevron, "chevron");
		 }
		 dict.setObjectForKey(datePromotion, "datePromotion");
		 dict.setObjectForKey(noArrete, "noArrete");
		 dict.setObjectForKey(dateArrete, "dateArrete");
		 coder.encodeObject(dict);
	 }
	 public static Class decodeClass() {
		 return IndividuPromouvableEc.class;
	 }
	 public static Object decodeObject(NSCoder coder) {
		 return new IndividuPromouvableEc((NSDictionary)coder.decodeObject());
	 }

	 //	Méthodes privées
	 private void preparerIndice(EOEditingContext editingContext) {
		 NSTimestamp date = DateCtrl.stringToDate(datePromotion);
		 indice = null;
		 if (grade != null && echelon != null) {
			 if (chevron != null) {
				 NSArray passages = rechercherPassageChevronOuvertPourGradeEchelonEtChevron(editingContext, grade.cGrade(), echelon, date);
				 if (passages.count() > 0) {
					 EOGenericRecord passageChevron = (EOGenericRecord)passages.objectAtIndex(0);
					 String indiceBrut = (String)passageChevron.valueForKey("cIndiceBrut");
					 if (indiceBrut != null) {
						 // indice à la date de promotion
						 indice = EOIndice.indiceMajorePourIndiceBrutEtDate(editingContext, indiceBrut, date);
					 }
				 }
			 } else {
				 // le passage échelon n'existe pas forcément car on peut être aux limites dans passage échelon
				 NSArray passages = EOPassageEchelon.rechercherPassageEchelonOuvertPourGradeEtEchelon(editingContext, grade.cGrade(), echelon, date, true);
				 if (passages.count() > 0) {
					 EOPassageEchelon passageEchelon = (EOPassageEchelon)passages.objectAtIndex(0);
					 if (passageEchelon.cIndiceBrut() != null) {
						 // indice à la date de promotion
						 indice = EOIndice.indiceMajorePourIndiceBrutEtDate(editingContext, passageEchelon.cIndiceBrut(), date);
					 }
				 }
			 }
		 }
	 }
	 private void preparerVisas() {
		 NSArray visas1 = EOVisa.rechercherVisaPourTypeArreteTypePopulationEtCorps(editingContext(),TYPE_ARRETE,grade().toCorps().toTypePopulation().code(),"");
		 NSArray visas2 = EOVisa.rechercherVisaPourTypeArreteTypePopulationEtCorps(editingContext(),TYPE_ARRETE,grade().toCorps().toTypePopulation().code(),grade().toCorps().cCorps());
		 visas = visas1.arrayByAddingObjectsFromArray(visas2);
	 }
	 private NSArray rechercherPassageChevronOuvertPourGradeEchelonEtChevron(EOEditingContext editingContext, String cGrade,String echelon,NSTimestamp dateReference) {
		 NSMutableArray args = new NSMutableArray(cGrade);
		 String qualifier = "cGrade = %@";
		 args.addObject(echelon);
		 args.addObject(chevron);
		 qualifier = qualifier + " AND cEchelon = %@ AND cChevron = %@";
		 if (dateReference != null) {
			 args.addObject(dateReference);
			 qualifier = qualifier + " AND (dFermeture = nil OR dFermeture >= %@)";
		 } else {
			 qualifier = qualifier + " AND dFermeture = nil";
		 }
		 EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(qualifier ,args);
		 // on ne fait pas le tri par la base de données car il ne renvoie pas les classements selon l'ordre alphabétique
		 // avec les numériques en tête correctement (du moins avec Oracle)
		 EOFetchSpecification myFetch = new EOFetchSpecification("PassageChevron",qual,null);
		 NSArray results = editingContext.objectsWithFetchSpecification(myFetch);
		 NSArray sorts = new NSArray (EOSortOrdering.sortOrderingWithKey("cEchelon",EOSortOrdering.CompareDescending));

		 return EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sorts);

	 }
 }
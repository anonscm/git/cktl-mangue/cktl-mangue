/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.modele;

import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOAffectation;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;
import org.cocktail.mangue.modele.mangue.individu.EOContratHeberges;
import org.cocktail.mangue.modele.mangue.individu.EOElementCarriere;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSCoder;
import com.webobjects.foundation.NSCoding;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/** contient toutes les donn&eacute;es pour comparer le grade, &eacute;chelon, indice des individus &agrave; 2 dates diff&eacute;rentes.<BR>
 * Supporte l'interface NSKeyValueCoding pour l'acc&eagrave;s aux attributs et NSCoding pour le transfert des donn&eacute;es entre le 
 * client et le serveur */
public class EtatComparatifCarriere implements NSKeyValueCoding, NSCoding  {
	public static int TOUT_TYPE_PERSONNEL = 0, TITULAIRE = 1,CONTRACTUEL = 2, HEBERGE = 3;
	public static int TOUT_TYPE_ENSEIGNANT = 0, ENSEIGNANT = 1,NON_ENSEIGNANT = 2;

	private NSMutableDictionary donnees;

	public EtatComparatifCarriere() {
		donnees = new NSMutableDictionary();
	}
	public EtatComparatifCarriere(NSDictionary aDict) {
		donnees = new NSMutableDictionary(aDict);
	}
	// Accesseurs
	public String nom() {
		return (String)objectForKey("nom");
	}
	public void setNom(String value) {
		setObjectForKey(value, "nom");
	}
	public String prenom() {
		return (String)objectForKey("prenom");
	}
	public void setPrenom(String value) {
		setObjectForKey(value, "prenom");
	}
	public String noInsee() {
		return (String)objectForKey("noInsee");
	}
	public void setNoInsee(String value) {
		setObjectForKey(value, "noInsee");
	}
	public String gradeDebut() {
		return (String)objectForKey("gradeDebut");
	}
	public void setGradeDebut(String value) {
		setObjectForKey(value, "gradeDebut");
	}
	public String echelonDebut() {
		return (String)objectForKey("echelonDebut");
	}
	public void setEchelonDebut(String value) {
		setObjectForKey(value, "echelonDebut");
	}
	public String indiceDebut() {
		return (String)objectForKey("indiceDebut");
	}
	public void setIndiceDebut(String value) {
		setObjectForKey(value, "indiceDebut");
	}
	public String gradeFin() {
		return (String)objectForKey("gradeFin");
	}
	public void setGradeFin(String value) {
		setObjectForKey(value, "gradeFin");
	}
	public String echelonFin() {
		return (String)objectForKey("echelonFin");
	}
	public void setEchelonFin(String value) {
		setObjectForKey(value, "echelonFin");
	}
	public String indiceFin() {
		return (String)objectForKey("indiceFin");
	}
	public void setIndiceFin(String value) {
		setObjectForKey(value, "indiceFin");
	}
	public Number quotiteServiceFin() {
		return (Number)objectForKey("quotiteServiceFin");
	}
	public void setQuotiteServiceFin(Number value) {
		setObjectForKey(value, "quotiteServiceFin");
	}
	public void initAvecIndividu(EOIndividu individu) {
		setNom(individu.nomUsuel());
		setPrenom(individu.prenom());
		if (individu.prendreEnCompteNumeroProvisoire()) {
			setNoInsee(individu.indNoInseeProv());
		} else {
			setNoInsee(individu.indNoInsee());
		}
	}
	public void modifierAvecElementCarriereEtQuotite(EOElementCarriere elementCarriere, Number quotite,boolean estComparaisonDebut) {
		if (estComparaisonDebut) {
			setGradeDebut(elementCarriere.toGrade().llGrade());
			setEchelonDebut(elementCarriere.cEchelon());
			Number indice = elementCarriere.indiceMajore();
			if (indice != null) {
				setIndiceDebut(indice.toString());
			}
		} else {
			setGradeFin(elementCarriere.toGrade().llGrade());
			setEchelonFin(elementCarriere.cEchelon());
			Number indice = elementCarriere.indiceMajore();
			if (indice != null) {
				setIndiceFin(indice.toString());
			}
			setQuotiteServiceFin(quotite);
		}
	}
	public void modifierAvecAvenantEtQuotite(EOContratAvenant avenant,Number quotite,boolean estComparaisonDebut) {
		if (estComparaisonDebut) {
			if (avenant.toGrade() != null) {
				setGradeDebut(avenant.toGrade().llGrade());
			}
			setEchelonDebut(avenant.cEchelon());
			setIndiceDebut(avenant.indiceContrat());
		} else {
			if (avenant.toGrade() != null) {
				setGradeFin(avenant.toGrade().llGrade());
			}
			setEchelonFin(avenant.cEchelon());
			setIndiceFin(avenant.indiceContrat());
			setQuotiteServiceFin(quotite);
		}
	}
	public void modifierAvecContratHebergeEtQuotite(EOContratHeberges contrat,Number quotite,boolean estComparaisonDebut) {
		if (estComparaisonDebut) {
			if (contrat.toGrade() != null) {
				setGradeDebut(contrat.toGrade().llGrade());
			}
			if (contrat.ctrhInm() != null) {
				setIndiceDebut(contrat.ctrhInm().toString());
			}
		} else {
			if (contrat.toGrade() != null) {
				setGradeFin(contrat.toGrade().llGrade());
			}
			if (contrat.ctrhInm() != null) {
				setIndiceFin(contrat.ctrhInm().toString());
			}
			setQuotiteServiceFin(quotite);
		}
	}

	// Interface NSKeyValueCoding
	public void takeValueForKey(Object valeur,String cle) {
		donnees.setObjectForKey(valeur,cle);
	}
	public Object valueForKey(String cle) {
		return donnees.objectForKey(cle);
	}
	// Interface NSCoding pour le transfert des valeurs entre le client et le serveur
	public Class classForCoder() {
		return EtatComparatifCarriere.class;
	}
	public void encodeWithCoder(NSCoder coder) {
		coder.encodeObject(donnees);
	}
	public static Class decodeClass() {
		return EtatComparatifCarriere.class;
	}
	public static Object decodeObject(NSCoder coder) {
		return new EtatComparatifCarriere((NSDictionary)coder.decodeObject());
	}
	// Méthodes privées
	private Object objectForKey(String value) {
		return donnees.objectForKey(value);
	}
	private void setObjectForKey(Object value,String cle) {
		if (value != null) {
			donnees.setObjectForKey(value, cle);
		}
	}
	// Méthodes statiques
	/** Pr&eacute;pare les comparaisons &agrave; partir :<BR>
	 * - Type personnel = TITULAIRE : des &eacute;l&eacute;ments de carri&egrave;re valides &grave; la date fin et les &eacute;l&eacute;ments de carri&egrave;re ou avenants valides &agrave; la date d&eacute;but pour un type d'enseignant.<BR>
	 * - Type personnel = CONTRACTUEL : des avenants valides &grave; la date fin et des avenants valides &agrave; la date d&eacute;but pour un type d'enseignant.<BR>
	 * - Type personnel = HEBERGE : des contrats h&eacute;berg&eacute;s valides &grave; la date fin et des h&eacute;berg&eacute;s valides &agrave; la date d&eacute;but pour un type d'enseignant.<BR>
	 *  Si typePersonnel = TOUT_TYPE_PERSONNEL, recherche pour les types Titulaires, Contractuel et H&eacute;berg&eacute;<BR>
	 *  Retourne un tableau de EtatComparatifCarriere */
	public static NSArray preparerEtatsComparatifs(EOEditingContext editingContext,NSTimestamp debutPeriode,NSTimestamp finPeriode,int typePersonnel, int typeEnseignant,boolean estComparaisonDebut) {
		NSMutableArray etatsComparatifs = new NSMutableArray();
		NSTimestamp dateAComparer = debutPeriode, dateComparee = finPeriode;
		if (estComparaisonDebut) {	// On veut comparer en utilisant la date de début
			dateAComparer = finPeriode;
			dateComparee = debutPeriode;
		}
		if (typePersonnel == TOUT_TYPE_PERSONNEL || typePersonnel == TITULAIRE) {
			// Rechercher tous les éléments de carrière valides à la date fin
			NSArray elementsCarriere = EOElementCarriere.rechercherElementsPourPeriodeEtTypePersonnel(editingContext, null, null, dateComparee, dateComparee, typeEnseignant, true, false);
			java.util.Enumeration e = elementsCarriere.objectEnumerator();
			while (e.hasMoreElements()) {
				EOElementCarriere element = (EOElementCarriere)e.nextElement();
				EtatComparatifCarriere etat = new EtatComparatifCarriere();
				etat.initAvecIndividu(element.toIndividu());
				Number quotite = null;
				if (estComparaisonDebut == false) {
					quotite = calculerQuotitePourIndividuEtDate(editingContext, element.toIndividu(),dateComparee);
				}
				etat.modifierAvecElementCarriereEtQuotite(element, quotite,estComparaisonDebut);
				// Vérifier si il existe un élément pour l'individu de carrière valide à la date début
				NSArray elementsDebut = EOElementCarriere.findForPeriode(editingContext, element.toIndividu(), dateAComparer, dateAComparer);
				if (elementsDebut.count() > 0) {
					// On prend le premier car les éléments de carrière ne se chevauchent pas
					EOElementCarriere elementDebut = (EOElementCarriere)elementsDebut.objectAtIndex(0);
					if (estComparaisonDebut) {
						quotite = calculerQuotitePourIndividuEtDate(editingContext, element.toIndividu(),dateAComparer);
					}					
					etat.modifierAvecElementCarriereEtQuotite(elementDebut,quotite,!estComparaisonDebut);
				} else {
					// Vérifier si il existe des avenants de contrat de rémunération principale valides à la date début
					NSArray avenants = EOContratAvenant.rechercherAvenantsRemunerationPrincipalePourIndividuEtDates(editingContext, element.toIndividu(), dateAComparer, dateAComparer);
					if (avenants.count() > 0) {
						// Les trier par date début croissante
						avenants = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenants, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
						// On cherche le premier qui a un grade
						java.util.Enumeration e1 = avenants.objectEnumerator();
						while (e1.hasMoreElements()) {
							EOContratAvenant avenant = (EOContratAvenant)e1.nextElement();
							if (avenant.toGrade() != null) {
								if (estComparaisonDebut) {
									quotite = calculerQuotitePourIndividuEtDate(editingContext, avenant.individu(),dateAComparer);
								}	
								etat.modifierAvecAvenantEtQuotite(avenant, quotite, !estComparaisonDebut);
								break;
							}
						}
					}
				}
				etatsComparatifs.addObject(etat);
			}
		}
		// Pour les contractuels
		if (typePersonnel == TOUT_TYPE_PERSONNEL || typePersonnel == CONTRACTUEL) {
			// Rechercher tous les avenants valides à la date fin
			NSArray avenantsCompares = EOContratAvenant.rechercherAvenantsPourDateEtTypePersonnel(editingContext, dateComparee, typeEnseignant);
			java.util.Enumeration e = avenantsCompares.objectEnumerator();
			while (e.hasMoreElements()) {
				EOContratAvenant avenant = (EOContratAvenant)e.nextElement();
				EtatComparatifCarriere etat = new EtatComparatifCarriere();
				etat.initAvecIndividu(avenant.individu());
				Number quotite = null;
				if (estComparaisonDebut == false) {
					quotite = calculerQuotitePourIndividuEtDate(editingContext, avenant.individu(),dateComparee);
				}
				etat.modifierAvecAvenantEtQuotite(avenant, quotite,estComparaisonDebut);
				// Vérifier si il existe des avenants de contrat de rémunération principale valides à la date début
				NSArray avenantsAComparer = EOContratAvenant.rechercherAvenantsRemunerationPrincipalePourIndividuEtDates(editingContext, avenant.individu(), dateAComparer, dateAComparer);
				if (avenantsAComparer.count() > 0) {
					// Les trier par date début croissante
					avenantsAComparer = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenantsAComparer, PeriodePourIndividu.SORT_ARRAY_DATE_DEBUT);
					// On cherche le premier qui a un grade
					java.util.Enumeration e1 = avenantsAComparer.objectEnumerator();
					while (e1.hasMoreElements()) {
						EOContratAvenant avenantAComparer = (EOContratAvenant)e1.nextElement();
						if (avenantAComparer.toGrade() != null) {
							if (estComparaisonDebut) {
								quotite = calculerQuotitePourIndividuEtDate(editingContext, avenantAComparer.individu(),dateAComparer);
							}	
							etat.modifierAvecAvenantEtQuotite(avenantAComparer, quotite, !estComparaisonDebut);
							break;
						}
					}
				}
				etatsComparatifs.addObject(etat);
			}
		}
		// Pour les hébergés
		if (typePersonnel == TOUT_TYPE_PERSONNEL || typePersonnel == HEBERGE) {
			// Rechercher tous les contrats hébergés valides à la date fin
			NSArray contratsFin = EOContratHeberges.rechercherContratsHebergesPourDateEtTypePersonnel(editingContext, dateComparee, typeEnseignant);
			java.util.Enumeration e = contratsFin.objectEnumerator();
			while (e.hasMoreElements()) {
				EOContratHeberges contrat = (EOContratHeberges)e.nextElement();
				EtatComparatifCarriere etat = new EtatComparatifCarriere();
				etat.initAvecIndividu(contrat.individu());
				Number quotite = null;
				if (estComparaisonDebut == false) {
					quotite = calculerQuotitePourIndividuEtDate(editingContext, contrat.individu(),dateComparee);
				}
				etat.modifierAvecContratHebergeEtQuotite(contrat, quotite, estComparaisonDebut);
				// Vérifier si il existe des contrats hébergés à la date début
				NSArray contratsAComparer = EOContratHeberges.findForIndividuAndPeriode(editingContext, contrat.individu(), dateAComparer, dateAComparer);
				if (contratsAComparer.count() > 0) {
					// Les trier par date début croissante
					contratsAComparer = EOSortOrdering.sortedArrayUsingKeyOrderArray(contratsAComparer, new NSArray(EOSortOrdering.sortOrderingWithKey("dateDebut", EOSortOrdering.CompareAscending)));
					// On cherche le premier qui a un grade
					java.util.Enumeration e1 = contratsAComparer.objectEnumerator();
					while (e1.hasMoreElements()) {
						EOContratHeberges contratAComparer = (EOContratHeberges)e1.nextElement();
						if (contratAComparer.toGrade() != null) {
							if (estComparaisonDebut) {
								quotite = calculerQuotitePourIndividuEtDate(editingContext, contratAComparer.individu(),dateAComparer);
							}
							etat.modifierAvecContratHebergeEtQuotite(contratAComparer,quotite,!estComparaisonDebut);
							break;
						}
					}
				}
				etatsComparatifs.addObject(etat);
			}
		}
		return etatsComparatifs;
	}
	
	/**
	 * 
	 * @param editingContext
	 * @param individu
	 * @param date
	 * @return
	 */
	private static Number calculerQuotitePourIndividuEtDate(EOEditingContext editingContext,EOIndividu individu,NSTimestamp date) {
		// Rechercher les affectations valides à la date et ajouter leur quotite
		NSArray affectations = EOAffectation.rechercherAffectationsPourIndividuEtPeriode(editingContext, individu, date, date, true);
		if (affectations.count() == 0) {
			return null;
		}
		int quotite = 0;
		java.util.Enumeration e = affectations.objectEnumerator();
		while (e.hasMoreElements()) {
			EOAffectation affectation = (EOAffectation)e.nextElement();
			quotite += affectation.quotite().intValue();
		}
		return new Integer(quotite);
	}

}

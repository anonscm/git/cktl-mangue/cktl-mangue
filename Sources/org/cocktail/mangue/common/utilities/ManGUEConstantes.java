/*
 * Created on 12 mai 2005
 *
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.utilities;

import java.math.BigDecimal;

import com.webobjects.foundation.NSTimestamp;


/**
 * Contient toutes les declarations de constantes
 */
public class ManGUEConstantes {
		
	// GRHUM_PARAMETRES
	
	public static final String GRHUM_PARAM_KEY_ANNUAIRE_FOUR_ARCHIVE = "ANNUAIRE_FOU_ARCHIVES";
	public static final String GRHUM_PARAM_KEY_ANNUAIRE_FOUR_ENCOURS_VALIDE = "ANNUAIRE_FOU_ENCOURS_VALIDE";
	public static final String GRHUM_PARAM_KEY_ANNUAIRE_FOU_VALIDE_PHYSIQUE = "ANNUAIRE_FOU_VALIDE_PHYSIQUE";

	public static final String GRHUM_PARAM_KEY_CONTROLE_SIRET = "GRHUM_CONTROLE_SIRET";
	public static final String GRHUM_PARAM_KEY_DEFAULT_GID = "GRHUM_DEFAULT_GID";
	public static final String GRHUM_PARAM_KEY_DEFAULT_TIME_ZONE = "DEFAULT_TIME_ZONE";
	public static final String GRHUM_PARAM_KEY_ANNUAIRE_FOU_VALIDE = "ANNUAIRE_FOU_VALIDE_PHYSIQUE";
	public static final String GRHUM_PARAM_KEY_EMPLOYEURS = "ANNUAIRE_EMPLOYEUR";
	public static final String GRHUM_PARAM_KEY_ENTREPRISES = "ENTREPRISES";
	public static final String GRHUM_PARAM_KEY_VILLE = "GRHUM_VILLE";
	public static final String GRHUM_PARAM_KEY_DEFAULT_RNE = "GRHUM_DEFAULT_RNE";
	public static final String GRHUM_PARAM_KEY_CONTROLE_INSEE = "GRHUM_CONTROLE_INSEE";
	public static final String GRHUM_PARAM_KEY_GRHUM_PHOTO = "GRHUM_PHOTO";
	public static final String GRHUM_PARAM_KEY_NO_POSTE_RECTORAT = "GRHUM_NO_POSTE_RECTORAT";
	public static final String GRHUM_PARAM_KEY_ETAB = "GRHUM_ETAB";
	public static final String GRHUM_PARAM_KEY_PRESIDENT = "GRHUM_PRESIDENT";
	
	// MANGUE_PARAMETRES

	public static final String PARAM_KEY_SIRET_OBLIGATOIRE = "org.cocktail.mangue.siret_obligatoire";
	public static final String PARAM_KEY_MINISTERE = "org.cocktail.mangue.ministere";
	public static final String PARAM_KEY_GESTION_NUMERO_EMPLOI = "org.cocktail.mangue.gestion_numero_local";
	public static final String PARAM_KEY_BLOCAGE_DOUBLON_INSEE = "org.cocktail.mangue.blocage_doublon_insee";
	public static final String PARAM_KEY_GESTION_HU = "org.cocktail.mangue.gestion_hu";
	public static final String PARAM_KEY_CONGES_ENS = "org.cocktail.mangue.conges_ens";
	public static final String PARAM_KEY_SIGNATURE_PRES = "org.cocktail.mangue.signature_president";
	public static final String PARAM_KEY_LISTE_ELECTORALE = "org.cocktail.mangue.liste_electorale";
	public static final String PARAM_KEY_PHOTOS = "org.cocktail.mangue.voir_photos";
	public static final String PARAM_KEY_CTRL_MAL_MTT = "org.cocktail.mangue.ctrl_tpt_mal";
	public static final String PARAM_KEY_INS_AFF_STRUCTURE = "org.cocktail.mangue.maj_repart_structure";
	public static final String PARAM_KEY_ORDO_UNIV = "org.cocktail.mangue.ordonnateur_univ";
	public static final String PARAM_KEY_ORDO_IUT = "org.cocktail.mangue.ordonnateur_iut";
	public static final String PARAM_KEY_TYPES_CONTRAT_VAC = "org.cocktail.mangue.vacations_type_contrat";
	public static final String PARAM_KEY_INS_AFF_VAC_STRUCTURE = "org.cocktail.mangue.maj_repart_structure_vacataires";
	public static final String PARAM_KEY_NO_ARRETE_AUTO = "org.cocktail.mangue.no_arrete_auto";
	public static final String PARAM_KEY_GESTIONNAIRES = "org.cocktail.mangue.use_gestionnaires";
	public static final String PARAM_KEY_MODULE_PRIMES = "org.cocktail.mangue.use_module_primes";
	public static final String PARAM_KEY_UNITE_GESTION= "org.cocktail.mangue.unite_gestion";
	public static final String PARAM_KEY_COMPTE_AUTO= "org.cocktail.mangue.compte_auto";
	public static final String PARAM_KEY_COMPTE_PASSWORD_CLAIR = "org.cocktail.mangue.compte_password_clair";
	public static final String PARAM_KEY_COMPLEMENT_VISITE_MEDICALE = "org.cocktail.mangue.complement_visite_medicale";
	public static final String PARAM_KEY_ANNEE_GARDE_ENFANT = "org.cocktail.mangue.annee_ref_garde_enfant";
	public static final String PARAM_KEY_USE_SIFAC = "org.cocktail.mangue.use_sifac";
	public static final String PARAM_KEY_GESTION_LOCAUX = "org.cocktail.mangue.gestion_locaux";

	public static final String PARAM_KEY_OCCUPATION_CARRIERE = "org.cocktail.mangue.occupation_carriere";
	public static final String PARAM_KEY_AFFECTATION_CARRIERE = "org.cocktail.mangue.affectation_carriere";
	public static final String PARAM_KEY_AFF_TYPE_CONTRAT = "org.cocktail.mangue.affichage_type_contrat";
	public static final String PARAM_KEY_NO_MATRICULE = "org.cocktail.mangue.no_matricule";

	// CONGES
	
	public static final String ABS_PARAM_KEY_CLM_DUREE_BLOQUANTE = "org.cocktail.mangue.conges.clm_duree_bloquante";
	public static final String ABS_PARAM_KEY_CLM_DUREE_NON_BLOQUANTE = "org.cocktail.mangue.conges.clm_duree_non_bloquante";
	public static final String ABS_PARAM_KEY_CLM_DEMI_TRAITEMENT = "org.cocktail.mangue.conges.clm_demi_traitement";
	public static final String ABS_PARAM_KEY_CLM_PLEIN_TRAITEMENT = "org.cocktail.mangue.conges.clm_plein_traitement";
	public static final String ABS_PARAM_KEY_CLM_PERIODE_REFERENCE = "org.cocktail.mangue.conges.clm_periode_reference";

	public static final String ABS_PARAM_KEY_CMO_DEMI_TRAITEMENT = "org.cocktail.mangue.conges.cmo_demi_traitement";
	public static final String ABS_PARAM_KEY_CMO_PLEIN_TRAITEMENT = "org.cocktail.mangue.conges.cmo_plein_traitement";
	public static final String ABS_PARAM_KEY_CMO_PERIODE_REFERENCE = "org.cocktail.mangue.conges.cmo_periode_reference";
	public static final String ABS_PARAM_KEY_CMO_DUREE_MAX = "org.cocktail.mangue.conges.cmo_duree_max";

	public static final String ABS_PARAM_KEY_CMNT_PERIODE_REFERENCE_C = "org.cocktail.mangue.conges.cmnt_periode_reference_c";
	public static final String ABS_PARAM_KEY_CMNT_PERIODE_REFERENCE_D = "org.cocktail.mangue.conges.cmnt_periode_reference_d";

	public static final String ABS_PARAM_KEY_CFP_DUREE_MIN = "org.cocktail.mangue.conges.cfp_duree_min";
	public static final String ABS_PARAM_KEY_CFP_DUREE_MAX = "org.cocktail.mangue.conges.cfp_duree_max";

	public static final String ABS_PARAM_KEY_CLD_DUREE_MIN = "org.cocktail.mangue.conges.cld_duree_min";
	public static final String ABS_PARAM_KEY_CLD_DUREE_MAX = "org.cocktail.mangue.conges.cld_duree_max";

	public static final String ABS_PARAM_KEY_CRCT_DUREE_MIN = "org.cocktail.mangue.conges.crct_duree_min";
	public static final String ABS_PARAM_KEY_CRCT_DUREE_MAX = "org.cocktail.mangue.conges.crct_duree_max";
	public static final String ABS_PARAM_KEY_CRCT_PERIODICITE = "org.cocktail.mangue.conges.crct_periodicite";
	public static final String ABS_PARAM_KEY_CRCT_DUREE_TOTALE = "org.cocktail.mangue.conges.crct_duree_totale";
	public static final String ABS_PARAM_KEY_CRCT_DUREE_ACTIVITE = "org.cocktail.mangue.conges.crct_duree_activite";

	public static final String ABS_PARAM_KEY_CGM_DUREE_MIN = "org.cocktail.mangue.conges.cgm_duree_min";
	public static final String ABS_PARAM_KEY_CGM_DUREE_MAX = "org.cocktail.mangue.conges.cgm_duree_max";
	public static final String ABS_PARAM_KEY_CGM_ANCIENNETE = "org.cocktail.mangue.conges.cgm_anciennete";
	public static final String ABS_PARAM_KEY_CGM_DEMI_TRAITEMENT = "org.cocktail.mangue.conges.cgm_demi_traitement";
	public static final String ABS_PARAM_KEY_CGM_PLEIN_TRAITEMENT = "org.cocktail.mangue.conges.cgm_plein_traitement";
	public static final String ABS_PARAM_KEY_CGM_DELAI = "org.cocktail.mangue.conges.cgm_delai";

	public static final String ABS_PARAM_KEY_CMAT_PERIODE_PRENATALE = "org.cocktail.mangue.conges.cmat_periode_prenatale";
	public static final String ABS_PARAM_KEY_CMAT_PERIODE_POSTNATALE = "org.cocktail.mangue.conges.cmat_periode_postnatale";

	public static final String ABS_PARAM_KEY_ADOPT_DUREE = "org.cocktail.mangue.conges.adopt_duree";
	public static final String ABS_PARAM_KEY_ADOPT_DUREE_2 = "org.cocktail.mangue.conges.adopt_duree_2";
	public static final String ABS_PARAM_KEY_ADOPT_DUREE_3 = "org.cocktail.mangue.conges.adopt_duree_3";

	// MODALITES
	
	public static final String ABS_PARAM_KEY_TP_DUREE_MAX_ENS = "org.cocktail.mangue.modalites_duree_max_tp_ens";
	public static final String ABS_PARAM_KEY_TP_DUREE_MIN_ENS = "org.cocktail.mangue.modalites.duree_min_tp_ens";
	public static final String ABS_PARAM_KEY_QUOT_MAX_DROIT = "org.cocktail.mangue.modalites.quotite_max_tp_droit";
	public static final String ABS_PARAM_KEY_QUOT_MIN_DROIT = "org.cocktail.mangue.modalites.quotite_min_tp_droit";

	public static final String ABS_PARAM_KEY_TPT_DUREE_ACC_SERV = "org.cocktail.mangue.modalites.tpt.duree_acc_serv";
	public static final String ABS_PARAM_KEY_TPT_DIREE_CLD_CLM = "org.cocktail.mangue.modalites.tpt.duree_cld_clm";

	// PROLONGATIONS
	
	public static final String ABS_PARAM_KEY_PROLONG_RA_MIN = "org.cocktail.mangue.prolong.recul_age_duree_min";
	public static final String ABS_PARAM_KEY_PROLONG_RA_MAX = "org.cocktail.mangue.prolong.recul_age_duree_max";
	public static final String ABS_PARAM_KEY_PROLONG_RA_AGE_MIN = "org.cocktail.mangue.prolong.recul_age_age_min";

	public static final String ABS_PARAM_KEY_PROLONG_MAINTIEN_DUREE_MAX = "org.cocktail.mangue.prolong.maintien_duree_max";
	public static final String ABS_PARAM_KEY_PROLONG_MAINTIEN_AGE_MIN = "org.cocktail.mangue.prolong.maintien_age_min";
	public static final String ABS_PARAM_KEY_PROLONG_MAINTIEN_AGE_MAX = "org.cocktail.mangue.prolong.maintien_age_max";

	public static final String ABS_PARAM_KEY_PROLONG_SURNOMBRE_DUREE = "org.cocktail.mangue.prolong.surnombre_duree";
	public static final String ABS_PARAM_KEY_PROLONG_SURNOMBRE_LIMITE_AGE = "org.cocktail.mangue.prolong.surnombre_limite_age";

	
	// MAD
	
	public static final String ABS_PARAM_KEY_MAD_DUREE_MAX = "org.cocktail.mangue.mad_duree_max";

	public final static String NOMENCLATURE_CONTRATS = "01/01/2014";

	public final static String DEBUT_PERIODES_ESSAI = "04/11/2014";

	public final static String DEFAULT_DATE_OUVERTURE = "01/01/1800";

	public final static int ANNEE_DEMARRAGE_CALCUL_DIF = 2008;
	public final static int ANNEE_DEMARRAGE_CONSO_DIF = 2008;

	// ETAT CIVIL
	
	public static final String CODE_SEXE_HOMME = "1";
	public static final String CODE_SEXE_FEMME = "2";
	public static final String IND_ACTIVITE_VACATION = "VACATAIRE";

	public static final int LONGUEUR_INSEE = 13;

	public final static String C_TYPE_STRUCTURE_ETABLISSEMENT = "E";
	public final static String C_TYPE_STRUCTURE_ETAB_SECONDAIRE = "ES";
	public final static String C_TYPE_STRUCTURE_COMPOSANTE = "C";
	public final static String C_TYPE_STRUCTURE_COMP_STATUTAIRE = "CS";
	public final static String C_TYPE_STRUCTURE_AUTRE = "A";

	// MINISTERES
	
	public final static String CODE_MIN_MESR = "MI280";
	public final static String CODE_MIN_MEN  = "MI180";
	public final static String CODE_MIN_MAAF = "MI130";
	
	// TEMPS PARTIEL

	public final static int DUREE_MOIS_MAX_TPS_PARTIEL_ENFANT = 36;
	/** Date a partir de laquelle il faut declarer les enfants dans les temps partiels */
	public final static NSTimestamp DATE_LIMITE_DECLARATION_ENFANT = DateCtrl.stringToDate("01/01/2004");

	// Positions	
	public final static String DEBUT_VALIDITE_REFERENS = "01/01/2002";

	public final static BigDecimal QUOTITE_TRAVAIL_80 = new BigDecimal(80);
	public final static BigDecimal QUOTITE_FINANCIERE_80 = new BigDecimal(85.7);
	public final static BigDecimal QUOTITE_TRAVAIL_90 = new BigDecimal(90);
	public final static BigDecimal QUOTITE_FINANCIERE_90 = new BigDecimal(91.4);
	public final static BigDecimal QUOTITE_100 = new BigDecimal(100);
	
	public final static String STATUT_RNE_PUBLIC = "PU";
	public final static String STATUT_RNE_PRIVE = "PR";

	/** code civilite pour une homme  */
	public final static String MONSIEUR = "M.";
	/** code civilite pour une femme mariee */
	public final static String MADAME = "MME";
	/** code civilite pour une demoiselle */
	public final static String MADEMOISELLE = "MLLE";
	 /** personnel enseignant */
	 public static int ENSEIGNANT = 0;
	 /** personnel non enseignant */ 
	 public static int NON_ENSEIGNANT = 1;
	 /** personnel vacataire */ 
	 public static int VACATAIRE = 2;
	 /** personnel heberge */
	 public static int HEBERGE = 3;
	 /** tout type de personnel */
	 public static int TOUT_PERSONNEL = 4;
	 /** personnel actuel */
	 public static int EMPLOYE_ACTUEL = 0;
	 /** personnel ancien */ 
	 public static int EMPLOYE_ANCIEN = 1;
	 /** personnel futur */ 
	 public static int EMPLOYE_FUTUR = 2;
	 /** personnel non-affecte */ 
	 public static int NON_AFFECTE = 3;
	 /** tout employe (non-affecte, passe, present, futur) */
	 public static int TOUT_EMPLOYE = 4;
	 /** tout emploi a afficher */
	 public static int TOUT_EMPLOI = 4;
	 /** tout poste a afficher */
	 public static int TOUT_POSTE = 3;
	 /** Electeurs retenus */
	 public static final int RETENUS = 0;
	 /** Electeurs ajoutes; */
	 public static final int AJOUTES = 1;
	 /** Electeurs exclus */
	 public static final int EXCLUS = 2;
	 /** Electeurs sans bureau de vote */
	 public static final int SANS_BUREAU_VOTE = 3;
	 /** Electeurs avec plusieurs affectations */
	 public static final int MULTI_AFFECTATIONS = 4;
	 /** Tous les electeurs  */
	 public static final int TOUS = 5;
	 /** Impression  de donnees resulant d'un module d'impression au format PDF */
	 public static final int IMPRESSION_PDF = 0;
	 /** Impression  de donn&eacute;es resultant d'un module d'impression au format XLS */
	 public static final int IMPRESSION_EXCEL = 1;
	 /** type d'arrete de nbi : prise de fonction */
	 public static String TYPE_ARRETE_NBI_PRISE_DE_FONCTION = "P";
	 /** type d'arrete de nbi : cessation de fonction */
	 public static String TYPE_ARRETE_NBI_CESSATION = "C";
	 /** type d'arrete de nbi : regularisation */
	 public static String TYPE_ARRETE_NBI_REGULARISATION = "M";

	 

}

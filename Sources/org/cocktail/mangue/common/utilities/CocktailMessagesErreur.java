package org.cocktail.mangue.common.utilities;


public class CocktailMessagesErreur {

    /* Message d'erreur général */
	
    public static final String ERREUR_INDIVIDU_NON_RENSEIGNE = "%s\nVeuillez renseigner un individu !";

    public static final String ERREUR_DATE_AVANT_1900 = "%s\nVous ne pouvez pas saisir une date antérieure au 01/01/1900";

    public static final String ERREUR_DATE_DEBUT_NON_RENSEIGNE = "%s\nVeuillez renseigner une date de début !";
    public static final String ERREUR_DATE_FIN_NON_RENSEIGNE = "%s\nVeuillez renseigner une date de fin !";
    public static final String ERREUR_STRUCTURE_NON_RENSEIGNE = "%s\nVeuillez renseigner une structure !";
    public static final String ERREUR_UAI_NON_RENSEIGNE = "%s\nVeuillez renseigner une structure UAI !";    

    public static final String ERREUR_DATE_FIN_AVANT_DATE_DEBUT = "%s\nLa date de fin (%s) doit être postérieure à la date de début (%s) !";
}
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.common.utilities;

import javax.swing.ImageIcon;

import com.webobjects.eoapplication.EOModalDialogController;
import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class ManGUEIcones extends EOModalDialogController 
{

	protected	static final EOClientResourceBundle resourceBundle = new EOClientResourceBundle();	
	public static final ImageIcon ICON_SERVICE_NATIONAL = (ImageIcon)resourceBundle.getObject("serv_nat");
	public static final ImageIcon ICON_DIPLOMES			= (ImageIcon)resourceBundle.getObject("diplome");
	public static final ImageIcon ICON_COORDONNEES			= (ImageIcon)resourceBundle.getObject("coordonnees");
	public static final ImageIcon ICON_TELEPHONE			= (ImageIcon)resourceBundle.getObject("telephone");
	public static final ImageIcon ICON_ADRESSE			= (ImageIcon)resourceBundle.getObject("adresse");
	public static final ImageIcon ICON_COMPTE			= (ImageIcon)resourceBundle.getObject("compte");
	public static final ImageIcon ICON_FORMATION		= (ImageIcon)resourceBundle.getObject("formation");
	public static final ImageIcon ICON_LOCK = (ImageIcon)resourceBundle.getObject("mangue_lock");

	public static final ImageIcon ICON_DOSSIER_FICHE = (ImageIcon)resourceBundle.getObject("dossier_fiche");
	public static final ImageIcon ICON_DOSSIER_PERSO = (ImageIcon)resourceBundle.getObject("dossier_perso");
	public static final ImageIcon ICON_DOSSIER_COMP = (ImageIcon)resourceBundle.getObject("dossier_comp");
	public static final ImageIcon ICON_DOSSIER_CIR = (ImageIcon)resourceBundle.getObject("dossier_cir");
	public static final ImageIcon ICON_DOSSIER_ONP = (ImageIcon)resourceBundle.getObject("dossier_cir");
	public static final ImageIcon ICON_DOSSIER_SITUATION = (ImageIcon)resourceBundle.getObject("dossier_situation");

	public static final ImageIcon ICON_DOSSIER_BLEU = (ImageIcon)resourceBundle.getObject("cktl_dossier_bleu");
	public static final ImageIcon ICON_DOSSIER_JAUNE = (ImageIcon)resourceBundle.getObject("cktl_dossier_jaune");
	public static final ImageIcon ICON_DOSSIER_ROUGE = (ImageIcon)resourceBundle.getObject("cktl_dossier_rouge");
	public static final ImageIcon ICON_DOSSIER_VERT = (ImageIcon)resourceBundle.getObject("cktl_dossier_vert");
	public static final ImageIcon ICON_DOSSIER_VIOLET = (ImageIcon)resourceBundle.getObject("cktl_dossier_violet");

	public static final ImageIcon ICON_EMPLOI = (ImageIcon)resourceBundle.getObject("emploi");

}
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.mangue.common.utilities;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;

public class AskForString 
{
	private static AskForString sharedInstance;	
	protected 		EOEditingContext 			ec;

	private AskForStringView myView;

	private boolean	selectionValide;

	/**
	 *	
	 */
	public AskForString()	{
		
		super();

		myView = new AskForStringView(new JFrame(), true);

		myView.getButtonValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				valider();
			}
		});

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.addWindowListener(new localWindowListener());

		myView.getTfLibelle().addActionListener(new ActionListenerMontant());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static AskForString sharedInstance()	{
		if (sharedInstance == null)	
			sharedInstance = new AskForString();
		return sharedInstance;
	}


	/** 
	 *	Recuperation d'un montant saisi par l'utilisateur 
	 */
	public String getString(String windowTitle, String prompt, String defaultLibelle)	{
		
		myView.setTitle(windowTitle);
		myView.getLblTitre().setText(prompt);

		if (defaultLibelle != null)
			myView.getTfLibelle().setText(defaultLibelle.toString());
		else
			myView.getTfLibelle().setText("");

		myView.setVisible(true);

		if (!selectionValide)	
			return null;

		try {
			return myView.getTfLibelle().getText();
		}
		catch (NumberFormatException e )	{
			EODialogs.runErrorDialog("ERREUR","Erreur du format de saisie. Veuillez entrer un nombre valide !.");
			return null;
		}
	}

	/** 
	 *	VALIDER 
	 */
	public void valider()	{

		if ( ("".equals(StringCtrl.recupererChaine(myView.getTfLibelle().getText()))))	{
			EODialogs.runInformationDialog("ERREUR","Erreur de saisie !");
			return;
		}
		
		selectionValide = true;
		
		myView.dispose();
	}

	/** 
	 *	ANNULER :
	 */
	public void annuler()	{

		selectionValide = false;
		myView.dispose();
		
	}

	/** 
	 *	Class listener sur la window principale 
	 */
	private class localWindowListener implements WindowListener	{
		public localWindowListener () 	{super();}

		public void windowActivated(WindowEvent e) 	{
			myView.getTfLibelle().requestFocus();			
		}

		public void windowClosed(WindowEvent e) {}

		public void windowOpened(WindowEvent e) 	{

			myView.getTfLibelle().requestFocus();	
			
		}

		public void windowIconified(WindowEvent e) {}
		public void windowDeiconified(WindowEvent e) {}
		public void windowClosing(WindowEvent e) {}
		public void windowDeactivated(WindowEvent e) {}
	}

	public class ActionListenerMontant implements ActionListener	{
		public void actionPerformed(ActionEvent e)	{
			valider();
		}
	}


}
package org.cocktail.mangue.common.utilities;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JPanel;


public final class BusyGlassPane extends JPanel     {
    public final Color COLOR_WASH = new Color(64, 64, 64, 32);
    public BusyGlassPane() {
        super.setOpaque(false);
        super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        //Suck up them events!!!
        super.addKeyListener( (new KeyAdapter() { }) );
        super.addMouseListener( (new MouseAdapter() { }) );
        super.addMouseMotionListener( (new MouseMotionAdapter() { }) );
    }

  
    public final void paintComponent(Graphics p_graphics) {
        Dimension l_size = super.getSize();

        // Wash the pane with translucent gray.
        p_graphics.setColor(COLOR_WASH);
        p_graphics.fillRect(0, 0, l_size.width, l_size.height);

        // Paint a grid of white/black dots.
        p_graphics.setColor(Color.white);
        for (int j=3; j<l_size.height; j+=8) {
            for (int i=3; i<l_size.width; i+=8) {
                p_graphics.fillRect(i,j,1,1);
            }
        }
        p_graphics.setColor(Color.black);
        for (int j=4; j<l_size.height; j+=8) {
            for (int i=4; i<l_size.width; i+=8) {
                p_graphics.fillRect(i,j,1,1);
            }
        }
    }

} 
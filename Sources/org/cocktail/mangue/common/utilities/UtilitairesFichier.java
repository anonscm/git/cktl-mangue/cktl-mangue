package org.cocktail.mangue.common.utilities;

import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import org.cocktail.application.common.utilities.CocktailExports;
import org.cocktail.common.LogManager;
import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

/** Contient des methodes statiques pour afficher les fichiers de differents formats */
public class UtilitairesFichier {
	
	private static final String LNX_CMD = "evince ";
	private static final String MAC_CMD = "open ";
	private final String TEMP_PATH = System.getProperty("java.io.tmpdir").concat("/");
	private String temporaryDir;

	/**
	 * 
	 * @return
	 */
	public String getTemporaryDir() {
		return temporaryDir;
	}

	/**
	 * 
	 * @param dir
	 */
	public void setTemporaryDir(String dir) {
		temporaryDir = dir;
	}

	private static int numImpression = 1;
	
	/** affiche des datas qui contiennent du pdf
	 * @param datas
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier Pdf sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherPdf(NSData datas,String pathDir, String fileName,boolean estCopieUnique)	{
		afficherFichier(datas,pathDir,fileName,"pdf",estCopieUnique);
	}
	/** affiche des datas qui contiennent des donnees pour Excel
	 * @param texte texte a exporter, converti au format ISO-8859-1
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier excel sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherFichierExcel(String texte,String pathDir, String fileName,boolean estCopieUnique)	{
		NSData data = new NSData(texte, CocktailExports.ENCODAGE_ISO_8859_1);
		afficherFichier(data,pathDir,fileName,"xls",estCopieUnique);
	}
	/** affiche des datas qui contiennent des donnees pour Excel
	 * @param datas
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier excel sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherFichierExcel(NSData datas,String pathDir, String fileName,boolean estCopieUnique)	{
		afficherFichier(datas,pathDir,fileName,"xls",estCopieUnique);
	}
	/** affiche des donnees a exporter au format tab-tab-return
	 * @param texte texte a exporter, converti au format ISO-8859-1
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherFichierAvecTabs(String texte, String pathDir,String fileName,boolean estCopieUnique)	{
		NSData data = new NSData(texte, CocktailExports.ENCODAGE_ISO_8859_1);
		afficherFichier(data,pathDir,fileName,"xls",estCopieUnique);
	}
	/** affiche des donnees a exporter au format tab-tab-return
	 * @param texte texte a exporter, converti au format ISO-8859-1
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */
	public static void afficherFichierTexte(String texte, String pathDir,String fileName,boolean estCopieUnique)	{
		NSData data = new NSData(texte, CocktailExports.ENCODAGE_ISO_8859_1);
		afficherFichier(data,pathDir,fileName,"txt",estCopieUnique);
	}
	/** affiche des donnees copiees dans un fichier
	 * @param texte texte a exporter, converti au format ISO-8859-1
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param extension extension du fichier
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */	
	public static void afficherFichier(String texte, String pathDir,String fileName,String extension,boolean estCopieUnique) {
		NSData data = new NSData(texte, CocktailExports.ENCODAGE_ISO_8859_1);
		afficherFichier(data,pathDir,fileName,extension,estCopieUnique);
	}
	/** enregistre en local les donnees dans un fichier
	 * @param texte texte a exporter, converti au format UTF-8 (compatible Windows et linux)
	 * @param cheminDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param extension extension du fichier
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 * @return le chemin d'acc&egrave;s du fichier
	 */	
	public static String enregistrerFichier(String texte, String cheminDir,String fileName,String extension,boolean estCopieUnique) {
		NSData datas = new NSData(texte, CocktailExports.ENCODAGE_ISO_8859_1);
		return enregistrerFichier(datas, cheminDir, fileName, extension, estCopieUnique);
	}
	/** enregistre en local les donnees dans un fichier
	 * @param datas datas a ecrire dans le fichier
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param extension extension du fichier (null si pas d'extension)
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 * @return le chemin d'acces du fichier
	 */	
	public static String enregistrerFichier(NSData datas, String cheminDir,String fileName,String extension,boolean estCopieUnique) {

		String fileSeparator = System.getProperty("file.separator");
		if (cheminDir.substring(cheminDir.length() -1, cheminDir.length()).equals(fileSeparator) == false) {
			cheminDir = cheminDir.concat(fileSeparator);
		}

		verifierPathEtCreer(cheminDir);

		byte b[] = datas.bytes();
		ByteArrayInputStream stream = new ByteArrayInputStream(b);
		// Remplacer tous les / par des - si il y en a dans le nom de fichier
		fileName = fileName.replaceAll("/", "-");
		if (estCopieUnique) {
			fileName = fileName + "_" + numImpression++;
		}
		String filePath = cheminDir + File.separator + fileName;
		LogManager.logDetail("path fichier " + filePath);

		if (extension != null  && extension.length() > 0) {
			if (StringCtrl.containsIgnoreCase(extension, "."))
				filePath +=  extension;
			else
				filePath +=  "." + extension;
		}
		LogManager.logDetail("path fichier avec extension " + filePath);
		try {
			StreamCtrl.saveContentToFile (stream, filePath);
			return filePath;
		} catch (Exception e) {
			e.printStackTrace();
			EODialogs.runErrorDialog("Erreur", "Une erreur s'est produite pendant la création du fichier : " + e.getMessage());
			return null;
		}
	}
	/** affiche un fichier de donnees
	 * @param datas datas a ecrire dans le fichier
	 * @param pathDir directory dans lequel generer le fichier
	 * @param fileName nom du fichier sans extension
	 * @param extension extension du fichier
	 * @param estCopieUnique true si on souhaite qu'un compteur soit incremente et ajouter au nom de fichier
	 */	
	public static void afficherFichier(NSData datas, String cheminDir,String fileName,String extension,boolean estCopieUnique) {
	
		try {
			String filePath = enregistrerFichier(datas, cheminDir, fileName, extension, estCopieUnique);			
			if (filePath != null)
				openFile(filePath);
		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR", e.getMessage());
		}
	}
	public static void verifierPathEtCreer(String unPath) {
		// Vérifier récursivement que tous les directories existent
		// 20/10/2010 suppression du split pour la création des directories parent et utilisation de mkdirs
		File file = new File(unPath);
		// créer le directory si ils n'existe pas
		if (file.exists() == false) {
			boolean result = file.mkdirs();
			LogManager.logDetail("creation du dir d'impression " + result);
		}
	}
	
	/**
	 * ouvre le pdf a partir d'un chemin param filePath le chemin du pdf a ouvrir
	 */
	public static void openFile(String filePath) throws Exception {
		File aFile = new File(filePath);
		Runtime runtime = Runtime.getRuntime();
		if (System.getProperty("os.name").startsWith("Windows")) {
			runtime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\"" });
		}
		else
			if (System.getProperty("os.name").startsWith("Linux")) {
				runtime.exec(LNX_CMD + aFile);
			}
			else {
				runtime.exec(MAC_CMD + aFile);
			}

	}	

	/** cree une archive (.zip) contenant les fichiers fournis dans le repertoire fourni
	 * @param pathArchive chemin d'acc&grave;s du directory dans lequel creer l'archive
	 * @param nomArchive nom de l'archive sans extension
	 * @param filePaths paths des fichiers à inclure dans l'archive avec leur extension 
	 * @param true si on supprime les fichiers après inclusion dans le zip
	 * @return true si le zip est cree */
	public static boolean creerArchiveZip(String pathArchive,String nomArchive, NSArray filePaths,boolean supprimerFichiers) {
		// Create a buffer for reading the files
		byte[] buf = new byte[1024];
		try {
			if (pathArchive == null) {
				return false;
			}
			// créer le fichier Zip
			if (pathArchive.endsWith("File.separator") == false) {
				pathArchive += File.separator;
			}
			String outFileName = pathArchive + nomArchive;
			if (nomArchive.indexOf(".zip") < 0) {
				outFileName += ".zip";
			}
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFileName));
			// Compresser les fichier
			java.util.Enumeration e =  filePaths.objectEnumerator();
			while (e.hasMoreElements()) {
				String pathFichier = (String)e.nextElement();
				File fileFichier = new File(pathFichier);
				FileInputStream in = new FileInputStream(pathFichier);
				// Ajouter le fichier
				out.putNextEntry(new ZipEntry(fileFichier.getName()));
				// Transférer les bytes du fichier dans le fichier ZIP
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				// Terminer
				out.closeEntry();
				in.close();
			}
			// Fermer le fichier Zip
			out.close();
			if (supprimerFichiers) {
				e =  filePaths.objectEnumerator();
				while (e.hasMoreElements()) {
					String pathFichier = (String)e.nextElement();
					File fileFichier = new File(pathFichier);
					fileFichier.delete();
				}
			}
			return true;
		} catch (IOException exc) {
			LogManager.logException(exc);
			return false;
		}
	}
	
	/**
	 * 
	 * @param data
	 * @param fileName
	 * @param extension
	 * @return
	 * @throws Exception
	 */
	public String dataToXXX(NSData data, String fileName, String extension) throws Exception {
		String filePath = "";

		if (data == null) {
			throw new Exception("Impossible de recupérer le fichier " + fileName);
		}

		setTemporaryDir(TEMP_PATH);

		filePath = getTemporaryDir() + fileName + "." + extension;

		FileOutputStream fileOutputStream = new FileOutputStream(filePath);
		data.writeToStream(fileOutputStream);
		fileOutputStream.close();

		File tmpFile = new File(filePath);
		if (!tmpFile.exists()) {
			throw new Exception("Le fichier " + filePath + " n'existe pas.");
		}

		return filePath;
	}

	/** retourne la liste des fichiers dans un repertoire */
	public static NSArray listFiles(File directory) {
		int i;
		if (directory == null || !directory.exists()) {
			return new NSArray();
		}
		NSMutableArray returnedList = new NSMutableArray();
		File[] dirFileList = directory.listFiles();
		for (i = 0; i < dirFileList.length; i++) {
			if (dirFileList[i].isDirectory()) {
				returnedList.addObjectsFromArray(listFiles(dirFileList[i]));
			}
			else {
				returnedList.addObject(dirFileList[i]);
			}
		}
		return returnedList;
	}

	public static byte[] imageToByteArray(Image img, String formatName) {
		if (img instanceof RenderedImage == false) {
			img = getBufferedImage(img, Transparency.TRANSLUCENT);
		}

		ByteArrayOutputStream byteOS = new ByteArrayOutputStream();
		try {
			ImageIO.write((RenderedImage) img, formatName, byteOS);
		}
		catch (IOException e) {
			return null;
		}
		return byteOS.toByteArray();
	}

	public static BufferedImage getBufferedImage(Image img, int transparency) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}

		GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
		BufferedImage bi = gc.createCompatibleImage(img.getWidth(null), img.getHeight(null), transparency);
		Graphics g = bi.createGraphics();
		g.drawImage(img, 0, 0, null);
		g.dispose();
		return bi;
	}

	/**
	 * Methode qui renvoie le chemin ou a ete depose le pdf
	 * 
	 * @param data
	 *            les donnees du pdf
	 * @param filename
	 *            le nom du fichier qu'on veut lui donner
	 * @return le chemin du pdf
	 */
	public String dataToPDF(NSData data, String fileName) {
		String filePath = "";

		if (data == null) {
			System.out.println("Impossible de recuperer le PDF.");
			return null;
		}

		setTemporaryDir(TEMP_PATH);
		filePath = getTemporaryDir() + fileName + ".pdf";

		try {
			FileOutputStream fileOutputStream = new FileOutputStream(filePath);
			data.writeToStream(fileOutputStream);
			fileOutputStream.close();
		}
		catch (Exception exception) {
			/*
			 * WindowHandler.showError( "Impossible d'ecrire le fichier PDF sur le disque.\n Verifiez qu'un autre fichier n'est pas deja
			 * ouvert." );
			 */
			exception.printStackTrace();
			return null;
		}
		try {
			File tmpFile = new File(filePath);
			if (!tmpFile.exists()) {
				System.out.println("Le fichier " + filePath + " n'existe pas.");
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return filePath;
	}

}

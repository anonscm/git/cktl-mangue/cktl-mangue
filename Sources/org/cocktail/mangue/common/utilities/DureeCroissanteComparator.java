/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.utilities;

import org.cocktail.mangue.modele.IDuree;

import com.webobjects.foundation.NSComparator;

/** Compare des objets de type InterfaceDuree : permet de ranger les dur&eacute;es par ordre de date d&eacute;but croissant. Si la date
 * de d&eacute;but est nulle, elle est consid&eacute;r&eacute;e comme une date minimum. En cas d'&eacute;galit&eacute; des dates
 * de d&eacute;but, les dates sont class&eacute;es par ordre de date fin croissante sachant que si la date fin est nulle, 
 * on la consid&egrave;re comme une date maximale. 
 * 
 * @author christine
 *
 */
public class DureeCroissanteComparator extends NSComparator {
	public DureeCroissanteComparator() {
		super();
	}
	public int compare(Object arg0, Object arg1) throws ComparisonException {
		//   OrderedAscending : la valeur du premier argument est moins que la valeur du second
		if (arg0 instanceof IDuree && arg1 instanceof IDuree) {
			IDuree duree0 = (IDuree)arg0, duree1 = (IDuree)arg1;
			if (duree0.dateDebut() == null) {	
				if (duree1.dateDebut() == null) {
					return NSComparator.OrderedSame;
				} else {
					return NSComparator.OrderedAscending;	 // debut null en tête
				}
			} else {	
				if (duree1.dateDebut() == null) {
					return NSComparator.OrderedDescending;		// debut null en tête
				} else {
					if (DateCtrl.isBefore(duree0.dateDebut(), duree1.dateDebut())) {
						return NSComparator.OrderedAscending;
					} else if (DateCtrl.isAfter(duree0.dateDebut(), duree1.dateDebut())) {
						return NSComparator.OrderedDescending;
					} else {
						// Mêmes dates de début, regarder les dates de fin
						if (duree0.dateFin() == null) {	
							if (duree1.dateFin() == null) {
								return NSComparator.OrderedSame;
							} else {
								return NSComparator.OrderedDescending;	 // fin null en queue
							}
						} else {	
							if (duree1.dateFin() == null) {
								return NSComparator.OrderedAscending;		// fin null en queue
							} else {
								if (DateCtrl.isBefore(duree0.dateFin(), duree1.dateFin())) {
									return NSComparator.OrderedAscending;
								} else if (DateCtrl.isAfter(duree0.dateFin(), duree1.dateFin())) {
									return NSComparator.OrderedDescending;
								} else {
									return NSComparator.OrderedSame;
								}
							}
						}
					}
				}
			}
		} else {	// Pas de comparaison
			throw new ComparisonException("Ce comparator ne peut que trier des objets de type InterfaceDuree");
		}
	}
}
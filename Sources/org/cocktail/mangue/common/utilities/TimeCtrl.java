package org.cocktail.mangue.common.utilities;


import com.webobjects.foundation.NSArray;

public class TimeCtrl 
{

//	 TimeCompletion
	public static String timeCompletion(String stringSaisie)
	{
		String 	chaine1 = "",chaine2 = "00";
		String	heureRetour = "";
		NSArray myArray = null;

		if (stringSaisie.length() > 5)
			return "";
				
	// On teste la plupart des types de saisie possibles ==> Mise dans un tableau de l'horaire (1 ou deux parties)
		if (stringSaisie.indexOf(',') != -1)
			myArray = NSArray.componentsSeparatedByString(stringSaisie,",");
		else
			if (stringSaisie.indexOf(';') != -1)
				myArray = NSArray.componentsSeparatedByString(stringSaisie,";");	
			else
				if (stringSaisie.indexOf('h') != -1)
					myArray = NSArray.componentsSeparatedByString(stringSaisie,"h");
				else
					if (stringSaisie.indexOf('H') != -1)
						myArray = NSArray.componentsSeparatedByString(stringSaisie,"H");
					else
						if (stringSaisie.indexOf('.') != -1)
						   	myArray = NSArray.componentsSeparatedByString(stringSaisie,".");
						else
							if (stringSaisie.indexOf(' ') != -1)
							   	myArray = NSArray.componentsSeparatedByString(stringSaisie," ");
							else
								myArray = NSArray.componentsSeparatedByString(stringSaisie,":");

	// On teste si on a deja quelque chose de correct a ce niveau dans myArray

		// On ne peut avoir que deux valeurs (Heures + Minutes)
		if (myArray.count() > 2)	{System.out.println("TimeCtrl.timeCompletion() COUNT MY ARRAY : " + myArray.count()); return "";}
		if (myArray.count() == 1)	{

			String myChaine = (String)myArray.objectAtIndex(0);

			// 3 chiffres saisis
			if (stringSaisie.length() == 3) {
				myChaine = myChaine.substring(0,1) + ":" + myChaine.substring(1);				
				myArray = NSArray.componentsSeparatedByString(myChaine,":");
				System.out.println("3 CHIFFRES : " + myChaine);
			}
			else
				if (stringSaisie.length() == 4) {
					myChaine = myChaine.substring(0,2) + ":" + myChaine.substring(2);				
					myArray = NSArray.componentsSeparatedByString(myChaine,":");
					System.out.println("4 CHIFFRES : " + myChaine);
				}
					
		}

		// Test sur la premiere partie
			// Recuperation des chiffres
			chaine1 = getChiffres(((String)myArray.objectAtIndex(0)));

			if ("".equals(chaine1)) {return "";}
			//if (((Number)new Integer(chaine1)).intValue() == 0)	{EODialogs.runInformationDialog("ERREUR", "Erreur de saisie d'heure ...");return "";}
			if (((Number)new Integer(chaine1)).intValue() > 24)	{return "";}

			// On met a jour la premiere partie de l'horaire
			if ( (((Number)new Integer(chaine1)).intValue() < 10) && (chaine1.length() == 1) )
				chaine1 = "0" + chaine1;

		// Test sur la deuxieme partie
		if (myArray.count() == 2)	{
			chaine2 = getChiffres(((String)myArray.objectAtIndex(1)));

			if ("".equals(chaine2)) chaine2 = "00";
			if (((Number)new Integer(chaine2)).intValue() > 60)	{System.out.println("TimeCtrl.timeCompletion() > 60mns");;return "";}
			if (chaine2.length() == 1)	chaine2 = "0" + chaine2;
		}

		// On met a jour la variable a retourner (heure:minutes)
		heureRetour = chaine1 + ":" + chaine2;
	
		System.out.println("TimeCtrl.timeCompletion() " + heureRetour);
		return heureRetour;
	}


	// GET CHIFFRES
	public static String getChiffres(String chaine)	{

		String tempo = "";

		for (int i=0;i< chaine.length();i++)	{
			if (NumberCtrl.estUnChiffre(String.valueOf(chaine.charAt(i))))
				tempo = tempo + chaine.charAt(i);
		}

		return tempo;
	}


	
	
// GET MINUTES : Retourne le nombre de minutes correspondant a la chaine string au format %H:%M (l'inverse de stringFor: )
	public static int getMinutes(String chaine)
	{
		NSArray			str =  NSArray.componentsSeparatedByString(chaine,":");
		int				nombre=0;

		if ( ("00:00".equals(chaine)) || ("".equals(chaine)) || (" ".equals(chaine)) || ("..:..".equals(chaine)) ) return 0;

		if (chaine.length() == 0) return 0;
	
		if (str.count() == 1)
			nombre = ((Number)str.objectAtIndex(0)).intValue() * 60;
		else
		{
			if ((((Number)new Integer((String)str.objectAtIndex(0))).intValue()) < 0)			
				nombre = ( -((((Number)new Integer((String)str.objectAtIndex(0))).intValue()) * 60) + ((((Number)new Integer((String)str.objectAtIndex(1))).intValue())) );
			else
				nombre = ( ((((Number)new Integer((String)str.objectAtIndex(0))).intValue()) * 60) + ((((Number)new Integer((String)str.objectAtIndex(1))).intValue())) );			
		}		

		if ((((Number)new Integer((String)str.objectAtIndex(0))).intValue()) < 0) nombre = -nombre;	// on a passe une valeur negative

		return nombre;
	}

// STRING FOR MINUTES
// Formatte le nombre de minutes en une chaine au format %H:%M (l'inverse de numberOfMinutesFor: )
	public static String stringForMinutes(int numberOfMinutes)
	{
		String	formattedString;
		int		hours, minutes;

		if (numberOfMinutes == 0) return "00:00";

		if (numberOfMinutes < 0) numberOfMinutes = -numberOfMinutes;

		hours = numberOfMinutes / 60;
		minutes = numberOfMinutes % 60;

		if (hours < 10)
			formattedString = "0" + hours;
		else	
			formattedString = "" + hours;

		if (minutes < 10)
			formattedString  = formattedString + ":0" + minutes;
		else 
			formattedString  = formattedString + ":" + minutes;

		return formattedString;
	}

}

package org.cocktail.mangue.common.utilities;


public class MangueMessagesErreur {

    public static final String ERREUR_NUMERO_ARRETE = "%s\nUn numéro d'arrêté comporte au plus %s caractères !";

    /* GENERAL */
    public static final String ERREUR_CATEGORIE_EMPLOI_NON_RENSEIGNE = "Veuillez renseigner une catégorie d'emploi";
    public static final String ERREUR_BUDGET_NON_RENSEIGNE = "Veuillez renseigner un type de budget";
    public static final String ERREUR_QUOTITE_NON_RENSEIGNE = "Veuillez renseigner la quotité";
    public static final String ERREUR_AFFECTATION_QUOTITE_SUPERIEUR_CENT = "La quotité totale des affectations ne peut pas dépasser 100% pour cette période";
    public static final String ERREUR_AFFECTATION_MEME_STRUCTURE = "L'agent ne peut pas être affecté à la même structure pendant la même période";
    public static final String ERREUR_QUOTITE_SUPERIEUR_CENT = "La quotité ne peut pas dépasser 100%";
    public static final String ERREUR_PROGRAMME_NON_RENSEIGNE = "Veuillez renseigner un programme";
    public static final String ERREUR_TITRE_NON_RENSEIGNE = "Veuillez renseigner un titre";
    public static final String ERREUR_CHAPITRE_NON_RENSEIGNE = "Veuillez renseigner un chapitre";
    public static final String ERREUR_ARTICLE_NON_RENSEIGNE = "Veuillez renseigner un article";
    public static final String ERREUR_LIBELLE_COURT_LONGUEUR = "Le libellé court comporte au plus %s caractères";
    public static final String ERREUR_LIBELLE_LONG_LONGUEUR = "Le libellé long comporte au plus %s caractères";
    public static final String ERREUR_CODE_LONGUEUR = "Le code comporte au plus %s caractères";
    public static final String ATTENTION_CATEGORIE_EMPLOI_SANS_CORPS = "Aucun corps n'est associé à la catégorie d'emploi";
    
    /* EMPLOI */
    public static final String ERREUR_EMPLOI_NO_NON_RENSEIGNE = "Veuillez renseigner un numéro d'emploi";
    public static final String ERREUR_EMPLOI_EXISTE_DEJA = "Ce numéro d'emploi existe déjà";
    public static final String ERREUR_EMPLOI_CATEGORIE_CHEVAUCHEMENT = "Cet emploi a déjà des catégories d'emploi pendant cette période";
    public static final String ERREUR_EMPLOI_SPEC_CHEVAUCHEMENT = "Cet emploi a déjà des spécialisations pendant cette période";
    public static final String ERREUR_EMPLOI_BUDGET_CHEVAUCHEMENT = "Cet emploi a déjà des budgets pendant cette période";
    public static final String ERREUR_EMPLOI_DATE_PUBLICATION_AVANT_EFFET = "La date de publication doit être postérieure à la date d'effet";
    public static final String ERREUR_EMPLOI_DATE_FERMETURE_AVANT_DATE_EMPLOI = "La date de fermeture doit être postérieure au %s";    
    public static final String ERREUR_EMPLOI_DATE_DEBUT_AVANT_DEBUT_EMPLOI = "La date de début doit être postérieure au %s";
    public static final String ERREUR_EMPLOI_DATE_FIN_APRES_FERMETURE = "La date de fin doit être antérieure à la date de fermeture de l'emploi";
    public static final String ERREUR_EMPLOI_ELEMENTS_DATE_FIN_NON_RENSEIGNE = "L'emploi est fermé. Il faut renseigner la date de fin";
    public static final String ERREUR_EMPLOI_TEMPORAIRE_DATE_FERMETURE = "La date de fermeture est obligatoire pour un emploi temporaire. Veuillez la renseigner";
    public static final String ERREUR_EMPLOI_OCCUPATION_DATE_FERMETURE_KO = "Il existe des occupations courantes pour cet emploi, vous ne pouvez pas le fermer";
    public static final String ERREUR_EMPLOI_OCCUPATION_SUPPRESSION_KO = "Il existe des occupations courantes pour cet emploi, vous ne pouvez pas le supprimer";
    public static final String ERREUR_OCCUPATION_EMPLOI_POUR_CONTRACTUEL = "Cet emploi est réservé pour un agent contractuel";
    public static final String ERREUR_EMPLOI_DATE_DEBUT_HISTO_KO = "La date de début doit être postérieure au %s";
    public static final String ERREUR_EMPLOI_DATE_FIN_HISTO_KO = "La date de fin doit être antérieure au %s";
    public static final String ERREUR_SPECIALISATION_EMPLOI_NON_RENSEIGNE = "Veuillez renseigner une spécialisation";
    public static final String ERREUR_EMPLOI_SELECTION_KO = "Veuillez sélectionner un emploi";
    
    public static final String ATTENTION_EMPLOI_ENSEIGNANT_MODIF_SPECIALISATION = "Pensez à modifier la spécialisaion de cet emploi";
    
    /* OCCUPATION */
    public static final String ERREUR_OCCUPATION_QUOTITE_DISPONIBLE = "Il ne reste que %s % de quotité d'occupation disponible sur cet emploi.";
    
    /* CONTRAT */
    public static final String ERREUR_CONTRAT_NON_AUTORISE_POUR_TITULAIRES = "L'agent a une carrière ou un passé titulaire sur cette période. On ne peut lui associer qu'un type de contrat "
    		+ " autorisé pour les titulaires";
    public static final String ERREUR_CONTRAT_DATE_DEBUT_AVANT_DEBUT_TYPE_CONTRAT = "La date de début du contrat doit être postérieure à la date de début du type de contrat";
    public static final String ATTENTION_CONTRAT_GRADE_A_RENSEIGNER = "Le type de contrat rend le grade obligatoire. Veuillez mettre à jour le détail du contrat";
    
    public static final String ATTENTION_TYPE_CONTRAT_ENFANT_NON_VISIBLE = "Voulez-vous également rendre tous les contrats ayant comme père le code %s invisibles ?";
    public static final String ATTENTION_TYPE_CONTRAT_ENFANT_VISIBLE = "Voulez-vous également rendre tous les contrats ayant comme père le code %s visibles ?";

	    
}
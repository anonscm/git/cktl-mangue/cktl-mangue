/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.utilities;

import org.cocktail.mangue.common.utilities.Utilitaires.IntervalleTemps;

import com.webobjects.foundation.NSComparator;

public class IntervalleTempsComparator extends NSComparator {
	public IntervalleTempsComparator() {
		super();
	}
	public int compare(Object arg0, Object arg1) throws ComparisonException {
		if (arg0 instanceof IntervalleTemps && arg1 instanceof IntervalleTemps) {
			IntervalleTemps intervalle0 = (IntervalleTemps)arg0, intervalle1 = (IntervalleTemps)arg1;
			if (intervalle0.debutPeriode() == null) {	
				if (intervalle1.debutPeriode() == null) {
					return NSComparator.OrderedSame;
				} else {
					return NSComparator.OrderedAscending;	 // Intervalle avec debut null en tête
				}
			} else {	// structure père non nulle
				if (intervalle1.debutPeriode() == null) {
					return NSComparator.OrderedDescending;		// Intervalle avec debut null en tête
				} else {
					if (DateCtrl.isBefore(intervalle0.debutPeriode(), intervalle1.debutPeriode())) {
						return NSComparator.OrderedAscending;
					} else if (DateCtrl.isAfter(intervalle0.debutPeriode(), intervalle1.debutPeriode())) {
						return NSComparator.OrderedDescending;
					} else {
						// Mêmes dates
						return NSComparator.OrderedSame;
					}
				}

			}
		} else {	// Pas de comparaison
			return NSComparator.OrderedSame;
		}
	}
}
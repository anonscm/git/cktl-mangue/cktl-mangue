package org.cocktail.mangue.common.modele.interfaces;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;


public interface INomenclature {

	public static String CODE_KEY = "code";
	public static String LIBELLE_KEY 	= "libelle";
	public static String LIBELLE_COURT_KEY 	= "libelleCourt";
	public static String LIBELLE_LONG_KEY 	= "libelleLong";
	public static String DATE_OUVERTURE_KEY = "dateOuverture";
	public static String DATE_FERMETURE_KEY = "dateFermeture";

	public static String CODE_LIBELLE_KEY = "codeEtLibelle";
	
	public static EOSortOrdering SORT_CODE = new EOSortOrdering(CODE_KEY, EOSortOrdering.CompareAscending);
	public static NSArray<EOSortOrdering> SORT_ARRAY_CODE = new NSArray<EOSortOrdering>(SORT_CODE);

	public static EOSortOrdering SORT_LIBELLE = new EOSortOrdering(LIBELLE_LONG_KEY, EOSortOrdering.CompareAscending);
	public static NSArray<EOSortOrdering> SORT_ARRAY_LIBELLE = new NSArray<EOSortOrdering>(SORT_LIBELLE);

	public static EOSortOrdering SORT_LIBELLE_COURT = new EOSortOrdering(LIBELLE_LONG_KEY, EOSortOrdering.CompareAscending);
	public static NSArray<EOSortOrdering> SORT_ARRAY_LIBELLE_COURT = new NSArray<EOSortOrdering>(SORT_LIBELLE_COURT);
	
	//public abstract String entityName();

	String code();
	void setCode(String value);
	String libelleCourt();
	void setLibelleCourt(String value);
	String libelleLong();
	void setLibelleLong(String value);

	String libelle();
	String codeEtLibelle();
}
// _EOTypeVisiteMedicale.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeVisiteMedicale.java instead.
package org.cocktail.mangue.common.modele.nomenclatures.medical;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;

import com.webobjects.eocontrol.EOGenericRecord;

public abstract class _EOTypeVisiteMedicale extends  Nomenclature {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "TypeVisiteMedicale";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TYPE_VISITE_MEDICALE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tvmeOrdre";

	public static final String TVME_ANNEE_KEY = "tvmeAnnee";
	public static final String TVME_JOUR_KEY = "tvmeJour";
	public static final String TVME_MOIS_KEY = "tvmeMois";

// Attributs non visibles
	public static final String TVME_ORDRE_KEY = "tvmeOrdre";

//Colonnes dans la base de donnees
	public static final String TVME_ANNEE_COLKEY = "TVME_ANNEE";
	public static final String TVME_JOUR_COLKEY = "TVME_JOUR";
	public static final String TVME_MOIS_COLKEY = "TVME_MOIS";

	public static final String TVME_ORDRE_COLKEY = "TVME_ORDRE";


	// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer tvmeAnnee() {
    return (Integer) storedValueForKey(TVME_ANNEE_KEY);
  }

  public void setTvmeAnnee(Integer value) {
    takeStoredValueForKey(value, TVME_ANNEE_KEY);
  }

  public Integer tvmeJour() {
    return (Integer) storedValueForKey(TVME_JOUR_KEY);
  }

  public void setTvmeJour(Integer value) {
    takeStoredValueForKey(value, TVME_JOUR_KEY);
  }

  public Integer tvmeMois() {
    return (Integer) storedValueForKey(TVME_MOIS_KEY);
  }

  public void setTvmeMois(Integer value) {
    takeStoredValueForKey(value, TVME_MOIS_KEY);
  }


/**
 * Créer une instance de EOTypeVisiteMedicale avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeVisiteMedicale createEOTypeVisiteMedicale(EOEditingContext editingContext			) {
    EOTypeVisiteMedicale eo = (EOTypeVisiteMedicale) createAndInsertInstance(editingContext, _EOTypeVisiteMedicale.ENTITY_NAME);    
    return eo;
  }

  
	  public EOTypeVisiteMedicale localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeVisiteMedicale)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeVisiteMedicale creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeVisiteMedicale creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypeVisiteMedicale object = (EOTypeVisiteMedicale)createAndInsertInstance(editingContext, _EOTypeVisiteMedicale.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeVisiteMedicale localInstanceIn(EOEditingContext editingContext, EOTypeVisiteMedicale eo) {
    EOTypeVisiteMedicale localInstance = (eo == null) ? null : (EOTypeVisiteMedicale)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeVisiteMedicale#localInstanceIn a la place.
   */
	public static EOTypeVisiteMedicale localInstanceOf(EOEditingContext editingContext, EOTypeVisiteMedicale eo) {
		return EOTypeVisiteMedicale.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeVisiteMedicale fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeVisiteMedicale fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeVisiteMedicale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeVisiteMedicale)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeVisiteMedicale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeVisiteMedicale fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeVisiteMedicale eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeVisiteMedicale)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeVisiteMedicale fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeVisiteMedicale eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeVisiteMedicale ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeVisiteMedicale fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

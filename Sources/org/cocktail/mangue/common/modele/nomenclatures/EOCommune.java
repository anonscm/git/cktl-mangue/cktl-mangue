// Commune.java
// Created on Wed Jul 06 14:42:08 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures;

import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelle;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOCommune extends _EOCommune {
	
    public EOCommune() {
        super();
    }

    public String libelle() {
    	return libelleLong();
    }

    public String toString() {
    		return libelle();
    }
    public static NSArray<EOCommune> rechercherCommunes(EOEditingContext ec, String cp) {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(C_POSTAL_KEY + "=%@",new NSArray(cp));        
        return fetchAll(ec, qual, SORT_ARRAY_LIBELLE);
    }

    /** Retourne la commune ayant le meme libelle long ou sinon la premiere commune trouvee,null si pas de commune trouvee */
    public static EOCommune findForCodePostalAndLibelle(EOEditingContext ec, String codePostal, String libelle) {

    	NSMutableArray qualifiers = new NSMutableArray();
    	NSMutableArray orQualifiers = new NSMutableArray();
    	
    	if (codePostal != null)
    		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(C_POSTAL_KEY + "=%@", new NSArray(codePostal)));

    	if (libelle != null) {
    		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LIBELLE_LONG_KEY + "=%@", new NSArray(libelle)));
    		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LIBELLE_COURT_KEY + "=%@", new NSArray(libelle)));
    		qualifiers.addObject(new EOOrQualifier(orQualifiers));
    	}

    	if (qualifiers.count() == 0)
    		return null;
    	
    	return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
	} 
}
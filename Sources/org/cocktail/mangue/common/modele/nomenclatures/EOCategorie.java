// EOCategorie.java
// Created on Fri Aug 19 09:42:33 Europe/Paris 2005 by Apple EOModeler Version 5.2

/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures;

import org.cocktail.common.utilities.RecordAvecLibelle;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class EOCategorie extends _EOCategorie implements RecordAvecLibelle {
	
	public static String CODE_SANS_CORRESPONDANCE = "Z";
	
    public EOCategorie() {
        super();
    }
    
    public static EOCategorie rechercherCategoriePourCode(EOEditingContext ec, String code) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@", new NSArray(code));
    	return fetchFirstByQualifier(ec, qualifier);
    }
    

    public String toString() {
    	return libelleLong();
    }
        
    /**
     * Liste des catégories de la fonction publique sans la catégorie "SANS CORRESPONDANCE"
     * @param edc : editingContext
     * @return la liste des catégories de la fonction publique 
     */
	@SuppressWarnings("unchecked")
    public static NSArray<EOCategorie> rechercherListeCategorieAvecCorrespondance(EOEditingContext edc) {
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "!=%@", new NSArray<String>(CODE_SANS_CORRESPONDANCE));
			return fetchAll(edc, qualifier, SORT_ARRAY_CODE);
			
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOCategorie>();
		}
	}
}

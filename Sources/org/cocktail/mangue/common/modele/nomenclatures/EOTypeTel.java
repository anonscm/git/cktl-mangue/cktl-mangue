// EOTypeTel.java
// Created on Fri Mar 19 10:13:49 Europe/Paris 2010 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

package org.cocktail.mangue.common.modele.nomenclatures;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOTypeTel extends _EOTypeTel {

	public static final String TYPE_NO_PRO = "PRF";
	public static final String TYPE_DEFAULT = TYPE_NO_PRO;

	public static final String TYPE_NO_PRIVE = "PRV";
	public static final String TYPE_INTERNE = "INT";
	public static final String TYPE_NO_ETUD = "ETUD";
	public static final String TYPE_NO_PAR = "PAR";

    public EOTypeTel() {
        super();
    }
    
    public String toString() {
    	return libelleLong();
    }    
    
    /** Retourne le type tel pour le type fourni (cTypeTel) ou null si il n'existe pas */
    public static NSArray<EOTypeTel> rechercher(EOEditingContext ec) {
    	NSMutableArray qualifiers = new NSMutableArray();
    	
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + " != %@", new NSArray(TYPE_NO_ETUD)));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + " != %@", new NSArray(TYPE_NO_PAR)));
    	
    	return fetchAll(ec, new EOAndQualifier(qualifiers));    	
    }
    public static EOTypeTel getDefault(EOEditingContext ec) {
    	NSMutableArray qualifiers = new NSMutableArray();
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY+" = %@", new NSArray(TYPE_DEFAULT)));
    	return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
    }

    
    /** Retourne le type tel pour le type fourni (cTypeTel) ou null si il n'existe pas */
    public static EOTypeTel rechercherTypeTelPourType(EOEditingContext editingContext,String type) {
    	EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "= %@", new NSArray(type));
    	return fetchFirstByQualifier(editingContext, myQualifier);    	
    }
    /** Retourne le type tel pour le libelle fourni ou null si il n'existe pas */
    public static EOTypeTel rechercherTypeTelPourLibelle(EOEditingContext editingContext,String libelle) {
    	EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "= %@", new NSArray(libelle));
    	return fetchFirstByQualifier(editingContext, myQualifier);    	
    }
}

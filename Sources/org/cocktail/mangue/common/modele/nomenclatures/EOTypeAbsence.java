//EOTypeAbsence.java
//Created on Wed Feb 26 13:50:26  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures;


import org.cocktail.common.LogManager;
import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOChangementPosition;
import org.cocktail.mangue.modele.mangue.individu.EOPasse;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
/** Modelise un type d'evenement : conge, modalite de service, changement de position…
 *
 */

public class EOTypeAbsence extends _EOTypeAbsence {

	private final static String[] TYPES_CONGE_COMMUNS =  {"CMATER","CPATER","ENFANT", "ADOPTION", "CSF", "CGRFP"};
	private final static String[] TYPES_CONGE_FONCTIONNAIRES =  {"MAL","CLM","CLD","MALSSTRT","ACCSERV","CFP","CRCT"};
	private final static String[] TYPES_CONGE_NORMALIENS =  {"CCP","CIR","RDT"};
	private final static String[] TYPES_CONGE_CONTRACTUELS_HORS_HU =  {"MALNT","ACCTRAV","CGM"};
	private final static String[] TYPES_CONGE_CONTRACTUELS =  {"MALNT","ACCTRAV","CGM","CGRFP","AHCUAO3","AHCUAO4","AHCUAO5","AHCUAO6"};

	public final static String TYPE_CONGE_MALADIE = "MAL";
	public final static String TYPE_CONGE_MATERNITE = "CMATER";
	public final static String TYPE_CONGE_PATERNITE = "CPATER";
	public final static String TYPE_CONGE_GARDE_ENFANT = "ENFANT";
	public final static String TYPE_CONGE_RFP = "CGRFP";
	public final static String TYPE_CONGE_ADOPTION = "ADOPTION";
	public final static String TYPE_CONGE_MAL_SS_TRAITEMENT = "MALSSTRT";
	public final static String TYPE_CONGE_SOLIDARITE_FAMILIALE = "CSF";

	public final static String TYPE_CONGE_MALADIE_NT = "MALNT";
	public final static String TYPE_CONGE_MALADIE_CGM = "CGM";

	public final static String TYPE_CONGE_ACC_SERV = "ACCSERV";
	public final static String TYPE_CONGE_ACC_TRAV = "ACCTRAV";

	public final static String TYPE_CONGE_HU_3 = "AHCUAO3";
	public final static String TYPE_CONGE_HU_4 = "AHCUAO4";
	public final static String TYPE_CONGE_HU_5 = "AHCUAO5";
	public final static String TYPE_CONGE_HU_6 = "AHCUAO6";

	public final static String TYPE_CONGE_CCP = "CCP";
	public final static String TYPE_CONGE_CIR = "CIR";
	public final static String TYPE_CONGE_RDT = "RDT";

	public final static String TYPE_CLD = "CLD";
	public final static String TYPE_CLM = "CLM";
	public final static String TYPE_CPA = "CPA";
	public final static String TYPE_CFP = "CFP";	// 24/11/2010
	private static String CONGE_FORMATION = "CFP";
	private static String HOSPITALO_UNIV = "AHCUAO";


	public final static String TYPE_DETACHEMENT = "DETA";
	public final static String TYPE_DISPONIBILITE = "DISP";
	public final static String TYPE_SERVICE_NATIONAL = "SNAT";
	public final static String TYPE_CONGE_PARENTAL = "CGPA";

	public final static String TYPE_TEMPS_PARTIEL = "TPAR";
	public final static String TYPE_TEMPS_PARTIEL_THERAP = "MTT";
	public static String TYPE_CRCT = "CRCT";
	public final static String TYPE_CST = "CGST";
	public final static String TYPE_DELEGATION = "DELEGATION";
	public final static String TYPE_CFA = "CFA";

	public final static String TYPE_DEPART = "DEPA";

	public final static String TYPE_PROLONGATION = "PROLONG";
	public final static String TYPE_RECUL_AGE = "RECUL";

	public EOTypeAbsence() {
		super();
	}

	public String toString() {
		return libelleLong();
	}

	public static EOTypeAbsence findForCode(EOEditingContext edc, String code) {
		return (EOTypeAbsence)NomenclatureFinder.findForCode(edc, ENTITY_NAME, code);
	}

	public static NSDictionary entitesConge() {

		NSMutableDictionary dicoEntites = new NSMutableDictionary();

		dicoEntites.setObjectForKey(TYPE_CONGE_MALADIE, "CongeMaladie");
		dicoEntites.setObjectForKey(TYPE_CONGE_MALADIE_NT, "CgntMaladie");

		return dicoEntites;

	}

	public boolean estTempsPartiel() {
		return code().equals(TYPE_TEMPS_PARTIEL);
	}
	public boolean estTempsPartielTherapeutique() {
		return code().equals(TYPE_TEMPS_PARTIEL_THERAP);
	}

	public boolean estCongeLegal() {
		return congeLegal() != null && congeLegal().equals(CocktailConstantes.VRAI);
	}
	public boolean estCongeCir() {
		return temCir() != null && temCir().equals(CocktailConstantes.VRAI);
	}
	public boolean requiertEnfant() {
		return temEnfant() != null && temEnfant().equals(CocktailConstantes.VRAI);
	}

	public boolean estHeuresComp() {
		return temHcomp() != null && temHcomp().equals(CocktailConstantes.VRAI);
	}
	public void setEstHeuresComp(boolean value) {

		if (value)
			setTemHcomp(CocktailConstantes.VRAI);
		else
			setTemHcomp(CocktailConstantes.FAUX);		

	}

	public boolean estCongeGardeEnfant() {
		return code().equals(TYPE_CONGE_GARDE_ENFANT);
	}

	public boolean estCongeMaternite() {
		return code().equals(TYPE_CONGE_MATERNITE);
	}

	public boolean estCongeRfp() {
		return code().equals(TYPE_CONGE_RFP);
	}
	public boolean estCongeGraveMaladie() {
		return code().equals(TYPE_CONGE_MALADIE_CGM);
	}
	
	public boolean estCongeMaladieContractuel() {

		return code().equals(TYPE_CONGE_MALADIE_NT);

	}

	public boolean estCongeMaladie() {
		return code().equals(TYPE_CONGE_MALADIE) || code().equals(TYPE_CONGE_MALADIE_NT);
	}
	public boolean estCongeLongueDuree() {
		return code().equals(TYPE_CLD);
	}
	public boolean estCongeLongueMaladie() {
		return code().equals(TYPE_CLM);
	}
	public boolean estCongeAccidentTravail() {
		return code().equals(TYPE_CONGE_ACC_TRAV);
	}
	public boolean estCongeAccidentService() {
		return code().equals(TYPE_CONGE_ACC_SERV);
	}


	public boolean estDepart() {
		return code().equals(TYPE_DEPART);
	}
	public boolean estDetachement() {
		return code().equals(TYPE_DETACHEMENT);
	}
	public boolean estDisponibilite() {
		return code().equals(TYPE_DISPONIBILITE);
	}
	public boolean estCongeParental() {
		return code().equals(TYPE_CONGE_PARENTAL);
	}
	public boolean estServiceNational() {
		return code().equals(TYPE_SERVICE_NATIONAL);
	}

	public boolean estCongePourFonctionnaire() {
		for (int i = 0; i < EOTypeAbsence.TYPES_CONGE_FONCTIONNAIRES.length;i++) {
			if (TYPES_CONGE_FONCTIONNAIRES[i].equals(code())) {
				return true;
			}
		}
		return false;
	}
	public boolean estCongePourContractuel() {
		for (int i = 0; i < EOTypeAbsence.TYPES_CONGE_CONTRACTUELS.length;i++) {
			if (TYPES_CONGE_CONTRACTUELS[i].equals(code())) {
				return true;
			}
		}
		return false;
	}
	public boolean estCongeCommun() {
		for (int i = 0; i < EOTypeAbsence.TYPES_CONGE_COMMUNS.length;i++) {
			if (TYPES_CONGE_COMMUNS[i].equals(code())) {
				return true;
			}
		}
		return false;
	}
	public boolean estCongeNormalien() {
		for (int i = 0; i < EOTypeAbsence.TYPES_CONGE_NORMALIENS.length;i++) {
			if (TYPES_CONGE_NORMALIENS[i].equals(code())) {
				return true;
			}
		}
		return false;
	}

	public boolean estCongePourLongueMaladie() {
		return code() != null && (code().equals(TYPE_CLD) || code().equals(TYPE_CLM));
	}



	/** retourne le type absence pour le nom de table et le type passes en parametre
	 * typeAbsence peut etre nulle si il y a une seule entree dans la table pour le nom de table  */
	public static EOTypeAbsence rechercherTypeAbsencePourTable(EOEditingContext editingContext,String nomTable,String typeAbsence) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_HARPEGE_KEY + "=%@", new NSArray(nomTable)));
			if (typeAbsence != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@", new NSArray(typeAbsence)));

			return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers));
		}
		catch (Exception e) {
			return null;
		}
	}
	public static NSArray rechercherTypesAbsencesFontionnaire(EOEditingContext editingContext) {
		NSMutableArray types = new NSMutableArray(TYPES_CONGE_COMMUNS);
		types.addObjectsFromArray(new NSArray(TYPES_CONGE_FONCTIONNAIRES));
		return rechercherTypesAbsence(editingContext,types);
	}
	public static NSArray rechercherTypesAbsencesNormalien(EOEditingContext editingContext) {
		NSMutableArray types = new NSMutableArray(TYPES_CONGE_NORMALIENS);
		return rechercherTypesAbsence(editingContext,types);
	}
	public static NSArray rechercherTypesAbsencesContractuels(EOEditingContext editingContext) {
		NSMutableArray types = new NSMutableArray(TYPES_CONGE_COMMUNS);
		if (EOGrhumParametres.isGestionHu()) {
			types.addObjectsFromArray(new NSArray(TYPES_CONGE_CONTRACTUELS));
		} else {
			types.addObjectsFromArray(new NSArray(TYPES_CONGE_CONTRACTUELS_HORS_HU));
		}
		return rechercherTypesAbsence(editingContext,types);
	}
	public static NSArray rechercherTypesAbsencesGeres(EOEditingContext editingContext) {
		NSMutableArray types = new NSMutableArray(TYPES_CONGE_COMMUNS);
		types.addObjectsFromArray(new NSArray(TYPES_CONGE_FONCTIONNAIRES));

		if (EOGrhumParametres.isGestionEns()) {
			types.addObjectsFromArray(new NSArray(TYPES_CONGE_NORMALIENS));
		}
		// 16/02/2011
		if (EOGrhumParametres.isGestionHu()) {
			types.addObjectsFromArray(new NSArray(TYPES_CONGE_CONTRACTUELS));
		} else {
			types.addObjectsFromArray(new NSArray(TYPES_CONGE_CONTRACTUELS_HORS_HU));
		}
		return rechercherTypesAbsence(editingContext,types);
	}

	/** retourne tous les types geres en dehors des types lies aux conges legaux */
	public static NSArray rechercherTypesEvenementsHorsConge(EOEditingContext editingContext) {

		NSMutableArray qualifiers = new NSMutableArray();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONGE_LEGAL_KEY + "=%@", new NSArray(CocktailConstantes.FAUX)));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_HARPEGE_KEY + " != nil", null));

		return fetchAll(editingContext, new EOAndQualifier(qualifiers));		
	}

	/** Recherche les types de conges legaux pour un titulaire/un non-titulaire a une periode donnee
	 * @param editingContext
	 * @param individu
	 * @param debut debut periode
	 * @param fin fin periode
	 * @return types trouves
	 */
	public static NSArray rechercherTypesCongesLegaux(EOEditingContext editingContext,EOIndividu individu,NSTimestamp debut,NSTimestamp fin) {

		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(CONGE_LEGAL_KEY + " = 'O'",null);
		EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME,qualifier,null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		NSMutableArray types = new NSMutableArray();
		boolean beneficieCongePourFonctionnaire = false,beneficieCongePourContractuel = false,beneficieCongePourHospitalo = false;
		boolean beneficieCongePourRecherche = false, beneficieCongeNormalien = false, afficherCongeNormalien = false;

		if (EOGrhumParametres.isGestionEns()) {
			afficherCongeNormalien = true;
		}
		if (debut == null) {
			//	 Rechercher si l'individu est fonctionnaire 
			beneficieCongePourFonctionnaire = EOChangementPosition.fonctionnaireEnActivite(editingContext,individu);
			if (beneficieCongePourFonctionnaire) {
				// vérifier dans ce cas si il peut bénéficier des congés recherche
				beneficieCongePourRecherche = individu.peutBeneficierCrctPourPeriode(null,null);
				if (afficherCongeNormalien) {
					beneficieCongeNormalien = individu.estNormalienSurPeriode(null, null);
				}
			}
			// 24/02/2011 - modification pour limiter les congés HU à certains types de contrat travail
			// Rechercher si il est contractuel sur toute la période
			beneficieCongePourContractuel = individu.beneficieCongeContractuelSurPeriode(null,null);
			if (beneficieCongePourContractuel) {
				beneficieCongePourHospitalo = individu.beneficieCongeHospitalierSurPeriode(null,null);
			}
			if (beneficieCongePourFonctionnaire && beneficieCongePourContractuel) {
				// On ne peut pas déterminer les types de congés sur toutes les périodes
				// si l'individu est à la fois contractuel et fonctionnaire
				return new NSArray();
			}
		} else {
			beneficieCongePourFonctionnaire = EOChangementPosition.fonctionnaireEnActivitePendantPeriode(editingContext,individu,debut,fin);
			if (!beneficieCongePourFonctionnaire) {
				beneficieCongePourFonctionnaire = EOPasse.rechercherPassesEASPourPeriode(editingContext,individu,debut,fin).size() > 0;
				if (!beneficieCongePourFonctionnaire) {
					beneficieCongePourContractuel = individu.beneficieCongeContractuelSurPeriode(debut,fin);
					if (beneficieCongePourContractuel) {
						beneficieCongePourHospitalo = individu.beneficieCongeHospitalierSurPeriode(debut,fin);
					}
				}
			} else {
				beneficieCongePourRecherche = individu.peutBeneficierCrctPourPeriode(debut,fin);
				if (afficherCongeNormalien) {
					beneficieCongeNormalien = individu.estNormalienSurPeriode(debut, fin);
				}
			}
		}
		if (!beneficieCongePourFonctionnaire && !beneficieCongePourContractuel) {
			// 15/10/07 - Vérifier si par hasard, il ne s'agit pas d'un type de population contractuel
			if (EOChangementPosition.assimileContractuelEnActivitePendantPeriode(editingContext,individu,debut,fin)) {
				beneficieCongePourContractuel = true;
				beneficieCongePourHospitalo = individu.beneficieCongeHospitalierSurPeriode(debut,fin);
			} else
				return new NSArray();
		}
		boolean beneficieCongeFormation = beneficieCongePourFonctionnaire && individu.peutBeneficierCongeFormation(debut, fin);

		// Prendre les types de congés correspondant aux critères : pour fonctionnaire,pour contractuel, hospitalo-univ, CRCT
		// en fonction des caractéristiques de l'individu
		for (java.util.Enumeration<EOTypeAbsence> e = results.objectEnumerator();e.hasMoreElements();) {
			EOTypeAbsence type = e.nextElement();

			if (type.estCongePourFonctionnaire()) {
				if (beneficieCongePourFonctionnaire) {
					if (type.code().equals(TYPE_CRCT)) {
						if (beneficieCongePourRecherche) {
							types.addObject(type);
						}
					} else if (type.code().equals(CONGE_FORMATION)) {
						if (beneficieCongeFormation) {
							types.addObject(type);
						}
					} else {
						types.addObject(type);
					}
				}
			} else if (type.estCongePourContractuel()) {
				if (beneficieCongePourContractuel) {
					if (type.code().startsWith(HOSPITALO_UNIV)) {
						if (beneficieCongePourHospitalo) {
							types.addObject(type);
						}
					} else {
						types.addObject(type);
					}
				}
			} else if (type.estCongeCommun()) {
				types.addObject(type);
			} else if (type.estCongeNormalien()) {
				if (afficherCongeNormalien && beneficieCongeNormalien) {
					types.addObject(type);
				} 
			} 
		}

		return (NSArray)types;
	}

	/**
	 * 
	 * @param editingContext
	 * @param types
	 * @return
	 */
	private static NSArray rechercherTypesAbsence(EOEditingContext editingContext,NSArray types) {

		try {
			NSMutableArray qualifiers = new NSMutableArray();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CONGE_LEGAL_KEY+"=%@", new NSArray(CocktailConstantes.VRAI)));

			qualifiers.addObject(SuperFinder.construireORQualifier(CODE_KEY,types));

			return fetchAll(editingContext, new EOAndQualifier(qualifiers));
		} catch (Exception e) {
			return null;
		}
	}
}


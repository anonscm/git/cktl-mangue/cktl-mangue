/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCategorieEmploi.java instead.
package org.cocktail.mangue.common.modele.nomenclatures.emploi;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCategorieEmploi extends  EOGenericRecord {
	public static final String ENTITY_NAME = "CategorieEmploi";
	public static final String ENTITY_TABLE_NAME = "GRHUM.CATEGORIE_EMPLOI";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cCategorieEmploi";

	public static final String C_CATEGORIE_EMPLOI_KEY = "cCategorieEmploi";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_CATEGORIE_EMPLOI_KEY = "lcCategorieEmploi";
	public static final String LL_CATEGORIE_EMPLOI_KEY = "llCategorieEmploi";
	public static final String TEM_BUDGET_PROPRE_KEY = "temBudgetPropre";
	public static final String TEM_PAST_KEY = "temPast";
	public static final String TEM_SURNOMBRE_KEY = "temSurnombre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_CATEGORIE_EMPLOI_COLKEY = "C_CATEGORIE_EMPLOI";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LC_CATEGORIE_EMPLOI_COLKEY = "LC_CATEGORIE_EMPLOI";
	public static final String LL_CATEGORIE_EMPLOI_COLKEY = "LL_CATEGORIE_EMPLOI";
	public static final String TEM_BUDGET_PROPRE_COLKEY = "TEM_BUDGET_PROPRE";
	public static final String TEM_PAST_COLKEY = "TEM_PAST";
	public static final String TEM_SURNOMBRE_COLKEY = "TEM_SURNOMBRE";



	// Relationships
	public static final String TO_LISTE_CORPS_CATEG_EMPLOI_KEY = "toListeCorpsCategEmploi";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cCategorieEmploi() {
    return (String) storedValueForKey(C_CATEGORIE_EMPLOI_KEY);
  }

  public void setCCategorieEmploi(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_EMPLOI_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String lcCategorieEmploi() {
    return (String) storedValueForKey(LC_CATEGORIE_EMPLOI_KEY);
  }

  public void setLcCategorieEmploi(String value) {
    takeStoredValueForKey(value, LC_CATEGORIE_EMPLOI_KEY);
  }

  public String llCategorieEmploi() {
    return (String) storedValueForKey(LL_CATEGORIE_EMPLOI_KEY);
  }

  public void setLlCategorieEmploi(String value) {
    takeStoredValueForKey(value, LL_CATEGORIE_EMPLOI_KEY);
  }

  public String temBudgetPropre() {
    return (String) storedValueForKey(TEM_BUDGET_PROPRE_KEY);
  }

  public void setTemBudgetPropre(String value) {
    takeStoredValueForKey(value, TEM_BUDGET_PROPRE_KEY);
  }

  public String temPast() {
    return (String) storedValueForKey(TEM_PAST_KEY);
  }

  public void setTemPast(String value) {
    takeStoredValueForKey(value, TEM_PAST_KEY);
  }

  public String temSurnombre() {
    return (String) storedValueForKey(TEM_SURNOMBRE_KEY);
  }

  public void setTemSurnombre(String value) {
    takeStoredValueForKey(value, TEM_SURNOMBRE_KEY);
  }

  public NSArray toListeCorpsCategEmploi() {
    return (NSArray)storedValueForKey(TO_LISTE_CORPS_CATEG_EMPLOI_KEY);
  }

  public NSArray toListeCorpsCategEmploi(EOQualifier qualifier) {
    return toListeCorpsCategEmploi(qualifier, null, false);
  }

  public NSArray toListeCorpsCategEmploi(EOQualifier qualifier, boolean fetch) {
    return toListeCorpsCategEmploi(qualifier, null, fetch);
  }

  public NSArray toListeCorpsCategEmploi(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi.TO_CATEGORIE_EMPLOI_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeCorpsCategEmploi();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeCorpsCategEmploiRelationship(org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_LISTE_CORPS_CATEG_EMPLOI_KEY);
  }

  public void removeFromToListeCorpsCategEmploiRelationship(org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LISTE_CORPS_CATEG_EMPLOI_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi createToListeCorpsCategEmploiRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CorpsCategEmploi");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_LISTE_CORPS_CATEG_EMPLOI_KEY);
    return (org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi) eo;
  }

  public void deleteToListeCorpsCategEmploiRelationship(org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_LISTE_CORPS_CATEG_EMPLOI_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeCorpsCategEmploiRelationships() {
    Enumeration objects = toListeCorpsCategEmploi().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeCorpsCategEmploiRelationship((org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCategorieEmploi avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCategorieEmploi createEOCategorieEmploi(EOEditingContext editingContext, String cCategorieEmploi
, NSTimestamp dCreation
, NSTimestamp dModification
, String temBudgetPropre
, String temPast
, String temSurnombre
			) {
    EOCategorieEmploi eo = (EOCategorieEmploi) createAndInsertInstance(editingContext, _EOCategorieEmploi.ENTITY_NAME);    
		eo.setCCategorieEmploi(cCategorieEmploi);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemBudgetPropre(temBudgetPropre);
		eo.setTemPast(temPast);
		eo.setTemSurnombre(temSurnombre);
    return eo;
  }

  
	  public EOCategorieEmploi localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCategorieEmploi)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCategorieEmploi creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCategorieEmploi creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCategorieEmploi object = (EOCategorieEmploi)createAndInsertInstance(editingContext, _EOCategorieEmploi.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCategorieEmploi localInstanceIn(EOEditingContext editingContext, EOCategorieEmploi eo) {
    EOCategorieEmploi localInstance = (eo == null) ? null : (EOCategorieEmploi)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCategorieEmploi#localInstanceIn a la place.
   */
	public static EOCategorieEmploi localInstanceOf(EOEditingContext editingContext, EOCategorieEmploi eo) {
		return EOCategorieEmploi.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCategorieEmploi fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCategorieEmploi fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCategorieEmploi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCategorieEmploi)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCategorieEmploi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCategorieEmploi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCategorieEmploi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCategorieEmploi)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCategorieEmploi fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCategorieEmploi eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCategorieEmploi ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCategorieEmploi fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

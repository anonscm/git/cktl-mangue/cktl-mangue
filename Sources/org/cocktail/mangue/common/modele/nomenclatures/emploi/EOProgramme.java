// EOProgramme.java
// Created on Mon Feb 05 15:28:03 Europe/Paris 2007 by Apple EOModeler Version 5.2

package org.cocktail.mangue.common.modele.nomenclatures.emploi;

import org.cocktail.common.utilities.RecordAvecLibelleEtCode;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class EOProgramme extends _EOProgramme implements RecordAvecLibelleEtCode {

	public static final String DEFAULT_PROGRAMME = "0150";
	
    public EOProgramme() {
        super();
    }

    public static EOProgramme defaultProgramme(EOEditingContext ec) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_PROGRAMME_KEY + "=%@", new NSArray(DEFAULT_PROGRAMME));
    	return fetchFirstByQualifier(ec, qualifier);
    }
    
    public String libelle() {
    	return llProgramme();
    }
    public String code() {
    	return cProgramme();
    }
    
    @Override
    public String toString() {
    	return code() + " - " + lcProgramme();
    }
    
    /**
     * @return le code et le libellé long du programme
     */
    public String codeEtLibelleLong() {
    	return code() + " - " + llProgramme();
    }
}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.common.modele.nomenclatures.emploi;

import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum._EOCorpsCategEmploi;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOCorpsCategEmploi extends _EOCorpsCategEmploi {

    public EOCorpsCategEmploi() {
        super();
    }

    /**
     * 
     * @param ec
     * @param categorie
     * @param corps
     * @return
     */
	public static EOCorpsCategEmploi creer(EOEditingContext ec, EOCategorieEmploi categorie, EOCorps corps) {

		EOCorpsCategEmploi newObject = (EOCorpsCategEmploi) createAndInsertInstance(ec, EOCorpsCategEmploi.ENTITY_NAME);    

		newObject.setToCategorieEmploiRelationship(categorie);
		newObject.setToCorpsRelationship(corps);
		newObject.setCCategorieEmploi(categorie.cCategorieEmploi());
		newObject.setCCorps(corps.cCorps());

		newObject.setTemCompatible("D");
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());
		
		return newObject;
	}

    /**
     * 
     * @param edc
     * @param categorie
     * @return
     */
    public static NSArray<EOCorpsCategEmploi> findForCategorie(EOEditingContext edc, EOCategorieEmploi categorie) {
    	
    	NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
    	
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CATEGORIE_EMPLOI_KEY + " = %@", new NSArray(categorie)));
    	
    	return fetchAll(edc, new EOAndQualifier(qualifiers));
    	
    }
    
    /**
     * 
     * @param edc
     * @param categorie
     * @param corps
     * @return
     */
    public static EOCorpsCategEmploi findForCategorieAndCorps(EOEditingContext edc, EOCategorieEmploi categorie, EOCorps corps) {
    	
    	NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
    	
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CATEGORIE_EMPLOI_KEY + " = %@", new NSArray(categorie)));
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TO_CORPS_KEY + " = %@", new NSArray(corps)));
    	
    	return fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers));
    	
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

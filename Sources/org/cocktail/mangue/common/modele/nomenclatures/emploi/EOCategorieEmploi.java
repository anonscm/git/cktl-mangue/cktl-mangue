// EOCategorieEmploi.java
// Created on Fri Feb 14 07:26:21  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures.emploi;


import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.grhum.EOCorps;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;
public class EOCategorieEmploi extends _EOCategorieEmploi implements RecordAvecLibelleEtCode {

	public static EOSortOrdering SORT_CODE_ASC = new EOSortOrdering(C_CATEGORIE_EMPLOI_KEY, EOSortOrdering.CompareAscending);
	public static EOSortOrdering SORT_LIBELLE_ASC = new EOSortOrdering(LC_CATEGORIE_EMPLOI_KEY, EOSortOrdering.CompareAscending);

	public static NSArray SORT_ARRAY_CODE_ASC = new NSArray(SORT_CODE_ASC);
	public static NSArray SORT_ARRAY_LIBELLE_ASC = new NSArray(SORT_LIBELLE_ASC);

	private static String CATEGORIE_ADT = "ADT";
	private static String CATEGORIE_AGT = "AGT";
	private static String CATEGORIE_ASTR = "ASTR";

	public static final String TYPE_POPULATION_KEY = "typePopulation";
	public static final String PREMIER_CORPS_KEY = "premierCorps";

	public EOCategorieEmploi() {
		super();
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EOCategorieEmploi creer(EOEditingContext ec) {
		EOCategorieEmploi newObject = new EOCategorieEmploi();    
		newObject.setDCreation(new NSTimestamp());
		newObject.setTemBudgetPropre(CocktailConstantes.FAUX);
		newObject.setTemSurnombre(CocktailConstantes.FAUX);
		newObject.setTemPast(CocktailConstantes.FAUX);
		ec.insertObject(newObject);
		return newObject;
	}
	public boolean estADT() {
		return cCategorieEmploi().equals(CATEGORIE_ADT);
	}
	public boolean estAGT() {
		return cCategorieEmploi().equals(CATEGORIE_AGT);
	}
	public boolean estAstr() {
		return cCategorieEmploi().equals(CATEGORIE_ASTR);
	}

	// interface RecordAvecLibelleEtCode
	public String code() {
		return cCategorieEmploi();
	}
	public String libelle() {
		return llCategorieEmploi();
	}
	/** Recherche le type de population de cette cat&eacute;gorie d'emploi
	 * @return type population trouv&eacute; ou null
	 */
	public EOTypePopulation typePopulation() {
		if (premierCorps() != null) {
			return premierCorps().toTypePopulation();
		}

		return null;
	}

	/** Recherche le type de population de cette cat&eacute;gorie d'emploi
	 * @return type population trouv&eacute; ou null
	 */
	public EOCorps premierCorps() {
		String stringQualifier = C_CATEGORIE_EMPLOI_KEY + " = '" + this.cCategorieEmploi() + "' and temCompatible='D'";
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier, null);
		EOFetchSpecification fs = new EOFetchSpecification("CorpsCategEmploi", myQualifier, null);
		fs.setFetchLimit(1);
		NSArray result = editingContext().objectsWithFetchSpecification(fs);
		if (((NSArray) result.valueForKey("toCorps")).count() > 0) {
			return ((EOCorps)(((NSArray) result.valueForKey("toCorps")).objectAtIndex(0)));
		} else {
			return null;
		}
	}

	// Méthodes statiques
	/** Retourne la liste des categories emploi pour les types de population passe en parametre
	 * Retourne toutes les catégories emploi si le tableau est nul ou vide */
	public static NSArray<EOCategorieEmploi> categoriesEmploi(EOEditingContext editingContext,NSArray typesPopulation) {
		EOQualifier qualifier = null;
		//     	EOQualifier.qualifierWithQualifierFormat("temCompatible='D'", null)

		if (typesPopulation != null && typesPopulation.count() > 0) {
			NSMutableArray qualifiers = new NSMutableArray();
			java.util.Enumeration e = typesPopulation.objectEnumerator();
			while (e.hasMoreElements()) {
				EOTypePopulation typePopulation = (EOTypePopulation)e.nextElement();
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("toCorps.toTypePopulation = %@", new NSArray(typePopulation)));
			}
			qualifier = new EOOrQualifier(qualifiers);
		}
		EOFetchSpecification fs = new EOFetchSpecification("CorpsCategEmploi",qualifier,null);
		NSArray result = editingContext.objectsWithFetchSpecification(fs);
		NSMutableArray categories = new NSMutableArray();
		java.util.Enumeration e = result.objectEnumerator();
		while (e.hasMoreElements()) {
			EOGenericRecord record = (EOGenericRecord)e.nextElement();
			EOCategorieEmploi categorie = (EOCategorieEmploi)record.valueForKey("toCategorieEmploi");
			if (categories.containsObject(categorie) == false) {
				categories.addObject(categorie);
			}
		}
		return categories;
	}

	/**
	 * @return Le code et le libellé court de la catégorie d'emploi
	 */
	public String codeEtLibelle() {
		return this.cCategorieEmploi() + " - " + this.libelle();
	}

	/**
	 * 
	 */
	public void validateForSave() throws NSValidation.ValidationException {

		if (cCategorieEmploi() == null || lcCategorieEmploi() == null || llCategorieEmploi() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un code, un libellé court et un libellé long !");
		}
		
		if (cCategorieEmploi().length() > 7)
			throw new NSValidation.ValidationException("Le code doit comporter au maximum 7 caractères !");
		if (lcCategorieEmploi().length() > 20)
			throw new NSValidation.ValidationException("Le libellé court doit comporter au maximum 7 caractères !");
		if (llCategorieEmploi().length() > 60)
			throw new NSValidation.ValidationException("Le libellé long doit comporter au maximum 7 caractères !");
		
		setCCategorieEmploi(cCategorieEmploi().toUpperCase());
	}
}


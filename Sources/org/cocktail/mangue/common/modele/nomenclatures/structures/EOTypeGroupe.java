// EOTypeGroupe.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
// Created on Fri Jun 13 11:32:07 Europe/Paris 2008 by Apple EOModeler Version 5.2

package org.cocktail.mangue.common.modele.nomenclatures.structures;

import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class EOTypeGroupe extends _EOTypeGroupe {

	public static final String TYPE_GROUPE_GROUPE = "G";
	public static final String TYPE_GROUPE_SERVICE = "S";
	public static final String TYPE_GROUPE_SERVICE_GESTIONNAIRE = "SG";
	public static final String TYPE_GROUPE_ENTREPRISE = "EN";
	public static final String TYPE_LABO = "LA";
	public static final String TYPE_ECOLE_DOCTORALE = "ED";
	public static final String TYPE_LOCAL = "LO";
	
	public static final NSArray<String> ARRAY_FILTRES = new NSArray<String>(new String[]{TYPE_GROUPE_SERVICE, TYPE_GROUPE_ENTREPRISE, TYPE_LABO, 
			TYPE_ECOLE_DOCTORALE, TYPE_LOCAL});
	
    public EOTypeGroupe() {
        super();
    }

    public String toString() {
    	return codeEtLibelle();
    }
    
    public boolean estService() {
    	return code().equals(TYPE_GROUPE_SERVICE);
    }
    public boolean estLabo() {
    	return code().equals(TYPE_LABO);
    }
    
    public static EOTypeGroupe getTypeGroupeGroupe(EOEditingContext edc) {
    	return (EOTypeGroupe)NomenclatureFinder.findForCode(edc, ENTITY_NAME, TYPE_GROUPE_GROUPE);    	
    }
    public static EOTypeGroupe getTypeGroupeService(EOEditingContext edc) {
    	return (EOTypeGroupe)NomenclatureFinder.findForCode(edc, ENTITY_NAME, TYPE_GROUPE_SERVICE);    	
    }
    public static EOTypeGroupe getTypeGroupeServiceGestionnaire(EOEditingContext edc) {
    	return (EOTypeGroupe)NomenclatureFinder.findForCode(edc, ENTITY_NAME, TYPE_GROUPE_SERVICE_GESTIONNAIRE);    	
    }
    public static EOTypeGroupe getTypeGroupeEntreprise(EOEditingContext edc) {
    	return (EOTypeGroupe)NomenclatureFinder.findForCode(edc, ENTITY_NAME, TYPE_GROUPE_ENTREPRISE);    	
    }
    
}

//EOTypePopulation.java
//Created on Sun Apr 13 17:30:48  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures.carriere;


import org.cocktail.common.utilities.RecordAvecLibelleEtCode;
import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOTypePopulation extends _EOTypePopulation implements RecordAvecLibelleEtCode {

	public  final static String ENSEIGNANT_CHERCHEUR = "SA";
	private final static String NORMALIEN = "N";

	public EOTypePopulation() {
		super();
	}

	/**
	 * @param ec : editing context
	 * @return la liste des types de population visibles
	 */
	@SuppressWarnings("unchecked")
	public static NSArray<EOTypePopulation> rechercherListeTypePopulationVisible(EOEditingContext ec) {
		try {
			return EOTypePopulation.fetchAll(ec, EOQualifier.qualifierWithQualifierFormat(TEM_VISIBLE_KEY + "=%@", new NSArray<String>(CocktailConstantes.VRAI)),
					SORT_ARRAY_LIBELLE_COURT);

		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOTypePopulation>();
		}
	}

	public String toString() {
		return code() + "     \r- "+ libelleLong();		
	}

	public boolean isLocal() {
		return false;
	}

	public boolean estVisible() {
		return temVisible().equals(CocktailConstantes.VRAI);
	}
	public void setEstVisible(Boolean yn) {
		if (yn)
			setTemVisible(CocktailConstantes.VRAI);
		else
			setTemVisible(CocktailConstantes.FAUX);
	}

	public boolean estItarf() {
		return temItarf() != null && temItarf().equals(CocktailConstantes.VRAI);
	}
	public boolean estBiblio() {
		return temBiblio() != null && temBiblio().equals(CocktailConstantes.VRAI);
	}
	public boolean est1erDegre() {
		return tem1Degre() != null && tem1Degre().equals(CocktailConstantes.VRAI);
	}
	public boolean est2Degre() {
		return tem2Degre() != null && tem2Degre().equals(CocktailConstantes.VRAI);
	}
	public boolean estAtos() {
		return temAtos() != null && temAtos().equals(CocktailConstantes.VRAI);
	}
	public boolean estEnseignant() {
		return temEnseignant() != null && temEnseignant().equals(CocktailConstantes.VRAI);
	}
	public boolean estFonctionnaire() {
		return temFonctionnaire() != null && temFonctionnaire().equals(CocktailConstantes.VRAI);
	}
	public boolean estHospitalier() {
		return temHospitalier() != null && temHospitalier().equals(CocktailConstantes.VRAI);
	}
	public boolean estNormalien() {
		return code().equals(NORMALIEN);
	}
	/**
	 * Est-ce que le type de population concerne les enseignants du supérieur?
	 * @return true/false
	 */
	public boolean estEnseignantSuperieur() {
		return temEnsSup() != null && temEnsSup().equals(CocktailConstantes.VRAI);
	}

	// interface RecordAvecLibelleEtCode
	public String libelle() {
		return libelleCourt();
	}

	public static EOTypePopulation fetchForCode(EOEditingContext ec,String code) {
		try {
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@", new NSArray(code));
			return fetchFirstByQualifier(ec, myQualifier);
		} catch (Exception e) {
			return null;
		}
	}

	/** recherche le type de population avec le libelle fourni
	 * @param editingContext
	 * @param libelle
	 * @return EOTypePopulation
	 */
	public static EOTypePopulation rechercherTypePopulationAvecLibelle(EOEditingContext ec,String libelle) {
		try {
			NSMutableArray args = new NSMutableArray(libelle);
			args.addObject(libelle);
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(LIBELLE_COURT_KEY + " = %@ OR " + LIBELLE_LONG_KEY + " = %@",args);

			return fetchFirstByQualifier(ec, myQualifier);
		} catch (Exception e) {
			return null;
		}
	}
	/** recherche les types de population avec les codes fournis. Si le tableau est nul ou vide, retourne tous les types de population
	 * @param editingContext
	 * @param codesPopulation liste de codes population
	 * @return tableau de EOTypePopulation tri&eacute; par ordre alphab&eacute;tique sur les lib&eacute;ll&eacute;s courts
	 */
	public static NSArray<EOTypePopulation> rechercherTypePopulationAvecCodes(EOEditingContext editingContext,NSArray codesPopulation) {

		EOQualifier qualifier = null;
		if (codesPopulation != null && codesPopulation.count() > 0) {
			NSMutableArray args = new NSMutableArray(codesPopulation.objectAtIndex(0));
			String stringQualifier = CODE_KEY + " = %@";
			for (int i = 1; i < codesPopulation.count();i++) {
				args.addObject(codesPopulation.objectAtIndex(i));
				stringQualifier = stringQualifier + " OR " + CODE_KEY + " = %@";
			}
			qualifier = EOQualifier.qualifierWithQualifierFormat(stringQualifier,args);
		}

		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(LIBELLE_COURT_KEY, EOSortOrdering.CompareAscending);
		EOFetchSpecification myFetch = new EOFetchSpecification(ENTITY_NAME,qualifier,new NSArray(sort));

		return editingContext.objectsWithFetchSpecification(myFetch);

	}

	/** Tests de coherence pour l'enregistrement d'un type de population */
	public void validateForSave() throws com.webobjects.foundation.NSValidation.ValidationException {

		super.validateForSave();
		
	}


}

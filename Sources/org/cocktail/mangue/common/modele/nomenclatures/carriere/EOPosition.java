//EOPosition.java
//Created on Mon Feb 24 10:33:22  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures.carriere;


import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.modele.IValidite;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOPosition extends _EOPosition implements IValidite {
	
	public static String 	CODE_POSITION_EN_ACTIVITE = "ACTI";
	public static String 	CODE_POSITION_DETACHEMENT = "DETA";
	public static String 	CODE_POSITION_DISPO = "DISP";
	private static String 	CODE_SERVICE_NATIONAL = "SNAT";
	private static String 	CODE_CONGE_PARENTAL = "CGPA";
	private static String	CODE_HORS_CADRE = "HCAD";

	public EOPosition() {
		super();
	}
	
	public String toString() {
		return libelleLong();
	}

	public static EOPosition getPositionActivite(EOEditingContext edc) {
		return fetchFirstByQualifier(edc, EOQualifier.qualifierWithQualifierFormat(CODE_KEY + " =%@", new NSArray(CODE_POSITION_EN_ACTIVITE)));
	}
	public static EOPosition getPositionDetachement(EOEditingContext edc) {
		return fetchFirstByQualifier(edc, EOQualifier.qualifierWithQualifierFormat(CODE_KEY + " =%@", new NSArray(CODE_POSITION_DETACHEMENT)));
	}
	
	/**
	 * 
	 * @param edc
	 * @param individu
	 * @param debutPeriode
	 * @param finPeriode
	 * @return
	 */
	public static NSArray<EOPosition> getPositionsForPasseEAS(EOEditingContext edc) {

		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@", new NSArray(CODE_POSITION_EN_ACTIVITE)));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@", new NSArray(CODE_CONGE_PARENTAL)));
		orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@", new NSArray(CODE_POSITION_DISPO)));

		return fetchAll(edc, new EOOrQualifier(orQualifiers), SORT_ARRAY_LIBELLE );

	}
	

	/** retourne true si c'est une position applicable uniquement aux titulaires */
	public boolean estUniquementPourTitulaire() {
		return temTitulaire() != null && temTitulaire().equals(CocktailConstantes.VRAI);
	}
	/** return 
	/** retourne true si c'est une position d'activite */
	public boolean estEnActivite() {
		return temActivite() != null && temActivite().equals(CocktailConstantes.VRAI);
	}
	/** return true si c'est une position d'activite dans l'etablissement */
	public boolean estEnActiviteDansEtablissement() {
		return temActivite().equals("O");
	}
	/** return true si c'est une position de service national */
	public boolean estServiceNational() {
		return code() != null && code().equals(CODE_SERVICE_NATIONAL);
	}
	public boolean estHorsCadre() {
		return code() != null && code().equals(CODE_HORS_CADRE);
	}
	public boolean estCongeParental() {
		return code() != null && code().equals(CODE_CONGE_PARENTAL);
	}
	/** return true si il s'agit d'un detachement */
	public boolean estUnDetachement() {
		return temDetachement() != null && temDetachement().equals(CocktailConstantes.VRAI);
	}

	public boolean estUneDispo() {
		return code().equals(CODE_POSITION_DISPO);
	}

	/** return true si il faut declarer un enfant */
	public boolean requiertEnfant() {
		return temEnfant() != null && temEnfant().equals(CocktailConstantes.VRAI);
	}
	/** return true si il faut declarer un enfant */
	public boolean requiertMotif() {
		return temMotif() != null && temMotif().equals(CocktailConstantes.VRAI);
	}

	/** Retourne true si la position doit generer un evenement dans la table des Absences */
	public boolean doitGenererEvenement() {
		return estUnDetachement() == false;
	}
	/** return true si la promotion de grade est autorisee */
	public boolean promotionGradePossible() {
		return prctDroitAvctGrad() != null && prctDroitAvctGrad().intValue() > 0;
	}
	/** return true si la promotion de grade est autorisee */
	public boolean promotionEchelonPossible() {
		return prctDroitAvctEch() != null && prctDroitAvctEch().intValue() > 0;
	}
	public boolean nonValidePourStagiaire() {
		return temStagiaire() == null || temStagiaire().equals(CocktailConstantes.FAUX);
	}
	public boolean dateFinObligatoire() {
		return temDFinOblig() != null && temDFinOblig().equals(CocktailConstantes.VRAI);
	}
	/** Retourne true si la position autorise un contrat de travail */
	public boolean autoriseContrat() {
		return temCtraTrav() != null && temCtraTrav().equals(CocktailConstantes.VRAI);
	}
	/** Retourne true si une position est valide pour le CRCT (toute position sauf hors cadre, disponibilite, conge parental et service national)*/
	public boolean estValidePourCrct() {
		return code() != null && estServiceNational() == false 
			&& estCongeParental() == false 
			&& estUneDispo() == false 
			&& estHorsCadre() == false;
	}
	
	// Méthodes statiques
	public static NSArray findPositions(EOEditingContext ec, EOQualifier qualifier) {

		NSMutableArray qualifiers = new NSMutableArray();
		
		if (qualifier != null)
			qualifiers.addObject(qualifier);
		
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "= %@", new NSArray(CocktailConstantes.VRAI)));
		
		return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_CODE);
	}
	
	public boolean estValide() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setEstValide(boolean aBool) {
		// TODO Auto-generated method stub
		if (aBool)
			setTemValide(CocktailConstantes.VRAI);
		else
			setTemValide(CocktailConstantes.FAUX);			
	}
}

// EOTypeMotProlongation.java
// Created on Wed Feb 18 13:23:35 Europe/Paris 2009 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures.carriere;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

// 16/06/2010 - ajout de la référence réglementaire
// 14/07/2010 - ajout du motif de prolongation Onp
public class EOTypeMotProlongation extends _EOTypeMotProlongation {
	
	public final static String TYPE_MOTIF_RECUL_AGE = "R";
	public final static String TYPE_MOTIF_PROLONGATION = "P";
	
	private final static String TYPE_A_CHARGE_A_65 = "1ENFANT65ANS";
	private final static String TYPE_3_ENFANTS_A_50 = "3ENFANTS50ANS";
	private final static String TYPE_MORT_POUR_LA_FRANCE = "ENFANTMORT";
	private final static String TYPE_MAINTIEN_SERVICE = "MAINTIENSERVICE";
	public final static String TYPE_SURNOMBRE = "MAINTIENSURNOMBRE";
	private final static String TYPE_ARTICLE_69 = "PROLONGART69";


	public EOTypeMotProlongation() {
		super();
	}

	public String toString() {
		return libelleLong();
	}
	
	public boolean estEnfantAChargeA65Ans() {
		return libelleCourt() != null && libelleCourt().equals(TYPE_A_CHARGE_A_65);
	}
	public boolean est3EnfantsA50Ans() {
		return libelleCourt() != null && libelleCourt().equals(TYPE_3_ENFANTS_A_50);
	}
	public boolean estEnfantMortPourFrance() {
		return libelleCourt() != null && libelleCourt().equals(TYPE_MORT_POUR_LA_FRANCE);
	}
	public boolean estMaintienService() {
		return libelleCourt() != null && libelleCourt().equals(TYPE_MAINTIEN_SERVICE);
	}
	public boolean estSurnombre() {
		return libelleCourt() != null && libelleCourt().equals(TYPE_SURNOMBRE);
	}
	public boolean estTypeArticle69() {
		return libelleCourt() != null && libelleCourt().equals(TYPE_ARTICLE_69);
	}

	/** Retourne les types de prolongation en fonction du type de motif qui doit &ecirc;tre une des 2 valeurs :
	 * TYPE_MOTIF_RECUL_AGE, TYPE_MOTIF_PROLONGATION
	 */
	public static NSArray<EOTypeMotProlongation> rechercherTypesProlongation(EOEditingContext edc,String typeMotif) {
		
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TYPE_MOTIF_KEY + " = %@", new NSArray(typeMotif)));		
		if (typeMotif.equals(TYPE_MOTIF_PROLONGATION)) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LIBELLE_COURT_KEY + " != %@", new NSArray("PROLONG1948")));
		}
		
		return fetchAll(edc, new EOAndQualifier(qualifiers));
	}

	/**
	 * 
	 * @param ec
	 * @param typeMotif
	 * @return
	 */
	public static EOTypeMotProlongation rechercherTypeProlongation(EOEditingContext ec,String typeMotif) {
		try {

			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(LIBELLE_COURT_KEY + " = %@", new NSArray(TYPE_SURNOMBRE));
			EOFetchSpecification fs = new EOFetchSpecification(ENTITY_NAME, qualifier,null);
			return (EOTypeMotProlongation)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			return null;
		}
	}

}

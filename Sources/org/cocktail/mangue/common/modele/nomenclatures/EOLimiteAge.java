// EOLimiteAge.java
// Created on Wed Jul 14 17:24:53 Europe/Paris 2010 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures;

import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EOLimiteAge extends _EOLimiteAge {

	public static String CODE_LIMITE_65 = "6500";
	public static String CODE_LIMITE_65_04 = "6504";
	public static String CODE_LIMITE_65_09 = "6509";
	public static String CODE_LIMITE_66_02 = "6602";
	public static String CODE_LIMITE_66_07 = "6607";
	public static String CODE_LIMITE_67 = "6700";

	public EOLimiteAge() {
		super();
	}

	/**
	 * 
	 * @param edc
	 * @param dateNaissance
	 * @return
	 */
	public static EOLimiteAge limitePourDateNaissance(EOEditingContext edc, NSTimestamp dateNaissance) {

		try {
			if ( DateCtrl.isBeforeEq(dateNaissance, DateCtrl.stringToDate("01/07/1951")) ) 
				return findForCode(edc, CODE_LIMITE_65);

			if ( DateCtrl.isBetween(dateNaissance, DateCtrl.stringToDate("02/07/1951"), DateCtrl.stringToDate("31/12/1951")) ) 
				return findForCode(edc, CODE_LIMITE_65_04);

			if ( DateCtrl.getYear(dateNaissance) == 1952)
				return findForCode(edc, CODE_LIMITE_65_09);

			if ( DateCtrl.getYear(dateNaissance) == 1953)
				return findForCode(edc, CODE_LIMITE_66_02);

			if ( DateCtrl.getYear(dateNaissance) == 1954)
				return findForCode(edc, CODE_LIMITE_66_07);

			return findForCode(edc, CODE_LIMITE_67);
			
		}
		catch (Exception e) {
			return null;
		}
	}

	public static NSArray<EOLimiteAge> findLimitesValides(EOEditingContext edc) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + "=%@", new NSArray(CocktailConstantes.VRAI));
		return fetchAll(edc, myQualifier, Nomenclature.SORT_ARRAY_CODE);
	}

	public static EOLimiteAge findForCode(EOEditingContext edc, String code) {
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@", new NSArray(code));
		return fetchFirstByQualifier(edc, myQualifier);
	}
}
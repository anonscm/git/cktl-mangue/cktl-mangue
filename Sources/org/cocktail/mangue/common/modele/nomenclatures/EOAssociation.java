// EOAssociation.java
// Created on Wed Jun 11 12:24:00 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures;

import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.component.utilities.InterfaceArbreAvecFils;
import org.cocktail.mangue.modele.grhum.EOAssociationReseau;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOAssociation extends _EOAssociation implements InterfaceArbreAvecFils, RecordAvecLibelle {
	
	public final static String LIBELLE_ASSOCIATION_AFFECTATION = "AFFECTATION";
	public final static String LIBELLE_ASSOCIATION_HEBERGE = "HEBERGE";
	public final static String LIBELLE_ASSOCIATION_ENSEIGNANT = "ENSEIGNANT(E)";
	
    public EOAssociation() {
        super();
    }

       /** Recherche dans AssociationReseau tous les fils du record courant et retourne les associations qui sont fils des peres trouves
     */
	public NSArray trouverFils() {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOAssociationReseau.PERE_KEY + "=%@", new NSArray(this));
		EOFetchSpecification fs = new EOFetchSpecification(EOAssociationReseau.ENTITY_NAME,qualifier, null);
		NSArray results = editingContext().objectsWithFetchSpecification(fs);
		NSMutableArray fils = new NSMutableArray(results.count());
		for (java.util.Enumeration<EOAssociationReseau> e = results.objectEnumerator();e.hasMoreElements();) {
			EOAssociationReseau reseau = e.nextElement();
			if (fils.containsObject(reseau.fils()) == false) {
				fils.addObject(reseau.fils());
			}
		}
		return fils;
	}

	// Méthodes statiques
	public static NSArray rechercherFils(EOEditingContext editingContext,String nomPere) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOAssociationReseau.PERE_KEY + "." + EOAssociation.LIBELLE_LONG_KEY + "=%@", new NSArray(nomPere));
		EOFetchSpecification fs = new EOFetchSpecification(EOAssociationReseau.ENTITY_NAME,qualifier, null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		return (NSArray)results.valueForKey(EOAssociationReseau.FILS_KEY);
	}
	
	public static EOAssociation associationPourAffectation(EOEditingContext editingContext) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(LIBELLE_LONG_KEY +"=%@", new NSArray(LIBELLE_ASSOCIATION_AFFECTATION));		
		return fetchFirstByQualifier(editingContext, qualifier);
	}
	
	public static EOAssociation associationPourHeberge(EOEditingContext editingContext) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(LIBELLE_LONG_KEY +"=%@", new NSArray(LIBELLE_ASSOCIATION_HEBERGE));		
		return fetchFirstByQualifier(editingContext, qualifier);
	}

	/**
	 * @param editingContext : editingContext
	 * @return Association pour le role Enseignant
	 */
	public static EOAssociation associationPourEnseignant(EOEditingContext editingContext) {
				
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(LIBELLE_LONG_KEY + "=%@", new NSArray(LIBELLE_ASSOCIATION_ENSEIGNANT));	
		return fetchFirstByQualifier(editingContext, qualifier);
	}
}

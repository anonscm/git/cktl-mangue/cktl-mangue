//EOPrime.java
//Created on Fri Sep 26 09:53:44 Europe/Paris 2008 by Apple EOModeler Version 5.2
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures.primes;

import org.cocktail.common.utilities.RecordAvecLibelle;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.goyave.DureeAvecValidite;
import org.cocktail.mangue.modele.goyave.EOPlanComptable;
import org.cocktail.mangue.modele.goyave.EOPrimeCode;
import org.cocktail.mangue.modele.goyave.EOPrimeExclusion;
import org.cocktail.mangue.modele.goyave.EOPrimeParam;
import org.cocktail.mangue.modele.goyave.ObjetAvecValidite;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/** R&egrave;les de validation des primes<BR>
 * Si on demande la verification des echelons pour une prime alors on force la verification des grades<BR>
 * Les champs obligatoires, les longueurs des cha&icirc;nes sont verifiees<BR>
 * Verifie que le debut de validite est anterieur a la fin de validite<BR>
 * Verifie les types de population et les types de periodicite<BR>
 * Verifie qu'une prime a attributions annuelles multiples est necessairement unique et individuelle
 * @author christine
 *
 */

public class EOPrime extends _EOPrime {
	public final static String PRIME_POUR_ENSEIGNANT = "Enseignant";
	public final static String PRIME_POUR_IATOS = "IATOS";
	public final static String PRIME_POUR_TOUS = "Commun";
	public final static String VERSEMENT_UNIQUE = "Unique";
	public final static String VERSEMENT_ANNUEL = "Annuel";
	public final static String VERSEMENT_SEMESTRIEL = "Semestriel";
	public final static String VERSEMENT_TRIMESTRIEL = "Trimestriel";
	public final static String VERSEMENT_MENSUEL = "Mensuel";
	private EOPrimeCode codePourMontant;	// Utilisé pour trouver le label à afficher lors de la saisie du montant
	
	public EOPrime() {
		super();
		}


	public boolean estValidePourPension() {
		return temPension() != null && temPension().equals(CocktailConstantes.VRAI);
	}
	public void setEstValidePourPension(boolean aBool) {
		if (aBool) {
			setTemPension(CocktailConstantes.VRAI);
		} else {
			setTemPension(CocktailConstantes.FAUX);
		}
	}
	public boolean estRetenue() {
		return temRetenue() != null && temRetenue().equals(CocktailConstantes.VRAI);
	}
	public void setEstRetenue(boolean aBool) {
		if (aBool) {
			setTemRetenue(CocktailConstantes.VRAI);
		} else {
			setTemRetenue(CocktailConstantes.FAUX);
		}
	}
	public boolean estProratisable() {
		return temProratisation() != null && temProratisation().equals(CocktailConstantes.VRAI);
	}
	public void setEstProratisable(boolean aBool) {
		if (aBool) {
			setTemProratisation(CocktailConstantes.VRAI);
		} else {
			setTemProratisation(CocktailConstantes.FAUX);
		}
	}
	public boolean estPersonnalisable() {
		return temPerso() != null && temPerso().equals(CocktailConstantes.VRAI);
	}
	public void setEstPersonnalisable(boolean aBool) {
		if (aBool) {
			setTemPerso(CocktailConstantes.VRAI);
		} else {
			setTemPerso(CocktailConstantes.FAUX);
		}
	}
	/** return true si la prime est locale &agrave; l'&eacute;tablissement  */
	public boolean estLocale() {
		return temLocal() != null && temLocal().equals(CocktailConstantes.VRAI);
	}
	public void setEstLocale(boolean aBool) {
		if (aBool) {
			setTemLocal(CocktailConstantes.VRAI);
		} else {
			setTemLocal(CocktailConstantes.FAUX);
		}
	}
	/** Une prime individuelle ne peut &ecirc;tre g&eacute;r&eacute;e par le gestionnaire central de primes */
	public boolean estIndividuelle() {
		return primTemIndividuelle() != null && primTemIndividuelle().equals(CocktailConstantes.VRAI);
	}
	public void setEstIndividuelle(boolean aBool) {
		if (aBool) {
			setPrimTemIndividuelle(CocktailConstantes.VRAI);
		} else {
			setPrimTemIndividuelle(CocktailConstantes.FAUX);
		}
	}
	/** Retourne true si une prime ne peut &ecirc;tre attribu&eacute;e qu'une seule fois dans l'ann&eacute;e */
	public boolean estAttributionUniqueDansAnnee() {
		return primTemMultiple() != null && primTemMultiple().equals(CocktailConstantes.FAUX);
	}
	public void setEstAttributionUniqueDansAnnee(boolean aBool) {
		if (aBool) {
			setPrimTemMultiple(CocktailConstantes.FAUX);
		} else {
			setPrimTemMultiple(CocktailConstantes.VRAI);
		}
	}
	public boolean verifierContrat() {
		return primVerifContrat() != null && primVerifContrat().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierContrat(boolean aBool) {
		if (aBool) {
			setPrimVerifContrat(CocktailConstantes.VRAI);
		} else {
			setPrimVerifContrat(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierCorps() {
		return primVerifCorps() != null && primVerifCorps().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierCorps(boolean aBool) {
		if (aBool) {
			setPrimVerifCorps(CocktailConstantes.VRAI);
		} else {
			setPrimVerifCorps(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierEchelon() {
		return primVerifEchelon() != null && primVerifEchelon().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierEchelon(boolean aBool) {
		if (aBool) {
			setPrimVerifEchelon(CocktailConstantes.VRAI);
			if (verifierGrade() == false) {
				setVerifierGrade(true);
			}
		} else {
			setPrimVerifEchelon(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierExclusion() {
		return primVerifExclusion() != null && primVerifExclusion().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierExclusion(boolean aBool) {
		if (aBool) {
			setPrimVerifExclusion(CocktailConstantes.VRAI);
		} else {
			setPrimVerifExclusion(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierFonction() {
		return primVerifFonction() != null && primVerifFonction().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierFonction(boolean aBool) {
		if (aBool) {
			setPrimVerifFonction(CocktailConstantes.VRAI);
		} else {
			setPrimVerifFonction(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierGrade() {
		return primVerifGrade() != null && primVerifGrade().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierGrade(boolean aBool) {
		if (aBool) {
			setPrimVerifGrade(CocktailConstantes.VRAI);
		} else {
			setPrimVerifGrade(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierIndice() {
		return primVerifIndice() != null && primVerifIndice().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierIndice(boolean aBool) {
		if (aBool) {
			setPrimVerifIndice(CocktailConstantes.VRAI);
		} else {
			setPrimVerifIndice(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierPopulation() {
		return primVerifPopulation() != null && primVerifPopulation().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierPopulation(boolean aBool) {
		if (aBool) {
			setPrimVerifPopulation(CocktailConstantes.VRAI);
		} else {
			setPrimVerifPopulation(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierPosition() {
		return primVerifPosition() != null && primVerifPosition().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierPosition(boolean aBool) {
		if (aBool) {
			setPrimVerifPosition(CocktailConstantes.VRAI);
		} else {
			setPrimVerifPosition(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierSpecialite() {
		return primVerifSpecialite() != null && primVerifSpecialite().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierSpecialite(boolean aBool) {
		if (aBool) {
			setPrimVerifSpecialite(CocktailConstantes.VRAI);
		} else {
			setPrimVerifSpecialite(CocktailConstantes.FAUX);
		}
	}
	public boolean verifierStructure() {
		return primVerifStructure() != null && primVerifStructure().equals(CocktailConstantes.VRAI);
	}
	public void setVerifierStructure(boolean aBool) {
		if (aBool) {
			setPrimVerifStructure(CocktailConstantes.VRAI);
		} else {
			setPrimVerifStructure(CocktailConstantes.FAUX);
		}
	}
	public boolean estPrimePourTous() {
		return primTypePopulation() != null && primTypePopulation().equals(type(PRIME_POUR_TOUS));
	}
	public boolean estPrimePourEnseignants() {
		return primTypePopulation() != null && primTypePopulation().equals(type(PRIME_POUR_ENSEIGNANT));
	}
	public boolean estPrimeIatos() {
		return primTypePopulation() != null && primTypePopulation().equals(type(PRIME_POUR_IATOS));
	}
	public boolean estCalculee() {
		return primNomClasse() != null && primNomClasse().length() > 0;
	}
	/** Une prime dans laquelle on doit saisir un montant personnalis&eacute; est une prime qui est associ&eacute; &agrave; seul
	 * code de param&egrave;tre personnel
	 */
	public boolean doitSaisirMontantPersonnalise() {
		int nbParametresPerso = 0;
		java.util.Enumeration e1 = codes().objectEnumerator();
		while (e1.hasMoreElements()) {
			EOPrimeCode code = (EOPrimeCode)e1.nextElement();
			NSArray primesParam = EOPrimeParam.rechercherParametresValidesPourCodeEtPeriode(editingContext(), code, null, null);
			if (primesParam.count() == 0) {
				nbParametresPerso++;
				if (nbParametresPerso == 1) {
					codePourMontant = code;
				} else {
					codePourMontant = null;
				}
			}
		}
		return (nbParametresPerso == 1);
	}
	/** Retourne true si il faut v&eacute;rifier le corps ou le grade ou la population */
	public boolean verifierCarriere() {
		return verifierCorps() || verifierGrade() || verifierPopulation();
	}
	/** Retourne true si une prime doit avoir des param&egrave;tres personnels (i.e personnalisable et calcul&eacute;e	 */
	public boolean doitAvoirParametresPersonnels() {
		return estPersonnalisable() && estCalculee() && !doitSaisirMontantPersonnalise();
	}
	/** Retourne le libell&eacute; du code associ&eacute; &agrave; l'unique param&egrave;tre personnel des primes personnalisables pour lesquelles
	 * on doit saisir un montant */
	public String libellePourMontant() {
		if (codePourMontant == null) {
			if (!doitSaisirMontantPersonnalise()) {	// codeParametrePersonnel est initialisé dans cette méthode, pour forcer l'initialisation
				return null;
			}
		}
		if (codePourMontant != null) {
			return codePourMontant.pcodLibelle();
		} else {
			return null;
		}
	}
	/** Retourne le type de population de la prime */
	public String typePopulation() {
		if (primTypePopulation() == null) {
			return null;
		} else if (primTypePopulation().equals(type(PRIME_POUR_ENSEIGNANT))) {
			return PRIME_POUR_ENSEIGNANT;
		} else if (primTypePopulation().equals(type(PRIME_POUR_IATOS))) {
			return PRIME_POUR_IATOS;
		} else if (primTypePopulation().equals(type(PRIME_POUR_TOUS))) {
			return PRIME_POUR_TOUS;
		} else {
			return null;
		}
	}
	/** Modifie le type de population<BR>
	 *  Met une valeur nulle si le type ne correspond pas &agrave; une des constantes PRIME_POUR_TOUS, PRIME_POUR_ENSEIGNANT, PRIME_POUR_IATOS
	 */
	public void setTypePopulation(String aStr) {
		if (aStr != null && aStr.equals(PRIME_POUR_ENSEIGNANT) == false && aStr.equals(PRIME_POUR_IATOS) == false && aStr.equals(PRIME_POUR_TOUS) == false) {
			setPrimTypePopulation(null);
		} else {
			setPrimTypePopulation(type(aStr));
		}
	}
	/** Retourne la p&eacute;riodicit&eacute; de la prime */
	public String periodicite() {
		if (primPeriodicite() == null) {
			return null;
		} else if (primPeriodicite().equals(type(VERSEMENT_UNIQUE))) {
			return VERSEMENT_UNIQUE;
		} else if (primPeriodicite().equals(type(VERSEMENT_ANNUEL))) {
			return VERSEMENT_ANNUEL;
		} else if (primPeriodicite().equals(type(VERSEMENT_SEMESTRIEL))) {
			return VERSEMENT_SEMESTRIEL;
		} else if (primPeriodicite().equals(type(VERSEMENT_TRIMESTRIEL))) {
			return VERSEMENT_TRIMESTRIEL;
		} else if (primPeriodicite().equals(type(VERSEMENT_MENSUEL))) {
			return VERSEMENT_MENSUEL;
		} else {
			return null;
		}
	}
	/** Modifie la p&eacute;riodicit&eacute; de la prime.<BR>
	 *  Met une valeur nulle si le type ne correspond pas &agrave; une des constantes VERSEMENT_UNIQUE, PERIODICITE_ANNUELLE, 
	 *  PERIODICITE_SEMESTRIELLE, PERIODICITE_TRIMESTRIELLE, PERIODICITE_MENSUELLE
	 */
	public void setPeriodicite(String aStr) {
		if (aStr != null && aStr.equals(VERSEMENT_UNIQUE) == false && aStr.equals(VERSEMENT_ANNUEL) == false && 
				aStr.equals(VERSEMENT_SEMESTRIEL) == false && aStr.equals(VERSEMENT_TRIMESTRIEL) == false && 
				aStr.equals(VERSEMENT_MENSUEL) == false) {
			setPrimPeriodicite(null);
		} else {
			setPrimPeriodicite(type(aStr));
		}
	}
	/** Retourne une date de d&eacute;marrage de p&eacute;riode en fonction de l'ann&eacute;e et du mois &agrave;
	 * laquelle la prime d&eacute;marre */
	public NSTimestamp debutPeriode(int annee) {
		NSTimestamp debut =	DateCtrl.stringToDate("01/01/" + annee);
		if (primMoisDemarrage() != null) {
			int mois = primMoisDemarrage().intValue();
			if (mois > 0) {
				debut = DateCtrl.dateAvecAjoutMois(debut, (mois - 1));
			}
		}
		if (DateCtrl.isBefore(debut, debutValidite())) {	// 09/10/09 - dans le cas où la prime commence postérieurement à la date de début de campagne
			debut = debutValidite();
		}
		return debut;
	}
	/** Retourne une date de fin de p&eacute;riode en fonction de l'ann&eacute;e et du mois &agrave;
	 * laquelle la prime d&eacute;marre */
	public NSTimestamp finPeriode(int annee) {
		NSTimestamp dateFin =  DateCtrl.jourPrecedent(debutPeriode(annee + 1));
		// 09/10/09 - dans le cas où la prime se termine avant la date de fin de campagne
		if (finValidite() != null && DateCtrl.isAfter(dateFin, finValidite())) {
			dateFin = finValidite();
		}
		return dateFin;
	}
	
	
	
	public void init() {
		super.init();
		setPrimMoisDemarrage(new Integer(1));
		setEstValidePourPension(false);
		setEstRetenue(false);
		setEstProratisable(false);
		setEstPersonnalisable(false);
		setEstLocale(false);
		setVerifierContrat(false);
		setVerifierCorps(false);
		setVerifierEchelon(false);
		setVerifierExclusion(false);
		setVerifierGrade(false);
		setVerifierIndice(false);
		setVerifierPopulation(false);
		setVerifierPosition(false);
		setVerifierSpecialite(false);
		setVerifierStructure(false);
		setEstValide(true);
	}
	public void validateForSave() throws NSValidation.ValidationException {
		super.validateForSave();
		if (primCode() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un code");
		} else if (primCode().length() > 10) {
			throw new NSValidation.ValidationException("Le code ne peut dépasser 10 caractères");
		}
		if (primCodeComptePce() != null && primCodeComptePce().length() > 2) {
			throw new NSValidation.ValidationException("Le code pce ne peut dépasser 2 caractères");
		}
		if (primCodeIndemnite() != null && primCodeIndemnite().length() > 4) {
			throw new NSValidation.ValidationException("Le code indemnité ne peut dépasser 4 caractères");
		}
		if (primCodeRubriqueTg() != null && primCodeRubriqueTg().length() > 6) {
			throw new NSValidation.ValidationException("Le code indemnité ne peut dépasser 6 caractères");
		}
		if (primDecret() != null && primDecret().length() > 10) {
			throw new NSValidation.ValidationException("Le décret ne peut dépasser 10 caractères");
		}
		if (primChapitre() != null && primChapitre().length() > 4) {
			throw new NSValidation.ValidationException("Le chapitre ne peut dépasser 4 caractères");
		}
		if (primArticle() != null && primArticle().length() > 2) {
			throw new NSValidation.ValidationException("L'article ne peut dépasser 2 caractères");
		}
		if (primParagraphe() != null && primParagraphe().length() > 2) {
			throw new NSValidation.ValidationException("Le paragraphe ne peut dépasser 2 caractères");
		}
		if (primLibCourt() == null) {
			throw new NSValidation.ValidationException("Vous devez fournir un libellé court");
		} else if (primLibCourt().length() > 20) {
			throw new NSValidation.ValidationException("Le libellé court ne peut dépasser 20 caractères");
		}
		if (primLibEdition() != null && primLibEdition().length() > 200) {
			throw new NSValidation.ValidationException("Le libellé pour édition ne peut dépasser 200 caractères");
		}
		if (primLibLong() != null && primLibLong().length() > 80) {
			throw new NSValidation.ValidationException("Le libellé long ne peut dépasser 80 caractères");
		}
		if (pcoNum() != null) {
			EOPlanComptable plan = EOPlanComptable.rechercherPlanAvecNum(editingContext(), pcoNum());
			if (plan == null) {
				throw new NSValidation.ValidationException("Valeur invalide pour l'imputation établissement");
			}
		}
		if (primRlr() != null && primRlr().length() > 10) {
			throw new NSValidation.ValidationException("La référence ne peut dépasser 10 caractères");
		}
		if (primTypePopulation() != null && primTypePopulation().equals(type(PRIME_POUR_ENSEIGNANT)) == false &&
				primTypePopulation().equals(type(PRIME_POUR_IATOS)) == false && primTypePopulation().equals(type(PRIME_POUR_TOUS)) == false) {
			throw new NSValidation.ValidationException("Type de poulation inconnue");
		}
		if (primMoisDemarrage() != null) {
			int mois = primMoisDemarrage().intValue();
			if (mois < 1 || mois > 12) {
				throw new NSValidation.ValidationException("Le mois de démarrage est compris entre 1 et 12");
			}
		}
		if (primPeriodicite() != null && primPeriodicite().equals(type(VERSEMENT_UNIQUE)) == false && 
				primPeriodicite().equals(type(VERSEMENT_ANNUEL)) == false &&
				primPeriodicite().equals(type(VERSEMENT_SEMESTRIEL)) == false && 
				primPeriodicite().equals(type(VERSEMENT_TRIMESTRIEL)) == false &&
				primPeriodicite().equals(type(VERSEMENT_MENSUEL)) == false) {
			throw new NSValidation.ValidationException("Périodicité inconnue");
		}
		if (estAttributionUniqueDansAnnee() == false) {
			if (estIndividuelle() == false) {
				throw new NSValidation.ValidationException("Une prime qui peut être attribuée plusieurs fois dans l'année est nécessairement individuelle");
			}
			if (primPeriodicite() != null && primPeriodicite().equals(type(VERSEMENT_UNIQUE)) == false) {
				throw new NSValidation.ValidationException("Une prime qui peut être attribuée plusieurs fois dans l'année a nécessairement un versement unique");
			}
		}
	}
	
	/** Invalide la prime, les exclusions, les exclusions de cong&eacute;, les fonctions, les populations, les programmes et les structures */
	public void invalider() {
		invalider("PrimeAttribution");
		invalider("PrimeFonction");
		invalider("PrimeExclusion");
		invalider("PrimeProgramme");
		invalider("PrimeExclusionConge");
		invalider("PrimePopulation");
		invalider("PrimeStructure");
		super.invalider();
	}
	public void updateAvecPrime(EOPrime prime) {
		takeValuesFromDictionary(prime.snapshot());

		//setco(null);
		java.util.Enumeration e = prime.codes().objectEnumerator();
		while (e.hasMoreElements()) {
			EOPrimeCode code = (EOPrimeCode)e.nextElement();
			addObjectToBothSidesOfRelationshipWithKey(code, "codes");
		}
	}
	public String libelleAvecCode() {
		return primCodeIndemnite() + " - " + primLibLong();
	}

	/** Retourne les primes valides pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherPrimesValidesPourPeriode(EOEditingContext editingContext,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		return DureeAvecValidite.rechercherObjetsValidesPourPeriode(editingContext, "Prime", debutPeriode, finPeriode);
	}
	/** Retourne les primes invaalides pendant la p&eacute;riode rang&eacute;s par date d&eacute;but d&eacute;croissante
	 * @param editingContext
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 */
	public static NSArray rechercherPrimesInvalidesPourPeriode(EOEditingContext editingContext,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		return DureeAvecValidite.rechercherObjetsInvalidesPourPeriode(editingContext, "Prime", debutPeriode, finPeriode);
	}
	/** Retourne les primes personnalisables pour la p&eacute;riode et la validit&eacute;
	 * @param editingContext
	 * @param estValide (true si on recherche les primes valides)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 * @return
	 */
	public static NSArray rechercherPrimesPersonnalisablesPourValiditeEtPeriode(EOEditingContext editingContext,boolean estValide,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = 'O'", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_PERSO_KEY + " = 'O'", null));
		EOFetchSpecification fs = new EOFetchSpecification("Prime",new EOAndQualifier(qualifiers),null);
		fs.setPrefetchingRelationshipKeyPaths(new NSArray("codes"));
		return editingContext.objectsWithFetchSpecification(fs);
	}
	/** Retourne la prime associ&eacute;e au code Indemnit&eacute; (primCodeIndemnite) pass&eacute; en param&egrave;tre 
	 * @param editingContext
	 * @param code prime indemnit&eacute; (sur 4 car., par exemple 5092)
	 * @param debutPeriode (peut &ecirc;tre nulle)
	 * @param finPeriode (peut &ecirc;tre nulle)
	 * @return prime trouv&eacute;e
	 */
	public static EOPrime rechercherPrimePourCodeIndemnite(EOEditingContext editingContext,String codeIndemnite,NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(TEM_VALIDE_KEY + " = 'O'", null));
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(PRIM_CODE_INDEMNITE_KEY + " = %@", new NSArray(codeIndemnite)));
		EOFetchSpecification fs = new EOFetchSpecification("Prime",new EOAndQualifier(qualifiers),null);
		try {
			return (EOPrime)editingContext.objectsWithFetchSpecification(fs).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}
	//	Méthodes privées
	private String type(String aStr) {
		if (aStr == null || aStr.length() == 0) {
			return null;
		} else {
			return aStr.substring(0,1);
		}
	}
	private void invalider(String nomEntite) {
		NSArray objets = EOPrimeExclusion.rechercherObjetsValidesPourPrime(editingContext(),nomEntite, this);
		java.util.Enumeration e = objets.objectEnumerator();
		while (e.hasMoreElements()) {
			ObjetAvecValidite objet = (ObjetAvecValidite)e.nextElement();
			objet.invalider();
		}
	}

	public String libelle() {
		return primCodeIndemnite() + " - " + primLibCourt();
	}

	public String code() {
		// TODO Auto-generated method stub
		return primCode();
	}


	public String libelleCourt() {
		// TODO Auto-generated method stub
		return primLibCourt();
	}


	public String libelleLong() {
		// TODO Auto-generated method stub
		return primLibLong();
	}


	public String codeEtLibelle() {
		// TODO Auto-generated method stub
		return code() + " - " + libelle();
	}

}

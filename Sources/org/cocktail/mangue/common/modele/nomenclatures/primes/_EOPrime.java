// _EOPrime.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrime.java instead.
package org.cocktail.mangue.common.modele.nomenclatures.primes;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.mangue.modele.goyave.DureeAvecValidite;

import com.webobjects.eocontrol.EOGenericRecord;

public abstract class _EOPrime extends  DureeAvecValidite {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Prime";
	public static final String ENTITY_TABLE_NAME = "GOYAVE.PRIME";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "primOrdre";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEBUT_VALIDITE_KEY = "debutValidite";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FIN_VALIDITE_KEY = "finValidite";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PRIM_ARTICLE_KEY = "primArticle";
	public static final String PRIM_CHAPITRE_KEY = "primChapitre";
	public static final String PRIM_CODE_KEY = "primCode";
	public static final String PRIM_CODE_COMPTE_PCE_KEY = "primCodeComptePce";
	public static final String PRIM_CODE_INDEMNITE_KEY = "primCodeIndemnite";
	public static final String PRIM_CODE_RUBRIQUE_TG_KEY = "primCodeRubriqueTg";
	public static final String PRIM_COMMENTAIRE_KEY = "primCommentaire";
	public static final String PRIM_DECRET_KEY = "primDecret";
	public static final String PRIM_LIB_COURT_KEY = "primLibCourt";
	public static final String PRIM_LIB_EDITION_KEY = "primLibEdition";
	public static final String PRIM_LIB_LONG_KEY = "primLibLong";
	public static final String PRIM_MOIS_DEMARRAGE_KEY = "primMoisDemarrage";
	public static final String PRIM_NOM_CLASSE_KEY = "primNomClasse";
	public static final String PRIM_PARAGRAPHE_KEY = "primParagraphe";
	public static final String PRIM_PERIODICITE_KEY = "primPeriodicite";
	public static final String PRIM_RLR_KEY = "primRlr";
	public static final String PRIM_TEM_INDIVIDUELLE_KEY = "primTemIndividuelle";
	public static final String PRIM_TEM_MULTIPLE_KEY = "primTemMultiple";
	public static final String PRIM_TYPE_POPULATION_KEY = "primTypePopulation";
	public static final String PRIM_VERIF_CONTRAT_KEY = "primVerifContrat";
	public static final String PRIM_VERIF_CORPS_KEY = "primVerifCorps";
	public static final String PRIM_VERIF_ECHELON_KEY = "primVerifEchelon";
	public static final String PRIM_VERIF_EXCLUSION_KEY = "primVerifExclusion";
	public static final String PRIM_VERIF_FONCTION_KEY = "primVerifFonction";
	public static final String PRIM_VERIF_GRADE_KEY = "primVerifGrade";
	public static final String PRIM_VERIF_INDICE_KEY = "primVerifIndice";
	public static final String PRIM_VERIF_POPULATION_KEY = "primVerifPopulation";
	public static final String PRIM_VERIF_POSITION_KEY = "primVerifPosition";
	public static final String PRIM_VERIF_SPECIALITE_KEY = "primVerifSpecialite";
	public static final String PRIM_VERIF_STRUCTURE_KEY = "primVerifStructure";
	public static final String TEM_LOCAL_KEY = "temLocal";
	public static final String TEM_PENSION_KEY = "temPension";
	public static final String TEM_PERSO_KEY = "temPerso";
	public static final String TEM_PRORATISATION_KEY = "temProratisation";
	public static final String TEM_RETENUE_KEY = "temRetenue";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String PRIM_ORDRE_KEY = "primOrdre";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DEBUT_VALIDITE_COLKEY = "PRIM_DEB_VALIDITE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String FIN_VALIDITE_COLKEY = "PRIM_FIN_VALIDITE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PRIM_ARTICLE_COLKEY = "PRIM_ARTICLE";
	public static final String PRIM_CHAPITRE_COLKEY = "PRIM_CHAPITRE";
	public static final String PRIM_CODE_COLKEY = "PRIM_CODE";
	public static final String PRIM_CODE_COMPTE_PCE_COLKEY = "PRIM_CODE_COMPTE_PCE";
	public static final String PRIM_CODE_INDEMNITE_COLKEY = "PRIM_CODE_INDEMNITE";
	public static final String PRIM_CODE_RUBRIQUE_TG_COLKEY = "PRIM_CODE_RUBRIQUE_TG";
	public static final String PRIM_COMMENTAIRE_COLKEY = "PRIM_COMMENTAIRE";
	public static final String PRIM_DECRET_COLKEY = "PRIM_DECRET";
	public static final String PRIM_LIB_COURT_COLKEY = "PRIM_LIB_COURT";
	public static final String PRIM_LIB_EDITION_COLKEY = "PRIM_LIB_EDITION";
	public static final String PRIM_LIB_LONG_COLKEY = "PRIM_LIB_LONG";
	public static final String PRIM_MOIS_DEMARRAGE_COLKEY = "PRIM_MOIS_DEMARRAGE";
	public static final String PRIM_NOM_CLASSE_COLKEY = "PRIM_NOM_CLASSE";
	public static final String PRIM_PARAGRAPHE_COLKEY = "PRIM_PARAGRAPHE";
	public static final String PRIM_PERIODICITE_COLKEY = "PRIM_PERIODICITE";
	public static final String PRIM_RLR_COLKEY = "PRIM_RLR";
	public static final String PRIM_TEM_INDIVIDUELLE_COLKEY = "PRIM_TEM_INDIVIDUELLE";
	public static final String PRIM_TEM_MULTIPLE_COLKEY = "PRIM_TEM_MULTIPLE";
	public static final String PRIM_TYPE_POPULATION_COLKEY = "PRIM_TYPE_POPULATION";
	public static final String PRIM_VERIF_CONTRAT_COLKEY = "PRIM_VERIF_CONTRAT";
	public static final String PRIM_VERIF_CORPS_COLKEY = "PRIM_VERIF_CORPS";
	public static final String PRIM_VERIF_ECHELON_COLKEY = "PRIM_VERIF_ECHELON";
	public static final String PRIM_VERIF_EXCLUSION_COLKEY = "PRIM_VERIF_EXCLUSION";
	public static final String PRIM_VERIF_FONCTION_COLKEY = "PRIM_VERIF_FONCTION";
	public static final String PRIM_VERIF_GRADE_COLKEY = "PRIM_VERIF_GRADE";
	public static final String PRIM_VERIF_INDICE_COLKEY = "PRIM_VERIF_INDICE";
	public static final String PRIM_VERIF_POPULATION_COLKEY = "PRIM_VERIF_POPULATION";
	public static final String PRIM_VERIF_POSITION_COLKEY = "PRIM_VERIF_POSITION";
	public static final String PRIM_VERIF_SPECIALITE_COLKEY = "PRIM_VERIF_SPECIALITE";
	public static final String PRIM_VERIF_STRUCTURE_COLKEY = "PRIM_VERIF_STRUCTURE";
	public static final String TEM_LOCAL_COLKEY = "PRIM_TEM_LOCAL";
	public static final String TEM_PENSION_COLKEY = "PRIM_TEM_PENSION";
	public static final String TEM_PERSO_COLKEY = "PRIM_TEM_PERSO";
	public static final String TEM_PRORATISATION_COLKEY = "PRIM_TEM_PRORATISATION";
	public static final String TEM_RETENUE_COLKEY = "PRIM_TEM_RETENUE";
	public static final String TEM_VALIDE_COLKEY = "PRIM_TEM_VALIDE";

	public static final String PRIM_ORDRE_COLKEY = "PRIM_ORDRE";


	// Relationships
	public static final String CODES_KEY = "codes";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp debutValidite() {
    return (NSTimestamp) storedValueForKey(DEBUT_VALIDITE_KEY);
  }

  public void setDebutValidite(NSTimestamp value) {
    takeStoredValueForKey(value, DEBUT_VALIDITE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp finValidite() {
    return (NSTimestamp) storedValueForKey(FIN_VALIDITE_KEY);
  }

  public void setFinValidite(NSTimestamp value) {
    takeStoredValueForKey(value, FIN_VALIDITE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public String primArticle() {
    return (String) storedValueForKey(PRIM_ARTICLE_KEY);
  }

  public void setPrimArticle(String value) {
    takeStoredValueForKey(value, PRIM_ARTICLE_KEY);
  }

  public String primChapitre() {
    return (String) storedValueForKey(PRIM_CHAPITRE_KEY);
  }

  public void setPrimChapitre(String value) {
    takeStoredValueForKey(value, PRIM_CHAPITRE_KEY);
  }

  public String primCode() {
    return (String) storedValueForKey(PRIM_CODE_KEY);
  }

  public void setPrimCode(String value) {
    takeStoredValueForKey(value, PRIM_CODE_KEY);
  }

  public String primCodeComptePce() {
    return (String) storedValueForKey(PRIM_CODE_COMPTE_PCE_KEY);
  }

  public void setPrimCodeComptePce(String value) {
    takeStoredValueForKey(value, PRIM_CODE_COMPTE_PCE_KEY);
  }

  public String primCodeIndemnite() {
    return (String) storedValueForKey(PRIM_CODE_INDEMNITE_KEY);
  }

  public void setPrimCodeIndemnite(String value) {
    takeStoredValueForKey(value, PRIM_CODE_INDEMNITE_KEY);
  }

  public String primCodeRubriqueTg() {
    return (String) storedValueForKey(PRIM_CODE_RUBRIQUE_TG_KEY);
  }

  public void setPrimCodeRubriqueTg(String value) {
    takeStoredValueForKey(value, PRIM_CODE_RUBRIQUE_TG_KEY);
  }

  public String primCommentaire() {
    return (String) storedValueForKey(PRIM_COMMENTAIRE_KEY);
  }

  public void setPrimCommentaire(String value) {
    takeStoredValueForKey(value, PRIM_COMMENTAIRE_KEY);
  }

  public String primDecret() {
    return (String) storedValueForKey(PRIM_DECRET_KEY);
  }

  public void setPrimDecret(String value) {
    takeStoredValueForKey(value, PRIM_DECRET_KEY);
  }

  public String primLibCourt() {
    return (String) storedValueForKey(PRIM_LIB_COURT_KEY);
  }

  public void setPrimLibCourt(String value) {
    takeStoredValueForKey(value, PRIM_LIB_COURT_KEY);
  }

  public String primLibEdition() {
    return (String) storedValueForKey(PRIM_LIB_EDITION_KEY);
  }

  public void setPrimLibEdition(String value) {
    takeStoredValueForKey(value, PRIM_LIB_EDITION_KEY);
  }

  public String primLibLong() {
    return (String) storedValueForKey(PRIM_LIB_LONG_KEY);
  }

  public void setPrimLibLong(String value) {
    takeStoredValueForKey(value, PRIM_LIB_LONG_KEY);
  }

  public Integer primMoisDemarrage() {
    return (Integer) storedValueForKey(PRIM_MOIS_DEMARRAGE_KEY);
  }

  public void setPrimMoisDemarrage(Integer value) {
    takeStoredValueForKey(value, PRIM_MOIS_DEMARRAGE_KEY);
  }

  public String primNomClasse() {
    return (String) storedValueForKey(PRIM_NOM_CLASSE_KEY);
  }

  public void setPrimNomClasse(String value) {
    takeStoredValueForKey(value, PRIM_NOM_CLASSE_KEY);
  }

  public String primParagraphe() {
    return (String) storedValueForKey(PRIM_PARAGRAPHE_KEY);
  }

  public void setPrimParagraphe(String value) {
    takeStoredValueForKey(value, PRIM_PARAGRAPHE_KEY);
  }

  public String primPeriodicite() {
    return (String) storedValueForKey(PRIM_PERIODICITE_KEY);
  }

  public void setPrimPeriodicite(String value) {
    takeStoredValueForKey(value, PRIM_PERIODICITE_KEY);
  }

  public String primRlr() {
    return (String) storedValueForKey(PRIM_RLR_KEY);
  }

  public void setPrimRlr(String value) {
    takeStoredValueForKey(value, PRIM_RLR_KEY);
  }

  public String primTemIndividuelle() {
    return (String) storedValueForKey(PRIM_TEM_INDIVIDUELLE_KEY);
  }

  public void setPrimTemIndividuelle(String value) {
    takeStoredValueForKey(value, PRIM_TEM_INDIVIDUELLE_KEY);
  }

  public String primTemMultiple() {
    return (String) storedValueForKey(PRIM_TEM_MULTIPLE_KEY);
  }

  public void setPrimTemMultiple(String value) {
    takeStoredValueForKey(value, PRIM_TEM_MULTIPLE_KEY);
  }

  public String primTypePopulation() {
    return (String) storedValueForKey(PRIM_TYPE_POPULATION_KEY);
  }

  public void setPrimTypePopulation(String value) {
    takeStoredValueForKey(value, PRIM_TYPE_POPULATION_KEY);
  }

  public String primVerifContrat() {
    return (String) storedValueForKey(PRIM_VERIF_CONTRAT_KEY);
  }

  public void setPrimVerifContrat(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_CONTRAT_KEY);
  }

  public String primVerifCorps() {
    return (String) storedValueForKey(PRIM_VERIF_CORPS_KEY);
  }

  public void setPrimVerifCorps(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_CORPS_KEY);
  }

  public String primVerifEchelon() {
    return (String) storedValueForKey(PRIM_VERIF_ECHELON_KEY);
  }

  public void setPrimVerifEchelon(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_ECHELON_KEY);
  }

  public String primVerifExclusion() {
    return (String) storedValueForKey(PRIM_VERIF_EXCLUSION_KEY);
  }

  public void setPrimVerifExclusion(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_EXCLUSION_KEY);
  }

  public String primVerifFonction() {
    return (String) storedValueForKey(PRIM_VERIF_FONCTION_KEY);
  }

  public void setPrimVerifFonction(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_FONCTION_KEY);
  }

  public String primVerifGrade() {
    return (String) storedValueForKey(PRIM_VERIF_GRADE_KEY);
  }

  public void setPrimVerifGrade(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_GRADE_KEY);
  }

  public String primVerifIndice() {
    return (String) storedValueForKey(PRIM_VERIF_INDICE_KEY);
  }

  public void setPrimVerifIndice(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_INDICE_KEY);
  }

  public String primVerifPopulation() {
    return (String) storedValueForKey(PRIM_VERIF_POPULATION_KEY);
  }

  public void setPrimVerifPopulation(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_POPULATION_KEY);
  }

  public String primVerifPosition() {
    return (String) storedValueForKey(PRIM_VERIF_POSITION_KEY);
  }

  public void setPrimVerifPosition(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_POSITION_KEY);
  }

  public String primVerifSpecialite() {
    return (String) storedValueForKey(PRIM_VERIF_SPECIALITE_KEY);
  }

  public void setPrimVerifSpecialite(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_SPECIALITE_KEY);
  }

  public String primVerifStructure() {
    return (String) storedValueForKey(PRIM_VERIF_STRUCTURE_KEY);
  }

  public void setPrimVerifStructure(String value) {
    takeStoredValueForKey(value, PRIM_VERIF_STRUCTURE_KEY);
  }

  public String temLocal() {
    return (String) storedValueForKey(TEM_LOCAL_KEY);
  }

  public void setTemLocal(String value) {
    takeStoredValueForKey(value, TEM_LOCAL_KEY);
  }

  public String temPension() {
    return (String) storedValueForKey(TEM_PENSION_KEY);
  }

  public void setTemPension(String value) {
    takeStoredValueForKey(value, TEM_PENSION_KEY);
  }

  public String temPerso() {
    return (String) storedValueForKey(TEM_PERSO_KEY);
  }

  public void setTemPerso(String value) {
    takeStoredValueForKey(value, TEM_PERSO_KEY);
  }

  public String temProratisation() {
    return (String) storedValueForKey(TEM_PRORATISATION_KEY);
  }

  public void setTemProratisation(String value) {
    takeStoredValueForKey(value, TEM_PRORATISATION_KEY);
  }

  public String temRetenue() {
    return (String) storedValueForKey(TEM_RETENUE_KEY);
  }

  public void setTemRetenue(String value) {
    takeStoredValueForKey(value, TEM_RETENUE_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public NSArray codes() {
    return (NSArray)storedValueForKey(CODES_KEY);
  }

  public NSArray codes(EOQualifier qualifier) {
    return codes(qualifier, null);
  }

  public NSArray codes(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = codes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToCodesRelationship(org.cocktail.mangue.modele.goyave.EOPrimeCode object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CODES_KEY);
  }

  public void removeFromCodesRelationship(org.cocktail.mangue.modele.goyave.EOPrimeCode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODES_KEY);
  }

  public org.cocktail.mangue.modele.goyave.EOPrimeCode createCodesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PrimeCode");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CODES_KEY);
    return (org.cocktail.mangue.modele.goyave.EOPrimeCode) eo;
  }

  public void deleteCodesRelationship(org.cocktail.mangue.modele.goyave.EOPrimeCode object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCodesRelationships() {
    Enumeration objects = codes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCodesRelationship((org.cocktail.mangue.modele.goyave.EOPrimeCode)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPrime avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrime createEOPrime(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp debutValidite
, NSTimestamp dModification
, String primCode
, String primLibCourt
			) {
    EOPrime eo = (EOPrime) createAndInsertInstance(editingContext, _EOPrime.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDebutValidite(debutValidite);
		eo.setDModification(dModification);
		eo.setPrimCode(primCode);
		eo.setPrimLibCourt(primLibCourt);
    return eo;
  }

  
	  public EOPrime localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrime)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrime creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrime creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPrime object = (EOPrime)createAndInsertInstance(editingContext, _EOPrime.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrime localInstanceIn(EOEditingContext editingContext, EOPrime eo) {
    EOPrime localInstance = (eo == null) ? null : (EOPrime)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrime#localInstanceIn a la place.
   */
	public static EOPrime localInstanceOf(EOEditingContext editingContext, EOPrime eo) {
		return EOPrime.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrime fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrime fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrime eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrime)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrime fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrime fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrime eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrime)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrime fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrime eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrime ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrime fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

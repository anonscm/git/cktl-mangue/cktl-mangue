// _EONatureBonifTaux.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONatureBonifTaux.java instead.
package org.cocktail.mangue.common.modele.nomenclatures.cir;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public abstract class _EONatureBonifTaux extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "NatureBonifTaux";
	public static final String ENTITY_TABLE_NAME = "GRHUM.NATURE_BONIF_TAUX";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "nbtOrdre";

	public static final String NBT_CODE_KEY = "nbtCode";
	public static final String NBT_DEN_TAUX_KEY = "nbtDenTaux";
	public static final String NBT_LIBELLE_KEY = "nbtLibelle";
	public static final String NBT_NUM_TAUX_KEY = "nbtNumTaux";
	public static final String NBT_ORDRE_KEY = "nbtOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String NBT_CODE_COLKEY = "NBT_CODE";
	public static final String NBT_DEN_TAUX_COLKEY = "NBT_DEN_TAUX";
	public static final String NBT_LIBELLE_COLKEY = "NBT_LIBELLE";
	public static final String NBT_NUM_TAUX_COLKEY = "NBT_NUM_TAUX";
	public static final String NBT_ORDRE_COLKEY = "NBT_ORDRE";



	// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String nbtCode() {
    return (String) storedValueForKey(NBT_CODE_KEY);
  }

  public void setNbtCode(String value) {
    takeStoredValueForKey(value, NBT_CODE_KEY);
  }

  public Integer nbtDenTaux() {
    return (Integer) storedValueForKey(NBT_DEN_TAUX_KEY);
  }

  public void setNbtDenTaux(Integer value) {
    takeStoredValueForKey(value, NBT_DEN_TAUX_KEY);
  }

  public String nbtLibelle() {
    return (String) storedValueForKey(NBT_LIBELLE_KEY);
  }

  public void setNbtLibelle(String value) {
    takeStoredValueForKey(value, NBT_LIBELLE_KEY);
  }

  public Integer nbtNumTaux() {
    return (Integer) storedValueForKey(NBT_NUM_TAUX_KEY);
  }

  public void setNbtNumTaux(Integer value) {
    takeStoredValueForKey(value, NBT_NUM_TAUX_KEY);
  }

  public Integer nbtOrdre() {
    return (Integer) storedValueForKey(NBT_ORDRE_KEY);
  }

  public void setNbtOrdre(Integer value) {
    takeStoredValueForKey(value, NBT_ORDRE_KEY);
  }


/**
 * Créer une instance de EONatureBonifTaux avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EONatureBonifTaux createEONatureBonifTaux(EOEditingContext editingContext, String nbtCode
, Integer nbtDenTaux
, Integer nbtNumTaux
, Integer nbtOrdre
			) {
    EONatureBonifTaux eo = (EONatureBonifTaux) createAndInsertInstance(editingContext, _EONatureBonifTaux.ENTITY_NAME);    
		eo.setNbtCode(nbtCode);
		eo.setNbtDenTaux(nbtDenTaux);
		eo.setNbtNumTaux(nbtNumTaux);
		eo.setNbtOrdre(nbtOrdre);
    return eo;
  }

  
	  public EONatureBonifTaux localInstanceIn(EOEditingContext editingContext) {
	  		return (EONatureBonifTaux)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONatureBonifTaux creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONatureBonifTaux creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EONatureBonifTaux object = (EONatureBonifTaux)createAndInsertInstance(editingContext, _EONatureBonifTaux.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EONatureBonifTaux localInstanceIn(EOEditingContext editingContext, EONatureBonifTaux eo) {
    EONatureBonifTaux localInstance = (eo == null) ? null : (EONatureBonifTaux)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EONatureBonifTaux#localInstanceIn a la place.
   */
	public static EONatureBonifTaux localInstanceOf(EOEditingContext editingContext, EONatureBonifTaux eo) {
		return EONatureBonifTaux.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EONatureBonifTaux fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EONatureBonifTaux fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EONatureBonifTaux eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONatureBonifTaux)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EONatureBonifTaux fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EONatureBonifTaux fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONatureBonifTaux eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONatureBonifTaux)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EONatureBonifTaux fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONatureBonifTaux eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONatureBonifTaux ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EONatureBonifTaux fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

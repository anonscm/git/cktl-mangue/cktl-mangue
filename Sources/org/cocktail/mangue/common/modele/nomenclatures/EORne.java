//EORne.java
//Created on Mon Feb 10 21:08:00  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures;


import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.NumberCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EORne extends _EORne {
	
	public EORne() {
		super();
	}	
	
	/**
	 * 
	 * @param ec
	 * @return
	 */
	public static EORne creer(EOEditingContext ec) {
		
		EORne newObject = (EORne) createAndInsertInstance(ec, ENTITY_NAME);
		newObject.setDCreation(new NSTimestamp());
		newObject.setEtabStatut(ManGUEConstantes.STATUT_RNE_PUBLIC);		

		return newObject;
	}

	/**
	 * 
	 * @param ec
	 * @param code
	 * @param libelle
	 * @return
	 */
	public static NSArray<EORne> rechercherRne(EOEditingContext ec, String code, String libelle)	{

			NSMutableArray qualifiers = new NSMutableArray();

			if (code != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + " caseInsensitiveLike %@", 
						new NSArray("*"+code+"*")));

			if (libelle != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(LIBELLE_LONG_KEY + " caseInsensitiveLike %@", 
						new NSArray("*"+libelle+"*")));

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_LIBELLE);			
	}
	
	/**
	 * 
	 * @return
	 */
	public String siretCir() {
		
		if (siret() != null && NumberCtrl.isANumber(siret()) )
				return siret();

		return null;
		
	}

	public String affectationCir() {

		String aff = libelleLong();

		if (aff.length() >= 25) {
			return aff.substring(0,24);
		}
		return aff;

	}
		
	@Override
    public String toString() {
    	return codeEtLibelle();
    }
	
	/** Tests de coherence pour l'enregistrement d'un individu */
	public void validateForSave () throws com.webobjects.foundation.NSValidation.ValidationException {
		
		super.validateForSave();
		
		if (dModification() == null && NomenclatureFinder.isNomenclatureExistante(new EOEditingContext(), EORne.ENTITY_NAME, code())) {
			throw new NSValidation.ValidationException("Ce code est déjà utilisé !");							
		}

		if (code().length() != 8) {
			throw new NSValidation.ValidationException("Le code UAI doit être de 8 caractères !");
		}
		
		setDModification(new NSTimestamp());
	}
}
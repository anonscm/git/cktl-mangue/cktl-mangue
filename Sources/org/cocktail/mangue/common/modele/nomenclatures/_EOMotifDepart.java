// _EOMotifDepart.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMotifDepart.java instead.
package org.cocktail.mangue.common.modele.nomenclatures;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



import com.webobjects.eocontrol.EOGenericRecord;

public abstract class _EOMotifDepart extends  NomenclatureAvecDate {

	private static final long serialVersionUID = -3258666968396815005L;


	public static final String ENTITY_NAME = "MotifDepart";
	public static final String ENTITY_TABLE_NAME = "GRHUM.MOTIF_DEPART";

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "code";

	public static final String C_MOTIF_DEPART_ONP_KEY = "cMotifDepartOnp";
	public static final String TEM_ENS_KEY = "temEns";
	public static final String TEM_FIN_CARRIERE_KEY = "temFinCarriere";
	public static final String TEM_RNE_LIEU_KEY = "temRneLieu";
	public static final String TEM_DECES_KEY = "temDeces";

	// Attributs non visibles
	//Colonnes dans la base de donnees
	public static final String C_MOTIF_DEPART_ONP_COLKEY = "C_MOTIF_DEPART_ONP";
	public static final String TEM_ENS_COLKEY = "TEM_ENS";
	public static final String TEM_FIN_CARRIERE_COLKEY = "TEM_FIN_CARRIERE";
	public static final String TEM_RNE_LIEU_COLKEY = "TEM_RNE_LIEU";
	public static final String TEM_DECES_COLKEY = "TEM_DECES";

	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}

	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}
	public String cMotifDepartOnp() {
		return (String) storedValueForKey(C_MOTIF_DEPART_ONP_KEY);
	}

	public void setCMotifDepartOnp(String value) {
		takeStoredValueForKey(value, C_MOTIF_DEPART_ONP_KEY);
	}
	public String temEns() {
		return (String) storedValueForKey(TEM_ENS_KEY);
	}

	public void setTemEns(String value) {
		takeStoredValueForKey(value, TEM_ENS_KEY);
	}

	public String temFinCarriere() {
		return (String) storedValueForKey(TEM_FIN_CARRIERE_KEY);
	}

	public void setTemFinCarriere(String value) {
		takeStoredValueForKey(value, TEM_FIN_CARRIERE_KEY);
	}

	public String temRneLieu() {
		return (String) storedValueForKey(TEM_RNE_LIEU_KEY);
	}

	public void setTemRneLieu(String value) {
		takeStoredValueForKey(value, TEM_RNE_LIEU_KEY);
	}

	public String temDeces() {
		return (String) storedValueForKey(TEM_DECES_KEY);
	}

	public void setTemDeces(String value) {
		takeStoredValueForKey(value, TEM_DECES_KEY);
	}

	public EOMotifDepart localInstanceIn(EOEditingContext editingContext) {
		return (EOMotifDepart)localInstanceOfObject(editingContext, this);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOMotifDepart creerInstance(EOEditingContext editingContext) {
		return creerInstance(editingContext, null);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOMotifDepart creerInstance(EOEditingContext editingContext, NSArray specificites) {
		EOMotifDepart object = (EOMotifDepart)createAndInsertInstance(editingContext, _EOMotifDepart.ENTITY_NAME, specificites);
		return object;
	}



	public static EOMotifDepart localInstanceIn(EOEditingContext editingContext, EOMotifDepart eo) {
		EOMotifDepart localInstance = (eo == null) ? null : (EOMotifDepart)localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOMotifDepart#localInstanceIn a la place.
	 */
	public static EOMotifDepart localInstanceOf(EOEditingContext editingContext, EOMotifDepart eo) {
		return EOMotifDepart.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	public static NSArray fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier)null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false, null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
		return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		if (prefetchs != null)
			fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
		NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOMotifDepart fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMotifDepart fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOMotifDepart eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOMotifDepart)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}




	public static EOMotifDepart fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOMotifDepart fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOMotifDepart eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOMotifDepart)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOMotifDepart fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOMotifDepart eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOMotifDepart ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	


	public static EOMotifDepart fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}

// EOSituationFamiliale.java
// Created on Wed Feb 12 15:34:59  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures;


import org.cocktail.application.common.utilities.CocktailFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EOSituationFamiliale extends _EOSituationFamiliale {
	
	public final static String CELIBATAIRE = "C";
	public final static String MARIE = "M";
	public final static String VEUF = "V";
	public final static String DIVORCE = "D";
	public final static String SEPARE = "S";
	public final static String PACS = "P";
	public final static String CONCUBINAGE = "U";
	public final static String NON_PRECISEE = "X";
    public EOSituationFamiliale() {
        super();
    }    
	public static NSArray<EOSituationFamiliale> fetch(EOEditingContext edc, String code)	{
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		if (code != null)
			andQualifiers.addObject(CocktailFinder.getQualifierEqual(CODE_KEY, code));
		return fetchAll(edc, new EOAndQualifier(andQualifiers),SORT_ARRAY_LIBELLE);			
	}

	public static EOSituationFamiliale getDefault(EOEditingContext edc)	{
		return fetchFirstByQualifier(edc, CocktailFinder.getQualifierEqual(CODE_KEY, NON_PRECISEE));
	}
	public static EOSituationFamiliale getSituationCelibataire(EOEditingContext ec)	{
		return fetchFirstByQualifier(ec, CocktailFinder.getQualifierEqual(CODE_KEY, CELIBATAIRE));
	}
	public static EOSituationFamiliale getForCode(EOEditingContext edc, String code)	{
		EOQualifier qualifier =  EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@", new NSArray(code));
		return fetchFirstByQualifier(edc, qualifier);
	}

    public String toString() {
    	return libelleLong();
    }
    public String libelleCourt() {
		return libelleLong();
    }
    public boolean estCelibataire() {
    	return code().equals(CELIBATAIRE);
    }
    public String libelle() {
    	return libelleLong();
    }
}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOKxElement.java instead.
package org.cocktail.mangue.common.modele.nomenclatures.paye;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;


public abstract class _EOKxElement extends Nomenclature {
	public static final String ENTITY_NAME = "KxElement";
	public static final String ENTITY_TABLE_NAME = "jefy_paf.KX_ELEMENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "kelmId";

	public static final String C_CAISSE_KEY = "cCaisse";
	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_NATURE_KEY = "cNature";
	public static final String CODE_KEY = "code";
	public static final String C_PERIODICITE_KEY = "cPeriodicite";
	public static final String LIBELLE_COURT_KEY = "libelleCourt";
	public static final String LIBELLE_LONG_KEY = "libelleLong";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String KELM_ID_KEY = "kelmId";
	public static final String MOIS_CODE_DEBUT_KEY = "moisCodeDebut";
	public static final String MOIS_CODE_FIN_KEY = "moisCodeFin";

//Colonnes dans la base de donnees
	public static final String C_CAISSE_COLKEY = "C_CAISSE";
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_NATURE_COLKEY = "C_NATURE";
	public static final String CODE_COLKEY = "IDELT";
	public static final String C_PERIODICITE_COLKEY = "C_PERIODICITE";
	public static final String LIBELLE_COURT_COLKEY = "LC_ELEMENT";
	public static final String LIBELLE_LONG_COLKEY = "L_ELEMENT";
	public static final String TEM_VALIDE_COLKEY = "tem_Valide";

	public static final String KELM_ID_COLKEY = "KELM_ID";
	public static final String MOIS_CODE_DEBUT_COLKEY = "MOIS_CODE_DEBUT";
	public static final String MOIS_CODE_FIN_COLKEY = "MOIS_CODE_FIN";


	// Relationships
	public static final String TO_MOIS_DEBUT_KEY = "toMoisDebut";
	public static final String TO_MOIS_FIN_KEY = "toMoisFin";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cCaisse() {
    return (String) storedValueForKey(C_CAISSE_KEY);
  }

  public void setCCaisse(String value) {
    takeStoredValueForKey(value, C_CAISSE_KEY);
  }

  public String cCategorie() {
    return (String) storedValueForKey(C_CATEGORIE_KEY);
  }

  public void setCCategorie(String value) {
    takeStoredValueForKey(value, C_CATEGORIE_KEY);
  }

  public String cNature() {
    return (String) storedValueForKey(C_NATURE_KEY);
  }

  public void setCNature(String value) {
    takeStoredValueForKey(value, C_NATURE_KEY);
  }

  public String code() {
    return (String) storedValueForKey(CODE_KEY);
  }

  public void setCode(String value) {
    takeStoredValueForKey(value, CODE_KEY);
  }

  public Integer cPeriodicite() {
    return (Integer) storedValueForKey(C_PERIODICITE_KEY);
  }

  public void setCPeriodicite(Integer value) {
    takeStoredValueForKey(value, C_PERIODICITE_KEY);
  }

  public String libelleCourt() {
    return (String) storedValueForKey(LIBELLE_COURT_KEY);
  }

  public void setLibelleCourt(String value) {
    takeStoredValueForKey(value, LIBELLE_COURT_KEY);
  }

  public String libelleLong() {
    return (String) storedValueForKey(LIBELLE_LONG_KEY);
  }

  public void setLibelleLong(String value) {
    takeStoredValueForKey(value, LIBELLE_LONG_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois toMoisDebut() {
    return (org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois)storedValueForKey(TO_MOIS_DEBUT_KEY);
  }

  public void setToMoisDebutRelationship(org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois oldValue = toMoisDebut();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MOIS_DEBUT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MOIS_DEBUT_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois toMoisFin() {
    return (org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois)storedValueForKey(TO_MOIS_FIN_KEY);
  }

  public void setToMoisFinRelationship(org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois oldValue = toMoisFin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MOIS_FIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MOIS_FIN_KEY);
    }
  }
  

/**
 * Créer une instance de EOKxElement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOKxElement createEOKxElement(EOEditingContext editingContext, String code
, Integer cPeriodicite
, String temValide
, org.cocktail.mangue.common.modele.nomenclatures.paye.EOMois toMoisDebut			) {
    EOKxElement eo = (EOKxElement) createAndInsertInstance(editingContext, _EOKxElement.ENTITY_NAME);    
		eo.setCode(code);
		eo.setCPeriodicite(cPeriodicite);
		eo.setTemValide(temValide);
    eo.setToMoisDebutRelationship(toMoisDebut);
    return eo;
  }

  
	  public EOKxElement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOKxElement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOKxElement creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOKxElement creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOKxElement object = (EOKxElement)createAndInsertInstance(editingContext, _EOKxElement.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOKxElement localInstanceIn(EOEditingContext editingContext, EOKxElement eo) {
    EOKxElement localInstance = (eo == null) ? null : (EOKxElement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOKxElement#localInstanceIn a la place.
   */
	public static EOKxElement localInstanceOf(EOEditingContext editingContext, EOKxElement eo) {
		return EOKxElement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOKxElement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOKxElement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOKxElement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOKxElement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOKxElement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOKxElement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOKxElement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOKxElement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOKxElement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOKxElement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOKxElement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOKxElement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

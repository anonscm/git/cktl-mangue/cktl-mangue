/*
 * Created on 25 janv. 2006
 *
 * Durée avec une relation sur la classe EOAbscences
 */
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures;

import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.utilities.CocktailMessagesErreur;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public abstract class NomenclatureAvecDate extends Nomenclature {

	private static final long serialVersionUID = 1L;

    public static final String ERREUR_DATE_OUVERTURE_NON_RENSEIGNE = "Veuillez renseigner une date d'ouverture !";
    public static final String ERREUR_DATE_OUVERTURE_HORS_BORNE = "Date d'ouverture hors bornes  ( >= 01/01/1900)!";
    public static final String ERREUR_DATE_FERMETURE_HORS_BORNE = "Date de fermeture hors bornes !";

	public final static String DATE_OUVERTURE_MINI = "31/12/1899";
	public final static String DATE_FERMETURE_MAXI = "31/12/2050";

	public NomenclatureAvecDate() {
		super();
	}

	public NSTimestamp dateOuverture() {
		return (NSTimestamp) storedValueForKey(DATE_OUVERTURE_KEY);
	}

	public void setDateOuverture(NSTimestamp value) {
		takeStoredValueForKey(value, DATE_OUVERTURE_KEY);
	}

	public NSTimestamp dateFermeture() {
		return (NSTimestamp) storedValueForKey(DATE_FERMETURE_KEY);
	}

	public void setDateFermeture(NSTimestamp value) {
		takeStoredValueForKey(value, DATE_FERMETURE_KEY);
	}

	public String dateOuvertureFormattee() {
		return DateCtrl.dateToString(dateOuverture());
	}
	public String dateFermetureFormattee() {
		return DateCtrl.dateToString(dateFermeture());
	}

	/**
	 * 
	 */
	public void validateForSave() {				

		super.validateForSave();
		
		if (dateOuverture() == null) {
			throw new NSValidation.ValidationException(ERREUR_DATE_OUVERTURE_NON_RENSEIGNE);
		}
		
		if (dateFermeture() != null && dateOuverture() != null && DateCtrl.isBefore(dateFermeture(),dateOuverture())) {
			throw new NSValidation.ValidationException(CocktailMessagesErreur.ERREUR_DATE_FIN_AVANT_DATE_DEBUT);
		}

		if (DateCtrl.isBefore(dateOuverture(), DateCtrl.stringToDate(DATE_OUVERTURE_MINI))) {
			throw new NSValidation.ValidationException(ERREUR_DATE_OUVERTURE_HORS_BORNE);						
		}
		if (dateFermeture() != null && DateCtrl.isAfter(dateFermeture(), DateCtrl.stringToDate(DATE_FERMETURE_MAXI))) {
			throw new NSValidation.ValidationException(ERREUR_DATE_FERMETURE_HORS_BORNE);						
		}

	}

	/**
	 * 
	 * @param ec
	 * @param nomEntite
	 * @return
	 */
	public static NSArray<INomenclature> findForDate(EOEditingContext edc, String entite, NSTimestamp dateReference, NSArray<EOSortOrdering> sorts) {

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(qualifierPourPeriode(dateReference, dateReference));
			EOFetchSpecification fs = new EOFetchSpecification( entite ,new EOAndQualifier(qualifiers), sorts);					
			return edc.objectsWithFetchSpecification(fs);

		} catch (Exception e) {
			return null;
		}
	}

	/** Retourne le qualifier pour determiner les objets valides pour une periode
	 * @param champDateDebut	nom du champ de date debut sur lequel construire le qualifier
	 * @param debutPeriode	date de debut de periode (ne peut pas &ecirc;tre nulle)
	 * @param champDateFin	nom du champ de date fin sur lequel construire le qualifier
	 * @param finPeriode	date de fin de periode
	 * @return qualifier construit ou null si date debut est nulle
	 */
	public static EOQualifier qualifierPourDate(NSTimestamp dateReference) {
		if (dateReference == null)
			return null;
		return qualifierPourPeriode(dateReference, dateReference);
	}
	/** Retourne le qualifier pour determiner les objets valides pour une periode
	 * @param champDateDebut	nom du champ de date debut sur lequel construire le qualifier
	 * @param debutPeriode	date de debut de periode (ne peut pas &ecirc;tre nulle)
	 * @param champDateFin	nom du champ de date fin sur lequel construire le qualifier
	 * @param finPeriode	date de fin de periode
	 * @return qualifier construit ou null si date debut est nulle
	 */
	public static EOQualifier qualifierPourPeriode(NSTimestamp debutPeriode, NSTimestamp finPeriode) {

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_FERMETURE_KEY + " = nil OR " + DATE_FERMETURE_KEY + " >=%@", new NSArray(debutPeriode)));
			if (finPeriode != null)
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(DATE_OUVERTURE_KEY + " = nil OR " + DATE_OUVERTURE_KEY + " <=%@", new NSArray(finPeriode)));

			return new EOAndQualifier(qualifiers);
		}
		catch (Exception e) {
			return null;
		}
	}

}

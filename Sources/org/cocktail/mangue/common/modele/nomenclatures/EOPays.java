// EOPays.java
// Created on Wed Feb 12 15:40:29  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures;



import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
public class EOPays extends _EOPays  {

	public final static String CODE_PAYS_FRANCE = "100";
	public final static String CODE_PAYS_ETRANGER_INCONNU = "999";

	public EOPays() {
		super();
	}

	public String libelle() {
		return libelleLong();
	}
	public boolean isDefault() {
		return CODE_PAYS_FRANCE.equals(code());
	}
	
	public static EOPays getDefault(EOEditingContext ec) {		
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@", new NSArray(CODE_PAYS_FRANCE));
		return fetchFirstByQualifier(ec, qualifier);
	}
	
	public static NSArray<EOPays> fetch(EOEditingContext ec, String code, NSTimestamp dateRef)	{
		NSMutableArray mesQualifiers = new NSMutableArray();
		if (code != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + " caseinsensitivelike %@", new NSArray("*"+code+"*")));
		if (dateRef != null)
			mesQualifiers.addObject(NomenclatureAvecDate.qualifierPourDate(dateRef));

		return fetchAll(ec, new EOAndQualifier(mesQualifiers), SORT_ARRAY_LIBELLE);			
	}
	// Méthodes statiques
	/** Retourne true si le pays correspondant au code pays est valide a la date fournie */
	public static boolean estPaysValide(EOEditingContext editingContext,String cPays) {
		if (cPays == null) {
			return false;
		}
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@",new NSArray(cPays)));
		return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers)) != null;		
	}
	/** Retourne true si le pays correspondant au code pays est valide a la date fournie */
	public static boolean estPaysValideADate(EOEditingContext editingContext,String cPays, NSTimestamp date) {
		if (cPays == null || date == null) {
			return false;
		}
		NSMutableArray qualifiers = new NSMutableArray(EOQualifier.qualifierWithQualifierFormat(CODE_KEY + "=%@",new NSArray(cPays)));
		qualifiers.addObject(NomenclatureAvecDate.qualifierPourDate(date));
		return fetchFirstByQualifier(editingContext, new EOAndQualifier(qualifiers)) != null;		
	}
}


//EOTypeContratTravail.java
//Created on Mon Feb 10 21:08:08  2003 by Apple EOModeler Version 4.5
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures.contrat;


import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.common.utilities.StringCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.NomenclatureAvecDate;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.MangueMessagesErreur;
import org.cocktail.mangue.modele.grhum.EOGestContrat;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOTypeGestionArrete;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOTypeContratTravail extends _EOTypeContratTravail {

	public static Integer NIVEAU_LOCAL = new Integer(2);
	public static Integer NIVEAU_1 = new Integer(1);

	public static String CODE_CDI_SAUVADET = "C3066";
	public static String CODE_POST_DOCTORANT = "CN109";
	public static String CODE_ALLOCATAIRE_RECHERCHE = "AR";

	public EOTypeContratTravail() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public boolean isLocal() {
		return temLocal() != null && temLocal().equals("O");
	}
	public boolean peutBeneficierConges() {
		return droitCongesContrat() != null && droitCongesContrat().equals(CocktailConstantes.VRAI);
	}
	
	public boolean isNiveau1() {
		return cNiveau() != null && NIVEAU_1.equals(cNiveau());
	}

	/**
	 * 
	 */
	private void initValues() {

		setDCreation(new NSTimestamp());
		setPotentielBrut(Double.valueOf(0.0));
		setTemVisible(CocktailConstantes.VRAI);
		setTemEnseignement(CocktailConstantes.FAUX);
		setCNiveau(NIVEAU_LOCAL);
		setTemLocal(CocktailConstantes.VRAI);

	}

	/**
	 * 
	 * @param edc
	 * @return
	 */
	public static EOTypeContratTravail creer(EOEditingContext edc, EOTypeContratTravail typePere) {

		EOTypeContratTravail newObject = new EOTypeContratTravail();    
		if (typePere != null && typePere.cNiveau().intValue() < NIVEAU_LOCAL.intValue()) {
			newObject.setToTypeContratPereRelationship(typePere);
		}
		newObject.initValues();
		edc.insertObject(newObject);
		return newObject;
	}

	/**
	 * 
	 * @param ec
	 * @return
	 */
	public EOTypeContratTravail dupliquer(EOEditingContext ec) {

		EOTypeContratTravail newObject = new EOTypeContratTravail();    

		newObject.initValues();

		newObject.setToTypeContratPereRelationship(toTypeContratPere());
		newObject.setCCategorie(cCategorie());
		newObject.setCode(null);
		newObject.setCodeSupinfo(codeSupinfo());
		newObject.setLibelleCourt(libelleCourt());
		newObject.setLibelleLong(libelleLong());
		newObject.setDateOuverture(dateOuverture());
		newObject.setDateFermeture(dateFermeture());
		newObject.setPotentielBrut(potentielBrut());
		newObject.setDureeInitContrat(dureeInitContrat());
		newObject.setDureeMaxContrat(dureeMaxContrat());
		newObject.setTemHospitalier(temHospitalier());
		newObject.setTemPourTitulaire(temPourTitulaire());
		newObject.setTemCdi(temCdi());
		newObject.setTemDelegation(temDelegation());
		newObject.setTemEnseignant(temEnseignant());
		newObject.setTemEquivGrade(temEquivGrade());
		newObject.setTemInviteAssocie(temInviteAssocie());
		newObject.setTemPartiel(temPartiel());
		newObject.setTemServicePublic(temServicePublic());
		newObject.setTemVacataire(temVacataire());
		newObject.setTemEtudiant(temEtudiant());
		newObject.setTemDoctorant(temDoctorant());
		ec.insertObject(newObject);
		return newObject;
	}

	/**
	 * 
	 * @return
	 */
	public String libelleValidite() {
		String libelle = "";
		if (dateOuverture() != null) {
			if (dateFermeture() == null) {
				libelle += " ouvert à partir du " + DateCtrl.dateToString(dateOuverture());
			} else {
				libelle += " ouvert du " + DateCtrl.dateToString(dateOuverture()) + " au " + DateCtrl.dateToString(dateFermeture());
			}
		} else if (dateOuverture() != null) {
			libelle += " fermé à partir du " + DateCtrl.dateToString(dateFermeture());
		}
		return libelle;
	}
	public boolean requiertGrade() {
		return temEquivGrade() != null && temEquivGrade().equals(CocktailConstantes.VRAI);
	}

	public boolean estAllocataireRecherche() {
		return code().equals(CODE_ALLOCATAIRE_RECHERCHE);
	}
	public boolean estEnseignant() {
		return temEnseignant() != null && temEnseignant().equals(CocktailConstantes.VRAI);
	}
	public boolean estVacataire() {
		return temVacataire() != null && temVacataire().equals(CocktailConstantes.VRAI);
	}
	public boolean estEtudiant() {
		return temEtudiant() != null && temEtudiant().equals(CocktailConstantes.VRAI);
	}
	public boolean estDoctorant() {
		return temDoctorant() != null && temDoctorant().equals(CocktailConstantes.VRAI);
	}
	public boolean estPourEnseignement() {
		return temEnseignement() != null && temEnseignement().equals(CocktailConstantes.VRAI);
	}
	public boolean estPourCir() {
		return temCir() != null && temCir().equals(CocktailConstantes.VRAI);
	}
	public boolean estPostDoctorant() {
		return codeSupinfo() != null && codeSupinfo().equals(CODE_POST_DOCTORANT);
	}
	public boolean estServicePublic() {
		return temServicePublic() != null && temServicePublic().equals(CocktailConstantes.VRAI);
	}
	public boolean estTempsPartiel() {
		return temPartiel() != null && temPartiel().equals(CocktailConstantes.VRAI);
	}
	public boolean estVisible() {
		return temVisible() != null && temVisible().equals(CocktailConstantes.VRAI);
	}
	public void setEstVisible(Boolean yn) {
		if (yn)
			setTemVisible(CocktailConstantes.VRAI);
		else
			setTemVisible(CocktailConstantes.FAUX);
	}
	
	public boolean estCdi() {
		return temCdi() != null && temCdi().equals(CocktailConstantes.VRAI);
	}
	public boolean estCdiSauvadet() {
		return codeSupinfo() != null && codeSupinfo().equals(CODE_CDI_SAUVADET);
	}
	
	public boolean tempsPartielPossible() {
		return temPartiel() != null && temPartiel().equals(CocktailConstantes.VRAI);
	}
	public boolean beneficieDelegation() {
		return temDelegation() != null && temDelegation().equals(CocktailConstantes.VRAI);
	}
	public boolean estAutorisePourTitulaire() {
		return temPourTitulaire() != null && temPourTitulaire().equals(CocktailConstantes.VRAI);
	}
	public boolean estHospitalier() {
		return temHospitalier() != null && temHospitalier().equals(CocktailConstantes.VRAI);
	}
	public boolean estInvite() {
		return temInviteAssocie() != null && temInviteAssocie().equals(CocktailConstantes.VRAI);
	}

	public EOTypeGestionArrete typeGestionArrete() {
		EOGestContrat gest = EOGestContrat.findForTypeContrat(editingContext(), this);
		if (gest != null) {
			return gest.typeGestionArrete();
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @param ec
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOTypeContratTravail> findTypesContrat(EOEditingContext ec) {
		try {
			return fetchAll(ec, null, SORT_ARRAY_LIBELLE);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray<EOTypeContratTravail>();
		}
	}

	
	public static EOQualifier getQualifierVacataires(boolean value) {
		return CocktailFinder.getQualifierEqual(TEM_VACATAIRE_KEY, (value)?"O":"N");
	}
	public static EOQualifier getQualifierHospitalo(boolean value) {
		return CocktailFinder.getQualifierEqual(TEM_HOSPITALIER_KEY, (value)?"O":"N");
	}
	public static EOQualifier getQualifierSupinfo(boolean value) {
		if (value)
			return EOQualifier.qualifierWithQualifierFormat(CODE_SUPINFO_KEY + " != nil", null);
		return EOQualifier.qualifierWithQualifierFormat(CODE_SUPINFO_KEY + " = nil", null);
	}
	public static EOQualifier getQualifierContratPere(EOTypeContratTravail typePere) {
		return EOQualifier.qualifierWithQualifierFormat(TO_TYPE_CONTRAT_PERE_KEY + " = %@", new NSArray<EOTypeContratTravail>(typePere));
	}
	public static EOQualifier getQualifierVisible(boolean value) {
		return CocktailFinder.getQualifierEqual(TEM_VISIBLE_KEY, (value)?"O":"N");
	}
	public static EOQualifier getQualifierVisible(String value) {
		return getQualifierVisible(value.equals("O"));
	}
	public static EOQualifier getQualifierDoctorant(boolean value) {
		return CocktailFinder.getQualifierEqual(TEM_DOCTORANT_KEY, (value)?"O":"N");
	}
	public static EOQualifier getQualifierDoctorant(String value) {
		return getQualifierDoctorant(value.equals("O"));
	}
	public static EOQualifier getQualifierCdi(boolean value) {
		return CocktailFinder.getQualifierEqual(TEM_CDI_KEY, (value)?"O":"N");
	}
	public static EOQualifier getQualifierCdi(String value) {
		return getQualifierCdi(value.equals("O"));
	}
	public static EOQualifier getQualifierEtudiant(boolean value) {
		return CocktailFinder.getQualifierEqual(TEM_ETUDIANT_KEY, (value)?"O":"N");
	}
	public static EOQualifier getQualifierEtudiant(String value) {
		return getQualifierEtudiant(value.equals("O"));
	}
	public static EOQualifier getQualifierInvite(boolean value) {
		return CocktailFinder.getQualifierEqual(TEM_INVITE_ASSOCIE_KEY, (value)?"O":"N");
	}
	public static EOQualifier getQualifierInvite(String value) {
		return getQualifierInvite(value.equals("O"));
	}
	public static EOQualifier getQualifierEnseignant(boolean value) {
		return CocktailFinder.getQualifierEqual(TEM_ENSEIGNANT_KEY, (value)?"O":"N");
	}
	public static EOQualifier getQualifierEnseignant(String value) {
		return getQualifierEnseignant(value.equals("O"));
	}
	public static EOQualifier getQualifierLocal(boolean value) {
		return CocktailFinder.getQualifierEqual(TEM_LOCAL_KEY, (value)?"O":"N");
	}
	public static EOQualifier getQualifierLocal(String value) {
		return getQualifierLocal(value.equals("O"));
	}
	public static EOQualifier getQualifierNiveau(Integer value) {
		return CocktailFinder.getQualifierEqual(C_NIVEAU_KEY, value);
	}
	public static EOQualifier getQualifierPeriode(NSTimestamp dateReference) {
		return (NomenclatureAvecDate.qualifierPourPeriode(dateReference, dateReference));
	}
	
	/**
	 * Prend en compte si la gestion des HU est activée ou non dans l'établissement
	 * @return qualifier pour la population HU
	 */
	public static EOQualifier qualifierHU() {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		
		if (EOGrhumParametres.isGestionHu()) {
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(true));			
			orQualifiers.addObject(EOTypeContratTravail.getQualifierHospitalo(false));		
			qualifiers.add(new EOOrQualifier(orQualifiers));
		} else {
			qualifiers.add(EOTypeContratTravail.getQualifierHospitalo(false));
		}
		
		return new EOAndQualifier(qualifiers);
	}
	
    public static NSArray<EOTypeContratTravail> findForPere(EOEditingContext ec, EOTypeContratTravail typePere) {
        return fetchAll(ec, getQualifierContratPere(typePere), SORT_ARRAY_LIBELLE);
    }

	/**
	 * 
	 * @param ec
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOTypeContratTravail> findTypesContratValidesForPere(EOEditingContext ec, EOTypeContratTravail typePere, Integer niveau, NSTimestamp dateReference) {

		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(getQualifierContratPere(typePere));
			qualifiers.addObject(getQualifierVisible(true));
			qualifiers.addObject(getQualifierNiveau(niveau));

			if (dateReference != null) {
				qualifiers.addObject(getQualifierPeriode(dateReference));
			}

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_LIBELLE);
		}
		catch (Exception ex) {
			return new NSArray<EOTypeContratTravail>();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOTypeContratTravail> findTypesContratSupinfo(EOEditingContext ec, NSTimestamp dateReference) {

		try {

			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(getQualifierSupinfo(true));

			if (dateReference != null) {
				qualifiers.addObject(NomenclatureAvecDate.qualifierPourPeriode(dateReference, dateReference));
			}

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_LIBELLE);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * 
	 * @param ec
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOTypeContratTravail> findTypesContratValidesForNiveau(EOEditingContext ec, Integer niveau, NSTimestamp dateReference) {

		try {

			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			qualifiers.addObject(getQualifierNiveau(niveau));
			qualifiers.addObject(getQualifierVisible(true));

			if (dateReference != null) {
				qualifiers.addObject(NomenclatureAvecDate.qualifierPourPeriode(dateReference, dateReference));
			}

			return fetchAll(ec, new EOAndQualifier(qualifiers), SORT_ARRAY_LIBELLE);
		} catch (Exception ex) {
			return new NSArray<EOTypeContratTravail>();
		}
	}
	
	/**
	 * @param ec : editing context
	 * @param dateReference : date de référence
	 * @return liste des types de contrat de travail de niveau 1
	 */
	public static NSArray<EOTypeContratTravail> findTypesContratValidesForNiveauUn(EOEditingContext ec, NSTimestamp dateReference) {
		return findTypesContratValidesForNiveau(ec, NIVEAU_1, dateReference);
	}

	/**
	 * 
	 * @param edc
	 * @return
	 */
	public static NSArray<INomenclature> findForSelection(EOEditingContext edc, NSTimestamp dateReference) {

		NSMutableArray<INomenclature> typesContrat = new NSMutableArray<INomenclature>();

		// Ajout de tous les types de contrat de niveau 1 visibles
		NSArray<EOTypeContratTravail> types = findTypesContratValidesForNiveau(edc, NIVEAU_1, dateReference);
		for (EOTypeContratTravail type : types) {
			NSArray<EOTypeContratTravail> fils = findTypesContratValidesForPere(edc, type, NIVEAU_LOCAL, dateReference);
			if( fils.size() == 0) {
				typesContrat.addObject(type);
			}
		}

		// Ajout de tous les niveaux 2 visibles et ouverts à la date de reference
		typesContrat.addObjectsFromArray(findTypesContratValidesForNiveau(edc, NIVEAU_LOCAL, dateReference));
		typesContrat.addObjectsFromArray(findTypesContratValidesForNiveau(edc, new Integer(3), dateReference));	//Pour gérer l'historique des anciens types de contrats
		
		EOQualifier.filterArrayWithQualifier(typesContrat, EOTypeContratTravail.getQualifierVacataires(false));

		return typesContrat.immutableClone();

	}

	/**
	 * 
	 * @param ec
	 * @param dateReference
	 * @return
	 */
	public static NSArray<EOTypeContratTravail> findTypesContratValides(EOEditingContext edc, NSTimestamp dateReference) {

		try {

			NSMutableArray qualifiers = new NSMutableArray();

			qualifiers.addObject(getQualifierVisible(true));

			if (dateReference != null) {
				qualifiers.addObject(NomenclatureAvecDate.qualifierPourPeriode(dateReference, dateReference));
			}

			return fetchAll(edc, new EOAndQualifier(qualifiers), SORT_ARRAY_LIBELLE);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return new NSArray();
		}
	}


	/**
	 * 
	 */
	public void validateForSave() {

		super.validateForSave();
		
		checkLongueurChamp();
		
		if (StringCtrl.containsIgnoreCase(code(), "CN") && isLocal()) {
			throw new NSValidation.ValidationException("Un code local ne doit pas contenir la chaine 'CN' !");				
		}
		if (StringCtrl.containsIgnoreCase(code(), "CN") && isLocal()) {
			throw new NSValidation.ValidationException("Un code local ne doit pas contenir la chaine 'CN' !");				
		}

		if (dModification() == null && NomenclatureFinder.isNomenclatureExistante(new EOEditingContext(), EOTypeContratTravail.ENTITY_NAME, code())) {
			throw new NSValidation.ValidationException("Ce code est déjà utilisé !");							
		}

		if (dureeInitContrat() != null && dureeMaxContrat() != null && (dureeInitContrat().intValue() > dureeMaxContrat().intValue()) ){
			throw new NSValidation.ValidationException("La durée initiale ne peut être supérieure à la durée maximale !");			
		}

		if (NIVEAU_LOCAL.equals(cNiveau()) && toTypeContratPere() == null) {
			throw new NSValidation.ValidationException("Pour un contrat local, vous devez renseigner un type contrat 'PERE' !");
		}
		if (potentielBrut() == null)
			setPotentielBrut(new Double(0));

		setDModification(new NSTimestamp());
	}
	
	/**
	 * vérifie la longueur des champs
	 */
	private void checkLongueurChamp() {
		
		if (code() != null && code().length() > 6) {
			throw new NSValidation.ValidationException(String.format(MangueMessagesErreur.ERREUR_CODE_LONGUEUR, 6));
		}
		
		if (libelleCourt()!= null && libelleCourt().length() > 30) {
			throw new NSValidation.ValidationException(String.format(MangueMessagesErreur.ERREUR_LIBELLE_COURT_LONGUEUR, 30));
		}
		
		if (libelleLong() != null && libelleLong().length() > 100) {
			throw new NSValidation.ValidationException(String.format(MangueMessagesErreur.ERREUR_LIBELLE_LONG_LONGUEUR, 100));
		}
	}
}


/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeContratTravail.java instead.
package org.cocktail.mangue.common.modele.nomenclatures.contrat;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.mangue.common.modele.nomenclatures.NomenclatureAvecDate;


public abstract class _EOTypeContratTravail extends  NomenclatureAvecDate {
	public static final String ENTITY_NAME = "TypeContratTravail";
	public static final String ENTITY_TABLE_NAME = "GRHUM.TYPE_CONTRAT_TRAVAIL";

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "code";

	public static final String C_CATEGORIE_KEY = "cCategorie";
	public static final String C_NIVEAU_KEY = "cNiveau";
	public static final String CODE_KEY = "code";
	public static final String CODE_SUPINFO_KEY = "codeSupinfo";
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DROIT_CONGES_CONTRAT_KEY = "droitCongesContrat";
	public static final String DUREE_INIT_CONTRAT_KEY = "dureeInitContrat";
	public static final String DUREE_MAX_CONTRAT_KEY = "dureeMaxContrat";
	public static final String LIBELLE_COURT_KEY = "libelleCourt";
	public static final String LIBELLE_LONG_KEY = "libelleLong";
	public static final String POTENTIEL_BRUT_KEY = "potentielBrut";
	public static final String REF_REGLEMENTAIRE_KEY = "refReglementaire";
	public static final String TEM_HOSPITALIER_KEY = "temHospitalier";
	public static final String TEM_POUR_TITULAIRE_KEY = "temPourTitulaire";
	public static final String TEM_CDI_KEY = "temCdi";
	public static final String TEM_DELEGATION_KEY = "temDelegation";
	public static final String TEM_ENSEIGNEMENT_KEY = "temEnseignement";
	public static final String TEM_CIR_KEY = "temCir";
	public static final String TEM_DOCTORANT_KEY = "temDoctorant";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_EQUIV_GRADE_KEY = "temEquivGrade";
	public static final String TEM_ETUDIANT_KEY = "temEtudiant";
	public static final String TEM_INVITE_ASSOCIE_KEY = "temInviteAssocie";
	public static final String TEM_LOCAL_KEY = "temLocal";
	public static final String TEM_PARTIEL_KEY = "temPartiel";
	public static final String TEM_SERVICE_PUBLIC_KEY = "temServicePublic";
	public static final String TEM_VACATAIRE_KEY = "temVacataire";
	public static final String TEM_VISIBLE_KEY = "temVisible";

	// Attributs non visibles
	public static final String CODE_PERE_KEY = "codePere";
	public static final String HORAIRE_HEBDOMADAIRE_KEY = "horaireHebdomadaire";

	//Colonnes dans la base de donnees
	public static final String C_CATEGORIE_COLKEY = "C_CATEGORIE";
	public static final String C_NIVEAU_COLKEY = "C_NIVEAU";
	public static final String CODE_COLKEY = "C_TYPE_CONTRAT_TRAV";
	public static final String DATE_FERMETURE_COLKEY = "D_FIN_VAL";
	public static final String DATE_OUVERTURE_COLKEY = "D_DEB_VAL";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String DROIT_CONGES_CONTRAT_COLKEY = "DROIT_CONGES_CONTRAT";
	public static final String DUREE_INIT_CONTRAT_COLKEY = "DUREE_INIT_CONTRAT";
	public static final String DUREE_MAX_CONTRAT_COLKEY = "DUREE_MAX_CONTRAT";
	public static final String LIBELLE_COURT_COLKEY = "LC_TYPE_CONTRAT_TRAV";
	public static final String LIBELLE_LONG_COLKEY = "LL_TYPE_CONTRAT_TRAV";
	public static final String POTENTIEL_BRUT_COLKEY = "POTENTIEL_BRUT";
	public static final String REF_REGLEMENTAIRE_COLKEY = "REF_REGLEMENTAIRE";
	public static final String TEM_HOSPITALIER_COLKEY = "TEM_HOSPITALIER";
	public static final String TEM_POUR_TITULAIRE_COLKEY = "TEM_CAV_AUTOR_TITUL";
	public static final String TEM_CDI_COLKEY = "TEM_CDI";
	public static final String TEM_DELEGATION_COLKEY = "TEM_DELEGATION";
	public static final String TEM_DOCTORANT_COLKEY = "TEM_DOCTORANT";
	public static final String TEM_ENSEIGNEMENT_COLKEY = "TEM_ENSEIGNEMENT";
	public static final String TEM_CIR_COLKEY = "TEM_CIR";
	public static final String TEM_ENSEIGNANT_COLKEY = "TEM_ENSEIGNANT";
	public static final String TEM_EQUIV_GRADE_COLKEY = "TEM_EQUIV_GRADE";
	public static final String TEM_ETUDIANT_COLKEY = "TEM_ETUDIANT";
	public static final String TEM_INVITE_ASSOCIE_COLKEY = "TEM_INVITE_ASSOCIE";
	public static final String TEM_LOCAL_COLKEY = "TEM_LOCAL";
	public static final String TEM_PARTIEL_COLKEY = "TEM_PARTIEL";
	public static final String TEM_REMUNERATION_PRINCIPALE_COLKEY = "TEM_REMUNERATION_PRINCIPALE";
	public static final String TEM_RENOUV_CONTRAT_COLKEY = "TEM_RENOUV_CONTRAT";
	public static final String TEM_SERVICE_PUBLIC_COLKEY = "TEM_SERVICE_PUBLIC";
	public static final String TEM_VACATAIRE_COLKEY = "TEM_VACATAIRE";
	public static final String TEM_VISIBLE_COLKEY = "TEM_VISIBLE";

	public static final String CODE_PERE_COLKEY = "C_TYPE_CONTRAT_PERE";

	// Relationships
	public static final String TO_TYPE_CONTRAT_PERE_KEY = "toTypeContratPere";



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
	public String cCategorie() {
		return (String) storedValueForKey(C_CATEGORIE_KEY);
	}

	public void setCCategorie(String value) {
		takeStoredValueForKey(value, C_CATEGORIE_KEY);
	}

	public Integer cNiveau() {
		return (Integer) storedValueForKey(C_NIVEAU_KEY);
	}

	public void setCNiveau(Integer value) {
		takeStoredValueForKey(value, C_NIVEAU_KEY);
	}

	public String code() {
		return (String) storedValueForKey(CODE_KEY);
	}

	public void setCode(String value) {
		takeStoredValueForKey(value, CODE_KEY);
	}

	public String codeSupinfo() {
		return (String) storedValueForKey(CODE_SUPINFO_KEY);
	}

	public void setCodeSupinfo(String value) {
		takeStoredValueForKey(value, CODE_SUPINFO_KEY);
	}

	public NSTimestamp dateFermeture() {
		return (NSTimestamp) storedValueForKey(DATE_FERMETURE_KEY);
	}

	public void setDateFermeture(NSTimestamp value) {
		takeStoredValueForKey(value, DATE_FERMETURE_KEY);
	}

	public NSTimestamp dateOuverture() {
		return (NSTimestamp) storedValueForKey(DATE_OUVERTURE_KEY);
	}

	public void setDateOuverture(NSTimestamp value) {
		takeStoredValueForKey(value, DATE_OUVERTURE_KEY);
	}

	public NSTimestamp dCreation() {
		return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
	}

	public void setDCreation(NSTimestamp value) {
		takeStoredValueForKey(value, D_CREATION_KEY);
	}

	public NSTimestamp dModification() {
		return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
	}

	public void setDModification(NSTimestamp value) {
		takeStoredValueForKey(value, D_MODIFICATION_KEY);
	}

	public Integer dureeInitContrat() {
		return (Integer) storedValueForKey(DUREE_INIT_CONTRAT_KEY);
	}

	public void setDureeInitContrat(Integer value) {
		takeStoredValueForKey(value, DUREE_INIT_CONTRAT_KEY);
	}

	public Integer dureeMaxContrat() {
		return (Integer) storedValueForKey(DUREE_MAX_CONTRAT_KEY);
	}

	public void setDureeMaxContrat(Integer value) {
		takeStoredValueForKey(value, DUREE_MAX_CONTRAT_KEY);
	}

	public String libelleCourt() {
		return (String) storedValueForKey(LIBELLE_COURT_KEY);
	}

	public void setLibelleCourt(String value) {
		takeStoredValueForKey(value, LIBELLE_COURT_KEY);
	}
	public String droitCongesContrat() {
		return (String) storedValueForKey(DROIT_CONGES_CONTRAT_KEY);
	}

	public void setDroitCongesContrat(String value) {
		takeStoredValueForKey(value, DROIT_CONGES_CONTRAT_KEY);
	}

	public String libelleLong() {
		return (String) storedValueForKey(LIBELLE_LONG_KEY);
	}

	public void setLibelleLong(String value) {
		takeStoredValueForKey(value, LIBELLE_LONG_KEY);
	}

	public Double potentielBrut() {
		return (Double) storedValueForKey(POTENTIEL_BRUT_KEY);
	}

	public void setPotentielBrut(Double value) {
		takeStoredValueForKey(value, POTENTIEL_BRUT_KEY);
	}

	public String refReglementaire() {
		return (String) storedValueForKey(REF_REGLEMENTAIRE_KEY);
	}

	public void setRefReglementaire(String value) {
		takeStoredValueForKey(value, REF_REGLEMENTAIRE_KEY);
	}

	public String temHospitalier() {
		return (String) storedValueForKey(TEM_HOSPITALIER_KEY);
	}

	public void setTemHospitalier(String value) {
		takeStoredValueForKey(value, TEM_HOSPITALIER_KEY);
	}

	public String temPourTitulaire() {
		return (String) storedValueForKey(TEM_POUR_TITULAIRE_KEY);
	}

	public void setTemPourTitulaire(String value) {
		takeStoredValueForKey(value, TEM_POUR_TITULAIRE_KEY);
	}

	public String temCdi() {
		return (String) storedValueForKey(TEM_CDI_KEY);
	}

	public void setTemCdi(String value) {
		takeStoredValueForKey(value, TEM_CDI_KEY);
	}

	public String temDelegation() {
		return (String) storedValueForKey(TEM_DELEGATION_KEY);
	}

	public void setTemDelegation(String value) {
		takeStoredValueForKey(value, TEM_DELEGATION_KEY);
	}

	public String temDoctorant() {
		return (String) storedValueForKey(TEM_DOCTORANT_KEY);
	}

	public void setTemDoctorant(String value) {
		takeStoredValueForKey(value, TEM_DOCTORANT_KEY);
	}

	public String temEnseignement() {
		return (String) storedValueForKey(TEM_ENSEIGNEMENT_KEY);
	}

	public void setTemEnseignement(String value) {
		takeStoredValueForKey(value, TEM_ENSEIGNEMENT_KEY);
	}
	public String temCir() {
		return (String) storedValueForKey(TEM_CIR_KEY);
	}

	public void setTemCir(String value) {
		takeStoredValueForKey(value, TEM_CIR_KEY);
	}

	public String temEnseignant() {
		return (String) storedValueForKey(TEM_ENSEIGNANT_KEY);
	}

	public void setTemEnseignant(String value) {
		takeStoredValueForKey(value, TEM_ENSEIGNANT_KEY);
	}

	public String temEquivGrade() {
		return (String) storedValueForKey(TEM_EQUIV_GRADE_KEY);
	}

	public void setTemEquivGrade(String value) {
		takeStoredValueForKey(value, TEM_EQUIV_GRADE_KEY);
	}

	public String temEtudiant() {
		return (String) storedValueForKey(TEM_ETUDIANT_KEY);
	}

	public void setTemEtudiant(String value) {
		takeStoredValueForKey(value, TEM_ETUDIANT_KEY);
	}

	public String temInviteAssocie() {
		return (String) storedValueForKey(TEM_INVITE_ASSOCIE_KEY);
	}

	public void setTemInviteAssocie(String value) {
		takeStoredValueForKey(value, TEM_INVITE_ASSOCIE_KEY);
	}

	public String temLocal() {
		return (String) storedValueForKey(TEM_LOCAL_KEY);
	}

	public void setTemLocal(String value) {
		takeStoredValueForKey(value, TEM_LOCAL_KEY);
	}

	public String temPartiel() {
		return (String) storedValueForKey(TEM_PARTIEL_KEY);
	}

	public void setTemPartiel(String value) {
		takeStoredValueForKey(value, TEM_PARTIEL_KEY);
	}

	public String temServicePublic() {
		return (String) storedValueForKey(TEM_SERVICE_PUBLIC_KEY);
	}

	public void setTemServicePublic(String value) {
		takeStoredValueForKey(value, TEM_SERVICE_PUBLIC_KEY);
	}

	public String temVacataire() {
		return (String) storedValueForKey(TEM_VACATAIRE_KEY);
	}

	public void setTemVacataire(String value) {
		takeStoredValueForKey(value, TEM_VACATAIRE_KEY);
	}

	public String temVisible() {
		return (String) storedValueForKey(TEM_VISIBLE_KEY);
	}

	public void setTemVisible(String value) {
		takeStoredValueForKey(value, TEM_VISIBLE_KEY);
	}

	public org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail toTypeContratPere() {
		return (org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail)storedValueForKey(TO_TYPE_CONTRAT_PERE_KEY);
	}

	public void setToTypeContratPereRelationship(org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail value) {
		if (value == null) {
			org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail oldValue = toTypeContratPere();
			if (oldValue != null) {
				removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_CONTRAT_PERE_KEY);
			}
		} else {
			addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_CONTRAT_PERE_KEY);
		}
	}


	/**
	 * Créer une instance de EOTypeContratTravail avec les champs et relations obligatoires et l'insere dans l'editingContext.
	 */
	public static  EOTypeContratTravail createEOTypeContratTravail(EOEditingContext editingContext, String code
			, NSTimestamp dateFermeture
			, NSTimestamp dCreation
			, NSTimestamp dModification
			, String temEquivGrade
			, org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail toTypeContratPere			) {
		EOTypeContratTravail eo = (EOTypeContratTravail) createAndInsertInstance(editingContext, _EOTypeContratTravail.ENTITY_NAME);    
		eo.setCode(code);
		eo.setDateFermeture(dateFermeture);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setTemEquivGrade(temEquivGrade);
		eo.setToTypeContratPereRelationship(toTypeContratPere);
		return eo;
	}


	public EOTypeContratTravail localInstanceIn(EOEditingContext editingContext) {
		return (EOTypeContratTravail)localInstanceOfObject(editingContext, this);
	}


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOTypeContratTravail creerInstance(EOEditingContext editingContext) {
		return creerInstance(editingContext, null);
	}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	public static EOTypeContratTravail creerInstance(EOEditingContext editingContext, NSArray specificites) {
		EOTypeContratTravail object = (EOTypeContratTravail)createAndInsertInstance(editingContext, _EOTypeContratTravail.ENTITY_NAME, specificites);
		return object;
	}



	public static EOTypeContratTravail localInstanceIn(EOEditingContext editingContext, EOTypeContratTravail eo) {
		EOTypeContratTravail localInstance = (eo == null) ? null : (EOTypeContratTravail)localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	/**
	 * 
	 * @param editingContext
	 * @param eo
	 * @return L'objet eo dans l'editingContext
	 * @deprecated Utilisez EOTypeContratTravail#localInstanceIn a la place.
	 */
	public static EOTypeContratTravail localInstanceOf(EOEditingContext editingContext, EOTypeContratTravail eo) {
		return EOTypeContratTravail.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	public static NSArray fetchAll(EOEditingContext editingContext) {
		return fetchAll(editingContext, (EOQualifier)null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	/**
	 * Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOTypeContratTravail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}


	/**
	 * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeContratTravail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOTypeContratTravail eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOTypeContratTravail)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}




	public static EOTypeContratTravail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	public static EOTypeContratTravail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOTypeContratTravail eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOTypeContratTravail)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  


	/**
	 * Une exception est declenchee si aucun objet est trouve.
	 * 
	 * @param editingContext
	 * @param qualifier Le filtre
	 * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	 * @throws NoSuchElementException si aucun objet est trouve
	 */
	public static EOTypeContratTravail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		EOTypeContratTravail eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOTypeContratTravail ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	


	public static EOTypeContratTravail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}

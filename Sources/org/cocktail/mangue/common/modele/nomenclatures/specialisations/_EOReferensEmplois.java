// _EOReferensEmplois.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReferensEmplois.java instead.
package org.cocktail.mangue.common.modele.nomenclatures.specialisations;

import java.util.NoSuchElementException;

import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.modele.nomenclatures.NomenclatureAvecDate;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOReferensEmplois extends  NomenclatureAvecDate {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "ReferensEmplois";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REFERENS_EMPLOIS";

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "code";

	public static final String BAP_ID_KEY = "bapId";
	public static final String CODEMEN_KEY = "codemen";
	public static final String DEFINITION_KEY = "definition";
	public static final String DEFINTION_CLEAN_KEY = "defintionClean";
	public static final String NUMEMPLOI_KEY = "numemploi";
	public static final String OUVERTCONCOURS_KEY = "ouvertconcours";
	public static final String SIGLECORPS_KEY = "siglecorps";

// Attributs non visibles
	public static final String NUMDCP_KEY = "numdcp";
	public static final String NUMFP_KEY = "numfp";

//Colonnes dans la base de donnees
	public static final String BAP_ID_COLKEY = "BAP_ID";
	public static final String CODEMEN_COLKEY = "CODEMEN";
	public static final String DEFINITION_COLKEY = "DEFINITION";
	public static final String DEFINTION_CLEAN_COLKEY = "DEFINTION_CLEAN";
	public static final String NUMEMPLOI_COLKEY = "NUMEMPLOI";
	public static final String OUVERTCONCOURS_COLKEY = "OUVERTCONCOURS";
	public static final String SIGLECORPS_COLKEY = "SIGLECORPS";

	public static final String NUMDCP_COLKEY = "NUMDCP";
	public static final String NUMFP_COLKEY = "NUMFP";

	// Relationships
	public static final String REFERENS_FP_KEY = "referensFp";
	public static final String TO_BAP_KEY = "toBap";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer bapId() {
    return (Integer) storedValueForKey(BAP_ID_KEY);
  }

  public void setBapId(Integer value) {
    takeStoredValueForKey(value, BAP_ID_KEY);
  }

  public String codemen() {
    return (String) storedValueForKey(CODEMEN_KEY);
  }

  public void setCodemen(String value) {
    takeStoredValueForKey(value, CODEMEN_KEY);
  }

  public String definition() {
    return (String) storedValueForKey(DEFINITION_KEY);
  }

  public void setDefinition(String value) {
    takeStoredValueForKey(value, DEFINITION_KEY);
  }

  public String defintionClean() {
    return (String) storedValueForKey(DEFINTION_CLEAN_KEY);
  }

  public void setDefintionClean(String value) {
    takeStoredValueForKey(value, DEFINTION_CLEAN_KEY);
  }
  public String numemploi() {
    return (String) storedValueForKey(NUMEMPLOI_KEY);
  }

  public void setNumemploi(String value) {
    takeStoredValueForKey(value, NUMEMPLOI_KEY);
  }

  public String ouvertconcours() {
    return (String) storedValueForKey(OUVERTCONCOURS_KEY);
  }

  public void setOuvertconcours(String value) {
    takeStoredValueForKey(value, OUVERTCONCOURS_KEY);
  }

  public String siglecorps() {
    return (String) storedValueForKey(SIGLECORPS_KEY);
  }

  public void setSiglecorps(String value) {
    takeStoredValueForKey(value, SIGLECORPS_KEY);
  }

  
  public org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap toBap() {
    return (org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap)storedValueForKey(TO_BAP_KEY);
  }

  public void setToBapRelationship(org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOBap oldValue = toBap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BAP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BAP_KEY);
    }
  }
  
  
	  public EOReferensEmplois localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReferensEmplois)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensEmplois creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReferensEmplois creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOReferensEmplois object = (EOReferensEmplois)createAndInsertInstance(editingContext, _EOReferensEmplois.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOReferensEmplois localInstanceIn(EOEditingContext editingContext, EOReferensEmplois eo) {
    EOReferensEmplois localInstance = (eo == null) ? null : (EOReferensEmplois)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOReferensEmplois#localInstanceIn a la place.
   */
	public static EOReferensEmplois localInstanceOf(EOEditingContext editingContext, EOReferensEmplois eo) {
		return EOReferensEmplois.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReferensEmplois fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReferensEmplois fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReferensEmplois eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReferensEmplois)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReferensEmplois fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReferensEmplois fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReferensEmplois eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReferensEmplois)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReferensEmplois fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReferensEmplois eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReferensEmplois ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReferensEmplois fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

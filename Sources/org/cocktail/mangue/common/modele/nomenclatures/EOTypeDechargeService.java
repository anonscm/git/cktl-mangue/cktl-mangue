/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.common.modele.nomenclatures;

import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOTypeDechargeService extends _EOTypeDechargeService {

    public EOTypeDechargeService() {
        super();
    }
    
    public String toString() {
    	return libelleLong();
    }

	public static EOTypeDechargeService creer(EOEditingContext ec) {
		EOTypeDechargeService newObject = (EOTypeDechargeService) createAndInsertInstance(ec,ENTITY_NAME);    
		newObject.setDCreation(new NSTimestamp());
		newObject.setDModification(new NSTimestamp());
		newObject.setDateOuverture(new NSTimestamp());
		newObject.setTemHcomp(CocktailConstantes.FAUX);
		newObject.setTemValide(CocktailConstantes.VRAI);
		return newObject;
	}

	public boolean autoriseHcomp() {
		return temHcomp().equals(CocktailConstantes.VRAI);
	}
	public void setAutoriseHcomp(boolean yn) {
		if (yn)
			setTemHcomp(CocktailConstantes.VRAI);
		else
			setTemHcomp(CocktailConstantes.FAUX);
	}
	
	public boolean estValide() {
		return temValide().equals(CocktailConstantes.VRAI);
	}
	public void setEstValide(boolean yn) {
		if (yn)
			setTemValide(CocktailConstantes.VRAI);
		else
			setTemValide(CocktailConstantes.FAUX);
	}

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	super.validateForSave();
       	if (code() == null || code().length() > 3)
    		throw new ValidationException("Veuillez renseigner un code (2 ou 3 caractères) !");
    	if (libelleLong() == null || libelleLong().length() > 40)
    		throw new ValidationException("Veuillez renseigner un libellé (< 40 caractères) !");
    	if (libelleCourt() == null || libelleCourt().length() > 20)
    		throw new ValidationException("Veuillez renseigner un libellé court (< 20 caractères) !");

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
    }

}

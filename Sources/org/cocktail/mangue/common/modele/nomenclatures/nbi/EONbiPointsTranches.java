/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.mangue.common.modele.nomenclatures.nbi;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;


public class EONbiPointsTranches extends _EONbiPointsTranches {

	public static final EOSortOrdering SORT_TRANCHE_ASC = new EOSortOrdering(NO_TRANCHE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray SORT_ARRAY_TRANCHE_ASC = new NSArray(SORT_TRANCHE_ASC);
	
    public EONbiPointsTranches() {
        super();
    }

    
    public static NSArray<EONbiPointsTranches> findForType(EOEditingContext ec, EONbiTypes type) {
    	try {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(C_TYPE_NBI_KEY + "=%@", new NSArray(type.code()));
    	return fetchAll(ec, qualifier, SORT_ARRAY_TRANCHE_ASC );
    	}
    	catch (Exception e) {
    		return new NSArray<EONbiPointsTranches>();
    	}
    }
    
    public String toString() {
    	return noTranche().toString();
    }
    
    public String noTrancheAsString() {
    		return noTranche().toString();
    }
}

/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.common.modele.nomenclatures;

import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class EOMinisteres extends _EOMinisteres {
	
    public EOMinisteres() {
        super();
    }

    public String toString() {
    	return libelleCourt();
    }
    
    public boolean estMESR() {
    	return code().equals(ManGUEConstantes.CODE_MIN_MESR);
    }
    public boolean estMAAF() {
    	return code().equals(ManGUEConstantes.CODE_MIN_MAAF);
    }
    
    public static NSArray<EOMinisteres> findMinisteresValides(EOEditingContext edc) {
    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(TEM_VISIBLE_KEY + "=%@", new NSArray (CocktailConstantes.VRAI));
    	return fetchAll(edc, qualifier, SORT_ARRAY_LIBELLE_COURT);
    }
    public static NSArray<EOMinisteres> findAllMinisteres(EOEditingContext edc) {
    	return fetchAll(edc, null, SORT_ARRAY_LIBELLE);
    }

}

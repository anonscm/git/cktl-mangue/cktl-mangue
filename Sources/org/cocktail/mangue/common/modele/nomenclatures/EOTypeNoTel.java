/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.common.modele.nomenclatures;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public class EOTypeNoTel extends _EOTypeNoTel {
	
	public static final String TYPE_FIXE = "TEL";
	public static final String TYPE_DEFAULT = TYPE_FIXE;

	public static final String TYPE_MOBILE = "MOB";
	public static final String TYPE_FAX = "FAX";
	public static final String TYPE_SIP = "SIP";
	
    public EOTypeNoTel() {
        super();
    }

    public String toString() {

    	String libelle ="";
    	
    	if (estTelephoneFixe())
    		libelle= "Téléphone";
    	if (estFax())
    		libelle= "Fax";
    	if (estTelephoneMobile())
    		libelle= "Mobile";
    	
    	return libelle;
    	
    }

    public static NSArray rechercher(EOEditingContext ec) {
    	NSMutableArray qualifiers = new NSMutableArray();
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY+" != %@", new NSArray(TYPE_SIP)));
    	return fetchAll(ec, new EOAndQualifier(qualifiers));
    }
    public static EOTypeNoTel getDefault(EOEditingContext ec) {
    	NSMutableArray qualifiers = new NSMutableArray();
    	qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(CODE_KEY+" = %@", new NSArray(TYPE_DEFAULT)));
    	return fetchFirstByQualifier(ec, new EOAndQualifier(qualifiers));
    }

	public boolean estTelephoneFixe() {
		return code().equals(EOTypeNoTel.TYPE_FIXE);
	}
	public boolean estTelephoneMobile() {
		return code().equals(EOTypeNoTel.TYPE_MOBILE);
	}
	public boolean estFax() {
		return code().equals(EOTypeNoTel.TYPE_FAX);
	}
}

// EOTypeElectionHu.java
// Created on Tue Feb 15 10:01:59 Europe/Paris 2011 by Apple EOModeler Version 5.2

package org.cocktail.mangue.common.modele.nomenclatures.hospitalo;

import org.cocktail.common.utilities.RecordAvecLibelle;

public class EOTypeElectionHu extends _EOTypeElectionHu implements RecordAvecLibelle {

    public EOTypeElectionHu() {
        super();
    }
	public String libelle() {
		return tehuLibelle();
	}
}

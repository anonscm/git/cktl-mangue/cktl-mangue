// EOTypeRecrutementHu.java
// Created on Tue Feb 15 10:02:30 Europe/Paris 2011 by Apple EOModeler Version 5.2

package org.cocktail.mangue.common.modele.nomenclatures.hospitalo;

import org.cocktail.common.utilities.RecordAvecLibelle;

public class EOTypeRecrutementHu extends _EOTypeRecrutementHu implements RecordAvecLibelle {

    public EOTypeRecrutementHu() {
        super();
    }
    public String toString() {
    	return libelle();
    }
	public String libelle() {
		return trhuLibelle();
	}
	
}

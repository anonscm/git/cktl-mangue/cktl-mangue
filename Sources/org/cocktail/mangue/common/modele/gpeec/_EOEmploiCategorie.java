// _EOEmploiCategorie.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEmploiCategorie.java instead.
package org.cocktail.mangue.common.modele.gpeec;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOEmploiCategorie extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "EmploiCategorie";
	public static final String ENTITY_TABLE_NAME = "MANGUE.EMPLOI_CATEGORIE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String PERSONNE_CREATION_KEY = "personneCreation";
	public static final String PERSONNE_MODIFICATION_KEY = "personneModification";

// Attributs non visibles
	public static final String CODE_CATEGORIE_EMPLOI_KEY = "codeCategorieEmploi";
	public static final String ID_KEY = "id";
	public static final String ID_EMPLOI_KEY = "idEmploi";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "D_CREATION";
	public static final String DATE_DEBUT_COLKEY = "D_DEBUT";
	public static final String DATE_FIN_COLKEY = "D_FIN";
	public static final String DATE_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String PERSONNE_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERSONNE_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";

	public static final String CODE_CATEGORIE_EMPLOI_COLKEY = "C_CATEGORIE_EMPLOI";
	public static final String ID_COLKEY = "ID_EMPLOI_CATEGORIE";
	public static final String ID_EMPLOI_COLKEY = "ID_EMPLOI";


	// Relationships
	public static final String TO_CATEGORIE_EMPLOI_KEY = "toCategorieEmploi";
	public static final String TO_EMPLOI_KEY = "toEmploi";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public Integer personneCreation() {
    return (Integer) storedValueForKey(PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreation(Integer value) {
    takeStoredValueForKey(value, PERSONNE_CREATION_KEY);
  }

  public Integer personneModification() {
    return (Integer) storedValueForKey(PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModification(Integer value) {
    takeStoredValueForKey(value, PERSONNE_MODIFICATION_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi toCategorieEmploi() {
    return (org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi)storedValueForKey(TO_CATEGORIE_EMPLOI_KEY);
  }

  public void setToCategorieEmploiRelationship(org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi oldValue = toCategorieEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CATEGORIE_EMPLOI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CATEGORIE_EMPLOI_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.gpeec.EOEmploi toEmploi() {
    return (org.cocktail.mangue.common.modele.gpeec.EOEmploi)storedValueForKey(TO_EMPLOI_KEY);
  }

  public void setToEmploiRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploi value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.gpeec.EOEmploi oldValue = toEmploi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EMPLOI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EMPLOI_KEY);
    }
  }
  

/**
 * Créer une instance de EOEmploiCategorie avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEmploiCategorie createEOEmploiCategorie(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateDebut
, NSTimestamp dateModification
, Integer personneCreation
, org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi toCategorieEmploi, org.cocktail.mangue.common.modele.gpeec.EOEmploi toEmploi			) {
    EOEmploiCategorie eo = (EOEmploiCategorie) createAndInsertInstance(editingContext, _EOEmploiCategorie.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateDebut(dateDebut);
		eo.setDateModification(dateModification);
		eo.setPersonneCreation(personneCreation);
    eo.setToCategorieEmploiRelationship(toCategorieEmploi);
    eo.setToEmploiRelationship(toEmploi);
    return eo;
  }

  
	  public EOEmploiCategorie localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEmploiCategorie)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEmploiCategorie creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEmploiCategorie creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOEmploiCategorie object = (EOEmploiCategorie)createAndInsertInstance(editingContext, _EOEmploiCategorie.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEmploiCategorie localInstanceIn(EOEditingContext editingContext, EOEmploiCategorie eo) {
    EOEmploiCategorie localInstance = (eo == null) ? null : (EOEmploiCategorie)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEmploiCategorie#localInstanceIn a la place.
   */
	public static EOEmploiCategorie localInstanceOf(EOEditingContext editingContext, EOEmploiCategorie eo) {
		return EOEmploiCategorie.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEmploiCategorie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEmploiCategorie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEmploiCategorie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEmploiCategorie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEmploiCategorie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEmploiCategorie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEmploiCategorie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEmploiCategorie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEmploiCategorie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEmploiCategorie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEmploiCategorie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEmploiCategorie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

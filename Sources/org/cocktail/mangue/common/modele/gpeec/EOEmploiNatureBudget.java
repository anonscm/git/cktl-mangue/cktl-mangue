package org.cocktail.mangue.common.modele.gpeec;

import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiNatureBudget;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EONatureBudget;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe des natures de budget des emplois
 * 
 * @author Chama LAATIK
 *
 */
public class EOEmploiNatureBudget extends _EOEmploiNatureBudget implements IEmploiNatureBudget {
	
	private static final long serialVersionUID = -1610825669974876029L;

	/**
	 * Constructeur
	 */
	public EOEmploiNatureBudget() {
        super();
    }
	
	/**
     * @return Affichage du budget de l'emploi
     */
    public String toString() {
    	return  getToNatureBudget().libelle() + getDatesEmploi();
    }
    
    /**
	 * @return les dates de début et de fin (si renseigné) de la spécialisation de l'emploi
	 */
    public String getDatesEmploi() {
    	String libelle = "";
    	if (getDateFin() != null) {
    		libelle += " (" + DateCtrl.dateToString(this.getDateDebut()) + " - " + DateCtrl.dateToString(this.getDateFin()) + ")";
    	} else {
    		libelle += " depuis le " + DateCtrl.dateToString(this.getDateDebut());
    	}
    	
    	return libelle;
    }
	
	/**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * 
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() {
        super.validateForDelete();
    }

    /**
     * Peut etre appele Ã  partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() {
		setDateModification(new NSTimestamp());
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    public void validateBeforeTransactionSave() {
    	
    }


	public Integer getPersonneCreation() {
		return personneCreation();
	}

	public Integer getPersonneModification() {
		return personneModification();
	}
	
	public NSTimestamp getDateCreation() {
		return dateCreation();
	}

	public NSTimestamp getDateModification() {
		return dateModification();
	}
	
	public NSTimestamp getDateDebut() {
		return dateDebut();
	}

	public NSTimestamp getDateFin() {
		return dateFin();
	}

	public EONatureBudget getToNatureBudget() {
		return toNatureBudget();
	}

	public IEmploi getToEmploi() {
		return toEmploi();
	}

	/**
	 *  Setter pour un emploi 
	 * @param value : un emploi {@link IEmploi}
	 **/
	public void setToEmploiRelationship(IEmploi value) {
		super.setToEmploiRelationship((EOEmploi) value);
	}
}

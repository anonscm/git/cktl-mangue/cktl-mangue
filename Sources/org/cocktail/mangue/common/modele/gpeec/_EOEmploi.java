// _EOEmploi.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEmploi.java instead.
package org.cocktail.mangue.common.modele.gpeec;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class _EOEmploi extends  EOGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Emploi";
	public static final String ENTITY_TABLE_NAME = "MANGUE.EMPLOI";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_EFFET_EMPLOI_KEY = "dateEffetEmploi";
	public static final String DATE_FERMETURE_EMPLOI_KEY = "dateFermetureEmploi";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String DATE_PUBLICATION_EMPLOI_KEY = "datePublicationEmploi";
	public static final String NO_EMPLOI_KEY = "noEmploi";
	public static final String NO_EMPLOI_FORMATTE_KEY = "noEmploiFormatte";
	public static final String PERSONNE_CREATION_KEY = "personneCreation";
	public static final String PERSONNE_MODIFICATION_KEY = "personneModification";
	public static final String QUOTITE_KEY = "quotite";
	public static final String TEM_ARBITRAGE_KEY = "temArbitrage";
	public static final String TEM_CONCOURS_KEY = "temConcours";
	public static final String TEM_CONTRACTUEL_KEY = "temContractuel";
	public static final String TEM_DURABILITE_KEY = "temDurabilite";
	public static final String TEM_ENSEIGNANT_KEY = "temEnseignant";
	public static final String TEM_NATIONAL_KEY = "temNational";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String CODE_ARTICLE_KEY = "codeArticle";
	public static final String CODE_CHAPITRE_KEY = "codeChapitre";
	public static final String CODE_PROGRAMME_KEY = "codeProgramme";
	public static final String CODE_RNE_KEY = "codeRne";
	public static final String CODE_TITRE_KEY = "codeTitre";
	public static final String ID_KEY = "id";

//Colonnes dans la base de donnees
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String DATE_CREATION_COLKEY = "D_CREATION";
	public static final String DATE_EFFET_EMPLOI_COLKEY = "D_EFFET_EMPLOI";
	public static final String DATE_FERMETURE_EMPLOI_COLKEY = "D_FERMETURE_EMPLOI";
	public static final String DATE_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String DATE_PUBLICATION_EMPLOI_COLKEY = "D_PUBLICATION_EMPLOI";
	public static final String NO_EMPLOI_COLKEY = "NO_EMPLOI";
	public static final String NO_EMPLOI_FORMATTE_COLKEY = "NO_EMPLOI_FORMATTE";
	public static final String PERSONNE_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERSONNE_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String QUOTITE_COLKEY = "QUOTITE";
	public static final String TEM_ARBITRAGE_COLKEY = "TEM_ARBITRAGE";
	public static final String TEM_CONCOURS_COLKEY = "TEM_CONCOURS";
	public static final String TEM_CONTRACTUEL_COLKEY = "TEM_CONTRACTUEL";
	public static final String TEM_DURABILITE_COLKEY = "TEM_DURABILITE";
	public static final String TEM_ENSEIGNANT_COLKEY = "TEM_ENSEIGNANT";
	public static final String TEM_NATIONAL_COLKEY = "TEM_NATIONAL";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String CODE_ARTICLE_COLKEY = "C_ARTICLE";
	public static final String CODE_CHAPITRE_COLKEY = "C_CHAPITRE";
	public static final String CODE_PROGRAMME_COLKEY = "C_PROGRAMME";
	public static final String CODE_RNE_COLKEY = "C_RNE";
	public static final String CODE_TITRE_COLKEY = "C_TITRE";
	public static final String ID_COLKEY = "ID_EMPLOI";


	// Relationships
	public static final String LISTE_EMPLOI_CATEGORIES_KEY = "listeEmploiCategories";
	public static final String LISTE_EMPLOI_LOCALISATIONS_KEY = "listeEmploiLocalisations";
	public static final String LISTE_EMPLOI_NATURE_BUDGETS_KEY = "listeEmploiNatureBudgets";
	public static final String LISTE_EMPLOI_SPECIALISATIONS_KEY = "listeEmploiSpecialisations";
	public static final String LISTE_OCCUPATIONS_KEY = "listeOccupations";
	public static final String TO_CHAPITRE_KEY = "toChapitre";
	public static final String TO_CHAPITRE_ARTICLE_KEY = "toChapitreArticle";
	public static final String TO_PROGRAMME_KEY = "toProgramme";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_TITRE_KEY = "toTitre";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String commentaire() {
    return (String) storedValueForKey(COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    takeStoredValueForKey(value, COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateEffetEmploi() {
    return (NSTimestamp) storedValueForKey(DATE_EFFET_EMPLOI_KEY);
  }

  public void setDateEffetEmploi(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_EFFET_EMPLOI_KEY);
  }

  public NSTimestamp dateFermetureEmploi() {
    return (NSTimestamp) storedValueForKey(DATE_FERMETURE_EMPLOI_KEY);
  }

  public void setDateFermetureEmploi(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FERMETURE_EMPLOI_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public NSTimestamp datePublicationEmploi() {
    return (NSTimestamp) storedValueForKey(DATE_PUBLICATION_EMPLOI_KEY);
  }

  public void setDatePublicationEmploi(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_PUBLICATION_EMPLOI_KEY);
  }

  public String noEmploi() {
    return (String) storedValueForKey(NO_EMPLOI_KEY);
  }

  public void setNoEmploi(String value) {
    takeStoredValueForKey(value, NO_EMPLOI_KEY);
  }

  public String noEmploiFormatte() {
    return (String) storedValueForKey(NO_EMPLOI_FORMATTE_KEY);
  }

  public void setNoEmploiFormatte(String value) {
    takeStoredValueForKey(value, NO_EMPLOI_FORMATTE_KEY);
  }

  public Integer personneCreation() {
    return (Integer) storedValueForKey(PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreation(Integer value) {
    takeStoredValueForKey(value, PERSONNE_CREATION_KEY);
  }

  public Integer personneModification() {
    return (Integer) storedValueForKey(PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModification(Integer value) {
    takeStoredValueForKey(value, PERSONNE_MODIFICATION_KEY);
  }

  public java.math.BigDecimal quotite() {
    return (java.math.BigDecimal) storedValueForKey(QUOTITE_KEY);
  }

  public void setQuotite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, QUOTITE_KEY);
  }

  public String temArbitrage() {
    return (String) storedValueForKey(TEM_ARBITRAGE_KEY);
  }

  public void setTemArbitrage(String value) {
    takeStoredValueForKey(value, TEM_ARBITRAGE_KEY);
  }

  public String temConcours() {
    return (String) storedValueForKey(TEM_CONCOURS_KEY);
  }

  public void setTemConcours(String value) {
    takeStoredValueForKey(value, TEM_CONCOURS_KEY);
  }

  public String temContractuel() {
    return (String) storedValueForKey(TEM_CONTRACTUEL_KEY);
  }

  public void setTemContractuel(String value) {
    takeStoredValueForKey(value, TEM_CONTRACTUEL_KEY);
  }

  public String temDurabilite() {
    return (String) storedValueForKey(TEM_DURABILITE_KEY);
  }

  public void setTemDurabilite(String value) {
    takeStoredValueForKey(value, TEM_DURABILITE_KEY);
  }

  public String temEnseignant() {
    return (String) storedValueForKey(TEM_ENSEIGNANT_KEY);
  }

  public void setTemEnseignant(String value) {
    takeStoredValueForKey(value, TEM_ENSEIGNANT_KEY);
  }

  public String temNational() {
    return (String) storedValueForKey(TEM_NATIONAL_KEY);
  }

  public void setTemNational(String value) {
    takeStoredValueForKey(value, TEM_NATIONAL_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitre toChapitre() {
    return (org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitre)storedValueForKey(TO_CHAPITRE_KEY);
  }

  public void setToChapitreRelationship(org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitre value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitre oldValue = toChapitre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CHAPITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CHAPITRE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitreArticle toChapitreArticle() {
    return (org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitreArticle)storedValueForKey(TO_CHAPITRE_ARTICLE_KEY);
  }

  public void setToChapitreArticleRelationship(org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitreArticle value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitreArticle oldValue = toChapitreArticle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CHAPITRE_ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CHAPITRE_ARTICLE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgramme toProgramme() {
    return (org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgramme)storedValueForKey(TO_PROGRAMME_KEY);
  }

  public void setToProgrammeRelationship(org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgramme value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgramme oldValue = toProgramme();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PROGRAMME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PROGRAMME_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.EORne toRne() {
    return (org.cocktail.mangue.common.modele.nomenclatures.EORne)storedValueForKey(TO_RNE_KEY);
  }

  public void setToRneRelationship(org.cocktail.mangue.common.modele.nomenclatures.EORne value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_KEY);
    }
  }
  
  public org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgrammeTitre toTitre() {
    return (org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgrammeTitre)storedValueForKey(TO_TITRE_KEY);
  }

  public void setToTitreRelationship(org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgrammeTitre value) {
    if (value == null) {
    	org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgrammeTitre oldValue = toTitre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TITRE_KEY);
    }
  }
  
  public NSArray listeEmploiCategories() {
    return (NSArray)storedValueForKey(LISTE_EMPLOI_CATEGORIES_KEY);
  }

  public NSArray listeEmploiCategories(EOQualifier qualifier) {
    return listeEmploiCategories(qualifier, null, false);
  }

  public NSArray listeEmploiCategories(EOQualifier qualifier, boolean fetch) {
    return listeEmploiCategories(qualifier, null, fetch);
  }

  public NSArray listeEmploiCategories(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie.TO_EMPLOI_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeEmploiCategories();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeEmploiCategoriesRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie object) {
    addObjectToBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_CATEGORIES_KEY);
  }

  public void removeFromListeEmploiCategoriesRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_CATEGORIES_KEY);
  }

  public org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie createListeEmploiCategoriesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EmploiCategorie");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, LISTE_EMPLOI_CATEGORIES_KEY);
    return (org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie) eo;
  }

  public void deleteListeEmploiCategoriesRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_CATEGORIES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeEmploiCategoriesRelationships() {
    Enumeration objects = listeEmploiCategories().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeEmploiCategoriesRelationship((org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie)objects.nextElement());
    }
  }

  public NSArray listeEmploiLocalisations() {
    return (NSArray)storedValueForKey(LISTE_EMPLOI_LOCALISATIONS_KEY);
  }

  public NSArray listeEmploiLocalisations(EOQualifier qualifier) {
    return listeEmploiLocalisations(qualifier, null, false);
  }

  public NSArray listeEmploiLocalisations(EOQualifier qualifier, boolean fetch) {
    return listeEmploiLocalisations(qualifier, null, fetch);
  }

  public NSArray listeEmploiLocalisations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation.TO_EMPLOI_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeEmploiLocalisations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeEmploiLocalisationsRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_LOCALISATIONS_KEY);
  }

  public void removeFromListeEmploiLocalisationsRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_LOCALISATIONS_KEY);
  }

  public org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation createListeEmploiLocalisationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EmploiLocalisation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, LISTE_EMPLOI_LOCALISATIONS_KEY);
    return (org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation) eo;
  }

  public void deleteListeEmploiLocalisationsRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_LOCALISATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeEmploiLocalisationsRelationships() {
    Enumeration objects = listeEmploiLocalisations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeEmploiLocalisationsRelationship((org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation)objects.nextElement());
    }
  }

  public NSArray listeEmploiNatureBudgets() {
    return (NSArray)storedValueForKey(LISTE_EMPLOI_NATURE_BUDGETS_KEY);
  }

  public NSArray listeEmploiNatureBudgets(EOQualifier qualifier) {
    return listeEmploiNatureBudgets(qualifier, null, false);
  }

  public NSArray listeEmploiNatureBudgets(EOQualifier qualifier, boolean fetch) {
    return listeEmploiNatureBudgets(qualifier, null, fetch);
  }

  public NSArray listeEmploiNatureBudgets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget.TO_EMPLOI_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeEmploiNatureBudgets();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeEmploiNatureBudgetsRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget object) {
    addObjectToBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_NATURE_BUDGETS_KEY);
  }

  public void removeFromListeEmploiNatureBudgetsRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_NATURE_BUDGETS_KEY);
  }

  public org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget createListeEmploiNatureBudgetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EmploiNatureBudget");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, LISTE_EMPLOI_NATURE_BUDGETS_KEY);
    return (org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget) eo;
  }

  public void deleteListeEmploiNatureBudgetsRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_NATURE_BUDGETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeEmploiNatureBudgetsRelationships() {
    Enumeration objects = listeEmploiNatureBudgets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeEmploiNatureBudgetsRelationship((org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget)objects.nextElement());
    }
  }

  public NSArray listeEmploiSpecialisations() {
    return (NSArray)storedValueForKey(LISTE_EMPLOI_SPECIALISATIONS_KEY);
  }

  public NSArray listeEmploiSpecialisations(EOQualifier qualifier) {
    return listeEmploiSpecialisations(qualifier, null, false);
  }

  public NSArray listeEmploiSpecialisations(EOQualifier qualifier, boolean fetch) {
    return listeEmploiSpecialisations(qualifier, null, fetch);
  }

  public NSArray listeEmploiSpecialisations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation.TO_EMPLOI_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeEmploiSpecialisations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeEmploiSpecialisationsRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_SPECIALISATIONS_KEY);
  }

  public void removeFromListeEmploiSpecialisationsRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_SPECIALISATIONS_KEY);
  }

  public org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation createListeEmploiSpecialisationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EmploiSpecialisation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, LISTE_EMPLOI_SPECIALISATIONS_KEY);
    return (org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation) eo;
  }

  public void deleteListeEmploiSpecialisationsRelationship(org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_EMPLOI_SPECIALISATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeEmploiSpecialisationsRelationships() {
    Enumeration objects = listeEmploiSpecialisations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeEmploiSpecialisationsRelationship((org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation)objects.nextElement());
    }
  }

  public NSArray listeOccupations() {
    return (NSArray)storedValueForKey(LISTE_OCCUPATIONS_KEY);
  }

  public NSArray listeOccupations(EOQualifier qualifier) {
    return listeOccupations(qualifier, null, false);
  }

  public NSArray listeOccupations(EOQualifier qualifier, boolean fetch) {
    return listeOccupations(qualifier, null, fetch);
  }

  public NSArray listeOccupations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.mangue.modele.mangue.individu.EOOccupation.TO_EMPLOI_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.mangue.modele.mangue.individu.EOOccupation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeOccupations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeOccupationsRelationship(org.cocktail.mangue.modele.mangue.individu.EOOccupation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, LISTE_OCCUPATIONS_KEY);
  }

  public void removeFromListeOccupationsRelationship(org.cocktail.mangue.modele.mangue.individu.EOOccupation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_OCCUPATIONS_KEY);
  }

  public org.cocktail.mangue.modele.mangue.individu.EOOccupation createListeOccupationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Occupation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, LISTE_OCCUPATIONS_KEY);
    return (org.cocktail.mangue.modele.mangue.individu.EOOccupation) eo;
  }

  public void deleteListeOccupationsRelationship(org.cocktail.mangue.modele.mangue.individu.EOOccupation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LISTE_OCCUPATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeOccupationsRelationships() {
    Enumeration objects = listeOccupations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeOccupationsRelationship((org.cocktail.mangue.modele.mangue.individu.EOOccupation)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOEmploi avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEmploi createEOEmploi(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateEffetEmploi
, NSTimestamp dateModification
, String noEmploi
, Integer personneCreation
, String temArbitrage
, String temConcours
, String temContractuel
, String temDurabilite
, String temEnseignant
, String temNational
, String temValide
			) {
    EOEmploi eo = (EOEmploi) createAndInsertInstance(editingContext, _EOEmploi.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateEffetEmploi(dateEffetEmploi);
		eo.setDateModification(dateModification);
		eo.setNoEmploi(noEmploi);
		eo.setPersonneCreation(personneCreation);
		eo.setTemArbitrage(temArbitrage);
		eo.setTemConcours(temConcours);
		eo.setTemContractuel(temContractuel);
		eo.setTemDurabilite(temDurabilite);
		eo.setTemEnseignant(temEnseignant);
		eo.setTemNational(temNational);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOEmploi localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEmploi)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEmploi creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEmploi creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOEmploi object = (EOEmploi)createAndInsertInstance(editingContext, _EOEmploi.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEmploi localInstanceIn(EOEditingContext editingContext, EOEmploi eo) {
    EOEmploi localInstance = (eo == null) ? null : (EOEmploi)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEmploi#localInstanceIn a la place.
   */
	public static EOEmploi localInstanceOf(EOEditingContext editingContext, EOEmploi eo) {
		return EOEmploi.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier,  NSArray sortOrderings) {
		  return fetchAll(editingContext, qualifier, sortOrderings, false, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, NSArray prefetchs) {
			return fetchAll(editingContext, qualifier, sortOrderings, false, prefetchs);
		  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false, null);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct, NSArray prefetchs) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    if (prefetchs != null)
	    	fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchs);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEmploi fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEmploi fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEmploi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEmploi)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEmploi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEmploi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEmploi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEmploi)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEmploi fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEmploi eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEmploi ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEmploi fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}

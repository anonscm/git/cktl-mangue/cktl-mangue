package org.cocktail.mangue.common.modele.gpeec;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.metier.finder.gpeec.EmploiCategorieFinder;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitre;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitreArticle;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgramme;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgrammeTitre;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.common.utilities.ManGUEConstantes;
import org.cocktail.mangue.common.utilities.StringCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.EOGrhumParametres;
import org.cocktail.mangue.modele.grhum.EOQuotite;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe des emplois 
 * 
 * @author Chama LAATIK
 *
 */
public class EOEmploi extends _EOEmploi implements IEmploi {

	private static final long serialVersionUID = -7878714521294407745L;

	/**
	 * Constructeur
	 */
	public EOEmploi() {
		super();
	}

	/**
	 * Invalider l'emploi
	 */
	public void invalider() {
		setTemValide(CocktailConstantes.FAUX);
	}

	/**
	 * Est-ce que l'emploi est postérieure au 01/01/2006?
	 * 	=> si oui, on renseigne le programme/Titre
	 *  => si non, on renseigne chapitre/article
	 * @return Vrai/Faux
	 */
	public boolean emploiPosterieurA2006() {
		return ((getDateEffetEmploi() == null) || (DateCtrl.isAfterEq(getDateEffetEmploi(), DateCtrl.stringToDate("01/01/2006"))));	
	}
	
	/**
	 * Retourne la date à prendre en compte pour les controles
	 *    Si date_Effet <= date_Publication => date_Effet
	 *    Si date_Effet >  date_Publication => date_Publication
	 * @return une date
	 */
	public NSTimestamp getDateDebutEmploi() {
		NSTimestamp date = getDateEffetEmploi();
		if (getDatePublicationEmploi() == null) {
			return date;
		}
		
		if (DateCtrl.isBeforeEq(getDatePublicationEmploi(), getDateEffetEmploi())) {
			 date = getDatePublicationEmploi();
		} 
		
		return date;
	}

	/**
	 * Est-ce que le numéro d'emploi est unique?
	 * @return Vrai/Faux
	 */
	public boolean verifierNoEmploiUnique() {
		EOEmploi unEmploi = (EOEmploi) SuperFinder.rechercherObjetAvecAttributEtValeurEgale(editingContext(), ENTITY_NAME, 
				EOEmploi.NO_EMPLOI_KEY, getNoEmploi().toUpperCase());
		
		NSTimestamp dateReference = DateCtrl.stringToDate("01/01/2015");
		if (this != unEmploi) {
			if (unEmploi != null && unEmploi.isValide() && DateCtrl.isBefore(getDateDebutEmploi(), dateReference)) {
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Formatte le numéro d'emploi (méthode appelée avant la validation de l'objet)
	 * @param noEmploi : numéro d'emploi concerné
	 * @param categorieEmploi : catégorie d'emploi
	 * @param code : code de la spécialisation
	 * @param isDiscSecondDegre : concerne la spécialisation discipline second degré?
	 * @param isCnu : concerne la spécialisation Cnu?
	 * @return numéro d'emploi formatté
	 */
	public String formatterNoEmploi(String noEmploi, EOCategorieEmploi categorieEmploi, String code, boolean isDiscSecondDegre, boolean isCnu) {
		String noFormatte = null;

		LogManager.logDetail("catégorie courante : " + categorieEmploi);
		LogManager.logDetail("code spécialisation : " + code);
		LogManager.logDetail("est une discipline second degré : " + isDiscSecondDegre);
		LogManager.logDetail("est une spécialisation CNU : " + isCnu);
		
		if (noEmploi != null) {
			//On supprime toutes les lettres de numéro d'emploi
	        String noNational = StringCtrl.chaineNumerique(noEmploi);
	        noFormatte = categorieEmploi.cCategorieEmploi() + noNational;
	
	        if (!(categorieEmploi.estADT() || categorieEmploi.estAGT() || categorieEmploi.estAstr())) {
	            if (isDiscSecondDegre && (code != null)) {
	            	//on concatène le code de la discipline second degré avec le no formatté
	            	noFormatte = code + noFormatte;
	            } else if (isCnu) {
	            	//on concatène le code de la spécialisation CNU avec le no formatté
	               noFormatte = majNoFormatteCnu(noFormatte, code);
	            } else {
	            	//Cas des ReferensEmploi
	            	noFormatte = majNoFormatteReferens(noFormatte, noNational);
	            }
	        }
	        LogManager.logDetail("no formatté: " + noFormatte);
		}
		return noFormatte;
	}
	
	/**
	 * on concatene le code de la specialisation CNU avec le no formatté
	 * @param noFormatte : no formatté avec la catégorie d'emploi
	 * @param code : code de la spécialisation CNU
	 * @return no formatté
	 */
	private String majNoFormatteCnu(String noFormatte, String code) {
		//on concatène le code de la spécialisation CNU avec le no formatté
        if (code == null) {
            noFormatte = "U0000" + noFormatte;
        } else {
            noFormatte = "U" + code + "00" + noFormatte;
        }
        
        return noFormatte;
	}
	
	/**
	 * Génére le numéro formatté à partir du numéro national 
	 * @param noFormatte : no formatté avec la catégorie d'emploi
	 * @param noNational : le numéro national de l'emploi (uniquement des chiffres)
	 * @return no formatté
	 */
	private String majNoFormatteReferens(String noFormatte, String noNational) {
    	if (StringUtils.isNotEmpty(noNational)) {
            int index = new Integer(noNational).intValue() % 23;
            noFormatte = noFormatte + CocktailConstantes.LETTRES_ALPHABET[index];
    	}
    	
    	return noFormatte;
	}

	/**
	 * @param dateDebut : date de début recherché
	 * @param dateFin : date de fin recherché
	 * @return la quotité qui reste à occuper
	 */
	public BigDecimal getQuotiteRestanteAOccuper(NSTimestamp dateDebut, NSTimestamp dateFin) {

		NSArray<EOOccupation> listeOccupation = EOOccupation.rechercherOccupationsPourEmploiEtPeriode(this.editingContext(), this, dateDebut, dateFin);
		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOOccupation.TO_EMPLOI_KEY + "." 
				+ EOEmploi.NO_EMPLOI_AFFICHAGE_KEY , EOSortOrdering.CompareAscending));
		sorts.addObject(PeriodePourIndividu.SORT_DATE_DEBUT);
		listeOccupation = EOSortOrdering.sortedArrayUsingKeyOrderArray(listeOccupation,sorts);
		return calculerQuotiteRestanteMinimumSurPeriode(listeOccupation, dateDebut, dateFin);
		
	}

	/**
	 * 
	 * Calcul de la quotite minimale d'occupation d'un emploi sur une periode donnee
	 * 
	 * @param ec : editingContext
	 * @param occupations : liste des occupations
	 * @param dateDebut : date de début
	 * @param dateFin : date de fin
	 * @return la quotité restante
	 */
	public static BigDecimal calculerQuotiteRestanteMinimumSurPeriode(NSArray<EOOccupation> occupations, NSTimestamp dateDebut, NSTimestamp dateFin) {
		
		BigDecimal quotiteTotale = new BigDecimal(0);
		BigDecimal quotiteARetirer = new BigDecimal(0);
		BigDecimal quotiteMaxTotale = new BigDecimal(0); 
		NSTimestamp finOccupation = null;

		BigDecimal quotiteEmploi = ManGUEConstantes.QUOTITE_100;
		if (occupations.size() > 0)  {
			quotiteEmploi = occupations.get(0).toEmploi().getQuotite();
			LogManager.logDetail(" ===> Calcul quotite emploi " + occupations.get(0).toEmploi().getNoEmploi() + " du " + DateCtrl.dateToString(dateDebut) + " au " + DateCtrl.dateToString(dateFin) + ".");
		}

		NSMutableArray sorts = new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOOccupation.TO_EMPLOI_KEY +"."+ EOEmploi.NO_EMPLOI_AFFICHAGE_KEY , EOSortOrdering.CompareAscending));
		sorts.addObject(PeriodePourIndividu.SORT_DATE_DEBUT);
		occupations = EOSortOrdering.sortedArrayUsingKeyOrderArray(occupations,sorts);

		boolean estDebut = true;
		for (EOOccupation myOccupation : occupations) {

			LogManager.logDetail(" \t** Occupation du " + DateCtrl.dateToString(myOccupation.dateDebut()) + " au " + DateCtrl.dateToString(myOccupation.dateFin()) + " , QUOTITE : " + myOccupation.quotite());

			// on ramène la quotité à sa valeur financière
			BigDecimal quotiteOccupation = EOQuotite.getQuotiteFinanciere(myOccupation.quotite());
			LogManager.logDetail("\t\tQuotite Financiere Occupation : " + quotiteOccupation);

			if (estDebut) {
				estDebut = false;
				finOccupation = myOccupation.dateFin();
				BigDecimal num = quotiteOccupation.setScale(2, BigDecimal.ROUND_HALF_UP);
				if (num.doubleValue() == 100.00) {
					// l'emploi est totalement occupé pendant une partie de la période
					LogManager.logDetail("\t\tEmploi occupe a 100% ==> RETURN 0");
					return new BigDecimal(0.00);
				}
				quotiteTotale = quotiteOccupation;
				quotiteARetirer = quotiteOccupation;
				quotiteMaxTotale = quotiteOccupation;
			} else {
				if (finOccupation == null || DateCtrl.isBefore(myOccupation.dateDebut(), finOccupation)) {
					// si l'occupation commence avant la fin de l'autre
					// Ajouter la quotité de l'occupation
					quotiteTotale = quotiteTotale.add(quotiteOccupation);
					LogManager.logDetail("\t\tQuotite totale ==> " + quotiteTotale);
					BigDecimal num = quotiteTotale.setScale(2, BigDecimal.ROUND_HALF_UP);
					if (num.doubleValue() == 100.00) {
						// l'emploi est totalement occupé pendant une partie de la période
						LogManager.logDetail("\t\tEmploi occupe a 100% (1) ==> RETURN 0");
						return new BigDecimal(0.00);
					}
					// si l'occupation se termine avant la fin de l'occupation précédente, c'est elle qui faut prendre en compte
					if (myOccupation.dateFin() != null && (finOccupation == null || DateCtrl.isBefore(myOccupation.dateFin(), finOccupation))) {
						finOccupation = myOccupation.dateFin();
						quotiteARetirer = quotiteOccupation;
					} else {
						quotiteARetirer = quotiteTotale.subtract(quotiteOccupation);	
					}
				} else {
					//	l'occupation commence après => il faut retirer la quotité d'occupation de la quotité totale
					// en vérifiant auparavant si on a dépassé la quotité maxtotale
					LogManager.logDetail("\t\tQuotite totale ==> " + quotiteTotale);
					LogManager.logDetail("\t\tQuotite Max totale ==> " + quotiteMaxTotale);
					LogManager.logDetail("\t\tCompare ==> " + quotiteTotale.compareTo(quotiteMaxTotale));
					if (quotiteTotale.compareTo(quotiteMaxTotale) > 0) {
						quotiteMaxTotale = quotiteTotale;
						LogManager.logDetail("\t\tQuotite Max totale ==> " + quotiteMaxTotale);
					}
					quotiteTotale = quotiteTotale.subtract(quotiteARetirer);
					// Ajouter la quotité de l'occupation courante
					quotiteTotale = quotiteTotale.add(quotiteOccupation);
					if (quotiteTotale.compareTo(ManGUEConstantes.QUOTITE_100) == 0 ) {
						// l'emploi est totalement occupé pendant une partie de la période
						LogManager.logDetail("\t\tEmploi occupe a 100% (2) ==> RETURN 0");
						return new BigDecimal(0.00);
					}
					quotiteARetirer = quotiteOccupation;
					finOccupation = myOccupation.dateFin();
				}
			}
		}

//		if (quotiteTotale.compareTo(quotiteMaxTotale) > 0) {
//			quotiteMaxTotale = quotiteTotale;
//		}
//
		LogManager.logDetail("\t\tQUOTITE MAX ==> " + quotiteMaxTotale);
		
		return (quotiteEmploi.subtract(quotiteMaxTotale)).setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	/**
	 * @return la catégorie courante de l'emploi
	 */
	public String categorieEmploiCourante() {
		IEmploiCategorie emploiCateg = EmploiCategorieFinder.sharedInstance().findEmploiCategorieCourant(this.editingContext(), this);
		if ((emploiCateg != null) && (emploiCateg.getToCategorieEmploi() != null)) {
			return emploiCateg.getToCategorieEmploi().code();
		}
		
		return "";
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele Ã  partir des factories.
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() {
		setDateModification(new NSTimestamp());
		//On force la majuscule dans le no emploi et le no emploi formatté
		setNoEmploi(getNoEmploi().toUpperCase());
		if (EOGrhumParametres.isNoEmploiFormatte() && getNoEmploiFormatte() != null) {
			setNoEmploiFormatte(getNoEmploiFormatte().toUpperCase());
		}
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	public void validateBeforeTransactionSave() {

	}

	//FIXME : voir si on peut supprimer cast dans _EO dans le template
	public List<EOEmploiCategorie> getListeEmploiCategories() {
		return listeEmploiCategories();
	}

	public List<EOEmploiSpecialisation> getListeEmploiSpecialisations() {
		return listeEmploiSpecialisations();
	}

	public List<EOEmploiLocalisation> getListeEmploiLocalisations() {
		return listeEmploiLocalisations();
	}

	public List<EOEmploiNatureBudget> getListeEmploiNatureBudgets() {
		return listeEmploiNatureBudgets();
	}

	public String getTemDurabilite() {
		return temDurabilite();
	}

	/** 
	 * @return le témoin de durabilité en toute lettre pour l'affichage
	 */
	public String getTemDurabiliteEnLettre() {
		String durabilite = null;

		if (temDurabilite() != null) {
			if (temDurabilite().equals(TEM_DURABILITE_T)) {
				durabilite = TEMPORAIRE;
			} else {
				durabilite = PERMANENT;
			}
		} 
		return durabilite;
	}

	public void setTemDurabiliteConvertisseur(String value) {
		if (IEmploi.PERMANENT.equals(value)) {
			setTemDurabilite(IEmploi.TEM_DURABILITE_P);
		} else {
			setTemDurabilite(IEmploi.TEM_DURABILITE_T);
		}
	}

	public String getTemEnseignant() {
		return temEnseignant();
	}

	public boolean isEmploiEnseignant() {
		return temEnseignant() != null && temEnseignant().equals(CocktailConstantes.VRAI);
	}

	public void setIsEmploiEnseignant(boolean value) {
		if (value) {
			setTemEnseignant(CocktailConstantes.VRAI);
		} else {
			setTemEnseignant(CocktailConstantes.FAUX);
		}
	}

	public String getTemArbitrage() {
		return temArbitrage();
	}

	public boolean isEnArbitrage() {
		return temArbitrage() != null && temArbitrage().equals(CocktailConstantes.VRAI);
	}

	public void setIsEnArbitrage(boolean value) {
		if (value) {
			setTemArbitrage(CocktailConstantes.VRAI);
		} else {
			setTemArbitrage(CocktailConstantes.FAUX);
		}
	}

	public String getTemConcours() {
		return temConcours();
	}

	public boolean isEnConcours() {
		return temConcours() != null && temConcours().equals(CocktailConstantes.VRAI);
	}

	public void setIsEnConcours(boolean value) {
		if (value) {
			setTemConcours(CocktailConstantes.VRAI);
		} else {
			setTemConcours(CocktailConstantes.FAUX);
		}
	}
	
	public String getTemContractuel() {
		return temContractuel();
	}

	public boolean isEmploiContractuel() {
		return temContractuel() != null && temContractuel().equals(CocktailConstantes.VRAI);
	}

	public void setIsEmploiContractuel(boolean value) {
		if (value) {
			setTemContractuel(CocktailConstantes.VRAI);
		} else {
			setTemContractuel(CocktailConstantes.FAUX);
		}
	}

	public String getTemValide() {
		return temValide();
	}

	public boolean isValide() {
		return temValide() != null && temValide().equals(CocktailConstantes.VRAI);
	}
	
	public String getTemNational() {
		return temNational();
	}

	public boolean isEmploiNational() {
		return temNational() != null && temNational().equals(CocktailConstantes.VRAI); 
	}
	
	/**
	 * setter pour le témoin national
	 * @param value : booléen
	 */
	public void setIsEmploiNational(boolean value) {
		if (value) {
			setTemNational(CocktailConstantes.VRAI);
		} else {
			setTemNational(CocktailConstantes.FAUX);
		}
	}

	/**
	 * On supprime les espaces du numéro d'emploi
	 * @return le numéro d'emploi
	 */
	public String getNoEmploi() {
		String noEmploi = noEmploi();

		if (noEmploi != null) {
			noEmploi = noEmploi.replaceAll("\\s", "");
		}

		return noEmploi;
	}
	
	public String getNoEmploiFormatte() {
		return noEmploiFormatte();
	}
	
	/**
	 * Renvoie le numéro d'emploi à afficher. 
	 * SOIT le no formatté SOIT le no emploi.
	 * 
	 * @return no emploi à afficher
	 */
	public String getNoEmploiAffichage() {
		if (EOGrhumParametres.isNoEmploiFormatte()) {
			return getNoEmploiFormatte();
		}
		return getNoEmploi();
	}

	public String getCommentaire() {
		return commentaire();
	}

	public BigDecimal getQuotite() {
		return quotite();
	}

	public Integer getPersonneCreation() {
		return personneCreation();
	}

	public Integer getPersonneModification() {
		return personneModification();
	}

	public EOChapitre getToChapitre() {
		return toChapitre();
	}

	public EOChapitreArticle getToChapitreArticle() {
		return toChapitreArticle();
	}

	public EOProgramme getToProgramme() {
		return toProgramme();
	}

	public EORne getToRne() {
		return toRne();
	}

	public EOProgrammeTitre getToTitre() {
		return toTitre();
	}

	public NSTimestamp getDateEffetEmploi() {
		return dateEffetEmploi();
	}

	public NSTimestamp getDatePublicationEmploi() {
		return datePublicationEmploi();
	}

	public NSTimestamp getDateFermetureEmploi() {
		return dateFermetureEmploi();
	}

	public NSTimestamp getDateCreation() {
		return dateCreation();
	}

	public NSTimestamp getDateModification() {
		return dateModification();
	}
}

/**
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.mangue.common.modele.gpeec;

import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe des emplois catégories
 * 
 * @author Chama LAATIK
 *
 */
public class EOEmploiCategorie extends _EOEmploiCategorie implements IEmploiCategorie {

	private static final long serialVersionUID = 1810706571656878855L;

	/**
	 * Constructeur
	 */
	public EOEmploiCategorie() {
		super();
	}

	/**
     * @return Affichage de la catégorie d'emploi
     */
    public String toString() {
    	return getToCategorieEmploi().codeEtLibelle() + getDatesEmploi();
    }
    
    /**
	 * @return les dates de début et de fin (si renseigné) de l'emploi catégorie
	 */
    public String getDatesEmploi() {
    	String libelle = "";
    	if (getDateFin() != null) {
    		libelle += " (" + DateCtrl.dateToString(this.getDateDebut()) + " - " + DateCtrl.dateToString(this.getDateFin()) + ")";
    	} else {
    		libelle += " depuis le " + DateCtrl.dateToString(this.getDateDebut());
    	}
    	
    	return libelle;
    }
    
	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() {
		super.validateForDelete();
	}

	/**
	 * Peut etre appelé à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() {
		setDateModification(new NSTimestamp());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	public void validateBeforeTransactionSave() {

	}

	public Integer getPersonneCreation() {
		return personneCreation();
	}

	public Integer getPersonneModification() {
		return personneModification();
	}

	public NSTimestamp getDateCreation() {
		return dateCreation();
	}

	public NSTimestamp getDateModification() {
		return dateModification();
	}

	public NSTimestamp getDateDebut() {
		return dateDebut();
	}

	public NSTimestamp getDateFin() {
		return dateFin();
	}

	public EOCategorieEmploi getToCategorieEmploi() {
		return toCategorieEmploi();
	}

	public IEmploi getToEmploi() {
		return toEmploi();
	}

	/**
	 *  Setter pour un emploi 
	 * @param value : un emploi {@link IEmploi}
	 **/
	public void setToEmploiRelationship(IEmploi value) {
		super.setToEmploiRelationship((EOEmploi) value);
	}

}

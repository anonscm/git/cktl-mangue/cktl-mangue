package org.cocktail.mangue.common.modele.gpeec.interfaces;

import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf;

import com.webobjects.foundation.NSTimestamp;

/**
 * Définit le contrat d'une spécialisation d'emploi
 * 
 * @author Chama LAATIK
 *
 */
public interface IEmploiSpecialisation {
	
	/**
	 * @return libellé de la spécialisation avec la date
	 */
	String libelleSpecialisationAffiche();
	
	/**
	 * @return libellé de la spécialisation
	 */
	String libelleSpecialisation();
	
	/**
	 * @return date de début
	 */
	NSTimestamp getDateDebut();

	void setDateDebut(NSTimestamp value);

	/**
	 * @return date de fin
	 */
	NSTimestamp getDateFin();

	void setDateFin(NSTimestamp value);
	
	/**
	 * @return date de création
	 */
	NSTimestamp getDateCreation();
	
	void setDateCreation(NSTimestamp dateCreation);

	/**
	 * @return date de modification
	 */
	NSTimestamp getDateModification();

	void setDateModification(NSTimestamp value);

	/**
	 * @return personne qui crée l'enregistrement
	 */
	Integer getPersonneCreation();
	
	void setPersonneCreation(Integer value);

	/**
	 * @return personne qui modifie l'enregistrement
	 */
	Integer getPersonneModification();
	
	void setPersonneModification(Integer value);
	
	/**
	 * @return l'emploi
	 */
	IEmploi getToEmploi();

	/**
	 *  Setter pour un emploi 
	 * @param value : un emploi {@link IEmploi}
	 **/
	void setToEmploiRelationship(IEmploi value);
	
	/**
	 * @return la catégorie CNU (si enseignant-chercheur)
	 */
	EOCnu getToCnu();

	void setToCnuRelationship(EOCnu value);

	/**
	 * @return la discipline du second degré (si enseignant du second degré)
	 */
	EODiscSecondDegre getToDisciplineSdDegre();

	void setToDisciplineSdDegreRelationship(EODiscSecondDegre value);

	/**
	 * @return le code referens
	 */
	EOReferensEmplois getToEmploiReferens();

	void setToEmploiReferensRelationship(EOReferensEmplois value);

	/**
	 * @return la spécialité ATOS si enseignant
	 */
	EOSpecialiteAtos getToSpecialiteAtos();

	void setToSpecialiteAtosRelationship(EOSpecialiteAtos value);

	/**
	 * @return la spécialité ITARF
	 */
	EOSpecialiteItarf getToSpecialiteItarf();

	void setToSpecialiteItarfRelationship(EOSpecialiteItarf value);
	
	/**
	 * @return les dates de début et de fin (si renseigné) de la spécialisation de l'emploi
	 */
	String getDatesEmploi();

}
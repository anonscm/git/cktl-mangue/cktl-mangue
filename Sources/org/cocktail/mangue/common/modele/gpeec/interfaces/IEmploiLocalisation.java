package org.cocktail.mangue.common.modele.gpeec.interfaces;

import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;

import com.webobjects.foundation.NSTimestamp;

/**
 * Définit le contrat d'une affectation d'emploi
 * 
 * @author Chama LAATIK
 *
 */
public interface IEmploiLocalisation {

	/**
	 * @return la quotité
	 */
	Integer getQuotite();
	
	void setQuotite(Integer value);

	/**
	 * @return date de début
	 */
	NSTimestamp getDateDebut();

	void setDateDebut(NSTimestamp value);

	/**
	 * @return date de fin
	 */
	NSTimestamp getDateFin();

	void setDateFin(NSTimestamp value);
	
	/**
	 * @return date de création
	 */
	NSTimestamp getDateCreation();
	
	void setDateCreation(NSTimestamp dateCreation);

	/**
	 * @return date de modification
	 */
	NSTimestamp getDateModification();

	void setDateModification(NSTimestamp value);

	/**
	 * @return personne qui crée l'enregistrement
	 */
	Integer getPersonneCreation();
	
	void setPersonneCreation(Integer value);

	/**
	 * @return personne qui modifie l'enregistrement
	 */
	Integer getPersonneModification();
	
	void setPersonneModification(Integer value);

	/**
	 * @return l'emploi
	 */
	IEmploi getToEmploi();

	void setToEmploiRelationship(IEmploi value);

	/**
	 * @return Structure d'affectation de l'mploi
	 */
	EOStructure getToStructure();

	void setToStructureRelationship(EOStructure value);
	
	/**
	 * @return les dates de début et de fin (si renseigné) de la localisation de l'emploi
	 */
	String getDatesEmploi();
}
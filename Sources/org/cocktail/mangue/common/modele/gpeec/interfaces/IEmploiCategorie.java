package org.cocktail.mangue.common.modele.gpeec.interfaces;

import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;

import com.webobjects.foundation.NSTimestamp;

/**
 * Définit le contrat d'un emploi catégorie
 * 
 * @author Chama LAATIK
 *
 */
public interface IEmploiCategorie {

	/**
	 * @return date de début
	 */
	NSTimestamp getDateDebut();

	void setDateDebut(NSTimestamp value);

	/**
	 * @return date de fin
	 */
	NSTimestamp getDateFin();

	void setDateFin(NSTimestamp value);
	
	/**
	 * @return date de création
	 */
	NSTimestamp getDateCreation();
	
	void setDateCreation(NSTimestamp dateCreation);

	/**
	 * @return date de modification
	 */
	NSTimestamp getDateModification();

	void setDateModification(NSTimestamp value);

	/**
	 * @return personne qui crée l'enregistrement
	 */
	Integer getPersonneCreation();
	
	void setPersonneCreation(Integer value);

	/**
	 * @return personne qui modifie l'enregistrement
	 */
	Integer getPersonneModification();
	
	void setPersonneModification(Integer value);

	/**
	 * @return la catégorie d'emploi
	 */
	EOCategorieEmploi getToCategorieEmploi();

	void setToCategorieEmploiRelationship(EOCategorieEmploi value);

	/**
	 * @return l'emploi
	 */
	IEmploi getToEmploi();

	/**
	 *  Setter pour un emploi 
	 * @param value : un emploi {@link IEmploi}
	 **/
	void setToEmploiRelationship(IEmploi value);
	
	/**
	 * @return les dates de début et de fin (si renseigné) de l'emploi catégorie
	 */
	String getDatesEmploi();

}
package org.cocktail.mangue.common.modele.gpeec.interfaces;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitre;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOChapitreArticle;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgramme;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOProgrammeTitre;

import com.webobjects.foundation.NSTimestamp;

/**
 * Contrat d'un emploi
 * 
 * @author Chama LAATIK
 */
public interface IEmploi {

	//Durabilité de l'emploi
	String TEM_DURABILITE_P = "P";
	String TEM_DURABILITE_T = "T";
	String PERMANENT = "Permanent";
	String TEMPORAIRE = "Temporaire";
	
	//Etat de l'emploi
	String EN_COURS = "En cours";
	String OCCUPES = "Occupés";
	String VACANTS = "Vacants";
	String ROMPUS = "Rompus";
	String FERMES = "Fermés";
	String FUTURS = "Futurs";
	String TOUS = "Tous";
	
	String CATEGORIE_EMPLOI_COURANTE = "categorieEmploiCourante";
	String NO_EMPLOI_AFFICHAGE_KEY = "getNoEmploiAffichage";

	/**
	 * Invalider l'emploi
	 */
	void invalider();
	
	/**
	 * Est-ce que l'emploi est postérieure au 01/01/2006?
	 * 	=> si oui, on renseigne le programme/Titre
	 *  => si non, on renseigne chapitre/article
	 * @return Vrai/Faux
	 */
	boolean emploiPosterieurA2006();
	
	/**
	 * Retourne la date à prendre en compte pour les controles
	 *    Si date_Effet <= date_Publication => date_Effet
	 *    Si date_Effet >  date_Publication => date_Publication
	 * @return une date
	 */
	NSTimestamp getDateDebutEmploi();
	
	/**
	 * Est-ce que le numéro d'emploi est unique?
	 * @return Vrai/Faux
	 */
	boolean verifierNoEmploiUnique();
	
	/**
	 * Formatte le numéro d'emploi
	 * @param noEmploi : numéro d'emploi concerné
	 * @param categorieEmploi : catégorie d'emploi
	 * @param code : code de la spécialisation
	 * @param isDiscSecondDegre : concerne la spécialisation discipline second degré?
	 * @param isCnu : concerne la spécialisation Cnu?
	 * @return numéro d'emploi formatté
	 */
	String formatterNoEmploi(String noEmploi, EOCategorieEmploi categorieEmploi, String code, boolean isDiscSecondDegre,
			boolean isCnu);
	
	/**
	 * @param dateDebut : date de début recherché
	 * @param dateFin : date de fin recherché
	 * @return la quotité qui reste à occuper
	 */
	BigDecimal getQuotiteRestanteAOccuper(NSTimestamp dateDebut, NSTimestamp dateFin);
	
	/**
	 * @return la catégorie courante de l'emploi
	 */
	String categorieEmploiCourante();
	
	/**
	 * @return le numéro d'emploi
	 */
	String getNoEmploi();
	
	/**
	 * @param value : numéro d'emploi
	 */
	void setNoEmploi(String value);
	
	/**
	 * @return le numéro d'emploi formatté
	 */
	String getNoEmploiFormatte();

	/**
	 * @return le numéro d'emploi formatté ou le no emploi en fonction du paramètre de gestion des no emplois utilisé
	 */
	String getNoEmploiAffichage();

	/**
	 * @param value : numéro d'emploi formatté
	 */
	void setNoEmploiFormatte(String value);
	
	/**
	 * @return commentaire
	 */
	String getCommentaire();

	void setCommentaire(String value);
	
	/**
	 * @return quotité
	 */
	BigDecimal getQuotite();

	void setQuotite(BigDecimal quotite);

	/**
	 * @return date création de l'emploi
	 */
	NSTimestamp getDateEffetEmploi();

	void setDateEffetEmploi(NSTimestamp value);

	/**
	 * @return date de publication de l'emploi
	 */
	NSTimestamp getDatePublicationEmploi();

	void setDatePublicationEmploi(NSTimestamp value);

	/**
	 * @return date de suppression de l'emploi
	 */
	NSTimestamp getDateFermetureEmploi();

	void setDateFermetureEmploi(NSTimestamp value);
	
	/**
	 * @return date de création
	 */
	NSTimestamp getDateCreation();
	
	void setDateCreation(NSTimestamp dateCreation);

	/**
	 * @return date de modification
	 */
	NSTimestamp getDateModification();

	void setDateModification(NSTimestamp value);
	
	/**
	 * @return personne qui crée l'enregistrement
	 */
	Integer getPersonneCreation();

	void setPersonneCreation(Integer value);

	/**
	 * @return personne qui modifie l'enregistrement
	 */
	Integer getPersonneModification();

	void setPersonneModification(Integer value);

	String getTemArbitrage();

	void setTemArbitrage(String temArbitrage);
	
	boolean isEnArbitrage();
	
	void setIsEnArbitrage(boolean value);

	String getTemConcours();

	void setTemConcours(String temConcours);
	
	boolean isEnConcours();
	
	void setIsEnConcours(boolean value);
	
	String getTemDurabilite();

	/** 
	 * @return le témoin de durabilité en toute lettre pour l'affichage
	 */
	String getTemDurabiliteEnLettre();

	/**
	 * Témoin de durabilité
	 * @param unCaractere : témoin de durabilité en un caractère P ou T
	 */
	void setTemDurabilite(String unCaractere);
	
	/**
	 * Convertit le témoin de durabilité en un caractère P ou T
	 * @param temDurabilite : témoin de durabilité
	 */
	void setTemDurabiliteConvertisseur(String temDurabilite);

	String getTemEnseignant();

	void setTemEnseignant(String temEnseignant);

	boolean isEmploiEnseignant();
	
	void setIsEmploiEnseignant(boolean value);
	
	/**
	 * @return le témoin de durabilité
	 */
	String getTemContractuel();

	/**
	 * @param temContractuel : témoin si l'emploi est contractuel
	 */
	void setTemContractuel(String temContractuel);

	/**
	 * Est-ce que l'emploi est contractuel?
	 * @return Vrai/Faux
	 */
	boolean isEmploiContractuel();
	
	/**
	 * Setter avec booléen 
	 * @param value : booléen
	 */
	void setIsEmploiContractuel(boolean value);

	String getTemValide();

	void setTemValide(String temValide);
	
	boolean isValide();
	
	/**
	 * @return témoin national (O, N)
	 */
	String getTemNational();

	/**
	 * @param temNational : O ou N
	 */
	void setTemNational(String temNational);
	
	/**
	 * @return Est-ce que l'emploi est national?
	 */
	boolean isEmploiNational();
	
	/**
	 * Setter avec booléen 
	 * @param value : booléen
	 */
	void setIsEmploiNational(boolean value);
	
	EORne getToRne();

	void setToRneRelationship(EORne value);

	EOProgramme getToProgramme();

	void setToProgrammeRelationship(EOProgramme value);

	EOProgrammeTitre getToTitre();

	void setToTitreRelationship(EOProgrammeTitre value);
	
	EOChapitre getToChapitre();

	void setToChapitreRelationship(EOChapitre value);

	EOChapitreArticle getToChapitreArticle();

	void setToChapitreArticleRelationship(EOChapitreArticle value);
	
	List<? extends IEmploiCategorie> getListeEmploiCategories();
	
	List<? extends IEmploiSpecialisation> getListeEmploiSpecialisations();
	
	List<? extends IEmploiLocalisation> getListeEmploiLocalisations();
	
	List<? extends IEmploiNatureBudget> getListeEmploiNatureBudgets();
}
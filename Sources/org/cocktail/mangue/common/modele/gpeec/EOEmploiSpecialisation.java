package org.cocktail.mangue.common.modele.gpeec;

import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOCnu;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EODiscSecondDegre;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOReferensEmplois;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteAtos;
import org.cocktail.mangue.common.modele.nomenclatures.specialisations.EOSpecialiteItarf;
import org.cocktail.mangue.common.utilities.DateCtrl;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe des spécialisation d'emploi
 * 
 * @author Chama LAATIK
 * 
 */
public class EOEmploiSpecialisation extends _EOEmploiSpecialisation implements IEmploiSpecialisation {

	private static final long serialVersionUID = 3946455461505225744L;

	public static final String LIBELLE_SPEC_KEY = "libelleSpecialisation";

	/**
	 * Constructeur
	 */
	public EOEmploiSpecialisation() {
		super();
	}

	/**
	 * @return le libellé de la spécialisation avec la date
	 */
	public String libelleSpecialisationAffiche() {
		return libelleSpecialisation() + getDatesEmploi();
	}
	
	/**
	 * @return le libellé de la spécialisation
	 */
	public String libelleSpecialisation() {
		String libelle = "";
		
		if (getToEmploiReferens() != null) {
			libelle = getLibelleReferens();
		} else 
			if (getToDisciplineSdDegre() != null) {
			libelle = getLibelleDisciplineSdDegre();
		} else 
			if (getToCnu() != null) {
			libelle = getLibelleCNU();
		} else 
			if (getToSpecialiteItarf() != null) {
			libelle = getLibelleItarf();
		} else 
			if (getToSpecialiteAtos() != null) {
			libelle = getLibelleAtos();
		}
		
		return libelle;
	}
	
	private String getLibelleReferens() {
		return "REFERENS : " + getToEmploiReferens().codeEtLibelle();
	}
	
	private String getLibelleDisciplineSdDegre() {
		return "DISC : " + getToDisciplineSdDegre().codeEtLibelle();
	}
	
	private String getLibelleCNU() {
		return "CNU : " + getToCnu().codeEtLibelle();
	}
	
	private String getLibelleItarf() {
		return "ITARF : " + getToSpecialiteItarf().code() + " - " + getToSpecialiteItarf().libelleLong();
	}
	
	private String getLibelleAtos() {
		return "ATOS : " + getToSpecialiteAtos().code() + " - " + getToSpecialiteAtos().libelleLong();
	}	
	
	/**
	 * @return les dates de début et de fin (si renseigné) de la spécialisation de l'emploi
	 */
    public String getDatesEmploi() {
    	String libelle = "";
    	if (getDateFin() != null) {
    		libelle += " (" + DateCtrl.dateToString(this.getDateDebut()) + " - " + DateCtrl.dateToString(this.getDateFin()) + ")";
    	} else {
    		libelle += " depuis le " + DateCtrl.dateToString(this.getDateDebut());
    	}
    	
    	return libelle;
    }

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() {
		super.validateForDelete();
	}

	/**
	 * Peut etre appelé à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	public void validateBeforeTransactionSave() {
		setDateModification(new NSTimestamp());
	}

	public Integer getPersonneCreation() {
		return personneCreation();
	}

	public Integer getPersonneModification() {
		return personneModification();
	}

	public NSTimestamp getDateCreation() {
		return dateCreation();
	}

	public NSTimestamp getDateModification() {
		return dateModification();
	}

	public NSTimestamp getDateDebut() {
		return dateDebut();
	}

	public NSTimestamp getDateFin() {
		return dateFin();
	}

	public IEmploi getToEmploi() {
		return toEmploi();
	}

	/**
	 * Setter pour un emploi
	 * 
	 * @param value : un emploi {@link IEmploi}
	 **/
	public void setToEmploiRelationship(IEmploi value) {
		super.setToEmploiRelationship((EOEmploi) value);
	}

	public EOCnu getToCnu() {
		return toCnu();
	}

	public EODiscSecondDegre getToDisciplineSdDegre() {
		return toDisciplineSdDegre();
	}

	public EOReferensEmplois getToEmploiReferens() {
		return toEmploiReferens();
	}

	public EOSpecialiteAtos getToSpecialiteAtos() {
		return toSpecialiteAtos();
	}

	public EOSpecialiteItarf getToSpecialiteItarf() {
		return toSpecialiteItarf();
	}

}

package org.cocktail.mangue.common.modele.finder;

import java.util.Enumeration;

import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.modele.interfaces.INomenclature;
import org.cocktail.mangue.common.modele.manager.Manager;
import org.cocktail.mangue.common.modele.nomenclatures.Nomenclature;
import org.cocktail.mangue.common.utilities.CocktailConstantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class NomenclatureFinder {

	public static String TEM_VALIDE_KEY = "temValide";
	
	public Manager manager;
	
	public NomenclatureFinder(Manager manager) {
		this.manager = manager;
	}
	
    public static EOQualifier getQualifierValidite() {
    	return CocktailFinder.getQualifierEqual(TEM_VALIDE_KEY, CocktailConstantes.VRAI);
    }

	/**
	 * 
	 * @param edc
	 * @param entite
	 * @return
	 */
	@Deprecated
	public static NSArray<INomenclature> findStatic(EOEditingContext edc, String entite) {
		return NomenclatureFinder.find(edc, entite, INomenclature.SORT_ARRAY_LIBELLE);
	}
	
	public NSArray<INomenclature> find(String entite) {
		return NomenclatureFinder.find(manager.getEdc(), entite, INomenclature.SORT_ARRAY_LIBELLE);
	}

	/**
	 * 
	 * @param ec
	 * @param nomEntite
	 * @return
	 */
	public static NSArray<INomenclature> findWithQualifier(EOEditingContext edc, String entite, EOQualifier qualifier) {
		return NomenclatureFinder.findWithQualifier(edc, entite, qualifier, Nomenclature.SORT_ARRAY_LIBELLE);
	}

	/**
	 * 
	 * @param ec
	 * @param nomEntite
	 * @return
	 */
	public static NSArray<INomenclature> find(EOEditingContext edc, String entite, NSArray<EOSortOrdering> sorts) {
	
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			EOFetchSpecification fs = new EOFetchSpecification( entite ,new EOAndQualifier(qualifiers), sorts);					
			return edc.objectsWithFetchSpecification(fs);
	
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param ec
	 * @param nomEntite
	 * @return
	 */
	public static NSArray<INomenclature> findWithQualifier(EOEditingContext edc, String entite, EOQualifier qualifier , NSArray<EOSortOrdering> sorts) {
	
		try {
			EOFetchSpecification fs = new EOFetchSpecification( entite , qualifier, sorts);					
			return edc.objectsWithFetchSpecification(fs);
	
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param edc
	 * @param entite
	 * @param code
	 * @return
	 */
	public static INomenclature findForCode(EOEditingContext edc, String entite, String code) {
	
		try {
			EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + "=%@", new NSArray(code));
			EOFetchSpecification fs = new EOFetchSpecification(entite, qualifier, null);					
			return (INomenclature) edc.objectsWithFetchSpecification(fs).get(0);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param ec
	 * @param types
	 * @return
	 */
	public static NSArray<INomenclature> findForCodes(EOEditingContext edc, String entite, NSArray<String> codes) {
	
		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
	
		for (Enumeration<String> e = codes.objectEnumerator();e.hasMoreElements();)
			orQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(INomenclature.CODE_KEY + "=%@", new NSArray(e.nextElement())));
	
		EOFetchSpecification fs = new EOFetchSpecification( entite , new EOOrQualifier(orQualifiers), null);					
		return edc.objectsWithFetchSpecification(fs);
	
	}

	/**
	 * 
	 * @param edc
	 * @param entite
	 * @param code
	 * @return
	 */
	public static boolean isNomenclatureExistante(EOEditingContext edc, String entite, String code) {
		return findForCode(edc, entite, code) != null;
	}

}

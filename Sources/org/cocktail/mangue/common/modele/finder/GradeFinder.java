package org.cocktail.mangue.common.modele.finder;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.application.common.utilities.CocktailConstantes;
import org.cocktail.application.common.utilities.CocktailFinder;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.modele.nomenclatures.contrat.EOTypeContratTravail;
import org.cocktail.mangue.modele.grhum.EOCorps;
import org.cocktail.mangue.modele.grhum.EOGrade;
import org.cocktail.mangue.modele.grhum.referentiel.EOIndividu;
import org.cocktail.mangue.modele.mangue.individu.EOContrat;
import org.cocktail.mangue.modele.mangue.individu.EOContratAvenant;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe de recherche des avenants de contrat
 * 
 * @author Chama LAATIK
 */
public final class GradeFinder extends CocktailFinder {

    /** L'instance singleton de cette classe */
    private static GradeFinder sharedInstance;
    

    private GradeFinder() {
        // Pas d'instanciation possible
    }

    /**
     * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
     * 
     * Ex : ContratAvenantFinder.sharedInstance().methode
     * 
     * @return une instance de la classe
     */
    public static GradeFinder sharedInstance() {
        if (sharedInstance == null) {
            sharedInstance = new GradeFinder();
        }
        return sharedInstance;
    }
        
    public static EOQualifier getQualifierValiditeForDate(NSTimestamp dateReference) {

		if (dateReference != null) {
			
			NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
			NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

			orQualifiers.removeAllObjects();
			orQualifiers.addObject(getQualifierNullValue(EOGrade.D_FERMETURE_KEY));
			orQualifiers.addObject(getQualifierAfterEq(EOGrade.D_FERMETURE_KEY, dateReference));
			andQualifiers.addObject(new EOOrQualifier(orQualifiers));

			orQualifiers.removeAllObjects();
			orQualifiers.addObject(getQualifierNullValue(EOGrade.D_OUVERTURE_KEY));
			orQualifiers.addObject(getQualifierBeforeEq(EOGrade.D_OUVERTURE_KEY, dateReference));
			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
			
			return new EOAndQualifier(andQualifiers);
		}

		return null;
    }
    
	/** Recherche les grades fermes ou non
	 * @param editingContext
	 * @param aDateFermeture true si selection des grades avec une date de fermeture
	 */
	public NSArray<EOGrade> findForDate(EOEditingContext edc, NSTimestamp dateReference, boolean gestionHU) {

		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();

		if(!gestionHU) {
			orQualifiers.addObject(getQualifierEqual(EOGrade.TO_CORPS_KEY+"."+EOCorps.TO_TYPE_POPULATION_KEY + "." + EOTypePopulation.TEM_HOSPITALIER_KEY, "N"));
			orQualifiers.addObject(getQualifierNullValue(EOGrade.TO_CORPS_KEY+"."+EOCorps.TO_TYPE_POPULATION_KEY));
			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		
		andQualifiers.addObject(getQualifierValiditeForDate(dateReference));
		
		return EOGrade.fetchAll(edc, new EOAndQualifier(andQualifiers));
	}
    
}
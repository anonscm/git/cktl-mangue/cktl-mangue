package org.cocktail.mangue.common.modele.conges.interfaces;

import com.webobjects.foundation.NSTimestamp;

public interface IConge {

	public abstract NSTimestamp dateDebut();

	public abstract NSTimestamp dateFin();

	public abstract NSTimestamp dCreation();

	public abstract NSTimestamp dModification();

	public abstract String commentaire();

	public abstract org.cocktail.mangue.modele.grhum.referentiel.EOIndividu individu();

	public abstract org.cocktail.mangue.modele.mangue.individu.EOAbsences absence();
	
	
	
}
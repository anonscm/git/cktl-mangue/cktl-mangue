package org.cocktail.mangue.common.modele.manager;

import org.cocktail.application.client.tools.UserInfoCocktail;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.mangue.client.nomenclatures.NomenclaturesCtrl;
import org.cocktail.mangue.common.modele.finder.NomenclatureFinder;
import org.cocktail.mangue.modele.mangue.EOAgentPersonnel;

import com.webobjects.eocontrol.EOEditingContext;

public class Manager {
	
	private NomenclatureFinder nomenclatureFinder;
	private EOEditingContext edc;
	private EOAgentPersonnel utilisateur;
	private UserInfoCocktail userInfo;
	
	private NomenclaturesCtrl ctrlNomenclatures;
	
	public Manager() {
		nomenclatureFinder = new NomenclatureFinder(this);
	}
	
	
	public NomenclatureFinder getNomenclatureFinder() {
		return nomenclatureFinder;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}


	public UserInfoCocktail getUserInfo() {
		return userInfo;
	}


	public void setUserInfo(UserInfoCocktail userInfo) {
		this.userInfo = userInfo;
	}


	public EOAgentPersonnel getUtilisateur() {
		return utilisateur;
	}


	public void setUtilisateur(EOAgentPersonnel utilisateur) {
		this.utilisateur = utilisateur;
	}


	public NomenclaturesCtrl getCtrlNomenclatures() {
		return ctrlNomenclatures;
	}


	public void setCtrlNomenclatures(NomenclaturesCtrl ctrlNomenclatures) {
		this.ctrlNomenclatures = ctrlNomenclatures;
	}

	
	
}

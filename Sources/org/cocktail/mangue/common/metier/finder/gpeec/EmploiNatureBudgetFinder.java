package org.cocktail.mangue.common.metier.finder.gpeec;


import org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiNatureBudget;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Classe de recherche des natures budget pour un emploi 
 * 
 * @author Chama LAATIK
 */
public final class EmploiNatureBudgetFinder {
	
	/** L'instance singleton de cette classe */
	private static EmploiNatureBudgetFinder sharedInstance;

	private EmploiNatureBudgetFinder() {
		// Pas d'instanciation possible
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiNatureBudgetFinder.sharedInstance().methode
	 * 
	 * @return une instance de la classe
	 */
	public static EmploiNatureBudgetFinder sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiNatureBudgetFinder();
		}
		return sharedInstance;
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des périodes de nature budget pour un emploi donné
	 */
	@SuppressWarnings("unchecked")
	public NSArray<IEmploiNatureBudget> findForEmploi(EOEditingContext edc, IEmploi emploi) {
		try {
			EOQualifier qualifier; 
			if (emploi != null) {
				qualifier = EOQualifier.qualifierWithQualifierFormat(EOEmploiNatureBudget.TO_EMPLOI_KEY + " = %@", new NSArray<IEmploi>(emploi));
				return EOEmploiNatureBudget.fetchAll(edc, qualifier, getSortDateDebutDesc());
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<IEmploiNatureBudget>();
		}
		
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des catégories pour un emploi donné
	 */
	public IEmploiNatureBudget findEmploiNatureBudgetCourant(EOEditingContext edc, IEmploi emploi) {
		try {
			EOQualifier qualifier; 
			if (emploi != null) {
				qualifier = EOQualifier.qualifierWithQualifierFormat(EOEmploiNatureBudget.TO_EMPLOI_KEY + " = %@", new NSArray<IEmploi>(emploi));
				return EOEmploiNatureBudget.fetchFirstByQualifier(edc, qualifier, getSortDateDebutDesc());
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * @return tri ascendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutAsc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOEmploiNatureBudget.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareAscending));
	}
	
	/**
	 * @return tri descendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutDesc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOEmploiNatureBudget.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareDescending));
	}

}
package org.cocktail.mangue.common.metier.finder.gpeec;


import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiLocalisation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe de recherche des affectations pour un emploi 
 * 
 * @author Chama LAATIK
 */
public final class EmploiLocalisationFinder {
	
	/** L'instance singleton de cette classe */
	private static EmploiLocalisationFinder sharedInstance;

	private EmploiLocalisationFinder() {
		// Pas d'instanciation possible
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiAffectationFinder.sharedInstance().methode
	 * 
	 * @return une instance de la classe
	 */
	public static EmploiLocalisationFinder sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiLocalisationFinder();
		}
		return sharedInstance;
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des périodes d'affectations pour un emploi donné
	 */
	@SuppressWarnings("unchecked")
	public NSArray<IEmploiLocalisation> findForEmploi(EOEditingContext edc, IEmploi emploi) {
		try {
			EOQualifier qualifier; 
			if (emploi != null) {
				qualifier = EOQualifier.qualifierWithQualifierFormat(EOEmploiLocalisation.TO_EMPLOI_KEY + " = %@", new NSArray<IEmploi>(emploi));
				return EOEmploiLocalisation.fetchAll(edc, qualifier, getSortDateDebutDesc());
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<IEmploiLocalisation>();
		}
		
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des affectations courantes pour un emploi donné
	 */
	@SuppressWarnings("unchecked")
	public NSArray<IEmploiLocalisation> findListeAffectationsCourantes(EOEditingContext edc, IEmploi emploi) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			
			if ((emploi != null)) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploiLocalisation.TO_EMPLOI_KEY + " = %@", new NSArray<IEmploi>(emploi)));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploiLocalisation.DATE_DEBUT_KEY + " <= %@", 
						new NSArray<NSTimestamp>(DateCtrl.now())));
				//Date de fin est null ou égale à la date de suppression de l'emploi
				if (emploi.getDateFermetureEmploi() != null) {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploiLocalisation.DATE_FIN_KEY + " = nil OR " + EOEmploiLocalisation.DATE_FIN_KEY + " =%@", 
							new NSArray<NSTimestamp>(emploi.getDateFermetureEmploi())));
				} else {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploiLocalisation.DATE_FIN_KEY + " = nil OR " + EOEmploiLocalisation.DATE_FIN_KEY + " >=%@", 
							new NSArray<NSTimestamp>(DateCtrl.now())));
				}
				
				return EOEmploiLocalisation.fetchAll(edc, new EOAndQualifier(qualifiers), getSortDateDebutDesc());
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<IEmploiLocalisation>();
		}
	}
	
	/**
	 * @return tri ascendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutAsc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOEmploiLocalisation.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareAscending));
	}
	
	/**
	 * @return tri descendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutDesc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOEmploiLocalisation.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareDescending));
	}

}
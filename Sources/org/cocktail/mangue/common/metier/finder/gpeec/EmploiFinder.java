package org.cocktail.mangue.common.metier.finder.gpeec;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.gpeec.EOEmploi;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiLocalisation;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiNatureBudget;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.EOCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.EORne;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCategorieEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EOCorpsCategEmploi;
import org.cocktail.mangue.common.modele.nomenclatures.emploi.EONatureBudget;
import org.cocktail.mangue.common.utilities.CocktailConstantes;
import org.cocktail.mangue.common.utilities.DateCtrl;
import org.cocktail.mangue.modele.PeriodePourIndividu;
import org.cocktail.mangue.modele.SuperFinder;
import org.cocktail.mangue.modele.grhum.referentiel.EOStructure;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe de recherche des emplois
 * 
 * @author Chama LAATIK
 */
public final class EmploiFinder {

	/** L'instance singleton de cette classe */
	private static EmploiFinder sharedInstance;

	private EmploiFinder() {
		// Pas d'instanciation possible
	}

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiFinder.sharedInstance().methode
	 * 
	 * @return une instance de la classe
	 */
	public static EmploiFinder sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiFinder();
		}
		return sharedInstance;
	}

	/** 
	 * @return liste des témoins de durabilité en toute lettre pour l'affichage
	 */
	public List<String> getListeDurabiliteEnLettre() {
		List<String> listeDurabilite = new ArrayList<String>();
		listeDurabilite.add(IEmploi.PERMANENT);
		listeDurabilite.add(IEmploi.TEMPORAIRE);
		return listeDurabilite;
	}

	/** 
	 * @return liste des états de l'emploi
	 */
	public List<String> getListeEtats() {
		List<String> listeEtats = new ArrayList<String>();
		listeEtats.add(IEmploi.EN_COURS);
		listeEtats.add(IEmploi.OCCUPES);
		listeEtats.add(IEmploi.VACANTS);
		listeEtats.add(IEmploi.ROMPUS);
		listeEtats.add(IEmploi.FERMES);
		listeEtats.add(IEmploi.FUTURS);
		listeEtats.add(IEmploi.TOUS);
		return listeEtats;
	}

	/**
	 * @param edc : editingContext
	 * @param qualifier : le qualifier pour filtrer
	 * @return Liste des emplois valides filtrés
	 */
	@SuppressWarnings("unchecked")
	public NSArray<IEmploi> rechercherTousLesEmploisAvecFiltre(EOEditingContext edc, EOQualifier qualifier) {
		try {
			//On prefetche les relations pour filtrer plus rapidement
			NSMutableArray<String> prefetches = new NSMutableArray<String>();
			prefetches.addObject(EOEmploi.LISTE_EMPLOI_CATEGORIES_KEY);
			prefetches.addObject(EOEmploi.LISTE_EMPLOI_LOCALISATIONS_KEY);
			prefetches.addObject(EOEmploi.LISTE_EMPLOI_NATURE_BUDGETS_KEY);

			return EOEmploi.fetchAll(edc, qualifier, getSortNoEmploiLocalAsc(), prefetches);
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<IEmploi>();
		}
	}

	/**
	 * Renvoie qualifier des emplois pour la catégorie d'emploi recherchée
	 * @param uneCategorieEmploi : catégorie d'emploi recherchée
	 * @return qualifier
	 */
	public EOQualifier qualifierEmploiParCategorieEmploi(EOCategorieEmploi uneCategorieEmploi) {
		return EOQualifier.qualifierWithQualifierFormat(EOEmploi.LISTE_EMPLOI_CATEGORIES_KEY + "." + EOEmploiCategorie.TO_CATEGORIE_EMPLOI_KEY 
				+ " contains %@", new NSArray<EOCategorieEmploi>(uneCategorieEmploi));
	}

	/**
	 * Renvoie qualifier des emplois pour la structure d'affectation recherchée
	 * 
	 * Les sous structures doivent etre incluses dans la recherche
	 * 
	 * @param uneStructure : structure d'affectation recherchée
	 * @return qualifier
	 */
	public EOQualifier qualifierEmploiParLocalisation(EOStructure uneStructure) {
		NSMutableArray<EOQualifier> listeQualifier = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOStructure> liste = new NSMutableArray<EOStructure>(uneStructure.toutesStructuresFils());
		liste.add(uneStructure);

		//On construit le qualifier pour récupérer la liste des structures filles manuellement
		for (EOStructure structure : liste) {
			listeQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploi.LISTE_EMPLOI_LOCALISATIONS_KEY + "."
					+ EOEmploiLocalisation.TO_STRUCTURE_KEY + " contains %@", new NSArray<EOStructure>(structure)));
		}

		return new EOOrQualifier(listeQualifier);
	}

	/**
	 * Renvoie qualifier des emplois pour la nature de budget recherchée
	 * @param uneNatureBudget : nature budget recherchée
	 * @return qualifier
	 */
	public EOQualifier qualifierEmploiParNatureBudget(EONatureBudget uneNatureBudget) {
		return EOQualifier.qualifierWithQualifierFormat(EOEmploi.LISTE_EMPLOI_NATURE_BUDGETS_KEY + "." 
				+ EOEmploiNatureBudget.TO_NATURE_BUDGET_KEY + "  contains %@", new NSArray<EONatureBudget>(uneNatureBudget));
	}

	/**
	 * Renvoie qualifier des emplois pour la période recherchée
	 * @param dateDebut : date de début recherché
	 * @param dateFin : date de fin recherché
	 * @return qualifier
	 */
	public EOQualifier qualifierEmploiParPeriode(NSTimestamp dateDebut, NSTimestamp dateFin) {
		return SuperFinder.qualifierPourPeriode(EOEmploi.DATE_EFFET_EMPLOI_KEY, dateDebut, EOEmploi.DATE_FERMETURE_EMPLOI_KEY, dateFin);
	}

	/**
	 * Renvoie qualifier des emplois pour le témoin enseignant recherché
	 * @param unTemoinEnseignant : témoin enseignant recherché (Oui/Non)
	 * @return qualifier
	 */
	public EOQualifier qualifierEmploiParTemoinEnseignant(String unTemoinEnseignant) {
		return EOQualifier.qualifierWithQualifierFormat(EOEmploi.TEM_ENSEIGNANT_KEY + "=%@", 
				new NSArray<String>(unTemoinEnseignant));
	}

	/**
	 * Renvoie qualifier des emplois pour le numéro d'emploi recherché
	 * @param unNumeroEmploi : numéro d'emploi recherché
	 * @param isUtilisationNumFormatte : Est-ce que l'établissement utilise les numéros formattés?
	 * @return qualifier
	 */
	public EOQualifier qualifierEmploiParNoEmploi(String unNumeroEmploi, boolean isUtilisationNumFormatte) {	
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploi.NO_EMPLOI_KEY + " caseInsensitiveLike %@", 
				new NSArray<String>("*" + unNumeroEmploi + "*")));

		//Si utilisation du numéro formatté, on recherche dessus aussi
		if (isUtilisationNumFormatte) {
			qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploi.NO_EMPLOI_FORMATTE_KEY + " caseInsensitiveLike %@", 
					new NSArray<String>("*" + unNumeroEmploi + "*")));
		}

		return new EOOrQualifier(qualifiers);
	}

	/**
	 * Renvoie qualifier des emplois pour le code UAI de l'établissement recherché
	 * @param unUAI : code UAI de l'établissement recherché
	 * @return qualifier
	 */
	public EOQualifier qualifierEmploiParImplantationUAI(EORne unUAI) {
		return EOQualifier.qualifierWithQualifierFormat(EOEmploi.TO_RNE_KEY + "=%@", 
				new NSArray<EORne>(unUAI));
	}

	/**
	 * @param emplois : liste des emplois 
	 * @param myTypePopulation : type de population recherché
	 * @return liste des emplois filtrés par le type de population recherché
	 */
	public NSArray<IEmploi> filtrerParTypePopulation(NSArray<IEmploi> emplois, EOTypePopulation myTypePopulation) {
		if (myTypePopulation != null) {
			LogManager.logDetail("Type de population selectionné : " + myTypePopulation.code());
			NSMutableArray<IEmploi> emploisFiltres = new NSMutableArray<IEmploi>();

			for (IEmploi emploi : emplois) {
				try {
					for (IEmploiCategorie emploiCategorie : emploi.getListeEmploiCategories()) {
						//On récupère la correspondance entre le corps et la catégorie d'emploi
						EOCorpsCategEmploi myCorpsCategEmploi = (EOCorpsCategEmploi) (emploiCategorie.getToCategorieEmploi()
								.toListeCorpsCategEmploi()).get(0);

						if (myTypePopulation.code().equals(myCorpsCategEmploi.toCorps().toTypePopulation().code())
								&& (!emploisFiltres.containsObject(emploi))) {
							emploisFiltres.addObject(emploi);
						}
					}
				} catch (Exception e) {
				}
			}
			return emploisFiltres;
		} else {
			return emplois;
		}
	}

	/**
	 * @param emplois : liste des emplois 
	 * @param myCategFonctionPublique : catégorie de la fonction publique recherchée
	 * @return liste des emplois filtrés par la catégorie de la fonction publique recherchée
	 */
	public NSArray<IEmploi> filtrerParCategorieFonctionPublique(NSArray<IEmploi> emplois, EOCategorie myCategFonctionPublique) {
		if (myCategFonctionPublique != null) {
			LogManager.logDetail("Catégorie de la fonction publique sélectionné : " + myCategFonctionPublique.code());
			NSMutableArray<IEmploi> emploisFiltres = new NSMutableArray<IEmploi>();

			for (IEmploi emploi : emplois) {
				try {
					for (IEmploiCategorie emploiCategorie : emploi.getListeEmploiCategories()) {
						//On récupère la correspondance entre le corps et la catégorie d'emploi
						EOCorpsCategEmploi myCorpsCategEmploi = (EOCorpsCategEmploi) (emploiCategorie.getToCategorieEmploi()
								.toListeCorpsCategEmploi()).get(0);

						if (myCategFonctionPublique.equals(myCorpsCategEmploi.toCorps().toCategorie())
								&& (!emploisFiltres.containsObject(emploi))) {
							emploisFiltres.addObject(emploi);
						}
					}
				} catch (Exception e) {	
					e.printStackTrace();
				}
			}
			return emploisFiltres;
		} else {
			return emplois;
		}
	}

	/**
	 * @param emplois : liste des emplois 
	 * @param myCategFonctionPublique : catégorie de la fonction publique recherchée
	 * @return liste des emplois filtrés par la catégorie de la fonction publique recherchée pour les 
	 * types de population non enseignants
	 */
	public NSArray<IEmploi> filtrerParCategFonctionPubliquePourNonEnseignant(NSArray<IEmploi> emplois, EOCategorie myCategFonctionPublique) {

		if (myCategFonctionPublique != null) {
			LogManager.logDetail("Type de population non enseignant");
			LogManager.logDetail("Catégorie de la fonction publique sélectionné : " + myCategFonctionPublique.code());
			NSMutableArray<IEmploi> emploisFiltres = new NSMutableArray<IEmploi>();

			for (IEmploi emploi : emplois) {
				try {
					for (IEmploiCategorie emploiCategorie : emploi.getListeEmploiCategories()) {
						//On récupère la correspondance entre le corps et la catégorie d'emploi
						EOCorpsCategEmploi myCorpsCategEmploi = (EOCorpsCategEmploi) (emploiCategorie.getToCategorieEmploi()
								.toListeCorpsCategEmploi()).get(0);

						if ((!myCorpsCategEmploi.toCorps().toTypePopulation().estEnseignant())
								&& (myCategFonctionPublique.equals(myCorpsCategEmploi.toCorps().toCategorie())
										&& (!emploisFiltres.containsObject(emploi)))) {
							emploisFiltres.addObject(emploi);
						}
					}
				} catch (Exception e) {	
					e.printStackTrace();
				}
			}
			return emploisFiltres;
		} else {
			return emplois;
		}
	}

	/**
	 * Renvoie qualifier des emplois pour l'état recherché
	 * @param monEtat : état des emplois recherchés
	 * @param dateDebut : date de début recherché
	 * @param dateFin : date de fin recherché
	 * @return qualifier
	 */
	public EOQualifier qualifierEmploiParEtat(String monEtat, NSTimestamp dateDebut, NSTimestamp dateFin) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();

			//Emplois en cours (valides)
			if (IEmploi.EN_COURS.equals(monEtat)) {
				LogManager.logDetail("Recherche des emplois en cours");
				qualifiers.addObject(qualifierEmploiParEtatEnCours(dateDebut, dateFin));
			} 

			//Emplois fermés
			if (IEmploi.FERMES.equals(monEtat)) {
				LogManager.logDetail("Recherche des emplois fermés");
				qualifiers.addObject(qualifierEmploiParEtatFerme());
			}

			//Emplois futures
			if (IEmploi.FUTURS.equals(monEtat)) {
				LogManager.logDetail("Recherche des emplois futures");
				qualifiers.addObject(qualifierEmploiParEtatFuture(dateDebut));
			}

			//Tous les emplois
			if (IEmploi.TOUS.equals(monEtat)) {
				LogManager.logDetail("Recherche de tous les emplois");
				qualifiers.addObject(qualifierEmploiValide());
			}

			return new EOAndQualifier(qualifiers);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	 
	}

	/**
	 * @param dateDebut : date de début recherché
	 * @param dateFin : date de fin recherché
	 * @return qualifier des emplois en cours (valides)
	 */
	public EOQualifier qualifierEmploiParEtatEnCours(NSTimestamp dateDebut, NSTimestamp dateFin) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>(qualifierEmploiValide());
		qualifiers.addObject(qualifierEmploiParPeriode(dateDebut, dateFin));

		return new EOAndQualifier(qualifiers);
	}

	/**
	 * @return qualifier des emplois fermés
	 */
	public EOQualifier qualifierEmploiParEtatFerme() {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>(qualifierEmploiValide());
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploi.DATE_FERMETURE_EMPLOI_KEY + " != nil", null));

		return new EOAndQualifier(qualifiers);
	}

	/**
	 * @param dateReference : date de référence recherchée
	 * @return qualifier des emplois futures (valides) après la date de référence
	 */
	public EOQualifier qualifierEmploiParEtatFuture(NSTimestamp dateReference) {
		NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>(qualifierEmploiValide());
		qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploi.DATE_EFFET_EMPLOI_KEY  + " >=%@", new NSArray<NSTimestamp>(dateReference)));

		return new EOAndQualifier(qualifiers);
	}

	/**
	 * @return qualifier des emplois valides
	 */
	public EOQualifier qualifierEmploiValide() {
		return EOQualifier.qualifierWithQualifierFormat(EOEmploi.TEM_VALIDE_KEY + "=%@", new NSArray<String>(CocktailConstantes.VRAI));
	}

	/**
	 * @param edc : editing context
	 * @param monEtat : l'état recherché
	 * @param dateDebut : date de début recherché
	 * @param dateFin : date de fin recherché
	 * @return liste des emplois avec l'état recherché
	 */
	public NSArray<IEmploi> rechercherListeEmploisAvecEtat(EOEditingContext edc, String monEtat, NSTimestamp dateDebut, NSTimestamp dateFin) {
		try {
			NSArray<IEmploi> listeEmplois = null;

			if (IEmploi.VACANTS.equals(monEtat)) {
				LogManager.logDetail("Recherche des emplois vacants");
				listeEmplois = rechercherListeEmploisVacants(edc, dateDebut, dateFin);
			} else 

				if (IEmploi.ROMPUS.equals(monEtat)) {
					LogManager.logDetail("Recherche des emplois rompus");
					listeEmplois = rechercherEmploisRompus(edc, dateDebut, dateFin);
				} else

					if (IEmploi.OCCUPES.equals(monEtat)) {
						LogManager.logDetail("Recherche des emplois occupés");
						listeEmplois = rechercherListeEmploisOccupes(edc, dateDebut, dateFin);
					} else {
						//Emploi en cours, fermés ou tous
						listeEmplois = rechercherTousLesEmploisAvecFiltre(edc, qualifierEmploiParEtat(monEtat, dateDebut, dateFin));
					}

			LogManager.logDetail(" > Nombre des emplois : " + listeEmplois.size());

			return listeEmplois;
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<IEmploi>();
		}
	}

	/** 
	 * Recherche des emplois vacants (i.e non occupés)
	 * @param edc : editingContext
	 * @param dateDebut : date de début recherché
	 * @param dateFin : date de fin recherché
	 * @return liste des emplois vacants (pas d'occupation)
	 */
	@SuppressWarnings("unchecked")
	public NSArray<IEmploi> rechercherListeEmploisVacants(EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {
		// Tous les emplois ouverts pour la période
		NSMutableArray<IEmploi> listeEmploisVacants = new NSMutableArray<IEmploi>(rechercherTousLesEmploisAvecFiltre(edc, qualifierEmploiParEtatEnCours(dateDebut, dateFin)));
		// Toutes les occupations pour une période
		NSArray<EOOccupation> occupations = EOOccupation.findForPeriode(edc, dateDebut, dateFin);

		listeEmploisVacants.removeObjectsInArray((NSArray<IEmploi>) occupations.valueForKey(EOOccupation.TO_EMPLOI_KEY));

		LogManager.logDetail(" Nombre des emplois vacants :" + listeEmploisVacants.size());
		return listeEmploisVacants;
	}

	/** 
	 * Recherche des emplois occupés 
	 * @param edc : editingContext
	 * @param dateDebut : date de début recherché
	 * @param dateFin : date de fin recherché
	 * @return liste des emplois occupés à la date de référence
	 */
	public NSArray<IEmploi> rechercherListeEmploisOccupes(EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {
		// rechercher les emplois occupés
		NSArray<EOOccupation> listeOccupations = EOOccupation.findForPeriode(edc, dateDebut, dateFin);
		LogManager.logDetail("  > Total des d'occupations : " + listeOccupations.size());

		NSMutableArray<IEmploi> mesEmplois = new NSMutableArray<IEmploi>();
		for (EOOccupation myOccupation : listeOccupations) {
			if (!mesEmplois.containsObject(myOccupation.toEmploi())) {
				mesEmplois.addObject(myOccupation.toEmploi());
			}
		}

		LogManager.logDetail("  > Nombre d'emplois occupés : " + mesEmplois.size());
		return mesEmplois;
	}

	/** 
	 * Recherche des emplois rompus 
	 * 
	 * On parcourt la liste complete des occupations pour une periode donnee et on recupere les emplois dont la quotite occupee est < quotiteEmploi et > 0
	 * 
	 * @param edc : editingContext
	 * @param dateRefDebut : debut de la periode de référence
	 * @param dateRefFin : fin de la date de référence
	 * @return liste des emplois rompus
	 */
	public NSArray<IEmploi> rechercherEmploisRompus(EOEditingContext edc, NSTimestamp dateRefDebut, NSTimestamp dateRefFin) {
		NSArray<EOOccupation> occupations = EOOccupation.findForPeriode(edc, dateRefDebut, dateRefFin);
		NSMutableArray<EOSortOrdering> sorts = new NSMutableArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOOccupation.TO_EMPLOI_KEY + "." 
				+ EOEmploi.NO_EMPLOI_AFFICHAGE_KEY , EOSortOrdering.CompareAscending));
		sorts.addObject(PeriodePourIndividu.SORT_DATE_DEBUT);
		occupations = EOSortOrdering.sortedArrayUsingKeyOrderArray(occupations, sorts);

		NSMutableArray<IEmploi> listeEmploisRompus = new NSMutableArray<IEmploi>();
		NSMutableArray<EOOccupation> occupationsPourEmploi = null;
		IEmploi emploiCourant = null;	

		for (EOOccupation myOccupation : occupations) {
			try {
				if (myOccupation.toEmploi() == emploiCourant) {
					occupationsPourEmploi.addObject(myOccupation);
				} else {
					if (emploiCourant != null) {

						BigDecimal quotiteRestante = EOEmploi.calculerQuotiteRestanteMinimumSurPeriode(occupationsPourEmploi, dateRefDebut, dateRefFin);
						if (quotiteRestante.floatValue() > 0 && quotiteRestante.floatValue() < myOccupation.toEmploi().getQuotite().floatValue()) {
							listeEmploisRompus.addObject(emploiCourant);
						}
					}
					occupationsPourEmploi = new NSMutableArray<EOOccupation>();
					occupationsPourEmploi.addObject(myOccupation);
					emploiCourant = myOccupation.toEmploi();
				}
			}
			catch (Exception e) {

			}
		}
		// Prendre en compte le dernier emploi rencontré
		if (occupationsPourEmploi != null && occupationsPourEmploi.count() > 0) {
			//	changement d'emploi, si la quotite totale est 100, on n'ajoute pas l'emploi à la liste des rompus
			BigDecimal quotiteRestante = EOEmploi.calculerQuotiteRestanteMinimumSurPeriode(occupationsPourEmploi, dateRefDebut, dateRefFin);
			if (quotiteRestante.floatValue() > 0) {
				listeEmploisRompus.addObject(emploiCourant);
			}
		}

		return listeEmploisRompus;
	}

	/** 
	 * Recherche de tous les emplois en cours pendant une période
	 * @param edc : editingContext
	 * @param dateDebut : date de début recherché
	 * @param dateFin : date de fin recherché
	 * @return liste de tous les emplois en cours 
	 */
	public NSArray<IEmploi> rechercherListeTousEmplois(EOEditingContext edc, NSTimestamp dateDebut, NSTimestamp dateFin) {
		LogManager.logDetail("Affichage de tous les emplois en cours du : " + DateCtrl.dateToString(dateDebut) 
				+ " au " + DateCtrl.dateToString(dateFin));
		return rechercherTousLesEmploisAvecFiltre(edc, qualifierEmploiParEtatEnCours(dateDebut, dateFin));
	}

	/**
	 * @return tri ascendant selon le numéro de l'emploi
	 */
	public NSArray<EOSortOrdering> getSortNoEmploiLocalAsc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOEmploi.NO_EMPLOI_KEY, EOSortOrdering.CompareAscending));
	}
}
package org.cocktail.mangue.common.metier.finder.gpeec;


import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.modele.mangue.individu.EOOccupation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe de recherche des occupations 
 * 
 * @author Chama LAATIK
 */
public final class OccupationFinder {
	
	/** L'instance singleton de cette classe */
	private static OccupationFinder sharedInstance;

	private OccupationFinder() {
		// Pas d'instanciation possible
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : OccupationFinder.sharedInstance().methode
	 * 
	 * @return une instance de la classe
	 */
	public static OccupationFinder sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new OccupationFinder();
		}
		return sharedInstance;
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des périodes d'affectations pour un emploi donné
	 */
	@SuppressWarnings("unchecked")
	public NSArray<EOOccupation> findForEmploi(EOEditingContext edc, IEmploi emploi) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			
			if (emploi != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOccupation.TEM_VALIDE_KEY + " = 'O'", null));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOccupation.TO_EMPLOI_KEY + " = %@", new NSArray<IEmploi>(emploi)));
				
				return EOOccupation.fetchAll(edc, new EOAndQualifier(qualifiers), getSortDateDebutDesc());
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOOccupation>();
		}
		
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des occupations courantes pour un emploi donné
	 */
	@SuppressWarnings("unchecked")
	public NSArray<EOOccupation> findListeOccupationsCourantes(EOEditingContext edc, IEmploi emploi) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			
			if ((emploi != null)) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOccupation.TEM_VALIDE_KEY + " = 'O'", null));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOccupation.TO_EMPLOI_KEY + " = %@", new NSArray<IEmploi>(emploi)));
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOccupation.DATE_DEBUT_KEY + " <= %@", 
						new NSArray<NSTimestamp>(DateCtrl.now())));
				//Date de fin est null ou égale à la date de suppression de l'emploi
				if (emploi.getDateFermetureEmploi() != null) {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOccupation.DATE_FIN_KEY + " = nil OR " + EOOccupation.DATE_FIN_KEY + " =%@", 
							new NSArray<NSTimestamp>(emploi.getDateFermetureEmploi())));
				} else {
					qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOccupation.DATE_FIN_KEY + " = nil OR " + EOOccupation.DATE_FIN_KEY + " >=%@", 
							new NSArray<NSTimestamp>(DateCtrl.now())));
				}
				
				return EOOccupation.fetchAll(edc, new EOAndQualifier(qualifiers), getSortDateDebutDesc());
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<EOOccupation>();
		}
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des dernières occupations pour un emploi donné 
	 * 	=> occupations courantes sinon la dernière
	 */
	public NSArray<EOOccupation> findListeOccupations(EOEditingContext edc, IEmploi emploi) {
		NSArray<EOOccupation> listeOccupations = new NSMutableArray<EOOccupation>(findListeOccupationsCourantes(edc, emploi));
		
		if ((listeOccupations != null) && (listeOccupations.size() == 0)) {
			//Si on n'a pas d'occupation courante => on renvoie la dernière
			if ((findForEmploi(edc, emploi) != null) && (findForEmploi(edc, emploi).size() > 0)) {
				listeOccupations.add(findForEmploi(edc, emploi).get(0));
			}
		}
		
		return listeOccupations;
	}
	
	/**
	 * @return tri ascendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutAsc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOOccupation.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareAscending));
	}
	
	/**
	 * @return tri descendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutDesc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOOccupation.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareDescending));
	}

}
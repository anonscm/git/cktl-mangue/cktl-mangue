package org.cocktail.mangue.common.metier.finder.gpeec;


import org.cocktail.common.LogManager;
import org.cocktail.mangue.common.modele.gpeec.EOEmploiCategorie;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiCategorie;
import org.cocktail.mangue.common.modele.nomenclatures.carriere.EOTypePopulation;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe de recherche des catégories d'emploi pour un emploi 
 * 
 * @author Chama LAATIK
 */
public final class EmploiCategorieFinder {
	
	/** L'instance singleton de cette classe */
	private static EmploiCategorieFinder sharedInstance;

	private EmploiCategorieFinder() {
		// Pas d'instanciation possible
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiCategorieFinder.sharedInstance().methode
	 * 
	 * @return une instance de la classe
	 */
	public static EmploiCategorieFinder sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiCategorieFinder();
		}
		return sharedInstance;
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des périodes de catégorie pour un emploi donné
	 */
	@SuppressWarnings("unchecked")
	public NSArray<IEmploiCategorie> findForEmploi(EOEditingContext edc, IEmploi emploi) {
		try {
			if (emploi != null) {
				return EOEmploiCategorie.fetchAll(edc, qualifierPourEmploi(emploi), getSortDateDebutDesc());
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<IEmploiCategorie>();
		}
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return la catégorie courante pour un emploi donné
	 */
	public IEmploiCategorie findEmploiCategorieCourant(EOEditingContext edc, IEmploi emploi) {
		try {
			if (emploi != null) {
				return EOEmploiCategorie.fetchFirstByQualifier(edc, qualifierPourEmploi(emploi), getSortDateDebutDesc());
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @param dateReference : date de référence
	 * @return Liste des catégories pour un emploi donné
	 */
	public IEmploiCategorie findEmploiCategoriePourEmploiEtDate(EOEditingContext edc, IEmploi emploi, NSTimestamp dateReference) {
		try {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			
			qualifiers.addObject(qualifierPourEmploi(emploi));
			
			if (dateReference != null) {
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploiCategorie.DATE_DEBUT_KEY  + " <=%@", new NSArray<NSTimestamp>(dateReference)));
				
				qualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOEmploiCategorie.DATE_FIN_KEY + " = nil OR " 
						 + EOEmploiCategorie.DATE_FIN_KEY + " >=%@", new NSArray<NSTimestamp>(dateReference)));
				
				return EOEmploiCategorie.fetchFirstByQualifier(edc, new EOAndQualifier(qualifiers), getSortDateDebutDesc());
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @param dateReference : date de référence
	 * @return le type de population correspondant à un emploi
	 */
	public EOTypePopulation rechercherTypePopulationPourEmploi(EOEditingContext edc, IEmploi emploi, NSTimestamp dateReference) {
		IEmploiCategorie unEmploiCategorie = findEmploiCategoriePourEmploiEtDate(edc, emploi, dateReference);
		
		if (unEmploiCategorie == null) {
			unEmploiCategorie = findEmploiCategorieCourant(edc, emploi);
		}
		LogManager.logDetail("  > catégorie d'emploi : " + unEmploiCategorie);
		
		if ((unEmploiCategorie != null) && unEmploiCategorie.getToCategorieEmploi() != null) {
			return unEmploiCategorie.getToCategorieEmploi().typePopulation();
		}
		
		return null;
	}
	
	/**
	 * Renvoie qualifier des emplois catégories pour l'emploi recherché
	 * @param emploi : l'emploi voulu
	 * @return qualifier
	 */
	public EOQualifier qualifierPourEmploi(IEmploi emploi) {
		if (emploi != null) {
			return EOQualifier.qualifierWithQualifierFormat(EOEmploiCategorie.TO_EMPLOI_KEY + " = %@", new NSArray<IEmploi>(emploi));
		}
		return null;
	}
	
	/**
	 * @return tri ascendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutAsc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOEmploiCategorie.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareAscending));
	}
	
	/**
	 * @return tri descendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutDesc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOEmploiCategorie.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareDescending));
	}

}
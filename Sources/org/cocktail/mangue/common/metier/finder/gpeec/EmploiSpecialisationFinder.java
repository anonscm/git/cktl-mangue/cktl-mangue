package org.cocktail.mangue.common.metier.finder.gpeec;


import org.cocktail.mangue.common.modele.gpeec.EOEmploiSpecialisation;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploi;
import org.cocktail.mangue.common.modele.gpeec.interfaces.IEmploiSpecialisation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Classe de recherche des spécialisations pour un emploi 
 * 
 * @author Cyril PINSARD
 */
public final class EmploiSpecialisationFinder {
	
	/** L'instance singleton de cette classe */
	private static EmploiSpecialisationFinder sharedInstance;

	private EmploiSpecialisationFinder() {
		// Pas d'instanciation possible
	}
	
	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : EmploiSpecialisationFinder.sharedInstance().methode
	 * 
	 * @return une instance de la classe
	 */
	public static EmploiSpecialisationFinder sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new EmploiSpecialisationFinder();
		}
		return sharedInstance;
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des périodes de spécialisation pour un emploi donné
	 */
	@SuppressWarnings("unchecked")
	public NSArray<IEmploiSpecialisation> findForEmploi(EOEditingContext edc, IEmploi emploi) {
		try {
			EOQualifier qualifier; 
			if (emploi != null) {
				qualifier = EOQualifier.qualifierWithQualifierFormat(EOEmploiSpecialisation.TO_EMPLOI_KEY + " = %@", new NSArray<IEmploi>(emploi));
				return EOEmploiSpecialisation.fetchAll(edc, qualifier, getSortDateDebutDesc());
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray<IEmploiSpecialisation>();
		}
		
	}
	
	/**
	 * @param edc : editingContext
	 * @param emploi : l'emploi voulu
	 * @return Liste des spécialisations courantes pour un emploi donné
	 */
	public IEmploiSpecialisation findEmploiSpecialisationCourante(EOEditingContext edc, IEmploi emploi) {
		try {
			EOQualifier qualifier; 
			if (emploi != null) {
				qualifier = EOQualifier.qualifierWithQualifierFormat(EOEmploiSpecialisation.TO_EMPLOI_KEY + " = %@", new NSArray<IEmploi>(emploi));
				return EOEmploiSpecialisation.fetchFirstByQualifier(edc, qualifier, getSortDateDebutDesc());
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * @return tri ascendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutAsc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOEmploiSpecialisation.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareAscending));
	}
	
	/**
	 * @return tri descendant selon la date de début
	 */
	public NSArray<EOSortOrdering> getSortDateDebutDesc() {
		return new NSArray<EOSortOrdering>(EOSortOrdering.sortOrderingWithKey(EOEmploiSpecialisation.DATE_DEBUT_KEY, 
				EOSortOrdering.CompareDescending));
	}

}